<?php
    include_once __DIR__.'/Api/v1/openModel.php';
    $model = new openModel();
    $type = 'no_response';
    $auth_success_msg = $auth_failure_msg = '';
    $c_id = $s_id = $p_type = $intent_type = $setup_intent = $payment_intent = $intent_id = $onboardinglink_url = '';
    if (isset($_REQUEST['type']) && !empty($_REQUEST['type'])) {
        $type = $_REQUEST['type'];
    }
    if ($type === 'authenticate') {
        if (isset($_REQUEST['c_id']) && !empty($_REQUEST['c_id'])) {
            $c_id = $_REQUEST['c_id'];
        }
        if (isset($_REQUEST['s_id']) && !empty($_REQUEST['s_id'])) {
            $s_id = $_REQUEST['s_id'];
        }
        if (isset($_REQUEST['p_type']) && !empty($_REQUEST['p_type'])) {
            $p_type = $_REQUEST['p_type'];
        }
        if (isset($_REQUEST['payment_intent']) && !empty($_REQUEST['payment_intent'])) {
            $payment_intent = $_REQUEST['payment_intent'];
            $intent_type = "PI";
        }
        if (isset($_REQUEST['setup_intent']) && !empty($_REQUEST['setup_intent'])) {
            $setup_intent = $_REQUEST['setup_intent'];
            $intent_type = "SI";
        }
        if ($intent_type === "SI") {
            $intent_id = $setup_intent;
        } else {
            $intent_id = $payment_intent;
        }
        if (!empty($c_id) && !empty($s_id) && !empty($p_type) && !empty($intent_id) && !empty($intent_type)) {
            $intent_result = $model->getIntentDetails($c_id, $s_id, $p_type, $intent_id, $intent_type,1);
            if($intent_result['status'] === "Success"){
                $auth_success_msg = $intent_result['msg'];
                $type = '';
            }else{
                $auth_failure_msg = $intent_result['msg'];
                $type = '';
            }            
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $model->response($model->json($error), 400);
        }
    }
    if ($type === 'stripeOnboarding') {
        if (isset($_REQUEST['c_id']) && !empty($_REQUEST['c_id'])) {
            $c_id = $_REQUEST['c_id'];
        }
        if (!empty($c_id)) {
            $onboardinglink_result = $model->createStripeOnboardinglink($c_id);
            $onboardinglink_url = $onboardinglink_result['msg']['url'];
            if(!empty($onboardinglink_url)){
                header('Location: '.$onboardinglink_url);
                die;
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $model->response($model->json($error), 400);
        }
    }
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Stripe update details On-boarding response</title>
        <link href="custom.css" rel="stylesheet" type="text/css"/>
        <style>
            body{
                background: #f8fbfd;
            }
            .content{
                display: flex;
                flex-direction: column;
                justify-content: center;
                padding: 30px;
                max-width: 800px;
                height: 350px;
                width: 70%;
                margin: 100px auto;
                background: #fff;
                box-shadow: 0 1px 3px 0 rgba(50, 50, 93, 0.15);
                border-radius: 6px;
                font-family: avenir;
            }
            h1 {
                font-size: 42px;
                font-weight: 300;
                color: #6863d8;
                letter-spacing: 0.3px;
                margin-bottom: 30px;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
                -webkit-text-size-adjust: none;
                box-sizing: border-box;
            }
            .row{
                width:100%;
                display: flex;
            }
            .col-30{
                width:30%;
                padding: 20px;
                display: flex;
            }
            .col-50{
                width:50%;
                padding: 20px;
                display: flex;
            }
            .col-70{
                width:70%;
                padding: 20px;
                display: flex;
            }
            img{
                max-width: 200px;
                max-height: 200px;
                margin: auto;
            }
            @media only screen and (max-width: 800px) {
                .col-30, .col-70, .row{
                    width:100%;
                    display: block;
                }
                label:first-child{
                    font-size: 28px !important;
                }
                label:nth-child(2){
                    font-size: 20px !important;
                }
            }
        </style>
    </head>
    <body>
     <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.1/angular.min.js"></script>
     <script type="text/javascript" src="https://cdn.jsdelivr.net/ngstorage/0.3.6/ngStorage.min.js"></script>
     
    <script type="text/javascript">
        var app = angular.module('MyStripeApp', ["ngStorage"])
        app.controller('MyStripeController', function ($scope, $localStorage) {   
            $scope.handlePage = function(){
                var $response_url_type = "<?php echo $type ?>";
                var hosturl;
                if (window.location.hostname === 'www.mystudio.academy') {
                    hosturl = 'https://www.mystudio.academy/mystudioapp.php?pg=payment';
                } else if (window.location.hostname === 'dev.mystudio.academy') {
                     hosturl = 'http://dev.mystudio.academy/mystudioapp.php?pg=payment';
                } else if (window.location.hostname === 'dev2.mystudio.academy') {
                     hosturl = 'http://dev2.mystudio.academy/mystudioapp.php?pg=payment';
                } else if (window.location.hostname === 'stage.mystudio.academy') {
                    hosturl = 'http://stage.mystudio.academy/mystudioapp.php?pg=payment';
                } else if (window.location.hostname === 'beta.mystudio.academy') {
                    hosturl = 'https://beta.mystudio.academy/mystudioapp.php?pg=payment';
                } else if (window.location.hostname === 'localhost') {
                    hosturl = 'http://localhost/nila/Mystudio_webapp/mystudioapp.php?pg=payment';
                }  
                if ($response_url_type === "success") {
                      $localStorage.stripe_onboard_status = 'success';
                }
            };
        });
    </script>
    
        <div class="content"  ng-app="MyStripeApp" ng-controller="MyStripeController"  ng-init="handlePage()">
            <div class="row">
                <div class="col-30">
                    <img src="uploads/Default/square_logo.png" alt="logo"/>
                </div>
                <div class="col-70">
                    <div class="row">
                        <div id='failure' style="<?php if($type == "failure"){ ?> display :block; <?php }else{ ?> display :none; <?php } ?>"><br>
                            <label  style="font-size: 35px;color: #c74b3e;font-weight: 600;" >Failure!</label><br><br>
                            <label  style="font-size: 24px;color: red;font-weight: 500;" >On-boarding flow for stripe connected account failed.</label>
                        </div>
                        <div id='success' style="<?php if($type == "success"){ ?> display :block; <?php }else{ ?> display :none; <?php } ?>"><br>
                            <label  style="font-size: 35px;color: #019a61;font-weight: 600;" >Congrats!</label><br><br>
                            <label  style="font-size: 24px;color: #019a61;font-weight: 500;" >You are now set up to receive payouts to your bank account!<br><br>
                            Kindly close the current tab in your browser and go back to the previous tab to see the updates.</label>
                        </div>
                        <div id='authenticate_success' style="<?php if(empty($auth_success_msg)){ ?> display :none; <?php }else{ ?> display :block; <?php } ?>"><br>
                            <label  style="font-size: 35px;color: #019a61;font-weight: 600;" >Success!</label><br><br>
                            <label  style="font-size: 24px;color: #019a61;font-weight: 500;" ><?php echo $auth_success_msg; ?></label>
                        </div>
                        <div id='authenticate_failure' style="<?php if(empty($auth_failure_msg)){ ?> display :none; <?php }else{ ?> display :block; <?php } ?>"><br>
                            <label  style="font-size: 35px;color: #c74b3e;font-weight: 600;" >Failure!</label><br><br>
                            <label  style="font-size: 24px;color: red;font-weight: 500;" ><?php echo $auth_failure_msg; ?></label>
                        </div>
                        <div id='no_response' style="<?php if($type == "no_response"){ ?> display :block; <?php }else{ ?> display :none; <?php } ?>"><br>
                            <label  style="font-size: 35px;color: #c74b3e;font-weight: 600;" >No Response!</label><br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>
</html> 