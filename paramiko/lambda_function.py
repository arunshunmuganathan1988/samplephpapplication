import paramiko
import socket
def lambda_handler(event, context):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    privkey = paramiko.RSAKey.from_private_key_file('/var/www/html/paramiko/ms_dev_img_key121319.pem')
    hostname456 = socket.gethostname()
    print(hostname456)
    ssh.connect(
        hostname=hostname456, username='ec2-user', pkey=privkey
    )
    commands = [
        "/var/www/html/scripts/install_composer_dependencies.sh"
        ]
    for command in commands:
        print("Executing {}".format(command))
        stdin , stdout, stderr = ssh.exec_command(command)
        stdin.flush()
        print(stdout.read())
        print(stderr.read())
    
#    stdin, stdout, stderr = ssh.exec_command(
#        'echo "ssh to ec2 instance successful"')
#    stdin.flush()
#    data = stdout.read().splitlines()
#    for line in data:
#        print(line)

    ssh.close()
    # username is most likely 'ec2-user' or 'root' or 'ubuntu'
    #print "Connecting to " + host
    #c.connect( hostname = host, username = "ec2-user", pkey = k )
    #print "Connected to " + host
    
    return
    {
        'message' : "Script execution completed. See Cloudwatch logs for complete output"
    }
if __name__ == '__main__':
    lambda_handler('null', 'null')