/*jslint browser: true*/
/*global console, MyApp*/
//www
MyApp.angular.controller('IndexPageController', ['$rootScope', '$scope', '$http', 'urlservice', 'InitService', '$localStorage', '$window', function ($rootScope, $scope, $http, urlservice, InitService, $localStorage, $window) {
        'use strict';
      
        $scope.PIList = $scope.PSList = [];                    
         //No Internet - show alert
        $scope.istatus = function () {
            MyApp.alert('Please turn on internet', 'No Internet');
            $scope.preloaderVisible = false;
            return;
        };
        
        $scope.sendExpiryEmailToStudio = function(companyid){
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            $http({
                method: 'POST',
                url: urlservice.url + 'sendExpiryEmailToStudio',
                data: {
                    "company_id": companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status == 'Success') {
                    MyApp.alert(response.data.msg, '');
                } else {
                    MyApp.alert(response.data.msg, 'Failure');
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };
        
        // GET COMPANY DETAILS
        $scope.getCompanyDetails = function (studio_code,companyid) {
            $scope.companyid = companyid;
            $scope.studio_code = studio_code;
            $scope.fav_company_name = "";
            
            $http({
                method: 'GET',
                url: urlservice.url + 'publiccompanydetails',
                params: {
                    "companyid": companyid,
                    "studio_code":studio_code,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U',
                    "page_from": "L"
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                if (response.data.status == 'Success') {
                    // Load page:                                       
                    mcView.router.load({
                        pageName: 'lead_add'
                    });
                    $scope.reset_RequestInfoForm();
                    $scope.companydetails = response.data.company_details;
                    $scope.logoUrl = $scope.companydetails.logo_URL;
                    $scope.wp_level = $scope.companydetails.wp_level;
                    $scope.wp_client_id = $scope.companydetails.wp_client_id;
                    $scope.company_status = $scope.companydetails.upgrade_status;
                    $scope.fav_company_name = $scope.companydetails.company_name;
                    $scope.wp_currency_symbol = $scope.companydetails.wp_currency_symbol;
                    $scope.wp_currency_code = $scope.companydetails.wp_currency_code;
                    $scope.lead_enable_flag = $scope.companydetails.lead_enable_flag;
                    $scope.getPOSSettings(companyid);
                } else {
                   // PREVENT FURTHUR EXECUTION & BLOCK THE URL IF STUDIO EXPIRED
                    
                    if (response.data.upgrade_status && response.data.studio_expiry_level) {
                        $scope.cmpny_email = response.data.studio_email;
                        $scope.company_status = response.data.upgrade_status;
                        $scope.company_expiry_level = response.data.studio_expiry_level;
                    }

                    if (($scope.company_status && $scope.company_status === 'F') || ($scope.company_expiry_level && $scope.company_expiry_level === 'L2')) {
                        // PREVENT FURTHUR EXECUTION & BLOCK THE APP
                        MyApp.modal({
                            title: '',
                            afterText:
                                    '<br>' +
                                    '<div id="studioExpiryText" class="modal-text custom-modal-content">Please contact the business operator to activate this link.</div>' +
                                    '<br>' +
                                    '<button class="button custom-modal-green-button button-text modalresetButton" id="send_expiry_email"></button>'
                        });

                        if ($scope.cmpny_email) {
                            $("#send_expiry_email").text("Send email message");
                        } else {
                            $("#send_expiry_email").text("Ok");
                        }

                        $('#send_expiry_email ').click(function () {
                            $scope.$apply(function () {
                                if ($("#send_expiry_email").text() === 'Send email message') {
                                    $scope.sendExpiryEmailToStudio(companyid);
                                }
                                MyApp.closeModal();
                            });
                        });

                    } else {
                        if(response.data.session_status === "Expired"){
                            $scope.preloader = false;
                            $scope.preloaderVisible = false;
                            MyApp.alert('Session Expired','', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        }else{
                            MyApp.alert(response.data.msg, 'Message');
                        }                        
                    }
                }
                $scope.preloader = false;
                $scope.preloaderVisible = false;
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };        
        
        $scope.urlredirection = function () {
            $scope.param1 = $scope.param2 = $scope.param3 = $scope.param4 = '';
            var decoded_url = decodeURIComponent(window.location.href);
            var params = decoded_url.split('?=')[1].split('/');

            $scope.param1 = params[0];
            $scope.param2 = params[1];
            $scope.param3 = params[2];
            $scope.param4 = params[3];
            
            if(params[5]){
                if(params[5] === 'spos'){ //If staff
                    $scope.pos_user_type = 'S';
                    // ENABLE ADMIN BANNER, SEARCH MEMBER, OPTIONAL DISCOUNTS
                    $('#view-1').addClass('with-banner');
                }else{
                    $scope.pos_user_type = 'P'; //If public
                }
                if(params[6]){ //For access token
                    $scope.pos_token = params[6];
                }
                if(params[7]){
                   $scope.pos_entry_type = params[7];
                }
                $('#view-1').addClass('from-pos with-footer');
            }else{
                $scope.pos_user_type = '';
                $scope.pos_token = $scope.pos_entry_type = '';
            }
//            alert('param1: '+$scope.param1+' param2: '+$scope.param2+' param3: '+$scope.param3+' param4: '+$scope.param4+'  param5: '+params[5]);

            if ($scope.param1 !== undefined && $scope.param2 !== undefined && ($scope.param3 === undefined || $scope.param3 === '') && $scope.param4 === undefined) {
                $scope.getCompanyDetails($scope.param1,$scope.param2, '', '');
            } else if ($scope.param1 !== undefined && $scope.param2 !== undefined && $scope.param3 !== undefined && ($scope.param4 === undefined || $scope.param4 === '')) {
                if (isNaN($scope.param3)) {
                    $scope.getCompanyDetails($scope.param1,$scope.param2, '', '');
                } else {
                    $scope.getCompanyDetails($scope.param1,$scope.param2, $scope.param3, '');         // Is a number
                }
            } else if ($scope.param1 !== undefined && $scope.param2 !== undefined && $scope.param3 !== undefined && $scope.param4 !== undefined) {
                $scope.getCompanyDetails($scope.param1,$scope.param2, $scope.param3, $scope.param4);
            } else {
                window.location.href = window.location.origin;
            }
        };
        $scope.urlredirection();
        
        $scope.getPOSSettings = function(companyid){
           $scope.preloader = false;
            $scope.preloaderVisible = false;
            $http({
                method: 'GET',
                url: urlservice.url + 'getleadssettingsforpos',
                params: {
                    "company_id": companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token":  $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status === 'Success') {
                    $scope.PIList = response.data.msg.program_interest;
                    $scope.PSList = response.data.msg.lead_source;  
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;                  
                } else {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                    if(response.data.session_status === "Expired"){
                        MyApp.alert('Session Expired','', function () {
                            $localStorage.pos_user_email_id = "";
                            $scope.backToPOS();
                        });
                    }else{
                        MyApp.alert(response.data.msg, 'Message');
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert(response.data.msg, 'Failure');
            }); 
        };
        
        $scope.reset_RequestInfoForm = function(){              
            $scope.request_info_form.$setPristine();
            $scope.request_info_form.$setUntouched();
            $scope.request_info_First_Name = "";
            $scope.request_info_Last_Name = "";
            $scope.request_info_Email = "";
            $scope.request_info_Phone = "";
            $scope.program_interest = "";
            $scope.program_source = "";
            $('.form-group').removeClass('focused');
        }
        
        $scope.AddLead = function () {
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            var checkout_message1 = '';            
            
            if (!$scope.request_info_First_Name || !$scope.request_info_Last_Name || !$scope.request_info_Email)
            {
                checkout_message1 += "Buyer's Info:";
                if (!$scope.request_info_First_Name) {
                    checkout_message1 += '"First Name"<br>';
                }
                if (!$scope.request_info_Last_Name) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if (!$scope.request_info_Email) {
                    checkout_message1 += '"Email"<br>';
                }
            }
            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }
            
            $http({
                method: 'POST',
                url: urlservice.url + 'addLeadsMemberFromPOS',
                data: {
                    "company_id": $scope.companyid,
                    "buyer_first_name":$scope.request_info_First_Name,
                    "buyer_last_name":$scope.request_info_Last_Name,
                    "email":$scope.request_info_Email,
                    "phone":$scope.request_info_Phone,
                    "pi_id" :$scope.program_interest, 
                    "ls_id":$scope.program_source,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status === 'Success') {
                    MyApp.alert(response.data.msg,'Thank You', function () {
                        $scope.backToPOS();
                    });
                    $scope.getCompanyDetails($scope.studio_code,$scope.companyid);
                } else {
                    if(response.data.session_status === "Expired"){
                        MyApp.alert('Session Expired','', function () {
                            $localStorage.pos_user_email_id = "";
                            $scope.backToPOS();
                        });
                    }else{
                        MyApp.alert(response.data.msg, 'Failure');
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };
        $scope.backToPOS = function () {
            var hosturl;
            if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
               window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                       window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                    var origin = window.location.origin;
                    hosturl = origin;
                }

            } else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            }  else if (window.location.hostname === 'beta.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'https://' + www_url;
                }
            }   else if (window.location.hostname === 'stage.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'localhost') {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            } else {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            }
            var pos_type, pos_string, pos_entry_type;
            if ($scope.pos_user_type === 'S'){
                pos_type = 'spos';
                pos_string = 'e92213e6721525d7spos712627e6db9b98b4';
            }else if($scope.pos_user_type === 'P'){
                pos_type = 'ppos';
                pos_string = '8f2f44c8580cc72ppos700ff5d197b5a18f0';
            }else{
                pos_type = '';
                pos_string = '';
            }
            if($scope.pos_entry_type){
                pos_entry_type = $scope.pos_entry_type;
            }else{
                pos_entry_type = 'l';
            }
            if (pos_type) {
                $window.open(hosturl+'/' + 'pos' + '/?=' + $scope.param1 + '/' + $scope.param2 + '/' + pos_type + '-' + pos_string + '/'+pos_entry_type+'/', '_self');
            }
        };
        
        //DOCUMENT READY FUNCTION
        angular.element(document).ready(function () {
            // NEW ANIMATION FORM PLACEHOLDER TO LABELS
            $('input').focus(function () {
                $(this).parents('.form-group').addClass('focused');
            });

            $('input').blur(function () {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.form-group').removeClass('focused');
                } else {
                    $(this).addClass('filled');
                }
            });
        });
    }]);

