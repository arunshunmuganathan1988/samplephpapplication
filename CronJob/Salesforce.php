<?php

class Salesforce {

    private $api_key = '';
    private $api_secret = '';
    private $url = '';
    public $list_id = '';
    
    public function __construct() {
        if(isset($GLOBALS['HOST_NAME']) && !empty($GLOBALS['HOST_NAME'])){
            $host = $GLOBALS['HOST_NAME'];
        }else{
            $host = $_SERVER['HTTP_HOST'];
        }
        if(strpos($host, 'dev.mystudio.academy') !== false){
            $this->file_name = __DIR__."/../Globals/salesforce_dev.props";
        }elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->file_name = __DIR__."/../Globals/salesforce_prod.props";
        }else{
            $this->file_name = __DIR__."/../Globals/salesforce_dev.props";
        }
        
        $this->url = "https://api.salesforceiq.com/v2/";        
        $this->getUserandSecret();
    }
    
    private function getUserandSecret(){        
        $file = explode( PHP_EOL, file_get_contents($this->file_name));
        $i=0;
        foreach( $file as $line ) {
            if($i<=2 && $line!=""){
                $api = explode("=", $line);
                if($i==0){
                    $this->api_key = $api[1];
                    $i++;
                }elseif($i==1){
                    $this->api_secret = $api[1];
                    $i++;
                }elseif($i==2){
                    $this->list_id = $api[1];
                    $i++;
                }
            }
        }
    }
    
    public function accessSalesforceApi($method,$request_type,$params){
        $url='';
        $url = $this->url.$method;
        if(strtoupper($request_type)=='GET'){
            $body='{}';
            $post_option = false;
            $url = $url.$params;
        }else{
            $body = $params;
            if(strtoupper($request_type)=='POST')
                $post_option = true;
            else
                $post_option = false;
        }
        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "$url");
        //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "$request_type");
        curl_setopt($ch, CURLOPT_POST, $post_option);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
        curl_setopt($ch, CURLOPT_USERPWD, $this->api_key.':'.$this->api_secret);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(   
            'Accept: application/json',
            'Content-Type: application/json')                                                           
        );

        $result = curl_exec($ch);
        $resultArr = json_decode($result,true);
        $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($returnCode !== 200){
            $error_log = array("Curl error: " => curl_error($ch)."($returnCode)", "Method: " => $method, "Type: " => $request_type, "Url: " => $this->url, "Params: " => $params);
            $this->log_info($this->json($error_log));
            $res['status'] = "Failed";
            $res['msg'] = $resultArr;
        }else{
            $succ_log = array("Curl : " => "Success"."($returnCode)", "Method: " => $method, "Type: " => $request_type, "Url: " => $this->url, "Params: " => $params, "Result: " => $result);
            $this->log_info($this->json($succ_log)); 
            $this->log_info($result);
            $res['status'] = "Success";
            $res['msg'] = $resultArr;
        }
        curl_close($ch);
        return $res;
    }
    
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    private function log_info($msg) {        
        $today = gmdate("d.m.Y")."_cron";
        $filename = __DIR__."/../Log/Salesforce/".$today."_cron.txt";
        $fd = fopen($filename, "a");
        $str = "[" . gmdate("d/m/Y H:i:s") . "] " . $msg;
        fwrite($fd, $str . PHP_EOL . PHP_EOL);
        fclose($fd);
    }
    
}