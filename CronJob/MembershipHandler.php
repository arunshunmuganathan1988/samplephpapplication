<?php
$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_old'){
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}
require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';


class MembershipHandler{
    
    private $db = NULL;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    private $server_url, $cron_props_file, $cron_sf, $cron_ps, $cron_ev, $cron_ms, $cron_ms_lt, $env;
    
    public function __construct(){
        require_once __DIR__.'/../Globals/config.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        if(!empty($GLOBALS['HOST_NAME'])){
            $host = $GLOBALS['HOST_NAME'];
        }else{
            $host = $_SERVER['HTTP_HOST'];
        }
        if(strpos($host, 'dev.mystudio.academy') !== false){
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'https://www.mystudio.academy';
            $this->cron_props_file = "cron_prod_props.php";
            $this->env = 'P';
        }else{
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        }
        $this->getUserandSecret();
    }
    
    private function getUserandSecret(){
        require_once "$this->cron_props_file";
        $this->cron_sf = SF_CRON;
        $this->cron_ps = PS_CRON;
        $this->cron_ev = EV_CRON;
        $this->cron_ms = MS_CRON;
        $this->cron_ms_lt = MS_LT_CRON;
    }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
        }
    }
    
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function processApi(){
        if($this->cron_ms_lt=='Y'){
            $this->runCronJobForMembershipLifeTime();
        }
        $this->runCronJobForResumePauseMembership();
    }
    
    private function runCronJobForMembershipLifeTime(){
        $total_records_processed = 0;
        $start_time = gmdate("Y-m-d H:i:s");
        cj_log_info("runCronJobForMembershipLifeTime() - Started");
        $query = sprintf("SELECT m.`company_id`, m.`membership_id`, mo.`membership_option_id`, m.`category_net_sales`, m.`members_active`, m.`members_onhold`, m.`members_cancelled`, mo.`options_net_sales`, mo.`options_members_active`, mo.`options_members_onhold`, mo.`options_members_cancelled` 
                    FROM `membership` m LEFT JOIN `membership_options` mo ON m.`membership_id`=mo.`membership_id`
                    WHERE m.`membership_id` IN (SELECT membership_id FROM `membership_registration` WHERE last_updt_dt between NOW() - INTERVAL 2 DAY AND NOW())
                    order by m.`membership_id`");
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
        }else{
            $num_rows = mysqli_num_rows($result);
            $membership_arr = $membership_option_arr = $output_ms = $output_mso = [];
            if($num_rows>0){
                $total_records_processed+=$num_rows;
                while($row = mysqli_fetch_assoc($result)){
                    $company_id = $row['company_id'];
                    $membership_id = $row['membership_id'];
                    $membership_option_id = $row['membership_option_id'];
                    $category_net_sales = $row['category_net_sales'];
                    $members_active = $row['members_active'];
                    $members_onhold = $row['members_onhold'];
                    $members_cancelled = $row['members_cancelled'];
                    $options_net_sales = $row['options_net_sales'];
                    $options_members_active = $row['options_members_active'];
                    $options_members_onhold = $row['options_members_onhold'];
                    $options_members_cancelled = $row['options_members_cancelled'];
                    
                    if(!empty($membership_id) && !in_array($membership_id, $membership_arr, true)){
                        $membership_arr[] = $membership_id;
                        $temp=[];
                        $temp['membership_id'] = $membership_id;
                        $total_registered_persons = $members_active+$members_onhold+$members_cancelled;
                        if($total_registered_persons>0){
                            $temp['category_life_time_value'] = number_format((float)($category_net_sales/$total_registered_persons), 2, '.', '');
                        }else{
                            $temp['category_life_time_value'] = 0;
                        }
                        $temp['category_average_membership_period'] = $this->getMembershipLifeTime($company_id, $membership_id, '');
                        $output_ms[] = $temp;
                    }
                    if(!empty($membership_option_id) && !in_array($membership_option_id, $membership_option_arr, true)){
                        $membership_option_arr[] = $membership_option_id;
                        $temp=[];
                        $temp['membership_id'] = $membership_id;
                        $temp['membership_option_id'] = $membership_option_id;
                        $total_registered_persons = $options_members_active+$options_members_onhold+$options_members_cancelled;
                        if($total_registered_persons>0){
                            $temp['options_life_time_value'] = number_format((float)($options_net_sales/$total_registered_persons), 2, '.', '');
                        }else{
                            $temp['options_life_time_value'] = 0;
                        }
                        $temp['options_average_membership_period'] = $this->getMembershipLifeTime($company_id, $membership_id, $membership_option_id);
                        $output_mso[] = $temp;
                    }
                }
                
                for($i=0;$i<count($output_ms);$i++){
                    $update_query = sprintf("UPDATE `membership` SET `category_life_time_value`='%s', `category_average_membership_period`='%s' WHERE `membership_id`='%s'",
                            mysqli_real_escape_string($this->db, $output_ms[$i]['category_life_time_value']), mysqli_real_escape_string($this->db, $output_ms[$i]['category_average_membership_period']), 
                            mysqli_real_escape_string($this->db, $output_ms[$i]['membership_id']));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        cj_log_info($this->json($error_log));
                    }
                }
                for($i=0;$i<count($output_mso);$i++){
                    $update_query = sprintf("UPDATE `membership_options` SET `options_life_time_value`='%s', `options_average_membership_period`='%s' WHERE `membership_id`='%s' AND `membership_option_id`='%s'",
                            mysqli_real_escape_string($this->db, $output_mso[$i]['options_life_time_value']), mysqli_real_escape_string($this->db, $output_mso[$i]['options_average_membership_period']), 
                            mysqli_real_escape_string($this->db, $output_mso[$i]['membership_id']), mysqli_real_escape_string($this->db, $output_mso[$i]['membership_option_id']));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_str");
                        cj_log_info($this->json($error_log));
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records available for update.");
                cj_log_info($this->json($error_log));
            }
        }
        cj_log_info("runCronJobForMembershipLifeTime() - Completed");
        if($total_records_processed>0){
            $end_time = gmdate("Y-m-d H:i:s");
            $cron_file_name = "MembershipHandler";
            $cron_function_name = "runCronJobForMembershipLifeTime";
            $cron_message = "Total membership & options records updated";
            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
            if(!$cron_log_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
                cj_log_info($this->json($error_log));
            }
        }
    }
    
    private function getMembershipLifeTime($company_id, $membership_id, $membership_option_id){
        $life_time = 0;
        if(!empty($membership_option_id)){
            $query = sprintf("SELECT SUM(diff)/30/SUM(per) as life_time FROM (SELECT if(`membership_status`='C', datediff(IF(`cancelled_date` IS NULL || `cancelled_date`='0000-00-00', `last_updt_dt`,`cancelled_date`),`created_dt`), 
                if(`membership_status`='P', datediff(IF(`hold_date` IS NULL || `hold_date`='0000-00-00', `last_updt_dt`,`hold_date`),`created_dt`), datediff(curdate(),`created_dt`))) diff, 1 as per 
                FROM `membership_registration` WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s')t", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$membership_option_id));
        }else{            
            $query = sprintf("SELECT SUM(diff)/30/SUM(per) as life_time FROM (SELECT if(`membership_status`='C', datediff(IF(`cancelled_date` IS NULL || `cancelled_date`='0000-00-00', `last_updt_dt`,`cancelled_date`),`created_dt`), 
                if(`membership_status`='P', datediff(IF(`hold_date` IS NULL || `hold_date`='0000-00-00', `last_updt_dt`,`hold_date`),`created_dt`), datediff(curdate(),`created_dt`))) diff, 1 as per 
                FROM `membership_registration` WHERE `company_id`='%s' AND `membership_id`='%s')t", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$membership_id));
        }
        $result = mysqli_query($this->db, $query);
        
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $life_time = number_format((float)($row['life_time']), 1, '.', '');
            }else{
                $life_time = 0;
            }
        }
        return $life_time;
    }
    
    private function runCronJobForResumePauseMembership(){
        $total_records_processed = 0;
        $start_time = gmdate("Y-m-d H:i:s");
        cj_log_info("runCronJobForResumePauseMembership() - Started");
        $reg_id_array = [];
        $curr_date = gmdate("Y-m-d");
        $query = sprintf("SELECT * FROM `membership_registration` WHERE `membership_status`='P' AND `resume_date`='$curr_date'");
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $total_records_processed+=$num_rows;
                while($row = mysqli_fetch_assoc($result)){
                    $reg_id = $row['membership_registration_id'];
                    $company_id = $row['company_id'];
                    $membership_id = $row['membership_id'];
                    $membership_option_id = $row['membership_option_id'];
                    $membership_status = 'R';
                    $membership_structure = $row['membership_structure'];
                    $membership_title=$row['membership_category_title'];
                    $membership_option_title=$row['membership_title'];
                    
                    if($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE'){
                        $status_str = "'P','C'";
                    }else{
                        $status_str = "'P'";
                    }
                                   
                    $update_payment = sprintf("UPDATE `membership_payment` SET `payment_status`=CASE WHEN `payment_date`<'$curr_date' THEN 'C' ELSE 'N' END
                            WHERE `membership_registration_id`='%s' AND `payment_status` IN ($status_str)", mysqli_real_escape_string($this->db, $reg_id));
                    $result_update_payment = mysqli_query($this->db, $update_payment);
                    if (!$result_update_payment) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_payment");
                        cj_log_info($this->json($error_log));
                        continue;
                    }
                    
                    $update_registration = sprintf("UPDATE `membership_registration` SET `membership_status`='R'
                            WHERE `membership_registration_id`='%s' AND `membership_status`='P'", mysqli_real_escape_string($this->db, $reg_id));
                    $result_update_registration = mysqli_query($this->db, $update_registration);
                    if (!$result_update_registration) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_registration");
                        cj_log_info($this->json($error_log));
                        continue;
                    }
                    
                    $activity_text = "Status changed to Active";
                    $currdate = gmdate("Y-m-d H:i:s");
                    $insert_history = sprintf("INSERT INTO `membership_history`(`company_id`, `membership_registration_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), 'status', 
                            mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $currdate));
                    $result_history = mysqli_query($this->db, $insert_history);
                    if (!$result_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                        cj_log_info($this->json($error_log));
                    }
                    
                    $res = $this->updateMembershipDimensionsMembers($company_id, $membership_id, $membership_status, $membership_option_id, 'P', $reg_id, $membership_title, $membership_option_title);

                    if ($res == 0) {
                        $reg_id_array[] = $reg_id;
                    } else {
                        $update_reg = sprintf("UPDATE `membership_registration` SET `hold_date`=NULL
                            WHERE `membership_registration_id`='%s' AND `membership_status`='R'", mysqli_real_escape_string($this->db, $reg_id));
                        $result_update_reg = mysqli_query($this->db, $update_reg);
                        if (!$result_update_reg) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg");
                            cj_log_info($this->json($error_log));
                            continue;
                        }
                    }
                }
            } else {
                $error_log = array('status' => "Failed", "msg" => "No membership registration available for resume.");
                cj_log_info($this->json($error_log));
            }
        }
        if(!empty($reg_id_array)){
            $reg_str = implode(", ", $reg_id_array);
            $msg = "Something went wrong in membership handler for membership_registration_ids=$reg_str, check cron log or table";
            $this->sendEmailForAdmins($msg);
        }
        cj_log_info("runCronJobForResumePauseMembership() - Completed");
        if($total_records_processed>0){
            $end_time = gmdate("Y-m-d H:i:s");
            $cron_file_name = "MembershipHandler";
            $cron_function_name = "runCronJobForResumePauseMembership";
            $cron_message = "Total membership registrations resumed";
            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
            if(!$cron_log_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
                cj_log_info($this->json($error_log));
            }
        }
    }
    
    public function addMembershipDimensions($company_id, $membership_id, $mem_option_id, $category_title, $option_title, $period) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($mem_option_id)) {
            $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                ipn_log_info($this->json($error_log));
                $reg_id = 1;
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $selectsql = sprintf("SELECT `active_members` from membership_dimensions where company_id='%s' and `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s' ",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $pre_dt));
                    $resultselectsql = mysqli_query($this->db, $selectsql);
                    if (!$resultselectsql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        ipn_log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
                        $num_of_rows2 = mysqli_num_rows($resultselectsql);
                        if ($num_of_rows2 == 0) {
                            $sqlselect1=sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and `membership_option_id`='%s' and  `company_id`= '%s'",  mysqli_real_escape_string($this->db,$membership_id),mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db,$company_id));
                            $resultsqlselect1=mysqli_query($this->db, $sqlselect1);
                            $value2 = mysqli_fetch_object($resultsqlselect1);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id),
                                    mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), mysqli_real_escape_string($this->db,$value2->active));
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                ipn_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        } else {
                            $value = mysqli_fetch_object($resultselectsql);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id),
                                    mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), $value->active_members);
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                ipn_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        }
                    }
                }
            }
        }

        $sql2 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            ipn_log_info($this->json($error_log));
            $reg_id = 1;
        } else{
            $num_of_rows3 = mysqli_num_rows($result2);
            if ($num_of_rows3 == 0) {
                $selectsql2 = sprintf("SELECT `active_members` FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $pre_dt));
                $resultselectsql2 = mysqli_query($this->db, $selectsql2);
                if (!$resultselectsql2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql2");
                    ipn_log_info($this->json($error_log));
                    $reg_id = 1;
                } else {
                    $num_of_rows4 = mysqli_num_rows($resultselectsql2);
                    if ($num_of_rows4 == 0) {
                        $sqlselect2 = sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and  `company_id`= '%s'", mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$company_id));
                        $resultsqlselect2 = mysqli_query($this->db, $sqlselect2);
                        $value3 = mysqli_fetch_object($resultsqlselect2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $value3->active));
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            ipn_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    } else {
                        $value1 = mysqli_fetch_object($resultselectsql2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), $value1->active_members);
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            ipn_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for addMembershipDimensions(), check log";
            $this->sendEmailForAdmins($msg);
        }
        
    }
    
    private function updateMembershipDimensionsMembers($company_id, $membership_id, $membership_status, $membership_option_id, $old_mem_status, $membership_reg_id,$membership_title,$membership_option_title) {
        $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id, $membership_title,$membership_option_title, '');
        $flag = 0;
        $check = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        $date_str = "%Y-%m";
        $every_month_dt = date("Y-m-d");

        $selectquery = sprintf("SELECT * FROM `membership_registration` WHERE DATE_FORMAT(`hold_date`,'%s')=DATE_FORMAT(`resume_date`,'%s') and  `company_id`='%s' and `membership_id`='%s' and `membership_option_id`='%s' and `membership_registration_id`='%s'",
                $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id), mysqli_real_escape_string($this->db, $membership_reg_id));
        $res = mysqli_query($this->db, $selectquery);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
            cj_log_info($this->json($error_log));
            return 0;
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows == 1) {
                $flag = 1;
            }
        }
        $selectquery1 = sprintf("SELECT * FROM `membership_registration` WHERE DATE_FORMAT(`hold_date`,'%s')=DATE_FORMAT(`cancelled_date`,'%s') and  `company_id`='%s' and `membership_id`='%s' and `membership_option_id`='%s' and `membership_registration_id`='%s'",
                $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id), mysqli_real_escape_string($this->db, $membership_reg_id));
        $res1 = mysqli_query($this->db, $selectquery1);
        if (!$res1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery1");
            cj_log_info($this->json($error_log));
            return 0;
        } else {
            $num_of_rows = mysqli_num_rows($res1);
            if ($num_of_rows == 1) {
                $check = 1;
            }
        }
        $mem_string = $cat_string = $opt_string = "";
        if ($membership_status == 'R') {
            if ($flag == 1) {
                $mem_string = "`active_members`=`active_members`+1, `reg_hold`=`reg_hold`-1";
            } elseif ($flag == 0) {
                 $mem_string = "`active_members`=`active_members`+1,`reg_resumed_from_hold`=IFNULL(`reg_resumed_from_hold`,0)+1";
            }
             $cat_string = "`members_active`=`members_active`+1, `members_onhold`=`members_onhold`-1";
                $opt_string = "`options_members_active`=`options_members_active`+1, `options_members_onhold`=`options_members_onhold`-1";
        } elseif ($membership_status == 'P') {
            $mem_string = "`active_members`=`active_members`-1, `reg_hold`=`reg_hold`+1";
            $cat_string = "`members_active`=`members_active`-1, `members_onhold`=`members_onhold`+1";
            $opt_string = "`options_members_active`=`options_members_active`-1, `options_members_onhold`=`options_members_onhold`+1";
        } elseif ($membership_status == 'C') {
            if ($old_mem_status == 'R') {
                $mem_string = "`active_members`=`active_members`-1, `reg_cancelled`=`reg_cancelled`+1";
                $cat_string = "`members_active`=`members_active`-1, `members_cancelled`=`members_cancelled`+1";
                $opt_string = "`options_members_active`=`options_members_active`-1, `options_members_cancelled`=`options_members_cancelled`+1";
            } else {
                if ($check == 1) {
                    $mem_string = "`reg_hold`=`reg_hold`-1, `reg_cancelled`=`reg_cancelled`+1";
                } else if ($check == 0) {
                    $mem_string = "`reg_cancelled`=`reg_cancelled`+1";
                }
                 $cat_string = "`members_onhold`=`members_onhold`-1, `members_cancelled`=`members_cancelled`+1";
                 $opt_string = "`options_members_onhold`=`options_members_onhold`-1, `options_members_cancelled`=`options_members_cancelled`+1";
            }
        } elseif ($membership_status == 'A') {
            $mem_string = "`active_members`=`active_members`+1,`reg_new`=`reg_new`+1";
        }

        if (!empty(trim($mem_string))) {
            $query1 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`=0",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), $every_month_dt, $date_str);
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                cj_log_info($this->json($error_log));
                return 0;
            }
            
            $query2 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), $every_month_dt, $date_str, mysqli_real_escape_string($this->db, $membership_option_id));
            $result2 = mysqli_query($this->db, $query2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                cj_log_info($this->json($error_log));
                return 0;
            }
        }
            
        if(!empty(trim($cat_string))){
            $query3 = sprintf("UPDATE `membership` SET $cat_string WHERE `company_id`='%s' AND `membership_id`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
            $result3 = mysqli_query($this->db, $query3);
            if (!$result3) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                cj_log_info($this->json($error_log));
                return 0;
            }
        }

        if(!empty(trim($opt_string))){
            $query4 = sprintf("UPDATE `membership_options` SET $opt_string WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
            $result4 = mysqli_query($this->db, $query4);
            if (!$result4) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query4");
                cj_log_info($this->json($error_log));
                return 0;
            }
        }
        date_default_timezone_set($curr_time_zone);
        return 1;
    }
    
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    private function sendEmailForAdmins($msg){
        if($this->env=='P'){
            $env = "Production";
        }else{
            $env = "Development";
        }
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "$env - Membership Handler Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        cj_log_info($this->json($error_log));
        return $response;
    }
}

$api = new MembershipHandler;
$api->processApi();
?>