<?php

require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';

class testEmail{
    
    public $data = "";
    public $sub = "";
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_code = 200;
    private $user_agent = '';
    
    public function __construct(){
//        $this->user_agent = $this->getUserAgent();
        date_default_timezone_set('UTC');
        if(isset($argv[1]) && !empty($argv[1])){
            if($argv[1]=='batch'){
                $this->sub = "Test Email From Batch Server";
            }else{
                $this->sub = "Test Email From Production Server";
            }
        }else{
            $this->sub = "Test Email From Production Server";
        }
    }
    
    public function processApi(){
        $this->sendEmailForAdmins();
    }
    
    private function sendEmailForAdmins(){
        $cmp_name = "MyStudio";
        $to = 'mcabrera3@sidekicks.me';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com,jim.p@technogemsinc.com,mcabrera3@sidekicks.me,sscrawley@sidekicks.me,jecabrera@sidekicks.me,support@mystudio.academy';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = $this->sub;
            $mail->Body = "Testing email functionality from all cases";
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "cc_list" => $cc_email_list, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
}

$api = new testEmail;
$api->processApi();


    
?>