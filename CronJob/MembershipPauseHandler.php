<?php
$GLOBALS['HOST_NAME'] = '';

if (isset($argv[1]) && !empty($argv[1])) {
    if ($argv[1] == 'prod') {
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    } elseif ($argv[1] == 'dev_old') {
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    } elseif ($argv[1] == 'dev_new') {
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    } else {
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }
} elseif (isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])) {
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
} else {
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}
require_once __DIR__ . '/../Globals/cont_log.php';
require_once __DIR__ . '/../Wepay2/class.phpmailer.php';
require_once __DIR__ . '/../Wepay2/class.smtp.php';

class MembershipPauseHandler {

    private $db = NULL;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    private $server = '';
    private $server_url, $cron_props_file, $cron_sf, $cron_ps, $cron_ev, $cron_ms, $cron_ms_lt, $env;

    public function __construct() {
        require_once __DIR__ . '/../Globals/config.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        if (!empty($GLOBALS['HOST_NAME'])) {
            $host = $GLOBALS['HOST_NAME'];
        } else {
            $host = $_SERVER['HTTP_HOST'];
        }
        $this->server = $host;
        if (strpos($host, 'dev.mystudio.academy') !== false) {
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        } elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'https://www.mystudio.academy';
            $this->cron_props_file = "cron_prod_props.php";
            $this->env = 'P';
        } else {
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        }
        $this->getUserandSecret();
    }

    private function getUserandSecret() {
        require_once "$this->cron_props_file";
        $this->cron_sf = SF_CRON;
        $this->cron_ps = PS_CRON;
        $this->cron_ev = EV_CRON;
        $this->cron_ms = MS_CRON;
        $this->cron_ms_lt = MS_LT_CRON;
    }

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
        }
    }

    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    public function processApi() {
        $this->runCronJobForProcessScheduledPause();
    }

    private function runCronJobForProcessScheduledPause() {
        $current_payment_date = $next_payment_date = $start_payment_date = $next_payment_date_string = '';
        cj_log_info("runCronJobForProcessScheduledPause() - Started");
        $reg_id_array = [];
        if (isset($_REQUEST['curr_date']) && !empty($_REQUEST['curr_date'])) {
            $curr_date = $_REQUEST['curr_date'];
        } elseif (isset($argv[2]) && !empty($argv[2])) {
            $curr_date = $argv[2];
        } else {
            $curr_date = gmdate("Y-m-d");
        }
        $query = sprintf("SELECT mr.*, c.upgrade_status FROM `membership_registration` mr LEFT JOIN `company` c ON mr.`company_id` = c.`company_id` WHERE mr.`membership_status` IN ('R') AND `scheduled_hold_date`<='%s'",mysqli_escape_string($this->db, $curr_date));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $reg_id = $row['membership_registration_id'];
                    $company_id = $row['company_id'];
                    $membership_id = $row['membership_id'];
                    $membership_option_id = $row['membership_option_id'];
                    $membership_status = 'P';
                    $old_mem_status = $row['membership_status'];
                    $membership_structure = $row['membership_structure'];
                    $membership_title = $row['membership_category_title'];
                    $membership_option_title = $row['membership_title'];
                    $resume_date = $row['resume_date'];
                    //$next_payment_date_in_db = $row['next_payment_date'];
                    $payment_frequency = $row['payment_frequency'];
                    $payment_amount = $row['payment_amount'];
                    $temp_amount = $payment_amount;
                    $processing_fee_type = $row['processing_fee_type'];
                    $upgrade_status = $row['upgrade_status'];
                    $p_f = $this->getProcessingFee($company_id, $upgrade_status);
                    $process_fee_per = $p_f['PP'];
                    $process_fee_val = $p_f['PT'];
                    $r1_fee = $temp_amount*($process_fee_per/100) + $process_fee_val;
                    $r2_fee = ($temp_amount + $r1_fee)*($process_fee_per/100) + $process_fee_val;
                    if($processing_fee_type==1){
                        $processing_fee = number_format((float)($r1_fee), 2, '.', '');
                    }else{
                        $processing_fee = number_format((float)($r2_fee), 2, '.', '');
                    }
                    $cc_id = $row['credit_card_id'];
                    $cc_name = $row['credit_card_name'];
                    
                    $get_registration_details = sprintf("SELECT * FROM `membership_payment` WHERE `membership_registration_id`='%s' AND `payment_status` IN ('N') AND `payment_date`>='$curr_date'", mysqli_real_escape_string($this->db, $reg_id));
                    $result_get_registration_details = mysqli_query($this->db, $get_registration_details);
                    if (!$result_get_registration_details) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_registration_details");
                        cj_log_info($this->json($error_log));
                        continue;
                    } else {
                        $num_rows_get = mysqli_num_rows($result_get_registration_details);
                        if ($num_rows_get > 0) {
                            $update_payment = sprintf("UPDATE `membership_payment` SET `payment_status`='P' WHERE `membership_registration_id`='%s' AND `payment_status`='N' AND `payment_date`>='$curr_date'", mysqli_real_escape_string($this->db, $reg_id));
                            $result_update_payment = mysqli_query($this->db, $update_payment);
                            if (!$result_update_payment) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_payment");
                                cj_log_info($this->json($error_log));
                                continue;
                            }
                        }
                        if (is_null($resume_date)) {
                            $next_payment_date_string = '';
                        } else {
                            $next_payment_date_string = "`next_payment_date` = '$resume_date',";
                        }
                        $update_registration = sprintf("UPDATE `membership_registration` SET $next_payment_date_string `membership_status`='P', `hold_date`='$curr_date',`Scheduled_hold_date`= NULL WHERE `membership_registration_id`='%s' AND `membership_status` in ('R')", mysqli_real_escape_string($this->db, $reg_id));
                        $result_update_registration = mysqli_query($this->db, $update_registration);
                        if (!$result_update_registration) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_registration");
                            cj_log_info($this->json($error_log));
                            continue;
                        }

                        $activity_text = "Status changed to Hold";
                        $currdate = gmdate("Y-m-d H:i:s");
                        $insert_history = sprintf("INSERT INTO `membership_history`(`company_id`, `membership_registration_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), 'status', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $currdate));
                        $result_history = mysqli_query($this->db, $insert_history);
                        if (!$result_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                            cj_log_info($this->json($error_log));
                            continue;
                        } else {
                            $update_history = sprintf("UPDATE `membership_history` mh JOIN (select `membership_history_id` from `membership_history` WHERE `membership_registration_id`='%s' AND `activity_type`='scheduled hold'  AND `deleted_flag`!='D' ORDER BY membership_history_id limit 0,1 ) t ON mh.`membership_history_id` = t.`membership_history_id` SET mh.`activity_type`='scheduled hold completed' WHERE `company_id`='%s' AND `membership_registration_id`='%s' AND `activity_type`='scheduled hold' ", mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id));
                            $result_update_history = mysqli_query($this->db, $update_history);
                            if (!$result_update_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_history");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                        // Inserting Records after Resume date - Arun
                        if($membership_structure=='OE'){
                            $start_payment_date = $resume_date;
                            $recurring_payment_date_array = [];
                            $end_payment_date = date("Y-m-d", strtotime("+93 days", strtotime($start_payment_date)));
                            $iteration_end = 1;
                            for ($i = 0; $i < $iteration_end; $i++) {
                                if ($i == 0) {
                                    $current_payment_date = $next_payment_date = $start_payment_date;
                                } else {
                                    if ($payment_frequency == 'W') {
                                        $next_payment_date = date('Y-m-d', strtotime('+1 weeks', strtotime($current_payment_date)));
                                    } elseif ($payment_frequency == 'B') {
                                        if (date('j', strtotime($current_payment_date)) < 16) {
                                            $next_payment_date = date('Y-m-16', strtotime($current_payment_date));
                                        } else {
                                            $next_payment_date = date('Y-m-d', strtotime('first day of next month', strtotime($current_payment_date)));
                                        }
                                    } elseif ($payment_frequency == 'M') {
                                        $next_payment_date = $this->getSameDayNextMonth(new DateTime($current_payment_date));
                                    } elseif ($payment_frequency == 'A') {
                                        $next_payment_date = date('Y-m-d', strtotime('+1 year', strtotime($current_payment_date)));
                                    } elseif ($payment_frequency == 'C') {
                                        if ($custom_recurring_frequency_period_type == 'CW') {
                                            $next_payment_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($current_payment_date)));
                                        } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                            $next_payment_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($current_payment_date)));
                                        }
                                    }
                                    $current_payment_date = $next_payment_date;
                                }

                                if ($current_payment_date <= $end_payment_date || (count($recurring_payment_date_array) == 0 )) {
                                    $iteration_end++;
                                    $recurring_payment_date_array[] = $current_payment_date;
                                }
                            }
                            for ($j = 0; $j < count($recurring_payment_date_array); $j++) {
                                $check_payment_record = sprintf("SELECT * FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s' AND `payment_date`='%s' AND `payment_status`='N'",
                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), $recurring_payment_date_array[$j]);
                                $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
                                if (!$result_check_payment_record) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
                                    cj_log_info($this->json($error_log));
                                }else{
                                    $num_of_rows = mysqli_num_rows($result_check_payment_record);
                                    if($num_of_rows==0){
                                        $insert_mem_pay = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $processing_fee), 
                                                mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        $result_mem_pay = mysqli_query($this->db, $insert_mem_pay);
                                        if (!$result_mem_pay) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay");
                                            cj_log_info($this->json($error_log));
                                        }
                                    }
                                }
                            }                                   
                        }
                        
                        $res = $this->updateMembershipDimensionsMembers($company_id, $membership_id, $membership_status, $membership_option_id, $old_mem_status, $reg_id, $membership_title, $membership_option_title);
                        if ($res == 0) {
                            $reg_id_array[] = $reg_id;
                        }
                    }
                }
            } else {
                $error_log = array('status' => "Failed", "msg" => "No membership registration available for Pause.");
                cj_log_info($this->json($error_log));
            }
        }
        cj_log_info("runCronJobForProcessScheduledPause() - Completed");
    }

    public function addMembershipDimensions($company_id, $membership_id, $mem_option_id, $category_title, $option_title, $period) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if (!empty($period)) {
            $curr_dt = date("Y-m", strtotime($period));
        } else {
            $curr_dt = date("Y-m");
        }
        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($mem_option_id)) {
            $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                cj_log_info($this->json($error_log));
                $reg_id = 1;
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $selectsql = sprintf("SELECT `active_members` from membership_dimensions where company_id='%s' and `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s' ", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $pre_dt));
                    $resultselectsql = mysqli_query($this->db, $selectsql);
                    if (!$resultselectsql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        cj_log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
                        $num_of_rows2 = mysqli_num_rows($resultselectsql);
                        if ($num_of_rows2 == 0) {
                            $sqlselect1 = sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and `membership_option_id`='%s' and  `company_id`= '%s'", mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $company_id));
                            $resultsqlselect1 = mysqli_query($this->db, $sqlselect1);
                            $value2 = mysqli_fetch_object($resultsqlselect1);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), mysqli_real_escape_string($this->db, $value2->active));
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                cj_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        } else {
                            $value = mysqli_fetch_object($resultselectsql);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), $value->active_members);
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                cj_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        }
                    }
                }
            }
        }

        $sql2 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            cj_log_info($this->json($error_log));
            $reg_id = 1;
        } else {
            $num_of_rows3 = mysqli_num_rows($result2);
            if ($num_of_rows3 == 0) {
                $selectsql2 = sprintf("SELECT `active_members` FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $pre_dt));
                $resultselectsql2 = mysqli_query($this->db, $selectsql2);
                if (!$resultselectsql2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql2");
                    cj_log_info($this->json($error_log));
                    $reg_id = 1;
                } else {
                    $num_of_rows4 = mysqli_num_rows($resultselectsql2);
                    if ($num_of_rows4 == 0) {
                        $sqlselect2 = sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and  `company_id`= '%s'", mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $company_id));
                        $resultsqlselect2 = mysqli_query($this->db, $sqlselect2);
                        $value3 = mysqli_fetch_object($resultsqlselect2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $value3->active));
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            cj_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    } else {
                        $value1 = mysqli_fetch_object($resultselectsql2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), $value1->active_members);
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            cj_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);

        if ($reg_id == 1) {
            $msg = "Something went wrong for addMembershipDimensions(), check cron log";
            $this->sendEmailForAdmins($msg);
        }
    }

    private function updateMembershipDimensionsMembers($company_id, $membership_id, $membership_status, $membership_option_id, $old_mem_status, $membership_reg_id, $membership_title, $membership_option_title) {
        $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id, $membership_title, $membership_option_title, '');
        $flag = 0;
        $check = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        $date_str = "%Y-%m";
        $every_month_dt = date("Y-m-d");

        $selectquery = sprintf("SELECT * FROM `membership_registration` WHERE DATE_FORMAT(`hold_date`,'%s')=DATE_FORMAT(`resume_date`,'%s') and  `company_id`='%s' and `membership_id`='%s' and `membership_option_id`='%s' and `membership_registration_id`='%s'", $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id), mysqli_real_escape_string($this->db, $membership_reg_id));
        $res = mysqli_query($this->db, $selectquery);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
            cj_log_info($this->json($error_log));
            return 0;
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows == 1) {
                $flag = 1;
            }
        }
        $selectquery1 = sprintf("SELECT * FROM `membership_registration` WHERE DATE_FORMAT(`hold_date`,'%s')=DATE_FORMAT(`cancelled_date`,'%s') and  `company_id`='%s' and `membership_id`='%s' and `membership_option_id`='%s' and `membership_registration_id`='%s'", $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id), mysqli_real_escape_string($this->db, $membership_reg_id));
        $res1 = mysqli_query($this->db, $selectquery1);
        if (!$res1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery1");
            cj_log_info($this->json($error_log));
            return 0;
        } else {
            $num_of_rows = mysqli_num_rows($res1);
            if ($num_of_rows == 1) {
                $check = 1;
            }
        }
        $mem_string = $cat_string = $opt_string = "";
        if ($membership_status == 'R') {
            if ($flag == 1) {
                $mem_string = "`active_members`=`active_members`+1, `reg_hold`=`reg_hold`-1";
            } elseif ($flag == 0) {
                $mem_string = "`active_members`=`active_members`+1,`reg_resumed_from_hold`=IFNULL(`reg_resumed_from_hold`,0)+1";
            }
            $cat_string = "`members_active`=`members_active`+1, `members_onhold`=`members_onhold`-1";
            $opt_string = "`options_members_active`=`options_members_active`+1, `options_members_onhold`=`options_members_onhold`-1";
        } elseif ($membership_status == 'P') {
            $mem_string = "`active_members`=`active_members`-1, `reg_hold`=`reg_hold`+1";
            $cat_string = "`members_active`=`members_active`-1, `members_onhold`=`members_onhold`+1";
            $opt_string = "`options_members_active`=`options_members_active`-1, `options_members_onhold`=`options_members_onhold`+1";
        } elseif ($membership_status == 'C') {
            if ($old_mem_status == 'R') {
                $mem_string = "`active_members`=`active_members`-1, `reg_cancelled`=`reg_cancelled`+1";
                $cat_string = "`members_active`=`members_active`-1, `members_cancelled`=`members_cancelled`+1";
                $opt_string = "`options_members_active`=`options_members_active`-1, `options_members_cancelled`=`options_members_cancelled`+1";
            } else {
                if ($check == 1) {
                    $mem_string = "`reg_hold`=`reg_hold`-1, `reg_cancelled`=`reg_cancelled`+1";
                } else if ($check == 0) {
                    $mem_string = "`reg_cancelled`=`reg_cancelled`+1";
                }
                $cat_string = "`members_onhold`=`members_onhold`-1, `members_cancelled`=`members_cancelled`+1";
                $opt_string = "`options_members_onhold`=`options_members_onhold`-1, `options_members_cancelled`=`options_members_cancelled`+1";
            }
        } elseif ($membership_status == 'A') {
            $mem_string = "`active_members`=`active_members`+1,`reg_new`=`reg_new`+1";
        }

        if (!empty(trim($mem_string))) {
            $query1 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`=0", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), $every_month_dt, $date_str);
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                cj_log_info($this->json($error_log));
                return 0;
            }

            $query2 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), $every_month_dt, $date_str, mysqli_real_escape_string($this->db, $membership_option_id));
            $result2 = mysqli_query($this->db, $query2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                cj_log_info($this->json($error_log));
                return 0;
            }
        }

        if (!empty(trim($cat_string))) {
            $query3 = sprintf("UPDATE `membership` SET $cat_string WHERE `company_id`='%s' AND `membership_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
            $result3 = mysqli_query($this->db, $query3);
            if (!$result3) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                cj_log_info($this->json($error_log));
                return 0;
            }
        }

        if (!empty(trim($opt_string))) {
            $query4 = sprintf("UPDATE `membership_options` SET $opt_string WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
            $result4 = mysqli_query($this->db, $query4);
            if (!$result4) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query4");
                cj_log_info($this->json($error_log));
                return 0;
            }
        }
        date_default_timezone_set($curr_time_zone);
        return 1;
    }

    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }
    
    
    public function getSameDayNextMonth(DateTime $startDate, $numberOfMonthsToAdd = 1) {
        $startDateDay = (int) $startDate->format('j');
        $startDateMonth = (int) $startDate->format('n');
        $startDateYear = (int) $startDate->format('Y');

        $numberOfYearsToAdd = floor(($startDateMonth + $numberOfMonthsToAdd) / 12);
        if ((($startDateMonth + $numberOfMonthsToAdd) % 12) === 0) {
          $numberOfYearsToAdd--;
        }
        $year = $startDateYear + $numberOfYearsToAdd;

        $month = ($startDateMonth + $numberOfMonthsToAdd) % 12;
        if ($month === 0) {
          $month = 12;
        }
        $month = sprintf('%02s', $month);

        $numberOfDaysInMonth = (new DateTime("$year-$month-01"))->format('t');
        $day = $startDateDay;
        if ($startDateDay > $numberOfDaysInMonth) {
          $day = $numberOfDaysInMonth;
        }
        $day = sprintf('%02s', $day);

        $output=new DateTime("$year-$month-$day");
        return $output->format('Y-m-d');
    }

}

$api = new MembershipPauseHandler;
$api->processApi();
?>