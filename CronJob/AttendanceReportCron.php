<?php
$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_old'){
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}
require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';

class AttendanceReportCron {

    public function __construct() {
        require_once __DIR__.'/../Globals/config.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
    }

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    public function processApi() {
        $this->generateAttendanceReport();
    }
    
    protected function generateAttendanceReport() {
        $company_id = $company_email = $company_name = '';
        $query = sprintf("SELECT company_id, email_id, company_name, referral_email_list, attendance_report_frequency FROM company WHERE attendance_report_flag = 'Y' AND 
                HOUR(CONVERT_TZ(NOW(),(IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone)), `timezone`))=1");
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
        } else {
            $num_of_rows_1 = mysqli_num_rows($result);
            if ($num_of_rows_1 > 0) {
                while ($row_1 = mysqli_fetch_assoc($result)) {
                    $company_id = $row_1['company_id'];
                    $company_email = $row_1['email_id'];
                    $company_name = $row_1['company_name'];
                    $cc_email_list = $row_1['referral_email_list'];
                    $attendance_report_frequency = $row_1['attendance_report_frequency'];
                    $curr_month_day = date("d");
                    $curr_week_day = date("w");
                    $last_day_of_month = date("t");
                    if($attendance_report_frequency=='W' && ($curr_week_day==0 || $curr_week_day==1)){
                        $this->generateAttendanceReportDetails($company_id,$company_email,$company_name,$cc_email_list);
                    }elseif($attendance_report_frequency=='M' && ($curr_month_day==$last_day_of_month || $curr_month_day==1)){
                        $this->generateAttendanceReportDetails($company_id,$company_email,$company_name,$cc_email_list);
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No Attendance Record.");
                cj_log_info($this->json($error_log));
            }
        }
    }
    
    protected function generateAttendanceReportDetails($company_id,$company_email,$company_name,$cc_email_list) {
        $header = $body = '';
        $attachment =$attachment_final= [];
        $fileNames = ["Zero Attendance","Attendance 2 or less","Trial Zero Attendance","Trial Attendance 2 or less"];        
        $not_attend_7_14 = $not_attend_15_21 = $not_attend_22_30 = $not_attend_31_60 = $last_14_days = $last_30_days = [];
        $date_format = '%M %e, %Y';
        $query1 = sprintf("select mr.membership_registration_id, concat(mr.`buyer_last_name`,', ',mr.`buyer_first_name`) buyer_name, concat(mr.membership_registration_column_2, ', ', mr.membership_registration_column_1) participant_name, 
		mr.buyer_phone, mr.buyer_email, IFNULL(mbr.rank_name, '') rank_value,
		IFNULL((SELECT DATE_FORMAT(checkin_datetime,'%s') FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id`and status='N' ORDER BY `checkin_datetime` DESC LIMIT 0,1), '') l_att,
                IFNULL(mbr.`required_attendance_count`, '') att_req, 
                IFNULL((SELECT SUM(1) FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `checkin_datetime`>=mr.`last_advanced_date` and status='N' GROUP BY `company_id`, `membership_registration_id`),0) act_att,
                IF(mbr.`required_attendance_count` IS NULL || mbr.`required_attendance_count`='', concat('total ', IFNULL((SELECT SUM(1) FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `checkin_datetime`>=mr.`last_advanced_date` and status='N' GROUP BY `company_id`, `membership_registration_id`),0)),
                concat(IFNULL((SELECT SUM(1) FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `checkin_datetime`>=mr.`last_advanced_date` and status='N' GROUP BY `company_id`, `membership_registration_id`),0), ' out of ', mbr.`required_attendance_count`)) att_count,
		sum( not_attend_7_14) not_attend_7_14, sum( not_attend_15_21) not_attend_15_21,
                sum( not_attend_22_30) not_attend_22_30, sum( not_attend_31_60) not_attend_31_60, 
                sum(case when last_14_days <= 2 then 1 else 0 end) last_14_days, sum(case when last_30_days <= 2 then 1 else 0 end) last_30_days
                from attendance_dimension ad left join membership_registration mr ON ad.company_id=mr.company_id and ad.membership_registration_id=mr.membership_registration_id
                left join membership_ranks mbr on mr.membership_id=mbr.membership_id and mr.rank_id=mbr.membership_rank_id
                where ad.company_id = '%s' group by ad.company_id, ad.membership_registration_id", $date_format, mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            cj_log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 500);
        }else{
            if(mysqli_num_rows($result1)>0){
                while($row = mysqli_fetch_assoc($result1)){
                    if($row['not_attend_7_14']>0){
                        $not_attend_7_14[] = $row;
                    }
                    if($row['not_attend_15_21']>0){
                        $not_attend_15_21[] = $row;
                    }
                    if($row['not_attend_22_30']>0){
                        $not_attend_22_30[] = $row;
                    }
                    if($row['not_attend_31_60']>0){
                        $not_attend_31_60[] = $row;
                    }
                    if($row['last_14_days']>0){
                        $last_14_days[] = $row;
                    }
                    if($row['last_30_days']>0){
                        $last_30_days[] = $row;
                    }
                }
                $all_details['Zero Attendance 7 to 14 days'] = $not_attend_7_14;
                $all_details['Zero Attendance 15 to 21 days'] = $not_attend_15_21;
                $all_details['Zero Attendance 22 to 30 days'] = $not_attend_22_30;
                $all_details['Zero Attendance 31+ days'] = $not_attend_31_60;
                $all_details['Attended 2 classes or less last 14 days'] = $last_14_days;
                $all_details['Attended 2 classes or less last 30 days'] = $last_30_days;

//              $error = array('status' => "Success", "msg" => $all_details);
//              $this->response($this->json($error), 401); // If no records "No Content" status
                
                $header .= "Zero Attendance 7 to 14 days,Buyer,Participant,Phone,Email,Last Attendance,Rank,Attendance Count" . "\r\n";
                for ($i = 0; $i < count($not_attend_7_14); $i++) {
                    $body .= "," . '"' . str_replace('"', "''", $not_attend_7_14[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_7_14[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_7_14[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_7_14[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_7_14[$i]['l_att']) . '"' . "," .
                            '"' . str_replace('"', "''", $not_attend_7_14[$i]['rank_value']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_7_14[$i]['att_count']) . '"' . "\r\n";
                }
                
                $body .= "\r\n" . "\r\n" . "Zero Attendance 15 to 21 days,Buyer,Participant,Phone,Email,Last Attendance,Rank,Attendance Count" . "\r\n";
                for ($i = 0; $i < count($not_attend_15_21); $i++) {
                    $body .= "," .'"' . str_replace('"', "''", $not_attend_15_21[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_15_21[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_15_21[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_15_21[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_15_21[$i]['l_att']) . '"' . "," .
                            '"' . str_replace('"', "''", $not_attend_15_21[$i]['rank_value']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_15_21[$i]['att_count']) . '"' . "\r\n";
                }
                
                $body .= "\r\n" . "\r\n" . "Zero Attendance 22 to 30 days,Buyer,Participant,Phone,Email,Last Attendance,Rank,Attendance Count" . "\r\n";
                for ($i = 0; $i < count($not_attend_22_30); $i++) {
                    $body .= ",".'"' . str_replace('"', "''", $not_attend_22_30[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_22_30[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_22_30[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_22_30[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_22_30[$i]['l_att']) . '"' . "," .
                            '"' . str_replace('"', "''", $not_attend_22_30[$i]['rank_value']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_22_30[$i]['att_count']) . '"' . "\r\n";
                }

                $body .= "\r\n" . "\r\n" . "Zero Attendance 31+ days,Buyer,Participant,Phone,Email,Last Attendance,Rank,Attendance Count" . "\r\n";
                for ($i = 0; $i < count($not_attend_31_60); $i++) {
                    $body .= ",".'"' . str_replace('"', "''", $not_attend_31_60[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_31_60[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_31_60[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_31_60[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_31_60[$i]['l_att']) . '"' . "," .
                            '"' . str_replace('"', "''", $not_attend_31_60[$i]['rank_value']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_31_60[$i]['att_count']) . '"' . "\r\n";
                }
                $body .= "\r\n" . "\r\n";
                $attachment[] = $file = __DIR__."/../uploads/".$company_id."_test1.csv";
                $ifp = fopen($file, "w");
                fwrite($ifp, $header);
                fwrite($ifp, $body);
                fclose($ifp);
//                header('Content-Type: application/x-download; charset=utf-8');
//                header('Content-Disposition: attachment; filename="' . basename($file) . '"');
//                header('Cache-Control: private, max-age=0, must-revalidate');
//                header('Pragma: public');

                $header2 = $body2 = '';
                $header2 .= "Attended 2 classes or less last 14 days,Buyer,Participant,Phone,Email,Last Attendance,Rank,Attendance Count";
                $header2 .= "\r\n";

                for ($i = 0; $i < count($last_14_days); $i++) {
                    $body2 .= "," . '"' . str_replace('"', "''", $last_14_days[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $last_14_days[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $last_14_days[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $last_14_days[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $last_14_days[$i]['l_att']) . '"' . "," .
                            '"' . str_replace('"', "''", $last_14_days[$i]['rank_value']) . '"' . "," . '"' . str_replace('"', "''", $last_14_days[$i]['att_count']) . '"' . "\r\n";
                }

                $body2 .= "\r\n" . "\r\n" . "Attended 2 classes or less last 30 days,Buyer,Participant,Phone,Email,Last Attendance,Rank,Attendance Count" . "\r\n";
                for ($i = 0; $i < count($last_30_days); $i++) {
                    $body2 .= "," . '"' . str_replace('"', "''", $last_30_days[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $last_30_days[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $last_30_days[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $last_30_days[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $last_30_days[$i]['l_att']) . '"' . "," .
                            '"' . str_replace('"', "''", $last_30_days[$i]['rank_value']) . '"' . "," . '"' . str_replace('"', "''", $last_30_days[$i]['att_count']) . '"' . "\r\n";
                }
                $body2 .= "\r\n" . "\r\n";

                $attachment[] = $file2 = __DIR__."/../uploads/".$company_id."_test2.csv";
                $ifp2 = fopen($file2, "w");
                fwrite($ifp2, $header2);
                fwrite($ifp2, $body2);
                fclose($ifp2);
//                header('Content-Type: application/x-download; charset=utf-8');
//                header('Content-Disposition: attachment; filename="' . basename($file2) . '"');
//                header('Cache-Control: private, max-age=0, must-revalidate');
//                header('Pragma: public');
                
                $subject = "Member Attendance Report";
                $trial_attendance_report=$this->generateTrialAttendanceReportDetails($company_id);
                if(is_array($trial_attendance_report)){
                    $message = "Hello,"."<br>"."Please see attached CSV file of your studio's attendance report."."<br>"."The file includes Four pages. The first file have zero attendance based a specified time for memberships. The second list members who have attended 2 classes or less within the past 14 days, and the past 30 days for memberships."."<br>"."The Third and Fourth files have the details containing trial attendance details. "."<br>"."<br>"."Thank you."."<br>"."<br>";
                    $attachment_final=array_merge($attachment ,$trial_attendance_report);
                }else{
                    $message = "Hello,"."<br>"."Please see attached CSV file of your studio's attendance report."."<br>"."The file includes two pages. The first file have zero attendance based a specified time for memberships. The second list members who have attended 2 classes or less within the past 14 days, and the past 30 days for memberships."."<br>"."<br>"."Thank you."."<br>"."<br>";
                    $attachment_final = $attachment;
                }
                $this->sendEmail($company_email,$subject,$message,$company_name,$company_email,$attachment_final,$fileNames,$cc_email_list);
                
            }else {
                $subject = "Member Attendance Report";
                $message = "Hello,"."<br>"."Please see attached CSV file of your studio's attendance report."."<br>"."The file includes two pages. The first file have zero attendance based a specified time for Trials. The second list members who have attended 2 classes or less within the past 14 days, and the past 30 days for Trials."."<br>"."<br>"."Thank you."."<br>"."<br>";
                $trial_attendance_report = $this->generateTrialAttendanceReportDetails($company_id);
                if(is_array($trial_attendance_report)){
                    $attachment_final = $trial_attendance_report;
                    $this->sendEmail($company_email,$subject,$message,$company_name,$company_email,$attachment_final,$fileNames,$cc_email_list);
                }else{                    
                    $error = array('status' => "Failed", "msg" => "No Content");
                    cj_log_info($this->json($error));
                }
            }
        }
        
    }
    
protected function generateTrialAttendanceReportDetails($company_id) {
        $header = $body = '';
        $attachment =$result_arr= [];
//        $fileNames = ["Trial Zero Attendance","Trial Attendance 2 or less"];
        $not_attend_7_14 = $not_attend_15_21 = $not_attend_22_30 = $not_attend_31_60 = $last_14_days = $last_30_days = [];
        $date_format = '%M %e, %Y';
        $query1 = sprintf("select tr.trial_reg_id, concat(tr.`buyer_last_name`,', ',tr.`buyer_first_name`) buyer_name, concat(tr.trial_registration_column_2, ', ', tr.trial_registration_column_1) participant_name, 
		tr.buyer_phone, tr.buyer_email,
		IFNULL((SELECT DATE_FORMAT(checkin_datetime,'%s') FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id`and status='N' ORDER BY `checkin_datetime` DESC LIMIT 0,1), '') l_att,
                concat('total ',IFNULL((SELECT SUM(1) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` AND   status='N' GROUP BY `company_id`, `trial_reg_id`),0)) att_count,
                sum( not_attend_7_14) not_attend_7_14, sum( not_attend_15_21) not_attend_15_21,
                sum( not_attend_22_30) not_attend_22_30, sum( not_attend_31_60) not_attend_31_60, 
                sum(case when last_14_days <= 2 then 1 else 0 end) last_14_days, sum(case when last_30_days <= 2 then 1 else 0 end) last_30_days
                from trial_attendance_dimension tad left join trial_registration tr ON tad.company_id=tr.company_id and tad.trial_reg_id=tr.trial_reg_id
                where tad.company_id = '%s' group by tad.company_id, tad.trial_reg_id", $date_format, mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            cj_log_info($this->json($error_log));
            return $this->json($error_log);
        }else{
            if(mysqli_num_rows($result1)>0){
                while($row = mysqli_fetch_assoc($result1)){
                    if($row['not_attend_7_14']>0){
                        $not_attend_7_14[] = $row;
                    }
                    if($row['not_attend_15_21']>0){
                        $not_attend_15_21[] = $row;
                    }
                    if($row['not_attend_22_30']>0){
                        $not_attend_22_30[] = $row;
                    }
                    if($row['not_attend_31_60']>0){
                        $not_attend_31_60[] = $row;
                    }
                    if($row['last_14_days']>0){
                        $last_14_days[] = $row;
                    }
                    if($row['last_30_days']>0){
                        $last_30_days[] = $row;
                    }
                }
                $all_details['Zero Attendance 7 to 14 days'] = $not_attend_7_14;
                $all_details['Zero Attendance 15 to 21 days'] = $not_attend_15_21;
                $all_details['Zero Attendance 22 to 30 days'] = $not_attend_22_30;
                $all_details['Zero Attendance 31+ days'] = $not_attend_31_60;
                $all_details['Attended 2 classes or less last 14 days'] = $last_14_days;
                $all_details['Attended 2 classes or less last 30 days'] = $last_30_days;

//              $error = array('status' => "Success", "msg" => $all_details);
//              $this->response($this->json($error), 401); // If no records "No Content" status
                
                $header .= "Zero Attendance 7 to 14 days,Buyer,Participant,Phone,Email,Last Attendance,Attendance Count" . "\r\n";
                for ($i = 0; $i < count($not_attend_7_14); $i++) {
                    $body .= "," . '"' . str_replace('"', "''", $not_attend_7_14[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_7_14[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_7_14[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_7_14[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_7_14[$i]['l_att']) . '"' . "," .
                              '"' . str_replace('"', "''", $not_attend_7_14[$i]['att_count']) . '"' . "\r\n";
                }
                
                $body .= "\r\n" . "\r\n" . "Zero Attendance 15 to 21 days,Buyer,Participant,Phone,Email,Last Attendance,Attendance Count" . "\r\n";
                for ($i = 0; $i < count($not_attend_15_21); $i++) {
                    $body .= "," .'"' . str_replace('"', "''", $not_attend_15_21[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_15_21[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_15_21[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_15_21[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_15_21[$i]['l_att']) . '"' . "," .
                              '"' . str_replace('"', "''", $not_attend_15_21[$i]['att_count']) . '"' . "\r\n";
                }
                
                $body .= "\r\n" . "\r\n" . "Zero Attendance 22 to 30 days,Buyer,Participant,Phone,Email,Last Attendance,Attendance Count" . "\r\n";
                for ($i = 0; $i < count($not_attend_22_30); $i++) {
                    $body .= ",".'"' . str_replace('"', "''", $not_attend_22_30[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_22_30[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_22_30[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_22_30[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_22_30[$i]['l_att']) . '"' . "," .
                             '"' . str_replace('"', "''", $not_attend_22_30[$i]['att_count']) . '"' . "\r\n";
                }

                $body .= "\r\n" . "\r\n" . "Zero Attendance 31+ days,Buyer,Participant,Phone,Email,Last Attendance,Attendance Count" . "\r\n";
                for ($i = 0; $i < count($not_attend_31_60); $i++) {
                    $body .= ",".'"' . str_replace('"', "''", $not_attend_31_60[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_31_60[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_31_60[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_31_60[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $not_attend_31_60[$i]['l_att']) . '"' . "," .
                              '"' . str_replace('"', "''", $not_attend_31_60[$i]['att_count']) . '"' . "\r\n";
                }
                $body .= "\r\n" . "\r\n";
                $attachment[] = $file = __DIR__."/../uploads/".$company_id."_test3.csv";
                $ifp = fopen($file, "w");
                fwrite($ifp, $header);
                fwrite($ifp, $body);
                fclose($ifp);

                $header2 = $body2 = '';
                $header2 .= "Attended 2 classes or less last 14 days,Buyer,Participant,Phone,Email,Last Attendance,Attendance Count";
                $header2 .= "\r\n";

                for ($i = 0; $i < count($last_14_days); $i++) {
                    $body2 .= "," . '"' . str_replace('"', "''", $last_14_days[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $last_14_days[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $last_14_days[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $last_14_days[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $last_14_days[$i]['l_att']) . '"' . "," .
                              '"' . str_replace('"', "''", $last_14_days[$i]['att_count']) . '"' . "\r\n";
                }

                $body2 .= "\r\n" . "\r\n" . "Attended 2 classes or less last 30 days,Buyer,Participant,Phone,Email,Last Attendance,Attendance Count" . "\r\n";
                for ($i = 0; $i < count($last_30_days); $i++) {
                    $body2 .= "," . '"' . str_replace('"', "''", $last_30_days[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $last_30_days[$i]['participant_name']) . '"' . "," . '"' . str_replace('"', "''", $last_30_days[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $last_30_days[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $last_30_days[$i]['l_att']) . '"' . "," .
                             '"' . str_replace('"', "''", $last_30_days[$i]['att_count']) . '"' . "\r\n";
                }
                $body2 .= "\r\n" . "\r\n";

                $attachment[] = $file2 = __DIR__."/../uploads/".$company_id."_test4.csv";
                $ifp2 = fopen($file2, "w");
                fwrite($ifp2, $header2);
                fwrite($ifp2, $body2);
                fclose($ifp2);
                
//                $subject = "Member Attendance Report";
//                $message = "Hello,"."<br>"."Please see attached CSV file of your studio's attendance report."."<br>"."The file includes two pages. One list members who have zero attendance based a specified time. The other list members who have attended 2 classes or less within the past 14 days, and the past 30 days."."<br>"."<br>"."Thank you."."<br>"."<br>";
//                $result_arr=$this->sendEmail($company_email,$subject,$message,$company_name,$company_email,$attachment,$fileNames,$cc_email_list);
//                $result_arr=$attachment;
                return $attachment;
            }else {
                $error = array('status' => "Failed", "msg" => "No Content");
                cj_log_info($this->json($error));                
                return $this->json($error);
            }
        }
        
    }
    
    protected function sendEmail($to, $subject, $message, $cmp_name, $reply_to, $attachment,$fileNames,$cc_email_list) {
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }            
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            if(!empty($attachment)){
                for($i=0;$i<count($attachment);$i++){
                $mail->AddAttachment($attachment[$i],"$fileNames[$i].csv");
               }
            }
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    if (filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)) {
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }        
        if(!empty($attachment)){
            for($i=0;$i<count($attachment);$i++){
                if(file_exists($attachment[$i])){
                    unlink($attachment[$i]);
                }
            }
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        cj_log_info($this->json($error_log));
        return $response;
    }
}

$api = new AttendanceReportCron;
$api->processApi();


