<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_old'){
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}
require_once __DIR__.'/../Stripe/init.php';
require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';

class StudioSubscriptionHandler{
    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $server_url, $env;
    
    public function __construct(){
        require_once __DIR__.'/../Globals/stripe_props.php';
        require_once __DIR__.'/../Globals/config.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        if(!empty($GLOBALS['HOST_NAME'])){
            $host = $GLOBALS['HOST_NAME'];
        }else{
            $host = $_SERVER['HTTP_HOST'];
        }
        if(strpos($host, 'dev.mystudio.academy') !== false){
            $this->server_url = "http://dev.mystudio.academy";
            $this->env = 'D';
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'https://www.mystudio.academy';
            $this->env = 'P';
        }else{
            $this->server_url = "http://dev.mystudio.academy";
            $this->env = 'D';
        }
    }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
        }
    }
    
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function processApi(){
        $this->runCronJobForStripe();
        $this->runCronJobForStripeStatusDailyCheck();
    }
    
    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
    
    //Cron Job for Stripe    
    private function runCronJobForStripe(){
        $this->getStripeKeys();
        
        $current_date = gmdate("Y-m-d");
        $total_records_processed =$outstanding_balance_c= 0;
        $start_time = gmdate("Y-m-d H:i:s");
        cj_log_info("runCronJobForStripe() - Started");
        $charge_type='free';
        $stripe_payment_status="";
        $query1 = sprintf("select `company_id`, `upgrade_status`, date(`expiry_dt`) expiry, stripe_currency_code, studio_expiry_level, `stripe_last_success_payment`  from `company` where `upgrade_status` in ('P','W','M','WM','B') AND stripe_subscription='Y' AND DATE(`expiry_dt`) = CURDATE()");
        $result1 = mysqli_query($this->db, $query1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            cj_log_info($this->json($error_log));
        } else {
            $num_of_rows1 = mysqli_num_rows($result1);
            if ($num_of_rows1 > 0) {
                $total_records_processed += $num_of_rows1;
                while ($row1 = mysqli_fetch_assoc($result1)) {
                    $company_id = $row1['company_id'];
                    $upgrade_status = $row1['upgrade_status'];
                    $expiry = $row1['expiry'];
                    $stripe_currency_code=$row1['stripe_currency_code'];
                    $studio_expiry_level=$row1['studio_expiry_level'];
                    $stripe_last_success_payment=$row1['stripe_last_success_payment'];
                
                    $query2 = sprintf("select * from `stripe_studio_subscription` where `company_id`='%s' and `premium_type`='%s' and `status`='A' and 
                                `expiry_date`='%s' AND `deleted_flag`!='Y' AND `subscription_end_date` IS NULL ORDER BY sss_id DESC LIMIT 0,1", $company_id, $upgrade_status, $expiry);
                    $result2 = mysqli_query($this->db, $query2);
                    if (!$result2) {
                        $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                        cj_log_info($this->json($error_log2));
                    } else {
                        $num_of_rows2 = mysqli_num_rows($result2);
                        if ($num_of_rows2 > 0) {
                            $row2 = mysqli_fetch_assoc($result2);
                            $sss_id = $row2['sss_id'];
                            $expiry_date = $row2['expiry_date'];
                            $stripe_customer_id=$row2['stripe_customer_id'];
                            $failed_outstanding_balance = $succeed_outstanding_balance = $outstanding_balance=$row2['outstanding_balance'];

                            if($expiry_date==$current_date){
                                $subscription_fee = $this->getSubscriptionPlanFee($company_id,$upgrade_status);
                                if(!empty($subscription_fee)){
                                    $plan_fee=$subscription_fee['current_plan_fee'];
                                }
                                if($outstanding_balance<0){
                                    if(-1*$outstanding_balance>=$plan_fee){
                                        $calculated_payment_amount=0;
//                                        $calculated_payment_amount=number_format(-1*$outstanding_balance-$plan_fee, 2, '.', '');//100-29=71
                                        $succeed_outstanding_balance += $plan_fee;
                                        $charge_type='free';
                                        $stripe_payment_status="old_credit";
                                    }else{
                                        $calculated_payment_amount=number_format($plan_fee+$outstanding_balance, 2, '.', '');//29-14=15
                                        $succeed_outstanding_balance = 0;
                                        $failed_outstanding_balance = $plan_fee + $outstanding_balance;
                                    }
                                }else{
                                    $calculated_payment_amount=number_format($outstanding_balance+$plan_fee, 2, '.', '');
                                    $succeed_outstanding_balance = 0;
                                    $failed_outstanding_balance = $plan_fee + $outstanding_balance;
                                }

                                $stripe_charge_id=$body3_arr=$brand=$last4=$stripe_old_card_id=$stripe_card_name=$payment_desc = $future_payment_date='';
                                if ($upgrade_status == 'W') {
                                    $future_payment_date = gmdate('Y-m-d', strtotime("+1 year"));
                                    $payment_desc = "MyStudio White Label Membership";
                                }else if($upgrade_status == 'P'){
                                    $future_payment_date = gmdate('Y-m-d', strtotime("+1 year"));
                                    $payment_desc = "MyStudio Premium Yearly Membership";
                                }else if($upgrade_status == 'M'){
                                    $future_payment_date = gmdate('Y-m-d', strtotime('first day of next month'));
                                    $payment_desc = "MyStudio Premium Monthly Membership";
                                }else if($upgrade_status == 'WM'){
                                    $future_payment_date = gmdate('Y-m-d', strtotime('first day of next month'));
                                    $payment_desc = "MyStudio Whitelabel Monthly Membership";
                                }else if($upgrade_status == 'B'){
                                    $future_payment_date = gmdate('Y-m-d', strtotime('first day of next month'));
                                    $payment_desc = "MyStudio Basic Monthly Membership";
                                }
                            }else{
                                $error = array('status' => "Failed", "msg" => "Something went wrong in your subscription plan. Expiration date mismatched for studio($company_id).","expiry_date"=>$expiry_date,"current date"=>$current_date);
                                cj_log_info($this->json($error));
                                continue;
                            }
                            
                            \Stripe\Stripe::setApiKey($this->stripe_sk);

                            try {
                                $err = [];
                                if($calculated_payment_amount>=1){
                                // Use Stripe's library to make requests...            
                                    $stripe_charge_create = \Stripe\Charge::create([
                                        "amount" => ($calculated_payment_amount*100),
                                        "currency" => "$stripe_currency_code",
                                        "customer" => "$stripe_customer_id", 
                                        "description" => "$payment_desc",
                                        "statement_descriptor" => substr($payment_desc,0,21)
                                    ]);
                                    $body3 = json_encode($stripe_charge_create);
                                    stripe_log_info("Customer_old_update : $body3");
                                    $body3_arr = json_decode($body3, true);
                                    $stripe_charge_id = $body3_arr['id'];
                                    $brand=$body3_arr['source']['brand'];
                                    $last4 = $body3_arr['source']['last4'];
                                    $stripe_old_card_id=$body3_arr['source']['id'];
                                    $charge_type=$body3_arr['source']['object'];
                                    $stripe_payment_status=$body3_arr['status'];
                                    $temp=' xxxxxx';
                                    $stripe_card_name=$brand.$temp.$last4;
                                }
                            } catch(\Stripe\Error\Card $e) {
                                // Since it's a decline, \Stripe\Error\Card will be caught     
                                    $body = $e->getJsonBody();
                                    $err  = $body['error'];
                                    $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\RateLimit $e) {
                              // Too many requests made to the API too quickly     
                                    $body = $e->getJsonBody();
                                    $err  = $body['error'];
                                    $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\InvalidRequest $e) {
                              // Invalid parameters were supplied to Stripe's API     
                                    $body = $e->getJsonBody();
                                    $err  = $body['error'];
                                    $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Authentication $e) {
                              // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                                    $body = $e->getJsonBody();
                                    $err  = $body['error'];
                                    $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\ApiConnection $e) {
                              // Network communication with Stripe failed     
                                    $body = $e->getJsonBody();
                                    $err  = $body['error'];
                                    $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Base $e) {
                              // Display a very generic error to the user, and maybe send yourself an email     
                                    $body = $e->getJsonBody();
                                    $err  = $body['error'];
                                    $err['status_code'] = $e->getHttpStatus();
                            } catch (Exception $e) {
                              // Something else happened, completely unrelated to Stripe     
                                    $body = $e->getJsonBody();
                                    $err  = $body['error'];
                                    $err['status_code'] = $e->getHttpStatus();
                            }
                            if(isset($err) && !empty($err)){
                                $error = array('status' => "Failed", "msg" => $err['message']);
                                stripe_log_info($this->json($error));
                                $this->sendEmailForAdmins($error['msg']);
                                $charge_type = "card";
                                $stripe_payment_status='';
                    //            $this->response($this->json($error), 200);// If no records "No Content" status
                            }
                            
                            if($stripe_payment_status=="succeeded" || $stripe_payment_status=="captured" || $stripe_payment_status=="old_credit"){
                                $payment_success=true;
                                $last_successfull_payment_date = $current_date;
                                $studio_expiry_level_new='A';
                                $outstanding_balance_c=$succeed_outstanding_balance;
                                $sub_sts = 'Y';
                                $stripe_last_success_payment = $current_date;
                                $s_payment_status = 'A';
                            }else{
                                $payment_success=false;
                                $studio_expiry_level_new=$studio_expiry_level;
                                $outstanding_balance_c = $failed_outstanding_balance;
                                $sub_sts = 'N';
                                $stripe_last_success_payment=$expiry;
                                $last_successfull_payment_date = $expiry_date;
                                $s_payment_status = 'F';
                            }
                            $sql2 = sprintf("update `company` set `studio_payment_status`='$s_payment_status', `expiry_dt`='%s', `subscription_status`='$sub_sts', `studio_expiry_level`='$studio_expiry_level_new', `stripe_last_success_payment`='$stripe_last_success_payment' WHERE `company_id`='%s'",
                                    mysqli_real_escape_string($this->db,$future_payment_date),mysqli_real_escape_string($this->db,$company_id));
                            $sql_result2 = mysqli_query($this->db, $sql2);
                            if(!$sql_result2){
                                $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                cj_log_info($this->json($err_log));
                            }

                            $update_query = sprintf("UPDATE `stripe_studio_subscription` SET `expiry_date`='%s', `next_payment_date`='%s', `last_successfull_payment_date`='$last_successfull_payment_date', `outstanding_balance`='%s' WHERE `sss_id`='%s' AND `stripe_customer_id`='%s' AND `company_id`='%s' AND `deleted_flag`!='Y' ",
                                        $future_payment_date,$future_payment_date,mysqli_real_escape_string($this->db,$outstanding_balance_c),mysqli_real_escape_string($this->db, $sss_id), mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $company_id));
                            $result_update_query = mysqli_query($this->db, $update_query);
                            if(!$result_update_query){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                                cj_log_info($this->json($error_log));
                            }else{
                                if(mysqli_affected_rows($this->db)>0){
                                    if($payment_success==true){
                                        $insert_query2 = sprintf("INSERT INTO `stripe_studio_subscription_payments`(`company_id`, `sss_id`, `source_id`, `charge_id`, `charge_amount`, `payment_amount`, `charge_status`,`charge_type`) 
                                        VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $sss_id),
                                                mysqli_real_escape_string($this->db, $stripe_old_card_id), mysqli_real_escape_string($this->db, $stripe_charge_id),
                                                mysqli_real_escape_string($this->db, $calculated_payment_amount), mysqli_real_escape_string($this->db, $plan_fee), 
                                                mysqli_real_escape_string($this->db, $stripe_payment_status), mysqli_real_escape_string($this->db, $charge_type));
                                        $result_query2 = mysqli_query($this->db, $insert_query2);
                                        if (!$result_query2) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query2");
                                            cj_log_info($this->json($error_log));
                                        }else{
                                            $res = array("status" => "Success", "msg" => "Subscription Sucessfully Updated for studio($company_id).", "studio_expiry_level"=>$studio_expiry_level_new);
                                            cj_log_info($this->json($res));

                                        }
                                    }else if($payment_success==false){
                                        $insert_query2 = sprintf("INSERT INTO `stripe_studio_subscription_payments_history`(`company_id`, `sss_id`, `source_id`, `charge_id`, `charge_amount`, `payment_amount`, `charge_status`,`charge_type`) 
                                                VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $sss_id),
                                                mysqli_real_escape_string($this->db, $stripe_old_card_id), mysqli_real_escape_string($this->db, $stripe_charge_id),
                                                mysqli_real_escape_string($this->db, $calculated_payment_amount), mysqli_real_escape_string($this->db, $plan_fee), 
                                                mysqli_real_escape_string($this->db, $stripe_payment_status), mysqli_real_escape_string($this->db, $charge_type));
                                        $result_query2 = mysqli_query($this->db, $insert_query2);
                                        if (!$result_query2) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query2");
                                            cj_log_info($this->json($error_log));
                                        }else{
                                            $res = array("status" => "Success", "msg" => "Payment not successfull for studio($company_id).", "studio_expiry_level"=>$studio_expiry_level_new);
                                            cj_log_info($this->json($res));
                                        } 
                                    }
                                }else{
                                    $res = array("status" => "Failed", "msg" => " Registration Details Not Updated .","studio_expiry_level"=>$studio_expiry_level_new,"company_id"=>$company_id);
                                    cj_log_info($this->json($res));
                                }
                            }
                        }else{
                            $res = array("status" => "Failed", "msg" => "Invalid stripe subscription record details.","company_id"=>$company_id);
                            cj_log_info($this->json($res));
                        }
                    }
                }                                                
            }else{
                $error_log = array('status' => "Failed", "msg" => "No studio details available to charge for $current_date");
                cj_log_info($this->json($error_log));
            }
        }
               
        cj_log_info("runCronJobForStripe() - Completed");
        if($total_records_processed>0){
            $end_time = gmdate("Y-m-d H:i:s");
            $cron_file_name = "StudioSubscriptionHandler";
            $cron_function_name = "runCronJobForStripe";
            $cron_message = "Total Stripe access for studio validity";
            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
            if(!$cron_log_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
                cj_log_info($this->json($error_log));
            }
        }
    }
    
    private function runCronJobForStripeStatusDailyCheck(){        
        $current_date = gmdate("Y-m-d");
        $stu_exp_lev=$diff=$diff2='';
        $check_trial_period = 0;
        $error_send = 0;
        cj_log_info("runCronJobForStripeStatusDailyCheck() - Started");
        $query1 = sprintf("SELECT `company_id`, `upgrade_status`, date(`expiry_dt`) expiry, studio_expiry_level, stripe_last_success_payment,studio_payment_status FROM `company` where `upgrade_status` IN ('T','P','W','M','WM','B') AND `stripe_subscription`='Y'  AND `deleted_flag`='N'");
        $result1 = mysqli_query($this->db, $query1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            cj_log_info($this->json($error_log));
            $error_send = 1;
        } else {
            $num_of_rows1 = mysqli_num_rows($result1);
            if ($num_of_rows1 > 0) {
                while ($row1 = mysqli_fetch_assoc($result1)) {
                    $outstanding_balance = 0;
                    $company_id = $row1['company_id'];
                    $upgrade_status = $row1['upgrade_status'];
                    $expiry = $row1['expiry'];
                    $studio_expiry_level = $row1['studio_expiry_level'];
                    $stripe_last_success_payment = $row1['stripe_last_success_payment'];
                    $studio_ps = $row1['studio_payment_status'];
                    
                    if($upgrade_status == 'T'){
                        $diff = strtotime($expiry)-strtotime($current_date);
                    }else{
                        if($studio_expiry_level=='L2'){
                            continue;
                        }
                        $query2 = sprintf("SELECT * FROM `stripe_studio_subscription` WHERE company_id='%s' AND deleted_flag!='Y' AND `premium_type`='%s' 
                                AND `subscription_end_date` IS NULL AND `expiry_date`='%s' ORDER BY sss_id DESC LIMIT 0,1", $company_id, $upgrade_status, $expiry);
                        $result2 = mysqli_query($this->db, $query2);
                        if(!$result2){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                            cj_log_info($this->json($error_log));
                            continue;
                            $error_send = 1;
                        }else{
                            if(mysqli_num_rows($result2)>0){
                                $row2 = mysqli_fetch_assoc($result2);
                                $outstanding_balance = $row2['outstanding_balance'];
                                $last_successfull_payment_date = $row2['last_successfull_payment_date'];
                                $sss_id=$row2['sss_id'];
                                
                                if($outstanding_balance<=0 && $upgrade_status!='T'){
                                    continue;
                                }
                            }else{
                                $error_log = array('status' => "Failed", "msg" => "Subscription details not found.", "company_id"=>$company_id);
                                cj_log_info($this->json($error_log));
                                $error_send = 1;
                                continue;
                            }
                        }
                        if(is_null($stripe_last_success_payment) || empty($stripe_last_success_payment) || $stripe_last_success_payment=='0000-00-00'){
                            if(is_null($last_successfull_payment_date) || empty($last_successfull_payment_date) || $last_successfull_payment_date=='0000-00-00'){
                                $diff = strtotime($expiry)-strtotime($current_date);
                            }else{
                                $diff = strtotime($last_successfull_payment_date)-strtotime($current_date);
                            }
                        }else{
                            $diff = strtotime($stripe_last_success_payment)-strtotime($current_date);
                        }
                    }
                    
                    $diff2 = round($diff / (60 * 60 * 24));
                    if($upgrade_status=='T'){
                        if($diff2<=0){
                            $stu_exp_lev = 'A';
                            $sss_status='F';
                            $sub_status='N';
                        }else{
                            $check_trial_period = 1;
                        }
                        $studio_ps='N';
                    }else if ($upgrade_status!='T' && $diff2 > 0) {
                        continue;
                    } else if($upgrade_status!='T' && $diff2>=-6) {
                        $stu_exp_lev='L1';
                        $sss_status=$upgrade_status;
                        $sub_status='N';
                        $studio_ps='F';
                    }else if($upgrade_status!='T') {
                        $stu_exp_lev='L2';
                        $sss_status=$upgrade_status;
                        $sub_status='N';
                        $studio_ps='F';
                    }
                    if($check_trial_period==1){
                        continue;
                    }else{
                        $update=sprintf("UPDATE `company` SET `studio_expiry_level`='$stu_exp_lev',`subscription_status`='$sub_status',`upgrade_status`='$sss_status',`studio_payment_status`='$studio_ps' WHERE `company_id`='$company_id'");
                        $result_update = mysqli_query($this->db, $update);
                        if (!$result_update) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update","company_id"=>$company_id);
                            cj_log_info($this->json($error_log));
                            $error_send = 1;
                        }else{
                            $update2=sprintf("UPDATE `stripe_studio_subscription` SET `premium_type`='$sss_status' WHERE `sss_id`='$sss_id' AND `company_id`='$company_id'");
                            $result_update2 = mysqli_query($this->db, $update2);
                            if (!$result_update2) {
                                $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update2","company_id"=>$company_id);
                                cj_log_info($this->json($error_log2));
                                $error_send = 1;
                            }
                        }
                    }
                }
            }
        }
        if($error_send == 1){
            $this->sendEmailForAdmins("Something went wrong in Studio Subscription Handler, check log");
        }
        cj_log_info("runCronJobForStripeStatusDailyCheck() - Completed");
    }
    
    private function getSubscriptionPlanFee($company_id,$upgrade_status){
        $plan_fee_arr=$plan_fee_arr_final=[];
        $plan_fee_arr_final['current_plan_fee']=$plan_fee_arr_final['current_plan_days']=$plan_fee_arr_final['current_plan_setup_fee']=0;
            $select_fee=sprintf("SELECT * FROM `studio_subscription_fee` WHERE 
                IF((SELECT count(*) count FROM `studio_subscription_fee` WHERE `company_id`='%s')=0,`company_id`=0,
                   (`company_id`='%s' or (`company_id`=0 and `premium_type` not in (select `premium_type` from studio_subscription_fee WHERE `company_id`='%s'))))",
                    mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $company_id));
        $result=mysqli_query($this->db,$select_fee);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_fee");
            cj_log_info($this->json($error_log));
        } else {
            $nums_rows = mysqli_num_rows($result);
            if ($nums_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $plan_fee_arr['plan_fee'] = $rows['plan_fee']+0;
                    $plan_fee_arr['setup_fee'] = $rows['setup_fee']+0;
                    $plan_fee_arr['premium_type'] = $rows['premium_type'];
                    $plan_fee_arr['plan_days'] = $rows['plan_days'];
                    if($plan_fee_arr['premium_type']==$upgrade_status){
                        $plan_fee_arr_final['current_plan_fee']=$plan_fee_arr['plan_fee'];
                        $plan_fee_arr_final['current_plan_days']=$plan_fee_arr['plan_days'];
                        $plan_fee_arr_final['current_plan_setup_fee']=$plan_fee_arr['setup_fee'];
                    }
                    $plan_fee_arr_final['plans'][]=$plan_fee_arr;
                }
            }
        }        
        return $plan_fee_arr_final;
    }
    
    private function sendEmailForAdmins($msg){
        if($this->env=='P'){
            $env = "Production";
        }else{
            $env = "Development";
        }
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com,ravi@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "$env - Stripe Subscription Handling Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        cj_log_info($this->json($error_log));
        return $response;
    }
    
        
}

$api = new StudioSubscriptionHandler;
$api->processApi();