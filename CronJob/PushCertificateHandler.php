<?php
$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}
require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';


class PushCertificateHandler{
    
    private $db = NULL;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    private $server_url, $cron_props_file, $cron_sf, $cron_ps, $cron_ev, $cron_ms, $cron_ms_lt, $env;
    
    public function __construct(){
        require_once __DIR__.'/../Globals/config.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        if(!empty($GLOBALS['HOST_NAME'])){
            $host = $GLOBALS['HOST_NAME'];
        }else{
            $host = $_SERVER['HTTP_HOST'];
        }
        if(strpos($host, 'dev.mystudio.academy') !== false){
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'https://www.mystudio.academy';
            $this->cron_props_file = "cron_prod_props.php";
            $this->env = 'P';
        }else{
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        }
    }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
        }
    }
    
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function processApi(){
        $this->checkMobilePushCertificateexpiration();
    }
    
    private function checkMobilePushCertificateexpiration() {
        cj_log_info("checkMobilePushCertificateexpiration() - Started");
        $expired_cert = $expired_cert['development'] = $expired_cert['production'] = [];
        
        $check_query = sprintf("select * from `mobile_push_certificate` where type='I'");
        $result_check = mysqli_query($this->db, $check_query);
        if (!$result_check) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_query");
            cj_log_info($this->json($error_log));
            $this->sendEmailForAdmins("Internal Server Error.");
        }else{
            $num_rows = mysqli_num_rows($result_check);
            if ($num_rows > 0) {
                while($rows = mysqli_fetch_assoc($result_check)){
                    $check_prod = $rows['prod_pem'];
                    $parsed_prod = openssl_x509_parse($check_prod);
                    if(date("Y-m-d",$parsed_prod['validTo_time_t']) < date("Y-m-d", strtotime("+2 week"))){
                        $error_log = array('status' => "Failed", "msg" => $rows['bundle_id'], "type" => 'production');
                        cj_log_info($this->json($error_log));
                        $expired_cert['production'][] = $rows['bundle_id'];
                    }
                    $check_dev = $rows['dev_pem'];
                    $parsed_dev = openssl_x509_parse($check_dev);
                    if(date("Y-m-d",$parsed_dev['validTo_time_t']) < date("Y-m-d", strtotime("+2 week"))){
                        $error_log = array('status' => "Failed", "msg" => $rows['bundle_id'], "type" => 'development');
                        cj_log_info($this->json($error_log));
                        $expired_cert['development'][] = $rows['bundle_id'];
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records available for update.");
                cj_log_info($this->json($error_log));
            }
        }
        if(!empty($expired_cert['production']) || !empty($expired_cert['development'])){
            $message = "Expired bundle Id list :<br>";
            if(!empty($expired_cert['production'])){
                $prod_bundle_id = implode(", ",$expired_cert['production']);
                $message .= "Production - $prod_bundle_id <br>";
            }
            if(!empty($expired_cert['development'])){
                $dev_bundle_id = implode(", ",$expired_cert['development']);
                $message .= "Development - $dev_bundle_id";
            }
            $this->sendEmailForAdmins($message);
        }
        cj_log_info("checkMobilePushCertificateexpiration() - Completed");
    }
    
    private function sendEmailForAdmins($msg){
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com, vigneshwari@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "Push Certificate Expired Handler Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        cj_log_info($this->json($error_log));
        return $response;
    }
    
}

$api = new PushCertificateHandler;
$api->processApi();
?>