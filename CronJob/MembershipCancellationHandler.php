<?php
$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_old'){
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}
require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';


class MembershipCancellationHandler{
    
    private $db = NULL;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    private $server = '';
    private $server_url, $cron_props_file, $cron_sf, $cron_ps, $cron_ev, $cron_ms, $cron_ms_lt, $env;
    
    public function __construct(){
        require_once __DIR__.'/../Globals/config.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        if(!empty($GLOBALS['HOST_NAME'])){
            $host = $GLOBALS['HOST_NAME'];
        }else{
            $host = $_SERVER['HTTP_HOST'];
        }
        $this->server = $host;
        if(strpos($host, 'dev.mystudio.academy') !== false){
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'https://www.mystudio.academy';
            $this->cron_props_file = "cron_prod_props.php";
            $this->env = 'P';
        }else{
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        }
        $this->getUserandSecret();
    }
    
    private function getUserandSecret(){
        require_once "$this->cron_props_file";
        $this->cron_sf = SF_CRON;
        $this->cron_ps = PS_CRON;
        $this->cron_ev = EV_CRON;
        $this->cron_ms = MS_CRON;
        $this->cron_ms_lt = MS_LT_CRON;
    }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
        }
    }
    
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function processApi(){
        $this->runCronJobForProcessScheduledCancellation();
        $this->sendReceiptForMembershipCompletion();
    }
    
   
    private function runCronJobForProcessScheduledCancellation(){
        cj_log_info("runCronJobForProcessScheduledCancellation() - Started");
        $reg_id_array = [];
        if(isset($_REQUEST['curr_date']) && !empty($_REQUEST['curr_date'])){
            $curr_date = $_REQUEST['curr_date'];
        }elseif(isset($argv[2]) && !empty($argv[2])){
            $curr_date = $argv[2];
        }else{
            $curr_date = gmdate("Y-m-d");
        }
        $query = sprintf("SELECT * FROM `membership_registration` WHERE `membership_status` IN ('R') AND `cancelled_date`<='$curr_date'");
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $reg_id = $row['membership_registration_id'];
                    $company_id = $row['company_id'];
                    $membership_id = $row['membership_id'];
                    $membership_option_id = $row['membership_option_id'];
                    $membership_status = 'C';
                    $old_mem_status =  $row['membership_status'];
                    $membership_structure = $row['membership_structure'];
                    $membership_title=$row['membership_category_title'];
                    $membership_option_title=$row['membership_title'];
                    
                    $get_payment = sprintf("SELECT * FROM `membership_payment` WHERE `membership_registration_id`='%s' AND `payment_status` IN ('N','P') AND `payment_date`>='$curr_date'", mysqli_real_escape_string($this->db, $reg_id));
                    $result_get_payment = mysqli_query($this->db, $get_payment);
                    if (!$result_get_payment) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_payment");
                        cj_log_info($this->json($error_log));
                       continue;
                    } else {
                        $num_rows_get = mysqli_num_rows($result_get_payment);
                        if ($num_rows_get > 0) {
                            $update_payment = sprintf("DELETE FROM `membership_payment` WHERE `membership_registration_id`='%s' AND `payment_status` IN ('N','P') AND `payment_date`>='$curr_date'", mysqli_real_escape_string($this->db, $reg_id));
                            $result_update_payment = mysqli_query($this->db, $update_payment);
                            if (!$result_update_payment) {
                                if ($membership_status == 'C' && (mysqli_errno($this->db)==1451 || mysqli_errno($this->db)=='1451')) {
                                    $update_payment2 = sprintf("UPDATE `membership_payment` SET `payment_status`='C' WHERE `membership_registration_id`='%s' AND `payment_status` IN ('N','P') AND `payment_date`>='$curr_date'", mysqli_real_escape_string($this->db, $reg_id));
                                    $result_update_payment2 = mysqli_query($this->db, $update_payment2);
                                    if (!$result_update_payment2) {
                                        $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_payment2");
                                        cj_log_info($this->json($error_log2));
                                        continue;
                                    }
                                }else{
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_payment");
                                    cj_log_info($this->json($error_log));
                                    continue;
                                }
                            }
                        }
                    }
                
                    $update_registration = sprintf("UPDATE `membership_registration` SET `membership_status`='C', `hold_date`=NULL, `resume_date`=NULL
                            WHERE `membership_registration_id`='%s' AND `membership_status` in ('R')", mysqli_real_escape_string($this->db, $reg_id));
                    $result_update_registration = mysqli_query($this->db, $update_registration);
                    if (!$result_update_registration) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_registration");
                        cj_log_info($this->json($error_log));
                        continue;
                    }
                    
                    $activity_text = "Status changed to cancel";
                    $currdate = gmdate("Y-m-d H:i:s");
                    $insert_history = sprintf("INSERT INTO `membership_history`(`company_id`, `membership_registration_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), 'status', 
                            mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $currdate));
                    $result_history = mysqli_query($this->db, $insert_history);
                    if (!$result_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                        cj_log_info($this->json($error_log));
                        continue;
                    } else {
                        $update_history = sprintf("UPDATE `membership_history` SET `activity_type`='scheduled cancel completed' WHERE `company_id`='%s' AND `membership_registration_id`='%s' AND `activity_type`='scheduled cancel' ", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id));
                        $result_update_history = mysqli_query($this->db, $update_history);
                        if (!$result_update_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_history");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }

                    $c_mail = $this->sendReceiptForMembershipCancellation($company_id, $reg_id);
                    $res = $this->updateMembershipDimensionsMembers($company_id, $membership_id, $membership_status, $membership_option_id, $old_mem_status, $reg_id,$membership_title,$membership_option_title);
                    if($res==0 || $c_mail==1){
                        $reg_id_array[] = $reg_id;
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No membership registration available for cancellation.");
                cj_log_info($this->json($error_log));
            }
        }
         if(!empty($reg_id_array)){
            $reg_str = implode(", ", $reg_id_array);
            $msg = "Something went wrong in Membership Cancellation Handler for membership_registration_ids=$reg_str, check cron log or table";
            $this->sendEmailForAdmins($msg);
        }
            cj_log_info("runCronJobForProcessScheduledCancellation() - Completed");
    }
    
    public function sendReceiptForMembershipCompletion() {
        $triggerC = $triggerNC = 0;
        $total_records_processed = 0;
        $start_time = gmdate("Y-m-d H:i:s");
        cj_log_info("sendReceiptForMembershipCompletion() - Started");
        $return_value = 0;
        if (isset($_REQUEST['curr_date'])) {
            $curr_date = $_REQUEST['curr_date'];
        } else {
            $get_date = mysqli_query($this->db, "select CURDATE() curr_date, CURDATE()-INTERVAL 1 DAY prev_date;");
            $row = mysqli_fetch_assoc($get_date);
            $curr_date = $row['curr_date'];
//            $curr_date_1 = "'" . $row['curr_date'] . "','" . $row['prev_date'] . "'";
        }
        
        $sql = sprintf("SELECT  mr.`company_id`, mr.`membership_status`, mr.`payment_frequency`,  mr.`membership_structure`, mr.`membership_registration_id`,mr.`membership_option_id`,mr.`membership_id`, mr.`membership_category_title`,mr.`membership_title`,
                        mr.`billing_options`, mr.`billing_options_no_of_payments`, mr.`billing_options_deposit_amount`, mr.`no_of_payments`, mr.`payment_type`, mr.`no_of_classes`,
                        mr.`specific_end_date`, IFNULL(mr.`billing_options_expiration_date`,'0000-00-00' ) billing_options_expiration_date, IFNULL(mr.`payment_start_date`,'0000-00-00') payment_start_date, 
                        IFNULL((SELECT MAX(`payment_date`) from `membership_payment` WHERE `membership_registration_id`=mr.`membership_registration_id`  AND `payment_status` IN ('N', 'S', 'F', 'P', 'R')),'0000-00-00') last_payment_date,
                        IFNULL((SELECT `payment_status` from `membership_payment` WHERE `membership_registration_id`=mr.`membership_registration_id` AND `payment_status` IN ('N', 'F', 'P') LIMIT 0,1),'') check_payment_status,
                        IF(`membership_structure`!='NC', 1, IFNULL((SELECT COUNT(*) FROM `attendance` WHERE `company_id`= mr.`company_id` AND `membership_registration_id`= mr.`membership_registration_id` AND `status`='N' ),0)) att_count
                        FROM membership_registration mr
                        WHERE mr.`membership_status` IN ('R') AND membership_structure IN ('NC','C','SE') AND (mr.`end_email_date` IS  NULL || mr.`end_email_date`='0000-00-00')");
        $result_sql = mysqli_query($this->db, $sql);
        if (!$result_sql) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            cj_log_info($this->json($error_log));
            return;
        } else {
            $num_rows = mysqli_num_rows($result_sql);
            if ($num_rows > 0) {
                while ($row1 = mysqli_fetch_assoc($result_sql)) {
                    $company_id = $row1['company_id'];
                    $membership_reg_id = $row1['membership_registration_id'];
                    $membership_registration_id_1 = $row1['membership_registration_id'];
                    $membership_id_1 = $row1['membership_id'];
                    $membership_option_id_1 = $row1['membership_option_id'];
                    $membership_structure = $row1['membership_structure'];
                    $membership_status_old = $row1['membership_status'];
                    $membership_category_title_1 = $row1['membership_category_title'];
                    $membership_title_1 = $row1['membership_title'];
                    $billing_options_no_of_payments = $row1['billing_options_no_of_payments'];
                    $no_of_payments = $row1['no_of_payments'];
                    $billing_options = $row1['billing_options'];
                    $billing_options_deposit_amount = $row1['billing_options_deposit_amount'];
                    $billing_options_expiration_date = $row1['billing_options_expiration_date'];
                    $specific_end_date = $row1['specific_end_date'];
                    $last_payment_date = $row1['last_payment_date'];
                    $check_payment_status = $row1['check_payment_status'];
                    $no_of_classes = $row1['no_of_classes'];
                    $att_count = $row1['att_count'];
                    $payment_type = $row1['payment_type'];
                    if($membership_structure=='SE' && $specific_end_date>$curr_date){
                        continue;
                    }
                    if($membership_structure=='C'){
                        if($check_payment_status!=''){
                            continue;
                        }                            
                        if($billing_options_expiration_date=='0000-00-00'){
                            $triggerC = 1;
                            cj_log_info("Invalid expiration date in Custom structure(reg_id : $membership_reg_id)");
                            continue;
                        }
                        if($payment_type!='R' && $billing_options_expiration_date>$curr_date){
                            continue;
                        }else{
                            if($billing_options_expiration_date>$curr_date || $last_payment_date>$curr_date){
                                continue;
                            }
                        }
                    }elseif($membership_structure=='NC'){
                        if($no_of_classes==0){
                            $triggerNC = 1;
                            cj_log_info("Invalid no of classes in Class Package structure(reg_id : $membership_reg_id)");
                            continue;
                        }
                        if($payment_type!='R'){
                            if($billing_options_expiration_date=='0000-00-00'){
                                if($att_count<$no_of_classes){
                                    continue;
                                }
                            }elseif($billing_options_expiration_date>$curr_date && $att_count<$no_of_classes){
                                continue;
                            }
                        }else{
                            if($check_payment_status!=''){
                                continue;
                            }
                            if($billing_options_expiration_date=='0000-00-00'){
                                if($att_count<$no_of_classes || $last_payment_date>$curr_date){
                                    continue;
                                }
                            }else{
                                if($billing_options_expiration_date>$curr_date || $att_count<$no_of_classes || $last_payment_date>$curr_date){
                                    continue;
                                }
                            }
                        }
                    }                
                    $total_records_processed++;
                    $this->addMembershipDimensions($company_id, $membership_id_1, $membership_option_id_1, $membership_category_title_1, $membership_title_1, '');
                    $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
                    $payment_details = [];
                    $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
                    $recurring_start_date = $membership_start_date = $payment_start_date = $payment_frequency = $custom_recurring_frequency_period_type = '';
                    $membership_structure = $billing_options = $billing_options_no_of_payments = '';

                    $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, mp.`cc_name`, `waiver_policies`, mr.credit_card_name, 
            mr.`membership_start_date`, mr.`payment_start_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
            `buyer_postal_code`, mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, mp.`created_dt` , mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`
            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id` LEFT JOIN `student` s ON s.`company_id`= mr.`company_id` AND s.`student_id`= mr.`student_id`
            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N') AND membership_structure IN ('NC','C','SE')
            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
                    $result = mysqli_query($this->db, $query);
//                    
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                        cj_log_info($this->json($error_log));
                        return;
                    } else {
                        $num_rows = mysqli_num_rows($result);
                        if ($num_rows > 0) {
                            while ($row = mysqli_fetch_assoc($result)) {
                                $studio_name = $row['company_name'];
                                $studio_mail = $row['email_id'];
                                $cc_email_list = $row['referral_email_list'];
                                $wp_currency_code = $row['wp_currency_code'];
                                $wp_currency_symbol = $row['wp_currency_symbol'];
                                $company_id = $row['company_id'];
                                //   $membership_id = $row['membership_id'];
                                //   $membership_option_id = $row['membership_option_id'];
                                $payment_type = $row['payment_type'];
                                $membership_category_title = $row['membership_category_title'];
                                $membership_title = $row['membership_title'];
                                $membership_structure = $row['membership_structure'];
                                $participant_name = $row['participant_name'];
                                //   $buyer_email = $row['buyer_email'];
//                                $waiver_policies = $row['waiver_policies'];
                                $signup_fee = $row['signup_fee'];
                                $signup_fee_disc = $row['signup_fee_disc'];
                                $mem_fee = $row['membership_fee'];
                                $mem_fee_disc = $row['membership_fee_disc'];
                                $initial_payment = $row['initial_payment'];
                                $inital_processing_fee = $row['initial_processing_fee'];
                                $membership_start_date = $row['membership_start_date'];
                                $payment_start_date = $row['payment_start_date'];
                                $recurring_start_date = $row['recurring_start_date'];
                                $next_payment_date = $row['next_payment_date'];
                                $payment_frequency = $row['payment_frequency'];
                                $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                                $recurring_amount = $row['recurring_amount'];
                                $processing_fee_type = $row['processing_fee_type'];
                                $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
                                $first_payment = $row['first_payment'];
                                $billing_options = $row['billing_options'];
                                $no_of_classes = $row['no_of_classes'];
                                $billing_options_expiration_date = $row['billing_options_expiration_date'];
                                $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                                $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                                $credit_card_name = $row['credit_card_name'];
                                $student_cc_email_list = $row['student_cc_email'];
                                $temp = [];
                                if (!empty($row['membership_payment_id'])) {
                                    if ($recurring_amount == $row['payment_amount']) {
                                        $recurring_processing_fee = $row['processing_fee'];
                                    }
                                    if ($row['processing_fee_type'] == 2) {
                                        $temp['amount'] = $row['payment_amount'] + $row['processing_fee'];
                                    } else {
                                        $temp['amount'] = $row['payment_amount'];
                                    }
                                    $temp['date'] = $row['payment_date'];
                                    $temp['created_dt'] = $row['created_dt'];
                                    $temp['processing_fee'] = $row['processing_fee'];
                                    $temp['payment_status'] = $row['payment_status'];
                                    $temp['paytime_type'] = $row['paytime_type'];
                                    $temp['cc_name'] = $row['cc_name'];
                                    $paid_amt += $temp['amount'];
                                    $payment_details[] = $temp;
                                }
                            }
                            if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE' ) && $billing_options == 'PP') {
                                if ($billing_options_no_of_payments > 1) {
                                    $already_recurring_records_count = 0;
                                    $next_date = '';
                                    for ($i = 0; $i < count($payment_details); $i++) {
                                        if ($payment_details[$i]['paytime_type'] == 'R') {
                                            $already_recurring_records_count++;
                                            $next_date = $payment_details[$i]['date'];
                                        }
                                    }
                                    if ($billing_options_no_of_payments > $already_recurring_records_count) {
                                        $count = $billing_options_no_of_payments - $already_recurring_records_count;
                                        $future_date = $next_date;
                                        for ($j = 0; $j < $count; $j++) {
                                            if ($payment_frequency == 'W') {
                                                $next_payment_date2 = date('Y-m-d', strtotime('+1 weeks', strtotime($future_date)));
                                            } elseif ($payment_frequency == 'B') {
                                                $next_payment_date2 = date('Y-m-d', strtotime('+14 days', strtotime($future_date)));
                                            } elseif ($payment_frequency == 'M') {
                                                $next_payment_date2=$this->getSameDayNextMonth(new DateTime($future_date));
                                            }

                                            $temp_1 = [];
                                            if ($processing_fee_type == 2) {
                                                $temp_1['amount'] = $recurring_amount + $recurring_processing_fee;
                                            } else {
                                                $temp_1['amount'] = $recurring_amount;
                                            }
                                            $temp_1['date'] = $next_payment_date2;
                                            $temp_1['processing_fee'] = $recurring_processing_fee;
                                            $temp_1['payment_status'] = 'N';
                                            $temp_1['paytime_type'] = 'R';
                                            $temp_1['cc_name'] = $credit_card_name;
                                            $payment_details[] = $temp_1;
                                            $future_date = $next_payment_date2;
                                        }
                                    }
                                }
                            }
                        } else {
                            $error_log = array('status' => "Failed", "msg" => "No records found.");
                            cj_log_info($this->json($error_log));
                            return;
                        }
                    }

                    $subject = $membership_category_title . " Membership Completion Confirmation";

                    $message .= "<b> Please see below for your membership completion details.</b><br><br>";
                    $message .= "<b>Membership Details</b><br><br>";
                    $message .= "Membership Start Date: " . date("M dS, Y", strtotime($membership_start_date)) . "<br>";
                    $message .= "Membership: $membership_category_title, $membership_title<br>Participant: " . $participant_name . "<br>";
                    if ($membership_structure == 'NC' && $billing_options == 'PP' && $billing_options_expiration_date != '0000-00-00') {
                        $message .= "$no_of_classes Class Package Expires: " . date("D, M d, Y", strtotime($billing_options_expiration_date)) . "<br>";
                    }
                    $message .= "<br>";

                    $prorated = "";
                    if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
                        $prorated = "(Pro-rated)";
                    }

                    if ($paid_amt > 0) {
//            if($initial_payment>0){
                        $message .= "<br><b>Registration Details</b><br><br>";
                        $message .= "Sign-up Fee: $wp_currency_symbol" . (number_format(($signup_fee - $signup_fee_disc), 2)) . "<br>";
                        if ($processing_fee_type == 2) {
                            if ($initial_payment > 0) {
                                $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment + $inital_processing_fee), 2)) . " (includes $wp_currency_symbol" . $inital_processing_fee . " administrative fees) $prorated<br>";
                            } else {
                                $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . "<br>";
                            }
                        } else {
                            if ($initial_payment > 0) {
                                $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . " $prorated<br>";
                            } else {
                                $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . "<br>";
                            }
                        }
//            }
                    }

                    if (count($payment_details) > 0) {
                        if ($payment_frequency != 'N' && $payment_type == 'R') {
                            $freq_str = "";
                            if ($membership_structure == 'OE' || $membership_structure == 1) {
                                if ($payment_frequency == 'B') {
                                    $freq_str = "Semi-Monthly Recurring: ";
                                } elseif ($payment_frequency == 'M') {
                                    $freq_str = "Monthly Recurring: ";
                                } elseif ($payment_frequency == 'W') {
                                    $freq_str = "Weekly Recurring: ";
                                } elseif ($payment_frequency == 'A') {
                                    $freq_str = "Annual Recurring: ";
                                } elseif ($payment_frequency == 'C') {
                                    if ($custom_recurring_frequency_period_type == 'CW') {
                                        $freq_str = "Weekly Recurring: ";
                                    } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                        $freq_str = "Monthly Recurring: ";
                                    }
                                }
                            } elseif ($billing_options == 'PP') {
                                if ($payment_frequency == 'W') {
                                    $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
                                } elseif ($payment_frequency == 'B') {
                                    $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
                                } elseif ($payment_frequency == 'M') {
                                    $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
                                }
                            }
                        }

                        $message .= "<br>";
                    }

//                    $waiver_present = 0;
//                    $file = '';
//                    if (!empty(trim($waiver_policies))) {
//                        $waiver_present = 1;
//                        $file_name_initialize = 10;
//                        for ($z = 0; $z < $file_name_initialize; $z++) {
//                            $dt = time();
//                            $file = __DIR__."/../uploads/".$dt."_waiver_policies.html";
//                            if (file_exists($file)) {
//                                sleep(1);
//                                $file_name_initialize++;
//                            } else {
//                                break;
//                            }
//                        }
//                        $ifp = fopen($file, "w");
//                        fwrite($ifp, $waiver_policies);
//                        fclose($ifp);
//                    }


                    //***PAYMENT HISTORY DETAILS***//     


                    $membership_registration_id = $membership_reg_id;
                    $membership_option_title = $membership_title = $payment_amount = $processing_fees = $payment_date = $payment_status = $refunded_amount = $cc_name = $refunded_date = $paytime_type = $payment_type = $payment_steps = '';
                    $prorate_first_payment_flg = $payment_frequency = $next_payment_date = $no_of_payments = $billing_options_no_of_payments = $wp_currency_symbol = $processing_fee_type = $membership_structure = $membership_status = $last_updt_dt = $cmp_name = $reply_to = '';
                    $studio_name = $buyer_email = $studio_mail = $message1 = $category = $type = $history_id = $payment_id = $reg_id = $activity_type = $activity_text = $checkout_status = $membership_id = $membership_option_id = '';
                    $payment_history_details = [];
                    $initial_check = $temp_no_payments =$num = 0;

                    $sql_reg = sprintf("SELECT *, 'membership' category FROM (
                    SELECT 'payment' as type, '' history_id,c.`company_id`, mp.`membership_payment_id` payment_id, mp.`membership_registration_id` reg_id, mp.`payment_amount`,mp.`processing_fee`,mp.`payment_date`,mp.`payment_status`,mp.`refunded_amount`,mp.`refunded_date`,mp.`paytime_type`,mp.`cc_name`,DATE(mp.`last_updt_dt`) as last_updt_dt,
                    mr.`buyer_email`,mr.`membership_category_title`, mr.`membership_title`,mr.`membership_status`,mr.`membership_structure`,mr.`processing_fee_type`,mr.`prorate_first_payment_flg`, '' activity_type, '' activity_text, mp.checkout_status, mr.`membership_id`, mr.`membership_option_id`,
                    mr.`payment_frequency`,mr.`no_of_payments`,mr.`billing_options_no_of_payments`,
                    IF(((mr.`membership_structure`='NC' || mr.`membership_structure`='C' || mr.`membership_structure`='SE') && mr.`billing_options`='PP'), IF(mp.`paytime_type`='I', 'Down Payment', 'Payment'), '') payment_steps,c.`wp_currency_symbol`,c.`company_name`, c.`email_id`
                    
                    FROM `membership_payment` mp LEFT JOIN `membership_registration` mr ON mp.`membership_registration_id` = mr.`membership_registration_id` AND mp.`company_id` = mr.`company_id` LEFT JOIN `company` c ON c.`company_id`=mr.`company_id`
                    WHERE mr.company_id='%s' AND mr.membership_registration_id= '%s' 
                    UNION
                    SELECT 'history' as type, `membership_payment_history_id` history_id,mph.`company_id`, `membership_payment_id` payment_id, `membership_registration_id` reg_id, '' payment_amount, '' processing_fee, DATE(`activity_date_time`) payment_date, '' payment_status, '' refunded_amount, '' refunded_date, '' paytime_type, '' cc_name, '' last_updt_dt, 
                    '' buyer_email,'' membership_category_title,'' membership_title,(SELECT `membership_status` FROM `membership_registration` WHERE `membership_registration_id`=mph.`membership_registration_id` AND `company_id`=mph.`company_id`) membership_status, '' membership_structure, '' processing_fee_type, '' prorate_first_payment_flg, `activity_type`, `activity_text`,
                    '' checkout_status, '' membership_id, '' membership_option_id,''payment_frequency,''no_of_payments,''billing_options_no_of_payments,''wp_currency_symbol,''company_name,''email_id,''payment_steps
                    FROM `membership_payment_history` mph WHERE `company_id`='%s' AND `membership_registration_id`='%s')t
                    ORDER BY t.`payment_id`, `payment_date`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_registration_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_registration_id));
                    $result_reg_details = mysqli_query($this->db, $sql_reg);

                    if (!$result_reg_details) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_reg");
                        cj_log_info($this->json($error_log));
                        return;
                    } else {
                        $num_rows = mysqli_num_rows($result_reg_details);
                        if ($num_rows > 0) {
                            while ($row1 = mysqli_fetch_assoc($result_reg_details)) {
                                $payment_history_details[] = $row1;
                                $category = $row1['category'];
                                $type = $row1['type'];
                                $history_id = $row1['history_id'];
                                $payment_id = $row1['payment_id'];
                                $reg_id = $row1['reg_id'];
                                $company_id = $row1['company_id'];
                                $activity_type = $row1['activity_type'];
                                $activity_text = $row1['activity_text'];
                                $payment_date = $row1['payment_date'];
                                $processing_fees = $row1['processing_fee'];
                                $payment_amount = $row1['payment_amount'];
                                $payment_status = $row1['payment_status'];
                                $refunded_amount = $row1['refunded_amount'];
                                $refunded_date = $row1['refunded_date'];
                                $checkout_status = $row1['checkout_status'];
                                $membership_id = $row1['membership_id'];
                                $membership_option_id = $row1['membership_option_id'];
                                $membership_title = $row1['membership_category_title'];
                                $paytime_type = $row1['paytime_type'];
                                $cc_name = $row1['cc_name'];
                                $last_updt_dt = $row1['last_updt_dt'];
                                $membership_structure = $row1['membership_structure'];
                                $processing_fee_type = $row1['processing_fee_type'];
                                $membership_status = $row1['membership_status'];
                                $membership_option_title = $row1['membership_title'];
                                $wp_currency_symbol = $row1['wp_currency_symbol'];
                                $prorate_first_payment_flg = $row1['prorate_first_payment_flg'];
                                $studio_name = $row1['company_name'];
//                                   $buyer_email = $row1['buyer_email'];
                                $studio_mail = $row1['email_id'];
                                $payment_steps = $row1['payment_steps'];
                                $payment_frequency = $row1['payment_frequency'];
                                $no_of_payments = $row1['no_of_payments'];
                                $billing_options_no_of_payments = $row1['billing_options_no_of_payments'];
                                if ($initial_check == 0) {
                                    $message1 .= "<b>Membership Payment History for  $membership_option_title</b><br><br>";
                                }
                                if ($type == 'history') {
                                    $message1 .= date("M dS, Y", strtotime($payment_date));
                                    $message1 .= " $activity_text ";
                                    $message1 .= "<br>";
                                } else if ($membership_structure == 'OE') {
                                    $message1 .= date("M dS, Y", strtotime($last_updt_dt));
                                    if ($payment_status == 'S') {
                                        $message1 .= " Payment: ";
                                    } else if ($payment_status == 'F') {
                                        $message1 .= " Past Due: ";
                                    } else if ($payment_status == 'R') {
                                        $message1 .= " Refunded: ";
                                    } else if ($payment_status == 'M') {
                                        $message1 .= " Mannual Credit Applied: ";
                                    } else if ($payment_status == 'MR') {
                                        $message1 .= " Manually Refunded: ";
                                    } else {
                                        $message1 .= "Scheduled Payment: ";
                                    }
                                    $message1 .= "$wp_currency_symbol";
                                    if ($payment_status == 'R') {
                                        if ($processing_fee_type == '2') {
                                            $message1 .= $refunded_amount + $processing_fees;
                                        } else {
                                            $message1 .= "$refunded_amount ";
                                        }
                                    } else {
                                        if ($processing_fee_type == '2') {
                                            $message1 .= $payment_amount + $processing_fees;
                                        } else {
                                            $message1 .= "$payment_amount ";
                                        }
                                    }
                                    if ($processing_fee_type == '2') {
                                        $message1 .= " (includes $wp_currency_symbol$processing_fees administrative fees) ";
                                    }
                                    if (isset($cc_name)) {
                                        $message1 .= "($cc_name)";
                                    }
                                    if ($prorate_first_payment_flg == 'Y') {
                                        $message1 .= "(Pro-rated)";
                                    }
                                    $message1 .= " Bill due :" . date("M dS, Y", strtotime($payment_date));
                                    $message1 .= "<br>";
                                }
                                if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') && ($payment_status != 'N')) {

                                    if ($payment_steps == 'Down Payment') {
                                        $message1 .= "$payment_steps : ";
                                    } else if ($payment_steps == 'Payment') {
                                        $num++ ;
                                    $message1 .= "$payment_steps $num : ";
                                    }
                                    $message1 .= "$wp_currency_symbol";

                                    if ($payment_status == 'R') {
                                        if ($processing_fee_type == '2') {
                                            $message1 .= $refunded_amount + $processing_fees;
                                        } else {
                                            $message1 .= "$refunded_amount ";
                                        }
                                    } else {
                                        if ($processing_fee_type == '2') {
                                            $message1 .= $payment_amount + $processing_fees;
                                        } else {
                                            $message1 .= "$payment_amount ";
                                        }
                                    }
                                    if ($processing_fee_type == '2') {
                                        $message1 .= " (includes $wp_currency_symbol$processing_fees administrative fees) ";
                                    }
                                    if (isset($cc_name)) {
                                        $message1 .= "($cc_name) ";
                                    }
                                    if ($payment_status == 'S') {
                                        $message1 .= "paid on ";
                                    } else if ($payment_status == 'F') {
                                        $message1 .= "past due on ";
                                    } else if ($payment_status == 'R') {
                                        $message1 .= "refunded on ";
                                    } else if ($membership_status == 'P') {
                                        $message1 .= "on hold ";
                                    } else if ($payment_status == 'M') {
                                        $message1 .= " Mannual Credit Applied: ";
                                    } else if ($payment_status == 'MR') {
                                        $message1 .= " Manually Refunded: ";
                                    }
                                    if ($membership_status != 'P') {
                                        $message1 .= date("M dS, Y", strtotime($payment_date));
                                    }
                                    $message1 .= "<br>";
                                }

                                $initial_check++;
                            }

                            $message = $message . "<br>" . $message1;


                            //*** PAYMENT HISTORY DETAILS END*** //

                            $send_to_studio = 1;
                            $sendEmail_status = $this->sendEmailForMembership('',$studio_mail, $subject, $message, '', '', '', 0, $cc_email_list, $student_cc_email_list, $send_to_studio,$company_id);
                            if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
                                $curr_date_time = date("Y-m-d ");
                                $sql_insert_date = sprintf("UPDATE membership_registration SET end_email_date ='%s' WHERE company_id='%s'  AND membership_registration_id='%s' ",
                                        mysqli_real_escape_string($this->db, $curr_date_time), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
                                $sql_insert_date_result = mysqli_query($this->db, $sql_insert_date);

                                if (!$sql_insert_date_result) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_insert_date");
                                    cj_log_info($this->json($error_log));
                                } else {
                                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $studio_mail . "   " . $sendEmail_status['mail_status']);
                                    cj_log_info($this->json($error_log));
                                }
                                $log = array('status' => "Success", "msg" => "Membership  Completion receipt sent successfully.");
                                cj_log_info($this->json($log));
                            } else {
                                $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $studio_mail);
                                cj_log_info($this->json($error_log));
                            }
                        }
                    }
                    $date_str = "%Y-%m";
                    $every_month_dt = gmdate("Y-m-d");

                    $mem_string = "`active_members`=`active_members`-1,`reg_completed`=`reg_completed`+1";
                    if (!empty(trim($mem_string))) {
                        $query1 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`=0",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id_1), $every_month_dt, $date_str);
                        $result1 = mysqli_query($this->db, $query1);
                        if (!$result1) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                            cj_log_info($this->json($error_log));
                        }
                    }
                    if (!empty(trim($mem_string))) {
                        $query2 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`='%s'",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id_1), $every_month_dt, $date_str, mysqli_real_escape_string($this->db, $membership_option_id_1));
                        $result2 = mysqli_query($this->db, $query2);
                        if (!$result2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                            cj_log_info($this->json($error_log));                            
                        }
                    }
                    if ($membership_status_old == 'P') {
                        $mem_string_1 = "`members_onhold`=`members_onhold`-1";
                        $mem_opt_string = "`options_members_onhold`=`options_members_onhold`-1";
                    } else {
                        $mem_string_1 = "`members_active`=`members_active`-1";
                        $mem_opt_string = "`options_members_active`=`options_members_active`-1";
                    }
                if(!empty(trim($mem_string_1))){
                    $query22 = sprintf("UPDATE `membership` SET $mem_string_1 WHERE `company_id`='%s' AND `membership_id`='%s'",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id_1));
                    $result22 = mysqli_query($this->db, $query22);
                    if (!$result22) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query22");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
                    
                if(!empty(trim($mem_opt_string))){
                    $query33 = sprintf("UPDATE `membership_options` SET $mem_opt_string WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", 
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id_1),
                            mysqli_real_escape_string($this->db, $membership_option_id));
                    $result33 = mysqli_query($this->db, $query33);
                    if (!$result33) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query33");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
                    $query3 = sprintf("UPDATE `membership_registration` SET `membership_status`='S',`mem_reg_completion_date`=CURDATE() WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_registration_id`='%s' ",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id_1), mysqli_real_escape_string($this->db, $membership_registration_id_1));
                    $result3 = mysqli_query($this->db, $query3);                    
                    if (!$result3) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                        cj_log_info($this->json($error_log));                        
                    }
                    
                }
            }
        }
        if($triggerC==1){
            $this->sendEmailForAdmins("Custom Structure invalid Expiration date");
        }
        if($triggerNC==1){
            $this->sendEmailForAdmins("Class Packages Structure invalid no of classes");
        }
        cj_log_info("sendReceiptForMembershipCompletion() - Completed");
        if($total_records_processed>0){
            $end_time = gmdate("Y-m-d H:i:s");
            $cron_file_name = "MembershipCancellationHandler";
            $cron_function_name = "sendReceiptForMembershipCompletion";
            $cron_message = "Total membership completion receipt processed";
            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
            if(!$cron_log_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
                cj_log_info($this->json($error_log));
            }
        }
    }
    
    public function addMembershipDimensions($company_id, $membership_id, $mem_option_id, $category_title, $option_title, $period) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($mem_option_id)) {
            $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                cj_log_info($this->json($error_log));
                $reg_id = 1;
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $selectsql = sprintf("SELECT `active_members` from membership_dimensions where company_id='%s' and `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s' ",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $pre_dt));
                    $resultselectsql = mysqli_query($this->db, $selectsql);
                    if (!$resultselectsql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        cj_log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
                        $num_of_rows2 = mysqli_num_rows($resultselectsql);
                        if ($num_of_rows2 == 0) {
                            $sqlselect1=sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and `membership_option_id`='%s' and  `company_id`= '%s'",  mysqli_real_escape_string($this->db,$membership_id),mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db,$company_id));
                            $resultsqlselect1=mysqli_query($this->db, $sqlselect1);
                            $value2 = mysqli_fetch_object($resultsqlselect1);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id),
                                    mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), mysqli_real_escape_string($this->db,$value2->active));
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                cj_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        } else {
                            $value = mysqli_fetch_object($resultselectsql);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id),
                                    mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), $value->active_members);
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                cj_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        }
                    }
                }
            }
        }

        $sql2 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            cj_log_info($this->json($error_log));
            $reg_id = 1;
        } else{
            $num_of_rows3 = mysqli_num_rows($result2);
            if ($num_of_rows3 == 0) {
                $selectsql2 = sprintf("SELECT `active_members` FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $pre_dt));
                $resultselectsql2 = mysqli_query($this->db, $selectsql2);
                if (!$resultselectsql2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql2");
                    cj_log_info($this->json($error_log));
                    $reg_id = 1;
                } else {
                    $num_of_rows4 = mysqli_num_rows($resultselectsql2);
                    if ($num_of_rows4 == 0) {
                        $sqlselect2 = sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and  `company_id`= '%s'", mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$company_id));
                        $resultsqlselect2 = mysqli_query($this->db, $sqlselect2);
                        $value3 = mysqli_fetch_object($resultsqlselect2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $value3->active));
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            cj_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    } else {
                        $value1 = mysqli_fetch_object($resultselectsql2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), $value1->active_members);
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            cj_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for addMembershipDimensions(), check cron log";
            $this->sendEmailForAdmins($msg);
        }
    }
                    
    private function updateMembershipDimensionsMembers($company_id, $membership_id, $membership_status, $membership_option_id, $old_mem_status, $membership_reg_id,$membership_title,$membership_option_title) {
        $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id, $membership_title,$membership_option_title, '');
        $flag = 0;
        $check = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        $date_str = "%Y-%m";
        $every_month_dt = date("Y-m-d");

        $selectquery = sprintf("SELECT * FROM `membership_registration` WHERE DATE_FORMAT(`hold_date`,'%s')=DATE_FORMAT(`resume_date`,'%s') and  `company_id`='%s' and `membership_id`='%s' and `membership_option_id`='%s' and `membership_registration_id`='%s'",
                $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id), mysqli_real_escape_string($this->db, $membership_reg_id));
        $res = mysqli_query($this->db, $selectquery);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
            cj_log_info($this->json($error_log));
            return 0;
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows == 1) {
                $flag = 1;
            }
        }
        $selectquery1 = sprintf("SELECT * FROM `membership_registration` WHERE DATE_FORMAT(`hold_date`,'%s')=DATE_FORMAT(`cancelled_date`,'%s') and  `company_id`='%s' and `membership_id`='%s' and `membership_option_id`='%s' and `membership_registration_id`='%s'",
                $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id), mysqli_real_escape_string($this->db, $membership_reg_id));
        $res1 = mysqli_query($this->db, $selectquery1);
        if (!$res1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery1");
            cj_log_info($this->json($error_log));
            return 0;
        } else {
            $num_of_rows = mysqli_num_rows($res1);
            if ($num_of_rows == 1) {
                $check = 1;
            }
        }
        $mem_string = $cat_string = $opt_string = "";
        if ($membership_status == 'R') {
            if ($flag == 1) {
                $mem_string = "`active_members`=`active_members`+1, `reg_hold`=`reg_hold`-1";
            } elseif ($flag == 0) {
                 $mem_string = "`active_members`=`active_members`+1,`reg_resumed_from_hold`=IFNULL(`reg_resumed_from_hold`,0)+1";
            }           
             $cat_string = "`members_active`=`members_active`+1, `members_onhold`=`members_onhold`-1";
             $opt_string = "`options_members_active`=`options_members_active`+1, `options_members_onhold`=`options_members_onhold`-1";
        } elseif ($membership_status == 'P') {
            $mem_string = "`active_members`=`active_members`-1, `reg_hold`=`reg_hold`+1";
            $cat_string = "`members_active`=`members_active`-1, `members_onhold`=`members_onhold`+1";
            $opt_string = "`options_members_active`=`options_members_active`-1, `options_members_onhold`=`options_members_onhold`+1";
        } elseif ($membership_status == 'C') {
            if ($old_mem_status == 'R') {
                $mem_string = "`active_members`=`active_members`-1, `reg_cancelled`=`reg_cancelled`+1";
                $cat_string = "`members_active`=`members_active`-1, `members_cancelled`=`members_cancelled`+1";
                $opt_string = "`options_members_active`=`options_members_active`-1, `options_members_cancelled`=`options_members_cancelled`+1";
            } else {
                if ($check == 1) {
                    $mem_string = "`reg_hold`=`reg_hold`-1, `reg_cancelled`=`reg_cancelled`+1";
                } else if ($check == 0) {
                    $mem_string = "`reg_cancelled`=`reg_cancelled`+1";
                }
                $cat_string = "`members_onhold`=`members_onhold`-1, `members_cancelled`=`members_cancelled`+1";
                    $opt_string = "`options_members_onhold`=`options_members_onhold`-1, `options_members_cancelled`=`options_members_cancelled`+1";
            }
        } elseif ($membership_status == 'A') {
            $mem_string = "`active_members`=`active_members`+1,`reg_new`=`reg_new`+1";
        }

        if (!empty(trim($mem_string))) {
            $query1 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`=0",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), $every_month_dt, $date_str);
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                cj_log_info($this->json($error_log));
                return 0;
            }
            
            $query2 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), $every_month_dt, $date_str, mysqli_real_escape_string($this->db, $membership_option_id));
            $result2 = mysqli_query($this->db, $query2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                cj_log_info($this->json($error_log));
                return 0;
            }
        }
            
        if(!empty(trim($cat_string))){
            $query3 = sprintf("UPDATE `membership` SET $cat_string WHERE `company_id`='%s' AND `membership_id`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
            $result3 = mysqli_query($this->db, $query3);
            if (!$result3) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                cj_log_info($this->json($error_log));
                return 0;
            }
        }

        if(!empty(trim($opt_string))){
            $query4 = sprintf("UPDATE `membership_options` SET $opt_string WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
            $result4 = mysqli_query($this->db, $query4);
            if (!$result4) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query4");
                cj_log_info($this->json($error_log));
                return 0;
            }
        }
        date_default_timezone_set($curr_time_zone);
        return 1;
    }
    
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    private function sendEmailForAdmins($msg){
        if($this->env=='P'){
            $env = "Production";
        }else{
            $env = "Development";
        }
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "$env - Membership Handler Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        cj_log_info($this->json($error_log));
        return $response;
    }
    
     public function sendReceiptForMembershipCancellation($company_id, $membership_reg_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $host = $_SERVER['HTTP_HOST'];
        if (strpos($this->server, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $mp_created_date = " DATE(CONVERT_TZ(mp.created_dt,$tzadd_add,'$new_timezone'))";
        $activity_dt_tm = " IF(TIME(`activity_date_time`)!='00:00:00', DATE(CONVERT_TZ(`activity_date_time`,$tzadd_add,'$new_timezone')), DATE(`activity_date_time`)) payment_date";
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";

        $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
        $payment_details = [];
        $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
        $recurring_start_date = $membership_start_date = $payment_start_date = $payment_frequency = $custom_recurring_frequency_period_type = $msg1 = $msg2 = $msg3 = '';
        $membership_structure = $billing_options = $billing_options_no_of_payments = '';

        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, mp.`cc_name`, `waiver_policies`, mr.credit_card_name, 
            mr.`membership_start_date`, mr.`payment_start_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
            `buyer_postal_code`, mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, DATE($mp_created_date) created_dt, mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`
            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id`
            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N')
            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
            return 1;
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
//                    $company_id = $row['company_id'];
//                    $membership_id = $row['membership_id'];
//                    $membership_option_id = $row['membership_option_id'];
                    $payment_type = $row['payment_type'];
                    $membership_category_title = $row['membership_category_title'];
                    $membership_title = $row['membership_title'];
                    $membership_structure = $row['membership_structure'];
                    $participant_name = $row['participant_name'];
//                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $signup_fee = $row['signup_fee'];
                    $signup_fee_disc = $row['signup_fee_disc'];
                    $mem_fee = $row['membership_fee'];
                    $mem_fee_disc = $row['membership_fee_disc'];
                    $initial_payment = $row['initial_payment'];
                    $inital_processing_fee = $row['initial_processing_fee'];
                    $membership_start_date = $row['membership_start_date'];
                    $payment_start_date = $row['payment_start_date'];
                    $recurring_start_date = $row['recurring_start_date'];
                    $next_payment_date = $row['next_payment_date'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $recurring_amount = $row['recurring_amount'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
                    $first_payment = $row['first_payment'];
                    $billing_options = $row['billing_options'];
                    $no_of_classes = $row['no_of_classes'];
                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $credit_card_name = $row['credit_card_name'];
                    $temp = [];
                    if (!empty($row['membership_payment_id'])) {
                        if ($recurring_amount == $row['payment_amount']) {
                            $recurring_processing_fee = $row['processing_fee'];
                        }
                        if ($row['processing_fee_type'] == 2) {
                            if ($membership_structure != 'SE') {
                            $temp['amount'] = $row['payment_amount'] + $row['processing_fee'];
                            } else if ($membership_structure == 'SE') {
                                $temp['amount'] = $row['payment_amount'];
                            }
                        } else {
                            $temp['amount'] = $row['payment_amount'];
                        }
                        $temp['date'] = $row['payment_date'];
                        $temp['created_dt'] = $row['created_dt'];
                        $temp['processing_fee'] = $row['processing_fee'];
                        $temp['payment_status'] = $row['payment_status'];
                        $temp['paytime_type'] = $row['paytime_type'];
                        $temp['cc_name'] = $row['cc_name'];
                        $paid_amt += $temp['amount'];
                        $payment_details[] = $temp;
                    }
                }
//
            } else {
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                cj_log_info($this->json($error_log));
            }
        }

        $subject = $membership_category_title . " Membership Cancellation Confirmation";

        $message .= "<b> Please see below for your membership cancellation details.</b><br><br>";
        $message .= "<b>Membership Details</b><br><br>";
        $message .= "Membership Start Date: " . date("M dS, Y", strtotime($membership_start_date)) . "<br>";
        $message .= "Membership: $membership_category_title, $membership_title<br>Participant: " . $participant_name . "<br>";
        if ($membership_structure == 'NC' && $billing_options == 'PP' && $billing_options_expiration_date != '0000-00-00') {
            $message .= "$no_of_classes Class Package Expires: " . date("D, M d, Y", strtotime($billing_options_expiration_date)) . "<br>";
        }
        $message .= "<br>";

        $prorated = "";
        if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
            $prorated = "(Pro-rated)";
        }

        if ($paid_amt > 0) {
            $message .= "<br><b>Registration Details</b><br><br>";
            $message .= "Sign-up Fee: $wp_currency_symbol" . (number_format(($signup_fee - $signup_fee_disc), 2)) . "<br>";
            if ($processing_fee_type == 2) {
                if ($initial_payment > 0) {
                    $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment + $inital_processing_fee), 2)) . " (includes $wp_currency_symbol" . $inital_processing_fee . " administrative fees) $prorated<br>";
                } else {
                    $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . "<br>";
                }
            } else {
                if ($initial_payment > 0) {
                    $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . " $prorated<br>";
                } else {
                    $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . "<br>";
                }
            }
        }

        if (count($payment_details) > 0) {
            if ($payment_frequency != 'N' && $payment_type == 'R') {
                $freq_str = "";
                if ($membership_structure == 'OE' || $membership_structure == 1) {
                    if ($payment_frequency == 'B') {
                        $freq_str = "Semi-Monthly Recurring: ";
                    } elseif ($payment_frequency == 'M') {
                        $freq_str = "Monthly Recurring: ";
                    } elseif ($payment_frequency == 'W') {
                        $freq_str = "Weekly Recurring: ";
                    } elseif ($payment_frequency == 'A') {
                        $freq_str = "Annual Recurring: ";
                    } elseif ($payment_frequency == 'C') {
                        if ($custom_recurring_frequency_period_type == 'CW') {
                            $freq_str = "Weekly Recurring: ";
                        } elseif ($custom_recurring_frequency_period_type == 'CM') {
                            $freq_str = "Monthly Recurring: ";
                        }
                    }
                } elseif ($billing_options == 'PP') {
                    if ($payment_frequency == 'W') {
                        $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
                    } elseif ($payment_frequency == 'B') {
                        $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
                    } elseif ($payment_frequency == 'M') {
                        $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
                    }
                }

            }

            $message .= "<br>";
        }

        $waiver_present = 0;
        $file = '';
        if (!empty(trim($waiver_policies))) {
            $waiver_present = 1;
            $file_name_initialize = 10;
            for ($z = 0; $z < $file_name_initialize; $z++) {
                $dt = time();
                $file = __DIR__."/../uploads/" . $dt . "_waiver_policies.html";
                if (file_exists($file)) {
                    sleep(1);
                    $file_name_initialize++;
                } else {
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }



        //***PAYMENT HISTORY DETAILS***//     



        $membership_registration_id = $membership_reg_id;
        $membership_option_title = $membership_title = $payment_amount = $processing_fees = $payment_date = $payment_status = $refunded_amount = $cc_name = $refunded_date = $paytime_type = $payment_type = $payment_steps = '';
        $prorate_first_payment_flg = $payment_frequency = $next_payment_date = $no_of_payments = $billing_options_no_of_payments = $wp_currency_symbol = $processing_fee_type = $membership_structure = $membership_status = $last_updt_dt = $cmp_name = $reply_to = '';
        $studio_name = $buyer_email = $studio_mail = $message1 = $category = $type = $history_id = $payment_id = $reg_id = $activity_type = $activity_text = $checkout_status = $membership_id = $membership_option_id = '';
        $payment_history_details = [];
        $initial_check = $temp_no_payments = 0;

        $sql_reg = sprintf("SELECT *, 'membership' category FROM (
                    SELECT 'payment' as type, '' history_id, mp.`membership_payment_id` payment_id, mp.`membership_registration_id` reg_id, mp.`payment_amount`,mp.`processing_fee`,$payment_date payment_date,mp.`payment_status`,mp.`refunded_amount`,mp.`refunded_date`,mp.`paytime_type`,mp.`cc_name`,DATE(mp.`last_updt_dt`) as last_updt_dt,
                    mr.`buyer_email`,mr.`membership_category_title`, mr.`membership_title`,mr.`membership_status`,mr.`membership_structure`,mr.`processing_fee_type`,mr.`prorate_first_payment_flg`, '' activity_type, '' activity_text, mp.checkout_status, mr.`membership_id`, mr.`membership_option_id`,
                    mr.`payment_frequency`,mr.`no_of_payments`,mr.`billing_options_no_of_payments`,
                    IF(((mr.`membership_structure`='NC' || mr.`membership_structure`='C' || mr.`membership_structure`='SE') && mr.`billing_options`='PP'), IF(mp.`paytime_type`='I', 'Down Payment', 'Payment'), '') payment_steps,c.`wp_currency_symbol`,c.`company_name`, c.`email_id`,s.`student_cc_email`
                    FROM `membership_payment` mp LEFT JOIN `membership_registration` mr ON mp.`membership_registration_id` = mr.`membership_registration_id` AND mp.`company_id` = mr.`company_id` LEFT JOIN `company` c ON c.`company_id`= mr.`company_id` LEFT JOIN `student` s ON s.`company_id`= mr.`company_id` AND s.`student_id`= mr.`student_id`
                    WHERE mr.company_id='%s' AND mr.membership_registration_id= '%s' AND mr.`membership_status`='C' AND mp.`payment_status` NOT IN ('N','P')
                    UNION
                    SELECT 'history' as type, `membership_payment_history_id` history_id, `membership_payment_id` payment_id, `membership_registration_id` reg_id, '' payment_amount, '' processing_fee, $activity_dt_tm, '' payment_status, '' refunded_amount, '' refunded_date, '' paytime_type, '' cc_name, '' last_updt_dt, 
                    '' buyer_email,'' membership_category_title,'' membership_title,(SELECT `membership_status` FROM `membership_registration` WHERE `membership_registration_id`=mph.`membership_registration_id` AND `company_id`=mph.`company_id` AND membership_status='C') membership_status, '' membership_structure, '' processing_fee_type, '' prorate_first_payment_flg, `activity_type`, `activity_text`,
                    '' checkout_status, '' membership_id, '' membership_option_id,''payment_frequency,''no_of_payments,''billing_options_no_of_payments,''wp_currency_symbol,''company_name,''email_id,'' student_cc_email,''payment_steps
                    FROM `membership_payment_history` mph WHERE `company_id`='%s' AND `membership_registration_id`='%s')t
                    ORDER BY t.`payment_id`, `payment_date`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_registration_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_registration_id));
        $result_reg_details = mysqli_query($this->db, $sql_reg);
        if (!$result_reg_details) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_reg");
            cj_log_info($this->json($error_log));
            return 1;
         } else {
            $num_rows = mysqli_num_rows($result_reg_details);
            if ($num_rows > 0) {
                while ($row1 = mysqli_fetch_assoc($result_reg_details)) {
                    $payment_history_details[] = $row1;
                    $category = $row1['category'];
                    $type = $row1['type'];
                    $history_id = $row1['history_id'];
                    $payment_id = $row1['payment_id'];
                    $reg_id = $row1['reg_id'];
                    $activity_type = $row1['activity_type'];
                    $activity_text = $row1['activity_text'];
                    $payment_date = $row1['payment_date'];
                    $processing_fees = $row1['processing_fee'];
                    $payment_amount = $row1['payment_amount'];
                    $payment_status = $row1['payment_status'];
                    $refunded_amount = $row1['refunded_amount'];
                    $refunded_date = $row1['refunded_date'];
                    $checkout_status = $row1['checkout_status'];
                    $membership_id = $row1['membership_id'];
                    $membership_option_id = $row1['membership_option_id'];
                    $membership_title = $row1['membership_category_title'];
                    $paytime_type = $row1['paytime_type'];
                    $cc_name = $row1['cc_name'];
                    $last_updt_dt = $row1['last_updt_dt'];
                    $membership_structure = $row1['membership_structure'];
                    $processing_fee_type = $row1['processing_fee_type'];
                    $membership_status = $row1['membership_status'];
                    $membership_option_title = $row1['membership_title'];
                    $wp_currency_symbol = $row1['wp_currency_symbol'];
                    $prorate_first_payment_flg = $row1['prorate_first_payment_flg'];
                    $studio_name = $row1['company_name'];
                    $buyer_email = $row1['buyer_email'];
                    $studio_mail = $row1['email_id'];
                    $payment_steps = $row1['payment_steps'];
                    $payment_frequency = $row1['payment_frequency'];
                    $no_of_payments = $row1['no_of_payments'];
                    $billing_options_no_of_payments = $row1['billing_options_no_of_payments'];
                    $student_cc_email_list = $row1['student_cc_email'];
                    if ($initial_check == 0) {
                        $message1 .= "<b>Membership Payment History for  $membership_option_title</b><br><br>";
                    }
                    if ($type == 'history') {
                        $message1 .= date("M dS, Y", strtotime($payment_date));
                        $message1 .= " $activity_text ";
                        $message1 .= "<br>";
                    } else if ($membership_structure == 'OE') {
                        $message1 .= date("M dS, Y", strtotime($last_updt_dt));
                        if ($payment_status == 'S') {
                            $message1 .= " Payment: ";
                        } else if ($payment_status == 'F') {
                            $message1 .= " Past Due: ";
                        } else if ($payment_status == 'R') {
                            $message1 .= " Refunded: ";
                        } else if ($payment_status == 'M') {
                            $message1 .= " Mannual Credit Applied: ";
                        } else if ($payment_status == 'MR') {
                            $message1 .= " Manually Refunded: ";
                        } else {
                            $message1 .= "Scheduled Payment: ";
                        }
                        $message1 .= "$wp_currency_symbol";
                        if ($payment_status == 'R') {
                            if($processing_fee_type == '2'){
                            $message1.=$refunded_amount+$processing_fees;
                            }else{
                            $message1 .= "$refunded_amount ";
                            }
                        } else {
                            if($processing_fee_type == '2'){
                            $message1.=$payment_amount+$processing_fees;
                            }else{
                            $message1 .= "$payment_amount ";
                            }
                        }
                        if ($processing_fee_type == '2') {
                            $message1 .= " (includes $wp_currency_symbol$processing_fees administrative fees) ";
                        }
                        if (isset($cc_name)) {
                            $message1 .= "($cc_name)";
                        }
                        if ($prorate_first_payment_flg == 'Y') {
                            $message1 .= "(Pro-rated)";
                        }
                        $message1 .= " Bill due :" . date("M dS, Y", strtotime($payment_date));
                        $message1 .= "<br>";
                    }
                    if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure=='SE') && ($payment_status != 'N')) {

                        if ($payment_steps == 'Down Payment') {
                            $message1 .= "$payment_steps : ";
                        } else if ($payment_steps == 'Payment') {
                            $message1 .= "$payment_steps : ";
                        }
                        $message1 .= "$wp_currency_symbol";
                        
                        if ($payment_status == 'R') {
                            if($processing_fee_type == '2'){
                                if ($membership_structure != 'SE') {
                            $message1.=$refunded_amount+$processing_fees;
                                } else if ($membership_structure == 'SE') {
                                    $message1 .= "$refunded_amount ";
                                }
                            }else{
                            $message1 .= "$refunded_amount ";
                            }
                        } else {
                            if($processing_fee_type == '2'){
                                if ($membership_structure != 'SE') {
                            $message1.=$payment_amount+$processing_fees;
                                } else if ($membership_structure == 'SE') {
                                    $message1 .= "$payment_amount ";
                                }
                            }else{
                            $message1 .= "$payment_amount ";
                            }
                        }
                        if ($processing_fee_type == '2') {
                            if ($membership_structure != 'SE') {
                            $message1 .= " (includes $wp_currency_symbol$processing_fees administrative fees) ";
                            } else if ($membership_structure == 'SE') {
                                $message1 .= " (plus $wp_currency_symbol$processing_fees administrative fees) ";
                        }
                        }
                        if (isset($cc_name)) {
                            $message1 .= "($cc_name) ";
                        }
                        if ($payment_status == 'S') {
                            $message1 .= "paid on ";
                        } else if ($payment_status == 'F') {
                            $message1 .= "past due on ";
                        } else if ($payment_status == 'R') {
                            $message1 .= "refunded on ";
                        } else if ($payment_status == 'P') {
                            $message1 .= "on hold ";
                        } else if ($payment_status == 'M') {
                            $message1 .= " Mannual Credit Applied: ";
                        } else if ($payment_status == 'MR') {
                            $message1 .= " Manually Refunded: ";
                        }
                        if ($membership_status != 'P') {
                            $message1 .= date("M dS, Y", strtotime($payment_date));
                        }
                        $message1 .= "<br>";
                    }
                    if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure=='SE') && ($payment_status == 'N')) {

                        $date_initial_check = 0;
                        $temp_no_payments = $billing_options_no_of_payments - $no_of_payments;
                        for ($k = 0; $k <= $temp_no_payments; $k++) {
                            if ($date_initial_check <= 1) {
                                $temp_date = $payment_date;
                            } else {
                                $temp_date = $next_payment_date;
                            }
                            if ($payment_frequency == 'W') {
                                $next_payment_date = date('Y-m-d', strtotime('+1 weeks', strtotime($temp_date)));
                            } elseif ($payment_frequency == 'B') {
                                $next_payment_date = date('Y-m-d', strtotime('+14 days', strtotime($temp_date)));
                            } elseif ($payment_frequency == 'M') {
                                $next_payment_date = date('Y-m-d', strtotime('next month', strtotime($temp_date)));
                            }

                            if ($payment_steps == 'Payment') {

                                $num = $date_initial_check + 1;
                                $message1 .= "$payment_steps $num : ";
                            }
                            $message1 .= "$wp_currency_symbol";

                            if($processing_fee_type == '2'){
                                if ($membership_structure != 'SE') {
                                $message1.=$payment_amount+$processing_fees;
                                } else if ($membership_structure == 'SE') {
                                    $message1 .= "$payment_amount ";
                                }
                                }else{
                                $message1 .= "$payment_amount ";
                                }
                            if ($processing_fee_type == '2') {
                                if ($membership_structure != 'SE') {
                                $message1 .= " (includes $wp_currency_symbol $processing_fees administrative fees) ";
                                } else if ($membership_structure == 'SE') {
                                    $message1 .= " (plus $wp_currency_symbol $processing_fees administrative fees) ";
                            }
                            }
                            $message1 .= "scheduled for ";
                            if ($date_initial_check == 0) {
                                $message1 .= date("M dS, Y", strtotime($payment_date));
                            } else {
                                $message1 .= date("M dS, Y", strtotime($next_payment_date));
                            }
                            $message1 .= "<br>";
                            $date_initial_check++;
                        }
                    }
                    $initial_check++;
                }

                $message = $message . "<br>" . $message1;


            //*** PAYMENT HISTORY DETAILS END*** //

                $send_to_studio = 1;
                $sendEmail_status = $this->sendEmailForMembership('',$studio_mail, $subject, $message, '', '', '', 0, $cc_email_list, $student_cc_email_list, $send_to_studio, $company_id);
                if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
                    
                    $curr_date_time = date("Y-m-d ");
                    $sql_insert_date = sprintf("UPDATE membership_registration SET end_email_date ='%s' WHERE company_id='%s'  AND membership_registration_id='%s' ", mysqli_real_escape_string($this->db, $curr_date_time), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
                    $sql_insert_date_result = mysqli_query($this->db, $sql_insert_date);

                    if (!$sql_insert_date_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_insert_date");
                    cj_log_info($this->json($error_log));
                    return 1;
                    } else {

                        $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $studio_mail . "   " . $sendEmail_status['mail_status']);
                        cj_log_info($this->json($error_log));
                    }
                  } else {
                    $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $studio_mail);
                    cj_log_info($this->json($error_log));
                    return 1;
                }
                date_default_timezone_set($curr_time_zone);
            }
        }
        return 0;
    }
    
    private function sendEmailForMembership($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list1, $student_cc_email_list1, $send_to_studio, $company_id) {
        $to_available = 0;
        $bounce_check=$this->sendBounceEmailCheck($to,$cc_email_list1,$student_cc_email_list1,$company_id);
        $cc_email_list=$bounce_check['cc_email_array'];
        $student_cc_email_list=$bounce_check['student_CC_array'];
        $bounced_flag=$bounce_check['bounced_flag'];
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
//            if(filter_var($from, FILTER_VALIDATE_EMAIL)){
//                $mail->setFrom($from, $cmp_name);
//                $mail->AddCC($from);
//            }else{
//                $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            if ($bounced_flag == 'Y') {
                if (!empty($student_cc_email_list)) {
                    $student_cc_addresses = $student_cc_email_list;
//                $student_cc_addresses = explode(',', $student_cc_email_list);
                    for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                        if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                            $mail->AddAddress($student_cc_addresses[$init_1]);
                            $to_available=1;
                        }
                    }
                }
                if($to_available==0) {
                    return false;
                }
            } else {
                $mail->AddAddress($to);
                if (!empty($student_cc_email_list)) {
                    $student_cc_addresses = $student_cc_email_list;
//                $student_cc_addresses = explode(',', $student_cc_email_list);
                    for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                        if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                            $mail->AddCC($student_cc_addresses[$init_1]);
                        }
                    }
                }
            }

            if($send_to_studio==1){     //send_to_studio - enable/disable sending the email to studio in certain cases
                if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                    $mail->AddCC($from);
                }
                if(!empty($cc_email_list)){
                    $cc_addresses=$cc_email_list;
//                    $cc_addresses = explode(',', $cc_email_list);
                    for($init=0;$init<count($cc_addresses);$init++){
                        if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                            $mail->AddCC($cc_addresses[$init]);
                        }
                    }
                }
            }
            
            
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
//        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
//        cj_log_info($this->json($error_log));
        return $response;
    }
    
    public function getSameDayNextMonth(DateTime $startDate, $numberOfMonthsToAdd = 1) {
        $startDateDay = (int) $startDate->format('j');
        $startDateMonth = (int) $startDate->format('n');
        $startDateYear = (int) $startDate->format('Y');

        $numberOfYearsToAdd = floor(($startDateMonth + $numberOfMonthsToAdd) / 12);
        if ((($startDateMonth + $numberOfMonthsToAdd) % 12) === 0) {
          $numberOfYearsToAdd--;
        }
        $year = $startDateYear + $numberOfYearsToAdd;

        $month = ($startDateMonth + $numberOfMonthsToAdd) % 12;
        if ($month === 0) {
          $month = 12;
        }
        $month = sprintf('%02s', $month);

        $numberOfDaysInMonth = (new DateTime("$year-$month-01"))->format('t');
        $day = $startDateDay;
        if ($startDateDay > $numberOfDaysInMonth) {
          $day = $numberOfDaysInMonth;
        }
        $day = sprintf('%02s', $day);

        $output=new DateTime("$year-$month-$day");
        return $output->format('Y-m-d');
    }
    
    
    private function sendBounceEmailCheck($to1,$cc_email_list1,$student_cc_email_list1,$company_id){
        $cc_email_list1 = preg_replace('/\s*,\s*/', ',', $cc_email_list1);
        $student_cc_email_list1 = preg_replace('/\s*,\s*/', ',', $student_cc_email_list1);
        $student_CC_array = explode(",",trim($student_cc_email_list1));
        $student_CC_list = implode("','", $student_CC_array);
        $cc_email_array = explode(",",$cc_email_list1);
        $cc_email_list= implode("','", $cc_email_array);
        $out=[];
        $bounced_flag='N';
        $sql= sprintf("SELECT 'T' as flag, count(*) as b_flag, '' bounced_mail FROM `bounce_email_verify` b WHERE b.`bounced_mail`='%s' AND (b.`company_id`=0 OR b.`company_id`='%s')
                    UNION
                    SELECT 'S' as flag, 0 as b_flag, `bounced_mail` FROM `bounce_email_verify` b WHERE b.`bounced_mail` in ('%s') AND (b.`company_id`=0 OR b.`company_id`='%s')
                    UNION
                    SELECT 'C' as flag, 0 as b_flag, `bounced_mail` FROM `bounce_email_verify` b WHERE b.`bounced_mail` in ('%s') AND (b.`company_id`=0 OR b.`company_id`='%s')",
                    mysqli_real_escape_string($this->db, $to1),$company_id, $student_CC_list,$company_id,  $cc_email_list,$company_id);
        $result= mysqli_query($this->db, $sql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
             cj_log_info($this->json($error_log));
        }else{
            if(mysqli_num_rows($result)>0){
                while($rows = mysqli_fetch_assoc($result)){
                  if($rows['flag']=='S'){
                       if(in_array($rows['bounced_mail'], $student_CC_array)){                           
                           array_splice($student_CC_array, array_search($rows['bounced_mail'], $student_CC_array), 1);
                       }
                    
                  }else if($rows['flag']=='C'){
                    
                      if(in_array($rows['bounced_mail'], $cc_email_array)){
                           array_splice($cc_email_array, array_search($rows['bounced_mail'], $cc_email_array), 1);
                       }
                  }else if($rows['flag']=='T'){
                      if($rows['b_flag']>0){
                          $bounced_flag ='Y';
                      }
                  }
                }
            }
        }
         $out['cc_email_array'] = $cc_email_array;
         $out['student_CC_array'] = $student_CC_array;
         $out['bounced_flag'] = $bounced_flag;
         return $out;
    }
    
    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }
    
}

$api = new MembershipCancellationHandler;
$api->processApi();
?>