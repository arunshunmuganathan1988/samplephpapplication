<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}
require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';

class CronJob {
    private $db = NULL;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $error_check = 0;
    public function __construct() {
        require_once __DIR__.'/../Globals/config.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        if(!empty($GLOBALS['HOST_NAME'])){
            $host = $GLOBALS['HOST_NAME'];
        }else{
            $host = $_SERVER['HTTP_HOST'];
        }
        if(strpos($host, 'www.mystudio.academy') !== false){
            $this->env = 'P';
        }else{
            $this->env = 'D';
        }
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
    }

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function response($data, $status) {
        cj_log_info($data);
        exit;
    }

    public function processApi() {
        $this->updateStudentIdInRegistrationTables();
        $this->updateParticipantIdInRegistrationTables();
        $this->setExpDateForCustomMembership();
        if($this->error_check==1){
            $this->sendEmailForAdmins();
        }
    }
    
    private function updateStudentIdInRegistrationTables(){
    
        $query1 = sprintf("SELECT er.`event_reg_id`, er.`company_id`, er.`buyer_first_name`, er.`buyer_last_name`, er.`buyer_email`, er.`buyer_phone`, s.`student_id` FROM `event_registration` er LEFT JOIN `student` s ON er.`buyer_email`=s.`student_email` AND er.`company_id`=s.`company_id` WHERE er.`student_id`=0");
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $query1);
            $this->response($this->json($error), 500);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                while($row1 = mysqli_fetch_assoc($result1)){
                    $company_id = $row1['company_id'];
                    $event_reg_id = $row1['event_reg_id'];
                    $buyer_first_name = $row1['buyer_first_name'];
                    $buyer_last_name = $row1['buyer_last_name'];
                    $buyer_email = $row1['buyer_email'];
                    $buyer_phone = $row1['buyer_phone'];
                    $student_id = $row1['student_id'];
                    if(is_null($student_id)){
                        $select_student = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'",mysqli_real_escape_string($this->db,$buyer_email),mysqli_real_escape_string($this->db,$company_id));
                        $result_select_student = mysqli_query($this->db, $select_student);
                        if(!$result_select_student){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student);
                            $this->response($this->json($error), 500);
                        }else{
                            if(mysqli_num_rows($result_select_student)>0){
                                $r = mysqli_fetch_assoc($result_select_student);
                                $student_id = $r['student_id'];
                            }else{
                                $insert_student = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_lastname`, `student_email`) VALUES('%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$buyer_first_name),
                                        mysqli_real_escape_string($this->db,$buyer_last_name), mysqli_real_escape_string($this->db,$buyer_email));
                                $result_insert_student = mysqli_query($this->db, $insert_student);
                                if(!$result_insert_student){
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student);
                                    $this->response($this->json($error), 500);
                                }else{
                                    $student_id = mysqli_insert_id($this->db);
                                }
                            }
                        }
                    }
                    $update_reg_table = sprintf("UPDATE `event_registration` SET `student_id`='%s' WHERE `event_reg_id`='%s'",$student_id,$event_reg_id);
                    $result_update_reg_table = mysqli_query($this->db, $update_reg_table);
                    if(!$result_update_reg_table){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_reg_table);
                        $this->response($this->json($error), 500);
                    } else {
                        $update_regdetails_table = sprintf("UPDATE `event_reg_details` SET `student_id`='%s' WHERE `event_reg_id`='%s'",$student_id,$event_reg_id);
                        $result_update_regdetails_table = mysqli_query($this->db, $update_regdetails_table);
                        if(!$result_update_regdetails_table){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_regdetails_table);
                            $this->response($this->json($error), 500);                           
                        
                        }
                    }
                }
            }
        }
        
        $query2 = sprintf("SELECT mr.`membership_registration_id`, mr.`company_id`, mr.`buyer_first_name`, mr.`buyer_last_name`, mr.`buyer_email`, mr.`buyer_phone`, s.`student_id` FROM `membership_registration` mr LEFT JOIN `student` s ON mr.`buyer_email`=s.`student_email` AND mr.`company_id`=s.`company_id` WHERE mr.`student_id`=0");
        $result2 = mysqli_query($this->db, $query2);
        if(!$result2){
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $query2);
            $this->response($this->json($error), 500);
        }else{
            $num_rows2 = mysqli_num_rows($result2);
            if($num_rows2>0){
                while($row2 = mysqli_fetch_assoc($result2)){
                    $company_id = $row2['company_id'];
                    $mem_reg_id = $row2['membership_registration_id'];
                    $buyer_first_name = $row1['buyer_first_name'];
                    $buyer_last_name = $row1['buyer_last_name'];
                    $buyer_email = $row2['buyer_email'];
                    $buyer_phone = $row2['buyer_phone'];
                    $student_id = $row2['student_id'];
                    if(is_null($student_id)){
                        $select_student2 = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$company_id));
                        $result_select_student2 = mysqli_query($this->db, $select_student2);
                        if(!$result_select_student2){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student2);
                            $this->response($this->json($error), 500);
                        }else{
                            if(mysqli_num_rows($result_select_student2)>0){
                                $r2 = mysqli_fetch_assoc($result_select_student2);
                                $student_id = $r2['student_id'];
                            }else{
                                $insert_student2 = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_lastname`, `student_email`) VALUES('%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$buyer_first_name),
                                        mysqli_real_escape_string($this->db,$buyer_last_name), mysqli_real_escape_string($this->db,$buyer_email));
                                $result_insert_student2 = mysqli_query($this->db, $insert_student2);
                                if(!$result_insert_student2){
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student2);
                                    $this->response($this->json($error), 500);
                                }else{
                                    $student_id = mysqli_insert_id($this->db);
                                }
                            }
                        }
                    }
                    $update_reg_table2 = sprintf("UPDATE `membership_registration` SET `student_id`='%s' WHERE `membership_registration_id`='%s'",$student_id,$mem_reg_id);
                    $result_update_reg_table2 = mysqli_query($this->db, $update_reg_table2);
                    if(!$result_update_reg_table2){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_reg_table2);
                        $this->response($this->json($error), 500);
                    }
                }
            }
        }
    }

    private function updateParticipantIdInRegistrationTables(){ 
               
        $query2 = sprintf("SELECT mr.`membership_registration_id`, mr.`company_id`, mr.`student_id`, mr.`membership_registration_column_1`, mr.`membership_registration_column_2`, mr.`membership_registration_column_3`, p.`participant_id` 
                FROM `membership_registration` mr LEFT JOIN `participant` p ON mr.`company_id`=p.`company_id` AND mr.`student_id`=p.`student_id` AND 
                mr.`membership_registration_column_1`=p.`participant_first_name` AND mr.`membership_registration_column_2`=p.`participant_last_name` AND mr.`membership_registration_column_3`=p.`date_of_birth`
                WHERE mr.`participant_id`=0");
        $result2 = mysqli_query($this->db, $query2);
        if(!$result2){
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $query2);
            $this->response($this->json($error), 500);
        }else{
            $num_rows2 = mysqli_num_rows($result2);
            if($num_rows2>0){
                while($row2 = mysqli_fetch_assoc($result2)){
                    $company_id = $row2['company_id'];
                    $mem_reg_id = $row2['membership_registration_id'];
                    $student_id = $row2['student_id'];
                    $first_name = $row2['membership_registration_column_1'];
                    $last_name = $row2['membership_registration_column_2'];
                    $dob = $row2['membership_registration_column_3'];
                    $participant_id = $row2['participant_id'];
                    if(is_null($participant_id)){
                        $select_participant2 = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s' AND `date_of_birth`='%s'",
                        mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$student_id), mysqli_real_escape_string($this->db,$first_name), mysqli_real_escape_string($this->db,$last_name), mysqli_real_escape_string($this->db,$dob));
                        $result_select_participant2 = mysqli_query($this->db, $select_participant2);
                        if(!$result_select_participant2){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_participant2);
                            $this->response($this->json($error), 500);
                        }else{
                            if(mysqli_num_rows($result_select_participant2)>0){
                                $r = mysqli_fetch_assoc($result_select_participant2);
                                $participant_id = $r['participant_id'];
                            }else{
                                $insert_participant2 = sprintf("INSERT INTO `participant`(`company_id`, `student_id`, `participant_first_name`, `participant_last_name`, `date_of_birth`) VALUES('%s', '%s', '%s', '%s', '%s')",
                                        $company_id, $student_id, mysqli_real_escape_string($this->db,$first_name), mysqli_real_escape_string($this->db,$last_name), $dob);
                                $result_insert_participant2 = mysqli_query($this->db, $insert_participant2);
                                if(!$result_insert_participant2){
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_participant2);
                                    cj_log_info("insert failure  ".$insert_participant2);
                                    $this->error_check=1;
                                    continue;
                                }else{
                                    $participant_id = mysqli_insert_id($this->db);
                                }
                            }
                        }
                    }
                    $update_reg_table2 = sprintf("UPDATE `membership_registration` SET `participant_id`='%s' WHERE `membership_registration_id`='%s'",$participant_id,$mem_reg_id);
                    $result_update_reg_table2 = mysqli_query($this->db, $update_reg_table2);
                    if(!$result_update_reg_table2){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_reg_table2);
                        $this->response($this->json($error), 500);
                    }
                }
            }
        }
        
        
        $query1 = sprintf("SELECT er.`event_reg_id`, er.`company_id`, er.`student_id`, er.`event_registration_column_1`, er.`event_registration_column_2`, p.`participant_id` 
                FROM `event_registration` er LEFT JOIN `participant` p ON er.`company_id`=p.`company_id` AND er.`student_id`=p.`student_id` AND 
                er.`event_registration_column_1`=p.`participant_first_name` AND er.`event_registration_column_2`=p.`participant_last_name`
                WHERE er.`participant_id`=0");
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $query1);
            $this->response($this->json($error), 500);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                while($row1 = mysqli_fetch_assoc($result1)){
                    $company_id = $row1['company_id'];
                    $event_reg_id = $row1['event_reg_id'];
                    $student_id = $row1['student_id'];
                    $first_name = $row1['event_registration_column_1'];
                    $last_name = $row1['event_registration_column_2'];
                    $participant_id = $row1['participant_id'];
                    if(is_null($participant_id)){
                        $select_participant = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s'",
                                $company_id, $student_id, mysqli_real_escape_string($this->db,$first_name), mysqli_real_escape_string($this->db,$last_name));
                        $result_select_participant = mysqli_query($this->db, $select_participant);
                        if(!$result_select_participant){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_participant);
                            $this->response($this->json($error), 500);
                        }else{
                            if(mysqli_num_rows($result_select_participant)>0){
                                $r = mysqli_fetch_assoc($result_select_participant);
                                $participant_id = $r['participant_id'];
                            }else{
                                $insert_participant = sprintf("INSERT INTO `participant`(`company_id`, `student_id`, `participant_first_name`, `participant_last_name`) VALUES('%s', '%s', '%s', '%s')",
                                        $company_id, $student_id, mysqli_real_escape_string($this->db,$first_name), mysqli_real_escape_string($this->db,$last_name));
                                $result_insert_participant = mysqli_query($this->db, $insert_participant);
                                if(!$result_insert_participant){
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_participant);
                                    cj_log_info("insert failure  ".$insert_participant);
                                    $this->error_check=1;
                                    continue;
                                }else{
                                    $participant_id = mysqli_insert_id($this->db);
                                }
                            }
                        }
                    }
                    $update_reg_table = sprintf("UPDATE `event_registration` SET `participant_id`='%s' WHERE `event_reg_id`='%s'",$participant_id,$event_reg_id);
                    $result_update_reg_table = mysqli_query($this->db, $update_reg_table);
                    if(!$result_update_reg_table){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_reg_table);
                        $this->response($this->json($error), 500);
                    }
                }
            }
        }
    }
    
    private function setExpDateForCustomMembership() {
        
        $query = sprintf("SELECT o.`company_id`,o.`membership_structure`,o.`membership_id` ,o.`membership_option_id`,o.`expiration_status` , 
         o.`expiration_date_type` , o.`expiration_date_val`,r.`membership_registration_id`,r.`payment_start_date`
        FROM `membership_registration` r
        JOIN `membership_options` o ON r.`membership_option_id`= o.`membership_option_id` AND  o.`membership_structure` = 'C'
        WHERE  r.`membership_status`  in ('R','P')
        AND r.`membership_structure` = 'C' AND r.`billing_options_expiration_date`='0000-00-00'
        ORDER BY o.`membership_structure`, o.`membership_option_id`,`membership_registration_id` ASC");


        $result_query = mysqli_query($this->db, $query);
        if (!$result_query) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");            
            $this->response("failiure ".$query, 500);
        } else {
            $num_rows = mysqli_num_rows($result_query);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_query)) {
                    $mebership_option_id = $row['membership_option_id'];
                    $mebership_registration_id = $row['membership_registration_id'];
                    $company_id = $row['company_id'];
                    $membership_structure = $row['membership_structure'];
                    $membership_id = $row['membership_id'];
                    $expiration_status = $row['expiration_status'];
                    $expiration_date_type = $row['expiration_date_type'];
                    $expiration_date_val = $row['expiration_date_val'];
                    $payment_start_date = $row['payment_start_date'];


                    if (!empty($expiration_status) && !empty($expiration_date_type) && $expiration_date_val != 0 && $expiration_status == 'Y') {
                        if ($expiration_date_type == 'W') {
                            $new_billing_expiration_date = date('Y-m-d', strtotime("+$expiration_date_val weeks", strtotime($payment_start_date)));
                        } elseif ($expiration_date_type == 'M') {
                            $new_billing_expiration_date = date('Y-m-d', strtotime("+$expiration_date_val months", strtotime($payment_start_date)));
                        } elseif ($expiration_date_type == 'Y') {
                            $new_billing_expiration_date = date('Y-m-d', strtotime("+$expiration_date_val years", strtotime($payment_start_date)));
                        }

                        $update = sprintf("UPDATE `membership_registration` SET `billing_options_expiration_date`= '$new_billing_expiration_date',`end_email_date`= NULL,`last_updt_dt`=`last_updt_dt` WHERE `membership_registration_id`='%s'", $mebership_registration_id);
                        $result_update = mysqli_query($this->db, $update);
                        if (!$result_update) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update");                            
                            $this->response($this->json($error_log), 500);
                        } else {
                            $log = array("status" => "Success", "msg" => "Membership registration id $mebership_registration_id updated.");
                            $this->response($this->json($log), 500);
                        }
                    }
                }
            }
        }
    }
    
    private function sendEmailForAdmins(){
        if($this->env=='P'){
            $env = "Production";
        }else{
            $env = "Development";
        }
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "$env - Error in updating Student/Participant Ids";
            $mail->Body = "check cron log for additional details";
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        cj_log_info($this->json($error_log));
        return $response;
    }

}

$api = new CronJob;
$api->processApi();

