<?php
$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_old'){
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}
require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';

class CronJob {

    public function __construct() {
        require_once __DIR__.'/../Globals/config.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
    }

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    public function processApi() {
        $this->runCronJob();
        $this->deleteMsgCronJob();
    }

    //Cron Job for message delivery
    protected function runCronJob() {
        $total_records_processed = 0;
        $output_1=[];
        $start_time = gmdate("Y-m-d H:i:s");
//        $sql1 = sprintf("SELECT C.company_id,C.timezone FROM company C WHERE exists
//                        (SELECT M.company_id from message M where C.company_id = M.company_id and M.push_delivered_date IS NULL )group by company_id");
//        $result1 = mysqli_query($this->db, $sql1);
//        if (!$result1) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//            cj_log_info($this->json($error_log));
//        } else {
//            $num_of_rows_1 = mysqli_num_rows($result1);
//            if ($num_of_rows_1 > 0) {
//                while ($row_1 = mysqli_fetch_assoc($result1)) {
//                    $output_1[] = $row_1;
//                }
//                $server_timezone = date_default_timezone_get();
//                for ($idx = 0; $idx < count($output_1); $idx++) {
//                    $obj = (Array) $output_1[$idx];
//                    $user_company_id = $obj["company_id"];
//                    $user_timezone = $obj["timezone"];
//                    date_default_timezone_set($user_timezone);
                    $current_time = gmdate("Y-m-d H:i:s");
                    $minus15time = gmdate("Y-m-d H:i:s", strtotime("-15 minutes"));
                    $sql = sprintf("SELECT `message_id`, `company_id`, `message_text`, `message_link`, `email_status` FROM `message` WHERE push_delivered_date IS NULL and message_delivery_date between '%s' and '%s'", $minus15time, $current_time);
                    $result_a = mysqli_query($this->db, $sql);
                    if (!$result_a) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                        cj_log_info($this->json($error_log));
                    } else {
                        $num_of_rows_2 = mysqli_num_rows($result_a);
                        if ($num_of_rows_2 > 0) {
                            $total_records_processed+=$num_of_rows_2;
                            while ($row_2 = mysqli_fetch_assoc($result_a)) {
                                $message_id = $row_2['message_id'];
                                $message_txt = $row_2['message_text'];
                                $company_id = $row_2['company_id'];
                                $error_log_check=array("company_id"=>$company_id,"message_id"=>$message_id);
                                cj_log_info($this->json($error_log_check));
                                $email_status = $row_2['email_status'];
                                $message_link = $row_2['message_link'];
                                $update_message = sprintf("UPDATE `message` SET `push_delivered_date`='%s' WHERE `message_id`='%s' and `company_id`='%s'", $current_time, $message_id, $company_id);
                                $result = mysqli_query($this->db, $update_message);
//                                    date_default_timezone_set($curr_time_zone);
                                if (!$result) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                                    cj_log_info($this->json($error_log));
                                }
                                $array = $this->preparepush($company_id, $message_txt);
                                if ($email_status === 'Y') {
                                    if (!empty(trim($message_link))) {
                                        $message = $message_txt . "<br><a href='$message_link'>Learn More</a>";
                                    } else {
                                        $message = $message_txt;
                                    }
                                    $sql2 = sprintf("SELECT c.`company_name` , c.`email_id` , c.`upgrade_status` , s.`student_email`,s.`student_cc_email` 
                                FROM  `student` s
                                LEFT JOIN  `company` c ON c.`company_id` = s.`company_id` LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
                                WHERE c.`company_id`='%s'   AND s.`student_email` != IFNULL(b.`bounced_mail`, '') and s.`deleted_flag`!='Y'", $company_id);
                                    $result2 = mysqli_query($this->db, $sql2);

                                    if (!$result2) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                        cj_log_info($this->json($error_log));
                                    } else {
                                        $num_of_rows = mysqli_num_rows($result2);
                                        if ($num_of_rows > 0) {
                                            $output_3 = [];
                                            $initial_check = 0;
                                            $cmp_name = $reply_to = '';
                                            while ($row_3 = mysqli_fetch_assoc($result2)) {
                                                if ($initial_check == 0) {
                                                    $cmp_name = $row_3['company_name'];
                                                    $reply_to = $row_3['email_id'];
                                                    $initial_check++;
                                                }
                                                $output_3[] = $row_3;
                                            }
                                            $footer_detail = $this->getunsubscribefooter($company_id);
                                            for ($idx_2 = 0; $idx_2 < count($output_3); $idx_2++) {
                                                $obj_2 = (Array) $output_3[$idx_2];
                                                $user_emailid = $obj_2["student_email"];
                                                $cc_email_list = $obj_2['student_cc_email'];

                                                $sendEmail_status = $this->sendEmail($user_emailid, "Message", $message, $cmp_name, $reply_to,$cc_email_list,$company_id,'',$footer_detail);
                                                if ($sendEmail_status['status'] == "true") {
                                                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                                                    cj_log_info($this->json($error_log));
                                                } else {
                                                    $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                                                    cj_log_info($this->json($error_log));
                                                }
                                            }
                                            //                        $out = array('status' => "Success", 'msg' => "");
                                            //                        // If success everythig is good send header as "OK" and user details
                                            //                        $this->response($this->json($out), 200);
                                        } else {
                                            $error_log = array('status' => "Failed", "msg" => "Students doesn't exist to send message for this company(ID - $company_id).");
                                            cj_log_info($this->json($error_log));  // If no records "No Content" status
                                        }
                                    }
                                }
                                $response_status = $array['status'];
                                $user_count = $array['msg'];
                                if ($response_status === "Success") {
//                                    date_default_timezone_set($user_timezone);
                                    $current_time = gmdate("Y-m-d H:i:s");

                                    $update_message = sprintf("UPDATE `message` SET `no_of_users`='%s' WHERE `message_id`='%s' and `company_id`='%s'", $user_count, $message_id, $company_id);
                                    $result = mysqli_query($this->db, $update_message);
//                                    date_default_timezone_set($curr_time_zone);
                                    if (!$result) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                                        cj_log_info($this->json($error_log));
                                    }
                                } else {
                                    cj_log_info($this->json($array));
                                }
                            }
                        } else {
//                            $error_log = array('status' => "Failed", "msg" => "Message doesn't exist between $minus15time and $current_time.");
//                            cj_log_info($this->json($error_log)); // If no records "No Content" status
                        }
                    }
//                    date_default_timezone_set($server_timezone);
//                }
//            }
//        }
        if($total_records_processed>0){
            $end_time = gmdate("Y-m-d H:i:s");
            $cron_file_name = "CronJob";
            $cron_function_name = "runCronJob";
            $cron_message = "Total messages pushed";
            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
            if(!$cron_log_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
                cj_log_info($this->json($error_log));
            }
        }
    }

    protected function sendEmail($to, $subject, $message, $cmp_name, $reply_to,$cc_email_list,$company_id,$type,$footer_detail) {
        if (empty(trim($cmp_name))) {
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try {
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if (filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)) {
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            if (!empty($company_id) && $type != 'subscribe') {
                $curr_loc = $GLOBALS['HOST_NAME'];
//                $tomail = $to;
                if (!empty(trim($cc_email_list))) {
                    $tomail = base64_encode($to . "," . "$cc_email_list");
                } else {
                    $tomail = base64_encode($to);
                }
                $activation_link = "$curr_loc/EmailSubscription.php?mail=$tomail?comp_id=$company_id?type=U";
                $message = "
            <html>
            <body>
                <p>$message</p><br><br>
                    <footer style='font-size: 13px;color: #746f6f;'>
                    <center>
                    <p>$footer_detail
                    </p>
                    <a href='$activation_link' target='_blank' style='color: #746f6f; !important;text-decoration: none !important;'><u style='color: #746f6f;'>Unsubscribe $to</u></a>
                    </center>
                    </footer>
            </body>
            </html>
            ";
            }
            $mail->Body = $message;
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $verify_email_for_bounce = $this->verifyBouncedEmailForCC($cc_addresses[$init], $company_id);
                        if($verify_email_for_bounce==1){
                            $mail->AddCC($cc_addresses[$init]);
                        }
                    }
                }
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        } catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
//        cj_log_info($this->json($error_log));
        return $response;
    }

    protected function preparepush($company_id, $message) {
        $output = $android = $ios = [];
        $sql2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
               IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props  FROM `student` s
            LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id)
            WHERE `company_id`='%s' AND `deleted_flag`!='Y' AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
            AND push_device_id IS NOT NULL AND trim(push_device_id)!='' order by app_id)t1 
                LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            cj_log_info($this->json($error_log));
        } else {
            $num_of_rows2 = mysqli_num_rows($result2);
            if ($num_of_rows2 > 0) {
                while ($row = mysqli_fetch_assoc($result2)) {
                    $output[] = $row;
                }
                 $r_rand = mt_rand(1,9999);
                $rfolder_name = __DIR__."/../uploads/Certificate/temp-$r_rand-$company_id".time();  //for creating pem file from db
                if (!file_exists($rfolder_name)) {
                    $oldmask = umask(0);
                    mkdir($rfolder_name, 0777, true);
                    umask($oldmask);
                }else{ // to make directory once.
                     $r = 0;
                    while ($r < 100) {
                        $rfolder_name = __DIR__."/../uploads/Certificate/temp-$r_rand-$company_id".time();
                        if (!file_exists($rfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($rfolder_name, 0777, true);
                            umask($oldmask);
                            break;
                        }
                     $r++;
                    }
                }
                $ios_socket_url ='';
                $ios_app_id_array = [];
                $host = $GLOBALS['HOST_NAME'];
                if (strpos($host, 'mystudio.academy') !== false) {
//                if (strpos($host, 'www.mystudio.academy') !== false || strpos($host, 'stage.mystudio.academy') !== false) {
                    //production
                    $ios_socket_url = "ssl://gateway.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];
                        
                        if ($devicetype == 'I') {                            
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['prod_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            cj_log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }else{
                    //development
                    $ios_socket_url = "ssl://gateway.sandbox.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];                        
                        
                        if ($devicetype == 'I') {
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['dev_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            cj_log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }
                $sent_count = $this->sendpush($message, $ios, $android, $ios_socket_url);
                $check = $this->deleteDirectory("$rfolder_name");// delete directory with content
                $out = array('status' => "Success", 'msg' => $sent_count);
                return $out;
            } else {
                $out = array('status' => "Failure", 'msg' => "No users");
                return $out;
            }
        }
    }

    private function deleteMsgCronJob() {
        $total_records_processed = 0;
        $start_time = gmdate("Y-m-d H:i:s");
//        $sql_del1 = sprintf("SELECT C.company_id, C.timezone
//                FROM company C LEFT JOIN message M ON C.company_id = M.company_id
//                WHERE M.push_delivered_date IS NOT NULL OR M.message_delivery_date IS NOT NULL 
//                GROUP BY M.company_id");
//        $result_del1 = mysqli_query($this->db, $sql_del1);
//        if (!$result_del1) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_del1");
//            cj_log_info($this->json($error_log));
//        } else {
//            $num_of_rows_del = mysqli_num_rows($result_del1);
//            if ($num_of_rows_del > 0) {
//                while ($row_del = mysqli_fetch_assoc($result_del1)) {
//                    $output_del[] = $row_del;
//                }
//
//                for ($idx_4 = 0; $idx_4 < count($output_del); $idx_4++) {
//                    $obj_del = (Array) $output_del[$idx_4];
//                    $user_company_id_del = $obj_del["company_id"];
//                    $user_timezone_del = $obj_del["timezone"];
//                    date_default_timezone_set($user_timezone_del);
                    $minus30days = gmdate("Y-m-d H:i:s", strtotime("-720 hours"));

                    $sql_del2 = sprintf("SELECT * FROM  `message` WHERE (`push_delivered_date` IS NOT NULL AND `push_delivered_date` <  '%s')
                                AND (`message_delivery_date` IS NULL OR `message_delivery_date` <  '%s')", $minus30days, $minus30days);
                    $result_del2 = mysqli_query($this->db, $sql_del2);
                    if (!$result_del2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_del2");
                        cj_log_info($this->json($error_log));
                    } else {
                        $msg_list = "";
                        $message_id=[];
                        $num_of_rows_del1 = mysqli_num_rows($result_del2);
                        if ($num_of_rows_del1 > 0) {
                            while($row_msg = mysqli_fetch_assoc($result_del2)){
                                $message_id[] = $row_msg['message_id'];
                            }
                        }
                        if(!empty($message_id)){
                            $msg_list = implode(",", $message_id);
                            $del_msg_map_query = sprintf("DELETE FROM `message_mapping` WHERE `message_id` IN (%s)", mysqli_real_escape_string($this->db, $msg_list));
                            $del_msg_map_result = mysqli_query($this->db, $del_msg_map_query);
                            if (!$del_msg_map_result) {
                                $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$del_msg_map_query");
                                cj_log_info($this->json($error_log2));
                            }
                        }
                        
                        $sql_del3 = sprintf("DELETE FROM `message` WHERE (`push_delivered_date` IS NOT NULL AND `push_delivered_date` <  '%s')
                                AND (`message_delivery_date` IS NULL OR `message_delivery_date` <  '%s')", $minus30days, $minus30days);
                        $result_del3 = mysqli_query($this->db, $sql_del3);
                        if (!$result_del3) {
                            $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_del3");
                            cj_log_info($this->json($error_log2));
                        } else {
                            $affected_rows = mysqli_affected_rows($this->db);
                            if ($affected_rows > 0) {
                                $total_records_processed += $affected_rows;
                                $log = array('status' => "Success", "msg" => "$affected_rows Messages deleted successfully for the period before $minus30days.");
                                cj_log_info($this->json($log));
                            } else {
//                                    $log = array('status' => "Failed", "msg" => "No messages deleted for the period before $minus30days.", "query" => "$sql_del3");
//                                    cj_log_info($this->json($log));
                            }
                        }
                    }
//                }
//            }
//        }
        if($total_records_processed>0){
            $end_time = gmdate("Y-m-d H:i:s");
            $cron_file_name = "CronJob";
            $cron_function_name = "deleteMsgCronJob";
            $cron_message = "Total messages deleted";
            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
            if(!$cron_log_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
                cj_log_info($this->json($error_log));
            }
        }
    }
    
     public function verifyBouncedEmailForCC($cc_address, $company_id){
        $selecsql=sprintf("SELECT * FROM `bounce_email_verify` b WHERE b.`bounced_mail`='%s' AND (b.`company_id`=0 OR b.`company_id`='%s')", mysqli_real_escape_string($this->db,$cc_address), mysqli_real_escape_string($this->db,$company_id));
        $result= mysqli_query($this->db,$selecsql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            cj_log_info($this->json($error_log));
        }else{
          if(mysqli_num_rows($result)>0){
              return 0;
          }else{
               return 1;
          }
        }
    }
    
    protected function sendpush($data, $ios, $android, $socket_url) {
        $send_count = 0;
        //ios
        if (!empty($ios)) {
            $ctx = stream_context_create();
            for ($r = 0; $r < count($ios['device_token']); $r++) {
//                $token_arr = [];
                $app_id = $ios['app_id'][$r];
                $deviceToken = $ios['device_token'][$r];
                $unread_msg_count = $ios['unread_msg_count'][$r];
//                $new_file_content = file_get_contents($ios['pem'][$r]);
//                if ((!empty($old_fle_content) && $old_fle_content !== $new_file_content) || $r == 0) {
//                    if ($r !== 0) {
//                        fclose($fp);
//                    }
                stream_context_set_option($ctx, 'ssl', 'local_cert', $ios['pem'][$r]);
                stream_context_set_option($ctx, 'ssl', 'passphrase', 'mystudio');
                $fp = stream_socket_client($socket_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                if (!$fp) {
                    $error_log = array('status' => "Failed", "msg" => "Message not delivered(I)[Error - $errstr($err)].", "App id" => $app_id, "Token" => "$deviceToken");
                    cj_log_info($this->json($error_log));
                    continue;
                }
//                }
                // Build the binary notification
                $token_arr = explode(",", $deviceToken);
                $unread_msg_arr = explode(",", $unread_msg_count);
                if (count($token_arr) > 0) {
                    $chunk_token_arr = array_chunk($token_arr, 8);
                    $chunk_bagde_count = array_chunk($unread_msg_arr, 8);
                    $flag = 1;
                    for ($j = 0; $j < count($chunk_token_arr); $j++) {
                        if ($j !== 0) {
                            if ($flag == 1) {
                                fclose($fp);
                            }
                            $deviceToken = implode(",", $chunk_token_arr[$j]);
                            stream_context_set_option($ctx, 'ssl', 'local_cert', $ios['pem'][$r]);
                            stream_context_set_option($ctx, 'ssl', 'passphrase', 'mystudio');
                            $fp = stream_socket_client($socket_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                            if (!$fp) {
                                $error_log = array('status' => "Failed", "msg" => "Message not delivered(I)[Error - $errstr($err)].", "App id" => $app_id, "Token" => "$deviceToken");
                                cj_log_info($this->json($error_log));
                                $flag = 0;
                                continue;
                            } else {
                                $flag = 1;
                            }
                        }
                        for ($c = 0; $c < count($chunk_token_arr[$j]); $c++) {// Create the payload body
                            $body['aps'] = array(
                                'alert' => array(
                                    'title' => 'New Message received',
                                    'body' => $data
                                ),
                                'sound' => 'default',
                                'badge' => (int)$chunk_bagde_count[$j][$c]
                            );
                            // Encode the payload as JSON
                            $payload = json_encode($body);
                            $msg = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $chunk_token_arr[$j][$c])) . pack("n", strlen($payload)) . $payload;
                            $result = fwrite($fp, $msg, strlen($msg));
                            // Send it to the server
                            if (!$result) {
                                $error_log = array('status' => "Failed", "msg" => "Message not delivered(I).", "Token" => $chunk_token_arr[$j][$c], "PL" => $payload);
                                cj_log_info($this->json($error_log));
                            } else {
                                $error_log = array('status' => "Success", "msg" => "Message successfully delivered(I).", "Token" => $chunk_token_arr[$j][$c], "PL" => $payload);
                                cj_log_info($this->json($error_log));
                                $send_count++;
                            }
                        }
                    }
                    fclose($fp);
                }
//                $old_fle_content = file_get_contents($ios['pem'][$r]);
            }
        }

        if (!empty($android)) {
            //Android	
            $url = 'https://fcm.googleapis.com/fcm/send';
//            for ($r = 0; $r < count($android['device_token']); $r++) {
            foreach ($android['device_token'] as $point => $values) {
                $key = $android['key'][$point];
                $deviceToken = array_chunk($values, 900);
                for ($j = 0; $j < count($deviceToken); $j++) {
                    $ch = curl_init();
                    $devicetoken = implode(",", $deviceToken[$j]);
                    $fields = array(
                        'registration_ids' => $deviceToken[$j],
                        'data' => array(
                            "message" => $data,
                            "title" => 'New Message',
                            "badge" => 1
                        )
                    );
                    $fields = json_encode($fields);
                    $headers = array(
                        'Authorization: key=' . $key,
                        'Content-Type: application/json'
                    );
                    if ($url) {
                        // Set the url, number of POST vars, POST data
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        // Disabling SSL Certificate support temporarly
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        if ($fields) {
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                        }

                        // Execute post
                        $result = curl_exec($ch);
                        $resultArr = json_decode($result, true);
                        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        if ($result === FALSE || $resultArr['success'] == 0) {
                            //die('Curl failed: ' . curl_error($ch));
                            $error_log = array('status' => "Failed", "msg" => "Message not delivered(A).", "Token" => "$devicetoken", "StatusCode" => $returnCode, "Result" => json_encode($resultArr));
                            cj_log_info($this->json($error_log));
                        } else {
                            $error_log = array('status' => "Success", "msg" => "Message successfully delivered(A).", "Token" => "$devicetoken", "StatusCode" => $returnCode, "Result" => json_encode($resultArr));
                            cj_log_info($this->json($error_log));
                            $send_count += $resultArr['success'];
                        }
                    }
                    curl_close($ch);
                }
            }
        }
        return $send_count;
    }
    
    public function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
    protected function getunsubscribefooter($company_id) {
        $sql = sprintf("SELECT company_name, CONCAT(street_address, IF(street_address = '' , city, CONCAT(', ',city)),IF(street_address = '' OR city = '', state, CONCAT(', ',state)),IF(street_address = '' OR city = '' OR state = '', postal_code, CONCAT(' ',postal_code))) address 
                    from company where company_id = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_name = $row['company_name'];
                $studio_address = trim($row['address']);
                if(!empty($studio_address)){
                    $studio_address = ($studio_address[strlen($studio_address) - 1] === "," ? substr($studio_address, 0, -1) : $studio_address);
                }
                $footer_detail = $studio_name . ($studio_address === "" ? '' : " | $studio_address");
                return $footer_detail;
            }
        }
    }

}

$api = new CronJob;
$api->processApi();

