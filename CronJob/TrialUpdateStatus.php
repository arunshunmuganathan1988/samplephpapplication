<?php
$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_old'){
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}
require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';


class TrialUpdateStatus{
    
    private $db = NULL;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    private $server_url, $cron_props_file, $cron_sf, $cron_ps, $cron_ev, $cron_ms, $cron_ms_lt, $env;
    
    public function __construct(){
        require_once __DIR__.'/../Globals/config.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        if(!empty($GLOBALS['HOST_NAME'])){
            $host = $GLOBALS['HOST_NAME'];
        }else{
            $host = $_SERVER['HTTP_HOST'];
        }
        if(strpos($host, 'dev.mystudio.academy') !== false){
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'https://www.mystudio.academy';
            $this->cron_props_file = "cron_prod_props.php";
            $this->env = 'P';
        }else{
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        }
        $this->getUserandSecret();
    }
    
    private function getUserandSecret(){
        require_once "$this->cron_props_file";
        $this->cron_sf = SF_CRON;
        $this->cron_ps = PS_CRON;
        $this->cron_ev = EV_CRON;
        $this->cron_ms = MS_CRON;
        $this->cron_ms_lt = MS_LT_CRON;
    }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
        }
    }
    
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function processApi(){
        $this->runCronJobForCancelPastTrial();
    }
    
   
    private function runCronJobForCancelPastTrial(){
        cj_log_info("runCronJobForCancelPastTrial() - Started");
        $flag = 0;
        $reg_id_array = [];
        $curr_date = gmdate("Y-m-d");
        $query = sprintf("SELECT * FROM `trial_registration` WHERE `trial_status`!='C' AND `end_date`<'$curr_date'");
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
            $flag = 1;
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $reg_id = $row['trial_reg_id'];
                    $company_id = $row['company_id'];
                    $trial_id = $row['trial_id'];
                    $trial_status = 'C';
                    $student_id = $row['student_id'];
                    $old_trial_status = $row['trial_status'];
                    if ($old_trial_status == 'E') {
                        $trial_string = "`enrolled_count`=`enrolled_count`-1,`cancelled_count`=`cancelled_count`+1";
                    } else if ($old_trial_status == 'A') {
                        $trial_string = "`active_count`=`active_count`-1,`cancelled_count`=`cancelled_count`+1";
                    } else if ($old_trial_status == 'D') {
                        $trial_string = "`didnotstart_count`=`didnotstart_count`-1,`cancelled_count`=`cancelled_count`+1";
                    }
                    $update_registration = sprintf("UPDATE `trial_registration` SET `trial_status`='C', `active_date`=NULL, `enrolled_date`=NULL, `cancelled_date`='%s', `didnotstart_date`=NULL
                            WHERE `trial_reg_id`='%s' ", mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $reg_id));
                    $result_update_registration = mysqli_query($this->db, $update_registration);
                    if (!$result_update_registration) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_registration");
                        cj_log_info($this->json($error_log));
                        continue;
                    }

                    if (!empty(trim($trial_string))) {
                        $query2 = sprintf("UPDATE `trial` SET $trial_string WHERE `company_id`='%s' AND `trial_id`='%s'", 
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
                        $result2 = mysqli_query($this->db, $query2);
                        if (!$result2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }

                    $activity_text = "Status changed to cancel";
                    $currdate = gmdate("Y-m-d H:i:s");
                    $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `student_id`, `trial_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id),$student_id, mysqli_real_escape_string($this->db, $reg_id), 'status', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $currdate));
                    $result_history = mysqli_query($this->db, $insert_history);
                    if (!$result_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                        cj_log_info($this->json($error_log));
                        continue;
                    }
                    
                    $res1 = $this->addTrialDimensions($company_id, $trial_id, '');
                    $res = $this->updateTrialDimensionsMembers($company_id, $trial_id, $trial_status, $old_trial_status, $reg_id);
                    
                    if ($res === 1 || $res1 === 1) {
                        $reg_id_array[] = $reg_id;
                    }
                }
            } else {
                $error_log = array('status' => "Failed", "msg" => "No trial registration available for cancellation.");
                cj_log_info($this->json($error_log));
            }
        }
        if (!empty($reg_id_array)) {
            $reg_str = implode(", ", $reg_id_array);
//            $msg = "Something went wrong in trial status update for trial_registration_ids=$reg_str, check cron log or table";
//            $this->sendEmailForAdmins($msg);
        }
        cj_log_info("runCronJobForCancelPastTrial() - Completed");
    }
    
   public function addTrialDimensions($company_id, $trial_id, $period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if (!empty($period)) {
            $curr_dt = date("Y-m", strtotime($period));
        } else {
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));


        $sql2 = sprintf("SELECT * FROM `trial_dimensions` WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            cj_log_info($this->json($error_log));
            return 1;
         } else {
            $num_of_rows1 = mysqli_num_rows($result2);
            if ($num_of_rows1 == 0) {
                $insertquery1 = sprintf("INSERT INTO `trial_dimensions`(`company_id`, `period`, `trial_id`) VALUES('%s','%s','%s')", 
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $trial_id));
                $res2 = mysqli_query($this->db, $insertquery1);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    cj_log_info($this->json($error_log));
                      return 1;
                 }
            }
        }
        date_default_timezone_set($curr_time_zone);
          return 0;
    }
    
      public function updateTrialDimensionsMembers($company_id, $trial_id, $trial_status,$old_trial_status,$trial_reg_id) {
        $flag = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $date_string = $trial_string = "";
//        $a_date = $e_date = $c_date = $d_date = '';
        $date_str = "%Y-%m";$reg_date='';
        $every_month_dt = date("Y-m-d");
        
//        if(!empty($old_date['reg'])){
//            $old_date['active'] = $old_date['reg'];
//        }
//        if(!empty($old_date)){
//        $a_date = "'".$old_date['active']."'";
//        $e_date = "'".$old_date['enrolled']."'";
//        $c_date = "'".$old_date['cancelled']."'";
//        $d_date = "'".$old_date['didnotstart']."'";
//       }
       if ($trial_status == 'A') {
            if ($old_trial_status == 'E') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
                 $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'C') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'D') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } elseif ($trial_status == 'E') {
            if ($old_trial_status == 'A') {
//                $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                 $trial_string = "`active_count`=`active_count`-1";
            } else if ($old_trial_status == 'C') {
//                 $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                  $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'D') {
//                $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                 $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } elseif ($trial_status == 'C') {
             if ($old_trial_status == 'E') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
               $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'A') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                 $trial_string = "`active_count`=`active_count`-1";
            } else if ($old_trial_status == 'D') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } else if ($trial_status == 'D') {
            if ($old_trial_status == 'E') {
//                 $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
                 $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'C') {
//                  $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                   $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'A') {
//                 $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                   $trial_string = "`active_count`=`active_count`-1";
            }
        }
//
//        $selectquery = sprintf("SELECT * FROM `trial_registration` WHERE $date_string and  `company_id`='%s' and `trial_id`='%s' and `trial_reg_id`='%s'",
//                $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $trial_reg_id));
//        $res = mysqli_query($this->db, $selectquery);
//       if (!$res) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
//            log_info($this->json($selectquery));
//            
//        } else {
//            $num_of_rows = mysqli_num_rows($res);
//            if ($num_of_rows == 1 && !empty($old_trial_status)) {
//                $flag = 1;
//            }
//        }
          $selectquery = sprintf("SELECT registration_date FROM `trial_registration` WHERE `company_id`='%s' and `trial_id`='%s' and `trial_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $trial_reg_id));
        $res = mysqli_query($this->db, $selectquery);
       if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
            cj_log_info($this->json($selectquery));
            $flag = 1;
         } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows == 1) {
               $rows = mysqli_fetch_object($res);
               $reg_date = $rows->registration_date;
            }
        }
      
        if (!empty($old_trial_status)) {
            if($trial_status == 'A'){
//                if($flag == 1){
                  $trial_string = "$trial_string, `active_count`=`active_count`+1";
//                }else{
//                  $trial_string = "`active_count`=`active_count`+1";
//                }
            }else if($trial_status == 'E'){
//                if($flag == 1){
                    $trial_string = "$trial_string, `enrolled_count`=`enrolled_count`+1";
//                }else{
//                     $trial_string = "`enrolled_count`=`enrolled_count`+1";
//                }
                
            }else if($trial_status == 'C'){
//                if($flag == 1){
                    $trial_string = "$trial_string, `cancelled_count`=`cancelled_count`+1";
//                }else{
//                     $trial_string = "`cancelled_count`=`cancelled_count`+1";
//                }
                
            }else if($trial_status == 'D'){
//                if($flag == 1){
                    $trial_string = "$trial_string, `didnotstart_count`=`didnotstart_count`+1";
//                }else{
//                     $trial_string = "`didnotstart_count`=`didnotstart_count`+1";
//                }
                
            }
            
        } else {
            $trial_string = "`active_count`=`active_count`+1";
            $reg_date = $every_month_dt;
        }

        if (!empty(trim($trial_string))) {
            $query1 = sprintf("UPDATE `trial_dimensions` SET $trial_string WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') ",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), $reg_date, $date_str);
            $result1 = mysqli_query($this->db, $query1);
//            log_info("dim   ".$query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                cj_log_info($this->json($error_log));
                $flag = 1;
             }
        }
        
        date_default_timezone_set($curr_time_zone);
        return $flag;
    }
    
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
//    private function sendEmailForAdmins($msg){
//        if($this->env=='P'){
//            $env = "Production";
//        }else{
//            $env = "Development";
//        }
//        $cmp_name = "MyStudio Cron Deamon";
//        $to = 'jim.p@technogemsinc.com';
//        $cc_email_list = 'rajeshsankar.k@technogemsinc.com';
//        $mail = new phpmailer(true);
//        try{
//            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
//            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
//            $mail->isSMTP();
//            $mail->SMTPAuth = true; //Username to use for SMTP authentication
//            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
//            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
//            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            $mail->SMTPSecure = 'tls';
//            $mail->Port = 587;
//            $mail->IsHTML(true);
//            $mail->FromName = $cmp_name;
//            $mail->AddAddress($to);
//            if(!empty(trim($cc_email_list))){
//                $cc_addresses = explode(',', $cc_email_list);
//                for($init=0;$init<count($cc_addresses);$init++){
//                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
//                        $mail->AddCC($cc_addresses[$init]);
//                    }
//                }
//            }
//            
//            $mail->Subject = "$env - Membership Handler Error.";
//            $mail->Body = $msg;
//            $response['mail_status'] = $mail->Send();
//            $response['status'] = "true";
//        }catch (phpmailerException $e) {
//            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
//            $response['status'] = "false";
//        } catch (Exception $e) {
//            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
//            $response['status'] = "false";
//        }
//        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
//        cj_log_info($this->json($error_log));
//        return $response;
//    }
    
}

$api = new TrialUpdateStatus;
$api->processApi();
?>