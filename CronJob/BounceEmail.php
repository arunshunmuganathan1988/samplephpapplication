<?php

require_once __DIR__.'/../Globals/cont_log.php';
$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}

class BounceEmail {    
    
    private $db = NULL, $prod_db = NULL;
    private $dbhost = '', $dbusername = '', $dbpassword = '', $dbName = '';
    private $prod_dbhost = '', $prod_dbusername = '', $prod_dbpassword = '', $prod_dbName = '';
    
    public function __construct() {
        require_once __DIR__.'/../Globals/config.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
         
    }
    
    private function getProdDetails() {
        $file_name = __DIR__ . "/../Globals/mystudio.academy.cfg";
        
        $file = explode( PHP_EOL, file_get_contents("$file_name"));
        $i=0;
        foreach( $file as $line ) {
            if($i<=3 && $line!=""){
                $config = explode("=", $line);
                if($i==0){
                    $this->prod_dbhost = $config[1];
                    $i++;
                }elseif($i==1){
                    $this->prod_dbusername = $config[1];
                    $i++;
                }elseif($i==2){
                    $this->prod_dbpassword = $config[1];
                    $i++;
                }elseif($i==3){
                    $this->prod_dbName = $config[1];
                    $i++;
                }
            }
        }
        $this->ProdDbConnect();     // Initiate Database connection
    }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }
    
    private function ProdDbConnect() {
        $this->prod_db = mysqli_connect($this->prod_dbhost, $this->prod_dbusername, $this->prod_dbpassword, $this->prod_dbName);
        if (!$this->prod_db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }
    
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    public function processApi() {
        if(strpos($GLOBALS['HOST_NAME'], 'dev.mystudio.academy') !== false) {
            $this->updateBouncedEmailFromProdToDev();
        }
        $this->verifyBouncedEmail();
        $this->update_campaign_log();
    }
    
    private function verifyBouncedEmail(){
        cj_log_info("verifyBouncedEmail - Started");
        $selectsql=sprintf("SELECT  `type`, `subtype`, `detail`, `recipient`, `error`, `source_ip`, `bounce_date`, `message_id` FROM `bounce_email` WHERE created_dt >= DATE_SUB(NOW(),INTERVAL 13 HOUR)");
        $result= mysqli_query($this->db, $selectsql);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.");
            cj_log_info($this->json($error_log));
            
        }else{
             $num_of_rows = mysqli_num_rows($result);
             if($num_of_rows>0){
                while($rows=mysqli_fetch_assoc($result)) {
                     
                    $sql = sprintf("INSERT INTO `bounce_email_verify`(`bounced_mail`, `type`, `subtype`, `detail`, `error`, `source_ip`, `bounce_date`, `message_id`)
                             VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') ON DUPLICATE KEY UPDATE  `type`='%s',`subtype`='%s',`detail`='%s',`error`='%s',`source_ip`='%s',`bounce_date`='%s',`message_id`='%s',`email_subscription_status`='B'"
                            ,mysqli_real_escape_string($this->db,$rows['recipient']),mysqli_real_escape_string($this->db,$rows['type']),mysqli_real_escape_string($this->db,$rows['subtype']),mysqli_real_escape_string($this->db,$rows['detail']),
                            mysqli_real_escape_string($this->db,$rows['error']),mysqli_real_escape_string($this->db,$rows['source_ip']),mysqli_real_escape_string($this->db,$rows['bounce_date']),mysqli_real_escape_string($this->db,$rows['message_id']),
                            mysqli_real_escape_string($this->db,$rows['type']),mysqli_real_escape_string($this->db,$rows['subtype']),mysqli_real_escape_string($this->db,$rows['detail']),
                            mysqli_real_escape_string($this->db,$rows['error']),mysqli_real_escape_string($this->db,$rows['source_ip']),mysqli_real_escape_string($this->db,$rows['bounce_date']),mysqli_real_escape_string($this->db,$rows['message_id']));
                    $result1 = mysqli_query($this->db, $sql);
                    if(!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.","sql"=>"$sql");
                        cj_log_info($this->json($error_log));                        
                    }
                }
            }
        }
        cj_log_info("verifyBouncedEmail - Completed");
    }
    
    private function updateBouncedEmailFromProdToDev(){
        cj_log_info("updateBouncedEmailFromProdToDev - Started");
        $this->getProdDetails();
        $selectsql=sprintf("SELECT  `type`, `subtype`, `detail`, `recipient`, `error`, `source_ip`, `bounce_date`, `message_id` FROM `bounce_email` WHERE created_dt >= DATE_SUB(NOW(),INTERVAL 13 HOUR)");
        $result= mysqli_query($this->prod_db, $selectsql);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.");
            cj_log_info($this->json($error_log));            
        }else{
            $num_of_rows = mysqli_num_rows($result);
            if($num_of_rows>0){
                while($rows=mysqli_fetch_assoc($result)) {
                    $sql = sprintf("INSERT INTO `bounce_email`(`recipient`, `type`, `subtype`, `detail`, `error`, `source_ip`, `bounce_date`, `message_id`)
                            VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$rows['recipient']), mysqli_real_escape_string($this->db,$rows['type']), 
                            mysqli_real_escape_string($this->db,$rows['subtype']), mysqli_real_escape_string($this->db,$rows['detail']), mysqli_real_escape_string($this->db,$rows['error']), 
                            mysqli_real_escape_string($this->db,$rows['source_ip']), mysqli_real_escape_string($this->db,$rows['bounce_date']), mysqli_real_escape_string($this->db,$rows['message_id']));
                    $result1 = mysqli_query($this->db, $sql);
                    if(!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.","sql"=>"$sql");
                        cj_log_info($this->json($error_log));                        
                    }
                }
            }
        }
        cj_log_info("updateBouncedEmailFromProdToDev - Completed");
    }
    
    
    
    private function update_campaign_log() {
        cj_log_info("update_campaign_log - start");
        $sql = sprintf("UPDATE campaign_email_log a INNER JOIN bounce_email_verify b ON a.`buyer_email` = b.`bounced_mail` SET  `email_status`= 'B'  WHERE b.created_dt >= DATE_SUB(NOW(),INTERVAL 26 HOUR)  and `email_status`='' and `email_sent_count` != 1");
          $result1 = mysqli_query($this->db, $sql);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "sql" => "$sql");
            cj_log_info($this->json($error_log));
        } else {
             $num_rows = mysqli_affected_rows($this->db);
            if ($num_rows > 0) {
                $selectsql = sprintf("select `email_id` from campaign_email_log a INNER JOIN bounce_email_verify b ON a.`buyer_email` = b.`bounced_mail` 
                            WHERE  b.created_dt >= DATE_SUB(NOW(),INTERVAL 26 HOUR)  and `email_status`='B' and `email_sent_count` != 1");
                $result = mysqli_query($this->db, $selectsql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "sql" => "$sql");
                    cj_log_info($this->json($error_log));
                } else {
                    while ($row = mysqli_fetch_assoc($result)) {
                        $updatecamp = sprintf("UPDATE `campaign_email_log` SET `email_sent_count`=1 WHERE `email_id`='%s'", mysqli_real_escape_string($this->db, $row['email_id']));
                        $updatecampresult = mysqli_query($this->db, $updatecamp);
                        if (!$updatecampresult) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "sql" => "$updatecamp");
                            cj_log_info($this->json($error_log));
                        }
                        $update = sprintf("UPDATE `campaign` SET `email_failed_count`=`email_failed_count`+1 WHERE `campaign_id`='%s'", mysqli_real_escape_string($this->db, $row['email_id']));
                        $updateresult = mysqli_query($this->db, $update);
                        if (!$updateresult) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "sql" => "$update");
                            cj_log_info($this->json($error_log));
                        }
                    }
                }
            }
        }


        cj_log_info("update_campaign_log - ended");
    }

}
$api = new BounceEmail;
$api->processApi();
