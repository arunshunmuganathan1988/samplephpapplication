<?php
$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_old'){
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}
require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';
require_once __DIR__.'/../Stripe/init.php';

class UpdateStripeNegativeBalance {

    public function __construct() {
        require_once __DIR__.'/../Globals/config.php';
        require_once __DIR__.'/../Globals/stripe_props.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
    }

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    public function processApi() {
        $this->checkStripeAvailableBalance();
    }
    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
    
    public function checkStripeAvailableBalance() {
        
        $query = sprintf("SELECT c.company_id,s.account_id,s.charges_enabled,s.payouts_enabled FROM  stripe_account s LEFT JOIN company c on c.company_id = s.company_id WHERE c.deleted_flag != 'Y' and s.account_id != 'NULL'");
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));
        } else {
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $company_id = $row['company_id'];
                    $account_id = $row['account_id'];
                    $charges_enabled = $row['charges_enabled'];
                    $payouts_enabled = $row['payouts_enabled'];
                    if($charges_enabled == 'N' || $payouts_enabled == 'N'){
                        
                    }else{
                        $this->updateStripeAvailableBalanceFlag($company_id,$account_id);
                    }
                }
            }else {
                $error = array('status' => "Failed", "msg" => "No company details");
                cj_log_info($this->json($error));                
                return $this->json($error);
            }
        } 
    }
    public function updateStripeAvailableBalanceFlag($company_id, $stripe_account_id) {
        $total_available_balance = $total_pending_balance = $total_balance = 0;
        $stripe_payout_last_date = '';
        $stripe_balance_flag = 'Y';
        $user_timezone = $this->getUserTimezone($company_id);
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $current_date = date("Y-m-d");
        
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
                
        // Payout last date
        try {                        
            $payout_list = \Stripe\Payout::all([
                "limit" => 10],
                ['stripe_account' => $stripe_account_id]);
                if(count($payout_list->data) > 0){
                    $stripe_payout_last_date = date("Y-m-d", $payout_list->data[0]->created);
                }
        }catch (Stripe\Error\Permission $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            stripe_log_info("Payout Balance of connected Account - Error:".json_encode($body));
            $error = array('status' => "Failed", 'msg' => $err['message']);
            $this->sendEmailForAdmins('stripe negative balance update '.json_encode($error));
            return $this->json($error);
        }catch (Stripe\Error\InvalidRequest $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            stripe_log_info("Payout Balance of connected Account - Error:".json_encode($body));
            $error = array('status' => "Failed", 'msg' => $err['message']);
            $this->sendEmailForAdmins('stripe negative balance update '.json_encode($error));
            return $this->json($error);
        }
        // calculate difference betwwen last payout date with 
        $stripe_payout_last_date_obj = date_create($stripe_payout_last_date);
        $current_date_obj = date_create($current_date);
        $diff_date = date_diff($stripe_payout_last_date_obj,$current_date_obj);
        $diff_date = $diff_date->format("%R%a");
                
        // retrieve balance
        try {                        
            $connected_account_balance = \Stripe\Balance::retrieve(
                        ['stripe_account' => $stripe_account_id]);
        } catch (Stripe\Error\Permission $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            stripe_log_info("Payout Balance of connected Account - Error:".json_encode($body));
            $error = array('status' => "Failed", 'msg' => $err['message']);
            $this->sendEmailForAdmins('stripe negative balance update '.json_encode($error));
            return $this->json($error);
        }catch (Stripe\Error\InvalidRequest $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            stripe_log_info("Payout Balance of connected Account - Error:".json_encode($body));
            $error = array('status' => "Failed", 'msg' => $err['message']);
            $this->sendEmailForAdmins('stripe negative balance update '.json_encode($error));
            return $this->json($error);
        }
        for($i=0; $i<count($connected_account_balance->available); $i++){
            $total_available_balance += $connected_account_balance->available[$i]->amount;
        }

        for($i=0; $i<count($connected_account_balance->pending); $i++){
            $total_pending_balance += $connected_account_balance->pending[$i]->amount;
        }
        // calculate balance 
        $total_balance = number_format((($total_available_balance+$total_pending_balance)/100), 2);
                
        if($total_balance >= 0){
           $stripe_balance_flag = 'Y';
        }else{
             if($diff_date < 30){
                $stripe_balance_flag = 'Y';
             }else{
                $stripe_balance_flag = 'N';
             }
        }
        $update_query = sprintf("UPDATE `stripe_account` SET `stripe_refund_flag`='%s' WHERE `account_id`='%s' AND `company_id`='%s'",
            mysqli_real_escape_string($this->db, $stripe_balance_flag),mysqli_real_escape_string($this->db, $stripe_account_id), mysqli_real_escape_string($this->db, $company_id));
        $update_result = mysqli_query($this->db, $update_query);
        if(!$update_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
            cj_log_info($this->json($error_log));
        }
    }
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cj_log_info($this->json($error_log));                
            return $this->json($error_log);
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }

    private function sendEmailForAdmins($msg){
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com, muthulakshmi.m@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "Push Certificate Expired Handler Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        cj_log_info($this->json($error_log));
        return $response;
    }    
}
$api = new UpdateStripeNegativeBalance;
$api->processApi();



