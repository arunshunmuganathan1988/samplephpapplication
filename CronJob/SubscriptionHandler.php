<?php
$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_old'){
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}
require_once __DIR__.'/../Stripe/init.php';
require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/Salesforce.php';
require_once __DIR__.'/PaySimple.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';

class SubscriptionHandler{
    
    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $sf;
    private $ps;
    private $list_id = '';
    private $server_url, $cron_props_file, $cron_sf, $cron_ps, $cron_ev, $cron_ms, $env;
    
    public function __construct(){
        require_once __DIR__.'/../Globals/stripe_props.php';
        require_once __DIR__.'/../Globals/config.php';
        $this->sf = new Salesforce();
        $this->ps = new PaySimple();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $this->list_id = $this->sf->list_id;
        if(!empty($GLOBALS['HOST_NAME'])){
            $host = $GLOBALS['HOST_NAME'];
        }else{
            $host = $_SERVER['HTTP_HOST'];
        }
        if(strpos($host, 'dev.mystudio.academy') !== false){
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'https://www.mystudio.academy';
            $this->cron_props_file = "cron_prod_props.php";
            $this->env = 'P';
        }else{
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = "cron_dev_props.php";
            $this->env = 'D';
        }
        $this->getUserandSecret();
    }
    
    private function getUserandSecret(){
        require_once "$this->cron_props_file";
        $this->cron_sf = SF_CRON;
        $this->cron_ps = PS_CRON;
    }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cj_log_info($this->json($error_log));
        }
    }
    
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function processApi(){
        if($this->cron_ps=='Y'){
            $this->runCronJobForPS();
            $this->runCronJobForLastPaymentStatus();
        }
        $this->runCronJobForDailyCheck();
    }
    
    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
    
    //Cron Job for Paysimple
    private function runCronJobForPS(){
        $total_records_processed = 0;
        $start_time = gmdate("Y-m-d H:i:s");
        cj_log_info("runCronJobForPS() - Started");
//        $curr_date = gmdate("Y-m-d");
        $query1 = sprintf("select `company_id`, `upgrade_status`, date(`expiry_dt`) expiry, `list_item_id`, `contact_id` from `company` where `subscription_status`='Y' AND stripe_subscription!='Y' and `upgrade_status` in ('P','W','M') and (DATE(`expiry_dt`) between DATE_SUB(CURDATE(), INTERVAL 1 DAY) and CURDATE())");
        $result1 = mysqli_query($this->db, $query1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            cj_log_info($this->json($error_log));
        } else {
            $num_of_rows1 = mysqli_num_rows($result1);
            if ($num_of_rows1 > 0) {
                $total_records_processed+=$num_of_rows1;
                while($row1 = mysqli_fetch_assoc($result1)){
                    $company_id = $row1['company_id'];
                    $upgrade_status = $row1['upgrade_status'];
                    $expiry = $row1['expiry'];
                    $list_item_id = $row1['list_item_id'];
                    $contact_id = $row1['contact_id'];
                    
                    $query2 = sprintf("select * from `user_account` where `company_id`='%s' and `premium_type`='%s' and `status`='Active' and `expiry_dt`='%s'",$company_id,$upgrade_status,$expiry);
                    $result2 = mysqli_query($this->db, $query2);
                    if(!$result2){
                        $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                        cj_log_info($this->json($error_log2));
                    }else{
                        $num_of_rows2 = mysqli_num_rows($result2);
                        if ($num_of_rows2 > 0) {
                            $row2 = mysqli_fetch_assoc($result2);
                            $user_account_id = $row2['user_account_id'];
                            $scheduleId = $row2['schedule_id'];
                            $expiry_date = $row2['expiry_dt'];
                            
                            if(!empty(trim($scheduleId))){
                                $ps_response = $this->ps->accessPaySimpleApi("recurringpayment/$scheduleId",'GET','');
                                cj_log_info("runCronJobForPS: ".$ps_response['status']);
                                if($ps_response['status'] == "Success"){
                                    $next_schedule = gmdate('Y-m-d', strtotime($ps_response['msg']['Response']['NextScheduleDate']));
                                    $next_schedule_with_time = gmdate('Y-m-d H:i:s', strtotime($ps_response['msg']['Response']['NextScheduleDate']));
                                    $status = $ps_response['msg']['Response']['ScheduleStatus'];

                                    if($status=='Active' && $next_schedule>$expiry_date){
                                        $query3 = sprintf("update `company` set `expiry_dt`='%s' where `company_id`='%s'",mysqli_real_escape_string($this->db,$next_schedule_with_time),mysqli_real_escape_string($this->db,$company_id));
                                        $result3 = mysqli_query($this->db, $query3);
                                        if(!$result3){
                                            $error_log3 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                                            cj_log_info($this->json($error_log3));
                                        }
                                        $query4 = sprintf("update `user_account` set `expiry_dt`='%s', `status`='%s' where `user_account_id`='%s'",
                                                mysqli_real_escape_string($this->db,$next_schedule),mysqli_real_escape_string($this->db,$status),mysqli_real_escape_string($this->db,$user_account_id));
                                        $result4 = mysqli_query($this->db, $query4);
                                        if(!$result4){
                                            $error_log4 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query4");
                                            cj_log_info($this->json($error_log4));
                                        }
                                    }elseif($status != 'Active'){
                                        $query3 = sprintf("update `company` set `subscription_status`='N', `upgrade_status`='F' where `company_id`='%s'",mysqli_real_escape_string($this->db,$company_id));
                                        $result3 = mysqli_query($this->db, $query3);
                                        if(!$result3){
                                            $error_log3 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                                            cj_log_info($this->json($error_log3));
                                        }
                                        $query4 = sprintf("update `user_account` set `status`='%s' where `user_account_id`='%s'",mysqli_real_escape_string($this->db,$status),mysqli_real_escape_string($this->db,$user_account_id));
                                        $result4 = mysqli_query($this->db, $query4);
                                        if(!$result4){
                                            $error_log4 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query4");
                                            cj_log_info($this->json($error_log4));
                                        }
                                    }else{
                                        $error_log = array('status' => "Failed", "msg" => "Paysimple status($status) & Next Schedule($next_schedule) for Company($company_id).", "query"=>"$query2");
                                        cj_log_info($this->json($error_log));
                                        $msg = "Paysimple status($status) & Next Schedule($next_schedule) for Company($company_id).";
                                        $this->sendEmailForAdmins($msg);
                                    }
                                }else{
                                    $error_log = array('status' => "Failed", "msg" => "Paysimple API call failure for scheduleId - $scheduleId.");
                                    cj_log_info($this->json($error_log));
                                }
                            }else{
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "ScheduleId not available for user_account : " => "$user_account_id");
                                cj_log_info($this->json($error_log));
                            }
                        }else{
                            $error_log = array('status' => "Failed", "msg" => "No Payment details available for Company($company_id).", "query"=>"$query2");
                            cj_log_info($this->json($error_log));
                            $msg = "No Paysimple Payment details available for Company($company_id).";
                            $this->sendEmailForAdmins($msg);
                        }
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No company details available for PaySimple validity check.");
                cj_log_info($this->json($error_log));
            }
        }
        cj_log_info("runCronJobForPS() - Completed");
        if($total_records_processed>0){
            $end_time = gmdate("Y-m-d H:i:s");
            $cron_file_name = "SubscriptionHandler";
            $cron_function_name = "runCronJobForPS";
            $cron_message = "Total paysimple access for studio validity";
            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
            if(!$cron_log_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
                cj_log_info($this->json($error_log));
            }
        }
    }
    
    //Cron Job for Last Payment Status
    private function runCronJobForLastPaymentStatus(){        
        $total_records_processed = 0;
        $start_time = gmdate("Y-m-d H:i:s");
        cj_log_info("runCronJobForLastPaymentStatus() - Started");
        $ua_array = [];
        $curr_month = gmdate("Y-m");
        $curr_date = gmdate("Y-m-d");
        $query1 = sprintf("select c.`company_id`,u.`user_id`,u.`user_email`,ua.`premium_type`, `upgrade_status`, date(c.`expiry_dt`) expiry, `user_account_id`, `schedule_id`, `last_payment_date`, DATEDIFF(CURDATE(), `last_payment_date`) waiting_days FROM `company` c 
                  LEFT JOIN `user_account` ua ON c.`company_id` = ua.`company_id` AND date(c.`expiry_dt`) = ua.`expiry_dt` AND `premium_type` = `upgrade_status`
                  LEFT JOIN `user` u ON c.`company_id` = u.`company_id`
                  WHERE `subscription_status`='Y' AND `upgrade_status` in ('P','W','M') AND `status`='Active' AND `last_payment_date` < CURDATE() AND c.`stripe_subscription`='N' AND u.`user_type`='A'");
        $result1 = mysqli_query($this->db, $query1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            cj_log_info($this->json($error_log));
        } else {
            $num_of_rows1 = mysqli_num_rows($result1);
            if ($num_of_rows1 > 0) {
                while($row1 = mysqli_fetch_assoc($result1)){
                    $company_id = $row1['company_id'];
                    $user_account_id = $row1['user_account_id'];
                    $scheduleId = $row1['schedule_id'];
                    $expiry_date = $row1['expiry'];
                    $waiting_days = $row1['waiting_days'];
                    $last_payment_date = $row1['last_payment_date'];
                    $user_id = $row1['user_id'];
                    $user_email=$row1['user_email'];
                    $premium_type=$row1['premium_type'];
                    $i=$k=0;
                    if($waiting_days>=1){
                        $total_records_processed+=1;
                        if(isset($scheduleId) && !empty(trim($scheduleId)) && $scheduleId!=0){
                            $ps_response = $this->ps->accessPaySimpleApi("recurringpayment/$scheduleId/payments",'GET','');
                            cj_log_info("runCronJobForLastPaymentStatus: ".$ps_response['status']);
                            if($ps_response['status'] == "Success"){
                                $res = $ps_response['msg']['Response'];
                                for($i=0;$i<count($res);$i++){
                                    if($curr_month==gmdate('Y-m', strtotime($res[$i]['PaymentDate']))){
                                        if (strpos($res[$i]['ProviderAuthCode'], 'DECLINE') !== false || $res[$i]['Status'] === 'Failed') {
//                                            if($waiting_days>15){
                                                
                                                $ps_response2 = $this->ps->accessPaySimpleApi("recurringpayment/$scheduleId/suspend",'PUT','{}');
                                                if($ps_response2['curl_status']==204 || $ps_response2['status'] == "Success"){
                                                    cj_log_info("paymentPS: Success");
                                                    $ps_response3 = $this->ps->accessPaySimpleApi("recurringpayment/$scheduleId",'GET','');
                                                    cj_log_info("paymentPS: ".$ps_response3['status']);
                                                    if($ps_response3['status'] == 'Success'){
                                                        $ps_res3 = $ps_response3['msg'];
//                                                        $scheduleId_suspend = $ps_res3['Response']['Id'];
//                                                        $next_schedule_suspend = gmdate('Y-m-d', strtotime($ps_res3['Response']['NextScheduleDate']));
                                                        $status_suspend = $ps_res3['Response']['ScheduleStatus'];
                                                        if($status_suspend=='Suspended' && isset($user_account_id) && !empty($user_account_id)){
                                                            $update_suspend = sprintf("update `user_account` set `status`='%s', `expiry_dt`='%s', `last_payment_status` = 'Success', `flag`='D' where `user_account_id`='%s'",
                                                                    mysqli_real_escape_string($this->db,$status_suspend),mysqli_real_escape_string($this->db,$last_payment_date),$user_account_id);
                                                            $result_suspend = mysqli_query($this->db, $update_suspend);
                                                            if(!$result_suspend){
                                                                $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_suspend");
                                                                cj_log_info($this->json($err_log));
                                                            }                                                            
                                                            $query3 = sprintf("update `company` set `expiry_dt`='%s',`subscription_status`='N' WHERE `company_id`='%s'",mysqli_real_escape_string($this->db,$last_payment_date),mysqli_real_escape_string($this->db,$company_id));
                                                            $result3 = mysqli_query($this->db, $query3);
                                                            if(!$result3){
                                                                $error_log3 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                                                                cj_log_info($this->json($error_log3));
                                                            }else{                                                                
                                                                if($k==0){
                                                                    $this->updateStudioSubscriptionCardDetails($company_id,$user_id,$premium_type, 'registration', $user_email);
                                                                    $k++;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }else{
                                                    $err_log = array('status' => "Failed", "msg" => "$ps_response2");
                                                    cj_log_info($this->json($err_log));
                                                }
//                                            }else{
//                                                $query2 = sprintf("update `user_account` set `last_payment_status` = 'Failure' where `user_account_id`='%s'", mysqli_real_escape_string($this->db,$user_account_id));
//                                                $result2 = mysqli_query($this->db, $query2);
//                                                if(!$result2){
//                                                    $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
//                                                    cj_log_info($this->json($error_log2));
//                                                }
//                                            }
                                        }elseif(strpos($res[$i]['ProviderAuthCode'], 'Approved') !== false){
                                            $query2 = sprintf("update `user_account` set `last_payment_date` = `expiry_dt` where `user_account_id`='%s'", mysqli_real_escape_string($this->db,$user_account_id));
                                            $result2 = mysqli_query($this->db, $query2);
                                            if(!$result2){
                                                $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                                                cj_log_info($this->json($error_log2));
                                            }
                                        }else{
                                            $ua_array[] = $user_account_id;
                                        }
                                    }
                                }
                            }else{
                                $error_log = array('status' => "Failed", "msg" => "Paysimple API call failure for scheduleId - $scheduleId.");
                                $ua_array[] = $user_account_id;
                                cj_log_info($this->json($error_log));
                            }
                        }else{
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "ScheduleId not available for user_account : " => "$user_account_id");
                            $ua_array[] = $user_account_id;
                            cj_log_info($this->json($error_log));
                        }
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No company details available for PaySimple last payment status check.");
                cj_log_info($this->json($error_log));
            }
        }
        if(!empty($ua_array)){
            $reg_str = implode(", ", $ua_array);
            $msg = "Something went wrong for user_account_ids=$reg_str, check cron log or table";
            $this->sendEmailForAdmins($msg);
        }
        cj_log_info("runCronJobForLastPaymentStatus() - Completed");
        if($total_records_processed>0){
            $end_time = gmdate("Y-m-d H:i:s");
            $cron_file_name = "SubscriptionHandler";
            $cron_function_name = "runCronJobForLastPaymentStatus";
            $cron_message = "Total paysimple access for studio last payment status";
            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
            if(!$cron_log_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
                cj_log_info($this->json($error_log));
            }
        }
    }
    
    private function sendEmailForAdmins($msg){
        if($this->env=='P'){
            $env = "Production";
        }else{
            $env = "Development";
        }
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com,ravi@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "$env - Membership Subscription Handling Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        cj_log_info($this->json($error_log));
        return $response;
    }
    
    //Cron Job for Salesforce
    private function runCronJobForDailyCheck(){
        $total_records_processed = 0;
        $start_time = gmdate("Y-m-d H:i:s");
        cj_log_info("runCronJobForDailyCheck() - Started");
        $curr_date = gmdate("Y-m-d");
//        $prev_date = date("Y-m-d", strtotime($curr_date."-1 day"));
        $studio_sales=[];
        $query1 = sprintf("SELECT t1 . * , COUNT( t2.`company_id` ) active_users FROM (
            SELECT cmp.*, COUNT( s.`company_id` ) total_users FROM (SELECT tb1.*, tb2.`payment_amount` FROM (SELECT u.`user_email` , c.`company_id` , c.`company_name` , c.`phone_number`, c.`web_page`, c.`created_date` , DATE( c.`expiry_dt` ) expiry_dt, c.`upgrade_status`, c.`subscription_status`, c.`company_type`, c.`list_item_id`, c.`contact_id`, c.`deleted_flag`, c.`wepay_status`,
            IF( `subscription_status` =  'Y' && ( `upgrade_status` =  'P' || `upgrade_status` =  'W' || `upgrade_status` =  'M' ) ,  DATEDIFF( `expiry_dt` , CURDATE( ) ) , IF( `expiry_dt` IS NULL || `expiry_dt` =  '0000-00-00 00:00:00', 30 - DATEDIFF( CURDATE( ) , `created_date` ) , DATEDIFF( `expiry_dt` , CURDATE( ) ) ) ) no_of_days,
            IF( `subscription_status` =  'Y' && ( `upgrade_status` =  'P' || `upgrade_status` =  'W' || `upgrade_status` =  'M' ) ,  'N', IF( IF( `expiry_dt` IS NULL || `expiry_dt` =  '0000-00-00 00:00:00', DATEDIFF( CURDATE( ) , `created_date` ) , IF( DATEDIFF( `expiry_dt` , CURDATE( ) )>=0, DATEDIFF( `expiry_dt` , CURDATE( ) ), 31) ) >30,  'Y',  'N' ) ) is_expired
            FROM  `company` c, `user` u WHERE c.`company_id` = u.`company_id`)tb1 LEFT JOIN `user_account` tb2 ON tb1.`company_id` = tb2.`company_id` AND DATE(tb1.`expiry_dt`)=tb2.`expiry_dt`) cmp
            LEFT JOIN  `student` s ON cmp.`company_id` = s.`company_id` 
            GROUP BY cmp.`company_id`) t1
            LEFT JOIN  `student` t2 ON t1.`company_id` = t2.`company_id` 
            AND t2.`last_login_dt` BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE() 
            GROUP BY t1.`company_id`");
        $result1 = mysqli_query($this->db, $query1);
        
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            cj_log_info($this->json($error_log));
        }else{
            $sql_membership_sales = sprintf("SELECT  SUM(IF( mr.processing_fee_type = 2, mp.`payment_amount` + mp.`processing_fee`, mp.`payment_amount` )) AS Membership_Sales ,mp.company_id  from `membership_payment` mp LEFT JOIN  membership_registration mr ON
                           mp.`membership_registration_id` = mr.`membership_registration_id` and  mp.company_id = mr.company_id WHERE mp.`payment_status`='S' GROUP BY mp.`company_id`");
            
            $result_membership_sales=  mysqli_query($this->db,$sql_membership_sales);
            if(!$result_membership_sales){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_membership_sales");
                cj_log_info($this->json($error_log));
            }else{
                $num_of_rows=  mysqli_num_rows($result_membership_sales);
                if($num_of_rows > 0){
                    while($rows=  mysqli_fetch_assoc($result_membership_sales)){
                        $company_id=$rows['company_id'];
                        $studio_sales[$company_id]['membership_sales']=$rows['Membership_Sales'];
                    }
                }
            }
            
            $sql_event_sales = sprintf("SELECT SUM(if(er.processing_fee_type=2,(ep.payment_amount+if(ep.schedule_status='PR',(SELECT SUM(processing_fee) FROM event_payment WHERE event_reg_id=ep.event_reg_id AND checkout_id=ep.checkout_id AND schedule_status IN ('R','PR')),ep.processing_fee)),ep.payment_amount)) Event_Sales, 
                         er.company_id from event_payment ep LEFT JOIN event_registration er ON ep.event_reg_id = er.event_reg_id WHERE ep.schedule_status IN ('S', 'PR') GROUP BY company_id");
            
            $result_event_sales=  mysqli_query($this->db,$sql_event_sales);
            if(!$result_event_sales){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_event_sales");
                cj_log_info($this->json($error_log));
            }else{
                $num_of_rows1=  mysqli_num_rows($result_event_sales);
                if($num_of_rows1 > 0){
                    while($rows1=  mysqli_fetch_assoc($result_event_sales)){
                        $company_id=$rows1['company_id'];
                        $studio_sales[$company_id]['event_sales']=$rows1['Event_Sales'];
                    }
                }
            }
            
            


            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $total_records_processed+=$num_rows1;
                while($row1 = mysqli_fetch_assoc($result1)){
                    $company_id = $row1['company_id'];
                    $expiry = $row1['expiry_dt'];
                    $payment_amount = $row1['payment_amount'];
                    $subscription_status = $row1['subscription_status'];
                    $upgrade_status = $row1['upgrade_status'];
                    $rem_days = $row1['no_of_days'];
                    $is_expired = $row1['is_expired'];
                    $list_item_id = $row1['list_item_id'];
                    $contact_id = $row1['contact_id'];
                    $total_users = $row1['total_users'];
                    $active_users = $row1['active_users'];
                    $phone_number = $row1['phone_number'];
                    $web_page = $row1['web_page'];
                    $deleted_flag = $row1['deleted_flag'];
                    $wepay_status = $row1['wepay_status'];
                    if(isset($studio_sales[$company_id]['membership_sales'])){
                        $m_sales = $studio_sales[$company_id]['membership_sales'];
                    }else{
                        $m_sales = 0;
                    }
                    if(isset($studio_sales[$company_id]['event_sales'])){
                        $e_sales = $studio_sales[$company_id]['event_sales'];
                    }else{
                        $e_sales = 0;
                    }
                    
//                    if($is_expired=='Y'){
//                        if($subscription_status=='N'){
//                            if($upgrade_status=='T'){
//                                $query2 = sprintf("update `company` set `upgrade_status`='F' where `company_id`='%s'",$company_id);
//                                $result2 = mysqli_query($this->db, $query2);
//                                if(!$result2){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
//                                    cj_log_info($this->json($error_log));
//                                }else{
//                                    if(mysqli_affected_rows($this->db)>0){
//                                        $upgrade_status = 'F';
//                                    }else{
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error. Affected rows - 0.", "query" => "$query2");
//                                        cj_log_info($this->json($error_log));
//                                    }
//                                }
//                            }elseif($upgrade_status!='F'){
//                                $error_log = array('status' => "Error", "msg" => "Company($company_id) expiry details.", "detail" => "Subscription status($subscription_status) and Upgrade Status($upgrade_status) error.");
//                                cj_log_info($this->json($error_log));
//                            }
//                        }elseif($subscription_status=='Y'){
//                            $error_log = array('status' => "Error", "msg" => "Company($company_id) expiry details.", "detail" => "Expired($is_expired), Expiry($expiry), Subscription status($subscription_status) and Upgrade Status($upgrade_status) error.");
//                            cj_log_info($this->json($error_log));
//                        }
//                    }elseif($is_expired=='N'){
//                        if($upgrade_status=='F'){
//                            $error_log = array('status' => "Error", "msg" => "Company($company_id) expiry details.", "detail" => "Expired($is_expired), Subscription status($subscription_status) and Upgrade Status($upgrade_status) error.");
//                            cj_log_info($this->json($error_log));
//                        }
//                        if($expiry<$curr_date && $rem_days<-2){
//                            $error_log = array('status' => "Error", "msg" => "Company($company_id) expiry details.", "detail" => "Expired($is_expired), Subscription status($subscription_status) and Upgrade Status($upgrade_status) error.");
//                            cj_log_info($this->json($error_log));
//                            
//                            $query2 = sprintf("update `company` set `upgrade_status`='F', `subscription_status`='N' where `company_id`='%s'",$company_id);
//                            $result2 = mysqli_query($this->db, $query2);
//                            if(!$result2){
//                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
//                                cj_log_info($this->json($error_log));
//                            }else{
//                                if(mysqli_affected_rows($this->db)>0){
//                                    $upgrade_status = 'F';
//                                }else{
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error. Affected rows - 0.", "query" => "$query2");
//                                    cj_log_info($this->json($error_log));
//                                }
//                            }
//                        }
//                    }
                    
                    if($this->cron_sf=='N'){
                        continue;
                    }
                    
                    if(!empty(trim($list_item_id)) && !empty(trim($contact_id))){
                        
                        switch($upgrade_status){
                            case 'W':
                                $stat = 4;
                                break;
                            case 'WM':
                                $stat = 11;
                                break;                            
                            case 'P':
                                $stat = 7;
                                break;
                            case 'M':
                                $stat = 10;
                                break;
                            case 'T':
                                $stat = 0;
                                break;
                            case 'F':
                                $stat = 12;
                                break;
                            case 'B':
                                $stat = 13;
                                break;
                            default :
                                $stat = 12;
                                break;
                        }
                        if($deleted_flag=='Y'){
                            $d_flag = 0;
                        }else{
                            $d_flag = 1;
                        }
                        if($wepay_status=='Y'){
                            $wp_status = '1';
                        }else{
                            $wp_status = '0';
                        }
                        if($stat>1){
                            $next_payment_date = $expiry;
                            $next_payment_amount = $payment_amount;
                            $json = array("contactIds"=>array("$contact_id"),"fieldValues"=>array("process_status"=>array(array("raw"=>"$stat")), "3"=>array(array("raw"=>"$active_users")), "5"=>array(array("raw"=>"$total_users")), "7"=>array(array("raw"=>"$phone_number")),"6"=>array(array("raw"=>"$web_page")),"17"=>array(array("raw"=>"$d_flag")),"19"=>array(array("raw"=>"$next_payment_date")),"22"=>array(array("raw"=>"$next_payment_amount")),"24"=>array(array("raw"=>"$wp_status")),"26"=>array(array("raw"=>"$e_sales")),"27"=>array(array("raw"=>"$m_sales"))));  
                        }else{
                            $json = array("contactIds"=>array("$contact_id"),"fieldValues"=>array("process_status"=>array(array("raw"=>"$stat")), "3"=>array(array("raw"=>"$active_users")), "5"=>array(array("raw"=>"$total_users")), "7"=>array(array("raw"=>"$phone_number")),"6"=>array(array("raw"=>"$web_page")),"17"=>array(array("raw"=>"$d_flag")),"24"=>array(array("raw"=>"$wp_status")),"26"=>array(array("raw"=>"$e_sales")),"27"=>array(array("raw"=>"$m_sales"))));  
                        }
                        $postData = json_encode($json);
                        $sf_response = $this->sf->accessSalesforceApi("lists/$this->list_id/listitems/$list_item_id",'PUT',$postData);
                        $log = array('status' => $sf_response['status'], "msg" => "Company($company_id) SF list_item_id details.", "detail" => "SF list_item_id=>($list_item_id) and Total Users => ($total_users).");
                        cj_log_info($this->json($log));
                    }else{
                        $error_log = array('status' => "Failed", "msg" => "Company($company_id) SF list_item_id details.", "detail" => "SF list_item_id=>($list_item_id) and Total Users => ($total_users).");
                        cj_log_info($this->json($error_log));
                    }
                    
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records available for daily validity check.");
                cj_log_info($this->json($error_log));
            }
        }
        cj_log_info("runCronJobForDailyCheck() - Completed");
        if($total_records_processed>0){
            $end_time = gmdate("Y-m-d H:i:s");
            $cron_file_name = "SubscriptionHandler";
            $cron_function_name = "runCronJobForDailyCheck";
            $cron_message = "Total salesforce access for studio data upload";
            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
            if(!$cron_log_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
                cj_log_info($this->json($error_log));
            }
        }
    }
    
    protected function updateStudioSubscriptionCardDetails($company_id,$user_id,$payment_type, $payee_type, $stripe_email){
        $stripe_present=0;
        $check=sprintf("SELECT `sss_id`,`company_id` from `stripe_studio_subscription` where `company_id`='%s' ORDER BY `sss_id` DESC LIMIT 0,1", mysqli_real_escape_string($this->db,$company_id));
        $cron_result = mysqli_query($this->db, $check);
        if(!$cron_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_result");
            cj_log_info($this->json($error_log));
        }else{
            $sql_num_rows = mysqli_num_rows($cron_result);
            if($sql_num_rows>0){
                $sql_row1 = mysqli_fetch_assoc($cron_result);
                $sssn_id=$sql_row1['sss_id'];
                $company_id=$sql_row1['company_id'];
                $stripe_present=1;
//                return true;
            }
        }
        $current_date=gmdate('Y-m-d');
        $this->getStripeKeys();
        $stripe_customer_id = $stripe_card_id =$body3_arr =$stripe_charge_id= $last4=$ref_string=$stripe_card_status=$payment_desc= $future_payment_date='';        
        $email= mysqli_real_escape_string($this->db,$stripe_email);                       
        
        $sql = sprintf("select `company_name`, date(`expiry_dt`) expiry, `subscription_status`, `upgrade_status`, `studio_expiry_level`
                from `company` c where c.`company_id`='%s'",mysqli_real_escape_string($this->db,$company_id));
        $sql_result = mysqli_query($this->db, $sql);
        if(!$sql_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            cj_log_info($this->json($error_log));
            return true;
        }else{
            $sql_num_rows = mysqli_num_rows($sql_result);
            if($sql_num_rows>0){
                $sql_row = mysqli_fetch_assoc($sql_result);
                $expiry = $sql_row['expiry'];
                $sub_status = $sql_row['subscription_status'];
                $upgrade_status = $sql_row['upgrade_status'];
                $company_name = $sql_row['company_name'];
            }else{
                return true;
            }
        }
        $subscription_fee=$this->getSubscriptionPlanFee($company_id,$upgrade_status);
        if(!empty($subscription_fee)){
            $plan_fee=$subscription_fee['current_plan_fee'];
        }else{
           $error = array('status' => "Failed", "msg" => "Unable to get the payment plan details.");
           cj_log_info($error);
           return true;
        }
        
        if ($upgrade_status == 'W') {
            $future_payment_date = gmdate('Y-m-d', strtotime('+1 year', strtotime($expiry)));
            $payment_desc = "MyStudio White Label Membership";
        }else if($upgrade_status == 'P'){
            $future_payment_date = gmdate('Y-m-d', strtotime('+1 year', strtotime($expiry)));
            $payment_desc = "MyStudio Premium Yearly Membership";
        }else if($upgrade_status == 'M'){
            $future_payment_date = gmdate('Y-m-d', strtotime('first day of next month'));
            $payment_desc = "MyStudio Premium Monthly Membership";
        }else{
                return true;
            }
        
        $diff=$diff_d=$diff_m=$diff_y=$diff_temp=$outstanding_balance=0;
        if ($expiry < $current_date && $sub_status != 'Y') {               //RERUN OLD DUE PAYMENT
            if($upgrade_status!=='F'||$upgrade_status!=='T'){        //PAY OLD DUE   
              //FOR CALCULATING NUMBER OF MONTHS OF OLD DUE 
                $d1 = new DateTime( $expiry );
                $d2 = new DateTime( $future_payment_date );
                $diff_temp = $d2->diff( $d1 );
                if ($upgrade_status !== 'W' && $upgrade_status !== 'P') {//  
                    $diff_m = ($diff_temp->y * 12); //FOR MONTH OLD DUE CALCULATION
                    $diff_m += $diff_temp->m;
                    if ($diff_temp->d >= 1) {
                        $diff_m += 1;
                    } 
                    $diff = $diff_m;
                } else {             //FOR YEAR OLD DUE CALCULATION                                  
                    $diff_y = $diff_temp->y;
                    if ($diff_temp->m >= 1) {
                        $diff_y += 1;
                    } else if ($diff_temp->d >= 1) {
                        $diff_y += 1;
                    }
                    $diff = $diff_y;
                }
                $rem_day_payment_sss=number_format((float) ($diff * $plan_fee), 2, '.', '');
                $outstanding_balance = number_format($rem_day_payment_sss, 2, '.', '');
            }
        }else{
            $succ_msg = array("status" => "Failed", "msg" => "Subscription status or schedule mismatch.");
            cj_log_info($this->json($succ_msg));
            $this->sendEmailForAdmins($succ_msg['msg']);
            return true;
            }
           if($stripe_present==0){
                if($payee_type=='registration' && (empty($company_name) || empty($upgrade_status) || empty($sub_status))){
                    $error = array('status' => "Failed", "msg" => "Studio Details not found.");
                    cj_log_info($this->json($error));
                    $this->sendEmailForAdmins($error['msg']);
                    return true;
                }else{

                    \Stripe\Stripe::setApiKey($this->stripe_sk);       

                    try {

                        // Use Stripe's library to make requests...
                        $stripe_customer_create = \Stripe\Customer::create([
                                    "email" => "$email",
                                    "description" => "Customer Subscription for $company_name"
                                 // "source" => "$token_id" // obtained with Stripe.js
                        ]);
                        $body = json_encode($stripe_customer_create);
                        stripe_log_info("Customer_new : $body");
                        $body_arr = json_decode($body, true);
                        $stripe_customer_id = $body_arr['id'];
                    } catch(\Stripe\Error\Card $e) {
                        // Since it's a decline, \Stripe\Error\Card will be caught       
                        $body = $e->getJsonBody();
                        $err  = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\RateLimit $e) {
                      // Too many requests made to the API too quickly      
                        $body = $e->getJsonBody();
                        $err  = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\InvalidRequest $e) {
                      // Invalid parameters were supplied to Stripe's API      
                        $body = $e->getJsonBody();
                        $err  = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Authentication $e) {
                      // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                        $body = $e->getJsonBody();
                        $err  = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\ApiConnection $e) {
                      // Network communication with Stripe failed      
                        $body = $e->getJsonBody();
                        $err  = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Base $e) {
                      // Display a very generic error to the user, and maybe send yourself an email      
                        $body = $e->getJsonBody();
                        $err  = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (Exception $e) {
                      // Something else happened, completely unrelated to Stripe      
                        $body = $e->getJsonBody();
                        $err  = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    }
                    if(isset($err) && !empty($err)){
                        cj_log_info($this->json($err));
                        return true;

                    }

                    $payment_type=$upgrade_status;
                    $sql2 = sprintf("update `company` set `stripe_subscription`='Y',`studio_payment_status`='F',`studio_expiry_level`='L1',`expiry_dt`='$future_payment_date' where `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
                    $sql_result2 = mysqli_query($this->db, $sql2);
                    if(!$sql_result2){
                        $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        cj_log_info($this->json($err_log));
                        return true;
                    }else{
                        $insert_query = sprintf("INSERT INTO `stripe_studio_subscription`(`company_id`, `user_id`, `status`, `expiry_date`, `premium_type`,`stripe_customer_id`,`email`,`outstanding_balance`,`last_successfull_payment_date`) 
                                VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                                mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $user_id), mysqli_real_escape_string($this->db, 'A'), mysqli_real_escape_string($this->db, $future_payment_date), 
                                mysqli_real_escape_string($this->db, $upgrade_status),mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $email), mysqli_real_escape_string($this->db, $outstanding_balance),
                                mysqli_real_escape_string($this->db, $expiry));
                        $result_query = mysqli_query($this->db, $insert_query);
                        if(!$result_query){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query");
                            cj_log_info($this->json($error_log));
                            return true;
                        }else{
                            $succ_msg = array("status" => "Success", "msg" => "Card Details Updated Successfully.");
                            cj_log_info($this->json($succ_msg));
                            return true;
                        }
                    }
                }
           }else if($stripe_present==1){
               $sql2 = sprintf("update `company` set `stripe_subscription`='Y',`studio_payment_status`='F',`studio_expiry_level`='L1',`expiry_dt`='$future_payment_date' where `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
                    $sql_result2 = mysqli_query($this->db, $sql2);
                    if(!$sql_result2){
                        $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        cj_log_info($this->json($err_log));
                        return true;
                    }else{
                        $update_query=sprintf("UPDATE `stripe_studio_subscription` SET `expiry_date='%s', `outstanding_balance`='%s', `last_successfull_payment_date`='%s' WHERE `sss_id`='%s' AND `company_id='%s'",
                                                mysqli_real_escape_string($this->db, $future_payment_date), mysqli_real_escape_string($this->db, $outstanding_balance), mysqli_real_escape_string($this->db, $expiry),
                                                mysqli_real_escape_string($this->db, $sssn_id), mysqli_real_escape_string($this->db, $company_id));
                        $result_query = mysqli_query($this->db, $update_query);
                        if(!$result_query){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                            cj_log_info($this->json($error_log));
                            return true;
                        }else{
                            $succ_msg = array("status" => "Success", "msg" => "Card Details Updated Successfully.");
                            cj_log_info($this->json($succ_msg));
                            return true;
                        }
                    }
           }else{
               return true;
           }
    }
 
    private function getSubscriptionPlanFee($company_id,$upgrade_status){
        $plan_fee_arr=$plan_fee_arr_final=[];
        $plan_fee_arr_final['current_plan_fee']=$plan_fee_arr_final['current_plan_days']=$plan_fee_arr_final['current_plan_setup_fee']=0;
            $select_fee=sprintf("SELECT * FROM `studio_subscription_fee` WHERE 
                IF((SELECT count(*) count FROM `studio_subscription_fee` WHERE `company_id`='%s')=0,`company_id`=0,
                   (`company_id`='%s' or (`company_id`=0 and `premium_type` not in (select `premium_type` from studio_subscription_fee WHERE `company_id`='%s' AND `fee_end_date` IS NULL))))",
                    mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $company_id));
        $result=mysqli_query($this->db,$select_fee);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_fee");
            cj_log_info($this->json($error_log));
        } else {
            $nums_rows = mysqli_num_rows($result);
            if ($nums_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $plan_fee_arr['plan_fee'] = $rows['plan_fee']+0;
                    $plan_fee_arr['setup_fee'] = $rows['setup_fee']+0;
                    $plan_fee_arr['premium_type'] = $rows['premium_type'];
                    $plan_fee_arr['plan_days'] = $rows['plan_days'];
                    if($plan_fee_arr['premium_type']==$upgrade_status){
                        $plan_fee_arr_final['current_plan_fee']=$plan_fee_arr['plan_fee'];
                        $plan_fee_arr_final['current_plan_days']=$plan_fee_arr['plan_days'];
                        $plan_fee_arr_final['current_plan_setup_fee']=$plan_fee_arr['setup_fee'];
                    }
                    $plan_fee_arr_final['plans'][]=$plan_fee_arr;
                }
            }
        }        
        return $plan_fee_arr_final;
    }
       
}

$api = new SubscriptionHandler;
$api->processApi();