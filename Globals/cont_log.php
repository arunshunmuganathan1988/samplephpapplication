<?php

$sessVal = $sess = '';
$temp_arr = [];
if(isset($_SESSION)){
    $temp_arr = $_SESSION;
    if(isset($temp_arr['pwd'])){
        unset($temp_arr['pwd']);
    }
    $sessVal = json_encode($temp_arr);
    $sess = "xSessionVar: " . $sessVal;
}
if(isset($_REQUEST['run'])){
    $func = strtolower(trim(str_replace("/","",$_REQUEST['run'])));
    if($func!='login' && $func!='adminlogin'){
        $_REQUEST['params'] = file_get_contents('php://input');
    }else{
        $_REQUEST['params'] = '{}';
    }
    $request = json_decode($_REQUEST['params'], true);
    if(isset($request['event_banner_img_content'])){
        unset($request['event_banner_img_content']);
    }
    if(isset($request['event_desc'])){
        unset($request['event_desc']);
    }
    if(isset($request['category_description'])){
        unset($request['category_description']);
    }
    if(isset($request['membership_description'])){
        unset($request['membership_description']);
    }
    if(isset($request['logo_url'])){
        unset($request['logo_url']);
    }
    if(isset($request['class_schedule'])){
        unset($request['class_schedule']);
    }
    if(isset ($request['event_picture'])){
        unset($request['event_picture']);
    }
    if(isset ($request['company_logo'])){
        unset($request['company_logo']);
    }
    if(isset ($request['waiver_policies'])){
        unset($request['waiver_policies']);
    }
    if(isset($request['attached_file'])) {
        unset($request['attached_file']);
    }
    if(isset ($request['category_banner_img_content'])){
        unset($request['category_banner_img_content']);
    }
    if(isset ($request['trial_banner_img_content'])){
        unset($request['trial_banner_img_content']);
    }
    if(isset ($request['student_password'])){
        unset($request['student_password']);
    }
    if(isset ($request['trial_category_image'])){
        unset($request['trial_category_image']);
    }
    if(isset ($request['user_password'])){
        unset($request['user_password']);
    }
    if(isset ($request['email_banner_image'])){
        unset($request['email_banner_image']);
    }
    if(isset ($request['retail_banner_img_content'])){
        unset($request['retail_banner_img_content']);
    }
    if(isset($request['class_schedule'])){
        unset($request['class_schedule']);
    }    
    if(isset($request['retail_agreement_text'])){
        unset($request['retail_agreement_text']);
    }
    if(isset($request['retail_product_desc'])){
        unset($request['retail_product_desc']);
    }
    
    $_REQUEST['params'] = $request;
    
    if((strpos($_SERVER['PHP_SELF'], 'AttendanceApi.php') !== false && $_REQUEST['run']=='getMembershipStudents') || $_REQUEST['run']=='communicationIntervalCall'){
        return;
    }

}
$reqVal = json_encode($_REQUEST);
$newVal = preg_replace("/(.*)(\"password\":\"\S{1,8}\",)(.*)/", "\\1 ***** \\3", $reqVal);


if (strpos($_SERVER['PHP_SELF'], 'MobileApi') !== false) {
    mlog_info($sess . " RequestVar--: " . $newVal . " Page: " .  $_SERVER['PHP_SELF']."(IP : ".getRealIpAddr().")");
}elseif(strpos($_SERVER['PHP_SELF'], 'Cron') !== false){
    cj_log_info($sess . " RequestVar--: " . $newVal . " Page: " .  $_SERVER['PHP_SELF']);
}elseif(strpos($_SERVER['PHP_SELF'], 'wepayipn') !== false || strpos($_SERVER['PHP_SELF'], 'stripeWebhook') !== false){
    ipn_log_info($sess . " RequestVar--: " . $newVal . " Page: " .  $_SERVER['PHP_SELF']);
}elseif(strpos($_SERVER['PHP_SELF'], 'openApi') !== false){
    zap_log_info($sess . " RequestVar--: " . $newVal . " Page: " .  $_SERVER['PHP_SELF'] . " , " . "Method:" . $_SERVER['REQUEST_METHOD']);
}else{
    log_info($sess . " RequestVar--: " . $newVal . " Page: " .  $_SERVER['PHP_SELF']."(ip : ".getRealIpAddr().")");
}

function log_info($msg) {
    $today = gmdate("d.m.Y-h");
    $filename = __DIR__."/../Log/Web/$today.txt";
    $fd = fopen($filename, "a");
    $str = "[" . gmdate("d/m/Y H:i:s") . "] " . $msg;
    fwrite($fd, $str . PHP_EOL . PHP_EOL);
    fclose($fd);
}

function mlog_info($msg) {
    $today = gmdate("d.m.Y-h");
    $filename = __DIR__."/../Log/Mobile/$today.txt";
    $fd = fopen($filename, "a");
    $str = "[" . gmdate("d/m/Y H:i:s") . "] " . $msg;
    fwrite($fd, $str . PHP_EOL . PHP_EOL);
    fclose($fd);
}

function cj_log_info($msg){
    $today = gmdate("d.m.Y");
    $filename = __DIR__."/../Log/Cron/$today.txt";
    $fd = fopen($filename, "a");
    $str = "[" . gmdate("d/m/Y H:i:s") . "] " . $msg;
    fwrite($fd, $str . PHP_EOL . PHP_EOL);
    fclose($fd);
}

function zap_log_info($msg){
    $today = gmdate("d.m.Y");
    $filename = __DIR__."/../Log/Zap/$today.txt";
    $fd = fopen($filename, "a");
    $str = "[" . gmdate("d/m/Y H:i:s") . "] " . $msg;
    fwrite($fd, $str . PHP_EOL . PHP_EOL);
    fclose($fd);
    chmod($filename, 0777);  //changed to add the zero
}

function ipn_log_info($msg){
    $today = gmdate("d.m.Y");
    $filename = __DIR__."/../Log/Ipn/$today.txt";
    $fd = fopen($filename, "a");
    $str = "[" . gmdate("d/m/Y H:i:s") . "] " . $msg;
    fwrite($fd, $str . PHP_EOL . PHP_EOL);
    fclose($fd);
}

function stripe_log_info($msg){
    $today = gmdate("d.m.Y");
    $filename = __DIR__."/../Log/Stripe/$today.txt";
    $fd = fopen($filename, "a");
    $str = "[" . gmdate("d/m/Y H:i:s") . "] " . $msg;
    fwrite($fd, $str . PHP_EOL . PHP_EOL);
    fclose($fd);
    chmod($filename, 0777);  //changed to add the zero
}

function getRealIpAddr(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet        
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function cjp_log_info($msg){
    $today = gmdate("d.m.Y");
    $filename = __DIR__."/../Log/Cron/".$today."_payments.txt";
    $fd = fopen($filename, "a");
    $str = "[" . gmdate("d/m/Y H:i:s") . "] " . $msg;
    fwrite($fd, $str . PHP_EOL . PHP_EOL);
    fclose($fd);
}

?>