<?php

/*
 * Date : Dec 26, 2018
 * Author : Raaz
 */

$dev_key = array("pk"=>"pk_test_AqoWbBVO5W9aYFqNMX1NUdZT", "sk"=>"sk_test_Qcah764sJdfS9EXkST8njvp2", "webhooks_charge" => "whsec_rQDWPlwy1rZe0o1TFF583Q9LY62bk3WP", "webhooks_customers"=>"whsec_dM79SfGWOdlWy7GRQKVkeVP8kHJGbHtK", "webhooks_connected_account"=>"whsec_TGzHi5td7YGTNz9mk91nKXlfs3fk0Nen","webhooks_payment_intent" => "whsec_jwVGxXzM1qduePFuFckuJJZbgIPWiv5Q");
$prod_key = array("pk"=>"pk_live_NOw22rc8HYUWLTOdI2nT6riz", "sk"=>"sk_live_NhTvwfOutBAy32xBL9Kqt6Gd","webhooks_charge" => "whsec_RG0ovZuSMb3OxRyfDa2lkWgcROqwHi4l", "webhooks_customers"=>"whsec_JL9BiXSzRrSU5tIq4Vi7VgCpw8FDpchW", "webhooks_connected_account"=>"whsec_dpy5qnt9aXlFgxtR61dTw7ARin8tJa4M","webhooks_payment_intent" => "whsec_IpZKBqzTwyE7jleKdPn9kommD5IbTN49");

if(isset($GLOBALS['HOST_NAME']) && !empty($GLOBALS['HOST_NAME'])){
    
}else{
    if(isset($argv[1]) && !empty($argv[1])){
        if($argv[1]=='prod'){
            $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
        }elseif($argv[1]=='dev_new'){
            $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
        }else{
            $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
        }
    }elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
        $GLOBALS['HOST_NAME'] = $_SERVER['HTTP_HOST'];
    }else{
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }
}
$host = $GLOBALS['HOST_NAME'];
if (strpos($host, 'dev.mystudio.academy') !== false || strpos($host, 'dev2.mystudio.academy') !== false) {
    $GLOBALS['stripe_keys'] = $dev_key;
} elseif (strpos($host, 'mystudio.academy') !== false) {
    $GLOBALS['stripe_keys'] = $prod_key;
} else {
    $GLOBALS['stripe_keys'] = $dev_key;
}