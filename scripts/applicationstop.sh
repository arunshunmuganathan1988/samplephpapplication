#!/bin/bash

pckarr=(ruby wget)
yum update -y
for i in  ${pckarr[*]}
 do
  isinstalled=$(rpm -q $i)
  if [ !  "$isinstalled" == "package $i is not installed" ];
   then
    echo Package  $i already installed
  else
    echo $i is not INSTALLED!!!!
    yum install $i -y
  fi
done

cd /home/ec2-user
wget https://aws-codedeploy-eu-central-1.s3.amazonaws.com/latest/install
chmod +x ./install
./install auto
service codedeploy-agent status
service httpd start
chkconfig httpd on