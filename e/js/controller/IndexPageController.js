/*jslint browser: true*/
/*global console, MyApp*/
//www
MyApp.angular.controller('IndexPageController', ['$rootScope', '$scope', '$http', 'urlservice', 'InitService','$localStorage','openurlservice','$window',  function ($rootScope, $scope, $http, urlservice, InitService, $localStorage, openurlservice, $window) {
        'use strict';
      
        InitService.addEventListener('ready', function ()
        {
            console.log('IndexPageController: ok, DOM ready');
            
        });
        
        $scope.showcardselection = false;
        $scope.erroroptionaldiscount = false;
        $scope.discount_applied = false;
        $scope.event_search_member = "";        
        $scope.event_payment_method = 'CC';
        $scope.event_optional_discount = "";
        $scope.event_optional_discount_applied_flag = 'N';
        $scope.recurring_flag = 'N';
        $scope.shownewcard = true;
        $scope.student_name = [];
        $scope.eventcarddetails = [];
        $scope.option_disc_error_flg = false;
        $scope.option_disc_error_msg = "";
        $scope.recurring_payment_status = 'N';
        $scope.wepaystatus = $scope.stripestatus = '';
        $scope.wepay_enabled = $scope.stripe_enabled = $scope.event_ach_chkbx = $scope.event_cc_chkbx = false;
        $scope.stripe_status='';
        $scope.event_ach_route_no = $scope.event_ach_acc_number = $scope.event_ach_holder_name = $scope.event_ach_acc_type = '';
        $scope.studentlist = [];
        $scope.stripe_card = $scope.stripe_variable = '';
        $scope.stripe_intent_method_type = "PI";
        $scope.payment_method_id = $scope.stripe_customer_id = '';

        //No Internet - show alert
        $scope.istatus = function () {
            MyApp.alert('Please turn on internet', 'No Internet');
            $scope.preloaderVisible = false;
            return;
        };
        
        $scope.initial_load_payment = function(){
            $scope.onetimecheckout = 0;
            $scope.ccyearlist = ["2018","2019","2020","2021","2022","2023","2024","2025","2026","2027","2028","2029","2030","2031","2032","2033","2034","2035","2036","2037"];
            $scope.ccmonthlist = ["1","2","3","4","5","6","7","8","9","10","11","12"];
            $scope.ccmonth = '';$scope.ccyear = '';$scope.country = '';
            if($scope.wp_level === 'P'){
                $scope.wepay_env = "production";
                $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
            }else if($scope.wp_level === 'S'){
                $scope.wepay_env = "stage";
                $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
            }
                WePay.set_endpoint($scope.wepay_env); // change to "production" when live  
        }; 
        
        
         // Shortcuts
        var valueById = '';
        var d = document;
        d.id = d.getElementById, valueById = function(id) {
            return d.id(id).value;
        };

        // For those not using DOM libraries
        var addEvent = function(e,v,f) {
            if (!!window.attachEvent) { e.attachEvent('on' + v, f); }
            else { e.addEventListener(v, f, false); }
        };

        // Attach the event to the DOM
        addEvent(d.id('cc-submit'), 'click', function() { 
            $scope.eventWepayCheckout();
        }); 
        
        addEvent(d.id('cc-submit1'), 'click', function() { 
            $scope.eventWepayCheckout();
        }); 
        
        // Muthulakshmi
        $scope.eventWepayCheckout = function(){
            var checkout_message = '';
            for (var i = 0; i < $scope.reg_col_names.length; i++) {
                if ($scope.reg_col_field_name[i] && $scope.reg_col_field_name[i] !== undefined && $scope.reg_col_field_name[i] !== '') {

                } else {
                    if ($scope.reg_col_names[i].reg_col_mandatory === 'Y') {
                        checkout_message += '"' + $scope.reg_col_names[i].reg_col_name + '"';
                        checkout_message += '<br>';
                    }
                    $scope.reg_col_field_name[i] = '';
                }
            }
            if (checkout_message !== '') {
                checkout_message = 'Participant Info:' + checkout_message;
            }
            if ($scope.firstname === '' || $scope.firstname === undefined || $scope.lastname === '' || $scope.lastname === undefined || $scope.email === '' || $scope.email === undefined || $scope.phone === '' || $scope.phone === undefined || $scope.zip_code === '' || $scope.zip_code === undefined)
            {
                checkout_message += "Buyer's Info:";
                if ($scope.firstname === '' || $scope.firstname === undefined) {
                    checkout_message += '"First Name"<br>';
                }
                if ($scope.lastname === '' || $scope.lastname === undefined) {
                    checkout_message += '"Last Name"<br>';
                }
                if ($scope.email === '' || $scope.email === undefined) {
                    checkout_message += '"Email"<br>';
                }
                if ($scope.phone === '' || $scope.phone === undefined) {
                    checkout_message += '"Cell Phone"<br>';
                }                
                if ($scope.zip_code === '' || $scope.zip_code === undefined) {
                    checkout_message += '"Zip Code"<br>';
                }
            }
            if ($scope.cardnumber === '' || $scope.cardnumber === undefined || $scope.ccmonth === '' || $scope.ccmonth === undefined || $scope.ccyear === '' || $scope.ccyear === undefined || $scope.cvv === '' || $scope.cvv === undefined || $scope.country === '' || $scope.country === undefined || $scope.postal_code === '' || $scope.postal_code === undefined)
            {
                checkout_message += 'Payment Info:';
                if ($scope.cardnumber === '' || $scope.cardnumber === undefined) {
                    checkout_message += '"Card number"<br>';
                }
                if ($scope.ccmonth === '' || $scope.ccmonth === undefined) {
                    checkout_message += '"Expiration Date:Month"<br>';
                }
                if ($scope.ccyear === '' || $scope.ccyear === undefined) {
                    checkout_message += '"Expiration Date:Year"<br>';
                }
                if ($scope.cvv === '' || $scope.cvv === undefined) {
                    checkout_message += '"CVV"<br>';
                }
                if ($scope.country === '' || $scope.country === undefined) {
                    checkout_message += '"Choose country"<br>';
                }
                if ($scope.postal_code === '' || $scope.postal_code === undefined) {
                    checkout_message += '"Zip code"<br>';
                }
            }
            if (!$scope.event_waiver_checked) {
                checkout_message += "Click the check box to accept Waiver & Agreement<br>";
            }
            if (checkout_message !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }
            
            var userName = [valueById('firstName')]+' '+[valueById('lastName')];
            var user_first_name = valueById('firstName');
            var user_last_name = valueById('lastName');
            var email = valueById('email').trim();
            var phone = valueById('phone');
            var postal_code = valueById('postal_code');
            var response = WePay.credit_card.create({
                "client_id":        $scope.wp_client_id,
                "user_name":        userName,
                "email":            valueById('email').trim(),
                "cc_number":        valueById('cc-number'),
                "cvv":              valueById('cc-cvv'),
                "expiration_month": valueById('cc-month'),
                "expiration_year":  valueById('cc-year'),
                "address": {
                    "country": valueById('country'),
                    "postal_code": valueById('postal_code')
                }
            }, function(data) {
                if (data.error) {      

                    $scope.preloader = false;
                    $scope.$apply();
                    MyApp.closeModal();
                    MyApp.alert(data.error_description,'Message');
//                        $scope.preloader =false;
                    // handle error response
                } else {
                    if ($scope.payment_event_type === 'S') {                        
                        if ($scope.onetimecheckout == 0) {
                            $scope.onetimecheckout = 1;
                            $scope.checkout(data.credit_card_id, data.state, user_first_name, user_last_name, email, phone, postal_code);
                        }
                    }else if ($scope.payment_event_type === 'M') {                        
                        if ($scope.onetimecheckout == 0) {
                                $scope.onetimecheckout = 1;
                                $scope.checkout(data.credit_card_id, data.state, user_first_name, user_last_name, email, phone, postal_code);
                        }
                    }
                }
            }); 
        };
        
        //get Stripe status of this company
        $scope.getStripeStatus = function (cmp_id) {
            if ($scope.stripestatus !== 'Y') {
                $scope.wepay_enabled = $scope.stripe_enabled = false;
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getstripestatus',
                    params: {
                        "company_id": cmp_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.stripestatus = response.data.msg;
                        if ($scope.stripestatus !== 'N') {
                            if ($scope.stripe_country_support === 'Y') {
                                $scope.stripe_enabled = true;                                
                                $scope.stripe_variable = Stripe($scope.stripe_publish_key);
                                $scope.getStripeElements();       // AFTER GETTING STRIPE PUBLISH KEY WE GET STRIPE PAYMENT FORM
                            } else {
                                $scope.getWepayStatus(cmp_id);
                            } 
                        }else{
                            $scope.getWepayStatus(cmp_id);
                        }       
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        MyApp.alert(response.data.msg, '');
                    } else {
                        if(response.data.msg && response.data.msg !== 'N'){
                            MyApp.alert(response.data.msg, '');
                        }else{
                            $scope.getWepayStatus(cmp_id);
                        }
                    }
                }, function (response) {
                    console.log(response.data);
                    MyApp.alert(response.data, '');
                });
            } else {
                if ($scope.stripestatus !== 'N' && $scope.stripe_country_support === 'Y') {
                    $scope.stripe_enabled = true;
                } else {
                    $scope.wepay_enabled = $scope.stripe_enabled = false;
                    $scope.getWepayStatus(cmp_id);
                }
            }
        };
        
        $scope.eventStripeValidation = function(){
            $scope.preloader = true;
            var checkout_message = '';
            for (var i = 0; i < $scope.reg_col_names.length; i++) {
                if ($scope.reg_col_field_name[i] && $scope.reg_col_field_name[i] !== undefined && $scope.reg_col_field_name[i] !== '') {

                } else {
                    if ($scope.reg_col_names[i].reg_col_mandatory === 'Y') {
                        checkout_message += '"' + $scope.reg_col_names[i].reg_col_name + '"';
                        checkout_message += '<br>';
                    }
                    $scope.reg_col_field_name[i] = '';
                }
            }
            if (checkout_message !== '') {
                checkout_message = 'Participant Info:' + checkout_message;
            }
            if (!$scope.firstname || !$scope.lastname || !$scope.email || !$scope.phone || !$scope.zip_code)
            {
                checkout_message += "Buyer's Info:";
                if ($scope.firstname === '' || $scope.firstname === undefined) {
                    checkout_message += '"First Name"<br>';
                }
                if ($scope.lastname === '' || $scope.lastname === undefined) {
                    checkout_message += '"Last Name"<br>';
                }
                if ($scope.email === '' || $scope.email === undefined) {
                    checkout_message += '"Email"<br>';
                }
                if ($scope.phone === '' || $scope.phone === undefined) {
                    checkout_message += '"Cell Phone"<br>';
                }                
                if ($scope.zip_code === '' || $scope.zip_code === undefined) {
                    checkout_message += '"Zip Code"<br>';
                }
            }
            if($scope.final_payment_amount > 0 || ($scope.final_payment_amount === 0 && $scope.recurring_payment_show)) {
                if (($scope.shownewcard && !$scope.valid_stripe_card && $scope.event_payment_method === 'CC')
                || ($scope.pos_user_type === 'S' && $scope.event_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex)
                || (($scope.pos_user_type === 'S' || $scope.pos_user_type === 'P') && $scope.event_payment_method ==='CH' && !$scope.event_check_number)){

                    checkout_message += 'Payment Info:';

                    if($scope.pos_user_type === 'S' && $scope.event_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex){
                       checkout_message += '"Select Card"<br>'; 
                    }
                    if(($scope.pos_user_type === 'S' || $scope.pos_user_type === 'P') && $scope.event_payment_method ==='CH' && !$scope.event_check_number){  
                        checkout_message += '"Check Number"<br>';
                    }
                    if($scope.shownewcard && !$scope.valid_stripe_card && $scope.event_payment_method === 'CC'){
                        checkout_message += '"Invalid card details"<br>';
                    }
                }
            }
            if (!$scope.event_waiver_checked && (($scope.final_payment_amount === 0 && !$scope.recurring_payment_show) || $scope.event_payment_method !== 'CC' ) ) {
                checkout_message += "Click the check box to accept Waiver & Agreement<br>";
            }
            if (!$scope.event_cc_chkbx && $scope.event_payment_method === 'CC' && ($scope.final_payment_amount > 0 || ($scope.final_payment_amount === 0 && $scope.recurring_payment_show))) {
                checkout_message += '"Click the check box to accept the Credit card agreement"<br>';
            }
            if (checkout_message !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }else{
                return true;
            }
        };
        
        $scope.stripeSubmitForm = function(){   
            if($scope.eventStripeValidation()){ // Stripe checkout validation
                $scope.payment_method_id = "";
                $scope.stripe_variable.createPaymentMethod('card', $scope.stripe_card).then(function (result) {
                    if (result.error) {
                        setTimeout(function () {
                            $scope.preloader = false;
                            $scope.$apply();
                        }, 1000);
                        // Inform the user if there was an error. 
                        MyApp.alert(result.error.message, '');
                        $scope.valid_stripe_card = false;
                    } else {
                        // Send paymentMethod.id to server 
                        (result.paymentMethod.id)? $scope.payment_method_id = result.paymentMethod.id : $scope.payment_method_id = "";
                        $scope.preloader = true;
                        $scope.eventStripeCheckout();
                    }
                });
            }else{
                $scope.preloader = false;
            }
        };
        
         // STRIPE PAYMENT METHOD ID CALL
        $scope.getStripeElements = function () {
            // Create a Stripe client
            $scope.valid_stripe_card = false;
            // Create an instance of Elements
            var elements = $scope.stripe_variable.elements();

            // Custom styling can be passed to options when creating an Element.
            var style = {
                base: {
                    color: 'rgb(74,74,74)',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: ($scope.mobiledevice)? '14px':'16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element
            $scope.stripe_card = elements.create('card', {style: style});
            // Add an instance of the card Element into the `card-element` <div>
            $scope.stripe_card.mount('#stripe-card-element');
            
            // Create a Stripe client
            $scope.valid_stripe_card = false;
            // Handle real-time validation errors from the card Element.
            $scope.stripe_card.addEventListener('change', function (event) {
                var displayError = document.getElementById('stripe-card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                    $scope.valid_stripe_card = false;
                } else {
                    displayError.textContent = '';
                    $scope.valid_stripe_card = true;
                }
            });
        };       
        
        // Stripe checkout
        $scope.eventStripeCheckout = function () {

            if ($scope.eventStripeValidation()) { // Validate stripe form

                if ($scope.event_payment_method === 'CH') {
                    $scope.processing_fee_value = 0;
                } else if ($scope.event_payment_method === 'CA') {
                    $scope.event_check_number = '';
                    $scope.processing_fee_value = 0;
                } else {
                    $scope.event_check_number = '';
                }
                if ($scope.final_payment_type === 'onetimesingle') {
                    $scope.final_multiple_event_array = [];
                    $scope.final_pay = [];
                } else if ($scope.final_payment_type === 'singlerecurring') {
                    $scope.final_multiple_event_array = [];
                    $scope.final_pay = $scope.pay;
                } else if ($scope.final_payment_type === 'onetimemultiple') {
                    $scope.final_multiple_event_array = $scope.event_array;
                    $scope.final_pay = [];
                } else if ($scope.final_payment_type === 'multiplerecurring') {
                    $scope.final_multiple_event_array = $scope.event_array;
                    $scope.final_pay = $scope.pay;
                }

                var curr_date = new Date();
                var reg_current_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);

                var discount_code = '';
                $scope.payment_amount = (Math.round(($scope.po_total_amount + 0.00001) * 100) / 100).toFixed(2);
                if ($scope.payment_event_type === 'S') {
                    $scope.discount_value = $scope.sdiscount_value;
                    $scope.event_cost = $scope.sevent_cost;
                    $scope.event_title = $scope.sevent_title;
                    $scope.event_id = $scope.sevent_id;
                    discount_code = $scope.sdiscount_code_value;
                } else if ($scope.payment_event_type === 'M') {
                    $scope.discount_value = $scope.mdiscount_value;
                    $scope.event_cost = $scope.po_total_amount;
                    $scope.event_title = $scope.mevent_title;
                    $scope.event_id = $scope.mevent_id;
                    discount_code = $scope.mdiscount_code_value;
                }
                // Intent method type for card payment
                if ($scope.recurring_payment_show && ($scope.final_payment_amount === 0)) {
                    $scope.stripe_intent_method_type = 'SI';
                } else {
                    $scope.stripe_intent_method_type = "PI";
                }
                $scope.preloader = true;
                $http({
                    method: 'POST',
                    url: urlservice.url + 'stripeEventCheckout',
                    data: {
                        companyid: $scope.company_id,
                        event_id: $scope.event_id,
                        event_name: $scope.event_title,
                        desc: $scope.event_title,
                        buyer_first_name: $scope.firstname,
                        buyer_last_name: $scope.lastname,
                        email: $scope.email,
                        phone: $scope.phone,
                        postal_code: $scope.zip_code,
                        registration_amount: $scope.event_cost,
                        discount: $scope.discount_value,
                        payment_amount: $scope.payment_amount,
                        quantity: $scope.total_quantity,
                        processing_fee_type: $scope.processing_fee_type,
                        fee: $scope.processing_fee_value,
                        payment_frequency: $scope.total_payment_frequency,
                        reg_col1: $scope.reg_col_field_name[0],
                        reg_col2: $scope.reg_col_field_name[1],
                        reg_col3: $scope.reg_col_field_name[2],
                        reg_col4: $scope.reg_col_field_name[3],
                        reg_col5: $scope.reg_col_field_name[4],
                        reg_col6: $scope.reg_col_field_name[5],
                        reg_col7: $scope.reg_col_field_name[6],
                        reg_col8: $scope.reg_col_field_name[7],
                        reg_col9: $scope.reg_col_field_name[8],
                        reg_col10: $scope.reg_col_field_name[9],
                        event_array: $scope.final_multiple_event_array,
                        payment_array: $scope.final_pay,
                        event_type: $scope.payment_event_type,
                        payment_type: $scope.final_payment_type,
                        upgrade_status: $scope.company_status,
                        total_order_amount: $scope.total_order_amount,
                        total_order_quantity: $scope.total_order_quantity,
                        discount_code: discount_code,
                        registration_date: reg_current_date, //kamal,
                        pos_email: $localStorage.pos_user_email_id,
                        payment_method: $scope.event_payment_method, //CC - creadit card,CH - check, CA- Cash
                        check_number: $scope.event_check_number,
                        event_optional_discount: $scope.event_optional_discount, //event optional discount
                        event_optional_discount_flag: $scope.event_optional_discount_applied_flag,
                        reg_type_user: $scope.pos_user_type === 'S' ? 'SP' : ($scope.pos_user_type === 'P' ? 'PP' : 'U'),
                        payment_start_date: $scope.total_payment_startdate,
                        total_no_of_payments: $scope.total_number_of_payments,
                        token: $scope.pos_token,
                        payment_method_id: $scope.payment_method_id,
                        intent_method_type: $scope.stripe_intent_method_type, // SI OR PI
                        cc_type: ($scope.shownewcard) ? 'N' : 'E',
                        stripe_customer_id: $scope.stripe_customer_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.onetimecheckout = 0;
                        MyApp.closeModal();
                        MyApp.alert(response.data.msg, 'Message');
                        $scope.resetCartAfterPaymentSucceed();
                        $scope.preloader = false;
                        $scope.$apply();
                    } else {
                        $scope.onetimecheckout = 0;
                        if (response.data.upgrade_status && response.data.studio_expiry_level) {
                            $scope.cmpny_email = response.data.studio_email;
                            $scope.company_status = response.data.upgrade_status;
                            $scope.company_expiry_level = response.data.studio_expiry_level;
                        }
                        if (response.data.error) {
                            $scope.preloader = false;
                            $scope.$apply();
                            MyApp.closeModal();
                            MyApp.alert(response.data.error + ' ' + response.data.error_description, 'Message');
                        } else if (($scope.company_status && $scope.company_status === 'F') || ($scope.company_expiry_level && $scope.company_expiry_level === 'L2')) {
                            MyApp.closeModal();
                            // PREVENT FURTHUR EXECUTION & BLOCK THE APP
                            MyApp.modal({
                                title: '',
                                afterText:
                                        '<br>' +
                                        '<div id="studioExpiryText" class="modal-text custom-modal-content">Please contact the business operator to activate this link.</div>' +
                                        '<br>' +
                                        '<button class="button custom-modal-green-button button-text modalresetButton" id="send_expiry_email"></button>'
                            });

                            if ($scope.cmpny_email) {
                                $("#send_expiry_email").text("Send email message");
                            } else {
                                $("#send_expiry_email").text("Ok");
                            }

                            $('#send_expiry_email').click(function () {
                                $scope.$apply(function () {
                                    if ($("#send_expiry_email").text() === 'Send email message') {
                                        $scope.sendExpiryEmailToStudio($scope.company_id);
                                    }
                                    MyApp.closeModal();
                                });
                            });

                        } else {
                            MyApp.closeModal();
                            if (response.data.session_status === "Expired") {
                                MyApp.alert('Session Expired', '', function () {
                                    $localStorage.pos_user_email_id = "";
                                    $scope.backToPOS();
                                });
                            } else {
                                MyApp.alert(response.data.msg, 'Message');
                            }
                            $scope.preloader = false;
                            $scope.preloaderVisible = false;
                            $scope.$apply();
                        }
                    }
                }, function (response) {
                    $scope.onetimecheckout = 0;
                    $scope.preloaderVisible = false;
                    $scope.preloader = false;
                    $scope.$apply();
                    MyApp.alert('Connection failed', 'Invalid Server Response');
                });
            } else {
                $scope.preloader = false;
            }
        };
        // Stripe checkout

        $scope.checkout = function (credit_card_id, state, user_first_name, user_last_name, email, phone, postal_code) {
            $scope.preloader = true;
            if (!user_first_name) {
                user_first_name = $scope.firstname;
            }
            if (!user_last_name) {
                user_last_name = $scope.lastname;
            }
            var checkout_message = '';
            for (var i = 0; i < $scope.reg_col_names.length; i++) {
                if ($scope.reg_col_field_name[i] && $scope.reg_col_field_name[i] !== undefined && $scope.reg_col_field_name[i] !== '') {

                } else {
                    if ($scope.reg_col_names[i].reg_col_mandatory === 'Y') {
                        checkout_message += '"' + $scope.reg_col_names[i].reg_col_name + '"';
                        checkout_message += '<br>';
                    }
                    $scope.reg_col_field_name[i] = '';
                }
            }
            if (checkout_message !== '') {
                checkout_message = 'Participant Info:' + checkout_message;
            }
            if ($scope.firstname === '' || $scope.firstname === undefined || $scope.lastname === '' || $scope.lastname === undefined || $scope.email === '' || $scope.email === undefined || $scope.phone === '' || $scope.phone === undefined || $scope.zip_code === '' || $scope.zip_code === undefined)
            {
                checkout_message += "Buyer's Info:";
                if ($scope.firstname === '' || $scope.firstname === undefined) {
                    checkout_message += '"First Name"<br>';
                }
                if ($scope.lastname === '' || $scope.lastname === undefined) {
                    checkout_message += '"Last Name"<br>';
                }
                if ($scope.email === '' || $scope.email === undefined) {
                    checkout_message += '"Email"<br>';
                }
                if ($scope.phone === '' || $scope.phone === undefined) {
                    checkout_message += '"Cell Phone"<br>';
                }
                if ($scope.zip_code === '' || $scope.zip_code === undefined) {
                    checkout_message += '"Zip Code"<br>';
                }
            }
            if($scope.pos_user_type === 'S' && $scope.event_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex && $scope.final_payment_amount > 0){
               checkout_message += '"Select Card"<br>'; 
            }
            if($scope.event_payment_method ==='CH' && !$scope.event_check_number){  
                checkout_message += '"Check Number"<br>';
            }
            if (!$scope.event_waiver_checked) {
                checkout_message += "Click the check box to accept Waiver & Agreement<br>";
            }
            if (checkout_message !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }
            if($scope.event_payment_method ==='CH'){                
                credit_card_id = '';
                state = '';
                $scope.processing_fee_value = 0;
            }else if($scope.event_payment_method ==='CA'){
                credit_card_id = '';
                state = '';
                $scope.event_check_number = '';
                $scope.processing_fee_value = 0;
            }else{
                $scope.event_check_number = '';
            }           
            
            if ($scope.final_payment_type === 'onetimesingle') {       
               $scope.final_multiple_event_array = []; 
               $scope.final_pay = [];
            }else if ($scope.final_payment_type === 'singlerecurring') {
                $scope.final_multiple_event_array = []; 
                $scope.final_pay = $scope.pay ;
            }else if ($scope.final_payment_type === 'onetimemultiple') {
                $scope.final_multiple_event_array = $scope.event_array; 
                $scope.final_pay = [];
            }else if ($scope.final_payment_type === 'multiplerecurring') {
                $scope.final_multiple_event_array = $scope.event_array; 
                $scope.final_pay = $scope.pay;
            }

            var curr_date = new Date();
            var reg_current_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);

            var discount_code = '';
            $scope.payment_amount = (Math.round(($scope.po_total_amount + 0.00001) * 100) / 100).toFixed(2);
            if ($scope.payment_event_type === 'S') {
                $scope.discount_value = $scope.sdiscount_value;
                $scope.event_cost = $scope.sevent_cost;
                $scope.event_title = $scope.sevent_title;
                $scope.event_id = $scope.sevent_id;
                discount_code = $scope.sdiscount_code_value;
            } else if ($scope.payment_event_type === 'M') {
                $scope.discount_value = $scope.mdiscount_value;
                $scope.event_cost = $scope.po_total_amount;
                $scope.event_title = $scope.mevent_title;
                $scope.event_id = $scope.mevent_id;
                discount_code = $scope.mdiscount_code_value;
            }

            $http({
                method: 'POST',
                url: urlservice.url + 'wepaycheckout',
                data: {
                    companyid: $scope.company_id,
                    event_id: $scope.event_id,
                    event_name: $scope.event_title,
                    desc: $scope.event_title,
                    buyer_first_name: user_first_name,
                    buyer_last_name: user_last_name,
                    email: email,
                    phone: phone,
                    postal_code: postal_code,
                    registration_amount: $scope.event_cost,
                    discount: $scope.discount_value,
                    payment_amount: $scope.payment_amount,
                    quantity: $scope.total_quantity,
                    processing_fee_type: $scope.processing_fee_type,
                    fee: $scope.processing_fee_value,
                    payment_frequency: $scope.total_payment_frequency,
                    reg_col1: $scope.reg_col_field_name[0],
                    reg_col2: $scope.reg_col_field_name[1],
                    reg_col3: $scope.reg_col_field_name[2],
                    reg_col4: $scope.reg_col_field_name[3],
                    reg_col5: $scope.reg_col_field_name[4],
                    reg_col6: $scope.reg_col_field_name[5],
                    reg_col7: $scope.reg_col_field_name[6],
                    reg_col8: $scope.reg_col_field_name[7],
                    reg_col9: $scope.reg_col_field_name[8],
                    reg_col10: $scope.reg_col_field_name[9],
                    cc_id: credit_card_id,
                    cc_state: state,
                    event_array: $scope.final_multiple_event_array,
                    payment_array: $scope.final_pay,
                    event_type: $scope.payment_event_type,
                    payment_type: $scope.final_payment_type,
                    upgrade_status: $scope.company_status,
                    total_order_amount: $scope.total_order_amount,
                    total_order_quantity: $scope.total_order_quantity,
                    discount_code: discount_code,
                    registration_date: reg_current_date, //kamal,
                    pos_email: $localStorage.pos_user_email_id,
                    payment_method :$scope.event_payment_method, //CC - creadit card,CH - check, CA- Cash
                    check_number:$scope.event_check_number,
                    event_optional_discount : $scope.event_optional_discount, //event optional discount
                    event_optional_discount_flag:$scope.event_optional_discount_applied_flag,
                    reg_type_user: $scope.pos_user_type === 'S' ? 'SP' : ($scope.pos_user_type === 'P' ? 'PP' : 'U'),
                    payment_start_date:$scope.total_payment_startdate,
                    total_no_of_payments:$scope.total_number_of_payments,
                    token: $scope.pos_token,
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.onetimecheckout = 0;
                    MyApp.closeModal();
                    MyApp.alert(response.data.msg, 'Message');
                    $scope.resetCartAfterPaymentSucceed();
                    $scope.preloader = false;
                    $scope.$apply();
                } else {
                    $scope.onetimecheckout = 0;
                    if (response.data.upgrade_status && response.data.studio_expiry_level) {
                        $scope.cmpny_email = response.data.studio_email;
                        $scope.company_status = response.data.upgrade_status;
                        $scope.company_expiry_level = response.data.studio_expiry_level;
                    }
                    if (response.data.error) {
                        $scope.preloader = false;
                        $scope.$apply();
                        MyApp.closeModal();
                        MyApp.alert(response.data.error + ' ' + response.data.error_description, 'Message');
                    } else if (($scope.company_status && $scope.company_status === 'F') || ($scope.company_expiry_level && $scope.company_expiry_level === 'L2')) {
                        MyApp.closeModal();
                        // PREVENT FURTHUR EXECUTION & BLOCK THE APP
                        MyApp.modal({
                            title: '',
                            afterText:
                                    '<br>' +
                                    '<div id="studioExpiryText" class="modal-text custom-modal-content">Please contact the business operator to activate this link.</div>' +
                                    '<br>' +
                                    '<button class="button custom-modal-green-button button-text modalresetButton" id="send_expiry_email"></button>'
                        });

                        if ($scope.cmpny_email) {
                            $("#send_expiry_email").text("Send email message");
                        } else {
                            $("#send_expiry_email").text("Ok");
                        }

                        $('#send_expiry_email').click(function () {
                            $scope.$apply(function () {
                                if ($("#send_expiry_email").text() === 'Send email message') {
                                    $scope.sendExpiryEmailToStudio($scope.company_id);
                                }
                                MyApp.closeModal();
                            });
                        });

                    } else {
                        MyApp.closeModal();
                        if(response.data.session_status === "Expired"){
                            MyApp.alert('Session Expired','', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        }else{
                            MyApp.alert(response.data.msg, 'Message');
                        }
                        $scope.preloader = false;
                        $scope.preloaderVisible = false;
                        $scope.$apply();
                    }
                }
            }, function (response) {
                $scope.onetimecheckout = 0;
                $scope.preloaderVisible = false;
                $scope.preloader = false;
                $scope.$apply();
                MyApp.alert('Connection failed', 'Invalid Server Response');
            });
        };
        
        // GET WEPAY STATUS
        $scope.getWepayStatus = function (cmp_id) {
            $http({
                method: 'GET',
                url: urlservice.url + 'getwepaystatus',
                params: {
                    "companyid": cmp_id,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.wepaystatus = response.data.msg;
                    if($scope.wepaystatus !== 'N'){
                        $scope.wepay_enabled = true;
                    }                    
                } else {
                    $scope.wepaystatus = 'N';
                    $scope.wepay_enabled = false;
                }
                $scope.preloader = false;
                $scope.preloaderVisible = false;
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
                $scope.preloader = false;
                $scope.preloaderVisible = false;
            });
        };
        
        $scope.sendExpiryEmailToStudio = function(cmp_id){
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            $http({
                method: 'POST',
                url: urlservice.url + 'sendExpiryEmailToStudio',
                data: {
                    "companyid": cmp_id,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status == 'Success') {
                    MyApp.alert(response.data.msg, '');
                } else {
                    MyApp.alert(response.data.msg, 'Failure');
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };

        //GET Events list
        $scope.getEventsList = function (cmp_id,evnt_id)
        {       
                $scope.fav_company_name = $scope.fav_event_name = '';
                $scope.company_id_status = $scope.event_id_status = $scope.child_id_status = false;
                $scope.preloader = true;
                $scope.preloaderVisible = true;
                $http({
                    method: 'GET',
                    url: urlservice.url + 'events',
                    params: {
                        "companyid": cmp_id,
                        "event_id": evnt_id,
                        "pos_email": $localStorage.pos_user_email_id,
                        "token": $scope.pos_token,
                        "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U',
                        "page_from": "E"
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then(function (response) {
                    
                    if (response.data.status === 'Success') {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                    $scope.dbcontent = "";
                    $scope.company_id = cmp_id;
                    $scope.dbeventno = response.data.status;
                    $scope.logoUrl = response.data.company_details.logo_URL;
                    $scope.wp_level = response.data.company_details.wp_level;
                    $scope.wp_client_id = response.data.company_details.wp_client_id;
                    $scope.company_status = response.data.company_details.upgrade_status;
                    $scope.processing_fee_percentage = response.data.company_details.processing_fee_percentage;
                    $scope.processing_fee_transaction = response.data.company_details.processing_fee_transaction;
                    $scope.fav_company_name = response.data.company_details.company_name;
                    $scope.wp_currency_symbol = response.data.company_details.wp_currency_symbol;
                    $scope.wp_currency_code = response.data.company_details.wp_currency_code;
                    $scope.pos_settings_payment_method = response.data.company_details.pos_event_payment_method;
                    //stripe set
                    $scope.stripe_upgrade_status = response.data.company_details.stripe_upgrade_status;
                    $scope.stripe_subscription = response.data.company_details.stripe_subscription;
                    $scope.stripe_publish_key = response.data.company_details.stripe_publish_key;
                    $scope.stripe_country_support = response.data.company_details.stripe_country_support;
                    $scope.getStripeStatus(cmp_id);

                    if ($scope.pos_user_type === 'S') {
                        $scope.getstudentlist(cmp_id);
                    }
                    $scope.childcartcontents = [];                    
                    $scope.initial_load_payment();

                    if (cmp_id !== '' && evnt_id === '') {
                        $scope.dbcontent = response.data.msg;
                        $scope.dynamic_event_id = '';
                        $scope.company_id_status = true;
                        $scope.event_id_status = false;
                        $scope.child_id_status = false;
                        $scope.fav_event_name = '';
                        // Load page:                                       
                        mainView.router.load({
                            pageName: 'index'
                        });

                    } else {
                        $scope.company_id_status = true;
                        $scope.event_id_status = true;
                        $scope.child_id_status = false;
                        $scope.dynamic_event_id = evnt_id;
                        $scope.dbcontent = response.data.msg;
                        if ($scope.dbcontent.event_type === 'S') {
                            $scope.singleeventdescription('', response.data.msg);
                        } else if ($scope.dbcontent.event_type === 'M') {
                            $scope.eventdetail('', response.data.msg);
                        } else if ($scope.dbcontent.event_type === 'C') {
                            $scope.child_id_status = true;
                            $scope.mevent_discount_array = [];
                            $scope.reg_col_names = "";
                            $scope.reg_col_names = response.data.msg.reg_columns;
                            $scope.mevent_discount_array = response.data.msg.discount;
                            $scope.childeventdetails(response.data.msg);
                            $scope.eventchilddescription('', response.data.msg);
                        }
                    }

                } else {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                    $scope.dbcontent = "";
                    $scope.dbeventno = response.data.status;
                    if (response.data.upgrade_status && response.data.studio_expiry_level) {
                        $scope.cmpny_email = response.data.studio_email;
                        $scope.company_status = response.data.upgrade_status;
                        $scope.company_expiry_level = response.data.studio_expiry_level;
                    }

                    if (($scope.company_status && $scope.company_status === 'F') || ($scope.company_expiry_level && $scope.company_expiry_level === 'L2')) {
                        // PREVENT FURTHUR EXECUTION & BLOCK THE APP
                        MyApp.modal({
                            title: '',
                            afterText:
                                    '<br>' +
                                    '<div id="studioExpiryText" class="modal-text custom-modal-content">Please contact the business operator to activate this link.</div>' +
                                    '<br>' +
                                    '<button class="button custom-modal-green-button button-text modalresetButton" id="send_expiry_email"></button>'
                        });

                        if ($scope.cmpny_email) {
                            $("#send_expiry_email").text("Send email message");
                        } else {
                            $("#send_expiry_email").text("Ok");
                        }

                        $('#send_expiry_email ').click(function () {
                            $scope.$apply(function () {
                                if ($("#send_expiry_email").text() === 'Send email message') {
                                    $scope.sendExpiryEmailToStudio(cmp_id);
                                }
                                MyApp.closeModal();
                            });
                        });

                    } else {
                            if(response.data.session_status === "Expired"){
                                MyApp.alert('Session Expired','', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                            }else{
                            MyApp.alert(response.data.msg, 'Message');
                        }
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };

        $scope.getstudentlist = function(companyid){
            $scope.preloader = false;
            $scope.preloaderVisible = false;
            $http({
                method: 'GET',
                url: openurlservice.url + 'getstudent',
                params: {
                    "companyid": companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status === 'Success') {
                    $scope.studentlist = response.data.msg.student_details;
                    for(var i=0;i<$scope.studentlist.length;i++){
                        $scope.student_name.push($scope.studentlist[i].student_name);
                    }

                    var autocompleteDropdownSimple  =  MyApp.autocomplete ({
                        input: '#autocomplete-dropdown',
                        openIn: 'dropdown',

                        source: function (autocomplete, query, render) {
                           var results  =  [];
                            if (query.length === 0) {
                              render(results);
                              return;
                           }

                           // You can find matched items
                           for (var i = 0; i < $scope.student_name.length; i++) {
                              if ($scope.student_name[i].toLowerCase().indexOf(query.toLowerCase()) >= 0) 
                                results.push($scope.student_name[i]);
                           }
                           // Display the items by passing array with result items
                           render(results);
                        }
                     });
                } else {
                    if(response.data.session_status === "Expired"){
                        $scope.preloader = false;
                        $scope.preloaderVisible = false;
                        MyApp.alert('Session Expired','', function () {
                            $localStorage.pos_user_email_id = "";
                            $scope.backToPOS();
                        });
                    }else{
//                        MyApp.alert(response.data.msg, 'Message');
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert(response.data.msg, 'Failure');
            }); 
        };
        
      $scope.urlredirection = function () {
           $scope.param1 = $scope.param2 = $scope.param3 = $scope.param4 = $scope.access_token = '';
           var decoded_url = decodeURIComponent(window.location.href);
           var params = decoded_url.split('?=')[1].split('/');
            $scope.param1 = params[0];
            $scope.param2 = params[1];
            $scope.param3 = params[2];
            $scope.param4 = params[3];
            if(params[5]){
                if(params[5] === 'spos'){ //If staff
                    $scope.pos_user_type = 'S';
                    // ENABLE ADMIN BANNER, SEARCH MEMBER, OPTIONAL DISCOUNTS
                    $('#view-1').addClass('with-banner');
                    $('#view-1').addClass('from-pos with-footer');
                }else if(params[5] === 'ppos'){
                    $scope.pos_user_type = 'P'; //If public
                    $('#view-1').addClass('from-pos with-footer');
                }else{
                    $scope.pos_user_type = 'O'; 
                }
                if(params[6] && $scope.pos_user_type !== 'O'){ //For access token
                    $scope.pos_token = params[6];
                }else{
                    $scope.pos_token = 0;
                }
                if(params[7] && $scope.pos_user_type !== 'O'){
                   $scope.pos_entry_type = params[7];
                }else{
                   $scope.pos_entry_type = "";
                }
            }else{
                $scope.pos_user_type = 'O'; // If open URL
                $scope.pos_token = $scope.pos_entry_type = '';
            }
            
//            alert('param1: '+$scope.param1+' param2: '+$scope.param2+' param3: '+$scope.param3+' param4: '+$scope.param4+' param5: '+params[5]+' access_token: '+$scope.access_token);
            
            if ($scope.param1 !== undefined && $scope.param2 !== undefined && ($scope.param3 === undefined || $scope.param3 ==='') && $scope.param4 === undefined) {
                $scope.getEventsList($scope.param2, '');  
            } else if ($scope.param1 !== undefined && $scope.param2 !== undefined && $scope.param3 !== undefined && ($scope.param4 === undefined || $scope.param4 ==='')) {
                if(isNaN($scope.param3)) {
                    $scope.getEventsList($scope.param2, ''); 
                }else{
                    $scope.getEventsList($scope.param2, $scope.param3);         // Is a number
                }                
            } else if ($scope.param1 !== undefined && $scope.param2 !== undefined && $scope.param3 !== undefined && $scope.param4 !== undefined) {
                $scope.getEventsList($scope.param2, $scope.param4);
            }else{
                window.location.href = window.location.origin;
            }
        
        };
      
      
        $scope.singleeventdescription = function (ind,details) {
            $scope.single_total_amount = 0;
            $scope.single_payment_amount = 0;
            $scope.payment_amount = 0;
            $scope.total_quantity = 0;
            $scope.event_type = 'S';
            $scope.sevent_quantity = 1;
            $scope.sdiscount_value  = 0;
            $scope.sdiscount_code_value  = '';
            $scope.sevent_discount_array = [];
            $scope.sdiscount_code_display = true;
            $scope.discount_applied = false;
            if(ind !== ''){
                $scope.singleEventDetails = $scope.dbcontent[ind];
            } else{
                $scope.singleEventDetails = details;
                // Load page:                                       
                mainView.router.load({
                    pageName: 'evntdesc'
                });
            }
            $scope.sevent_id = $scope.singleEventDetails.event_id;
            $scope.sevent_cost = $scope.singleEventDetails.event_cost;
            $scope.reg_col_names = $scope.singleEventDetails.reg_columns;
            
            if($scope.singleEventDetails.waiver_policies === null || $scope.singleEventDetails.waiver_policies === undefined || $scope.singleEventDetails.waiver_policies === '' ){ 
               $scope.waiver_text = '';
            } else{
               $scope.waiver_text = $scope.singleEventDetails.waiver_policies;
            }
            $scope.fav_event_name = '- '+$scope.singleEventDetails.event_title;
            $scope.sevent_title = $scope.singleEventDetails.event_title;
            $scope.sevent_desc = $scope.singleEventDetails.event_desc;
            $scope.sevent_banner_img_url = $scope.singleEventDetails.event_banner_img_url;
            $scope.sevent_more_detail_url = $scope.singleEventDetails.event_more_detail_url;
            $scope.sevent_video_detail_url = $scope.singleEventDetails.event_video_detail_url;
            $scope.sevent_begin_dt = $scope.singleEventDetails.event_begin_dt;
            $scope.sevent_end_dt = $scope.singleEventDetails.event_end_dt;
            $scope.processing_fee_value = 0;
            $scope.onetime_payment_flag = $scope.singleEventDetails.event_onetime_payment_flag;
            $scope.recurring_payment_flag = $scope.singleEventDetails.event_recurring_payment_flag;
            $scope.total_order_amount = $scope.singleEventDetails.total_order_text;
            $scope.total_order_quantity = $scope.singleEventDetails.total_quantity_text;
            $scope.total_deposit_amount = $scope.singleEventDetails.deposit_amount;
            $scope.total_number_of_payments = $scope.singleEventDetails.number_of_payments;
            $scope.total_payment_startdate_type = $scope.singleEventDetails.payment_startdate_type;
            $scope.processing_fee_type = $scope.singleEventDetails.processing_fees;
            if($scope.total_payment_startdate_type === 4 || $scope.total_payment_startdate_type === '4'){
                $scope.total_payment_startdate = $scope.singleEventDetails.payment_startdate;
            }else{
                var curr_date = new Date();
                var curr_server_date = curr_date.toISOString().split('T')[0];
                var newDate = curr_server_date.split('-');
                var given_start_year = newDate[0];
                var given_start_month = newDate[1]-1;
                var given_start_day = newDate[2];
                var dup_date = new Date(given_start_year, given_start_month, given_start_day);
                if($scope.total_payment_startdate_type === '1'){
                        dup_date.setMonth(dup_date.getMonth()+1);
                        var dup = dup_date;   
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2); 
                    }else if($scope.total_payment_startdate_type === '2'){
                        dup_date.setDate(dup_date.getDate() + 7);
                        var dup = dup_date;   
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2); 
                    }else if($scope.total_payment_startdate_type === '3'){
                        dup_date.setDate(dup_date.getDate() + 14);
                        var dup = dup_date;   
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2); 
                    }
            }
            $scope.total_payment_frequency = $scope.singleEventDetails.payment_frequency;
            $scope.sevent_capacity = $scope.singleEventDetails.event_capacity;
            $scope.sregistrations_count = $scope.singleEventDetails.registrations_count;
            $scope.scapacity_text = $scope.singleEventDetails.capacity_text;
            $scope.getTotal();
            
            for (var i = 0; i < $scope.singleEventDetails.discount.length; i++) {
                $scope.sevent_discount_array.push($scope.singleEventDetails.discount[i]);
            }
            
        };

        $$('#sdiscountcode').on('click', function () {
            MyApp.prompt('Enter Discount code', 'Discount', function (value) {
                $scope.singlediscountCheck(value);
            });
        });

        $$('#sdiscountval').on('click', function () {
            MyApp.prompt('Enter Discount code', 'Discount', function (value) {
                $scope.singlediscountCheck(value);
            });
        });
        
        $scope.singlediscountCheck = function (value) {
            if($scope.sevent_quantity !== '' && $scope.sevent_quantity !== 0 ||  $scope.sevent_quantity !== undefined ){
                var temp_check_for_disc_available = 0;
                for (var j = 0; j < $scope.sevent_discount_array.length; j++) {
                    if ($scope.sevent_discount_array[j]['discount_code'].toUpperCase() === value.toUpperCase()) {
                        temp_check_for_disc_available = 1;
                        $scope.sdiscount_code_display = false;
                        $scope.sdiscount_code_value = value.toUpperCase();
                        var disc_val = $scope.sevent_discount_array[j]['discount_amount'];
                        var sdiscount_type = $scope.sevent_discount_array[j]['discount_type'];
                        if (sdiscount_type === 'V') {
                            if (disc_val > 0) {
                                $scope.sdiscount_value = disc_val;
                                $scope.discount_applied = true;
                            } else {
                                $scope.sdiscount_value = 0;
                                $scope.discount_applied = false;
                            }
                        } else {
                            if (disc_val > 0) {
                                $scope.sdiscount_value = $scope.singleEventDetails.event_cost * disc_val / 100;
                                $scope.discount_applied = true;
                            } else {
                                $scope.sdiscount_value = 0;
                                $scope.discount_applied = false;
                            }
                        }
                        $scope.getTotal();
                        $scope.$apply();
                    }
                }

                if (temp_check_for_disc_available === 0 && value !== '')  {
                    $scope.sdiscount_code_value = "";
                    $scope.sdiscount_code_display = true;
                    MyApp.alert('Your Discount code "' + value + '" is invalid','');
                    $scope.sdiscount_value = 0;
                    $scope.discount_applied = false;
                    $scope.getTotal();
                    $scope.$apply();
                }
            }
        };
        
        $scope.updateSingleTotalAmount = function(){
            $scope.getTotal();
        };
                                                 
        $scope.processPaymentAmount = function(){
            if($scope.event_type === 'S'){
                $scope.getTotal();
            }else{
                $scope.getMultipleTotal();
            }
        };
        $scope.getremainingstatus = function(event_capacity,capacity_text,registrations_count){
            if(event_capacity > 0){
                if(capacity_text === '2'){
                    if((event_capacity - registrations_count) === 1){
                        return '1 spot left';
                    }else if((event_capacity - registrations_count) < 1){
                        return 'Sold Out';
                    }else{
                        return (event_capacity - registrations_count)+' spots left';
                    }
                }else if(capacity_text === '3'){
                    if((event_capacity - registrations_count) === 1){
                        return '1 spot left';
                    }else if(event_capacity - registrations_count < 1){
                        return 'Sold Out';
                    }else{
                        return Math.ceil((event_capacity - registrations_count)/2)+' spots left';
                    }
                }else if(capacity_text === '4'){
                    if((event_capacity - registrations_count) < 1){
                        return 'Sold Out';
                    }else{
                   return '<span style="font-size:12px;">Limited space remaining</span>';
                    }
                }else{
                    if((event_capacity - registrations_count) < 1){
                        return 'Sold Out';
                    }else{
                    return ' ';
                    } 
                }
            }else{
                return '  ';
            }
        }
        

        $scope.availablecapacity = function (eventtype, event_capacity, registrations_count) {
            //    alert(event_capacity+" "+capacity_text+" "+registrations_count)
            if (event_capacity > 0) {
                if ((event_capacity - registrations_count) < 1) {
                    MyApp.alert('Event is sold out', ' ');
                } else {
                    if (eventtype === 'S') {
                        // LOAD PAGE:
                        mainView.router.loadPage({
                            pageName: 'subcart'
                        });
                    } else {
                        // LOAD PAGE:
                        mainView.router.loadPage({
                            pageName: 'childsubcart'
                        });
                    }
                    return true;
                }

            } else if (event_capacity == 0){
                MyApp.alert('Event is sold out', ' ');
            }else {
                if (eventtype === 'S') {
                    // LOAD PAGE:
                    mainView.router.loadPage({
                        pageName: 'subcart'
                    });
                } else {
                    // LOAD PAGE:
                    mainView.router.loadPage({
                        pageName: 'childsubcart'
                    });
                }
                return true;
            }
        }

        $scope.descProcessingFee = function(damount){
            var dprocess_fee_per = $scope.processing_fee_percentage;
            var dprocess_fee_val = $scope.processing_fee_transaction;
            var recur_process_fee = 0;
            if ($scope.processing_fee_type === 2 || $scope.processing_fee_type === '2') {
                if (parseFloat(damount) > 0) {
                    var process_cost1 = +(parseFloat(damount) + +(((parseFloat(damount) * parseFloat(dprocess_fee_per)) / 100) + +parseFloat(dprocess_fee_val)));
                    var process_cost = +(((parseFloat(process_cost1) * parseFloat(dprocess_fee_per)) / 100) + +parseFloat(dprocess_fee_val));
                    recur_process_fee = parseFloat(Math.round((+process_cost + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    recur_process_fee = 0;
                }
            } else {
                recur_process_fee = 0;
            }
            return recur_process_fee;
        };
        
        $scope.recurringProcessingFee = function(total_amount){
            var process_fee_per = $scope.processing_fee_percentage;
            var process_fee_val = $scope.processing_fee_transaction;
            var recur_process_fee = 0;
            
            if($scope.processing_fee_type === 2 || $scope.processing_fee_type === '2') {
                if (parseFloat(total_amount) > 0) {
                    var process_cost1 = +(parseFloat(total_amount) + +(((parseFloat(total_amount) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val)));
                    var process_cost = +(((parseFloat(process_cost1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val));
                    recur_process_fee = parseFloat(Math.round((+process_cost + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    recur_process_fee = 0;
                }
            } else if ($scope.processing_fee_type === 1 || $scope.processing_fee_type === '1') {
               if (parseFloat(total_amount) > 0) {
                    var process_fee = +((parseFloat(total_amount) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                    recur_process_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    recur_process_fee = 0;
                }
            }
            return recur_process_fee;
        };
        
        $scope.getRecurringAccess = function(purchase_amount,deposit_amount,number_of_payments){
           var balance_amount = parseFloat(purchase_amount) - parseFloat(deposit_amount); 
           var recurring_payment_amount = (Math.round(((parseFloat(balance_amount) / parseInt(number_of_payments)) + 0.00001) * 100) / 100).toFixed(2);
           if(parseFloat(recurring_payment_amount)>5){
               return true;
           }else{
               return false;
           }
        };
        
        $scope.getTotal = function(){
            if($scope.sevent_capacity > 0){
                $scope.savail_qty = parseInt($scope.sevent_capacity) - parseInt($scope.sregistrations_count);
                if(parseInt($scope.sevent_quantity) > $scope.savail_qty){
                    $scope.avail_qty_error = true;
                    return;
                }else{
                    $scope.avail_qty_error = false;
                }
            }
            $scope.processing_fee_value = 0;
            $scope.po_total_amount = 0;
            $scope.po_view_total_amount = 0;
            $scope.total_quantity = $scope.sevent_quantity;
            var process_fee_per=0;
            var process_fee_val=0;
            $scope.single_payment_amount = $scope.sevent_cost;
            if($scope.sdiscount_value > 0){
                $scope.single_payment_amount = +$scope.sevent_cost - +$scope.sdiscount_value;
            }
            var amount = $scope.sevent_quantity * $scope.single_payment_amount;
            $scope.po_total_amount = parseFloat(amount);
            $scope.static_po_total_amount = $scope.po_total_amount;
            if($scope.event_payment_method === 'CC' && ($scope.processing_fee_type === 2 || $scope.processing_fee_type === '2')){
                var process_fee_per = $scope.processing_fee_percentage;
                var process_fee_val = $scope.processing_fee_transaction;
                if(amount>0){
                    var process_fee1 = +(parseFloat(amount)) + +((parseFloat(amount) * parseFloat(process_fee_per))/100) + +parseFloat(process_fee_val);
                    var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per))/100) + +parseFloat(process_fee_val);
                    $scope.processing_fee_value = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                }else{
                    $scope.processing_fee_value = 0;
                }
                $scope.single_total_amount = +parseFloat(amount) + +$scope.processing_fee_value;
            }else{
                $scope.single_total_amount = amount;
            }
            $scope.final_payment_amount = $scope.single_total_amount;
            $scope.po_view_total_amount = $scope.single_total_amount;
            $scope.pay_infull_processingfee = $scope.processing_fee_value;
            $scope.static_final_payment_amount = $scope.final_payment_amount; //BACKUP VALUES
            $scope.static_processing_fee_value = $scope.processing_fee_value;
            
            $scope.recurring_payment_show = false;
            var todayDate = new Date();
            var start_date = new Date($scope.total_payment_startdate);
            if(!$scope.total_order_amount){
                $scope.total_order_amount = 0;
            }
            if(!$scope.total_order_quantity){
                $scope.total_order_quantity = 0;
            }
            if ($scope.recurring_payment_flag === 'Y' && (parseFloat($scope.po_total_amount) > parseFloat($scope.total_order_amount)) && (parseInt($scope.total_quantity) > parseInt($scope.total_order_quantity)) && (start_date > todayDate) 
                    && $scope.getRecurringAccess($scope.po_total_amount, $scope.total_deposit_amount, $scope.total_number_of_payments) && parseInt($scope.total_number_of_payments) > 0) {
                $scope.recurring_payment_show = true;
            } else {
                $scope.recurring_payment_show = false;
            } 
            $scope.static_recurring_payment_show = $scope.recurring_payment_show;
        };
        
        $scope.childeventdetails = function(detail){
           
            $scope.ChildEventDetail = detail;
            if($scope.ChildEventDetail.waiver_policies === null || $scope.ChildEventDetail.waiver_policies === undefined || $scope.ChildEventDetail.waiver_policies === '' ){ 
               $scope.waiver_text = '';
            } else{
               $scope.waiver_text = $scope.ChildEventDetail.waiver_policies;
            }
            
            $scope.processing_fee_type = $scope.ChildEventDetail.processing_fees;
            $scope.processing_fee_value = 0;
            $scope.onetime_payment_flag = $scope.ChildEventDetail.event_onetime_payment_flag;
            $scope.recurring_payment_flag = $scope.ChildEventDetail.event_recurring_payment_flag;
            $scope.total_order_amount = $scope.ChildEventDetail.total_order_text;
            $scope.total_order_quantity = $scope.ChildEventDetail.total_quantity_text;
            $scope.total_deposit_amount = $scope.ChildEventDetail.deposit_amount;
            $scope.total_number_of_payments = $scope.ChildEventDetail.number_of_payments;
            $scope.total_payment_startdate_type = $scope.ChildEventDetail.payment_startdate_type;
            if($scope.total_payment_startdate_type === 4 || $scope.total_payment_startdate_type === '4'){
                $scope.total_payment_startdate = $scope.ChildEventDetail.payment_startdate;
            }else{
                var curr_date = new Date();
                var curr_server_date = curr_date.toISOString().split('T')[0];
                var newDate = curr_server_date.split('-');
                var given_start_year = newDate[0];
                var given_start_month = newDate[1]-1;
                var given_start_day = newDate[2];
                var dup_date = new Date(given_start_year, given_start_month, given_start_day);
                if($scope.total_payment_startdate_type === '1'){
                        dup_date.setMonth(dup_date.getMonth()+1);
                        var dup = dup_date;   
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2); 
                    }else if($scope.total_payment_startdate_type === '2'){
                        dup_date.setDate(dup_date.getDate() + 7);
                        var dup = dup_date;   
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2); 
                    }else if($scope.total_payment_startdate_type === '3'){
                        dup_date.setDate(dup_date.getDate() + 14);
                        var dup = dup_date;   
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2); 
                    }
            }
            $scope.total_payment_frequency = $scope.ChildEventDetail.payment_frequency; 
        };
        
        $scope.eventdetail = function (ind,details) {
            $scope.dynamic_parent_index = '';
            $scope.event_type = 'M';
            $scope.multiple_total_amount = 0;
            $scope.childcartcontents = [];
            $scope.mevent_discount_array = [];
            if(ind !== ''){
                $scope.multipleEventDetails = $scope.dbcontent[ind];
                $scope.dynamic_parent_index = ind;                 
            } else{
                $scope.multipleEventDetails = details ;
            }
            mainView.router.load({
                pageName: 'parentevntdesc'
            });
                                
//            alert(JSON.stringify($scope.multipleEventDetails));
            $scope.fav_event_name = '- '+$scope.multipleEventDetails.event_title;
            $scope.reg_col_names = $scope.multipleEventDetails.reg_columns;
            
            if($scope.multipleEventDetails.waiver_policies === null || $scope.multipleEventDetails.waiver_policies === undefined || $scope.multipleEventDetails.waiver_policies === '' ){ 
               $scope.waiver_text = '';
            } else{
               $scope.waiver_text = $scope.multipleEventDetails.waiver_policies;
            }
            
            $scope.processing_fee_type = $scope.multipleEventDetails.processing_fees;
            $scope.processing_fee_value = 0;
            $scope.onetime_payment_flag = $scope.multipleEventDetails.event_onetime_payment_flag;
            $scope.recurring_payment_flag = $scope.multipleEventDetails.event_recurring_payment_flag;
            $scope.total_order_amount = $scope.multipleEventDetails.total_order_text;
            $scope.total_order_quantity = $scope.multipleEventDetails.total_quantity_text;
            $scope.total_deposit_amount = $scope.multipleEventDetails.deposit_amount;
            $scope.total_number_of_payments = $scope.multipleEventDetails.number_of_payments;
            $scope.total_payment_startdate_type = $scope.multipleEventDetails.payment_startdate_type;
            if($scope.total_payment_startdate_type === 4 || $scope.total_payment_startdate_type === '4'){
                $scope.total_payment_startdate = $scope.multipleEventDetails.payment_startdate;
            }else{
                var curr_date = new Date();
                var curr_server_date = curr_date.toISOString().split('T')[0];
                var newDate = curr_server_date.split('-');
                var given_start_year = newDate[0];
                var given_start_month = newDate[1]-1;
                var given_start_day = newDate[2];
                var dup_date = new Date(given_start_year, given_start_month, given_start_day);
                if($scope.total_payment_startdate_type === '1'){
                        dup_date.setMonth(dup_date.getMonth()+1);
                        var dup = dup_date;   
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2); 
                    }else if($scope.total_payment_startdate_type === '2'){
                        dup_date.setDate(dup_date.getDate() + 7);
                        var dup = dup_date;   
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2); 
                    }else if($scope.total_payment_startdate_type === '3'){
                        dup_date.setDate(dup_date.getDate() + 14);
                        var dup = dup_date;   
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2); 
                    }
//                    alert($scope.total_payment_startdate);
            }
            $scope.total_payment_frequency = $scope.multipleEventDetails.payment_frequency;
            $scope.mevent_title = $scope.multipleEventDetails.event_title;
            $scope.mevent_desc = $scope.multipleEventDetails.event_desc;            
            $scope.eventParentBannerImage = $scope.multipleEventDetails.event_banner_img_url;
            $scope.pevent_more_detail_url = $scope.multipleEventDetails.event_more_detail_url;
            $scope.pevent_video_detail_url = $scope.multipleEventDetails.event_video_detail_url;
            $scope.mdiscount_code_value = '';
            $scope.mdiscount_value = 0;
            $scope.discount_applied = false;
            for (var j = 0; j < $scope.multipleEventDetails.discount.length; j++) {
                $scope.mevent_discount_array.push($scope.multipleEventDetails.discount[j]);
            }
            if ($scope.multipleEventDetails.child_events.length > 0) {
                $scope.childeventContentStatus = 'Success';
                $scope.eventChildContent = $scope.multipleEventDetails.child_events;
            } else {
                $scope.childeventContentStatus = 'Failure';
                $scope.eventChildContent = "";
            }
            $scope.$apply();
        }; 

        $scope.eventchilddescription = function (ind, detail) { 
            if(ind !== ''){
               $scope.eventchilddescContent = $scope.eventChildContent[ind];
            }else{
               $scope.eventchilddescContent = detail;
               $scope.mevent_title = $scope.eventchilddescContent.parent_event_title;
               // Load page:                                       
                mainView.router.load({
                    pageName: 'childevntdesc'
                });
            }  
            $scope.mevent_id = $scope.eventchilddescContent.parent_id;
            $scope.cevent_quantity = 1;
            $scope.subcart_flag = 'N';
            $scope.cevent_id = $scope.eventchilddescContent.event_id;
            $scope.eventChildBannerImage = $scope.eventchilddescContent.event_banner_img_url;
            $scope.cevent_cost = $scope.eventchilddescContent.event_cost;
            $scope.cpayment_amount = $scope.eventchilddescContent.event_cost;
            $scope.cevent_more_detail_url = $scope.eventchilddescContent.event_more_detail_url;
            $scope.cevent_video_detail_url = $scope.eventchilddescContent.event_video_detail_url;
            $scope.cevent_capacity = $scope.eventchilddescContent.event_capacity;
            $scope.ccapacity_text = $scope.eventchilddescContent.capacity_text;
            $scope.cregistrations_count = $scope.eventchilddescContent.registrations_count;
            $scope.cavail_qty = parseInt($scope.eventchilddescContent.event_capacity) - parseInt($scope.eventchilddescContent.registrations_count);            
            if ($scope.childcartcontents && $scope.childcartcontents.length > 0) {
                for (var i = 0; i < $scope.childcartcontents.length; i++) {
                    if ($scope.cevent_id === $scope.childcartcontents[i]['detailcontent'].event_id) {
                        $scope.subcart_flag = 'U';
                        $scope.cevent_quantity = $scope.childcartcontents[i]['quantity'];
                    }
                }
            }
            $scope.cdesctotal = $scope.eventchilddescContent.event_cost;
            $scope.$apply();
        };
        $scope.updatechildtotal = function(){
            if($scope.cavail_qty > 0){
            if($scope.cavail_qty < $scope.cevent_quantity){
               $scope.cavail_qty_error = true;
                return;
            }else{
                $scope.cavail_qty_error = false;
            }
        }
            $scope.cdesctotal = parseInt($scope.eventchilddescContent.event_cost) * parseInt($scope.cevent_quantity);
        };
        
        //Add child event to cart
        $scope.childaddtocart = function (event_id, subcart_flag) {
            $scope.cpayment_amount = 0;
            $scope.total_quantity = 0;
            $scope.childcartitems = [];
            if ($scope.childcartcontents && $scope.childcartcontents.length > 0 && $scope.child_id_status) {
                for (var i = 0; i < $scope.childcartcontents.length; i++) {
                    if ($scope.cevent_id === $scope.childcartcontents[i]['detailcontent'].event_id) {
                        $scope.subcart_flag = subcart_flag = 'U';
                    }
                }
            }
            
            if (subcart_flag === 'U') { // U - Update , N - New
                if($scope.childcartcontents && $scope.childcartcontents.length > 0){
                    $scope.discount_applied = false;
                    for (var i = 0; i < $scope.childcartcontents.length; i++) {
                        if (event_id === $scope.childcartcontents[i]['detailcontent'].event_id) {
                            $scope.childcartcontents[i]['detailcontent'] = $scope.eventchilddescContent;
    //                        $scope.childcartcontents[i]['event_id'] = event_id;
                            $scope.childcartcontents[i]['event_cost'] = $scope.eventchilddescContent.event_cost;
                            if($scope.mdiscount_value > 0){
                                $scope.discount_applied = true;
                                if($scope.childcartcontents[i]['disc'] === undefined ){
                                    $scope.childcartcontents[i]['disc'] = 0;
                                }
                            }else{
                                $scope.childcartcontents[i]['disc'] = 0;
                            }
                            $scope.childcartcontents[i]['quantity'] = $scope.cevent_quantity;
                            $scope.cpayment_amount = $scope.eventchilddescContent.event_cost * $scope.cevent_quantity;                        
                            $scope.childcartcontents[i]['payment_amount'] = $scope.cpayment_amount;

                            // Load page:                                       
                            mainView.router.load({
                                pageName: 'mulcart'
                            });
                            $scope.getMultipleTotal();
                            $scope.$apply();
                        }
                    } 
                }
            } else {
                if($scope.childcartcontents && $scope.childcartcontents.length > 0){
                    for (var i = 0; i < $scope.childcartcontents.length; i++) {
                        if (event_id === $scope.childcartcontents[i]['detailcontent'].event_id){
                          return; 
                        } 
                    }
                }
                $scope.childcartitems['event_id'] = event_id;
                $scope.childcartitems['detailcontent'] = $scope.eventchilddescContent;
                $scope.childcartitems['disc'] = 0;
                $scope.childcartitems['quantity'] = $scope.cevent_quantity;
                $scope.childcartitems['event_cost'] = $scope.eventchilddescContent.event_cost;
                $scope.cpayment_amount = $scope.eventchilddescContent.event_cost * $scope.cevent_quantity;
                $scope.childcartitems['payment_amount'] = $scope.cpayment_amount;
                $scope.childcartcontents.push($scope.childcartitems);
//                alert(JSON.stringify($scope.childcartcontents));
                if($scope.mdiscount_code_value !== '' && $scope.mdiscount_code_value !== undefined ){
                    $scope.discountCheck($scope.mdiscount_code_value, 0);
                }
                MyApp.alert($scope.eventchilddescContent.event_title +''+ " Event Added to your Cart",'');
                if($scope.child_id_status){
                    $scope.viewCart();
                    // Load page:                                       
                    mainView.router.load({
                        pageName: 'mulcart'
                    }); 
                }else{ 
                    // Load page:                                       
                    mainView.router.load({
                        pageName: 'childlist'
                    }); 
                }
                $scope.getMultipleTotal();
                $scope.$apply();
            }
        };

        //View CART                                                
        $scope.viewCart = function () {
            $scope.recurring_payment_show = false;
            var todayDate = new Date();
            var start_date = new Date($scope.total_payment_startdate);
            if(!$scope.total_order_amount){
                $scope.total_order_amount = 0;
            }
            if(!$scope.total_order_quantity){
                $scope.total_order_quantity = 0;
            }
            if ($scope.recurring_payment_flag === 'Y' && (parseFloat($scope.po_total_amount) > parseFloat($scope.total_order_amount)) && (parseInt($scope.total_quantity) > parseInt($scope.total_order_quantity)) && (start_date > todayDate) 
                    && $scope.getRecurringAccess($scope.po_total_amount, $scope.total_deposit_amount, $scope.total_number_of_payments) && parseInt($scope.total_number_of_payments) > 0) {
                $scope.recurring_payment_show = true;
            } else {
                $scope.recurring_payment_show = false;
            }
            $scope.static_recurring_payment_show = $scope.recurring_payment_show;
        };


        //EDIT CART
        $scope.editcart = function (event_id) {
            $scope.subcart_flag = 'U';
            for (var i = 0; i < $scope.childcartcontents.length; i++) {
                if (event_id === $scope.childcartcontents[i]['detailcontent'].event_id) {
                    $scope.eventchilddescContent = $scope.childcartcontents[i]['detailcontent'];
                    $scope.cevent_quantity = $scope.childcartcontents[i]['quantity'];
                    $scope.cdesctotal = parseInt($scope.childcartcontents[i]['quantity']) * parseInt($scope.childcartcontents[i]['event_cost']);
                }
            }
        };

        //DELETE CART ITEMS     
        $scope.deletecart = function (event_id) {
            MyApp.closePanel();
            MyApp.confirm('Are you sure to Delete?', 'Alert',
                    function () {
                        for (var i = 0; i < $scope.childcartcontents.length; i++) {
                            if (event_id === $scope.childcartcontents[i]['detailcontent'].event_id) {
                                $scope.childcartcontents.splice(i, 1);
                                if($scope.childcartcontents.length>=1){
                                    $scope.getMultipleTotal();
                                    $scope.viewCart();
                                    $scope.$apply();
                                }else{
                                    $scope.mdiscount_code_value = '';       // changes
                                    $scope.multiple_total_amount = 0;
                                    $scope.$apply();
                                    
                                    // Load page:                                       
                                    mainView.router.load({
                                        pageName: 'childlist'
                                    });
                                }
                            }
                        }
                    }
            );
        };
        
        $scope.getMultipleTotal = function(){
            $scope.mtotal_amount = 0;
            $scope.po_total_amount = 0;
            $scope.po_view_total_amount = 0;
            $scope.multiple_total_amount = 0;
            $scope.processing_fee_value = 0;
            $scope.total_quantity = 0;
            var process_fee_per=0;
            var process_fee_val=0;
            var disc = 0;
            
            for(var j=0; j<$scope.childcartcontents.length; j++){
                if($scope.mdiscount_value > 0){
                    disc = $scope.childcartcontents[j]['disc'];
                }else{
                    disc = 0;
                    $scope.childcartcontents[j]['disc'] = 0;
                }
                $scope.childcartcontents[j]['payment_amount'] = (+$scope.childcartcontents[j]['event_cost'] - +disc)*$scope.childcartcontents[j]['quantity'];
                $scope.multiple_total_amount = +$scope.multiple_total_amount + +$scope.childcartcontents[j]['payment_amount'];
                $scope.total_quantity = +$scope.total_quantity + +$scope.childcartcontents[j]['quantity'];
            }
            
            var amount = $scope.multiple_total_amount;
            $scope.po_total_amount = parseFloat($scope.multiple_total_amount);
            $scope.static_po_total_amount = $scope.po_total_amount;
            if($scope.event_payment_method === 'CC' && ($scope.processing_fee_type === 2 || $scope.processing_fee_type === '2')){
                var process_fee_per = $scope.processing_fee_percentage;                
                var process_fee_val = $scope.processing_fee_transaction;
                
                if(amount>0){
                    var process_fee1 = +(parseFloat(amount)) + +((parseFloat(amount) * parseFloat(process_fee_per))/100) + +parseFloat(process_fee_val);
                    var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per))/100) + +parseFloat(process_fee_val);
                    $scope.processing_fee_value = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                }else{
                    $scope.processing_fee_value = 0;
                }
                $scope.mtotal_amount = +parseFloat(amount) + +$scope.processing_fee_value;
            }else{
                $scope.mtotal_amount = amount;
            }
            $scope.final_payment_amount = $scope.mtotal_amount;
            $scope.po_view_total_amount = $scope.final_payment_amount;
            $scope.static_final_payment_amount = $scope.final_payment_amount;    //BACKUP VALUES
            $scope.static_processing_fee_value = $scope.processing_fee_value;
            //for processing fee display
            $scope.payment_amount = $scope.multiple_total_amount;
            $scope.pay_infull_processingfee = $scope.processing_fee_value;
            
            $scope.recurring_payment_show = false;
            var todayDate = new Date();
            var start_date = new Date($scope.total_payment_startdate);
            if(!$scope.total_order_amount){
                $scope.total_order_amount = 0;
            }
            if(!$scope.total_order_quantity){
                $scope.total_order_quantity = 0;
            }
            if ($scope.recurring_payment_flag === 'Y' && (parseFloat($scope.po_total_amount) > parseFloat($scope.total_order_amount)) && (parseInt($scope.total_quantity) > parseInt($scope.total_order_quantity)) && (start_date > todayDate) 
                    && $scope.getRecurringAccess($scope.po_total_amount, $scope.total_deposit_amount, $scope.total_number_of_payments) && parseInt($scope.total_number_of_payments) > 0) {
                $scope.recurring_payment_show = true;
            } else {
                $scope.recurring_payment_show = false;
            }
            $scope.static_recurring_payment_show = $scope.recurring_payment_show;
        };
        
        $$('#mdiscountcode').on('click', function () {
            MyApp.prompt('Enter Discount code', 'Discount', function (value) {
                $scope.discountCheck(value, 1);
            });
        });
        
        $$('#mdiscountval').on('click', function () {
            MyApp.prompt('Enter Discount code', 'Discount', function (value) {
                $scope.discountCheck(value, 1);
            });
        });

        $scope.discountCheck = function (value, apply) {
            $scope.mdiscount_value = 0;
            $scope.mdiscount_code_value = '';
            $scope.discount_applied = false;
            var temp_check_for_disc_available = 0;
            for (var j = 0; j < $scope.mevent_discount_array.length; j++) {
                if ($scope.mevent_discount_array[j]['discount_code'].toUpperCase() === value.toUpperCase()) {
                    temp_check_for_disc_available = 1;
                    $scope.mdiscountcode = false;
                    $scope.mdiscountval = true;
                    $scope.mdiscount_code_value = value;
                    var disc_val = $scope.mevent_discount_array[j]['discount_amount'];
                    var mdiscount_type = $scope.mevent_discount_array[j]['discount_type'];
                    if (mdiscount_type === 'V') {
                        for(var k=0; k<$scope.childcartcontents.length; k++){
                            if (disc_val > 0 && $scope.childcartcontents[k]['event_cost'] > 0) {
                                $scope.discount_applied = true;
                                $scope.childcartcontents[k]['disc'] = disc_val;
                            }else{
//                                $scope.discount_applied = false;
                                $scope.childcartcontents[k]['disc'] = 0;
                            }
                        }
                        $scope.mdiscount_value = disc_val;
                    } else {
                        for(var k=0; k<$scope.childcartcontents.length; k++){
                            if (disc_val > 0 && $scope.childcartcontents[k]['event_cost'] > 0) {
                                $scope.discount_applied = true;
                                var discount_value = ($scope.childcartcontents[k]['event_cost'] * disc_val )/ 100;                                
                                $scope.childcartcontents[k]['disc'] = discount_value;
                            }else{
//                                $scope.discount_applied = false;
                                $scope.childcartcontents[k]['disc'] = 0;
                            }
                        }
                        $scope.mdiscount_value = ($scope.multiple_total_amount * disc_val)/ 100;
                    }
                    
                    $scope.getMultipleTotal();
                    if(apply === 1){
                        $scope.$apply();
                    }
                }
            }
            
            if (temp_check_for_disc_available === 0 && value !== '')  {
                $scope.mdiscount_code_value = "";
                $scope.mdiscountcode = true;
                $scope.mdiscountval = false;
                MyApp.alert('Your Discount code "' + value + '" is invalid','');
                $scope.discount_applied = false;
                $scope.mdiscount_value = 0;
                $scope.getMultipleTotal();
                if(apply === 1){
                    $scope.$apply();
                }
            }
        };
        
        $scope.resetForm = function(){
            for(var i=0; i<$scope.reg_col_field_name.length; i++){
                $scope.reg_col_field_name[i] = '';
            }
            $scope.reg_col_field_name = [];
            $scope.firstname = '';
            $scope.lastname = '';
            $scope.email = '';
            $scope.phone = '';
            $scope.zip_code = '';
            $scope.event_check_number = '';   
            $scope.selectedcardindex = '';
            $scope.event_credit_card_id = '';
            $scope.event_credit_card_status = '';
            $scope.event_payment_method = 'CC';
            $scope.cc_name = '';
            $scope.cardnumber = '';
            $scope.ccmonth = '';
            $scope.ccyear = '';
            $scope.country = '';
            $scope.postal_code = '';
            $scope.event_waiver_checked = false;
            $scope.shownewcard = true;
            $scope.showcardselection = false;
            $scope.cvv = '';
            if ($scope.stripe_card) {
                $scope.stripe_card.clear();
            }
            $scope.payment_method_id = '';
            $scope.stripe_customer_id = '';
            $scope.stripe_intent_method_type = "PI";
            $scope.event_cc_chkbx = false;
            // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
            $('.ph-text').css('display','block');
        };
        
        $scope.resetCartAfterPaymentSucceed = function(){
            if(($scope.event_type==='S' && ($scope.total_quantity>0 || $scope.total_quantity === undefined || $scope.total_quantity=== null || $scope.total_quantity === '')) || ($scope.event_type!=='S' && $scope.childcartcontents.length>0)){
                $scope.resetForm();
                $scope.paymentform.$setPristine();
                $scope.paymentform.$setUntouched();
                $scope.reg_col_names = [];
                $scope.waiver_text = '';
                $scope.processing_fee_type = '';
                $scope.onetime_payment_flag = 'N';
                $scope.recurring_payment_flag = 'N';
                $scope.total_order_amount = '';
                $scope.total_order_quantity = '';
                $scope.total_deposit_amount = 0;
                $scope.total_number_of_payments = '';
                $scope.total_payment_startdate = '';
                $scope.total_payment_frequency = '';
                $scope.po_total_amount = 0;
                $scope.po_view_total_amount = 0;
                $scope.discount_applied = false;
                $('.form-group').removeClass('focused');
                if($scope.event_type==='S'){
                    $scope.sevent_quantity = 0;
                    $scope.sdiscount_value  = 0;
                    $scope.sdiscount_code_value  = '';
                    $scope.sevent_discount_array = [];
                    $scope.sdiscount_code_display = true;
                    $scope.sdiscount_value_display = false;
                    $scope.sevent_id = '';
                    $scope.sevent_cost = '';
                    $scope.sevent_title = '';
                    $scope.sevent_desc = '';
                    $scope.sevent_banner_img_url = '';
                    $scope.sevent_begin_dt = '';
                    $scope.sevent_end_dt = '';

                    $scope.single_total_amount = 0;
                    $scope.single_payment_amount = 0;
                    $scope.total_quantity = 0;
                    $scope.final_payment_amount = 0;
                }else{
                    $scope.childeventContentStatus = 'Failure';
                    $scope.eventParentBannerImage = "";
                    $scope.eventChildBannerImage = "";
                    $scope.childcartcontents = [];
                    $scope.mevent_discount_array = [];
                    $scope.cevent_quantity = 1;
                    $scope.cevent_title = '';
                    $scope.cevent_desc = '';
                    $scope.mdiscountcode = true;
                    $scope.mdiscountval = false;
                    $scope.mdiscount_value  = 0;
                    $scope.mdiscount_code_value  = '';
                    $scope.mevent_title = '';
                    $scope.mevent_desc = '';

                    $scope.multiple_total_amount = 0;
                    $scope.multiple_payment_amount = 0;
                    $scope.total_quantity = 0;
                    $scope.final_payment_amount = 0;
                }
                $scope.getEventsList($scope.company_id,$scope.dynamic_event_id);
            }else{
                $scope.getEventsList($scope.company_id,$scope.dynamic_event_id);
            }
            $scope.showcardselection = false;
            $scope.erroroptionaldiscount = false;
            $scope.discount_applied = false;
            $scope.event_search_member = "";        
            $scope.event_payment_method = 'CC';
            $scope.event_check_number = '';   
            $scope.selectedcardindex = '';
            $scope.event_credit_card_id = '';
            $scope.event_credit_card_status = '';
            $scope.cc_name = '';
            $scope.cardnumber = '';
            $scope.ccmonth = '';
            $scope.ccyear = '';
            $scope.cvv = '';
            $scope.country = '';
            $scope.postal_code = '';
            $scope.recurring_flag = 'N';
            $scope.event_optional_discount = "";
            $scope.event_optional_discount_applied_flag = 'N';
            $scope.shownewcard = true;
            $scope.eventcarddetails = [];
            $scope.option_disc_error_flg = false;
            $scope.option_disc_error_msg = "";
            $scope.preloader = false;
            $scope.$apply();
        };        

        $scope.resetcartdetails = function(){
            if(($scope.event_type==='S' && ($scope.total_quantity>0 || $scope.total_quantity === undefined || $scope.total_quantity=== null || $scope.total_quantity === '')) || ($scope.event_type!=='S' && $scope.childcartcontents.length>0)){
                MyApp.closePanel();
                MyApp.confirm(' Discard cart items?', 'Alert', function () {
                    $scope.resetForm();
                    $scope.paymentform.$setPristine();
                    $scope.paymentform.$setUntouched();
                    $scope.preloader = false;
                    $scope.discount_applied = false;
                    $scope.reg_col_names = [];
                    $scope.waiver_text = '';
                    $scope.processing_fee_type = '';
                    $scope.onetime_payment_flag = 'N';
                    $scope.recurring_payment_flag = 'N';
                    $scope.total_order_amount = '';
                    $scope.total_order_quantity = '';
                    $scope.total_deposit_amount = 0;
                    $scope.total_number_of_payments = '';
                    $scope.total_payment_startdate = '';
                    $scope.total_payment_frequency = '';
                    $scope.po_total_amount = 0;
                    $scope.po_view_total_amount = 0;
                    if($scope.event_type==='S'){
                        $scope.sevent_quantity = 0;
                        $scope.sdiscount_value  = 0;
                        $scope.sdiscount_code_value  = '';
                        $scope.sevent_discount_array = [];
                        $scope.sdiscount_code_display = true;
                        $scope.sdiscount_value_display = false;
                        $scope.sevent_id = '';
                        $scope.sevent_cost = '';
                        $scope.sevent_title = '';
                        $scope.sevent_desc = '';
                        $scope.sevent_banner_img_url = '';
                        $scope.sevent_begin_dt = '';
                        $scope.sevent_end_dt = '';
                        $scope.sevent_more_detail_url='';
                        $scope.sevent_video_detail_url='';
                        
                        $scope.single_total_amount = 0;
                        $scope.single_payment_amount = 0;
                        $scope.total_quantity = 0;
                        $scope.final_payment_amount = 0;
                        
                    }else{
                        $scope.childeventContentStatus = 'Failure';
                        $scope.eventChildContent = "";
                        $scope.eventParentBannerImage = "";
                        $scope.eventChildBannerImage = "";
                        $scope.childcartcontents = [];
                        $scope.mevent_discount_array = [];
                        $scope.cevent_quantity = 1;
                        $scope.cevent_title = '';
                        $scope.cevent_desc = '';
                        $scope.mdiscountcode = true;
                        $scope.mdiscountval = false;
                        $scope.mdiscount_value  = 0;
                        $scope.mdiscount_code_value  = '';
                        $scope.mevent_title = '';
                        $scope.mevent_desc = '';
                        
                        $scope.multiple_total_amount = 0;
                        $scope.multiple_payment_amount = 0;
                        $scope.total_quantity = 0;
                        $scope.final_payment_amount = 0;                       
                    }
                    if(($scope.dynamic_event_id===undefined || $scope.dynamic_event_id==='') && $scope.event_type==='M'){
                        $scope.eventdetail($scope.dynamic_parent_index, '');
                    } else {
                        $scope.getEventsList($scope.company_id, $scope.dynamic_event_id);
                    }
                    
                    $scope.showcardselection = false;
                    $scope.erroroptionaldiscount = false;
                    $scope.discount_applied = false;
                    $scope.event_search_member = "";        
                    $scope.event_check_number = '';   
                    $scope.selectedcardindex = '';
                    $scope.event_credit_card_id = '';
                    $scope.event_credit_card_status = '';
                    $scope.cc_name = '';
                    $scope.cardnumber = '';
                    $scope.ccmonth = '';
                    $scope.ccyear = '';
                    $scope.cvv = '';
                    $scope.country = '';
                    $scope.postal_code = '';
                    $scope.recurring_flag = 'N';
                    $scope.event_optional_discount = "";
                    $scope.event_optional_discount_applied_flag = 'N';
                    $scope.shownewcard = true;
                    $scope.eventcarddetails = [];
                    $scope.option_disc_error_flg = false;
                    $scope.option_disc_error_msg = "";
                    
                });
            }else{
                if(($scope.dynamic_event_id===undefined || $scope.dynamic_event_id==='') && $scope.event_type==='M'){
                     mainView.router.load({
                        pageName: 'parentevntdesc'
                                });
                }else{
                 $scope.getEventsList($scope.company_id,$scope.dynamic_event_id);
                }
            }
            $scope.$apply();
        };

        //PAYMENT OPTIONS
        $scope.paymentOptions = function (event_type) {
            $scope.payment_event_type = event_type;
            if(event_type === 'S'){
                $scope.getTotal();
                $scope.final_payment_type = 'singlerecurring';
            }else if(event_type === 'M'){
                $scope.getMultipleTotal();               
                $scope.final_payment_type = 'multiplerecurring';
                $scope.event_array = [];                
                for (var i = 0; i < $scope.childcartcontents.length; i++) {
                    $scope.evnt_arr = {};                    
                    $scope.evnt_arr['event_id'] = $scope.childcartcontents[i]['detailcontent'].event_id;
                    $scope.evnt_arr['event_title']=$scope.childcartcontents[i]['detailcontent'].event_title;
                    $scope.evnt_arr['event_quantity'] = $scope.childcartcontents[i]['quantity'];
                    $scope.evnt_arr['event_cost'] = $scope.childcartcontents[i]['detailcontent'].event_cost;
                    $scope.evnt_arr['event_discount_amount'] = $scope.childcartcontents[i]['disc'];
                    $scope.evnt_arr['event_payment_amount'] = (+$scope.childcartcontents[i]['detailcontent'].event_cost - +$scope.childcartcontents[i]['disc'])*$scope.childcartcontents[i]['quantity'];
                    $scope.evnt_arr['event_begin_dt'] = $scope.childcartcontents[i]['detailcontent'].event_begin_dt;                    
                    $scope.event_array.push($scope.evnt_arr);
                }
            }          
             
            var todayDate = new Date();
            var start_date = new Date($scope.total_payment_startdate);


            if ($scope.recurring_payment_flag === 'Y' && (parseFloat($scope.po_total_amount) > parseFloat($scope.total_order_amount)) && (parseInt($scope.total_quantity) > parseInt($scope.total_order_quantity)) && (start_date > todayDate)
                    && $scope.getRecurringAccess($scope.po_total_amount, $scope.total_deposit_amount, $scope.total_number_of_payments) && parseInt($scope.total_number_of_payments) > 0)  {
                
                // Load page:                                       
                mainView.router.load({
                    pageName: 'payment_options'
                });
                
                $scope.balance_amount = $scope.po_total_amount - $scope.total_deposit_amount; 
                $scope.recurring_payment_amount = (Math.round((($scope.balance_amount / $scope.total_number_of_payments) + 0.00001) * 100) / 100).toFixed(2);

                if($scope.total_payment_frequency === '1'){  //Monthly                         
                    $scope.payment_schedule = $scope.payment_recurring_period($scope.total_payment_startdate,$scope.total_number_of_payments,'monthly',$scope.recurring_payment_amount,$scope.total_deposit_amount,$scope.balance_amount);
                }else if($scope.total_payment_frequency === '2'){  //Weekly - 7 days                        
                    $scope.payment_schedule = $scope.payment_recurring_period($scope.total_payment_startdate,$scope.total_number_of_payments,'weekly',$scope.recurring_payment_amount,$scope.total_deposit_amount,$scope.balance_amount);
                }else if($scope.total_payment_frequency === '3'){  //Bi-Weekly - 14 days
                    $scope.payment_schedule = $scope.payment_recurring_period($scope.total_payment_startdate,$scope.total_number_of_payments,'biweekly',$scope.recurring_payment_amount,$scope.total_deposit_amount,$scope.balance_amount);
                }
            }
            $scope.$apply();
            
        };
        
        $scope.payment_recurring_period = function (startDate, number_of_payments, intervalType, recurring_amnt, deposit_amt,balance_amount) {
            $scope.pay=[];
            $scope.recurrent = {};
            var newDate = startDate.split('-');
            var given_start_year = newDate[0];
            var given_start_month = newDate[1]-1;
            var given_start_day = newDate[2];
            var curr_date = new Date();
            curr_date = curr_date.toISOString().split('T')[0];
            var dup_date = new Date(given_start_year, given_start_month, given_start_day);
            $scope.recurrent.date=curr_date;
            $scope.recurrent.amount=deposit_amt;
            $scope.recurrent.type='D';
            $scope.recurrent.processing_fee = $scope.recurringProcessingFee(deposit_amt);
            $scope.pay.push($scope.recurrent);
            $scope.recurring_processing_fee = $scope.recurringProcessingFee(recurring_amnt);
            
            //Register screen display:
            if($scope.processing_fee_type === 2 || $scope.processing_fee_type === '2'){
                $scope.r_total_deposit_amount = parseFloat(deposit_amt) + parseFloat($scope.recurrent.processing_fee);
                $scope.deposit_processingfee = $scope.recurringProcessingFee(deposit_amt);
            }else{
                $scope.r_total_deposit_amount = parseFloat(deposit_amt);
                $scope.deposit_processingfee = 0;
            }
            $scope.static_po_total_amount = deposit_amt;
            if(intervalType === 'monthly' && parseFloat(recurring_amnt)>0 ){
                for(var i=0;i<number_of_payments;i++){
                    $scope.recurrent = {};
                    if(i===0){
                        $scope.recurrent['date']=startDate;
                        $scope.recurrent['amount']=recurring_amnt;
                        $scope.recurrent['type']='R';
                        $scope.recurrent['processing_fee']=$scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    }else{
                        if(i=== number_of_payments - 1){
                            recurring_amnt = +balance_amount - (recurring_amnt * (+number_of_payments - +1));
                            $scope.recurring_processing_fee = $scope.recurringProcessingFee(recurring_amnt);
                        }
                        dup_date.setMonth(dup_date.getMonth()+1);
                        var dup = dup_date;   
                        var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2); 
                        $scope.recurrent['date']=date;
                        $scope.recurrent['amount']=recurring_amnt;
                        $scope.recurrent['type']='R';
                        $scope.recurrent['processing_fee']=$scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    }
                }
            }else if(intervalType === 'weekly' && parseFloat(recurring_amnt)>0){
                for(var i=0;i<number_of_payments;i++){
                    $scope.recurrent = {};
                    if(i===0){
                        $scope.recurrent['date']=startDate;
                        $scope.recurrent['amount']=recurring_amnt;
                        $scope.recurrent['type']='R';
                        $scope.recurrent['processing_fee']=$scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    }else{
                        if(i=== number_of_payments - 1){
                            recurring_amnt = +balance_amount - (recurring_amnt * (+number_of_payments - +1));
                            $scope.recurring_processing_fee = $scope.recurringProcessingFee(recurring_amnt);
                        }
                        dup_date.setDate(dup_date.getDate() + 7);
                        var dup = dup_date;
                        var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        $scope.recurrent['date']=date;                        
                        $scope.recurrent['amount']=recurring_amnt;
                        $scope.recurrent['type']='R';
                        $scope.recurrent['processing_fee']=$scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    }
                }
            }else if(intervalType === 'biweekly' && parseFloat(recurring_amnt)>0){
                for(var i=0;i<number_of_payments;i++){
                    $scope.recurrent = {};
                    if(i===0){
                        $scope.recurrent['date']=startDate;
                        $scope.recurrent['amount']=recurring_amnt;
                        $scope.recurrent['type']='R';
                        $scope.recurrent['processing_fee']=$scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    }else{
                        if(i=== number_of_payments - 1){
                            recurring_amnt = +balance_amount - (recurring_amnt * (+number_of_payments - +1));
                            $scope.recurring_processing_fee = $scope.recurringProcessingFee(recurring_amnt);
                        }
                        dup_date.setDate(dup_date.getDate() + 14);
                        var dup = dup_date;
                        var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth()+1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        $scope.recurrent['date']=date;                        
                        $scope.recurrent['amount']=recurring_amnt;
                        $scope.recurrent['type']='R';
                        $scope.recurrent['processing_fee']=$scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    }
                }
            }
        };
        $scope.ccValidation = function () {
            var val = $scope.pay.length - 1;
            var adate = $scope.pay[val]['date'];
            var reDate = adate.split('-');
            var re_year = parseInt(reDate[0]);
            var re_month = parseInt(reDate[1]);

            if (re_year <= parseInt($scope.ccyear)) {                
                if (re_year ===  parseInt($scope.ccyear)) {                    
                    if (re_month <=  parseInt($scope.ccmonth)) {                       

                    } else {                     
                        return false;
                    }
                }

            } else {             
                return false;
            } return true;
            
        };
        
        $scope.payinfull = function(){
            $scope.final_payment_amount = $scope.po_view_total_amount; 
            $scope.processing_fee_value = $scope.pay_infull_processingfee;
            $scope.static_po_total_amount = parseFloat($scope.po_view_total_amount) - parseFloat($scope.pay_infull_processingfee);
            $scope.paymenttyp = 0;
            $scope.recurring_flag = 'N';
            if($scope.payment_event_type === 'S'){
                $scope.final_payment_type = 'onetimesingle';
            }else if($scope.payment_event_type === 'M'){
                $scope.final_payment_type = 'onetimemultiple';
            }
        };
        $scope.falseloader =function(){
            $scope.preloader = false;
        }
        $scope.selectpaymentplan = function(){
             $scope.final_payment_amount = $scope.r_total_deposit_amount;
             $scope.processing_fee_value = $scope.deposit_processingfee;
             $scope.static_po_total_amount = parseFloat($scope.r_total_deposit_amount) - parseFloat($scope.deposit_processingfee);
             $scope.paymenttyp = 1;
             $scope.recurring_flag = 'Y';
             if($scope.payment_event_type === 'S'){
                $scope.final_payment_type = 'singlerecurring';
            }else if($scope.payment_event_type === 'M'){
                $scope.final_payment_type = 'multiplerecurring';
            }
        };
        

        $scope.singlecheckout =function(event_type) {
            $scope.payment_event_type = event_type;
            $scope.final_payment_type = 'onetimesingle';
            $scope.recurring_flag = 'N';
            $scope.paymenttyp = 0;
            $scope.getTotal();
//            $scope.$apply();
        };

        $scope.multiplecheckout =function(event_type) {
            $scope.payment_event_type = event_type;
            $scope.recurring_flag = 'N';
            $scope.final_payment_type = 'onetimemultiple';
            $scope.paymenttyp = 0;
            $scope.event_array = [];
            for (var i = 0; i < $scope.childcartcontents.length; i++) {
                $scope.evnt_arr = {};
                $scope.evnt_arr['event_id'] = $scope.childcartcontents[i]['detailcontent'].event_id;
                $scope.evnt_arr['event_title']=$scope.childcartcontents[i]['detailcontent'].event_title;
                $scope.evnt_arr['event_quantity'] = $scope.childcartcontents[i]['quantity'];
                $scope.evnt_arr['event_cost'] = $scope.childcartcontents[i]['detailcontent'].event_cost;
                $scope.evnt_arr['event_discount_amount'] = $scope.childcartcontents[i]['disc'];
                $scope.evnt_arr['event_payment_amount'] = (+$scope.childcartcontents[i]['detailcontent'].event_cost - +$scope.childcartcontents[i]['disc'])*$scope.childcartcontents[i]['quantity'];
                $scope.evnt_arr['event_begin_dt'] = $scope.childcartcontents[i]['detailcontent'].event_begin_dt;                   
                $scope.event_array.push($scope.evnt_arr);
            }
            $scope.getMultipleTotal();
        };       
        
        $scope.redirectPage = function(){ 
            $scope.recurring_flag = 'N';
            $scope.event_optional_discount = "";
            $scope.event_optional_discount_applied_flag = 'N';            
            $scope.option_disc_error_flg = false;
            $scope.option_disc_error_msg = "";  
            if($scope.payment_event_type === 'S'){
                $scope.preloader = false;
                $scope.getTotal();
            }else{
                $scope.preloader = false;
                $scope.getMultipleTotal();
            }
        };
               
        $scope.checkdatetimevalue = function (dat) {
            if (dat != undefined) {
                if (dat !== '0000-00-00' && dat !== '') {
                    var timecheck = dat.split(" ");
                    if (timecheck[1] === "00:00:00") {
                        return 1;
                    } else {
                        return 0;
                    }
                } else {
                    var newDate = '';
                    return newDate;
                }
            }
        };


        $scope.getstartDate = function (dat) {
            if (dat != undefined) {
                if (dat !== '0000-00-00' && dat !== '') {
                    var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                            navigator.userAgent && !navigator.userAgent.match('CriOS');
                    if (isSafari) {
                        var safaritimecheck = dat.split(" ");
                        if (safaritimecheck[1] === "00:00:00") {
                            var ddateonly = new Date(safaritimecheck[0].replace(/-/g, "/"));
                            var dateStr = new Date(ddateonly);
                            return dateStr;
                        } else {
                            var dda = dat.toString();
                            dda = dda.replace(/-/g, "/");
                            var time = new Date(dda).toISOString();   // For date in safari format
                            return time;
                        }
                    } else {

                        var datetimecheck = dat.split(" ");
                        if (datetimecheck[1] === "00:00:00") {
                            var ddateonly = datetimecheck[0] + 'T00:00:00+00:00';
                            var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                            return newDate;
                        } else {
                            var ddateonly = datetimecheck[0] + 'T' + datetimecheck[1] + '+00:00';
                            var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                            return newDate;

                        }
                    }
                } else {
                    var newDate = '';
                    return newDate;
                }
            }
        };
        
        $scope.getPaymentdate = function (dat) {
            if (dat !== '0000-00-00' && dat !== '') {
            var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
            if (isSafari) {
                var ddateonly = new Date(dat.replace(/-/g, "/"));
                var dateStr = new Date(ddateonly);
                return dateStr;                
            } else {
                    var ddateonly = dat + 'T00:00:00+00:00';
                    var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                    return newDate;                
            }}else{
                var newDate = '';
                return newDate;
            }
        };

        $scope.openevents = function (eventurl) {
               MyApp.closePanel();
            if ($rootScope.online != "online") {
                $scope.istatus();
                return;
            }
            if(eventurl ==='' && eventurl === undefined && eventurl === null){
                return false;
            }else if(eventurl !=='' && eventurl !== undefined && eventurl !== null){
                window.open(eventurl, '_blank');            
            }

        };
    
        $scope.formatDate = function (dat) {
            var date = dat.split("-").join("/");
            var dateOut = new Date(date);
            return dateOut;
        };
        
        $scope.catchSearchMember = function(){
             if($scope.event_search_member){
                if ($scope.studentlist.length > 0) {
                    for (var i = 0; i < $scope.studentlist.length; i++) { 
                        if ($scope.event_search_member === $scope.studentlist[i].student_name){
                            $scope.event_selected_student_id = $scope.studentlist[i].student_id;
                            $scope.reg_col_field_name[0] = $scope.studentlist[i].reg_col_1;
                            $scope.reg_col_field_name[1] = $scope.studentlist[i].reg_col_2;
                            $scope.firstname = $scope.studentlist[i].buyer_first_name;
                            $scope.lastname = $scope.studentlist[i].buyer_last_name;
                            $scope.phone = $scope.studentlist[i].buyer_phone;
                            $scope.email = $scope.studentlist[i].buyer_email;
                            $(".form-group > input").parents('.form-group').addClass('focused');
                            //If no values then remove focus class
                            if(!$scope.firstname)
                                $("#firstName").parents('.form-group').removeClass('focused');
                            if(!$scope.lastname)
                                $("#lastName").parents('.form-group').removeClass('focused');
                            if(!$scope.phone)
                                $("#phone").parents('.form-group').removeClass('focused');
                            if(!$scope.email)
                                $("#email").parents('.form-group').removeClass('focused');
                            if(!$scope.zip_code)
                                $("#zip_code").parents('.form-group').removeClass('focused');
                            
                            for(var j = 0; j < 10; j++){
                                $scope.catchEventInputBlur('column'+j,$scope.reg_col_field_name[j]); // For Dynamic registration column
                            }
                            //For credit card
                            if ($scope.studentlist[i].card_details && $scope.studentlist[i].card_details.length > 0){
                                $scope.eventcarddetails = $scope.studentlist[i].card_details;
                            }else{
                                $scope.eventcarddetails = [];
                            } 
                            if($scope.event_payment_method === 'CC'){  //kamal
                               if ($scope.studentlist[i].card_details && $scope.studentlist[i].card_details.length > 0){
                                    $scope.showcardselection = true;
                                    $scope.shownewcard = false; 
                                } else{
                                    $scope.showcardselection = false;
                                    $scope.shownewcard = true; 
                                    $("#cc_name").parents('.form-group').removeClass('focused');
                                    $("#cc-number").parents('.form-group').removeClass('focused');
                                    $("#postal_code").parents('.form-group').removeClass('focused');
                                    $("#cc-cvv").parents('.form-group').removeClass('focused');
                                }
                            }else{
                                $scope.showcardselection = false;
                                $scope.shownewcard = false; 
                            }
                        }
                    }
                }
                // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
                $('.ph-text').css('display','none');
            }else{
                $scope.event_selected_student_id = '';
                $scope.firstname = '';
                $scope.lastname = '';
                $scope.phone = '';
                $scope.email = ''; 
                $scope.reg_col_field_name[0] = '';
                $scope.reg_col_field_name[1] = '';
                $scope.event_credit_card_id = '';
                $scope.event_credit_card_status = '';
                $scope.cc_name = '';
                $scope.cardnumber = '';
                $scope.ccmonth = '';
                $scope.ccyear = '';
                $scope.cvv = '';
                $scope.country = '';
                $scope.postal_code = '';
                $scope.payment_method_id = '';
                $scope.stripe_customer_id = '';
                $(".form-group > input").parents('.form-group').removeClass('focused');
                $scope.showcardselection = false;
                $scope.shownewcard = true; 
                $scope.eventcarddetails = [];
                // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
                $('.ph-text').css('display','block');
            }
         };
         
         // RESET PAYMENT METHOD SELECTION         
         $scope.selectPaymentMethod = function () { //kamal
            $scope.event_check_number = '';   
            $scope.selectedcardindex = '';
            $scope.event_credit_card_id = '';
            $scope.event_credit_card_status = '';
            $scope.cc_name = '';
            $scope.cardnumber = '';
            $scope.ccmonth = '';
            $scope.ccyear = '';
            $scope.cvv = '';
            $scope.country = '';
            $scope.postal_code = '';
            $scope.event_cc_chkbx = false;
            $scope.payment_method_id = '';
            $scope.stripe_customer_id = '';
            if($scope.event_payment_method === 'CC'){  //kamal
               if ($scope.eventcarddetails && $scope.eventcarddetails.length > 0){
                    $scope.showcardselection = true;
                    $scope.shownewcard = false; 
                } else{
                    $scope.showcardselection = false;
                    $scope.shownewcard = true; 
                }
            }else{
                $scope.showcardselection = false;
                $scope.shownewcard = false; 
            }
            $scope.catchEventOptionalDis();
        };
        
        //For selecting existing card of a user
        $scope.selecteventCardDetail = function (ind) {
            if ($scope.selectedcardindex === 'Add New credit card') {
                $scope.shownewcard = true;
                $scope.event_credit_card_id = '';
                $scope.event_credit_card_status = '';
                $scope.cc_name = '';
                $scope.cardnumber = '';
                $scope.ccmonth = '';
                $scope.ccyear = '';
                $scope.cvv = '';
                $scope.country = '';
                $scope.postal_code = '';
                $scope.payment_method_id = '';
                $scope.stripe_customer_id = '';
                $("#cc_name").parents('.form-group').removeClass('focused');
                $("#cc-number").parents('.form-group').removeClass('focused');
                $("#postal_code").parents('.form-group').removeClass('focused');
                $("#cc-cvv").parents('.form-group').removeClass('focused');
            } else {
                $scope.shownewcard = false;
                if($scope.stripe_enabled) {
                    $scope.payment_method_id = $scope.eventcarddetails[ind].payment_method_id;
                    $scope.stripe_customer_id = $scope.eventcarddetails[ind].stripe_customer_id;
                } else {
                    $scope.event_credit_card_id = parseInt($scope.eventcarddetails[ind].credit_card_id);
                    $scope.event_credit_card_status = $scope.eventcarddetails[ind].credit_card_status;
                    $scope.cc_name = $scope.eventcarddetails[ind].credit_card_name;
                    $scope.ccmonth = $scope.eventcarddetails[ind].credit_card_expiration_month;
                    $scope.ccyear = $scope.eventcarddetails[ind].credit_card_expiration_year;
                    $scope.country = $scope.eventcarddetails[ind].credit_card_countryr;
                    $scope.postal_code = $scope.eventcarddetails[ind].postal_code;
                }
            }
        };
        
        $scope.backToPOS = function () {
            var hosturl;
            if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
               window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                       window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                    var origin = window.location.origin;
                    hosturl = origin;
                }

            } else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            }  else if (window.location.hostname === 'beta.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'https://' + www_url;
                }
            }   else if (window.location.hostname === 'stage.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'localhost') {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            } else {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            }
            var pos_type, pos_string, pos_entry_type;
            if ($scope.pos_user_type === 'S'){
                pos_type = 'spos';
                pos_string = 'e92213e6721525d7spos712627e6db9b98b4';
            }else if($scope.pos_user_type === 'P'){
                pos_type = 'ppos';
                pos_string = '8f2f44c8580cc72ppos700ff5d197b5a18f0';
            }else{
                pos_type = '';
                pos_string = '';
            }
            if($scope.pos_entry_type){
                pos_entry_type = $scope.pos_entry_type;
            }else{
                pos_entry_type = 'l';
            }
            if (pos_type) {
                $window.open(hosturl+'/' + 'pos' + '/?=' + $scope.param1 + '/' + $scope.param2 + '/' + pos_type + '-' + pos_string + '/'+pos_entry_type+'/', '_self');
            }
        };
        
        $scope.resetOptionalDiscount = function () {
            $scope.event_optional_discount_applied_flag = "N";
            var amount = parseFloat($scope.static_po_total_amount);
            $scope.recurring_payment_show = $scope.static_recurring_payment_show;
            if ($scope.event_payment_method === 'CC' && ($scope.processing_fee_type === 2 || $scope.processing_fee_type === '2')) {
                var process_fee_per = $scope.processing_fee_percentage;
                var process_fee_val = $scope.processing_fee_transaction;
                if (amount > 0) {
                    var process_fee1 = +(parseFloat(amount)) + +((parseFloat(amount) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                    var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                    $scope.processing_fee_value = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    $scope.processing_fee_value = 0;
                }
                $scope.final_payment_amount = +parseFloat(amount) + +$scope.processing_fee_value;
            } else {
                $scope.final_payment_amount = amount;
                $scope.processing_fee_value = 0;
            }
        };
        
         //For single event & Multiple event total cost calculation based on discount and quantity
        $scope.catchEventOptionalDis = function () {            
            $scope.event_optional_discount = this.event_optional_discount; 
            if($scope.event_optional_discount && parseFloat($scope.event_optional_discount)>0){
                $scope.preloader = true;
                $scope.event_optional_discount_applied_flag = "Y";

                if ($scope.final_payment_type === 'onetimesingle') {       
                   $scope.final_multiple_event_array = []; 
                   $scope.final_pay = [];
                }else if ($scope.final_payment_type === 'singlerecurring') {
                    $scope.final_multiple_event_array = []; 
                    $scope.final_pay = $scope.pay ;
                }else if ($scope.final_payment_type === 'onetimemultiple') {
                    $scope.final_multiple_event_array = $scope.event_array; 
                    $scope.final_pay = [];
                }else if ($scope.final_payment_type === 'multiplerecurring') {
                    $scope.final_multiple_event_array = $scope.event_array; 
                    $scope.final_pay = $scope.pay;
                }

                var curr_date = new Date();
                var reg_current_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);

                $scope.payment_amount = (Math.round(($scope.po_total_amount + 0.00001) * 100) / 100).toFixed(2);
                if ($scope.payment_event_type === 'S') {
                    $scope.discount_value = $scope.sdiscount_value;
                    $scope.event_cost = $scope.sevent_cost;
                    $scope.event_title = $scope.sevent_title;
                    $scope.event_id = $scope.sevent_id;
                } else if ($scope.payment_event_type === 'M') {
                    $scope.discount_value = $scope.mdiscount_value;
                    $scope.event_cost = $scope.po_total_amount;
                    $scope.event_title = $scope.mevent_title;
                    $scope.event_id = $scope.mevent_id;
                }

                $http({
                    method: 'POST',
                    url: urlservice.url + 'getOptionalEventTotal',
                    data: {
                        companyid: $scope.company_id,
                        event_id: $scope.event_id,
                        registration_amount: $scope.event_cost,
                        payment_amount: $scope.payment_amount,
                        quantity: $scope.total_quantity,
                        processing_fee_type: $scope.processing_fee_type,
                        payment_frequency: $scope.total_payment_frequency,
                        event_array: $scope.final_multiple_event_array,
                        payment_array: $scope.final_pay,
                        event_type: $scope.payment_event_type,
                        payment_type: $scope.final_payment_type,
                        upgrade_status: $scope.company_status,
                        payment_start_date:$scope.total_payment_startdate,
                        total_no_of_payments:$scope.total_number_of_payments,
                        total_order_amount: $scope.total_order_amount,
                        total_order_quantity: $scope.total_order_quantity,
                        registration_date: reg_current_date, 
                        pos_email: $localStorage.pos_user_email_id,
                        payment_method:$scope.event_payment_method, //CC - creadit card,CH - check, CA- Cash
                        event_optional_discount: $scope.event_optional_discount_applied_flag ==='Y' ? $scope.event_optional_discount : "", //event optional discount
                        event_optional_discount_flag:$scope.event_optional_discount_applied_flag,
                        reg_type_user: $scope.pos_user_type === 'S' ? 'SP': ($scope.pos_user_type === 'P' ? 'PP' : 'U'), 
                        token: $scope.pos_token
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then(function (response) {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                    if (response.data.status === 'Success') {
                        if($scope.event_optional_discount){
                            $scope.final_payment_amount = response.data.payment_amount;
                            $scope.recurring_payment_status = response.data.recurring_payment_status;
                            if($scope.recurring_payment_status === 'Y'){
                                $scope.recurring_payment_show = true;
                            }else{
                                $scope.recurring_payment_show = false;
                            }                        
                            $scope.processing_fee_value = response.data.processing_fee;
                            $scope.option_disc_error_flg = false;
                            $scope.option_disc_error_msg = "";
                        }else{                                           
                            $scope.option_disc_error_flg = false;
                            $scope.option_disc_error_msg = "";
                            $scope.resetOptionalDiscount();
                        }
                    } else {
                        if(response.data.session_status === "Expired"){
                            MyApp.alert('Session Expired','', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        }else{
                            $scope.option_disc_error_flg = true;
                            $scope.option_disc_error_msg = response.data.msg;
                            $scope.resetOptionalDiscount();
                        }
                    }
                }, function (response) {
                    $scope.preloaderVisible = false;
                    $scope.preloader = false;
                    MyApp.alert('Connection failed', 'Invalid Server Response');
                });
            }else{                
                $scope.option_disc_error_flg = false;
                $scope.option_disc_error_msg = "";
                $scope.resetOptionalDiscount();
                
            }
        };
//        For Dynamic field label focus
        $scope.catchEventInputBlur = function (selectedid,inputvalue){
            if (inputvalue === "" || inputvalue === undefined) {
                $('#'+selectedid).parents('.form-group').removeClass('focused');
            } else {
                $('#'+selectedid).parents('.form-group').addClass('focused');
            }
        };
        $scope.catchEventInputFocus = function (selectedid){
                $('#'+selectedid).parents('.form-group').addClass('focused');
        };
        
        //DOCUMENT READY FUNCTION
        angular.element(document).ready(function () {
            $('input').focus(function () {
                $(this).parents('.form-group').addClass('focused');
            });

            $('input').blur(function () {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.form-group').removeClass('focused');
                } else {
                    $(this).addClass('filled');
                }
            });
            ($(window).width() < 641) ? $scope.mobiledevice = true : $scope.mobiledevice = false;
            $scope.urlredirection();
        });
       
    }]);

MyApp.angular.filter('date2', function ($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function (input, format) {
        console.clear();
//        console.log(input, format);
        var dtfilter = $filter('date')(input, format);
        if (dtfilter != undefined) {
            var day = parseInt($filter('date')(input, 'dd'));
            var relevantDigits = (day < 30) ? day % 20 : day % 30;
//            console.log(day, relevantDigits);
            var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
            return dtfilter;
            //return dtfilter+suffix;
        }
    };
});

MyApp.angular.filter("trust", ['$sce', function ($sce) {
        return function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        }
    }]);
MyApp.angular.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');

                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
MyApp.angular.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
MyApp.angular.directive('compileTemplate', function($compile, $parse){
    return {
          link: function(scope, element, attr){
            var parsed = $parse(attr.ngBindHtml);
          function getStringValue() { return (parsed(scope) || '').toString(); }

            //Recompile if the template changes
          scope.$watch(getStringValue, function() {
                $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
            });
        }
    }
});
//compile-template

