<?php

require_once 'EventModel.php';

class EventApi extends EventModel {
    
    public function __construct(){
        parent::__construct();          // Init parent contructor
    }
    
    public function processApi(){
        $func = strtolower(trim(str_replace("/","",$_REQUEST['run'])));
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)){
            $this->_request = json_decode(file_get_contents('php://input'), true);
 	}
        if(isset($this->_request['companyid']) && $this->_request['companyid']==6){
            $error = array('status' => "Failed", "msg" => "No events available.");
            $this->response($this->json($error), 404);
        }
        if((int)method_exists($this,$func) > 0){
            $this->$func();
        }else{
            $error = array("status" => "Failed", "code" => 404, "msg" => "Page not found.", "func" => $func);
            $this->response($this->json($error),404); // If the method not exist with in this class, response would be "Page not found".
        }
    }
		
    private function events(){
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if($this->get_request_method() != "GET"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        //log_info("Ser : ".json_encode($_SERVER));
        $company_id = $event_id = $token = $pos_email = '';
        $reg_type_user = 'U';
        $from = 'U';        //U - user, S - Social networks crawler
        
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        if(isset($this->_request['event_id'])){
            $event_id = $this->_request['event_id'];
        }        
        if(isset($this->_request['from'])){
            $from = $this->_request['from'];
        }
        if (isset($this->_request['token'])) {
            $token = $this->_request['token'];
        }
        if (isset($this->_request['pos_email'])) {
            $pos_email = $this->_request['pos_email'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        
        if(!empty($company_id)){
            if(!empty($from) && $from=='S'){
                if(empty($event_id)){
                    header("Location:crawlerPage.php?company_id=$company_id");
//                    $this->getEventsDetailByCompanyForSocialMedia($company_id);
                }else{
                    header("Location:crawlerPage.php?company_id=$company_id&event_id=$event_id");
//                    $this->getEventsDetailByEventIdForSocialMedia($company_id,$event_id);
                }
                exit();
            }else{
                if($reg_type_user!='U'){
//                    $this->verifyStudioPOSToken($company_id, $pos_email, $token);
                    $this->checkpossettings($company_id, '', $pos_email, $token);
                }
                if(empty($event_id)){
                    $this->getEventsDetailByCompany($company_id, $reg_type_user);
                }else{
                    $this->getEventsDetailByEventId($company_id,$event_id, $reg_type_user);
                }
            }
//            $this->geteventsdetails($companyid);            
        }else{
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 400); 
        }
    }
    
    private function getwepaystatus(){
        if($this->get_request_method() != "GET"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id='';
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        
        if(!empty($company_id)){
            $this->wepayStatus($company_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function getstripestatus() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->stripeStatus($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function addParticipantDetails(){
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id = $event_id = $event_name = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $processing_fee_type = $event_type =$discount_code= '';
        $student_id=$participant_id = $fee = $dicount = $registration_amount = $payment_amount = 0;
        $quantity = 1;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $event_array = [];
        $reg_type_user='U';        
        $reg_version_user = '';
        
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        if(isset($this->_request['event_id'])){
            $event_id = $this->_request['event_id'];
        }
        if(isset($this->_request['studentid'])){
            $student_id = $this->_request['studentid'];
        }
        if(isset($this->_request['participant_id'])){
            $participant_id = $this->_request['participant_id'];
        }
        if(isset($this->_request['event_name'])){
            $event_name = $this->_request['event_name'];
        }
        if(isset($this->_request['desc'])){
            $desc = $this->_request['desc'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if(isset($this->_request['email'])){
            $buyer_email = $this->_request['email'];
        }
        if(isset($this->_request['phone'])){
            $buyer_phone = $this->_request['phone'];
        }
        if(isset($this->_request['postal_code'])){
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if(isset($this->_request['registration_amount'])){
            $registration_amount = $this->_request['registration_amount'];
        }
        if(isset($this->_request['payment_amount'])){
            $payment_amount = $this->_request['payment_amount'];
        }
        if(isset($this->_request['processing_fee_type'])){
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if(isset($this->_request['fee'])){
            $fee = $this->_request['fee'];
        }
        if(isset($this->_request['reg_col1'])){
            $reg_col1 = $this->_request['reg_col1'];
        }
        if(isset($this->_request['reg_col2'])){
            $reg_col2 = $this->_request['reg_col2'];
        }
        if(isset($this->_request['reg_col3'])){
            $reg_col3 = $this->_request['reg_col3'];
        }
        if(isset($this->_request['reg_col4'])){
            $reg_col4 = $this->_request['reg_col4'];
        }
        if(isset($this->_request['reg_col5'])){
            $reg_col5 = $this->_request['reg_col5'];
        }
        if(isset($this->_request['reg_col6'])){
            $reg_col6 = $this->_request['reg_col6'];
        }
        if(isset($this->_request['reg_col7'])){
            $reg_col7 = $this->_request['reg_col7'];
        }
        if(isset($this->_request['reg_col8'])){
            $reg_col8 = $this->_request['reg_col8'];
        }
        if(isset($this->_request['reg_col9'])){
            $reg_col9 = $this->_request['reg_col9'];
        }
        if(isset($this->_request['reg_col10'])){
            $reg_col10 = $this->_request['reg_col10'];
        }
        if(isset($this->_request['discount'])){
            $discount = $this->_request['discount'];
        }
        if(isset($this->_request['quantity'])){
            $quantity = $this->_request['quantity'];
        }
        if(isset($this->_request['event_type'])){
            $event_type = $this->_request['event_type'];
        }
        if(isset($this->_request['event_array'])){
            $event_array = $this->_request['event_array'];
        }
        if(isset($this->_request['discount_code'])){
            $discount_code = $this->_request['discount_code'];
        }
        
        
        if(!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($event_type)){
            if($event_type!='M' && $event_type!='S'){
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }else{
                if($event_type=='M' && empty($event_array)){
                    $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 400);
                }else{
                    $this->addParticipantDetailsForFreeEvent($company_id, $event_id, $student_id,$participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_type, $event_array,$reg_type_user,$reg_version_user,$discount_code);
                }
            }
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function wepaycheckout(){
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id = $event_id = $event_name =$event_type= $sub_events_id = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = $payment_frequency =$discount_code= '';
        $student_id =$participant_id= $fee = $discount = $registration_amount = $payment_amount = $total_order_quantity = $total_order_amount = 0;
        $quantity = 1;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $event_array = $payment_array = [];
        $upgrade_status = $p_type = 'F';        //$p_type F - Free, CO - One time Checkout, RE - Recurring Type
        $country = 'US';
        $reg_type_user='U';        
        $reg_version_user = '';
        $pos_email = $token = '';
        $optional_discount_flag = 'N';
        $payment_method = "CC";
        $check_number = $optional_discount = $total_no_of_payments = 0;
        $payment_type = 'onetimesingle';
        $payment_start_date = '';
        
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        if(isset($this->_request['event_id'])){
            $event_id = $this->_request['event_id'];
        }
        if(isset($this->_request['studentid'])){
            $student_id = $this->_request['studentid'];
        }
        if(isset($this->_request['participant_id'])){
            $participant_id = $this->_request['participant_id'];
        }
        if(isset($this->_request['event_name'])){
            $event_name = $this->_request['event_name'];
        }
        if(isset($this->_request['desc'])){
            $desc = $this->_request['desc'];
        }
        if(isset($this->_request['event_array'])){
            $event_array = $this->_request['event_array'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if(isset($this->_request['email'])){
            $buyer_email = $this->_request['email'];
        }
        if(isset($this->_request['phone'])){
            $buyer_phone = $this->_request['phone'];
        }
        if(isset($this->_request['postal_code'])){
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if(isset($this->_request['country'])){
            $country = $this->_request['country'];
        }
        if(isset($this->_request['cc_id'])){
            $cc_id = $this->_request['cc_id'];
        }
        if(isset($this->_request['cc_state'])){
            $cc_state = $this->_request['cc_state'];
        }
        if(isset($this->_request['registration_amount'])){
            $registration_amount = $this->_request['registration_amount'];
        }
        if(isset($this->_request['payment_amount'])){
            $payment_amount = $this->_request['payment_amount'];
        }
        if(isset($this->_request['processing_fee_type'])){
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if(isset($this->_request['fee'])){
            $fee = $this->_request['fee'];
        }
        if(isset($this->_request['discount'])){
            $discount = $this->_request['discount'];
        }
        if(isset($this->_request['quantity'])){
            $quantity = $this->_request['quantity'];
        }
        if(isset($this->_request['reg_col1'])){
            $reg_col1 = $this->_request['reg_col1'];
        }
        if(isset($this->_request['reg_col2'])){
            $reg_col2 = $this->_request['reg_col2'];
        }
        if(isset($this->_request['reg_col3'])){
            $reg_col3 = $this->_request['reg_col3'];
        }
        if(isset($this->_request['reg_col4'])){
            $reg_col4 = $this->_request['reg_col4'];
        }
        if(isset($this->_request['reg_col5'])){
            $reg_col5 = $this->_request['reg_col5'];
        }
        if(isset($this->_request['reg_col6'])){
            $reg_col6 = $this->_request['reg_col6'];
        }
        if(isset($this->_request['reg_col7'])){
            $reg_col7 = $this->_request['reg_col7'];
        }
        if(isset($this->_request['reg_col8'])){
            $reg_col8 = $this->_request['reg_col8'];
        }
        if(isset($this->_request['reg_col9'])){
            $reg_col9 = $this->_request['reg_col9'];
        }
        if(isset($this->_request['reg_col10'])){
            $reg_col10 = $this->_request['reg_col10'];
        }
        if(isset($this->_request['payment_frequency'])){
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if(isset($this->_request['event_array']) && !empty($this->_request['event_array'])){
            $event_array = $this->_request['event_array'];
        }
        if(isset($this->_request['payment_array']) && !empty($this->_request['payment_array'])){
            $payment_array = $this->_request['payment_array'];
        }
        if(isset($this->_request['event_type'])){
            $event_type = $this->_request['event_type'];
        }
        if(isset($this->_request['payment_type'])){
            $payment_type = $this->_request['payment_type'];
        }
        if(isset($this->_request['upgrade_status'])){
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if(isset($this->_request['total_order_amount'])){
            $total_order_amount = $this->_request['total_order_amount'];
        }
        if(isset($this->_request['total_order_quantity'])){
            $total_order_quantity = $this->_request['total_order_quantity'];
        }if(isset($this->_request['discount_code'])){
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['check_number'])) {
            if(!empty($this->_request['check_number'])){
            $check_number = $this->_request['check_number'];
            }
        }
        if (isset($this->_request['event_optional_discount'])) {
            if(!empty($this->_request['event_optional_discount'])){
                $optional_discount = $this->_request['event_optional_discount'];
            }
        }
        if (isset($this->_request['event_optional_discount_flag'])) {
            $optional_discount_flag = $this->_request['event_optional_discount_flag'];
        }
        if(isset($this->_request['payment_start_date'])){
            $payment_start_date = $this->_request['payment_start_date'];
        }
        if(isset($this->_request['total_no_of_payments'])){
            $total_no_of_payments = $this->_request['total_no_of_payments'];
        }
        if(isset($this->_request['token'])){
            $token = $this->_request['token'];
        }
        if(isset($this->_request['pos_email'])){
            $pos_email = $this->_request['pos_email'];
        }
        if($reg_type_user !== 'U'){
            $this->checkpossettings($company_id, $payment_method, $pos_email, $token);
        }
        if($reg_type_user === "PP" && (((int)$optional_discount > 0) || $optional_discount_flag === "Y")){
            $error = array('status' => "Failed", "msg" => "Optional discount is not applicable");
            $this->response($this->json($error), 200);
        }
        if($payment_method === "CH" && empty(trim($check_number))){
            $error = array('status' => "Failed", "msg" => "Please provide check number");
            $this->response($this->json($error), 200);
        }
        if(empty($reg_type_user) || (($reg_type_user === 'PP' || $reg_type_user === 'U') && !empty(trim($student_id)))){
            $error = array('status' => "Failed", "msg" => "Authentication Error");
            $this->response($this->json($error), 200);
        }
//        if(($payment_method === "CH" || $payment_method === "CA") && ($payment_type == 'singlerecurring' || $payment_type == 'multiplerecurring')){
//            $error = array('status' => "Failed", "msg" => "Payment method is not valid for this payment type");
//            $this->response($this->json($error), 200);
//        }
        if($payment_amount==0){
            $payment_array = [];
            $p_type = 'F';
            $payment_type = 'free';
            $payment_method = "CA";
            $payment_frequency = 4;
        }
        if($payment_method === "CC" && empty(trim($cc_id)) && ($payment_amount-$optional_discount)>5){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if(!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)&& !empty($event_type)){
            
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if($event_type=='S' && $payment_type == 'onetimesingle'){
            $event_array = [];
            $payment_array = [];
            $p_type = 'CO';
            $payment_frequency = 4;
        }elseif($event_type=='S' && $payment_type == 'singlerecurring'){
            $event_array = [];
            $p_type = 'RE';
        }elseif($event_type=='M' && $payment_type == 'onetimemultiple'){
            $payment_array = [];
            $p_type = 'CO';
            $payment_frequency = 4;
        }elseif($event_type=='M' && $payment_type == 'multiplerecurring'){
            $p_type = 'RE';
        }
        if($payment_method=='CC'){
            $check_number = 0;
        }elseif($payment_method=='CH'){
            $cc_id = $cc_state = '';
        }elseif($payment_method=='CA'){
            $cc_id = $cc_state = '';
            $check_number = 0;
        }
        $this->wepayCreateCheckout($optional_discount_flag,$optional_discount,$check_number,$payment_method,$payment_start_date, $total_no_of_payments,$company_id, $event_id, $student_id,$participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, $payment_type, $p_type, $upgrade_status, $total_order_amount, $total_order_quantity, $event_type,$reg_type_user,$reg_version_user,$discount_code);
    }
    
    private function sendExpiryEmailToStudio(){
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($error, 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id)) {
            $this->sendExpiryEmailDetailsToStudio($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function getOptionalEventTotal(){
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id = $processing_fee_type = $payment_frequency = '';
        $registration_amount = $payment_amount = $total_order_quantity = $total_order_amount = $payment_start_date = $total_no_of_payments = $call_back = 0;
        $quantity = 1;
        $event_array = $payment_array = [];
        $reg_type_user='U';        
        $pos_email = $token = $payment_type = '';
        
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        if(isset($this->_request['event_array'])){
            $event_array = $this->_request['event_array'];
        }
        if(isset($this->_request['registration_amount'])){
            $registration_amount = $this->_request['registration_amount'];
        }
        if(isset($this->_request['payment_amount'])){
            $payment_amount = $this->_request['payment_amount'];
        }
        if(isset($this->_request['processing_fee_type'])){
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if(isset($this->_request['quantity'])){
            $quantity = $this->_request['quantity'];
        }
        if(isset($this->_request['event_array'])){
            $event_array = $this->_request['event_array'];
        }
        if(isset($this->_request['payment_array'])){
            $payment_array = $this->_request['payment_array'];
        }
        if(isset($this->_request['payment_type'])){
            $payment_type = $this->_request['payment_type'];
        }
        if(isset($this->_request['total_order_amount'])){
            $total_order_amount = $this->_request['total_order_amount'];
        }
        if(isset($this->_request['total_order_quantity'])){
            $total_order_quantity = $this->_request['total_order_quantity'];
        }
        if(isset($this->_request['total_no_of_payments'])){
            $total_no_of_payments = $this->_request['total_no_of_payments'];
        }
        $payment_method =$event_optional_discount_flag = $event_optional_discount = "";
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['event_optional_discount'])) {
            if(!empty($this->_request['event_optional_discount'])){
                $event_optional_discount = $this->_request['event_optional_discount'];
            }
        }
        if (isset($this->_request['event_optional_discount_flag'])) {
            $event_optional_discount_flag = $this->_request['event_optional_discount_flag'];
        }
        if(isset($this->_request['token'])){
            $token = $this->_request['token'];
        }
        if(isset($this->_request['pos_email'])){
            $pos_email = $this->_request['pos_email'];
        }
         if(isset($this->_request['payment_start_date'])){
            $payment_start_date = $this->_request['payment_start_date'];
        }
        if($reg_type_user !== 'U'){
            $this->checkpossettings($company_id, $payment_method, $pos_email, $token);
        }
         if($reg_type_user === "PP" && (((int)$event_optional_discount > 0) || $event_optional_discount_flag === "Y")){
            $error = array('status' => "Failed", "msg" => "Optional discount is not applicable");
            $this->response($this->json($error), 200);
        }
//         if(($payment_method === "CH" || $payment_method === "CA") && ($payment_type == 'singlerecurring' || $payment_type == 'multiplerecurring')) {
//            $error = array('status' => "Failed", "msg" => "Payment method is not valid for this payment type");
//            $this->response($this->json($error), 200);
//        }


        if (!empty($company_id) && !empty($payment_type)) {
            if ($event_optional_discount_flag == 'Y' && $event_optional_discount > 0) {
                $this->calculateOptionalDiscountForevents($event_optional_discount, $payment_method, $company_id, $payment_amount, $processing_fee_type, $quantity, $event_array, $payment_array, $total_order_amount, $total_order_quantity, $payment_start_date, $payment_type, $total_no_of_payments, $call_back);
            } else {
                $error = array('status' => "Failed", "msg" => "optional discount flag is not set or the optional discount is 0.");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    //    Muthulakshmi
    private function stripeEventCheckout() {
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id = $event_id = $event_name =$event_type= $sub_events_id = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $processing_fee_type = $payment_frequency =$discount_code= '';
        $student_id =$participant_id= $fee = $discount = $registration_amount = $payment_amount = $total_order_quantity = $total_order_amount = 0;
        $quantity = 1;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $event_array = $payment_array = [];
        $upgrade_status = $p_type = 'F';        //$p_type F - Free, CO - One time Checkout, RE - Recurring Type
        $country = 'US';
        $reg_type_user='U';        
        $reg_version_user = '';
        $pos_email = $token = '';
        $optional_discount_flag = 'N';
        $payment_method = "CC";
        $check_number = $optional_discount = $total_no_of_payments = 0;
        $payment_type = 'onetimesingle';
        $payment_start_date = '';
        $intent_method_type = $payment_method_id = $stripe_customer_id = $cc_type = '';
        
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        if(isset($this->_request['event_id'])){
            $event_id = $this->_request['event_id'];
        }
        if(isset($this->_request['studentid'])){
            $student_id = $this->_request['studentid'];
        }
        if(isset($this->_request['participant_id'])){
            $participant_id = $this->_request['participant_id'];
        }
        if(isset($this->_request['event_name'])){
            $event_name = $this->_request['event_name'];
        }
        if(isset($this->_request['desc'])){
            $desc = $this->_request['desc'];
        }
        if(isset($this->_request['event_array'])){
            $event_array = $this->_request['event_array'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if(isset($this->_request['email'])){
            $buyer_email = $this->_request['email'];
        }
        if(isset($this->_request['phone'])){
            $buyer_phone = $this->_request['phone'];
        }
        if(isset($this->_request['postal_code'])){
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if(isset($this->_request['country'])){
            $country = $this->_request['country'];
        }
        if(isset($this->_request['registration_amount'])){
            $registration_amount = $this->_request['registration_amount'];
        }
        if(isset($this->_request['payment_amount'])){
            $payment_amount = $this->_request['payment_amount'];
        }
        if(isset($this->_request['processing_fee_type'])){
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if(isset($this->_request['fee'])){
            $fee = $this->_request['fee'];
        }
        if(isset($this->_request['discount'])){
            $discount = $this->_request['discount'];
        }
        if(isset($this->_request['quantity'])){
            $quantity = $this->_request['quantity'];
        }
        if(isset($this->_request['reg_col1'])){
            $reg_col1 = $this->_request['reg_col1'];
        }
        if(isset($this->_request['reg_col2'])){
            $reg_col2 = $this->_request['reg_col2'];
        }
        if(isset($this->_request['reg_col3'])){
            $reg_col3 = $this->_request['reg_col3'];
        }
        if(isset($this->_request['reg_col4'])){
            $reg_col4 = $this->_request['reg_col4'];
        }
        if(isset($this->_request['reg_col5'])){
            $reg_col5 = $this->_request['reg_col5'];
        }
        if(isset($this->_request['reg_col6'])){
            $reg_col6 = $this->_request['reg_col6'];
        }
        if(isset($this->_request['reg_col7'])){
            $reg_col7 = $this->_request['reg_col7'];
        }
        if(isset($this->_request['reg_col8'])){
            $reg_col8 = $this->_request['reg_col8'];
        }
        if(isset($this->_request['reg_col9'])){
            $reg_col9 = $this->_request['reg_col9'];
        }
        if(isset($this->_request['reg_col10'])){
            $reg_col10 = $this->_request['reg_col10'];
        }
        if(isset($this->_request['payment_frequency'])){
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if(isset($this->_request['event_array']) && !empty($this->_request['event_array'])){
            $event_array = $this->_request['event_array'];
        }
        if(isset($this->_request['payment_array']) && !empty($this->_request['payment_array'])){
            $payment_array = $this->_request['payment_array'];
        }
        if(isset($this->_request['event_type'])){
            $event_type = $this->_request['event_type'];
        }
        if(isset($this->_request['payment_type'])){
            $payment_type = $this->_request['payment_type'];
        }
        if(isset($this->_request['upgrade_status'])){
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if(isset($this->_request['total_order_amount'])){
            $total_order_amount = $this->_request['total_order_amount'];
        }
        if(isset($this->_request['total_order_quantity'])){
            $total_order_quantity = $this->_request['total_order_quantity'];
        }if(isset($this->_request['discount_code'])){
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['check_number'])) {
            if(!empty($this->_request['check_number'])){
            $check_number = $this->_request['check_number'];
            }
        }
        if (isset($this->_request['event_optional_discount'])) {
            if(!empty($this->_request['event_optional_discount'])){
                $optional_discount = $this->_request['event_optional_discount'];
            }
        }
        if (isset($this->_request['event_optional_discount_flag'])) {
            $optional_discount_flag = $this->_request['event_optional_discount_flag'];
        }
        if(isset($this->_request['payment_start_date'])){
            $payment_start_date = $this->_request['payment_start_date'];
        }
        if(isset($this->_request['total_no_of_payments'])){
            $total_no_of_payments = $this->_request['total_no_of_payments'];
        }
        if(isset($this->_request['token'])){
            $token = $this->_request['token'];
        }
        if(isset($this->_request['pos_email'])){
            $pos_email = $this->_request['pos_email'];
        }
        if(isset($this->_request['payment_method_id'])){
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if(isset($this->_request['intent_method_type'])){
            $intent_method_type = $this->_request['intent_method_type'];
        }
        if (isset($this->_request['cc_type'])) {    // cc_type - N - New, E - Existing
            $cc_type = $this->_request['cc_type'];
        }
        if (isset($this->_request['stripe_customer_id'])) {    // cc_type - N - New, E - Existing
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        if($reg_type_user !== 'U'){
            $this->checkpossettings($company_id, $payment_method, $pos_email, $token);
        }
        if($reg_type_user === "PP" && (((int)$optional_discount > 0) || $optional_discount_flag === "Y")){
            $error = array('status' => "Failed", "msg" => "Optional discount is not applicable");
            $this->response($this->json($error), 200);
        }
        if($payment_method === "CH" && empty(trim($check_number))){
            $error = array('status' => "Failed", "msg" => "Please provide check number");
            $this->response($this->json($error), 200);
        }
        if(empty($reg_type_user) || (($reg_type_user === 'PP' || $reg_type_user === 'U') && !empty(trim($student_id)))){
            $error = array('status' => "Failed", "msg" => "Authentication Error");
            $this->response($this->json($error), 200);
        }
        if($payment_amount==0){
            $payment_array = [];
            $p_type = 'F';
            $payment_type = 'free';
            $payment_method = "CA";
            $payment_frequency = 4;
        }
        if($payment_method === "CC" && empty(trim($payment_method_id))){
            if($optional_discount_flag !== "Y"){
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        }
        if(!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)&& !empty($event_type)){
            
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if($event_type=='S' && $payment_type == 'onetimesingle'){
            $event_array = [];
            $payment_array = [];
            $p_type = 'CO';
            $payment_frequency = 4;
        }elseif($event_type=='S' && $payment_type == 'singlerecurring'){
            $event_array = [];
            $p_type = 'RE';
        }elseif($event_type=='M' && $payment_type == 'onetimemultiple'){
            $payment_array = [];
            $p_type = 'CO';
            $payment_frequency = 4;
        }elseif($event_type=='M' && $payment_type == 'multiplerecurring'){
            $p_type = 'RE';
        }
        if($payment_method=='CC'){
            $check_number = 0;
        }elseif($payment_method=='CH'){
            $cc_id = $cc_state = '';
        }elseif($payment_method=='CA'){
            $cc_id = $cc_state = '';
            $check_number = 0;
        }
        $this->stripeCreateCheckout($optional_discount_flag,$optional_discount,$check_number,$payment_method,$payment_start_date, $total_no_of_payments,$company_id, $event_id, $student_id,$participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, $payment_type, $p_type, $upgrade_status, $total_order_amount, $total_order_quantity, $event_type,$reg_type_user,$reg_version_user,$discount_code,$payment_method_id,$intent_method_type,$cc_type,$stripe_customer_id);
    }

} 

// Initiiate Library
    $api = new EventApi;
    $api->processApi();



?>