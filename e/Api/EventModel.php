<?php

require_once __DIR__."/../../Globals/cont_log.php";
require_once 'WePay.php';
require_once 'PHPMailer_5.2.1/class.phpmailer.php';
require_once 'PHPMailer_5.2.1/class.smtp.php';
//require_once __DIR__.'/../../aws/aws-autoloader.php';
require_once __DIR__.'/../../aws/aws.phar';
require_once '../../Stripe/init.php';

use Aws\Sns\SnsClient;

class EventModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $server_url;
    private $stripe_pk = '', $stripe_sk = '';

    public function __construct() {
        require_once __DIR__."/../../Globals/config.php";
        require_once '../../Globals/stripe_props.php';
        $this->inputs();
        $this->wp = new wepay();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = __DIR__."/../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'dev.mystudio.academy') !== false){  //Development
            $this->server_url = "http://dev.mystudio.academy";
        }elseif(strpos($host, 'dev2.mystudio.academy') !== false){  //Development
            $this->server_url = "http://dev2.mystudio.academy";
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME'];
        }else{
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    private function sendEmailForEvent($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list,$student_cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if(!empty(trim($student_cc_email_list))){
                $student_cc_addresses = explode(',', $student_cc_email_list);
                for($init_1=0;$init_1<count($student_cc_addresses);$init_1++){
                    if(filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($student_cc_addresses[$init_1]);
                    }
                }
            }
            
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_policies.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
    
     //Get Event details by Company Code
    protected function getEventsDetailByCompany($company_id, $reg_type_user) {
        $this->getStripeKeys();
        $stripe_publish_key = $this->stripe_pk;
        $this->getStudioSubscriptionStatus($company_id);
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $company_name = $logo_URL = $upgrade_status = $wp_currency_code = $wp_currency_symbol = '';
        $wepay_status = 'N';
        $currency = [];
        $stripe_upgrade_status = $stripe_subscription = '';
        
        $selecsql=sprintf("SELECT c.event_enabled, s.event_status FROM `company` c LEFT JOIN `studio_pos_settings` s ON c.company_id=s.company_id WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $selectresult= mysqli_query($this->db,$selecsql);
        if (!$selectresult) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else{
            if (mysqli_num_rows($selectresult) > 0) {
                $statusoutput = mysqli_fetch_assoc($selectresult);
                $event_enable_flag = $statusoutput['event_enabled'];
                if($statusoutput['event_enabled']=='N'){
                    $res = array('status' => "Failed", "msg" => "Landing page is disabled. Please contact the business operator for more information.");
                    $this->response($this->json($res), 200);
                }
                if($reg_type_user!='U' && $statusoutput['event_status']=='D'){
                    $res = array('status' => "Failed", "msg" => "Events POS Setting is disabled. Please enable this in POS Settings.");
                    $this->response($this->json($res), 200);
                }
            }
        }
        
        $sql = sprintf("SELECT  `event_id` , c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`, c.`company_id`, c.`logo_URL`, c.`upgrade_status` , c.`event_enabled`,  `parent_id` ,  `event_type` ,  e.`event_status` ,  `event_begin_dt` ,  `event_end_dt` ,  `event_title` ,  `event_category_subtitle` ,  
            `event_desc` ,  `event_more_detail_url`, `event_video_detail_url` , `event_cost` ,  `event_compare_price` ,  `registrations_count` ,  `event_capacity` ,  `capacity_text` ,  `processing_fees`, `event_onetime_payment_flag`, 
            `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `net_sales` ,  `event_sort_order` ,  
            `event_banner_img_url`, `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, 
            `event_registration_column_2`, `event_reg_col_2_mandatory_flag`,`event_registration_column_3`, `event_reg_col_3_mandatory_flag`,
            `event_registration_column_4`, `event_reg_col_4_mandatory_flag`,`event_registration_column_5`, `event_reg_col_5_mandatory_flag`,
            `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, `event_reg_col_7_mandatory_flag`,
            `event_registration_column_8`, `event_reg_col_8_mandatory_flag`,`event_registration_column_9`, `event_reg_col_9_mandatory_flag`,
            `event_registration_column_10`, `event_reg_col_10_mandatory_flag`,`event_show_status`, c.upgrade_status, c.`stripe_status`,c.`country`, c.`stripe_subscription`, pos.`event_payment_method`,
            IF( e.`event_status` = 'S', 'draft', IF( `event_type`='M', 'past', IF( DATE(`event_begin_dt`) <= ($currdate_add), IF(DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00', 'live', 'past' ) , 'live' ))) list_type,
            wp.account_state, wp.currency
            FROM  `event` e LEFT JOIN `company` c ON e.`company_id`=c.`company_id` 
            LEFT JOIN `wp_account` wp ON e.`company_id`=wp.`company_id`
            LEFT JOIN `studio_pos_settings` pos ON e.`company_id` = pos.`company_id`
            WHERE  e.`company_id` = '%s'
            AND  e.`event_status` =  'P' ORDER BY `event_sort_order` desc",mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $output = $parent_mapping = $parent_integrated = $response['live'] = $response['draft'] = $response['past'] = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $company_name = $row['company_name'];
                    if($row['logo_URL']){
                        $logo_URL = $row['logo_URL'];
                    }else{
                        $logo_URL = $this->server_url."/uploads/Default/default_logo.png";
                    }
                    $upgrade_status = $row['upgrade_status'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    $pos_event_payment_method = $row['event_payment_method'];
                    $stripe_upgrade_status = $row['stripe_status'];
                    $stripe_subscription = $row['stripe_subscription'];
                    $country = $row['country'];
                    if(!empty($row['account_state'])){
                        $account_state = $row['account_state'];
                        $curr = $row['currency'];
                        if(!empty(trim($curr))){
                            $currency = explode(",", trim($curr));
                        }
                        $wepay_status = 'Y';
                    }
                    $disc = $this->getDiscountDetails($company_id, $row['event_id']);
                    $event_enable_flag = $row['event_enabled'];
                    if($event_enable_flag=='N'){
                        $res = array('status' => "Failed", "msg" => "Landing page is disabled. Please contact the business operator for more information.");
                        $this->response($this->json($res), 200);
                    }
                    $row['discount'] = $disc['discount'];
                    $row['child_events']=[];
                    $reg_field_name_array = [];
                    $temp_reg_field = "event_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "event_reg_col_".$z."_mandatory_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row['reg_columns'] = $reg_field_name_array;
                    if($row['event_type']=='M'){
                        $parent_mapping[$row['event_id']] = count($parent_integrated);
                        $parent_integrated[] = $row;
                    }
                    if($row['event_type']=='S' && empty($row['parent_id'])){
                        $parent_integrated[] = $row;
                    }
                    $output[] = $row;
                }
                for($i=0;$i<count($output);$i++){//integrate the child event with parent event for multiple event
                    if(!empty(trim($output[$i]['parent_id']))){                        
                        $parent_integrated[$parent_mapping[$output[$i]['parent_id']]]['child_events'][] = $output[$i];
//                        if($output[$i]['list_type']=='live'){//default parent status - past, when atleast a child event is in live then parent status changed to live
//                            $parent_integrated[$parent_mapping[$output[$i]['parent_id']]]['list_type'] = 'live';
//                        }
                    }
                }
                $parent_count = count($parent_integrated);                
                for($i=0;$i<$parent_count;$i++){//split-up by live, past & draft
                    if($parent_integrated[$i]['event_show_status'] == 'Y'){
                        $child_count = count($parent_integrated[$i]['child_events']);
                        if ($child_count > 0) {
                            usort($parent_integrated[$i]['child_events'], function($a, $b) {
                                return $a['event_begin_dt'] > $b['event_begin_dt'];
                            });

                            $check_type_conflict = $cevent_capacity = 0;
                            $inital_child_type = $parent_integrated[$i]['child_events'][0]['list_type'];
                            $parent_integrated[$i]['list_type'] = $inital_child_type;

                            for($j=0;$j<$child_count;$j++){
                                if($parent_integrated[$i]['child_events'][$j]['event_show_status'] == "Y"){
                                    $cevent_capacity += $parent_integrated[$i]['child_events'][$j]['event_capacity'];
                                    $current_child_type = $parent_integrated[$i]['child_events'][$j]['list_type'];
                                    if($j>0 && $current_child_type != $inital_child_type){
                                        if($check_type_conflict==0){
                                            $check_type_conflict = 1;
                                            $parent_integrated[] = $parent_integrated[$i];
                                            $parent_count++;
                                            $parent_integrated[count($parent_integrated)-1]['list_type'] = $current_child_type;
                                            $parent_integrated[count($parent_integrated)-1]['child_events'] = [];
                                            $parent_integrated[count($parent_integrated)-1]['child_events'][] = $parent_integrated[$i]['child_events'][$j];
                                            array_splice($parent_integrated[$i]['child_events'],$j, 1);
                                            $j--;
                                            $child_count--;
                                        }else{
                                            $parent_integrated[count($parent_integrated)-1]['child_events'][] = $parent_integrated[$i]['child_events'][$j];
                                            array_splice($parent_integrated[$i]['child_events'], $j, 1);
                                            $child_count--;
                                            $j--;
                                        }
                                    }
                                }else{
                                    array_splice($parent_integrated[$i]['child_events'], $j, 1);
                                    $child_count--;
                                    $j--;
                                }                  
                            }
                            $parent_integrated[$i]['event_capacity'] = $cevent_capacity;
                        }
                        if($parent_integrated[$i]['list_type']=='live'){
                            $response['live'][] = $parent_integrated[$i];                        
                        }elseif($parent_integrated[$i]['list_type']=='draft'){
                            $response['draft'][] = $parent_integrated[$i];
                        }elseif($parent_integrated[$i]['list_type']=='past'){
                            $response['past'][] = $parent_integrated[$i];
                        }
                    }
                }
                usort($response['live'], function($a, $b) {
                    return $a['event_sort_order'] < $b['event_sort_order'];
                });
                
                $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['event_enable_flag'] = $event_enable_flag;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $country_payment_specific = $this->checkCountryPaymentSupport($company_id,$country,1); // callback - 1/0
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = preg_replace('~[\r\n]+~', '', $process_fee['PT']);
                $company_details['pos_event_payment_method'] = $pos_event_payment_method;
                //stripe set
                $company_details['stripe_publish_key'] = $stripe_publish_key;  // KEY FROM - Globals/stripe_props.php
                $company_details['stripe_upgrade_status'] = $stripe_upgrade_status;
                $company_details['stripe_subscription'] = $stripe_subscription;
                $company_details['stripe_country_support'] = $stripe_country_support = ($country_payment_specific['status'] == 'Success' && $country_payment_specific['support_status'] =='Y' && $country_payment_specific['payment_support'] =='S') ? "Y":"N";  // CHECK COUNTRY SUPPORT STATUS FOR STRIPE
                $out = array('status' => "Success", 'msg' => $response['live'], "company_details" => $company_details);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
                    
            } else {
                $error = array('status' => "Failed", "msg" => "No events available.");
                $this->response($this->json($error), 401); // If no records "No Content" status
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
//    private function getProcessingFee($upgrade_status){
//        $temp=[];
//        $x=0;
//        $file_handle = fopen($this->processing_fee_file_name, "r");
//        while (!feof($file_handle)) {
//            $lines = fgets($file_handle);
//            $line_contents = explode(",", $lines);
//            for($i=0;$i<count($line_contents);$i++){                
//                $cnt = explode("=", $line_contents[$i]);
//                $temp[$x][$cnt[0]] = $cnt[1];
//            }
//            $x++;
//        }
//        fclose($file_handle);
//        
//        for($i=0;$i<count($temp);$i++){
//            if($temp[$i]['level']==$upgrade_status){
//                return $temp[$i];
//            }
//        }
//    }
    
    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }
    
    private function checkEventWithcompany($company_id,$event_id){
        $query = sprintf("SELECT * FROM `event` WHERE `company_id`='%s' AND `event_id`='%s' AND `event_status`='P'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
            }else{
                $error = array('status' => "Failed", "msg" => "The event is not available.");
                $this->response($this->json($error), 401);
            }
        }
    }
    
    protected function getEventsDetailByEventId($company_id, $event_id, $reg_type_user){
        $this->getStripeKeys();
        $stripe_publish_key = $this->stripe_pk;
        $this->getStudioSubscriptionStatus($company_id);
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $this->checkEventWithcompany($company_id,$event_id);
        $list_type = 'live';
        $company_name = $logo_URL = $upgrade_status = $wp_currency_code = $wp_currency_symbol = $event_enable_flag = '';
        $wepay_status = 'N';
        $currency = [];
        $child_flag = false;
        
        
        $selecsql=sprintf("SELECT c.event_enabled, s.event_status FROM `company` c LEFT JOIN `studio_pos_settings` s ON c.company_id=s.company_id WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $selectresult= mysqli_query($this->db,$selecsql);
        if (!$selectresult) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else{
            if (mysqli_num_rows($selectresult) > 0) {
                $statusoutput = mysqli_fetch_assoc($selectresult);
                $event_enable_flag = $statusoutput['event_enabled'];
                if($statusoutput['event_enabled']=='N'){
                    $res = array('status' => "Failed", "msg" => "Landing page is disabled. Please contact the business operator for more information.");
                    $this->response($this->json($res), 200);
                }
                if($reg_type_user!='U' && $statusoutput['event_status']=='D'){
                    $res = array('status' => "Failed", "msg" => "Events POS Setting is disabled. Please enable this in POS Settings.");
                    $this->response($this->json($res), 200);
                }
            }
        }
        
//        $sql1 = sprintf("SELECT `event_enabled` FROM  `company` WHERE `company_id` = '%s'", mysqli_real_escape_string($this->db,$company_id));
//        $result1 = mysqli_query($this->db, $sql1);
//        if (!$result1) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 500);
//        } else {
//            $num_of_rows1 = mysqli_num_rows($result1);
//            if ($num_of_rows1 > 0) {
//                while($row = mysqli_fetch_assoc($result1)) {
//                    $event_enable_flag = $row['event_enabled'];
//                }
//            }
//        }
//        if($event_enable_flag=='N'){
//            $res = array('status' => "Failed", "msg" => "Landing page is disabled. Please contact the business operator for more information.");
//            $this->response($this->json($res), 200);
//        }else{
//        $sql = sprintf("SELECT  `event_id` , c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`,  c.`company_id` , c.`logo_URL`, c.`upgrade_status`,  `parent_id` ,  `event_type` ,  `event_status` ,  `event_begin_dt` ,  `event_end_dt` ,  `event_title` ,  `event_category_subtitle` ,  
//            `event_desc` ,  `event_more_detail_url`, `event_video_detail_url` , `event_cost` ,  `event_compare_price` ,  `registrations_count` ,  `event_capacity` ,  `capacity_text` ,  `processing_fees`, `event_onetime_payment_flag`, 
//            `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `net_sales` ,  `event_sort_order` ,  
//            `event_banner_img_url`, `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, 
//            `event_registration_column_2`, `event_reg_col_2_mandatory_flag`,`event_registration_column_3`, `event_reg_col_3_mandatory_flag`,
//            `event_registration_column_4`, `event_reg_col_4_mandatory_flag`,`event_registration_column_5`, `event_reg_col_5_mandatory_flag`,
//            `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, `event_reg_col_7_mandatory_flag`,
//            `event_registration_column_8`, `event_reg_col_8_mandatory_flag`,`event_registration_column_9`, `event_reg_col_9_mandatory_flag`,
//            `event_registration_column_10`, `event_reg_col_10_mandatory_flag`, c.upgrade_status,
//            IF( `event_status` = 'S', 'draft', IF( `event_type`='M', 'past', IF( DATE(`event_begin_dt`) <= ($currdate_add), IF(DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00', 'live', 'past' ) , 'live' ))) list_type,
//            wp.account_state, wp.currency
//            FROM  `event` e LEFT JOIN `company` c ON e.`company_id`=c.`company_id` 
//            LEFT JOIN `wp_account` wp ON e.`company_id`=wp.`company_id`
//            WHERE (`event_id` = '%s' or `parent_id` = '%s')
//            AND  `event_status` =  'P' ORDER BY  `parent_id` ASC, `event_begin_dt` ASC", mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$event_id));
        
            $sql = sprintf("SELECT * FROM (SELECT  `event_id` , c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`,c.`stripe_subscription`, c.`country`, c.`stripe_status`, c.`company_id` , c.`logo_URL`, c.`upgrade_status`,  `parent_id` ,  `event_type` ,  e.`event_status` ,  `event_begin_dt` ,  `event_end_dt` ,  `event_title` ,  `event_category_subtitle` ,  
                `event_desc` ,  `event_more_detail_url`, `event_video_detail_url` , `event_cost` ,  `event_compare_price` ,  `registrations_count` ,  `event_capacity` ,  `capacity_text` ,  `net_sales` ,  `event_sort_order` ,  
                `event_banner_img_url`,`event_show_status`,  
                IF( e.`event_status` = 'S', 'draft', IF( `event_type`='M', 'past', IF( DATE(`event_begin_dt`) <= ($currdate_add), IF(DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00', 'live', 'past' ) , 'live' ))) list_type,
                wp.account_state, wp.currency
                FROM  `event` e LEFT JOIN `company` c ON e.`company_id`=c.`company_id` 
                LEFT JOIN `wp_account` wp ON e.`company_id`=wp.`company_id`
                WHERE (`event_id` = '%s' or `parent_id` = '%s')
                AND  e.`event_status` =  'P' ORDER BY  `parent_id` ASC, `event_begin_dt` ASC) a JOIN 
                (SELECT event_id e_id, `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, 
                `event_registration_column_2`, `event_reg_col_2_mandatory_flag`,`event_registration_column_3`, `event_reg_col_3_mandatory_flag`,
                `event_registration_column_4`, `event_reg_col_4_mandatory_flag`,`event_registration_column_5`, `event_reg_col_5_mandatory_flag`,
                `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, `event_reg_col_7_mandatory_flag`,
                `event_registration_column_8`, `event_reg_col_8_mandatory_flag`,`event_registration_column_9`, `event_reg_col_9_mandatory_flag`,
                `event_registration_column_10`, `event_reg_col_10_mandatory_flag`, `event_title` as parent_event_title,`processing_fees`, `event_onetime_payment_flag`, 
                `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`  FROM event WHERE 
                `event_id` = (SELECT IFNULL(parent_id, event_id) FROM event WHERE event_id = '%s')) b ON 1", mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$event_id));
            $result = mysqli_query($this->db, $sql);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $child = [];
                $disc = [];
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        if($num_of_rows==1 && $row['event_type']=='C'){
                              $child_flag = true;
    //                        $error = array('status' => "Failed", "msg" => "The event is not available.");
    //                        $this->response($this->json($error), 401); // If no records "No Content" status
                        }
                        $company_name = $row['company_name'];
                        if(!empty(trim($row['logo_URL']))){
                            $logo_URL = $row['logo_URL'];
                        }else{
                            $logo_URL = $this->server_url."/uploads/Default/default_logo.png";
                        }
                        $upgrade_status = $row['upgrade_status'];
                        $wp_currency_code = $row['wp_currency_code'];
                        $wp_currency_symbol = $row['wp_currency_symbol'];
                        $stripe_upgrade_status = $row['stripe_status'];
                        $stripe_subscription = $row['stripe_subscription'];
                        $country = $row['country'];
                        if(!empty($row['account_state'])){
                            $account_state = $row['account_state'];
                            $curr = $row['currency'];
                            if(!empty(trim($curr))){
                                $currency = explode(",", trim($curr));
                            }
                            $wepay_status = 'Y';
                        }
                        if($row['event_type']=='C' && $child_flag){
                            $disc = $this->getDiscountDetails($company_id, $row['parent_id']);
                        }else{
                            $disc = $this->getDiscountDetails($company_id, $row['event_id']);
                        }
                        $row['discount'] = $disc['discount'];
                        $reg_field_name_array = [];
                        $temp_reg_field = "event_registration_column_";
                        for($z=1;$z<=10;$z++){ //collect registration fields as array
                            $y=$z-1;
                            $reg_field = $temp_reg_field."$z";      //for filed names
                            $reg_field_mandatory = "event_reg_col_".$z."_mandatory_flag";
                            if($row[$reg_field]!=''){
                                $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                            }
                        }
                        $row['reg_columns'] = $reg_field_name_array;
                        if($row['event_type']=='M' || $row['event_type']=='S' || ($row['event_type']=='C' && $child_flag)){
                            $row['child_events'] = [];
                            if(($row['list_type']=='live' && ($row['event_type']=='S' || $row['event_type']=='C')) || $row['event_type']=='M'){
                                $response = $row;
                            }
                        }else{
                            if($row['event_show_status']=='Y'){
                                $child[] = $row;
                            }
                        }
                    }

                    if(count($child)>0){
                        usort($child, function($a, $b) {
                            return $a['event_begin_dt'] > $b['event_begin_dt'];
                        });
                        $check_child_type = 0;                    
                        for($i=0;$i<count($child);$i++){
                            if($list_type == '' || $list_type == $child[$i]['list_type']){
                                if($check_child_type==0){
                                    $response['list_type'] = $child[$i]['list_type'];
                                    $check_child_type++;
                                }
                                $response['child_events'][] = $child[$i];
                            }
                        }
                    }else{
                        $response['child_events']=[];
                    }
                    if(!isset($response['event_id']) && !isset($response['list_type']) && empty($response['child_events'])){
                        $error = array('status' => "Failed", "msg" => "The event is not available.");
                        $this->response($this->json($error), 401); // If no records "No Content" status
                    }
                    $company_details = [];
                    $company_details['company_name'] = $company_name;
                    $company_details['logo_URL'] = $logo_URL;
                    $company_details['upgrade_status'] = $upgrade_status;
                    $company_details['wp_currency_code'] = $wp_currency_code;
                    $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                    $company_details['wepay_status'] = $wepay_status;
                    $company_details['wp_client_id'] = $this->wp_client_id;
                    $company_details['wp_level'] = $this->wp_level;
                    $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                    $country_payment_specific = $this->checkCountryPaymentSupport($company_id,$country,1); // callback - 1/0
                    //stripe set
                    $company_details['stripe_publish_key'] = $stripe_publish_key;  // KEY FROM - Globals/stripe_props.php
                    $company_details['stripe_upgrade_status'] = $stripe_upgrade_status;
                    $company_details['stripe_subscription'] = $stripe_subscription;
                    $company_details['stripe_country_support'] = $stripe_country_support = ($country_payment_specific['status'] == 'Success' && $country_payment_specific['support_status'] =='Y' && $country_payment_specific['payment_support'] =='S') ? "Y":"N";  // CHECK COUNTRY SUPPORT STATUS FOR STRIPE
                    $company_details['processing_fee_percentage'] = $process_fee['PP'];
                    $company_details['processing_fee_transaction'] = preg_replace('~[\r\n]+~', '', $process_fee['PT']);
                    $out = array('status' => "Success", 'msg' => $response, "company_details" => $company_details);
                    // If success everythig is good send header as "OK" and user details
                    $this->response($this->json($out), 200);
                } else {
                    $error = array('status' => "Failed", "msg" => "The event is not available.");
                    $this->response($this->json($error), 401); // If no records "No Content" status
                }
            }
            date_default_timezone_set($curr_time_zone);
//        }
    }
    
    //Get Discount details by Event ID
    protected function getDiscountDetails($company_id, $event_id){
        $sql = sprintf("SELECT `event_discount_id` , `discount_type` , `discount_code` , `discount_amount` FROM `event_discount`
                WHERE `company_id` = '%s' AND `event_id`='%s' AND `is_expired`!='Y'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id));
        $result = mysqli_query($this->db, $sql);
        $output = $response = [];
        if($result){
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $output[] = $row;
                }
                $response['status'] = "Success";
            }else{
                $response['status'] = "Failed";
            }
        }else{
            $response['status'] = "Failed";
        }
        $response['discount'] = $output;
        return $response;
    }    
    
    protected function wepayStatus($company_id){
        $query = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if(!empty(trim($curr))){
                    $currency = explode(",", trim($curr));
                }
                if($row['account_state']=='active'){
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                }else{
                    $res = array('status' => "Success", "msg" => "A", "info" => "Deleted/Disabled/Action needed.");
                }
            }else{
                $res = array('status' => "Failed", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }
    
    protected function stripeStatus($company_id){
        $query = sprintf("SELECT * FROM `stripe_account` where `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        
        $account_state = '';
        
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if(!empty(trim($curr))){
                    $currency = explode(",", trim($curr));
                }
                if($row['account_state'] === 'Y'){
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                }else{
                    $res = array('status' => "Success", "msg" => "Y", "info" => "Deleted/Disabled/Action needed.");
                }
            }else{
                $res = array('status' => "Failed", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }
        
    private function calculateProcessingFee($amount, $fee_type, $process_fee_per, $process_fee_val) {
        $p1_fee = ($amount * ($process_fee_per / 100)) + $process_fee_val;
        $p2_fee = ($amount + $p1_fee) * ($process_fee_per / 100) + $process_fee_val;
        if ($fee_type === 2 || $fee_type === '2') {
            $p_fee = $p2_fee;
        } else {
            $p_fee = $p1_fee;
        }
        return $p_fee;
    }
                       
    protected function wepayCreateCheckout($optional_discount_flag,$optional_discount,$check_number,$payment_method, $payment_start_date, $total_no_of_payments,$company_id, $event_id, $actual_student_id,$actual_participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $pay_amount, $fee, $processing_fee_type, $discount, $quantity, $ev_array, $pay_array, $payment_frequency, $payment_type, $p_type, $upgrade_status, $total_order_amount, $total_order_quantity, $event_type,$reg_type_user,$reg_version_user,$discount_code){
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $upgrade_status = $sts['upgrade_status'];
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $recurring_payment_status = '';
        $checkout_amount = 0;
        $payment_amount = $pay_amount;
        $event_array = $ev_array;
        $payment_array = $pay_array;
//        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $this->checkQuantityDuringCheckout($company_id,$event_id,$ev_array,$quantity, $event_type);
        if($optional_discount_flag=='Y' && $optional_discount>0){
            $opt_disc = $this->calculateOptionalDiscountForevents($optional_discount, $payment_method, $company_id, $pay_amount, $processing_fee_type, $quantity, $ev_array, $pay_array, $total_order_amount, $total_order_quantity, $payment_start_date, $payment_type, $total_no_of_payments, 1);
            if($opt_disc['status']=='Success'){
                $recurring_payment_status = $opt_disc['recurring_payment_status'];
                $payment_amount = $opt_disc['total_payment_amount'];
                $checkout_amount = $opt_disc['payment_amount_wo_fee'];
                $event_array = $opt_disc['event_array'];
                $payment_array = $opt_disc['payment_array'];
            }else{
                $error = array("status" => "Failed", "msg" => $opt_disc['msg']);
                $this->response($this->json($error), 200);
            }
        }
        $buyer_name = $buyer_first_name.' '.$buyer_last_name;
        if (is_null($actual_student_id) || $actual_student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'wepay', '');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $actual_student_id;
        }
        if (is_null($actual_participant_id) || $actual_participant_id == 0) {
            $participant_id = $this->getParticipantIdForEvent($company_id, $student_id, $reg_col1, $reg_col2);
        } else {
            $participant_id = $actual_participant_id;
        }
        $balance_due = $deposit_amt = $pr_fee = $temp_pr_fee = 0;
        $authorize_only = 0;
        $no_of_payments = 1;
        $payment_startdate = $current_date = date("Y-m-d");
//        $check_deposit_failure = 0;
        $p_f = $this->getProcessingFee($company_id,$upgrade_status);
        $process_fee_per = $p_f['PP'];
        $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);
        if($fee==0 && $processing_fee_type==1){
            $fee = number_format((float)($payment_amount*$process_fee_per/100+$process_fee_val), 2, '.', '');
        }
        if($payment_method!='CC'){
            $fee = 0;
        }
        if(count($payment_array)>0){
            $no_of_payments = count($payment_array)-1;
            for($i=0;$i<count($payment_array);$i++){
                if(isset($payment_array[$i]['date']) && empty($payment_array[$i]['date'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),400);
                }            
                if(isset($payment_array[$i]['amount']) && empty($payment_array[$i]['amount'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),400);
                }
                if(isset($payment_array[$i]['type']) && empty($payment_array[$i]['type'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),400);
                }
                
                if($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount']>0){
//                    $balance_due = $payment_array[$i]['amount'];
                    $deposit_amt = $payment_array[$i]['amount'];
                    if($optional_discount_flag!='Y'){
                        $checkout_amount = $deposit_amt;
                    }
                    if($payment_method!='CC'){
                        $balance_due = $payment_array[$i]['amount'];
                    }
                    $pr_fee = $payment_array[$i]['processing_fee'];
                }elseif($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount'] == 0){
                    $authorize_only = 1;
                    $no_of_payments = count($payment_array);
                }
                $temp_pr_fee += $payment_array[$i]['processing_fee'];
            }
            $fee = $temp_pr_fee;
        }else{
            $pr_fee = $fee;
            if($optional_discount_flag!='Y'){
                $checkout_amount = $payment_amount;
            }
        }
        
        usort($event_array, function($a, $b) {
            return $a['event_begin_dt'] > $b['event_begin_dt'];
        });
        
        if(count($event_array)>0){
            for($i=0;$i<count($event_array);$i++){
                if(isset($event_array[$i]['event_id']) && empty($event_array[$i]['event_id'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),400);
                }            
                if(isset($event_array[$i]['event_quantity']) && empty($event_array[$i]['event_quantity'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),400);
                }
                if(!isset($event_array[$i]['event_cost'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),400);
                }
                if(!isset($event_array[$i]['event_discount_amount'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),400);
                }            
                if(!isset($event_array[$i]['event_payment_amount'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),400);
                }            
                if(!isset($event_array[$i]['event_begin_dt'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),400);
                }
                if(count($payment_array)>0){
                    if($balance_due>0){
                        $amt = $event_array[$i]['event_payment_amount'];
                        if($balance_due>=$event_array[$i]['event_payment_amount']){
                            if($balance_due==$amt){
                                $t_amt = $amt;
                            }else{
                                $t_amt = $balance_due - $amt;
                            }
                            $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($t_amt, $processing_fee_type, $process_fee_per, $process_fee_val);
                            $event_array[$i]['balance_due'] = 0;
                            $balance_due = $balance_due - $amt;
                        }else{
                            $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($balance_due, $processing_fee_type, $process_fee_per, $process_fee_val);
                            $event_array[$i]['balance_due'] = $amt - $balance_due;
                            $balance_due = 0;
                        }
                    }elseif($balance_due==0){
                        $event_array[$i]['processing_fee'] = 0;
                        $event_array[$i]['balance_due'] = $event_array[$i]['event_payment_amount'];
                    }
                }else{
                    if($payment_method=='CC'){
                        $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($event_array[$i]['event_payment_amount'], $processing_fee_type, $process_fee_per, $process_fee_val);
                    }else{
                        $event_array[$i]['processing_fee'] = 0;
                    }
                    $event_array[$i]['balance_due'] = 0;
                }
            }
        }
        
        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency` FROM `wp_account` WHERE `company_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                if($user_state=='deleted'){
                    $msg = "Wepay user registered was deleted.";
                    $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
                    $this->response($this->json($log),200);
                }
                
                if($acc_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log),200);
                }elseif($acc_state=='disabled'){
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log),200);
                }
                
                if(!empty($access_token) && !empty($account_id)){
                    if (!function_exists('getallheaders')){
                        if(!function_exists ('apache_request_headers')){
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        }else{
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    }else{
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        
                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                    
//                    $json = array("account_id"=>"$account_id");
//                    $postData = json_encode($json);
//                    $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent,$sns_msg);
//                    $this->wp->log_info("wepay_get_account: ".$response['status']);
//                    if($response['status']=='Success'){
//                        $res = $response['msg'];
//                        $account_state = $res['state'];
//                        $currency = $res['currencies'][0];
//                        $curr_acc = implode(",", $res['currencies']);
//                        $disabled_reasons = implode(",", $res['disabled_reasons']);
//                        if($account_state=='deleted'){
//                            $msg = "Wepay Account associated with this MyStudio account was deleted.";
//                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
//                            $this->response($this->json($log),200);
//                        }elseif($account_state=='disabled'){
//                            $msg = "Wepay Account associated with this MyStudio account was disabled(For ".$disabled_reasons.").";
//                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
//                            $this->response($this->json($log),200);
//                        }
//                    }else{
//                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                        $this->response($this->json($log),200);
//                    }
                    
                    $paid_amount = $paid_fee = $gross = 0;
                    $checkout_id = $checkout_state = '';                    
                    $cc_name = $cc_month = $cc_year = '';
                    $cc_status = $cc_state;
                    
                    if($payment_method == "CC"){
                        if($payment_amount>0 && $authorize_only==0){
                            $checkout_id_flag = 1;
                            $rand = mt_rand(1,99999);
                            $ip = $this->getRealIpAddr();
                            $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout";
                            $unique_id = $company_id."_".$event_id."_".$ip."_".$rand."_".time();
                            $date = date("Y-m-d_H:i:s");
                            $ref_id = $company_id."_".$event_id."_".$date;
    //                        if($processing_fee_type==2){
    //                            $fee_payer = "payer_from_app";
    //                        }elseif($processing_fee_type==1){
                                $fee_payer = "payee_from_app";
    //                        }
//                            $paid_amount = $payment_amount;
                            $paid_fee = $fee;
                            if(count($payment_array)>0){
                                for($i=0;$i<count($payment_array);$i++){
                                    if($payment_array[$i]['type']=='D'){
//                                        $paid_amount = $payment_array[$i]['amount'];
                                        $paid_fee = $payment_array[$i]['processing_fee'];
                                        break;
                                    }
                                }
                            }

                            if($processing_fee_type==2){
                                $w_paid_amount = $payment_amount + $fee;
                            }elseif($processing_fee_type==1){
                                $w_paid_amount = $payment_amount;
                            }
                            if (count($payment_array) > 0) {
                                for ($i = 0; $i < count($payment_array); $i++) {
                                    if ($payment_array[$i]['type'] == 'D') {
                                        if ($processing_fee_type == 2) {
                                            $w_paid_amount = $payment_array[$i]['amount'] + $payment_array[$i]['processing_fee'];
                                        } elseif ($processing_fee_type == 1) {
                                            $w_paid_amount = $payment_array[$i]['amount'];
                                        }
                                        break;
                                    }
                                }
                            }
                            $time = time();
                            $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                                "fee"=>array("app_fee"=>$paid_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                                "email_message"=>array("to_payee"=>"Payment has been added for $event_name","to_payer"=>"Payment has been made for $event_name"),
                                "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                                "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                    array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                    array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code","country"=>"$country")))),
                                "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                    "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                            $postData2 = json_encode($json2);
                            $sns_msg['call'] = "Checkout";
                            $sns_msg['type'] = "Checkout Creation";
                            $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg);
                            $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                            if($response2['status']=="Success"){
                                $res2 = $response2['msg'];
                                $checkout_id = $res2['checkout_id'];
                                $checkout_state = $res2['state'];
                                $gross = $res2['gross'];
    //                            $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                            }else{
                                if($response2['msg']['error_code']==1008){
                                    $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg);
                                    $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                                    if($response2['status']=="Success"){
                                        $res2 = $response2['msg'];
                                        $checkout_id = $res2['checkout_id'];
                                        $checkout_state = $res2['state'];
                                        $gross = $res2['gross'];
    //                                    $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                                    }else{
                                        if($response2['msg']['error_code']==1008){
                                            $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                            $this->response($this->json($error),400);
                                        }else{
                                            $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                            $this->response($this->json($error),400);
                                        }
                                    }
                                }else{
                                    $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                    $this->response($this->json($error),400);
                                }
                            }
                        }else{
                            $checkout_id_flag = 0;
                            if(!empty(trim($cc_id))){
                                $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                                $postData3 = json_encode($json3);
                                $sns_msg['call'] = "Credit Card";
                                $sns_msg['type'] = "Credit Card details";
                                $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg);
                                $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                                if($response3['status']=="Success"){
                                    $res3 = $response3['msg'];
                                    if($res3['state']=='new'){
                                        $json2 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id,"account_id"=>$account_id);
                                        $postData2 = json_encode($json2);
                                        $sns_msg['call'] = "Credit Card";
                                        $sns_msg['type'] = "Credit Card Authorization";
                                        $response2 = $this->wp->accessWepayApi("credit_card/authorize",'POST',$postData2,$token,$user_agent,$sns_msg);
                                        $this->wp->log_info("wepay_credit_card_authorize: ".$response2['status']);
                                        if($response2['status']=="Success"){
                                            $res2 = $response2['msg'];
                                            $cc_name = $res2['credit_card_name'];
                                            $cc_new_state = $res2['state'];
                                            if($cc_state!=$cc_new_state){
                                                $cc_status = $cc_new_state;
                                            }
                                            $cc_month = $res2['expiration_month'];
                                            $cc_year = $res2['expiration_year'];
                                        }else{
                                            $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                            $this->response($this->json($error),400);
                                        }
                                    }elseif($res3['state']=='authorized'){
                                        $cc_name = $res3['credit_card_name'];
                                        $cc_new_state = $res3['state'];
                                        $cc_month = $res3['expiration_month'];
                                        $cc_year = $res3['expiration_year'];
                                    }else{
                                        if($res3['state']=='expired'){
                                            $msg = "Given credit card is expired.";
                                        }elseif($res3['state']=='deleted'){
                                            $msg = "Given credit card was deleted.";
                                        }else{
                                            $msg = "Given credit card details is invalid.";
                                        }
                                        $error = array("status" => "Failed", "msg" => $msg);
                                        $this->response($this->json($error),400);
                                    }
                                }else{
                                    $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                    $this->response($this->json($error),400);
                                }
                            }
    //                        $json2 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id,"account_id"=>$account_id);
    //                        $postData2 = json_encode($json2);
    //                        $response2 = $this->wp->accessWepayApi("credit_card/authorize",'POST',$postData2,$token,$user_agent);
    //                        $this->wp->log_info("wepay_credit_card_authorize: ".$response2['status']);
    //                        if($response2['status']=="Success"){
    //                            $res2 = $response2['msg'];
    //                            $cc_name = $res2['credit_card_name'];
    //                            $cc_new_state = $res2['state'];
    //                            if($cc_state!=$cc_new_state){
    //                                $cc_status = $cc_new_state;
    //                            }
    //                            $cc_month = $res2['expiration_month'];
    //                            $cc_year = $res2['expiration_year'];
    //                        }else{
    //                            $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
    //                            $this->response($this->json($error),400);
    //                        }
                        }
                        
                        if(!empty(trim($cc_id))){
                            $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                            $postData3 = json_encode($json3);
                            $sns_msg['call'] = "Credit Card";
                            $sns_msg['type'] = "Credit Card details";
                            $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg);
                            $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                            if($response3['status']=="Success"){
                                $res3 = $response3['msg'];
                                $cc_name = $res3['credit_card_name'];
                                $cc_new_state = $res3['state'];
                                if($cc_state!=$cc_new_state){
                                    $cc_status = $cc_new_state;
                                }
                                $cc_month = $res3['expiration_month'];
                                $cc_year = $res3['expiration_year'];
                            }else{
                                for($k=0;$k<2;$k++){
                                    if($response3['msg']['error_code'] == 503){
                                        sleep(2);
                                        $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg);
                                        $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                                        if($response3['status']=="Success"){
                                            $res3 = $response3['msg'];
                                            $cc_name = $res3['credit_card_name'];
                                            $cc_new_state = $res3['state'];
                                            if($cc_state!=$cc_new_state){
                                                $cc_status = $cc_new_state;
                                            }
                                            $cc_month = $res3['expiration_month'];
                                            $cc_year = $res3['expiration_year'];
                                            break;
                                        }
                                        if($k==1){
                                            if($checkout_id_flag == 1){
                                                $type = $response3['msg']['error'];
                                                $description =  $response3['msg']['error_description']." Buyer Name:".$buyer_name." Buyer Email:".$buyer_email." Checkout Id:".$checkout_id." Participant Name:".$reg_col1." ".$reg_col2;       
                                                $insert_alertquery = sprintf("INSERT INTO `wepay_critical_alert`(`company_id`, `type`, `description`) VALUES ('%s','%s','%s')",mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $type),mysqli_real_escape_string($this->db, $description));
                                                $result_alert = mysqli_query($this->db, $insert_alertquery);
                                                if(!$result_alert){
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_alertquery");
                                                    log_info($this->json($error_log));
                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                    $this->response($this->json($error), 500);
                                                }   
                                                $type1 = $response3['msg']['error']." Regarding Credit Card";
                                                $description1 = "Fetching Credit Card details failed"."\n"." Buyer Name:".$buyer_name."\n"." Buyer Email:".$buyer_email."\n"." Checkout Id:".$checkout_id."\n"." Participant Name:".$reg_col1." ".$reg_col2;      
                                                $this->sendSnsforfailedcheckout($type1,$description1);
                                            }
                                            $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                            $this->response($this->json($error),400);
                                        }
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                        $this->response($this->json($error),400);
                                    }
                                }
                            }
                        }
                    }else{
                        if($recurring_payment_status=='N'){
                            $paid_amount = $payment_amount;
                        }else{
                            $paid_amount = $checkout_amount;
                        }
                    }
                    
                    $query = sprintf("INSERT INTO `event_registration`(`optional_discount_flag`,`check_number`,`payment_method`,`paid_amount`,`company_id`, `event_id`, `reg_event_type`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`,`buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                        `payment_type`, `payment_amount`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `deposit_amount`, `number_of_payments`, `payment_startdate`, `payment_frequency`, `total_order_amount`, `total_order_quantity`, 
                        `event_registration_column_1`, `event_registration_column_2`, `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`, `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`,`event_reg_type_user`,`event_reg_version_user`)
                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $optional_discount_flag), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $paid_amount),
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_type), mysqli_real_escape_string($this->db, $student_id), 
                            mysqli_real_escape_string($this->db, $participant_id),mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name),mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code),
                            mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $p_type), 
                            mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name),
                            mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $deposit_amt), mysqli_real_escape_string($this->db, $no_of_payments), mysqli_real_escape_string($this->db, $payment_startdate), mysqli_real_escape_string($this->db, $payment_frequency), 
                            mysqli_real_escape_string($this->db, $total_order_amount), mysqli_real_escape_string($this->db, $total_order_quantity), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4),
                            mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10),
                            mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user));
                    $result = mysqli_query($this->db, $query);
                    if(!$result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $event_reg_id = mysqli_insert_id($this->db);
                        
                        if(count($payment_array)>0){
                            $temp1 = $temp4 = 0;
                            for($i=0;$i<count($payment_array);$i++){
                                $pdate = $payment_array[$i]['date'];
                                $pamount = $payment_array[$i]['amount'];
                                $ptype = $payment_array[$i]['type'];
                                $pfee = $payment_array[$i]['processing_fee'];
                                if($ptype!='D'){
                                    $checkout_id = '';
                                    $checkout_state = '';
                                    $pstatus = 'N';
                                }else{
                                    if($payment_method=='CC'){
                                        $pstatus = 'S';
                                    }else{
                                        $pstatus = 'M';
                                    }
                                }
                                if($payment_method == "CA" || $payment_method == "CH"){
                                       $pfee=0;
                                }
                                $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_date`, `schedule_status`, `cc_id`, `cc_name`, `credit_method`, `check_number`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), 
                                        mysqli_real_escape_string($this->db, $pfee), mysqli_real_escape_string($this->db, $pamount), mysqli_real_escape_string($this->db, $pdate), mysqli_real_escape_string($this->db, $pstatus)
                                        , mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $check_number));
                                $payment_result = mysqli_query($this->db, $payment_query);
                                if(!$payment_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                                    log_info($this->json($error_log));
                                    $temp1 += 1;
                                }
                            }
                            if($temp1>0){
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp1);
                                $this->response($this->json($error), 500);
                            }
                        }else{
                            if($payment_method=='CC'){
                                $pstatus = 'S';
                            }else{
                                $pstatus = 'M';
                            }
                            if($payment_method == "CA" || $payment_method == "CH"){
                                       $fee=0;
                            }
                            $payment_query = sprintf("INSERT INTO `event_payment`(`credit_method`,`check_number`,`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_status`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $check_number),
                                    mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), 
                                    mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $pstatus), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name));
                            $payment_result = mysqli_query($this->db, $payment_query);
                            if(!$payment_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }
                        
                        if(count($event_array)>0){
                            $qty = $net_sales = $parent_net_sales = 0;
                            $temp = $temp2= $temp3 = 0;
                            for($i=0;$i<count($event_array);$i++){
                                $cevent_id = $event_array[$i]['event_id'];
                                $cquantity =  $event_array[$i]['event_quantity'];
                                $cevent_cost = $event_array[$i]['event_cost'];
                                $cdiscount = $event_array[$i]['event_discount_amount'];
                                $cpayment_amount = $event_array[$i]['event_payment_amount'];
                                $cbalance = $event_array[$i]['balance_due'];
                                $cprocessing_fee = $event_array[$i]['processing_fee'];
                                if(count($payment_array)>0){
                                    if($cbalance>0){
                                        $cstatus = 'RP';
                                    }else{
                                        $cstatus = 'RC';
                                    }
                                }else{
                                    $cstatus = 'CC';
                                }
                        
                                $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $cevent_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cevent_cost), 
                                        mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cbalance), mysqli_real_escape_string($this->db, $cprocessing_fee), mysqli_real_escape_string($this->db, $cstatus));
                                $reg_det_result = mysqli_query($this->db, $reg_det_query);
                                if(!$reg_det_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                                    log_info($this->json($error_log));
                                    $temp += 1;
                                }
                                
                                $pd_amount = $cpayment_amount - $cbalance;
                                
                                if(count($payment_array)>0){
                                    if($processing_fee_type==2){
                                        $net_sales += $pd_amount;
                                        $parent_net_sales += $pd_amount;
                                    }elseif($processing_fee_type==1){
                                        $net_sales += $pd_amount-$cprocessing_fee;
                                        $parent_net_sales += $pd_amount-$cprocessing_fee;
                                    }
                                }else{
                                    if($processing_fee_type==2){
                                        $parent_net_sales = $payment_amount;
                                    }else{
                                        $parent_net_sales = $payment_amount-$fee;
                                    }
                                }
                                $qty += $cquantity;
                                if($payment_method!='CC'){
                                    $nt_sales_str = ", `net_sales`=`net_sales`+$pd_amount";
                                }else{
                                    $nt_sales_str = '';
                                }
//                                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity $nt_sales_str WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                                $update_event_result = mysqli_query($this->db, $update_event_query);
                                if(!$update_event_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                                    log_info($this->json($error_log));
                                    $temp2 += 1;
                                }
                            }
                            if($temp>0){
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp);
                                $this->response($this->json($error), 500);
                            }
                            if($temp2>0){
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp2);
                                $this->response($this->json($error), 500);
                            }
                            if($payment_method!='CC'){
                                $nt_sales_str = ", `net_sales`=`net_sales`+$parent_net_sales";
                            }else{
                                $nt_sales_str = '';
                            }
                            
                            $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$qty $nt_sales_str WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                            $update_event_result = mysqli_query($this->db, $update_event_query);
                            if(!$update_event_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }else{
//                            $sbalance = $payment_amount - $paid_amount;
                            if($payment_method=='CC'){
                                $sbalance = $payment_amount;
                            }else{
                                if($p_type=='CO'){
                                    $sbalance = 0;
                                }else{
                                    $sbalance = $payment_amount-$deposit_amt;
                                }
                            }
                            if($sbalance>0){
                                $fee = $pr_fee;
                            }
//                            if($processing_fee_type==1){
//                                $paid_amount -= $fee;
//                            }
                            if(count($payment_array)>0){
                                if($sbalance>0){
                                    $sstatus = 'RP';
                                }else{
                                    $sstatus = 'RC';
                                }
                            }else{
                                $sstatus = 'CC';
                            }
                            $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $registration_amount), 
                                    mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $quantity), mysqli_real_escape_string($this->db, $sbalance), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $sstatus));
                            $reg_det_result = mysqli_query($this->db, $reg_det_query);
                            if(!$reg_det_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                            
                            if($payment_method!='CC'){
                                $nt_sales_str = ", `net_sales`=`net_sales`+$paid_amount";
                            }else{
                                $nt_sales_str = '';
                            }
                            $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity $nt_sales_str WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                            $update_event_result = mysqli_query($this->db, $update_event_query);
                            if(!$update_event_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }
                        
                        $selectcurrency=sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'",  mysqli_escape_string($this->db, $company_id));
                        $resultselectcurrency=  mysqli_query($this->db, $selectcurrency);
                        if(!$resultselectcurrency){
                             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                            log_info($this->json($error_log));
                        }else{
                            $row = mysqli_fetch_assoc($resultselectcurrency);
                            $wp_currency_symbol=$row['wp_currency_symbol'];
                        }
                        
                        $curr_date = gmdate("Y-m-d H:i:s");
                        if ($event_type == 'S') {
                            if (!empty($discount_code)) {
                                $activity_text = "Registered for " . $event_name . ', quantity ' . $quantity . '. Total cost ' . $wp_currency_symbol . $payment_amount . '. Discount code used ' . $discount_code;
                            } else {
                                if (empty($discount)) {
                                    if($optional_discount_flag=='Y' && $optional_discount>0){
                                        $optional_str = " Optional Discount amount used $optional_discount";
                                    }else{
                                        $optional_str = "";
                                    }
                                    $activity_text = "Registered for $event_name, quantity $quantity. Total cost $wp_currency_symbol"."$payment_amount.$optional_str";
                                } else {
                                    $activity_text = "Registered for " . $event_name . ', quantity ' . $quantity . '. Total cost ' . $wp_currency_symbol . $payment_amount.". Discount amount used $discount";
                                }
                            }
                            $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id),
                                    'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                            $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                            if (!$result_insert_event_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                                log_info($this->json($error_log));
                            }
                        } else {
                            for ($i = 0; $i < count($event_array); $i++) {
                                $event_title = $event_array[$i]['event_title'];
                                $event_quantity = $event_array[$i]['event_quantity'];
                                $event_cost = $event_array[$i]['event_cost'];
                                $event_discount_amount = $event_array[$i]['event_discount_amount'];
                                if (!empty($discount_code)) {
                                    $activity_text = "Registered for " . $event_name . " " . $event_title . ',quantity ' . $event_quantity . '.Total cost ' . $wp_currency_symbol . $event_cost . '.Discount code used ' . $discount_code;
                                } else {
                                    if (empty($event_discount_amount)) {
                                        if($optional_discount_flag=='Y' && $optional_discount>0){
                                            $optional_str = " Optional Discount amount used $optional_discount";
                                        }else{
                                            $optional_str = "";
                                        }
                                        $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol"."$event_cost.$optional_str";
                                    } else {
                                        $activity_text = "Registered for " . $event_name . " " . $event_title . ',quantity ' . $event_quantity . '.Total cost ' . $wp_currency_symbol . $event_cost.".  Discount amount used $event_discount_amount";
                                    }
                                }
                                $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id),
                                        'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                                $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                                if (!$result_insert_event_history) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                                    log_info($this->json($error_log));
                                }
                            }
                        }
                        
                        if($payment_amount>0){
                            $msg = array("status" => "Success", "msg" => "Registration successful. Confirmation of purchase has been sent to your email.");
                        }else{
                            $msg = array("status" => "Success", "msg" => "Registration successful.");
                        }
                        $this->responseWithWepay($this->json($msg),200);
                        $this->sentEmailForEventPaymentSuccess($company_id,$event_reg_id);
                    }
                }
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error),200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    protected function rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $amount, $currency){
        $time = time();
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg1=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
        $json = array("associated_object_type"=>"checkout","associated_object_id"=>$checkout_id,"receive_time"=>$time,"type"=>"person","source"=>"user","properties"=>array("name"=>"$buyer_name"), 
            "related_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code","country"=>"$country")))));
        $postData = json_encode($json);
        $response = $this->wp->accessWepayApi("rbit/create",'POST',$postData,$token,$user_agent,$sns_msg1);
        $this->wp->log_info("wepay_rbit_create: ".$response['status']);
        if($response['status']=="Success"){
            $json2 = array("associated_object_type"=>"checkout","associated_object_id"=>$checkout_id,"receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$amount, "quantity"=>1, "amount"=>$amount, "currency"=>"$currency"))));
            $postData2 = json_encode($json2);
            $response2 = $this->wp->accessWepayApi("rbit/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
            $this->wp->log_info("wepay_rbit_create: ".$response2['status']);
            if($response2['status']=="Success"){
                
            }else{
                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                $this->response($this->json($error),400);
            }
        }else{            
            $error = array("status" => "Failed", "msg" => $response['msg']['error_description']);
            $this->response($this->json($error),400);
        }
    }
    
    protected function addParticipantDetailsForFreeEvent($company_id, $event_id, $actual_student_id,$actual_participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_type, $event_array,$reg_type_user,$reg_version_user,$discount_code){
        $this->getStudioSubscriptionStatus($company_id);
        $buyer_name = $buyer_first_name.' '.$buyer_last_name;
        if (is_null($actual_student_id) || $actual_student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'wepay', '');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $actual_student_id;
        }
        if (is_null($actual_participant_id) || $actual_participant_id == 0) {
            $participant_id = $this->getParticipantIdForEvent($company_id, $student_id, $reg_col1, $reg_col2);
        } else {
            $participant_id = $actual_participant_id;
        }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
//        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $current_date = date("Y-m-d");
        $payment_type = 'F';
        $checkout_id = $checkout_state = '';
        
        $query = sprintf("INSERT INTO `event_registration`(`company_id`, `event_id`, `reg_event_type`, `student_id`, `participant_id`,  `buyer_first_name`, `buyer_last_name`,`buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                        `payment_type`, `payment_amount`, `paid_amount`, `event_registration_column_1`, `event_registration_column_2`,
                        `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`, `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`,`event_reg_type_user`,`event_reg_version_user`)
                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_type), mysqli_real_escape_string($this->db, $student_id), 
                            mysqli_real_escape_string($this->db, $participant_id),mysqli_real_escape_string($this->db, $buyer_first_name),mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code),
                            mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_type), 
                            mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4),
                            mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10),
                mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $event_reg_id = mysqli_insert_id($this->db);
            $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_status`) VALUES ('%s','%s','%s','%s','%s','%s')",
                    mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), 
                    mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, 'S'));
            $payment_result = mysqli_query($this->db, $payment_query);
            if(!$payment_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }

            if(count($event_array)>0){
                $qty = $net_sales = 0;
                $temp = $temp2= $temp3 = 0;
                for($i=0;$i<count($event_array);$i++){
                    $cevent_id = $event_array[$i]['event_id'];
                    $cquantity =  $event_array[$i]['event_quantity'];
                    $cevent_cost = $event_array[$i]['event_cost'];
                    $cdiscount = $event_array[$i]['event_discount_amount'];
                    $cpayment_amount = $event_array[$i]['event_payment_amount'];
                    $cstatus = 'CC';

                    $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $cevent_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cevent_cost), 
                            mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cstatus));
                    $reg_det_result = mysqli_query($this->db, $reg_det_query);
                    if(!$reg_det_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                        log_info($this->json($error_log));
                        $temp += 1;
                    }

                    $qty += $cquantity;

                    $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                    $update_event_result = mysqli_query($this->db, $update_event_query);
                    if(!$update_event_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                        log_info($this->json($error_log));
                        $temp2 += 1;
                    }
                }
                if($temp>0){
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp);
                    $this->response($this->json($error), 500);
                }
                if($temp2>0){
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp2);
                    $this->response($this->json($error), 500);
                }

                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$qty WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                $update_event_result = mysqli_query($this->db, $update_event_query);
                if(!$update_event_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }else{
                
                $sstatus = 'CC';
                $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $registration_amount), 
                        mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $quantity), mysqli_real_escape_string($this->db, $sstatus));
                $reg_det_result = mysqli_query($this->db, $reg_det_query);
                if(!$reg_det_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }

                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                $update_event_result = mysqli_query($this->db, $update_event_query);
                if(!$update_event_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
            $selectcurrency = sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'", mysqli_escape_string($this->db, $company_id));
            $resultselectcurrency = mysqli_query($this->db, $selectcurrency);
            if (!$resultselectcurrency) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                log_info($this->json($error_log));
            } else {
                $row = mysqli_fetch_assoc($resultselectcurrency);
                $wp_currency_symbol = $row['wp_currency_symbol'];
            }

            $curr_date = gmdate("Y-m-d H:i:s");
            if ($event_type == 'S') {
                if (!empty($discount_code)) {
                    $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount . '.Discount code used ' . $discount_code;
                } else {
                    if (!empty($discount)) {
                        $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount . '.Discount amount used ' . $discount;
                    } else {
                        $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount;
                    }
                }
                $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                if (!$result_insert_event_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                    log_info($this->json($error_log));
                }
            } else {
                for ($i = 0; $i < count($event_array); $i++) {
                    $event_title = $event_array[$i]['event_title'];
                    $event_quantity = $event_array[$i]['event_quantity'];
                    $event_cost = $event_array[$i]['event_cost'];
                    $event_discount_amount = $event_array[$i]['event_discount_amount'];
                    if (!empty($discount_code)) {
                        $activity_text = "Registered for " . $event_name . " " . $event_title . ',quantity ' . $event_quantity . '.Total cost ' . $wp_currency_symbol . $event_cost . '.Discount code used ' . $discount_code;
                    } else {
                        if (!empty($event_discount_amount)) {
                            $activity_text = "Registered for " . $event_name . " " . $event_title . ',quantity ' . $event_quantity . '.Total cost ' . $wp_currency_symbol . $event_cost . '.Discount amount used ' . $event_discount_amount;
                        } else {
                            $activity_text = "Registered for " . $event_name . " " . $event_title . ',quantity ' . $event_quantity . '.Total cost ' . $wp_currency_symbol . $event_cost;
                        }
                    }
                    $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                    $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                    if (!$result_insert_event_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                        log_info($this->json($error_log));
                    }
                }
            }

            $log = array("status" => "Success", "msg" => "Registration successful.");
            $this->responseWithWepay($this->json($log),200);
            $this->sentEmailForEventPaymentSuccess($company_id,$event_reg_id);
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    private function sentEmailForEventPaymentSuccess($company_id,$event_reg_id){
       $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $created_date = " DATE(CONVERT_TZ(ep.created_dt,$tzadd_add,'$new_timezone'))" ;
        $waiver_policies = '';
        $query = sprintf("SELECT er.`company_id`, er.`event_id`, `event_title`, if(ep.`payment_from`='S',ep.`stripe_card_name`,ep.`cc_name`) as cc_name, `waiver_policies`, concat(er.event_registration_column_2, ',', er.event_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `payment_type`, `payment_method`,
            er.`payment_amount` total_due, er.`processing_fee_type`, er.`processing_fee` total_processing_fee, `paid_amount`, ep.`payment_amount`, ep.`processing_fee`, ep.`refunded_amount`, `schedule_date`, `schedule_status`, DATE($created_date) created_dt, `credit_method`, ep.`check_number`
            FROM `event_registration` er LEFT JOIN `event_payment` ep ON er.`event_reg_id` = ep.`event_reg_id` AND ep.`schedule_status` IN ('N', 'S', 'PR', 'M')
            LEFT JOIN `event` e ON e.`event_id` = er.`event_id` AND e.`company_id` =  er.`company_id`
            WHERE er.`event_reg_id` = '%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            exit();
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
                $event_name = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol =$processing_fee_type= '';
                $inital_stage = $initial_down_pay = 0;
                $current_plan_details = $event_details = $bill_due_details = $payment_details = [];
                $total_due = $paid_due = $rem_due = 0;
                $student_cc_email_list = '';
                while($row = mysqli_fetch_assoc($result)){
                    $paid_amt = 0;
                    $company_id = $row['company_id'];
                    $event_id = $row['event_id'];
                    $event_name = $row['event_title'];
                    $participant_name = $row['participant_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $payment_method = $row['payment_method'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $temp=[];
                    
                    if($row['processing_fee_type']==2){
                        $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                    }else{
                        $temp['amount'] = $row['payment_amount'];
                    }
//                    $temp['amount'] = $row['payment_amount'];
                    $temp['date'] = $row['schedule_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['edit_status'] = 'N';
                    $temp['schedule_status'] = $row['schedule_status'];
                    $temp['processing_fee'] = $row['processing_fee']; 
                    $temp['cc_name'] = $row['cc_name'];
                    $temp['credit_method'] = $row['credit_method'];
                    $temp['check_number'] = $row['check_number'];
                    if($row['schedule_status']=='N'){
                        $temp['status'] = 'N';
                    }elseif($row['schedule_status']=='S'){
                        $temp['status'] = 'S';
                        $paid_amt = $temp['amount'];
                        $inital_stage++;
                    }elseif($row['schedule_status']=='PR' || $row['schedule_status']=='M'){
                        $paid_amt = $temp['amount']-$temp['refunded_amount'];
                    }
//                    $total_due = $current_plan_details['total_due'] = $row['total_due'];
                    $total_due += $temp['amount'];
                    $current_plan_details['total_due'] = $total_due;
                    $paid_due += $paid_amt;
                    $current_plan_details['paid_due'] = $paid_due;
                    
                    $current_plan_details['success_count'] = $inital_stage;
                    $rem_due = $current_plan_details['rem_due'] = number_format((float)($total_due - $paid_due), 2, '.', '');
//                    $payment_details[] = $temp;
                    if ($row['schedule_status'] == 'N') {
                        $bill_due_details[] = $temp;
                    } elseif ($row['schedule_status'] == 'S') {
                        $payment_details[] = $temp;
                        if ($initial_down_pay > 0) {
                            $bill_due_details[] = $temp;
                        }
                        $initial_down_pay++;
                    } elseif ($row['schedule_status'] == 'M') {
                        $payment_details[] = $temp;
                    }
                }
                
                $query2 = sprintf("SELECT c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`, e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, `event_end_dt`, erd.`event_cost`, `discount`, erd.`payment_amount`, `quantity`, `payment_status`, `balance_due`,s.`student_cc_email`  
                    FROM `event` e LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` LEFT JOIN `event_registration` er ON er.`event_reg_id`= erd.`event_reg_id`
                    LEFT JOIN `company` c ON e.`company_id` =  c.`company_id` LEFT JOIN `student` s ON s.`company_id`= er.`company_id` AND s.`student_id`= er.`student_id`
                    WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND erd.`event_reg_id`='%s' AND `payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", 
                        mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                $result2 = mysqli_query($this->db, $query2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    exit();
                }else{
                    $temp_quantity = $temp_char_count = 0;
                    while($row2 = mysqli_fetch_assoc($result2)){
                        $temp2 = [];
                        $temp2['title'] = $row2['event_title'];
                        if($temp2['title']>$temp_char_count){
                            $temp_char_count = strlen($temp2['title']);
                        }
                        
                        if (!empty(trim($row2['event_begin_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_begin_dt']) {
                                $start = " - ";
                            } else {
                                $start = $row2['event_begin_dt'];

                                $split_start = explode(" ", $start);
                                $start_date_value = $split_start[0];
                                $start_time_value = $split_start[1];
                                if ("00:00:00" == $start_time_value) {
                                    $start = date('M d, Y', strtotime($start));
                                } else {
                                    $start = date('M d, Y @ h:i A', strtotime($start));
                                }
                            }
                        }
                        
                        if (!empty(trim($row2['event_end_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_end_dt']) {
                                $end = " - ";
                            } else {
                                $end = $row2['event_end_dt'];

                                $split_end = explode(" ", $end);
                                $end_date_value = $split_end[0];
                                $end_time_value = $split_end[1];
                                if ("00:00:00" == $end_time_value) {
                                    $end = date('M d, Y', strtotime($end));
                                } else {
                                    $end = date('M d, Y @ h:i A', strtotime($end));
                                }
                            }
                        }

                        $temp2['start'] = $start;
                        $temp2['end'] = $end;
                        $temp2['event_cost'] = $row2['event_cost'];
                        $temp2['quantity'] = $row2['quantity'];
                        $temp2['event_disc'] = $row2['discount'];
                        $temp2['discounted_price'] = $row2['event_cost'] - $row2['discount'];
                        $temp_quantity += $row2['quantity'];
                        $temp2['payment_amount'] = $row2['payment_amount'];
                        $temp2['paid_due'] = $temp2['payment_amount'] - $row2['balance_due'];
                        $temp2['rem_due'] = $row2['balance_due'];
                        $temp2['state'] = 'N';
                        $event_details[] = $temp2;
                        $studio_name = $row2['company_name'];
                        $studio_mail = $row2['email_id'];
                        $cc_email_list = $row2['referral_email_list'];
                        $wp_currency_code = $row2['wp_currency_code'];
                        $wp_currency_symbol = $row2['wp_currency_symbol'];
                        $student_cc_email_list = $row2['student_cc_email']; 
                    }
                    $current_plan_details['total_event'] = $temp_quantity;
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                log_info($this->json($error_log));
                exit();
            }
        }
        
        $subject = $event_name." Order Confirmation";
        
        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
        $message .= "<b>Event Details</b><br><br>";
        for($i=0;$i<count($event_details);$i++){
            $message .= "Event: ".$event_details[$i]['title']."<br>Starts: ".$event_details[$i]['start']."<br>Ends: ".$event_details[$i]['end']."<br>Participant: ".$participant_name."<br><br>";
        }

        if($total_due>0){
            $message .= "<b>Billing Details</b><br><br>";
            if(count($bill_due_details)>0){
                $message .= "Total due: $wp_currency_symbol".$total_due."<br>";
                $message .= "Balance due: $wp_currency_symbol".$rem_due."<br>";                
                for($j=0;$j<count($bill_due_details);$j++){
                    $x= $j+1;
                    $amount = $bill_due_details[$j]['amount'];
                    $refunded_amount = $bill_due_details[$j]['refunded_amount'];
                    $final_amt = $amount-$refunded_amount;
                    $schedule_status = $bill_due_details[$j]['schedule_status'];
                    $date = date("M dS, Y", strtotime($bill_due_details[$j]['date']));
                    $recurring_processing_fee = $bill_due_details[$j]['processing_fee'];
                    if($schedule_status=='N'){
                        $paid_str = "due";
                    }
                    $recur_pf_str = "";
                    if($processing_fee_type==2){
                        $recur_pf_str = " (plus $wp_currency_symbol".$recurring_processing_fee." administrative fees)";
                }
                    $message .= "Bill ".$x.": $wp_currency_symbol".$final_amt." ".$paid_str." ".$date." " .$recur_pf_str."  <br>";
                }
                
                if(count($payment_details)>0){
                    $pf_str = $pf_str1 = $credit_name = "";
                    $message.="<br><br><b>Payment Details</b><br><br>";
                    $pr_fee = $payment_details[0]['processing_fee'];
                    if($payment_details[0]['credit_method']=='CC'){
                        $credit_name = "(".$payment_details[0]['cc_name'].")";
                        if($processing_fee_type==2){
                            $pf_str = " (includes $wp_currency_symbol".$pr_fee." administrative fees)";
                        }
                    }elseif($payment_details[0]['credit_method']=='CA'){
                        $credit_name = " (Cash)";
                    }elseif($payment_details[0]['credit_method']=='CH'){
                        $credit_name = " (Check #". $payment_details[0]['check_number'] .")";
                    }
                    $down_pay = $payment_details[0]['amount']-$payment_details[0]['refunded_amount'].$pf_str." paid on ".date("M dS, Y", strtotime($payment_details[0]['date'])).$credit_name;
                    
                    $message .= "Down Payment: $wp_currency_symbol".$down_pay."<br>";
                    for($k=1;$k<count($payment_details);$k++){
                        $x= $k;
                        $amount = $payment_details[$k]['amount'];
                        $refunded_amount = $payment_details[$k]['refunded_amount'];
                        $final_amt = $amount-$refunded_amount;
                        $schedule_status = $payment_details[$k]['schedule_status'];
                        $date = date("M dS, Y", strtotime($payment_details[$k]['date']));
                        $paid_str = "paid on";
                        $pr_fee1 = $payment_details[$k]['processing_fee'];
                        if($payment_details[$k]['credit_method']=='CC'){
                            if($processing_fee_type==2){
                                $pf_str1 = " (includes $wp_currency_symbol".$pr_fee1." administrative fees)";
                            }
                            $pay_type= "(".$payment_details[$k]['cc_name'].")";
                        }elseif($payment_details[$k]['credit_method']=='CA'){
                            $pay_type = " (Cash)";
                        }elseif($payment_details[$k]['credit_method']=='CH'){
                            $pay_type = " (Check #". $payment_details[$k]['check_number'] .")";
                        }else{
                            $pay_type = '';
                        }
                        $message .= "Payment ".$x.": $wp_currency_symbol".$final_amt." ".$pf_str1." ".$paid_str." ".$date." ".$pay_type." <br>";
                    }
                }
            }else{
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $schedule_status = $payment_details[0]['schedule_status'];
                $final_amount = $amount-$refunded_amount;
                $pr_fee2 = $payment_details[0]['processing_fee'];
                $pf_str2 = $credit_name = "";
                if($payment_details[0]['credit_method']=='CC'){
                    $credit_name = "(".$payment_details[0]['cc_name'].")";
                    if($processing_fee_type==2){
                        $pf_str2 = " (includes $wp_currency_symbol".$pr_fee2." administrative fees)";
                    }
                }elseif($payment_details[0]['credit_method']=='CA'){
                    $credit_name = " (Cash)";
                }elseif($payment_details[0]['credit_method']=='CH'){
                    $credit_name = " (Check #". $payment_details[0]['check_number'] .")";
                }
                $message .= "Total Due: $wp_currency_symbol".$final_amount."<br>Amount Paid: $wp_currency_symbol".$final_amount.$pf_str2.$credit_name."<br>";
            }
            $message .= "<br><br>";
        }
        
        $waiver_present = 0;
        $file = '';
        if(!empty(trim($waiver_policies))){
            $waiver_present = 1;
            $file_name_initialize = 10;
            for($z=0; $z<$file_name_initialize; $z++){
                $dt = time();
                $file = __DIR__."/../../uploads/".$dt."_waiver_policies.html";        
                if(file_exists($file)){
                    sleep(1);
                    $file_name_initialize++;
                }else{
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }
        
        $sendEmail_status = $this->sendEmailForEvent($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list,$student_cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
        date_default_timezone_set($curr_time_zone);
    }
    
     //Get Event details by Company Code
    public function getEventsDetailByCompanyForSocialMedia($company_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $company_name = $logo_URL = $upgrade_status = $wp_currency_code = $wp_currency_symbol = '';
        $wepay_status = 'N';
        $currency = [];
        
        $sql = sprintf("SELECT  `event_id` , c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`, c.`company_id`, c.`logo_URL`, c.`upgrade_status` ,  `parent_id` ,  `event_type` ,  `event_status` ,  `event_begin_dt` ,  `event_end_dt` ,  `event_title` ,  `event_category_subtitle` ,  
            `event_desc` ,  `event_more_detail_url`, `event_video_detail_url` , `event_cost` ,  `event_compare_price` ,  `registrations_count` ,  `event_capacity` ,  `capacity_text` ,  `processing_fees`, `event_onetime_payment_flag`, 
            `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `net_sales` ,  `event_sort_order` ,  
            `event_banner_img_url`, `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, 
            `event_registration_column_2`, `event_reg_col_2_mandatory_flag`,`event_registration_column_3`, `event_reg_col_3_mandatory_flag`,
            `event_registration_column_4`, `event_reg_col_4_mandatory_flag`,`event_registration_column_5`, `event_reg_col_5_mandatory_flag`,
            `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, `event_reg_col_7_mandatory_flag`,
            `event_registration_column_8`, `event_reg_col_8_mandatory_flag`,`event_registration_column_9`, `event_reg_col_9_mandatory_flag`,
            `event_registration_column_10`, `event_reg_col_10_mandatory_flag`, c.upgrade_status,
            IF( `event_status` = 'S', 'draft', IF( `event_type`='M', 'past', IF( DATE(`event_begin_dt`) <= ($currdate_add), IF(DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00', 'live', 'past' ) , 'live' ))) list_type,
            wp.account_state, wp.currency
            FROM  `event` e LEFT JOIN `company` c ON e.`company_id`=c.`company_id` 
            LEFT JOIN `wp_account` wp ON e.`company_id`=wp.`company_id`
            WHERE  e.`company_id` = '%s'
            AND  `event_status` =  'P' ORDER BY `event_sort_order` desc",mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "code"=>500, "title"=>"Server Error");
            return $error;
        } else {
            $output = $parent_mapping = $parent_integrated = $response['live'] = $response['draft'] = $response['past'] = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $company_name = $row['company_name'];
                    $logo_URL = $row['logo_URL'];
                    $upgrade_status = $row['upgrade_status'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    if(!empty($row['account_state'])){
                        $account_state = $row['account_state'];
                        $curr = $row['currency'];
                        if(!empty(trim($curr))){
                            $currency = explode(",", trim($curr));
                        }
                        $wepay_status = 'Y';
                    }
                    $disc = $this->getDiscountDetails($company_id, $row['event_id']);
                    $row['discount'] = $disc['discount'];
                    $row['child_events']=[];
                    $reg_field_name_array = [];
                    $temp_reg_field = "event_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "event_reg_col_".$z."_mandatory_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row['reg_columns'] = $reg_field_name_array;
                    if($row['event_type']=='M'){
                        $parent_mapping[$row['event_id']] = count($parent_integrated);
                        $parent_integrated[] = $row;
                    }
                    if($row['event_type']=='S' && empty($row['parent_id'])){
                        $parent_integrated[] = $row;
                    }
                    $output[] = $row;
                }
                for($i=0;$i<count($output);$i++){//integrate the child event with parent event for multiple event
                    if(!empty(trim($output[$i]['parent_id']))){
                        $parent_integrated[$parent_mapping[$output[$i]['parent_id']]]['child_events'][] = $output[$i];
//                        if($output[$i]['list_type']=='live'){//default parent status - past, when atleast a child event is in live then parent status changed to live
//                            $parent_integrated[$parent_mapping[$output[$i]['parent_id']]]['list_type'] = 'live';
//                        }
                    }
                }
                $parent_count = count($parent_integrated);
                for($i=0;$i<$parent_count;$i++){//split-up by live, past & draft
                    $child_count = count($parent_integrated[$i]['child_events']);
                    if ($child_count > 0) {
                        usort($parent_integrated[$i]['child_events'], function($a, $b) {
                            return $a['event_begin_dt'] > $b['event_begin_dt'];
                        });
                        
                        $check_type_conflict = $cevent_capacity = 0;
                        $inital_child_type = $parent_integrated[$i]['child_events'][0]['list_type'];
                        $parent_integrated[$i]['list_type'] = $inital_child_type;
                        
                        for($j=0;$j<$child_count;$j++){
                            $cevent_capacity += $parent_integrated[$i]['child_events'][$j]['event_capacity'];
                            $current_child_type = $parent_integrated[$i]['child_events'][$j]['list_type'];
                            if($j>0 && $current_child_type != $inital_child_type){
                                if($check_type_conflict==0){
                                    $check_type_conflict = 1;
                                    $parent_integrated[] = $parent_integrated[$i];
                                    $parent_count++;
                                    $parent_integrated[count($parent_integrated)-1]['list_type'] = $current_child_type;
                                    $parent_integrated[count($parent_integrated)-1]['child_events'] = [];
                                    $parent_integrated[count($parent_integrated)-1]['child_events'][] = $parent_integrated[$i]['child_events'][$j];
                                    array_splice($parent_integrated[$i]['child_events'],$j, 1);
                                    $j--;
                                    $child_count--;
                                }else{
                                    $parent_integrated[count($parent_integrated)-1]['child_events'][] = $parent_integrated[$i]['child_events'][$j];
                                    array_splice($parent_integrated[$i]['child_events'], $j, 1);
                                    $child_count--;
                                    $j--;
                                }
                            }
                        }
                        $parent_integrated[$i]['event_capacity'] = $cevent_capacity;
                    }
                    if($parent_integrated[$i]['list_type']=='live'){
                        $response['live'][] = $parent_integrated[$i];
                    }elseif($parent_integrated[$i]['list_type']=='draft'){
                        $response['draft'][] = $parent_integrated[$i];
                    }elseif($parent_integrated[$i]['list_type']=='past'){
                        $response['past'][] = $parent_integrated[$i];
                    }
                }
                $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = preg_replace('~[\r\n]+~', '', $process_fee['PT']);
                $out = array('status' => "Success", 'msg' => $response['live'][0], "company_details" => $company_details);
                // If success everythig is good send header as "OK" and user details
//                $this->response($this->json($out), 200);
//                log_info(json_encode($out));
                return $out;
            } else {
                $error = array('status' => "Failed", "msg" => "No events available.", "code"=>404, "title"=>"404 Error");
                return $error;
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function getEventsDetailByEventIdForSocialMedia($company_id,$event_id){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $res = $this->checkEventWithcompanyForSocialMedia($company_id,$event_id);
        $list_type = 'live';
        $company_name = $logo_URL = $upgrade_status = $wp_currency_code = $wp_currency_symbol = '';
        $wepay_status = 'N';
        $currency = [];
        
        $sql = sprintf("SELECT  `event_id` , c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`,  c.`company_id` , c.`logo_URL`, c.`upgrade_status`,  `parent_id` ,  `event_type` ,  `event_status` ,  `event_begin_dt` ,  `event_end_dt` ,  `event_title` ,  `event_category_subtitle` ,  
            `event_desc` ,  `event_more_detail_url`, `event_video_detail_url` , `event_cost` ,  `event_compare_price` ,  `registrations_count` ,  `event_capacity` ,  `capacity_text` ,  `processing_fees`, `event_onetime_payment_flag`, 
            `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `net_sales` ,  `event_sort_order` ,  
            `event_banner_img_url`, `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, 
            `event_registration_column_2`, `event_reg_col_2_mandatory_flag`,`event_registration_column_3`, `event_reg_col_3_mandatory_flag`,
            `event_registration_column_4`, `event_reg_col_4_mandatory_flag`,`event_registration_column_5`, `event_reg_col_5_mandatory_flag`,
            `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, `event_reg_col_7_mandatory_flag`,
            `event_registration_column_8`, `event_reg_col_8_mandatory_flag`,`event_registration_column_9`, `event_reg_col_9_mandatory_flag`,
            `event_registration_column_10`, `event_reg_col_10_mandatory_flag`, c.upgrade_status,
            IF( `event_status` = 'S', 'draft', IF( `event_type`='M', 'past', IF( DATE(`event_begin_dt`) <= ($currdate_add), IF(DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00', 'live', 'past' ) , 'live' ))) list_type,
            wp.account_state, wp.currency
            FROM  `event` e LEFT JOIN `company` c ON e.`company_id`=c.`company_id` 
            LEFT JOIN `wp_account` wp ON e.`company_id`=wp.`company_id`
            WHERE (`event_id` = '%s' or `parent_id` = '%s')
            AND  `event_status` =  'P' ORDER BY  `parent_id` ASC, `event_begin_dt` ASC", mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$event_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "code"=>500, "title"=>"Server Error");
            return $error;
        } else {
            $child = [];
            $disc = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                    if($num_of_rows==1 && $row['event_type']=='C'){
                        $error = array('status' => "Failed", "msg" => "The event is not available.");
                        $this->response($this->json($error), 401); // If no records "No Content" status
                    }
                    $company_name = $row['company_name'];
                    $logo_URL = $row['logo_URL'];
                    $upgrade_status = $row['upgrade_status'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    if(!empty($row['account_state'])){
                        $account_state = $row['account_state'];
                        $curr = $row['currency'];
                        if(!empty(trim($curr))){
                            $currency = explode(",", trim($curr));
                        }
                        $wepay_status = 'Y';
                    }
                    $disc = $this->getDiscountDetails($company_id, $row['event_id']);
                    $row['discount'] = $disc['discount'];
                    $reg_field_name_array = [];
                    $temp_reg_field = "event_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "event_reg_col_".$z."_mandatory_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row['reg_columns'] = $reg_field_name_array;
                    if($row['event_type']=='M' || $row['event_type']=='S'){
                        $row['child_events'] = [];
                        $response = $row;
                    }else{
                        $child[] = $row;
                    }
                }
                
                if(count($child)>0){
                    $check_child_type = 0;                    
                    for($i=0;$i<count($child);$i++){
                        if($list_type == '' || $list_type == $child[$i]['list_type']){
                            if($check_child_type==0){
                                $response['list_type'] = $child[$i]['list_type'];
                                $check_child_type++;
                            }
                            $response['child_events'][] = $child[$i];
                        }
                    }
                }else{
                    $response['child_events']=[];
                }
                $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = preg_replace('~[\r\n]+~', '', $process_fee['PT']);
                $out = array('status' => "Success", 'msg' => $response, "company_details" => $company_details);
                // If success everythig is good send header as "OK" and user details
//                $this->response($this->json($out), 200);
                return $out;
            } else {
                $error = array('status' => "Failed", "msg" => "The event is not available.", "code"=>404, "title"=>"404 Error");
                return $error;
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    private function checkEventWithcompanyForSocialMedia($company_id,$event_id){
        $query = sprintf("SELECT * FROM `event` WHERE `company_id`='%s' AND `event_id`='%s' AND `event_status`='P'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "code"=>500, "title"=>"Server Error");
            return $error;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $succ = array('status' => "Success", "msg" => "The event is available.");
                return $succ;
            }else{
                $error = array('status' => "Failed", "msg" => "The event is not available.", "code"=>404, "title"=>"404 Error");
                return $error;
            }
        }
    }
    
    private function sendCrawlerOutputData($data) {
        $title = $desc = $image = $imageUrl = '';
        if($data['status']=='Failed'){
            if($data['code']==500){
                $title = "Server Error";
                $desc = "Internal Server Error";
            }else{
                $title = "404 Error";
                $desc = "The Event is not available";
            }
        }else{
            
        }
        echo "
        <!DOCTYPE html>
        <html>
            <head>
                <meta property='og:title' content='$title'/>
                <meta property='og:description' content='$desc' />
                <meta property='og:image' content='$image' />
                <!-- etc. -->
            </head>
            <body>
                <p> $desc </p>
                <img src='$imageUrl'>
            </body>
        </html>";
        exit();
    }
    
    public function checkQuantityDuringCheckout($company_id, $event_id, $event_array, $quantity, $event_type) {
        $event_capacity = $registration_count = $remaining_qty = $child_qty = 0;
        if ($event_type == "M") {
            $capacity_query = sprintf("SELECT `event_id`,`event_capacity`,`registrations_count`,`event_title` FROM `event` WHERE `parent_id`='%s' and `company_id`='%s' ", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
        } else if ($event_type == "S") {
            $capacity_query = sprintf("SELECT `event_id`,`event_capacity`,`registrations_count`,`event_title` FROM `event` WHERE `event_id`='%s' and `company_id`='%s' ", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
        }
        $cap_result = mysqli_query($this->db, $capacity_query);
        if (!$cap_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$capacity_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            if (mysqli_num_rows($cap_result) > 0) {
                while ($row_1 = mysqli_fetch_assoc($cap_result)) {
                    $output_1[] = $row_1;
                }
                if ($event_type == "S") {
                    $event_capacity = $output_1[0]['event_capacity'];
                    $registration_count = $output_1[0]["registrations_count"];
                    if (isset($event_capacity) && $event_capacity > 0) {
                        $remaining_qty = $event_capacity - $registration_count;
                        if ($remaining_qty == 0) {
                            $error = array('status' => "Failed", "msg" => "Event Sold Out.");
                            $this->response($this->json($error), 500);
                        }
                        if ($remaining_qty < $quantity) {
                            $error = array('status' => "Failed", "msg" => "Only $remaining_qty Capacity is Available  .");
                            $this->response($this->json($error), 500);
                        }
                    }elseif (isset($event_capacity) && $event_capacity == 0){
                        $error = array('status' => "Failed", "msg" => "Event Sold Out.");
                        $this->response($this->json($error), 500); 
                    }
                } elseif ($event_type == "M") {
                    $failure_flag=0;
                    $error_msg='';
                    for ($idx = 0; $idx < count($event_array); $idx++) {
                        $event_capacity = $registration_count = $remaining_qty = $child_qty = 0;
                        $event_title='';
                        $child_qty = $event_array[$idx]["event_quantity"]; 
                        for ($idx_1 = 0; $idx_1 < count($output_1); $idx_1++) {
                            if($output_1[$idx_1]['event_id']==$event_array[$idx]['event_id']){
                                $event_capacity = $output_1[$idx_1]['event_capacity'];
                                $registration_count = $output_1[$idx_1]["registrations_count"];
                                $event_title= $output_1[$idx_1]["event_title"];
                                if (isset($event_capacity) && $event_capacity > 0) {
                                    $remaining_qty = $event_capacity - $registration_count;
                                    if ($remaining_qty == 0) {
                                        $failure_flag=1;
                                        $error_msg .= "Event $event_title Sold Out. ";
                                    }
                                    if ($remaining_qty < $child_qty) {
                                        $failure_flag=1;
                                        $error_msg .= "Only $remaining_qty Capacity is available for $event_title. ";
                                    }
                                } elseif (isset($event_capacity) && $event_capacity == 0) {
                                    $error = array('status' => "Failed", "msg" => "Event $event_title Sold Out.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }
                    if($failure_flag==1){
                        $error = array('status' => "Failed", "msg" => $error_msg);
                        $this->response($this->json($error), 500);
                    }
                }
            }
        }
    }
    
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone='';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result_tz);                       
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    private function getRealIpAddr(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet        
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    private function sendSnsforfailedcheckout($sub,$msg){
    
            $topicarn = 'arn:aws:sns:us-east-1:241334827363:mystudio';
         
                    $subject = $sub;
    //                $subject = $sns_sub . " -transaction failed";
                    $message = $msg;

                    $credentials = new Aws\Credentials\Credentials('AKIAIGUWYNJGAHDPJGIA', 'w4oKbP0UhJZr1W0hGuULhkdEoxEHY2YxEq/6uycT');               
                    $sns = SnsClient::factory([
                        'credentials' => $credentials,
                        'region' => 'us-east-1',
                        'version' => 'latest'
                    ]);
    //                log_info("object cretaed");
                    $result1 = $sns->publish(array(
                        'TopicArn' => $topicarn,
                        // Message is required
                        'Message' => $message,
                        'Subject' => $subject,
                        //'MessageStructure' => 'string',
                        'MessageAttributes' => array(
                            // Associative array of custom 'String' key names
                            'String' => array(
                                // DataType is required
        //            'DataType' => '',
                                'DataType' => 'String',
                                'StringValue' => '200',
                            ),
                        // ... repeated
                        ),
                    ));
               
    }
    
    public function getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $payment_processor, $studio_name) {
        $select_student = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$company_id));
        $result_select_student = mysqli_query($this->db, $select_student);
        if (!$result_select_student) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student);
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result_select_student) > 0) {
                $r = mysqli_fetch_assoc($result_select_student);
                $student_id = $r['student_id'];
                if(is_null($r['stripe_customer_id']) || empty(trim($r['stripe_customer_id']))){
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = $r['stripe_customer_id'];
                    }
                }else{
                    $stripe_customer_id = $r['stripe_customer_id'];
                }
            } else {
                $insert_student = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_lastname`, `student_email`) VALUES('%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), 
                        mysqli_real_escape_string($this->db,$buyer_first_name), mysqli_real_escape_string($this->db,$buyer_last_name), mysqli_real_escape_string($this->db,$buyer_email));
                $result_insert_student = mysqli_query($this->db, $insert_student);
                if (!$result_insert_student) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student);
                    $this->response($this->json($error), 200);
                } else {
                    $student_id = mysqli_insert_id($this->db);
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = '';
                    }
                }
            }
            return array("student_id" => $student_id, "stripe_customer_id" => $stripe_customer_id);
        }
    }
    
    public function getParticipantIdForEvent($company_id, $student_id, $first_name, $last_name) {

        $select_participant2 = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name));
        $result_select_participant2 = mysqli_query($this->db, $select_participant2);
        if (!$result_select_participant2) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_participant2);
            $this->response($this->json($error), 500);
        } else {
            if (mysqli_num_rows($result_select_participant2) > 0) {
                $r = mysqli_fetch_assoc($result_select_participant2);
                $participant_id = $r['participant_id'];
                $deleted_flag = $r['deleted_flag'];
                if($deleted_flag=='D'){
                    $update_participant = sprintf("UPDATE `participant` SET `deleted_flag`='N' WHERE `participant_id`='%s'", mysqli_real_escape_string($this->db, $participant_id));
                    $result_update_participant = mysqli_query($this->db, $update_participant);
                    if (!$result_update_participant) {
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_participant);
                        $this->response($this->json($error), 500);
                    }
                }
            } else {
                $insert_participant2 = sprintf("INSERT INTO `participant`(`company_id`, `student_id`, `participant_first_name`, `participant_last_name`) VALUES('%s', '%s', '%s', '%s')",
                        $company_id, $student_id, mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name));
                $result_insert_participant2 = mysqli_query($this->db, $insert_participant2);
                if (!$result_insert_participant2) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_participant2);
                    $this->response($this->json($error), 500);
                } else {
                    $participant_id = mysqli_insert_id($this->db);
                }
            }
            return $participant_id;
        }
    }
    
    public function getStudioSubscriptionStatus($company_id){
        $query = sprintf("SELECT email_id,upgrade_status,subscription_status,wepay_status,stripe_status,stripe_subscription,studio_expiry_level,expiry_dt FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_email = $row['email_id'];
                $upgrade_status = $row['upgrade_status'];
                $subscription_status = $row['subscription_status'];
                $wepay_status = $row['wepay_status'];
                $stripe_status = $row['stripe_status'];
                $stripe_subscription = $row['stripe_subscription'];
                $studio_expiry_level = $row['studio_expiry_level'];
                $expiry_dt = $row['expiry_dt'];
                if (!empty(trim($upgrade_status)) && $subscription_status=='N' && ($studio_expiry_level=='L2' || $upgrade_status=='F')) {
                    $res = array('status' => "Failed", "msg" => "Please contact the business operator to activate this link.", "studio_email"=>$studio_email, "upgrade_status"=>$upgrade_status, "studio_expiry_level" => $studio_expiry_level);
                    $this->response($this->json($res), 200);
                }else{
                    $res = array('status' => "Success", "msg" => "Studio subscription status success", "upgrade_status" => $upgrade_status);
                    return $res;
                }
                
            } else {
                $res = array('status' => "Failed", "msg" => "Studio Details not found.");
                $this->response($this->json($res), 200);
            }

        }
    }
    
    public function sendExpiryEmailDetailsToStudio($company_id){
        $query = sprintf("SELECT c.`email_id`, c.`company_name`, c.`referral_email_list` FROM `company` c WHERE c.`company_id`='%s'", $company_id);
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $company_name = $reply_to = "";
                $row = mysqli_fetch_assoc($result);
                $owner_email_id = $row['email_id'];
                $referral_email_list = $row['referral_email_list'];
                
                $subject = "A member has requested that you activate your MyStudio account.";
                $body_msg = '<div style="max-width:520px;">';
                $body_msg .= "Members are trying to access your app and/or URL registration links.<br><br>However, your account is currently inactive, please click here to activate your account.<br>";
                $body_msg .= '<center><a href="'.$this->server_url.'/mystudioapp.php?pg=login"><button type="button" style="width: 140px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 500;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 40px;font-size: 15px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">Activate</button></a></center>';
                $body_msg .= "</div>";
                $sendEmail_status = $this->sendEmail($owner_email_id, $referral_email_list, $subject, $body_msg, $company_name, $reply_to, true);
                if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
                    $refer_check = 1;
                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $owner_email_id . "   " . $sendEmail_status['mail_status']);
                    log_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                } else {
                    $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $owner_email_id);
                    log_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                }                
            }else{
                $error = array('status' => "Failed", "msg" => "Studio details doesn't exist.");
                $this->response($this->json($error), 401);
            }
        }
    }
    
    protected function sendEmail($to, $cc_email_list, $subject, $message, $cmp_name, $reply_to, $isHtml) {

        if (empty(trim($cmp_name))) {
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try {
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if (filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)) {
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML($isHtml);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    $mail->AddCC($cc_addresses[$init]);
                }
            }
            if($isHtml){                
                $mail->CharSet = 'UTF-8';
                $mail->Encoding = 'base64';
            }
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        } catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    protected function checkpossettings($company_id, $payment_method, $pos_email, $token) {
        $this->verifyStudioPOSToken($company_id, $pos_email, $token);
        $sql = sprintf("SELECT event_status,event_payment_method from studio_pos_settings where company_id = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                if($row['event_status'] === "D"){
                    $error = array('status' => "Failed", "msg" => "Event POS is disabled");
                    $this->response($this->json($error), 200);
                }
                $payment_binary = $row['event_payment_method'];
                if($payment_method === "CA"){
                    if($payment_binary[1] === "0"){
                        $error = array('status' => "Failed", "msg" => "Payment method cash is disabled");
                        $this->response($this->json($error), 200);
                    }
                }
                if($payment_method === "CH"){
                    if($payment_binary[2] === "0"){
                        $error = array('status' => "Failed", "msg" => "Payment method check is disabled");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
    }
    
    public function checkStudioAndEmail($company_id, $email){
        $query = sprintf("SELECT * FROM `user` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Studio details.", "session_status" => "Expired");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function verifyStudioPOSToken($company_id, $email, $token) {
        $this->checkStudioAndEmail($company_id, $email);
        $query = sprintf("SELECT * FROM `pos_token` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $old_token = $row['token'];
                $token_id = $row['pos_token_id'];
                if($old_token==$token){
                    $query2 = sprintf("UPDATE `pos_token` SET `no_of_access`=`no_of_access`+1 WHERE `pos_token_id`='%s'", mysqli_real_escape_string($this->db, $token_id));
                    $result2 = mysqli_query($this->db, $query2);                
                    if(!$result2){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $msg = array("status"=>"Success", "msg"=>"Session is active.");
                    return $msg;
                }else{
                    $msg = array("status"=>"Failed", "msg"=>"Session expired.", "session_status" => "Expired");
                    $this->response($this->json($msg), 200);
                }
            }else{
                $msg = array("status"=>"Failed", "msg"=>"Session expired.", "session_status" => "Expired");
                $this->response($this->json($msg), 200);
            }
        }
    }
    
    public function calculateOptionalDiscountForevents($event_optional_discount, $payment_method, $company_id, $payment_amount, $processing_fee_type, $quantity, $event_array, $payment_array, $total_order_amount, $total_order_quantity, $payment_start_date, $payment_type, $total_no_of_payments, $call_back){
        $final_payment_amount = $new_payment_amount = $rec_amount = $rec_processing_fee = $check_payment_amount = $total_quantity = $total_processing_fee = $down_payment = $total_payment_amount = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $recurring_payment_status = 'N';
//        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $upgrade_status = $sts['upgrade_status'];
        $p_f = $this->getProcessingFee($company_id,$upgrade_status);
        $process_fee_per = $p_f['PP'];
        $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);
        usort($event_array, function($a, $b) {
            return $a['event_payment_amount'] > $b['event_payment_amount'];
        });
        if($payment_start_date < date("Y-m-d") && ($payment_type == 'singlerecurring' || $payment_type == 'multiplerecurring')){
            $error = array('status' => "Failed", "msg" => "Payment start date should be greater than registration date.");
            $this->response($this->json($error), 200);
        }else if ($event_optional_discount > $payment_amount){
            $error = array('status' => "Failed", "msg" => "Discount Amount Is Greater Than Payment Amount.");
            $this->response($this->json($error), 200);
        }

//        if ($payment_method == 'CC') {
            if ($payment_type == 'singlerecurring') {
                $recurring_payment_status = 'Y';
                $new_payment_amount = (float)$payment_amount - (float)$event_optional_discount;
                if ($new_payment_amount < $total_order_amount || $quantity < $total_order_quantity || $total_no_of_payments <= 0) {
                    $recurring_payment_status = 'N';
                    $error = array('status' => "Failed", "msg" => "Recurring payment does not satisfies the requirements.");
                    $this->response($this->json($error), 200);
                }
                if ($new_payment_amount > 0 && $new_payment_amount < 5) {
                    $error = array('status' => "Failed", "msg" => "Amount Should Be Greater Than 5.");
                    $this->response($this->json($error), 200);
                }
                $down_payment = (float)$payment_array[0]['amount'];
                $check_payment_amount = (float)$new_payment_amount - (float)$payment_array[0]['amount'];
                if ($new_payment_amount == 0) {
                    $down_payment = 0;
                    $recurring_payment_status = 'N';
                    $payment_array = [];
                } else if ($check_payment_amount == 0) {
                    $recurring_payment_status = 'N';
                    $payment_array = [];
                } else {
                    $rec_amount = number_format((float)($check_payment_amount / $total_no_of_payments),2, '.', '');
                    if($rec_amount < 5){
                        $recurring_payment_status = 'N';
                        $error = array('status' => "Failed", "msg" => "Recurring payment does not satisfies the requirements.");
                        $this->response($this->json($error), 200);
                    }
                    if($payment_method=='CC'){
                        $rec_processing_fee = number_format($this->calculateProcessingFee($rec_amount, $processing_fee_type, $process_fee_per, $process_fee_val),2, '.', '');
                    }
                    for ($j = 1; $j < count($payment_array); $j++) {
                        $payment_array[$j]['amount'] = $rec_amount;
                        $payment_array[$j]['processing_fee'] = $rec_processing_fee;
                    }
                }
                $final_payment_amount = (float)$down_payment;
                $total_payment_amount = $new_payment_amount;
            } else if ($payment_type == 'multiplerecurring') {
                $recurring_payment_status = 'Y';
                $temp_total_disc_amount = 0;
                for ($i = 0; $i < count($event_array); $i++) {
                    $temp_disc_amount = 0;
                    if($i==count($event_array)-1){
                        $temp_disc_amount = number_format((float)($event_optional_discount-$temp_total_disc_amount), 2, '.', '');
                    }else{
                        $temp_disc_amount = number_format((float)(($event_array[$i]['event_payment_amount'] / $payment_amount) * $event_optional_discount), 2, '.', '');
                    }
                    $event_array[$i]['event_discount_amount'] = number_format((float)($temp_disc_amount/$event_array[$i]['event_quantity']), 2, '.', '');
                    $event_array[$i]['event_payment_amount'] = number_format((float)$event_array[$i]['event_payment_amount'] - (float)$temp_disc_amount, 2, '.', '');
                    $total_quantity += $event_array[$i]['event_quantity'];
                    if ($event_array[$i]['event_payment_amount'] > 0 && $event_array[$i]['event_payment_amount'] < 5) {
                        $event_optional_discount -= $event_array[$i]['event_cost'] * $event_array[$i]['event_quantity'];
                        $payment_amount -= $event_array[$i]['event_cost'] * $event_array[$i]['event_quantity'];
                        $event_array[$i]['event_discount_amount'] = $event_array[$i]['event_cost'];
                        $event_array[$i]['event_payment_amount'] = 0;
                        $temp_disc_amount = $event_array[$i]['event_cost'] * $event_array[$i]['event_quantity'];
                        continue;
                    }
                    $temp_total_disc_amount += $temp_disc_amount;
                    $new_payment_amount += (float)$event_array[$i]['event_payment_amount'];
                }
                if ($new_payment_amount < $total_order_amount || $total_quantity < $total_order_quantity || $total_no_of_payments <= 0) {
                    $recurring_payment_status = 'N';
                    $error = array('status' => "Failed", "msg" => "Recurring payment does not satisfies the requirements.");
                    $this->response($this->json($error), 200);
                }
                if ($new_payment_amount > 0 && $new_payment_amount < 5) {
                    $error = array('status' => "Failed", "msg" => "Amount Should Be Greater Than 5.");
                    $this->response($this->json($error), 200);
                }
                $down_payment = (float)$payment_array[0]['amount'];
                $check_payment_amount = (float)$new_payment_amount - (float)$payment_array[0]['amount'];
                if ($new_payment_amount == 0) {
                    $down_payment = 0;
                    $recurring_payment_status = 'N';
                    $payment_array = [];
                } else if ($check_payment_amount == 0) {
                    $recurring_payment_status = 'N';
                    $payment_array = [];
                } else {
                    $rec_amount = number_format((float)($check_payment_amount / $total_no_of_payments), 2, '.', '');
                    if ($rec_amount < 5) {
                        $recurring_payment_status = 'N';
                        $error = array('status' => "Failed", "msg" => "Recurring payment does not satisfies the requirements.");
                        $this->response($this->json($error), 200);
                    }
                    if($payment_method=='CC'){
                        $rec_processing_fee = number_format($this->calculateProcessingFee($rec_amount, $processing_fee_type, $process_fee_per, $process_fee_val), 2, '.', '');
                    }
                    for ($j = 1; $j < count($payment_array); $j++) {
                        $payment_array[$j]['amount'] = $rec_amount;
                        $payment_array[$j]['processing_fee'] = $rec_processing_fee;
                    }
                }
                $final_payment_amount = (float)$down_payment;
                $total_payment_amount = $new_payment_amount;
            } else if ($payment_type == 'onetimesingle') {
                $new_payment_amount = (float)$payment_amount - (float)$event_optional_discount;
                if ($new_payment_amount > 0 && $new_payment_amount < 5) {
                    $error = array('status' => "Failed", "msg" => "Amount Should Be Greater Than 5.");
                    $this->response($this->json($error), 200);
                }
                $final_payment_amount = (float)$new_payment_amount;
                $total_payment_amount = $final_payment_amount;
            } else if ($payment_type == 'onetimemultiple') {
                $temp_total_disc_amount = 0;
                for ($i = 0; $i < count($event_array); $i++) {
                    $temp_disc_amount = 0;
                    if($i==count($event_array)-1){
                        $temp_disc_amount = number_format((float)($event_optional_discount-$temp_total_disc_amount), 2, '.', '');
                    }else{
                        $temp_disc_amount = number_format((float)(($event_array[$i]['event_payment_amount'] / $payment_amount) * $event_optional_discount), 2, '.', '');
                    }
                    $event_array[$i]['event_discount_amount'] = number_format((float)($temp_disc_amount/$event_array[$i]['event_quantity']), 2, '.', '');
                    $event_array[$i]['event_payment_amount'] = number_format((float)$event_array[$i]['event_payment_amount'] - (float)$temp_disc_amount, 2, '.', '');
                    $total_quantity += $event_array[$i]['event_quantity'];
                    if ($event_array[$i]['event_payment_amount'] > 0 && $event_array[$i]['event_payment_amount'] < 5) {
                        $event_optional_discount -= $event_array[$i]['event_cost'] * $event_array[$i]['event_quantity'];
                        $payment_amount -= $event_array[$i]['event_cost'] * $event_array[$i]['event_quantity'];
                        $event_array[$i]['event_discount_amount'] = $event_array[$i]['event_cost'];
                        $event_array[$i]['event_payment_amount'] = 0;
                        $temp_disc_amount = $event_array[$i]['event_cost'] * $event_array[$i]['event_quantity'];
                        continue;
                    }
                    $temp_total_disc_amount += $temp_disc_amount;
                    $new_payment_amount += (float)$event_array[$i]['event_payment_amount'];
                }
                if ($new_payment_amount > 0 && $new_payment_amount < 5) {
                    $error = array('status' => "Failed", "msg" => "Amount Should Be Greater Than 5.");
                    $this->response($this->json($error), 200);
                }
                $final_payment_amount = (float)$new_payment_amount;
                $total_payment_amount = $final_payment_amount;
            }
            if($payment_method=='CC' && $final_payment_amount>0){
                $total_processing_fee = number_format($this->calculateProcessingFee($final_payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val),2, '.', '');
            }
//        } else {
//            if ($payment_type == 'onetimesingle') {
//                $new_payment_amount = (float)$payment_amount - (float)$event_optional_discount;
//                if ($new_payment_amount > 0 && $new_payment_amount < 5) {
//                    $error = array('status' => "Failed", "msg" => "Amount Should Be Greater Than 5.");
//                    $this->response($this->json($error), 200);
//                }
//                $final_payment_amount = (float)$new_payment_amount;
//            } else if ($payment_type == 'onetimemultiple') {
//                for ($i = 0; $i < count($event_array); $i++) {
//                    $event_array[$i]['event_discount_amount'] = number_format((float)(($event_array[$i]['event_payment_amount'] / $payment_amount) * $event_optional_discount), 2, '.', '');
//                    $event_array[$i]['event_payment_amount'] = number_format((float)($event_array[$i]['event_payment_amount'] - $event_array[$i]['event_discount_amount']),2, '.', '');
//                    $total_quantity += $event_array[$i]['event_quantity'];
//                    if ($event_array[$i]['event_payment_amount'] > 0 && $event_array[$i]['event_payment_amount'] < 5) {
//                        $event_optional_discount -= $event_array[$i]['event_cost'] * $event_array[$i]['event_quantity'];
//                        $payment_amount -= $event_array[$i]['event_cost'] * $event_array[$i]['event_quantity'];
//                        $event_array[$i]['event_discount_amount'] = $event_array[$i]['event_cost'] * $event_array[$i]['event_quantity'];
//                        $event_array[$i]['event_payment_amount'] = 0;
//                        continue;
//                    }
//                    $new_payment_amount += (float)$event_array[$i]['event_payment_amount'];
//                }
//                if ($new_payment_amount > 0 && $new_payment_amount < 5) {
//                    $error = array('status' => "Failed", "msg" => "Amount Should Be Greater Than 5.");
//                    $this->response($this->json($error), 200);
//                }
//                $final_payment_amount = (float)$new_payment_amount;
//            }
//        }
        if($processing_fee_type==2){
            $fp_amount = $final_payment_amount+$total_processing_fee;
        }else{
            $fp_amount = $final_payment_amount;
        }
        $out = array('status' => "Success", "recurring_payment_status" => $recurring_payment_status, "total_payment_amount"=>$total_payment_amount, "payment_amount_wo_fee"=>$final_payment_amount, "payment_amount"=>$fp_amount, "processing_fee"=>$total_processing_fee, "event_array"=>$event_array, "payment_array"=>$payment_array);
        if($call_back == 0){
            $this->response($this->json($out), 200);
        }else{
            return $out;
        }
    }
    
    protected function checkCountryPaymentSupport($company_id, $country, $callback) { // GET COUNTRY PAYMENT SUPPORT
        $row = [];
        $sql = sprintf("SELECT * FROM country_payment_specific WHERE `payment_support` = 'S' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        UNION
                        SELECT * FROM country_payment_specific WHERE `payment_support` = 'W' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        ORDER BY `payment_support` ASC, `company_id` DESC", mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id)
                , mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                if ($callback == 0) {
                    $out = array("status" => "Success", "msg" => $row);
                } else {
                    $row['status'] = 'Success';
                    return $row;
                }
                $this->response($this->json($out), 200);
            } else {
                $out = array("status" => "Failed", "msg" => "Studio country is not yet available for payment support.");
                return $out;
            }
        }
    }
//    Muthulakshmi
    public function setup_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$studio_name,$buyer_email,$stripe_account_id,$p_type,$cc_type){
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        if ($cc_type == 'N') {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }
        try {
            $setup_intent_create = \Stripe\SetupIntent::create([
                'payment_method' => $payment_method_id,
                'customer' => $stripe_customer_id,
                'confirm' => true,
                'return_url' => $this->server_url."/stripeUpdation.php?type=authenticate&c_id=$company_id&p_type=$p_type&s_id=$student_id",
                'metadata' =>['c_id' => $company_id,'p_type' => $p_type,'s_id' => $student_id ,'s_name' => $studio_name],
                'description' => 'Payment Intent for ' . $buyer_email,
                'payment_method_types' => ['card'],
                'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                'on_behalf_of' => $stripe_account_id,
            ]);
            $body3 = json_encode($setup_intent_create);
            stripe_log_info("Stripe Setup Intent Creation : $body3");
            $return_array['temp_payment_status'] = $setup_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded'){
                $return_array['payment_intent_id'] = $setup_intent_create->id; // return response
                $return_array['setup_intent_client_secret'] = $setup_intent_create->client_secret;
            }else if ($return_array['temp_payment_status'] == 'requires_action'){
                $return_array['payment_intent_id'] = $setup_intent_create->id; // return response
                $return_array['setup_intent_client_secret'] = $setup_intent_create->client_secret;
                $return_array['next_action']['type'] = $setup_intent_create->next_action->type;
                $return_array['next_action']['use_stripe_sdk']['type'] = $setup_intent_create->next_action->use_stripe_sdk->type;
                $return_array['next_action']['use_stripe_sdk']['stripe_js'] = $setup_intent_create->next_action->use_stripe_sdk->stripe_js;         
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }
    
     public function payment_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$amount_in_dollars,$stripe_currency,$studio_name,$buyer_email,$stripe_account_id,$dest_amount_in_dollars,$p_type,$cc_type){
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");   
         if ($cc_type == 'N') {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }
        try {
            $payment_intent_create = \Stripe\PaymentIntent::create([
                'payment_method' => $payment_method_id,
                'customer' => $stripe_customer_id,
                'amount' => $amount_in_dollars,
                'currency' => $stripe_currency,
                'confirmation_method' => 'automatic',
                'capture_method' => 'automatic',
                'confirm' => true,
                'return_url' => $this->server_url."/stripeUpdation.php?type=authenticate&c_id=$company_id&p_type=$p_type&s_id=$student_id",
                'metadata' =>['c_id' => $company_id,'p_type' => $p_type,'s_id' => $student_id ,'s_name' => $studio_name],
                'setup_future_usage' => 'off_session',
                'description' => 'Payment Intent for ' . $buyer_email,
                'payment_method_types' => ['card'],
                'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                'receipt_email' => $buyer_email,
                'transfer_data' => ['destination' => $stripe_account_id, 'amount' => $dest_amount_in_dollars],
                'on_behalf_of' => $stripe_account_id,
                'statement_descriptor' => substr($studio_name,0,21),
            ]);
            $body3 = json_encode($payment_intent_create);
            stripe_log_info("Stripe Payment Creation : $body3");
            $return_array['temp_payment_status'] = $payment_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['cc_month'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_month;
                $return_array['cc_year'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_year;
                $brand = $payment_intent_create->charges->data[0]->payment_method_details->card->brand;
                $last4 = $payment_intent_create->charges->data[0]->payment_method_details->card->last4;
                $temp = ' xxxxxx';
                $return_array['stripe_card_name'] = $brand . $temp . $last4;
            }else if ($return_array['temp_payment_status'] == 'requires_action'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['payment_intent_client_secret'] = $payment_intent_create->client_secret;
                $return_array['next_action'] = $payment_intent_create->next_action;
//                $return_array['next_action']['type'] = $payment_intent_create->next_action->type;
//                $return_array['next_action']['redirect_to_url']['url'] = $payment_intent_create->next_action->redirect_to_url->url;
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }
    
    public function createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name){
        $stripe_customer_id = '';
        $query = sprintf("SELECT `stripe_customer_id` FROM `student` WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                $stripe_customer_id = $row['stripe_customer_id'];
            }
        }
        if(is_null($stripe_customer_id) || empty(trim($stripe_customer_id))){
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");

            try {
                $stripe_customer_create = \Stripe\Customer::create([
                            "email" => $buyer_email,
                            "description" => "Customer created with $buyer_email for $studio_name($company_id)",
                ]);
                $body = json_encode($stripe_customer_create);
                stripe_log_info("Customer_new : $body");
                $body_arr = json_decode($body, true);
                $stripe_customer_id = $body_arr['id'];

                //update stripe customer id to db - arun 
                $query2 = sprintf("UPDATE `student` SET `stripe_customer_id` = '%s' WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $student_id));
                $result2 = mysqli_query($this->db, $query2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }

            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
        
        return $stripe_customer_id;
    }
    
    public function getCompanyStripeAccount($company_id, $call_back){
        $query = sprintf("SELECT * FROM `stripe_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result) >0){
                $row = mysqli_fetch_assoc($result);
                if($call_back == 1){
                    return $row;
                }else{
                    $msg = array("status" => "Success", "msg" => "Stripe Account details", "stripe_details" => "$row");
                    $this->response($this->json($msg), 200);
                }
            }else{
                if($call_back == 1){
                    return '';
                }else{
                    $msg = array("status" => "Failed", "msg" => "Stripe Account details", "stripe_details" => "");
                    $this->response($this->json($msg), 200);
                }
            }
        }
    }
    public function stripeCreateCheckout($optional_discount_flag,$optional_discount,$check_number,$payment_method, $payment_start_date, $total_no_of_payments,$company_id, $event_id, $actual_student_id,$actual_participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $pay_amount, $fee, $processing_fee_type, $discount, $quantity, $ev_array, $pay_array, $payment_frequency, $payment_type, $p_type, $upgrade_status, $total_order_amount, $total_order_quantity, $event_type,$reg_type_user,$reg_version_user,$discount_code,$payment_method_id,$intent_method_type,$cc_type,$stripe_customer_id){
        $payment_from = $text = '';
        $registration_from = $payment_intent_id = '';
        $stripe_account_id = $stripe_currency = $studio_name = '';
        $w_paid_amount = $paid_fee = $paid_amount = 0;
        $sts = $this->getStudioSubscriptionStatus($company_id);
        
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
        if (($payment_method == "CC" && $stripe_account != '' && $stripe_account['charges_enabled'] == 'Y' && $pay_amount > 0) || ($payment_method == "CA" || $payment_method == "CH")|| $pay_amount == 0) {  // ALLOWS FOR CASH & CHECK, IF CC IT CHECK THE CHARGES ENABLED TO BE 'Y'
            
            $upgrade_status = $sts['upgrade_status'];
            $querypf = sprintf("SELECT c.`company_name`, sa.`account_id`, sa.`currency` FROM `company` c 
                        LEFT JOIN `stripe_account` sa ON c.`company_id`=sa.`company_id`
                        WHERE c.`company_id`='%s' ", mysqli_real_escape_string($this->db, $company_id));
            $result = mysqli_query($this->db, $querypf);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$querypf");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $stripe_account_id = $row['account_id'];
                    $stripe_currency = $row['currency'];
                    $studio_name = $row['company_name'];
                }
            }
            
            $gmt_date=gmdate(DATE_RFC822);
            $sns_msg=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone=$user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
            $host = $_SERVER['HTTP_HOST'];
            if(strpos($host, 'mystudio.academy') !== false) {
                $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
            }else{
                $tzadd_add ="'".$curr_time_zone."'";
            }
            $recurring_payment_status = '';
            $checkout_amount = 0;
            $payment_amount = $pay_amount;
            $event_array = $ev_array;
            $payment_array = $pay_array;
            $this->checkQuantityDuringCheckout($company_id,$event_id,$ev_array,$quantity, $event_type);
            if($optional_discount_flag=='Y' && $optional_discount>0){
                $opt_disc = $this->calculateOptionalDiscountForevents($optional_discount, $payment_method, $company_id, $pay_amount, $processing_fee_type, $quantity, $ev_array, $pay_array, $total_order_amount, $total_order_quantity, $payment_start_date, $payment_type, $total_no_of_payments, 1);
                if($opt_disc['status']=='Success'){
                    $recurring_payment_status = $opt_disc['recurring_payment_status'];
                    $payment_amount = $opt_disc['total_payment_amount'];
                    $checkout_amount = $opt_disc['payment_amount_wo_fee'];
                    $event_array = $opt_disc['event_array'];
                    $payment_array = $opt_disc['payment_array'];
                }else{
                    $error = array("status" => "Failed", "msg" => $opt_disc['msg']);
                    $this->response($this->json($error), 200);
                }
            }
            $buyer_name = $buyer_first_name.' '.$buyer_last_name;
            if (is_null($actual_student_id) || $actual_student_id == 0) {
                $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Stripe', $studio_name);
                $student_id = $stud_array['student_id'];
                $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
            }else {
                $student_id = $actual_student_id;
                if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                    $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                }
            }
            if (is_null($actual_participant_id) || $actual_participant_id == 0) {
                $participant_id = $this->getParticipantIdForEvent($company_id, $student_id, $reg_col1, $reg_col2);
            } else {
                $participant_id = $actual_participant_id;
            }
            $balance_due = $deposit_amt = $pr_fee = $temp_pr_fee = 0;
            $authorize_only = 0;
            $no_of_payments = 1;
            $payment_startdate = $current_date = date("Y-m-d");
            $p_f = $this->getProcessingFee($company_id,$upgrade_status);
            $process_fee_per = $p_f['PP'];
            $process_fee_val = $p_f['PT'];
            if($fee==0 && $processing_fee_type==1){
                $fee = number_format((float)($payment_amount*$process_fee_per/100+$process_fee_val), 2, '.', '');
            }
            if($payment_method!='CC'){
                $fee = 0;
            }
            if(count($payment_array)>0){
                $no_of_payments = count($payment_array)-1;
                for($i=0;$i<count($payment_array);$i++){
                    if(isset($payment_array[$i]['date']) && empty($payment_array[$i]['date'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),400);
                    }            
                    if(isset($payment_array[$i]['amount']) && empty($payment_array[$i]['amount'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),400);
                    }
                    if(isset($payment_array[$i]['type']) && empty($payment_array[$i]['type'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),400);
                    }

                    if($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount']>0){
    //                    $balance_due = $payment_array[$i]['amount'];
                        $deposit_amt = $payment_array[$i]['amount'];
                        if($optional_discount_flag!='Y'){
                            $checkout_amount = $deposit_amt;
                        }
                        if($payment_method!='CC'){
                            $balance_due = $payment_array[$i]['amount'];
                        }
                        $pr_fee = $payment_array[$i]['processing_fee'];
                    }elseif($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount'] == 0){
                        $authorize_only = 1;
                        $no_of_payments = count($payment_array);
                    }
                    $temp_pr_fee += $payment_array[$i]['processing_fee'];
                }
                $fee = $temp_pr_fee;
            }else{
                $pr_fee = $fee;
                if($optional_discount_flag!='Y'){
                    $checkout_amount = $payment_amount;
                }
            }

            usort($event_array, function($a, $b) {
                return $a['event_begin_dt'] > $b['event_begin_dt'];
            });
            
            if(count($event_array)>0){
                for($i=0;$i<count($event_array);$i++){
                    if(isset($event_array[$i]['event_id']) && empty($event_array[$i]['event_id'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),400);
                    }            
                    if(isset($event_array[$i]['event_quantity']) && empty($event_array[$i]['event_quantity'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),400);
                    }
                    if(!isset($event_array[$i]['event_cost'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),400);
                    }
                    if(!isset($event_array[$i]['event_discount_amount'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),400);
                    }            
                    if(!isset($event_array[$i]['event_payment_amount'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),400);
                    }            
                    if(!isset($event_array[$i]['event_begin_dt'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),400);
                    }
                    if(count($payment_array)>0){
                        if($balance_due>0){
                            $amt = $event_array[$i]['event_payment_amount'];
                            if($balance_due>=$event_array[$i]['event_payment_amount']){
                                if($balance_due==$amt){
                                    $t_amt = $amt;
                                }else{
                                    $t_amt = $balance_due - $amt;
                                }
                                $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($t_amt, $processing_fee_type, $process_fee_per, $process_fee_val);
                                $event_array[$i]['balance_due'] = 0;
                                $balance_due = $balance_due - $amt;
                            }else{
                                $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($balance_due, $processing_fee_type, $process_fee_per, $process_fee_val);
                                $event_array[$i]['balance_due'] = $amt - $balance_due;
                                $balance_due = 0;
                            }
                        }elseif($balance_due==0){
                            $event_array[$i]['processing_fee'] = 0;
                            $event_array[$i]['balance_due'] = $event_array[$i]['event_payment_amount'];
                        }
                    }else{
                        if($payment_method=='CC'){
                            $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($event_array[$i]['event_payment_amount'], $processing_fee_type, $process_fee_per, $process_fee_val);
                        }else{
                            $event_array[$i]['processing_fee'] = 0;
                        }
                        $event_array[$i]['balance_due'] = 0;
                    }
                }
            }
            $paid_fee = $fee; // for deposit discount
            // Amount calculation
            if($payment_method == "CC"){
                if($processing_fee_type==2){
                    $w_paid_amount = $payment_amount + $fee;
                }elseif($processing_fee_type==1){
                    $w_paid_amount = $payment_amount;
                }
                if (count($payment_array) > 0) {
                    for ($i = 0; $i < count($payment_array); $i++) {
                        if ($payment_array[$i]['type'] == 'D') {
                            if ($processing_fee_type == 2) {
                                $w_paid_amount = $payment_array[$i]['amount'] + $payment_array[$i]['processing_fee'];
                                $paid_fee = $payment_array[$i]['processing_fee'];
                            } elseif ($processing_fee_type == 1) {
                                $w_paid_amount = $payment_array[$i]['amount'];
                                $paid_fee = $payment_array[$i]['processing_fee'];
                            }
                            break;
                        }
                    }
                }
            }else{
                if($recurring_payment_status=='N'){
                    $paid_amount = $payment_amount;
                }else{
                    $paid_amount = $checkout_amount;
                }
            }
            
            // STRIPE ACTION START
            $amount_in_dollars = $dest_amount = $dest_amount_in_dollars = $w_paid_amount_round = $dest_amount_round = 0;
            $checkout_state = $cc_month = $cc_year = $stripe_card_name = $payment_status = $payment_intent_id = '';
            $next_action_type = $next_action_type2 = $button_url = '';
            $has_3D_secure = 'N';
                               
            if (!empty($stripe_customer_id) && !(is_null($stripe_customer_id)) && !empty($payment_method_id) && $payment_method == 'CC') {
                
                $this->getStripeKeys();
                \Stripe\Stripe::setApiKey($this->stripe_sk);
                \Stripe\Stripe::setApiVersion("2019-05-16");
                    
                if ($payment_amount > 0) {
                    $w_paid_amount_round = round($w_paid_amount, 2);
                    $dest_amount = $w_paid_amount - $paid_fee;
                    $dest_amount_round = round($dest_amount, 2);
                    $dest_amount_in_dollars = $dest_amount_round * 100;
                    $amount_in_dollars = $w_paid_amount_round * 100;
                    
                    $checkout_type = 'event';
                    if($intent_method_type == "SI"){ // If it is setup intent creation
                       $payment_intent_result = $this->setup_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $studio_name, $buyer_email, $stripe_account_id, $checkout_type,$cc_type); 
                    }else{ //If it is payment intent creation
                        $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $checkout_type,$cc_type);
                    }
                    if ($payment_intent_result['temp_payment_status'] == 'succeeded'){
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'released';
                    }elseif($payment_intent_result['temp_payment_status'] == 'requires_action') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'requires_action';
                        $has_3D_secure = 'Y';
                        $next_action_type = $payment_intent_result['next_action']['type'];
                        if($next_action_type != "redirect_to_url"){
                            # Payment not supported
                            $out['status'] = 'Failed';
                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                            $this->response($this->json($out), 200);
                        }
                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                    } else {
                        # Invalid status
                        $out['status'] = 'Failed';
                        $out['msg'] = 'Invalid SetupIntent status.';
                        $out['error'] = 'Invalid SetupIntent status';
                        $this->response($this->json($out), 200);
                    }
                    $payment_status = 'S';
                }
            } 
            if(!empty($payment_method_id)) {

                try {
                    $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                    $brand = $retrive_payment_method->card->brand;
                    $cc_month = $retrive_payment_method->card->exp_month;
                    $cc_year = $retrive_payment_method->card->exp_year;
                    $last4 = $retrive_payment_method->card->last4;
                    $temp = ' xxxxxx';
                    $stripe_card_name = $brand . $temp . $last4;
                } catch (\Stripe\Error\Card $e) {
                    // Since it's a decline, \Stripe\Error\Card will be caught     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\RateLimit $e) {
                    // Too many requests made to the API too quickly     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\InvalidRequest $e) {
                    // Invalid parameters were supplied to Stripe's API     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Authentication $e) {
                    // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\ApiConnection $e) {
                    // Network communication with Stripe failed     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Base $e) {
                    // Display a very generic error to the user, and maybe send yourself an email     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (Exception $e) {
                    // Something else happened, completely unrelated to Stripe     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                }
                if (isset($err) && !empty($err)) {
                    $error = array('status' => "Failed", "msg" => $err['message']);
                    $this->response($this->json($error), 200);
                }
            }
            $payment_from = 'S';
            $registration_from = 'S';
            if ($payment_method == 'CA' || $payment_method == 'CH'){
                $payment_status = 'M';
            }
            
            $query = sprintf("INSERT INTO `event_registration`(`optional_discount_flag`,`check_number`,`payment_method`,`paid_amount`,`company_id`, `event_id`, `reg_event_type`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`,`buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                    `payment_type`, `payment_amount`, `deposit_amount`, `number_of_payments`, `payment_startdate`, `payment_frequency`, `total_order_amount`, `total_order_quantity`, 
                    `event_registration_column_1`, `event_registration_column_2`, `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`, `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`,`event_reg_type_user`,`event_reg_version_user`,`payment_method_id`,`stripe_card_name`,`stripe_customer_id`,`registration_from`, `credit_card_expiration_month`, `credit_card_expiration_year`)
                        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $optional_discount_flag), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $paid_amount),
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_type), mysqli_real_escape_string($this->db, $student_id), 
                        mysqli_real_escape_string($this->db, $participant_id),mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name),mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code),
                        mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $p_type), 
                        mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $deposit_amt), mysqli_real_escape_string($this->db, $no_of_payments), mysqli_real_escape_string($this->db, $payment_startdate), mysqli_real_escape_string($this->db, $payment_frequency), 
                        mysqli_real_escape_string($this->db, $total_order_amount), mysqli_real_escape_string($this->db, $total_order_quantity), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4),
                        mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10),
                        mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user),mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name),mysqli_real_escape_string($this->db, $stripe_customer_id),mysqli_real_escape_string($this->db, $registration_from),mysqli_real_escape_string($this->db, $cc_month),mysqli_real_escape_string($this->db, $cc_year));
                $result = mysqli_query($this->db, $query);
                if(!$result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }else{
                    $event_reg_id = mysqli_insert_id($this->db);

                    if(count($payment_array)>0){
                        $temp1 = $temp4 = 0;
                        for($i=0;$i<count($payment_array);$i++){
                            $pdate = $payment_array[$i]['date'];
                            $pamount = $payment_array[$i]['amount'];
                            $ptype = $payment_array[$i]['type'];
                            $pfee = $payment_array[$i]['processing_fee'];
                            if($ptype!='D'){
                                $pstatus = 'N';
                                $pi_id = '';
                            }else{
                                $pi_id = $payment_intent_id;
                                if($payment_method=='CC'){
                                    $pstatus = 'S';
                                }else{
                                    $pstatus = 'M';
                                }
                            }

                            $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_date`, `schedule_status`, `credit_method`, `check_number`, `payment_method_id`,`stripe_card_name`,`payment_intent_id`,`payment_from`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_state), 
                                    mysqli_real_escape_string($this->db, $pfee), mysqli_real_escape_string($this->db, $pamount), mysqli_real_escape_string($this->db, $pdate), mysqli_real_escape_string($this->db, $pstatus)
                                    ,mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $check_number),mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name),mysqli_real_escape_string($this->db, $pi_id), mysqli_real_escape_string($this->db, $payment_from));
                            $payment_result = mysqli_query($this->db, $payment_query);
                            if(!$payment_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                                log_info($this->json($error_log));
                                $temp1 += 1;
                            }
                        }
                        if($temp1>0){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp1);
                            $this->response($this->json($error), 500);
                        }
                    }else{
                        if($payment_method=='CC'){
                            $pstatus = 'S';
                        }else{
                            $pstatus = 'M';
                        }
                        $payment_query = sprintf("INSERT INTO `event_payment`(`credit_method`,`check_number`,`event_reg_id`, `student_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_status`, `payment_method_id`,`stripe_card_name`,`payment_intent_id`,`payment_from`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $check_number),
                                mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_state), 
                                mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $pstatus),mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name),mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $payment_from));
                        $payment_result = mysqli_query($this->db, $payment_query);
                        if(!$payment_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }

                    if(count($event_array)>0){
                        $qty = $net_sales = $parent_net_sales = 0;
                        $temp = $temp2= $temp3 = 0;
                        for($i=0;$i<count($event_array);$i++){
                            $cevent_id = $event_array[$i]['event_id'];
                            $cquantity =  $event_array[$i]['event_quantity'];
                            $cevent_cost = $event_array[$i]['event_cost'];
                            $cdiscount = $event_array[$i]['event_discount_amount'];
                            $cpayment_amount = $event_array[$i]['event_payment_amount'];
                            $cbalance = $event_array[$i]['balance_due'];
                            $cprocessing_fee = $event_array[$i]['processing_fee'];
                            if(count($payment_array)>0){
                                if($cbalance>0){
                                    $cstatus = 'RP';
                                }else{
                                    $cstatus = 'RC';
                                }
                            }else{
                                $cstatus = 'CC';
                            }

                            $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $cevent_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cevent_cost), 
                                    mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cbalance), mysqli_real_escape_string($this->db, $cprocessing_fee), mysqli_real_escape_string($this->db, $cstatus));
                            $reg_det_result = mysqli_query($this->db, $reg_det_query);
                            if(!$reg_det_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                                log_info($this->json($error_log));
                                $temp += 1;
                            }

                            $pd_amount = $cpayment_amount - $cbalance;

                            if(count($payment_array)>0){
                                if($processing_fee_type==2){
                                    $net_sales += $pd_amount;
                                    $parent_net_sales += $pd_amount;
                                }elseif($processing_fee_type==1){
                                    $net_sales += $pd_amount-$cprocessing_fee;
                                    $parent_net_sales += $pd_amount-$cprocessing_fee;
                                }
                            }else{
                                if($processing_fee_type==2){
                                    $parent_net_sales = $payment_amount;
                                }else{
                                    $parent_net_sales = $payment_amount-$fee;
                                }
                            }
                            $qty += $cquantity;
                            if($payment_method!='CC'){
                                $nt_sales_str = ", `net_sales`=`net_sales`+$pd_amount";
                            }else{
                                $nt_sales_str = '';
                            }
//                                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                            $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity $nt_sales_str WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                            $update_event_result = mysqli_query($this->db, $update_event_query);
                            if(!$update_event_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                                log_info($this->json($error_log));
                                $temp2 += 1;
                            }
                        }
                        if($temp>0){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp);
                            $this->response($this->json($error), 500);
                        }
                        if($temp2>0){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp2);
                            $this->response($this->json($error), 500);
                        }
                        if($payment_method!='CC'){
                            $nt_sales_str = ", `net_sales`=`net_sales`+$parent_net_sales";
                        }else{
                            $nt_sales_str = '';
                        }

                        $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$qty $nt_sales_str WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                        $update_event_result = mysqli_query($this->db, $update_event_query);
                        if(!$update_event_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }else{
//                            $sbalance = $payment_amount - $paid_amount;
                        if($payment_method=='CC'){
                            $sbalance = $payment_amount;
                        }else{
                            if($p_type=='CO'){
                                $sbalance = 0;
                            }else{
                                $sbalance = $payment_amount-$deposit_amt;
                            }
                        }
                        if($sbalance>0){
                            $fee = $pr_fee;
                        }
//                            if($processing_fee_type==1){
//                                $paid_amount -= $fee;
//                            }
                        if(count($payment_array)>0){
                            if($sbalance>0){
                                $sstatus = 'RP';
                            }else{
                                $sstatus = 'RC';
                            }
                        }else{
                            $sstatus = 'CC';
                        }
                        $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $registration_amount), 
                                mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $quantity), mysqli_real_escape_string($this->db, $sbalance), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $sstatus));
                        $reg_det_result = mysqli_query($this->db, $reg_det_query);
                        if(!$reg_det_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }

                        if($payment_method!='CC'){
                            $nt_sales_str = ", `net_sales`=`net_sales`+$paid_amount";
                        }else{
                            $nt_sales_str = '';
                        }
                        $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity $nt_sales_str WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                        $update_event_result = mysqli_query($this->db, $update_event_query);
                        if(!$update_event_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }
                    
                    //update cc information if release state
                    if ($checkout_state == 'released') {
                         $this->updateStripeCCDetailsAfterRelease($payment_intent_id);
                    }
                    //End update cc information if release state
                    
                    $selectcurrency=sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'",  mysqli_escape_string($this->db, $company_id));
                    $resultselectcurrency=  mysqli_query($this->db, $selectcurrency);
                    if(!$resultselectcurrency){
                         $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                        log_info($this->json($error_log));
                    }else{
                        $row = mysqli_fetch_assoc($resultselectcurrency);
                        $wp_currency_symbol=$row['wp_currency_symbol'];
                    }

                    $curr_date = gmdate("Y-m-d H:i:s");
                    if ($event_type == 'S') {
                        if (!empty($discount_code)) {
                            $activity_text = "Registered for " . $event_name . ', quantity ' . $quantity . '. Total cost ' . $wp_currency_symbol . $payment_amount . '. Discount code used ' . $discount_code;
                        } else {
                            if (empty($discount)) {
                                if($optional_discount_flag=='Y' && $optional_discount>0){
                                    $optional_str = " Optional Discount amount used $optional_discount";
                                }else{
                                    $optional_str = "";
                                }
                                $activity_text = "Registered for $event_name, quantity $quantity. Total cost $wp_currency_symbol"."$payment_amount.$optional_str";
                            } else {
                                $activity_text = "Registered for " . $event_name . ', quantity ' . $quantity . '. Total cost ' . $wp_currency_symbol . $payment_amount.". Discount amount used $discount";
                            }
                        }
                        $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id),
                                'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                        $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                        if (!$result_insert_event_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                            log_info($this->json($error_log));
                        }
                    } else {
                        for ($i = 0; $i < count($event_array); $i++) {
                            $event_title = $event_array[$i]['event_title'];
                            $event_quantity = $event_array[$i]['event_quantity'];
                            $event_cost = $event_array[$i]['event_cost'];
                            $event_discount_amount = $event_array[$i]['event_discount_amount'];
                            if (!empty($discount_code)) {
                                $activity_text = "Registered for " . $event_name . " " . $event_title . ',quantity ' . $event_quantity . '.Total cost ' . $wp_currency_symbol . $event_cost . '.Discount code used ' . $discount_code;
                            } else {
                                if (empty($event_discount_amount)) {
                                    if($optional_discount_flag=='Y' && $optional_discount>0){
                                        $optional_str = " Optional Discount amount used $optional_discount";
                                    }else{
                                        $optional_str = "";
                                    }
                                    $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol"."$event_cost.$optional_str";
                                } else {
                                    $activity_text = "Registered for " . $event_name . " " . $event_title . ',quantity ' . $event_quantity . '.Total cost ' . $wp_currency_symbol . $event_cost.".  Discount amount used $event_discount_amount";
                                }
                            }
                            $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id),
                                    'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                            $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                            if (!$result_insert_event_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                                log_info($this->json($error_log));
                            }
                        }
                    }
                    
                    if ($has_3D_secure == 'N') {
                        if($payment_amount>0){
                            $text = "Registration successful. Confirmation of purchase has been sent to your email.";
                        }else{
                            $text = "Registration successful.";
                        }
                    } else {
                        $text = "Event charge was processed and waiting for customer action. Kindly review and authenticate the payment.";
                    }
                    $msg = array("status" => "Success", "msg" => $text);
                    $this->responseWithWepay($this->json($msg), 200);
                    if ($has_3D_secure == 'N') {
                        $this->sentEmailForEventPaymentSuccess($company_id,$event_reg_id);
                    } else {
                        $email_msg = 'Hi '.$buyer_first_name.' '.$buyer_last_name.',<br>';
                        $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">This is an authentication email for Event '.$event_name.' registration. Kindly click the launch button for redirecting to Stripe.</p>';
                        $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">Complete a required action for this payment. Once the redirect is completed for this PaymentIntent, the payment will proceed.</p>';
                        $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg);
                    }
                }
        } else {
            $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
            $this->response($this->json($error), 200);
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    // 3D secure authentication email
    public function sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg){
        
        // EMAIL CONTENT
        $button_text = 'Launch';
        $subject = 'Stripe authenticate';
        $message = $email_msg.'<br>'.'<center><a href="'.$button_url.'"><button type="button" style="width: 190px;border-radius: 5px;background: #009a61;border:1px solid 009a61;color: #ffffff;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 45px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;text-transform: none;overflow: visible;box-sizing: border-box;cursor: pointer;">'.$button_text.'</button></a></center>';
        $sendEmail_status = $this->sendEmail($buyer_email, '', $subject, $message, $studio_name, '', true);
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
    }
    
    public function updateStripeCCDetailsAfterRelease($payment_intent_id){
        $event_reg_id = $event_id = $payment_id = $processing_fee_type = $payment_type = '';
        $payment_amount = 0;
        
        $query = sprintf("SELECT er.company_id, er.`event_reg_id`, ep.`event_payment_id`, e.`event_title`, ep.`payment_amount`, ep.`processing_fee`, er.`processing_fee_type`, er.`event_id`, er.`payment_type`
                FROM `event_registration` er 
                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` 
                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
                WHERE ep.`payment_intent_id` = '%s' ORDER BY ep.`event_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        log_info($this->json($query));
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $event_reg_id = $row['event_reg_id'];
                $event_id = $row['event_id'];
                $payment_id = $row['event_payment_id'];
                $payment_amount = $row['payment_amount'];
                $processing_fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_type = $row['payment_type'];
                
                $temp_payment_amount = $payment_amount;
                $onetime_netsales_for_parent_event = 0;

                $update_event_reg_query = sprintf("UPDATE `event_registration` SET `paid_amount`=`paid_amount`+$payment_amount WHERE `event_reg_id`='%s'",$event_reg_id);
                $result_update_event_reg_query = mysqli_query($this->db, $update_event_reg_query);
                if(!$result_update_event_reg_query){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_reg_query", "ipn" => "1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }

                if($payment_type=='CO'){
                    $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_type`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
                               LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
                               AND `payment_status` IN ('CC') ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                    $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                    if(!$get_event_details_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query", "ipn" => "1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $num_rows2 = mysqli_num_rows($get_event_details_result);
                        if($num_rows2>0){
                            $rem_due = 0;
                            while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                                if($temp_payment_amount>0){
                                    $event_reg_details_id = $row2['event_reg_details_id'];
                                    $child_id = $row2['event_id'];
                                    $event_parent_id = $row2['parent_id'];
                                    $child_payment = $row2['payment_amount'];
                                    $rem_due = $row2['balance_due'];
                                    $event_type = $row2['event_type'];
                                    $payment_status = $row2['payment_status'];
                                    $child_net_sales = 0;
                                    if($onetime_netsales_for_parent_event==0){
                                        if($processing_fee_type==2){
                                            $net_sales = $payment_amount;
                                        }elseif($processing_fee_type==1){
                                            $net_sales = $payment_amount-$processing_fee;
                                        }
                                        $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
                                        $result_update_event_query = mysqli_query($this->db, $update_event_query);
                                        if(!$result_update_event_query){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query", "ipn" => "1");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        }
                                        $onetime_netsales_for_parent_event=1;
                                    }
                                    if($temp_payment_amount>=$child_payment){
                                        $temp_payment_amount = $temp_payment_amount-$child_payment;
                                        $child_net_sales = $child_payment;
                                        $child_payment = 0;
                                    }else{
                                        $child_payment = $child_payment - $temp_payment_amount;
                                        $child_net_sales = $temp_payment_amount;
                                        $temp_payment_amount = 0;
                                    }
                                    $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$child_payment WHERE `event_reg_details_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
                                    $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
                                    if(!$result_update_reg_query){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query", "ipn" => "1");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 500);
                                    }
                                    if($child_id!=$event_parent_id){
                                        $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
                                        $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
                                        if(!$result_update_child_query){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales", "ipn" => "1");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_type`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
                               LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
                               AND `payment_status` = 'RP' ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                    $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                    if(!$get_event_details_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query", "ipn" => "1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $num_rows2 = mysqli_num_rows($get_event_details_result);
                        if($num_rows2>0){
                            $rem_due = 0;
                            while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                                if($temp_payment_amount>0){
                                    $event_reg_details_id = $row2['event_reg_details_id'];
                                    $child_id = $row2['event_id'];
                                    $event_parent_id = $row2['parent_id'];
                                    $rem_due = $row2['balance_due'];
                                    $event_type = $row2['event_type'];
                                    $payment_status = $row2['payment_status'];
                                    $child_net_sales = 0;
                                    if($onetime_netsales_for_parent_event==0){
                                        if($processing_fee_type==2){
                                            $net_sales = $payment_amount;
                                        }elseif($processing_fee_type==1){
                                            $net_sales = $payment_amount-$processing_fee;
                                        }
                                        $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
                                        $result_update_event_query = mysqli_query($this->db, $update_event_query);
                                        if(!$result_update_event_query){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query", "ipn" => "1");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        }
                                        $onetime_netsales_for_parent_event=1;
                                    }
                                    if($temp_payment_amount>=$rem_due){
                                        $temp_payment_amount = $temp_payment_amount-$rem_due;
                                        $child_net_sales = $rem_due;
                                        $rem_due = 0;
                                        $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due, `payment_status`='RC' WHERE `event_reg_details_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
                                    }else{
                                        $rem_due = $rem_due - $temp_payment_amount;
                                        $child_net_sales = $temp_payment_amount;
                                        $temp_payment_amount = 0;
                                        $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due WHERE `event_reg_details_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
                                    }
                                    $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
                                    if(!$result_update_reg_query){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query", "ipn" => "1");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 500);
                                    }else{
                                        if($child_id!=$event_parent_id){
                                            $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
                                            $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
                                            if(!$result_update_child_query){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales", "ipn" => "1");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 500);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $payment_status = "";
                $checkout_state = "released";
                
                $update_query = sprintf("UPDATE `event_payment` SET `checkout_status`='%s' $payment_status WHERE `event_reg_id`='%s' and `event_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                        mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                $update_result = mysqli_query($this->db, $update_query);
                if(!$update_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query", "ipn" => "1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }
    }
    
}

?>
