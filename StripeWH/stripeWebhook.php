<?php

require_once '../Globals/cont_log.php';
require_once __DIR__.'/../Stripe/init.php';
require_once('class.phpmailer.php');
require_once('class.smtp.php');

class stripeWebhook {
    
    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $stripe;
    private $stripe_pk = '', $stripe_sk = '', $wh_charge_keys = '', $wh_customers_keys = '',$wh_ca_keys = '',$wh_payment_intent_keys = '';
    private $user_agent = '';
    
    public function __construct(){
        require_once '../Globals/config.php';
        require_once '../Globals/stripe_props.php';
        $this->inputs();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        $this->user_agent = $this->getUserAgent();
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey("$this->stripe_sk");
    }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            stripe_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }
    
    private function getUserAgent(){
        if (!function_exists('getallheaders')){
            if(!function_exists ('apache_request_headers')){
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            }else{
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
            }
        }else{
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
        }
        return $user_agent;
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithMail($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
        header("Content-Type:" . $this->_content_type);
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function processApi(){
        $func = strtolower(trim(str_replace("/","",$_REQUEST['run'])));
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)){
            $this->_request = json_decode(file_get_contents('php://input'), true);
 	}
        
        if((int)method_exists($this,$func) > 0){
            $this->$func();
        }else{
            $error = array("status" => "Failed", "msg" => "Page not found.");
            $this->response($this->json($error),404); // If the method not exist with in this class, response would be "Page not found".
        }
    }  

    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
        $this->wh_charge_keys = $GLOBALS['stripe_keys']['webhooks_charge'];
        $this->wh_payment_intent_keys = $GLOBALS['stripe_keys']['webhooks_payment_intent'];
        $this->wh_customers_keys = $GLOBALS['stripe_keys']['webhooks_customers'];
        $this->wh_ca_keys = $GLOBALS['stripe_keys']['webhooks_connected_account'];
    }

    public function getChargeDetails() {
        $payload = @file_get_contents('php://input');


        $endpoint_secret =$this->wh_charge_keys;

        $sig_header = $_SERVER["HTTP_STRIPE_SIGNATURE"];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                            $payload, $sig_header, $endpoint_secret
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400); // PHP 5.4 or greater
            exit();
        } catch (\Stripe\Error\SignatureVerification $e) {
            // Invalid signature
            http_response_code(400); // PHP 5.4 or greater
            exit();
        }

        if (!empty($event) && !is_null($event)) {
            try {
                $charge = \Stripe\Charge::retrieve($event->data->object->id);
            } catch (Exception $ex) {
                $body = $ex->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $ex->getHttpStatus();
                stripe_log_info($this->json($err));
                http_response_code(400);
                exit();
            }
            $body = json_encode($charge);
            $charge_aray=explode('.',$charge->type);
            switch ($event->type) {
                case "charge.captured":                    
                    $selectsql=sprintf("SELECT  `company_id`, `sss_id`, `source_id`, `charge_id`, `charge_amount`, `charge_status`, `charge_type` FROM `stripe_studio_subscription_payments_history` WHERE `charge_id`='%s' AND `charge_status`!='captured'", mysqli_real_escape_string($this->db, $charge->id));
                    $resultselectsql= mysqli_query($this->db, $selectsql);
                    if(!$resultselectsql){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else {
                        if(mysqli_num_rows($resultselectsql)>0) {
                            $rows_value = mysqli_fetch_object($selectsql);
                            if(strtolower($rows_value->charge_type)=='pending'){
                                $insertsql = sprintf("INSERT INTO `stripe_studio_subscription_payments`( `company_id`, `sss_id`, `source_id`, `charge_id`, `charge_amount`, `payment_amount`, `charge_status`,`charge_type`)
                                   VALUES ('%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $rows_value->company_id), mysqli_real_escape_string($this->db, $rows_value->sss_id), mysqli_real_escape_string($this->db, $rows_value->source_id),
                                        mysqli_real_escape_string($this->db, $rows_value->charge_id), mysqli_real_escape_string($this->db, $rows_value->charge_amount), mysqli_real_escape_string($this->db, $rows_value->payment_amount), 
                                        mysqli_real_escape_string($this->db, 'captured'),mysqli_real_escape_string($this->db, $rows_value->charge_type));
                                $resultinsertsql = mysqli_query($this->db, $insertsql);
                                if (!$resultinsertsql) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insertsql");
                                    stripe_log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }

                                $update= sprintf("UPDATE `stripe_studio_subscription` s, `company` c SET s.outstanding_balance = s.outstanding_balance-$rows_value->charge_amount, s.`last_succesfull_payment_date`=CURDATE(), c.`stripe_last_success_payment`=CURDATE(),c.`subscription_status`='Y',c.`studio_payment_status`='A',c.`studio_expiry_level`='A' WHERE s.`sss_id`='%s' and c.`company_id`=s.`company_id` AND c.`company_id`='%s'", 
                                        mysqli_real_escape_string($this->db, $rows_value->sss_id), mysqli_real_escape_string($this->db, $rows_value->company_id));
                                $resultupdate = mysqli_query($this->db, $update);
                                if (!$resultupdate) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update");
                                    stripe_log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }
                    break;
                case "charge.expired":
                    $updatesql=sprintf("UPDATE `stripe_studio_subscription_payments_history` SET `charge_status`='Expired', `charge_type`='%s' WHERE `charge_id`='%s'", mysqli_real_escape_string($this->db, $charge_aray[1]), mysqli_real_escape_string($this->db, $charge->id));
                    $resultupdatesql = mysqli_query($this->db, $updatesql);
                    if (!$resultupdatesql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesql");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $getcompanyid = sprintf("SELECT  `company_id` FROM `stripe_studio_subscription_payments_history` WHERE `charge_id`='%s'", mysqli_real_escape_string($this->db, $charge->id));
                    $resultgetcompanyid = mysqli_query($this->db, $updatesql);
                    if (!$resultgetcompanyid) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getcompanyid");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else {
                        $num_rows = mysqli_num_rows($resultgetcompanyid);
                        if($num_rows>0){
                            $row = mysqli_fetch_assoc($resultgetcompanyid);
                            $studio_company_id = $row['company_id'];
                        }    
                    }
                    $updatecompany=sprintf("UPDATE `company` SET `studio_payment_status`='F' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $studio_company_id));
                    $resultupdatecompany = mysqli_query($this->db, $updatesql);
                    if (!$resultupdatecompany) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatecompany");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }                    
                    break;
                case "charge.failed":   
                    $updatesql=sprintf("UPDATE `stripe_studio_subscription_payments_history` SET `charge_status`='Failed', `charge_type`='%s' WHERE `charge_id`='%s'", mysqli_real_escape_string($this->db, $charge_aray[1]), mysqli_real_escape_string($this->db, $charge->id));
                    $resultupdatesql = mysqli_query($this->db, $updatesql);
                    if (!$resultupdatesql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesql");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $getcompanyid = sprintf("SELECT  `company_id` FROM `stripe_studio_subscription_payments_history` WHERE `charge_id`='%s'", mysqli_real_escape_string($this->db, $charge->id));
                    $resultgetcompanyid = mysqli_query($this->db, $updatesql);
                    if (!$resultgetcompanyid) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getcompanyid");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500); 
                    }else {
                        $num_rows = mysqli_num_rows($resultgetcompanyid);
                        if($num_rows>0){
                            $row = mysqli_fetch_assoc($resultgetcompanyid);
                            $studio_company_id = $row['company_id'];
                        }    
                    }
                    
                    $updatecompany=sprintf("UPDATE `company` SET `studio_payment_status`='F' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $studio_company_id));
                    $resultupdatecompany = mysqli_query($this->db, $updatesql);
                    if (!$resultupdatecompany) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatecompany");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    break;
                case "charge.refunded":
                    $sql_sss = sprintf("SELECT `sss_id` FROM `stripe_studio_subscription` s
                            WHERE  s.`stripe_customer_id`='%s' AND s.`status`='A' AND `deleted_flag`!='Y' order by s.`sss_id` desc limit 0,1",
                            mysqli_real_escape_string($this->db, $charge->customer));
                    $result_sss = mysqli_query($this->db, $sql_sss);
                    if(!$result_sss){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_sss");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $sql_sss_numrows = mysqli_num_rows($result_sss);
                        if($sql_sss_numrows>0){
                            $row = mysqli_fetch_assoc($result_sss);
                            $sss_id= $row['sss_id'];
                                $refunded_amt=mysqli_real_escape_string($this->db, $charge->amount_refunded);
                                $captured_amt=mysqli_real_escape_string($this->db, $charge->amount);
                                if($refunded_amt<$captured_amt){
                                   $payment_status='Partially Refunded'; 
                                }else{
                                    $payment_status='Refunded';
                                }
                                $charge_amount=number_format($refunded_amt/100, 2, '.', '');
                                $updatesql=sprintf("UPDATE `stripe_studio_subscription_payments` ssp ,`stripe_studio_subscription` sss SET ssp.`charge_status`='%s' ,ssp.`charge_type`='%s', ssp.`refunded_amount`='%s', sss.`outstanding_balance` = sss.`outstanding_balance`+'%s'   WHERE ssp.`charge_id`='%s' AND sss.`sss_id`='%s'",$payment_status, mysqli_real_escape_string($this->db, $charge_aray[1]),$charge_amount, $charge_amount, mysqli_real_escape_string($this->db, $charge->id),$sss_id);
                                $resultupdatesql = mysqli_query($this->db, $updatesql);
                                if (!$resultupdatesql) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesql");
                                    stripe_log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                        }
                    }
            
                    break;
                default:
                    stripe_log_info("default : $body");
            }
            http_response_code(200); // Success
            exit();
        }else{
            http_response_code(400); // PHP 5.4 or greater
            exit();
        }
    }
    
    public function getCustomerDetails() {
        $payload = @file_get_contents('php://input');


        $endpoint_secret = $this->wh_customers_keys;

        $sig_header = $_SERVER["HTTP_STRIPE_SIGNATURE"];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                            $payload, $sig_header, $endpoint_secret
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400); // PHP 5.4 or greater
            exit();
        } catch (\Stripe\Error\SignatureVerification $e) {
            // Invalid signature
            http_response_code(400); // PHP 5.4 or greater
            exit();
        }

        if (!empty($event) && !is_null($event)) {
            try {
              $customers = \Stripe\Customer::retrieve($event->data->object->id);
            } catch (Exception $ex) {
                stripe_log_info($ex);
                http_response_code(400);
                exit();
            }
            $body = json_encode($customers);
            switch ($event->type) {
                case "customer.deleted":
                    $updatesql=sprintf("UPDATE `stripe_studio_subscription` SET `deleted_flag`='Y' WHERE `stripe_customer_id`='%s'",mysqli_real_escape_string($this->db, $customers->id));
                    $resultupdatesql = mysqli_query($this->db, $updatesql);
                    if (!$resultupdatesql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesql");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    break;
                default:
                      stripe_log_info("default : $body");
            }
            http_response_code(200);
            exit();            
        }else{
            http_response_code(400);
            exit();
        }
    }  
    
    public function getConnectedAccountDetails() {
        $payload = @file_get_contents('php://input');


        $endpoint_secret = $this->wh_ca_keys;

        $sig_header = $_SERVER["HTTP_STRIPE_SIGNATURE"];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                            $payload, $sig_header, $endpoint_secret
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400); // PHP 5.4 or greater
            exit();
        } catch (\Stripe\Error\SignatureVerification $e) {
            // Invalid signature
            http_response_code(400); // PHP 5.4 or greater
            exit();
        }

        if (!empty($event) && !is_null($event)) {
            
            switch ($event->type) {
                case "account.updated":
                    $connected_account = $event->data->object;
                    $charges_enabled = ($connected_account->charges_enabled) ? 'Y' : 'N';
                    $payouts_enabled = ($connected_account->payouts_enabled) ? 'Y' : 'N';
                    $details_submitted = ($connected_account->details_submitted) ? 'Y' : 'N';
                    $account_state = ($connected_account->charges_enabled) ? 'Y' : 'A';
                    $payout_time_period = ($connected_account->payouts_enabled) ? $connected_account->settings->payouts->schedule->interval : '';
                    if($connected_account->business_type == "individual"){
                        if($connected_account->individual->relationship->account_opener){
                            $relationship_status = "Account Creator";
                        }else if($connected_account->individual->relationship->director){
                             $relationship_status = "Director";
                        }else if($connected_account->individual->relationship->owner){
                             $relationship_status = "Owner";
                        }else{
                             $relationship_status = "";
                        }  
                    }else{
                        $relationship_status = "";
                    }
                    
//                    if(count($connected_account->requirements->currently_due) > 0 || count($connected_account->requirements->eventually_due) > 0){
//                        $requirement_status = 'Y';
//                    }else{
//                        $requirement_status = 'N';
//                    }
                    $requirement_status = ($connected_account->details_submitted) ? 'N' : 'Y';
                    
                    if ($connected_account->business_type == "company") {
                        $updatesql=sprintf("UPDATE `stripe_account` SET `business_type`='%s', `company_name`='%s' , `disabled_reasons`='%s', `requirement_status`='%s', `payout_time_period` ='%s',
                            `charges_enabled`='%s',`payouts_enabled`='%s',`details_submitted`='%s',`external_accounts_url`='%s',`account_state`= '%s',`payout_time_period`= '%s' WHERE `account_id`='%s'"
                            ,mysqli_real_escape_string($this->db, 'C')
                            ,mysqli_real_escape_string($this->db, $connected_account->company->name)
                            ,mysqli_real_escape_string($this->db, $connected_account->disabled_reason)
                            ,mysqli_real_escape_string($this->db, $requirement_status)
                            ,mysqli_real_escape_string($this->db, $connected_account->settings->payouts->schedule->interval)
                            ,mysqli_real_escape_string($this->db, $charges_enabled)
                            ,mysqli_real_escape_string($this->db, $payouts_enabled)
                            ,mysqli_real_escape_string($this->db, $details_submitted)
                            ,mysqli_real_escape_string($this->db, $connected_account->external_accounts->url)
                            ,mysqli_real_escape_string($this->db, $account_state)
                            ,mysqli_real_escape_string($this->db, $payout_time_period)
                            ,mysqli_real_escape_string($this->db, $connected_account->id));
                    }else if($connected_account->business_type == "individual"){
                        $updatesql=sprintf("UPDATE `stripe_account` SET `business_type`='%s', `first_name`='%s', `last_name`='%s', `disabled_reasons`='%s', `relationship_status`='%s',`requirement_status`='%s',`payout_time_period` ='%s',
                            `charges_enabled`='%s',`payouts_enabled`='%s',`details_submitted`='%s',`external_accounts_url`='%s',`account_state`= '%s',`payout_time_period`= '%s' WHERE `account_id`='%s'"
                            ,mysqli_real_escape_string($this->db, 'I')
                            ,mysqli_real_escape_string($this->db, $connected_account->individual->first_name)
                            ,mysqli_real_escape_string($this->db, $connected_account->individual->last_name)
                            ,mysqli_real_escape_string($this->db, $connected_account->disabled_reason)
                            ,mysqli_real_escape_string($this->db, $relationship_status)
                            ,mysqli_real_escape_string($this->db, $requirement_status)
                            ,mysqli_real_escape_string($this->db, $connected_account->settings->payouts->schedule->interval)
                            ,mysqli_real_escape_string($this->db, $charges_enabled)
                            ,mysqli_real_escape_string($this->db, $payouts_enabled)
                            ,mysqli_real_escape_string($this->db, $details_submitted)
                            ,mysqli_real_escape_string($this->db, $connected_account->external_accounts->url)
                            ,mysqli_real_escape_string($this->db, $account_state)
                            ,mysqli_real_escape_string($this->db, $payout_time_period)
                            ,mysqli_real_escape_string($this->db, $connected_account->id));
                    }else{
                        $updatesql=sprintf("UPDATE `stripe_account` SET `disabled_reasons`='%s',`requirement_status`='%s',`charges_enabled`='%s',`payouts_enabled`='%s',`details_submitted`='%s',`external_accounts_url`='%s',`account_state`= '%s',`payout_time_period`= '%s' WHERE `account_id`='%s'"                            
                            ,mysqli_real_escape_string($this->db, $connected_account->disabled_reason)
                            ,mysqli_real_escape_string($this->db, $requirement_status)
                            ,mysqli_real_escape_string($this->db, $charges_enabled)
                            ,mysqli_real_escape_string($this->db, $payouts_enabled)
                            ,mysqli_real_escape_string($this->db, $details_submitted)
                            ,mysqli_real_escape_string($this->db, $connected_account->external_accounts->url)
                            ,mysqli_real_escape_string($this->db, $account_state)
                            ,mysqli_real_escape_string($this->db, $payout_time_period)
                            ,mysqli_real_escape_string($this->db, $connected_account->id));
                    }
                    $resultaccountsql = mysqli_query($this->db, $updatesql);
                    if (!$resultaccountsql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesql");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    break;
                case "account.external_account.created" || "account.external_account.updated":
                        $external_account_details = $event->data;
                        $acc_query = sprintf("SELECT `company_id`,`user_email` FROM `stripe_account` WHERE `account_id` = '%s'",mysqli_real_escape_string($this->db, $event->account));
                        $acc_result = mysqli_query($this->db, $acc_query);
                        if (!$acc_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$acc_query");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        } else {
                            $row = mysqli_fetch_assoc($acc_result);
                            $company_id = $row['company_id'];
                            $user_email = $row['user_email'];
                            $ex_acc_query = sprintf("SELECT `external_account_id` FROM `stripe_external_account` WHERE `company_id` = '%s' AND `external_account_id`='%s'",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event->data->object->id));
                            $ex_acc_result = mysqli_query($this->db, $ex_acc_query);
                            if (!$ex_acc_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$ex_acc_query");
                                stripe_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }else{
                                if (mysqli_num_rows($ex_acc_result) > 0) {
                                    $row = mysqli_fetch_assoc($ex_acc_result);                                    
                                    $external_account_id = $row['external_account_id'];
                                    if (!empty($external_account_id) && $external_account_id != $event->data->object->id) {
                                         $query1 = sprintf("INSERT INTO `stripe_external_account`(`company_id`, `user_email`, `external_account_id`, `account_type`, `account_id`, `country`, `currency`, `last4_digit`, `account_holder_name`, `account_holder_type`, `bank_name`, `routing_number`)
                                            VALUE ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                                                  mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $user_email),mysqli_real_escape_string($this->db, $event->data->object->id), mysqli_real_escape_string($this->db, $event->data->object->object), mysqli_real_escape_string($this->db, $event->data->object->account), mysqli_real_escape_string($this->db, $event->data->object->country), mysqli_real_escape_string($this->db, $event->data->object->currency), mysqli_real_escape_string($this->db, $event->data->object->last4)
                                                 ,mysqli_real_escape_string($this->db, $event->data->object->account_holder_name),mysqli_real_escape_string($this->db, $event->data->object->account_holder_type),mysqli_real_escape_string($this->db, $event->data->object->bank_name),mysqli_real_escape_string($this->db, $event->data->object->routing_number));
                                            $result1 = mysqli_query($this->db, $query1);
                                            
                                            if (!$result1) {
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                                                stripe_log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 500);
                                            } else {
                                                // success
                                            }
                                    } else {
                                        $updatelinksql=sprintf("UPDATE `stripe_external_account` SET `account_type` = '%s' , `country` = '%s', `currency` = '%s', `last4_digit` = '%s', `account_holder_name` = '%s', `account_holder_type` = '%s', `bank_name` = '%s', `routing_number` = '%s') WHERE `account_id`='%s' AND `external_account_id`='%s' AND `company_id`='%s' "  
                                                  ,mysqli_real_escape_string($this->db, $event->data->object->object),  mysqli_real_escape_string($this->db, $event->data->object->country), mysqli_real_escape_string($this->db, $event->data->object->currency), mysqli_real_escape_string($this->db, $event->data->object->last4)
                                                 ,mysqli_real_escape_string($this->db, $event->data->object->account_holder_name),mysqli_real_escape_string($this->db, $event->data->object->account_holder_type),mysqli_real_escape_string($this->db, $event->data->object->bank_name),mysqli_real_escape_string($this->db, $event->data->object->routing_number), mysqli_real_escape_string($this->db, $event->data->object->account),mysqli_real_escape_string($this->db, $event->data->object->id),mysqli_real_escape_string($this->db, $company_id));
                                        $resultupdatelinksql = mysqli_query($this->db, $updatelinksql);
                                        if (!$result1) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                                            stripe_log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        } else {
                                            // success
                                        }
                                    }
                            } else {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$acc_query");
                                stripe_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }
                    }
                    break;
                case "account.external_account.deleted":                        
                        $acc_query = sprintf("SELECT `external_account_id` FROM `stripe_external_account` WHERE `company_id` = '%s' AND `external_account_id`='%s'",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event->data->object->id));
                        $acc_result = mysqli_query($this->db, $acc_query);
                        if (!$acc_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$acc_query");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        } else {
                            if (mysqli_num_rows($acc_result) > 0) {
                                $updatelinksql=sprintf("UPDATE `stripe_external_account` SET `deleted_flag`='Y' WHERE `account_id`='%s' AND `external_account_id`='%s'"                                    
                                    , mysqli_real_escape_string($this->db, $event->data->object->account),mysqli_real_escape_string($this->db, $event->data->object->id));
                                $resultupdatelinksql = mysqli_query($this->db, $updatelinksql);
                            } else {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$acc_query");
                                stripe_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }
                        if (!$resultupdatelinksql) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatelinksql");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    break;
                default:
                    stripe_log_info("default : ".json_encode($event));
            }
            http_response_code(200);
            exit();            
        }else{
            http_response_code(400);
            exit();
        }
    }
    
    public function getPaymentIntentDetails() {
        $payload = @file_get_contents('php://input');
        $endpoint_secret =$this->wh_payment_intent_keys;
        $sig_header = $_SERVER["HTTP_STRIPE_SIGNATURE"];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                            $payload, $sig_header, $endpoint_secret
            );
          stripe_log_info($event);
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400); // PHP 5.4 or greater
            exit();
        } catch (\Stripe\Error\SignatureVerification $e) {
            // Invalid signature
            http_response_code(400); // PHP 5.4 or greater
            exit();
        }
        
        if (!empty($event) && !is_null($event)) {
            // Handle the event
            switch ($event->type) {
                case 'payment_intent.succeeded':
                    $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent
                    if($paymentIntent->metadata->p_type == 'misc'){
                        stripe_log_info("misc_payment_success");
                        $this->getMiscCheckoutDetails($paymentIntent, 'S');
                    }elseif($paymentIntent->metadata->p_type == 'event'){
                        stripe_log_info("event_payment_success");
                        $this->getEventCheckoutDetails($paymentIntent, 'S');
                    }elseif($paymentIntent->metadata->p_type == 'membership'){
                        stripe_log_info("Membership_payment_success");
                        $this->getMembershipCheckoutDetails($paymentIntent, 'S');
                    }elseif($paymentIntent->metadata->p_type == 'trial'){
                        stripe_log_info("trial_payment_success");
                        $this->getTrialCheckoutDetails($paymentIntent, 'S');
                    }elseif($paymentIntent->metadata->p_type == 'retail'){
                        stripe_log_info("retail_payment_success");
                        $this->getRetailCheckoutDetails($paymentIntent, 'S');
                    }
                    //handlePaymentIntentSucceeded($paymentIntent);
//                    stripe_log_info($paymentIntent);
                    break;
                case 'payment_intent.payment_failed':
                    $paymentIntent = $event->data->object; // contains a \Stripe\PaymentMethod
                    if($paymentIntent->metadata->p_type == 'misc'){
                        stripe_log_info("misc_payment_failure");
                        $this->getMiscCheckoutDetails($paymentIntent, 'F');
                    }elseif($paymentIntent->metadata->p_type == 'event'){
                        stripe_log_info("event_payment_failure");
                        $this->getEventCheckoutDetails($paymentIntent, 'F');
                    }elseif($paymentIntent->metadata->p_type == 'membership'){
                        stripe_log_info("Membership_payment_failure");
                        $this->getMembershipCheckoutDetails($paymentIntent, 'F');
                    }elseif($paymentIntent->metadata->p_type == 'trial'){
                        stripe_log_info("trial_payment_failure");
                        $this->getTrialCheckoutDetails($paymentIntent, 'F');
                    }elseif($paymentIntent->metadata->p_type == 'retail'){
                        stripe_log_info("retail_payment_failure");
                        $this->getRetailCheckoutDetails($paymentIntent, 'F');
                    }
                    //handlePaymentMethodAttached($paymentMethod);
//                    stripe_log_info($paymentIntent);
                    break;
                // ... handle other event types
                default:
                    // Unexpected event type
                    http_response_code(400);
                    exit();
            }
            http_response_code(200); // Success
            exit();
        }else{
            http_response_code(400); // PHP 5.4 or greater
            exit();
        }
    }
    
    private function getMiscCheckoutDetails($paymentIntent, $status){
        $checkout_state = $misc_order_id = $payment_id = $payment_intent_id = $company_id = $processing_fee_type = '';
        $fee = $payment_amount = 0;
        
        $payment_intent_id = $paymentIntent->id;
        $query = sprintf("SELECT ms.`company_id`, ms.`misc_order_id`, msp.`misc_payment_id`, msp.`payment_amount`, msp.`processing_fee`, s.`misc_charge_fee_type`,
                        ms.registration_from, ms.stripe_customer_id, msp.payment_intent_id, msp.payment_method_id, msp.stripe_card_name, msp.`checkout_status`
                    FROM `misc_order` ms 
                    LEFT JOIN `misc_payment` msp ON ms.`misc_order_id`=msp.`misc_order_id`
                    LEFT JOIN `studio_pos_settings` s ON ms.`company_id` = s.`company_id` 
                    WHERE msp.`payment_intent_id` = '%s' ORDER BY msp.`misc_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            stripe_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $misc_order_id = $row['misc_order_id'];
                $payment_id = $row['misc_payment_id'];
                $payment_intent_id = $row['payment_intent_id'];
                $fee = $row['processing_fee'];
                $processing_fee_type = $row['misc_charge_fee_type'];
                $payment_amount = $row['payment_amount'];
                $company_id = $row['company_id'];
                $checkout_state = $row['checkout_status'];
                
                if($checkout_state == 'released'){ // return success already updated
                    $out = array('status' => "Success", "msg" => "Checkout status already updated.");
                    stripe_log_info($this->json($out));
                    $this->response($this->json($out), 200);
                }else{
                    if($status=='S'){
                        $payment_status = "";
                        $checkout_state = "released";
                        if ($processing_fee_type == 1) {
                            $paid_amount = $payment_amount - $fee;
                        } elseif ($processing_fee_type == 2) {
                            $paid_amount = $payment_amount;
                        }

                        $update_sales_in_reg = sprintf("UPDATE `misc_order` SET `paid_amount`=`paid_amount`+$paid_amount WHERE `misc_order_id`='%s'", mysqli_real_escape_string($this->db, $misc_order_id));
                        $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);
                        if(!$result_sales_in_reg){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }else{
                        $payment_status = ", `payment_status`='F'";
                        $checkout_state = "failed";
                    }

                    $update_query = sprintf("UPDATE `misc_payment` SET `checkout_status`='%s' $payment_status WHERE `misc_order_id`='%s' and `misc_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $misc_order_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        stripe_log_info($this->json($error));
                        if($status=='S'){
                            $this->responseWithMail($this->json($error), 200);
                            $this->addMiscDimensions($company_id,'',$paid_amount);
                        }else{
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Payment details doesn't exist.");
                stripe_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
    public function addMiscDimensions($company_id, $period, $amount) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if (!empty($period)) {
            $curr_dt = date("Y-m", strtotime($period));
        } else {
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));

        $sql2 = sprintf("SELECT * FROM `misc_dimensions` WHERE `company_id`='%s' AND `period`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            stripe_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows1 = mysqli_num_rows($result2);
            if ($num_of_rows1 == 0) {
                $insertquery1 = sprintf("INSERT INTO `misc_dimensions`(`company_id`, `period`,`sales_amount`) VALUES('%s','%s','0')", 
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $amount));
                $res2 = mysqli_query($this->db, $insertquery1);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    stripe_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
            
            $updatequery1 = sprintf("UPDATE `misc_dimensions` SET `sales_amount`=sales_amount+'%s' where company_id='%s' AND period = '%s'", 
                        mysqli_real_escape_string($this->db, $amount),mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt));
            $res2 = mysqli_query($this->db, $updatequery1);
            if (!$res2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$updatequery1");
                stripe_log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
            
        }
        date_default_timezone_set($curr_time_zone);
    }
//    Trial details updatation
    private function getTrialCheckoutDetails($paymentIntent, $status){
        $trial_reg_id = $payment_id = $company_id = $trial_id = $checkout_state = '';
        $fee = $processing_fee_type = $payment_amount = 0;
        
        $payment_intent_id = $paymentIntent->id;
        $query = sprintf("SELECT tr.`company_id`, tr.`trial_id`, tr.`trial_reg_id`, tp.`trial_payment_id`, tp.`payment_amount`, tp.`processing_fee`, tr.`processing_fee_type`, tp.`checkout_status`
                    FROM `trial_registration` tr 
                    LEFT JOIN `trial_payment` tp ON tr.`trial_reg_id`=tp.`trial_reg_id`
                    WHERE tp.`payment_intent_id` = '%s' ORDER BY tp.`trial_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            stripe_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $trial_reg_id = $row['trial_reg_id'];
                $payment_id = $row['trial_payment_id'];
                $fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_amount = $row['payment_amount'];
                $company_id = $row['company_id'];
                $trial_id = $row['trial_id'];
                $checkout_state = $row['checkout_status'];
                
                if($checkout_state == 'released'){ // return success already updated
                    $out = array('status' => "Success", "msg" => "Checkout status already updated.");
                    stripe_log_info($this->json($out));
                    $this->response($this->json($out), 200);
                }else{
                    if($status=='S'){
                        $payment_status = "";
                        $checkout_state = "released";
                        $paid_amount = 0;
                        if($processing_fee_type==1){
                            $paid_amount = $payment_amount-$fee;
                        }elseif($processing_fee_type==2){
                            $paid_amount = $payment_amount;
                        }
                        $update_sales_in_reg = sprintf("UPDATE `trial_registration` SET `paid_amount`=`paid_amount`+$paid_amount WHERE `trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_id));
                        $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);

                        if(!$result_sales_in_reg){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }

                        $update_count = sprintf("UPDATE `trial` SET `net_sales`=`net_sales`+$paid_amount WHERE `company_id`='%s' AND `trial_id`='%s'",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
                        $result_count = mysqli_query($this->db, $update_count);

                        if(!$result_count){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }else{
                        $payment_status = ", `payment_status`='F'";
                        $checkout_state = "failed";
                    }
                    $update_query = sprintf("UPDATE `trial_payment` SET `checkout_status`='%s' $payment_status WHERE `trial_reg_id`='%s' and `trial_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        stripe_log_info($this->json($error));
                        if($status=='S'){
                            $this->responseWithMail($this->json($error), 200);
                            $this->updateTrialDimensionsNetSales($company_id,$trial_id,'');
                        }else{
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Payment details doesn't exist.");
                stripe_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    public function updateTrialDimensionsNetSales($company_id, $trial_id, $date) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $mp_last_upd_date=" DATE(CONVERT_TZ(tp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $last_upd_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";
        if(empty($date)){
            $curr_dt = "($currdate_add)";
        }else{
            $curr_dt = "'$date'";
        }
        $date_inc1= "DATE_FORMAT($curr_dt,'%Y-%m')";
        $date_inc2= "DATE_FORMAT($mp_last_upd_date,'%Y-%m')";
        $date_inc3= "DATE_FORMAT($last_upd_date,'%Y-%m')";
        
        $sql = sprintf("SELECT company_id, trial_id,  mp_ud, sum(amount) amount, payment_date FROM (

                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND (tp.payment_status='S' AND tp.checkout_status='released' || tp.payment_status IN ('MF', 'FR') || tp.payment_status='M' AND tp.`credit_method`!='MC')
                  AND %s = %s 
                  
                  UNION
                  
                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, concat('-',if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount)) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND  (tp.`payment_status`='R' 
                  AND IF(tr.registration_from='S', tp.payment_intent_id NOT IN (SELECT payment_intent_id FROM trial_payment WHERE payment_status='FR' AND %s = %s),
                  tp.checkout_id NOT IN (SELECT checkout_id FROM trial_payment WHERE payment_status='FR' AND %s = %s)) || tp.`payment_status`='MR')
                  AND %s = %s
                  )t1
                  GROUP BY t1.`trial_id`"

                 , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc2, $date_inc1, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc3, $date_inc1, $date_inc3, $date_inc1, $date_inc2, $date_inc1);
        
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            stripe_log_info($this->json($error_log));
            $reg_id = 1;
        } else {
            $trial = $trial_id_arr = [];
            $number_rows = mysqli_num_rows($result);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $temp = $temp2 = [];
                    $temp['trial_id'] = $temp2['trial_id'] = $trial_id = $row['trial_id'];
                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
                    $temp2['amount'] = $amount = $row['amount'];
                    $temp2['mp_ud'] = $row['mp_ud'];

                    if (!empty($trial_id) && !in_array($trial_id, $trial_id_arr, true)) {
                        $trial_id_arr[] = $trial_id;
                        $trial[] = $temp;
                    }
                    $option[] = $temp2;
                }

                for ($i = 0; $i < count($option); $i++) {
                    $date_init = 0;
                   
                    $date_str = "%Y-%m";
                    $curr_dt = date("Y-m-d");
                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
//                     
                    $sql1 = sprintf("SELECT * FROM `trial_dimensions` WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')", mysqli_real_escape_string($this->db, $company_id), $option[$i]['trial_id'], $every_month_dt, $date_str);
                    $res1 = mysqli_query($this->db, $sql1);
                    if (!$res1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        stripe_log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
//                           

                        $num_of_rows = mysqli_num_rows($res1);
                        if ($num_of_rows == 1) {

                            if($i==0){
                                    $sales_value = "'%s'";
                                }else{
                                    $sales_value = "`sales_amount`+'%s'";
                                }
                                $sql3 = sprintf("UPDATE `trial_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `trial_id`='%s' ",mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str,mysqli_real_escape_string($this->db, $trial_id));
                                $res3 = mysqli_query($this->db, $sql3);
                                if (!$res3) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                                    stripe_log_info($this->json($error_log));
                                    $reg_id = 1;
                                }
                           
                        }
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "No records.","query" => "$sql");
                $this->response($this->json($error), 204);
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for updateTrialDimensionsNetSales(), check log";
            $this->sendEmailForAdmins($msg);
        }
    }
    private function sendEmailForAdmins($msg){
        $cmp_name = "MyStudio WepayIpn";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "Wepay Ipn Handling Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        stripe_log_info($this->json($error_log));
        return $response;
    }
    
    // Retail details Updation
    private function getRetailCheckoutDetails($paymentIntent, $status){
        $checkout_state = $retail_order_id = $payment_id = $company_id = '';
        $retail_total_processing_fee = $payment_amount = 0;
        $retail_arr=[];
        
        $payment_intent_id = $paymentIntent->id;
        $query = sprintf("SELECT rto.`company_id`,rod.`retail_order_detail_id`,rod.`retail_payment_amount`, rod.`retail_product_id`, rto.`retail_order_id`,rod.`retail_parent_id`,rod.`rem_due`, rp.`retail_payment_id`, rp.`checkout_status`, rp.`payment_amount`, rp.`processing_fee`, rto.`processing_fee_type`,rto.`retail_discount_id`,rod.`retail_tax_percentage`,rod.`retail_discount`
                    FROM `retail_orders` rto 
                    LEFT JOIN `retail_payment` rp ON rto.`retail_order_id`=rp.`retail_order_id`
                    LEFT JOIN `retail_order_details` rod ON rto.`retail_order_id`=rod.`retail_order_id`
                    WHERE rp.`payment_intent_id` = '%s' AND rp.`payment_status` IN ('S', 'PR') ORDER BY rp.`retail_payment_id`", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            stripe_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $k=0;
                while($row = mysqli_fetch_assoc($result)){
                    $retail_order_id = $row['retail_order_id'];
                    $payment_id = $row['retail_payment_id'];
                    $retail_total_processing_fee = $fee = $row['processing_fee'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $payment_amount = $row['payment_amount'];
                    $company_id = $row['company_id'];
                    $checkout_state = $row['checkout_status'];
                    $retail_arr[$k]['retail_order_detail_id'] = $row['retail_order_detail_id'];
                    $retail_arr[$k]['retail_product_id'] = $row['retail_product_id'];
                    $retail_arr[$k]['retail_parent_id'] = $row['retail_parent_id'];
                    $retail_arr[$k]['retail_price'] = $row['retail_payment_amount'];
                    $retail_arr[$k]['retail_tax_percentage'] = $row['retail_tax_percentage'];
                    $retail_arr[$k]['retail_discount'] = $row['retail_discount'];
                    $retail_arr[$k]['rem_due'] = $row['rem_due'];
                    $k++;
                }
                if($checkout_state == 'released'){ // return success already updated
                    $out = array('status' => "Success", "msg" => "Checkout status already updated.");
                    stripe_log_info($this->json($out));
                    $this->response($this->json($out), 200);
                }else {
                    if($status=='S'){
                        $payment_status = "";
                        $checkout_state = "released";
                        $update_amount = 0;
                        $paid_amount = $payment_amount;
                        if($processing_fee_type==1){
                            $total_amount = $update_amount = $payment_amount;
                        }elseif($processing_fee_type==2){
                            $total_amount = $update_amount = $payment_amount - $fee;
                        }
                        $update_sales_in_reg = sprintf("UPDATE `retail_orders` SET `retail_order_paid_amount`=`retail_order_paid_amount`+$paid_amount WHERE `retail_order_id`='%s'", mysqli_real_escape_string($this->db, $retail_order_id));
                        $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);

                        if(!$result_sales_in_reg){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                        for($i=0;$i<count($retail_arr);$i++){
                            $retail_order_detail_id = $retail_arr[$i]['retail_order_detail_id'];
                            $retail_parent_id=$retail_arr[$i]['retail_parent_id'];
                            $retail_product_id=$retail_arr[$i]['retail_product_id'];
                            $rem_due = $retail_arr[$i]['rem_due'];
                            $str = '';

                            if($rem_due>0 && $update_amount>0){
                                $due = $sales_due = 0;
                                if($rem_due>$update_amount){
                                    $sales_due = $update_amount;
                                    $due = $rem_due - $update_amount;
                                    $update_amount = 0;
                                    if($due<.5){
                                        $sts = 'S';
                                    }else{
                                        $sts = 'P';
                                    }
                                }else{
                                    $sales_due = $rem_due;
                                    $due = 0;
                                    $sts = 'S';
                                    $update_amount -= $rem_due;
                                }
                                $product_processing_fee = $retail_total_processing_fee * ($retail_arr[$i]['rem_due']/$total_amount);
                                $final_net_update = $sales_due;
                                if($processing_fee_type==1){
                                    $final_net_update = $sales_due-$product_processing_fee;
                                }
                                if (empty($retail_parent_id) || is_null($retail_parent_id)) {
                                    $str = "IN (%s)";
                                    $this->addretailDimensions($company_id, $retail_arr[$i]['retail_product_id'], '', '');
                                    $this->updateRetailDimensionsNetSales($company_id, $retail_arr[$i]['retail_product_id'], '', '', $final_net_update);
                                } else {
                                    $str = "IN (%s,$retail_parent_id)";
                                    $this->addretailDimensions($company_id, $retail_arr[$i]['retail_parent_id'], $retail_arr[$i]['retail_product_id'], '');
                                    $this->updateRetailDimensionsNetSales($company_id, $retail_arr[$i]['retail_parent_id'], $retail_arr[$i]['retail_product_id'], '', $final_net_update);
                                }

                                $update_order_details = sprintf("UPDATE `retail_order_details` SET `rem_due`='%s', `retail_payment_status`='$sts' WHERE `company_id`='%s' AND `retail_order_id`='%s' AND `retail_order_detail_id`='%s'", mysqli_real_escape_string($this->db, $due),
                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $retail_order_detail_id));
                                $result_order_details = mysqli_query($this->db, $update_order_details);
                                if(!$result_order_details){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_order_details");
                                    stripe_log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }

                                $update_count = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`+$final_net_update WHERE `company_id`='%s' AND `retail_product_id` $str",
                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
                                $result_count = mysqli_query($this->db, $update_count);
                                if(!$result_count){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                                    stripe_log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }else{
                        $payment_status = ", `payment_status`='F'";
                        $checkout_state = "failed";
                    }
                    $update_query = sprintf("UPDATE `retail_payment` SET `checkout_status`='%s', `actual_paid_date`=CURDATE() $payment_status WHERE `retail_order_id`='%s' and `retail_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                                mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        stripe_log_info($this->json($error));
                        if($status=='S'){
                            $this->responseWithMail($this->json($error), 200);
                        }else{
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Payment details doesn't exist.");
                stripe_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
     //retail dashboard
    public function addretailDimensions($company_id, $product_id, $child_id, $period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
        if (!empty($child_id)) {
            $sql1 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND  `child_id`='%s' AND child_id !=0  AND `period`='%s' AND `deleted_flg`!='D'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $insertquery = sprintf("INSERT INTO `retail_dimensions`(`company_id`, `period`, `product_id`, `child_id`) VALUES('%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id));
                    $res = mysqli_query($this->db, $insertquery);
                    if (!$res) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        if (!empty($product_id)) {
            $sql2 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s' AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result2 = mysqli_query($this->db, $sql2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows1 = mysqli_num_rows($result2);
                if ($num_of_rows1 == 0) {
                    $insertquery1 = sprintf("INSERT INTO `retail_dimensions`(`company_id`, `period`, `product_id`,  `child_id`) VALUES('%s', '%s','%s',0)",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $product_id));
                    $res2 = mysqli_query($this->db, $insertquery1);
                    if (!$res2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    public function updateRetailDimensionsNetSales($company_id, $product_id, $child_id, $date, $amount) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        if (!empty($date)) {
            $every_month_dt = date("Y-m", strtotime($date));
        } else {
            $every_month_dt = date("Y-m");
        }
        if (!empty($child_id)) {
            $sql1 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`='%s' AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), $every_month_dt);
            $res1 = mysqli_query($this->db, $sql1);
            if (!$res1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($res1);
                if ($num_of_rows == 1) {
                    $sql2 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`='%s' AND `period`='%s'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), $every_month_dt);
                    $res2 = mysqli_query($this->db, $sql2);
                    if (!$res2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $sql3 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
                        $res3 = mysqli_query($this->db, $sql3);
                        if (!$res3) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }
        } else {
            $sql2 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
            $res2 = mysqli_query($this->db, $sql2);
            if (!$res2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($res2);
                if ($num_of_rows == 1) {
                    $sql3 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `period`='%s' AND `child_id`='0'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
                    $res3 = mysqli_query($this->db, $sql3);
                    if (!$res3) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    // Event details update
    private function getEventCheckoutDetails($paymentIntent, $status){
        $checkout_state = $event_reg_id = $event_id = $payment_id = $processing_fee_type = $payment_type = '';
        $payment_amount = 0;
        
        $payment_intent_id = $paymentIntent->id;
        $query = sprintf("SELECT er.company_id, er.`event_reg_id`, ep.`event_payment_id`, e.`event_title`, ep.`payment_amount`, ep.`processing_fee`, er.`processing_fee_type`, er.`event_id`, er.`payment_type`, ep.`checkout_status`
                FROM `event_registration` er 
                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` 
                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
                WHERE ep.`payment_intent_id` = '%s' ORDER BY ep.`event_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            stripe_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $event_reg_id = $row['event_reg_id'];
                $event_id = $row['event_id'];
                $payment_id = $row['event_payment_id'];
                $payment_amount = $row['payment_amount'];
                $processing_fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_type = $row['payment_type'];
                $payment_type = $row['payment_type'];
                $checkout_state = $row['checkout_status'];
                
                if($checkout_state == 'released'){ // return success already updated
                    $out = array('status' => "Success", "msg" => "Checkout status already updated.");
                    stripe_log_info($this->json($out));
                    $this->response($this->json($out), 200);
                }else{
                    if($status=='S'){
                        $temp_payment_amount = $payment_amount;
                        $onetime_netsales_for_parent_event = 0;

                        $update_event_reg_query = sprintf("UPDATE `event_registration` SET `paid_amount`=`paid_amount`+$payment_amount WHERE `event_reg_id`='%s'",$event_reg_id);
                        $result_update_event_reg_query = mysqli_query($this->db, $update_event_reg_query);
                        if(!$result_update_event_reg_query){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_reg_query", "ipn" => "1");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }

                        if($payment_type=='CO'){
                            $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_type`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
                                       LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
                                       AND `payment_status` IN ('CC') ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                            $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                            if(!$get_event_details_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query", "ipn" => "1");
                                stripe_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }else{
                                $num_rows2 = mysqli_num_rows($get_event_details_result);
                                if($num_rows2>0){
                                    $rem_due = 0;
                                    while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                                        if($temp_payment_amount>0){
                                            $event_reg_details_id = $row2['event_reg_details_id'];
                                            $child_id = $row2['event_id'];
                                            $event_parent_id = $row2['parent_id'];
                                            $child_payment = $row2['payment_amount'];
                                            $rem_due = $row2['balance_due'];
                                            $event_type = $row2['event_type'];
                                            $payment_status = $row2['payment_status'];
                                            $child_net_sales = 0;
                                            if($onetime_netsales_for_parent_event==0){
                                                if($processing_fee_type==2){
                                                    $net_sales = $payment_amount;
                                                }elseif($processing_fee_type==1){
                                                    $net_sales = $payment_amount-$processing_fee;
                                                }
                                                $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
                                                $result_update_event_query = mysqli_query($this->db, $update_event_query);
                                                if(!$result_update_event_query){
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query", "ipn" => "1");
                                                    stripe_log_info($this->json($error_log));
                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                    $this->response($this->json($error), 500);
                                                }
                                                $onetime_netsales_for_parent_event=1;
                                            }
                                            if($temp_payment_amount>=$child_payment){
                                                $temp_payment_amount = $temp_payment_amount-$child_payment;
                                                $child_net_sales = $child_payment;
                                                $child_payment = 0;
                                            }else{
                                                $child_payment = $child_payment - $temp_payment_amount;
                                                $child_net_sales = $temp_payment_amount;
                                                $temp_payment_amount = 0;
                                            }
                                            $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$child_payment WHERE `event_reg_details_id`='%s'", 
                                                        mysqli_real_escape_string($this->db, $event_reg_details_id));
                                            $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
                                            if(!$result_update_reg_query){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query", "ipn" => "1");
                                                stripe_log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 500);
                                            }
                                            if($child_id!=$event_parent_id){
                                                $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
                                                $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
                                                if(!$result_update_child_query){
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales", "ipn" => "1");
                                                    stripe_log_info($this->json($error_log));
                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                    $this->response($this->json($error), 500);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }else{
                            $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_type`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
                                       LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
                                       AND `payment_status` = 'RP' ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                            $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                            if(!$get_event_details_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query", "ipn" => "1");
                                stripe_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }else{
                                $num_rows2 = mysqli_num_rows($get_event_details_result);
                                if($num_rows2>0){
                                    $rem_due = 0;
                                    while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                                        if($temp_payment_amount>0){
                                            $event_reg_details_id = $row2['event_reg_details_id'];
                                            $child_id = $row2['event_id'];
                                            $event_parent_id = $row2['parent_id'];
                                            $rem_due = $row2['balance_due'];
                                            $event_type = $row2['event_type'];
                                            $payment_status = $row2['payment_status'];
                                            $child_net_sales = 0;
                                            if($onetime_netsales_for_parent_event==0){
                                                if($processing_fee_type==2){
                                                    $net_sales = $payment_amount;
                                                }elseif($processing_fee_type==1){
                                                    $net_sales = $payment_amount-$processing_fee;
                                                }
                                                $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
                                                $result_update_event_query = mysqli_query($this->db, $update_event_query);
                                                if(!$result_update_event_query){
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query", "ipn" => "1");
                                                    stripe_log_info($this->json($error_log));
                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                    $this->response($this->json($error), 500);
                                                }
                                                $onetime_netsales_for_parent_event=1;
                                            }
                                            if($temp_payment_amount>=$rem_due){
                                                $temp_payment_amount = $temp_payment_amount-$rem_due;
                                                $child_net_sales = $rem_due;
                                                $rem_due = 0;
                                                $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due, `payment_status`='RC' WHERE `event_reg_details_id`='%s'", 
                                                        mysqli_real_escape_string($this->db, $event_reg_details_id));
                                            }else{
                                                $rem_due = $rem_due - $temp_payment_amount;
                                                $child_net_sales = $temp_payment_amount;
                                                $temp_payment_amount = 0;
                                                $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due WHERE `event_reg_details_id`='%s'", 
                                                        mysqli_real_escape_string($this->db, $event_reg_details_id));
                                            }
                                            $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
                                            if(!$result_update_reg_query){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query", "ipn" => "1");
                                                stripe_log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 500);
                                            }else{
                                                if($child_id!=$event_parent_id){
                                                    $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
                                                    $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
                                                    if(!$result_update_child_query){
                                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales", "ipn" => "1");
                                                        stripe_log_info($this->json($error_log));
                                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                        $this->response($this->json($error), 500);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $payment_status = "";
                        $checkout_state = "released";
                    }else{
                        $payment_status = ", `payment_status`='F'";
                        $checkout_state = "failed";
                    }
                    $update_query = sprintf("UPDATE `event_payment` SET `checkout_status`='%s' $payment_status WHERE `event_reg_id`='%s' and `event_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query", "ipn" => "1");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $out = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        stripe_log_info($this->json($error));
                        if($status=='S'){
                            $this->responseWithMail($this->json($error), 200);
                            $this->sendFailedOrderReceiptForEventPayment($event_reg_id,$checkout_state,0);
                        }else{
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Payment details doesn't exist.");
                stripe_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    protected function sendFailedOrderReceiptForEventPayment($event_reg_id, $checkout_state, $return_value){
       
        $waiver_policies = '';
        $query = sprintf("SELECT er.`company_id`, er.`event_id`, `event_title`, ep.`cc_name`, `waiver_policies`, concat(er.event_registration_column_2, ',', er.event_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `payment_type`, 
            er.`payment_amount` total_due, er.`processing_fee_type`, er.`processing_fee` total_processing_fee, `paid_amount`, ep.`payment_amount`, ep.`processing_fee`, ep.`refunded_amount`, `schedule_date`, `schedule_status`, DATE(ep.`created_dt`) created_dt
            FROM `event_registration` er LEFT JOIN `event_payment` ep ON er.`event_reg_id` = ep.`event_reg_id` AND ep.`schedule_status` IN ('N', 'S', 'PR', 'M')
            LEFT JOIN `event` e ON e.`event_id` = er.`event_id` AND e.`company_id` =  er.`company_id`
            LEFT JOIN `company` c ON c.`company_id` =  er.`company_id`
            WHERE er.`event_reg_id` = '%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            stripe_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
                $event_name = $participant_name = $buyer_name = $buyer_email = $cc_email_list = '';
                $inital_stage = $initial_down_pay = 0;
                $current_plan_details = $event_details = $bill_due_details = $payment_details = [];
                $total_due = $paid_due = $rem_due = 0;
                while($row = mysqli_fetch_assoc($result)){
                    $paid_amt = 0;
                    $event_id = $row['event_id'];
                    $event_name = $row['event_title'];
                    $participant_name = $row['participant_name'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $temp=[];
                    
                    if($row['processing_fee_type']==2){
                        $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                    }else{
                        $temp['amount'] = $row['payment_amount'];
                    }
                    $temp['date'] = $row['schedule_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['edit_status'] = 'N';
                    $temp['schedule_status'] = $row['schedule_status'];
                    $temp['processing_fee'] = $row['processing_fee'];
                    $temp['credit_card_name'] = $row['cc_name'];
                    if($row['schedule_status']=='N'){
                        $temp['status'] = 'N';
                    }elseif($row['schedule_status']=='S'){
                        $temp['status'] = 'S';
                        $paid_amt = $temp['amount'];
                        $inital_stage++;
                    }elseif($row['schedule_status']=='PR' || $row['schedule_status']=='M'){
                        $paid_amt = $temp['amount']-$temp['refunded_amount'];
                    }
                    $total_due += $temp['amount'];
                    $current_plan_details['total_due'] = $total_due;
                    $paid_due += $paid_amt;
                    $current_plan_details['paid_due'] = $paid_due;
                    
                    $current_plan_details['success_count'] = $inital_stage;
                    $rem_due = $current_plan_details['rem_due'] = number_format((float)($total_due - $paid_due), 2, '.', '');
                    if ($row['schedule_status'] == 'N') {
                        $bill_due_details[] = $temp;
                    } elseif ($row['schedule_status'] == 'S') {
                        $payment_details[] = $temp;
                        if ($initial_down_pay > 0) {
                            $bill_due_details[] = $temp;
                        }
                        $initial_down_pay++;
                    } elseif ($row['schedule_status'] == 'M') {
                        $payment_details[] = $temp;
                    }
                }
                
                $studio_name = $studio_mail = $message = '';
                $query2 = sprintf("SELECT c.`company_name`, c.`email_id`, c.`referral_email_list`, e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, `event_end_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` 
                    FROM `event` e LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` 
                    LEFT JOIN `company` c ON e.`company_id` =  c.`company_id`
                    WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' AND `payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", 
                        mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                $result2 = mysqli_query($this->db, $query2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    stripe_log_info($this->json($error_log));
                    if($return_value==1){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    return;
                }else{
                    $temp_quantity = $temp_char_count = 0;
                    while($row2 = mysqli_fetch_assoc($result2)){
                        $temp2 = [];
                        $temp2['title'] = $row2['event_title'];
                        if($temp2['title']>$temp_char_count){
                            $temp_char_count = strlen($temp2['event_title']);
                        }
                        
                        if (!empty(trim($row2['event_begin_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_begin_dt']) {
                                $start = " - ";
                            } else {
                                $start = $row2['event_begin_dt'];

                                $split_start = explode(" ", $start);
                                $start_date_value = $split_start[0];
                                $start_time_value = $split_start[1];
                                if ("00:00:00" == $start_time_value) {
                                    $start = date('M d, Y', strtotime($start));
                                } else {
                                    $start = date('M d, Y @ h:i A', strtotime($start));
                                }
                            }
                        }
                        
                        if (!empty(trim($row2['event_end_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_end_dt']) {
                                $end = " - ";
                            } else {
                                $end = $row2['event_end_dt'];

                                $split_end = explode(" ", $end);
                                $end_date_value = $split_end[0];
                                $end_time_value = $split_end[1];
                                if ("00:00:00" == $end_time_value) {
                                    $end = date('M d, Y', strtotime($end));
                                } else {
                                    $end = date('M d, Y @ h:i A', strtotime($end));
                                }
                            }
                        }

                        $temp2['start'] = $start;
                        $temp2['end'] = $end;
                        $temp2['event_cost'] = $row2['event_cost'];
                        $temp2['quantity'] = $row2['quantity'];
                        $temp2['event_disc'] = $row2['discount'];
                        $temp2['discounted_price'] = $row2['event_cost'] - $row2['discount'];
                        $temp_quantity += $row2['quantity'];
                        $temp2['payment_amount'] = $row2['payment_amount'];
                        $temp2['paid_due'] = $temp2['payment_amount'] - $row2['balance_due'];
                        $temp2['rem_due'] = $row2['balance_due'];
                        $temp2['state'] = 'N';
                        $event_details[] = $temp2;
                        $studio_name = $row2['company_name'];
                        $studio_mail = $row2['email_id'];
                        $cc_email_list = $row2['referral_email_list'];
                    }
                    $current_plan_details['total_event'] = $temp_quantity;
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                stripe_log_info($this->json($error_log));
                if($return_value==1){
                    $error = array('status' => "Failed", "msg" => "No records found.");
                    $this->response($this->json($error), 200);
                }
                return;
            }
        }
        
        $failed_str = 'failed';
        if($checkout_state=='charged back'){
            $failed_str = 'charged back';
        }
        
        $subject = $event_name." - Payment $failed_str";
                
        $message .= "<b>The following payment for the Buyer $buyer_name $failed_str.</b><br><br>";
        $message .= "<b>Event Details</b><br><br>";
        for($i=0;$i<count($event_details);$i++){
            $message .= "Event: ".$event_details[$i]['title']."<br>Starts: ".$event_details[$i]['start']."<br>Ends: ".$event_details[$i]['end']."<br>Participant: ".$participant_name."<br><br>";
        }

        if($total_due>0){
            $message .= "<b>Billing Details</b><br><br>";
            if(count($bill_due_details)>0){
                $message .= "Total due: $".$total_due."<br>";
                $message .= "Balance due: $".$rem_due."<br>";                
                for($j=0;$j<count($bill_due_details);$j++){
                    $x= $j+1;
                    $amount = $bill_due_details[$j]['amount'];
                    $refunded_amount = $bill_due_details[$j]['refunded_amount'];
                    $final_amt = $amount-$refunded_amount;
                    $schedule_status = $bill_due_details[$j]['schedule_status'];
                    $date = date("M dS, Y", strtotime($bill_due_details[$j]['date']));
                    if($schedule_status=='N'){
                        $paid_str = "due";
                    }
                    $message .= "Bill ".$x.": $".$final_amt." ".$paid_str." ".$date."  <br>";
                }
                
                if(count($payment_details)>0){
                    $message.="<br><br><b>Payemt Details</b><br><br>";
                    $down_pay = $payment_details[0]['amount']-$payment_details[0]['refunded_amount']." paid on ".date("M dS, Y", strtotime($payment_details[0]['date']))."(".$payment_details[0]['credit_card_name'].")";
                    $message .= "Down Payment: $".$down_pay."<br>";
                    for($k=1;$k<count($payment_details);$k++){
                        $x= $k;
                        $amount = $payment_details[$k]['amount'];
                        $refunded_amount = $payment_details[$k]['refunded_amount'];
                        $final_amt = $amount-$refunded_amount;
                        $schedule_status = $payment_details[$k]['schedule_status'];
                        $date = date("M dS, Y", strtotime($payment_details[$k]['date']));
                        $paid_str = "paid on";
                        if($schedule_status=='S'){
                            $pay_type= "(".$payment_details[$k]['credit_card_name'].")";
                        }else{
                            $pay_type="(manual credit applied)";
                        }
                        $message .= "Payment ".$x.": $".$final_amt." ".$paid_str." ".$date." ".$pay_type." <br>";
                    }
                }
            }else{
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $schedule_status = $payment_details[0]['schedule_status'];
                $final_amount = $amount-$refunded_amount;
                $message .= "Total Due: $".$final_amount."<br>Amount Paid: $".$final_amount."(".$payment_details[0]['credit_card_name'].")<br>";
            }
            $message .= "<br><br>";
        }

        $sendEmail_status = $this->sendEmailForEvent('', $studio_mail, $subject, $message, '', '', $cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $studio_mail."   ".$sendEmail_status['mail_status']);
            stripe_log_info($this->json($error_log));
            if($return_value==1){
                $log = array('status' => "Success", "msg" => "Event order receipt sent successfully.");
                $this->response($this->json($log), 200);
            }
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $studio_mail);
            stripe_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Event order receipt sent failed.");
                $this->response($this->json($error), 200);
            }
        }
    }
    protected function sendEmailForEvent($from, $to, $subject, $message, $cmp_name, $reply_to, $cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        stripe_log_info($this->json($error_log));
        return $response;
    }
    
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone='';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_tz);                       
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    // Membership details update
    private function getMembershipCheckoutDetails($paymentIntent, $status){
        $checkout_state = $membership_reg_id = $company_id = $membership_id = $payment_id = $processing_fee_type = $payment_type = $payment_intent_id = $membership_option_id = $membership_title = $membership_category_title = '';
        $payment_amount = $fee = 0;
        $send_email = 0;
        
        $payment_intent_id = $paymentIntent->id;
        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_registration_id`, mr.`membership_category_title`, mr.`membership_title`, mp.`membership_payment_id`, mp.`checkout_status`, mp.`payment_amount`, mp.`processing_fee`, mr.`processing_fee_type`, mp.`checkout_status`
                    FROM `membership_registration` mr 
                    LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id`=mp.`membership_registration_id` 
                    WHERE mp.`payment_intent_id` = '%s' ORDER BY mp.`membership_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            stripe_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $membership_reg_id = $row['membership_registration_id'];
                $payment_id = $row['membership_payment_id'];
                $fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_amount = $row['payment_amount'];
                $company_id = $row['company_id'];
                $membership_id = $row['membership_id'];
                $membership_option_id = $row['membership_option_id'];
                $membership_title = $desc = $row['membership_title'];
                $membership_category_title=$row['membership_category_title']; 
                $checkout_state = $row['checkout_status'];
                
                if($checkout_state == 'released'){ // return success already updated
                    $out = array('status' => "Success", "msg" => "Checkout status already updated.");
                    stripe_log_info($this->json($out));
                    $this->response($this->json($out), 200);
                }else{
                    if($status=='S'){
                        $paid_amount = 0;
                        if($processing_fee_type==1){
                            $paid_amount = $payment_amount-$fee;
                        }elseif($processing_fee_type==2){
                            $paid_amount = $payment_amount;
                        }
                        $update_sales_in_reg = sprintf("UPDATE `membership_registration` SET `paid_amount`=`paid_amount`+$paid_amount WHERE `membership_registration_id`='%s'", mysqli_real_escape_string($this->db, $membership_reg_id));
                        $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);
                        
                        if(!$result_sales_in_reg){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                        
                        $update_count = sprintf("UPDATE `membership` SET `category_net_sales`=`category_net_sales`+$paid_amount WHERE `company_id`='%s' AND `membership_id`='%s'",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
                        $result_count = mysqli_query($this->db, $update_count);
                        
                        if(!$result_count){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                        
                        $update_count2 = sprintf("UPDATE `membership_options` SET `options_net_sales` = `options_net_sales`+$paid_amount WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
                        $result_count2 = mysqli_query($this->db, $update_count2);
                        
                        if(!$result_count2){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count2");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }  
                    }else{
                        $payment_status = ", `payment_status`='F'";
                        $checkout_state = "failed";
                        $send_email = 1;
                    }
                    $update_query = sprintf("UPDATE `membership_payment` SET `checkout_status`='%s' $payment_status WHERE `membership_registration_id`='%s' and `membership_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        stripe_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        stripe_log_info($this->json($error));
                        if($send_email==1){
                            $update_query2 = sprintf("UPDATE `membership_registration` SET `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'",  mysqli_real_escape_string($this->db, $membership_reg_id));
                            $update_result2 = mysqli_query($this->db, $update_query2);
                            if(!$update_result2){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query2");
                                stripe_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                            $this->responseWithMail($this->json($error), 200);
                            $this->sendFailedOrderReceiptForMembershipPayment($membership_reg_id,0);
                        }else{
                            $this->responseWithMail($this->json($error), 200);
                            $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id, $membership_category_title, $membership_title, '');
                            $this->updateMembershipDimensionsNetSales($company_id, $membership_id, '');
                        }
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Payment details doesn't exist.");
                stripe_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    protected function sendFailedOrderReceiptForMembershipPayment($membership_reg_id, $return_value){
       
        $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
        $payment_details = [];
        $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
        $recurring_start_date = $payment_start_date = $payment_frequency = $custom_recurring_frequency_period_type = $msg1 = $msg2 = $msg3 = '';
        $membership_structure = $billing_options = $billing_options_no_of_payments = '';
        
        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, mp.`cc_name`, `waiver_policies`, mr.credit_card_name, 
            mr.`payment_start_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
            `buyer_postal_code`, mp.`checkout_status`,mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, DATE(mp.`created_dt`) created_dt, mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`
            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id`
            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N','F')
            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            stripe_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){                
                while($row = mysqli_fetch_assoc($result)){
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
//                    $company_id = $row['company_id'];
//                    $membership_id = $row['membership_id'];
//                    $membership_option_id = $row['membership_option_id'];
                    $payment_type = $row['payment_type'];
                    $membership_category_title = $row['membership_category_title'];
                    $membership_title = $row['membership_title'];
                    $membership_structure = $row['membership_structure'];
                    $participant_name = $row['participant_name'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $signup_fee = $row['signup_fee'];
                    $signup_fee_disc = $row['signup_fee_disc'];
                    $mem_fee = $row['membership_fee'];
                    $mem_fee_disc = $row['membership_fee_disc'];
                    $initial_payment = $row['initial_payment'];
                    $inital_processing_fee = $row['initial_processing_fee'];
                    $payment_start_date = $row['payment_start_date'];
                    $recurring_start_date = $row['recurring_start_date'];
                    $next_payment_date = $row['next_payment_date'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $recurring_amount = $row['recurring_amount'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
                    $first_payment = $row['first_payment'];
                    $billing_options = $row['billing_options'];
                    $no_of_classes = $row['no_of_classes'];
                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $credit_card_name = $row['credit_card_name'];
                    $checkout_state = $row['checkout_status'];
                    $temp=[];
                    if(!empty($row['membership_payment_id'])){
                        if($recurring_amount == $row['payment_amount']){
                            $recurring_processing_fee = $row['processing_fee'];
                        }
                        if($row['processing_fee_type']==2){
                            $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                        }else{
                            $temp['amount'] = $row['payment_amount'];
                        }
                        $temp['date'] = $row['payment_date'];
                        $temp['created_dt'] = $row['created_dt'];
                        $temp['processing_fee'] = $row['processing_fee'];
                        $temp['payment_status'] = $row['payment_status'];
                        $temp['paytime_type'] = $row['paytime_type'];
                        $temp['cc_name'] = $row['cc_name'];
                        $paid_amt += $temp['amount'];
                        $payment_details[] = $temp;
                    }
                }
                if(($membership_structure=='NC' || $membership_structure=='C') && $billing_options=='PP'){
                    if($billing_options_no_of_payments>1){
                        $already_recurring_records_count = 0;
                        $next_date = '';
                        for($i=0;$i<count($payment_details);$i++){
                            if($payment_details[$i]['paytime_type']=='R'){
                                $already_recurring_records_count++;
                                $next_date = $payment_details[$i]['date'];
                            }
                        }
                        if($billing_options_no_of_payments>$already_recurring_records_count){
                            $count = $billing_options_no_of_payments - $already_recurring_records_count;
                            $future_date = $next_date;
                            for($j=0;$j<$count;$j++){
                                if($payment_frequency=='W'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+1 weeks', strtotime($future_date)));
                                }elseif($payment_frequency=='B'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+14 days', strtotime($future_date)));
                                }elseif($payment_frequency=='M'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('next month', strtotime($future_date)));
                                }
                                
                                $temp=[];
                                if($processing_fee_type==2){
                                    $temp['amount'] = $recurring_amount+$recurring_processing_fee;
                                }else{
                                    $temp['amount'] = $recurring_amount;
                                }
                                $temp['date'] = $next_payment_date2;
                                $temp['processing_fee'] = $recurring_processing_fee;
                                $temp['payment_status'] = 'N';
                                $temp['paytime_type'] = 'R';
                                $temp['cc_name'] = $credit_card_name;
                                $payment_details[] = $temp;
                                $future_date = $next_payment_date2;
                            }
                        }
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                stripe_log_info($this->json($error_log));
                if($return_value==1){
                    $error = array('status' => "Failed", "msg" => "No records found.");
                    $this->response($this->json($error), 200);
                }
                return;
            }
        }

        $failed_str = 'failed';
        if ($checkout_state == 'charged back') {
            $failed_str = 'charged back';
        }

        $subject = $membership_category_title . " - Payment $failed_str";

        $message .= "<b>The following payment for the Buyer $buyer_name failed.</b><br><br>";
        $message .= "<b>Membership Details</b><br><br>";
        $message .= "Membership Start Date: ".date("M dS, Y", strtotime($payment_start_date))."<br>";
        $message .= "Membership: $membership_category_title, $membership_title<br>Participant: ".$participant_name."<br>";
        if($membership_structure=='NC' && $billing_options=='PP' && $billing_options_expiration_date!='0000-00-00'){
            $message .= "$no_of_classes Class Package Expires: ".date("D, M d, Y", strtotime($billing_options_expiration_date))."<br>";
        }
        $message .= "<br>";
            
        $prorated = "";
        if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
            $prorated = "(Pro-rated)";
        }

        if($paid_amt>0){
//            if($initial_payment>0){
                $message .= "<br><b>Registration Details</b><br><br>";
                $message .= "Sign-up Fee: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
                if($processing_fee_type==2){
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment+$inital_processing_fee),2))." (includes $wp_currency_symbol".$inital_processing_fee." administrative fees) $prorated<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }else{
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))." $prorated<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }
//            }
        }

        if(count($payment_details)>0){
            if($payment_frequency!='N' && $payment_type=='R'){
                $freq_str = "";
                if($membership_structure=='OE' || $membership_structure==1){
                    if($payment_frequency=='B'){
                        $freq_str = "Semi-Monthly Recurring: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "Monthly Recurring: ";
                    }elseif($payment_frequency=='W'){
                        $freq_str = "Weekly Recurring: ";
                    }elseif($payment_frequency=='A'){
                        $freq_str = "Annual Recurring: ";
                    }elseif($payment_frequency=='C'){
                        if($custom_recurring_frequency_period_type=='CW'){
                            $freq_str = "Weekly Recurring: ";
                        }elseif($custom_recurring_frequency_period_type=='CM'){
                            $freq_str = "Monthly Recurring: ";
                        }
                    }
                }elseif($billing_options=='PP'){
                    if($payment_frequency=='W'){
                        $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
                    }elseif($payment_frequency=='B'){
                        $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
                    }
                }
                
                $message .= "<br><br><b>Membership Payment Plan</b><br><br>";
                $recur_pf_str = "";
                if($processing_fee_type==2){
                    $recur_pf_str = " (plus $wp_currency_symbol".$recurring_processing_fee." administrative fees)";
                }
                if($freq_str!=""){
                    $message .= "$freq_str $wp_currency_symbol".$recurring_amount.$recur_pf_str."<br>";
                }
                $message .= "Next Payment Date: ".date("M dS, Y", strtotime($next_payment_date))."<br>";
            }
            
            $message.="<br><br><b>Payment Details</b><br><br>";
            for($init_success_payments=0;$init_success_payments<count($payment_details);$init_success_payments++){
                $pf_str = "";
                if($processing_fee_type==2){
                    $pf_str = " (includes $wp_currency_symbol".$payment_details[$init_success_payments]['processing_fee']." administrative fees)";
                }
                if($initial_payment>0 && $payment_details[$init_success_payments]['paytime_type']=='I'){
                    $down_pay = $payment_details[$init_success_payments]['amount'].$pf_str." paid on ".date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']))." (".$payment_details[$init_success_payments]['cc_name'].") " .$prorated;
                    $msg1 .= "Payment: $wp_currency_symbol".$down_pay."<br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='F'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    if($payment_details[$init_success_payments]['payment_status']=='S'){
                        $paid_str = "paid on";
                        $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    }else{
                        $paid_str = "due";
                        $pay_type= "";
                    }
                    $msg2 .= "First Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='S'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "paid on";
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='N' && ($membership_structure=='NC' || $membership_structure=='C') && $billing_options=='PP'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "due";
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
            }
            $message .= $msg1.$msg2.$msg3;
            $message .= "<br><br><br>";
        }
        
        $sendEmail_status = $this->sendEmailForMembership('',$studio_mail, $subject, $message, '', '', $cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            stripe_log_info($this->json($error_log));
            if($return_value==1){
                $log = array('status' => "Success", "msg" => "Membership order receipt sent successfully.");
                $this->response($this->json($log), 200);
            }
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            stripe_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Membership order receipt sent failed.");
                $this->response($this->json($error), 200);
            }
        }
    }
    protected function sendEmailForMembership($from, $to, $subject, $message, $cmp_name, $reply_to, $cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
//            if(filter_var($from, FILTER_VALIDATE_EMAIL)){
//                $mail->setFrom($from, $cmp_name);
//                $mail->AddCC($from);
//            }else{
//                $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        stripe_log_info($this->json($error_log));
        return $response;
    }
    public function addMembershipDimensions($company_id, $membership_id, $mem_option_id, $category_title, $option_title, $period) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($mem_option_id)) {
            $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                stripe_log_info($this->json($error_log));
                $reg_id = 1;
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $selectsql = sprintf("SELECT `active_members` from membership_dimensions where company_id='%s' and `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s' ",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $pre_dt));
                    $resultselectsql = mysqli_query($this->db, $selectsql);
                    if (!$resultselectsql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        stripe_log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
                        $num_of_rows2 = mysqli_num_rows($resultselectsql);
                        if ($num_of_rows2 == 0) {
                            $sqlselect1=sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and `membership_option_id`='%s' and  `company_id`= '%s'",  mysqli_real_escape_string($this->db,$membership_id),mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db,$company_id));
                            $resultsqlselect1=mysqli_query($this->db, $sqlselect1);
                            $value2 = mysqli_fetch_object($resultsqlselect1);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id),
                                    mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), mysqli_real_escape_string($this->db,$value2->active));
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                stripe_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        } else {
                            $value = mysqli_fetch_object($resultselectsql);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id),
                                    mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), $value->active_members);
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                stripe_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        }
                    }
                }
            }
        }

        $sql2 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            stripe_log_info($this->json($error_log));
            $reg_id = 1;
        } else{
            $num_of_rows3 = mysqli_num_rows($result2);
            if ($num_of_rows3 == 0) {
                $selectsql2 = sprintf("SELECT `active_members` FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $pre_dt));
                $resultselectsql2 = mysqli_query($this->db, $selectsql2);
                if (!$resultselectsql2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql2");
                    stripe_log_info($this->json($error_log));
                    $reg_id = 1;
                } else {
                    $num_of_rows4 = mysqli_num_rows($resultselectsql2);
                    if ($num_of_rows4 == 0) {
                        $sqlselect2 = sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and  `company_id`= '%s'", mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$company_id));
                        $resultsqlselect2 = mysqli_query($this->db, $sqlselect2);
                        $value3 = mysqli_fetch_object($resultsqlselect2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $value3->active));
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            stripe_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    } else {
                        $value1 = mysqli_fetch_object($resultselectsql2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), $value1->active_members);
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            stripe_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for addMembershipDimensions(), check log";
            $this->sendEmailForAdmins($msg);
        }
    }
    public function updateMembershipDimensionsNetSales($company_id, $membership_id, $date) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $mp_last_upd_date=" DATE(CONVERT_TZ(mp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $last_upd_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";
        if(empty($date)){
            $curr_dt = "($currdate_add)";
        }else{
            $curr_dt = "'$date'";
        }
//        $sql = sprintf("SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, sum(if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
//                  mp.payment_date FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
//                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
//                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  MONTH( $mp_last_upd_date) = MONTH($curr_dt)
//                  AND YEAR($mp_last_upd_date) = YEAR($curr_dt) AND mp.payment_status='S' AND mp.checkout_status='released' GROUP BY mr.`membership_id`, mr.`membership_option_id`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
        $date_inc1= "DATE_FORMAT($curr_dt,'%Y-%m')";
        $date_inc2= "DATE_FORMAT($mp_last_upd_date,'%Y-%m')";
        $date_inc3= "DATE_FORMAT($last_upd_date,'%Y-%m')";
        
        $sql = sprintf("SELECT company_id, membership_id, membership_option_id, mp_ud, sum(amount) amount, payment_date FROM (

                  SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount) amount, 
                  mp.payment_date, mp.membership_payment_id FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND (mp.payment_status='S' AND mp.checkout_status='released' || mp.payment_status IN ('MF', 'FR') || mp.payment_status='M' AND mp.`credit_method`!='MC')
                  AND %s = %s 
                  
                  UNION
                  
                  SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, concat('-',if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
                  mp.payment_date, mp.membership_payment_id FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  (mp.`payment_status`='R' 
                  AND IF(mr.registration_from='S', mp.payment_intent_id NOT IN (SELECT payment_intent_id FROM membership_payment WHERE payment_status='FR' AND %s = %s),
                  mp.checkout_id NOT IN (SELECT checkout_id FROM membership_payment WHERE payment_status='FR' AND %s = %s)) || mp.`payment_status`='MR')
                  AND %s = %s
                  )t1
                  
                  GROUP BY t1.`membership_id`, t1.`membership_option_id`"

                 , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id),
                 $date_inc2, $date_inc1, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id),
                 $date_inc3, $date_inc1, $date_inc3, $date_inc1, $date_inc2, $date_inc1);
        
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            stripe_log_info($this->json($error_log));
            $reg_id = 1;
        } else {
            $category = $membership_id_arr = $membership_options_id_arr = [];
            $number_rows = mysqli_num_rows($result);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $temp = $temp2 = [];
                    $temp['category_id'] = $temp2['category_id'] = $membership_id = $row['membership_id'];
                    $temp2['option_id'] = $membership_option_id = $row['membership_option_id'];
                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
                    $temp2['amount'] = $amount = $row['amount'];
                    $temp2['mp_ud'] = $row['mp_ud'];

                    if (!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)) {
                        $membership_id_arr[] = $membership_id;
                        $category[] = $temp;
                    }
                    $option[] = $temp2;
                }

                for ($i = 0; $i < count($option); $i++) {
                    $date_init = 0;
                   
                    $date_str = "%Y-%m";
                    $curr_dt = date("Y-m-d");
                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
//                     
                    $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')", mysqli_real_escape_string($this->db, $company_id), $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);
                    $res1 = mysqli_query($this->db, $sql1);
                    if (!$res1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        stripe_log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
//                           

                        $num_of_rows = mysqli_num_rows($res1);
                        if ($num_of_rows == 1) {

                            $sql2 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`='%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='%s'", $option[$i]['amount'], mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id']);
                            $res2 = mysqli_query($this->db, $sql2);
                            if (!$res2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                stripe_log_info($this->json($error_log));
                                $reg_id = 1;
                            } else {
                                if($i==0){
                                    $sales_value = "'%s'";
                                }else{
                                    $sales_value = "`sales_amount`+'%s'";
                                }
                                $sql3 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='0'",mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str,mysqli_real_escape_string($this->db, $membership_id));
                                $res3 = mysqli_query($this->db, $sql3);
                                if (!$res3) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                                    stripe_log_info($this->json($error_log));
                                    $reg_id = 1;
                                }
                            }
                        }
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "No records.","query" => "$sql");
                $this->response($this->json($error), 204);
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for updateMembershipDimensionsNetSales(), check log";
            $this->sendEmailForAdmins($msg);
        }
    }
    
}

$api = new stripeWebhook;
$api->processApi();