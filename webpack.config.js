var path = require('path');
var webpack = require('webpack');
var HtmlWebPackPlugin = require("html-webpack-plugin");
var MiniCssExtractPlugin = require("mini-css-extract-plugin");
var CopyPlugin = require('copy-webpack-plugin');
// var zopfli = require('@gfx/zopfli');
// var CompressionPlugin = require('compression-webpack-plugin');
var ZipPlugin = require('zip-webpack-plugin');

module.exports = {
  
    entry: {
        lib_bs:['./vx/WebPortal/bootstrap/css/bootstrap.min.css','./vx/WebPortal/fonts/css/font-awesome.min.css','./vx/WebPortal/bootstrap/css/nprogress.css',
                './vx/WebPortal/bootstrap/css/green.css','./vx/WebPortal/bootstrap/css/bootstrap-progressbar-3.3.4.min.css',
                './vx/WebPortal/bootstrap/css/jqvmap.min.css' 
                ],
        lib_css:['./vx/WebPortal/css/bootstrap-datepicker.css', './vx/WebPortal/css/jquery.timepicker.css',
                './vx/WebPortal/css/toastr.min.css','./vx/WebPortal/css/custom.min.css',
                 './vx/WebPortal/css/fixedColumns.dataTables.min.css',
                './vx/WebPortal/css/select.dataTables.css'],
        lib_ang_script:'./vx/WebPortal/js/lib.js',
        lib_bs_script:'./vx/WebPortal/js/lib1.js',
        // Location:'./vx/WebPortal/script/Location.js',
        app: ['./vx/WebPortal/script/app.js',],
        bundle:['./vx/WebPortal/Module/Controller/entry.js','./vx/WebPortal/css/mystudio.css','./vx/WebPortal/css/style.css'],
        bundle1:['./vx/WebPortal/Module/Controller/entry1.js','./vx/WebPortal/css/retail.css','./vx/WebPortal/css/tooltip.css']
    },
       
output: {
    filename: '[name].[contenthash].js',
    path: __dirname + '/dist'
},
 mode:'development',

module: {
    rules: [
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: true },
           
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
        {
            loader: 'file-loader',
            
          }
          
        ]
       
      },
      {
        test: /\.(sa|sc|c)ss$/,
        exclude:/node_modules/,
        use: [
         MiniCssExtractPlugin.loader,
          'css-loader',
         ],
         
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
            loader: 'file-loader',
           
        }]
     }

    ]
  }, 
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    // proxy: {
    //   '/api': {
    //       target: 'http://localhost:80',
    //       secure: false
    //   },
    // },
    port: 9000,
    disableHostCheck: true,
  },
 
plugins: [
    new HtmlWebPackPlugin({
      template: "./vx/WebPortal/index.html",
      filename: "./index.html",
      chunks:'[name].[contenthash]',
      title: 'Caching'
    }),
    new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/AddEvent.html",
        filename: "./Module/View/AddEvent.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/AddMembership.html",
        filename: "./Module/View/AddMembership.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/AddTrial.html",
        filename: "./Module/View/AddTrial.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/admin.html",
        filename: "./Module/View/admin.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/AdminClonePanel.html",
        filename: "./Module/View/AdminClonePanel.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/AdminHome.html",
        filename: "./Module/View/AdminHome.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/AdminLoginTool.html",
        filename: "./Module/View/AdminLoginTool.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/AdminStudioMembers.html",
        filename: "./Module/View/AdminStudioMembers.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/AppVerification.html",
        filename: "./Module/View/AppVerification.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/AttendanceDashboard.html",
        filename: "./Module/View/AttendanceDashboard.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/BillingStripe.html",
        filename: "./Module/View/BillingStripe.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/CurriculumContent.html",
        filename: "./Module/View/CurriculumContent.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/Customers.html",
        filename: "./Module/View/Customers.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/dashBoard.html",
        filename: "./Module/View/dashBoard.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/EmailSettings.html",
        filename: "./Module/View/EmailSettings.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/EventDetail.html",
        filename: "./Module/View/EventDetail.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/MiscDetail.html",
        filename: "./Module/View/MiscDetail.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/ForgotPassword.html",
        filename: "./Module/View/ForgotPassword.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/Home.html",
        filename: "./Module/View/Home.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/Integrations.html",
        filename: "./Module/View/Integrations.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/LeadsDetail.html",
        filename: "./Module/View/LeadsDetail.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/PosDashboard.html",
        filename: "./Module/View/PosDashboard.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/Login.html",
        filename: "./Module/View/Login.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/ManageEvent.html",
        filename: "./Module/View/ManageEvent.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/ManageLeads.html",
        filename: "./Module/View/ManageLeads.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/ManageMembership.html",
        filename: "./Module/View/ManageMembership.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/ManageTrial.html",
        filename: "./Module/View/ManageTrial.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/Members.html",
        filename: "./Module/View/Members.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/MembershipDetail.html",
        filename: "./Module/View/MembershipDetail.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/Message.html",
        filename: "./Module/View/Message.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/Payment.html",
        filename: "./Module/View/Payment.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/Referral.html",
        filename: "./Module/View/Referral.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/registration.html",
        filename: "./Module/View/registration.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/ResetPassword.html",
        filename: "./Module/View/ResetPassword.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/Social.html",
        filename: "./Module/View/Social.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/Template.html",
        filename: "./Module/View/Template.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/TemplateMembership.html",
        filename: "./Module/View/TemplateMembership.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/TrialDetail.html",
        filename: "./Module/View/TrialDetail.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/Trials.html",
        filename: "./Module/View/Trials.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/TrialTemplate.html",
        filename: "./Module/View/TrialTemplate.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/WepayCreation.html",
        filename: "./Module/View/WepayCreation.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/ManageRetail.html",
        filename: "./Module/View/ManageRetail.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/AddRetail.html",
        filename: "./Module/View/AddRetail.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/RetailDiscount.html",
        filename: "./Module/View/RetailDiscount.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/RetailFulfillment.html",
        filename: "./Module/View/RetailFulfillment.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/RetailTemplate.html",
        filename: "./Module/View/RetailTemplate.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/WhiteLabelSetup.html",
        filename: "./Module/View/WhiteLabelSetup.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/StripeCreation.html",
        filename: "./Module/View/StripeCreation.html",
        chunks:[],
        title: 'Caching'
      }),
      new HtmlWebPackPlugin({
        template: "./vx/WebPortal/Module/View/WhiteLabelDashboard.html",
        filename: "./Module/View/WhiteLabelDashboard.html",
        chunks:[],
        title: 'Caching'
      }),
      
      
      
    new CopyPlugin([
          { from: './vx/WebPortal/css/jquery.dataTables.min.css', to: './css/jquery.dataTables.min.css' },
          { from: './vx/WebPortal/css/dataTables.scroller.css', to: './css/dataTables.scroller.css' },
          { from: './vx/WebPortal/bootstrap/js/jquery.min.js', to: './bootstrap/js/jquery.min.js' },
          { from: './vx/WebPortal/js/jquery-1.12.4.js', to: './js/jquery-1.12.4.js' },
          { from: './vx/WebPortal/js/jquery-1.10.16.dataTables.min.js', to: './js/jquery-1.10.16.dataTables.min.js' },
          { from: './vx/WebPortal/js/angular.min.js', to: './js/angular.min.js' },
          { from: './vx/WebPortal/js/intercom_chat.js', to: './js/intercom_chat.js' },
          { from: './vx/WebPortal/js/angular-route.min.js', to: './js/angular-route.min.js' },
          { from: './vx/WebPortal/js/dataTables.scroller.js', to: './js/dataTables.scroller.js' },
          { from: './vx/WebPortal/js/dataTables.select.js', to: './js/dataTables.select.js' },
          { from: './vx/WebPortal/js/tooltip.js', to: './js/tooltip.js' },
          { from: './vx/WebPortal/js/angular-wysiwyg.js', to: './js/angular-wysiwyg.js' },
          { from: './vx/WebPortal/js/clipboard.min.js', to: './js/clipboard.min.js' },
          { from: './vx/WebPortal/js/ngclipboard.min.js', to: './js/ngclipboard.min.js' },
          { from: './vx/WebPortal/js/moment.js', to: './js/moment.js' },
          { from: './vx/WebPortal/js/jquery.date-dropdowns.min.js', to: './js/jquery.date-dropdowns.min.js' },
          { from: './vx/WebPortal/js/jquery.timepicker.js', to: './js/jquery.timepicker.js' },
          { from: './vx/WebPortal/js/bootstrap-datepicker.js', to: './js/bootstrap-datepicker.js' },
          { from: './vx/WebPortal/image/flags', to: './image/flags' },
          { from: './vx/WebPortal/iframe-mobile/dist', to: './iframe-mobile' },
          { from: './vx/WebPortal/css/angularjs-color-picker.min.css', to: './css/angularjs-color-picker.min.css' },
          { from: './vx/WebPortal/js/angularjs-color-picker.min.js', to: './js/angularjs-color-picker.min.js' },
          { from: './vx/WebPortal/js/tinycolor-min.js', to: './js/tinycolor-min.js' },
          { from: './vx/WebPortal/css/angularjs-color-picker-bootstrap.min.css', to: './css/angularjs-color-picker-bootstrap.min.css' },
          
          { from: './vx/WebPortal/js/bootstrap-colorpicker-module.js', to: './js/bootstrap-colorpicker-module.js' },
          { from: './vx/WebPortal/css/colorpicker.css', to: './css/colorpicker.css' },
           
           
    ]),
    
    new MiniCssExtractPlugin({
      filename: "[name].[contenthash].css",
    }),
    
      
  ]
};
