<?php

$ms_url=$ms_version='';
   
if(strpos($_SERVER['HTTP_HOST'], "dev.mystudio.academy") !== false || strpos($_SERVER['HTTP_HOST'], "dev2.mystudio.academy") !== false){  //Development
    $ms_url = 'http://dev.mystudio.app.s3-website-us-east-1.amazonaws.com/';
}elseif(strpos($_SERVER['HTTP_HOST'], "dev2.mystudio.academy") !== false){  //Development
    $ms_url = 'http://dev2.mystudio.academy/mystudioapp.php';
}elseif(strpos( $_SERVER['HTTP_HOST'], 'stage.mystudio.academy') !== false){  //Stage
    $ms_url = 'https://www.mystudio.app/';
}elseif(strpos( $_SERVER['HTTP_HOST'], 'mystudio.academy') !== false){ //Production
    $ms_url = 'https://www.mystudio.app/';
}else{
    $ms_url='vx/WebPortal/#/login';
}


if(isset($_REQUEST['pg']) && $_REQUEST['pg'] =='registration'){
    if(isset($_REQUEST['email']))
        header("Location: ".$ms_url.'v'.$ms_version.'/WebPortal/#/registration?email='.$_REQUEST['email']);
    else
        header("Location: ".$ms_url.'v'.$ms_version.'/WebPortal/#/registration');
}else{
    header("Location: ".$ms_url);
}

?>