<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * process single payment object from sqs aws 
 */
//$json_string = $_POST; 
// print_r($_POST);


require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/WePay.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';
//require_once __DIR__.'/../aws/aws-autoloader.php';
require_once __DIR__.'/../aws/aws.phar';
require_once __DIR__."/../Globals/sqs_credentials.php";

use Aws\Sqs\SqsClient;

class batchPaymentProcess{
     public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_pswd = '';
      private $sqs_key = '', $sqs_secret = '',$sqs_token='',$sqs_queueurl='';
    private $user_agent = '';
     private $server_url;
    private $stripe_pk = '', $stripe_sk = '';
    public function __construct(){
        require_once '../Globals/config.php';
        require_once  __DIR__ . "/../Globals/stripe_props.php";
        $this->wp = new wepay();        
        $this->inputs();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->user_agent = $this->getUserAgent();
        $this->sqs_key = $GLOBALS['sqs_keys']['key'];
        $this->sqs_secret = $GLOBALS['sqs_keys']['secret'];
        $this->sqs_queueurl = $GLOBALS['sqs_keys']['queueURL'];
        $this->sqs_token= $GLOBALS['sqs_token'];
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        if(!empty($GLOBALS['HOST_NAME'])){
            $host = $GLOBALS['HOST_NAME'];
        }else{
            $host = $_SERVER['HTTP_HOST'];
        }
        if(strpos($host, 'dev.mystudio.academy') !== false){
            $this->server_url = "http://dev.mystudio.academy";
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'https://www.mystudio.academy';
        }else{
            $this->server_url = "http://dev.mystudio.academy";
        }
    }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }
    
    private function getUserAgent(){
        if (!function_exists('getallheaders')){
            if(!function_exists ('apache_request_headers')){
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            }else{
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
            }
        }else{
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
        }
        return $user_agent;
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithMail($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
        header("Content-Type:" . $this->_content_type);
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function processApi(){
        if ($_SERVER['REQUEST_METHOD'] != 'POST'){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        if(isset($_REQUEST['run'])){
            $func = strtolower(trim(str_replace("/","",$_REQUEST['run'])));
        
            if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)){
                $this->_request = json_decode(file_get_contents('php://input'), true);
            }

            if((int)method_exists($this,$func) > 0){
                $this->$func();
            }else{
                $error = array("status" => "Failed", "msg" => "Page not found.");
                $this->response($this->json($error),404); // If the method not exist with in this class, response would be "Page not found".
            }
        }else{
            $error = array("status" => "Failed", "msg" => "Page not found.");
            $this->response($this->json($error),404); // If the method not exist with in this class, response would be "Page not found".
        }
        
    }
    
      private function processPayment() {
          $credentials = new Aws\Credentials\Credentials($this->sqs_key, $this->sqs_secret);
          $this->sqs = SqsClient::factory([
                    'credentials' => $credentials,
                    'region' => 'us-east-1',
                    'version' => '2012-11-05'
         ]);

        $payment_data = json_decode($this->_request['data'],true);
        $payment_date =  $payment_data['payment_date'];
        $key= str_replace(' ', '+', $payment_data['key']);
        $receipt_handle= str_replace(' ', '+', $payment_data['receipt']);

        $password1 = $this->sqs_token;
        $method1 = 'aes-256-cbc';
        // Must be exact 32 chars (256 bit)
        $password1 = substr(hash('sha256', $password1, true), 0, 32);
        // IV must be exact 16 chars (128 bit)
        $iv1 = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

        $token = openssl_decrypt(base64_decode($key), $method1, $password1, OPENSSL_RAW_DATA, $iv1);
        $reg_id=$payment_data['reg_id'];
        $payment_id=$payment_data['payment_id'];
        if( $token === "$password1.$payment_date"){
             if($payment_data['category'] === 'Membership'){
                 if($payment_data['payment_processsor'] == "wepay"){
                    $this->runCronJobForMembershipPayment($reg_id,$payment_id,$payment_date, $receipt_handle,$key);
                 }else if($payment_data['payment_processsor'] == "stripe"){
                    $this->runCronJobForMembershipPaymentFromStripe($reg_id,$payment_id,$payment_date, $receipt_handle,$key);
                 }
             }elseif ($payment_data['category'] === 'Event') {
                 if($payment_data['payment_processsor'] == "wepay"){
                    $this->runCronJobForEventPreapproval($reg_id,$payment_id,$payment_date,$receipt_handle,$key);
                 }else if($payment_data['payment_processsor'] == "stripe"){
                    $this->runCronJobForEventPreapprovalFromStripe($reg_id,$payment_id,$payment_date,$receipt_handle,$key);
                 }
             }
        }else{
             cjp_log_info("token invaild");
        }
        
    }
    
    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
    
    public function payment_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$amount_in_dollars,$stripe_currency,$studio_name,$buyer_email,$stripe_account_id,$dest_amount_in_dollars,$p_type){
        
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try {
            $payment_intent_create = \Stripe\PaymentIntent::create([
                        'payment_method' => $payment_method_id,
                        'customer' => $stripe_customer_id,
                        'amount' => $amount_in_dollars,
                        'currency' => $stripe_currency,
                        'confirmation_method' => 'automatic',
                        'capture_method' => 'automatic',
                        'confirm' => true,
                        'metadata' =>['c_id' => $company_id,'p_type' => $p_type,'s_id' => $student_id ,'s_name' => $studio_name],
                        'setup_future_usage' => 'off_session',
                        'description' => 'Payment Intent for ' . $buyer_email,
                        'payment_method_types' => ['card'],
                        'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                        'receipt_email' => $buyer_email,
                        'transfer_data' => ['destination' => $stripe_account_id, 'amount' => $dest_amount_in_dollars],
                        'on_behalf_of' => $stripe_account_id,
            ]);
            $body3 = json_encode($payment_intent_create);
            stripe_log_info("Stripe Payment Creation : $body3");
            $return_array['temp_payment_status'] = $payment_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['cc_month'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_month;
                $return_array['cc_year'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_year;
                $brand = $payment_intent_create->charges->data[0]->payment_method_details->card->brand;
                $last4 = $payment_intent_create->charges->data[0]->payment_method_details->card->last4;
                $temp = ' xxxxxx';
                $return_array['stripe_card_name'] = $brand . $temp . $last4;
            }else if ($return_array['temp_payment_status'] == 'requires_action'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['payment_intent_client_secret'] = $payment_intent_create->client_secret;
                $return_array['next_action'] = $payment_intent_create->next_action;
//                $return_array['next_action']['type'] = $payment_intent_create->next_action->type;
//                $return_array['next_action']['redirect_to_url']['url'] = $payment_intent_create->next_action->redirect_to_url->url;      
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $return_array = array('temp_payment_status' => "Failed", "msg" => $err['message'], "status_code" => $err['status_code']);
//            $this->response($this->json($error), 200);
        }
        return $return_array;
    }
    
    public function getCompanyStripeAccount($company_id, $call_back){
        $query = sprintf("SELECT * FROM `stripe_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result) >0){
                $row = mysqli_fetch_assoc($result);
                if($call_back == 1){
                    return $row;
                }else{
                    $msg = array("status" => "Success", "msg" => "Stripe Account details", "stripe_details" => "$row");
                    $this->response($this->json($msg), 200);
                }
            }else{
                if($call_back == 1){
                    return '';
                }else{
                    $msg = array("status" => "Failed", "msg" => "Stripe Account details", "stripe_details" => "");
                    $this->response($this->json($msg), 200);
                }
            }
        }
    }
    
    private function runCronJobForEventPreapprovalFromStripe($register_id,$payment_id,$payment_date,$receipt_data,$key){
        $failure = $success = $throttle = 0;
        $start_time = gmdate("Y-m-d H:i:s");
        cjp_log_info("runCronJobForEventPreapprovalFromStripe() - Started");
        $curr_date = gmdate("Y-m-d");
        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
        $stripe_account_id = $stripe_currency = $studio_name = $button_url = $checkout_state = '';
        $has_3D_secure = 'N';
        $output = [];
        
        $query = sprintf("SELECT  c.company_name,er.student_id,er.payment_method_id, er.stripe_card_name,er.stripe_customer_id,sa.`account_id`, sa.`currency`, er.`company_id`, c.`upgrade_status`, er.`student_id`, e.`event_title`, er.`event_reg_id`, `event_payment_id`, er.`event_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, 
                `registration_amount`, ep.`processing_fee`, `payment_type`, er.`payment_frequency`, er.`payment_amount` total_due, `paid_amount`, `credit_card_id`, `credit_card_name`, `credit_card_status`, `total_order_amount`, `total_order_quantity`, 
                ep.`payment_amount`, ep.`schedule_date`, ep.`schedule_status`, er.`processing_fee_type`,IFNULL(ep.start_time,0) as start_time ,IFNULL(ep.end_time,0) as end_time
                FROM `event_registration` er LEFT JOIN `stripe_account` sa ON c.`company_id`=sa.`company_id` 
                LEFT JOIN `company` c ON er.`company_id` = c.`company_id`
                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` AND ep.`schedule_status`='N'
                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
                WHERE ep.`schedule_date`='$payment_date' AND `payment_type`='RE' AND ep.`payment_amount`>0 and ep.`event_payment_id`='%s' and er.`event_reg_id`='%s' ORDER BY er.`company_id`", mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $register_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $company_id = $row['company_id'];   
                    $stripe_account_id = $row['account_id'];
                    $studio_name = $row['company_name'];
                    $event_reg_id = $row['event_reg_id'];
                    $event_payment_id = $row['event_payment_id'];
                    $processing_start_time=$row['start_time'];
                    $processing_end_time=$row['end_time'];
                    $stripe_customer_id = $row['stripe_customer_id'];
                    $payment_method_id = $row['payment_method_id'];
                    $stripe_card_name = $row['stripe_card_name'];
                    $stripe_currency = $row['currency'];
                    $student_id = $row['student_id'];
                    if($processing_start_time > 0) {
                        $msg = "Event Payment($event_payment_id) already in processing state";
                        $this->sendEmailForAdmins($msg);                        
                        $error = array("status" => "Failed", "msg" => $msg);
                        $this->response($this->json($error),500);
                    } else {
                         $payment_start_time = sprintf("UPDATE `event_payment` SET `start_time`=now() WHERE `event_payment_id`='%s'",mysqli_real_escape_string($this->db, $event_payment_id));
                            $result_start_time = mysqli_query($this->db, $payment_start_time);
                            if(!$result_start_time){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_start_time");
                                cjp_log_info($this->json($error_log));
                                if(isset($error_log['query'])){
                                    unset($error_log['query']);
                                }
                                $failure=1;
                                $output[] = $error_log;
                            }
                    }
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $buyer_postal_code = $row['buyer_postal_code'];
                    $event_id = $row['event_id'];
                    $event_name = $desc = $row['event_title'];
                    $payment_amount = $row['payment_amount'];
                    $processing_fee = $row['processing_fee'];
                    $processing_fee_type = $row['processing_fee_type'];
                    
                    $this->getStripeKeys();
                    \Stripe\Stripe::setApiKey($this->stripe_sk);
                    \Stripe\Stripe::setApiVersion("2019-05-16");
                    $stripe_account = $this->getCompanyStripeAccount($company_id, 1);
                    if ($stripe_account == '' || $stripe_account['charges_enabled'] == 'N') {
                        $error_log = "Stripe charges are disabled($company_id)";
                        cjp_log_info($error_log);
                        $failure = 1;
                        $output[] = $error_log;
                    }
                    
                    if($failure==0){
//                      payment intent
                        if (!empty($stripe_customer_id) && !(is_null($stripe_customer_id)) && !empty($payment_method_id)) {

                            $this->getStripeKeys();
                            \Stripe\Stripe::setApiKey($this->stripe_sk);
                            \Stripe\Stripe::setApiVersion("2019-05-16");

                            
//                            $paid_amount = $payment_amount;
                            if ($processing_fee_type == 2) {
                                $w_paid_amount = $payment_amount + $processing_fee;
                            } elseif ($processing_fee_type == 1) {
                                $w_paid_amount = $payment_amount;
                            }
                            $w_paid_amount_round = round($w_paid_amount, 2);
                            $dest_amount = $w_paid_amount - $processing_fee;
                            $dest_amount_round = round($dest_amount, 2);
                            $dest_amount_in_dollars = $dest_amount_round * 100;
                            $amount_in_dollars = $w_paid_amount_round * 100;
                            $p_type = 'event';
                            $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $p_type);
                            if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                                $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                $checkout_state = 'succeeded';
                                $success = 1;
                            } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {
                                $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                $checkout_state = 'requires_action';
                                $has_3D_secure = 'Y';
                                $next_action_type = $payment_intent_result['next_action']['type'];
                                if($next_action_type != "redirect_to_url"){
                                    # Payment not supported
                                    $out['status'] = 'Failed';
                                    $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                                    cjp_log_info($out['msg']."($payment_method_id)");
                                    $this->response($this->json($out), 200);
                                }
                                $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                                $success = 1;
                            } else {
                                $error = array("status" => "Failed", "status_code" => $payment_intent_result['status_code'], "msg" => $payment_intent_result['msg'], "id"=>$payment_id);
                                cjp_log_info($this->json($error));
                                $output[] = $error;
                                $failure = 1;
                                if($payment_intent_result['temp_payment_status'] == 'Failed' && $payment_intent_result['status_code']==429){
                                    $throttle = 1;
                                }
                            }
                        }else{
                            $output[] = "Empty stripe_customer_id or payment_method_id";
                            $failure = 1;
                        }
                    }
                    
                    if($failure==1){
                        $payment_query = sprintf("UPDATE `event_payment` SET `schedule_status`='F', `payment_method_id`='$payment_method_id', `stripe_card_name`='$stripe_card_name', `end_time`=now() WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $event_payment_id));
                        $payment_result = mysqli_query($this->db, $payment_query);
                        if(!$payment_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                            cjp_log_info($this->json($error_log));   
                        }
                        $this->sendFailedOrderReceiptForEventPayment($event_reg_id,0);
                        if($throttle==1){
                            $this->resendThrottlePayment($register_id,$payment_id,$payment_date,$key,"Event",$receipt_data);
                        }
                        $error = array("status" => "Failed", "throttle" => $throttle);
                        $output[] = $error;
                    }elseif($success==1){
                        $payment_query_7 = sprintf("UPDATE `event_payment` SET  `checkout_status`='%s', `schedule_status`='%s', `payment_method_id`='$payment_method_id', `payment_intent_id`='$payment_intent_id', `end_time`=now() WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $event_payment_id));
                        $payment_result = mysqli_query($this->db, $payment_query_7);
                        if (!$payment_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query_7");
                            cjp_log_info($this->json($error_log));
                        }
                        if($has_3D_secure == 'N'){
                            $this->sendOrderReceiptForEventPayment($event_reg_id, 0);
                        }else{
                            $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name);
                        }
                    }
                }
                
            }else{
                $error_log = array('status' => "Failed", "msg" => "Payment details mismatch(Reg_id : $register_id, Payment_id : $payment_id).");
                cjp_log_info($this->json($error_log));
            }
        }
        cjp_log_info("runCronJobForEventPreapprovalfromstripe() - Completed");
        $result1 = $this->sqs->deleteMessage([
            'QueueUrl' =>$this->sqs_queueurl, // REQUIRED
            'ReceiptHandle' => $receipt_data, // REQUIRED
        ]);
        if($failure==1){
            $error = array("status" => "Failed", "msg" => $output);
            $this->response($this->json($error),500);
        }elseif($success==1){
            $error = array("status" => "Success", "msg" => $output);
            $this->response($this->json($error),200);
        }else{
            $error = array("status" => "Failed", "msg" => $output);
            $this->response($this->json($error),500);
        }
//        $queueUrl = "https://sqs.us-east-1.amazonaws.com/241334827363/ms-bill-processing-dev";
    }
    
    protected function sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name){
        
        // EMAIL CONTENT
        $button_text = 'Launch';
        $subject = 'Stripe authenticate';
        $message = '';
        $message .= '<center><a href="'.$button_url.'"><button type="button" style="width: 230px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;padding: 12px 0;height: 50px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">'.$button_text.'</button></a></center>';
        $sendEmail_status = $this->sendEmail($buyer_email, $subject, $message, $studio_name, '', '', '', '', $company_id, '');
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            cjp_log_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            cjp_log_info($this->json($error_log));
        }
    }

    private function sendEmail($to, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file, $cc_email_list, $company_id, $footer_detail) {
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }            
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
            $mail->Subject = $subject;
            if (!empty($company_id)) {
                $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                $curr_loc = implode('/', explode('/', $curr_loc, -4));
//                $tomail = $to;
//                if (!empty(trim($cc_email_list))) {
//                    $tomail = base64_encode($to . "," . "$cc_email_list");
//                } else {
                    $tomail = base64_encode($to);
//                }
                $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/EmailSubscription.php?mail=$tomail?comp_id=$company_id?type=U";
                $message = "
            <html>
            <body>
                <p>$message</p><br><br>
                    <footer style='font-size: 13px;color: #746f6f;'>
                    <center>
                    <p>$footer_detail
                    </p>
                    <a href='$activation_link' target='_blank' style='color: #746f6f; !important;text-decoration: none !important;'><u style='color: #746f6f;'>Unsubscribe $to</u></a>
                    </center>
                    </footer>
            </body>
            </html>
            ";
            }
            $mail->Body = $message;
            if(!empty($file_name)){
                $mail->AddStringAttachment(base64_decode($attached_file), $file_name, 'base64');
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $verify_email_for_bounce = $this->verifyBouncedEmailForCC($cc_addresses[$init], $company_id);
                         if($verify_email_for_bounce==1){
                               $mail->AddCC($cc_addresses[$init]);
                         }
//                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        cjp_log_info($this->json($error_log));
        return $response;
    }
    
    private function runCronJobForMembershipPaymentFromStripe($register_id,$payment_id,$process_date,$receipt_data,$key){
        $total_records_processed = $failure = $success = 0;
        $start_time = gmdate("Y-m-d H:i:s");
        cjp_log_info("runCronJobForMembershipPaymentFromStripe() - Started");
        $reg_id = $output = [];
        $curr_date = $process_date;
        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = $button_url = $checkout_state = '';
        $has_3D_secure = 'N';
        $throttle = 0;
//        $throttle_count = 0;
        if(isset($_REQUEST['curr_date'])){
            $curr_date = $_REQUEST['curr_date'];
            $curr_date1 = "'".$_REQUEST['curr_date']."'";
        }else{
            $get_date = mysqli_query($this->db, "select CURDATE() curr_date, CURDATE()-INTERVAL 1 DAY prev_date;");
            $row = mysqli_fetch_assoc($get_date);
            $curr_date1 = "'".$row['curr_date']."','".$row['prev_date']."'";
        }
        $stripe_account_id = $stripe_currency = $studio_name = '';
        $query = sprintf("SELECT c.company_name,mr.student_id,mr.payment_method_id,mr.payment_method_id,mr.stripe_customer_id,sa.`account_id`, sa.`currency`, mr.`company_id`, c.`upgrade_status`, `membership_category_title`, `membership_title`, mr.`membership_registration_id`, mr.`membership_id`, mr.`membership_option_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `credit_card_id`, `credit_card_name`, `credit_card_status`, 
                mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`payment_type`, mr.`membership_status`, mr.`payment_frequency`, `membership_start_date`, `payment_start_date`, `next_payment_date` payment_date, mr.`membership_structure`, `first_payment`, `initial_payment`,
                mr.`custom_recurring_frequency_period_type`, mr.`custom_recurring_frequency_period_val`, `delay_recurring_payment_start_flg`, `delay_recurring_payment_start_date`, `delay_recurring_payment_amount`, `initial_payment_include_membership_fee`, 
                mr.`billing_options`, mr.`billing_options_no_of_payments`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`no_of_payments`, mp.`payment_amount`, mp.`membership_payment_id`, mp.`processing_fee`, mp.`paytime_type` record_type, IFNULL(mp.start_time,0) as start_time, IFNULL(mp.end_time,0) as end_time
                FROM `membership_payment` mp LEFT JOIN `stripe_account` sa ON c.`company_id`=sa.`company_id`
                LEFT JOIN `membership_registration` mr ON mr.`membership_registration_id`=mp.`membership_registration_id` AND mr.`company_id` = mp.`company_id`
                LEFT JOIN `company` c ON mr.`company_id` = c.`company_id`
                WHERE mp.`payment_date` IN ('%s') AND mp.`payment_status`='N' AND mr.`membership_status`='R' AND mp.`membership_payment_id`='%s' and mr.`membership_registration_id`='%s' and 
                ((`delay_recurring_payment_start_flg`='Y' AND `delay_recurring_payment_amount`>=5 AND `delay_recurring_payment_start_date` IN ('%s') AND mp.`paytime_type`='F')
                OR (`initial_payment_include_membership_fee`='N' AND `first_payment`>=5 AND `payment_start_date` IN ('%s') AND mp.`paytime_type`='F') 
                OR (`next_payment_date` IN ('%s') AND mr.`payment_amount`>=5) AND mp.`paytime_type`='R')", $process_date, mysqli_real_escape_string($this->db, $payment_id),mysqli_real_escape_string($this->db, $register_id),
                $process_date, $process_date, $process_date);
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
            if(isset($error_log['query'])){
                unset($error_log['query']);
            }
            $output[] = $error_log;
            $failure=1;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $company_id = $row['company_id'];
                    $stripe_account_id = $row['account_id'];
                    $studio_name = $row['company_name'];
                    $upgrade_status = $row['upgrade_status'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $buyer_postal_code = $row['buyer_postal_code'];
                    $membership_reg_id = $row['membership_registration_id'];
                    $membership_payment_id = $row['membership_payment_id'];
                    $membership_id = $row['membership_id'];
                    $membership_option_id = $row['membership_option_id'];
                    $membership_title = $desc = $row['membership_title'];
                    $membership_category_title=$row['membership_category_title'];
                    $membership_status = $row['membership_status'];
                    $payment_type = $row['payment_type'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $custom_recurring_frequency_period_val = $row['custom_recurring_frequency_period_val'];
                    $delay_recurring_payment_start_flg = $row['delay_recurring_payment_start_flg'];
                    $delay_recurring_payment_start_flg = $row['delay_recurring_payment_start_date'];
                    $delay_recurring_payment_amount = $row['delay_recurring_payment_amount'];
                    $recurring_amount = $row['recurring_amount'];
                    $payment_amount = $row['payment_amount'];
                    $initial_payment = $row['initial_payment'];
                    $temp_amount = $payment_amount;
                    $payment_date = $row['payment_date'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $processing_start_time=$row['start_time'];
                    $processing_end_time=$row['end_time'];
                    $stripe_customer_id = $row['stripe_customer_id'];
                    $payment_method_id = $row['payment_method_id'];
                    $stripe_currency = $row['currency'];
                    $student_id = $row['student_id'];
                    
                    $this->getStripeKeys();
                    \Stripe\Stripe::setApiKey($this->stripe_sk);
                    \Stripe\Stripe::setApiVersion("2019-05-16");
                    $stripe_account = $this->getCompanyStripeAccount($company_id, 1);
                    if ($stripe_account == '' || $stripe_account['charges_enabled'] == 'N') {
                        $error = "Stripe charges are disabled";
                        cjp_log_info($error);
                        $failure = 1;
                        $output[] = $error;
                    }

                    if($processing_start_time > 0){
                        $msg = "Membership Payment($membership_payment_id) already in processing state";
                        $this->sendEmailForAdmins($msg);
                        $error = array("status" => "Failed", "msg" => $msg);
                        $this->response($this->json($error),500);
                    }else{
                         $update_start_time = sprintf("UPDATE `membership_payment` SET `start_time`= now() WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
                                mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
                            $result_start_time = mysqli_query($this->db, $update_start_time);

                            if(!$result_start_time){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_start_time");
                                cjp_log_info($this->json($error_log));
                                if(isset($error_log['query'])){
                                    unset($error_log['query']);
                                }
                                $output[] = $error_log;
                            }
                    }
//                    $processing_fee = $row['processing_fee'];
                    $membership_structure = $row['membership_structure'];
                    $billing_options = $row['billing_options'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
                    $no_of_payments = $row['no_of_payments'];
                    $record_type = $row['record_type'];
                    $gmt_date=gmdate(DATE_RFC822);
                    $sns_msg=array("buyer_email"=>$buyer_email,"membership_title"=>$membership_title,"gmt_date"=>$gmt_date);
                    $p_f = $this->getProcessingFee($company_id, $upgrade_status);
                    $process_fee_per = $p_f['PP'];
                    $process_fee_val = $p_f['PT'];
                    $r1_fee = $temp_amount*($process_fee_per/100) + $process_fee_val;
                    $r2_fee = ($temp_amount + $r1_fee)*($process_fee_per/100) + $process_fee_val;
                    if($processing_fee_type==1){
                        $processing_fee = number_format((float)($r1_fee), 2, '.', '');
                    }else{
                        $processing_fee = number_format((float)($r2_fee), 2, '.', '');
                    }
                    
                    if(!empty($membership_reg_id)){
                        if(empty($membership_payment_id)){
                            cjp_log_info("Empty payment_id for reg_id($membership_reg_id), <br>Query : $query<br>");
                            $reg_id[] = $membership_reg_id;
//                            continue;
                            $error = array("status" => "Failed", "msg" => "Empty payment_id for reg_id($membership_reg_id)");
                            $this->response($this->json($error),500);
                        }
                        if(($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE' ) && $billing_options=='PP'){
                            if($billing_options_deposit_amount>=5 || $initial_payment>=5){
                                if($billing_options_no_of_payments==($no_of_payments-1)){
//                                    continue;
                                    $error = array("status" => "Failed", "msg" => "Payment count");
                                    $this->response($this->json($error),500);
                                }
                            }else{
                                if($billing_options_no_of_payments==$no_of_payments){
//                                    continue;
                                    $error = array("status" => "Failed", "msg" => "Payment count");
                                    $this->response($this->json($error),500);
                                }
                            }
                        }
                        
                        if($failure==0){
//                        payment intent
                            if (!empty($stripe_customer_id) && !(is_null($stripe_customer_id)) && !empty($payment_method_id)) {

                                $this->getStripeKeys();
                                \Stripe\Stripe::setApiKey($this->stripe_sk);
                                \Stripe\Stripe::setApiVersion("2019-05-16");

                                if ($payment_amount > 0) {
                                    $paid_amount = $payment_amount;
                                    if ($processing_fee_type == 2) {
                                        $w_paid_amount = $payment_amount + $processing_fee;
                                    } elseif ($processing_fee_type == 1) {
                                        $w_paid_amount = $payment_amount;
                                    }
                                    $w_paid_amount_round = round($w_paid_amount, 2);
                                    $dest_amount = $w_paid_amount - $processing_fee;
                                    $dest_amount_round = round($dest_amount, 2);
                                    $dest_amount_in_dollars = $dest_amount_round * 100;
                                    $amount_in_dollars = $w_paid_amount_round * 100;
                                    $p_type = 'membership';
                                    $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $p_type);
                                    if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                        $checkout_state = 'succeeded';
                                        $success = 1;
                                    } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {
                                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                        $checkout_state = 'requires_action';
                                        $has_3D_secure = 'Y';
                                        $next_action_type = $payment_intent_result['next_action']['type'];
                                        if($next_action_type != "redirect_to_url"){
                                            # Payment not supported
                                            $out['status'] = 'Failed';
                                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                                            $this->response($this->json($out), 200);
                                        }
                                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                                        $success = 1;
                                    } else {
                                        $error = array("status" => "Failed", "status_code" => $payment_intent_result['status_code'], "msg" => $payment_intent_result['msg'], "id"=>$payment_id);
                                        cjp_log_info($this->json($error));
                                        $failure = 1;
                                        $output[] = $error;
                                        if($payment_intent_result['temp_payment_status'] == 'Failed' && $payment_intent_result['status_code']==429){
                                            $throttle = 1;
                                        }
                                    }
                                }
                            }
                        }

                        if($failure==1){
                            $update_mem_payment = sprintf("UPDATE `membership_payment` SET `payment_status`='F', `processing_fee`='$processing_fee', end_time = now() WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
                                mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
                            $result_pay = mysqli_query($this->db, $update_mem_payment);

                            if(!$result_pay){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_mem_payment");
                                cjp_log_info($this->json($error_log));
                                if(isset($error_log['query'])){
                                    unset($error_log['query']);
                                }
                                $output[] = $error_log;
                            }
                                                        
                            if($record_type=='F' || $record_type=='I'){
                                $reg_query = sprintf("UPDATE `membership_registration` SET `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
                                        mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                $reg_result = mysqli_query($this->db, $reg_query);
                                if(!$reg_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                    cjp_log_info($this->json($error_log));
                                    if(isset($error_log['query'])){
                                        unset($error_log['query']);
                                    }
                                    $output[] = $error_log;
                                }
                            }elseif($record_type=='R'){
                                if($membership_structure=='OE' || $membership_structure==1){
                                    $next_pay_date = $payment_start_date = date("Y-m-d", strtotime($payment_date));
                                    $payment_end_date = date("Y-m-d", strtotime("+93 days"));
                                    $recurring_payment_date_array = [];
                                    $iteration_end = 1;
                                    $payment_date = $next_pay_date;
                                    for ($i = 0; $i < $iteration_end; $i++) {
                                        if ($payment_frequency == 'W') {
                                            $next_pay_date = date('Y-m-d', strtotime('+1 weeks', strtotime($payment_date)));
                                        } elseif ($payment_frequency == 'B') {
                                                if (date('j', strtotime($payment_date)) < 16) {
                                                    $next_pay_date = date('Y-m-16', strtotime($payment_date));
                                                } else {
                                                    $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($payment_date)));
                                                }
                                        } elseif ($payment_frequency == 'M') {
                                            $next_pay_date=$this->getSameDayNextMonth(new DateTime($payment_date));
                                        } elseif ($payment_frequency == 'A') {
                                            $next_pay_date = date('Y-m-d', strtotime('+1 year', strtotime($payment_date)));
                                        } elseif ($payment_frequency == 'C') {
                                            if ($custom_recurring_frequency_period_type == 'CW') {
                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($payment_date)));
                                            } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($payment_date)));
                                            }
                                        }
                                        $payment_date = $next_pay_date;
                                        if ($next_pay_date <= $payment_end_date || count($recurring_payment_date_array)==0) {
                                            $iteration_end++;
                                            $recurring_payment_date_array[] = $next_pay_date;
                                        }
                                    }

                                    for ($j = 0; $j < count($recurring_payment_date_array); $j++) {
                                        $check_payment_record = sprintf("SELECT * FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s' AND `payment_date`='%s' AND `payment_status`='N'",
                                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), $recurring_payment_date_array[$j]);
                                        $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
                                        if (!$result_check_payment_record) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                        }else{
                                            $num_of_rows = mysqli_num_rows($result_check_payment_record);
                                            if($num_of_rows==0){
                                                $insert_mem_pay = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $processing_fee), 
                                                        mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, 'R'));
                                                $result_mem_pay = mysqli_query($this->db, $insert_mem_pay);
                                                if (!$result_mem_pay) {
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                }
                                            }
                                        }
                                    }
                                    
                                    if(count($recurring_payment_date_array)>0){
                                        $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $recurring_payment_date_array[0]), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                        $reg_result = mysqli_query($this->db, $reg_query);
                                        if(!$reg_result){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                        }
                                    }else{
                                        $error_log = array('status' => "Failed", "msg" => "Error in calculate Next payment date.", "query" => "$check_payment_record");
                                        cjp_log_info($this->json($error_log));
                                        if(isset($error_log['query'])){
                                            unset($error_log['query']);
                                        }
                                        $output[] = $error_log;
                                        $reg_id[]=$membership_reg_id;
                                    }                                    
                                }else{
                                    $check_payment_record = sprintf("SELECT *, IF(`payment_date`>'$curr_date','upcoming','past') status FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s'",
                                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
                                    $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
                                    if (!$result_check_payment_record) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
                                        cjp_log_info($this->json($error_log));
                                        if(isset($error_log['query'])){
                                            unset($error_log['query']);
                                        }
                                        $output[] = $error_log;
                                    }else{
                                        $num_of_rows = mysqli_num_rows($result_check_payment_record);
                                        $next_payment_date = $curr_date;
                                        if($num_of_rows>0){
                                            $check_recent = 0;
                                            while($row2 = mysqli_fetch_assoc($result_check_payment_record)){
                                                if($row2['status']=='upcoming' && $row2['payment_status']=='N' && $check_recent==0){
                                                    $next_payment_date = $row2['payment_date'];
                                                    $check_recent = 1;
                                                }
                                            }
                                            if($check_recent==0){
                                                if(($initial_payment==0 && $no_of_payments==$billing_options_no_of_payments) || ($initial_payment>0 && $billing_options_no_of_payments==$no_of_payments-1)){
                                                    $error_log = array('status' => "Failed", "msg" => "Completed but failed payment available.", "query" => "$check_payment_record");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                    $reg_id[]=$membership_reg_id;
                                                }
                                            }else{
                                                $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
                                                        mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                                $reg_result = mysqli_query($this->db, $reg_query);
                                                if(!$reg_result){
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                }
                                            }
                                        }else{
                                            $error_log = array('status' => "Failed", "msg" => "No records found.", "query" => "$check_payment_record");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                        }
                                    }
                                }                                
                            }else{
                                $error = "reg_id($membership_reg_id), <br>Query : $query<br>";
                                cjp_log_info($error);
                                $reg_id[]=$membership_reg_id;
                                $failure=1;
                                $output[] = $error;
                            }
                            
                            $this->sendFailedOrderReceiptForMembershipPayment($membership_reg_id,0);
                        }
                        
                        if($success==1){
                            $update_mem_payment = sprintf("UPDATE `membership_payment` SET  `checkout_status`='%s', `payment_status`='S', `payment_method_id`='%s', `payment_intent_id`='%s', `processing_fee`='$processing_fee',end_time = now()  WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
                                     mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id),
                                    mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
                            $result_pay = mysqli_query($this->db, $update_mem_payment);

                            if(!$result_pay){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_mem_payment");
                                cjp_log_info($this->json($error_log));
                                if(isset($error_log['query'])){
                                    unset($error_log['query']);
                                }
                                $output[] = $error_log;
                                $failure=1;
                            }
                            
                            if($record_type=='F' || $record_type=='I'){
                                $reg_query = sprintf("UPDATE `membership_registration` SET `no_of_payments`=`no_of_payments`+1, `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
                                        mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                $reg_result = mysqli_query($this->db, $reg_query);
                                if(!$reg_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                    cjp_log_info($this->json($error_log));
                                    if(isset($error_log['query'])){
                                        unset($error_log['query']);
                                    }
                                    $output[] = $error_log;
                                    $failure=1;
                                }
                            }elseif($record_type=='R'){
                                if($membership_structure=='OE' || $membership_structure==1){
                                    $next_pay_date = $payment_start_date = date("Y-m-d", strtotime($payment_date));
                                    $payment_end_date = date("Y-m-d", strtotime("+93 days"));
                                    $recurring_payment_date_array = [];
                                    $iteration_end = 1;
                                    $payment_date = $next_pay_date;
                                    for ($i = 0; $i < $iteration_end; $i++) {
                                        if ($payment_frequency == 'W') {
                                            $next_pay_date = date('Y-m-d', strtotime('+1 weeks', strtotime($payment_date)));
                                        } elseif ($payment_frequency == 'B') {
                                                if (date('j', strtotime($payment_date)) < 16) {
                                                    $next_pay_date = date('Y-m-16', strtotime($payment_date));
                                                } else {
                                                    $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($payment_date)));
                                                }
                                        } elseif ($payment_frequency == 'M') {
                                            $next_pay_date=$this->getSameDayNextMonth(new DateTime($payment_date));
                                        } elseif ($payment_frequency == 'A') {
                                            $next_pay_date = date('Y-m-d', strtotime('+1 year', strtotime($payment_date)));
                                        } elseif ($payment_frequency == 'C') {
                                            if ($custom_recurring_frequency_period_type == 'CW') {
                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($payment_date)));
                                            } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($payment_date)));
                                            }
                                        }
                                        $payment_date = $next_pay_date;
                                        if ($next_pay_date <= $payment_end_date || count($recurring_payment_date_array)==0) {
                                            $iteration_end++;
                                            $recurring_payment_date_array[] = $next_pay_date;
                                        }
                                    }

                                    for ($j = 0; $j < count($recurring_payment_date_array); $j++) {
                                        $check_payment_record = sprintf("SELECT * FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s' AND `payment_date`='%s' AND `payment_status`='N'",
                                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), $recurring_payment_date_array[$j]);
                                        $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
                                        if (!$result_check_payment_record) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                            $failure=1;
                                        }else{
                                            $num_of_rows = mysqli_num_rows($result_check_payment_record);
                                            if($num_of_rows==0){
                                                $insert_mem_pay = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s')", 
                                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $processing_fee), 
                                                        mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, 'R'));
                                                $result_mem_pay = mysqli_query($this->db, $insert_mem_pay);
                                                if (!$result_mem_pay) {
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                    $failure=1;
                                                }
                                            }
                                        }
                                    }
                                    if(count($recurring_payment_date_array)>0){
                                        $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $recurring_payment_date_array[0]), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                        $reg_result = mysqli_query($this->db, $reg_query);
                                        if(!$reg_result){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                            $failure=1;
                                        }
                                    }else{
                                        $error_log = array('status' => "Failed", "msg" => "Error in calculate Next payment date.", "query" => "$check_payment_record");
                                        cjp_log_info($this->json($error_log));
                                        if(isset($error_log['query'])){
                                        unset($error_log['query']);
                                    }
                                        $output[] = $error_log;
                                        $failure=1;
                                        $reg_id[]=$membership_reg_id;
                                    }                                    
                                }else{
                                    $check_payment_record = sprintf("SELECT *, IF(`payment_date`>'$curr_date','upcoming','past') status FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s'",
                                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
                                    $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
                                    if (!$result_check_payment_record) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
                                        cjp_log_info($this->json($error_log));
                                        if(isset($error_log['query'])){
                                            unset($error_log['query']);
                                        }
                                        $output[] = $error_log;
                                        $failure=1;
                                    }else{
                                        $num_of_rows = mysqli_num_rows($result_check_payment_record);
                                        $next_payment_date = $curr_date;
                                        if($num_of_rows>0){
                                            $check_recent = 0;
                                            while($row2 = mysqli_fetch_assoc($result_check_payment_record)){
                                                if($row2['status']=='upcoming' && $row2['payment_status']=='N' && $check_recent==0){
                                                    $next_payment_date = $row2['payment_date'];
                                                    $check_recent = 1;
                                                }
                                            }
                                            if($check_recent==0){
                                                if(($initial_payment==0 && $no_of_payments==$billing_options_no_of_payments) || ($initial_payment>0 && $billing_options_no_of_payments==$no_of_payments-1)){
                                                    $reg_query = sprintf("UPDATE `membership_registration` SET `membership_status`='S', `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
                                                        mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                                    $reg_result = mysqli_query($this->db, $reg_query);
                                                    if(!$reg_result){
                                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                                        cjp_log_info($this->json($error_log));
                                                        if(isset($error_log['query'])){
                                                            unset($error_log['query']);
                                                        }
                                                        $output[] = $error_log;
                                                        $failure=1;
                                                    }
                                                }else{
                                                    $error_log = array('status' => "Failed", "msg" => "No records found.", "query" => "$check_payment_record");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                    $failure=1;
                                                }
                                            }else{
                                                $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
                                                        mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                                $reg_result = mysqli_query($this->db, $reg_query);
                                                if(!$reg_result){
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                    $failure=1;
                                                }
                                            }
                                        }else{
                                            $error_log = array('status' => "Failed", "msg" => "No records found.", "query" => "$check_payment_record");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                            $failure=1;
                                        }
                                    }
                                }                                
                            }else{
                                cjp_log_info("reg_id($membership_reg_id), <br>Query : $query<br>");
                                $reg_id[]=$membership_reg_id;                                
                                $error = array("status" => "Failed", "msg" => "Invalid Payment type");
                                $this->response($this->json($error),500);
                            }
                            $this->sendOrderReceiptForMembershipPayment($membership_reg_id, 0);
                        }
                    }else{
                        $error = "Empty Mem_reg_id($register_id)";
                        $output[] = $error;
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "Payment mismatch details(Reg_id : $register_id, Payment_id : $payment_id)");
                cjp_log_info($this->json($error_log));
                $output[] = $error_log;
                $failure = 1;
            }
            
        }
        
        if(!empty($output)){
            if($failure==1){
                $error = array("status" => "Failed", "msg" => $output);
                $this->response($this->json($error),500);
            }elseif($success==1){
                $error = array("status" => "Success", "msg" => $output);
                $this->response($this->json($error),200);
            }else{
                $error = array("status" => "Failed", "msg" => $output);
                $this->response($this->json($error),200);
            }
        }
//        $queueUrl = "https://sqs.us-east-1.amazonaws.com/241334827363/ms-bill-processing-dev";
            $result1 = $this->sqs->deleteMessage([
                'QueueUrl' => $this->sqs_queueurl, // REQUIRED
                'ReceiptHandle' => $receipt_data, // REQUIRED
            ]);
            echo $result1;
    }
    
    private function runCronJobForMembershipPayment($register_id,$payment_id,$process_date,$receipt_data,$key){
        $failure = $success = 0;
        cjp_log_info("runCronJobForMembershipPayment() - Started");
        $reg_id = $output = [];
        $curr_date = $process_date;
        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
//        $throttle_count = 0;
        if(isset($_REQUEST['curr_date'])){
            $curr_date = $_REQUEST['curr_date'];
            $curr_date1 = "'".$_REQUEST['curr_date']."'";
        }else{
            $get_date = mysqli_query($this->db, "select CURDATE() curr_date, CURDATE()-INTERVAL 1 DAY prev_date;");
            $row = mysqli_fetch_assoc($get_date);
            $curr_date1 = "'".$row['curr_date']."','".$row['prev_date']."'";
        }
        
        
        $query = sprintf("SELECT `access_token`, wp.`wp_user_state`, wp.`account_id`, wp.`currency`, wp.`account_state`, mr.`company_id`, c.`upgrade_status`, `membership_category_title`, `membership_title`, mr.`membership_registration_id`, mr.`membership_id`, mr.`membership_option_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `credit_card_id`, `credit_card_name`, `credit_card_status`, 
                mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`payment_type`, mr.`membership_status`, mr.`payment_frequency`, `membership_start_date`, `payment_start_date`, `next_payment_date` payment_date, mr.`membership_structure`, `first_payment`, `initial_payment`,
                mr.`custom_recurring_frequency_period_type`, mr.`custom_recurring_frequency_period_val`, `delay_recurring_payment_start_flg`, `delay_recurring_payment_start_date`, `delay_recurring_payment_amount`, `initial_payment_include_membership_fee`, 
                mr.`billing_options`, mr.`billing_options_no_of_payments`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`no_of_payments`, mp.`payment_amount`, mp.`membership_payment_id`, mp.`processing_fee`, mp.`paytime_type` record_type, IFNULL(mp.start_time,0) as start_time, IFNULL(mp.end_time,0) as end_time
                FROM `membership_payment` mp 
                LEFT JOIN `membership_registration` mr ON mr.`membership_registration_id`=mp.`membership_registration_id` AND mr.`company_id` = mp.`company_id`
                LEFT JOIN `wp_account` wp ON mr.`company_id` = wp.`company_id` LEFT JOIN `company` c ON mr.`company_id` = c.`company_id`
                WHERE mp.`payment_date` IN ('%s') AND mp.`payment_status`='N' AND mr.`membership_status`='R' AND mp.`membership_payment_id`='%s' and mr.`membership_registration_id`='%s' and 
                ((`delay_recurring_payment_start_flg`='Y' AND `delay_recurring_payment_amount`>=5 AND `delay_recurring_payment_start_date` IN ('%s') AND mp.`paytime_type`='F')
                OR (`initial_payment_include_membership_fee`='N' AND `first_payment`>=5 AND `payment_start_date` IN ('%s') AND mp.`paytime_type`='F') 
                OR (`next_payment_date` IN ('%s') AND mr.`payment_amount`>=5) AND mp.`paytime_type`='R')", $process_date, mysqli_real_escape_string($this->db, $payment_id),mysqli_real_escape_string($this->db, $register_id),
                $process_date, $process_date, $process_date);
        $result = mysqli_query($this->db, $query);
//        echo "Q - $query";
//        exit();
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
            if(isset($error_log['query'])){
                unset($error_log['query']);
            }
            $output[] = $error_log;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $company_list = [];
                while($row = mysqli_fetch_assoc($result)){
                    $company_id = $row['company_id'];
                    $access_token = $row['access_token'];
                    $account_id = $row['account_id'];
                    $acc_state = $row['account_state'];
                    $user_state = $row['wp_user_state'];
                    $currency = $row['currency'];
                    $upgrade_status = $row['upgrade_status'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $buyer_postal_code = $row['buyer_postal_code'];
                    $cc_id = $row['credit_card_id'];
                    $cc_name = $row['credit_card_name'];
                    $membership_reg_id = $row['membership_registration_id'];
                    $membership_payment_id = $row['membership_payment_id'];
                    $membership_id = $row['membership_id'];
                    $membership_option_id = $row['membership_option_id'];
                    $membership_title = $desc = $row['membership_title'];
                    $membership_category_title=$row['membership_category_title'];
                    $membership_status = $row['membership_status'];
                    $payment_type = $row['payment_type'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $custom_recurring_frequency_period_val = $row['custom_recurring_frequency_period_val'];
                    $delay_recurring_payment_start_flg = $row['delay_recurring_payment_start_flg'];
                    $delay_recurring_payment_start_flg = $row['delay_recurring_payment_start_date'];
                    $delay_recurring_payment_amount = $row['delay_recurring_payment_amount'];
                    $recurring_amount = $row['recurring_amount'];
                    $payment_amount = $row['payment_amount'];
                    $initial_payment = $row['initial_payment'];
                    $temp_amount = $payment_amount;
                    $payment_date = $row['payment_date'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $processing_start_time=$row['start_time'];
                    $processing_end_time=$row['end_time'];
                    if($processing_start_time > 0){
                        $msg = "Membership Payment($membership_payment_id) already in processing state";
                        $this->sendEmailForAdmins($msg);
                        $error = array("status" => "Failed", "msg" => $msg);
                        $this->response($this->json($error),500);
                    }else{
                         $update_start_time = sprintf("UPDATE `membership_payment` SET `start_time`= now() WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
                                mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
                            $result_start_time = mysqli_query($this->db, $update_start_time);

                            if(!$result_start_time){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_start_time");
                                cjp_log_info($this->json($error_log));
                                if(isset($error_log['query'])){
                                    unset($error_log['query']);
                                }
                                $output[] = $error_log;
                            }
                    }
//                    $processing_fee = $row['processing_fee'];
                    $membership_structure = $row['membership_structure'];
                    $billing_options = $row['billing_options'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
                    $no_of_payments = $row['no_of_payments'];
                    $record_type = $row['record_type'];
                    $gmt_date=gmdate(DATE_RFC822);
                    $sns_msg=array("buyer_email"=>$buyer_email,"membership_title"=>$membership_title,"gmt_date"=>$gmt_date);
                    $p_f = $this->getProcessingFee($company_id, $upgrade_status);
                    $process_fee_per = $p_f['PP'];
                    $process_fee_val = $p_f['PT'];
                    $r1_fee = $temp_amount*($process_fee_per/100) + $process_fee_val;
                    $r2_fee = ($temp_amount + $r1_fee)*($process_fee_per/100) + $process_fee_val;
                    if($processing_fee_type==1){
                        $processing_fee = number_format((float)($r1_fee), 2, '.', '');
                    }else{
                        $processing_fee = number_format((float)($r2_fee), 2, '.', '');
                    }
                    if(!empty($access_token) && !empty($membership_reg_id)){
                        if(empty($membership_payment_id)){
                            cjp_log_info("Empty payment_id for reg_id($membership_reg_id), <br>Query : $query<br>");
                            $reg_id[] = $membership_reg_id;
//                            continue;
                            $error = array("status" => "Failed", "msg" => "Empty payment_id for reg_id($membership_reg_id)");
                            $this->response($this->json($error),500);
                        }
                        if(($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE' ) && $billing_options=='PP'){
                            if($billing_options_deposit_amount>=5 || $initial_payment>=5){
                                if($billing_options_no_of_payments==($no_of_payments-1)){
//                                    continue;
                                    $error = array("status" => "Failed", "msg" => "Payment count");
                                    $this->response($this->json($error),500);
                                }
                            }else{
                                if($billing_options_no_of_payments==$no_of_payments){
//                                    continue;
                                    $error = array("status" => "Failed", "msg" => "Payment count");
                                    $this->response($this->json($error),500);
                                }
                            }
                        }
                        
                        $user_agent = "mystudio.academy";
                        $temp_password = $this->wp_pswd;
                        $method = 'aes-256-cbc';
                        // Must be exact 32 chars (256 bit)
                        $password = substr(hash('sha256', $temp_password, true), 0, 32);
                        // IV must be exact 16 chars (128 bit)
                        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                        
                        if($user_state=='deleted'){
                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. Wepay user registered was deleted.");
                            cjp_log_info("wepay_user: ".json_encode($log));
                            $failure=1;
                            $output[] = $log;
                        }
                        if($acc_state=='deleted'){
                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                            cjp_log_info("wepay_account: ".json_encode($log));
                            $failure=1;
                            $output[] = $log;
                        }elseif($acc_state=='disabled'){
                            $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                            cjp_log_info("wepay_account: ".json_encode($log));
                            $failure=1;
                            $output[] = $log;
                        }
                        
                        if($failure==0){
                            $rand = mt_rand(1,99999);
                            $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout?for=membership";
                            $unique_id = $company_id."_".$membership_id."_".$membership_option_id."_".$rand."_".time();
                            $date = gmdate("Y-m-d_H:i:s");
                            $ref_id = $company_id."_".$membership_id."_".$membership_option_id."_".$date;
                            $fee_payer = "payee_from_app";
                            $time = time();

                            if($processing_fee_type==2){
                                $w_paid_amount = $payment_amount + $processing_fee;
                                $paid_amount = $payment_amount;
                            }elseif($processing_fee_type==1){
                                $w_paid_amount = $payment_amount;
                                $paid_amount = $payment_amount - $processing_fee;
                            }
                            $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                                "fee"=>array("app_fee"=>$processing_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                                "email_message"=>array("to_payee"=>"Payment has been added for $membership_title","to_payer"=>"Payment has been made for $membership_title"),
                                "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                                "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                    array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                    array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))),
                                "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                    "properties"=>array("itemized_receipt"=>array(array("description"=>"$membership_category_title", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                            $postData2 = json_encode($json2);
                            $sns_msg['call'] = "Checkout";
                            $sns_msg['type'] = "Checkout Creation";
                            $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg);
                            cjp_log_info("wepay_checkout_create: ".$response2['status']);
                            if($response2['status']=="Success"){
                                $res2 = $response2['msg'];
                                $checkout_id = $res2['checkout_id'];
                                $checkout_state = $res2['state'];
                                $success=1;
                            }else{
                                if($response2['msg']['error_code']==1008 && strpos($response2['msg']['error_description'], 'api@wepay.com') !== false){
                                    $unique_id = $company_id."_".$membership_id."_".$membership_option_id."_".$rand."_".time();
                                    $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                                        "fee"=>array("app_fee"=>$processing_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                                        "email_message"=>array("to_payee"=>"Payment has been added for $membership_title","to_payer"=>"Payment has been made for $membership_title"),
                                        "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                                        "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                            array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                            array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))),
                                        "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                            "properties"=>array("itemized_receipt"=>array(array("description"=>"$membership_category_title", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                                    $postData2 = json_encode($json2);
                                    $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg);
                                    cjp_log_info("wepay_checkout_create: ".$response2['status']);
                                    if($response2['status']=="Success"){
                                        $res2 = $response2['msg'];
                                        $checkout_id = $res2['checkout_id'];
                                        $checkout_state = $res2['state'];
                                        $success=1;
                                    }else{
                                        if($response2['msg']['error_code']==1008){
                                            $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                            cjp_log_info($this->json($error));
                                        }else{
                                            $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                            cjp_log_info($this->json($error));
                                        }
                                        $output[] = $error;
                                        $failure=1;
                                    }
                                }else{
                                    $error = array("status" => "Failed", "msg" => $response2['msg']['error_description'],"id"=>$payment_id);
                                    cjp_log_info($this->json($error));
                                    if($response2['msg']['error_code']==1007 || $response2['msg']['error_code']==429){
                                        $this->resendThrottlePayment($register_id,$payment_id,$process_date,$key,"Membership",$receipt_data);
                                    }
                                    $failure=1;
                                    $output[] = $error;
                                }
                            }
                        }
                        
                        if($failure==1){
                            $update_mem_payment = sprintf("UPDATE `membership_payment` SET `payment_status`='F', `processing_fee`='$processing_fee', end_time = now() WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
                                mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
                            $result_pay = mysqli_query($this->db, $update_mem_payment);

                            if(!$result_pay){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_mem_payment");
                                cjp_log_info($this->json($error_log));
                                if(isset($error_log['query'])){
                                    unset($error_log['query']);
                                }
                                $output[] = $error_log;
                            }
                                                        
                            if($record_type=='F' || $record_type=='I'){
                                $reg_query = sprintf("UPDATE `membership_registration` SET `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
                                        mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                $reg_result = mysqli_query($this->db, $reg_query);
                                if(!$reg_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                    cjp_log_info($this->json($error_log));
                                    if(isset($error_log['query'])){
                                        unset($error_log['query']);
                                    }
                                    $output[] = $error_log;
                                }
                            }elseif($record_type=='R'){
                                if($membership_structure=='OE' || $membership_structure==1){
                                    $next_pay_date = $payment_start_date = date("Y-m-d", strtotime($payment_date));
                                    $payment_end_date = date("Y-m-d", strtotime("+93 days"));
                                    $recurring_payment_date_array = [];
                                    $iteration_end = 1;
                                    $payment_date = $next_pay_date;
                                    for ($i = 0; $i < $iteration_end; $i++) {
                                        if ($payment_frequency == 'W') {
                                            $next_pay_date = date('Y-m-d', strtotime('+1 weeks', strtotime($payment_date)));
                                        } elseif ($payment_frequency == 'B') {
                                                if (date('j', strtotime($payment_date)) < 16) {
                                                    $next_pay_date = date('Y-m-16', strtotime($payment_date));
                                                } else {
                                                    $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($payment_date)));
                                                }
                                        } elseif ($payment_frequency == 'M') {
                                            $next_pay_date=$this->getSameDayNextMonth(new DateTime($payment_date));
                                        } elseif ($payment_frequency == 'A') {
                                            $next_pay_date = date('Y-m-d', strtotime('+1 year', strtotime($payment_date)));
                                        } elseif ($payment_frequency == 'C') {
                                            if ($custom_recurring_frequency_period_type == 'CW') {
                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($payment_date)));
                                            } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($payment_date)));
                                            }
                                        }
                                        $payment_date = $next_pay_date;
                                        if ($next_pay_date <= $payment_end_date || count($recurring_payment_date_array)==0) {
                                            $iteration_end++;
                                            $recurring_payment_date_array[] = $next_pay_date;
                                        }
                                    }

                                    for ($j = 0; $j < count($recurring_payment_date_array); $j++) {
                                        $check_payment_record = sprintf("SELECT * FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s' AND `payment_date`='%s' AND `payment_status`='N'",
                                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), $recurring_payment_date_array[$j]);
                                        $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
                                        if (!$result_check_payment_record) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                        }else{
                                            $num_of_rows = mysqli_num_rows($result_check_payment_record);
                                            if($num_of_rows==0){
                                                $insert_mem_pay = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $processing_fee), 
                                                        mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                                $result_mem_pay = mysqli_query($this->db, $insert_mem_pay);
                                                if (!$result_mem_pay) {
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                }
                                            }
                                        }
                                    }
                                    
                                    if(count($recurring_payment_date_array)>0){
                                        $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $recurring_payment_date_array[0]), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                        $reg_result = mysqli_query($this->db, $reg_query);
                                        if(!$reg_result){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                        }
                                    }else{
                                        $error_log = array('status' => "Failed", "msg" => "Error in calculate Next payment date.", "query" => "$check_payment_record");
                                        cjp_log_info($this->json($error_log));
                                        if(isset($error_log['query'])){
                                            unset($error_log['query']);
                                        }
                                        $output[] = $error_log;
                                        $reg_id[]=$membership_reg_id;
                                    }                                    
                                }else{
                                    $check_payment_record = sprintf("SELECT *, IF(`payment_date`>'$curr_date','upcoming','past') status FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s'",
                                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
                                    $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
                                    if (!$result_check_payment_record) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
                                        cjp_log_info($this->json($error_log));
                                        if(isset($error_log['query'])){
                                            unset($error_log['query']);
                                        }
                                        $output[] = $error_log;
                                    }else{
                                        $num_of_rows = mysqli_num_rows($result_check_payment_record);
                                        $next_payment_date = $curr_date;
                                        if($num_of_rows>0){
                                            $check_recent = 0;
                                            while($row2 = mysqli_fetch_assoc($result_check_payment_record)){
                                                if($row2['status']=='upcoming' && $row2['payment_status']=='N' && $check_recent==0){
                                                    $next_payment_date = $row2['payment_date'];
                                                    $check_recent = 1;
                                                }
                                            }
                                            if($check_recent==0){
                                                if(($initial_payment==0 && $no_of_payments==$billing_options_no_of_payments) || ($initial_payment>0 && $billing_options_no_of_payments==$no_of_payments-1)){
                                                    $error_log = array('status' => "Failed", "msg" => "Completed but failed payment available.", "query" => "$check_payment_record");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                    $reg_id[]=$membership_reg_id;
                                                }
                                            }else{
                                                $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
                                                        mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                                $reg_result = mysqli_query($this->db, $reg_query);
                                                if(!$reg_result){
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                }
                                            }
                                        }else{
                                            $error_log = array('status' => "Failed", "msg" => "No records found.", "query" => "$check_payment_record");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                        }
                                    }
                                }                                
                            }else{
                                cjp_log_info("reg_id($membership_reg_id), <br>Query : $query<br>");
                                $reg_id[]=$membership_reg_id;
                                $failure=1;
                            }
                            
                            $this->sendFailedOrderReceiptForMembershipPayment($membership_reg_id,0);
//                            continue;
                        }
                        
                        if($success==1){
                            $update_mem_payment = sprintf("UPDATE `membership_payment` SET `checkout_id`='%s', `checkout_status`='%s', `payment_status`='S', `cc_id`='%s', `cc_name`='%s', `processing_fee`='$processing_fee',end_time = now()  WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
                                    mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name),
                                    mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
                            $result_pay = mysqli_query($this->db, $update_mem_payment);

                            if(!$result_pay){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_mem_payment");
                                cjp_log_info($this->json($error_log));
                                if(isset($error_log['query'])){
                                    unset($error_log['query']);
                                }
                                $output[] = $error_log;
                                $failure=1;
                            }
                            
                            if($record_type=='F' || $record_type=='I'){
                                $reg_query = sprintf("UPDATE `membership_registration` SET `no_of_payments`=`no_of_payments`+1, `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
                                        mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                $reg_result = mysqli_query($this->db, $reg_query);
                                if(!$reg_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                    cjp_log_info($this->json($error_log));
                                    if(isset($error_log['query'])){
                                        unset($error_log['query']);
                                    }
                                    $output[] = $error_log;
                                    $failure=1;
                                }
                            }elseif($record_type=='R'){
                                if($membership_structure=='OE' || $membership_structure==1){
                                    $next_pay_date = $payment_start_date = date("Y-m-d", strtotime($payment_date));
                                    $payment_end_date = date("Y-m-d", strtotime("+93 days"));
                                    $recurring_payment_date_array = [];
                                    $iteration_end = 1;
                                    $payment_date = $next_pay_date;
                                    for ($i = 0; $i < $iteration_end; $i++) {
                                        if ($payment_frequency == 'W') {
                                            $next_pay_date = date('Y-m-d', strtotime('+1 weeks', strtotime($payment_date)));
                                        } elseif ($payment_frequency == 'B') {
                                                if (date('j', strtotime($payment_date)) < 16) {
                                                    $next_pay_date = date('Y-m-16', strtotime($payment_date));
                                                } else {
                                                    $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($payment_date)));
                                                }
                                        } elseif ($payment_frequency == 'M') {
                                            $next_pay_date=$this->getSameDayNextMonth(new DateTime($payment_date));
                                        } elseif ($payment_frequency == 'A') {
                                            $next_pay_date = date('Y-m-d', strtotime('+1 year', strtotime($payment_date)));
                                        } elseif ($payment_frequency == 'C') {
                                            if ($custom_recurring_frequency_period_type == 'CW') {
                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($payment_date)));
                                            } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($payment_date)));
                                            }
                                        }
                                        $payment_date = $next_pay_date;
                                        if ($next_pay_date <= $payment_end_date || count($recurring_payment_date_array)==0) {
                                            $iteration_end++;
                                            $recurring_payment_date_array[] = $next_pay_date;
                                        }
                                    }

                                    for ($j = 0; $j < count($recurring_payment_date_array); $j++) {
                                        $check_payment_record = sprintf("SELECT * FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s' AND `payment_date`='%s' AND `payment_status`='N'",
                                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), $recurring_payment_date_array[$j]);
                                        $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
                                        if (!$result_check_payment_record) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                            $failure=1;
                                        }else{
                                            $num_of_rows = mysqli_num_rows($result_check_payment_record);
                                            if($num_of_rows==0){
                                                $insert_mem_pay = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $processing_fee), 
                                                        mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                                $result_mem_pay = mysqli_query($this->db, $insert_mem_pay);
                                                if (!$result_mem_pay) {
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                    $failure=1;
                                                }
                                            }
                                        }
                                    }
                                    if(count($recurring_payment_date_array)>0){
                                        $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $recurring_payment_date_array[0]), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                        $reg_result = mysqli_query($this->db, $reg_query);
                                        if(!$reg_result){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                            $failure=1;
                                        }
                                    }else{
                                        $error_log = array('status' => "Failed", "msg" => "Error in calculate Next payment date.", "query" => "$check_payment_record");
                                        cjp_log_info($this->json($error_log));
                                        if(isset($error_log['query'])){
                                            unset($error_log['query']);
                                        }
                                        $output[] = $error_log;
                                        $failure=1;
                                        $reg_id[]=$membership_reg_id;
                                    }                                    
                                }else{
                                    $check_payment_record = sprintf("SELECT *, IF(`payment_date`>'$curr_date','upcoming','past') status FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s'",
                                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
                                    $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
                                    if (!$result_check_payment_record) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
                                        cjp_log_info($this->json($error_log));
                                        if(isset($error_log['query'])){
                                            unset($error_log['query']);
                                        }
                                        $output[] = $error_log;
                                        $failure=1;
                                    }else{
                                        $num_of_rows = mysqli_num_rows($result_check_payment_record);
                                        $next_payment_date = $curr_date;
                                        if($num_of_rows>0){
                                            $check_recent = 0;
                                            while($row2 = mysqli_fetch_assoc($result_check_payment_record)){
                                                if($row2['status']=='upcoming' && $row2['payment_status']=='N' && $check_recent==0){
                                                    $next_payment_date = $row2['payment_date'];
                                                    $check_recent = 1;
                                                }
                                            }
                                            if($check_recent==0){
                                                if(($initial_payment==0 && $no_of_payments==$billing_options_no_of_payments) || ($initial_payment>0 && $billing_options_no_of_payments==$no_of_payments-1)){
                                                    $reg_query = sprintf("UPDATE `membership_registration` SET `membership_status`='S', `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
                                                        mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                                    $reg_result = mysqli_query($this->db, $reg_query);
                                                    if(!$reg_result){
                                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                                        cjp_log_info($this->json($error_log));
                                                        if(isset($error_log['query'])){
                                                            unset($error_log['query']);
                                                        }
                                                        $output[] = $error_log;
                                                        $failure=1;
                                                    }
                                                }else{
                                                    $error_log = array('status' => "Failed", "msg" => "No records found.", "query" => "$check_payment_record");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                    $failure=1;
                                                }
                                            }else{
                                                $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
                                                        mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                                $reg_result = mysqli_query($this->db, $reg_query);
                                                if(!$reg_result){
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                                    cjp_log_info($this->json($error_log));
                                                    if(isset($error_log['query'])){
                                                        unset($error_log['query']);
                                                    }
                                                    $output[] = $error_log;
                                                    $failure=1;
                                                }
                                            }
                                        }else{
                                            $error_log = array('status' => "Failed", "msg" => "No records found.", "query" => "$check_payment_record");
                                            cjp_log_info($this->json($error_log));
                                            if(isset($error_log['query'])){
                                                unset($error_log['query']);
                                            }
                                            $output[] = $error_log;
                                            $failure=1;
                                        }
                                    }
                                }                                
                            }else{
                                cjp_log_info("reg_id($membership_reg_id), <br>Query : $query<br>");
                                $reg_id[]=$membership_reg_id;                                
                                $error = array("status" => "Failed", "msg" => "Invalid Payment type");
                                $this->response($this->json($error),500);
                            }
                            $this->sendOrderReceiptForMembershipPayment($membership_reg_id, 0);
                        }
                    }else{
                        if(!in_array($company_id, $company_list) && empty($access_token)){
                            $error_log = array('status' => "Failed", "msg" => "Access token is empty for company(Id = $company_id). Cannot be accept payments anymore.");
                            cjp_log_info($this->json($error_log));
                            $failure = 1;
                            $output[] = $error_log;
                            $company_list[] = $company_id;
                        }
                        
                        $update_mem_payment = sprintf("UPDATE `membership_payment` SET `payment_status`='F', `end_time`=now() WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
                                mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
                        $result_pay = mysqli_query($this->db, $update_mem_payment);

                        if(!$result_pay){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_mem_payment");
                            cjp_log_info($this->json($error_log));
                            if(isset($error_log['query'])){
                                unset($error_log['query']);
                            }
                            $failure = 1;
                            $output[] = $error_log;
                        }

                        $reg_query = sprintf("UPDATE `membership_registration` SET `preceding_payment_date`='%s',`preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
                                mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                        $reg_result = mysqli_query($this->db, $reg_query);
                        if(!$reg_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                            cjp_log_info($this->json($error_log));
                            if(isset($error_log['query'])){
                                unset($error_log['query']);
                            }
                            $failure = 1;
                            $output[] = $error_log;
                        }
                        
                        $this->sendFailedOrderReceiptForMembershipPayment($membership_reg_id,0);
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No scheduled payment available for today($curr_date).");
                cjp_log_info($this->json($error_log));
                $output[] = $error_log;
                $failure = 1;
            }
            
        }
        
        if(!empty($output)){
            if($failure==1){
                $error = array("status" => "Failed", "msg" => $output);
                $this->response($this->json($error),500);
            }elseif($success==1){
                $error = array("status" => "Success", "msg" => $output);
                $this->response($this->json($error),200);
            }else{
                $error = array("status" => "Failed", "msg" => $output);
                $this->response($this->json($error),500);
            }
        }
//        if(!empty($reg_id)){
//            $reg_str = implode(", ", $reg_id);
//            $msg = "Something went wrong for reg_ids=$reg_str, check cron log";
//            $this->sendEmailForAdmins($msg);
//        }
//        cjp_log_info("runCronJobForMembershipPayment() - Completed");
//        if($total_records_processed>0){
//            $end_time = gmdate("Y-m-d H:i:s");
//            $cron_file_name = "RecurringPaymentHandler";
//            $cron_function_name = "runCronJobForMembershipPayment";
//            $cron_message = "Total membership payment records processed";
//            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
//                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
//            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
//            if(!$cron_log_result){
//                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
//                cjp_log_info($this->json($error_log));
//            }
//        }
//        $queueUrl = "https://sqs.us-east-1.amazonaws.com/241334827363/ms-bill-processing-dev";
            $result1 = $this->sqs->deleteMessage([
                'QueueUrl' => $this->sqs_queueurl, // REQUIRED
                'ReceiptHandle' => $receipt_data, // REQUIRED
            ]);
            echo $result1;
    }
    
    private function runCronJobForEventPreapproval($register_id,$payment_id,$payment_date,$receipt_data,$key){
        $total_records_processed = 0;
        $failure = $success = 0;
        $output=[];
        $start_time = gmdate("Y-m-d H:i:s");
        cjp_log_info("runCronJobForEventPreapproval() - Started");
        $curr_date = gmdate("Y-m-d");
        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
        
        $query = sprintf("SELECT `access_token`, wp.`wp_user_state`, wp.`account_id`, wp.`currency`, wp.`account_state`, wp.`account_id`, er.`company_id`, c.`upgrade_status`, er.`student_id`, e.`event_title`, er.`event_reg_id`, `event_payment_id`, er.`event_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, 
                `registration_amount`, ep.`processing_fee`, `payment_type`, er.`payment_frequency`, er.`payment_amount` total_due, `paid_amount`, `credit_card_id`, `credit_card_name`, `credit_card_status`, `total_order_amount`, `total_order_quantity`, 
                ep.`payment_amount`, ep.`schedule_date`, ep.`schedule_status`, er.`processing_fee_type`,IFNULL(ep.start_time,0) as start_time ,IFNULL(ep.end_time,0) as end_time
                FROM `event_registration` er 
                LEFT JOIN `wp_account` wp ON er.`company_id` = wp.`company_id` 
                LEFT JOIN `company` c ON er.`company_id` = c.`company_id`
                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` AND ep.`schedule_status`='N'
                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
                WHERE ep.`schedule_date`='$payment_date' AND `payment_type`='RE' AND ep.`payment_amount`>0 and ep.`event_payment_id`='%s' and er.`event_reg_id`='%s' ORDER BY er.`company_id`", mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $register_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
            if(isset($error_log['query'])){
                unset($error_log['query']);
            }
            $failure=1;
            $output[] = $error_log;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $total_records_processed += $num_rows;
                $company_list = [];
                while($row = mysqli_fetch_assoc($result)){
                    $company_id = $row['company_id'];
                    $access_token = $row['access_token'];
                    $account_id = $row['account_id'];
                    $acc_state = $row['account_state'];
                    $user_state = $row['wp_user_state'];
                    $currency = $row['currency'];
                    $event_reg_id = $row['event_reg_id'];
                    $event_payment_id = $row['event_payment_id'];
                    $processing_start_time=$row['start_time'];
                    $processing_end_time=$row['end_time'];
                    if($processing_start_time > 0) {
                        $msg = "Event Payment($event_payment_id) already in processing state";
                        $this->sendEmailForAdmins($msg);                        
                        $error = array("status" => "Failed", "msg" => $msg);
                        $this->response($this->json($error),500);
                    } else { 
                         $payment_start_time = sprintf("UPDATE `event_payment` SET `start_time`=now() WHERE `event_payment_id`='%s'",mysqli_real_escape_string($this->db, $event_payment_id));
                            $result_start_time = mysqli_query($this->db, $payment_start_time);
                            if(!$result_start_time){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_start_time");
                                cjp_log_info($this->json($error_log));
                                if(isset($error_log['query'])){
                                    unset($error_log['query']);
                                }
                                $failure=1;
                                $output[] = $error_log;
                            }
                    }
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $buyer_postal_code = $row['buyer_postal_code'];
                    $cc_id = $row['credit_card_id'];
                    $cc_name = $row['credit_card_name'];
                    if(!empty($access_token)){
                        $event_id = $row['event_id'];
                        $event_name = $desc = $row['event_title'];
                        $payment_amount = $row['payment_amount'];
                        $processing_fee = $row['processing_fee'];
                        $processing_fee_type = $row['processing_fee_type'];
                        $gmt_date=gmdate(DATE_RFC822);
                        $sns_msg1=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
                
                        $user_agent = "mystudio.academy";
                        $temp_password = $this->wp_pswd;
                        $method = 'aes-256-cbc';
                        // Must be exact 32 chars (256 bit)
                        $password = substr(hash('sha256', $temp_password, true), 0, 32);
                        // IV must be exact 16 chars (128 bit)
                        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                        
                        if($user_state=='deleted'){
                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. Wepay user registered was deleted.");
                            cjp_log_info("wepay_user: ".json_encode($log));
                            $output[] = $log;
                            $failure=1;
                        }
                        if($acc_state=='deleted'){
                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                            cjp_log_info("wepay_account: ".json_encode($log));
                            $output[] = $log;
                            $failure=1;
                        }elseif($acc_state=='disabled'){
                            $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                            cjp_log_info("wepay_account: ".json_encode($log));
                            $output[] = $log;
                            $failure=1;
                        }
                        
                        if($failure==1){
                            $payment_query_5 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name', `end_time`=now() WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $event_payment_id));
                            $payment_result = mysqli_query($this->db, $payment_query_5);
                            if(!$payment_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query_5");
                                cjp_log_info($this->json($error_log));
                                if(isset($error_log['query'])){
                                    unset($error_log['query']);
                                }
                                $failure=1;
                                $output[] = $error_log;
                            }
                            $this->sendFailedOrderReceiptForEventPayment($event_reg_id,0);
//                            continue;
                            $error = array("status" => "Failed", "msg" => $output);
                            $this->response($this->json($error),500);
                        }
                        
                        $rand = mt_rand(1,99999);
                        $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout";
                        $unique_id = $company_id."_".$event_id."_".$rand."_".time();
                        $date = gmdate("Y-m-d_H:i:s");
                        $ref_id = $company_id."_".$event_id."_".$date;
//                        if($processing_fee_type==2){
//                            $fee_payer = "payer_from_app";
//                        }elseif($processing_fee_type==1){
                            $fee_payer = "payee_from_app";
//                        }
                        if($processing_fee_type==2){
                            $w_paid_amount = $payment_amount + $processing_fee;
                        }elseif($processing_fee_type==1){
                            $w_paid_amount = $payment_amount;
                        }
                        $time = time();
                        $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                            "fee"=>array("app_fee"=>$processing_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                            "email_message"=>array("to_payee"=>"Payment has been added for $event_name","to_payer"=>"Payment has been made for $event_name"),
                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                            "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))),
                            "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                        $postData2 = json_encode($json2);
                        $sns_msg1['call'] = "Checkout";
                        $sns_msg1['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                        cjp_log_info("wepay_checkout_create: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $success=1;
                        }else{
                            if($response2['msg']['error_code']==1008 && strpos($response2['msg']['error_description'], 'api@wepay.com') !== false){
                                $unique_id = $company_id."_".$event_id."_".$rand."_".time();
                                $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                                    "fee"=>array("app_fee"=>$processing_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                                    "email_message"=>array("to_payee"=>"Payment has been added for $event_name","to_payer"=>"Payment has been made for $event_name"),
                                    "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                                    "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                        array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                        array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))),
                                    "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                        "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                                $postData2 = json_encode($json2);
                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                                cjp_log_info("wepay_checkout_create: ".$response2['status']);
                                if($response2['status']=="Success"){
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $success=1;
                                }else{
                                    if($response2['msg']['error_code']==1008){
                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        cjp_log_info($this->json($error));                               
                                        $failure=1;
                                        $output[] = $error;
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        cjp_log_info($this->json($error));                                                                       
                                        $failure=1;
                                        $output[] = $error;
                                    }
                                    $failure=1;
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description'],"id"=>$payment_id);
                                cjp_log_info($this->json($error));
                                if($response2['msg']['error_code']==1007 || $response2['msg']['error_code']==429){
                                    $this->resendThrottlePayment($register_id,$payment_id,$payment_date,$key,"Event",$receipt_data);
                                }
                                
                                $failure=1;
                                $output[] = $error;
                            }
                            if($failure==1){
                                $payment_query_6 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name', `end_time`=now() WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $event_payment_id));
                                $payment_result = mysqli_query($this->db, $payment_query_6);
                                if(!$payment_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query_6");
                                    cjp_log_info($this->json($error_log));
                                }
                                $this->sendFailedOrderReceiptForEventPayment($event_reg_id,0);
                                continue;
                            }
                        }
                        if($success==1){
                            $payment_query_7 = sprintf("UPDATE `event_payment` SET `checkout_id`='%s', `checkout_status`='%s', `schedule_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name', `end_time`=now() WHERE `event_payment_id`='%s'",
                                    mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, 'S'),
                                    mysqli_real_escape_string($this->db, $event_payment_id));
                            $payment_result = mysqli_query($this->db, $payment_query_7);
                            if(!$payment_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query_7");
                                cjp_log_info($this->json($error_log));
                            }else{
                                $this->sendOrderReceiptForEventPayment($event_reg_id, 0);
                            }
                        }

                    }else{
                        if(!in_array($company_id, $company_list)){
                            $error_log = array('status' => "Failed", "msg" => "Access token is empty for company(Id = $company_id). Cannot be accept payments anymore.");
                            cjp_log_info($this->json($error_log));
                            $company_list[] = $company_id;
                        }
                        $payment_query_8 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name',`end_time`=now() WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $event_payment_id));
                        $payment_result = mysqli_query($this->db, $payment_query_8);
                        if(!$payment_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query_8");
                            cjp_log_info($this->json($error_log));
                        }
                        $this->sendFailedOrderReceiptForEventPayment($event_reg_id,0);
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No scheduled payment available for today($curr_date).");
                cjp_log_info($this->json($error_log));
            }
        }
        cjp_log_info("runCronJobForEventPreapproval() - Completed");
//        if($total_records_processed>0){
//            $end_time = gmdate("Y-m-d H:i:s");
//            $cron_file_name = "RecurringPaymentHandler";
//            $cron_function_name = "runCronJobForEventPreapproval";
//            $cron_message = "Total event payment records processed";
//            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
//                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
//            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
//            if(!$cron_log_result){
//                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
//                cjp_log_info($this->json($error_log));
//            }
//        }
//        $queueUrl = "https://sqs.us-east-1.amazonaws.com/241334827363/ms-bill-processing-dev";
        $result1 = $this->sqs->deleteMessage([
            'QueueUrl' =>$this->sqs_queueurl, // REQUIRED
            'ReceiptHandle' => $receipt_data, // REQUIRED
        ]);
    }
    private function sendFailedOrderReceiptForEventPayment($event_reg_id, $return_value){
       
        $waiver_policies = '';
        $query = sprintf("SELECT er.`company_id`, er.`event_id`, `event_title`, ep.`cc_name`, `waiver_policies`, concat(er.event_registration_column_2, ',', er.event_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `payment_type`, 
            er.`payment_amount` total_due, er.`processing_fee_type`, er.`processing_fee` total_processing_fee, `paid_amount`, ep.`payment_amount`, ep.`processing_fee`, ep.`refunded_amount`, `schedule_date`, `schedule_status`, DATE(ep.`created_dt`) created_dt
            FROM `event_registration` er LEFT JOIN `event_payment` ep ON er.`event_reg_id` = ep.`event_reg_id` AND ep.`schedule_status` IN ('N', 'S', 'PR', 'M')
            LEFT JOIN `event` e ON e.`event_id` = er.`event_id` AND e.`company_id` =  er.`company_id`
            LEFT JOIN `company` c ON c.`company_id` =  er.`company_id`
            WHERE er.`event_reg_id` = '%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
                $event_name = $participant_name = $buyer_name = $buyer_email = $cc_email_list =$paid_str = '';
                $inital_stage = $initial_down_pay = 0;
                $current_plan_details = $event_details = $bill_due_details = $payment_details = [];
                $total_due = $paid_due = $rem_due = 0;
                while($row = mysqli_fetch_assoc($result)){
                    $paid_amt = 0;
                    $event_id = $row['event_id'];
                    $event_name = $row['event_title'];
                    $participant_name = $row['participant_name'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $temp=[];
                    
                    if($row['processing_fee_type']==2){
                        $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                    }else{
                        $temp['amount'] = $row['payment_amount'];
                    }
//                    $temp['amount'] = $row['payment_amount'];
                    $temp['date'] = $row['schedule_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['edit_status'] = 'N';
                    $temp['schedule_status'] = $row['schedule_status'];
                    $temp['processing_fee'] = $row['processing_fee'];
                    $temp['credit_card_name'] = $row['cc_name'];
                    if($row['schedule_status']=='N'){
                        $temp['status'] = 'N';
                    }elseif($row['schedule_status']=='S'){
                        $temp['status'] = 'S';
                        $paid_amt = $temp['amount'];
                        $inital_stage++;
                    }elseif($row['schedule_status']=='PR' || $row['schedule_status']=='M'){
                        $paid_amt = $temp['amount']-$temp['refunded_amount'];
                    }
//                    $total_due = $current_plan_details['total_due'] = $row['total_due'];
//                    $paid_due = $current_plan_details['paid_due'] = $row['paid_amount'];
                    $total_due += $temp['amount'];
                    $current_plan_details['total_due'] = $total_due;
                    $paid_due += $paid_amt;
                    $current_plan_details['paid_due'] = $paid_due;
                    
                    $current_plan_details['success_count'] = $inital_stage;
                    $rem_due = $current_plan_details['rem_due'] = number_format((float)($total_due - $paid_due), 2, '.', '');
//                    $payment_details[] = $temp;
                    if ($row['schedule_status'] == 'N') {
                        $bill_due_details[] = $temp;
                    } elseif ($row['schedule_status'] == 'S') {
                        $payment_details[] = $temp;
                        if ($initial_down_pay > 0) {
                            $bill_due_details[] = $temp;
                        }
                        $initial_down_pay++;
                    } elseif ($row['schedule_status'] == 'M') {
                        $payment_details[] = $temp;
                    }
                }
                
                $studio_name = $studio_mail = $message = '';
                $query2 = sprintf("SELECT c.`company_name`, c.`email_id`, c.`referral_email_list`, e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, `event_end_dt`, erd.`event_cost`, `discount`, erd.`payment_amount`, `quantity`, `payment_status`, `balance_due`,s.`student_cc_email`   
                    FROM `event` e LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` LEFT JOIN `event_registration` er ON er.`event_reg_id`= erd.`event_reg_id`
                    LEFT JOIN `company` c ON e.`company_id` =  c.`company_id` LEFT JOIN `student` s ON s.`company_id`= er.`company_id` AND s.`student_id`= er.`student_id`
                    WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND erd.`event_reg_id`='%s' AND `payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", 
                        mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                $result2 = mysqli_query($this->db, $query2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    cjp_log_info($this->json($error_log));
                    return;
                }else{
                    $temp_quantity = $temp_char_count = 0;
                    while($row2 = mysqli_fetch_assoc($result2)){
                        $temp2 = [];
                        $temp2['title'] = $row2['event_title'];
                        if($temp2['title']>$temp_char_count){
                            $temp_char_count = strlen($temp2['title']);
                        }
                        
                        if (!empty(trim($row2['event_begin_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_begin_dt']) {
                                $start = " - ";
                            } else {
                                $start = $row2['event_begin_dt'];

                                $split_start = explode(" ", $start);
                                $start_date_value = $split_start[0];
                                $start_time_value = $split_start[1];
                                if ("00:00:00" == $start_time_value) {
                                    $start = date('M d, Y', strtotime($start));
                                } else {
                                    $start = date('M d, Y @ h:i A', strtotime($start));
                                }
                            }
                        }
                        
                        if (!empty(trim($row2['event_end_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_end_dt']) {
                                $end = " - ";
                            } else {
                                $end = $row2['event_end_dt'];

                                $split_end = explode(" ", $end);
                                $end_date_value = $split_end[0];
                                $end_time_value = $split_end[1];
                                if ("00:00:00" == $end_time_value) {
                                    $end = date('M d, Y', strtotime($end));
                                } else {
                                    $end = date('M d, Y @ h:i A', strtotime($end));
                                }
                            }
                        }

                        $temp2['start'] = $start;
                        $temp2['end'] = $end;
                        $temp2['event_cost'] = $row2['event_cost'];
                        $temp2['quantity'] = $row2['quantity'];
                        $temp2['event_disc'] = $row2['discount'];
                        $temp2['discounted_price'] = $row2['event_cost'] - $row2['discount'];
                        $temp_quantity += $row2['quantity'];
                        $temp2['payment_amount'] = $row2['payment_amount'];
                        $temp2['paid_due'] = $temp2['payment_amount'] - $row2['balance_due'];
                        $temp2['rem_due'] = $row2['balance_due'];
                        $temp2['state'] = 'N';
                        $event_details[] = $temp2;
                        $studio_name = $row2['company_name'];
                        $studio_mail = $row2['email_id'];
                        $cc_email_list = $row2['referral_email_list'];
                        $student_cc_email_list = $row2['student_cc_email'];
                    }
                    $current_plan_details['total_event'] = $temp_quantity;
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                cjp_log_info($this->json($error_log));
                return;
            }
        }
        
        $subject = $event_name." - Payment failed";
        
        $message .= "<b>The following payment for the Buyer $buyer_name failed.</b><br><br>";
        $message .= "<b>Event Details</b><br><br>";
        for($i=0;$i<count($event_details);$i++){
            $message .= "Event: ".$event_details[$i]['title']."<br>Starts: ".$event_details[$i]['start']."<br>Ends: ".$event_details[$i]['end']."<br>Participant: ".$participant_name."<br><br>";
        }

        if($total_due>0){
            $message .= "<b>Billing Details</b><br><br>";
            if(count($bill_due_details)>0){
                $message .= "Total due: $".$total_due."<br>";
                $message .= "Balance due: $".$rem_due."<br>";                
                for($j=0;$j<count($bill_due_details);$j++){
                    $x= $j+1;
                    $amount = $bill_due_details[$j]['amount'];
                    $refunded_amount = $bill_due_details[$j]['refunded_amount'];
                    $final_amt = $amount-$refunded_amount;
                    $schedule_status = $bill_due_details[$j]['schedule_status'];
                    $date = date("M dS, Y", strtotime($bill_due_details[$j]['date']));
                    if($schedule_status=='N'){
                        $paid_str = "due";
                    }
                    $message .= "Bill ".$x.": $".$final_amt." ".$paid_str." ".$date."  <br>";
                }
                
                if(count($payment_details)>0){
                    $message.="<br><br><b>Payment Details</b><br><br>";
                    $down_pay = $payment_details[0]['amount']-$payment_details[0]['refunded_amount']." paid on ".date("M dS, Y", strtotime($payment_details[0]['date']))."(".$payment_details[0]['credit_card_name'].")";
                    $message .= "Down Payment: $".$down_pay."<br>";
                    for($k=1;$k<count($payment_details);$k++){
                        $x= $k;
                        $amount = $payment_details[$k]['amount'];
                        $refunded_amount = $payment_details[$k]['refunded_amount'];
                        $final_amt = $amount-$refunded_amount;
                        $schedule_status = $payment_details[$k]['schedule_status'];
                        $date = date("M dS, Y", strtotime($payment_details[$k]['date']));
                        $paid_str = "paid on";
                        if($schedule_status=='S'){
                            $pay_type= "(".$payment_details[$k]['credit_card_name'].")";
                        }else{
                            $pay_type="(manual credit applied)";
                        }
                        $message .= "Payment ".$x.": $".$final_amt." ".$paid_str." ".$date." ".$pay_type." <br>";
                    }
                }
            }else{
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $schedule_status = $payment_details[0]['schedule_status'];
                $final_amount = $amount-$refunded_amount;
                $message .= "Total Due: $".$final_amount."<br>Amount Paid: $".$final_amount."(".$payment_details[0]['credit_card_name'].")<br>";
            }
            $message .= "<br><br>";
        }
        $send_to_studio = 1;
        $sendEmail_status = $this->sendEmailForEvent('', $studio_mail, $subject, $message, '', '', '', 0, $cc_email_list, $student_cc_email_list, $send_to_studio);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $studio_mail."   ".$sendEmail_status['mail_status']);
            cjp_log_info($this->json($error_log));
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $studio_mail);
            cjp_log_info($this->json($error_log));
        }
    }
    
     private function sendEmailForEvent($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list1, $student_cc_email_list1, $send_to_studio) {
        $to_available = 0;
        $bounce_check=$this->sendBounceEmailCheck($to,$cc_email_list1,$student_cc_email_list1);
        
        $cc_email_list=$bounce_check['cc_email_array'];
        $student_cc_email_list=$bounce_check['student_CC_array'];
        $bounced_flag=$bounce_check['bounced_flag'];
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            if ($bounced_flag == 'Y') {
                if (!empty($student_cc_email_list)) {
                    $student_cc_addresses = $student_cc_email_list;
//                $student_cc_addresses = explode(',', $student_cc_email_list);
                    for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                        if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                            $mail->AddAddress($student_cc_addresses[$init_1]);
                            $to_available = 1;
                        }
                    }
                }
                
                if($to_available==0) {
                    return false;
                }
            } else {
                $mail->AddAddress($to);
                if (!empty($student_cc_email_list)) {
                    $student_cc_addresses = $student_cc_email_list;
//                $student_cc_addresses = explode(',', $student_cc_email_list);
                    for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                        if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                            $mail->AddCC($student_cc_addresses[$init_1]);
                        }
                    }
                }
            }

            if($send_to_studio==1){     //send_to_studio - enable/disable sending the email to studio in certain cases
                if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                    $mail->AddCC($from);
                }
                if(!empty($cc_email_list)){
                    $cc_addresses = $cc_email_list;
//                    $cc_addresses = explode(',', $cc_email_list);
                    for($init=0;$init<count($cc_addresses);$init++){
                        if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                            $mail->AddCC($cc_addresses[$init]);
                        }
                    }
                }
            }
           
            
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
//        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
//        cjp_log_info($this->json($error_log));
        return $response;
    }
    private function sendBounceEmailCheck($to1,$cc_email_list1,$student_cc_email_list1){
        $cc_email_list1 = preg_replace('/\s*,\s*/', ',', $cc_email_list1);
        $student_cc_email_list1 = preg_replace('/\s*,\s*/', ',', $student_cc_email_list1);
        $student_CC_array = explode(",",trim($student_cc_email_list1));
        $student_CC_list = implode("','", $student_CC_array);
        $cc_email_array = explode(",",$cc_email_list1);
        $cc_email_list= implode("','", $cc_email_array);
        $out=[];
        $bounced_flag='N';
        $sql= sprintf("SELECT 'T' as flag, count(*) as b_flag, '' bounced_mail FROM `bounce_email_verify` WHERE `bounced_mail`='%s'
                    UNION
                    SELECT 'S' as flag, 0 as b_flag, `bounced_mail` FROM `bounce_email_verify` WHERE `bounced_mail` in ('%s')
                    UNION
                    SELECT 'C' as flag, 0 as b_flag, `bounced_mail` FROM `bounce_email_verify` WHERE `bounced_mail` in ('%s')",
                    mysqli_real_escape_string($this->db, $to1), $student_CC_list,  $cc_email_list);
        $result= mysqli_query($this->db, $sql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
             cjp_log_info($this->json($error_log));
        }else{
            if(mysqli_num_rows($result)>0){
                while($rows = mysqli_fetch_assoc($result)){
                  if($rows['flag']=='S'){
                       if(in_array($rows['bounced_mail'], $student_CC_array)){                           
                           array_splice($student_CC_array, array_search($rows['bounced_mail'], $student_CC_array), 1);
                       }
                    
                  }else if($rows['flag']=='C'){
                    
                      if(in_array($rows['bounced_mail'], $cc_email_array)){
                           array_splice($cc_email_array, array_search($rows['bounced_mail'], $cc_email_array), 1);
                       }
                  }else if($rows['flag']=='T'){
                      if($rows['b_flag']>0){
                          $bounced_flag ='Y';
                      }
                  }
                }
            }
        }
         $out['cc_email_array'] = $cc_email_array;
         $out['student_CC_array'] = $student_CC_array;
         $out['bounced_flag'] = $bounced_flag;
         return $out;
    }
     public function getSameDayNextMonth(DateTime $startDate, $numberOfMonthsToAdd = 1) {
        $startDateDay = (int) $startDate->format('j');
        $startDateMonth = (int) $startDate->format('n');
        $startDateYear = (int) $startDate->format('Y');

        $numberOfYearsToAdd = floor(($startDateMonth + $numberOfMonthsToAdd) / 12);
        if ((($startDateMonth + $numberOfMonthsToAdd) % 12) === 0) {
          $numberOfYearsToAdd--;
        }
        $year = $startDateYear + $numberOfYearsToAdd;

        $month = ($startDateMonth + $numberOfMonthsToAdd) % 12;
        if ($month === 0) {
          $month = 12;
        }
        $month = sprintf('%02s', $month);

        $numberOfDaysInMonth = (new DateTime("$year-$month-01"))->format('t');
        $day = $startDateDay;
        if ($startDateDay > $numberOfDaysInMonth) {
          $day = $numberOfDaysInMonth;
        }
        $day = sprintf('%02s', $day);

        $output=new DateTime("$year-$month-$day");
        return $output->format('Y-m-d');
    }
    
    private function sendEmailForAdmins($msg){
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com,jeeva@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "Membership Subscription Handling Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        cjp_log_info($this->json($error_log));
        return $response;
    }
    
    private function sendEmailForMembership($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list1, $student_cc_email_list1, $send_to_studio) {
       $to_available = 0;
        $bounce_check=$this->sendBounceEmailCheck($to,$cc_email_list1,$student_cc_email_list1);
         $cc_email_list=$bounce_check['cc_email_array'];
        $student_cc_email_list=$bounce_check['student_CC_array'];
        $bounced_flag=$bounce_check['bounced_flag'];
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
//            if(filter_var($from, FILTER_VALIDATE_EMAIL)){
//                $mail->setFrom($from, $cmp_name);
//                $mail->AddCC($from);
//            }else{
//                $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            if ($bounced_flag == 'Y') {
                if (!empty($student_cc_email_list)) {
                    $student_cc_addresses = $student_cc_email_list;
//                $student_cc_addresses = explode(',', $student_cc_email_list);
                    for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                        if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                            $mail->AddAddress($student_cc_addresses[$init_1]);
                            $to_available=1;
                        }
                    }
                }
                if($to_available==0) {
                    return false;
                }
            } else {
                $mail->AddAddress($to);
                if (!empty($student_cc_email_list)) {
                    $student_cc_addresses = $student_cc_email_list;
//                $student_cc_addresses = explode(',', $student_cc_email_list);
                    for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                        if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                            $mail->AddCC($student_cc_addresses[$init_1]);
                        }
                    }
                }
            }

            if($send_to_studio==1){     //send_to_studio - enable/disable sending the email to studio in certain cases
                if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                    $mail->AddCC($from);
                }
                if(!empty($cc_email_list)){
                    $cc_addresses=$cc_email_list;
//                    $cc_addresses = explode(',', $cc_email_list);
                    for($init=0;$init<count($cc_addresses);$init++){
                        if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                            $mail->AddCC($cc_addresses[$init]);
                        }
                    }
                }
            }
            
            
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
//        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
//        cjp_log_info($this->json($error_log));
        return $response;
    }
    private function sendFailedOrderReceiptForMembershipPayment($membership_reg_id, $return_value){
       
        $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
        $payment_details = [];
        $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
        $recurring_start_date = $membership_start_date = $payment_start_date = $payment_frequency = $custom_recurring_frequency_period_type = $msg1 = $msg2 = $msg3 = '';
        $membership_structure = $billing_options = $billing_options_no_of_payments = '';
        
        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, mp.`cc_name`, `waiver_policies`, mr.credit_card_name, 
            mr.`membership_start_date`, mr.`payment_start_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
            `buyer_postal_code`, mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, DATE(mp.`created_dt`) created_dt, mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`
            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id` LEFT JOIN `student` s ON s.`company_id`= mr.`company_id` AND s.`student_id`= mr.`student_id`
            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N','F','M','R','MR')
            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){                
                while($row = mysqli_fetch_assoc($result)){
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
//                    $company_id = $row['company_id'];
//                    $membership_id = $row['membership_id'];
//                    $membership_option_id = $row['membership_option_id'];
                    $payment_type = $row['payment_type'];
                    $membership_category_title = $row['membership_category_title'];
                    $membership_title = $row['membership_title'];
                    $membership_structure = $row['membership_structure'];
                    $participant_name = $row['participant_name'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $signup_fee = $row['signup_fee'];
                    $signup_fee_disc = $row['signup_fee_disc'];
                    $mem_fee = $row['membership_fee'];
                    $mem_fee_disc = $row['membership_fee_disc'];
                    $initial_payment = $row['initial_payment'];
                    $inital_processing_fee = $row['initial_processing_fee'];
                    $membership_start_date = $row['membership_start_date'];
                    $payment_start_date = $row['payment_start_date'];
                    $recurring_start_date = $row['recurring_start_date'];
                    $next_payment_date = $row['next_payment_date'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $recurring_amount = $row['recurring_amount'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
                    $first_payment = $row['first_payment'];
                    $billing_options = $row['billing_options'];
                    $no_of_classes = $row['no_of_classes'];
                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $credit_card_name = $row['credit_card_name'];
                    $student_cc_email_list = $row['student_cc_email'];
                    $temp=[];
                    if(!empty($row['membership_payment_id'])){
                        if($recurring_amount == $row['payment_amount']){
                            $recurring_processing_fee = $row['processing_fee'];
                        }
                        if($row['processing_fee_type']==2){
                            if ($membership_structure != 'SE') {
                            $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                            } else if ($membership_structure == 'SE') {
                                $temp['amount'] = $row['payment_amount'];
                            }
                        }else{
                            $temp['amount'] = $row['payment_amount'];
                        }
                        $temp['date'] = $row['payment_date'];
                        $temp['created_dt'] = $row['created_dt'];
                        $temp['processing_fee'] = $row['processing_fee'];
                        $temp['payment_status'] = $row['payment_status'];
                        $temp['paytime_type'] = $row['paytime_type'];
                        $temp['cc_name'] = $row['cc_name'];
                        $paid_amt += $temp['amount'];
                        $payment_details[] = $temp;
                    }
                }
                if(($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE') && $billing_options=='PP'){
                    if($billing_options_no_of_payments>1){
                        $already_recurring_records_count = 0;
                        $next_date = '';
                        for($i=0;$i<count($payment_details);$i++){
                            if($payment_details[$i]['paytime_type']=='R'){
                                $already_recurring_records_count++;
                                $next_date = $payment_details[$i]['date'];
                            }
                        }
                        if($billing_options_no_of_payments>$already_recurring_records_count){
                            $count = $billing_options_no_of_payments - $already_recurring_records_count;
                            $future_date = $next_date;
                            for($j=0;$j<$count;$j++){
                                if($payment_frequency=='W'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+1 weeks', strtotime($future_date)));
                                }elseif($payment_frequency=='B'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+14 days', strtotime($future_date)));
                                }elseif($payment_frequency=='M'){
                                    $next_payment_date2=$this->getSameDayNextMonth(new DateTime($future_date));
                                }
                                
                                $temp_1=[];
                                if($processing_fee_type==2){
                                    if ($membership_structure != 'SE') {
                                    $temp_1['amount'] = $recurring_amount+$recurring_processing_fee;
                                    } else if ($membership_structure == 'SE') {
                                        $temp_1['amount'] = $recurring_amount;
                                    }
                                }else{
                                    $temp_1['amount'] = $recurring_amount;
                                }
                                $temp_1['date'] = $next_payment_date2;
                                $temp_1['processing_fee'] = $recurring_processing_fee;
                                $temp_1['payment_status'] = 'N';
                                $temp_1['paytime_type'] = 'R';
                                $temp_1['cc_name'] = $credit_card_name;
                                $payment_details[] = $temp_1;
                                $future_date = $next_payment_date2;
                            }
                        }
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                cjp_log_info($this->json($error_log));
                return;
            }
        }
        
        $subject = $membership_category_title." - Payment failed";
        
        $message .= "<b>The following payment for the Buyer $buyer_name failed.</b><br><br>";
        $message .= "<b>Membership Details</b><br><br>";
        $message .= "Membership Start Date: ".date("M dS, Y", strtotime($membership_start_date))."<br>";
        $message .= "Membership: $membership_category_title, $membership_title<br>Participant: ".$participant_name."<br>";
        if($membership_structure=='NC' && $billing_options=='PP' && $billing_options_expiration_date!='0000-00-00'){
            $message .= "$no_of_classes Class Package Expires: ".date("D, M d, Y", strtotime($billing_options_expiration_date))."<br>";
        }
        $message .= "<br>";
            
        $prorated = "";
        if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
            $prorated = "(Pro-rated)";
        }

        if($paid_amt>0){
//            if($initial_payment>0){
                $message .= "<br><b>Registration Details</b><br><br>";
                $message .= "Sign-up Fee: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
                if($processing_fee_type==2){
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment+$inital_processing_fee),2))." (includes $wp_currency_symbol".$inital_processing_fee." administrative fees) $prorated<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }else{
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))." $prorated<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }
//            }
        }
        if(count($payment_details)>0){
            if($payment_frequency!='N' && $payment_type=='R'){
                $freq_str = "";
                if($membership_structure=='OE' || $membership_structure==1){
                    if($payment_frequency=='B'){
                        $freq_str = "Semi-Monthly Recurring: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "Monthly Recurring: ";
                    }elseif($payment_frequency=='W'){
                        $freq_str = "Weekly Recurring: ";
                    }elseif($payment_frequency=='A'){
                        $freq_str = "Annual Recurring: ";
                    }elseif($payment_frequency=='C'){
                        if($custom_recurring_frequency_period_type=='CW'){
                            $freq_str = "Weekly Recurring: ";
                        }elseif($custom_recurring_frequency_period_type=='CM'){
                            $freq_str = "Monthly Recurring: ";
                        }
                    }
                }elseif($billing_options=='PP'){
                    if($payment_frequency=='W'){
                        $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
                    }elseif($payment_frequency=='B'){
                        $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
                    }
                }
                
                $message .= "<br><br><b>Membership Payment Plan</b><br><br>";
                $recur_pf_str = "";
                if ($membership_structure !== 'SE') {
                if($processing_fee_type==2){
                    $recur_pf_str = " (plus $wp_currency_symbol".$recurring_processing_fee." administrative fees)";
                }
                if($freq_str!=""){
                    $message .= "$freq_str $wp_currency_symbol".$recurring_amount.$recur_pf_str."<br>";
                }
                $message .= "Next Payment Date: ".date("M dS, Y", strtotime($next_payment_date))."<br>";
            }
            }
            
            if ($membership_structure !== 'SE') {
            $message.="<br><br><b>Payment Details</b><br><br>";
            }
            for($init_success_payments=0;$init_success_payments<count($payment_details);$init_success_payments++){
                $pf_str = "";
                if($processing_fee_type==2){
                    if ($membership_structure !== 'SE') {
                    $pf_str = " (includes $wp_currency_symbol".$payment_details[$init_success_payments]['processing_fee']." administrative fees)";
                    } else if ($membership_structure == 'SE') {
                        $pf_str = " (plus $wp_currency_symbol" . $payment_details[$init_success_payments]['processing_fee'] . " administrative fees)";
                }
                }
                if($initial_payment>0 && $payment_details[$init_success_payments]['paytime_type']=='I'){
                    $down_pay = $payment_details[$init_success_payments]['amount'].$pf_str." paid on ".date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']))." (".$payment_details[$init_success_payments]['cc_name'].") " .$prorated;
                    $msg1 .= "Payment: $wp_currency_symbol".$down_pay."<br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='F'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    if($payment_details[$init_success_payments]['payment_status']=='S'){
                        $paid_str = "paid on";
                        $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    }else{
                        $paid_str = "due";
                        $pay_type= "";
                    }
                    $msg2 .= "First Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }

                if($payment_details[$init_success_payments]['paytime_type']=='R' && ($payment_details[$init_success_payments]['payment_status']=='F') ||($payment_details[$init_success_payments]['payment_status']=='S')||
                        ($payment_details[$init_success_payments]['payment_status']=='R')||($payment_details[$init_success_payments]['payment_status']=='MR')||($payment_details[$init_success_payments]['payment_status']=='FR')||($payment_details[$init_success_payments]['payment_status']=='M')){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    if($payment_details[$init_success_payments]['payment_status']=='S'){
                      $paid_str = "paid on";  
                    }elseif($payment_details[$init_success_payments]['payment_status']=='R'){
                      $paid_str = "Refunded";  
                    }elseif($payment_details[$init_success_payments]['payment_status']=='MR'){
                      $paid_str = "Mannually Refunded";  
                    }elseif($payment_details[$init_success_payments]['payment_status']=='M'){
                      $paid_str = "Manual credit applied";  
                    }else{
                    $paid_str = "past due";
                    }
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='N' && ($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE') && $billing_options=='PP'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "due";
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
            }
            $message .= $msg1.$msg2.$msg3;
            $message .= "<br><br><br>";
        }
        $send_to_studio = 1;
        $sendEmail_status = $this->sendEmailForMembership('',$studio_mail, $subject, $message, '', '', '', 0, $cc_email_list, $student_cc_email_list, $send_to_studio);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            cjp_log_info($this->json($error_log));
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            cjp_log_info($this->json($error_log));
        }
    }
    
    private function sendOrderReceiptForEventPayment($event_reg_id, $return_value){
        $waiver_policies = '';
        $query = sprintf("SELECT er.`company_id`, er.`event_id`, `event_title`, ep.`cc_name`, `waiver_policies`, concat(er.event_registration_column_2, ',', er.event_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `payment_type`, 
            er.`payment_amount` total_due, er.`processing_fee_type`, er.`processing_fee` total_processing_fee, `paid_amount`, ep.`payment_amount`, ep.`processing_fee`, ep.`refunded_amount`, `schedule_date`, `schedule_status`, DATE(ep.`created_dt`) created_dt
            FROM `event_registration` er LEFT JOIN `event_payment` ep ON er.`event_reg_id` = ep.`event_reg_id` AND ep.`schedule_status` IN ('N', 'S', 'PR', 'M', 'F')
            LEFT JOIN `event` e ON e.`event_id` = er.`event_id` AND e.`company_id` =  er.`company_id`
            WHERE er.`event_reg_id` = '%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
                $event_name = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol =$paid_str= '';
                $inital_stage = $initial_down_pay = 0;
                $current_plan_details = $event_details = $bill_due_details = $payment_details = [];
                $total_due = $paid_due = $rem_due = 0;
                while($row = mysqli_fetch_assoc($result)){
                    $paid_amt = 0;
                    $company_id = $row['company_id'];
                    $event_id = $row['event_id'];
                    $event_name = $row['event_title'];
                    $participant_name = $row['participant_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $temp=[];
                    
                    if($row['processing_fee_type']==2){
                        $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                    }else{
                        $temp['amount'] = $row['payment_amount'];
                    }
//                    $temp['amount'] = $row['payment_amount'];
                    $temp['date'] = $row['schedule_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['edit_status'] = 'N';
                    $temp['schedule_status'] = $row['schedule_status'];
                    $temp['processing_fee'] = $row['processing_fee']; 
                    $temp['cc_name'] = $row['cc_name'];
                    if($row['schedule_status']=='N'){
                        $temp['status'] = 'N';
                    }elseif($row['schedule_status']=='S'){
                        $temp['status'] = 'S';
                        $paid_amt = $temp['amount'];
                        $inital_stage++;
                    }elseif($row['schedule_status']=='PR' || $row['schedule_status']=='M'){
                        $paid_amt = $temp['amount']-$temp['refunded_amount'];
                    }
//                    $total_due = $current_plan_details['total_due'] = $row['total_due'];
//                    $paid_due = $current_plan_details['paid_due'] = $row['paid_amount'];
                    $total_due += $temp['amount'];
                    $current_plan_details['total_due'] = $total_due;
                    $paid_due += $paid_amt;
                    $current_plan_details['paid_due'] = $paid_due;
                    
                    $current_plan_details['success_count'] = $inital_stage;
                    $rem_due = $current_plan_details['rem_due'] = number_format((float)($total_due - $paid_due), 2, '.', '');
//                    $payment_details[] = $temp;
                    if ($row['schedule_status'] == 'N' || $row['schedule_status'] == 'F') {
                        $bill_due_details[] = $temp;
                    } elseif ($row['schedule_status'] == 'S' || $row['schedule_status'] == 'CR') {
                        $payment_details[] = $temp;
                        if ($initial_down_pay > 0) {
                            $bill_due_details[] = $temp;
                        }
                        $initial_down_pay++;
                    }elseif ($row['schedule_status'] == 'M') {
                        $payment_details[] = $temp;
                    }
                }
                
                $query2 = sprintf("SELECT c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`, e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, `event_end_dt`, erd.`event_cost`, `discount`, erd.`payment_amount`, `quantity`, `payment_status`, `balance_due`,s.`student_cc_email` 
                    FROM `event` e LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` LEFT JOIN `event_registration` er ON er.`event_reg_id`= erd.`event_reg_id`
                    LEFT JOIN `company` c ON e.`company_id` =  c.`company_id` LEFT JOIN `student` s ON s.`company_id`= er.`company_id` AND s.`student_id`= er.`student_id`
                    WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND erd.`event_reg_id`='%s' AND `payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", 
                        mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                $result2 = mysqli_query($this->db, $query2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    cjp_log_info($this->json($error_log));
                    return;
                }else{
                    $temp_quantity = $temp_char_count = 0;
                    while($row2 = mysqli_fetch_assoc($result2)){
                        $temp2 = [];
                        $temp2['title'] = $row2['event_title'];
                        if($temp2['title']>$temp_char_count){
                            $temp_char_count = strlen($temp2['title']);
                        }
                        
                        if (!empty(trim($row2['event_begin_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_begin_dt']) {
                                $start = " - ";
                            } else {
                                $start = $row2['event_begin_dt'];

                                $split_start = explode(" ", $start);
                                $start_date_value = $split_start[0];
                                $start_time_value = $split_start[1];
                                if ("00:00:00" == $start_time_value) {
                                    $start = date('M d, Y', strtotime($start));
                                } else {
                                    $start = date('M d, Y @ h:i A', strtotime($start));
                                }
                            }
                        }
                        
                        if (!empty(trim($row2['event_end_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_end_dt']) {
                                $end = " - ";
                            } else {
                                $end = $row2['event_end_dt'];

                                $split_end = explode(" ", $end);
                                $end_date_value = $split_end[0];
                                $end_time_value = $split_end[1];
                                if ("00:00:00" == $end_time_value) {
                                    $end = date('M d, Y', strtotime($end));
                                } else {
                                    $end = date('M d, Y @ h:i A', strtotime($end));
                                }
                            }
                        }

                        $temp2['start'] = $start;
                        $temp2['end'] = $end;
                        $temp2['event_cost'] = $row2['event_cost'];
                        $temp2['quantity'] = $row2['quantity'];
                        $temp2['event_disc'] = $row2['discount'];
                        $temp2['discounted_price'] = $row2['event_cost'] - $row2['discount'];
                        $temp_quantity += $row2['quantity'];
                        $temp2['payment_amount'] = $row2['payment_amount'];
                        $temp2['paid_due'] = $temp2['payment_amount'] - $row2['balance_due'];
                        $temp2['rem_due'] = $row2['balance_due'];
                        $temp2['state'] = 'N';
                        $event_details[] = $temp2;
                        $studio_name = $row2['company_name'];
                        $studio_mail = $row2['email_id'];
                        $cc_email_list = $row2['referral_email_list'];
                        $wp_currency_code = $row2['wp_currency_code'];
                        $wp_currency_symbol = $row2['wp_currency_symbol'];
                        $student_cc_email_list = $row2['student_cc_email']; 
                    }
                    $current_plan_details['total_event'] = $temp_quantity;
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                cjp_log_info($this->json($error_log));
                return;
            }
        }
        
        $subject = $event_name." Order Confirmation";
        
        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
        $message .= "<b>Event Details</b><br><br>";
        for($i=0;$i<count($event_details);$i++){
            $message .= "Event: ".$event_details[$i]['title']."<br>Starts: ".$event_details[$i]['start']."<br>Ends: ".$event_details[$i]['end']."<br>Participant: ".$participant_name."<br><br>";
        }

        if($total_due>0){
            $message .= "<b>Billing Details</b><br><br>";
            if(count($bill_due_details)>0){
                $message .= "Total due: $wp_currency_symbol".$total_due."<br>";
                $message .= "Balance due: $wp_currency_symbol".$rem_due."<br>";                
                for($j=0;$j<count($bill_due_details);$j++){
                    $x= $j+1;
                    $amount = $bill_due_details[$j]['amount'];
                    $refunded_amount = $bill_due_details[$j]['refunded_amount'];
                    $final_amt = $amount-$refunded_amount;
                    $schedule_status = $bill_due_details[$j]['schedule_status'];
                    $date = date("M dS, Y", strtotime($bill_due_details[$j]['date']));
                    if($schedule_status=='N'){
                        $paid_str = "due";
                    }
                    $message .= "Bill ".$x.": $wp_currency_symbol".$final_amt." ".$paid_str." ".$date."  <br>";
                }
                
                if(count($payment_details)>0){
                    $message.="<br><br><b>Payment Details</b><br><br>";
                    $down_pay = $payment_details[0]['amount']-$payment_details[0]['refunded_amount']." paid on ".date("M dS, Y", strtotime($payment_details[0]['date']))."(".$payment_details[0]['cc_name'].")";
                    $message .= "Down Payment: $wp_currency_symbol".$down_pay."<br>";
                    for($k=1;$k<count($payment_details);$k++){
                        $x= $k;
                        $amount = $payment_details[$k]['amount'];
                        $refunded_amount = $payment_details[$k]['refunded_amount'];
                        $final_amt = $amount-$refunded_amount;
                        $schedule_status = $payment_details[$k]['schedule_status'];
                        $date = date("M dS, Y", strtotime($payment_details[$k]['date']));
                        $paid_str = "paid on";
                        if($schedule_status=='S'){                            
                            $pay_type= "(".$payment_details[$k]['cc_name'].")";
                        }else if($schedule_status=='F'){
                            $paid_str= "past due";
                            $pay_type ="";
                        }elseif($schedule_status=='M'){
                            $paid_str= "";
                            $pay_type="(manual credit applied)";
                        }
                        $message .= "Payment ".$x.": $wp_currency_symbol".$final_amt." ".$paid_str." ".$date." ".$pay_type." <br>";
                    }
                }
            }else{
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $schedule_status = $payment_details[0]['schedule_status'];
                $final_amount = $amount-$refunded_amount;
                $message .= "Total Due: $wp_currency_symbol".$final_amount."<br>Amount Paid: $wp_currency_symbol".$final_amount."(".$payment_details[0]['cc_name'].")<br>";
            }
            $message .= "<br><br>";
        }
        
        $waiver_present = 0;
        $file = '';
        if(!empty(trim($waiver_policies))){
            $waiver_present = 1;
            $file_name_initialize = 10;
            for($z=0; $z<$file_name_initialize; $z++){
                $dt = time();
                $file = __DIR__."/../uploads/".$dt."_waiver_policies.html";        
                if(file_exists($file)){
                    sleep(1);
                    $file_name_initialize++;
                }else{
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, urldecode($waiver_policies));
            fclose($ifp);
        }
        $send_to_studio = 0;
        $sendEmail_status = $this->sendEmailForEvent($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list, $student_cc_email_list, $send_to_studio);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            cjp_log_info($this->json($error_log));
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            cjp_log_info($this->json($error_log));
        }
    }
    
    private function sendOrderReceiptForMembershipPayment($membership_reg_id, $return_value){
       
        $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
        $payment_details = $prorate_flag_check = [];
        $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
        $recurring_start_date = $payment_start_date = $payment_frequency = $custom_recurring_frequency_period_type = $msg1 = $msg2 = $msg3 = '';
        $membership_structure = $billing_options = $billing_options_no_of_payments = $paid_str= $pay_type = '';
        $total_mem_fee = $registration_fee = 0;
        
        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, mp.`cc_name`, `waiver_policies`, mr.credit_card_name, 
            mr.`membership_start_date`, mr.`payment_start_date`,mr.`specific_end_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
            `buyer_postal_code`, mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, DATE(mp.`created_dt`) created_dt, mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`,mp.`prorate_flag`
            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id` LEFT JOIN `student` s ON s.`company_id`=mr.`company_id` AND s.`student_id`= mr.`student_id`
            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N')
            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){                
                while($row = mysqli_fetch_assoc($result)){
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
//                    $company_id = $row['company_id'];
//                    $membership_id = $row['membership_id'];
//                    $membership_option_id = $row['membership_option_id'];
                    $payment_type = $row['payment_type'];
                    $membership_category_title = $row['membership_category_title'];
                    $membership_title = $row['membership_title'];
                    $membership_structure = $row['membership_structure'];
                    $participant_name = $row['participant_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $signup_fee = $row['signup_fee'];
                    $signup_fee_disc = $row['signup_fee_disc'];
                    $mem_fee = $row['membership_fee'];
                    $mem_fee_disc = $row['membership_fee_disc'];
                    $initial_payment = $row['initial_payment'];
                    $inital_processing_fee = $row['initial_processing_fee'];
                    $membership_start_date = $row['membership_start_date'];
                    $payment_start_date = $row['payment_start_date'];
                    $recurring_start_date = $row['recurring_start_date'];
                    $next_payment_date = $row['next_payment_date'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $recurring_amount = $row['recurring_amount'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
                    $first_payment = $row['first_payment'];
                    $billing_options = $row['billing_options'];
                    $no_of_classes = $row['no_of_classes'];
                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $credit_card_name = $row['credit_card_name'];
                    $prorate_flag_check[] = $row['prorate_flag'];
                    $specific_end_date = $row['specific_end_date'];
                    $student_cc_email_list = $row['student_cc_email'];
                    $temp=[];
                    if(!empty($row['membership_payment_id'])){
                        if($recurring_amount == $row['payment_amount']){
                            $recurring_processing_fee = $row['processing_fee'];
                        }
                        if($row['processing_fee_type']==2){
                            if ($membership_structure != 'SE') {
                            $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                            } else if ($membership_structure == 'SE') {
                            $temp['amount'] = $row['payment_amount'];
                        }
                        }else{
                            $temp['amount'] = $row['payment_amount'];
                        }
                       $payment_date_temp = $temp['date'] = $row['payment_date'];
                        $temp['created_dt'] = $row['created_dt'];
                        $temp['processing_fee'] = $row['processing_fee'];
                        $temp['payment_status'] = $row['payment_status'];
                        $temp['paytime_type'] = $row['paytime_type'];
                        $temp['cc_name'] = $row['cc_name'];
                        $paid_amt += $temp['amount'];
                        $payment_details[] = $temp;
                    }
                }
                if($membership_structure=='SE'){
                    $registration_fee = number_format(($signup_fee-$signup_fee_disc),2);
                    for($i = 0;$i<count($payment_details);$i++){
                        $total_mem_fee += $payment_details[$i]['amount'];  
                    }
                        $total_mem_fee -= $registration_fee;
                }
                if(($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE') && $billing_options=='PP'){
                    if($billing_options_no_of_payments>1){
                        $already_recurring_records_count = 0;
                        $next_date = '';
                        if($membership_structure!=='SE'){
                        for($i=0;$i<count($payment_details);$i++){
                            if($payment_details[$i]['paytime_type']=='R'){
                                $already_recurring_records_count++;
                                $next_date = $payment_details[$i]['date'];
                            }
                        }
                        }
                        if($billing_options_no_of_payments>$already_recurring_records_count && $next_date !== ''){
                            $count = $billing_options_no_of_payments - $already_recurring_records_count;
                            $future_date = $next_date;
                            for($j=0;$j<$count;$j++){
                                if($payment_frequency=='W'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+1 weeks', strtotime($future_date)));
                                }elseif($payment_frequency=='B'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+14 days', strtotime($future_date)));
                                }elseif($payment_frequency=='M'){
                                    $next_payment_date2=$this->getSameDayNextMonth(new DateTime($future_date));
                                }
                                
                                $temp_1 =[];
                                if($processing_fee_type==2){
                                    if ($membership_structure != 'SE') {
                                    $temp_1['amount'] = $recurring_amount+$recurring_processing_fee;
                                    } elseif ($membership_structure == 'SE') {
                                        $temp_1['amount'] = $recurring_amount;
                                    }
                                }else{
                                    $temp_1['amount'] = $recurring_amount;
                                }
                                $temp_1['date'] = $next_payment_date2;
                                $temp_1['processing_fee'] = $recurring_processing_fee;
                                $temp_1['payment_status'] = 'N';
                                $temp_1['paytime_type'] = 'R';
                                $temp_1['cc_name'] = $credit_card_name;
                                $payment_details[] = $temp_1;
                                $future_date = $next_payment_date2;
                            }
                        }
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                cjp_log_info($this->json($error_log));
                return;
            }
        }
        
        $subject = $membership_category_title." Membership Order Confirmation";
        
        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
        $message .= "<b>Membership Details</b><br><br>";
        $message .= "Membership Start Date: ".date("M dS, Y", strtotime($membership_start_date))."<br>";
        if($membership_structure == 'SE'){
        $message .= "Membership End Date: ".date("M dS, Y", strtotime($specific_end_date))."<br>";
        $message .= "Registration Fee: $wp_currency_symbol".(number_format(($registration_fee),2))."<br>";
        $message .= "Total Membership Fee: $wp_currency_symbol".(number_format(($total_mem_fee),2))."<br>";
        }
        $message .= "Membership: $membership_category_title, $membership_title<br>Participant: ".$participant_name."<br>";
        if($membership_structure=='NC' && $billing_options=='PP' && $billing_options_expiration_date!='0000-00-00'){
            $message .= "$no_of_classes Class Package Expires: ".date("D, M d, Y", strtotime($billing_options_expiration_date))."<br>";
        }
        $message .= "<br>";
            
        $prorated = "";
        if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
            $prorated = "(Pro-rated)";
        }
        
        if($paid_amt>0){
//            if($initial_payment>0){
            if($membership_structure !== 'SE'){
                $message .= "<br><b>Registration Details</b><br><br>";
                $message .= "Sign-up Fee: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
            }
            else if($membership_structure == 'SE' && $billing_options=='PF'){
                $message .= "<br><b>Membership payment plan</b><br><br>";
//                $message .= "payment: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
//                $message .= " (plus $wp_currency_symbol".$inital_processing_fee." administrative fees)"."<br>";
            }   //check ravi
             if($membership_structure !== 'SE'){
                if($processing_fee_type==2){
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment+$inital_processing_fee),2))." (includes $wp_currency_symbol".$inital_processing_fee." administrative fees)<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }else{
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }
        }
        }
        if(count($payment_details)>0){
            if($payment_frequency!='N' && $payment_type=='R'){
                $freq_str = "";
                if($membership_structure=='OE' || $membership_structure==1){
                    if($payment_frequency=='B'){
                        $freq_str = "Semi-Monthly Recurring: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "Monthly Recurring: ";
                    }elseif($payment_frequency=='W'){
                        $freq_str = "Weekly Recurring: ";
                    }elseif($payment_frequency=='A'){
                        $freq_str = "Annual Recurring: ";
                    }elseif($payment_frequency=='C'){
                        if($custom_recurring_frequency_period_type=='CW'){
                            $freq_str = "Weekly Recurring: ";
                        }elseif($custom_recurring_frequency_period_type=='CM'){
                            $freq_str = "Monthly Recurring: ";
                        }
                    }
                }elseif($billing_options=='PP'){
                    if($payment_frequency=='W'){
                        $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
                    }elseif($payment_frequency=='B'){
                        $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
                    }
                }
                
                $message .= "<br><br><b>Membership Payment Plan</b><br><br>";
                $recur_pf_str = "";
                if ($membership_structure !== 'SE') {
                if($processing_fee_type==2){
                    $recur_pf_str = " (plus $wp_currency_symbol".$recurring_processing_fee." administrative fees)";
                }
                if($freq_str!=""){
                    $message .= "$freq_str $wp_currency_symbol".$recurring_amount.$recur_pf_str."<br>";
                }
//                $message .= "Next Payment Date: ".date("M dS, Y", strtotime($next_payment_date))."<br>";
                for ($init_success_payments = 0; $init_success_payments < count($payment_details); $init_success_payments++) {
                    if ($payment_details[$init_success_payments]['paytime_type'] == 'R' && $payment_details[$init_success_payments]['payment_status'] == 'N') {
                        $date_temp = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                        $message .= "Next Payment Date: " . date("M dS, Y", strtotime($date_temp)) . "<br>";
                        break;
                    }
                }
            }
            }
            if ($membership_structure !== 'SE') {
            $message.="<br><br><b>Payment Details</b><br><br>";
            }
            for($init_success_payments=0;$init_success_payments<count($payment_details);$init_success_payments++){
                $prorated_1 = $prorated_2 = $prorated_3 ="";
                $temp_se_date_check = date("d,m,Y,w",strtotime($payment_details[$init_success_payments]['date']));
                $se_check_prorate = explode(",",$temp_se_date_check);
                $pf_str = "";
                if ($processing_fee_type == 2){
                    if($membership_structure !== 'SE') {
                        $pf_str = " (includes $wp_currency_symbol".$payment_details[$init_success_payments]['processing_fee']." administrative fees)";
                    } else {
                        $pf_str = " (plus $wp_currency_symbol" . $payment_details[$init_success_payments]['processing_fee'] . " administrative fees)";
                    }
                }
                if($membership_structure !== 'SE'){
                    if($processing_fee_type==2){
                        $amount_temp=$payment_details[$init_success_payments]['amount']-$payment_details[$init_success_payments]['processing_fee'];
                    }else{
                        $amount_temp=$payment_details[$init_success_payments]['amount']; 
                    }
                if($payment_details[$init_success_payments]['paytime_type']=='I'){
                    if(($mem_fee - $mem_fee_disc)+ ($signup_fee - $signup_fee_disc) == ($amount_temp) || $signup_fee - $signup_fee_disc == ($amount_temp)){
                        $prorated_1 = "";
                    }else{
                        $prorated_1 = "(Pro-rated)";
                    }
                }elseif($payment_details[$init_success_payments]['paytime_type']=='F'){
                       if($mem_fee - $mem_fee_disc == ($amount_temp)){
                        $prorated_2 = "";
                    }else{
                        $prorated_2 = "(Pro-rated)";
                    } 
                }
                }else if($membership_structure == 'SE' && $billing_options == 'PP'){
                    if($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type']=='I'){
                         $prorated_1 = "(Pro-rated)";
                    }else if($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type']=='F') {
                         $prorated_2 = "(Pro-rated)";
                    }else if($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type']=='R'){
                         $prorated_3 = "(Pro-rated)";
                    }
                }
                    if($membership_structure !== 'SE'){    
                        if(!empty($prorated_1)){
                         $prorated_2 = "";
                    }
                    }
                if($initial_payment>0 && $payment_details[$init_success_payments]['paytime_type']=='I'){
                    $down_pay = $payment_details[$init_success_payments]['amount'].$pf_str." paid on ".date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']))." (".$payment_details[$init_success_payments]['cc_name'].") ".$prorated_1;
                    $msg1 .= "Payment: $wp_currency_symbol".$down_pay."<br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='F'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    if($payment_details[$init_success_payments]['payment_status']=='S'){
                        $paid_str = "paid on";
                        $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    }else{
                        $paid_str = "due";
                        if($membership_structure === 'SE'){
                         $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                        }else{
                        $pay_type= "";
                    }
                    }
                    if($membership_structure !== 'SE' || ($membership_structure == 'SE' && $billing_options == 'PP')){
                    $msg2 .= "First Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_2." <br>";
                    }else{
                    $msg2 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_2." <br>"; 
                    }
                }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='S'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "paid on";
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                        if($membership_structure !== 'SE'){
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                        }else{
                        $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_3." <br>";
                }
                    }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='N' && ($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE') && $billing_options=='PP'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "due";
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                        if($membership_structure !== 'SE'){
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                        }else{
                        $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_3." <br>";  
                }
            }
            }
            $message .= $msg1.$msg2.$msg3;
            $message .= "<br><br><br>";
        }
        
        $waiver_present = 0;
        $file = '';
        if(!empty(trim($waiver_policies))){
            $waiver_present = 1;
            $file_name_initialize = 10;
            for($z=0; $z<$file_name_initialize; $z++){
                $dt = time();
                $file = __DIR__."/../uploads/".$dt."_waiver_policies.html";        
                if(file_exists($file)){
                    sleep(1);
                    $file_name_initialize++;
                }else{
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }
        $send_to_studio = 0;
        $sendEmail_status = $this->sendEmailForMembership($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list, $student_cc_email_list, $send_to_studio);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            cjp_log_info($this->json($error_log));
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            cjp_log_info($this->json($error_log));
        }
    }
    
//   private function getProcessingFee($company_id, $upgrade_status) {
//        $temp=[];
//        $sql = sprintf("SELECT `processing_percentage`,`processing_transaction` FROM `wepay_processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE)", mysqli_real_escape_string($this->db, $company_id));
//        $result = mysqli_query($this->db, $sql);
//        if (!$result) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
//            cjp_log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 500);
//        } else {
//            $num_of_rows = mysqli_num_rows($result);
//            if ($num_of_rows > 0) {
//                while ($rows = mysqli_fetch_assoc($result)) {
//                    $temp['PP'] = $rows['processing_percentage'];
//                    $temp['PT'] = $rows['processing_transaction'];
//            }
//                return $temp;
//            } else {
//                $sql2 = sprintf("SELECT `processing_percentage`,`processing_transaction` FROM `wepay_processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, 0)) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE)", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status));
//                $result2 = mysqli_query($this->db, $sql2);
//                if (!$result2) {
//                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                    cjp_log_info($this->json($error_log));
//                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                    $this->response($this->json($error), 500);
//                } else {
//                    $num_of_rows = mysqli_num_rows($result2);
//                    if ($num_of_rows > 0) {
//                        while ($rows = mysqli_fetch_assoc($result2)) {
//                            $temp['PP'] = $rows['processing_percentage'];
//                            $temp['PT'] = $rows['processing_transaction'];
//                        }
//                        return $temp;
//                    }
//                }
//            }
//        }
//    }
    
    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }
    
    private function resendThrottlePayment($register_id, $payment_id, $process_date, $key, $type, $receipt_data){
        
//        $queueUrl = "https://sqs.us-east-1.amazonaws.com/241334827363/ms-bill-processing-dev";
        $result1 = $this->sqs->deleteMessage([
            'QueueUrl' => $this->sqs_queueurl, // REQUIRED
            'ReceiptHandle' => $receipt_data, // REQUIRED
        ]);
        
        $this->total_payment_records=[];
        if($type=="Event"){
            $sql= sprintf("UPDATE `event_payment` SET `start_time`=NULL WHERE `event_reg_id`='%s' and `event_payment_id`='%s'",mysqli_real_escape_string($this->db, $register_id),mysqli_real_escape_string($this->db, $payment_id));
        }else{
            $sql= sprintf("UPDATE `membership_payment` SET `start_time`=NULL WHERE `membership_registration_id`='%s' and `membership_payment_id`='%s'",mysqli_real_escape_string($this->db, $register_id),mysqli_real_escape_string($this->db, $payment_id));
        }
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            cjp_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } 
        $url=$this->server_url."/CronIPN/batchPaymentProcess.php?run=processPayment";
        $this->total_payment_records = array("category"=>"$type", "reg_id"=>$register_id, "payment_id"=>$payment_id, "payment_date"=>$process_date, "status"=>"U", "key"=>"$key","url"=>"$url");
//        $queueUrl = "https://sqs.us-east-1.amazonaws.com/241334827363/ms-bill-processing-dev";
        $result1 = $this->sqs->sendMessage(array(
            'QueueUrl' =>$this->sqs_queueurl,
            'MessageBody' => json_encode($this->total_payment_records),
            'DelaySeconds' => rand(60, 180),
        ));
        cjp_log_info($result1);
        exit();
     }
}

$api = new batchPaymentProcess;
$api->processApi();
?>