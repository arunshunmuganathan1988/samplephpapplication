<?php
$GLOBALS['HOST_NAME'] = '';
if(isset($argv[1]) && !empty($argv[1])){
    if($argv[1]=='prod'){
        $GLOBALS['HOST_NAME'] = 'https://www.mystudio.academy';
    }elseif($argv[1]=='dev_old'){
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }elseif($argv[1]=='dev_new'){
        $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
    }else{
        $GLOBALS['HOST_NAME'] = 'http://mystudio-dev.technogemsinc.com';
    }
}elseif(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
    $GLOBALS['HOST_NAME'] = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['HTTP_HOST'];
}else{
    $GLOBALS['HOST_NAME'] = 'http://dev.mystudio.academy';
}

require_once __DIR__.'/../Globals/cont_log.php';
require_once __DIR__.'/../Wepay2/WePay.php';
require_once __DIR__.'/../Wepay2/class.phpmailer.php';
require_once __DIR__.'/../Wepay2/class.smtp.php';
//require_once __DIR__.'/../aws/aws-autoloader.php';
require_once __DIR__.'/../aws/aws.phar';
require_once __DIR__."/../Globals/sqs_credentials.php";

use Aws\Sqs\SqsClient;

class RecurringPaymentHandler{
    
    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $sqs_key = '', $sqs_secret = '',$sqs_token='',$sqs_queueurl='';
    private $processing_fee_file_name;
    private $server_url, $cron_props_file, $cron_ev, $cron_ms;
    private $total_payments_list = [];
    
    public function __construct(){
        require_once __DIR__.'/../Globals/config.php';
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        $this->processing_fee_file_name = __DIR__.'/../Globals/processing_fee.props';
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        if(!empty($GLOBALS['HOST_NAME'])){
            $host = $GLOBALS['HOST_NAME'];
        }else{
            $host = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['HTTP_HOST'];
        }
        if(strpos($host, 'dev.mystudio.academy') !== false){
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = __DIR__."/../CronJob/cron_dev_props.php";
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'https://www.mystudio.academy';
            $this->cron_props_file = __DIR__."/../CronJob/cron_prod_props.php";
        }else{
            $this->server_url = "http://dev.mystudio.academy";
            $this->cron_props_file = __DIR__."/../CronJob/cron_dev_props.php";
        }
        $this->getUserandSecret();
    }
    
    private function getUserandSecret(){
        require_once "$this->cron_props_file";
        $this->cron_ev = EV_CRON;
        $this->cron_ms = MS_CRON;
        $this->sqs_key = $GLOBALS['sqs_keys']['key'];
        $this->sqs_secret = $GLOBALS['sqs_keys']['secret'];
        $this->sqs_queueurl = $GLOBALS['sqs_keys']['queueURL'];
        $this->sqs_token= $GLOBALS['sqs_token'];
    }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            cjp_log_info($this->json($error_log));
        }
    }
    
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function processApi(){
        
        if(isset($_GET['date']) && !empty($_GET['date'])) {
            $format = 'Y-m-d';
            $checkdate = $this->validateDate($_GET['date'], $format);
            if ($checkdate == true && $_GET['date'] < date("Y-m-d")) {
                $process_date = $_GET['date'];
                $password_check = "mystudio";
                $method_check = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password_check = substr(hash('sha256', $password_check, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv1 = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                $token = openssl_decrypt(base64_decode($_GET['key']), $method_check, $password_check, OPENSSL_RAW_DATA, $iv1);
                if ($token !== "$password_check.$process_date") {
                    echo "permission denied";
                      exit();
                }
            } else {
                echo 'permission denied or date format is invalid';
                exit();
            }
        } else {
            echo 'permission denied ';
            exit();
        }

      
        if($this->cron_ms=='Y'){
            $this->getMembershipPaymentsList($process_date);
        }
        if($this->cron_ev=='Y'){
            $this->getEventPaymentsList($process_date);
        }
        
        
        if(count($this->total_payments_list)>0){
            $this->sendPaymentsListToSQSQueue();
        }
    }
    
   private function validateDate($date, $format) {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    private function sendPaymentsListToSQSQueue(){
        cjp_log_info("sendPaymentsListToSQSQueue() - Started");
        $curr_date = gmdate("Y-m-d");
        $credentials = new Aws\Credentials\Credentials($this->sqs_key, $this->sqs_secret);
        $sqs = SqsClient::factory([
            'credentials' => $credentials,
            'region' => 'us-east-1',
            'version' => '2012-11-05'
        ]);
        //echo json_encode($sqs)."<br>";
//        $queueUrl = "https://sqs.us-east-1.amazonaws.com/241334827363/ms-bill-processing-dev";

        $total_count = count($this->total_payments_list);
        
        for($a=0;$a<$total_count;$a++){
           $this->total_payments_list[$a]['url']=$GLOBALS['HOST_NAME']."/CronIPN/batchPaymentProcess.php?run=processPayment";
           $data= $this->total_payments_list[$a];
           echo json_encode($data);
           $result = $sqs->sendMessage(array(
                'QueueUrl' =>   $this->sqs_queueurl,
                'MessageBody' => json_encode($data),
            ));
        }
//        $per_try = (int)($total_count/10);
//        if($total_count % 10 === 0){
//            $no_of_try = $per_try;
//        }else{
//            $no_of_try = $per_try+1;
//        }
//        $k=0;
//        echo " total_count ".$total_count." per try ".$per_try." no_of_try ".$no_of_try;
//        echo json_encode($this->total_payments_list);
//        for($i=0; $i<$no_of_try;$i++){
//            $entries = [];
//            $l=$k+10;
//            if($l>$total_count){
//                $l=$total_count;
//            }
//            for($j=$k;$j<$l;$j++){
//                $entries[] = array(
//                        "Id" =>(string) $curr_date."_".$j.$this->total_payments_list[$j]['category']."_".$this->total_payments_list[$j]['reg_id'] ,
//                        "MessageBody" =>  serialize($this->total_payments_list[$j])
//                    );
//            }
//            
//            $result = $sqs->sendMessageBatch(array(
//                    "QueueUrl" => $queueUrl,
//                    "Entries" => $entries
//                ));
//            
//            echo "$result<br>";
//            $k=($i+1)*10;
//        }
             
        cjp_log_info("sendPaymentsListToSQSQueue() - Completed");
    }
    
    private function getMembershipPaymentsList($process_date){
        cjp_log_info("getMembershipPaymentsList() - Started");
        $reg_id = $company_list = [];
        $curr_date = $process_date;
//        if(isset($_REQUEST['curr_date'])){
//            $curr_date = $_REQUEST['curr_date'];
//            $curr_date1 = "'".$_REQUEST['curr_date']."'";
//        }else{
//            $get_date = mysqli_query($this->db, "select CURDATE() curr_date, CURDATE()-INTERVAL 1 DAY prev_date;");
//            $row = mysqli_fetch_assoc($get_date);
//            $curr_date1 = "'".$row['curr_date']."','".$row['prev_date']."'";
//        }
        
        $query = sprintf("SELECT mr.registration_from,`access_token`, wp.`wp_user_state`, wp.`account_id`, wp.`currency`, wp.`account_state`, mr.`company_id`, c.`upgrade_status`, `membership_category_title`, `membership_title`, mr.`membership_registration_id`, mr.`membership_id`, mr.`membership_option_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `credit_card_id`, `credit_card_name`, `credit_card_status`, 
                mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`payment_type`, mr.`membership_status`, mr.`payment_frequency`, `membership_start_date`, `payment_start_date`, `next_payment_date` payment_date, mr.`membership_structure`, `first_payment`, `initial_payment`,
                mr.`custom_recurring_frequency_period_type`, mr.`custom_recurring_frequency_period_val`, `delay_recurring_payment_start_flg`, `delay_recurring_payment_start_date`, `delay_recurring_payment_amount`, `initial_payment_include_membership_fee`, 
                mr.`billing_options`, mr.`billing_options_no_of_payments`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`no_of_payments`, mp.`payment_amount`, mp.`membership_payment_id`, mp.`processing_fee`, mp.`paytime_type` record_type
                FROM `membership_payment` mp 
                LEFT JOIN `membership_registration` mr ON mr.`membership_registration_id`=mp.`membership_registration_id` AND mr.`company_id` = mp.`company_id`
                LEFT JOIN `wp_account` wp ON mr.`company_id` = wp.`company_id` LEFT JOIN `company` c ON mr.`company_id` = c.`company_id`
                WHERE mp.`payment_date` IN ('%s') AND mp.`payment_status`='N' AND mr.`membership_status`='R' AND 
                ((`delay_recurring_payment_start_flg`='Y' AND `delay_recurring_payment_amount`>=5 AND `delay_recurring_payment_start_date` IN ('%s') AND mp.`paytime_type`='F')
                OR (`initial_payment_include_membership_fee`='N' AND `first_payment`>=5 AND `payment_start_date` IN ('%s') AND mp.`paytime_type`='F') 
                OR (`next_payment_date` IN ('%s') AND mr.`payment_amount`>=5) AND mp.`paytime_type`='R')", mysqli_real_escape_string($this->db, $process_date), mysqli_real_escape_string($this->db, $process_date), mysqli_real_escape_string($this->db, $process_date), mysqli_real_escape_string($this->db, $process_date));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    
                    $company_id = $row['company_id'];
                    $access_token = $row['access_token'];
                    $acc_state = $row['account_state'];
                    $user_state = $row['wp_user_state'];
                    $upgrade_status = $row['upgrade_status'];
                    $buyer_email = $row['buyer_email'];
                    $cc_id = $row['credit_card_id'];
                    $cc_name = $row['credit_card_name'];
                    $membership_reg_id = $row['membership_registration_id'];
                    $membership_payment_id = $row['membership_payment_id'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $custom_recurring_frequency_period_val = $row['custom_recurring_frequency_period_val'];
                    $payment_amount = $row['payment_amount'];
                    $initial_payment = $row['initial_payment'];
                    $temp_amount = $payment_amount;
                    $payment_date = $row['payment_date'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $membership_structure = $row['membership_structure'];
                    $billing_options = $row['billing_options'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $no_of_payments = $row['no_of_payments'];
                    $record_type = $row['record_type'];
                    $p_f = $this->getProcessingFee($company_id, $upgrade_status);
                    $process_fee_per = $p_f['PP'];
                    $process_fee_val = $p_f['PT'];
                    $registration_from = $row['registration_from'];
                    $r1_fee = $temp_amount*($process_fee_per/100) + $process_fee_val;
                    $r2_fee = ($temp_amount + $r1_fee)*($process_fee_per/100) + $process_fee_val;
                    if($processing_fee_type==1){
                        $processing_fee = number_format((float)($r1_fee), 2, '.', '');
                    }else{
                        $processing_fee = number_format((float)($r2_fee), 2, '.', '');
                    }
                    $delay_recurring_payment_start_flg = $row['delay_recurring_payment_start_flg'];
                    $delay_recurring_payment_amount = $row['delay_recurring_payment_amount'];
                    $delay_recurring_payment_start_date = $row['delay_recurring_payment_start_date'];
                    $initial_payment_include_membership_fee = $row['initial_payment_include_membership_fee'];
                    $first_payment = $row['first_payment'];
                    $p_start_date = $row['payment_start_date'];
                    
                    if(!empty($access_token) && !empty($membership_reg_id)){
                        if(empty($membership_payment_id)){
                            cjp_log_info("Empty payment_id for reg_id($membership_reg_id), <br>Query : $query<br>");
                            $reg_id[] = $membership_reg_id;
                            continue;
                        }
                        $failure = 0;
                        if(($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE' ) && $billing_options=='PP'){
                            if($billing_options_deposit_amount>=5 || $initial_payment>=5){
                                if($billing_options_no_of_payments==($no_of_payments-1)){
                                    continue;
                                }
                            }else{
                                if($billing_options_no_of_payments==$no_of_payments){
                                    continue;
                                }
                            }
                        }
                        if($user_state=='deleted'){
                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. Wepay user registered was deleted.");
                            cjp_log_info("wepay_user: ".json_encode($log));
                            $failure=1;
                        }
                        if($acc_state=='deleted'){
                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                            cjp_log_info("wepay_account: ".json_encode($log));
                            $failure=1;
                        }elseif($acc_state=='disabled'){
                            $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                            cjp_log_info("wepay_account: ".json_encode($log));
                            $failure=1;
                        }
                        if($failure==0){
                            $send_date = $payment_date;
                            if($record_type=='F'){
                                if($delay_recurring_payment_start_flg=='Y' && $delay_recurring_payment_start_date==$process_date && $delay_recurring_payment_amount>=5){
                                    $send_date = $process_date;
                                }elseif($initial_payment_include_membership_fee=='N' && $first_payment >= 5 && $p_start_date == $process_date){
                                    $send_date = $process_date;
                                }
                            }
                            
                            $password1 = $this->sqs_token;
                            $method1 = 'aes-256-cbc';
                                    // Must be exact 32 chars (256 bit)
                            $password1 = substr(hash('sha256', $password1, true), 0, 32);
                                    // IV must be exact 16 chars (128 bit)
                            $iv1 = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                            $encrypted_access_token1 = base64_encode(openssl_encrypt("$password1.$send_date", $method1, $password1, OPENSSL_RAW_DATA, $iv1));
                            $this->total_payments_list[] = array("category"=>"Membership","payment_processsor"=>($registration_from == "W" ? "wepay" : "stripe"), "reg_id"=>$membership_reg_id, "payment_id"=>$membership_payment_id, "payment_date"=>$send_date, "status"=>"U","key"=>"$encrypted_access_token1");
                        }
                        if($failure==1){
                            $update_mem_payment = sprintf("UPDATE `membership_payment` SET `payment_status`='F', `processing_fee`='$processing_fee' WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
                                mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
                            $result_pay = mysqli_query($this->db, $update_mem_payment);

                            if(!$result_pay){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_mem_payment");
                                cjp_log_info($this->json($error_log));
                            }
                                                        
                            if($record_type=='F' || $record_type=='I'){
                                $reg_query = sprintf("UPDATE `membership_registration` SET `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
                                        mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                $reg_result = mysqli_query($this->db, $reg_query);
                                if(!$reg_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                    cjp_log_info($this->json($error_log));
                                }
                            }elseif($record_type=='R'){
                                if($membership_structure=='OE' || $membership_structure==1){
                                    $next_pay_date = $payment_start_date = date("Y-m-d", strtotime($payment_date));
                                    $payment_end_date = date("Y-m-d", strtotime("+93 days"));
                                    $recurring_payment_date_array = [];
                                    $iteration_end = 1;
                                    $payment_date = $next_pay_date;
                                    for ($i = 0; $i < $iteration_end; $i++) {
                                        if ($payment_frequency == 'W') {
                                            $next_pay_date = date('Y-m-d', strtotime('+1 weeks', strtotime($payment_date)));
                                        } elseif ($payment_frequency == 'B') {
                                                if (date('j', strtotime($payment_date)) < 16) {
                                                    $next_pay_date = date('Y-m-16', strtotime($payment_date));
                                                } else {
                                                    $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($payment_date)));
                                                }
                                        } elseif ($payment_frequency == 'M') {
                                            $next_pay_date=$this->getSameDayNextMonth(new DateTime($payment_date));
                                        } elseif ($payment_frequency == 'A') {
                                            $next_pay_date = date('Y-m-d', strtotime('+1 year', strtotime($payment_date)));
                                        } elseif ($payment_frequency == 'C') {
                                            if ($custom_recurring_frequency_period_type == 'CW') {
                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($payment_date)));
                                            } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($payment_date)));
                                            }
                                        }
                                        $payment_date = $next_pay_date;
                                        if ($next_pay_date <= $payment_end_date || count($recurring_payment_date_array)==0) {
                                            $iteration_end++;
                                            $recurring_payment_date_array[] = $next_pay_date;
                                        }
                                    }

                                    for ($j = 0; $j < count($recurring_payment_date_array); $j++) {
                                        $check_payment_record = sprintf("SELECT * FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s' AND `payment_date`='%s' AND `payment_status`='N'",
                                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), $recurring_payment_date_array[$j]);
                                        $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
                                        if (!$result_check_payment_record) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
                                            cjp_log_info($this->json($error_log));
                                        }else{
                                            $num_of_rows = mysqli_num_rows($result_check_payment_record);
                                            if($num_of_rows==0){
                                                $insert_mem_pay = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $processing_fee), 
                                                        mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                                $result_mem_pay = mysqli_query($this->db, $insert_mem_pay);
                                                if (!$result_mem_pay) {
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay");
                                                    cjp_log_info($this->json($error_log));
                                                }
                                            }
                                        }
                                    }
                                    
                                    if(count($recurring_payment_date_array)>0){
                                        $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $recurring_payment_date_array[0]), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                        $reg_result = mysqli_query($this->db, $reg_query);
                                        if(!$reg_result){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                            cjp_log_info($this->json($error_log));
                                        }
                                    }else{
                                        $error_log = array('status' => "Failed", "msg" => "Error in calculate Next payment date.", "query" => "$check_payment_record");
                                        cjp_log_info($this->json($error_log));
                                        $reg_id[]=$membership_reg_id;
                                    }                                    
                                }else{
                                    $check_payment_record = sprintf("SELECT *, IF(`payment_date`>'$curr_date','upcoming','past') status FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s'",
                                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
                                    $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
                                    if (!$result_check_payment_record) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
                                        cjp_log_info($this->json($error_log));
                                    }else{
                                        $num_of_rows = mysqli_num_rows($result_check_payment_record);
                                        $next_payment_date = $curr_date;
                                        if($num_of_rows>0){
                                            $check_recent = 0;
                                            while($row2 = mysqli_fetch_assoc($result_check_payment_record)){
                                                if($row2['status']=='upcoming' && $row2['payment_status']=='N' && $check_recent==0){
                                                    $next_payment_date = $row2['payment_date'];
                                                    $check_recent = 1;
                                                }
                                            }
                                            if($check_recent==0){
                                                if(($initial_payment==0 && $no_of_payments==$billing_options_no_of_payments) || ($initial_payment>0 && $billing_options_no_of_payments==$no_of_payments-1)){
                                                    $error_log = array('status' => "Failed", "msg" => "Completed but failed payment available.", "query" => "$check_payment_record");
                                                    cjp_log_info($this->json($error_log));
                                                    $reg_id[]=$membership_reg_id;
                                                }
                                            }else{
                                                $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
                                                        mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                                                $reg_result = mysqli_query($this->db, $reg_query);
                                                if(!$reg_result){
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                                                    cjp_log_info($this->json($error_log));
                                                }
                                            }
                                        }else{
                                            $error_log = array('status' => "Failed", "msg" => "No records found.", "query" => "$check_payment_record");
                                            cjp_log_info($this->json($error_log));
                                        }
                                    }
                                }                                
                            }else{
                                cjp_log_info("reg_id($membership_reg_id), <br>Query : $query<br>");
                                $reg_id[]=$membership_reg_id;
                            }
                            
                            $this->sendFailedOrderReceiptForMembershipPayment($membership_reg_id,0);
                            continue;
                        }
                    }else{
                        if(!in_array($company_id, $company_list) && empty($access_token)){
                            $error_log = array('status' => "Failed", "msg" => "Access token is empty for company(Id = $company_id). Cannot be accept payments anymore.");
                            cjp_log_info($this->json($error_log));
                            $company_list[] = $company_id;
                        }
                        
                        $update_mem_payment = sprintf("UPDATE `membership_payment` SET `payment_status`='F' WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
                                mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
                        $result_pay = mysqli_query($this->db, $update_mem_payment);

                        if(!$result_pay){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_mem_payment");
                            cjp_log_info($this->json($error_log));
                        }

                        $reg_query = sprintf("UPDATE `membership_registration` SET `preceding_payment_date`='%s',`preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
                                mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
                        $reg_result = mysqli_query($this->db, $reg_query);
                        if(!$reg_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
                            cjp_log_info($this->json($error_log));
                        }
                        
                        $this->sendFailedOrderReceiptForMembershipPayment($membership_reg_id,0);
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No Membership scheduled payment available for today($curr_date).");
                cjp_log_info($this->json($error_log));
            }
        }
        cjp_log_info("getMembershipPaymentsList() - Completed");
    }
    
    private function getEventPaymentsList($process_date){
        cjp_log_info("runCronJobForEventPreapproval() - Started");
        $curr_date = $process_date;
        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
        $total_records_processed=0;
        
        $query = sprintf("SELECT er.registration_from,`access_token`, wp.`wp_user_state`, wp.`account_id`, wp.`currency`, wp.`account_state`, wp.`account_id`, er.`company_id`, c.`upgrade_status`, er.`student_id`, e.`event_title`, er.`event_reg_id`, `event_payment_id`, er.`event_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, 
                `registration_amount`, ep.`processing_fee`, `payment_type`, er.`payment_frequency`, er.`payment_amount` total_due, `paid_amount`, `credit_card_id`, `credit_card_name`, `credit_card_status`, `total_order_amount`, `total_order_quantity`, 
                ep.`payment_amount`, ep.`schedule_date`, ep.`schedule_status`, er.`processing_fee_type`
                FROM `event_registration` er 
                LEFT JOIN `wp_account` wp ON er.`company_id` = wp.`company_id` 
                LEFT JOIN `company` c ON er.`company_id` = c.`company_id`
                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` AND ep.`schedule_status`='N'
                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
                WHERE ep.`schedule_date`='%s' AND `payment_type`='RE' AND ep.`payment_amount`>0 ORDER BY er.`company_id`", mysqli_real_escape_string($this->db, $process_date));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $total_records_processed += $num_rows;
                $company_list = [];
                while($row = mysqli_fetch_assoc($result)){
                    $company_id = $row['company_id'];
                    $registration_from = $row['registration_from'];
                    $access_token = $row['access_token'];
                    $acc_state = $row['account_state'];
                    $user_state = $row['wp_user_state'];
                    $event_reg_id = $row['event_reg_id'];
                    $event_payment_id = $row['event_payment_id'];
                    $payment_date = $row['schedule_date'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $buyer_postal_code = $row['buyer_postal_code'];
                    $cc_id = $row['credit_card_id'];
                    $cc_name = $row['credit_card_name'];
                    if(!empty($access_token)){
                        $failure = 0;
                        
                        if($user_state=='deleted'){
                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. Wepay user registered was deleted.");
                            cjp_log_info("wepay_user: ".json_encode($log));
                            $failure=1;
                        }
                        if($acc_state=='deleted'){
                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                            cjp_log_info("wepay_account: ".json_encode($log));
                            $failure=1;
                        }elseif($acc_state=='disabled'){
                            $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                            cjp_log_info("wepay_account: ".json_encode($log));
                            $failure=1;
                        }
                        if($failure==0){
                            $password1 = $this->sqs_token;
                            $method1 = 'aes-256-cbc';
                                    // Must be exact 32 chars (256 bit)
                            $password1 = substr(hash('sha256', $password1, true), 0, 32);
                                    // IV must be exact 16 chars (128 bit)
                            $iv1 = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                            $encrypted_access_token1 = base64_encode(openssl_encrypt("$password1.$payment_date", $method1, $password1, OPENSSL_RAW_DATA, $iv1));
                            $this->total_payments_list[] = array("category"=>"Event","payment_processsor"=>($registration_from == "W" ? "wepay" : "stripe"), "reg_id"=>$event_reg_id, "payment_id"=>$event_payment_id, "payment_date"=>$payment_date, "status"=>"U","key"=>"$encrypted_access_token1");
                        }
                        if($failure==1){
                            $payment_query_5 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $event_payment_id));
                            $payment_result = mysqli_query($this->db, $payment_query_5);
                            if(!$payment_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query_5");
                                cjp_log_info($this->json($error_log));
                            }
                            $this->sendFailedOrderReceiptForEventPayment($event_reg_id,0);
                            continue;
                        }

                    }else{
                        if(!in_array($company_id, $company_list)){
                            $error_log = array('status' => "Failed", "msg" => "Access token is empty for company(Id = $company_id). Cannot be accept payments anymore.");
                            cjp_log_info($this->json($error_log));
                            $company_list[] = $company_id;
                        }
                        $payment_query_8 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $event_payment_id));
                        $payment_result = mysqli_query($this->db, $payment_query_8);
                        if(!$payment_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query_8");
                            cjp_log_info($this->json($error_log));
                        }
                        $this->sendFailedOrderReceiptForEventPayment($event_reg_id,0);
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No Event scheduled payment available for today($curr_date).");
                cjp_log_info($this->json($error_log));
            }
        }
        cjp_log_info("runCronJobForEventPreapproval() - Completed");
    }
    
    //Cron Job for Membership recurring payment
//    private function runCronJobForMembershipPayment(){
//        $total_records_processed = 0;
//        $start_time = gmdate("Y-m-d H:i:s");
//        cjp_log_info("runCronJobForMembershipPayment() - Started");
//        $reg_id = [];
//        $curr_date = gmdate("Y-m-d");
//        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
////        $throttle_count = 0;
//        if(isset($_REQUEST['curr_date'])){
//            $curr_date = $_REQUEST['curr_date'];
//            $curr_date1 = "'".$_REQUEST['curr_date']."'";
//        }else{
//            $get_date = mysqli_query($this->db, "select CURDATE() curr_date, CURDATE()-INTERVAL 1 DAY prev_date;");
//            $row = mysqli_fetch_assoc($get_date);
//            $curr_date1 = "'".$row['curr_date']."','".$row['prev_date']."'";
//        }
//        
//        $query = sprintf("SELECT `access_token`, wp.`wp_user_state`, wp.`account_id`, wp.`currency`, wp.`account_state`, mr.`company_id`, c.`upgrade_status`, `membership_category_title`, `membership_title`, mr.`membership_registration_id`, mr.`membership_id`, mr.`membership_option_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `credit_card_id`, `credit_card_name`, `credit_card_status`, 
//                mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`payment_type`, mr.`membership_status`, mr.`payment_frequency`, `membership_start_date`, `payment_start_date`, `next_payment_date` payment_date, mr.`membership_structure`, `first_payment`, `initial_payment`,
//                mr.`custom_recurring_frequency_period_type`, mr.`custom_recurring_frequency_period_val`, `delay_recurring_payment_start_flg`, `delay_recurring_payment_start_date`, `delay_recurring_payment_amount`, `initial_payment_include_membership_fee`, 
//                mr.`billing_options`, mr.`billing_options_no_of_payments`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`no_of_payments`, mp.`payment_amount`, mp.`membership_payment_id`, mp.`processing_fee`, mp.`paytime_type` record_type
//                FROM `membership_payment` mp 
//                LEFT JOIN `membership_registration` mr ON mr.`membership_registration_id`=mp.`membership_registration_id` AND mr.`company_id` = mp.`company_id`
//                LEFT JOIN `wp_account` wp ON mr.`company_id` = wp.`company_id` LEFT JOIN `company` c ON mr.`company_id` = c.`company_id`
//                WHERE mp.`payment_date` IN (%s) AND mp.`payment_status`='N' AND mr.`membership_status`='R' AND 
//                ((`delay_recurring_payment_start_flg`='Y' AND `delay_recurring_payment_amount`>=5 AND `delay_recurring_payment_start_date` IN (%s) AND mp.`paytime_type`='F')
//                OR (`initial_payment_include_membership_fee`='N' AND `first_payment`>=5 AND `payment_start_date` IN (%s) AND mp.`paytime_type`='F') 
//                OR (`next_payment_date` IN (%s) AND mr.`payment_amount`>=5) AND mp.`paytime_type`='R')", $curr_date1, $curr_date1, $curr_date1, $curr_date1);
//        $result = mysqli_query($this->db, $query);
////        echo "Q - $query";
////        exit();
//        if(!$result){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
//            cjp_log_info($this->json($error_log));
//        }else{
//            $num_rows = mysqli_num_rows($result);
//            if($num_rows>0){
//                $total_records_processed += $num_rows;
//                $company_list = [];
//                while($row = mysqli_fetch_assoc($result)){
//                    $company_id = $row['company_id'];
//                    $access_token = $row['access_token'];
//                    $account_id = $row['account_id'];
//                    $acc_state = $row['account_state'];
//                    $user_state = $row['wp_user_state'];
//                    $currency = $row['currency'];
//                    $upgrade_status = $row['upgrade_status'];
//                    $buyer_name = $row['buyer_name'];
//                    $buyer_email = $row['buyer_email'];
//                    $buyer_phone = $row['buyer_phone'];
//                    $buyer_postal_code = $row['buyer_postal_code'];
//                    $cc_id = $row['credit_card_id'];
//                    $cc_name = $row['credit_card_name'];
//                    $membership_reg_id = $row['membership_registration_id'];
//                    $membership_payment_id = $row['membership_payment_id'];
//                    $membership_id = $row['membership_id'];
//                    $membership_option_id = $row['membership_option_id'];
//                    $membership_title = $desc = $row['membership_title'];
//                    $membership_category_title=$row['membership_category_title'];
//                    $membership_status = $row['membership_status'];
//                    $payment_type = $row['payment_type'];
//                    $payment_frequency = $row['payment_frequency'];
//                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
//                    $custom_recurring_frequency_period_val = $row['custom_recurring_frequency_period_val'];
//                    $delay_recurring_payment_start_flg = $row['delay_recurring_payment_start_flg'];
//                    $delay_recurring_payment_start_flg = $row['delay_recurring_payment_start_date'];
//                    $delay_recurring_payment_amount = $row['delay_recurring_payment_amount'];
//                    $recurring_amount = $row['recurring_amount'];
//                    $payment_amount = $row['payment_amount'];
//                    $initial_payment = $row['initial_payment'];
//                    $temp_amount = $payment_amount;
//                    $payment_date = $row['payment_date'];
//                    $processing_fee_type = $row['processing_fee_type'];
////                    $processing_fee = $row['processing_fee'];
//                    $membership_structure = $row['membership_structure'];
//                    $billing_options = $row['billing_options'];
//                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
//                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
//                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
//                    $no_of_payments = $row['no_of_payments'];
//                    $record_type = $row['record_type'];
//                    $gmt_date=gmdate(DATE_RFC822);
//                    $sns_msg=array("buyer_email"=>$buyer_email,"membership_title"=>$membership_title,"gmt_date"=>$gmt_date);
//                    $p_f = $this->getProcessingFee($company_id, $upgrade_status);
//                    $process_fee_per = $p_f['PP'];
//                    $process_fee_val = $p_f['PT'];
//                    $r1_fee = $temp_amount*($process_fee_per/100) + $process_fee_val;
//                    $r2_fee = ($temp_amount + $r1_fee)*($process_fee_per/100) + $process_fee_val;
//                    if($processing_fee_type==1){
//                        $processing_fee = number_format((float)($r1_fee), 2, '.', '');
//                    }else{
//                        $processing_fee = number_format((float)($r2_fee), 2, '.', '');
//                    }
//                    if(!empty($access_token) && !empty($membership_reg_id)){
//                        if(empty($membership_payment_id)){
//                            cjp_log_info("Empty payment_id for reg_id($membership_reg_id), <br>Query : $query<br>");
//                            $reg_id[] = $membership_reg_id;
//                            continue;
//                        }
//                        $failure = $success = 0;
//                        if(($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE' ) && $billing_options=='PP'){
//                            if($billing_options_deposit_amount>=5 || $initial_payment>=5){
//                                if($billing_options_no_of_payments==($no_of_payments-1)){
//                                    continue;
//                                }
//                            }else{
//                                if($billing_options_no_of_payments==$no_of_payments){
//                                    continue;
//                                }
//                            }
//                        }
//                        
//                        $user_agent = "mystudio.academy";
//                        $password = $this->wp_pswd;
//                        $method = 'aes-256-cbc';
//                        // Must be exact 32 chars (256 bit)
//                        $password = substr(hash('sha256', $password, true), 0, 32);
//                        // IV must be exact 16 chars (128 bit)
//                        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
//
//                        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
//                        
//                        if($user_state=='deleted'){
//                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. Wepay user registered was deleted.");
//                            cjp_log_info("wepay_user: ".json_encode($log));
//                            $failure=1;
//                        }
//                        if($acc_state=='deleted'){
//                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
//                            cjp_log_info("wepay_account: ".json_encode($log));
//                            $failure=1;
//                        }elseif($acc_state=='disabled'){
//                            $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
//                            cjp_log_info("wepay_account: ".json_encode($log));
//                            $failure=1;
//                        }
//                        
//                        if($failure==0){
//                            $rand = mt_rand(1,99999);
//                            $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout?for=membership";
//                            $unique_id = $company_id."_".$membership_id."_".$membership_option_id."_".$rand."_".time();
//                            $date = gmdate("Y-m-d_H:i:s");
//                            $ref_id = $company_id."_".$membership_id."_".$membership_option_id."_".$date;
//                            $fee_payer = "payee_from_app";
//                            $time = time();
//
//                            if($processing_fee_type==2){
//                                $w_paid_amount = $payment_amount + $processing_fee;
//                                $paid_amount = $payment_amount;
//                            }elseif($processing_fee_type==1){
//                                $w_paid_amount = $payment_amount;
//                                $paid_amount = $payment_amount - $processing_fee;
//                            }
//                            $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
//                                "fee"=>array("app_fee"=>$processing_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
//                                "email_message"=>array("to_payee"=>"Payment has been added for $membership_title","to_payer"=>"Payment has been made for $membership_title"),
//                                "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
//                                "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
//                                    array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
//                                    array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))),
//                                "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
//                                    "properties"=>array("itemized_receipt"=>array(array("description"=>"$membership_category_title", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
//                            $postData2 = json_encode($json2);
//                            $sns_msg['call'] = "Checkout";
//                            $sns_msg['type'] = "Checkout Creation";
//                            $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg);
//                            cjp_log_info("wepay_checkout_create: ".$response2['status']);
//                            if($response2['status']=="Success"){
//                                $res2 = $response2['msg'];
//                                $checkout_id = $res2['checkout_id'];
//                                $checkout_state = $res2['state'];
//                                $success=1;
//                            }else{
//                                if($response2['msg']['error_code']==1008){
//                                    $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg);
//                                    cjp_log_info("wepay_checkout_create: ".$response2['status']);
//                                    if($response2['status']=="Success"){
//                                        $res2 = $response2['msg'];
//                                        $checkout_id = $res2['checkout_id'];
//                                        $checkout_state = $res2['state'];
//                                        $success=1;
//                                    }else{
//                                        if($response2['msg']['error_code']==1008){
//                                            $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
//                                            cjp_log_info($this->json($error));
//                                        }else{
//                                            $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                                            cjp_log_info($this->json($error));
//                                        }
//                                        $failure=1;
//                                    }
//                                }else{
//                                    $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                                    cjp_log_info($this->json($error));
//                                    $failure=1;
//                                }
//                            }
//                        }
//                        
//                        if($failure==1){
//                            $update_mem_payment = sprintf("UPDATE `membership_payment` SET `payment_status`='F', `processing_fee`='$processing_fee' WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
//                                mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
//                            $result_pay = mysqli_query($this->db, $update_mem_payment);
//
//                            if(!$result_pay){
//                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_mem_payment");
//                                cjp_log_info($this->json($error_log));
//                            }
//                                                        
//                            if($record_type=='F' || $record_type=='I'){
//                                $reg_query = sprintf("UPDATE `membership_registration` SET `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
//                                        mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
//                                $reg_result = mysqli_query($this->db, $reg_query);
//                                if(!$reg_result){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
//                                    cjp_log_info($this->json($error_log));
//                                }
//                            }elseif($record_type=='R'){
//                                if($membership_structure=='OE' || $membership_structure==1){
//                                    $next_pay_date = $payment_start_date = date("Y-m-d", strtotime($payment_date));
//                                    $payment_end_date = date("Y-m-d", strtotime("+93 days"));
//                                    $recurring_payment_date_array = [];
//                                    $iteration_end = 1;
//                                    $payment_date = $next_pay_date;
//                                    for ($i = 0; $i < $iteration_end; $i++) {
//                                        if ($payment_frequency == 'W') {
//                                            $next_pay_date = date('Y-m-d', strtotime('+1 weeks', strtotime($payment_date)));
//                                        } elseif ($payment_frequency == 'B') {
//                                                if (date('j', strtotime($payment_date)) < 16) {
//                                                    $next_pay_date = date('Y-m-16', strtotime($payment_date));
//                                                } else {
//                                                    $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($payment_date)));
//                                                }
//                                        } elseif ($payment_frequency == 'M') {
//                                            $next_pay_date=$this->getSameDayNextMonth(new DateTime($payment_date));
//                                        } elseif ($payment_frequency == 'A') {
//                                            $next_pay_date = date('Y-m-d', strtotime('+1 year', strtotime($payment_date)));
//                                        } elseif ($payment_frequency == 'C') {
//                                            if ($custom_recurring_frequency_period_type == 'CW') {
//                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($payment_date)));
//                                            } elseif ($custom_recurring_frequency_period_type == 'CM') {
//                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($payment_date)));
//                                            }
//                                        }
//                                        $payment_date = $next_pay_date;
//                                        if ($next_pay_date <= $payment_end_date || count($recurring_payment_date_array)==0) {
//                                            $iteration_end++;
//                                            $recurring_payment_date_array[] = $next_pay_date;
//                                        }
//                                    }
//
//                                    for ($j = 0; $j < count($recurring_payment_date_array); $j++) {
//                                        $check_payment_record = sprintf("SELECT * FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s' AND `payment_date`='%s' AND `payment_status`='N'",
//                                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), $recurring_payment_date_array[$j]);
//                                        $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
//                                        if (!$result_check_payment_record) {
//                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
//                                            cjp_log_info($this->json($error_log));
//                                        }else{
//                                            $num_of_rows = mysqli_num_rows($result_check_payment_record);
//                                            if($num_of_rows==0){
//                                                $insert_mem_pay = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
//                                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $processing_fee), 
//                                                        mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
//                                                $result_mem_pay = mysqli_query($this->db, $insert_mem_pay);
//                                                if (!$result_mem_pay) {
//                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay");
//                                                    cjp_log_info($this->json($error_log));
//                                                }
//                                            }
//                                        }
//                                    }
//                                    
//                                    if(count($recurring_payment_date_array)>0){
//                                        $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
//                                                mysqli_real_escape_string($this->db, $recurring_payment_date_array[0]), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
//                                        $reg_result = mysqli_query($this->db, $reg_query);
//                                        if(!$reg_result){
//                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
//                                            cjp_log_info($this->json($error_log));
//                                        }
//                                    }else{
//                                        $error_log = array('status' => "Failed", "msg" => "Error in calculate Next payment date.", "query" => "$check_payment_record");
//                                        cjp_log_info($this->json($error_log));
//                                        $reg_id[]=$membership_reg_id;
//                                    }                                    
//                                }else{
//                                    $check_payment_record = sprintf("SELECT *, IF(`payment_date`>'$curr_date','upcoming','past') status FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s'",
//                                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
//                                    $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
//                                    if (!$result_check_payment_record) {
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
//                                        cjp_log_info($this->json($error_log));
//                                    }else{
//                                        $num_of_rows = mysqli_num_rows($result_check_payment_record);
//                                        $next_payment_date = $curr_date;
//                                        if($num_of_rows>0){
//                                            $check_recent = 0;
//                                            while($row2 = mysqli_fetch_assoc($result_check_payment_record)){
//                                                if($row2['status']=='upcoming' && $row2['payment_status']=='N' && $check_recent==0){
//                                                    $next_payment_date = $row2['payment_date'];
//                                                    $check_recent = 1;
//                                                }
//                                            }
//                                            if($check_recent==0){
//                                                if(($initial_payment==0 && $no_of_payments==$billing_options_no_of_payments) || ($initial_payment>0 && $billing_options_no_of_payments==$no_of_payments-1)){
//                                                    $error_log = array('status' => "Failed", "msg" => "Completed but failed payment available.", "query" => "$check_payment_record");
//                                                    cjp_log_info($this->json($error_log));
//                                                    $reg_id[]=$membership_reg_id;
//                                                }
//                                            }else{
//                                                $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
//                                                        mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
//                                                $reg_result = mysqli_query($this->db, $reg_query);
//                                                if(!$reg_result){
//                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
//                                                    cjp_log_info($this->json($error_log));
//                                                }
//                                            }
//                                        }else{
//                                            $error_log = array('status' => "Failed", "msg" => "No records found.", "query" => "$check_payment_record");
//                                            cjp_log_info($this->json($error_log));
//                                        }
//                                    }
//                                }                                
//                            }else{
//                                cjp_log_info("reg_id($membership_reg_id), <br>Query : $query<br>");
//                                $reg_id[]=$membership_reg_id;
//                            }
//                            
//                            $this->sendFailedOrderReceiptForMembershipPayment($membership_reg_id,0);
//                            continue;
//                        }
//                        
//                        if($success==1){
//                            $update_mem_payment = sprintf("UPDATE `membership_payment` SET `checkout_id`='%s', `checkout_status`='%s', `payment_status`='S', `cc_id`='%s', `cc_name`='%s', `processing_fee`='$processing_fee' WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
//                                    mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name),
//                                    mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
//                            $result_pay = mysqli_query($this->db, $update_mem_payment);
//
//                            if(!$result_pay){
//                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_mem_payment");
//                                cjp_log_info($this->json($error_log));
//                            }
//                            
//                            if($record_type=='F' || $record_type=='I'){
//                                $reg_query = sprintf("UPDATE `membership_registration` SET `no_of_payments`=`no_of_payments`+1, `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
//                                        mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
//                                $reg_result = mysqli_query($this->db, $reg_query);
//                                if(!$reg_result){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
//                                    cjp_log_info($this->json($error_log));
//                                }
//                            }elseif($record_type=='R'){
//                                if($membership_structure=='OE' || $membership_structure==1){
//                                    $next_pay_date = $payment_start_date = date("Y-m-d", strtotime($payment_date));
//                                    $payment_end_date = date("Y-m-d", strtotime("+93 days"));
//                                    $recurring_payment_date_array = [];
//                                    $iteration_end = 1;
//                                    $payment_date = $next_pay_date;
//                                    for ($i = 0; $i < $iteration_end; $i++) {
//                                        if ($payment_frequency == 'W') {
//                                            $next_pay_date = date('Y-m-d', strtotime('+1 weeks', strtotime($payment_date)));
//                                        } elseif ($payment_frequency == 'B') {
//                                                if (date('j', strtotime($payment_date)) < 16) {
//                                                    $next_pay_date = date('Y-m-16', strtotime($payment_date));
//                                                } else {
//                                                    $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($payment_date)));
//                                                }
//                                        } elseif ($payment_frequency == 'M') {
//                                            $next_pay_date=$this->getSameDayNextMonth(new DateTime($payment_date));
//                                        } elseif ($payment_frequency == 'A') {
//                                            $next_pay_date = date('Y-m-d', strtotime('+1 year', strtotime($payment_date)));
//                                        } elseif ($payment_frequency == 'C') {
//                                            if ($custom_recurring_frequency_period_type == 'CW') {
//                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($payment_date)));
//                                            } elseif ($custom_recurring_frequency_period_type == 'CM') {
//                                                $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($payment_date)));
//                                            }
//                                        }
//                                        $payment_date = $next_pay_date;
//                                        if ($next_pay_date <= $payment_end_date || count($recurring_payment_date_array)==0) {
//                                            $iteration_end++;
//                                            $recurring_payment_date_array[] = $next_pay_date;
//                                        }
//                                    }
//
//                                    for ($j = 0; $j < count($recurring_payment_date_array); $j++) {
//                                        $check_payment_record = sprintf("SELECT * FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s' AND `payment_date`='%s' AND `payment_status`='N'",
//                                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), $recurring_payment_date_array[$j]);
//                                        $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
//                                        if (!$result_check_payment_record) {
//                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
//                                            cjp_log_info($this->json($error_log));
//                                        }else{
//                                            $num_of_rows = mysqli_num_rows($result_check_payment_record);
//                                            if($num_of_rows==0){
//                                                $insert_mem_pay = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
//                                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $processing_fee), 
//                                                        mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
//                                                $result_mem_pay = mysqli_query($this->db, $insert_mem_pay);
//                                                if (!$result_mem_pay) {
//                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay");
//                                                    cjp_log_info($this->json($error_log));
//                                                }
//                                            }
//                                        }
//                                    }
//                                    if(count($recurring_payment_date_array)>0){
//                                        $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
//                                                mysqli_real_escape_string($this->db, $recurring_payment_date_array[0]), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
//                                        $reg_result = mysqli_query($this->db, $reg_query);
//                                        if(!$reg_result){
//                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
//                                            cjp_log_info($this->json($error_log));
//                                        }
//                                    }else{
//                                        $error_log = array('status' => "Failed", "msg" => "Error in calculate Next payment date.", "query" => "$check_payment_record");
//                                        cjp_log_info($this->json($error_log));
//                                        $reg_id[]=$membership_reg_id;
//                                    }                                    
//                                }else{
//                                    $check_payment_record = sprintf("SELECT *, IF(`payment_date`>'$curr_date','upcoming','past') status FROM `membership_payment` WHERE `company_id`='%s' AND `membership_registration_id`='%s'",
//                                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
//                                    $result_check_payment_record = mysqli_query($this->db, $check_payment_record);
//                                    if (!$result_check_payment_record) {
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_payment_record");
//                                        cjp_log_info($this->json($error_log));
//                                    }else{
//                                        $num_of_rows = mysqli_num_rows($result_check_payment_record);
//                                        $next_payment_date = $curr_date;
//                                        if($num_of_rows>0){
//                                            $check_recent = 0;
//                                            while($row2 = mysqli_fetch_assoc($result_check_payment_record)){
//                                                if($row2['status']=='upcoming' && $row2['payment_status']=='N' && $check_recent==0){
//                                                    $next_payment_date = $row2['payment_date'];
//                                                    $check_recent = 1;
//                                                }
//                                            }
//                                            if($check_recent==0){
//                                                if(($initial_payment==0 && $no_of_payments==$billing_options_no_of_payments) || ($initial_payment>0 && $billing_options_no_of_payments==$no_of_payments-1)){
//                                                    $reg_query = sprintf("UPDATE `membership_registration` SET `membership_status`='S', `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
//                                                        mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
//                                                    $reg_result = mysqli_query($this->db, $reg_query);
//                                                    if(!$reg_result){
//                                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
//                                                        cjp_log_info($this->json($error_log));
//                                                    }
//                                                }else{
//                                                    $error_log = array('status' => "Failed", "msg" => "No records found.", "query" => "$check_payment_record");
//                                                    cjp_log_info($this->json($error_log));
//                                                }
//                                            }else{
//                                                $reg_query = sprintf("UPDATE `membership_registration` SET `next_payment_date`='%s', `preceding_payment_date`='%s', `preceding_payment_status`='S' WHERE `membership_registration_id`='%s'", 
//                                                        mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
//                                                $reg_result = mysqli_query($this->db, $reg_query);
//                                                if(!$reg_result){
//                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
//                                                    cjp_log_info($this->json($error_log));
//                                                }
//                                            }
//                                        }else{
//                                            $error_log = array('status' => "Failed", "msg" => "No records found.", "query" => "$check_payment_record");
//                                            cjp_log_info($this->json($error_log));
//                                        }
//                                    }
//                                }                                
//                            }else{
//                                cjp_log_info("reg_id($membership_reg_id), <br>Query : $query<br>");
//                                $reg_id[]=$membership_reg_id;
//                            }
//                            $this->sendOrderReceiptForMembershipPayment($membership_reg_id, 0);
//                        }
//                    }else{
//                        if(!in_array($company_id, $company_list) && empty($access_token)){
//                            $error_log = array('status' => "Failed", "msg" => "Access token is empty for company(Id = $company_id). Cannot be accept payments anymore.");
//                            cjp_log_info($this->json($error_log));
//                            $company_list[] = $company_id;
//                        }
//                        
//                        $update_mem_payment = sprintf("UPDATE `membership_payment` SET `payment_status`='F' WHERE `membership_registration_id`='%s' AND `membership_payment_id`='%s'",
//                                mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $membership_payment_id));
//                        $result_pay = mysqli_query($this->db, $update_mem_payment);
//
//                        if(!$result_pay){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_mem_payment");
//                            cjp_log_info($this->json($error_log));
//                        }
//
//                        $reg_query = sprintf("UPDATE `membership_registration` SET `preceding_payment_date`='%s',`preceding_payment_status`='F' WHERE `membership_registration_id`='%s'", 
//                                mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, $membership_reg_id));
//                        $reg_result = mysqli_query($this->db, $reg_query);
//                        if(!$reg_result){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_query");
//                            cjp_log_info($this->json($error_log));
//                        }
//                        
//                        $this->sendFailedOrderReceiptForMembershipPayment($membership_reg_id,0);
//                    }
//                }
//            }else{
//                $error_log = array('status' => "Failed", "msg" => "No scheduled payment available for today($curr_date).");
//                cjp_log_info($this->json($error_log));
//            }
//        }
//        if(!empty($reg_id)){
//            $reg_str = implode(", ", $reg_id);
//            $msg = "Something went wrong for reg_ids=$reg_str, check cron log";
//            $this->sendEmailForAdmins($msg);
//        }
//        cjp_log_info("runCronJobForMembershipPayment() - Completed");
//        if($total_records_processed>0){
//            $end_time = gmdate("Y-m-d H:i:s");
//            $cron_file_name = "RecurringPaymentHandler";
//            $cron_function_name = "runCronJobForMembershipPayment";
//            $cron_message = "Total membership payment records processed";
//            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
//                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
//            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
//            if(!$cron_log_result){
//                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
//                cjp_log_info($this->json($error_log));
//            }
//        }
//    }
//    
//    //Cron Job for event payment preapproval in Wepay
//    private function runCronJobForEventPreapproval(){
//        $total_records_processed = 0;
//        $start_time = gmdate("Y-m-d H:i:s");
//        cjp_log_info("runCronJobForEventPreapproval() - Started");
//        $curr_date = gmdate("Y-m-d");
//        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
//        
//        $query = sprintf("SELECT `access_token`, wp.`wp_user_state`, wp.`account_id`, wp.`currency`, wp.`account_state`, wp.`account_id`, er.`company_id`, c.`upgrade_status`, er.`student_id`, e.`event_title`, er.`event_reg_id`, `event_payment_id`, er.`event_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, 
//                `registration_amount`, ep.`processing_fee`, `payment_type`, er.`payment_frequency`, er.`payment_amount` total_due, `paid_amount`, `credit_card_id`, `credit_card_name`, `credit_card_status`, `total_order_amount`, `total_order_quantity`, 
//                ep.`payment_amount`, ep.`schedule_date`, ep.`schedule_status`, er.`processing_fee_type`
//                FROM `event_registration` er 
//                LEFT JOIN `wp_account` wp ON er.`company_id` = wp.`company_id` 
//                LEFT JOIN `company` c ON er.`company_id` = c.`company_id`
//                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` AND ep.`schedule_status`='N'
//                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
//                WHERE ep.`schedule_date`=CURDATE() AND `payment_type`='RE' AND ep.`payment_amount`>0 ORDER BY er.`company_id`");
//        $result = mysqli_query($this->db, $query);
//        if(!$result){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
//            cjp_log_info($this->json($error_log));
//        }else{
//            $num_rows = mysqli_num_rows($result);
//            if($num_rows>0){
//                $total_records_processed += $num_rows;
//                $company_list = [];
//                while($row = mysqli_fetch_assoc($result)){
//                    $company_id = $row['company_id'];
//                    $access_token = $row['access_token'];
//                    $account_id = $row['account_id'];
//                    $acc_state = $row['account_state'];
//                    $user_state = $row['wp_user_state'];
//                    $currency = $row['currency'];
//                    $event_reg_id = $row['event_reg_id'];
//                    $event_payment_id = $row['event_payment_id'];
//                    $buyer_name = $row['buyer_name'];
//                    $buyer_email = $row['buyer_email'];
//                    $buyer_phone = $row['buyer_phone'];
//                    $buyer_postal_code = $row['buyer_postal_code'];
//                    $cc_id = $row['credit_card_id'];
//                    $cc_name = $row['credit_card_name'];
//                    if(!empty($access_token)){
//                        $failure = $success = 0;
//                        $event_id = $row['event_id'];
//                        $event_name = $desc = $row['event_title'];
//                        $payment_amount = $row['payment_amount'];
//                        $processing_fee = $row['processing_fee'];
//                        $processing_fee_type = $row['processing_fee_type'];
//                        $gmt_date=gmdate(DATE_RFC822);
//                        $sns_msg1=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
//                
////                        if (!function_exists('getallheaders')){
////                            if(!function_exists ('apache_request_headers')){
////                                $user_agent = $_SERVER['HTTP_USER_AGENT'];
////                            }else{
////                                $headers = apache_request_headers();
////                                $user_agent = $headers['User-Agent'];
////                            }
////                        }else{
////                            $headers = getallheaders();
////                            $user_agent = $headers['User-Agent'];
////                        }
//                        $user_agent = "mystudio.academy";
//                        $password = $this->wp_pswd;
//                        $method = 'aes-256-cbc';
//                        // Must be exact 32 chars (256 bit)
//                        $password = substr(hash('sha256', $password, true), 0, 32);
//                        // IV must be exact 16 chars (128 bit)
//                        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
//
//                        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
//                        
//                        if($user_state=='deleted'){
//                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. Wepay user registered was deleted.");
//                            cjp_log_info("wepay_user: ".json_encode($log));
//                            $failure=1;
//                        }
//                        if($acc_state=='deleted'){
//                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
//                            cjp_log_info("wepay_account: ".json_encode($log));
//                            $failure=1;
//                        }elseif($acc_state=='disabled'){
//                            $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
//                            cjp_log_info("wepay_account: ".json_encode($log));
//                            $failure=1;
//                        }                        
////                        $json = array("account_id"=>"$account_id");
////                        $postData = json_encode($json);
////                        $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent,$sns_msg1);
////                        cjp_log_info("wepay_get_account: ".$response['status']);
////                        if($response['status']=='Success'){
////                            $res = $response['msg'];
////                            $account_state = $res['state'];
////                            $currency = $res['currencies'][0];                            
////                            $action_reasons = implode(",", $res['action_reasons']);
////                            $disabled_reasons = implode(",", $res['disabled_reasons']);
////                            $curr_acc = implode(",", $res['currencies']);
////                            if($account_state=='deleted'){
////                                $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");                                
////                                cjp_log_info("wepay_get_account: ".json_encode($log));
////                                $failure=1;
////                            }elseif($account_state=='disabled'){
////                                $log = array("status" => "Failed", "msg" => "The account has been disabled for following reason(s) - ".$disabled_reasons." by WePay and can no longer accept payments.");
////                                cjp_log_info("wepay_get_account: ".json_encode($log));
////                                $failure=1;
////                            }
////                        }else{
////                            $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
////                            cjp_log_info("wepay_get_account: ".json_encode($log));
////                            $failure=1;
////                        }
//                        
//                        if($failure==1){
//                            $payment_query_5 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $event_payment_id));
//                            $payment_result = mysqli_query($this->db, $payment_query_5);
//                            if(!$payment_result){
//                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query_5");
//                                cjp_log_info($this->json($error_log));
//                            }
//                            $this->sendFailedOrderReceiptForEventPayment($event_reg_id,0);
//                            continue;
//                        }
//                        
//                        $rand = mt_rand(1,99999);
//                        $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout";
//                        $unique_id = $company_id."_".$event_id."_".$rand."_".time();
//                        $date = gmdate("Y-m-d_H:i:s");
//                        $ref_id = $company_id."_".$event_id."_".$date;
////                        if($processing_fee_type==2){
////                            $fee_payer = "payer_from_app";
////                        }elseif($processing_fee_type==1){
//                            $fee_payer = "payee_from_app";
////                        }
//                        if($processing_fee_type==2){
//                            $w_paid_amount = $payment_amount + $processing_fee;
//                        }elseif($processing_fee_type==1){
//                            $w_paid_amount = $payment_amount;
//                        }
//                        $time = time();
//                        $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
//                            "fee"=>array("app_fee"=>$processing_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
//                            "email_message"=>array("to_payee"=>"Payment has been added for $event_name","to_payer"=>"Payment has been made for $event_name"),
//                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
//                            "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
//                                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
//                                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))),
//                            "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
//                                "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
//                        $postData2 = json_encode($json2);
//                        $sns_msg1['call'] = "Checkout";
//                        $sns_msg1['type'] = "Checkout Creation";
//                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
//                        cjp_log_info("wepay_checkout_create: ".$response2['status']);
//                        if($response2['status']=="Success"){
//                            $res2 = $response2['msg'];
//                            $checkout_id = $res2['checkout_id'];
//                            $checkout_state = $res2['state'];
//                            $success=1;
//                        }else{
//                            if($response2['msg']['error_code']==1008){
//                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
//                                cjp_log_info("wepay_checkout_create: ".$response2['status']);
//                                if($response2['status']=="Success"){
//                                    $res2 = $response2['msg'];
//                                    $checkout_id = $res2['checkout_id'];
//                                    $checkout_state = $res2['state'];
//                                    $success=1;
//                                }else{
//                                    if($response2['msg']['error_code']==1008){
//                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
//                                        cjp_log_info($this->json($error));
//                                    }else{
//                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                                        cjp_log_info($this->json($error));
//                                    }
//                                    $failure=1;
//                                }
//                            }else{
//                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                                cjp_log_info($this->json($error));
//                                $failure=1;
//                            }
//                            if($failure==1){
//                                $payment_query_6 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $event_payment_id));
//                                $payment_result = mysqli_query($this->db, $payment_query_6);
//                                if(!$payment_result){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query_6");
//                                    cjp_log_info($this->json($error_log));
//                                }
//                                $this->sendFailedOrderReceiptForEventPayment($event_reg_id,0);
//                                continue;
//                            }
//                        }
//                        if($success==1){
//                            $payment_query_7 = sprintf("UPDATE `event_payment` SET `checkout_id`='%s', `checkout_status`='%s', `schedule_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name' WHERE `event_payment_id`='%s'",
//                                    mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, 'S'),
//                                    mysqli_real_escape_string($this->db, $event_payment_id));
//                            $payment_result = mysqli_query($this->db, $payment_query_7);
//                            if(!$payment_result){
//                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query_7");
//                                cjp_log_info($this->json($error_log));
//                            }else{
////                                $num_affected_rows = mysqli_affected_rows($this->db);
////                                if($num_affected_rows>0){
////                                    $temp_payment_amount = $payment_amount;
////                                    $onetime_netsales_for_parent_event = 0;
////                                    
////                                    $update_event_reg_query = sprintf("UPDATE `event_registration` SET `paid_amount`=`paid_amount`+$payment_amount WHERE `event_reg_id`='%s'",$event_reg_id);
////                                    $result_update_event_reg_query = mysqli_query($this->db, $update_event_reg_query);
//////                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_reg_query");
//////                                    cjp_log_info($this->json($error_log));
////                                    if(!$result_update_event_reg_query){
////                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_reg_query");
////                                        cjp_log_info($this->json($error_log));
////                                    }
////                                                        
////                                    $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
////                                               LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
////                                               AND `payment_status` = 'RP' ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
////                                    $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
//////                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query");
//////                                    cjp_log_info($this->json($error_log));
////                                    if(!$get_event_details_result){
////                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query");
////                                        cjp_log_info($this->json($error_log));
////                                    }else{
////                                        $num_rows2 = mysqli_num_rows($get_event_details_result);
////                                        if($num_rows2>0){
////                                            $rem_due = 0;
////                                            while($row2 = mysqli_fetch_assoc($get_event_details_result)){
////                                                if($temp_payment_amount>0){
////                                                    $event_reg_details_id = $row2['event_reg_details_id'];
////                                                    $child_id = $row2['event_id'];
////                                                    $event_parent_id = $row2['parent_id'];
////                                                    $rem_due = $row2['balance_due'];
////                                                    $child_net_sales = 0;
////                                                    if($onetime_netsales_for_parent_event==0){
////                                                        if($processing_fee_type==2){
////                                                            $net_sales = $payment_amount;
////                                                        }elseif($processing_fee_type==1){
////                                                            $net_sales = $payment_amount-$processing_fee;
////                                                        }
////                                                        $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
////                                                        $result_update_event_query = mysqli_query($this->db, $update_event_query);
//////                                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
//////                                                        cjp_log_info($this->json($error_log));
////                                                        if(!$result_update_event_query){
////                                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
//////                                                            cjp_log_info($this->json($error_log));
////                                                        }
////                                                        $onetime_netsales_for_parent_event=1;
////                                                    }
////                                                    if($temp_payment_amount>=$rem_due){
////                                                        $temp_payment_amount = $temp_payment_amount-$rem_due;
////                                                        $child_net_sales = $rem_due;
////                                                        $rem_due = 0;
////                                                        $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due, `payment_status`='RC' WHERE `event_reg_details_id`='%s'", 
////                                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
////                                                    }else{
////                                                        $rem_due = $rem_due - $temp_payment_amount;
////                                                        $child_net_sales = $temp_payment_amount;
////                                                        $temp_payment_amount = 0;
////                                                        $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due WHERE `event_reg_details_id`='%s'", 
////                                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
////                                                    }
////                                                    $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
//////                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query");
//////                                                    cjp_log_info($this->json($error_log));
////                                                    if(!$result_update_reg_query){
////                                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query");
//////                                                       cjp_log_info($this->json($error_log));
////                                                    }else{
////                                                        if($child_id!=$event_parent_id){
////                                                            $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
////                                                            $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
//////                                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales");
//////                                                            cjp_log_info($this->json($error_log));
////                                                            if(!$result_update_child_query){
////                                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales");
////                                                                cjp_log_info($this->json($error_log));
////                                                            }
////                                                        }
////                                                    }
//                                                    $this->sendOrderReceiptForEventPayment($event_reg_id, 0);
////                                                }
////                                            }
////                                        }else{
////                                            $error = array("status" => "Failed", "msg" => "Participant details doesn't exist.");
////                                            cjp_log_info($this->json($error));
////                                        }
////                                    }
////                                }else{
////                                    $error_log = array('status' => "Failed", "msg" => "Affected rows 0.", "query" => "$payment_query_7");
////                                    cjp_log_info($this->json($error_log));
////                                }
//                            }
//                        }
//
//                    }else{
//                        if(!in_array($company_id, $company_list)){
//                            $error_log = array('status' => "Failed", "msg" => "Access token is empty for company(Id = $company_id). Cannot be accept payments anymore.");
//                            cjp_log_info($this->json($error_log));
//                            $company_list[] = $company_id;
//                        }
//                        $payment_query_8 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $event_payment_id));
//                        $payment_result = mysqli_query($this->db, $payment_query_8);
//                        if(!$payment_result){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query_8");
//                            cjp_log_info($this->json($error_log));
//                        }
//                        $this->sendFailedOrderReceiptForEventPayment($event_reg_id,0);
//                    }
//                }
//            }else{
//                $error_log = array('status' => "Failed", "msg" => "No scheduled payment available for today($curr_date).");
//                cjp_log_info($this->json($error_log));
//            }
//        }
//        cjp_log_info("runCronJobForEventPreapproval() - Completed");
//        if($total_records_processed>0){
//            $end_time = gmdate("Y-m-d H:i:s");
//            $cron_file_name = "RecurringPaymentHandler";
//            $cron_function_name = "runCronJobForEventPreapproval";
//            $cron_message = "Total event payment records processed";
//            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
//                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
//            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
//            if(!$cron_log_result){
//                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
//                cjp_log_info($this->json($error_log));
//            }
//        }
//    }
    
    private function sendEmailForAdmins($msg){
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "Membership Subscription Handling Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        cjp_log_info($this->json($error_log));
        return $response;
    }
    
//    private function sendOrderReceiptForMembershipPayment($membership_reg_id, $return_value){
//       
//        $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
//        $payment_details = $prorate_flag_check = [];
//        $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
//        $recurring_start_date = $payment_start_date = $payment_frequency = $custom_recurring_frequency_period_type = $msg1 = $msg2 = $msg3 = '';
//        $membership_structure = $billing_options = $billing_options_no_of_payments = $paid_str= $pay_type = '';
//        $total_mem_fee = $registration_fee = 0;
//        
//        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, mp.`cc_name`, `waiver_policies`, mr.credit_card_name, 
//            mr.`membership_start_date`, mr.`payment_start_date`,mr.`specific_end_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
//            `buyer_postal_code`, mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, DATE(mp.`created_dt`) created_dt, mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
//            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
//            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`,mp.`prorate_flag`
//            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id` LEFT JOIN `student` s ON s.`company_id`=mr.`company_id` AND s.`student_id`= mr.`student_id`
//            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
//            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N')
//            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
//        $result = mysqli_query($this->db, $query);
//        if(!$result){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
//            cjp_log_info($this->json($error_log));
//            return;
//        }else{
//            $num_rows = mysqli_num_rows($result);
//            if($num_rows>0){                
//                while($row = mysqli_fetch_assoc($result)){
//                    $studio_name = $row['company_name'];
//                    $studio_mail = $row['email_id'];
//                    $cc_email_list = $row['referral_email_list'];
//                    $wp_currency_code = $row['wp_currency_code'];
//                    $wp_currency_symbol = $row['wp_currency_symbol'];
////                    $company_id = $row['company_id'];
////                    $membership_id = $row['membership_id'];
////                    $membership_option_id = $row['membership_option_id'];
//                    $payment_type = $row['payment_type'];
//                    $membership_category_title = $row['membership_category_title'];
//                    $membership_title = $row['membership_title'];
//                    $membership_structure = $row['membership_structure'];
//                    $participant_name = $row['participant_name'];
//                    $buyer_email = $row['buyer_email'];
//                    $waiver_policies = $row['waiver_policies'];
//                    $signup_fee = $row['signup_fee'];
//                    $signup_fee_disc = $row['signup_fee_disc'];
//                    $mem_fee = $row['membership_fee'];
//                    $mem_fee_disc = $row['membership_fee_disc'];
//                    $initial_payment = $row['initial_payment'];
//                    $inital_processing_fee = $row['initial_processing_fee'];
//                    $membership_start_date = $row['membership_start_date'];
//                    $payment_start_date = $row['payment_start_date'];
//                    $recurring_start_date = $row['recurring_start_date'];
//                    $next_payment_date = $row['next_payment_date'];
//                    $payment_frequency = $row['payment_frequency'];
//                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
//                    $recurring_amount = $row['recurring_amount'];
//                    $processing_fee_type = $row['processing_fee_type'];
//                    $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
//                    $first_payment = $row['first_payment'];
//                    $billing_options = $row['billing_options'];
//                    $no_of_classes = $row['no_of_classes'];
//                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
//                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
//                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
//                    $credit_card_name = $row['credit_card_name'];
//                    $prorate_flag_check[] = $row['prorate_flag'];
//                    $specific_end_date = $row['specific_end_date'];
//                    $student_cc_email_list = $row['student_cc_email'];
//                    $temp=[];
//                    if(!empty($row['membership_payment_id'])){
//                        if($recurring_amount == $row['payment_amount']){
//                            $recurring_processing_fee = $row['processing_fee'];
//                        }
//                        if($row['processing_fee_type']==2){
//                            if ($membership_structure != 'SE') {
//                            $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
//                            } else if ($membership_structure == 'SE') {
//                            $temp['amount'] = $row['payment_amount'];
//                        }
//                        }else{
//                            $temp['amount'] = $row['payment_amount'];
//                        }
//                       $payment_date_temp = $temp['date'] = $row['payment_date'];
//                        $temp['created_dt'] = $row['created_dt'];
//                        $temp['processing_fee'] = $row['processing_fee'];
//                        $temp['payment_status'] = $row['payment_status'];
//                        $temp['paytime_type'] = $row['paytime_type'];
//                        $temp['cc_name'] = $row['cc_name'];
//                        $paid_amt += $temp['amount'];
//                        $payment_details[] = $temp;
//                    }
//                }
//                if($membership_structure=='SE'){
//                    $registration_fee = number_format(($signup_fee-$signup_fee_disc),2);
//                    for($i = 0;$i<count($payment_details);$i++){
//                        $total_mem_fee += $payment_details[$i]['amount'];  
//                    }
//                        $total_mem_fee -= $registration_fee;
//                }
//                if(($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE') && $billing_options=='PP'){
//                    if($billing_options_no_of_payments>1){
//                        $already_recurring_records_count = 0;
//                        $next_date = '';
//                        if($membership_structure!=='SE'){
//                        for($i=0;$i<count($payment_details);$i++){
//                            if($payment_details[$i]['paytime_type']=='R'){
//                                $already_recurring_records_count++;
//                                $next_date = $payment_details[$i]['date'];
//                            }
//                        }
//                        }
//                        if($billing_options_no_of_payments>$already_recurring_records_count && $next_date !== ''){
//                            $count = $billing_options_no_of_payments - $already_recurring_records_count;
//                            $future_date = $next_date;
//                            for($j=0;$j<$count;$j++){
//                                if($payment_frequency=='W'){
//                                    $next_payment_date2 = date('Y-m-d', strtotime('+1 weeks', strtotime($future_date)));
//                                }elseif($payment_frequency=='B'){
//                                    $next_payment_date2 = date('Y-m-d', strtotime('+14 days', strtotime($future_date)));
//                                }elseif($payment_frequency=='M'){
//                                    $next_payment_date2=$this->getSameDayNextMonth(new DateTime($future_date));
//                                }
//                                
//                                $temp_1 =[];
//                                if($processing_fee_type==2){
//                                    if ($membership_structure != 'SE') {
//                                    $temp_1['amount'] = $recurring_amount+$recurring_processing_fee;
//                                    } elseif ($membership_structure == 'SE') {
//                                        $temp_1['amount'] = $recurring_amount;
//                                    }
//                                }else{
//                                    $temp_1['amount'] = $recurring_amount;
//                                }
//                                $temp_1['date'] = $next_payment_date2;
//                                $temp_1['processing_fee'] = $recurring_processing_fee;
//                                $temp_1['payment_status'] = 'N';
//                                $temp_1['paytime_type'] = 'R';
//                                $temp_1['cc_name'] = $credit_card_name;
//                                $payment_details[] = $temp_1;
//                                $future_date = $next_payment_date2;
//                            }
//                        }
//                    }
//                }
//            }else{
//                $error_log = array('status' => "Failed", "msg" => "No records found.");
//                cjp_log_info($this->json($error_log));
//                return;
//            }
//        }
//        
//        $subject = $membership_category_title." Membership Order Confirmation";
//        
//        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
//        $message .= "<b>Membership Details</b><br><br>";
//        $message .= "Membership Start Date: ".date("M dS, Y", strtotime($membership_start_date))."<br>";
//        if($membership_structure == 'SE'){
//        $message .= "Membership End Date: ".date("M dS, Y", strtotime($specific_end_date))."<br>";
//        $message .= "Registration Fee: $wp_currency_symbol".(number_format(($registration_fee),2))."<br>";
//        $message .= "Total Membership Fee: $wp_currency_symbol".(number_format(($total_mem_fee),2))."<br>";
//        }
//        $message .= "Membership: $membership_category_title, $membership_title<br>Participant: ".$participant_name."<br>";
//        if($membership_structure=='NC' && $billing_options=='PP' && $billing_options_expiration_date!='0000-00-00'){
//            $message .= "$no_of_classes Class Package Expires: ".date("D, M d, Y", strtotime($billing_options_expiration_date))."<br>";
//        }
//        $message .= "<br>";
//            
//        $prorated = "";
//        if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
//            $prorated = "(Pro-rated)";
//        }
//        
//        if($paid_amt>0){
////            if($initial_payment>0){
//            if($membership_structure !== 'SE'){
//                $message .= "<br><b>Registration Details</b><br><br>";
//                $message .= "Sign-up Fee: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
//            }
//            else if($membership_structure == 'SE' && $billing_options=='PF'){
//                $message .= "<br><b>Membership payment plan</b><br><br>";
////                $message .= "payment: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
////                $message .= " (plus $wp_currency_symbol".$inital_processing_fee." administrative fees)"."<br>";
//            }   //check ravi
//             if($membership_structure !== 'SE'){
//                if($processing_fee_type==2){
//                    if($initial_payment>0){
//                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment+$inital_processing_fee),2))." (includes $wp_currency_symbol".$inital_processing_fee." administrative fees)<br>";
//                    }else{
//                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
//                    }
//                }else{
//                    if($initial_payment>0){
//                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
//                    }else{
//                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
//                    }
//                }
//        }
//        }
//        if(count($payment_details)>0){
//            if($payment_frequency!='N' && $payment_type=='R'){
//                $freq_str = "";
//                if($membership_structure=='OE' || $membership_structure==1){
//                    if($payment_frequency=='B'){
//                        $freq_str = "Semi-Monthly Recurring: ";
//                    }elseif($payment_frequency=='M'){
//                        $freq_str = "Monthly Recurring: ";
//                    }elseif($payment_frequency=='W'){
//                        $freq_str = "Weekly Recurring: ";
//                    }elseif($payment_frequency=='A'){
//                        $freq_str = "Annual Recurring: ";
//                    }elseif($payment_frequency=='C'){
//                        if($custom_recurring_frequency_period_type=='CW'){
//                            $freq_str = "Weekly Recurring: ";
//                        }elseif($custom_recurring_frequency_period_type=='CM'){
//                            $freq_str = "Monthly Recurring: ";
//                        }
//                    }
//                }elseif($billing_options=='PP'){
//                    if($payment_frequency=='W'){
//                        $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
//                    }elseif($payment_frequency=='B'){
//                        $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
//                    }elseif($payment_frequency=='M'){
//                        $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
//                    }
//                }
//                
//                $message .= "<br><br><b>Membership Payment Plan</b><br><br>";
//                $recur_pf_str = "";
//                if ($membership_structure !== 'SE') {
//                if($processing_fee_type==2){
//                    $recur_pf_str = " (plus $wp_currency_symbol".$recurring_processing_fee." administrative fees)";
//                }
//                if($freq_str!=""){
//                    $message .= "$freq_str $wp_currency_symbol".$recurring_amount.$recur_pf_str."<br>";
//                }
////                $message .= "Next Payment Date: ".date("M dS, Y", strtotime($next_payment_date))."<br>";
//                for ($init_success_payments = 0; $init_success_payments < count($payment_details); $init_success_payments++) {
//                    if ($payment_details[$init_success_payments]['paytime_type'] == 'R' && $payment_details[$init_success_payments]['payment_status'] == 'N') {
//                        $date_temp = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
//                        $message .= "Next Payment Date: " . date("M dS, Y", strtotime($date_temp)) . "<br>";
//                        break;
//                    }
//                }
//            }
//            }
//            if ($membership_structure !== 'SE') {
//            $message.="<br><br><b>Payment Details</b><br><br>";
//            }
//            for($init_success_payments=0;$init_success_payments<count($payment_details);$init_success_payments++){
//                $prorated_1 = $prorated_2 = $prorated_3 ="";
//                $temp_se_date_check = date("d,m,Y,w",strtotime($payment_details[$init_success_payments]['date']));
//                $se_check_prorate = explode(",",$temp_se_date_check);
//                $pf_str = "";
//                if ($processing_fee_type == 2){
//                    if($membership_structure !== 'SE') {
//                        $pf_str = " (includes $wp_currency_symbol".$payment_details[$init_success_payments]['processing_fee']." administrative fees)";
//                    } else {
//                        $pf_str = " (plus $wp_currency_symbol" . $payment_details[$init_success_payments]['processing_fee'] . " administrative fees)";
//                    }
//                }
//                if($membership_structure !== 'SE'){
//                    if($processing_fee_type==2){
//                        $amount_temp=$payment_details[$init_success_payments]['amount']-$payment_details[$init_success_payments]['processing_fee'];
//                    }else{
//                        $amount_temp=$payment_details[$init_success_payments]['amount']; 
//                    }
//                if($payment_details[$init_success_payments]['paytime_type']=='I'){
//                    if(($mem_fee - $mem_fee_disc)+ ($signup_fee - $signup_fee_disc) == ($amount_temp) || $signup_fee - $signup_fee_disc == ($amount_temp)){
//                        $prorated_1 = "";
//                    }else{
//                        $prorated_1 = "(Pro-rated)";
//                    }
//                }elseif($payment_details[$init_success_payments]['paytime_type']=='F'){
//                       if($mem_fee - $mem_fee_disc == ($amount_temp)){
//                        $prorated_2 = "";
//                    }else{
//                        $prorated_2 = "(Pro-rated)";
//                    } 
//                }
//                }else if($membership_structure == 'SE' && $billing_options == 'PP'){
//                    if($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type']=='I'){
//                         $prorated_1 = "(Pro-rated)";
//                    }else if($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type']=='F') {
//                         $prorated_2 = "(Pro-rated)";
//                    }else if($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type']=='R'){
//                         $prorated_3 = "(Pro-rated)";
//                    }
//                }
//                    if($membership_structure !== 'SE'){    
//                        if(!empty($prorated_1)){
//                         $prorated_2 = "";
//                    }
//                    }
//                if($initial_payment>0 && $payment_details[$init_success_payments]['paytime_type']=='I'){
//                    $down_pay = $payment_details[$init_success_payments]['amount'].$pf_str." paid on ".date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']))." (".$payment_details[$init_success_payments]['cc_name'].") ".$prorated_1;
//                    $msg1 .= "Payment: $wp_currency_symbol".$down_pay."<br>";
//                }
//                if($payment_details[$init_success_payments]['paytime_type']=='F'){
//                    $amount = $payment_details[$init_success_payments]['amount'];
//                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
//                    if($payment_details[$init_success_payments]['payment_status']=='S'){
//                        $paid_str = "paid on";
//                        $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
//                    }else{
//                        $paid_str = "due";
//                        if($membership_structure === 'SE'){
//                         $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
//                        }else{
//                        $pay_type= "";
//                    }
//                    }
//                    if($membership_structure !== 'SE' || ($membership_structure == 'SE' && $billing_options == 'PP')){
//                    $msg2 .= "First Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_2." <br>";
//                    }else{
//                    $msg2 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_2." <br>"; 
//                    }
//                }
//                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='S'){
//                    $amount = $payment_details[$init_success_payments]['amount'];
//                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
//                    $paid_str = "paid on";
//                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
//                        if($membership_structure !== 'SE'){
//                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
//                        }else{
//                        $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_3." <br>";
//                }
//                    }
//                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='N' && ($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE') && $billing_options=='PP'){
//                    $amount = $payment_details[$init_success_payments]['amount'];
//                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
//                    $paid_str = "due";
//                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
//                        if($membership_structure !== 'SE'){
//                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
//                        }else{
//                        $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_3." <br>";  
//                }
//            }
//            }
//            $message .= $msg1.$msg2.$msg3;
//            $message .= "<br><br><br>";
//        }
//        
//        $waiver_present = 0;
//        $file = '';
//        if(!empty(trim($waiver_policies))){
//            $waiver_present = 1;
//            $file_name_initialize = 10;
//            for($z=0; $z<$file_name_initialize; $z++){
//                $dt = time();
//                $file = __DIR__."/../uploads/".$dt."_waiver_policies.html";        
//                if(file_exists($file)){
//                    sleep(1);
//                    $file_name_initialize++;
//                }else{
//                    break;
//                }
//            }
//            $ifp = fopen($file, "w");
//            fwrite($ifp, $waiver_policies);
//            fclose($ifp);
//        }
//        $send_to_studio = 0;
//        $sendEmail_status = $this->sendEmailForMembership($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list, $student_cc_email_list, $send_to_studio);
//        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
//            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
//            cjp_log_info($this->json($error_log));
//        }else{
//            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
//            cjp_log_info($this->json($error_log));
//        }
//    }
    
    private function sendFailedOrderReceiptForMembershipPayment($membership_reg_id, $return_value){
       
        $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
        $payment_details = [];
        $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
        $recurring_start_date = $membership_start_date = $payment_start_date = $payment_frequency = $custom_recurring_frequency_period_type = $msg1 = $msg2 = $msg3 = '';
        $membership_structure = $billing_options = $billing_options_no_of_payments = '';
        
        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, mp.`cc_name`, `waiver_policies`, mr.credit_card_name, 
            mr.`membership_start_date`, mr.`payment_start_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
            `buyer_postal_code`, mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, DATE(mp.`created_dt`) created_dt, mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`
            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id` LEFT JOIN `student` s ON s.`company_id`= mr.`company_id` AND s.`student_id`= mr.`student_id`
            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N','F','M','R','MR')
            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){                
                while($row = mysqli_fetch_assoc($result)){
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
//                    $company_id = $row['company_id'];
//                    $membership_id = $row['membership_id'];
//                    $membership_option_id = $row['membership_option_id'];
                    $payment_type = $row['payment_type'];
                    $membership_category_title = $row['membership_category_title'];
                    $membership_title = $row['membership_title'];
                    $membership_structure = $row['membership_structure'];
                    $participant_name = $row['participant_name'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $signup_fee = $row['signup_fee'];
                    $signup_fee_disc = $row['signup_fee_disc'];
                    $mem_fee = $row['membership_fee'];
                    $mem_fee_disc = $row['membership_fee_disc'];
                    $initial_payment = $row['initial_payment'];
                    $inital_processing_fee = $row['initial_processing_fee'];
                    $membership_start_date = $row['membership_start_date'];
                    $payment_start_date = $row['payment_start_date'];
                    $recurring_start_date = $row['recurring_start_date'];
                    $next_payment_date = $row['next_payment_date'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $recurring_amount = $row['recurring_amount'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
                    $first_payment = $row['first_payment'];
                    $billing_options = $row['billing_options'];
                    $no_of_classes = $row['no_of_classes'];
                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $credit_card_name = $row['credit_card_name'];
                    $student_cc_email_list = $row['student_cc_email'];
                    $temp=[];
                    if(!empty($row['membership_payment_id'])){
                        if($recurring_amount == $row['payment_amount']){
                            $recurring_processing_fee = $row['processing_fee'];
                        }
                        if($row['processing_fee_type']==2){
                            if ($membership_structure != 'SE') {
                            $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                            } else if ($membership_structure == 'SE') {
                                $temp['amount'] = $row['payment_amount'];
                            }
                        }else{
                            $temp['amount'] = $row['payment_amount'];
                        }
                        $temp['date'] = $row['payment_date'];
                        $temp['created_dt'] = $row['created_dt'];
                        $temp['processing_fee'] = $row['processing_fee'];
                        $temp['payment_status'] = $row['payment_status'];
                        $temp['paytime_type'] = $row['paytime_type'];
                        $temp['cc_name'] = $row['cc_name'];
                        $paid_amt += $temp['amount'];
                        $payment_details[] = $temp;
                    }
                }
                if(($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE') && $billing_options=='PP'){
                    if($billing_options_no_of_payments>1){
                        $already_recurring_records_count = 0;
                        $next_date = '';
                        for($i=0;$i<count($payment_details);$i++){
                            if($payment_details[$i]['paytime_type']=='R'){
                                $already_recurring_records_count++;
                                $next_date = $payment_details[$i]['date'];
                            }
                        }
                        if($billing_options_no_of_payments>$already_recurring_records_count){
                            $count = $billing_options_no_of_payments - $already_recurring_records_count;
                            $future_date = $next_date;
                            for($j=0;$j<$count;$j++){
                                if($payment_frequency=='W'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+1 weeks', strtotime($future_date)));
                                }elseif($payment_frequency=='B'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+14 days', strtotime($future_date)));
                                }elseif($payment_frequency=='M'){
                                    $next_payment_date2=$this->getSameDayNextMonth(new DateTime($future_date));
                                }
                                
                                $temp_1=[];
                                if($processing_fee_type==2){
                                    if ($membership_structure != 'SE') {
                                    $temp_1['amount'] = $recurring_amount+$recurring_processing_fee;
                                    } else if ($membership_structure == 'SE') {
                                        $temp_1['amount'] = $recurring_amount;
                                    }
                                }else{
                                    $temp_1['amount'] = $recurring_amount;
                                }
                                $temp_1['date'] = $next_payment_date2;
                                $temp_1['processing_fee'] = $recurring_processing_fee;
                                $temp_1['payment_status'] = 'N';
                                $temp_1['paytime_type'] = 'R';
                                $temp_1['cc_name'] = $credit_card_name;
                                $payment_details[] = $temp_1;
                                $future_date = $next_payment_date2;
                            }
                        }
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                cjp_log_info($this->json($error_log));
                return;
            }
        }
        
        $subject = $membership_category_title." - Payment failed";
        
        $message .= "<b>The following payment for the Buyer $buyer_name failed.</b><br><br>";
        $message .= "<b>Membership Details</b><br><br>";
        $message .= "Membership Start Date: ".date("M dS, Y", strtotime($membership_start_date))."<br>";
        $message .= "Membership: $membership_category_title, $membership_title<br>Participant: ".$participant_name."<br>";
        if($membership_structure=='NC' && $billing_options=='PP' && $billing_options_expiration_date!='0000-00-00'){
            $message .= "$no_of_classes Class Package Expires: ".date("D, M d, Y", strtotime($billing_options_expiration_date))."<br>";
        }
        $message .= "<br>";
            
        $prorated = "";
        if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
            $prorated = "(Pro-rated)";
        }

        if($paid_amt>0){
//            if($initial_payment>0){
                $message .= "<br><b>Registration Details</b><br><br>";
                $message .= "Sign-up Fee: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
                if($processing_fee_type==2){
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment+$inital_processing_fee),2))." (includes $wp_currency_symbol".$inital_processing_fee." administrative fees) $prorated<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }else{
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))." $prorated<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }
//            }
        }
        if(count($payment_details)>0){
            if($payment_frequency!='N' && $payment_type=='R'){
                $freq_str = "";
                if($membership_structure=='OE' || $membership_structure==1){
                    if($payment_frequency=='B'){
                        $freq_str = "Semi-Monthly Recurring: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "Monthly Recurring: ";
                    }elseif($payment_frequency=='W'){
                        $freq_str = "Weekly Recurring: ";
                    }elseif($payment_frequency=='A'){
                        $freq_str = "Annual Recurring: ";
                    }elseif($payment_frequency=='C'){
                        if($custom_recurring_frequency_period_type=='CW'){
                            $freq_str = "Weekly Recurring: ";
                        }elseif($custom_recurring_frequency_period_type=='CM'){
                            $freq_str = "Monthly Recurring: ";
                        }
                    }
                }elseif($billing_options=='PP'){
                    if($payment_frequency=='W'){
                        $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
                    }elseif($payment_frequency=='B'){
                        $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
                    }
                }
                
                $message .= "<br><br><b>Membership Payment Plan</b><br><br>";
                $recur_pf_str = "";
                if ($membership_structure !== 'SE') {
                if($processing_fee_type==2){
                    $recur_pf_str = " (plus $wp_currency_symbol".$recurring_processing_fee." administrative fees)";
                }
                if($freq_str!=""){
                    $message .= "$freq_str $wp_currency_symbol".$recurring_amount.$recur_pf_str."<br>";
                }
                $message .= "Next Payment Date: ".date("M dS, Y", strtotime($next_payment_date))."<br>";
            }
            }
            
            if ($membership_structure !== 'SE') {
            $message.="<br><br><b>Payment Details</b><br><br>";
            }
            for($init_success_payments=0;$init_success_payments<count($payment_details);$init_success_payments++){
                $pf_str = "";
                if($processing_fee_type==2){
                    if ($membership_structure !== 'SE') {
                    $pf_str = " (includes $wp_currency_symbol".$payment_details[$init_success_payments]['processing_fee']." administrative fees)";
                    } else if ($membership_structure == 'SE') {
                        $pf_str = " (plus $wp_currency_symbol" . $payment_details[$init_success_payments]['processing_fee'] . " administrative fees)";
                }
                }
                if($initial_payment>0 && $payment_details[$init_success_payments]['paytime_type']=='I'){
                    $down_pay = $payment_details[$init_success_payments]['amount'].$pf_str." paid on ".date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']))." (".$payment_details[$init_success_payments]['cc_name'].") " .$prorated;
                    $msg1 .= "Payment: $wp_currency_symbol".$down_pay."<br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='F'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    if($payment_details[$init_success_payments]['payment_status']=='S'){
                        $paid_str = "paid on";
                        $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    }else{
                        $paid_str = "due";
                        $pay_type= "";
                    }
                    $msg2 .= "First Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }

                if($payment_details[$init_success_payments]['paytime_type']=='R' && ($payment_details[$init_success_payments]['payment_status']=='F') ||($payment_details[$init_success_payments]['payment_status']=='S')||
                        ($payment_details[$init_success_payments]['payment_status']=='R')||($payment_details[$init_success_payments]['payment_status']=='MR')||($payment_details[$init_success_payments]['payment_status']=='FR')||($payment_details[$init_success_payments]['payment_status']=='M')){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    if($payment_details[$init_success_payments]['payment_status']=='S'){
                      $paid_str = "paid on";  
                    }elseif($payment_details[$init_success_payments]['payment_status']=='R'){
                      $paid_str = "Refunded";  
                    }elseif($payment_details[$init_success_payments]['payment_status']=='MR'){
                      $paid_str = "Mannually Refunded";  
                    }elseif($payment_details[$init_success_payments]['payment_status']=='M'){
                      $paid_str = "Manual credit applied";  
                    }else{
                    $paid_str = "past due";
                    }
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='N' && ($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE') && $billing_options=='PP'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "due";
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
            }
            $message .= $msg1.$msg2.$msg3;
            $message .= "<br><br><br>";
        }
        $send_to_studio = 1;
        $sendEmail_status = $this->sendEmailForMembership('',$studio_mail, $subject, $message, '', '', '', 0, $cc_email_list, $student_cc_email_list, $send_to_studio);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            cjp_log_info($this->json($error_log));
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            cjp_log_info($this->json($error_log));
        }
    }
    
    private function sendEmailForMembership($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list1, $student_cc_email_list1, $send_to_studio) {
       $to_available = 0;
        $bounce_check=$this->sendBounceEmailCheck($to,$cc_email_list1,$student_cc_email_list1);
         $cc_email_list=$bounce_check['cc_email_array'];
        $student_cc_email_list=$bounce_check['student_CC_array'];
        $bounced_flag=$bounce_check['bounced_flag'];
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
//            if(filter_var($from, FILTER_VALIDATE_EMAIL)){
//                $mail->setFrom($from, $cmp_name);
//                $mail->AddCC($from);
//            }else{
//                $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            if ($bounced_flag == 'Y') {
                if (!empty($student_cc_email_list)) {
                    $student_cc_addresses = $student_cc_email_list;
//                $student_cc_addresses = explode(',', $student_cc_email_list);
                    for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                        if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                            $mail->AddAddress($student_cc_addresses[$init_1]);
                            $to_available=1;
                        }
                    }
                }
                if($to_available==0) {
                    return false;
                }
            } else {
                $mail->AddAddress($to);
                if (!empty($student_cc_email_list)) {
                    $student_cc_addresses = $student_cc_email_list;
//                $student_cc_addresses = explode(',', $student_cc_email_list);
                    for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                        if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                            $mail->AddCC($student_cc_addresses[$init_1]);
                        }
                    }
                }
            }

            if($send_to_studio==1){     //send_to_studio - enable/disable sending the email to studio in certain cases
                if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                    $mail->AddCC($from);
                }
                if(!empty($cc_email_list)){
                    $cc_addresses=$cc_email_list;
//                    $cc_addresses = explode(',', $cc_email_list);
                    for($init=0;$init<count($cc_addresses);$init++){
                        if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                            $mail->AddCC($cc_addresses[$init]);
                        }
                    }
                }
            }
            
            
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
//        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
//        cjp_log_info($this->json($error_log));
        return $response;
    }
    
    private function sendFailedOrderReceiptForEventPayment($event_reg_id, $return_value){
       
        $waiver_policies = '';
        $query = sprintf("SELECT er.`company_id`, er.`event_id`, `event_title`, ep.`cc_name`, `waiver_policies`, concat(er.event_registration_column_2, ',', er.event_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `payment_type`, 
            er.`payment_amount` total_due, er.`processing_fee_type`, er.`processing_fee` total_processing_fee, `paid_amount`, ep.`payment_amount`, ep.`processing_fee`, ep.`refunded_amount`, `schedule_date`, `schedule_status`, DATE(ep.`created_dt`) created_dt
            FROM `event_registration` er LEFT JOIN `event_payment` ep ON er.`event_reg_id` = ep.`event_reg_id` AND ep.`schedule_status` IN ('N', 'S', 'PR', 'M')
            LEFT JOIN `event` e ON e.`event_id` = er.`event_id` AND e.`company_id` =  er.`company_id`
            LEFT JOIN `company` c ON c.`company_id` =  er.`company_id`
            WHERE er.`event_reg_id` = '%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
                $event_name = $participant_name = $buyer_name = $buyer_email = $cc_email_list =$paid_str = '';
                $inital_stage = $initial_down_pay = 0;
                $current_plan_details = $event_details = $bill_due_details = $payment_details = [];
                $total_due = $paid_due = $rem_due = 0;
                while($row = mysqli_fetch_assoc($result)){
                    $paid_amt = 0;
                    $event_id = $row['event_id'];
                    $event_name = $row['event_title'];
                    $participant_name = $row['participant_name'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $temp=[];
                    
                    if($row['processing_fee_type']==2){
                        $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                    }else{
                        $temp['amount'] = $row['payment_amount'];
                    }
//                    $temp['amount'] = $row['payment_amount'];
                    $temp['date'] = $row['schedule_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['edit_status'] = 'N';
                    $temp['schedule_status'] = $row['schedule_status'];
                    $temp['processing_fee'] = $row['processing_fee'];
                    $temp['credit_card_name'] = $row['cc_name'];
                    if($row['schedule_status']=='N'){
                        $temp['status'] = 'N';
                    }elseif($row['schedule_status']=='S'){
                        $temp['status'] = 'S';
                        $paid_amt = $temp['amount'];
                        $inital_stage++;
                    }elseif($row['schedule_status']=='PR' || $row['schedule_status']=='M'){
                        $paid_amt = $temp['amount']-$temp['refunded_amount'];
                    }
//                    $total_due = $current_plan_details['total_due'] = $row['total_due'];
//                    $paid_due = $current_plan_details['paid_due'] = $row['paid_amount'];
                    $total_due += $temp['amount'];
                    $current_plan_details['total_due'] = $total_due;
                    $paid_due += $paid_amt;
                    $current_plan_details['paid_due'] = $paid_due;
                    
                    $current_plan_details['success_count'] = $inital_stage;
                    $rem_due = $current_plan_details['rem_due'] = number_format((float)($total_due - $paid_due), 2, '.', '');
//                    $payment_details[] = $temp;
                    if ($row['schedule_status'] == 'N') {
                        $bill_due_details[] = $temp;
                    } elseif ($row['schedule_status'] == 'S') {
                        $payment_details[] = $temp;
                        if ($initial_down_pay > 0) {
                            $bill_due_details[] = $temp;
                        }
                        $initial_down_pay++;
                    } elseif ($row['schedule_status'] == 'M') {
                        $payment_details[] = $temp;
                    }
                }
                
                $studio_name = $studio_mail = $message = '';
                $query2 = sprintf("SELECT c.`company_name`, c.`email_id`, c.`referral_email_list`, e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, `event_end_dt`, erd.`event_cost`, `discount`, erd.`payment_amount`, `quantity`, `payment_status`, `balance_due`,s.`student_cc_email`   
                    FROM `event` e LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` LEFT JOIN `event_registration` er ON er.`event_reg_id`= erd.`event_reg_id`
                    LEFT JOIN `company` c ON e.`company_id` =  c.`company_id` LEFT JOIN `student` s ON s.`company_id`= er.`company_id` AND s.`student_id`= er.`student_id`
                    WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND erd.`event_reg_id`='%s' AND `payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", 
                        mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                $result2 = mysqli_query($this->db, $query2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    cjp_log_info($this->json($error_log));
                    return;
                }else{
                    $temp_quantity = $temp_char_count = 0;
                    while($row2 = mysqli_fetch_assoc($result2)){
                        $temp2 = [];
                        $temp2['title'] = $row2['event_title'];
                        if($temp2['title']>$temp_char_count){
                            $temp_char_count = strlen($temp2['title']);
                        }
                        
                        if (!empty(trim($row2['event_begin_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_begin_dt']) {
                                $start = " - ";
                            } else {
                                $start = $row2['event_begin_dt'];

                                $split_start = explode(" ", $start);
                                $start_date_value = $split_start[0];
                                $start_time_value = $split_start[1];
                                if ("00:00:00" == $start_time_value) {
                                    $start = date('M d, Y', strtotime($start));
                                } else {
                                    $start = date('M d, Y @ h:i A', strtotime($start));
                                }
                            }
                        }
                        
                        if (!empty(trim($row2['event_end_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_end_dt']) {
                                $end = " - ";
                            } else {
                                $end = $row2['event_end_dt'];

                                $split_end = explode(" ", $end);
                                $end_date_value = $split_end[0];
                                $end_time_value = $split_end[1];
                                if ("00:00:00" == $end_time_value) {
                                    $end = date('M d, Y', strtotime($end));
                                } else {
                                    $end = date('M d, Y @ h:i A', strtotime($end));
                                }
                            }
                        }

                        $temp2['start'] = $start;
                        $temp2['end'] = $end;
                        $temp2['event_cost'] = $row2['event_cost'];
                        $temp2['quantity'] = $row2['quantity'];
                        $temp2['event_disc'] = $row2['discount'];
                        $temp2['discounted_price'] = $row2['event_cost'] - $row2['discount'];
                        $temp_quantity += $row2['quantity'];
                        $temp2['payment_amount'] = $row2['payment_amount'];
                        $temp2['paid_due'] = $temp2['payment_amount'] - $row2['balance_due'];
                        $temp2['rem_due'] = $row2['balance_due'];
                        $temp2['state'] = 'N';
                        $event_details[] = $temp2;
                        $studio_name = $row2['company_name'];
                        $studio_mail = $row2['email_id'];
                        $cc_email_list = $row2['referral_email_list'];
                        $student_cc_email_list = $row2['student_cc_email'];
                    }
                    $current_plan_details['total_event'] = $temp_quantity;
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                cjp_log_info($this->json($error_log));
                return;
            }
        }
        
        $subject = $event_name." - Payment failed";
        
        $message .= "<b>The following payment for the Buyer $buyer_name failed.</b><br><br>";
        $message .= "<b>Event Details</b><br><br>";
        for($i=0;$i<count($event_details);$i++){
            $message .= "Event: ".$event_details[$i]['title']."<br>Starts: ".$event_details[$i]['start']."<br>Ends: ".$event_details[$i]['end']."<br>Participant: ".$participant_name."<br><br>";
        }

        if($total_due>0){
            $message .= "<b>Billing Details</b><br><br>";
            if(count($bill_due_details)>0){
                $message .= "Total due: $".$total_due."<br>";
                $message .= "Balance due: $".$rem_due."<br>";                
                for($j=0;$j<count($bill_due_details);$j++){
                    $x= $j+1;
                    $amount = $bill_due_details[$j]['amount'];
                    $refunded_amount = $bill_due_details[$j]['refunded_amount'];
                    $final_amt = $amount-$refunded_amount;
                    $schedule_status = $bill_due_details[$j]['schedule_status'];
                    $date = date("M dS, Y", strtotime($bill_due_details[$j]['date']));
                    if($schedule_status=='N'){
                        $paid_str = "due";
                    }
                    $message .= "Bill ".$x.": $".$final_amt." ".$paid_str." ".$date."  <br>";
                }
                
                if(count($payment_details)>0){
                    $message.="<br><br><b>Payment Details</b><br><br>";
                    $down_pay = $payment_details[0]['amount']-$payment_details[0]['refunded_amount']." paid on ".date("M dS, Y", strtotime($payment_details[0]['date']))."(".$payment_details[0]['credit_card_name'].")";
                    $message .= "Down Payment: $".$down_pay."<br>";
                    for($k=1;$k<count($payment_details);$k++){
                        $x= $k;
                        $amount = $payment_details[$k]['amount'];
                        $refunded_amount = $payment_details[$k]['refunded_amount'];
                        $final_amt = $amount-$refunded_amount;
                        $schedule_status = $payment_details[$k]['schedule_status'];
                        $date = date("M dS, Y", strtotime($payment_details[$k]['date']));
                        $paid_str = "paid on";
                        if($schedule_status=='S'){
                            $pay_type= "(".$payment_details[$k]['credit_card_name'].")";
                        }else{
                            $pay_type="(manual credit applied)";
                        }
                        $message .= "Payment ".$x.": $".$final_amt." ".$paid_str." ".$date." ".$pay_type." <br>";
                    }
                }
            }else{
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $schedule_status = $payment_details[0]['schedule_status'];
                $final_amount = $amount-$refunded_amount;
                $message .= "Total Due: $".$final_amount."<br>Amount Paid: $".$final_amount."(".$payment_details[0]['credit_card_name'].")<br>";
            }
            $message .= "<br><br>";
        }
        $send_to_studio = 1;
        $sendEmail_status = $this->sendEmailForEvent('', $studio_mail, $subject, $message, '', '', '', 0, $cc_email_list, $student_cc_email_list, $send_to_studio);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $studio_mail."   ".$sendEmail_status['mail_status']);
            cjp_log_info($this->json($error_log));
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $studio_mail);
            cjp_log_info($this->json($error_log));
        }
    }
    
    private function sendEmailForEvent($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list1, $student_cc_email_list1, $send_to_studio) {
        $to_available = 0;
        $bounce_check=$this->sendBounceEmailCheck($to,$cc_email_list1,$student_cc_email_list1);
        
        $cc_email_list=$bounce_check['cc_email_array'];
        $student_cc_email_list=$bounce_check['student_CC_array'];
        $bounced_flag=$bounce_check['bounced_flag'];
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            if ($bounced_flag == 'Y') {
                if (!empty($student_cc_email_list)) {
                    $student_cc_addresses = $student_cc_email_list;
//                $student_cc_addresses = explode(',', $student_cc_email_list);
                    for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                        if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                            $mail->AddAddress($student_cc_addresses[$init_1]);
                            $to_available = 1;
                        }
                    }
                }
                
                if($to_available==0) {
                    return false;
                }
            } else {
                $mail->AddAddress($to);
                if (!empty($student_cc_email_list)) {
                    $student_cc_addresses = $student_cc_email_list;
//                $student_cc_addresses = explode(',', $student_cc_email_list);
                    for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                        if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                            $mail->AddCC($student_cc_addresses[$init_1]);
                        }
                    }
                }
            }

            if($send_to_studio==1){     //send_to_studio - enable/disable sending the email to studio in certain cases
                if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                    $mail->AddCC($from);
                }
                if(!empty($cc_email_list)){
                    $cc_addresses = $cc_email_list;
//                    $cc_addresses = explode(',', $cc_email_list);
                    for($init=0;$init<count($cc_addresses);$init++){
                        if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                            $mail->AddCC($cc_addresses[$init]);
                        }
                    }
                }
            }
           
            
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
//        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
//        cjp_log_info($this->json($error_log));
        return $response;
    }
    
    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }
    
    public function addMembershipDimensions($company_id, $membership_id, $mem_option_id, $category_title, $option_title, $period) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($mem_option_id)) {
            $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                ipn_log_info($this->json($error_log));
                $reg_id = 1;
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $selectsql = sprintf("SELECT `active_members` from membership_dimensions where company_id='%s' and `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s' ",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $pre_dt));
                    $resultselectsql = mysqli_query($this->db, $selectsql);
                    if (!$resultselectsql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        ipn_log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
                        $num_of_rows2 = mysqli_num_rows($resultselectsql);
                        if ($num_of_rows2 == 0) {
                            $sqlselect1=sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and `membership_option_id`='%s' and  `company_id`= '%s'",  mysqli_real_escape_string($this->db,$membership_id),mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db,$company_id));
                            $resultsqlselect1=mysqli_query($this->db, $sqlselect1);
                            $value2 = mysqli_fetch_object($resultsqlselect1);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id),
                                    mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), mysqli_real_escape_string($this->db,$value2->active));
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                ipn_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        } else {
                            $value = mysqli_fetch_object($resultselectsql);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id),
                                    mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), $value->active_members);
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                ipn_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        }
                    }
                }
            }
        }

        $sql2 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            ipn_log_info($this->json($error_log));
            $reg_id = 1;
        } else{
            $num_of_rows3 = mysqli_num_rows($result2);
            if ($num_of_rows3 == 0) {
                $selectsql2 = sprintf("SELECT `active_members` FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $pre_dt));
                $resultselectsql2 = mysqli_query($this->db, $selectsql2);
                if (!$resultselectsql2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql2");
                    ipn_log_info($this->json($error_log));
                    $reg_id = 1;
                } else {
                    $num_of_rows4 = mysqli_num_rows($resultselectsql2);
                    if ($num_of_rows4 == 0) {
                        $sqlselect2 = sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and  `company_id`= '%s'", mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$company_id));
                        $resultsqlselect2 = mysqli_query($this->db, $sqlselect2);
                        $value3 = mysqli_fetch_object($resultsqlselect2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $value3->active));
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            ipn_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    } else {
                        $value1 = mysqli_fetch_object($resultselectsql2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), $value1->active_members);
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            ipn_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for addMembershipDimensions(), check cron log";
            $this->sendEmailForAdmins($msg);
        }
    }
    
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            cjp_log_info($this->json($error_log));
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    public function sendReceiptForMembershipCompletion() {
        $triggerC = $triggerNC = 0;
        $total_records_processed = 0;
        $start_time = gmdate("Y-m-d H:i:s");
        cjp_log_info("sendReceiptForMembershipCompletion() - Started");
        $return_value = 0;
        if (isset($_REQUEST['curr_date'])) {
            $curr_date = $_REQUEST['curr_date'];
        } else {
            $get_date = mysqli_query($this->db, "select CURDATE() curr_date, CURDATE()-INTERVAL 1 DAY prev_date;");
            $row = mysqli_fetch_assoc($get_date);
            $curr_date = $row['curr_date'];
//            $curr_date_1 = "'" . $row['curr_date'] . "','" . $row['prev_date'] . "'";
        }
        
        $sql = sprintf("SELECT  mr.`company_id`, mr.`membership_status`, mr.`payment_frequency`,  mr.`membership_structure`, mr.`membership_registration_id`,mr.`membership_option_id`,mr.`membership_id`, mr.`membership_category_title`,mr.`membership_title`,
                        mr.`billing_options`, mr.`billing_options_no_of_payments`, mr.`billing_options_deposit_amount`, mr.`no_of_payments`, mr.`payment_type`, mr.`no_of_classes`,
                        mr.`specific_end_date`, IFNULL(mr.`billing_options_expiration_date`,'0000-00-00' ) billing_options_expiration_date, IFNULL(mr.`payment_start_date`,'0000-00-00') payment_start_date, 
                        IFNULL((SELECT MAX(`payment_date`) from `membership_payment` WHERE `membership_registration_id`=mr.`membership_registration_id`  AND `payment_status` IN ('N', 'S', 'F', 'P', 'R')),'0000-00-00') last_payment_date,
                        IFNULL((SELECT `payment_status` from `membership_payment` WHERE `membership_registration_id`=mr.`membership_registration_id` AND `payment_status` IN ('N', 'F', 'P') LIMIT 0,1),'') check_payment_status,
                        IF(`membership_structure`!='NC', 1, IFNULL((SELECT COUNT(*) FROM `attendance` WHERE `company_id`= mr.`company_id` AND `membership_registration_id`= mr.`membership_registration_id` AND `status`='N' ),0)) att_count
                        FROM membership_registration mr
                        WHERE mr.`membership_status` IN ('R') AND membership_structure IN ('NC','C','SE') AND (mr.`end_email_date` IS  NULL || mr.`end_email_date`='0000-00-00')");
        $result_sql = mysqli_query($this->db, $sql);
        if (!$result_sql) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            cjp_log_info($this->json($error_log));
            return;
        } else {
            $num_rows = mysqli_num_rows($result_sql);
            if ($num_rows > 0) {                
                $total_records_processed+=$num_rows;
                while ($row1 = mysqli_fetch_assoc($result_sql)) {
                    $company_id = $row1['company_id'];
                    $membership_reg_id = $row1['membership_registration_id'];
                    $membership_registration_id_1 = $row1['membership_registration_id'];
                    $membership_id_1 = $row1['membership_id'];
                    $membership_option_id_1 = $row1['membership_option_id'];
                    $membership_structure = $row1['membership_structure'];
                    $membership_status_old = $row1['membership_status'];
                    $membership_category_title_1 = $row1['membership_category_title'];
                    $membership_title_1 = $row1['membership_title'];
                    $billing_options_no_of_payments = $row1['billing_options_no_of_payments'];
                    $no_of_payments = $row1['no_of_payments'];
                    $billing_options = $row1['billing_options'];
                    $billing_options_deposit_amount = $row1['billing_options_deposit_amount'];
                    $billing_options_expiration_date = $row1['billing_options_expiration_date'];
                    $specific_end_date = $row1['specific_end_date'];
                    $last_payment_date = $row1['last_payment_date'];
                    $check_payment_status = $row1['check_payment_status'];
                    $no_of_classes = $row1['no_of_classes'];
                    $att_count = $row1['att_count'];
                    $payment_type = $row1['payment_type'];
                    if($membership_structure=='SE' && $specific_end_date>$curr_date){
                        continue;
                    }
                    if($membership_structure=='C'){
                        if($billing_options_expiration_date=='0000-00-00'){
                            $triggerC = 1;
                            cjp_log_info("Invalid expiration date in Custom structure(reg_id : $membership_reg_id)");
                            continue;
                        }
                        if($payment_type!='R' && $billing_options_expiration_date>$curr_date){
                            continue;
                        }else{
                            if($billing_options_expiration_date>$curr_date || $last_payment_date>$curr_date){
                                continue;
                            }
                        }
                    }elseif($membership_structure=='NC'){
                        if($no_of_classes==0){
                            $triggerNC = 1;
                            cjp_log_info("Invalid no of classes in Class Package structure(reg_id : $membership_reg_id)");
                            continue;
                        }
                        if($payment_type!='R'){
                            if($billing_options_expiration_date=='0000-00-00'){
                                if($att_count<$no_of_classes){
                                    continue;
                                }
                            }elseif($billing_options_expiration_date>$curr_date && $att_count<$no_of_classes){
                                continue;
                            }
                        }else{
                            if($check_payment_status!=''){
                                continue;
                            }
                            if($billing_options_expiration_date=='0000-00-00'){
                                if($att_count<$no_of_classes || $last_payment_date>$curr_date){
                                    continue;
                                }
                            }else{
                                if($billing_options_expiration_date>$curr_date || $att_count<$no_of_classes || $last_payment_date>$curr_date){
                                    continue;
                                }
                            }
                        }
                    }
                    $this->addMembershipDimensions($company_id, $membership_id_1, $membership_option_id_1, $membership_category_title_1, $membership_title_1, '');
                    $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
                    $payment_details = [];
                    $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
                    $recurring_start_date = $membership_start_date = $payment_start_date = $payment_frequency = $custom_recurring_frequency_period_type = '';
                    $membership_structure = $billing_options = $billing_options_no_of_payments = '';

                    $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, mp.`cc_name`, `waiver_policies`, mr.credit_card_name, 
            mr.`membership_start_date`, mr.`payment_start_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
            `buyer_postal_code`, mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, mp.`created_dt` , mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`
            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id` LEFT JOIN `student` s ON s.`company_id`= mr.`company_id` AND s.`student_id`= mr.`student_id`
            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N') AND membership_structure IN ('NC','C','SE')
            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
                    $result = mysqli_query($this->db, $query);
//                    
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                        cjp_log_info($this->json($error_log));
                        return;
                    } else {
                        $num_rows = mysqli_num_rows($result);
                        if ($num_rows > 0) {
                            while ($row = mysqli_fetch_assoc($result)) {
                                $studio_name = $row['company_name'];
                                $studio_mail = $row['email_id'];
                                $cc_email_list = $row['referral_email_list'];
                                $wp_currency_code = $row['wp_currency_code'];
                                $wp_currency_symbol = $row['wp_currency_symbol'];
                                //   $company_id = $row['company_id'];
                                //   $membership_id = $row['membership_id'];
                                //   $membership_option_id = $row['membership_option_id'];
                                $payment_type = $row['payment_type'];
                                $membership_category_title = $row['membership_category_title'];
                                $membership_title = $row['membership_title'];
                                $membership_structure = $row['membership_structure'];
                                $participant_name = $row['participant_name'];
                                //   $buyer_email = $row['buyer_email'];
//                                $waiver_policies = $row['waiver_policies'];
                                $signup_fee = $row['signup_fee'];
                                $signup_fee_disc = $row['signup_fee_disc'];
                                $mem_fee = $row['membership_fee'];
                                $mem_fee_disc = $row['membership_fee_disc'];
                                $initial_payment = $row['initial_payment'];
                                $inital_processing_fee = $row['initial_processing_fee'];
                                $membership_start_date = $row['membership_start_date'];
                                $payment_start_date = $row['payment_start_date'];
                                $recurring_start_date = $row['recurring_start_date'];
                                $next_payment_date = $row['next_payment_date'];
                                $payment_frequency = $row['payment_frequency'];
                                $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                                $recurring_amount = $row['recurring_amount'];
                                $processing_fee_type = $row['processing_fee_type'];
                                $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
                                $first_payment = $row['first_payment'];
                                $billing_options = $row['billing_options'];
                                $no_of_classes = $row['no_of_classes'];
                                $billing_options_expiration_date = $row['billing_options_expiration_date'];
                                $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                                $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                                $credit_card_name = $row['credit_card_name'];
                                $student_cc_email_list = $row['student_cc_email'];
                                $temp = [];
                                if (!empty($row['membership_payment_id'])) {
                                    if ($recurring_amount == $row['payment_amount']) {
                                        $recurring_processing_fee = $row['processing_fee'];
                                    }
                                    if ($row['processing_fee_type'] == 2) {
                                        $temp['amount'] = $row['payment_amount'] + $row['processing_fee'];
                                    } else {
                                        $temp['amount'] = $row['payment_amount'];
                                    }
                                    $temp['date'] = $row['payment_date'];
                                    $temp['created_dt'] = $row['created_dt'];
                                    $temp['processing_fee'] = $row['processing_fee'];
                                    $temp['payment_status'] = $row['payment_status'];
                                    $temp['paytime_type'] = $row['paytime_type'];
                                    $temp['cc_name'] = $row['cc_name'];
                                    $paid_amt += $temp['amount'];
                                    $payment_details[] = $temp;
                                }
                            }
                            if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE' ) && $billing_options == 'PP') {
                                if ($billing_options_no_of_payments > 1) {
                                    $already_recurring_records_count = 0;
                                    $next_date = '';
                                    for ($i = 0; $i < count($payment_details); $i++) {
                                        if ($payment_details[$i]['paytime_type'] == 'R') {
                                            $already_recurring_records_count++;
                                            $next_date = $payment_details[$i]['date'];
                                        }
                                    }
                                    if ($billing_options_no_of_payments > $already_recurring_records_count) {
                                        $count = $billing_options_no_of_payments - $already_recurring_records_count;
                                        $future_date = $next_date;
                                        for ($j = 0; $j < $count; $j++) {
                                            if ($payment_frequency == 'W') {
                                                $next_payment_date2 = date('Y-m-d', strtotime('+1 weeks', strtotime($future_date)));
                                            } elseif ($payment_frequency == 'B') {
                                                $next_payment_date2 = date('Y-m-d', strtotime('+14 days', strtotime($future_date)));
                                            } elseif ($payment_frequency == 'M') {
                                                $next_payment_date2=$this->getSameDayNextMonth(new DateTime($future_date));
                                            }

                                            $temp_1 = [];
                                            if ($processing_fee_type == 2) {
                                                $temp_1['amount'] = $recurring_amount + $recurring_processing_fee;
                                            } else {
                                                $temp_1['amount'] = $recurring_amount;
                                            }
                                            $temp_1['date'] = $next_payment_date2;
                                            $temp_1['processing_fee'] = $recurring_processing_fee;
                                            $temp_1['payment_status'] = 'N';
                                            $temp_1['paytime_type'] = 'R';
                                            $temp_1['cc_name'] = $credit_card_name;
                                            $payment_details[] = $temp_1;
                                            $future_date = $next_payment_date2;
                                        }
                                    }
                                }
                            }
                        } else {
                            $error_log = array('status' => "Failed", "msg" => "No records found.");
                            cjp_log_info($this->json($error_log));
                            return;
                        }
                    }

                    $subject = $membership_category_title . " Membership Completion Confirmation";

                    $message .= "<b> Please see below for your membership completion details.</b><br><br>";
                    $message .= "<b>Membership Details</b><br><br>";
                    $message .= "Membership Start Date: " . date("M dS, Y", strtotime($membership_start_date)) . "<br>";
                    $message .= "Membership: $membership_category_title, $membership_title<br>Participant: " . $participant_name . "<br>";
                    if ($membership_structure == 'NC' && $billing_options == 'PP' && $billing_options_expiration_date != '0000-00-00') {
                        $message .= "$no_of_classes Class Package Expires: " . date("D, M d, Y", strtotime($billing_options_expiration_date)) . "<br>";
                    }
                    $message .= "<br>";

                    $prorated = "";
                    if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
                        $prorated = "(Pro-rated)";
                    }

                    if ($paid_amt > 0) {
//            if($initial_payment>0){
                        $message .= "<br><b>Registration Details</b><br><br>";
                        $message .= "Sign-up Fee: $wp_currency_symbol" . (number_format(($signup_fee - $signup_fee_disc), 2)) . "<br>";
                        if ($processing_fee_type == 2) {
                            if ($initial_payment > 0) {
                                $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment + $inital_processing_fee), 2)) . " (includes $wp_currency_symbol" . $inital_processing_fee . " administrative fees) $prorated<br>";
                            } else {
                                $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . "<br>";
                            }
                        } else {
                            if ($initial_payment > 0) {
                                $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . " $prorated<br>";
                            } else {
                                $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . "<br>";
                            }
                        }
//            }
                    }

                    if (count($payment_details) > 0) {
                        if ($payment_frequency != 'N' && $payment_type == 'R') {
                            $freq_str = "";
                            if ($membership_structure == 'OE' || $membership_structure == 1) {
                                if ($payment_frequency == 'B') {
                                    $freq_str = "Semi-Monthly Recurring: ";
                                } elseif ($payment_frequency == 'M') {
                                    $freq_str = "Monthly Recurring: ";
                                } elseif ($payment_frequency == 'W') {
                                    $freq_str = "Weekly Recurring: ";
                                } elseif ($payment_frequency == 'A') {
                                    $freq_str = "Annual Recurring: ";
                                } elseif ($payment_frequency == 'C') {
                                    if ($custom_recurring_frequency_period_type == 'CW') {
                                        $freq_str = "Weekly Recurring: ";
                                    } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                        $freq_str = "Monthly Recurring: ";
                                    }
                                }
                            } elseif ($billing_options == 'PP') {
                                if ($payment_frequency == 'W') {
                                    $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
                                } elseif ($payment_frequency == 'B') {
                                    $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
                                } elseif ($payment_frequency == 'M') {
                                    $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
                                }
                            }
                        }

                        $message .= "<br>";
                    }

//                    $waiver_present = 0;
//                    $file = '';
//                    if (!empty(trim($waiver_policies))) {
//                        $waiver_present = 1;
//                        $file_name_initialize = 10;
//                        for ($z = 0; $z < $file_name_initialize; $z++) {
//                            $dt = time();
//                            $file = __DIR__."/../uploads/".$dt."_waiver_policies.html";
//                            if (file_exists($file)) {
//                                sleep(1);
//                                $file_name_initialize++;
//                            } else {
//                                break;
//                            }
//                        }
//                        $ifp = fopen($file, "w");
//                        fwrite($ifp, $waiver_policies);
//                        fclose($ifp);
//                    }


                    //***PAYMENT HISTORY DETAILS***//     


                    $membership_registration_id = $membership_reg_id;
                    $membership_option_title = $membership_title = $payment_amount = $processing_fees = $payment_date = $payment_status = $refunded_amount = $cc_name = $refunded_date = $paytime_type = $payment_type = $payment_steps = '';
                    $prorate_first_payment_flg = $payment_frequency = $next_payment_date = $no_of_payments = $billing_options_no_of_payments = $wp_currency_symbol = $processing_fee_type = $membership_structure = $membership_status = $last_updt_dt = $cmp_name = $reply_to = '';
                    $studio_name = $buyer_email = $studio_mail = $message1 = $category = $type = $history_id = $payment_id = $reg_id = $activity_type = $activity_text = $checkout_status = $membership_id = $membership_option_id = '';
                    $payment_history_details = [];
                    $initial_check = $temp_no_payments =$num = 0;

                    $sql_reg = sprintf("SELECT *, 'membership' category FROM (
                    SELECT 'payment' as type, '' history_id, mp.`membership_payment_id` payment_id, mp.`membership_registration_id` reg_id, mp.`payment_amount`,mp.`processing_fee`,mp.`payment_date`,mp.`payment_status`,mp.`refunded_amount`,mp.`refunded_date`,mp.`paytime_type`,mp.`cc_name`,DATE(mp.`last_updt_dt`) as last_updt_dt,
                    mr.`buyer_email`,mr.`membership_category_title`, mr.`membership_title`,mr.`membership_status`,mr.`membership_structure`,mr.`processing_fee_type`,mr.`prorate_first_payment_flg`, '' activity_type, '' activity_text, mp.checkout_status, mr.`membership_id`, mr.`membership_option_id`,
                    mr.`payment_frequency`,mr.`no_of_payments`,mr.`billing_options_no_of_payments`,
                    IF(((mr.`membership_structure`='NC' || mr.`membership_structure`='C' || mr.`membership_structure`='SE') && mr.`billing_options`='PP'), IF(mp.`paytime_type`='I', 'Down Payment', 'Payment'), '') payment_steps,c.`wp_currency_symbol`,c.`company_name`, c.`email_id`
                    
                    FROM `membership_payment` mp LEFT JOIN `membership_registration` mr ON mp.`membership_registration_id` = mr.`membership_registration_id` AND mp.`company_id` = mr.`company_id` LEFT JOIN `company` c ON c.`company_id`=mr.`company_id`
                    WHERE mr.company_id='%s' AND mr.membership_registration_id= '%s' 
                    UNION
                    SELECT 'history' as type, `membership_payment_history_id` history_id, `membership_payment_id` payment_id, `membership_registration_id` reg_id, '' payment_amount, '' processing_fee, DATE(`activity_date_time`) payment_date, '' payment_status, '' refunded_amount, '' refunded_date, '' paytime_type, '' cc_name, '' last_updt_dt, 
                    '' buyer_email,'' membership_category_title,'' membership_title,(SELECT `membership_status` FROM `membership_registration` WHERE `membership_registration_id`=mph.`membership_registration_id` AND `company_id`=mph.`company_id`) membership_status, '' membership_structure, '' processing_fee_type, '' prorate_first_payment_flg, `activity_type`, `activity_text`,
                    '' checkout_status, '' membership_id, '' membership_option_id,''payment_frequency,''no_of_payments,''billing_options_no_of_payments,''wp_currency_symbol,''company_name,''email_id,''payment_steps
                    FROM `membership_payment_history` mph WHERE `company_id`='%s' AND `membership_registration_id`='%s')t
                    ORDER BY t.`payment_id`, `payment_date`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_registration_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_registration_id));
                    $result_reg_details = mysqli_query($this->db, $sql_reg);

                    if (!$result_reg_details) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_reg");
                        cjp_log_info($this->json($error_log));
                        return;
                    } else {
                        $num_rows = mysqli_num_rows($result_reg_details);
                        if ($num_rows > 0) {
                            while ($row1 = mysqli_fetch_assoc($result_reg_details)) {
                                $payment_history_details[] = $row1;
                                $category = $row1['category'];
                                $type = $row1['type'];
                                $history_id = $row1['history_id'];
                                $payment_id = $row1['payment_id'];
                                $reg_id = $row1['reg_id'];
                                $activity_type = $row1['activity_type'];
                                $activity_text = $row1['activity_text'];
                                $payment_date = $row1['payment_date'];
                                $processing_fees = $row1['processing_fee'];
                                $payment_amount = $row1['payment_amount'];
                                $payment_status = $row1['payment_status'];
                                $refunded_amount = $row1['refunded_amount'];
                                $refunded_date = $row1['refunded_date'];
                                $checkout_status = $row1['checkout_status'];
                                $membership_id = $row1['membership_id'];
                                $membership_option_id = $row1['membership_option_id'];
                                $membership_title = $row1['membership_category_title'];
                                $paytime_type = $row1['paytime_type'];
                                $cc_name = $row1['cc_name'];
                                $last_updt_dt = $row1['last_updt_dt'];
                                $membership_structure = $row1['membership_structure'];
                                $processing_fee_type = $row1['processing_fee_type'];
                                $membership_status = $row1['membership_status'];
                                $membership_option_title = $row1['membership_title'];
                                $wp_currency_symbol = $row1['wp_currency_symbol'];
                                $prorate_first_payment_flg = $row1['prorate_first_payment_flg'];
                                $studio_name = $row1['company_name'];
//                                   $buyer_email = $row1['buyer_email'];
                                $studio_mail = $row1['email_id'];
                                $payment_steps = $row1['payment_steps'];
                                $payment_frequency = $row1['payment_frequency'];
                                $no_of_payments = $row1['no_of_payments'];
                                $billing_options_no_of_payments = $row1['billing_options_no_of_payments'];
                                if ($initial_check == 0) {
                                    $message1 .= "<b>Membership Payment History for  $membership_option_title</b><br><br>";
                                }
                                if ($type == 'history') {
                                    $message1 .= date("M dS, Y", strtotime($payment_date));
                                    $message1 .= " $activity_text ";
                                    $message1 .= "<br>";
                                } else if ($membership_structure == 'OE') {
                                    $message1 .= date("M dS, Y", strtotime($last_updt_dt));
                                    if ($payment_status == 'S') {
                                        $message1 .= " Payment: ";
                                    } else if ($payment_status == 'F') {
                                        $message1 .= " Past Due: ";
                                    } else if ($payment_status == 'R') {
                                        $message1 .= " Refunded: ";
                                    } else if ($payment_status == 'M') {
                                        $message1 .= " Mannual Credit Applied: ";
                                    } else if ($payment_status == 'MR') {
                                        $message1 .= " Manually Refunded: ";
                                    } else {
                                        $message1 .= "Scheduled Payment: ";
                                    }
                                    $message1 .= "$wp_currency_symbol";
                                    if ($payment_status == 'R') {
                                        if ($processing_fee_type == '2') {
                                            $message1 .= $refunded_amount + $processing_fees;
                                        } else {
                                            $message1 .= "$refunded_amount ";
                                        }
                                    } else {
                                        if ($processing_fee_type == '2') {
                                            $message1 .= $payment_amount + $processing_fees;
                                        } else {
                                            $message1 .= "$payment_amount ";
                                        }
                                    }
                                    if ($processing_fee_type == '2') {
                                        $message1 .= " (includes $wp_currency_symbol$processing_fees administrative fees) ";
                                    }
                                    if (isset($cc_name)) {
                                        $message1 .= "($cc_name)";
                                    }
                                    if ($prorate_first_payment_flg == 'Y') {
                                        $message1 .= "(Pro-rated)";
                                    }
                                    $message1 .= " Bill due :" . date("M dS, Y", strtotime($payment_date));
                                    $message1 .= "<br>";
                                }
                                if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') && ($payment_status != 'N')) {

                                    if ($payment_steps == 'Down Payment') {
                                        $message1 .= "$payment_steps : ";
                                    } else if ($payment_steps == 'Payment') {
                                        $num++ ;
                                    $message1 .= "$payment_steps $num : ";
                                    }
                                    $message1 .= "$wp_currency_symbol";

                                    if ($payment_status == 'R') {
                                        if ($processing_fee_type == '2') {
                                            $message1 .= $refunded_amount + $processing_fees;
                                        } else {
                                            $message1 .= "$refunded_amount ";
                                        }
                                    } else {
                                        if ($processing_fee_type == '2') {
                                            $message1 .= $payment_amount + $processing_fees;
                                        } else {
                                            $message1 .= "$payment_amount ";
                                        }
                                    }
                                    if ($processing_fee_type == '2') {
                                        $message1 .= " (includes $wp_currency_symbol$processing_fees administrative fees) ";
                                    }
                                    if (isset($cc_name)) {
                                        $message1 .= "($cc_name) ";
                                    }
                                    if ($payment_status == 'S') {
                                        $message1 .= "paid on ";
                                    } else if ($payment_status == 'F') {
                                        $message1 .= "past due on ";
                                    } else if ($payment_status == 'R') {
                                        $message1 .= "refunded on ";
                                    } else if ($membership_status == 'P') {
                                        $message1 .= "on hold ";
                                    } else if ($payment_status == 'M') {
                                        $message1 .= " Mannual Credit Applied: ";
                                    } else if ($payment_status == 'MR') {
                                        $message1 .= " Manually Refunded: ";
                                    }
                                    if ($membership_status != 'P') {
                                        $message1 .= date("M dS, Y", strtotime($payment_date));
                                    }
                                    $message1 .= "<br>";
                                }

                                $initial_check++;
                            }

                            $message = $message . "<br>" . $message1;


                            //*** PAYMENT HISTORY DETAILS END*** //

                            $send_to_studio = 1;
                            $sendEmail_status = $this->sendEmailForMembership('',$studio_mail, $subject, $message, '', '', '', 0, $cc_email_list, $student_cc_email_list, $send_to_studio);
                            if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
                                $curr_date_time = date("Y-m-d ");
                                $sql_insert_date = sprintf("UPDATE membership_registration SET end_email_date ='%s' WHERE company_id='%s'  AND membership_registration_id='%s' ",
                                        mysqli_real_escape_string($this->db, $curr_date_time), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
                                $sql_insert_date_result = mysqli_query($this->db, $sql_insert_date);

                                if (!$sql_insert_date_result) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_insert_date");
                                    cjp_log_info($this->json($error_log));
                                } else {
                                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $studio_mail . "   " . $sendEmail_status['mail_status']);
                                    cjp_log_info($this->json($error_log));
                                }
                                $log = array('status' => "Success", "msg" => "Membership  Completion receipt sent successfully.");
                                cjp_log_info($this->json($log));
                            } else {
                                $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $studio_mail);
                                cjp_log_info($this->json($error_log));
                            }
                        }
                    }
                    $date_str = "%Y-%m";
                    $every_month_dt = gmdate("Y-m-d");

                    $mem_string = "`active_members`=`active_members`-1,`reg_completed`=`reg_completed`+1";
                    if (!empty(trim($mem_string))) {
                        $query1 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`=0",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id_1), $every_month_dt, $date_str);
                        $result1 = mysqli_query($this->db, $query1);
                        if (!$result1) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                            cjp_log_info($this->json($error_log));
                        }
                    }
                    if (!empty(trim($mem_string))) {
                        $query2 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`='%s'",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id_1), $every_month_dt, $date_str, mysqli_real_escape_string($this->db, $membership_option_id_1));
                        $result2 = mysqli_query($this->db, $query2);
                        if (!$result2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                            cjp_log_info($this->json($error_log));                            
                        }
                    }
                    if ($membership_status_old == 'P') {
                        $mem_string_1 = "`members_onhold`=`members_onhold`-1";
                        $mem_opt_string = "`options_members_onhold`=`options_members_onhold`-1";
                    } else {
                        $mem_string_1 = "`members_active`=`members_active`-1";
                        $mem_opt_string = "`options_members_active`=`options_members_active`-1";
                    }
                if(!empty(trim($mem_string_1))){
                    $query22 = sprintf("UPDATE `membership` SET $mem_string_1 WHERE `company_id`='%s' AND `membership_id`='%s'",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id_1));
                    $result22 = mysqli_query($this->db, $query22);
                    if (!$result22) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query22");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
                    
                if(!empty(trim($mem_opt_string))){
                    $query33 = sprintf("UPDATE `membership_options` SET $mem_opt_string WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", 
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id_1),
                            mysqli_real_escape_string($this->db, $membership_option_id));
                    $result33 = mysqli_query($this->db, $query33);
                    if (!$result33) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query33");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
                    $query3 = sprintf("UPDATE `membership_registration` SET `membership_status`='S',`mem_reg_completion_date`=CURDATE() WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_registration_id`='%s' ",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id_1), mysqli_real_escape_string($this->db, $membership_registration_id_1));
                    $result3 = mysqli_query($this->db, $query3);                    
                    if (!$result3) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                        cjp_log_info($this->json($error_log));                        
                    }
                    
                }
            }
        }
        if($triggerC==1){
            $this->sendEmailForAdmins("Custom Structure invalid Expiration date");
        }
        if($triggerNC==1){
            $this->sendEmailForAdmins("Class Packages Structure invalid no of classes");
        }
        cjp_log_info("sendReceiptForMembershipCompletion() - Completed");
        if($total_records_processed>0){
            $end_time = gmdate("Y-m-d H:i:s");
            $cron_file_name = "RecurringPaymentHandler";
            $cron_function_name = "sendReceiptForMembershipCompletion";
            $cron_message = "Total membership completion receipt processed";
            $cron_log_insert = sprintf("INSERT INTO `cron_log`(`cron_file_name`, `cron_function_name`, `start_time`, `end_time`, `message`, `records_count`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $cron_file_name), 
                    mysqli_real_escape_string($this->db, $cron_function_name), mysqli_real_escape_string($this->db, $start_time), mysqli_real_escape_string($this->db, $end_time), mysqli_real_escape_string($this->db, $cron_message), $total_records_processed);
            $cron_log_result = mysqli_query($this->db, $cron_log_insert);
            if(!$cron_log_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$cron_log_result");
                cjp_log_info($this->json($error_log));
            }
        }
    }
    
    
    private function sendBounceEmailCheck($to1,$cc_email_list1,$student_cc_email_list1){
        $cc_email_list1 = preg_replace('/\s*,\s*/', ',', $cc_email_list1);
        $student_cc_email_list1 = preg_replace('/\s*,\s*/', ',', $student_cc_email_list1);
        $student_CC_array = explode(",",trim($student_cc_email_list1));
        $student_CC_list = implode("','", $student_CC_array);
        $cc_email_array = explode(",",$cc_email_list1);
        $cc_email_list= implode("','", $cc_email_array);
        $out=[];
        $bounced_flag='N';
        $sql= sprintf("SELECT 'T' as flag, count(*) as b_flag, '' bounced_mail FROM `bounce_email_verify` WHERE `bounced_mail`='%s'
                    UNION
                    SELECT 'S' as flag, 0 as b_flag, `bounced_mail` FROM `bounce_email_verify` WHERE `bounced_mail` in ('%s')
                    UNION
                    SELECT 'C' as flag, 0 as b_flag, `bounced_mail` FROM `bounce_email_verify` WHERE `bounced_mail` in ('%s')",
                    mysqli_real_escape_string($this->db, $to1), $student_CC_list,  $cc_email_list);
        $result= mysqli_query($this->db, $sql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
             cjp_log_info($this->json($error_log));
        }else{
            if(mysqli_num_rows($result)>0){
                while($rows = mysqli_fetch_assoc($result)){
                  if($rows['flag']=='S'){
                       if(in_array($rows['bounced_mail'], $student_CC_array)){                           
                           array_splice($student_CC_array, array_search($rows['bounced_mail'], $student_CC_array), 1);
                       }
                    
                  }else if($rows['flag']=='C'){
                    
                      if(in_array($rows['bounced_mail'], $cc_email_array)){
                           array_splice($cc_email_array, array_search($rows['bounced_mail'], $cc_email_array), 1);
                       }
                  }else if($rows['flag']=='T'){
                      if($rows['b_flag']>0){
                          $bounced_flag ='Y';
                      }
                  }
                }
            }
        }
         $out['cc_email_array'] = $cc_email_array;
         $out['student_CC_array'] = $student_CC_array;
         $out['bounced_flag'] = $bounced_flag;
         return $out;
    }
    
    public function getSameDayNextMonth(DateTime $startDate, $numberOfMonthsToAdd = 1) {
        $startDateDay = (int) $startDate->format('j');
        $startDateMonth = (int) $startDate->format('n');
        $startDateYear = (int) $startDate->format('Y');

        $numberOfYearsToAdd = floor(($startDateMonth + $numberOfMonthsToAdd) / 12);
        if ((($startDateMonth + $numberOfMonthsToAdd) % 12) === 0) {
          $numberOfYearsToAdd--;
        }
        $year = $startDateYear + $numberOfYearsToAdd;

        $month = ($startDateMonth + $numberOfMonthsToAdd) % 12;
        if ($month === 0) {
          $month = 12;
        }
        $month = sprintf('%02s', $month);

        $numberOfDaysInMonth = (new DateTime("$year-$month-01"))->format('t');
        $day = $startDateDay;
        if ($startDateDay > $numberOfDaysInMonth) {
          $day = $numberOfDaysInMonth;
        }
        $day = sprintf('%02s', $day);

        $output=new DateTime("$year-$month-$day");
        return $output->format('Y-m-d');
    }

}

$api = new RecurringPaymentHandler;
$api->processApi();
?>