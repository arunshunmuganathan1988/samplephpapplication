<?php

require_once __DIR__."/../../Globals/cont_log.php";
require_once 'WePay.php';
require_once 'PHPMailer_5.2.1/class.phpmailer.php';
require_once 'PHPMailer_5.2.1/class.smtp.php';
//require_once __DIR__.'/../../aws/aws-autoloader.php';
require_once __DIR__.'/../../aws/aws.phar';
require_once '../../Stripe/init.php';

use Aws\Sns\SnsClient;

class TrialModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $server_url;
    private $stripe_pk = '', $stripe_sk = '';
    
    public function __construct() {
        require_once __DIR__."/../../Globals/config.php";
        require_once  __DIR__ . "/../../Globals/stripe_props.php";
        $this->inputs();
        $this->wp = new wepay();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = __DIR__."/../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'dev.mystudio.academy') !== false){  //Development
            $this->server_url = "http://dev.mystudio.academy";
        }elseif(strpos($host, 'dev2.mystudio.academy') !== false){  //Development
            $this->server_url = "http://dev2.mystudio.academy";
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME'];
        }else{
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = (select if('%s'='S','S','W'))", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = (select if('%s'='S','S','W'))", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }
                }
            }
        }
    }
    
    protected function wepayStatus($company_id){
        $query = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if(!empty(trim($curr))){
                    $currency = explode(",", trim($curr));
                }
                if($row['account_state']=='active'){
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                }else{
                    $res = array('status' => "Success", "msg" => "A", "info" => "Deleted/Disabled/Action needed.");
                }
            }else{
                $res = array('status' => "Failed", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }
    
    protected function stripeStatus($company_id){
        $query = sprintf("SELECT * FROM `stripe_account` where `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        
        $account_state = '';
        
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if(!empty(trim($curr))){
                    $currency = explode(",", trim($curr));
                }
                if($row['account_state'] === 'Y'){
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                }else{
                    $res = array('status' => "Success", "msg" => "Y", "info" => "Deleted/Disabled/Action needed.");
                }
            }else{
                $res = array('status' => "Failed", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }
    
    
    //Get trial details by Company ID
    public function getTrialDetailByCompany($company_id, $reg_type_user) {
        $this->getStripeKeys();
        $stripe_publish_key = $this->stripe_pk;
        $this->getStudioSubscriptionStatus($company_id);
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $stripe_upgrade_status = $stripe_subscription = '';
        //Get Trial POS Status
        if($reg_type_user!='U'){
            $selecsql=sprintf("SELECT s.trial_status FROM `studio_pos_settings` s WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
            $selectresult= mysqli_query($this->db,$selecsql);
            if (!$selectresult) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else{
                if (mysqli_num_rows($selectresult) > 0) {
                    $statusoutput = mysqli_fetch_assoc($selectresult);
                    if($statusoutput['trial_status']=='D'){
                        $res = array('status' => "Failed", "msg" => "Trial POS Setting is disabled. Please enable this in POS Settings.");
                        $this->response($this->json($res), 200);
                    }
                }
            }
        }
        
        $trial_list_url_init = 0;
        $company_name = '';
        $sql = sprintf("SELECT `trial_id`, t.`company_id`, t.`trial_status`, `trial_title`, `trial_subtitle`, `trial_desc`, `trial_prog_url`, `trial_welcome_url`, `trial_banner_img_url`, `trial_banner_img_content`, `trial_waiver_policies`,`registrations_count` registration_count, `net_sales`, `processing_fee_type`, 
            `price_amount`, `program_length_type`, `program_length`, `trial_sort_order`,  `trial_registration_column_1`, `trial_registration_column_1_flag`, `trial_registration_column_2`, `trial_registration_column_2_flag`, `trial_registration_column_3`, `trial_registration_column_3_flag`, `trial_registration_column_4`, 
            `trial_registration_column_4_flag`, `trial_registration_column_5`, `trial_registration_column_5_flag`, `trial_registration_column_6`, `trial_registration_column_6_flag`, `trial_registration_column_7`, `trial_registration_column_7_flag`, `trial_registration_column_8`, `trial_registration_column_8_flag`, 
            `trial_registration_column_9`, `trial_registration_column_9_flag`, `trial_registration_column_10`, `trial_registration_column_10_flag`,`trial_lead_source_1`,  `trial_lead_source_2`,  `trial_lead_source_3`, 
             `trial_lead_source_4`, `trial_lead_source_5`,  `trial_lead_source_6`,  `trial_lead_source_7`, `trial_lead_source_8`, 
            `trial_lead_source_9`,  `trial_lead_source_10`, c.`company_code`,IF(t.`trial_status`='P','published',IF(t.`trial_status`='S','draft','unpublished')) list_type,
             c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`, c.`company_id`, c.`logo_URL`, c.`country`, c.`upgrade_status`, c.`stripe_status`, c.`stripe_subscription`, wp.account_state, wp.currency, pos.`trial_payment_method`
        FROM `trial` t LEFT JOIN company c on t.`company_id`=c.`company_id` LEFT JOIN `wp_account` wp ON t.`company_id` = wp.`company_id` LEFT JOIN `studio_pos_settings` pos ON t.`company_id` = pos.`company_id` where c.`company_id`='%s' and t.`trial_status`='P' ORDER BY `trial_sort_order` desc",  mysqli_real_escape_string($this->db,$company_id));
        
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else { 
            $wepay_status = 'N';$currency = [];
            $output =  $response['live'] = $response['draft'] = $response['unpublished'] = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $company_name = $row['company_name'];
                    if(!empty(trim($row['logo_URL']))){
                        $logo_URL = $row['logo_URL'];
                    }else{
                        $logo_URL = $this->server_url."/uploads/Default/default_logo.png";
                    }
                    $upgrade_status = $row['upgrade_status'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    $pos_trial_payment_method = $row['trial_payment_method'];
                    $stripe_upgrade_status = $row['stripe_status'];
                    $stripe_subscription = $row['stripe_subscription'];
                    $country = $row['country'];
                    if(!empty($row['account_state'])){
                        $account_state = $row['account_state'];
                        $curr = $row['currency'];
                        if(!empty(trim($curr))){
                            $currency = explode(",", trim($curr));
                        }
                        $wepay_status = 'Y';
                    }
                   
                    $disc = $this->getTrialDiscountDetails($company_id, $row['trial_id']);
                    $row['discount'] = $disc['discount'];
                    $reg_field_name_array = [];
                    $temp_reg_field = "trial_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "trial_registration_column_".$z."_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row['reg_columns'] = $reg_field_name_array;
                    $lead_name_array = [];
                    $temp_lead_field = "trial_lead_source_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_lead = $temp_lead_field."$z";      //for filed names
                        if($row[$reg_lead]!=''){
                            $lead_name_array[] = array("lead_col_name"=>$row[$reg_lead],"lead_col_index"=>$y);
                        }
                    } 
                  $row['lead_columns'] = $lead_name_array;
                    
                   $output[] = $row;
                }
                
                $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                $company_details['stripe_publish_key'] = $stripe_publish_key;  // KEY FROM - Globals/stripe_props.php
                $company_details['stripe_upgrade_status'] = $stripe_upgrade_status;
                $company_details['stripe_subscription'] = $stripe_subscription;
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $country_payment_specific = $this->checkCountryPaymentSupport($company_id,$country,1); // callback - 1/0
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = $process_fee['PT'];
                $company_details['pos_trial_payment_method'] = $pos_trial_payment_method;
                $company_details['stripe_country_support'] = $stripe_country_support = ($country_payment_specific['status'] == 'Success' && $country_payment_specific['support_status'] =='Y' && $country_payment_specific['payment_support'] =='S') ? "Y":"N";  // CHECK COUNTRY SUPPORT STATUS FOR STRIPE
                
                for ($i = 0; $i < count($output); $i++) {//split-up by live, past & draft
                    if ($output[$i]['list_type'] == 'published') {
                        $response['live'][] = $output[$i];
                    } elseif ($output[$i]['list_type'] == 'draft') {
                        $response['draft'][] = $output[$i];
                    } elseif ($output[$i]['list_type'] == 'unpublished') {
                        $response['unpublished'][] = $output[$i];
                    }
                }
                usort($response['live'], function($a, $b) {
                    return $a['trial_sort_order'] < $b['trial_sort_order'];
                });
                $out = array('status' => "Success", 'msg' => $response['live'], 'company_details' => $company_details);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
             } else {
                $error = array('status' => "Failed", "msg" => "Trial details not available.");
                $this->response($this->json($error), 401);
             }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
     public function getTrialDiscountDetails($company_id, $trial_id){
        $sql = sprintf("SELECT `trial_discount_id` , `discount_type` , `discount_code` , `discount_amount` FROM `trial_discount`
                WHERE `company_id` = '%s' AND `trial_id`='%s' AND `is_expired`!='Y'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id));
        $result = mysqli_query($this->db, $sql);
        $output = $response = [];
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $output[] = $row;
                }
                $response['status'] = "Success";
            }else{
                $response['status'] = "Failed";
            }
        }
        $response['discount'] = $output;
        return $response;
    }
    
    public function getTrialProgramDetails($company_id, $trial_id, $reg_type_user) {//call_back, 0 -> returned & exit, 1 -> return response
        $this->getStripeKeys();
        $stripe_publish_key = $this->stripe_pk;
        $this->getStudioSubscriptionStatus($company_id);
        $stripe_upgrade_status = $stripe_subscription = '';
        //Get Membership POS Status
        if($reg_type_user!='U'){
            $selecsql=sprintf("SELECT s.trial_status FROM `studio_pos_settings` s WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
            $selectresult= mysqli_query($this->db,$selecsql);
            if (!$selectresult) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else{
                if (mysqli_num_rows($selectresult) > 0) {
                    $statusoutput = mysqli_fetch_assoc($selectresult);
                    if($statusoutput['trial_status']=='D'){
                        $res = array('status' => "Failed", "msg" => "Trial POS Setting is disabled. Please enable this in POS Settings.");
                        $this->response($this->json($res), 200);
                    }
                }
            }
        }
        
        //check array_slice in following before alter or add any columns in this query
        $sql = sprintf("SELECT `trial_id`, t.`company_id`, `trial_status`, `trial_title`, `trial_subtitle`, `trial_desc`, `trial_prog_url`, `trial_welcome_url`, `trial_banner_img_url`, `trial_banner_img_content`, `trial_waiver_policies`, `net_sales`, `processing_fee_type`, 
            `price_amount`, `program_length_type`, `program_length`, `trial_sort_order`,  `trial_registration_column_1`, `trial_registration_column_1_flag`, `trial_registration_column_2`, `trial_registration_column_2_flag`, `trial_registration_column_3`, `trial_registration_column_3_flag`, `trial_registration_column_4`, 
            `trial_registration_column_4_flag`, `trial_registration_column_5`, `trial_registration_column_5_flag`, `trial_registration_column_6`, `trial_registration_column_6_flag`, `trial_registration_column_7`, `trial_registration_column_7_flag`, `trial_registration_column_8`, `trial_registration_column_8_flag`, 
            `trial_registration_column_9`, `trial_registration_column_9_flag`, `trial_registration_column_10`, `trial_registration_column_10_flag`,`trial_lead_source_1`, `trial_lead_source_2`,  `trial_lead_source_3`, 
             `trial_lead_source_4`, `trial_lead_source_5`, `trial_lead_source_6`,  `trial_lead_source_7`,  `trial_lead_source_8`, 
            `trial_lead_source_9`,  `trial_lead_source_10`,IF(t.`trial_status`='P','published',IF(t.`trial_status`='S','draft','unpublished')) list_type,
            c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`, c.`company_id`, c.`logo_URL`, c.`upgrade_status`, c.`stripe_status`, c.`stripe_subscription`, wp.account_state, wp.currency
        FROM `trial` t LEFT JOIN company c on t.`company_id`=c.`company_id` LEFT JOIN `wp_account` wp ON t.`company_id` = wp.`company_id` where `trial_id`='%s' and t.`company_id`='%s' and `trial_status`='P'", mysqli_real_escape_string($this->db,$trial_id), mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
//        log_info($sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
          $trial_program_list = [];
          $currency = []; $wepay_status = 'N';     
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                    $company_name = $row['company_name'];
                    if(!empty(trim($row['logo_URL']))){
                        $logo_URL = $row['logo_URL'];
                    }else{
                        $logo_URL = $this->server_url."/uploads/Default/default_logo.png";
                    }
                    $upgrade_status = $row['upgrade_status'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    $stripe_upgrade_status = $row['stripe_status'];
                    $stripe_subscription = $row['stripe_subscription'];
                    if(!empty($row['account_state'])){
                        $account_state = $row['account_state'];
                        $curr = $row['currency'];
                        if(!empty(trim($curr))){
                            $currency = explode(",", trim($curr));
                        }
                        $wepay_status = 'Y';
                    }
                    $reg_columns =  [];
                    $trial_id = $row['trial_id'];
                   $row_category = array_slice($row, 0, 17);
                    $reg_field_name_array = [];
                    $temp_reg_field = "trial_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "trial_registration_column_".$z."_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row_category['reg_columns'] = $reg_field_name_array;
                   
                    $temp_lead_field = "trial_lead_source_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_lead = $temp_lead_field."$z";      //for filed names
                       if($row[$reg_lead]!=''){
                            $lead_name_array[] = array("lead_col_name"=>$row[$reg_lead],"lead_col_index"=>$y);
                        }
                    } 
                  $row_category['lead_columns'] = $lead_name_array;
                   $row_category['list_type'] = $row['list_type'];
                   
             }
             
             $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                $company_details['stripe_publish_key'] = $stripe_publish_key;  // KEY FROM - Globals/stripe_props.php
                $company_details['stripe_upgrade_status'] = $stripe_upgrade_status;
                $company_details['stripe_subscription'] = $stripe_subscription;
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = $process_fee['PT'];
                $company_details['stripe_country_support'] = $stripe_country_support = 'Y';  // CHECK COUNTRY SUPPORT STATUS FOR STRIPE
             
                 $disc = $this->getTrialDiscountDetails($company_id, $row_category['trial_id']);
                    $row_category['discount'] = $disc['discount'];
             $trial_program_list = $row_category;
                    
                $out = array('status' => "Success", 'msg' => $trial_program_list, 'company_details' => $company_details);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
             } else {
                $error = array('status' => "Failed", "msg" => "Trial details not available.");
                $this->response($this->json($error), 401);
             }
        }
    }    
    
    //Get trial details by Company ID
    public function getTrialDetailByCompanyForSocialSharing($company_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        
        $trial_list_url_init = 0;
        $company_name = '';
        $sql = sprintf("SELECT `trial_id`, t.`company_id`, `trial_status`, `trial_title`, `trial_subtitle`, `trial_desc`, `trial_prog_url`, `trial_welcome_url`, `trial_banner_img_url`, `trial_banner_img_content`, `trial_waiver_policies`,`registrations_count` registration_count, `net_sales`, `processing_fee_type`, 
            `price_amount`, `program_length_type`, `program_length`, `trial_sort_order`,  `trial_registration_column_1`, `trial_registration_column_1_flag`, `trial_registration_column_2`, `trial_registration_column_2_flag`, `trial_registration_column_3`, `trial_registration_column_3_flag`, `trial_registration_column_4`, 
            `trial_registration_column_4_flag`, `trial_registration_column_5`, `trial_registration_column_5_flag`, `trial_registration_column_6`, `trial_registration_column_6_flag`, `trial_registration_column_7`, `trial_registration_column_7_flag`, `trial_registration_column_8`, `trial_registration_column_8_flag`, 
            `trial_registration_column_9`, `trial_registration_column_9_flag`, `trial_registration_column_10`, `trial_registration_column_10_flag`,`trial_lead_source_1`,  `trial_lead_source_2`,  `trial_lead_source_3`, 
             `trial_lead_source_4`, `trial_lead_source_5`,  `trial_lead_source_6`,  `trial_lead_source_7`, `trial_lead_source_8`, 
            `trial_lead_source_9`,  `trial_lead_source_10`, c.`company_code`,IF(t.`trial_status`='P','published',IF(t.`trial_status`='S','draft','unpublished')) list_type,
             c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`, c.`company_id`, c.`logo_URL`, c.`upgrade_status`, wp.account_state, wp.currency
        FROM `trial` t LEFT JOIN company c on t.`company_id`=c.`company_id` LEFT JOIN `wp_account` wp ON t.`company_id` = wp.`company_id` where c.`company_id`='%s' and `trial_status`='P' ORDER BY `trial_sort_order` desc",  mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "code"=>500, "title"=>"Server Error");
            return $error;
        } else { 
            $wepay_status = 'N';$currency = [];
            $output =  $response['live'] = $response['draft'] = $response['unpublished'] = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $company_name = $row['company_name'];
                    $logo_URL = $row['logo_URL'];
                    $upgrade_status = $row['upgrade_status'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    if(!empty($row['account_state'])){
                        $account_state = $row['account_state'];
                        $curr = $row['currency'];
                        if(!empty(trim($curr))){
                            $currency = explode(",", trim($curr));
                        }
                        $wepay_status = 'Y';
                    }
                   
                    $disc = $this->getTrialDiscountDetails($company_id, $row['trial_id']);
                    $row['discount'] = $disc['discount'];
                    $reg_field_name_array = [];
                    $temp_reg_field = "trial_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "trial_registration_column_".$z."_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row['reg_columns'] = $reg_field_name_array;
                    $lead_name_array = [];
                    $temp_lead_field = "trial_lead_source_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_lead = $temp_lead_field."$z";      //for filed names
                        if($row[$reg_lead]!=''){
                            $lead_name_array[] = array("lead_col_name"=>$row[$reg_lead],"lead_col_index"=>$y);
                        }
                    } 
                  $row['lead_columns'] = $lead_name_array;
                    
                   $output[] = $row;
                }
                
                $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = $process_fee['PT'];
                
                for ($i = 0; $i < count($output); $i++) {//split-up by live, past & draft
                    if ($output[$i]['list_type'] == 'published') {
                        $response['live'][] = $output[$i];
                    } elseif ($output[$i]['list_type'] == 'draft') {
                        $response['draft'][] = $output[$i];
                    } elseif ($output[$i]['list_type'] == 'unpublished') {
                        $response['unpublished'][] = $output[$i];
                    }
                }
                usort($response['live'], function($a, $b) {
                    return $a['trial_sort_order'] < $b['trial_sort_order'];
                });
                $out = array('status' => "Success", 'msg' => $response['live'][0], 'company_details' => $company_details);
                // If success everythig is good send header as "OK" and user details
                return $out;
             } else {
                $error = array('status' => "Failed", "msg" => "Trial details not available.","code"=>404, "title"=>"404 Error");
                return $error;
             }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function getTrialProgramDetailsForSocialSharing($company_id, $trial_id) {//call_back, 0 -> returned & exit, 1 -> return response
        //check array_slice in following before alter or add any columns in this query
        $sql = sprintf("SELECT `trial_id`, t.`company_id`, `trial_status`, `trial_title`, `trial_subtitle`, `trial_desc`, `trial_prog_url`, `trial_welcome_url`, `trial_banner_img_url`, `trial_banner_img_content`, `trial_waiver_policies`, `net_sales`, `processing_fee_type`, 
            `price_amount`, `program_length_type`, `program_length`, `trial_sort_order`,  `trial_registration_column_1`, `trial_registration_column_1_flag`, `trial_registration_column_2`, `trial_registration_column_2_flag`, `trial_registration_column_3`, `trial_registration_column_3_flag`, `trial_registration_column_4`, 
            `trial_registration_column_4_flag`, `trial_registration_column_5`, `trial_registration_column_5_flag`, `trial_registration_column_6`, `trial_registration_column_6_flag`, `trial_registration_column_7`, `trial_registration_column_7_flag`, `trial_registration_column_8`, `trial_registration_column_8_flag`, 
            `trial_registration_column_9`, `trial_registration_column_9_flag`, `trial_registration_column_10`, `trial_registration_column_10_flag`,`trial_lead_source_1`, `trial_lead_source_2`,  `trial_lead_source_3`, 
             `trial_lead_source_4`, `trial_lead_source_5`, `trial_lead_source_6`,  `trial_lead_source_7`,  `trial_lead_source_8`, 
            `trial_lead_source_9`,  `trial_lead_source_10`,IF(t.`trial_status`='P','published',IF(t.`trial_status`='S','draft','unpublished')) list_type,
            c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`, c.`company_id`, c.`logo_URL`, c.`upgrade_status`, wp.account_state, wp.currency
        FROM `trial` t LEFT JOIN company c on t.`company_id`=c.`company_id` LEFT JOIN `wp_account` wp ON t.`company_id` = wp.`company_id` where `trial_id`='%s' and t.`company_id`='%s'", mysqli_real_escape_string($this->db,$trial_id), mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
//        log_info($sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "code"=>500, "title"=>"Server Error");
            return $error;
        } else {
            $trial_program_list = [];
            $currency = []; $wepay_status = 'N';     
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                    $company_name = $row['company_name'];
                    $logo_URL = $row['logo_URL'];
                    $upgrade_status = $row['upgrade_status'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    if(!empty($row['account_state'])){
                        $account_state = $row['account_state'];
                        $curr = $row['currency'];
                        if(!empty(trim($curr))){
                            $currency = explode(",", trim($curr));
                        }
                        $wepay_status = 'Y';
                    }
                    $reg_columns =  [];
                    $trial_id = $row['trial_id'];
                   $row_category = array_slice($row, 0, 17);
                    $reg_field_name_array = [];
                    $temp_reg_field = "trial_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "trial_registration_column_".$z."_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row_category['reg_columns'] = $reg_field_name_array;
                   
                    $temp_lead_field = "trial_lead_source_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_lead = $temp_lead_field."$z";      //for filed names
                       if($row[$reg_lead]!=''){
                            $lead_name_array[] = array("lead_col_name"=>$row[$reg_lead],"lead_col_index"=>$y);
                        }
                    } 
                    $row_category['lead_columns'] = $lead_name_array;
                    $row_category['list_type'] = $row['list_type'];
                }
             
                $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = $process_fee['PT'];
             
                $disc = $this->getTrialDiscountDetails($company_id, $row_category['trial_id']);
                $row_category['discount'] = $disc['discount'];
                $trial_program_list = $row_category;
                    
                $out = array('status' => "Success", 'msg' => $trial_program_list, 'company_details' => $company_details);
                // If success everythig is good send header as "OK" and user details
                return $out;
             } else {
                $error = array('status' => "Failed", "msg" => "Trial details not available.","code"=>404, "title"=>"404 Error");
                return $error;
             }
        }
    }
    
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone='';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result_tz);                       
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    //wepay trial program checkout
   
    
    private function calculateProcessingFee($amount, $fee_type, $process_fee_per, $process_fee_val) {
        $p1_fee = ($amount * ($process_fee_per / 100)) + $process_fee_val;
        $p2_fee = ($amount + $p1_fee) * ($process_fee_per / 100) + $process_fee_val;
        if ($fee_type === 2 || $fee_type === '2') {
            $p_fee = $p2_fee;
        } else {
            $p_fee = $p1_fee;
        }
        return $p_fee;
    }
    
    public function trialProgramCheckout($optional_discount_flag,$optional_discount,$check_number,$payment_method,$company_id, $trial_id, $actual_student_id, $actual_participant_id, $trial_name, $desc,  $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount,  $processing_fee_type, $discount,  $payment_type, $upgrade_status,  $discount_code, $reg_type_user, $reg_version_user,$program_length,$program_length_type,$start_date,$participant_street, $participant_city, $participant_state, $participant_zip, $participant_country){
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $upgrade_status = $sts['upgrade_status'];
        $this->addTrialDimensions($company_id, $trial_id,'');
        $buyer_name = $buyer_first_name.' '.$buyer_last_name;
        if (is_null($actual_student_id) || $actual_student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Wepay', '');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $actual_student_id;
        }
        if (is_null($actual_participant_id) || $actual_participant_id == 0) {
            $participant_id = $this->getParticipantId($company_id, $student_id, $reg_col1, $reg_col2, $reg_col3);
        } else {
            $participant_id = $actual_participant_id;
        }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        
        $pr_fee = $temp_pr_fee = 0;
        $current_date = date("Y-m-d");
        if($program_length_type == 'D'){
            $end_date = date("Y-m-d", strtotime("+$program_length days",strtotime($start_date)));
        }else{
            $end_date = date("Y-m-d", strtotime("+$program_length week",strtotime($start_date)));
        }
        
        
//        $check_deposit_failure = 0;
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg1=array("buyer_email"=>$buyer_email,"membership_title"=>$trial_name,"gmt_date"=>$gmt_date);
        $p_f = $this->getProcessingFee($company_id, $upgrade_status);
        $process_fee_per = $p_f['PP'];
        $process_fee_val = $p_f['PT'];
        
        
        if ($payment_amount > 0 && $payment_method == "CC") {
            $payment_type = 'O';
            $processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
        } else {
            if ($payment_amount > 0) {
                $payment_type = 'O';
                $processing_fee = 0;
            } else {
                $payment_type = 'F';
                $processing_fee = 0;
            }
        }


        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency` FROM `wp_account` WHERE `company_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                if($user_state=='deleted'){
                    $msg = "Wepay user registered was deleted.";
                    $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
                    $this->response($this->json($log),200);
                }
                
                if($acc_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log),200);
                }elseif($acc_state=='disabled'){
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log),200);
                }
                
                if(!empty($access_token) && !empty($account_id)){
                    if (!function_exists('getallheaders')){
                        if(!function_exists ('apache_request_headers')){
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        }else{
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    }else{
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        
                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                    
                    $w_paid_amount = $paid_amount = $paid_fee = $gross = 0;
                    $checkout_id = $checkout_state = '';                    
                    $cc_name = $cc_month = $cc_year = '';
                    $cc_status = $cc_state;
                    $temp_payment_status = 'M';
                    
                    if($payment_amount>0 && $payment_method == "CC"){
                        $temp_payment_status = 'S';
                        $checkout_id_flag = 1;
                        $rand = mt_rand(1,99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout?for=trial";
                        $unique_id = $company_id."_".$trial_id."_".$ip."_".$rand."_".time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id."_".$trial_id."_".$date;
//                        if($processing_fee_type==2){
//                            $fee_payer = "payer_from_app";
//                        }elseif($processing_fee_type==1){
                            $fee_payer = "payee_from_app";
//                        }
                        $paid_amount = $payment_amount;
                        $paid_fee = $processing_fee;
                        
                        if($processing_fee_type==2){
                            $w_paid_amount = $payment_amount + $processing_fee;
                        }elseif($processing_fee_type==1){
                            $w_paid_amount = $payment_amount;
                        }
                        
                        
                        $time = time();
                        $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                            "fee"=>array("app_fee"=>$paid_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                            "email_message"=>array("to_payee"=>"Payment has been added for $trial_name","to_payer"=>"Payment has been made for $trial_name"),
                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                            "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code","country"=>"$country")))),
                            "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                "properties"=>array("itemized_receipt"=>array(array("description"=>"$trial_name", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                        $postData2 = json_encode($json2);
                        $sns_msg1['call'] = "Checkout";
                        $sns_msg1['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                        $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $gross = $res2['gross'];
//                            $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                        }else{
                            if($response2['msg']['error_code']==1008){
                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                                $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                                if($response2['status']=="Success"){
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $gross = $res2['gross'];
//                                    $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                                }else{
                                    if($response2['msg']['error_code']==1008){
                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error),400);
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),400);
                                    }
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error),400);
                            }
                        }
                    }else{
                        $checkout_id_flag = 0;
                        if(!empty(trim($cc_id))){
                            $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                            $postData3 = json_encode($json3);
                            $sns_msg1['call'] = "Credit Card";
                            $sns_msg1['type'] = "Credit card details";
                            $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                            $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                            if($response3['status']=="Success"){
                                $res3 = $response3['msg'];
                                if($res3['state']=='new'){
                                    $json2 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id,"account_id"=>$account_id);
                                    $postData2 = json_encode($json2);                                    
                                    $sns_msg1['call'] = "Credit Card";
                                    $sns_msg1['type'] = "Credit Card Authorization";
                                    $response2 = $this->wp->accessWepayApi("credit_card/authorize",'POST',$postData2,$token,$user_agent,$sns_msg1);
                                    $this->wp->log_info("wepay_credit_card_authorize: ".$response2['status']);
                                    if($response2['status']=="Success"){
                                        $res2 = $response2['msg'];
                                        $cc_name = $res2['credit_card_name'];
                                        $cc_new_state = $res2['state'];
                                        if($cc_state!=$cc_new_state){
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res2['expiration_month'];
                                        $cc_year = $res2['expiration_year'];
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),400);
                                    }
                                }elseif($res3['state']=='authorized'){
                                    $cc_name = $res3['credit_card_name'];
                                    $cc_new_state = $res3['state'];
                                    $cc_month = $res3['expiration_month'];
                                    $cc_year = $res3['expiration_year'];
                                }else{
                                    if($res3['state']=='expired'){
                                        $msg = "Given credit card is expired.";
                                    }elseif($res3['state']=='deleted'){
                                        $msg = "Given credit card was deleted.";
                                    }else{
                                        $msg = "Given credit card details is invalid.";
                                    }
                                    $error = array("status" => "Failed", "msg" => $msg);
                                    $this->response($this->json($error),400);
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                $this->response($this->json($error),400);
                            }
                        }

                    }
                    
                    if(!empty(trim($cc_id))){
                        $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                        $postData3 = json_encode($json3);
                        $sns_msg1['call'] = "Credit Card";
                        $sns_msg1['type'] = "Credit Card details";
                        $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                        $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                        if($response3['status']=="Success"){
                            $res3 = $response3['msg'];
                            $cc_name = $res3['credit_card_name'];
                            $cc_new_state = $res3['state'];
                            if($cc_state!=$cc_new_state){
                                $cc_status = $cc_new_state;
                            }
                            $cc_month = $res3['expiration_month'];
                            $cc_year = $res3['expiration_year'];
                        }else{
                            for($k=0;$k<2;$k++){
                                if($response3['msg']['error_code'] == 503){
                                    sleep(2);
                                    $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                                    $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                                    if($response3['status']=="Success"){
                                        $res3 = $response3['msg'];
                                        $cc_name = $res3['credit_card_name'];
                                        $cc_new_state = $res3['state'];
                                        if($cc_state!=$cc_new_state){
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res3['expiration_month'];
                                        $cc_year = $res3['expiration_year'];
                                        break;
                                    }
                                    if($k==1){
                                        if($checkout_id_flag == 1){
                                            $type = $response3['msg']['error'];
                                            $description =  $response3['msg']['error_description']." Buyer Name:".$buyer_name." Buyer Email:".$buyer_email." Checkout Id:".$checkout_id." Participant Name:".$reg_col1." ".$reg_col2;      
                                            $insert_alertquery = sprintf("INSERT INTO `wepay_critical_alert`(`company_id`, `type`, `description`) VALUES ('%s','%s','%s')",mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $type),mysqli_real_escape_string($this->db, $description));
                                            $result_alert = mysqli_query($this->db, $insert_alertquery);
                                            if(!$result_alert){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_alertquery");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 500);
                                            } 
                                            $type1 = $response3['msg']['error']." Regarding Credit Card";
                                            $description1 = "Fetching Credit Card details failed"."\n"." Buyer Name:".$buyer_name."\n"." Buyer Email:".$buyer_email."\n"." Checkout Id:".$checkout_id."\n"." Participant Name:".$reg_col1." ".$reg_col2;     
                                            $this->sendSnsforfailedcheckout($type1,$description1);
                                        }
                                        $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                        $this->response($this->json($error),400);
                                    }
                                }else{
                                    $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                    $this->response($this->json($error),400);
                                }
                            }
                        }
                    }
                    
                     
                    $query = sprintf("INSERT INTO `trial_registration`(`optional_discount_flag`,`check_number`,`payment_method`,`company_id`, `trial_id`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                        `payment_type`, `payment_amount`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`,  `start_date`, `end_date`, `trial_status`, `discount`,
                        `trial_registration_column_1`, `trial_registration_column_2`, `trial_registration_column_3`, `trial_registration_column_4`, `trial_registration_column_5`, `trial_registration_column_6`, `trial_registration_column_7`, `trial_registration_column_8`, `trial_registration_column_9`, `trial_registration_column_10`,
                        `trial_reg_type_user`,`trial_reg_version_user`,`program_length`,`program_length_type`,`trial_participant_street`, `trial_participant_city`, `trial_participant_state`, `trial_participant_zip`, `trial_participant_country`)
                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $optional_discount_flag), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), 
                            mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db,$participant_id),mysqli_real_escape_string($this->db, $buyer_first_name),mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code),
                            mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_type), 
                            mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name),
                            mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year),  mysqli_real_escape_string($this->db, $start_date),mysqli_real_escape_string($this->db, $end_date), mysqli_real_escape_string($this->db, 'A'), mysqli_real_escape_string($this->db, $discount),
                            mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4),
                            mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), 
                            mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $program_length),mysqli_real_escape_string($this->db, $program_length_type),mysqli_real_escape_string($this->db, $participant_street), mysqli_real_escape_string($this->db, $participant_city),
                            mysqli_real_escape_string($this->db, $participant_state), mysqli_real_escape_string($this->db, $participant_zip), mysqli_real_escape_string($this->db, $participant_country));
                    $result = mysqli_query($this->db, $query);
                    if(!$result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $trial_reg_id = mysqli_insert_id($this->db);

                        if ($payment_amount > 0) {
                            $payment_query = sprintf("INSERT INTO `trial_payment`(`credit_method`,`check_number`,`trial_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `payment_date`, `payment_status`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name));
                            $payment_result = mysqli_query($this->db, $payment_query);
                            if (!$payment_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                                mlog_info($this->json($error_log));
                            }
                        }

                         if ($processing_fee_type == 1) {
                            $paid_amount -= $processing_fee;
                        }

                        if ($payment_method != "CC") {
                            $update_sales_in_reg = sprintf("UPDATE `trial_registration` SET `paid_amount`=`paid_amount`+$payment_amount WHERE `trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_id));
                            $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);

                            if (!$result_sales_in_reg) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                                ipn_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }

                            $update_count = sprintf("UPDATE `trial` SET `registrations_count` = `registrations_count`+1,`active_count`=`active_count`+1, `net_sales`=`net_sales`+$paid_amount WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
                            $result_count = mysqli_query($this->db, $update_count);

                            if (!$result_count) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                                ipn_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                            $this->updateTrialDimensionsNetSales($company_id,$trial_id,'');
                        }else{
                            $update_trial_query = sprintf("UPDATE `trial` SET `registrations_count` = `registrations_count`+1,`active_count`=`active_count`+1  WHERE `trial_id`='%s'", mysqli_real_escape_string($this->db, $trial_id));
                            $update_trial_result = mysqli_query($this->db, $update_trial_query);
                            if(!$update_trial_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial_query");
                                mlog_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }
                        
                        $activity_text = 'Registration date.';
                        $activity_type = "registration";

                        if (!empty($discount_code)) {
                            $activity_text .= " Trial fee discount code used is " . "$discount_code";
                        }

                        $curr_date = gmdate("Y-m-d H:i:s");
                    $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), $activity_type, 
                            mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                    $result_history = mysqli_query($this->db, $insert_history);
                    if (!$result_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                        
                     $this->updateTrialDimensionsMembers($company_id, $trial_id, 'A','',$trial_reg_id);
                        $msg = array("status" => "Success", "msg" => "Trial program registration was successful. Email confirmation sent.");
                        $this->responseWithWepay($this->json($msg),200);
                        $this->sendOrderReceiptForTrialPayment($company_id,$trial_reg_id);
                    }
                }
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error),200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function addTrialDimensions($company_id, $trial_id, $period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if (!empty($period)) {
            $curr_dt = date("Y-m", strtotime($period));
        } else {
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));


        $sql2 = sprintf("SELECT * FROM `trial_dimensions` WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows1 = mysqli_num_rows($result2);
            if ($num_of_rows1 == 0) {
                $insertquery1 = sprintf("INSERT INTO `trial_dimensions`(`company_id`, `period`, `trial_id`) VALUES('%s','%s','%s')", 
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $trial_id));
                $res2 = mysqli_query($this->db, $insertquery1);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    $this->response($this->json($error), 200);
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
     public function updateTrialDimensionsMembers($company_id, $trial_id, $trial_status,$old_trial_status,$trial_reg_id) {
        $flag = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $date_string = $trial_string = "";
//        $a_date = $e_date = $c_date = $d_date = '';
        $date_str = "%Y-%m";$reg_date = '';
        $every_month_dt = date("Y-m-d");
        
//        if(!empty($old_date['reg'])){
//            $old_date['active'] = $old_date['reg'];
//        }
//        if(!empty($old_date)){
//        $a_date = "'".$old_date['active']."'";
//        $e_date = "'".$old_date['enrolled']."'";
//        $c_date = "'".$old_date['cancelled']."'";
//        $d_date = "'".$old_date['didnotstart']."'";
//       }
       if ($trial_status == 'A') {
            if ($old_trial_status == 'E') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
                 $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'C') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'D') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } elseif ($trial_status == 'E') {
            if ($old_trial_status == 'A') {
//                $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                 $trial_string = "`active_count`=`active_count`-1";
            } else if ($old_trial_status == 'C') {
//                 $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                  $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'D') {
//                $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                 $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } elseif ($trial_status == 'C') {
             if ($old_trial_status == 'E') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
               $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'A') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                 $trial_string = "`active_count`=`active_count`-1";
            } else if ($old_trial_status == 'D') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } else if ($trial_status == 'D') {
            if ($old_trial_status == 'E') {
//                 $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
                 $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'C') {
//                  $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                   $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'A') {
//                 $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                   $trial_string = "`active_count`=`active_count`-1";
            }
        }
//
//        $selectquery = sprintf("SELECT * FROM `trial_registration` WHERE $date_string and  `company_id`='%s' and `trial_id`='%s' and `trial_reg_id`='%s'",
//                $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $trial_reg_id));
//        $res = mysqli_query($this->db, $selectquery);
//       if (!$res) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
//            log_info($this->json($selectquery));
//            
//        } else {
//            $num_of_rows = mysqli_num_rows($res);
//            if ($num_of_rows == 1 && !empty($old_trial_status)) {
//                $flag = 1;
//            }
//        }
          $selectquery = sprintf("SELECT registration_date FROM `trial_registration` WHERE `company_id`='%s' and `trial_id`='%s' and `trial_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $trial_reg_id));
        $res = mysqli_query($this->db, $selectquery);
       if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
            mlog_info($this->json($selectquery));
            
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows == 1) {
               $rows = mysqli_fetch_object($res);
               $reg_date = $rows->registration_date;
            }
        }
      
        if (!empty($old_trial_status)) {
            if($trial_status == 'A'){
//                if($flag == 1){
                  $trial_string = "$trial_string, `active_count`=`active_count`+1";
//                }else{
//                  $trial_string = "`active_count`=`active_count`+1";
//                }
            }else if($trial_status == 'E'){
//                if($flag == 1){
                    $trial_string = "$trial_string, `enrolled_count`=`enrolled_count`+1";
//                }else{
//                     $trial_string = "`enrolled_count`=`enrolled_count`+1";
//                }
                
            }else if($trial_status == 'C'){
//                if($flag == 1){
                    $trial_string = "$trial_string, `cancelled_count`=`cancelled_count`+1";
//                }else{
//                     $trial_string = "`cancelled_count`=`cancelled_count`+1";
//                }
                
            }else if($trial_status == 'D'){
//                if($flag == 1){
                    $trial_string = "$trial_string, `didnotstart_count`=`didnotstart_count`+1";
//                }else{
//                     $trial_string = "`didnotstart_count`=`didnotstart_count`+1";
//                }
                
            }
            
        } else {
            $trial_string = "`active_count`=`active_count`+1";
            $reg_date = $every_month_dt;
        }

        if (!empty(trim($trial_string))) {
            $query1 = sprintf("UPDATE `trial_dimensions` SET $trial_string WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') ",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), $reg_date, $date_str);
            $result1 = mysqli_query($this->db, $query1);
//            log_info("dim   ".$query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
        }
        
        date_default_timezone_set($curr_time_zone);
    }
    
      public function sendOrderReceiptForTrialPayment($company_id,$trial_reg_id){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $created_date =" DATE(CONVERT_TZ(tp.created_dt,$tzadd_add,'$new_timezone'))";
       
        $waiver_policies = '';
        $query = sprintf("SELECT tr.`trial_status`, tr.`buyer_name`, tr.`buyer_email`, tr.`buyer_phone`, tr.`buyer_postal_code`,tr.`start_date`,tr.`end_date`,
                tr.`registration_date`, tr.`registration_amount`, tr.`processing_fee_type`, tr.`payment_type`, c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`,
                tr.`program_length`, tp.`credit_method`,tp.`check_number`,tr.`program_length_type`, concat(tr.`trial_registration_column_2`,',',tr.`trial_registration_column_1`) participant,  tr.`trial_registration_column_4`
                lead_Source,t.`trial_title`,t.`trial_waiver_policies`,tp.`payment_date`,tp.`payment_amount`,tp.`processing_fee`,tp.`payment_status`,tp.`refunded_amount`,tp.`refunded_date`,if(tp.`payment_from`='S',tp.`stripe_card_name`,tp.`cc_name`) as cc_name,DATE($created_date) created_dt,tp.`last_updt_dt` FROM `trial_registration` tr 
                left join `company` c on(c.`company_id`=tr.`company_id`) left join `student` s on(tr.`student_id`=s.`student_id`) left join `trial` t
                on(t.`trial_id`=tr.`trial_id`) left join `trial_payment` tp on(tr.`trial_reg_id`=tp.`trial_reg_id`) 
                WHERE tr.`trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            exit();
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
                $trial_name = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $student_cc_email_list = $wp_currency_code = $wp_currency_symbol =$processing_fee_type= '';
                $current_plan_details = $trial_details =  $payment_details = [];
                 while($row = mysqli_fetch_assoc($result)){
                    $paid_amt = 0;
                    $trial_name = $row['trial_title'];
                    $participant_name = $row['participant'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['trial_waiver_policies'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $payment_status = $row['payment_status'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $student_cc_email_list = $row['student_cc_email'];
                    $start_date  = date("M dS, Y", strtotime($row['start_date']));
                    $end_date  = date("M dS, Y", strtotime($row['end_date']));
                    $temp=[];
                    
                    if($row['processing_fee_type']==2){
                      $payment_amount  = $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                    }else{
                       $payment_amount = $temp['amount'] = $row['payment_amount'];
                    }
                    $temp['date'] = $row['payment_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
//                    $temp['edit_status'] = 'N';
                    $temp['payment_status'] = $row['payment_status'];
                    $temp['processing_fee'] = $row['processing_fee']; 
                    $temp['cc_name'] = $row['cc_name'];
                    $temp['credit_method'] = $row['credit_method'];
                    $temp['check_number'] = $row['check_number'];
                   
                        $payment_details[] = $temp;
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                log_info($this->json($error_log));
                 $error = array('status' => "Failed", "msg" => "No records found.");
                    $this->response($this->json($error), 200);
                exit();
            }
        }
        
        $subject = $trial_name." Order Confirmation";
        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
        $message .= "<b>Order Details</b><br><br>";
        $message .= "$trial_name"."<br>";
        $message .= "Starts: "."$start_date"."<br>";
        $message .= "Ends: "."$end_date"."<br>";
        $message .= "Participant: "."$participant_name"."<br><br>";
          
        if(($payment_status == 'S' || $payment_status == 'M') && $payment_details[0]['amount'] > 0){
            $message .= "<b>Billing Details</b><br><br>";
            $message .= "Total Due: "."$wp_currency_symbol$payment_amount"."<br>";
            if(count($payment_details)>0){
                $check_number = $payment_details[0]['check_number'];
                $pf_str = "";
                $pr_fee = $payment_details[0]['processing_fee'];
                if($processing_fee_type==2 && $payment_details[0]['credit_method'] == 'CC'){
                    $pf_str = " (includes $wp_currency_symbol".$pr_fee." administrative fees)";
                }
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $final_amt = $amount-$refunded_amount;
                $payment_status = $payment_details[0]['payment_status'];
//                        $date = date("M dS, Y", strtotime($payment_details[0]['date']));
                $paid_str = "Amount paid: ";
                if($payment_status == 'S'){
                    $pay_type= "(".$payment_details[0]['cc_name'].")";
                }else{
                    if($payment_details[0]['credit_method'] == 'CA'){
                        $pay_type="(Cash)";
                    }else{
                        $pay_type="(Check #$check_number)";
                    }
                }
                $message .= " $paid_str"."$wp_currency_symbol$payment_amount"."$pf_str$pay_type"." <br>";
            }
            $message .= "<br><br>";
        }
        
        $waiver_present = 0;
        $file = '';
        if(!empty(trim($waiver_policies))){
            $waiver_present = 1;
            $file_name_initialize = 10;
            for($z=0; $z<$file_name_initialize; $z++){
                $dt = time();
                $file = "../../uploads/".$dt."_waiver_policies.html";        
                if(file_exists($file)){
                    sleep(1);
                    $file_name_initialize++;
                }else{
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }

        $sendEmail_status = $this->sendEmailForTrial($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list,$student_cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            mlog_info($this->json($error_log));
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            mlog_info($this->json($error_log));
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function sendEmailForTrial($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list,$student_cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if(!empty(trim($student_cc_email_list))){
                $student_cc_addresses = explode(',', $student_cc_email_list);
                for($init_1=0;$init_1<count($student_cc_addresses);$init_1++){
                    if(filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($student_cc_addresses[$init_1]);
                    }
                }
            }
            
            $mail->Subject = $subject;
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        mlog_info($this->json($error_log));
        return $response;
    }   
    
    
    private function getRealIpAddr(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet        
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    private function sendSnsforfailedcheckout($sub,$msg){
    
            $topicarn = 'arn:aws:sns:us-east-1:241334827363:mystudio';
         
                    $subject = $sub;
    //                $subject = $sns_sub . " -transaction failed";
                    $message = $msg;

                    $credentials = new Aws\Credentials\Credentials('AKIAIGUWYNJGAHDPJGIA', 'w4oKbP0UhJZr1W0hGuULhkdEoxEHY2YxEq/6uycT');               
                    $sns = SnsClient::factory([
                        'credentials' => $credentials,
                        'region' => 'us-east-1',
                        'version' => 'latest'
                    ]);
    //                log_info("object cretaed");
                    $result1 = $sns->publish(array(
                        'TopicArn' => $topicarn,
                        // Message is required
                        'Message' => $message,
                        'Subject' => $subject,
                        //'MessageStructure' => 'string',
                        'MessageAttributes' => array(
                            // Associative array of custom 'String' key names
                            'String' => array(
                                // DataType is required
        //            'DataType' => '',
                                'DataType' => 'String',
                                'StringValue' => '200',
                            ),
                        // ... repeated
                        ),
                    ));
               
    }
    
    public function getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $payment_processor, $studio_name) {
        $select_student = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$company_id));
        $result_select_student = mysqli_query($this->db, $select_student);
        if (!$result_select_student) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student);
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result_select_student) > 0) {
                $r = mysqli_fetch_assoc($result_select_student);
                $student_id = $r['student_id'];
                if(is_null($r['stripe_customer_id']) || empty(trim($r['stripe_customer_id']))){
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = $r['stripe_customer_id'];
                    }
                }else{
                    $stripe_customer_id = $r['stripe_customer_id'];
                }
            } else {
                $insert_student = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_lastname`, `student_email`) VALUES('%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), 
                        mysqli_real_escape_string($this->db,$buyer_first_name), mysqli_real_escape_string($this->db,$buyer_last_name), mysqli_real_escape_string($this->db,$buyer_email));
                $result_insert_student = mysqli_query($this->db, $insert_student);
                if (!$result_insert_student) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student);
                    $this->response($this->json($error), 200);
                } else {
                    $student_id = mysqli_insert_id($this->db);
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = '';
                    }
                }
            }
            return array("student_id" => $student_id, "stripe_customer_id" => $stripe_customer_id);
        }
    }
    
    public function getParticipantId($company_id, $student_id, $first_name, $last_name, $dob) {

        $select_participant2 = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name), mysqli_real_escape_string($this->db, $dob));
        $result_select_participant2 = mysqli_query($this->db, $select_participant2);
        if (!$result_select_participant2) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_participant2);
            $this->response($this->json($error), 500);
        } else {
            if (mysqli_num_rows($result_select_participant2) > 0) {
                $r = mysqli_fetch_assoc($result_select_participant2);
                $participant_id = $r['participant_id'];
                $deleted_flag = $r['deleted_flag'];
                if($deleted_flag=='D'){
                    $update_participant = sprintf("UPDATE `participant` SET `deleted_flag`='N' WHERE `participant_id`='%s'", mysqli_real_escape_string($this->db, $participant_id));
                    $result_update_participant = mysqli_query($this->db, $update_participant);
                    if (!$result_update_participant) {
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_participant);
                        $this->response($this->json($error), 500);
                    }
                }
            } else {
                $insert_participant2 = sprintf("INSERT INTO `participant`(`company_id`, `student_id`, `participant_first_name`, `participant_last_name`, `date_of_birth`) VALUES('%s', '%s', '%s', '%s', '%s')",
                        $company_id, $student_id, mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name), $dob);
                $result_insert_participant2 = mysqli_query($this->db, $insert_participant2);
                if (!$result_insert_participant2) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_participant2);
                    $this->response($this->json($error), 500);
                } else {
                    $participant_id = mysqli_insert_id($this->db);
                }
            }
            return $participant_id;
        }
    }
    
    public function getStudioSubscriptionStatus($company_id){
        $query = sprintf("SELECT email_id,upgrade_status,subscription_status,wepay_status,stripe_status,stripe_subscription,studio_expiry_level,expiry_dt FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_email = $row['email_id'];
                $upgrade_status = $row['upgrade_status'];
                $subscription_status = $row['subscription_status'];
                $wepay_status = $row['wepay_status'];
                $stripe_status = $row['stripe_status'];
                $stripe_subscription = $row['stripe_subscription'];
                $studio_expiry_level= $row['studio_expiry_level'];
                $expiry_dt = $row['expiry_dt'];
                if (!empty(trim($upgrade_status)) && $subscription_status=='N' && ($studio_expiry_level=='L2' || $upgrade_status=='F')) {
                    $res = array('status' => "Failed", "msg" => "Please contact the business operator to activate this link.", "studio_email"=>$studio_email, "upgrade_status"=>$upgrade_status, "studio_expiry_level" => $studio_expiry_level);
                    $this->response($this->json($res), 200);
                }else{
                    $res = array('status' => "Success", "msg" => "Studio subscription status success", "upgrade_status" => $upgrade_status);
                    return $res;
                }                
            } else {
                $res = array('status' => "Failed", "msg" => "Studio Details not found.");
                $this->response($this->json($res), 200);
            }
        }
    }
    
    public function sendExpiryEmailDetailsToStudio($company_id){
        $query = sprintf("SELECT c.`email_id`, c.`company_name`, c.`referral_email_list` FROM `company` c WHERE c.`company_id`='%s'", $company_id);
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $company_name = $reply_to = "";
                $row = mysqli_fetch_assoc($result);
                $owner_email_id = $row['email_id'];
                $referral_email_list = $row['referral_email_list'];
                
                $subject = "A member has requested that you activate your MyStudio account.";
                $body_msg = '<div style="max-width:520px;">';
                $body_msg .= "Members are trying to access your app and/or URL registration links.<br><br>However, your account is currently inactive, please click here to activate your account.<br>";
                $body_msg .= '<center><a href="'.$this->server_url.'/mystudioapp.php?pg=login"><button type="button" style="width: 140px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 500;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 40px;font-size: 15px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">Activate</button></a></center>';
                $body_msg .= "</div>";
                $sendEmail_status = $this->sendEmail($owner_email_id, $referral_email_list, $subject, $body_msg, $company_name, $reply_to, true);
                if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
                    $refer_check = 1;
                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $owner_email_id . "   " . $sendEmail_status['mail_status']);
                    log_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                } else {
                    $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $owner_email_id);
                    log_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                }                
            }else{
                $error = array('status' => "Failed", "msg" => "Studio details doesn't exist.");
                $this->response($this->json($error), 401);
            }
        }
    }
    
    protected function sendEmail($to, $cc_email_list, $subject, $message, $cmp_name, $reply_to, $isHtml) {

        if (empty(trim($cmp_name))) {
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try {
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if (filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)) {
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML($isHtml);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    $mail->AddCC($cc_addresses[$init]);
                }
            }
            if($isHtml){                
                $mail->CharSet = 'UTF-8';
                $mail->Encoding = 'base64';
            }
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        } catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    protected function checkpossettings($company_id,$payment_method, $pos_email, $token) {
        $this->verifyStudioPOSToken($company_id, $pos_email, $token);
        $sql = sprintf("SELECT trial_status,trial_payment_method from studio_pos_settings where company_id = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                if($row['trial_status'] === "D"){
                    $error = array('status' => "Failed", "msg" => "Trial POS is disabled");
                    $this->response($this->json($error), 200);
                }
                $payment_binary = $row['trial_payment_method'];
                if($payment_method === "CA"){
                    if($payment_binary[1] === "0"){
                        $error = array('status' => "Failed", "msg" => "Payment method cash is disabled");
                        $this->response($this->json($error), 200);
                    }
                }
                if($payment_method === "CH"){
                    if($payment_binary[2] === "0"){
                        $error = array('status' => "Failed", "msg" => "Payment method check is disabled");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
    }
    
    public function checkStudioAndEmail($company_id, $email){
        $query = sprintf("SELECT * FROM `user` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Studio details.", "session_status" => "Expired");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function verifyStudioPOSToken($company_id, $email, $token) {
        $this->checkStudioAndEmail($company_id, $email);
        $query = sprintf("SELECT * FROM `pos_token` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $old_token = $row['token'];
                $token_id = $row['pos_token_id'];
                if($old_token==$token){
                    $query2 = sprintf("UPDATE `pos_token` SET `no_of_access`=`no_of_access`+1 WHERE `pos_token_id`='%s'", mysqli_real_escape_string($this->db, $token_id));
                    $result2 = mysqli_query($this->db, $query2);                
                    if(!$result2){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $msg = array("status"=>"Success", "msg"=>"Session is active.");
                    return $msg;
                }else{
                    $msg = array("status"=>"Failed", "msg"=>"Session expired.", "session_status" => "Expired");
                    $this->response($this->json($msg), 200);
                }
            }else{
                $msg = array("status"=>"Failed", "msg"=>"Session expired.", "session_status" => "Expired");
                $this->response($this->json($msg), 200);
            }
        }
    }
    
    public function updateTrialDimensionsNetSales($company_id, $trial_id, $date) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $mp_last_upd_date=" DATE(CONVERT_TZ(tp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $last_upd_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";
        if(empty($date)){
            $curr_dt = "($currdate_add)";
        }else{
            $curr_dt = "'$date'";
        }
//        $sql = sprintf("SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, sum(if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
//                  mp.payment_date FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
//                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
//                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  MONTH( $mp_last_upd_date) = MONTH($curr_dt)
//                  AND YEAR($mp_last_upd_date) = YEAR($curr_dt) AND mp.payment_status='S' AND mp.checkout_status='released' GROUP BY mr.`membership_id`, mr.`membership_option_id`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
        $date_inc1= "DATE_FORMAT($curr_dt,'%Y-%m')";
        $date_inc2= "DATE_FORMAT($mp_last_upd_date,'%Y-%m')";
        $date_inc3= "DATE_FORMAT($last_upd_date,'%Y-%m')";
        
        $sql = sprintf("SELECT company_id, trial_id,  mp_ud, sum(amount) amount, payment_date FROM (

                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND (tp.payment_status='S' AND tp.checkout_status='released' || tp.payment_status IN ('MF') || tp.payment_status='M' AND tp.`credit_method`!='MC')
                  AND %s = %s 
                  
                  UNION
                  
                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, concat('-',if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount)) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND  (tp.`payment_status`='R' 
                  AND IF(tr.registration_from='S', tp.payment_intent_id NOT IN (SELECT payment_intent_id FROM trial_payment WHERE payment_status='FR' AND %s = %s),
                  tp.checkout_id NOT IN (SELECT checkout_id FROM trial_payment WHERE payment_status='FR' AND %s = %s)) || tp.`payment_status`='MR')
                  AND %s = %s
                  )t1
                  GROUP BY t1.`trial_id`"

                 , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc2, $date_inc1, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc3, $date_inc1, $date_inc3, $date_inc1, $date_inc2, $date_inc1);
        
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $reg_id = 1;
        } else {
            $trial = $trial_id_arr = [];
            $number_rows = mysqli_num_rows($result);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $temp = $temp2 = [];
                    $temp['trial_id'] = $temp2['trial_id'] = $trial_id = $row['trial_id'];
                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
                    $temp2['amount'] = $amount = $row['amount'];
                    $temp2['mp_ud'] = $row['mp_ud'];

                    if (!empty($trial_id) && !in_array($trial_id, $trial_id_arr, true)) {
                        $trial_id_arr[] = $trial_id;
                        $trial[] = $temp;
                    }
                    $option[] = $temp2;
                }

                for ($i = 0; $i < count($option); $i++) {
                    $date_init = 0;
                   
                    $date_str = "%Y-%m";
                    $curr_dt = date("Y-m-d");
                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
//                     
                    $sql1 = sprintf("SELECT * FROM `trial_dimensions` WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')", mysqli_real_escape_string($this->db, $company_id), $option[$i]['trial_id'], $every_month_dt, $date_str);
                    $res1 = mysqli_query($this->db, $sql1);
                    if (!$res1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
//                           

                        $num_of_rows = mysqli_num_rows($res1);
                        if ($num_of_rows == 1) {

                            if($i==0){
                                    $sales_value = "'%s'";
                                }else{
                                    $sales_value = "`sales_amount`+'%s'";
                                }
                                $sql3 = sprintf("UPDATE `trial_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `trial_id`='%s' ",mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str,mysqli_real_escape_string($this->db, $trial_id));
                                $res3 = mysqli_query($this->db, $sql3);
                                if (!$res3) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                                    log_info($this->json($error_log));
                                    $reg_id = 1;
                                }
                           
                        }
                    }
                }
            } else {
                $sql3 = sprintf("UPDATE `trial_dimensions` SET `sales_amount`=0 WHERE `company_id`='%s' AND `period`=%s AND `trial_id`='%s' ",mysqli_real_escape_string($this->db, $company_id), $date_inc1, mysqli_real_escape_string($this->db, $trial_id));
                $res3 = mysqli_query($this->db, $sql3);
                if (!$res3) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                    log_info($this->json($error_log));
                }
                $error = array('status' => "Failed", "msg" => "No records.", "query" => "$sql");
                log_info($this->json($error));
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for updateTrialDimensionsNetSales(), check log";
            $this->sendEmailForAdmins($msg);
        }
    }
    
    private function sendEmailForAdmins($msg){
        if($this->env=='P'){
            $env = "Production";
        }else{
            $env = "Development";
        }
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com,ravi@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "$env - Stripe Subscription Handling Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
   
    public function stripetrialProgramCheckoutcreate($optional_discount_flag, $optional_discount, $check_number, $payment_method, $company_id, $trial_id, $actual_student_id, $actual_participant_id, $trial_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $payment_method_id, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $processing_fee_type, $discount, $payment_type, $upgrade_status, $discount_code, $reg_type_user, $reg_version_user, $program_length, $program_length_type, $start_date, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $intent_method_type, $verification_status, $payment_intent_id, $cc_type, $stripe_customer_id) {
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        $payment_from = $registration_from = '';
        $stripe_account_id = $stripe_currency = $studio_name = $studio_stripe_status = '';
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
        if (($payment_method == "CC" && $stripe_account != '' && $stripe_account['charges_enabled'] == 'Y' && $payment_amount > 0) || ($payment_method == "CA" || $payment_method == "CH") || $payment_amount == 0) {  // ALLOWS FOR CASH & CHECK, IF CC, CHECK THE CHARGES ENABLED TO BE 'Y'
            $upgrade_status = $sts['upgrade_status'];
            $this->addTrialDimensions($company_id, $trial_id, '');
            
            $query1 = sprintf("SELECT c.`company_name`,c.`stripe_status`,s.`account_state`,s.`currency`,s.`account_id` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error1), 200);
            } else {
                if (mysqli_num_rows($result1) > 0) {
                    $row = mysqli_fetch_assoc($result1);
                    $stripe_account_id = $row['account_id'];
                    $stripe_currency = $row['currency'];
                    $stripe_account_state = $row['account_state'];
                    $studio_name = $row['company_name'];
                    $studio_stripe_status = $row['stripe_status'];
                }
            }
            
            if (is_null($actual_student_id) || $actual_student_id == 0) {
                $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Stripe', $studio_name);
                $student_id = $stud_array['student_id'];
                $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
            } else {
                $student_id = $actual_student_id;
                if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                    $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                }
            }
            if (is_null($actual_participant_id) || $actual_participant_id == 0) {
                $participant_id = $this->getParticipantId($company_id, $student_id, $reg_col1, $reg_col2, $reg_col3);
            } else {
                $participant_id = $actual_participant_id;
            }
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone = $user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
            $current_date = date("Y-m-d");
            if ($program_length_type == 'D') {
                $end_date = date("Y-m-d", strtotime("+$program_length days", strtotime($start_date)));
            } else {
                $end_date = date("Y-m-d", strtotime("+$program_length week", strtotime($start_date)));
            }

            $p_f = $this->getProcessingFee($company_id, $upgrade_status);
            $process_fee_per = $p_f['PP'];
            $process_fee_val = $p_f['PT'];

            if ($payment_amount > 0 && $payment_method == "CC") {
                $payment_type = 'O';
                $processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
            } else {
                if ($payment_amount > 0) {
                    $payment_type = 'O';
                    $processing_fee = 0;
                } else {
                    $payment_type = 'F';
                    $processing_fee = 0;
                }
            }

            $w_paid_amount = $paid_amount = $paid_fee = $amount_in_dollars = $dest_amount = $dest_amount_in_dollars = $w_paid_amount_round = $dest_amount_round = $insert_paid_amount = 0;
            $checkout_state = $button_url = $cc_month = $cc_year = '';
            $temp_payment_status = $payment_status = $stripe_card_name = '';
            $has_3D_secure='N';
            if ($studio_stripe_status == 'Y' && $stripe_account_state != 'N') {
                if ($payment_amount > 0 && $payment_method == "CC") {
                    $temp_payment_status = 'S';
                    $paid_amount = $payment_amount;
                    $paid_fee = $processing_fee;
                    if ($processing_fee_type == 2) {
                        $w_paid_amount = $payment_amount + $processing_fee;
                    } elseif ($processing_fee_type == 1) {
                        $w_paid_amount = $payment_amount;
                    }
                    $w_paid_amount_round = round($w_paid_amount, 2);
                    $dest_amount = $w_paid_amount - $processing_fee;
                    $dest_amount_round = round($dest_amount, 2);
                    $dest_amount_in_dollars = $dest_amount_round * 100;
                    $amount_in_dollars = $w_paid_amount_round * 100;
                    $p_type = 'trial';
                    $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $p_type, $cc_type);
                    if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'released';
                    } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'requires_action';
                        $has_3D_secure = 'Y';
                        $next_action_type = $payment_intent_result['next_action']['type'];
                        if($next_action_type != "redirect_to_url"){
                            # Payment not supported
                            $out['status'] = 'Failed';
                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                            $this->response($this->json($out), 200);
                        }
                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                    } else {
                        # Invalid status
                        $out['status'] = 'Failed';
                        $out['msg'] = 'Invalid SetupIntent status.';
                        $out['error'] = 'Invalid SetupIntent status';
                        $this->response($this->json($out), 200);
                    }
                    $payment_status = 'S';
                }
                if (!empty($payment_method_id)) {

                    try {
                        $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                        $brand = $retrive_payment_method->card->brand;
                        $cc_month = $retrive_payment_method->card->exp_month;
                        $cc_year = $retrive_payment_method->card->exp_year;
                        $last4 = $retrive_payment_method->card->last4;
                        $temp = ' xxxxxx';
                        $stripe_card_name = $brand . $temp . $last4;
                    } catch (\Stripe\Error\Card $e) {
                        // Since it's a decline, \Stripe\Error\Card will be caught     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\RateLimit $e) {
                        // Too many requests made to the API too quickly     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        // Invalid parameters were supplied to Stripe's API     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Authentication $e) {
                        // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\ApiConnection $e) {
                        // Network communication with Stripe failed     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Base $e) {
                        // Display a very generic error to the user, and maybe send yourself an email     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (Exception $e) {
                        // Something else happened, completely unrelated to Stripe     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    }
                    if (isset($err) && !empty($err)) {
                        $error = array('status' => "Failed", "msg" => $err['message']);
                        $this->response($this->json($error), 200);
                    }
                }
            }
            $payment_from = 'S';
            $registration_from = 'S';
            if ($payment_method == 'CA' || $payment_method == 'CH'){
                $payment_status = 'M';
                $insert_paid_amount = $payment_amount;
            }else if ($checkout_state == 'released'){
                $payment_status = 'S'; 
                if ($processing_fee_type == 2) {
                    $insert_paid_amount = $payment_amount;
                } elseif ($processing_fee_type == 1) {
                    $insert_paid_amount = $payment_amount - $processing_fee;
                }
            }else if ($payment_method == 'CC' && $payment_amount == 0){
                $payment_status = 'S';    
                $insert_paid_amount = 0;
            }else{
               $payment_status = 'S'; 
               $insert_paid_amount = 0; 
            }
            $query = sprintf("INSERT INTO `trial_registration`(`stripe_customer_id`,`registration_from`,`optional_discount_flag`,`check_number`,`payment_method`,`company_id`, `trial_id`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                    `payment_type`, `payment_amount`, `payment_method_id`, `stripe_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`,  `start_date`, `end_date`, `trial_status`, `discount`,
                    `trial_registration_column_1`, `trial_registration_column_2`, `trial_registration_column_3`, `trial_registration_column_4`, `trial_registration_column_5`, `trial_registration_column_6`, `trial_registration_column_7`, `trial_registration_column_8`, `trial_registration_column_9`, `trial_registration_column_10`,
                    `trial_reg_type_user`,`trial_reg_version_user`,`program_length`,`program_length_type`,`trial_participant_street`, `trial_participant_city`, `trial_participant_state`, `trial_participant_zip`, `trial_participant_country`, `paid_amount`)
                        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $registration_from), mysqli_real_escape_string($this->db, $optional_discount_flag), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id), mysqli_real_escape_string($this->db, $buyer_first_name),
                    mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $start_date), mysqli_real_escape_string($this->db, $end_date), mysqli_real_escape_string($this->db, 'A'),
                    mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4), mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user),
                    mysqli_real_escape_string($this->db, $program_length), mysqli_real_escape_string($this->db, $program_length_type), mysqli_real_escape_string($this->db, $participant_street), mysqli_real_escape_string($this->db, $participant_city), mysqli_real_escape_string($this->db, $participant_state), mysqli_real_escape_string($this->db, $participant_zip), mysqli_real_escape_string($this->db, $participant_country), mysqli_real_escape_string($this->db, $insert_paid_amount));
            $result = mysqli_query($this->db, $query);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $trial_reg_id = mysqli_insert_id($this->db);
                if ($payment_amount > 0) {
                    $payment_query = sprintf("INSERT INTO `trial_payment`(`payment_from`,`credit_method`,`check_number`,`trial_reg_id`, `student_id`, `checkout_status`, `processing_fee`, `payment_amount`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`,`stripe_card_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_state) ,mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $payment_status), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name));
                    $payment_result = mysqli_query($this->db, $payment_query);
                    if (!$payment_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
                // update trial table
                $update_count = sprintf("UPDATE `trial` SET `registrations_count` = `registrations_count`+1,`active_count`=`active_count`+1, `net_sales`=`net_sales`+$insert_paid_amount WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
                $result_count = mysqli_query($this->db, $update_count);

                if (!$result_count) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
                if (($payment_amount > 0) && ($checkout_state != 'requires_action')){
                    $this->updateTrialDimensionsNetSales($company_id, $trial_id, '');
                }

                $activity_text = 'Registration date.';
                $activity_type = "registration";

                if (!empty($discount_code)) {
                    $activity_text .= " Trial fee discount code used is " . "$discount_code";
                }

                $curr_date = gmdate("Y-m-d H:i:s");
                $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), $activity_type, mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }

                $this->updateTrialDimensionsMembers($company_id, $trial_id, 'A', '', $trial_reg_id);
                if ($has_3D_secure == 'N') {
                    $text = "Trial program registration was successful. Email confirmation sent.";
                } else {
                    $text = "Trial charge was processed and waiting for customer action. Kindly review and authenticate the payment.";
                }
                $msg = array("status" => "Success", "msg" => $text);
                $this->responseWithWepay($this->json($msg), 200);
                if ($has_3D_secure == 'N') {
                    $this->sendOrderReceiptForTrialPayment($company_id, $trial_reg_id);
                } else {
                    $email_msg = 'Hi '.$buyer_first_name.' '.$buyer_last_name.',<br>';
                    $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">This is an authentication email for Trial program '.$trial_name.' registration. Kindly click the launch button for redirecting to Stripe.</p>';
                    $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">Complete a required action for this payment. Once the redirect is completed for this PaymentIntent, the payment will proceed.</p>';
                    $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg);
                }
            }
            date_default_timezone_set($curr_time_zone);
        } else {
            $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
            $this->response($this->json($error), 200);
        }
    }

    public function payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $p_type, $cc_type) {
        if ($cc_type == 'N') {
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try {
            $payment_intent_create = \Stripe\PaymentIntent::create([
                        'payment_method' => $payment_method_id,
                        'customer' => $stripe_customer_id,
                        'amount' => $amount_in_dollars,
                        'currency' => $stripe_currency,
                        'confirmation_method' => 'automatic',
                        'capture_method' => 'automatic',
                        'confirm' => true,
                        'return_url' => $this->server_url."/stripeUpdation.php?type=authenticate&c_id=$company_id&p_type=$p_type&s_id=$student_id",
                        'metadata' => ['c_id' => $company_id, 'p_type' => $p_type, 's_id' => $student_id, 's_name' => $studio_name],
                        'setup_future_usage' => 'off_session',
                        'description' => 'Payment Intent for ' . $buyer_email,
                        'payment_method_types' => ['card'],
                        'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                        'receipt_email' => $buyer_email,
                        'transfer_data' => ['destination' => $stripe_account_id, 'amount' => $dest_amount_in_dollars],
                        'on_behalf_of' => $stripe_account_id,
                        'statement_descriptor' => substr($studio_name,0,21),
            ]);
            $body3 = json_encode($payment_intent_create);
            stripe_log_info("Stripe Payment Creation : $body3");
            $return_array['temp_payment_status'] = $payment_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded') {
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['cc_month'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_month;
                $return_array['cc_year'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_year;
                $brand = $payment_intent_create->charges->data[0]->payment_method_details->card->brand;
                $last4 = $payment_intent_create->charges->data[0]->payment_method_details->card->last4;
                $temp = ' xxxxxx';
                $return_array['stripe_card_name'] = $brand . $temp . $last4;
            } else if ($return_array['temp_payment_status'] == 'requires_action') {
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['payment_intent_client_secret'] = $payment_intent_create->client_secret;
                $return_array['next_action'] = $payment_intent_create->next_action;
//                $return_array['next_action']['type'] = $payment_intent_create->next_action->type;
//                $return_array['next_action']['redirect_to_url']['url'] = $payment_intent_create->next_action->redirect_to_url->url;
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }

    private function getStripeKeys() {
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
    
    protected function setupIntentCreation($company_id, $actual_student_id, $payment_method_id, $buyer_email, $buyer_first_name, $buyer_last_name, $stripe_customer_id){
        $out = [];
        $studio_name = $stripe_customer_id = "";
        
        $query2 = sprintf("SELECT c.`company_name`,s.`account_id` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result2 = mysqli_query($this->db, $query2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $row = mysqli_fetch_assoc($result2);
            $stripe_account_id = $row['account_id'];
            $studio_name = $row['company_name'];
        }
        
        if (is_null($actual_student_id) || $actual_student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Stripe', $studio_name);
            $student_id = $stud_array['student_id'];
            $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
        } else {
            $student_id = $actual_student_id;
            if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
            }
        }
        
        if (is_null($stripe_customer_id) || empty($stripe_customer_id)) {  //Creates a new customer if there is no previous customer - arun
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");
            
            try {
                $stripe_customer_create = \Stripe\Customer::create([
                            "email" => $buyer_email,
                            "description" => 'Customer created with '. $buyer_email .' for ' . $studio_name . ' studio(' . $company_id . ')',
                ]);
                $body = json_encode($stripe_customer_create);
                stripe_log_info("Customer_new : $body");
                $body_arr = json_decode($body, true);
                $stripe_customer_id = $body_arr['id'];
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            
            if(isset($err) && !empty($err)){
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);// If no records "No Content" status
            }
            //update stripe customer id to db - arun 
            $query2 = sprintf("UPDATE `student` SET `stripe_customer_id` = '%s' WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $student_id));
            $result2 = mysqli_query($this->db, $query2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }           
        }
        
        //attaching customer to payment method - arun 
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try{
            $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
            $payment_method->attach(['customer' => $stripe_customer_id]);
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught       
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        // creatiing setup indent to process payment - arun 
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try{
            $create_setup_indent = \Stripe\SetupIntent::create([
                                'payment_method_types' => ['card'],
                                'customer' => $stripe_customer_id,
                                'payment_method' => $payment_method_id,
                                'payment_method_options' => ['card'=>['request_three_d_secure'=>'any']],
                                'confirm' => true,
                                'on_behalf_of' => $stripe_account_id, // only accepted for card payments
            ]);
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught       
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        if ($create_setup_indent->status == 'requires_action' && $create_setup_indent->next_action->type == 'use_stripe_sdk') {
            $out['status'] = 'Success';
            $out['msg'] = 'Setup Intent created successfully.';
            $out['requires_action'] = true;
            $out['payment_intent_client_secret'] = $create_setup_indent->client_secret;
        } else if ($create_setup_indent->status == 'succeeded') {
            # The payment didn’t need any additional actions and completed!
            # # Handle post-payment fulfillment
            $out['status'] = 'Success';
            $out['msg'] = 'Setup Intent created successfully.';
        } else {
            # Invalid status
            $out['status'] = 'Failed';
            $out['msg'] = 'Invalid SetupIntent status.';
            $out['error'] = 'Invalid SetupIntent status';
        }
       $this->response($this->json($out), 200);
    }
    
    protected function checkCountryPaymentSupport($company_id, $country, $callback) { // GET COUNTRY PAYMENT SUPPORT
        $row = [];
        $sql = sprintf("SELECT * FROM country_payment_specific WHERE `payment_support` = 'S' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        UNION
                        SELECT * FROM country_payment_specific WHERE `payment_support` = 'W' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        ORDER BY `payment_support` ASC, `company_id` DESC", mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id)
                , mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                if ($callback == 0) {
                    $out = array("status" => "Success", "msg" => $row);
                } else {
                    $row['status'] = 'Success';
                    return $row;
                }
                $this->response($this->json($out), 200);
            } else {
                $out = array("status" => "Failed", "msg" => "Studio country is not yet available for payment support.");
                return $out;
            }
        }
    }
    
    public function getCompanyStripeAccount($company_id, $call_back){
        $query = sprintf("SELECT * FROM `stripe_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result) >0){
                $row = mysqli_fetch_assoc($result);
                if($call_back == 1){
                    return $row;
                }else{
                    $msg = array("status" => "Success", "msg" => "Stripe Account details", "stripe_details" => "$row");
                    $this->response($this->json($msg), 200);
                }
            }else{
                if($call_back == 1){
                    return '';
                }else{
                    $msg = array("status" => "Failed", "msg" => "Stripe Account details", "stripe_details" => "");
                    $this->response($this->json($msg), 200);
                }
            }
        }
    }

    public function sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg){
        
        // EMAIL CONTENT
        $button_text = 'Launch';
        $subject = 'Stripe authenticate';
        $message = $email_msg.'<br>'.'<center><a href="'.$button_url.'"><button type="button" style="width: 190px;border-radius: 5px;background: #009a61;border:1px solid 009a61;color: #ffffff;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 45px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;text-transform: none;overflow: visible;box-sizing: border-box;cursor: pointer;">'.$button_text.'</button></a></center>';
//        sendEmail($to, $cc_email_list, $subject, $message, $cmp_name, $reply_to, $isHtml);
        $sendEmail_status = $this->sendEmail($buyer_email, '', $subject, $message, $studio_name, '', true);
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
    }
    
    public function createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name){
        $stripe_customer_id = '';
        $query = sprintf("SELECT `stripe_customer_id` FROM `student` WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                $stripe_customer_id = $row['stripe_customer_id'];
            }
        }
        if(is_null($stripe_customer_id) || empty(trim($stripe_customer_id))){
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");

            try {
                $stripe_customer_create = \Stripe\Customer::create([
                            "email" => $buyer_email,
                            "description" => "Customer created with $buyer_email for $studio_name($company_id)",
                ]);
                $body = json_encode($stripe_customer_create);
                stripe_log_info("Customer_new : $body");
                $body_arr = json_decode($body, true);
                $stripe_customer_id = $body_arr['id'];
                
                //update stripe customer id to db - arun 
                $query2 = sprintf("UPDATE `student` SET `stripe_customer_id` = '%s' WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $student_id));
                $result2 = mysqli_query($this->db, $query2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }

            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
        
        return $stripe_customer_id;
    }

}

?>
