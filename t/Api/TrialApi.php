<?php

require_once 'TrialModel.php';

class TrialApi extends TrialModel {
    
    public function __construct(){
        parent::__construct();          // Init parent contructor
    }
    
    public function processApi(){
        $func = strtolower(trim(str_replace("/","",$_REQUEST['run'])));
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)){
            $this->_request = json_decode(file_get_contents('php://input'), true);
 	}
        if(isset($this->_request['companyid']) && $this->_request['companyid']==6){
            $error = array('status' => "Failed", "msg" => "No trial category available.");
            $this->response($this->json($error), 404);
        }
        if((int)method_exists($this,$func) > 0){
            $this->$func();
        }else{
            $error = array("status" => "Failed", "code" => 404, "msg" => "Page not found.", "func" => $func);
            $this->response($this->json($error),404); // If the method not exist with in this class, response would be "Page not found".
        }
    }
		
    private function trialdetails(){
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if($this->get_request_method() != "GET"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        $company_id = $trial_id = '';
        $reg_type_user = 'U';
        $token = $pos_email = '';
              //U - user, S - Social networks crawler
        
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        if(isset($this->_request['trial_id'])){
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if (isset($this->_request['token'])) {
            $token = $this->_request['token'];
        }
        if (isset($this->_request['pos_email'])) {
            $pos_email = $this->_request['pos_email'];
        }
        
        if(!empty($company_id)){
            if($reg_type_user !== 'U'){
                $this->checkpossettings($company_id, '', $pos_email, $token);
            }
            if(empty($trial_id)){
                $this->getTrialDetailByCompany($company_id, $reg_type_user);
            }else{
                $this->getTrialProgramDetails($company_id, $trial_id, $reg_type_user);
            }
        }else{
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 400); 
        }
    }
    
    private function getwepaystatus(){
        if($this->get_request_method() != "GET"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id='';
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        
        if(!empty($company_id)){
            $this->wepayStatus($company_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function getstripestatus() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->stripeStatus($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function wepayTrialprogramCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_name = $discount_code= $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = $program_length_type = $payment_type =$start_date='';
        $student_id = $participant_id = $fee = $discount = $registration_amount = $payment_amount =  $program_length = 0;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $upgrade_status = 'F';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        $country = 'US';
        $reg_type_user = 'U';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = '';
        $token = $pos_email = '';
       

        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['trial_name'])) {
            $trial_name = $this->_request['trial_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['cc_id'])) {
            $cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_state'])) {
            $cc_state = $this->_request['cc_state'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
         if (isset($this->_request['payment_startdate'])) {
            $start_date = $this->_request['payment_startdate'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['lead_source'])) {
            $reg_col4 = $this->_request['lead_source'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['program_length_type'])) {
            $program_length_type = $this->_request['program_length_type'];
        }
        
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['program_length'])) {
            $program_length = $this->_request['program_length'];
        }
        
        if (isset($this->_request['participant_street'])) {
            $participant_street = $this->_request['participant_street'];
        }
        if (isset($this->_request['participant_city'])) {
            $participant_city = $this->_request['participant_city'];
        }
        if (isset($this->_request['participant_state'])) {
            $participant_state = $this->_request['participant_state'];
        }
        if (isset($this->_request['participant_zip'])) {
            $participant_zip = $this->_request['participant_zip'];
        }
        if (isset($this->_request['participant_country'])) {
            $participant_country = $this->_request['participant_country'];
        }
        $payment_method =$optional_discount_flag = $optional_discount = "";
        $check_number = 0;        
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['check_number'])) {
            if(!empty($this->_request['check_number'])){
            $check_number = $this->_request['check_number'];
            }
        }
        if (isset($this->_request['optional_discount'])) {
            if(!empty($this->_request['optional_discount'])){
            $optional_discount = $this->_request['optional_discount'];
            }
        }
        if (isset($this->_request['optional_discount_flag'])) {
            $optional_discount_flag = $this->_request['optional_discount_flag'];
        }
        if (isset($this->_request['token'])) {
            $token = $this->_request['token'];
        }
        if (isset($this->_request['pos_email'])) {
            $pos_email = $this->_request['pos_email'];
        }
        
        if($reg_type_user !== 'U'){
            $this->checkpossettings($company_id, $payment_method, $pos_email, $token);
        }
        
        if($reg_type_user === "PP" && (((int)$optional_discount > 0) || $optional_discount_flag === "Y")){
            $error = array('status' => "Failed", "msg" => "Optional discount is not applicable");
            $this->response($this->json($error), 200);
        }
        if($payment_method === "CH" && empty(trim($check_number))){
            $error = array('status' => "Failed", "msg" => "Please provide check number");
            $this->response($this->json($error), 200);
        }
        if($payment_method === "CC" && empty(trim($cc_id)) && $payment_amount > 0){
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 200);
        }
        if(empty($reg_type_user) || ($reg_type_user === 'PP' && !empty(trim($student_id)))){
            $error = array('status' => "Failed", "msg" => "Authentication Error");
            $this->response($this->json($error), 200);
        }
       if (!empty($company_id) && !empty($trial_id) && !empty($trial_name) &&  !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
            $this->trialProgramCheckout($optional_discount_flag,$optional_discount,$check_number,$payment_method,$company_id, $trial_id, $student_id, $participant_id, $trial_name, $desc,  $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount,  $processing_fee_type, $discount,  $payment_type, $upgrade_status,  $discount_code, $reg_type_user, $reg_version_user,$program_length,$program_length_type,$start_date,$participant_street, $participant_city, $participant_state, $participant_zip, $participant_country);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function sendExpiryEmailToStudio(){
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($error, 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id)) {
            $this->sendExpiryEmailDetailsToStudio($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function stripeTrialprogramCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_name = $discount_code= $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $processing_fee_type = $program_length_type = $payment_type =$start_date='';
        $student_id = $participant_id = $fee = $discount = $registration_amount = $payment_amount =  $program_length = 0;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = $payment_method_id ='';
        $upgrade_status = 'F';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        $country = 'US';
        $reg_type_user = 'U';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = '';
        $token = $pos_email = '';
        $intent_method_type = $verification_status = $payment_intent_id = $cc_type = $stripe_customer_id = '';

        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['trial_name'])) {
            $trial_name = $this->_request['trial_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
         if (isset($this->_request['payment_startdate'])) {
            $start_date = $this->_request['payment_startdate'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['lead_source'])) {
            $reg_col4 = $this->_request['lead_source'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['program_length_type'])) {
            $program_length_type = $this->_request['program_length_type'];
        }
        
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['program_length'])) {
            $program_length = $this->_request['program_length'];
        }
        
        if (isset($this->_request['participant_street'])) {
            $participant_street = $this->_request['participant_street'];
        }
        if (isset($this->_request['participant_city'])) {
            $participant_city = $this->_request['participant_city'];
        }
        if (isset($this->_request['participant_state'])) {
            $participant_state = $this->_request['participant_state'];
        }
        if (isset($this->_request['participant_zip'])) {
            $participant_zip = $this->_request['participant_zip'];
        }
        if (isset($this->_request['participant_country'])) {
            $participant_country = $this->_request['participant_country'];
        }
        $payment_method =$optional_discount_flag = $optional_discount = "";
        $check_number = 0;        
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['check_number'])) {
            if(!empty($this->_request['check_number'])){
            $check_number = $this->_request['check_number'];
            }
        }
        if (isset($this->_request['optional_discount'])) {
            if(!empty($this->_request['optional_discount'])){
            $optional_discount = $this->_request['optional_discount'];
            }
        }
        if (isset($this->_request['optional_discount_flag'])) {
            $optional_discount_flag = $this->_request['optional_discount_flag'];
        }
        if (isset($this->_request['token'])) {
            $token = $this->_request['token'];
        }
        if (isset($this->_request['pos_email'])) {
            $pos_email = $this->_request['pos_email'];
        }
        if (isset($this->_request['intent_method_type'])) {
            $intent_method_type = $this->_request['intent_method_type'];
        }
        if (isset($this->_request['verification_status'])) {
            $verification_status = $this->_request['verification_status'];
        }
        if (isset($this->_request['payment_intent_id'])) {
            $payment_intent_id = $this->_request['payment_intent_id'];
        }   
        if (isset($this->_request['cc_type'])) {    // cc_type - N - New, E - Existing
            $cc_type = $this->_request['cc_type'];
        }
        if (isset($this->_request['stripe_customer_id'])) {    // cc_type - N - New, E - Existing
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        
        if($reg_type_user !== 'U'){
            $this->checkpossettings($company_id, $payment_method, $pos_email, $token);
        }
        
        if($reg_type_user === "PP" && (((int)$optional_discount > 0) || $optional_discount_flag === "Y")){
            $error = array('status' => "Failed", "msg" => "Optional discount is not applicable");
            $this->response($this->json($error), 200);
        }
        if($payment_method === "CH" && empty(trim($check_number))){
            $error = array('status' => "Failed", "msg" => "Please provide check number");
            $this->response($this->json($error), 200);
        }
        if($payment_method === "CC" && empty(trim($payment_method_id)) && $payment_amount > 0 && !empty($cc_type)){
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 200);
        }
        if(empty($reg_type_user) || ($reg_type_user === 'PP' && !empty(trim($student_id)))){
            $error = array('status' => "Failed", "msg" => "Authentication Error");
            $this->response($this->json($error), 200);
        }
       if (!empty($company_id) && !empty($trial_id) && !empty($trial_name) &&  !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
            $this->stripetrialProgramCheckoutcreate($optional_discount_flag,$optional_discount,$check_number,$payment_method,$company_id, $trial_id, $student_id, $participant_id, $trial_name, $desc,  $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $payment_method_id, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount,  $processing_fee_type, $discount,  $payment_type, $upgrade_status,  $discount_code, $reg_type_user, $reg_version_user,$program_length,$program_length_type,$start_date,$participant_street, $participant_city, $participant_state, $participant_zip, $participant_country,$intent_method_type,$verification_status,$payment_intent_id,$cc_type,$stripe_customer_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function setupIntent(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $buyer_email = $student_id = $payment_method_id = $actual_student_id = $stripe_customer_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['buyer_email'])) {
            $buyer_email = $this->_request['buyer_email'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['student_id'])) {
            $actual_student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['stripe_customer_id'])) {
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        
        if (!empty($company_id) && !empty($payment_method_id) && !empty($buyer_email) && !empty($buyer_first_name) && !empty($buyer_last_name)) {
            $this->setupIntentCreation($company_id, $actual_student_id, $payment_method_id, $buyer_email, $buyer_first_name, $buyer_last_name, $stripe_customer_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
} 

// Initiiate Library
    $api = new TrialApi;
    $api->processApi();



?>