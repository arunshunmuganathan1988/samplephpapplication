/*jslint browser: true*/
/*global console, MyApp*/
//www
MyApp.angular.controller('IndexPageController', ['$rootScope', '$scope', '$http', '$localStorage', 'urlservice', 'openurlservice', 'InitService','$window',  function ($rootScope, $scope, $http, $localStorage, urlservice, openurlservice, InitService, $window) {
        'use strict';
      
        InitService.addEventListener('ready', function ()
        {
            console.log('IndexPageController: ok, DOM ready');
            // Call company details on menu open
            $$('.panel-left').on('panel:opened', function () {
//                $scope.companydetails();
            });
            
        });
//        Muthulakshmi
        $scope.studentlist = [];
        $scope.student_name = [];
        $scope.trialcarddetails = [];
        $scope.showcardselection = false;
        $scope.shownewcard = true; 
        $scope.trial_check_number = ''; 
        $scope.trial_discount_applied_flag = 'N';
        $scope.trial_search_member = '';
        $scope.erroroptionaldiscount = false;
        $scope.erroroptionalMsg = '';
        $scope.optional_discount_applied_flag = 'N';
        $scope.trial_optional_processing_fee_value = '';
        $scope.trialfee_Discountcode = '';
        $scope.trial_discount_applied_flag = 'N';
        $scope.trialfee_Discode_Found = false;
        $scope.mobiledevice = false;
        $scope.trial_payment_method = 'CC';
        $scope.trial_ach_route_no = $scope.trial_ach_acc_number = $scope.trial_ach_holder_name = $scope.trial_ach_acc_type = $scope.trial_ach_chkbx ='';
        $scope.wepaystatus = $scope.stripestatus = '';
        $scope.wepay_enabled = $scope.stripe_enabled = false;
        $scope.trial_waiver_checked = $scope.trial_ach_chkbx = $scope.trial_cc_chkbx = false;
        $scope.stripe_card = $scope.payment_method_id = $scope.stripe_customer_id = '';
        
        //No Internet - show alert
        $scope.istatus = function () {
            MyApp.alert('Please turn on internet', 'No Internet');
            $scope.preloaderVisible = false;
            return;
        };
        
        $scope.initial_load_payment = function(){
            $scope.onetimecheckout = 0;
            $scope.ccyearlist = ["2018","2019","2020","2021","2022","2023","2024","2025","2026","2027","2028","2029","2030","2031","2032","2033","2034","2035","2036","2037"];
            $scope.ccmonthlist = ["1","2","3","4","5","6","7","8","9","10","11","12"];
          $scope.ccmonth = '';$scope.ccyear = '';$scope.country = '';
            if($scope.wp_level === 'P'){
                $scope.wepay_env = "production";
                $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
            }else if($scope.wp_level === 'S'){
                $scope.wepay_env = "stage";
                $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
            }
                WePay.set_endpoint($scope.wepay_env); // change to "production" when live  
            }; 
            

         // Shortcuts
        var valueById = '';
        var d = document;
        d.id = d.getElementById, valueById = function(id) {
            return d.id(id).value;
        };

        // For those not using DOM libraries
        var addEvent = function(e,v,f) {
            if (!!window.attachEvent) { e.attachEvent('on' + v, f); }
            else { e.addEventListener(v, f, false); }
        };

         // ATTACH THE MEMBERSHIP TO THE DOM
        addEvent(d.id('cc-submit-trial'), 'click', function () {
            $scope.trialWepayCheckout();
        });
        
        addEvent(d.id('cc-submit-trial1'), 'click', function () {
            $scope.trialWepayCheckout();
        });
        
        $scope.trialWepayCheckout = function(){
            var checkout_message1 = '';
            for (var i = 0; i < $scope.trial_reg_col_names.length; i++) {
                if( parseInt(i) < 2 || parseInt(i) > 3){     // index 3 contains lead source values
                    if ($scope.trialreg_col_field_name[i]) {

                    } else {
                        if ($scope.trial_reg_col_names[i].reg_col_mandatory === 'Y') {
                            checkout_message1 += '"' + $scope.trial_reg_col_names[i].reg_col_name + '"';
                            checkout_message1 += '<br>'
                        }
                        $scope.trialreg_col_field_name[i] = '';
                    }
                }
            }
            var birthday = document.getElementById("trialcolumn2").value;
            if (birthday == '') {
                checkout_message1 += '"Date of birth"<br>';
            }
            if(birthday){
                var birthdaysplit = birthday.split("-");
                var bmonth = birthdaysplit[1];
                var bday = birthdaysplit[2];
                var byear = birthdaysplit[0];
                if(!bmonth || !bday || !byear){
                    if (!bmonth) {
                        checkout_message1 += '"DOB Month"<br>';
                    }
                    if (!bday) {
                        checkout_message1 += '"DOB Day"<br>';
                    }
                    if (!byear) {
                        checkout_message1 += '"DOB Year"<br>';
                    }
                }
            }
            if (checkout_message1 !== '') {
                checkout_message1 = 'Participant Info:' + checkout_message1;
            }
            if (!$scope.trial_firstname || !$scope.trial_lastname || !$scope.trial_email || !$scope.trial_phone)
            {
                checkout_message1 += "Buyer's Info:";
                if (!$scope.trial_firstname) {
                    checkout_message1 += '"First Name"<br>';
                }
                if (!$scope.trial_lastname) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if (!$scope.trial_email) {
                    checkout_message1 += '"Email"<br>';
                }
                if (!$scope.trial_phone) {
                    checkout_message1 += '"Cell Phone"<br>';
                }
            }
            if (!$scope.trial_participant_Streetaddress || !$scope.trial_participant_City  || !$scope.trial_participant_State || !$scope.trial_participant_Zip || !$scope.trial_participant_Country )
            {
                checkout_message1 += 'Address:';
                if (!$scope.trial_participant_Streetaddress) {
                    checkout_message1 += '"Street address"<br>';
                }
                if (!$scope.trial_participant_City) {
                    checkout_message1 += '"City"<br>';
                }
                if (!$scope.trial_participant_State) {
                    checkout_message1 += '"State"<br>';
                }
                if (!$scope.trial_participant_Zip) {
                    checkout_message1 += '"Zip"<br>';
                }
                if (!$scope.trial_participant_Country) {
                    checkout_message1 += '"Country"<br>';
                }
            }
            if (!$scope.trial_cardnumber || !$scope.trial_ccmonth  || !$scope.trial_ccyear || !$scope.trial_cvv  || !$scope.trial_country || !$scope.trial_postal_code)
            {
                checkout_message1 += 'Payment Info:';
                if (!$scope.trial_cardnumber) {
                    checkout_message1 += '"Card number"<br>';
                }
                if (!$scope.trial_ccmonth) {
                    checkout_message1 += '"Expiration Date:Month"<br>';
                }
                if (!$scope.trial_ccyear) {
                    checkout_message1 += '"Expiration Date:Year"<br>';
                }
                if (!$scope.trial_cvv) {
                    checkout_message1 += '"CVV"<br>';
                }
                if (!$scope.trial_country) {
                    checkout_message1 += '"Choose country"<br>';
                }
                if (!$scope.trial_postal_code) {
                    checkout_message1 += '"Zip code"<br>';
                }
            }
            if (!$scope.trial_waiver_checked) {
                checkout_message1 += "Click the check box to accept Waiver & Agreement<br>";
            }
            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }
            var userName = $scope.trial_firstname + ' ' + $scope.trial_lastname;
            var user_first_name = $scope.trial_firstname;
            var user_last_name = $scope.trial_lastname;
            var email = valueById('trial_email').trim();
            var phone = valueById('trial_phone');
            var postal_code = valueById('trial_postal_code');
            var country = valueById('trial_country');
            var response = WePay.credit_card.create({
                "client_id": $scope.wp_client_id,
                "user_name": userName,
                "email": valueById('trial_email').trim(),
                "cc_number": valueById('trial_cc-number'),
                "cvv": valueById('trial_cc-cvv'),
                "expiration_month": valueById('trial_cc-month'),
                "expiration_year": valueById('trial_cc-year'),
                "address": {
                    "country": valueById('trial_country'),
                    "postal_code": valueById('trial_postal_code')
                }
            }, function (data) {
                if (data.error) {
                    $scope.preloader = false;
                    $scope.$apply();
                    MyApp.closeModal();
                    MyApp.alert(data.error_description, 'Message');
                    // HANDLES ERROR RESPONSE
                } else {
                    if ($scope.onetimecheckout == 0) {
                        $scope.onetimecheckout = 1;
                        $scope.trialCheckout(data.credit_card_id, data.state, user_first_name, user_last_name, email, phone, postal_code, country);
                    }
                }
            });
        };
        
        $scope.trialStripeValidation = function(){
            $scope.preloader = true;
            var checkout_message1 = '';
            for (var i = 0; i < $scope.trial_reg_col_names.length; i++) {
                if( parseInt(i) < 2 || parseInt(i) > 3){     // index 3 contains lead source values
                    if ($scope.trialreg_col_field_name[i]) {

                    } else {
                        if ($scope.trial_reg_col_names[i].reg_col_mandatory === 'Y') {
                            checkout_message1 += '"' + $scope.trial_reg_col_names[i].reg_col_name + '"';
                            checkout_message1 += '<br>'
                        }
                        $scope.trialreg_col_field_name[i] = '';
                    }
                }
            }
            var birthday = document.getElementById("trialcolumn2").value;
            if (birthday == '') {
                checkout_message1 += '"Date of birth"<br>';
            }
            if(birthday){
                var birthdaysplit = birthday.split("-");
                var bmonth = birthdaysplit[1];
                var bday = birthdaysplit[2];
                var byear = birthdaysplit[0];
                if(!bmonth || !bday || !byear){
                    if (!bmonth) {
                        checkout_message1 += '"DOB Month"<br>';
                    }
                    if (!bday) {
                        checkout_message1 += '"DOB Day"<br>';
                    }
                    if (!byear) {
                        checkout_message1 += '"DOB Year"<br>';
                    }
                }
            }
            if (checkout_message1 !== '') {
                checkout_message1 = 'Participant Info:' + checkout_message1;
            }
            if (!$scope.trial_firstname || !$scope.trial_lastname || !$scope.trial_email || !$scope.trial_phone){
                checkout_message1 += "Buyer's Info:";
                if (!$scope.trial_firstname) {
                    checkout_message1 += '"First Name"<br>';
                }
                if (!$scope.trial_lastname) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if (!$scope.trial_email) {
                    checkout_message1 += '"Email"<br>';
                }
                if (!$scope.trial_phone) {
                    checkout_message1 += '"Cell Phone"<br>';
                }
            }
            if (!$scope.trial_participant_Streetaddress || !$scope.trial_participant_City  || !$scope.trial_participant_State || !$scope.trial_participant_Zip || !$scope.trial_participant_Country )            {
                checkout_message1 += 'Address:';
                if (!$scope.trial_participant_Streetaddress) {
                    checkout_message1 += '"Street address"<br>';
                }
                if (!$scope.trial_participant_City) {
                    checkout_message1 += '"City"<br>';
                }
                if (!$scope.trial_participant_State) {
                    checkout_message1 += '"State"<br>';
                }
                if (!$scope.trial_participant_Zip) {
                    checkout_message1 += '"Zip"<br>';
                }
                if (!$scope.trial_participant_Country) {
                    checkout_message1 += '"Country"<br>';
                }
            }
            if ($scope.trial_amount_view > 0) {
                if (($scope.trial_payment_method == 'CC' && $scope.shownewcard && !$scope.valid_stripe_card) 
                        || (($scope.pos_user_type === 'S' || $scope.pos_user_type === 'P') && $scope.trial_payment_method == 'CH' && !$scope.trial_check_number) 
                        || ($scope.pos_user_type === 'S' && $scope.trial_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex)
                        || ($scope.trial_payment_method == 'ACH' && (!$scope.trial_ach_route_no || !$scope.trial_ach_acc_number || !$scope.trial_ach_holder_name || !$scope.trial_ach_acc_type))) {
                    checkout_message1 += 'Payment Info:';
                    if (($scope.pos_user_type === 'S' || $scope.pos_user_type === 'P') && $scope.trial_payment_method == 'CH' && !$scope.trial_check_number) {
                        checkout_message1 += '"Check Number"<br>';
                    }
                    if($scope.pos_user_type === 'S' && $scope.trial_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex){
                       checkout_message1 += '"Select Card"<br>'; 
                    }
                    if ($scope.trial_payment_method == 'CC' && $scope.shownewcard && !$scope.valid_stripe_card) {
                        checkout_message1 += '"Invalid card details"<br>';
                    }
                    if ($scope.trial_payment_method == 'ACH') {
                        checkout_message1 += '"ACH details"<br>';
                        if (!$scope.trial_ach_route_no) {
                            checkout_message1 += '"Route number"<br>';
                        }
                        if (!$scope.trial_ach_acc_number) {
                            checkout_message1 += '"Account number"<br>';
                        }
                        if (!$scope.trial_ach_holder_name) {
                            checkout_message1 += '"Account holder name"<br>';
                        }
                        if (!$scope.trial_ach_acc_type) {
                            checkout_message1 += '"Account type"<br>';
                        }
                    }
                }
            }
            if (!$scope.trial_waiver_checked && ($scope.trial_amount_view == 0 || $scope.trial_payment_method !== 'CC' ))  {
                checkout_message1 += "Click the check box to accept Waiver & Agreement<br>";
            }
            if ($scope.trial_amount_view > 0 && $scope.stripe_enabled && $scope.trial_payment_method == 'ACH' && !$scope.trial_ach_chkbx) {
                checkout_message1 += '"Click the check box to accept the ACH plugin agreement"<br>';
            }
            if ($scope.trial_amount_view > 0 && $scope.stripe_enabled && $scope.trial_payment_method == 'CC' && !$scope.trial_cc_chkbx) {
                checkout_message1 += '"Click the check box to accept the Credit card agreement"<br>';
            }
            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }else{
                return true;
            }
        };
        
        // STRIPE CHECKOUT
        $scope.trialStripeCheckout = function () {
            if ($scope.trialStripeValidation()) { // stripe checkout form validation
                $scope.preloader = true;
                var birthday = document.getElementById("trialcolumn2").value;
                var curr_date = new Date();
                var reg_current_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);

                var leadsource;
                if ($scope.leadsource) {
                    leadsource = $scope.leadsource;
                } else {
                    leadsource = $scope.trial_lead_columns[0].lead_col_name;
                }
//            Muthulakshmi
                if ($scope.pos_user_type === 'S' && $scope.optional_discount_applied_flag === 'Y') {
                    $scope.trial_amount_view = $scope.trial_amount_view - $scope.trial_optional_processing_fee_value;
                } else if ($scope.pos_user_type === 'S' && $scope.optional_discount_applied_flag === 'N' && !$scope.trialfee_Discountcode && !$scope.trialfee_Discode_Found && $scope.trial_discount_applied_flag === 'N') {
                    $scope.trial_amount_view = $scope.actual_trial_cost;
                }

                var final_total_trial_amount = (Math.round((+$scope.trial_amount_view + 0.00001) * 100) / 100).toFixed(2);
                var fee = 0;
                var credit_card_id, state;
                fee = $scope.trial_processing_fee_value;
                if ($scope.trial_payment_method === 'CH') {
                    credit_card_id = '';
                    state = '';
                } else if ($scope.trial_payment_method === 'CA') {
                    credit_card_id = '';
                    state = '';
                    $scope.trial_check_number = '';
                } else {
                    $scope.trial_check_number = '';
                }

                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'stripeTrialprogramCheckout',
                    data: {
                        companyid: $scope.company_id,
                        trial_id: $scope.TrialID,
                        trial_name: $scope.TrialTitle,
                        desc: $scope.TrialTitle,
                        buyer_first_name: $scope.trial_firstname,
                        buyer_last_name: $scope.trial_lastname,
                        email: $scope.trial_email,
                        phone: $scope.trial_phone,
                        postal_code: $scope.trial_participant_Zip,
                        country: $scope.trial_participant_Country,
                        registration_amount: $scope.actual_trial_cost,
                        discount: $scope.optional_discount_applied_flag === 'Y' ? $scope.trial_optional_discount : $scope.register_trial_discount_value,
                        discount_code: $scope.trialfee_Discountcode,
                        payment_amount: final_total_trial_amount,
                        processing_fee_type: $scope.trial_processing_fee_type,
                        processing_fee: $scope.optional_discount_applied_flag === 'Y' ? $scope.trial_optional_processing_fee_value : fee,
                        lead_source: leadsource,
                        reg_col1: $scope.trialreg_col_field_name[0],
                        reg_col2: $scope.trialreg_col_field_name[1],
                        reg_col3: birthday,
                        reg_col5: $scope.trialreg_col_field_name[4],
                        reg_col6: $scope.trialreg_col_field_name[5],
                        reg_col7: $scope.trialreg_col_field_name[6],
                        reg_col8: $scope.trialreg_col_field_name[7],
                        reg_col9: $scope.trialreg_col_field_name[8],
                        reg_col10: $scope.trialreg_col_field_name[9],
                        upgrade_status: $scope.company_status,
                        participant_street: $scope.trial_participant_Streetaddress,
                        participant_city: $scope.trial_participant_City,
                        participant_state: $scope.trial_participant_State,
                        participant_zip: $scope.trial_participant_Zip,
                        participant_country: $scope.trial_participant_Country,
                        program_length: $scope.triallength,
                        program_length_type: $scope.triallengthtype,
                        payment_startdate: $scope.trial_start_date,
                        registration_date: reg_current_date,
                        pos_email: $localStorage.pos_user_email_id,
                        payment_method: (parseFloat(final_total_trial_amount) > 0) ? $scope.trial_payment_method : 'CC', //CC - credit card,CH - check, CA- Cash
                        check_number: $scope.trial_check_number,
                        optional_discount_flag: $scope.optional_discount_applied_flag,
                        optional_discount: $scope.trial_optional_discount ? $scope.trial_optional_discount : 0,
                        token: $scope.pos_token,
                        reg_type_user: $scope.pos_user_type === 'S' ? 'SP' : ($scope.pos_user_type === 'P' ? 'PP' : 'U'),
                        payment_method_id: ($scope.trial_payment_method == 'CC') ? $scope.payment_method_id : '',
                        intent_method_type: parseFloat(final_total_trial_amount) > 0 ? "PI" : "", // SI - SETUP INTENT /  PI - PAYMENT INTENT
                        cc_type: ($scope.shownewcard) ? 'N' : 'E',
                        stripe_customer_id: $scope.stripe_customer_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then(function (response) {
                    $scope.preloader = false;
                    if (response.data.status === 'Success') {
                        $scope.redirect_conversionpage = "";
                        if ($scope.conversionpage) {
                            if ($scope.conversionpage.indexOf("http://") === 0 || $scope.conversionpage.indexOf("https://") === 0) {
                                $scope.redirect_conversionpage = $scope.conversionpage;
                            } else {
                                $scope.redirect_conversionpage = 'http://' + $scope.conversionpage;
                            }
                        }
                        $scope.onetimecheckout = 0;
                        $scope.resetCartAfterTrialPaymentSucceed();
                        $scope.preloader = false;
                        $(".form-group > input.ng-valid").parents('.form-group').removeClass('focused');
                        MyApp.alert(response.data.msg, 'Thank You', function () {
                            if ($scope.redirect_conversionpage) {
                                $scope.redirecturl();
                            }
                        });
                    } else {
                        if (response.data.session_status === "Expired") {
                            MyApp.alert('Session Expired', '', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        } else {
                            MyApp.alert(response.data.msg, 'Failure');
                        }
                    }
                }, function (response) {
                    $scope.preloader = false;
                    MyApp.alert('Connection failed', 'Invalid server response');
                });
            } else {
                $scope.preloader = false;
            }
        };
        // STRIPE TOKEN CALL
        $scope.getStripeElements = function () {
            // Create a Stripe client
            $scope.valid_stripe_card = false;           

            // Create an instance of Elements
            var elements = $scope.stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: 'rgb(74,74,74)',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: ($scope.mobiledevice)? '14px':'16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element
            $scope.stripe_card = elements.create('card', {style: style});

            // Add an instance of the card Element into the `card-element` <div>
            $scope.stripe_card.mount('#stripe-card-element');

            // Handle real-time validation errors from the card Element.
            $scope.stripe_card.addEventListener('change', function (event) {
                var displayError = document.getElementById('stripe-card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                    $scope.valid_stripe_card = false;
                } else {
                    displayError.textContent = '';
                    $scope.valid_stripe_card = true;
                }
            });            
        }; 
        
        // Handle form submission
        $scope.submittrialform = function(){
            if($scope.trialStripeValidation()){
                $scope.payment_method_id = "";
                $scope.stripe.createPaymentMethod('card', $scope.stripe_card).then(function (result) {
                    if (result.error) {
                        setTimeout(function () {
                            $scope.preloader = false;
                            $scope.$apply();
                        }, 1000);
                        // Inform the user if there was an error. 
                         MyApp.alert(result.error.message, '');
                         $scope.valid_stripe_card = false;
                    } else {
                        // Send paymentMethod.id to server 
                        (result.paymentMethod.id)? $scope.payment_method_id = result.paymentMethod.id : $scope.payment_method_id = "";
                        $scope.preloader = true;
                        $scope.trialStripeCheckout();
                    }
                });
            }else{                    
                $scope.preloader = false;
            }
        };
        
        // GET WEPAY STATUS
        $scope.getWepayStatus = function (cmp_id) {
            $http({
                method: 'GET',
                url: urlservice.url + 'getwepaystatus',
                params: {
                    "companyid": cmp_id,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.wepaystatus = response.data.msg;
                    if($scope.wepaystatus !== 'N'){
                        $scope.wepay_enabled = true;
                    }
                } else {
                    $scope.wepaystatus = 'N';
                    $scope.wepay_enabled = false;
                }
                $scope.preloader = false;
                $scope.preloaderVisible = false;
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
                $scope.preloader = false;
                $scope.preloaderVisible = false;
            });
        };
        
        //get Stripe status of this company
        $scope.getStripeStatus = function (cmp_id) {
            
            if ($scope.stripestatus !== 'Y') {
                $scope.wepay_enabled = $scope.stripe_enabled = false;
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getstripestatus',
                    params: {
                        company_id: $scope.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.stripestatus = response.data.msg;
                        if ($scope.stripestatus !== 'N') {
                            if ($scope.stripe_country_support === 'Y') {
                                $scope.stripe_enabled = true;
                                $scope.stripe = Stripe($scope.stripe_publish_key);
                                $scope.getStripeElements();       // AFTER GETTING STRIPE PUBLISH KEY WE GET STRIPE PAYMENT FORM
                            } else {
                                $scope.getWepayStatus(cmp_id);
                            } 
                        }else{
                            $scope.getWepayStatus(cmp_id);
                        }      
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        MyApp.alert(response.data.msg, '');
                    } else {
                        console.log(response.data);
                        if(response.data.msg && response.data.msg !== 'N'){
                            MyApp.alert(response.data.msg, '');
                        }else{
                            $scope.getWepayStatus(cmp_id);
                        }
                    }
                }, function (response) {
                    console.log(response.data);
                    MyApp.alert(response.data.msg, '');   
                });
            } else {
                if ($scope.stripestatus !== 'N' && $scope.stripe_country_support === 'Y') {
                    $scope.stripe_enabled = true;
                } else {
                    $scope.wepay_enabled = $scope.stripe_enabled = false;
                    $scope.getWepayStatus(cmp_id);
                }
            }
        };
        
        
         $scope.sendExpiryEmailToStudio = function(cmp_id){
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            $http({
                method: 'POST',
                url: urlservice.url + 'sendExpiryEmailToStudio',
                data: {
                    "companyid": cmp_id,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status === 'Success') {
                    MyApp.alert(response.data.msg, '');
                } else {
                    MyApp.alert(response.data.msg, 'Failure');
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };

        //GET TRIAL LIST CALL
        $scope.getTrialList = function  (cmp_id, trial_id) {
            $scope.fav_company_name = '';
            $scope.company_id_status = $scope.trial_id_status = false;
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            $http({
                method: 'GET',
                url: urlservice.url + 'trialdetails',
                params: {
                    "companyid": cmp_id,
                    "trial_id": trial_id,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U',
                    "page_from": "T"
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                    $scope.trialListContent = "";
                    $scope.db_Trialstatus = response.data.status;
                    $scope.trialListContent = response.data.msg;
                    $scope.company_id = cmp_id;                   
                    
                    $scope.logoUrl = response.data.company_details.logo_URL;
                    $scope.wp_level = response.data.company_details.wp_level;
                    $scope.wp_client_id = response.data.company_details.wp_client_id;
                    $scope.company_status = response.data.company_details.upgrade_status;                        
                    $scope.processing_fee_percentage = response.data.company_details.processing_fee_percentage;
                    $scope.processing_fee_transaction = response.data.company_details.processing_fee_transaction;
                    $scope.fav_company_name = response.data.company_details.company_name;
                    $scope.wp_currency_symbol = $scope.cursymbol = response.data.company_details.wp_currency_symbol;
                    $scope.wp_currency_code = response.data.company_details.wp_currency_code;
                    $scope.pos_settings_payment_method = response.data.company_details.pos_trial_payment_method;
                    // STRIPE CREDENTIALS 
                    $scope.stripe_upgrade_status = response.data.company_details.stripe_upgrade_status;
                    $scope.stripe_subscription = response.data.company_details.stripe_subscription;
                    $scope.stripe_publish_key = response.data.company_details.stripe_publish_key;
                    $scope.stripe_country_support = response.data.company_details.stripe_country_support;
                    
                    if($scope.pos_user_type === 'S'){
                        $scope.getstudentlist(cmp_id);
                    }
                    $scope.getStripeStatus(cmp_id);
//                    $scope.getWepayStatus(cmp_id);
                    $scope.initial_load_payment();
                        
                    if (cmp_id !== '' && trial_id === '') {
                        $scope.trialListContent = response.data.msg; 
                        $scope.dynamic_trial_id = '';
                        $scope.company_id_status = true;
                        $scope.trial_id_status = false;
                        // Load page:                                       
                        trialView.router.load({
                            pageName: 'index'
                        });                            

                    }else if(cmp_id !== '' && trial_id !== ''){                        
                        $scope.company_id_status = true;
                        $scope.trial_id_status = true;  
                        $scope.dynamic_trial_id = trial_id;
                        $scope.trialListContent = "";
                        $scope.trialdetail('',response.data.msg);
                    }
                } else {
                    $scope.trialListContent = "";
                    $scope.db_Trialstatus = response.data.status;
                    if(response.data.upgrade_status && response.data.studio_expiry_level){
                        $scope.cmpny_email = response.data.studio_email;
                        $scope.company_status = response.data.upgrade_status;
                        $scope.company_expiry_level = response.data.studio_expiry_level;
                    }
                    
                    if (($scope.company_status && $scope.company_status === 'F') || ($scope.company_expiry_level && $scope.company_expiry_level === 'L2')) {
                        // PREVENT FURTHUR EXECUTION & BLOCK THE APP
                        MyApp.modal({
                            title: '',
                            afterText:
                                    '<br>' +
                                    '<div id="studioExpiryText" class="modal-text custom-modal-content">Please contact the business operator to activate this link.</div>' +
                                    '<br>' +
                                    '<button class="button custom-modal-green-button button-text modalresetButton" id="send_expiry_email"></button>'
                        });

                        if ($scope.cmpny_email) {
                            $("#send_expiry_email").text("Send email message");
                        } else {
                            $("#send_expiry_email").text("Ok");
                        }

                        $('#send_expiry_email').click(function () {
                            $scope.$apply(function () {
                                if ($("#send_expiry_email").text() === 'Send email message') {
                                    $scope.sendExpiryEmailToStudio(cmp_id);
                                }
                                MyApp.closeModal();
                            });
                        });

                    } else {
                        if(response.data.session_status === "Expired"){
                            $scope.preloader = false;
                            $scope.preloaderVisible = false;
                            MyApp.alert('Session Expired','', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        }else{
                            MyApp.alert(response.data.msg, 'Message');
                        } 
                    }
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };
        
        
        $scope.urlredirection = function () {
           $scope.param1 = $scope.param2 = $scope.param3 = '';
           var decoded_url = decodeURIComponent(window.location.href);
           var params = decoded_url.split('?=')[1].split('/');
          
            $scope.param1 = params[0];  //company code
            $scope.param2 = params[1];  //company id
            $scope.param3 = params[2];  //trial id
            
            if(params[5]){
                if(params[5] === 'spos'){ //If staff
                    $scope.pos_user_type = 'S';
                    // ENABLE ADMIN BANNER, SEARCH MEMBER, OPTIONAL DISCOUNTS
                    $('#view-1').addClass('with-banner');
                }else{
                    $scope.pos_user_type = 'P'; //If public
                }
                if(params[6]){ //For access token
                    $scope.pos_token = params[6];
                }
                if(params[7]){
                   $scope.pos_entry_type = params[7];
                }
                 $('#view-1').addClass('from-pos with-footer');
            }else{
                $scope.pos_user_type = 'O'; // If open URL
                $scope.pos_token = $scope.pos_entry_type = '';
            }
//            alert('param1: '+$scope.param1+' param2: '+$scope.param2+' param3: '+$scope.param3);
            
            if ($scope.param1 !== undefined && $scope.param2 !== undefined && ($scope.param3 === undefined || $scope.param3 ==='')) {
                $scope.getTrialList($scope.param2, '');  
            } else if ($scope.param1 !== undefined && $scope.param2 !== undefined && $scope.param3 !== undefined) {
                if(isNaN($scope.param3)) {
                    $scope.getTrialList($scope.param2, ''); 
                }else{
                    $scope.getTrialList($scope.param2, $scope.param3);         // Is a number
                }                
            } else{
                window.location.href = window.location.origin;
            }
        
        };
      
        //DATE TO READABLE FORMAT
        $scope.dateString = function (date) {
            if(date !== '0000-00-00' && date !== '' && date !== undefined){
                var Displaydate = date.toString();
                Displaydate = date.split("-");
                var year = Displaydate[0];
                var month = Displaydate[1] - 1;
                var date = Displaydate[2];
                var ReadDate = new Date(year, month, date);
                var datesuccess = ReadDate.toString();
                var finaldateformat = datesuccess.split(" ");
                var finalized = finaldateformat[0] + " " + finaldateformat[1] + " " + finaldateformat[2] + " " + finaldateformat[3];
                return finalized;
            }else{
                return "";
            }
        };
      
        $scope.safariiosdate = function (dat) {

            var dda = dat.toString();
            dda = dda.replace(/-/g, "/");
            var curr_date = new Date(dda);   //  FOR DATE FORMAT IN SAFARI 

            var given_start_year = curr_date.getFullYear();
            var given_start_month = curr_date.getMonth();
            var given_start_day = curr_date.getDate();
            var given_start_month = curr_date.getMonth() + 1;
            var dda = given_start_year + "-" + given_start_month + "-" + given_start_day;
            dda = dda.toString();
            dda = dda.replace(/-/g, "/");
            var dup_date = new Date(dda);  //  FOR DATE FORMAT IN SAFARI 

            return dup_date;
        };
        
        $scope.newSEdateFormat = function (SEdate) {
            var sdate = SEdate.split("-");
            var syear = sdate[0];
            var smonth = parseInt(sdate[1], 10) - 1;
            var sdate = sdate[2];
            var sedate = new Date(syear, smonth, sdate);
            return sedate;
        };

        //TRIAL DETAIL CONTENT
        $scope.trialdetail = function (Selected_Index, trial_details) {
            $scope.trialfee_DiscountAmount = 0;   
            $scope.trialDetails = '';
            if(Selected_Index !== ''){
                $scope.trialDetails = $scope.trialListContent[Selected_Index];
            } else{
                $scope.trialDetails = trial_details;
                // Load page:  
                trialView.router.load({
                    pageName: 'trialdescription'
                });
            }
            $scope.conversionpage = $scope.trialDetails.trial_welcome_url;
            $scope.current_trial_manage = $scope.trialDetails;
            $scope.TrialImage = $scope.trialDetails.trial_banner_img_url;
            if ($scope.TrialImage === '' || $scope.TrialImage === undefined) {
                $scope.showTrialImage = false;
            } else {
                $scope.showTrialImage = true;
            }
            $scope.TrialID = $scope.trialDetails.trial_id;
            $scope.TrialTitle = $scope.trialDetails.trial_title;
            $scope.TrialSubTitle = $scope.trialDetails.trial_subtitle;
            $scope.TrialPrgmUrl = $scope.trialDetails.trial_prog_url;
            if ($scope.TrialPrgmUrl === '' || $scope.TrialPrgmUrl === undefined) {
                $scope.showTrialPrgmUrl = false;
            } else {
                $scope.showTrialPrgmUrl = true;
            }

            $scope.Trialdesc = $scope.trialDetails.trial_desc;
            if ($scope.Trialdesc === '' || $scope.Trialdesc === undefined) {
                $scope.showTrialDesc = false;
            } else {
                $scope.showTrialDesc = true;
            }
            $scope.triallengthtype = $scope.trialDetails.program_length_type;
            $scope.triallength = $scope.trialDetails.program_length;
            $scope.actual_trial_cost = $scope.trialDetails.price_amount;
            $scope.trial_processing_fee_type = $scope.trialDetails.processing_fee_type;
            
            $scope.trial_reg_col_names = $scope.current_trial_manage.reg_columns;
            $scope.trial_lead_columns = $scope.current_trial_manage.lead_columns;
            $scope.leadsource = $scope.trial_lead_columns[0].lead_col_name;    
            
            $scope.trialresetForm();
            $scope.showTrialTotal();
        };

        $scope.setDefault_trialstartDate = function () {
            var curr_date = new Date();
            $scope.trial_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);
            var dupdate = $scope.safariiosdate($scope.trial_start_date);
            var trialenddatequantity, trial_end;
            if ($scope.triallengthtype == 'W') {
                trialenddatequantity = $scope.triallength * 7;
                dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
            } else if ($scope.triallengthtype == 'D') {
                trialenddatequantity = $scope.triallength;
                dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
            }
            $scope.setTrialStartDate();
            $scope.trial_start_date_view = $scope.dateString($scope.trial_start_date);
            $scope.trial_end_date_view = $scope.dateString(trial_end);
            
            trialView.router.loadPage({
                pageName: 'trialdateselect'
            });
        };

        $scope.trial_paymentdetails = function () {
            
            $("#trialcolumn2").dateDropdowns({
                displayFormat: "mdy",
                required: true
            });
            
            if ($scope.current_trial_manage.trial_waiver_policies === null) {
                $scope.trialwaiverShow = false;
                $scope.trialwaiverarea = '';
            } else {
                $scope.trialwaiverShow = true;
                $scope.trialwaiverarea = $scope.current_trial_manage.trial_waiver_policies;
            }
            
            if ($scope.current_trial_manage.discount && $scope.current_trial_manage.discount.length > 0) {
                $scope.Trial_DiscountList = $scope.current_trial_manage.discount;
                $scope.trial_discount_exist = true;
            } else {
                $scope.Trial_DiscountList = "";
                $scope.trial_discount_exist = false;
            }
            $scope.showTrialTotal();
            
            trialView.router.loadPage({
                pageName: 'trialCart'
            });
        };
        
        $$('#trialdiscountcode').on('click', function () {
            var distrialmodelhtml = '';
            if ($scope.actual_trial_cost > 0 && $scope.trial_discount_exist) {
                distrialmodelhtml = '<input placeholder="Discount Trial Cost" id="trialfee_code" class="modal-text-input" style="text-align:center;" type="text">';
            } else {
                distrialmodelhtml = ' ';
            }

            MyApp.modal({
                afterText:'<div class="modal-title">Enter Discount Code</div>' +
                        '<div class="modal-text"></div>' +
                        '<div class="input-field modal-input" style="margin-bottom:-5px;">' +
                        distrialmodelhtml +
                        '</div>',
                buttons: [
                    {
                        text: 'Cancel',
                        onClick: function () {
                            MyApp.closeModal();
                        }
                    },
                    {
                        text: 'Ok',
                        onClick: function () {
                            $scope.trialfee_Discode = '';
                            if ($scope.actual_trial_cost > 0 && $scope.trial_discount_exist)
                                $scope.trialfee_Discode = document.getElementById("trialfee_code").value;
                            $scope.trialdiscountCheck($scope.trialfee_Discode);
                        }
                    }
                ]
            });

        });

        $scope.trialdiscountCheck = function (trialfee_Discode) {
            $scope.trial_discount_applied_flag = 'N';
            $scope.trialfee_afterdiscount = 0;
            $scope.trialfee_DiscountAmount = 0;
            $scope.trialfee_Discode_Found = false;
            if (trialfee_Discode == '') {
                $scope.trialfee_DiscountAmount = '';
                $scope.trialfee_afterdiscount = 0;
                $scope.trialfee_Discountcode = '';
                $scope.trial_discount_applied_flag = 'N';
                $scope.trialfee_Discode_Found = false;
            } else {
                if (trialfee_Discode !== '') {
                    for (var index = 0; index < $scope.Trial_DiscountList.length; index++) {
//                      TRIAL FEE DISCOUNT CHECK  
                        if (trialfee_Discode.toUpperCase() === $scope.Trial_DiscountList[index].discount_code.toUpperCase()) {                            
                                $scope.trialfee_Discode_Found = true;
                                $scope.trial_discount_applied_flag = 'Y';
                                if ($scope.Trial_DiscountList[index].discount_type === 'V') {
                                    $scope.trialfee_DiscountAmount = parseFloat($scope.Trial_DiscountList[index].discount_amount);
                                    $scope.trialfee_afterdiscount = +$scope.actual_trial_cost - +$scope.trialfee_DiscountAmount;
                                } else {
                                    $scope.trialfee_DiscountAmount = +$scope.actual_trial_cost * (parseFloat($scope.Trial_DiscountList[index].discount_amount) / 100);
                                    $scope.trialfee_afterdiscount = +$scope.actual_trial_cost - +$scope.trialfee_DiscountAmount;
                                }
                        }
                    }
                    $scope.trialfee_Discountcode = trialfee_Discode.toUpperCase();
                    if ($scope.trialfee_Discode_Found === false) {
                        $scope.trial_discount_applied_flag = 'N';
                        $scope.trialfee_Discountcode = '';
                        MyApp.alert('Invalid Trial Discount Code : ' + trialfee_Discode,'');
                    }
                }

                if (parseFloat($scope.trialfee_afterdiscount) > 5 || parseFloat($scope.trialfee_afterdiscount) == 0) {
                    $scope.showTrialTotal();
                    $scope.$apply();
                } else {
                    $scope.trialfee_Discountcode = '';
                    $scope.trialfee_Discode_Found = false;
                    $scope.trial_discount_applied_flag = 'N';
                    MyApp.alert('Discount value exceeds the actual cost and must be atleast ' + $scope.cursymbol + '5','');
                    return false;
                }

            }
        };

        //  INITIALIZING THE TRIAL DATEPICKER        
        var calendarInline;
        $scope.setTrialStartDate = function () {
            if (calendarInline !== undefined) {
                calendarInline.destroy();
                document.getElementById("calendar-inline-container").innerHTML = '';
            }

            var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var curr_date = new Date();
            var current_day = $scope.newSEdateFormat(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2));
            var today = new Date(current_day);

            calendarInline = MyApp.calendar({
                container: '#calendar-inline-container',
                minDate: today,
                value: [today],
                weekHeader: true,
                firstDay: 0,
                toolbarTemplate:
                        '<div class="toolbar calendar-custom-toolbar">' +
                        '<div class="toolbar-inner">' +
                        '<div class="left">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
                        '</div>' +
                        '<div class="center"></div>' +
                        '<div class="right">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
                        '</div>' +
                        '</div>' +
                        '</div>',
                onOpen: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                    $$('.calendar-custom-toolbar .left .link').on('click', function () {
                        calendarInline.prevMonth();
                    });
                    $$('.calendar-custom-toolbar .right .link').on('click', function () {
                        calendarInline.nextMonth();
                    });
                },
                onMonthYearChangeStart: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                },
                onDayClick: function (p, dayContainer, year, month, day) {
                    $scope.trial_start_date = year + '-' + ('0' + (parseInt(month) + parseInt(1))).slice(-2) + '-' + ('0' + day).slice(-2);
                    var dupdate = $scope.safariiosdate($scope.trial_start_date);
                    var trialenddatequantity, trial_end;
                    if ($scope.triallengthtype == 'W') {
                        trialenddatequantity = $scope.triallength * 7;
                        dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                        trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
                    } else if ($scope.triallengthtype == 'D') {
                        trialenddatequantity = $scope.triallength;
                        dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                        trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
                    }
                    $scope.trial_start_date_view = $scope.dateString($scope.trial_start_date);
                    $scope.trial_end_date_view = $scope.dateString(trial_end);
                }
            });
        };
        
        $scope.showTrialTotal = function(){
            
            var process_fee_per = 0, process_fee_val = 0;
            var process_fee_per = $scope.processing_fee_percentage;
            var process_fee_val = $scope.processing_fee_transaction;
            $scope.register_trial_discount_value = 0;
            $scope.trial_processing_fee_value = 0;
            $scope.trial_total_amount_view = 0;
            $scope.trial_amount_view = 0;
            if(parseFloat($scope.actual_trial_cost) > 0){
                $scope.trial_amount_view = $scope.actual_trial_cost;
            }
            
            if (parseFloat($scope.trialfee_DiscountAmount) > 0) {
                $scope.trial_amount_view = +$scope.actual_trial_cost - +$scope.trialfee_DiscountAmount;
                $scope.register_trial_discount_value = $scope.trialfee_DiscountAmount;
            } else {
                $scope.trial_amount_view = +$scope.actual_trial_cost;
                $scope.register_trial_discount_value = 0;
            }

            var amount = parseFloat($scope.trial_amount_view);
             if($scope.trial_payment_method === 'CC'){ 
                if ($scope.trial_processing_fee_type === 2 || $scope.trial_processing_fee_type === '2') {
                    if (amount > 0) {
                        var process_fee1 = +amount + +((+amount * +process_fee_per) / 100) + +process_fee_val;
                        var process_fee = +((+process_fee1 * +process_fee_per) / 100) + +process_fee_val;
                        $scope.trial_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.trial_processing_fee_value = 0;
                    }
                    $scope.trial_total_amount_view = parseFloat($scope.trial_amount_view) + parseFloat($scope.trial_processing_fee_value);
                } else if ($scope.trial_processing_fee_type === 1 || $scope.trial_processing_fee_type === '1') {
                    if (amount > 0) {
                        var process_fee = +((+amount * +process_fee_per) / 100) + +process_fee_val;
                        $scope.trial_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.trial_processing_fee_value = 0;
                    }
                    $scope.trial_total_amount_view = parseFloat($scope.trial_amount_view);
                }
            }else{
                $scope.trial_total_amount_view = parseFloat($scope.trial_amount_view);
                $scope.trial_processing_fee_value = 0;
            }
            $scope.static_trial_amount = $scope.trial_total_amount_view;
            $scope.static_trial_processing_fee_value = $scope.trial_processing_fee_value;
        };
        
        $scope.trialresetForm = function () {
            for (var i = 0; i < $scope.trialreg_col_field_name.length; i++) {
                $scope.trialreg_col_field_name[i] = '';
            }
            $scope.trialreg_col_field_name = [];     
            if(document.getElementsByClassName("dob_month")[0]|| document.getElementsByClassName("dob_day")[0] || document.getElementsByClassName("dob_year")[0]){
                document.getElementsByClassName("dob_month")[0].value = ""; 
                document.getElementsByClassName("dob_day")[0].value = ""; 
                document.getElementsByClassName("dob_year")[0].value = "";
            }
            if($scope.trial_lead_columns)
                $scope.leadsource = $scope.trial_lead_columns[0].lead_col_name;
            $scope.trial_firstname = '';
            $scope.trial_lastname = '';
            $scope.trial_email = '';
            $scope.trial_phone = '';
            $scope.trial_cc_name = '';
            $scope.trial_cardnumber = '';
            $scope.trial_ccmonth = '';
            $scope.trial_ccyear = '';
            $scope.trial_cvv = '';
            $scope.trial_country = '';
            $scope.trial_postal_code = '';
            $scope.trial_waiver_checked = $scope.trial_ach_chkbx = $scope.trial_cc_chkbx = false;
            $scope.trial_participant_Streetaddress = '';
            $scope.trial_participant_City = '';
            $scope.trial_participant_State = '';
            $scope.trial_participant_Zip = '';
            $scope.trial_participant_Country = '';

            //participant information            
            $scope.trialreg_col_field_name[0] = $scope.trialreg_col_field_name[1] = $scope.trialreg_col_field_name[2] = '';

            //credit card details
            $scope.trial_cc_name = $scope.trial_cardnumber = $scope.trial_ccmonth = $scope.trial_ccyear = $scope.trial_cvv = $scope.trial_country = $scope.trial_postal_code = '';
            $scope.trial_credit_card_id = $scope.trial_credit_card_status = '';
            
            $scope.trialcarddetails = [];
            $scope.trial_payment_method = 'CC';
            $scope.showcardselection = false;
            $scope.shownewcard = true; 
            $scope.trial_check_number = ''; 
            $scope.trial_discount_applied_flag = 'N';
            $scope.trial_search_member = '';
            $scope.erroroptionaldiscount = false;
            $scope.erroroptionalMsg = '';
            $scope.optional_discount_applied_flag = 'N';
            $scope.trial_optional_processing_fee_value = '';
            $scope.trial_optional_discount = '';
            $scope.trialfee_Discountcode = '';
            $scope.trial_discount_applied_flag = 'N';
            $scope.selectedcardindex = '';
            $scope.trialfee_Discode_Found = false;
            // CLEAR STRIPE FORM
            if ($scope.stripe_card) {
                $scope.stripe_card.clear();
            }
            // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
            $('.ph-text').css('display','block');
        };
        
         $scope.resetCartAfterTrialPaymentSucceed = function (page) {
             
            $scope.trialresetForm();
            $scope.trialpaymentform.$setPristine();
            $scope.trialpaymentform.$setUntouched();
            $scope.preloader = false;
            $scope.trialreg_col_field_name = [];
            $scope.register_trial_discount_value = 0;
            $scope.trial_total_amount_view = 0;
            $scope.trial_amount_view = 0;
            $scope.trial_processing_fee_type = '';
            $scope.trial_processing_fee_value = 0;
            $scope.trialwaiverarea = '';
            $scope.trialfee_afterdiscount = 0;
            $scope.trialfee_DiscountAmount = 0;
            $scope.trialfee_Discode_Found = false;
            $scope.trialfee_Discountcode = '';
            $scope.current_trial_manage = '';
            $scope.trialDetails = '';
            $scope.Trial_DiscountList = '';
            $('.form-group').removeClass('focused');
            if(page != 'POS') {
                if ($scope.company_id !== '' && $scope.dynamic_trial_id === '') {
                    // Load page:                                       
                    trialView.router.load({
                        pageName: 'index'
                    });
                    $scope.getTrialList($scope.company_id, '');
                } else if ($scope.company_id !== '' && $scope.dynamic_mem_category_id !== '') {
                    $scope.getTrialList($scope.company_id, $scope.dynamic_trial_id);
                }
            }
        };
        
        $scope.resetTrialcartdetails = function () {

            MyApp.closePanel();
            MyApp.confirm(' Discard cart items?', 'Alert', function () {
                $scope.trialresetForm();
                $scope.trialpaymentform.$setPristine();
                $scope.trialpaymentform.$setUntouched();
                $scope.preloader = false;
                $scope.register_trial_discount_value = 0;
                $scope.trial_total_amount_view = 0;
                $scope.trialfee_afterdiscount = 0;
                $scope.trialfee_DiscountAmount = 0;
                $scope.trialfee_Discode_Found = false;
                $scope.trialfee_Discountcode = ''; 
                $scope.trial_discount_applied_flag = 'N';
                $('.form-group').removeClass('focused');
                $scope.showTrialTotal();
                $scope.$apply();
                // Load page:
                trialView.router.loadPage({
                    pageName: 'trialdescription'
                });
            });
        };
        
        $scope.redirecturl = function () {
            window.open($scope.redirect_conversionpage, "_blank");
        };
        
        $scope.trialCheckout = function (credit_card_id, state, user_first_name, user_last_name, email, phone, postal_code, country) {
            $scope.preloader = true;
            var checkout_message1 = '';
            if (!user_first_name) {
                user_first_name = $scope.trial_firstname;
            }
            if (!user_last_name) {
                user_last_name = $scope.trial_lastname;
            }
            for (var i = 0; i < $scope.trial_reg_col_names.length; i++) {
                if (parseInt(i) < 2 || parseInt(i) > 3) {        // index 3 contains lead source values
                    if ($scope.trialreg_col_field_name[i]) {

                    } else {
                        if ($scope.trial_reg_col_names[i].reg_col_mandatory === 'Y') {
                            checkout_message1 += '"' + $scope.trial_reg_col_names[i].reg_col_name + '"';
                            checkout_message1 += '<br>';
                        }
                        $scope.trialreg_col_field_name[i] = '';
                    }
                }
            }
            var birthday = document.getElementById("trialcolumn2").value;
            if (birthday == '') {
                checkout_message1 += '"Date of birth"<br>';
            }
            if(birthday){
                var birthdaysplit = birthday.split("-");
                var bmonth = birthdaysplit[1];
                var bday = birthdaysplit[2];
                var byear = birthdaysplit[0];
                var byear1 = '';
                if(byear > 1000 && byear < 4000)
                    byear1 = byear;  
                if(!bmonth || !bday || !byear){
                    if (!bmonth) {
                        checkout_message1 += '"DOB Month"<br>';
                    }
                    if (!bday) {
                        checkout_message1 += '"DOB Day"<br>';
                    }
                    if (!byear1) {
                        checkout_message1 += '"DOB Year"<br>';
                    }
                }
            }
            if (checkout_message1 !== '') {
                checkout_message1 = 'Participant Info:' + checkout_message1;
            }
            if (!$scope.trial_firstname || !$scope.trial_lastname || !$scope.trial_email || !$scope.trial_phone)
            {
                checkout_message1 += "Buyer's Info:";
                if (!$scope.trial_firstname) {
                    checkout_message1 += '"First Name"<br>';
                }
                if (!$scope.trial_lastname) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if (!$scope.trial_email) {
                    checkout_message1 += '"Email"<br>';
                }
                if (!$scope.trial_phone) {
                    checkout_message1 += '"Cell Phone"<br>';
                }
            }
            if (!$scope.trial_participant_Streetaddress || !$scope.trial_participant_City || !$scope.trial_participant_State || !$scope.trial_participant_Zip || !$scope.trial_participant_Country)
            {
                checkout_message1 += 'Address:';
                if (!$scope.trial_participant_Streetaddress) {
                    checkout_message1 += '"Street address"<br>';
                }
                if (!$scope.trial_participant_City) {
                    checkout_message1 += '"City"<br>';
                }
                if (!$scope.trial_participant_State) {
                    checkout_message1 += '"State"<br>';
                }
                if (!$scope.trial_participant_Zip) {
                    checkout_message1 += '"Zip"<br>';
                }
                if (!$scope.trial_participant_Country) {
                    checkout_message1 += '"Country"<br>';
                }
            }
            if($scope.pos_user_type === 'S' && $scope.trial_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex && $scope.trial_amount_view > 0){
               checkout_message1 += '"Select Card"<br>'; 
            }
            if($scope.trial_payment_method ==='CH' && !$scope.trial_check_number){  
                checkout_message1 += '"Check Number"<br>';
            }
            if (!$scope.trial_waiver_checked) {
                checkout_message1 += "Click the check box to accept Waiver & Agreement<br>";
            }
            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }

            var curr_date = new Date();
            var reg_current_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);

            var leadsource;
            if ($scope.leadsource) {
                leadsource = $scope.leadsource;
            } else {
                leadsource = $scope.trial_lead_columns[0].lead_col_name;
            }
//            Muthulakshmi
            if($scope.pos_user_type === 'S' && $scope.optional_discount_applied_flag === 'Y'){
                $scope.trial_amount_view = $scope.trial_amount_view - $scope.trial_optional_processing_fee_value;
            }else if($scope.pos_user_type === 'S' && $scope.optional_discount_applied_flag === 'N' && !$scope.trialfee_Discountcode && !$scope.trialfee_Discode_Found && $scope.trial_discount_applied_flag === 'N'){
                $scope.trial_amount_view = $scope.actual_trial_cost;
            }
            
            var final_total_trial_amount = (Math.round((+$scope.trial_amount_view + 0.00001) * 100) / 100).toFixed(2);
            var fee = 0;
            fee = $scope.trial_processing_fee_value;
            if($scope.trial_payment_method ==='CH'){                
                credit_card_id = '';
                state = '';
            }else if($scope.trial_payment_method ==='CA'){
                credit_card_id = '';
                state = '';
                $scope.trial_check_number = '';
            }else{
                $scope.trial_check_number = '';
            }

            $http({
                method: 'POST',
                url: urlservice.url + 'wepayTrialprogramCheckout',
                data: {
                    companyid: $scope.company_id,
                    trial_id: $scope.TrialID,
                    trial_name: $scope.TrialTitle,
                    desc: $scope.TrialTitle,
                    buyer_first_name: user_first_name,
                    buyer_last_name: user_last_name,
                    email: email,
                    phone: phone,
                    postal_code: postal_code,
                    country: country,
                    registration_amount: $scope.actual_trial_cost,
                    discount: $scope.optional_discount_applied_flag === 'Y'? $scope.trial_optional_discount : $scope.register_trial_discount_value,
                    discount_code: $scope.trialfee_Discountcode,
                    payment_amount: final_total_trial_amount,
                    processing_fee_type: $scope.trial_processing_fee_type,
                    processing_fee: $scope.optional_discount_applied_flag === 'Y'? $scope.trial_optional_processing_fee_value : fee,
                    lead_source: leadsource,
                    reg_col1: $scope.trialreg_col_field_name[0],
                    reg_col2: $scope.trialreg_col_field_name[1],
                    reg_col3: birthday,
                    reg_col5: $scope.trialreg_col_field_name[4],
                    reg_col6: $scope.trialreg_col_field_name[5],
                    reg_col7: $scope.trialreg_col_field_name[6],
                    reg_col8: $scope.trialreg_col_field_name[7],
                    reg_col9: $scope.trialreg_col_field_name[8],
                    reg_col10: $scope.trialreg_col_field_name[9],
                    cc_id: parseInt(credit_card_id),
                    cc_state: state,
                    upgrade_status: $scope.company_status,
                    participant_street: $scope.trial_participant_Streetaddress,
                    participant_city: $scope.trial_participant_City,
                    participant_state: $scope.trial_participant_State,
                    participant_zip: $scope.trial_participant_Zip,
                    participant_country: $scope.trial_participant_Country,
                    program_length: $scope.triallength,
                    program_length_type: $scope.triallengthtype,
                    payment_startdate: $scope.trial_start_date,
                    registration_date: reg_current_date,
                    pos_email: $localStorage.pos_user_email_id,
                    payment_method :$scope.trial_payment_method, //CC - creadit card,CH - check, CA- Cash
                    check_number:$scope.trial_check_number,
                    optional_discount_flag:$scope.optional_discount_applied_flag,
                    optional_discount: $scope.trial_optional_discount ? $scope.trial_optional_discount : 0,
                    token: $scope.pos_token,
                    reg_type_user:$scope.pos_user_type === 'S' ? 'SP' : ($scope.pos_user_type === 'P' ? 'PP' : 'U')
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.redirect_conversionpage = "";
                    if ($scope.conversionpage) {
                        if ($scope.conversionpage.indexOf("http://") === 0 || $scope.conversionpage.indexOf("https://") === 0) {
                            $scope.redirect_conversionpage = $scope.conversionpage;
                        } else {
                            $scope.redirect_conversionpage = 'http://' + $scope.conversionpage;
                        }
                    }
                    $scope.onetimecheckout = 0;
                    $scope.resetCartAfterTrialPaymentSucceed();
                    $scope.preloader = false;
                    MyApp.alert(response.data.msg, 'Message', function () {
                        if ($scope.redirect_conversionpage) {
                            $scope.redirecturl();
                        }
                    });
                    $scope.$apply();
                } else {
                    $scope.onetimecheckout = 0;
                    if (response.data.upgrade_status && response.data.studio_expiry_level) {
                        $scope.cmpny_email = response.data.studio_email;
                        $scope.company_status = response.data.upgrade_status;
                        $scope.company_expiry_level = response.data.studio_expiry_level;
                    }
                    if (response.data.error) {
                        $scope.preloader = false;
                        $scope.$apply();
                        MyApp.closeModal();
                        MyApp.alert(response.data.error + ' ' + response.data.error_description, 'Message');
                    } else if (($scope.company_status && $scope.company_status === 'F') || ($scope.company_expiry_level && $scope.company_expiry_level === 'L2')) {
                        MyApp.closeModal();
                        // PREVENT FURTHUR EXECUTION & BLOCK THE APP
                        MyApp.modal({
                            title: '',
                            afterText:
                                    '<br>' +
                                    '<div id="studioExpiryText" class="modal-text custom-modal-content">Please contact the business operator to activate this link.</div>' +
                                    '<br>' +
                                    '<button class="button custom-modal-green-button button-text modalresetButton" id="send_expiry_email"></button>'
                        });

                        if ($scope.cmpny_email) {
                            $("#send_expiry_email").text("Send email message");
                        } else {
                            $("#send_expiry_email").text("Ok");
                        }

                        $('#send_expiry_email').click(function () {
                            $scope.$apply(function () {
                                if ($("#send_expiry_email").text() === 'Send email message') {
                                    $scope.sendExpiryEmailToStudio($scope.company_id);
                                }
                                MyApp.closeModal();
                            });
                        });

                    } else {
                        MyApp.closeModal();
                        if(response.data.session_status === "Expired"){
                            $scope.preloader = false;
                            $scope.preloaderVisible = false;
                            MyApp.alert('Session Expired','', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        }else{
                            MyApp.alert(response.data.msg, 'Message');
                        }
                        $scope.preloader = false;
                        $scope.$apply();
                    }
                }
            }, function (response) {
                $scope.onetimecheckout = 0;
                $scope.preloaderVisible = false;
                $scope.preloader = false;
                $scope.$apply();
                MyApp.alert('Connection failed', 'Invalid Server Response');
            });
        };
        $scope.openevents = function (eventurl) {
               MyApp.closePanel();
            if ($rootScope.online != "online") {
                $scope.istatus();
                return;
            }
            if(eventurl ==='' && eventurl === undefined && eventurl === null){
                return false;
            }else if(eventurl !=='' && eventurl !== undefined && eventurl !== null){
                window.open(eventurl, '_blank');            
            }
        };
        
//        Muthulakshmi
        $scope.getstudentlist = function(companyid){
            $scope.preloader = false;
            $scope.preloaderVisible = false;
            $http({
                method: 'GET',
                url:  openurlservice.url + 'getstudent',
                params: {
                    "companyid": companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status === 'Success') {
                    $scope.studentlist = response.data.msg.student_details;
                    for(var i=0;i<$scope.studentlist.length;i++){
                        $scope.student_name.push($scope.studentlist[i].student_name);
                    }
                } else {
                    if(response.data.session_status === "Expired"){
                        $scope.preloader = false;
                        $scope.preloaderVisible = false;
                        MyApp.alert('Session Expired','', function () {
                            $localStorage.pos_user_email_id = "";
                            $scope.backToPOS();
                        });
                    }else{
//                        MyApp.alert(response.data.msg, 'Failure');
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert(response.data.msg, 'Failure');
            }); 
        };
        var autocompleteDropdownSimple  =  MyApp.autocomplete ({
            input: '#autocomplete-dropdown',
            openIn: 'dropdown',
            
            source: function (autocomplete, query, render) {
               var results  =  [];
               if (query.length === 0) {
                  render(results);
                  return;
               }
               
               // You can find matched items
               for (var i = 0; i < $scope.student_name.length; i++) {
                  if ($scope.student_name[i].toLowerCase().indexOf(query.toLowerCase()) >= 0) 
                    results.push($scope.student_name[i]);
               }
               // Display the items by passing array with result items
               render(results);
            }
         });
         // RESET PAYMENT METHOD SELECTION         
         $scope.selectPaymentMethod = function () {
            $scope.trial_check_number = '';   
            $scope.selectedcardindex = '';
            $scope.trial_credit_card_id = '';
            $scope.trial_credit_card_status = '';
            $scope.trial_cc_name = '';
            $scope.trial_cardnumber = '';
            $scope.trial_ccmonth = '';
            $scope.trial_ccyear = '';
            $scope.trial_cvv = '';
            $scope.trial_country = '';
            $scope.trial_postal_code = '';
            $scope.trial_optional_discount = '';
            if($scope.trial_payment_method === 'CC'){ 
               if ($scope.trialcarddetails && $scope.trialcarddetails.length > 0){
                    $scope.showcardselection = true;
                    $scope.shownewcard = false; 
                } else{
                    $scope.showcardselection = false;
                    $scope.shownewcard = true; 
                }
            }else{
                $scope.showcardselection = false;
                $scope.shownewcard = false; 
            }   
            $scope.showTrialTotal();
            $scope.catchOptionalDiscount();
        };
        
        $scope.catchSearchTrialMember = function(){
             if($scope.trial_search_member){
                if ($scope.studentlist.length > 0) {
                    for (var i = 0; i < $scope.studentlist.length; i++) { 
                        if ($scope.trial_search_member === $scope.studentlist[i].student_name){
                            $scope.trial_selected_student_id = $scope.studentlist[i].student_id;
                            $scope.trialreg_col_field_name[0] = $scope.studentlist[i].reg_col_1;
                            $scope.trialreg_col_field_name[1] = $scope.studentlist[i].reg_col_2;
                            $scope.trialreg_col_field_name[2] = $scope.studentlist[i].reg_col_3;
                            if($scope.trialreg_col_field_name[2]){
                                var bchoose_date = $scope.trialreg_col_field_name[2].split("-");
                                if(bchoose_date[1])
                                    document.getElementsByClassName("dob_month")[0].value = bchoose_date[1]; 
                                if(bchoose_date[2])
                                    document.getElementsByClassName("dob_day")[0].value = bchoose_date[2]; 
                                if(bchoose_date[0]){
                                    if(bchoose_date[0] > 1000 && bchoose_date[0] < 4000)
                                        document.getElementsByClassName("dob_year")[0].value = bchoose_date[0];  
                                    else
                                       document.getElementsByClassName("dob_year")[0].value = ''; 
                                }  
                                document.getElementById("trialcolumn2").value = $scope.trialreg_col_field_name[2];  
                            }
                            $scope.trial_firstname = $scope.studentlist[i].buyer_first_name;
                            $scope.trial_lastname = $scope.studentlist[i].buyer_last_name;
                            $scope.trial_email = $scope.studentlist[i].buyer_email;
                            $scope.trial_phone = $scope.studentlist[i].buyer_phone;
                            $scope.trial_participant_Streetaddress = $scope.studentlist[i].participant_street;
                            $scope.trial_participant_City = $scope.studentlist[i].participant_city;
                            $scope.trial_participant_State = $scope.studentlist[i].participant_state;
                            $scope.trial_participant_Zip = $scope.studentlist[i].participant_zip;
                            $scope.trial_participant_Country = $scope.studentlist[i].participant_country;
                            $(".form-group > input.ng-valid").parents('.form-group').addClass('focused');
                            for(var j = 0; j < 10; j++){
                                $scope.catchTrialInputBlur('trialcolumn'+j,$scope.trialreg_col_field_name[j]); // For Dynamic registration column
                            }
                            //If no values then remove focus class
                            if(!$scope.trial_firstname)
                                $("#trial_firstName").parents('.form-group').removeClass('focused');
                            if(!$scope.trial_lastname)
                                $("#trial_lastName").parents('.form-group').removeClass('focused');
                            if(!$scope.trial_email)
                                $("#trial_email").parents('.form-group').removeClass('focused');
                            if(!$scope.trial_phone)
                                $("#trial_phone").parents('.form-group').removeClass('focused');
                            if(!$scope.trial_participant_Streetaddress)
                                $("#trial_participant_Streetaddress").parents('.form-group').removeClass('focused');
                            if(!$scope.trial_participant_City)
                                $("#trial_participant_City").parents('.form-group').removeClass('focused');
                            if(!$scope.trial_participant_State)
                                $("#trial_participant_State").parents('.form-group').removeClass('focused');
                            if(!$scope.trial_participant_Zip)
                                $("#trial_participant_Zip").parents('.form-group').removeClass('focused');
                            if(!$scope.trial_participant_Country)
                                $("#trial_participant_Country").parents('.form-group').removeClass('focused');
                            
                            // ACH FIELDS RESET
                            if(!$scope.trial_ach_route_no)
                                $("#trial_ach_route_no").parents('.form-group').removeClass('focused');
                            if(!$scope.trial_ach_acc_number)
                                $("#trial_ach_acc_number").parents('.form-group').removeClass('focused');
                            if(!$scope.trial_ach_holder_name)
                                $("#trial_ach_holder_name").parents('.form-group').removeClass('focused');
                            if(!$scope.trial_ach_acc_type)
                                $("#trial_ach_acc_type").parents('.form-group').removeClass('focused');
                            
                            //For credit card
                            if ($scope.studentlist[i].card_details && $scope.studentlist[i].card_details.length > 0){
                                $scope.trialcarddetails = $scope.studentlist[i].card_details;
                            }else{
                                $scope.trialcarddetails = [];
                            } 
                            if($scope.trial_payment_method === 'CC'){ 
                               if ($scope.studentlist[i].card_details && $scope.studentlist[i].card_details.length > 0){
                                    $scope.showcardselection = true;
                                    $scope.shownewcard = false; 
                                } else{
                                    $scope.showcardselection = false;
                                    $scope.shownewcard = true; 
                                    $("#trial_cc_name").parents('.form-group').removeClass('focused');
                                    $("#trial_cc-number").parents('.form-group').removeClass('focused');
                                    $("#trial_postal_code").parents('.form-group').removeClass('focused');
                                    $("#trial_cvv").parents('.form-group').removeClass('focused');
                                }
                            }else{
                                $scope.showcardselection = false;
                                $scope.shownewcard = false; 
                            }
                        }
                    }
                }
                // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
                $('.ph-text').css('display','none');
            }else{
                $scope.trial_selected_student_id = '';
                $scope.trialreg_col_field_name[0] = '';
                $scope.trialreg_col_field_name[1] = '';
                $scope.trialreg_col_field_name[2] = '';
                document.getElementsByClassName("dob_month")[0].value = '';
                document.getElementsByClassName("dob_day")[0].value = '';
                document.getElementsByClassName("dob_year")[0].value = '';
                document.getElementById("trialcolumn2").value = '';
                $scope.trial_firstname = '';
                $scope.trial_lastname = '';
                $scope.trial_email = '';
                $scope.trial_phone = '';
                $scope.trial_participant_Streetaddress = '';
                $scope.trial_participant_City = '';
                $scope.trial_participant_State = '';
                $scope.trial_participant_Zip = '';
                $scope.trial_participant_Country = '';
                $scope.trial_credit_card_id = '';
                $scope.trial_credit_card_status = '';
                $scope.trial_cc_name = '';
                $scope.trial_cardnumber = '';
                $scope.trial_cvv = '';
                $scope.trial_ccmonth = '';
                $scope.trial_ccyear = '';
                $scope.trial_country = '';
                $scope.trial_postal_code = '';
                $(".form-group > input.ng-valid").parents('.form-group').removeClass('focused');
                $scope.showcardselection = false;
                $scope.shownewcard = true; 
                $scope.trial_cc_name = '';
                $scope.trialcarddetails = [];
                $scope.selectPaymentMethod();
                // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
                $('.ph-text').css('display', 'block');
            }
         };
         $scope.catchTrialCheck = function(){
             $scope.trial_check_number = this.trial_check_number;
         };
         //For selecting existing card of a user
        $scope.selecttrialCardDetail = function (ind) {
            if ($scope.selectedcardindex === 'Add New credit card') {
                $scope.shownewcard = true;
                $scope.trial_credit_card_id = '';
                $scope.trial_credit_card_status = '';
                $scope.trial_cc_name = '';
                $scope.trial_cardnumber = '';
                $scope.trial_cvv = '';
                $scope.trial_ccmonth = '';
                $scope.trial_ccyear = '';
                $scope.trial_country = '';
                $scope.trial_postal_code = '';
                $("#trial_cc_name").parents('.form-group').removeClass('focused');
                $("#trial_cc-number").parents('.form-group').removeClass('focused');
                $("#trial_postal_code").parents('.form-group').removeClass('focused');
                $("#trial_cvv").parents('.form-group').removeClass('focused');
            } else {
                $scope.shownewcard = false;
                if($scope.stripe_enabled) {
                    $scope.payment_method_id = $scope.trialcarddetails[ind].payment_method_id;
                    $scope.stripe_customer_id = $scope.trialcarddetails[ind].stripe_customer_id;
                } else {
                    $scope.trial_credit_card_id = parseInt($scope.trialcarddetails[ind].credit_card_id);
                    $scope.trial_credit_card_status = $scope.trialcarddetails[ind].credit_card_status;
                    $scope.trial_cardnumber = $scope.trialcarddetails[ind].credit_card_name;
                    $scope.trial_ccmonth = $scope.trialcarddetails[ind].credit_card_expiration_month;
                    $scope.trial_ccyear = $scope.trialcarddetails[ind].credit_card_expiration_year;
                    $scope.trial_country = $scope.trialcarddetails[ind].credit_card_country;
                    $scope.trial_postal_code = $scope.trialcarddetails[ind].postal_code;
                }
            }
        };
        $scope.resetoptionaldiscount = function(){
            $scope.trial_optional_discount = '';
            $scope.trial_optional_processing_fee_value = '';
            $scope.optional_discount_applied_flag = 'N';
            $scope.erroroptionaldiscount = false;
            $scope.erroroptionalMsg = '';
            $scope.trial_amount_view = $scope.static_trial_amount;
            $scope.trial_processing_fee_value = $scope.static_trial_processing_fee_value;
            $scope.trial_total_amount_view = $scope.static_trial_amount;
        };
        //For optional discount
        $scope.catchOptionalDiscount = function(){
             var temp_trial_amount_view = 0;
             $scope.trial_optional_discount = this.trial_optional_discount;
             
             if (parseFloat($scope.trial_optional_discount) > 0 && $scope.trial_amount_view > 0) {
                if (parseFloat($scope.trial_optional_discount) <= parseFloat($scope.actual_trial_cost)) {
                    temp_trial_amount_view = parseFloat($scope.actual_trial_cost) - parseFloat($scope.trial_optional_discount);
                    if ((parseFloat($scope.actual_trial_cost) === parseFloat($scope.trial_optional_discount))) { //changed to zero
                        $scope.optional_discount_applied_flag = 'Y';
                        $scope.erroroptionaldiscount = false;
                        $scope.erroroptionalMsg = '';
                        $scope.trial_amount_view = 0;
                        $scope.trial_optional_processing_fee_value = 0;
                    } else if (temp_trial_amount_view < 5 && temp_trial_amount_view > 0) {
                        $scope.trial_amount_view = $scope.static_trial_amount;
                        $scope.trial_optional_processing_fee_value = $scope.static_trial_processing_fee_value;
                        $scope.erroroptionaldiscount = true;
                        $scope.erroroptionalMsg = 'After discount applied the amount should be greater than or equal to '+$scope.wp_currency_symbol+' 5';
                        $scope.optional_discount_applied_flag = 'N';
                    } else if (temp_trial_amount_view >= 5) {
                        $scope.optional_discount_applied_flag = 'Y';
                        $scope.erroroptionaldiscount = false;
                        $scope.erroroptionalMsg = '';
                        //   calculation for processing fee
                        var process_fee_per = 0, process_fee_val = 0;
                        var process_fee_per = $scope.processing_fee_percentage;
                        var process_fee_val = $scope.processing_fee_transaction;
                        if($scope.trial_payment_method === 'CC'){
                            if ($scope.trial_processing_fee_type === 2 || $scope.trial_processing_fee_type === '2') {
                                if (temp_trial_amount_view > 0) {
                                    var process_fee1 = +temp_trial_amount_view + +((+temp_trial_amount_view * +process_fee_per) / 100) + +process_fee_val;
                                    var process_fee = +((+process_fee1 * +process_fee_per) / 100) + +process_fee_val;
                                    $scope.trial_optional_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                                } else {
                                    $scope.trial_optional_processing_fee_value = $scope.static_trial_processing_fee_value;
                                }
                                $scope.trial_amount_view = parseFloat(temp_trial_amount_view) + parseFloat($scope.trial_optional_processing_fee_value);
                            } else if ($scope.trial_processing_fee_type === 1 || $scope.trial_processing_fee_type === '1') {
                                if (temp_trial_amount_view > 0) {
                                    var process_fee = +((+temp_trial_amount_view * +process_fee_per) / 100) + +process_fee_val;
                                    $scope.trial_optional_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                                } else {
                                    $scope.trial_optional_processing_fee_value = $scope.static_trial_processing_fee_value;
                                }
                                $scope.trial_amount_view = temp_trial_amount_view;
                            }
                        }else{
                             $scope.trial_amount_view = temp_trial_amount_view;
                             $scope.trial_optional_processing_fee_value = 0;
                        }
                    }
                } else {
                    $scope.optional_discount_applied_flag = 'N';
                    $scope.trial_amount_view = $scope.static_trial_amount;
                    $scope.trial_optional_processing_fee_value = $scope.static_trial_processing_fee_value;
                    $scope.erroroptionaldiscount = true;
                    $scope.erroroptionalMsg = 'Discount amount should not be greater than actual amount';
                }
            } else {
                $scope.optional_discount_applied_flag = 'N';
                $scope.trial_optional_processing_fee_value = $scope.static_trial_processing_fee_value;
                $scope.trial_amount_view = $scope.static_trial_amount;
                $scope.erroroptionaldiscount = false;
                $scope.erroroptionalMsg = '';
            }
            $scope.trial_total_amount_view = $scope.trial_amount_view;
            $scope.trial_processing_fee_value = $scope.trial_optional_processing_fee_value;
        };
        
        $scope.backToPOS = function () {
            // CANCEL TRIAL CART ACTIONS
                $scope.resetCartAfterTrialPaymentSucceed('POS');
            // TAKE USER TO POS HOME
            var hosturl;
            if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
               window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                       window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                    var origin = window.location.origin;
                    hosturl = origin;
                }

            } else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            }  else if (window.location.hostname === 'beta.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'https://' + www_url;
                }
            }   else if (window.location.hostname === 'stage.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'localhost') {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            } else {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            }
            var pos_type, pos_string, pos_entry_type;
            if ($scope.pos_user_type === 'S'){
                pos_type = 'spos';
                pos_string = 'e92213e6721525d7spos712627e6db9b98b4';
            }else if($scope.pos_user_type === 'P'){
                pos_type = 'ppos';
                pos_string = '8f2f44c8580cc72ppos700ff5d197b5a18f0';
            }else{
                pos_type = '';
                pos_string = '';
            }
            if($scope.pos_entry_type){
                pos_entry_type = $scope.pos_entry_type;
            }else{
                pos_entry_type = 'l';
            }
            if (pos_type) {
                $window.open(hosturl+'/' + 'pos' + '/?=' + $scope.param1 + '/' + $scope.param2 + '/' + pos_type + '-' + pos_string + '/'+pos_entry_type+'/', '_self');
            }
        };
        
        $scope.catchTrialInputBlur = function (selectedid,inputvalue){
            if (inputvalue === "" || inputvalue === undefined) {
                $('#'+selectedid).parents('.form-group').removeClass('focused');
            } else {
                $('#'+selectedid).parents('.form-group').addClass('focused');
            }
        };
        $scope.catchTrialInputFocus = function (selectedid){
                $('#'+selectedid).parents('.form-group').addClass('focused');
        };
        
        $scope.handleCheckout = function(){
            if($scope.stripe_enabled){
                $scope.trialStripeCheckout();
            }else{
                $scope.trialCheckout('', '', $scope.trial_firstname, $scope.trial_lastname, $scope.trial_email, $scope.trial_phone, $scope.trial_postal_code, $scope.trial_country);
            }
        };
        
        //DOCUMENT READY FUNCTION
        angular.element(document).ready(function () {
            // NEW ANIMATION FORM PLACEHOLDER TO LABELS
            $('input').focus(function () {
                $(this).parents('.form-group').addClass('focused');
            });

            $('input').blur(function () {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.form-group').removeClass('focused');
                } else {
                    $(this).addClass('filled');
                }
            });
            ($(window).width() < 641) ? $scope.mobiledevice = true : $scope.mobiledevice = false;
            $scope.urlredirection();
            
        });
        
        }]);

MyApp.angular.filter('date2', function ($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function (input, format) {
        console.clear();
        console.log(input, format);
        var dtfilter = $filter('date')(input, format);
        var day = parseInt($filter('date')(input, 'dd'));
        var relevantDigits = (day < 30) ? day % 20 : day % 30;
        console.log(day, relevantDigits);
        var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
        return dtfilter;
    };
});

MyApp.angular.filter("trust", ['$sce', function ($sce) {
        return function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        }
    }]);

MyApp.angular.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
 MyApp.angular.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');

                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});

//compile-template

MyApp.angular.directive('compileTemplate', function($compile, $parse){
    return {
          link: function(scope, element, attr){
            var parsed = $parse(attr.ngBindHtml);
          function getStringValue() { return (parsed(scope) || '').toString(); }

            //Recompile if the template changes
          scope.$watch(getStringValue, function() {
                $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
            });
        }
    }
});

//Username validation
MyApp.angular.directive('validUserName', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            function fromUser(text) {
                var transformedInput = text.replace(/[^a-zA-Z0-9'_]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});

//NAME VALIDATION
MyApp.angular.directive('validName', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            function fromUser(text) {
                var transformedInput = text.replace(/[^a-zA-Z'\s]/g, '');
                        if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

//UTF-8 VALIDATION
MyApp.angular.directive('validText', function () {
    return {
        require: '?ngModel',
        scope: {
            model: '=ngModel'
        },
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            function fromUser(text) {
                var transformedInput = text.replace(/[^a-zA-Z0-9$&+,:;=?@#|'<>.^*()%!-\/_\s]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

