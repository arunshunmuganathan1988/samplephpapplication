<?php

require_once __DIR__."/../../Globals/cont_log.php";
require_once 'WePay.php';
require_once 'PHPMailer_5.2.1/class.phpmailer.php';
require_once 'PHPMailer_5.2.1/class.smtp.php';
//require_once __DIR__.'/../../aws/aws-autoloader.php';
require_once __DIR__.'/../../aws/aws.phar';
require_once '../../Stripe/init.php';

use Aws\Sns\SnsClient;

class MembershipModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $server_url;
    private $stripe_pk = '', $stripe_sk = '';

    public function __construct() {
        require_once __DIR__."/../../Globals/config.php";
        require_once  __DIR__ . "/../../Globals/stripe_props.php";
        $this->inputs();
        $this->wp = new wepay();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = __DIR__."/../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'dev.mystudio.academy') !== false){  //Development
            $this->server_url = "http://dev.mystudio.academy";
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME'];
        }else{
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    
    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }
    
    protected function wepayStatus($company_id){
        $query = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if(!empty(trim($curr))){
                    $currency = explode(",", trim($curr));
                }
                if($row['account_state']=='active'){
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                }else{
                    $res = array('status' => "Success", "msg" => "A", "info" => "Deleted/Disabled/Action needed.");
                }
            }else{
                $res = array('status' => "Failed", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }
    
    protected function stripeStatus($company_id){
        $query = sprintf("SELECT * FROM `stripe_account` where `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        
        $account_state = '';
        
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if(!empty(trim($curr))){
                    $currency = explode(",", trim($curr));
                }
                if($row['account_state'] === 'Y'){
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                }else{
                    $res = array('status' => "Success", "msg" => "Y", "info" => "Deleted/Disabled/Action needed.");
                }
            }else{
                $res = array('status' => "Failed", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }
    
    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
    
    public function getAllMembershipDetails($company_id, $reg_type_user) {
        $this->getStripeKeys();
        $stripe_publish_key = $this->stripe_pk;
        $this->getStudioSubscriptionStatus($company_id);
        $stripe_upgrade_status = $stripe_subscription = '';
        //Get Membership POS Status
        if($reg_type_user!='U'){
            $selecsql=sprintf("SELECT s.membership_status FROM `studio_pos_settings` s WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
            $selectresult= mysqli_query($this->db,$selecsql);
            if (!$selectresult) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else{
                if (mysqli_num_rows($selectresult) > 0) {
                    $statusoutput = mysqli_fetch_assoc($selectresult);
                    if($statusoutput['membership_status']=='D'){
                        $res = array('status' => "Failed", "msg" => "Membership POS Setting is disabled. Please enable this in POS Settings.");
                        $this->response($this->json($res), 200);
                    }
                }
            }
        }
        
        //check array_slice in following before alter or add any columns in this query
        $sql = sprintf("SELECT m.`membership_id`, m.`category_status`, m.`category_title`, m.`category_subtitle`, m.`category_image_url`, m.`category_video_url`, m.`category_description`, m.`members_active`, m.`members_onhold`, m.`members_cancelled`, m.`category_life_time_value`, m.`category_average_membership_period`, m.`category_net_sales`, m.`category_sort_order`, IF(m.`category_status`='P','live','Draft') list_type, m.`waiver_policies`, 
            m.`membership_registration_column_1`, m.`membership_reg_col_1_mandatory_flag`, m.`membership_registration_column_2`, m.`membership_reg_col_2_mandatory_flag`, m.`membership_registration_column_3`, m.`membership_reg_col_3_mandatory_flag`, m.`membership_registration_column_4`, m.`membership_reg_col_4_mandatory_flag`, 
                m.`membership_registration_column_5`, m.`membership_reg_col_5_mandatory_flag`, m.`membership_registration_column_6`, m.`membership_reg_col_6_mandatory_flag`, m.`membership_registration_column_7`, m.`membership_reg_col_7_mandatory_flag`, m.`membership_registration_column_8`, m.`membership_reg_col_8_mandatory_flag`, 
                m.`membership_registration_column_9`, m.`membership_reg_col_9_mandatory_flag`, m.`membership_registration_column_10`, m.`membership_reg_col_10_mandatory_flag`,
                mr.`membership_rank_id` rank_id, mr.`rank_name`, mr.`rank_order`,
                mo.`membership_option_id`, mo.`membership_title`, mo.`membership_subtitle`, mo.`membership_description`, IF(mo.`membership_structure`=1, 'OE', mo.`membership_structure`) membership_structure, mo.`membership_processing_fee_type`, 
                mo.`membership_signup_fee`, mo.`membership_fee`, mo.`show_in_app_flg`, mo.`options_net_sales`, mo.`membership_recurring_frequency`, mo.`custom_recurring_frequency_period_type`, 
                mo.`custom_recurring_frequency_period_val`, mo.`prorate_first_payment_flg`, mo.`delay_recurring_payment_start_flg`, mo.`delayed_recurring_payment_type`, mo.`delayed_recurring_payment_val`, mo.`recurring_start_date`, mo.`option_sort_order`, mo.`initial_payment_include_membership_fee`, 
                mo.`billing_options`, mo.`no_of_classes`, mo.`expiration_date_type`, mo.`expiration_date_val`, mo.`expiration_status`, mo.`deposit_amount`, mo.`no_of_payments`, mo.`billing_payment_start_date_type`, mo.`billing_options_payment_start_date`,
                mo.`specific_start_date`,mo.`specific_end_date`,mo.`specific_payment_frequency`,mo.`exclude_from_billing_flag`,mo.`billing_days`,
                mo.`options_members_active`, mo.`options_members_onhold`, mo.`options_members_cancelled`, mo.`options_life_time_value`, mo.`options_average_membership_period`,
                md.`membership_discount_id`, md.`membership_option_id` disc_mem_opt_id, md.`discount_off`, md.`discount_type`, md.`discount_code`, md.`discount_amount`,
                mbx.`membership_billing_exclude_id`, mbx.`membership_option_id` exclude_mem_opt_id, mbx.`billing_exclude_startdate`, mbx.`billing_exclude_enddate`, mbx.`deleted_flag`,
                c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`, c.`company_id`, c.`logo_URL`, c.`upgrade_status`, c.`country`, c.`stripe_status`, c.`stripe_subscription`, wp.account_state, wp.currency, pos.`membership_payment_method`
                FROM `membership` m LEFT JOIN `membership_ranks` mr ON m.`membership_id` = mr.`membership_id` AND m.`company_id` = mr.`company_id` AND mr.`rank_deleted_flg`!='D'
                LEFT JOIN `membership_options` mo ON m.`membership_id` = mo.`membership_id` AND m.`company_id` = mo.`company_id` AND mo.`deleted_flg`!='D'
                LEFT JOIN `membership_discount` md ON m.`membership_id` = md.`membership_id` AND mo.`membership_option_id` = md.`membership_option_id` AND m.`company_id` = md.`company_id` AND md.`is_expired`='N'
                LEFT JOIN `membership_billing_exclude` mbx ON m.`membership_id` = mbx.`membership_id` AND mo.`membership_option_id` = mbx.`membership_option_id` AND m.`company_id` = mbx.`company_id` AND mbx.`deleted_flag`!='D'
                LEFT JOIN `company` c ON m.`company_id` = c.`company_id`
                LEFT JOIN `wp_account` wp ON m.`company_id` = wp.`company_id`
                LEFT JOIN `studio_pos_settings` pos ON m.`company_id` = pos.`company_id`
                WHERE m.`company_id`='%s' AND m.`deleted_flg`!='D' AND `category_status` = 'P'  AND IF(membership_structure='SE', IF(specific_end_date IS NOT NULL && specific_end_date!='0000-00-00', specific_end_date>=CURDATE(), 0), 1)  order by `category_sort_order` desc", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $membership_arr = $membership_id_arr = $membership_options_arr = $membership_options_id_arr = $membership_billing_exclude_id_arr=[];
            $membership_rank_arr = $membership_rank_id_arr = $membership_disc_arr = $membership_disc_id_arr = $membership_billing_exclude=[];
            $company_name = $logo_URL = $upgrade_status = $wp_currency_code = $wp_currency_symbol = '';
            $wepay_status = 'N';
            $currency = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                    $company_name = $row['company_name'];
                    if(!empty(trim($row['logo_URL']))){
                        $logo_URL = $row['logo_URL'];
                    }else{
                        $logo_URL = $this->server_url."/uploads/Default/default_logo.png";
                    }
                    $upgrade_status = $row['upgrade_status'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    $pos_mem_payment_method = $row['membership_payment_method'];
                    $stripe_upgrade_status = $row['stripe_status'];
                    $stripe_subscription = $row['stripe_subscription'];
                    $country = $row['country'];
                    if(!empty($row['account_state'])){
                        $account_state = $row['account_state'];
                        $curr = $row['currency'];
                        if(!empty(trim($curr))){
                            $currency = explode(",", trim($curr));
                        }
                        $wepay_status = 'Y';
                    }
                    
                    $reg_columns = [];
                    $membership_id = $row['membership_id'];
                    $billing_days = $row['billing_days']; 
//                    $selected_days_new[] = explode(",", $selecteddays);
                    $row_category = array_slice($row, 0, 16);
                    $reg_field_name_array = [];
                    $temp_reg_field = "membership_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "membership_reg_col_".$z."_mandatory_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row_category['reg_columns'] = $reg_field_name_array;
                    if(!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)){
                        $membership_id_arr[] = $membership_id;
                        $membership_arr[] = $row_category;
                    }
                    
                    $row_ranks = array_slice($row, 36, 3);
                    $row_ranks['membership_id'] = $membership_id;
                    $rank_id = $row_ranks['rank_id'];
                    if(!empty($rank_id) && !in_array($rank_id, $membership_rank_id_arr, true)){
                        $membership_rank_id_arr[] = $rank_id;
                        $membership_rank_arr[] = $row_ranks;
                    }
                    
                    
//                    $row_options = array_slice($row, 39, 34);
                    $row_options = array_slice($row, 39, 39);
                    $row_options['membership_id'] = $membership_id;
                    $option_id = $row_options['membership_option_id'];
                    if(!empty($option_id) && !in_array($option_id, $membership_options_id_arr, true)){
                        $membership_options_id_arr[] = $option_id;                        
                        $reg_field_name_array = [];
                        $temp_reg_field = "membership_registration_column_";
                        for($z=1;$z<=10;$z++){ //collect registration fields as array
                            $y=$z-1;
                            $reg_field = $temp_reg_field."$z";      //for filed names
                            $reg_field_mandatory = "membership_reg_col_".$z."_mandatory_flag";
                            if($row[$reg_field]!=''){
                                $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                            }
                        }
                        $row_options['reg_columns'] = $reg_field_name_array;
                        $row_options['waiver_policies'] = $row['waiver_policies'];                   
                        $membership_options_arr[] = $row_options;
                    }
                    
//                    $row_disc = array_slice($row, 73);
                    $row_disc = array_slice($row, 78, 6);
                    $row_disc['membership_id'] = $membership_id;
                    $disc_id = $row_disc['membership_discount_id'];
                    if(!empty($disc_id) && !in_array($disc_id, $membership_disc_id_arr, true)){
                        $row_disc['membership_option_id'] = $row_disc['disc_mem_opt_id'];
                        $membership_disc_id_arr[] = $disc_id;
                        $membership_disc_arr[] = $row_disc;
                    }
                    
                    $row_exclude_days = array_slice($row, 84, 5);
                    $row_exclude_days['membership_id'] = $membership_id;
                    $membership_billing_exclude_id = $row_exclude_days['membership_billing_exclude_id'];

                    if (!empty($membership_billing_exclude_id) && !in_array($membership_billing_exclude_id, $membership_billing_exclude_id_arr, true)) {
                        $row_exclude_days['membership_option_id'] = $row_exclude_days['exclude_mem_opt_id'];
                        $membership_billing_exclude_id_arr[] = $membership_billing_exclude_id;
                        $membership_billing_exclude[] = $row_exclude_days;
                    }
                }
                
                for($i=0;$i<count($membership_arr);$i++){
                    $rank = $rank_id = $option = $option_id = [];
                    $m_id = $membership_arr[$i]['membership_id'];
                    for($j=0;$j<count($membership_rank_arr);$j++){
                        if($m_id == $membership_rank_arr[$j]['membership_id'] && !in_array($membership_rank_arr[$j]['rank_id'], $rank_id)){
                            $rank_id[] = $membership_rank_arr[$j]['rank_id'];
                            $rank[] = $membership_rank_arr[$j];
                        }
                    }
                    for ($k = 0; $k < count($membership_options_arr); $k++) {
                        $disc = $disc_id = [];
                        $exclude = $membership_billing_exclude_id = [];
                        if ($m_id == $membership_options_arr[$k]['membership_id']) {
                            for ($l = 0; $l < count($membership_disc_arr); $l++) {
                                if ($m_id == $membership_disc_arr[$l]['membership_id'] && $membership_disc_arr[$l]['disc_mem_opt_id'] == $membership_options_arr[$k]['membership_option_id']) {
                                    $disc[] = $membership_disc_arr[$l];
                                }
                            }
                            $membership_options_arr[$k]['membership_disc'] = $disc;

                            for ($m = 0; $m < count($membership_billing_exclude); $m++) {
                                if ($m_id == $membership_billing_exclude[$m]['membership_id'] && $membership_billing_exclude[$m]['exclude_mem_opt_id'] == $membership_options_arr[$k]['membership_option_id']) {
                                    $exclude[] = $membership_billing_exclude[$m];
                                }
                            }
                            $membership_options_arr[$k]['mem_billing_exclude'] = $exclude;
                            $option[] = $membership_options_arr[$k];
                        }
                    }
                    if(count($rank)>0){
                        usort($rank, function($a, $b) {
                            return $a['rank_order'] > $b['rank_order'];
                        });
                    }
                    if(count($option)>0){
                        usort($option, function($a, $b) {
                            return $a['option_sort_order'] < $b['option_sort_order'];
                        });
                    }
                    $membership_arr[$i]['rank_details'] = $rank;
                    $membership_arr[$i]['membership_options'] = $option;
                }
                
                if(count($membership_arr)>0){
                    usort($membership_arr, function($a, $b) {
                        return $a['category_sort_order'] < $b['category_sort_order'];
                    });
                }
                
                $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $country_payment_specific = $this->checkCountryPaymentSupport($company_id,$country,1); // callback - 1/0
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = preg_replace('~[\r\n]+~', '', $process_fee['PT']);
                $company_details['pos_mem_payment_method'] = $pos_mem_payment_method;
                //stripe set
                $company_details['stripe_publish_key'] = $stripe_publish_key;  // KEY FROM - Globals/stripe_props.php
                $company_details['stripe_upgrade_status'] = $stripe_upgrade_status;
                $company_details['stripe_subscription'] = $stripe_subscription;
                $company_details['stripe_country_support'] = $stripe_country_support = ($country_payment_specific['status'] == 'Success' && $country_payment_specific['support_status'] =='Y' && $country_payment_specific['payment_support'] =='S') ? "Y":"N";  // CHECK COUNTRY SUPPORT STATUS FOR STRIPE
                
                $response['live'] = $response['draft'] = [];                
                for($i=0;$i<count($membership_arr);$i++){
                    if($membership_arr[$i]['list_type']=='live'){
                        $response['live'][] = $membership_arr[$i];
                    }else{
                        $response['draft'][] = $membership_arr[$i];
                    }
                }
                
                $out = array('status' => "Success", 'msg' => $response['live'], "company_details" => $company_details);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Membership category details not available.");
                $this->response($this->json($error), 401);
            }
        }
    }
    
    public function getMembershipDetails($company_id, $category_id, $reg_type_user) {
        $this->getStripeKeys();
        $stripe_publish_key = $this->stripe_pk;
        $this->getStudioSubscriptionStatus($company_id);
        $stripe_upgrade_status = $stripe_subscription = '';
        //Get Membership POS Status
        if($reg_type_user!='U'){
            $selecsql=sprintf("SELECT s.membership_status FROM `studio_pos_settings` s WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
            $selectresult= mysqli_query($this->db,$selecsql);
            if (!$selectresult) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else{
                if (mysqli_num_rows($selectresult) > 0) {
                    $statusoutput = mysqli_fetch_assoc($selectresult);
                    if($statusoutput['membership_status']=='D'){
                        $res = array('status' => "Failed", "msg" => "Membership POS Setting is disabled. Please enable this in POS Settings.");
                        $this->response($this->json($res), 200);
                    }
                }
            }
        }
        
        //check array_slice in following before alter or add any columns in this query
        $sql = sprintf("SELECT m.`membership_id`, m.`category_status`, m.`category_title`, m.`category_subtitle`, m.`category_image_url`, m.`category_video_url`, m.`category_description`, m.`members_active`, m.`members_onhold`, m.`members_cancelled`, m.`category_life_time_value`, m.`category_average_membership_period`, m.`category_net_sales`, m.`category_sort_order`, IF(m.`category_status`='P','live','Draft') list_type, m.`waiver_policies`, 
                m.`membership_registration_column_1`, m.`membership_reg_col_1_mandatory_flag`, m.`membership_registration_column_2`, m.`membership_reg_col_2_mandatory_flag`, m.`membership_registration_column_3`, m.`membership_reg_col_3_mandatory_flag`, m.`membership_registration_column_4`, m.`membership_reg_col_4_mandatory_flag`, 
                m.`membership_registration_column_5`, m.`membership_reg_col_5_mandatory_flag`, m.`membership_registration_column_6`, m.`membership_reg_col_6_mandatory_flag`, m.`membership_registration_column_7`, m.`membership_reg_col_7_mandatory_flag`, m.`membership_registration_column_8`, m.`membership_reg_col_8_mandatory_flag`, 
                m.`membership_registration_column_9`, m.`membership_reg_col_9_mandatory_flag`, m.`membership_registration_column_10`, m.`membership_reg_col_10_mandatory_flag`,
                mr.`membership_rank_id` rank_id, mr.`rank_name`, mr.`rank_order`,
                mo.`membership_option_id`, mo.`membership_title`, mo.`membership_subtitle`, mo.`membership_description`, IF(mo.`membership_structure`=1, 'OE', mo.`membership_structure`) membership_structure, mo.`membership_processing_fee_type`, 
                mo.`membership_signup_fee`, mo.`membership_fee`, mo.`show_in_app_flg`, mo.`options_net_sales`, mo.`membership_recurring_frequency`, mo.`custom_recurring_frequency_period_type`,
                mo.`custom_recurring_frequency_period_val`, mo.`prorate_first_payment_flg`, mo.`delay_recurring_payment_start_flg`, mo.`delayed_recurring_payment_type`, mo.`delayed_recurring_payment_val`, mo.`recurring_start_date`, mo.`option_sort_order`, mo.`initial_payment_include_membership_fee`,
                mo.`billing_options`, mo.`no_of_classes`, mo.`expiration_date_type`, mo.`expiration_date_val`, mo.`expiration_status`, mo.`deposit_amount`, mo.`no_of_payments`, mo.`billing_payment_start_date_type`, mo.`billing_options_payment_start_date`,
                mo.`specific_start_date`,mo.`specific_end_date`,mo.`specific_payment_frequency`,mo.`exclude_from_billing_flag`,mo.`billing_days`,
                mo.`options_members_active`, mo.`options_members_onhold`, mo.`options_members_cancelled`, mo.`options_life_time_value`, mo.`options_average_membership_period`,
                md.`membership_discount_id`, md.`membership_option_id` disc_mem_opt_id, md.`discount_off`, md.`discount_type`, md.`discount_code`, md.`discount_amount`,
                mbx.`membership_billing_exclude_id`,mbx.`membership_option_id`exclude_mem_opt_id, mbx.`billing_exclude_startdate`,mbx.`billing_exclude_enddate`,mbx.`deleted_flag`,
                c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`, c.`company_id`, c.`logo_URL`, c.`upgrade_status`, c.`country`, c.`stripe_status`, c.`stripe_subscription`, wp.account_state, wp.currency
                FROM `membership` m LEFT JOIN `membership_ranks` mr ON m.`membership_id` = mr.`membership_id` AND m.`company_id` = mr.`company_id` AND mr.`rank_deleted_flg`!='D'
                LEFT JOIN `membership_options` mo ON m.`membership_id` = mo.`membership_id` AND m.`company_id` = mo.`company_id` AND mo.`deleted_flg`!='D'
                LEFT JOIN `membership_discount` md ON m.`membership_id` = md.`membership_id` AND mo.`membership_option_id` = md.`membership_option_id` AND m.`company_id` = md.`company_id` AND md.`is_expired`='N'
                LEFT JOIN `membership_billing_exclude` mbx ON m.`membership_id` = mbx.`membership_id` AND mo.`membership_option_id` = mbx.`membership_option_id` AND m.`company_id` = mbx.`company_id` AND mbx.`deleted_flag`!='D'
                LEFT JOIN `company` c ON m.`company_id` = c.`company_id`
                LEFT JOIN `wp_account` wp ON m.`company_id` = wp.`company_id`
                WHERE m.`company_id`='%s' AND m.`membership_id`='%s' AND m.`deleted_flg`!='D' AND `category_status` = 'P' AND IF(membership_structure='SE', IF(specific_end_date IS NOT NULL && specific_end_date!='0000-00-00', specific_end_date>=CURDATE(), 0), 1) ", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$category_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $membership_options_id_arr = $membership_disc_id_arr = $membership_billing_exclude_id_arr = [];
            $membership_options = $membership_category = $membership_disc = $membership_billing_exclude =[];
            $membership_rank = $temp_membership_rank = $temp_cat = [];
            $company_name = $logo_URL = $upgrade_status = $wp_currency_code = $wp_currency_symbol = '';
            $wepay_status = 'N';
            $currency = [];
            
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                    $company_name = $row['company_name'];
                    if(!empty(trim($row['logo_URL']))){
                        $logo_URL = $row['logo_URL'];
                    }else{
                        $logo_URL = $this->server_url."/uploads/Default/default_logo.png";
                    }
                    $upgrade_status = $row['upgrade_status'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    $stripe_upgrade_status = $row['stripe_status'];
                    $stripe_subscription = $row['stripe_subscription'];
                    $country = $row['country'];
                    if(!empty($row['account_state'])){
                        $account_state = $row['account_state'];
                        $curr = $row['currency'];
                        if(!empty(trim($curr))){
                            $currency = explode(",", trim($curr));
                        }
                        $wepay_status = 'Y';
                    }
                    
                    $reg_columns =  [];
                    $membership_id = $row['membership_id'];
                    $billing_days = $row['billing_days']; 
//                    $selected_days_new[] = explode(",", $selecteddays);
                    $row_category = array_slice($row, 0, 16);
                    $reg_field_name_array = [];
                    $temp_reg_field = "membership_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "membership_reg_col_".$z."_mandatory_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row_category['reg_columns'] = $reg_field_name_array;
                    
                    $row_ranks = array_slice($row, 36, 3);
                    $row_ranks['membership_id'] = $membership_id;
                    $rank_id = $row_ranks['rank_id'];
                    if(!empty($rank_id) && !in_array($rank_id, $temp_membership_rank, true)){
                        $temp_membership_rank[] = $rank_id;
                        $membership_rank[] = $row_ranks;
                    }
                    $row_options = array_slice($row, 39, 39);
//                    $row_options = array_slice($row, 39, 34);
                    $row_options['membership_id'] = $membership_id;
                    $option_id = $row_options['membership_option_id'];
                    if(!empty($option_id) && !in_array($option_id, $membership_options_id_arr, true)){
                        $membership_options_id_arr[] = $option_id;
                        $row_options['reg_columns'] = $reg_field_name_array;
                        $row_options['waiver_policies'] = $row['waiver_policies'];
                        $membership_options[] = $row_options;
                    }
                    $row_disc = array_slice($row, 78, 6);
                    $row_disc['membership_id'] = $membership_id;
                    $disc_id = $row_disc['membership_discount_id'];
                    if(!empty($disc_id) && !in_array($disc_id, $membership_disc_id_arr, true)){
                        $row_disc['membership_option_id'] = $row_disc['disc_mem_opt_id'];
                        $membership_disc_id_arr[] = $disc_id;
                        $membership_disc[] = $row_disc;
                    }
                    
                    $row_exclude_days = array_slice($row, 84, 5);
                    $row_exclude_days['membership_id'] = $membership_id;
                    $membership_billing_exclude_id = $row_exclude_days['membership_billing_exclude_id'];

                    if (!empty($membership_billing_exclude_id) && !in_array($membership_billing_exclude_id, $membership_billing_exclude_id_arr, true)) {
                        $row_exclude_days['membership_option_id'] = $row_exclude_days['exclude_mem_opt_id'];
                        $membership_billing_exclude_id_arr[] = $membership_billing_exclude_id;
                        $membership_billing_exclude[] = $row_exclude_days;
                    }
                    
                    if(!empty($membership_id) && !in_array($membership_id, $temp_cat, true)){
                        $temp_cat[] = $membership_id;
                        $membership_category = $row_category;
                    }
                }
                    
                for ($k = 0; $k < count($membership_options); $k++) {
                    $disc = $exclude = [];
                    for ($l = 0; $l < count($membership_disc); $l++) {
                        if ($membership_disc[$l]['disc_mem_opt_id'] == $membership_options[$k]['membership_option_id']) {
                            $disc[] = $membership_disc[$l];
                        }
                    }
                    $membership_options[$k]['membership_disc'] = $disc;

                    for ($m = 0; $m < count($membership_billing_exclude); $m++) {
                        if ($membership_billing_exclude[$m]['exclude_mem_opt_id'] == $membership_options[$k]['membership_option_id']) {
                            $exclude[] = $membership_billing_exclude[$m];
                        }
                    }
                    $membership_options[$k]['mem_billing_exclude'] = $exclude;
                }
                
                $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $country_payment_specific = $this->checkCountryPaymentSupport($company_id,$country,1); // callback - 1/0
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = preg_replace('~[\r\n]+~', '', $process_fee['PT']);
                //stripe set
                $company_details['stripe_publish_key'] = $stripe_publish_key;  // KEY FROM - Globals/stripe_props.php
                $company_details['stripe_upgrade_status'] = $stripe_upgrade_status;
                $company_details['stripe_subscription'] = $stripe_subscription;
                $company_details['stripe_country_support'] = $stripe_country_support = ($country_payment_specific['status'] == 'Success' && $country_payment_specific['support_status'] =='Y' && $country_payment_specific['payment_support'] =='S') ? "Y":"N";  // CHECK COUNTRY SUPPORT STATUS FOR STRIPE

                usort($membership_rank, function($a, $b) {
                    return $a['rank_order'] > $b['rank_order'];
                });
                
                usort($membership_options, function($a, $b) {
                    return $a['option_sort_order'] < $b['option_sort_order'];
                });
                
                $membership_category['rank_details'] = $membership_rank;
                $membership_category['membership_options'] = $membership_options;
                
                $out = array('status' => "Success", 'msg' => $membership_category, "company_details" => $company_details);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Membership category details not available.");
                $this->response($this->json($error), 401);
            }
        }
    }
    
    public function getAllMembershipDetailsForSocialSharing($company_id) {
        //check array_slice in following before alter or add any columns in this query
        $sql = sprintf("SELECT m.`membership_id`, m.`category_status`, m.`category_title`, m.`category_subtitle`, m.`category_image_url`, m.`category_video_url`, m.`category_description`, m.`members_active`, m.`members_onhold`, m.`members_cancelled`, m.`category_life_time_value`, m.`category_average_membership_period`, m.`category_net_sales`, m.`category_sort_order`, IF(m.`category_status`='P','live','Draft') list_type, m.`waiver_policies`, 
            m.`membership_registration_column_1`, m.`membership_reg_col_1_mandatory_flag`, m.`membership_registration_column_2`, m.`membership_reg_col_2_mandatory_flag`, m.`membership_registration_column_3`, m.`membership_reg_col_3_mandatory_flag`, m.`membership_registration_column_4`, m.`membership_reg_col_4_mandatory_flag`, 
                m.`membership_registration_column_5`, m.`membership_reg_col_5_mandatory_flag`, m.`membership_registration_column_6`, m.`membership_reg_col_6_mandatory_flag`, m.`membership_registration_column_7`, m.`membership_reg_col_7_mandatory_flag`, m.`membership_registration_column_8`, m.`membership_reg_col_8_mandatory_flag`, 
                m.`membership_registration_column_9`, m.`membership_reg_col_9_mandatory_flag`, m.`membership_registration_column_10`, m.`membership_reg_col_10_mandatory_flag`,
                mr.`membership_rank_id` rank_id, mr.`rank_name`, mr.`rank_order`,
                mo.`membership_option_id`, mo.`membership_title`, mo.`membership_subtitle`, mo.`membership_description`, IF(mo.`membership_structure`=1, 'OE', mo.`membership_structure`) membership_structure, mo.`membership_processing_fee_type`, 
                mo.`membership_signup_fee`, mo.`membership_fee`, mo.`show_in_app_flg`, mo.`options_net_sales`, mo.`membership_recurring_frequency`, mo.`custom_recurring_frequency_period_type`, 
                mo.`custom_recurring_frequency_period_val`, mo.`prorate_first_payment_flg`, mo.`delay_recurring_payment_start_flg`, mo.`delayed_recurring_payment_type`, mo.`delayed_recurring_payment_val`, mo.`recurring_start_date`, mo.`option_sort_order`, mo.`initial_payment_include_membership_fee`, 
                mo.`billing_options`, mo.`no_of_classes`, mo.`expiration_date_type`, mo.`expiration_date_val`, mo.`expiration_status`, mo.`deposit_amount`, mo.`no_of_payments`, mo.`billing_payment_start_date_type`, mo.`billing_options_payment_start_date`,
                mo.`specific_start_date`,mo.`specific_end_date`,mo.`specific_payment_frequency`,mo.`exclude_from_billing_flag`,mo.`billing_days`,
                mo.`options_members_active`, mo.`options_members_onhold`, mo.`options_members_cancelled`, mo.`options_life_time_value`, mo.`options_average_membership_period`,
                md.`membership_discount_id`, md.`membership_option_id` disc_mem_opt_id, md.`discount_off`, md.`discount_type`, md.`discount_code`, md.`discount_amount`,
                mbx.`membership_billing_exclude_id`, mbx.`membership_option_id` exclude_mem_opt_id, mbx.`billing_exclude_startdate`, mbx.`billing_exclude_enddate`, mbx.`deleted_flag`,
                c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`, c.`company_id`, c.`logo_URL`, c.`upgrade_status`, wp.account_state, wp.currency
                FROM `membership` m LEFT JOIN `membership_ranks` mr ON m.`membership_id` = mr.`membership_id` AND m.`company_id` = mr.`company_id` AND mr.`rank_deleted_flg`!='D'
                LEFT JOIN `membership_options` mo ON m.`membership_id` = mo.`membership_id` AND m.`company_id` = mo.`company_id` AND mo.`deleted_flg`!='D'
                LEFT JOIN `membership_discount` md ON m.`membership_id` = md.`membership_id` AND mo.`membership_option_id` = md.`membership_option_id` AND m.`company_id` = md.`company_id` AND md.`is_expired`='N'
                LEFT JOIN `membership_billing_exclude` mbx ON m.`membership_id` = mbx.`membership_id` AND mo.`membership_option_id` = mbx.`membership_option_id` AND m.`company_id` = mbx.`company_id` AND mbx.`deleted_flag`!='D'
                LEFT JOIN `company` c ON m.`company_id` = c.`company_id`
                LEFT JOIN `wp_account` wp ON m.`company_id` = wp.`company_id`
                WHERE m.`company_id`='%s' AND m.`deleted_flg`!='D' AND `category_status` = 'P' order by `category_sort_order` desc", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "code"=>500, "title"=>"Server Error");
            return $error;
        } else {
            $membership_arr = $membership_id_arr = $membership_options_arr = $membership_options_id_arr = $membership_billing_exclude_id_arr=[];
            $membership_rank_arr = $membership_rank_id_arr = $membership_disc_arr = $membership_disc_id_arr = $membership_billing_exclude=[];
            $company_name = $logo_URL = $upgrade_status = $wp_currency_code = $wp_currency_symbol = '';
            $wepay_status = 'N';
            $currency = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                    $company_name = $row['company_name'];
                    $logo_URL = $row['logo_URL'];
                    $upgrade_status = $row['upgrade_status'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    if(!empty($row['account_state'])){
                        $account_state = $row['account_state'];
                        $curr = $row['currency'];
                        if(!empty(trim($curr))){
                            $currency = explode(",", trim($curr));
                        }
                        $wepay_status = 'Y';
                    }
                    
                    $reg_columns = [];
                    $membership_id = $row['membership_id'];
                    $billing_days = $row['billing_days']; 
//                    $selected_days_new[] = explode(",", $selecteddays);
                    $row_category = array_slice($row, 0, 16);
                    $reg_field_name_array = [];
                    $temp_reg_field = "membership_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "membership_reg_col_".$z."_mandatory_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row_category['reg_columns'] = $reg_field_name_array;
                    if(!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)){
                        $membership_id_arr[] = $membership_id;
                        $membership_arr[] = $row_category;
                    }
                    
                    $row_ranks = array_slice($row, 36, 3);
                    $row_ranks['membership_id'] = $membership_id;
                    $rank_id = $row_ranks['rank_id'];
                    if(!empty($rank_id) && !in_array($rank_id, $membership_rank_id_arr, true)){
                        $membership_rank_id_arr[] = $rank_id;
                        $membership_rank_arr[] = $row_ranks;
                    }
                    
                    
//                    $row_options = array_slice($row, 39, 34);
                    $row_options = array_slice($row, 39, 39);
                    $row_options['membership_id'] = $membership_id;
                    $option_id = $row_options['membership_option_id'];
                    if(!empty($option_id) && !in_array($option_id, $membership_options_id_arr, true)){
                        $membership_options_id_arr[] = $option_id;                        
                        $reg_field_name_array = [];
                        $temp_reg_field = "membership_registration_column_";
                        for($z=1;$z<=10;$z++){ //collect registration fields as array
                            $y=$z-1;
                            $reg_field = $temp_reg_field."$z";      //for filed names
                            $reg_field_mandatory = "membership_reg_col_".$z."_mandatory_flag";
                            if($row[$reg_field]!=''){
                                $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                            }
                        }
                        $row_options['reg_columns'] = $reg_field_name_array;
                        $row_options['waiver_policies'] = $row['waiver_policies'];                   
                        $membership_options_arr[] = $row_options;
                    }
                    
//                    $row_disc = array_slice($row, 73);
                    $row_disc = array_slice($row, 78, 6);
                    $row_disc['membership_id'] = $membership_id;
                    $disc_id = $row_disc['membership_discount_id'];
                    if(!empty($disc_id) && !in_array($disc_id, $membership_disc_id_arr, true)){
                        $row_disc['membership_option_id'] = $row_disc['disc_mem_opt_id'];
                        $membership_disc_id_arr[] = $disc_id;
                        $membership_disc_arr[] = $row_disc;
                    }
                    
                    $row_exclude_days = array_slice($row, 84, 5);
                    $row_exclude_days['membership_id'] = $membership_id;
                    $membership_billing_exclude_id = $row_exclude_days['membership_billing_exclude_id'];

                    if (!empty($membership_billing_exclude_id) && !in_array($membership_billing_exclude_id, $membership_billing_exclude_id_arr, true)) {
                        $row_exclude_days['membership_option_id'] = $row_exclude_days['exclude_mem_opt_id'];
                        $membership_billing_exclude_id_arr[] = $membership_billing_exclude_id;
                        $membership_billing_exclude[] = $row_exclude_days;
                    }
                }
                
                for($i=0;$i<count($membership_arr);$i++){
                    $rank = $rank_id = $option = $option_id = [];
                    $m_id = $membership_arr[$i]['membership_id'];
                    for($j=0;$j<count($membership_rank_arr);$j++){
                        if($m_id == $membership_rank_arr[$j]['membership_id'] && !in_array($membership_rank_arr[$j]['rank_id'], $rank_id)){
                            $rank_id[] = $membership_rank_arr[$j]['rank_id'];
                            $rank[] = $membership_rank_arr[$j];
                        }
                    }
                    for ($k = 0; $k < count($membership_options_arr); $k++) {
                        $disc = $disc_id = [];
                        $exclude = $membership_billing_exclude_id = [];
                        if ($m_id == $membership_options_arr[$k]['membership_id']) {
                            for ($l = 0; $l < count($membership_disc_arr); $l++) {
                                if ($m_id == $membership_disc_arr[$l]['membership_id'] && $membership_disc_arr[$l]['disc_mem_opt_id'] == $membership_options_arr[$k]['membership_option_id']) {
                                    $disc[] = $membership_disc_arr[$l];
                                }
                            }
                            $membership_options_arr[$k]['membership_disc'] = $disc;

                            for ($m = 0; $m < count($membership_billing_exclude); $m++) {
                                if ($m_id == $membership_billing_exclude[$m]['membership_id'] && $membership_billing_exclude[$m]['exclude_mem_opt_id'] == $membership_options_arr[$k]['membership_option_id']) {
                                    $exclude[] = $membership_billing_exclude[$m];
                                }
                            }
                            $membership_options_arr[$k]['mem_billing_exclude'] = $exclude;
                            $option[] = $membership_options_arr[$k];
                        }
                    }
                    if(count($rank)>0){
                        usort($rank, function($a, $b) {
                            return $a['rank_order'] > $b['rank_order'];
                        });
                    }
                    if(count($option)>0){
                        usort($option, function($a, $b) {
                            return $a['option_sort_order'] < $b['option_sort_order'];
                        });
                    }
                    $membership_arr[$i]['rank_details'] = $rank;
                    $membership_arr[$i]['membership_options'] = $option;
                }
                
                if(count($membership_arr)>0){
                    usort($membership_arr, function($a, $b) {
                        return $a['category_sort_order'] < $b['category_sort_order'];
                    });
                }
                
                $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = preg_replace('~[\r\n]+~', '', $process_fee['PT']);
                
                $response['live'] = $response['draft'] = [];                
                for($i=0;$i<count($membership_arr);$i++){
                    if($membership_arr[$i]['list_type']=='live'){
                        $response['live'][] = $membership_arr[$i];
                    }else{
                        $response['draft'][] = $membership_arr[$i];
                    }
                }
                
                $out = array('status' => "Success", 'msg' => $response['live'][0], "company_details" => $company_details);
                // If success everythig is good send header as "OK" and user details
                return $out;
            } else {
                $error = array('status' => "Failed", "msg" => "Membership category details not available.", "code"=>404, "title"=>"404 Error");
                return $error;
            }
        }
    }
    
    public function getMembershipDetailsForSocialSharing($company_id, $category_id) {
        //check array_slice in following before alter or add any columns in this query
        $sql = sprintf("SELECT m.`membership_id`, m.`category_status`, m.`category_title`, m.`category_subtitle`, m.`category_image_url`, m.`category_video_url`, m.`category_description`, m.`members_active`, m.`members_onhold`, m.`members_cancelled`, m.`category_life_time_value`, m.`category_average_membership_period`, m.`category_net_sales`, m.`category_sort_order`, IF(m.`category_status`='P','live','Draft') list_type, m.`waiver_policies`, 
                m.`membership_registration_column_1`, m.`membership_reg_col_1_mandatory_flag`, m.`membership_registration_column_2`, m.`membership_reg_col_2_mandatory_flag`, m.`membership_registration_column_3`, m.`membership_reg_col_3_mandatory_flag`, m.`membership_registration_column_4`, m.`membership_reg_col_4_mandatory_flag`, 
                m.`membership_registration_column_5`, m.`membership_reg_col_5_mandatory_flag`, m.`membership_registration_column_6`, m.`membership_reg_col_6_mandatory_flag`, m.`membership_registration_column_7`, m.`membership_reg_col_7_mandatory_flag`, m.`membership_registration_column_8`, m.`membership_reg_col_8_mandatory_flag`, 
                m.`membership_registration_column_9`, m.`membership_reg_col_9_mandatory_flag`, m.`membership_registration_column_10`, m.`membership_reg_col_10_mandatory_flag`,
                mr.`membership_rank_id` rank_id, mr.`rank_name`, mr.`rank_order`,
                mo.`membership_option_id`, mo.`membership_title`, mo.`membership_subtitle`, mo.`membership_description`, IF(mo.`membership_structure`=1, 'OE', mo.`membership_structure`) membership_structure, mo.`membership_processing_fee_type`, 
                mo.`membership_signup_fee`, mo.`membership_fee`, mo.`show_in_app_flg`, mo.`options_net_sales`, mo.`membership_recurring_frequency`, mo.`custom_recurring_frequency_period_type`,
                mo.`custom_recurring_frequency_period_val`, mo.`prorate_first_payment_flg`, mo.`delay_recurring_payment_start_flg`, mo.`delayed_recurring_payment_type`, mo.`delayed_recurring_payment_val`, mo.`recurring_start_date`, mo.`option_sort_order`, mo.`initial_payment_include_membership_fee`,
                mo.`billing_options`, mo.`no_of_classes`, mo.`expiration_date_type`, mo.`expiration_date_val`, mo.`expiration_status`, mo.`deposit_amount`, mo.`no_of_payments`, mo.`billing_payment_start_date_type`, mo.`billing_options_payment_start_date`,
                mo.`specific_start_date`,mo.`specific_end_date`,mo.`specific_payment_frequency`,mo.`exclude_from_billing_flag`,mo.`billing_days`,
                mo.`options_members_active`, mo.`options_members_onhold`, mo.`options_members_cancelled`, mo.`options_life_time_value`, mo.`options_average_membership_period`,
                md.`membership_discount_id`, md.`membership_option_id` disc_mem_opt_id, md.`discount_off`, md.`discount_type`, md.`discount_code`, md.`discount_amount`,
                mbx.`membership_billing_exclude_id`,mbx.`membership_option_id`exclude_mem_opt_id, mbx.`billing_exclude_startdate`,mbx.`billing_exclude_enddate`,mbx.`deleted_flag`,
                c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`, c.`company_id`, c.`logo_URL`, c.`upgrade_status`, wp.account_state, wp.currency
                FROM `membership` m LEFT JOIN `membership_ranks` mr ON m.`membership_id` = mr.`membership_id` AND m.`company_id` = mr.`company_id` AND mr.`rank_deleted_flg`!='D'
                LEFT JOIN `membership_options` mo ON m.`membership_id` = mo.`membership_id` AND m.`company_id` = mo.`company_id` AND mo.`deleted_flg`!='D'
                LEFT JOIN `membership_discount` md ON m.`membership_id` = md.`membership_id` AND mo.`membership_option_id` = md.`membership_option_id` AND m.`company_id` = md.`company_id` AND md.`is_expired`='N'
                LEFT JOIN `membership_billing_exclude` mbx ON m.`membership_id` = mbx.`membership_id` AND mo.`membership_option_id` = mbx.`membership_option_id` AND m.`company_id` = mbx.`company_id` AND mbx.`deleted_flag`!='D'
                LEFT JOIN `company` c ON m.`company_id` = c.`company_id`
                LEFT JOIN `wp_account` wp ON m.`company_id` = wp.`company_id`
                WHERE m.`company_id`='%s' AND m.`membership_id`='%s' AND m.`deleted_flg`!='D' AND `category_status` = 'P'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$category_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "code"=>500, "title"=>"Server Error");
            return $error;
        } else {
            $membership_options_id_arr = $membership_disc_id_arr = $membership_billing_exclude_id_arr = [];
            $membership_options = $membership_category = $membership_disc = $membership_billing_exclude =[];
            $membership_rank = $temp_membership_rank = $temp_cat = [];
            $company_name = $logo_URL = $upgrade_status = $wp_currency_code = $wp_currency_symbol = '';
            $wepay_status = 'N';
            $currency = [];
            
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                    $company_name = $row['company_name'];
                    $logo_URL = $row['logo_URL'];
                    $upgrade_status = $row['upgrade_status'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    if(!empty($row['account_state'])){
                        $account_state = $row['account_state'];
                        $curr = $row['currency'];
                        if(!empty(trim($curr))){
                            $currency = explode(",", trim($curr));
                        }
                        $wepay_status = 'Y';
                    }
                    
                    $reg_columns =  [];
                    $membership_id = $row['membership_id'];
                    $billing_days = $row['billing_days']; 
//                    $selected_days_new[] = explode(",", $selecteddays);
                    $row_category = array_slice($row, 0, 16);
                    $reg_field_name_array = [];
                    $temp_reg_field = "membership_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "membership_reg_col_".$z."_mandatory_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row_category['reg_columns'] = $reg_field_name_array;
                    
                    $row_ranks = array_slice($row, 36, 3);
                    $row_ranks['membership_id'] = $membership_id;
                    $rank_id = $row_ranks['rank_id'];
                    if(!empty($rank_id) && !in_array($rank_id, $temp_membership_rank, true)){
                        $temp_membership_rank[] = $rank_id;
                        $membership_rank[] = $row_ranks;
                    }
                    $row_options = array_slice($row, 39, 39);
//                    $row_options = array_slice($row, 39, 34);
                    $row_options['membership_id'] = $membership_id;
                    $option_id = $row_options['membership_option_id'];
                    if(!empty($option_id) && !in_array($option_id, $membership_options_id_arr, true)){
                        $membership_options_id_arr[] = $option_id;
                        $row_options['reg_columns'] = $reg_field_name_array;
                        $row_options['waiver_policies'] = $row['waiver_policies'];
                        $membership_options[] = $row_options;
                    }
                    $row_disc = array_slice($row, 78, 6);
                    $row_disc['membership_id'] = $membership_id;
                    $disc_id = $row_disc['membership_discount_id'];
                    if(!empty($disc_id) && !in_array($disc_id, $membership_disc_id_arr, true)){
                        $row_disc['membership_option_id'] = $row_disc['disc_mem_opt_id'];
                        $membership_disc_id_arr[] = $disc_id;
                        $membership_disc[] = $row_disc;
                    }
                    
                    $row_exclude_days = array_slice($row, 84, 5);
                    $row_exclude_days['membership_id'] = $membership_id;
                    $membership_billing_exclude_id = $row_exclude_days['membership_billing_exclude_id'];

                    if (!empty($membership_billing_exclude_id) && !in_array($membership_billing_exclude_id, $membership_billing_exclude_id_arr, true)) {
                        $row_exclude_days['membership_option_id'] = $row_exclude_days['exclude_mem_opt_id'];
                        $membership_billing_exclude_id_arr[] = $membership_billing_exclude_id;
                        $membership_billing_exclude[] = $row_exclude_days;
                    }
                    
                    if(!empty($membership_id) && !in_array($membership_id, $temp_cat, true)){
                        $temp_cat[] = $membership_id;
                        $membership_category = $row_category;
                    }
                }
                    
                for ($k = 0; $k < count($membership_options); $k++) {
                    $disc = $exclude = [];
                    for ($l = 0; $l < count($membership_disc); $l++) {
                        if ($membership_disc[$l]['disc_mem_opt_id'] == $membership_options[$k]['membership_option_id']) {
                            $disc[] = $membership_disc[$l];
                        }
                    }
                    $membership_options[$k]['membership_disc'] = $disc;

                    for ($m = 0; $m < count($membership_billing_exclude); $m++) {
                        if ($membership_billing_exclude[$m]['exclude_mem_opt_id'] == $membership_options[$k]['membership_option_id']) {
                            $exclude[] = $membership_billing_exclude[$m];
                        }
                    }
                    $membership_options[$k]['mem_billing_exclude'] = $exclude;
                }
                
                $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = preg_replace('~[\r\n]+~', '', $process_fee['PT']);

                usort($membership_rank, function($a, $b) {
                    return $a['rank_order'] > $b['rank_order'];
                });
                
                usort($membership_options, function($a, $b) {
                    return $a['option_sort_order'] < $b['option_sort_order'];
                });
                
                $membership_category['rank_details'] = $membership_rank;
                $membership_category['membership_options'] = $membership_options;
                
                $out = array('status' => "Success", 'msg' => $membership_category, "company_details" => $company_details);
                // If success everythig is good send header as "OK" and user details
                return $out;
            } else {
                $error = array('status' => "Failed", "msg" => "Membership category details not available.", "code"=>404, "title"=>"404 Error");
                return $error;
            }
        }
    }
    
    private function sendCrawlerOutputData($data) {
        $title = $desc = $image = $imageUrl = '';
        if($data['status']=='Failed'){
            if($data['code']==500){
                $title = "Server Error";
                $desc = "Internal Server Error";
            }else{
                $title = "404 Error";
                $desc = "The Event is not available";
            }
        }else{
            
        }
        echo "
        <!DOCTYPE html>
        <html>
            <head>
                <meta property='og:title' content='$title'/>
                <meta property='og:description' content='$desc' />
                <meta property='og:image' content='$image' />
                <!-- etc. -->
            </head>
            <body>
                <p> $desc </p>
                <img src='$imageUrl'>
            </body>
        </html>";
        exit();
    }
    
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone='';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result_tz);                       
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    //wepay membership checkout
    public function membershipCheckout($company_id, $membership_id, $membership_option_id, $actual_student_id,$actual_participant_id, $upgrade_status, $membership_category_title, $membership_title, $membership_desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state,
                    $processing_fee_type, $processing_fee, $membership_fee, $membership_fee_disc, $signup_fee, $signup_fee_disc, $payment_amount, $initial_payment, $first_payment, $payment_frequency, $payment_start_date, $recurring_start_date, 
                    $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $prorate_first_payment_flg, 
                    $delay_recurring_payment_start_flg, $delay_recurring_payment_start_date, $delay_recurring_payment_amount, $custom_recurring_frequency_period_type, $custom_recurring_frequency_period_val, $initial_payment_include_membership_fee, $membership_structure,
                    $billing_options, $no_of_classes, $billing_options_expiration_date, $billing_options_deposit_amount, $billing_options_no_of_payments, $payment_array,
                    $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $membership_discount_code, $signup_discount_code,$exclude_days_array,$program_start_date,$program_end_date,$exclude_from_billing_flag,$reg_type_user,$reg_version_user,$reg_current_date,$membership_start_date,$payment_method,$check_number,$optional_discount,$optional_discount_flag){
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $upgrade_status = $sts['upgrade_status'];
        $payment_method = mysqli_real_escape_string($this->db, $payment_method);
        $buyer_name = $buyer_first_name.' '.$buyer_last_name;
        if (is_null($actual_student_id) || $actual_student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email,'Wepay','');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $actual_student_id;
        }
        if (is_null($actual_participant_id) || $actual_participant_id == 0) {
            $participant_id = $this->getParticipantId($company_id, $student_id, $reg_col1, $reg_col2, $reg_col3);
        } else {
            $participant_id = $actual_participant_id;
        }
        $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id,$membership_category_title, $membership_title, '');
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $current_date = date("Y-m-d");
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg2=array("buyer_email"=>$buyer_email,"membership_title"=>$membership_title,"gmt_date"=>$gmt_date);
        if(($membership_structure=='NC' ||$membership_structure=='C' || $membership_structure=='SE') && $billing_options=='PP' && !empty($payment_array)){
            if($payment_amount<5){
                $payment_amount = $payment_array[0]['amount'];
            }
//            if($recurring_start_date=='' || $recurring_start_date=='0000-00-00'){
                $recurring_start_date = $payment_array[0]['date'];
                $billing_options_no_of_payments = count($payment_array);
//            }
        }
        if(($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE') && $billing_options=='PF'){
            $payment_amount = 0;
            $recurring_start_date = '';
        }
        if(empty($membership_start_date)){
            $membership_start_date = $payment_start_date;
        }
        if ($membership_structure == 'SE' && $billing_options == 'PP' && !empty($payment_array) && $initial_payment_include_membership_fee == 'N' && $reg_current_date!=$payment_start_date) {
            $first_payment = $payment_array[0]['amount'];
            if(isset($payment_array[1]) && isset($payment_array[1]['date'])){
                $recurring_start_date = $payment_array[1]['date'];
            }
        }
        
        if(($first_payment<5 && $first_payment>0) && $payment_amount>0 && $reg_current_date==$payment_start_date && $delay_recurring_payment_start_flg=='N' && $initial_payment_include_membership_fee=='N' && $prorate_first_payment_flg=='Y' && $membership_structure=='OE' && ($payment_frequency=='M' || $payment_frequency=='B')){
            if ($payment_frequency == 'B') {
                if (date('j', strtotime($recurring_start_date)) < 16) {
                    $next_pay_date = date('Y-m-16', strtotime($recurring_start_date));
                } else {
                    $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($recurring_start_date)));
                }
            } elseif ($payment_frequency == 'M') {
                $next_pay_date = date('Y-m-d', strtotime('next month', strtotime($recurring_start_date)));
            }
            $recurring_start_date = $next_pay_date;
        }
        
        if($initial_payment_include_membership_fee=='N' && $reg_current_date==$payment_start_date){
            $first_payment=0;
        }
        
        if ($payment_method == 'CC') {
            $p_f = $this->getProcessingFee($company_id, $upgrade_status);
            $process_fee_per = $p_f['PP'];
            $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);
            if ($initial_payment <= 0) {
                $initial_payment_processing_fee = 0;
            } else {
                $initial_payment_processing_fee = $this->calculateProcessingFee($initial_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
            }
            if ($first_payment <= 0) {
                $first_payment_processing_fee = 0;
            } else {
                $first_payment_processing_fee = $this->calculateProcessingFee($first_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
            }
            if ($payment_amount <= 0) {
                $recurring_payment_processing_fee = 0;
            } else {
                $recurring_payment_processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
            }
            if ($delay_recurring_payment_amount <= 0) {
                $delay_recurring_payment_processing_fee = 0;
            } else {
                $delay_recurring_payment_processing_fee = $this->calculateProcessingFee($delay_recurring_payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
            }
        } else {
            $process_fee_per = $process_fee_val = $initial_payment_processing_fee = $first_payment_processing_fee = $recurring_payment_processing_fee = $delay_recurring_payment_processing_fee = 0;
        }


        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency` FROM `wp_account` WHERE `company_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                if($user_state=='deleted'){
                    $msg = "Wepay user registered was deleted.";
                    $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
                    $this->response($this->json($log),200);
                }
                
                if($acc_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log),200);
                }elseif($acc_state=='disabled'){
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log),200);
                }
                
                if(!empty($access_token) && !empty($account_id)){
                    if (!function_exists('getallheaders')){
                        if(!function_exists ('apache_request_headers')){
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        }else{
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    }else{
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        
                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                    
                    if($payment_frequency=='N'){
                        if($initial_payment!=0 || $first_payment!=0){
                            $payment_type = 'O';
                        }else{
                            $payment_type = 'F';
                        }
                    }else{
                        if($payment_amount!=0){
                            $payment_type = 'R';
                        }else{
                            if($initial_payment!=0 || $first_payment!=0){
                                $payment_type = 'O';
                            }else{
                                $payment_type = 'F';
                            }
                        }
                    }
                    
                    $initial_payment_success_flag = 'Y';
                    $initial_payment_date = $current_date;
                    $next_payment_date = $recurring_start_date;
                    $preceding_payment_date = $current_date;
                    $preceding_payment_status = 'S';
                    
                    $paid_amount = $w_paid_amount = $payment_done = 0;
                    $checkout_id = $checkout_state = '';
                    $cc_name = $cc_month = $cc_year = '';
                    $cc_status = $cc_state;
                    $paid_amount_string = "`paid_amount`,";
                    $paid_amount_string1 = "'". mysqli_real_escape_string($this->db, $initial_payment)."',";
                    $temp_payment_status = 'M';
                    
                    if($initial_payment>=5 && $payment_method == 'CC'){
                        $paid_amount_string = '';
                        $paid_amount_string1 = '';
                        $temp_payment_status = 'S';
                        $checkout_id_flag = 1;
                        $rand = mt_rand(1,99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout?for=membership";
                        $unique_id = $company_id."_".$membership_id."_".$membership_option_id."_".$ip."_".$rand."_".time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id."_".$membership_id."_".$membership_option_id."_".$date;
                        $fee_payer = "payee_from_app";
                                
                        if($processing_fee_type==2){
                            $w_paid_amount = $initial_payment + $initial_payment_processing_fee;
                            $paid_amount = $initial_payment;
                        }elseif($processing_fee_type==1){
                            $w_paid_amount = $initial_payment;
                            $paid_amount = $initial_payment - $initial_payment_processing_fee;
                        }
                        $time = time();
                        $json2 = array("account_id"=>"$account_id","short_description"=>"$membership_desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                            "fee"=>array("app_fee"=>$initial_payment_processing_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                            "email_message"=>array("to_payee"=>"Payment has been added for $membership_category_title","to_payer"=>"Payment has been made for $membership_category_title"),
                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                            "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code","country"=>"$country")))),
                            "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                "properties"=>array("itemized_receipt"=>array(array("description"=>"$membership_category_title", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                        $postData2 = json_encode($json2);
                        $sns_msg2['call'] = "Checkout";
                        $sns_msg2['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg2);
                        $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $gross = $res2['gross'];
                            $payment_done = 1;
//                            $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $membership_category_title, $w_paid_amount, $currency);
                        }else{
                            if($response2['msg']['error_code']==1008){
                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg2);
                                $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                                if($response2['status']=="Success"){
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $gross = $res2['gross'];
                                    $payment_done = 1;
//                                    $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $membership_category_title, $w_paid_amount, $currency);
                                }else{
                                    if($response2['msg']['error_code']==1008){
                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error),400);
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),400);
                                    }
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error),400);
                            }
                        }                        
                    }else{
                        $checkout_id_flag = 0;
                        if(!empty(trim($cc_id))){
                            $paid_amount_string = '';
                            $paid_amount_string1 = '';
                             $temp_payment_status = 'S';
                            $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                            $postData3 = json_encode($json3);
                            $sns_msg2['call'] = "Credit Card";
                            $sns_msg2['type'] = "Credit Card details";
                            $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg2);
                            $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                            if($response3['status']=="Success"){
                                $res3 = $response3['msg'];
                                if($res3['state']=='new'){
                                    $json2 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id,"account_id"=>$account_id);
                                    $postData2 = json_encode($json2);
                                    $sns_msg2['call'] = "Credit Card";
                                    $sns_msg2['type'] = "Credit Card Authorization";
                                    $response2 = $this->wp->accessWepayApi("credit_card/authorize",'POST',$postData2,$token,$user_agent,$sns_msg2);
                                    $this->wp->log_info("wepay_credit_card_authorize: ".$response2['status']);
                                    if($response2['status']=="Success"){
                                        $res2 = $response2['msg'];
                                        $cc_name = $res2['credit_card_name'];
                                        $cc_new_state = $res2['state'];
                                        if($cc_state!=$cc_new_state){
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res2['expiration_month'];
                                        $cc_year = $res2['expiration_year'];
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),400);
                                    }
                                }elseif($res3['state']=='authorized'){
                                    $cc_name = $res3['credit_card_name'];
                                    $cc_new_state = $res3['state'];
                                    $cc_month = $res3['expiration_month'];
                                    $cc_year = $res3['expiration_year'];
                                }else{
                                    if($res3['state']=='expired'){
                                        $msg = "Given credit card is expired.";
                                    }elseif($res3['state']=='deleted'){
                                        $msg = "Given credit card was deleted.";
                                    }else{
                                        $msg = "Given credit card details is invalid.";
                                    }
                                    $error = array("status" => "Failed", "msg" => $msg);
                                    $this->response($this->json($error),400);
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                $this->response($this->json($error),400);
                            }
                        }
                        
                        $initial_payment_date = '';
                        $initial_payment_success_flag='N';
                        $preceding_payment_date = '';
                        $preceding_payment_status = 'N';
                    }
                    
                    if(!empty(trim($cc_id))){
                        $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                        $postData3 = json_encode($json3);
                        $sns_msg2['call'] = "Credit Card";
                        $sns_msg2['type'] = "Credit Card details";
                        $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg2);
                        $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                        if($response3['status']=="Success"){
                            $res3 = $response3['msg'];
                            $cc_name = $res3['credit_card_name'];
                            $cc_new_state = $res3['state'];
                            if($cc_state!=$cc_new_state){
                                $cc_status = $cc_new_state;
                            }
                            $cc_month = $res3['expiration_month'];
                            $cc_year = $res3['expiration_year'];
                        }else{
                            for($k=0;$k<2;$k++){
                                if($response3['msg']['error_code'] == 503){
                                    sleep(2);
                                    $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg2);
                                    $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                                    if($response3['status']=="Success"){
                                        $res3 = $response3['msg'];
                                        $cc_name = $res3['credit_card_name'];
                                        $cc_new_state = $res3['state'];
                                        if($cc_state!=$cc_new_state){
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res3['expiration_month'];
                                        $cc_year = $res3['expiration_year'];
                                        break;
                                    }
                                    if($k==1 ){
                                        if($checkout_id_flag == 1){
                                            $type = $response3['msg']['error'];
                                            $description =  $response3['msg']['error_description']." Buyer Name:".$buyer_name." Buyer Email:".$buyer_email." Checkout Id:".$checkout_id." Participant Name:".$reg_col1." ".$reg_col2;       
                                            $insert_alertquery = sprintf("INSERT INTO `wepay_critical_alert`(`company_id`, `type`, `description`) VALUES ('%s','%s','%s')",mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $type),mysqli_real_escape_string($this->db, $description));
                                            $result_alert = mysqli_query($this->db, $insert_alertquery);
                                            if(!$result_alert){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_alertquery");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 500);
                                            }   
                                            $type1 = $response3['msg']['error']." Regarding Credit Card";
                                            $description1 = "Fetching Credit Card details failed"."\n"." Buyer Name:".$buyer_name."\n"." Buyer Email:".$buyer_email."\n"." Checkout Id:".$checkout_id."\n"." Participant Name:".$reg_col1." ".$reg_col2;     
                                            $this->sendSnsforfailedcheckout($type1,$description1);
                                        }
                                        $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                        $this->response($this->json($error),400);
                                    }
                                }else{
                                    $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                    $this->response($this->json($error),400);
                                }
                            }
                        }
                    }

                    $rank_details = $this->getInitialRank($membership_id);
                    $rank_status=$rank_details['rank_status'];
                    $rank_id=$rank_details['rank_id'];
                    $insert_reg = sprintf("INSERT INTO `membership_registration`($paid_amount_string`optional_discount_flag`,`check_number`,`payment_method`,`company_id`, `membership_id`, `membership_option_id`, `student_id`,`participant_id`, `membership_status`, `membership_category_title`, `membership_title`,
                                `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `processing_fee_type`, `processing_fee`, `payment_type`, `membership_structure`, `prorate_first_payment_flg`, 
                                `delay_recurring_payment_start_flg`, `delay_recurring_payment_start_date`, `delay_recurring_payment_amount`, `custom_recurring_frequency_period_type`, `custom_recurring_frequency_period_val`, `initial_payment_include_membership_fee`, 
                                `billing_options`, `no_of_classes`, `billing_options_expiration_date`, `billing_options_deposit_amount`, `billing_options_no_of_payments`, 
                                `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `signup_fee`, `signup_fee_disc`, 
                                `membership_fee`, `membership_fee_disc`, `initial_payment`, `initial_payment_date`, `initial_payment_success_flag`, `first_payment`, `payment_amount`, `payment_frequency`, `membership_start_date`, `payment_start_date`, `recurring_start_date`, `next_payment_date`, 
                                `preceding_payment_date`, `preceding_payment_status`, `no_of_payments`, `membership_registration_column_1`, `membership_registration_column_2`, `membership_registration_column_3`, 
                                `membership_registration_column_4`, `membership_registration_column_5`, `membership_registration_column_6`, `membership_registration_column_7`, `membership_registration_column_8`, 
                                `membership_registration_column_9`, `membership_registration_column_10`, `participant_street`, `participant_city`, `participant_state`, `participant_zip`, `participant_country`, `rank_status`,`rank_id`,`membership_reg_type_user`,`membership_reg_version_user`,`specific_start_date`,`specific_end_date`,`exclude_billing_flag`) VALUES($paid_amount_string1'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',
                                '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $optional_discount_flag),mysqli_real_escape_string($this->db, $check_number),mysqli_real_escape_string($this->db, $payment_method),mysqli_real_escape_string($this->db, $company_id),
                            mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db,$participant_id), mysqli_real_escape_string($this->db, 'R'), mysqli_real_escape_string($this->db, $membership_category_title), mysqli_real_escape_string($this->db, $membership_title),
                            mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code),
                            mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $membership_structure), mysqli_real_escape_string($this->db, $prorate_first_payment_flg),
                            mysqli_real_escape_string($this->db, $delay_recurring_payment_start_flg), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount), mysqli_real_escape_string($this->db, $custom_recurring_frequency_period_type), mysqli_real_escape_string($this->db, $custom_recurring_frequency_period_val), mysqli_real_escape_string($this->db, $initial_payment_include_membership_fee),
                            mysqli_real_escape_string($this->db, $billing_options), mysqli_real_escape_string($this->db, $no_of_classes), mysqli_real_escape_string($this->db, $billing_options_expiration_date), mysqli_real_escape_string($this->db, $billing_options_deposit_amount), mysqli_real_escape_string($this->db, $billing_options_no_of_payments),
                            mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year),
                            mysqli_real_escape_string($this->db, $signup_fee), mysqli_real_escape_string($this->db, $signup_fee_disc), mysqli_real_escape_string($this->db, $membership_fee), mysqli_real_escape_string($this->db, $membership_fee_disc),
                            mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_date), mysqli_real_escape_string($this->db, $initial_payment_success_flag), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $payment_frequency),
                            mysqli_real_escape_string($this->db, $membership_start_date), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, $recurring_start_date), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, $preceding_payment_date), mysqli_real_escape_string($this->db, $preceding_payment_status), mysqli_real_escape_string($this->db, $payment_done),
                            mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4), mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8),
                            mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $participant_street), mysqli_real_escape_string($this->db, $participant_city), mysqli_real_escape_string($this->db, $participant_state), mysqli_real_escape_string($this->db, $participant_zip), mysqli_real_escape_string($this->db, $participant_country),
                            mysqli_real_escape_string($this->db, $rank_status), mysqli_real_escape_string($this->db, $rank_id), mysqli_real_escape_string($this->db,$reg_type_user), mysqli_real_escape_string($this->db,$reg_version_user), mysqli_real_escape_string($this->db,$program_start_date), mysqli_real_escape_string($this->db,$program_end_date), mysqli_real_escape_string($this->db,$exclude_from_billing_flag));
                    $result_reg = mysqli_query($this->db, $insert_reg);
                    
                    if(!$result_reg){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_reg");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $membership_reg_id = mysqli_insert_id($this->db);
                        
                        if($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') {
                         if($initial_payment>0){ 
                             if($payment_method == "CA" || $payment_method == "CH"){
                                 $temp_payment_status = 'M';
                             }
                            $insert_success_payment_nc = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), 
                                    mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'));
                            $result_paid_insert = mysqli_query($this->db, $insert_success_payment_nc);
                            if (!$result_paid_insert) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_success_payment_nc");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                         }
                            if ($billing_options == 'PP' && !empty($payment_array)) {
                                for ($i = 0; $i < count($payment_array); $i++) {
                                    if($membership_structure == 'SE'){
                                    $prorate_flag_se = $payment_array[$i]['prorate_flag'];
                                    }else{
                                    $prorate_flag_se = 'F';    
                                    }
                                    $paytime_type_check =  $payment_array[$i]['type'];
                                    if($membership_structure == 'SE' && $initial_payment_include_membership_fee == 'N' && $reg_current_date!=$payment_start_date && $i == 0){
                                        $paytime_type_check = 'F';
                                    }
                                    if($payment_method == "CA" || $payment_method == "CH"){
                                       $payment_array[$i]['processing_fee']=0;
                                    }
                                    $insert_mem_payment_NC = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`, `prorate_flag`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                                            mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_array[$i]['amount']), 
                                            mysqli_real_escape_string($this->db, $payment_array[$i]['processing_fee']), mysqli_real_escape_string($this->db, $payment_array[$i]['date']), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $paytime_type_check), mysqli_real_escape_string($this->db, $prorate_flag_se));
                                    $result_pay_insert = mysqli_query($this->db, $insert_mem_payment_NC);
                                    if (!$result_pay_insert) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment_NC");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 500);
                                    }
                                }
                            }elseif($membership_structure == 'SE' && $billing_options == 'PF' && $first_payment>0){
                                $insert_first_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), 
                                    mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'));
                                $result_first_insert = mysqli_query($this->db, $insert_first_payment);
                                if (!$result_first_insert) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_first_payment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }elseif ($membership_structure=='OE'){
                            if ($payment_method == "CA" || $payment_method == "CH") {
                                $temp_payment_status_cashorcheck = 'M';
                            } else {
                                $temp_payment_status_cashorcheck = 'S';
                            }
                            if($initial_payment>=5){
                                if($payment_type=='R'){
                                    if($delay_recurring_payment_start_flg=='Y'){
                                        if($delay_recurring_payment_amount>=5){
                                            
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment),
                                                    mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'), 
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount),
                                                    mysqli_real_escape_string($this->db, $delay_recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'),
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount),
                                                    mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }elseif($delay_recurring_payment_amount<5 && $delay_recurring_payment_amount>0){
                                            $added_payment = $payment_amount+$delay_recurring_payment_amount;
                                            if($added_payment<=0){
                                                $added_processing_fee = 0;
                                            }else{
                                                $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                            }
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment),
                                                    mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'),
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment),
                                                    mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }else{
                                            if(!empty($first_payment) && $first_payment>=5 && $initial_payment_include_membership_fee=='N'){
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'), ('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                        mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment),
                                                        mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'),
                                                        mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment),
                                                        mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'),
                                                        mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount),
                                                        mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            }elseif(!empty($first_payment) && $first_payment<5 && $first_payment>0 && $initial_payment_include_membership_fee=='N'){
                                                $added_payment = $payment_amount+$first_payment;
                                                if($added_payment<=0){
                                                    $added_processing_fee = 0;
                                                }else{
                                                    $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                                }
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                        mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment),
                                                        mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'),
                                                        mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment),
                                                        mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            }else{
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                        mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment),
                                                        mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'),
                                                        mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount),
                                                        mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            }
                                        }
                                    }else{
                                        if(!empty($first_payment) && $first_payment>=5 && $initial_payment_include_membership_fee=='N'){
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'), ('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment),
                                                    mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'),
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment),
                                                    mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'),
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount),
                                                    mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }elseif(!empty($first_payment) && $first_payment<5 && $first_payment>0 && $initial_payment_include_membership_fee=='N'){
                                            $added_payment = $payment_amount+$first_payment;
                                            if($added_payment<=0){
                                                $added_processing_fee = 0;
                                            }else{
                                                $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                            }
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment),
                                                    mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'),
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment),
                                                    mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }else{
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment),
                                                    mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'),
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount),
                                                    mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }
                                    }
                                }else{
                                    if(!empty($first_payment) && $first_payment>=5 && $initial_payment_include_membership_fee=='N'){
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment),
                                                mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'),
                                                mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment),
                                                mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'));
                                    }else{
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment),
                                                mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'));
                                    }
                                }
                                $result_pay = mysqli_query($this->db, $insert_mem_payment);

                                if(!$result_pay){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }else{
                                if($payment_type=='R'){
                                    if($delay_recurring_payment_start_flg=='Y'){
                                        if($delay_recurring_payment_amount>=5){
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount),
                                                    mysqli_real_escape_string($this->db, $delay_recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'),
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount),
                                                    mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }elseif($delay_recurring_payment_amount<5 && $delay_recurring_payment_amount>0){
                                            $added_payment = $payment_amount+$delay_recurring_payment_amount;
                                            if($added_payment<=0){
                                                $added_processing_fee = 0;
                                            }else{
                                                $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                            }

                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`,`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment),
                                                    mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }else{
                                            if(!empty($first_payment) && $first_payment>=5 && $initial_payment_include_membership_fee=='N'){
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`,`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                        mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment),
                                                        mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'),
                                                        mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount),
                                                        mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            }elseif(!empty($first_payment) && $first_payment<5 && $first_payment>0 && $initial_payment_include_membership_fee=='N'){
                                                $added_payment = $payment_amount+$first_payment;
                                                if($added_payment<=0){
                                                    $added_processing_fee = 0;
                                                }else{
                                                    $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                                }
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                        mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment),
                                                        mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            }else{
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                        mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount),
                                                        mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            }
                                        }
                                    }else{
                                        if(!empty($first_payment) && $first_payment>=5 && $initial_payment_include_membership_fee=='N'){
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment),
                                                    mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'),
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount),
                                                    mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }elseif(!empty($first_payment) && $first_payment<5 && $first_payment>0 && $initial_payment_include_membership_fee=='N'){
                                            $added_payment = $payment_amount+$first_payment;
                                            if($added_payment<=0){
                                                $added_processing_fee = 0;
                                            }else{
                                                $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                            }
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment),
                                                    mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }else{
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount),
                                                    mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }
                                    }
                                    $result_pay = mysqli_query($this->db, $insert_mem_payment);

                                    if(!$result_pay){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 500);
                                    }
                                }else{
                                    if(!empty($first_payment) && $first_payment>=5 && $initial_payment_include_membership_fee=='N'){
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment),
                                                mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'));

                                        $result_pay = mysqli_query($this->db, $insert_mem_payment);

                                        if(!$result_pay){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        }
                                    }
                                }
                            }

                            if($payment_amount>0 && $payment_type=='R'){
                                $next_pay_date = $payment_start_date = date("Y-m-d", strtotime($next_payment_date));
                                $payment_end_date = date("Y-m-d", strtotime("+93 days"));
                                $recurring_payment_date_array = [];
                                $recurring_payment_date_array[] = $payment_start_date;
                                $iteration_end = 1;
                                $payment_date = $next_pay_date;
                                for ($i = 0; $i < $iteration_end; $i++) {
                                    if ($payment_frequency == 'W') {
                                        $next_pay_date = date('Y-m-d', strtotime('+1 weeks', strtotime($payment_date)));
                                    } elseif ($payment_frequency == 'B') {
                                            if (date('j', strtotime($payment_date)) < 16) {
                                                $next_pay_date = date('Y-m-16', strtotime($payment_date));
                                            } else {
                                                $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($payment_date)));
                                            }
                                    } elseif ($payment_frequency == 'M') {
                                        $next_pay_date = date('Y-m-d', strtotime('next month', strtotime($payment_date)));
                                    } elseif ($payment_frequency == 'A') {
                                        $next_pay_date = date('Y-m-d', strtotime('+1 year', strtotime($payment_date)));
                                    } elseif ($payment_frequency == 'C') {
                                        if ($custom_recurring_frequency_period_type == 'CW') {
                                            $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($payment_date)));
                                        } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                            $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($payment_date)));
                                        }
                                    }
                                    $payment_date = $next_pay_date;
                                    if ($next_pay_date <= $payment_end_date || count($recurring_payment_date_array)==0) {
                                        $iteration_end++;
                                        $recurring_payment_date_array[] = $next_pay_date;
                                    }
                                }

                                for ($j = 1; $j < count($recurring_payment_date_array); $j++) {
                                    $insert_mem_pay_OE_R = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                    $result_mem_pay_OE_r = mysqli_query($this->db, $insert_mem_pay_OE_R);
                                    if (!$result_mem_pay_OE_r) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay_OE_R");
                                        log_info($this->json($error_log));
                                    }
                                }
                            }
                        }
                        
                         if ($payment_method !== 'CC') {

                            $update_count = sprintf("UPDATE `membership` SET `category_net_sales`=`category_net_sales`+%s,`members_active`=`members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s'",
                                   mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
                            $result_count = mysqli_query($this->db, $update_count);

                            if (!$result_count) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }

                            $update_count2 = sprintf("UPDATE `membership_options` SET `options_net_sales` = `options_net_sales`+%s,`options_members_active`=`options_members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", 
                                    mysqli_real_escape_string($this->db, $initial_payment),mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
                            $result_count2 = mysqli_query($this->db, $update_count2);

                            if (!$result_count2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                            $this->updateMembershipDimensionsNetSales($company_id, $membership_id, '');
                        } else {
                            $update_count = sprintf("UPDATE `membership` SET `members_active`=`members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s'", 
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
                            $result_count = mysqli_query($this->db, $update_count);

                            if (!$result_count) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }

                            $update_count2 = sprintf("UPDATE `membership_options` SET `options_members_active`=`options_members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", 
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
                            $result_count2 = mysqli_query($this->db, $update_count2);

                            if (!$result_count2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }
                    }

                    $activity_text = 'Registration date.';
                    if(!empty($signup_discount_code)){
                        $activity_text .= ' Sign-up fee discount code used "'.$signup_discount_code.'".';
                        if(!empty($membership_discount_code)){
                            $activity_text .= ' Membership fee discount code used "'.$membership_discount_code.'".';
                        }elseif(!empty ($membership_fee_disc)){
                            $activity_text .= ' Membership fee discount value used "'.$membership_fee_disc.'".';
                        }
                    }else{
                        if(!empty($signup_fee_disc)){
                            $activity_text .= ' Sign-up fee discount value used "'.$signup_fee_disc.'".';
                            if(!empty($membership_discount_code)){
                                $activity_text .= ' Membership fee discount code used "'.$membership_discount_code.'".';
                            }elseif(!empty ($membership_fee_disc)){
                                $activity_text .= ' Membership fee discount value used "'.$membership_fee_disc.'".';
                            }
                        }else{
                            if(!empty($membership_discount_code)){
                                $activity_text .= ' Membership fee discount code used "'.$membership_discount_code.'".';
                            }elseif(!empty ($membership_fee_disc)){
                                $activity_text .= ' Membership fee discount value used "'.$membership_fee_disc.'".';
                            }
                        }
                    }
                    
                    $curr_date = gmdate("Y-m-d H:i:s");
                    $sql1 = sprintf("INSERT INTO `membership_history`(`company_id`, `membership_registration_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $student_id), 'registration', 
                            mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                    $result1 = mysqli_query($this->db, $sql1);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    
                    if ($exclude_from_billing_flag == 'Y' && count($exclude_days_array) > 0 && $membership_structure == 'SE') {
                        usort($exclude_days_array, function($a, $b) {
                            return $a['billing_exclude_startdate'] > $b['billing_exclude_startdate'];
                        });
                        for ($x = 0; $x < count($exclude_days_array); $x++) {
                            $activity_text = "Exclude days  : from " . date('M d Y', strtotime($exclude_days_array[$x]['billing_exclude_startdate'])) . " to " . date('M d Y', strtotime($exclude_days_array[$x]['billing_exclude_enddate']));
                            $curr_date = gmdate("Y-m-d H:i:s");
                            $insert_history_exclude = sprintf("INSERT INTO `membership_history`(`company_id`, `membership_registration_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Exclude', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                            $result_history_exculde = mysqli_query($this->db, $insert_history_exclude);
                            if (!$result_history_exculde) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history_exclude");
                                log_info($this->json($error_log));
                            }
                        }
                    }
                    
                    if($payment_type=='F'){
                        $email_msg = "Registration successful.";
                    }else{
                        $email_msg = "Registration successful. Confirmation of purchase has been sent to your email.";
                    }
                    
                    $msg = array("status" => "Success", "msg" => $email_msg);
                    $this->responseWithWepay($this->json($msg),200);
                    $this->sendEmailForMembershipPaymentSuccess($membership_reg_id,$company_id);
                   
                    $this->updateMembershipDimensionsMembers($company_id, $membership_id,'A',$membership_option_id);
                    $curr_date_time = date("Y-m-d ");
                    $sql_insert_date = sprintf("UPDATE membership_registration SET start_email_date ='%s' WHERE company_id='%s'  AND membership_registration_id='%s' ", mysqli_real_escape_string($this->db, $curr_date_time), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
                    $sql_insert_date_result = mysqli_query($this->db, $sql_insert_date);
                    if (!$sql_insert_date_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_insert_date");
                        log_info($this->json($error_log));
                    } else {
                        $message = array('status' => "Success", "msg" => "Email start date updated successfully.");
                        log_info($this->json($message));
                    }
                }else{
                    $error = array("status" => "Failed", "msg" => "Access token error. Cannot be accept payments anymore.");
                    $this->response($this->json($error),200);
                }
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error),200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    //dashboard check for particular membership category & option for current month & year
    public function addMembershipDimensions($company_id, $membership_id, $mem_option_id, $category_title, $option_title, $period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($mem_option_id)) {
            $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $curr_dt));
                    $result = mysqli_query($this->db, $sql1);
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        $this->response($this->json($error), 500);
                    } else {
                        $num_of_rows = mysqli_num_rows($result);
                        if ($num_of_rows == 0) {
                    $selectsql = sprintf("SELECT `active_members` from membership_dimensions where company_id='%s' and `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s' ",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $pre_dt));
                    $resultselectsql = mysqli_query($this->db, $selectsql);
                    if (!$resultselectsql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        $this->response($this->json($error), 500);
                    } else {
                        $num_of_rows = mysqli_num_rows($resultselectsql);
                        if ($num_of_rows == 0) {
                            $sqlselect1=sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and `membership_option_id`='%s' and  `company_id`= '%s'",  mysqli_real_escape_string($this->db,$membership_id),mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db,$company_id));
                            $resultsqlselect1=mysqli_query($this->db, $sqlselect1);
                            $value2 = mysqli_fetch_object($resultsqlselect1);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id),
                                    mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), mysqli_real_escape_string($this->db,$value2->active));
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                $this->response($this->json($error), 500);
                            }
                        } else {
                            $value = mysqli_fetch_object($resultselectsql);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id),
                                    mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), $value->active_members);
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                $this->response($this->json($error), 500);
                            }
                        }
                    }
                }
            }
        }

        $sql2 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            $this->response($this->json($error), 500);
        } else{
            $num_of_rows1 = mysqli_num_rows($result2);
            if ($num_of_rows1 == 0) {
                $selectsql2 = sprintf("SELECT `active_members` FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $pre_dt));
                $resultselectsql2 = mysqli_query($this->db, $selectsql2);
                if (!$resultselectsql2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql2");
                    $this->response($this->json($error), 500);
                } else {
                    $num_of_rows = mysqli_num_rows($resultselectsql2);
                    if ($num_of_rows == 0) {
                        $sqlselect2 = sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and  `company_id`= '%s'", mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$company_id));
                        $resultsqlselect2 = mysqli_query($this->db, $sqlselect2);
                        $value3 = mysqli_fetch_object($resultsqlselect2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $value3->active));
                $res2 = mysqli_query($this->db, $insertquery1);
                if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    $this->response($this->json($error), 500);
                }
                    } else {
                        $value1 = mysqli_fetch_object($resultselectsql2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), $value1->active_members);
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            $this->response($this->json($error), 500);
                        }
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    private function calculateProcessingFee($amount, $fee_type, $process_fee_per, $process_fee_val) {
        $p1_fee = ($amount * ($process_fee_per / 100)) + $process_fee_val;
        $p2_fee = ($amount + $p1_fee) * ($process_fee_per / 100) + $process_fee_val;
        if ($fee_type === 2 || $fee_type === '2') {
            $p_fee = $p2_fee;
        } else {
            $p_fee = $p1_fee;
        }
        return $p_fee;
    }
    
    public function getInitialRank($membership_id) {
        $query = sprintf("SELECT `rank_name` as rank_status, `membership_rank_id`as rank_id FROM `membership_ranks` WHERE `membership_id`='%s' AND `rank_deleted_flg`!='D' ORDER by `rank_order` ASC LIMIT 0,1", mysqli_real_escape_string($this->db, $membership_id));
        $result = mysqli_query($this->db, $query);
        $rank_details = [];
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);                       
            if ($num_rows > 0) {
                $rank_details = mysqli_fetch_assoc($result);
            } else {                            
                $rank_details['rank_status']='';
                $rank_details['rank_id']='';
            }
            return $rank_details;
        }
    }
    
    private function sendEmailForMembershipPaymentSuccess($membership_reg_id,$company_id){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $created_date =" DATE(CONVERT_TZ(mp.created_dt,$tzadd_add,'$new_timezone'))";

        $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
        $payment_details = $prorate_flag_check = [];
        $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
        $recurring_start_date = $payment_start_date = $specific_end_date = $payment_frequency = $custom_recurring_frequency_period_type = $custom_recurring_frequency_period_val = $msg1 = $msg2 = $msg3 = '';
        $membership_structure = $billing_options = $billing_options_no_of_payments = ''; $total_mem_fee = $registration_fee = 0;
        
        $query = sprintf("SELECT mr.`company_id`, IF(mp.`credit_method`='CA','Cash',IF(mp.`credit_method`='CH',concat('Check',' #',mp.`check_number`),if(mp.`payment_from`='S',mp.`stripe_card_name`,mp.`cc_name`))) credit_method,mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, if(mp.`payment_from`='S',mp.`stripe_card_name`,mp.`cc_name`) as cc_name, `waiver_policies`, mr.credit_card_name, 
            mr.`membership_start_date`, mr.`payment_start_date`,mr.`specific_end_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, mr.`custom_recurring_frequency_period_val`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
            `buyer_postal_code`, mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, DATE($created_date) created_dt, mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`,mp.`prorate_flag`
            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id` LEFT JOIN `student` s ON s.`company_id`= mr.`company_id` AND s.`student_id`= mr.`student_id`
            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N')
            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            exit();
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){                
                while($row = mysqli_fetch_assoc($result)){
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
//                    $company_id = $row['company_id'];
//                    $membership_id = $row['membership_id'];
//                    $membership_option_id = $row['membership_option_id'];
                    $payment_type = $row['payment_type'];
                    $membership_category_title = $row['membership_category_title'];
                    $membership_title = $row['membership_title'];
                    $membership_structure = $row['membership_structure'];
                    $participant_name = $row['participant_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $signup_fee = $row['signup_fee'];
                    $signup_fee_disc = $row['signup_fee_disc'];
                    $mem_fee = $row['membership_fee'];
                    $mem_fee_disc = $row['membership_fee_disc'];
                    $initial_payment = $row['initial_payment'];
                    $inital_processing_fee = $row['initial_processing_fee'];
                    $membership_start_date = $row['membership_start_date'];
                    $payment_start_date = $row['payment_start_date'];
                    $recurring_start_date = $row['recurring_start_date'];
                    $next_payment_date = $row['next_payment_date'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $custom_recurring_frequency_period_val = $row['custom_recurring_frequency_period_val'];
                    $recurring_amount = $row['recurring_amount'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
                    $first_payment = $row['first_payment'];
                    $billing_options = $row['billing_options'];
                    $no_of_classes = $row['no_of_classes'];
                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $credit_card_name = $row['credit_card_name'];
                    $prorate_flag_check[] = $row['prorate_flag'];
                    $specific_end_date = $row['specific_end_date'];
                    $student_cc_email_list = $row['student_cc_email'];
                    $temp=[];
                    if(!empty($row['membership_payment_id'])){
                        if($recurring_amount == $row['payment_amount']){
                            $recurring_processing_fee = $row['processing_fee'];
                        }
                        if ($row['processing_fee_type'] == 2) {
                            if ($membership_structure != 'SE') {
                                $temp['amount'] = $row['payment_amount'] + $row['processing_fee'];
                            } else if ($membership_structure == 'SE') {
                            $temp['amount'] = $row['payment_amount'];
                        }
                        } else{
                            $temp['amount'] = $row['payment_amount'];
                        }
                       $payment_date_temp = $temp['date'] = $row['payment_date'];
                        $temp['created_dt'] = $row['created_dt'];
                        $temp['processing_fee'] = $row['processing_fee'];
                        $temp['payment_status'] = $row['payment_status'];
                        $temp['paytime_type'] = $row['paytime_type'];
                        $temp['cc_name'] = $row['cc_name'];
                        $temp['credit_method'] = $row['credit_method'];
                        $paid_amt += $temp['amount'];
                        $payment_details[] = $temp;
                    }
                }
                if($membership_structure=='SE'){
                    $registration_fee = number_format(($signup_fee-$signup_fee_disc),2);
                    for($i = 0;$i<count($payment_details);$i++){
                        $total_mem_fee += $payment_details[$i]['amount'];  
                    }
                        $total_mem_fee -= $registration_fee;
                }
                if(($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE') && $billing_options=='PP'){
                    if($billing_options_no_of_payments>1){
                        $already_recurring_records_count = 0;
                        $next_date = '';
                        if($membership_structure!=='SE'){
                            for($i=0;$i<count($payment_details);$i++){
                                if($payment_details[$i]['paytime_type']=='R'){
                                    $already_recurring_records_count++;
                                    $next_date = $payment_details[$i]['date'];
                                }
                            }
                        }
                        if($billing_options_no_of_payments>$already_recurring_records_count && $next_date !== ''){
                            $count = $billing_options_no_of_payments - $already_recurring_records_count;
                            $future_date = $next_date;
                            for($j=0;$j<$count;$j++){
                                if($payment_frequency=='W'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+1 weeks', strtotime($future_date)));
                                }elseif($payment_frequency=='B'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+14 days', strtotime($future_date)));
                                }elseif($payment_frequency=='M'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('next month', strtotime($future_date)));
                                }
                                
                                $temp=[];
                                if($processing_fee_type==2){
                                    if ($membership_structure != 'SE') {
                                    $temp['amount'] = $recurring_amount+$recurring_processing_fee;
                                    } elseif ($membership_structure == 'SE') {
                                        $temp['amount'] = $recurring_amount;
                                    }
                                }else{
                                    $temp['amount'] = $recurring_amount;
                                }
                                $temp['date'] = $next_payment_date2;
                                $temp['processing_fee'] = $recurring_processing_fee;
                                $temp['payment_status'] = 'N';
                                $temp['paytime_type'] = 'R';
                                $temp['cc_name'] = $credit_card_name;
                                $payment_details[] = $temp;
                                $future_date = $next_payment_date2;
                            }
                        }
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                log_info($this->json($error_log));
                exit();
            }
        }
        
        $subject = $membership_category_title." Membership Order Confirmation";
        
        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
        $message .= "<b>Membership Details</b><br><br>";
        $message .= "Membership Start Date: ".date("M dS, Y", strtotime($membership_start_date))."<br>";
        if($membership_structure == 'SE'){
        $message .= "Membership End Date: ".date("M dS, Y", strtotime($specific_end_date))."<br>";
        $message .= "Registration Fee: $wp_currency_symbol".(number_format(($registration_fee),2))."<br>";
        $message .= "Total Membership Fee: $wp_currency_symbol".(number_format(($total_mem_fee),2))."<br>";
        }
        $message .= "Membership: $membership_category_title, $membership_title<br>Participant: ".$participant_name."<br>";
        if($membership_structure=='NC' && $billing_options=='PP' && $billing_options_expiration_date!='0000-00-00'){
            $message .= "$no_of_classes Class Package Expires: ".date("D, M d, Y", strtotime($billing_options_expiration_date))."<br>";
        }
        $message .= "<br>";
            
        $prorated = "";
        if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
            $prorated = "(Pro-rated)";
        }
        
        if($paid_amt>0){
//            if($initial_payment>0){
            if($membership_structure !== 'SE'){
                $message .= "<br><b>Registration Details</b><br><br>";
                $message .= "Sign-up Fee: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
            }
            else if($membership_structure == 'SE' && $billing_options=='PF'){
                $message .= "<br><b>Membership payment plan</b><br><br>";
//                $message .= "payment: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
//                $message .= " (plus $wp_currency_symbol".$inital_processing_fee." administrative fees)"."<br>";
            }   //check ravi
             if($membership_structure !== 'SE'){
                if($processing_fee_type==2){
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment+$inital_processing_fee),2))." (includes $wp_currency_symbol".$inital_processing_fee." administrative fees) <br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }else{
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))." <br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }
        }
        }
        if(count($payment_details)>0){
            if($payment_frequency!='N' && $payment_type=='R'){
                $freq_str = "";
                if($membership_structure=='OE' || $membership_structure==1){
                    if($payment_frequency=='B'){
                        $freq_str = "Semi-Monthly Recurring: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "Monthly Recurring: ";
                    }elseif($payment_frequency=='W'){
                        $freq_str = "Weekly Recurring: ";
                    }elseif($payment_frequency=='A'){
                        $freq_str = "Annual Recurring: ";
                    }elseif($payment_frequency=='C'){
                        if($custom_recurring_frequency_period_type=='CW'){
                            $freq_str = "Weekly Recurring: ";
                        }elseif($custom_recurring_frequency_period_type=='CM'){
                            $freq_str = "Monthly Recurring: ";
                        }
                    }
                }elseif($billing_options=='PP'){
                    if($payment_frequency=='W'){
                        $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
                    }elseif($payment_frequency=='B'){
                        $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
                    }
                }
                $message .= "<br><br><b>Membership Payment Plan</b><br><br>";
                $recur_pf_str = "";
                if ($membership_structure !== 'SE') {
                if($processing_fee_type==2){
                    $recur_pf_str = " (plus $wp_currency_symbol".$recurring_processing_fee." administrative fees)";
                }
                if($freq_str!=""){
                    if(($membership_structure=='OE' || $membership_structure==1) && $payment_frequency=='C'){ // Ticket 1113
                        if($custom_recurring_frequency_period_type=='CW'){
                            $message .= "$wp_currency_symbol".$recurring_amount.' Recurring every '.$custom_recurring_frequency_period_val.' week(s)'.$recur_pf_str."<br>";
                        }elseif($custom_recurring_frequency_period_type=='CM'){
                            $message .= "$wp_currency_symbol".$recurring_amount.' Recurring every '.$custom_recurring_frequency_period_val.' month(s)'.$recur_pf_str."<br>";
                        }
                    }else{
                        $message .= "$freq_str $wp_currency_symbol".$recurring_amount.$recur_pf_str."<br>";
                    }
                }
//                $message .= "Next Payment Date: ".date("M dS, Y", strtotime($next_payment_date))."<br>";
                for ($init_success_payments = 0; $init_success_payments < count($payment_details); $init_success_payments++) {
                    if ($payment_details[$init_success_payments]['paytime_type'] == 'R' && $payment_details[$init_success_payments]['payment_status'] == 'N') {
                        $date_temp = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                        $message .= "Next Payment Date: " . date("M dS, Y", strtotime($date_temp)) . "<br>";
                        break;
                    }
                }
            }
            }
            if ($membership_structure !== 'SE') {
            $message.="<br><br><b>Payment Details</b><br><br>";
            }
            for($init_success_payments=0;$init_success_payments<count($payment_details);$init_success_payments++){
                $prorated_1 = $prorated_2 = $prorated_3 ="";
                $temp_se_date_check = date("d,m,Y,w",strtotime($payment_details[$init_success_payments]['date']));
                $se_check_prorate = explode(",",$temp_se_date_check);
                $pf_str = "";
                if ($processing_fee_type == 2){
                    if($membership_structure !== 'SE') {
                        $pf_str = " (includes $wp_currency_symbol".$payment_details[$init_success_payments]['processing_fee']." administrative fees)";
                    } else {
                        $pf_str = " (plus $wp_currency_symbol" . $payment_details[$init_success_payments]['processing_fee'] . " administrative fees)";
                    }
                }
                if($membership_structure !== 'SE'){
               if($processing_fee_type==2){
                        $amount_temp=$payment_details[$init_success_payments]['amount']-$payment_details[$init_success_payments]['processing_fee'];
                    }else{
                        $amount_temp=$payment_details[$init_success_payments]['amount']; 
                    }
                    if($payment_details[$init_success_payments]['paytime_type']=='I'){
                        if(($mem_fee - $mem_fee_disc)+ ($signup_fee - $signup_fee_disc) == ($amount_temp) || $signup_fee - $signup_fee_disc == ($amount_temp)){
                            $prorated_1 = "";
                        }else{
                        $prorated_1 = "(Pro-rated)";
                        }
                    }elseif($payment_details[$init_success_payments]['paytime_type']=='F'){
                           if($mem_fee - $mem_fee_disc == ($amount_temp)){
                            $prorated_2 = "";
                        }else{
                        $prorated_2 = "(Pro-rated)";
                        } 
                    }
                }else if($membership_structure == 'SE' && $billing_options == 'PP'){
                    if($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type']=='I'){
                         $prorated_1 = "(Pro-rated)";
                    }else if($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type']=='F') {
                         $prorated_2 = "(Pro-rated)";
                    }else if($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type']=='R'){
                         $prorated_3 = "(Pro-rated)";
                    }
                }
                    if($membership_structure !== 'SE'){    
                        if(!empty($prorated_1)){
                            $prorated_2="";
                        }
                    }
                if($initial_payment>0 && $payment_details[$init_success_payments]['paytime_type']=='I'){
                    $down_pay = $payment_details[$init_success_payments]['amount'].$pf_str." paid on ".date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']))." (".$payment_details[$init_success_payments]['credit_method'].") " .$prorated_1;
                    $msg1 .= "Payment: $wp_currency_symbol".$down_pay."<br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='F'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    if($payment_details[$init_success_payments]['payment_status']=='S'){
                        $paid_str = "paid on";
                        $pay_type= "(".$payment_details[$init_success_payments]['credit_method'].")";
                    }else{
                        $paid_str = "due";
                        if($membership_structure === 'SE'){
                         $pay_type= "(".$payment_details[$init_success_payments]['credit_method'].")";
                        }else{
                        $pay_type= "";
                        }
                    }
                    if($membership_structure !== 'SE' || ($membership_structure == 'SE' && $billing_options == 'PP')){
                    $msg2 .= "First Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_2." <br>";
                    }else{
                    $msg2 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_2." <br>"; 
                }
                }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='S'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "paid on";
                    $pay_type= "(".$payment_details[$init_success_payments]['credit_method'].")";
                        if($membership_structure !== 'SE'){
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                        }else{
                        $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_3." <br>";
                }
                    }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='N' && ($membership_structure=='NC' || $membership_structure=='C' || $membership_structure=='SE' ) && $billing_options=='PP'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "due";
                    $pay_type= "(".$payment_details[$init_success_payments]['credit_method'].")";
                        if($membership_structure !== 'SE'){
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                        }else{
                        $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." ".$prorated_3." <br>";  
                }
            }
            }
            $message .= $msg1.$msg2.$msg3;
            $message .= "<br><br><br>";
        }
        
        $waiver_present = 0;
        $file = '';
        if(!empty(trim($waiver_policies))){
            $waiver_present = 1;
            $file_name_initialize = 10;
            for($z=0; $z<$file_name_initialize; $z++){
                $dt = time();
                $file = __DIR__."/../../uploads/".$dt."_waiver_policies.html";
                if(file_exists($file)){
                    sleep(1);
                    $file_name_initialize++;
                }else{
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }
        
        $sendEmail_status = $this->sendEmailForMembership($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list,$student_cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    protected function sendEmailForMembership($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list,$student_cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
//            if(filter_var($from, FILTER_VALIDATE_EMAIL)){
//                $mail->setFrom($from, $cmp_name);
//                $mail->AddCC($from);
//            }else{
//                $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if(!empty(trim($student_cc_email_list))){
                $student_cc_addresses = explode(',', $student_cc_email_list);
                for($init_1=0;$init_1<count($student_cc_addresses);$init_1++){
                    if(filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($student_cc_addresses[$init_1]);
                    }
                }
            }
            
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }    
    
    public function updateMembershipDimensionsMembers($company_id, $membership_id, $membership_status, $membership_option_id) {
        $flag=0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        $date_str = "%Y-%m";
        $every_month_dt = date("Y-m-d");

        $selectquery = sprintf("SELECT * FROM `membership_registration` WHERE DATE_FORMAT(`hold_date`,'%s')=DATE_FORMAT(`resume_date`,'%s') and  `company_id`='%s' and `membership_id`='%s' and `membership_option_id`='%s'", $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
        $res = mysqli_query($this->db, $selectquery);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows == 1) {
                $flag = 1;
            }
        }
        if ($membership_status == 'R') {
            if ($flag == 1) {
                $mem_string = "`active_members`=`active_members`+1, `reg_hold`=`reg_hold`-1";
            } elseif ($flag == 0) {
                $mem_string = "`active_members`=`active_members`+1";
            }
        } elseif ($membership_status == 'P') {
            $mem_string = "`active_members`=`active_members`-1, `reg_hold`=`reg_hold`+1";
        } elseif ($membership_status == 'C') {
            $mem_string = "`active_members`=`active_members`-1, `reg_cancelled`=`reg_cancelled`+1";
        } elseif ($membership_status == 'A') {
            $mem_string = "`active_members`=`active_members`+1,`reg_new`=`reg_new`+1";
        }

        if (!empty(trim($mem_string))) {
            $query1 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`=0", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), $every_month_dt, $date_str);
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
        }
        if (!empty(trim($mem_string))) {
            $query2 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), $every_month_dt, $date_str, mysqli_real_escape_string($this->db, $membership_option_id));
            $result2 = mysqli_query($this->db, $query2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    private function getRealIpAddr(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet        
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    private function sendSnsforfailedcheckout($sub,$msg){
    
            $topicarn = 'arn:aws:sns:us-east-1:241334827363:mystudio';
         
                    $subject = $sub;
    //                $subject = $sns_sub . " -transaction failed";
                    $message = $msg;

                    $credentials = new Aws\Credentials\Credentials('AKIAIGUWYNJGAHDPJGIA', 'w4oKbP0UhJZr1W0hGuULhkdEoxEHY2YxEq/6uycT');               
                    $sns = SnsClient::factory([
                        'credentials' => $credentials,
                        'region' => 'us-east-1',
                        'version' => 'latest'
                    ]);
    //                log_info("object cretaed");
                    $result1 = $sns->publish(array(
                        'TopicArn' => $topicarn,
                        // Message is required
                        'Message' => $message,
                        'Subject' => $subject,
                        //'MessageStructure' => 'string',
                        'MessageAttributes' => array(
                            // Associative array of custom 'String' key names
                            'String' => array(
                                // DataType is required
        //            'DataType' => '',
                                'DataType' => 'String',
                                'StringValue' => '200',
                            ),
                        // ... repeated
                        ),
                    ));
               
    }
    
    public function getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $payment_processor, $studio_name) {
        $select_student = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$company_id));
        $result_select_student = mysqli_query($this->db, $select_student);
        if (!$result_select_student) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student);
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result_select_student) > 0) {
                $r = mysqli_fetch_assoc($result_select_student);
                $student_id = $r['student_id'];
                if(is_null($r['stripe_customer_id']) || empty(trim($r['stripe_customer_id']))){
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = $r['stripe_customer_id'];
                    }
                }else{
                    $stripe_customer_id = $r['stripe_customer_id'];
                }
            } else {
                $insert_student = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_lastname`, `student_email`) VALUES('%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), 
                        mysqli_real_escape_string($this->db,$buyer_first_name), mysqli_real_escape_string($this->db,$buyer_last_name), mysqli_real_escape_string($this->db,$buyer_email));
                $result_insert_student = mysqli_query($this->db, $insert_student);
                if (!$result_insert_student) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student);
                    $this->response($this->json($error), 200);
                } else {
                    $student_id = mysqli_insert_id($this->db);
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = '';
                    }
                }
            }
            return array("student_id" => $student_id, "stripe_customer_id" => $stripe_customer_id);
        }
    }
    
    public function getParticipantId($company_id, $student_id, $first_name, $last_name, $dob) {

        $select_participant2 = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name), mysqli_real_escape_string($this->db, $dob));
        $result_select_participant2 = mysqli_query($this->db, $select_participant2);
        if (!$result_select_participant2) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_participant2);
            $this->response($this->json($error), 500);
        } else {
            if (mysqli_num_rows($result_select_participant2) > 0) {
                $r = mysqli_fetch_assoc($result_select_participant2);
                $participant_id = $r['participant_id'];
                $deleted_flag = $r['deleted_flag'];
                if($deleted_flag=='D'){
                    $update_participant = sprintf("UPDATE `participant` SET `deleted_flag`='N' WHERE `participant_id`='%s'", mysqli_real_escape_string($this->db, $participant_id));
                    $result_update_participant = mysqli_query($this->db, $update_participant);
                    if (!$result_update_participant) {
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_participant);
                        $this->response($this->json($error), 500);
                    }
                }
            } else {
                $insert_participant2 = sprintf("INSERT INTO `participant`(`company_id`, `student_id`, `participant_first_name`, `participant_last_name`, `date_of_birth`) VALUES('%s', '%s', '%s', '%s', '%s')",
                        $company_id, $student_id, mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name), $dob);
                $result_insert_participant2 = mysqli_query($this->db, $insert_participant2);
                if (!$result_insert_participant2) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_participant2);
                    $this->response($this->json($error), 500);
                } else {
                    $participant_id = mysqli_insert_id($this->db);
                }
            }
            return $participant_id;
        }
    }
    
    public function getStudioSubscriptionStatus($company_id){
        $query = sprintf("SELECT email_id,upgrade_status,subscription_status,wepay_status,stripe_status,stripe_subscription,studio_expiry_level,expiry_dt FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_email = $row['email_id'];
                $upgrade_status = $row['upgrade_status'];
                $subscription_status = $row['subscription_status'];
                $wepay_status = $row['wepay_status'];
                $stripe_status = $row['stripe_status'];
                $stripe_subscription = $row['stripe_subscription'];
                $studio_expiry_level = $row['studio_expiry_level'];
                $expiry_dt = $row['expiry_dt'];
                if (!empty(trim($upgrade_status)) && $subscription_status=='N' && ($studio_expiry_level=='L2' || $upgrade_status=='F')) {
                    $res = array('status' => "Failed", "msg" => "Please contact the business operator to activate this link.", "studio_email"=>$studio_email, "upgrade_status"=>$upgrade_status, "studio_expiry_level" => $studio_expiry_level);
                    $this->response($this->json($res), 200);
                }else{
                    $res = array('status' => "Success", "msg" => "Studio subscription status success", "upgrade_status" => $upgrade_status);
                    return $res;
                }                
            } else {
                $res = array('status' => "Failed", "msg" => "Studio Details not found.");
                $this->response($this->json($res), 200);
            }
        }
    }
    
    public function sendExpiryEmailDetailsToStudio($company_id){
        $query = sprintf("SELECT c.`email_id`, c.`company_name`, c.`referral_email_list` FROM `company` c WHERE c.`company_id`='%s'", $company_id);
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $company_name = $reply_to = "";
                $row = mysqli_fetch_assoc($result);
                $owner_email_id = $row['email_id'];
                $referral_email_list = $row['referral_email_list'];
                
                $subject = "A member has requested that you activate your MyStudio account.";
                $body_msg = '<div style="max-width:520px;">';
                $body_msg .= "Members are trying to access your app and/or URL registration links.<br><br>However, your account is currently inactive, please click here to activate your account.<br>";
                $body_msg .= '<center><a href="'.$this->server_url.'/mystudioapp.php?pg=login"><button type="button" style="width: 140px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 500;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 40px;font-size: 15px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">Activate</button></a></center>';
                $body_msg .= "</div>";
                $sendEmail_status = $this->sendEmail($owner_email_id, $referral_email_list, $subject, $body_msg, $company_name, $reply_to, true);
                if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
                    $refer_check = 1;
                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $owner_email_id . "   " . $sendEmail_status['mail_status']);
                    log_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                } else {
                    $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $owner_email_id);
                    log_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                }                
            }else{
                $error = array('status' => "Failed", "msg" => "Studio details doesn't exist.");
                $this->response($this->json($error), 401);
            }
        }
    }
    
    protected function sendEmail($to, $cc_email_list, $subject, $message, $cmp_name, $reply_to, $isHtml) {

        if (empty(trim($cmp_name))) {
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try {
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if (filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)) {
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML($isHtml);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    $mail->AddCC($cc_addresses[$init]);
                }
            }
            if($isHtml){                
                $mail->CharSet = 'UTF-8';
                $mail->Encoding = 'base64';
            }
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        } catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    protected function checkpossettings($company_id, $payment_method, $pos_email, $token) {
        $this->verifyStudioPOSToken($company_id, $pos_email, $token);
        $sql = sprintf("SELECT membership_status, membership_payment_method from studio_pos_settings where company_id = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                if($row['membership_status'] === "D"){
                    $error = array('status' => "Failed", "msg" => "Membership POS is disabled");
                    $this->response($this->json($error), 200);
                }
                $payment_binary = $row['membership_payment_method'];
                if($payment_method === "CA"){
                    if($payment_binary[1] === "0"){
                        $error = array('status' => "Failed", "msg" => "Payment method cash is disabled");
                        $this->response($this->json($error), 200);
                    }
                }
                if($payment_method === "CH"){
                    if($payment_binary[2] === "0"){
                        $error = array('status' => "Failed", "msg" => "Payment method check is disabled");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
    }
    
    public function checkStudioAndEmail($company_id, $email){
        $query = sprintf("SELECT * FROM `user` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Studio details.", "session_status" => "Expired");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function verifyStudioPOSToken($company_id, $email, $token) {
        $this->checkStudioAndEmail($company_id, $email);
        $query = sprintf("SELECT * FROM `pos_token` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $old_token = $row['token'];
                $token_id = $row['pos_token_id'];
                if($old_token==$token){
                    $query2 = sprintf("UPDATE `pos_token` SET `no_of_access`=`no_of_access`+1 WHERE `pos_token_id`='%s'", mysqli_real_escape_string($this->db, $token_id));
                    $result2 = mysqli_query($this->db, $query2);                
                    if(!$result2){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $msg = array("status"=>"Success", "msg"=>"Session is active.");
                    return $msg;
                }else{
                    $msg = array("status"=>"Failed", "msg"=>"Session expired.", "session_status" => "Expired");
                    $this->response($this->json($msg), 200);
                }
            }else{
                $msg = array("status"=>"Failed", "msg"=>"Session expired.", "session_status" => "Expired");
                $this->response($this->json($msg), 200);
            }
        }
    }
    
    public function updateMembershipDimensionsNetSales($company_id, $membership_id, $date) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $mp_last_upd_date=" DATE(CONVERT_TZ(mp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $last_upd_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";
        if(empty($date)){
            $curr_dt = "($currdate_add)";
        }else{
            $curr_dt = "'$date'";
        }
//        $sql = sprintf("SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, sum(if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
//                  mp.payment_date FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
//                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
//                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  MONTH( $mp_last_upd_date) = MONTH($curr_dt)
//                  AND YEAR($mp_last_upd_date) = YEAR($curr_dt) AND mp.payment_status='S' AND mp.checkout_status='released' GROUP BY mr.`membership_id`, mr.`membership_option_id`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
        $date_inc1= "DATE_FORMAT($curr_dt,'%Y-%m')";
        $date_inc2= "DATE_FORMAT($mp_last_upd_date,'%Y-%m')";
        $date_inc3= "DATE_FORMAT($last_upd_date,'%Y-%m')";
        
        $sql = sprintf("SELECT company_id, membership_id, membership_option_id, mp_ud, sum(amount) amount, payment_date FROM (

                  SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount) amount, 
                  mp.payment_date, mp.membership_payment_id FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND (mp.payment_status='S' AND mp.checkout_status='released' || mp.payment_status IN ('MF') || mp.payment_status='M' AND mp.`credit_method`!='MC')
                  AND %s = %s 
                  
                  UNION
                  
                  SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, concat('-',if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
                  mp.payment_date, mp.membership_payment_id FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  (mp.`payment_status`='R' 
                  AND IF(mr.registration_from='S', mp.payment_intent_id NOT IN (SELECT payment_intent_id FROM membership_payment WHERE payment_status='FR' AND %s = %s),
                  mp.checkout_id NOT IN (SELECT checkout_id FROM membership_payment WHERE payment_status='FR' AND %s = %s)) || mp.`payment_status`='MR')
                  AND %s = %s
                  )t1
                  GROUP BY t1.`membership_id`, t1.`membership_option_id`"

                 , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id),
                 $date_inc2, $date_inc1, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id),
                 $date_inc3, $date_inc1, $date_inc3, $date_inc1, $date_inc2, $date_inc1);
        
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $reg_id = 1;
        } else {
            $category = $membership_id_arr = $membership_options_id_arr = [];
            $number_rows = mysqli_num_rows($result);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $temp = $temp2 = [];
                    $temp['category_id'] = $temp2['category_id'] = $membership_id = $row['membership_id'];
                    $temp2['option_id'] = $membership_option_id = $row['membership_option_id'];
                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
                    $temp2['amount'] = $amount = $row['amount'];
                    $temp2['mp_ud'] = $row['mp_ud'];

                    if (!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)) {
                        $membership_id_arr[] = $membership_id;
                        $category[] = $temp;
                    }
                    $option[] = $temp2;
                }

                for ($i = 0; $i < count($option); $i++) {
                    $date_init = 0;
                   
                    $date_str = "%Y-%m";
                    $curr_dt = date("Y-m-d");
                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
//                     
                    $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')", mysqli_real_escape_string($this->db, $company_id), $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);
                    $res1 = mysqli_query($this->db, $sql1);
                    if (!$res1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
//                           

                        $num_of_rows = mysqli_num_rows($res1);
                        if ($num_of_rows == 1) {

                            $sql2 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`='%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='%s'", $option[$i]['amount'], mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id']);
                            $res2 = mysqli_query($this->db, $sql2);
                            if (!$res2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                log_info($this->json($error_log));
                                $reg_id = 1;
                            } else {
                                if($i==0){
                                    $sales_value = "'%s'";
                                }else{
                                    $sales_value = "`sales_amount`+'%s'";
                                }
                                $sql3 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='0'",mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str,mysqli_real_escape_string($this->db, $membership_id));
                                $res3 = mysqli_query($this->db, $sql3);
                                if (!$res3) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                                    log_info($this->json($error_log));
                                    $reg_id = 1;
                                }
                            }
                        }
                    }
                }
            } else {
                 $sql2 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=0 WHERE `company_id`='%s' AND `period`=%s AND `category_id`='%s'", mysqli_real_escape_string($this->db, $company_id),$date_inc1, $membership_id);
                $res2 = mysqli_query($this->db, $sql2);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
//                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                $this->response($this->json($error), 200);
                }
                $error = array('status' => "Failed", "msg" => "No records.", "query" => "$sql");
                log_info($this->json($error));
//                $this->response($this->json($error), 204);
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for updateMembershipDimensionsNetSales(), check log";
            $this->sendEmailForAdmins($msg);
        }
    }
    
    protected function checkCountryPaymentSupport($company_id, $country, $callback) { // GET COUNTRY PAYMENT SUPPORT
        $row = [];
        $sql = sprintf("SELECT * FROM country_payment_specific WHERE `payment_support` = 'S' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        UNION
                        SELECT * FROM country_payment_specific WHERE `payment_support` = 'W' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        ORDER BY `payment_support` ASC, `company_id` DESC", mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id)
                , mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                if ($callback == 0) {
                    $out = array("status" => "Success", "msg" => $row);
                } else {
                    $row['status'] = 'Success';
                    return $row;
                }
                $this->response($this->json($out), 200);
            } else {
                $out = array("status" => "Failed", "msg" => "Studio country is not yet available for payment support.");
                return $out;
            }
        }
    }
    //    Muthulakshmi
    public function setup_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$studio_name,$buyer_email,$stripe_account_id,$p_type,$cc_type){
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        if ($cc_type == 'N') {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }
        try {
            $setup_intent_create = \Stripe\SetupIntent::create([
                'payment_method' => $payment_method_id,
                'customer' => $stripe_customer_id,
                'confirm' => true,
                'return_url' => $this->server_url."/stripeUpdation.php?type=authenticate&c_id=$company_id&p_type=$p_type&s_id=$student_id",
                'metadata' =>['c_id' => $company_id,'p_type' => $p_type,'s_id' => $student_id ,'s_name' => $studio_name],
                'description' => 'Payment Intent for ' . $buyer_email,
                'payment_method_types' => ['card'],
                'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                'on_behalf_of' => $stripe_account_id,
            ]);
            $body3 = json_encode($setup_intent_create);
            stripe_log_info("Stripe Setup Intent Creation : $body3");
            $return_array['temp_payment_status'] = $setup_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded'){
                $return_array['payment_intent_id'] = $setup_intent_create->id; // return response
                $return_array['setup_intent_client_secret'] = $setup_intent_create->client_secret;
            }else if ($return_array['temp_payment_status'] == 'requires_action'){
                $return_array['payment_intent_id'] = $setup_intent_create->id; // return response
                $return_array['setup_intent_client_secret'] = $setup_intent_create->client_secret;
                $return_array['next_action']['type'] = $setup_intent_create->next_action->type;
                $return_array['next_action']['use_stripe_sdk']['type'] = $setup_intent_create->next_action->use_stripe_sdk->type;
                $return_array['next_action']['use_stripe_sdk']['stripe_js'] = $setup_intent_create->next_action->use_stripe_sdk->stripe_js;         
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }
    public function payment_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$amount_in_dollars,$stripe_currency,$studio_name,$buyer_email,$stripe_account_id,$dest_amount_in_dollars,$p_type,$cc_type){
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");   
         if ($cc_type == 'N') {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }
        try {
            $payment_intent_create = \Stripe\PaymentIntent::create([
                'payment_method' => $payment_method_id,
                'customer' => $stripe_customer_id,
                'amount' => $amount_in_dollars,
                'currency' => $stripe_currency,
                'confirmation_method' => 'automatic',
                'capture_method' => 'automatic',
                'confirm' => true,
                'return_url' => $this->server_url."/stripeUpdation.php?type=authenticate&c_id=$company_id&p_type=$p_type&s_id=$student_id",
                'metadata' =>['c_id' => $company_id,'p_type' => $p_type,'s_id' => $student_id ,'s_name' => $studio_name],
                'setup_future_usage' => 'off_session',
                'description' => 'Payment Intent for ' . $buyer_email,
                'payment_method_types' => ['card'],
                'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                'receipt_email' => $buyer_email,
                'transfer_data' => ['destination' => $stripe_account_id, 'amount' => $dest_amount_in_dollars],
                'on_behalf_of' => $stripe_account_id,
                'statement_descriptor' => substr($studio_name,0,21),
            ]);
            $body3 = json_encode($payment_intent_create);
            stripe_log_info("Stripe Payment Creation : $body3");
            $return_array['temp_payment_status'] = $payment_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['cc_month'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_month;
                $return_array['cc_year'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_year;
                $brand = $payment_intent_create->charges->data[0]->payment_method_details->card->brand;
                $last4 = $payment_intent_create->charges->data[0]->payment_method_details->card->last4;
                $temp = ' xxxxxx';
                $return_array['stripe_card_name'] = $brand . $temp . $last4;
            }else if ($return_array['temp_payment_status'] == 'requires_action'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['payment_intent_client_secret'] = $payment_intent_create->client_secret;
                $return_array['next_action'] = $payment_intent_create->next_action;
//                $return_array['next_action']['type'] = $payment_intent_create->next_action->type;
//                $return_array['next_action']['redirect_to_url']['url'] = $payment_intent_create->next_action->redirect_to_url->url;
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }
    
    public function createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name){
        $stripe_customer_id = '';
        $query = sprintf("SELECT `stripe_customer_id` FROM `student` WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                $stripe_customer_id = $row['stripe_customer_id'];
            }
        }
        if(is_null($stripe_customer_id) || empty(trim($stripe_customer_id))){
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");

            try {
                $stripe_customer_create = \Stripe\Customer::create([
                            "email" => $buyer_email,
                            "description" => "Customer created with $buyer_email for $studio_name($company_id)",
                ]);
                $body = json_encode($stripe_customer_create);
                stripe_log_info("Customer_new : $body");
                $body_arr = json_decode($body, true);
                $stripe_customer_id = $body_arr['id'];

                //update stripe customer id to db - arun 
                $query2 = sprintf("UPDATE `student` SET `stripe_customer_id` = '%s' WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $student_id));
                $result2 = mysqli_query($this->db, $query2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }

            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
        
        return $stripe_customer_id;
    }
    
    public function getCompanyStripeAccount($company_id, $call_back){
        $query = sprintf("SELECT * FROM `stripe_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result) >0){
                $row = mysqli_fetch_assoc($result);
                if($call_back == 1){
                    return $row;
                }else{
                    $msg = array("status" => "Success", "msg" => "Stripe Account details", "stripe_details" => "$row");
                    $this->response($this->json($msg), 200);
                }
            }else{
                if($call_back == 1){
                    return '';
                }else{
                    $msg = array("status" => "Failed", "msg" => "Stripe Account details", "stripe_details" => "");
                    $this->response($this->json($msg), 200);
                }
            }
        }
    }
//    Stripe checkout start
    public function stripeMembershipCreateCheckout($company_id, $membership_id, $membership_option_id, $actual_student_id, $actual_participant_id, $upgrade_status, $membership_category_title, $membership_title, $membership_desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $processing_fee_type, $processing_fee, $membership_fee, $membership_fee_disc, $signup_fee, $signup_fee_disc, $payment_amount, $initial_payment, $first_payment, $payment_frequency, $payment_start_date, $recurring_start_date, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $prorate_first_payment_flg, $delay_recurring_payment_start_flg, $delay_recurring_payment_start_date, $delay_recurring_payment_amount, $custom_recurring_frequency_period_type, $custom_recurring_frequency_period_val, $initial_payment_include_membership_fee, $membership_structure, $billing_options, $no_of_classes, $billing_options_expiration_date, $billing_options_deposit_amount, $billing_options_no_of_payments, $payment_array, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $membership_discount_code, $signup_discount_code, $exclude_days_array, $program_start_date, $program_end_date, $exclude_from_billing_flag, $reg_type_user, $reg_version_user, $reg_current_date, $membership_start_date, $payment_method, $check_number, $optional_discount, $optional_discount_flag, $payment_method_id, $intent_method_type, $cc_type, $stripe_customer_id) {
        $payment_intent_id = '';
        $payment_from = $registration_from = '';
        $stripe_account_id = $stripe_currency = $studio_name = '';
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1   
        if (($payment_method == "CC" && $stripe_account != '' && $stripe_account['charges_enabled'] == 'Y' && ($initial_payment > 0 || $first_payment > 0 || $payment_amount > 0)) || ($payment_method == "CA" || $payment_method == "CH") || $payment_amount == 0) {  // ALLOWS FOR CASH & CHECK, IF CC, CHECK THE CHARGES ENABLED TO BE 'Y'
            $upgrade_status = $sts['upgrade_status'];
            $payment_method = mysqli_real_escape_string($this->db, $payment_method);
            $buyer_name = $buyer_first_name . ' ' . $buyer_last_name;
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");

            $query = sprintf("SELECT c.`company_name`,s.`currency`,s.`account_id` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
            $result = mysqli_query($this->db, $query);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($result);
                if ($num_rows > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $studio_name = $row['company_name'];
                    $stripe_currency = $row['currency'];
                    $stripe_account_id = $row['account_id'];
                }
            }
            if (is_null($actual_student_id) || $actual_student_id == 0) {
                $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Stripe', $studio_name);
                $student_id = $stud_array['student_id'];
                $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
            } else {
                $student_id = $actual_student_id;
                if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                    $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                }
            }
            if (is_null($actual_participant_id) || $actual_participant_id == 0) {
                $participant_id = $this->getParticipantId($company_id, $student_id, $reg_col1, $reg_col2, $reg_col3);
            } else {
                $participant_id = $actual_participant_id;
            }
            $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id, $membership_category_title, $membership_title, '');
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone = $user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
            $current_date = date("Y-m-d");
            if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') && $billing_options == 'PP' && !empty($payment_array)) {
                if ($payment_amount < 5) {
                    $payment_amount = $payment_array[0]['amount'];
                }
//            if($recurring_start_date=='' || $recurring_start_date=='0000-00-00'){
                $recurring_start_date = $payment_array[0]['date'];
                $billing_options_no_of_payments = count($payment_array);
//            }
            }
            if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') && $billing_options == 'PF') {
                $payment_amount = 0;
                $recurring_start_date = '';
            }
            if (empty($membership_start_date)) {
                $membership_start_date = $payment_start_date;
            }
            if ($membership_structure == 'SE' && $billing_options == 'PP' && !empty($payment_array) && $initial_payment_include_membership_fee == 'N' && $reg_current_date != $payment_start_date) {
                $first_payment = $payment_array[0]['amount'];
                if (isset($payment_array[1]) && isset($payment_array[1]['date'])) {
                    $recurring_start_date = $payment_array[1]['date'];
                }
            }

            if (($first_payment < 5 && $first_payment > 0) && $payment_amount > 0 && $reg_current_date == $payment_start_date && $delay_recurring_payment_start_flg == 'N' && $initial_payment_include_membership_fee == 'N' && $prorate_first_payment_flg == 'Y' && $membership_structure == 'OE' && ($payment_frequency == 'M' || $payment_frequency == 'B')) {
                if ($payment_frequency == 'B') {
                    if (date('j', strtotime($recurring_start_date)) < 16) {
                        $next_pay_date = date('Y-m-16', strtotime($recurring_start_date));
                    } else {
                        $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($recurring_start_date)));
                    }
                } elseif ($payment_frequency == 'M') {
                    $next_pay_date = date('Y-m-d', strtotime('next month', strtotime($recurring_start_date)));
                }
                $recurring_start_date = $next_pay_date;
            }

            if ($initial_payment_include_membership_fee == 'N' && $reg_current_date == $payment_start_date) {
                $first_payment = 0;
            }

            if ($payment_method == 'CC') {
                $p_f = $this->getProcessingFee($company_id, $upgrade_status);
                $process_fee_per = $p_f['PP'];
                $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);
                if ($initial_payment <= 0) {
                    $initial_payment_processing_fee = 0;
                } else {
                    $initial_payment_processing_fee = $this->calculateProcessingFee($initial_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                }
                if ($first_payment <= 0) {
                    $first_payment_processing_fee = 0;
                } else {
                    $first_payment_processing_fee = $this->calculateProcessingFee($first_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                }
                if ($payment_amount <= 0) {
                    $recurring_payment_processing_fee = 0;
                } else {
                    $recurring_payment_processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
                }
                if ($delay_recurring_payment_amount <= 0) {
                    $delay_recurring_payment_processing_fee = 0;
                } else {
                    $delay_recurring_payment_processing_fee = $this->calculateProcessingFee($delay_recurring_payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
                }
            } else {
                $process_fee_per = $process_fee_val = $initial_payment_processing_fee = $first_payment_processing_fee = $recurring_payment_processing_fee = $delay_recurring_payment_processing_fee = 0;
            }

            if ($payment_frequency == 'N') {
                if ($initial_payment != 0 || $first_payment != 0) {
                    $payment_type = 'O';
                } else {
                    $payment_type = 'F';
                }
            } else {
                if ($payment_amount != 0) {
                    $payment_type = 'R';
                } else {
                    if ($initial_payment != 0 || $first_payment != 0) {
                        $payment_type = 'O';
                    } else {
                        $payment_type = 'F';
                    }
                }
            }

            $initial_payment_success_flag = 'Y';
            $initial_payment_date = $current_date;
            $next_payment_date = $recurring_start_date;
            $preceding_payment_date = $current_date;
            $preceding_payment_status = 'S';
            
            if($initial_payment == 0 && count($payment_array) > 0) {
                $intent_method_type = "SI";
            }else{
                $intent_method_type = "PI";
            }


            $paid_amount_string = "`paid_amount`,";
            $paid_amount_string1 = "'" . mysqli_real_escape_string($this->db, $initial_payment) . "',";
            $temp_payment_status = 'M';

            $w_paid_amount = $paid_amount = $paid_fee = $amount_in_dollars = $dest_amount = $dest_amount_in_dollars = $w_paid_amount_round = $dest_amount_round = $payment_done = 0;
            $checkout_state = $cc_month = $cc_year = $stripe_card_name = $payment_status = '';
            $next_action_type = $next_action_type2 = $button_url = '';
            $has_3D_secure = 'N';

            if (!empty($stripe_customer_id) && !(is_null($stripe_customer_id)) && !empty($payment_method_id) && $payment_method == 'CC') {
                $this->getStripeKeys();
                \Stripe\Stripe::setApiKey($this->stripe_sk);
                \Stripe\Stripe::setApiVersion("2019-05-16");
                if ($initial_payment >= 5 && $payment_method == 'CC') {
                    $paid_amount = $initial_payment;
                    if ($processing_fee_type == 2) {
                        $w_paid_amount = $initial_payment + $initial_payment_processing_fee;
                    } elseif ($processing_fee_type == 1) {
                        $w_paid_amount = $initial_payment;
                    }
                    $w_paid_amount_round = round($w_paid_amount, 2);
                    $dest_amount = $w_paid_amount - $initial_payment_processing_fee;
                    $dest_amount_round = round($dest_amount, 2);
                    $dest_amount_in_dollars = $dest_amount_round * 100;
                    $amount_in_dollars = $w_paid_amount_round * 100;
                    $checkout_type = 'membership';
                    if ($intent_method_type == "SI") { // If it is setup intent creation
                        $payment_intent_result = $this->setup_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $studio_name, $buyer_email, $stripe_account_id, $checkout_type, $cc_type);
                    } else { //If it is payment intent creation
                        $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $checkout_type, $cc_type);
                    }
                    if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'released';
                    } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'requires_action';
                        $has_3D_secure = 'Y';
                        $next_action_type = $payment_intent_result['next_action']['type'];
                        if($next_action_type != "redirect_to_url"){
                            # Payment not supported
                            $out['status'] = 'Failed';
                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                            $this->response($this->json($out), 200);
                        }
                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                    } else {
                        # Invalid status
                        $out['status'] = 'Failed';
                        $out['msg'] = 'Invalid PaymentIntent status.';
                        $out['error'] = 'Invalid PaymentIntent status';
                        $this->response($this->json($out), 200);
                    }
                    $payment_status = 'S';
                }
            }

            if (!empty($payment_method_id)) {
                try {
                    $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                    $brand = $retrive_payment_method->card->brand;
                    $cc_month = $retrive_payment_method->card->exp_month;
                    $cc_year = $retrive_payment_method->card->exp_year;
                    $last4 = $retrive_payment_method->card->last4;
                    $temp = ' xxxxxx';
                    $stripe_card_name = $brand . $temp . $last4;
                } catch (\Stripe\Error\Card $e) {
                    // Since it's a decline, \Stripe\Error\Card will be caught     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\RateLimit $e) {
                    // Too many requests made to the API too quickly     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\InvalidRequest $e) {
                    // Invalid parameters were supplied to Stripe's API     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Authentication $e) {
                    // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\ApiConnection $e) {
                    // Network communication with Stripe failed     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Base $e) {
                    // Display a very generic error to the user, and maybe send yourself an email     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (Exception $e) {
                    // Something else happened, completely unrelated to Stripe     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                }
                if (isset($err) && !empty($err)) {
                    $error = array('status' => "Failed", "msg" => $err['message']);
                    $this->response($this->json($error), 200);
                }
            }

            $payment_from = 'S';
            $registration_from = 'S';
            $rank_details = $this->getInitialRank($membership_id);
            $rank_status = $rank_details['rank_status'];
            $rank_id = $rank_details['rank_id'];
            $insert_reg = sprintf("INSERT INTO `membership_registration`($paid_amount_string`optional_discount_flag`,`check_number`,`payment_method`,`company_id`, `membership_id`, `membership_option_id`, `student_id`,`participant_id`, `membership_status`, `membership_category_title`, `membership_title`,
                                `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `processing_fee_type`, `processing_fee`, `payment_type`, `membership_structure`, `prorate_first_payment_flg`, 
                                `delay_recurring_payment_start_flg`, `delay_recurring_payment_start_date`, `delay_recurring_payment_amount`, `custom_recurring_frequency_period_type`, `custom_recurring_frequency_period_val`, `initial_payment_include_membership_fee`, 
                                `billing_options`, `no_of_classes`, `billing_options_expiration_date`, `billing_options_deposit_amount`, `billing_options_no_of_payments`, 
                                `credit_card_expiration_month`, `credit_card_expiration_year`, `signup_fee`, `signup_fee_disc`, 
                                `membership_fee`, `membership_fee_disc`, `initial_payment`, `initial_payment_date`, `initial_payment_success_flag`, `first_payment`, `payment_amount`, `payment_frequency`, `membership_start_date`, `payment_start_date`, `recurring_start_date`, `next_payment_date`, 
                                `preceding_payment_date`, `preceding_payment_status`, `no_of_payments`, `membership_registration_column_1`, `membership_registration_column_2`, `membership_registration_column_3`, 
                                `membership_registration_column_4`, `membership_registration_column_5`, `membership_registration_column_6`, `membership_registration_column_7`, `membership_registration_column_8`, 
                                `membership_registration_column_9`, `membership_registration_column_10`, `participant_street`, `participant_city`, `participant_state`, `participant_zip`, `participant_country`, `rank_status`,`rank_id`,`membership_reg_type_user`,`membership_reg_version_user`,`specific_start_date`,`specific_end_date`,`exclude_billing_flag`, `stripe_customer_id`,`registration_from`,`payment_method_id`,`stripe_card_name`) VALUES($paid_amount_string1'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',
                                '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $optional_discount_flag), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id), mysqli_real_escape_string($this->db, 'R'), mysqli_real_escape_string($this->db, $membership_category_title), mysqli_real_escape_string($this->db, $membership_title), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $membership_structure), mysqli_real_escape_string($this->db, $prorate_first_payment_flg), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_flg), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount), mysqli_real_escape_string($this->db, $custom_recurring_frequency_period_type), mysqli_real_escape_string($this->db, $custom_recurring_frequency_period_val), mysqli_real_escape_string($this->db, $initial_payment_include_membership_fee), mysqli_real_escape_string($this->db, $billing_options), mysqli_real_escape_string($this->db, $no_of_classes), mysqli_real_escape_string($this->db, $billing_options_expiration_date), mysqli_real_escape_string($this->db, $billing_options_deposit_amount), mysqli_real_escape_string($this->db, $billing_options_no_of_payments), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $signup_fee), mysqli_real_escape_string($this->db, $signup_fee_disc), mysqli_real_escape_string($this->db, $membership_fee), mysqli_real_escape_string($this->db, $membership_fee_disc), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_date), mysqli_real_escape_string($this->db, $initial_payment_success_flag), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $payment_frequency), mysqli_real_escape_string($this->db, $membership_start_date), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, $recurring_start_date), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, $preceding_payment_date), mysqli_real_escape_string($this->db, $preceding_payment_status), mysqli_real_escape_string($this->db, $payment_done), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4), mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $participant_street), mysqli_real_escape_string($this->db, $participant_city), mysqli_real_escape_string($this->db, $participant_state), mysqli_real_escape_string($this->db, $participant_zip), mysqli_real_escape_string($this->db, $participant_country), mysqli_real_escape_string($this->db, $rank_status), mysqli_real_escape_string($this->db, $rank_id), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $program_start_date), mysqli_real_escape_string($this->db, $program_end_date), mysqli_real_escape_string($this->db, $exclude_from_billing_flag), mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $registration_from), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name));
            $result_reg = mysqli_query($this->db, $insert_reg);

            if (!$result_reg) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_reg");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $membership_reg_id = mysqli_insert_id($this->db);

                if ($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') {
                    if ($initial_payment > 0) {
                        if ($payment_method == "CA" || $payment_method == "CH") {
                            $temp_payment_status = 'M';
                        }
                        $insert_success_payment_nc = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'));
                        $result_paid_insert = mysqli_query($this->db, $insert_success_payment_nc);
                        if (!$result_paid_insert) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_success_payment_nc");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }
                    if ($billing_options == 'PP' && !empty($payment_array)) {
                        for ($i = 0; $i < count($payment_array); $i++) {
                            if ($membership_structure == 'SE') {
                                $prorate_flag_se = $payment_array[$i]['prorate_flag'];
                            } else {
                                $prorate_flag_se = 'F';
                            }
                            $paytime_type_check = $payment_array[$i]['type'];
                            if ($membership_structure == 'SE' && $initial_payment_include_membership_fee == 'N' && $reg_current_date != $payment_start_date && $i == 0) {
                                $paytime_type_check = 'F';
                            }
                            $insert_mem_payment_NC = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`, `prorate_flag`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_array[$i]['amount']), mysqli_real_escape_string($this->db, $payment_array[$i]['processing_fee']), mysqli_real_escape_string($this->db, $payment_array[$i]['date']), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, $paytime_type_check), mysqli_real_escape_string($this->db, $prorate_flag_se));
                            $result_pay_insert = mysqli_query($this->db, $insert_mem_payment_NC);
                            if (!$result_pay_insert) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment_NC");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }
                    } elseif ($membership_structure == 'SE' && $billing_options == 'PF' && $first_payment > 0) {
                        $insert_first_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'));
                        $result_first_insert = mysqli_query($this->db, $insert_first_payment);
                        if (!$result_first_insert) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_first_payment");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }
                } elseif ($membership_structure == 'OE') {
                    if ($payment_method == "CA" || $payment_method == "CH") {
                        $temp_payment_status_cashorcheck = 'M';
                    } else {
                        $temp_payment_status_cashorcheck = 'S';
                    }
                    if ($initial_payment >= 5) {
                        if ($payment_type == 'R') {
                            if ($delay_recurring_payment_start_flg == 'Y') {
                                if ($delay_recurring_payment_amount >= 5) {

                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount), mysqli_real_escape_string($this->db, $delay_recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, 'R'));
                                } elseif ($delay_recurring_payment_amount < 5 && $delay_recurring_payment_amount > 0) {
                                    $added_payment = $payment_amount + $delay_recurring_payment_amount;
                                    if ($added_payment <= 0) {
                                        $added_processing_fee = 0;
                                    } else {
                                        $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                    }
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } else {
                                    if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'), ('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                        $added_payment = $payment_amount + $first_payment;
                                        if ($added_payment <= 0) {
                                            $added_processing_fee = 0;
                                        } else {
                                            $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                        }
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    } else {
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    }
                                }
                            } else {
                                if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'), ('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                    $added_payment = $payment_amount + $first_payment;
                                    if ($added_payment <= 0) {
                                        $added_processing_fee = 0;
                                    } else {
                                        $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                    }
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } else {
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                }
                            }
                        } else {
                            if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'));
                            } else {
                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'));
                            }
                        }
                        $result_pay = mysqli_query($this->db, $insert_mem_payment);

                        if (!$result_pay) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    } else {
                        if ($payment_type == 'R') {
                            if ($delay_recurring_payment_start_flg == 'Y') {
                                if ($delay_recurring_payment_amount >= 5) {
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount), mysqli_real_escape_string($this->db, $delay_recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } elseif ($delay_recurring_payment_amount < 5 && $delay_recurring_payment_amount > 0) {
                                    $added_payment = $payment_amount + $delay_recurring_payment_amount;
                                    if ($added_payment <= 0) {
                                        $added_processing_fee = 0;
                                    } else {
                                        $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                    }

                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`,`company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } else {
                                    if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`,`company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from` `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                        $added_payment = $payment_amount + $first_payment;
                                        if ($added_payment <= 0) {
                                            $added_processing_fee = 0;
                                        } else {
                                            $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                        }
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    } else {
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    }
                                }
                            } else {
                                if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                    $added_payment = $payment_amount + $first_payment;
                                    if ($added_payment <= 0) {
                                        $added_processing_fee = 0;
                                    } else {
                                        $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                    }
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } else {
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                }
                            }
                            $result_pay = mysqli_query($this->db, $insert_mem_payment);

                            if (!$result_pay) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        } else {
                            if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'));

                                $result_pay = mysqli_query($this->db, $insert_mem_payment);

                                if (!$result_pay) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }

                    if ($payment_amount > 0 && $payment_type == 'R') {
                        $next_pay_date = $payment_start_date = date("Y-m-d", strtotime($next_payment_date));
                        $payment_end_date = date("Y-m-d", strtotime("+93 days"));
                        $recurring_payment_date_array = [];
                        $recurring_payment_date_array[] = $payment_start_date;
                        $iteration_end = 1;
                        $payment_date = $next_pay_date;
                        for ($i = 0; $i < $iteration_end; $i++) {
                            if ($payment_frequency == 'W') {
                                $next_pay_date = date('Y-m-d', strtotime('+1 weeks', strtotime($payment_date)));
                            } elseif ($payment_frequency == 'B') {
                                if (date('j', strtotime($payment_date)) < 16) {
                                    $next_pay_date = date('Y-m-16', strtotime($payment_date));
                                } else {
                                    $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($payment_date)));
                                }
                            } elseif ($payment_frequency == 'M') {
                                $next_pay_date = date('Y-m-d', strtotime('next month', strtotime($payment_date)));
                            } elseif ($payment_frequency == 'A') {
                                $next_pay_date = date('Y-m-d', strtotime('+1 year', strtotime($payment_date)));
                            } elseif ($payment_frequency == 'C') {
                                if ($custom_recurring_frequency_period_type == 'CW') {
                                    $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($payment_date)));
                                } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                    $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($payment_date)));
                                }
                            }
                            $payment_date = $next_pay_date;
                            if ($next_pay_date <= $payment_end_date || count($recurring_payment_date_array) == 0) {
                                $iteration_end++;
                                $recurring_payment_date_array[] = $next_pay_date;
                            }
                        }

                        for ($j = 1; $j < count($recurring_payment_date_array); $j++) {
                            $insert_mem_pay_OE_R = sprintf("INSERT INTO `membership_payment`(`credit_method`, `check_number`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                            $result_mem_pay_OE_r = mysqli_query($this->db, $insert_mem_pay_OE_R);
                            if (!$result_mem_pay_OE_r) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay_OE_R");
                                log_info($this->json($error_log));
                            }
                        }
                    }
                }

                if ($payment_method !== 'CC') {

                    $update_count = sprintf("UPDATE `membership` SET `category_net_sales`=`category_net_sales`+%s,`members_active`=`members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s'", mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
                    $result_count = mysqli_query($this->db, $update_count);

                    if (!$result_count) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }

                    $update_count2 = sprintf("UPDATE `membership_options` SET `options_net_sales` = `options_net_sales`+%s,`options_members_active`=`options_members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
                    $result_count2 = mysqli_query($this->db, $update_count2);

                    if (!$result_count2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $this->updateMembershipDimensionsNetSales($company_id, $membership_id, '');
                } else {
                    $update_count = sprintf("UPDATE `membership` SET `members_active`=`members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
                    $result_count = mysqli_query($this->db, $update_count);

                    if (!$result_count) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }

                    $update_count2 = sprintf("UPDATE `membership_options` SET `options_members_active`=`options_members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
                    $result_count2 = mysqli_query($this->db, $update_count2);

                    if (!$result_count2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
            }
            
            //update cc information if release state
            if ($checkout_state == 'released') {
                 $this->updateStripeCCDetailsAfterRelease($payment_intent_id);
            }
            //End update cc information if release state

            $activity_text = 'Registration date.';
            if (!empty($signup_discount_code)) {
                $activity_text .= ' Sign-up fee discount code used "' . $signup_discount_code . '".';
                if (!empty($membership_discount_code)) {
                    $activity_text .= ' Membership fee discount code used "' . $membership_discount_code . '".';
                } elseif (!empty($membership_fee_disc)) {
                    $activity_text .= ' Membership fee discount value used "' . $membership_fee_disc . '".';
                }
            } else {
                if (!empty($signup_fee_disc)) {
                    $activity_text .= ' Sign-up fee discount value used "' . $signup_fee_disc . '".';
                    if (!empty($membership_discount_code)) {
                        $activity_text .= ' Membership fee discount code used "' . $membership_discount_code . '".';
                    } elseif (!empty($membership_fee_disc)) {
                        $activity_text .= ' Membership fee discount value used "' . $membership_fee_disc . '".';
                    }
                } else {
                    if (!empty($membership_discount_code)) {
                        $activity_text .= ' Membership fee discount code used "' . $membership_discount_code . '".';
                    } elseif (!empty($membership_fee_disc)) {
                        $activity_text .= ' Membership fee discount value used "' . $membership_fee_disc . '".';
                    }
                }
            }

            $curr_date = gmdate("Y-m-d H:i:s");
            $sql1 = sprintf("INSERT INTO `membership_history`(`company_id`, `membership_registration_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $student_id), 'registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
            $result1 = mysqli_query($this->db, $sql1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }

            if ($exclude_from_billing_flag == 'Y' && count($exclude_days_array) > 0 && $membership_structure == 'SE') {
                usort($exclude_days_array, function($a, $b) {
                    return $a['billing_exclude_startdate'] > $b['billing_exclude_startdate'];
                });
                for ($x = 0; $x < count($exclude_days_array); $x++) {
                    $activity_text = "Exclude days  : from " . date('M d Y', strtotime($exclude_days_array[$x]['billing_exclude_startdate'])) . " to " . date('M d Y', strtotime($exclude_days_array[$x]['billing_exclude_enddate']));
                    $curr_date = gmdate("Y-m-d H:i:s");
                    $insert_history_exclude = sprintf("INSERT INTO `membership_history`(`company_id`, `membership_registration_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Exclude', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                    $result_history_exculde = mysqli_query($this->db, $insert_history_exclude);
                    if (!$result_history_exculde) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history_exclude");
                        log_info($this->json($error_log));
                    }
                }
            }
            if ($payment_type == 'F') {
                $email_msg = "Registration successful.";
            } else {
                if ($has_3D_secure == 'N') {
                    $email_msg = "Registration successful. Confirmation of purchase has been sent to your email.";
                } elseif ($has_3D_secure == 'Y') {
                    $email_msg = "Membership Checkout was processed and waiting for customer action. Kindly review and authenticate the payment.";
                }
            }
            $msg = array("status" => "Success", "msg" => $email_msg);
            $this->responseWithWepay($this->json($msg), 200);
            if ($has_3D_secure == 'N') {
                $this->sendEmailForMembershipPaymentSuccess($membership_reg_id, $company_id);
            } else {
                $email_msg = 'Hi '.$buyer_first_name.' '.$buyer_last_name.',<br>';
                $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">This is an authentication email for Membership '.$membership_title.' registration. Kindly click the launch button for redirecting to Stripe.</p>';
                $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">Complete a required action for this payment. Once the redirect is completed for this PaymentIntent, the payment will proceed.</p>';
                $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg);
            }

            $this->updateMembershipDimensionsMembers($company_id, $membership_id, 'A', $membership_option_id);
            $curr_date_time = date("Y-m-d ");
            $sql_insert_date = sprintf("UPDATE membership_registration SET start_email_date ='%s' WHERE company_id='%s'  AND membership_registration_id='%s' ", mysqli_real_escape_string($this->db, $curr_date_time), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
            $sql_insert_date_result = mysqli_query($this->db, $sql_insert_date);
            if (!$sql_insert_date_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_insert_date");
                log_info($this->json($error_log));
            } else {
                $message = array('status' => "Success", "msg" => "Email start date updated successfully.");
                log_info($this->json($message));
            }
        } else {
            $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
            $this->response($this->json($error), 200);
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    // 3D secure authentication email
    public function sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg){
        
        // EMAIL CONTENT
        $button_text = 'Launch';
        $subject = 'Stripe authenticate';
        $message = $email_msg.'<br>'.'<center><a href="'.$button_url.'"><button type="button" style="width: 190px;border-radius: 5px;background: #009a61;border:1px solid 009a61;color: #ffffff;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 45px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;text-transform: none;overflow: visible;box-sizing: border-box;cursor: pointer;">'.$button_text.'</button></a></center>';
        $sendEmail_status = $this->sendEmail($buyer_email, '', $subject, $message, $studio_name, '', true);
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
    }

    //End of stripe checkout  
    public function updateStripeCCDetailsAfterRelease($payment_intent_id){
        $checkout_state = $membership_reg_id = $company_id = $membership_id = $payment_id = $processing_fee_type = $payment_type = $membership_option_id = $membership_title = $membership_category_title = '';
        $payment_amount = $fee = 0;
        $payment_status = "";
        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_registration_id`, mr.`membership_category_title`, mr.`membership_title`, mp.`membership_payment_id`, mp.`checkout_id`, mp.`checkout_status`, mp.`payment_amount`, mp.`processing_fee`, mr.`processing_fee_type`, mp.`checkout_status`
                    FROM `membership_registration` mr 
                    LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id`=mp.`membership_registration_id` 
                    WHERE mp.`payment_intent_id` = '%s' ORDER BY mp.`membership_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $membership_reg_id = $row['membership_registration_id'];
                $payment_id = $row['membership_payment_id'];
                $fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_amount = $row['payment_amount'];
                $company_id = $row['company_id'];
                $membership_id = $row['membership_id'];
                $membership_option_id = $row['membership_option_id'];
                $membership_title = $desc = $row['membership_title'];
                $membership_category_title=$row['membership_category_title']; 
                $checkout_state = $row['checkout_status'];
                
                $paid_amount = 0;
                if($processing_fee_type==1){
                    $paid_amount = $payment_amount-$fee;
                }elseif($processing_fee_type==2){
                    $paid_amount = $payment_amount;
                }
                $update_sales_in_reg = sprintf("UPDATE `membership_registration` SET `paid_amount`=`paid_amount`+$paid_amount WHERE `membership_registration_id`='%s'", mysqli_real_escape_string($this->db, $membership_reg_id));
                $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);

                if(!$result_sales_in_reg){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }

                $update_count = sprintf("UPDATE `membership` SET `category_net_sales`=`category_net_sales`+$paid_amount WHERE `company_id`='%s' AND `membership_id`='%s'",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
                $result_count = mysqli_query($this->db, $update_count);

                if(!$result_count){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }

                $update_count2 = sprintf("UPDATE `membership_options` SET `options_net_sales` = `options_net_sales`+$paid_amount WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
                $result_count2 = mysqli_query($this->db, $update_count2);

                if(!$result_count2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } 
                $update_query = sprintf("UPDATE `membership_payment` SET `checkout_status`='%s' $payment_status WHERE `membership_registration_id`='%s' and `membership_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                        mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                $update_result = mysqli_query($this->db, $update_query);
                if(!$update_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }else{
                    $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                    log_info($this->json($error));
                    $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id, $membership_category_title, $membership_title, '');
                    $this->updateMembershipDimensionsNetSales($company_id, $membership_id, '');
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Payment details doesn't exist.");
                log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
}

?>
