<?php

require_once 'MembershipModel.php';

class MembershipApi extends MembershipModel {
    
    public function __construct(){
        parent::__construct();          // Init parent contructor
    }
    
    public function processApi(){
        $func = strtolower(trim(str_replace("/","",$_REQUEST['run'])));
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)){
            $this->_request = json_decode(file_get_contents('php://input'), true);
 	}
        if($this->_request['companyid']==6){
            $error = array('status' => "Failed", "msg" => "No membership category available.");
            $this->response($this->json($error), 404);
        }
        if((int)method_exists($this,$func) > 0){
            $this->$func();
        }else{
            $error = array("status" => "Failed", "code" => 404, "msg" => "Page not found.", "func" => $func);
            $this->response($this->json($error),404); // If the method not exist with in this class, response would be "Page not found".
        }
    }
		
    private function membership(){
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if($this->get_request_method() != "GET"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        $company_id = $category_id = $option_id = $token = $pos_email = '';
        $reg_type_user = 'U';
        $from = 'U';        //U - user, S - Social networks crawler
        
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        if(isset($this->_request['category_id'])){
            $category_id = $this->_request['category_id'];
        }
        if(isset($this->_request['option_id'])){
            $option_id = $this->_request['option_id'];
        }
        if (isset($this->_request['token'])) {
            $token = $this->_request['token'];
        }
        if (isset($this->_request['pos_email'])) {
            $pos_email = $this->_request['pos_email'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        
        if(!empty($company_id)){
            if($reg_type_user!='U'){
                $this->checkpossettings($company_id, '', $pos_email, $token);
//                $this->verifyStudioPOSToken($company_id, $pos_email, $token);
            }
            if(empty($category_id)){
                $this->getAllMembershipDetails($company_id, $reg_type_user);
            }else{
                $this->getMembershipDetails($company_id, $category_id, $option_id, $reg_type_user);
            }
        }else{
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 400); 
        }
    }
    
    private function getwepaystatus(){
        if($this->get_request_method() != "GET"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id='';
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        
        if(!empty($company_id)){
            $this->wepayStatus($company_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function getstripestatus() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }

        if (!empty($company_id)) {
            $this->stripeStatus($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function wepayMembershipCheckout(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $membership_id = $membership_option_id = $membership_category_title = $membership_title = $membership_desc = $recurring_start_date = '';
        $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = $payment_frequency = $membership_start_date = $payment_start_date = $delay_recurring_payment_start_date = $custom_recurring_frequency_period_type = '';
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = $membership_discount_code = $signup_discount_code = '';
        $student_id =$participant_id = $payment_amount = $processing_fee = $membership_fee = $membership_fee_disc = $signup_fee = $signup_fee_disc = $initial_payment = $custom_recurring_frequency_period_val = 0;
        $upgrade_status = 'F';
        $country = 'US';
        $prorate_first_payment_flg = $delay_recurring_payment_start_flg = 'N';
        $membership_payment_array = ['N','W','B','M','A','C'];
        $payment_array = [];
        $no_of_classes = $billing_options_deposit_amount = $billing_options_no_of_payments = $delay_recurring_payment_amount = $first_payment = 0;
        $billing_options = $billing_options_expiration_date = '';
        $membership_structure = 'OE';
        $initial_payment_include_membership_fee = 'Y';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        $reg_type_user='U';
        $reg_version_user = '';
        $exclude_from_billing_flag='N';
        $exclude_days_array=[];
        $program_start_date = $program_end_date = $reg_current_date = '';
        $token = $pos_email = '';
        $optional_discount_flag = $optional_discount = $check_number = '';
        $payment_method = "CC";
        
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        if(isset($this->_request['membership_id'])){
            $membership_id = $this->_request['membership_id'];
        }
        if(isset($this->_request['membership_option_id'])){
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if(isset($this->_request['studentid'])){
            $student_id = $this->_request['studentid'];
        }
        if(isset($this->_request['participant_id'])){
            $participant_id = $this->_request['participant_id'];
        }
        if(isset($this->_request['membership_category_title'])){
            $membership_category_title = $this->_request['membership_category_title'];
        }
        if(isset($this->_request['membership_title'])){
            $membership_title = $this->_request['membership_title'];
        }
        if(isset($this->_request['membership_desc'])){
            $membership_desc = $this->_request['membership_desc'];
        }
        if(isset($this->_request['membership_structure'])){
            $membership_structure = $this->_request['membership_structure'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if(isset($this->_request['email'])){
            $buyer_email = $this->_request['email'];
        }
        if(isset($this->_request['phone'])){
            $buyer_phone = $this->_request['phone'];
        }
        if(isset($this->_request['postal_code'])){
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if(isset($this->_request['country'])){
            $country = $this->_request['country'];
        }
        if(isset($this->_request['cc_id'])){
            $cc_id = $this->_request['cc_id'];
        }
        if(isset($this->_request['cc_state'])){
            $cc_state = $this->_request['cc_state'];
        }
        if(isset($this->_request['payment_amount'])){
            $payment_amount = $this->_request['payment_amount'];
        }
        if(isset($this->_request['processing_fee_type'])){
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if(isset($this->_request['processing_fee'])){
            $processing_fee = $this->_request['processing_fee'];
        }
        if(isset($this->_request['membership_fee'])){
            $membership_fee = $this->_request['membership_fee'];
        }
        if(isset($this->_request['membership_fee_disc'])){
            $membership_fee_disc = $this->_request['membership_fee_disc'];
        }
        if(isset($this->_request['signup_fee'])){
            $signup_fee = $this->_request['signup_fee'];
        }
        if(isset($this->_request['signup_fee_disc'])){
            $signup_fee_disc = $this->_request['signup_fee_disc'];
        }
        if(isset($this->_request['initial_payment'])){      //registration time payment
            $initial_payment = $this->_request['initial_payment'];
        }
        if(isset($this->_request['first_payment'])){      //first payment on membership start date
            $first_payment = $this->_request['first_payment'];
        }
        if(isset($this->_request['payment_frequency'])){
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if(isset($this->_request['membership_start_date'])){
            $membership_start_date = $this->_request['membership_start_date'];
        }
        if(isset($this->_request['payment_start_date'])){
            $payment_start_date = $this->_request['payment_start_date'];
        }
        if(isset($this->_request['recurring_start_date'])){
            $recurring_start_date = $this->_request['recurring_start_date'];
        }
        if(isset($this->_request['reg_col1'])){
            $reg_col1 = $this->_request['reg_col1'];
        }
        if(isset($this->_request['reg_col2'])){
            $reg_col2 = $this->_request['reg_col2'];
        }
        if(isset($this->_request['reg_col3'])){
            $reg_col3 = $this->_request['reg_col3'];
        }
        if(isset($this->_request['reg_col4'])){
            $reg_col4 = $this->_request['reg_col4'];
        }
        if(isset($this->_request['reg_col5'])){
            $reg_col5 = $this->_request['reg_col5'];
        }
        if(isset($this->_request['reg_col6'])){
            $reg_col6 = $this->_request['reg_col6'];
        }
        if(isset($this->_request['reg_col7'])){
            $reg_col7 = $this->_request['reg_col7'];
        }
        if(isset($this->_request['reg_col8'])){
            $reg_col8 = $this->_request['reg_col8'];
        }
        if(isset($this->_request['reg_col9'])){
            $reg_col9 = $this->_request['reg_col9'];
        }
        if(isset($this->_request['reg_col10'])){
            $reg_col10 = $this->_request['reg_col10'];
        }
        if(isset($this->_request['upgrade_status'])){
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if(isset($this->_request['prorate_first_payment_flg']) && !empty(trim($this->_request['prorate_first_payment_flg']))){
            $prorate_first_payment_flg = $this->_request['prorate_first_payment_flg'];
        }
        if(isset($this->_request['delay_recurring_payment_start_flg']) && !empty(trim($this->_request['delay_recurring_payment_start_flg']))){
            $delay_recurring_payment_start_flg = $this->_request['delay_recurring_payment_start_flg'];
        }
        if(isset($this->_request['delay_recurring_payment_start_date'])){
            $delay_recurring_payment_start_date = $this->_request['delay_recurring_payment_start_date'];
        }
        if(isset($this->_request['delay_recurring_payment_amount'])){
            $delay_recurring_payment_amount = $this->_request['delay_recurring_payment_amount'];
        }
        if(isset($this->_request['custom_recurring_frequency_period_type'])){
            $custom_recurring_frequency_period_type = $this->_request['custom_recurring_frequency_period_type'];
        }
        if(isset($this->_request['custom_recurring_frequency_period_val'])){
            $custom_recurring_frequency_period_val = $this->_request['custom_recurring_frequency_period_val'];
        }
        if(isset($this->_request['initial_payment_include_membership_fee'])){
            $initial_payment_include_membership_fee = $this->_request['initial_payment_include_membership_fee'];
        }
        if(isset($this->_request['billing_options'])){
            $billing_options = $this->_request['billing_options'];
        }
        if(isset($this->_request['no_of_classes'])){
            $no_of_classes = $this->_request['no_of_classes'];
        }
        if(isset($this->_request['billing_options_expiration_date'])){
            $billing_options_expiration_date = $this->_request['billing_options_expiration_date'];
        }
        if(isset($this->_request['billing_options_deposit_amount'])){
            $billing_options_deposit_amount = $this->_request['billing_options_deposit_amount'];
        }
        if(isset($this->_request['billing_options_no_of_payments'])){
            $billing_options_no_of_payments = $this->_request['billing_options_no_of_payments'];
        }
        if(isset($this->_request['payment_array'])){
            $payment_array = $this->_request['payment_array'];
        }
        if(isset($this->_request['participant_street'])){
            $participant_street = $this->_request['participant_street'];
        }
        if(isset($this->_request['participant_city'])){
            $participant_city = $this->_request['participant_city'];
        }
        if(isset($this->_request['participant_state'])){
            $participant_state = $this->_request['participant_state'];
        }
        if(isset($this->_request['participant_zip'])){
            $participant_zip = $this->_request['participant_zip'];
        }
        if(isset($this->_request['participant_country'])){
            $participant_country = $this->_request['participant_country'];
        }
        if(isset($this->_request['membership_discount_code'])){
            $membership_discount_code = $this->_request['membership_discount_code'];
        }
        if(isset($this->_request['signup_discount_code'])){
            $signup_discount_code = $this->_request['signup_discount_code'];
        }
        if(isset($this->_request['exclude_days_array'])){
            $exclude_days_array = $this->_request['exclude_days_array'];
        }
        if(isset($this->_request['program_start_date'])){
            $program_start_date = $this->_request['program_start_date'];
        }
        if(isset($this->_request['program_end_date'])){
            $program_end_date = $this->_request['program_end_date'];
        }
        if(isset($this->_request['exclude_from_billing_flag'])){
            $exclude_from_billing_flag = $this->_request['exclude_from_billing_flag'];
        }
        if(isset($this->_request['registration_date'])){
            $reg_current_date = $this->_request['registration_date'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['check_number']) && !empty($this->_request['check_number'])){
            $check_number = $this->_request['check_number'];
        }
        if (isset($this->_request['optional_discount'])) {
            if(!empty($this->_request['optional_discount'])){
            $optional_discount = $this->_request['optional_discount'];
            }
        }
        if (isset($this->_request['optional_discount_flag'])) {
            $optional_discount_flag = $this->_request['optional_discount_flag'];
        }
        if (isset($this->_request['token'])) {
            $token = $this->_request['token'];
        }
        if (isset($this->_request['pos_email'])) {
            $pos_email = $this->_request['pos_email'];
        }
        if($reg_type_user !== 'U'){
            $this->checkpossettings($company_id, '', $pos_email, $token);
        }
        if($reg_type_user === "PP" && (((int)$optional_discount > 0) || $optional_discount_flag === "Y")){
            $error = array('status' => "Failed", "msg" => "Optional discount is not applicable");
            $this->response($this->json($error), 200);
        }
        if($payment_method === "CH" && empty(trim($check_number))){
            $error = array('status' => "Failed", "msg" => "Please provide check number");
            $this->response($this->json($error), 200);
        }
        if(empty($reg_type_user) || ($reg_type_user === 'PP' && !empty(trim($student_id)))){
            $error = array('status' => "Failed", "msg" => "Authentication Error");
            $this->response($this->json($error), 200);
        }
        //Input validations
        if(!empty(trim($payment_frequency)) && !in_array($payment_frequency, $membership_payment_array)){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
//        if (($payment_method == 'CA' || $payment_method == 'CH')) {
//            if ($payment_frequency == 'N') {
//                if ($initial_payment != 0 && $first_payment != 0) {
//                    $error = array('status' => "Failed", "msg" => "Check/Cash Payment Are Not allowed For First Payment.");
//                    $this->response($this->json($error), 400);
//                }
//            } else {
//                if ($payment_amount != 0 && $billing_options == 'PP') {
//                    $error = array('status' => "Failed", "msg" => "Check/Cash Payment Are Not allowed For Reccurring Payments.");
//                    $this->response($this->json($error), 400);
//                } else {
//                    if ($initial_payment != 0 && $first_payment != 0) {
//                        $error = array('status' => "Failed", "msg" => "Check/Cash Payment Are Not allowed For First Payment.");
//                        $this->response($this->json($error), 400);
//                    }
//                }
//            }
//        } else {
            if ($initial_payment != 0 || $first_payment != 0 || $payment_amount != 0) {
                if ($payment_method === "CC" && empty(trim($cc_id))) {
                    $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                    $this->response($this->json($error), 200);
                }
            }
//        }
        if(!empty(trim($payment_frequency)) && $payment_frequency=='C' && (empty(trim($custom_recurring_frequency_period_type)) || empty(trim($custom_recurring_frequency_period_val)))){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if(!empty($membership_structure) && $membership_structure=='OE' && $payment_amount>0 && !empty(trim($delay_recurring_payment_start_flg)) && $delay_recurring_payment_start_flg=='Y' && empty(trim($delay_recurring_payment_start_date))){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }        
        if(!empty($membership_structure) && $membership_structure=='NC' && empty($billing_options)){
            if(!empty($billing_options) && $billing_options=='PP' && empty($payment_array)){
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }
        }
        if(!empty($membership_structure) && $membership_structure=='SE' && ((empty($program_start_date) || $program_start_date=='0000-00-00' || empty($program_end_date) || $program_end_date=='0000-00-00') || (!empty($billing_options) && $billing_options=='PP' && $payment_amount > 0 && empty($payment_array)))){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);        
        }
        
        if(!empty($company_id) && !empty($membership_id) && !empty($membership_option_id) && !empty($membership_category_title) && !empty($membership_title) && !empty($membership_desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)){
            $this->membershipCheckout($company_id, $membership_id, $membership_option_id, $student_id,$participant_id, $upgrade_status, $membership_category_title, $membership_title, $membership_desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state,
                    $processing_fee_type, $processing_fee, $membership_fee, $membership_fee_disc, $signup_fee, $signup_fee_disc, $payment_amount, $initial_payment, $first_payment, $payment_frequency, $payment_start_date, $recurring_start_date, 
                    $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $prorate_first_payment_flg, 
                    $delay_recurring_payment_start_flg, $delay_recurring_payment_start_date, $delay_recurring_payment_amount, $custom_recurring_frequency_period_type, $custom_recurring_frequency_period_val, $initial_payment_include_membership_fee, $membership_structure,
                    $billing_options, $no_of_classes, $billing_options_expiration_date, $billing_options_deposit_amount, $billing_options_no_of_payments, $payment_array,
                    $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $membership_discount_code, $signup_discount_code,$exclude_days_array,$program_start_date,$program_end_date,$exclude_from_billing_flag,$reg_type_user,$reg_version_user,$reg_current_date,$membership_start_date,$payment_method,$check_number,$optional_discount,$optional_discount_flag);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function sendExpiryEmailToStudio(){
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($error, 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id)) {
            $this->sendExpiryEmailDetailsToStudio($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
//    Stripe checkout
    private function stripeMembershipCheckout(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $membership_id = $membership_option_id = $membership_category_title = $membership_title = $membership_desc = $recurring_start_date = '';
        $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = $payment_frequency = $membership_start_date = $payment_start_date = $delay_recurring_payment_start_date = $custom_recurring_frequency_period_type = '';
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = $membership_discount_code = $signup_discount_code = '';
        $student_id =$participant_id = $payment_amount = $processing_fee = $membership_fee = $membership_fee_disc = $signup_fee = $signup_fee_disc = $initial_payment = $custom_recurring_frequency_period_val = 0;
        $upgrade_status = 'F';
        $country = 'US';
        $prorate_first_payment_flg = $delay_recurring_payment_start_flg = 'N';
        $membership_payment_array = ['N','W','B','M','A','C'];
        $payment_array = [];
        $no_of_classes = $billing_options_deposit_amount = $billing_options_no_of_payments = $delay_recurring_payment_amount = $first_payment = 0;
        $billing_options = $billing_options_expiration_date = '';
        $membership_structure = 'OE';
        $initial_payment_include_membership_fee = 'Y';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        $reg_type_user='U';
        $reg_version_user = '';
        $exclude_from_billing_flag='N';
        $exclude_days_array=[];
        $program_start_date = $program_end_date = $reg_current_date = '';
        $token = $pos_email = '';
        $optional_discount_flag = $optional_discount = $check_number = '';
        $payment_method = "CC";
        $intent_method_type = $payment_method_id = $stripe_customer_id = $cc_type = '';
        
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        if(isset($this->_request['membership_id'])){
            $membership_id = $this->_request['membership_id'];
        }
        if(isset($this->_request['membership_option_id'])){
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if(isset($this->_request['studentid'])){
            $student_id = $this->_request['studentid'];
        }
        if(isset($this->_request['participant_id'])){
            $participant_id = $this->_request['participant_id'];
        }
        if(isset($this->_request['membership_category_title'])){
            $membership_category_title = $this->_request['membership_category_title'];
        }
        if(isset($this->_request['membership_title'])){
            $membership_title = $this->_request['membership_title'];
        }
        if(isset($this->_request['membership_desc'])){
            $membership_desc = $this->_request['membership_desc'];
        }
        if(isset($this->_request['membership_structure'])){
            $membership_structure = $this->_request['membership_structure'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if(isset($this->_request['email'])){
            $buyer_email = $this->_request['email'];
        }
        if(isset($this->_request['phone'])){
            $buyer_phone = $this->_request['phone'];
        }
        if(isset($this->_request['postal_code'])){
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if(isset($this->_request['country'])){
            $country = $this->_request['country'];
        }
        if(isset($this->_request['cc_id'])){
            $cc_id = $this->_request['cc_id'];
        }
        if(isset($this->_request['cc_state'])){
            $cc_state = $this->_request['cc_state'];
        }
        if(isset($this->_request['payment_amount'])){
            $payment_amount = $this->_request['payment_amount'];
        }
        if(isset($this->_request['processing_fee_type'])){
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if(isset($this->_request['processing_fee'])){
            $processing_fee = $this->_request['processing_fee'];
        }
        if(isset($this->_request['membership_fee'])){
            $membership_fee = $this->_request['membership_fee'];
        }
        if(isset($this->_request['membership_fee_disc'])){
            $membership_fee_disc = $this->_request['membership_fee_disc'];
        }
        if(isset($this->_request['signup_fee'])){
            $signup_fee = $this->_request['signup_fee'];
        }
        if(isset($this->_request['signup_fee_disc'])){
            $signup_fee_disc = $this->_request['signup_fee_disc'];
        }
        if(isset($this->_request['initial_payment'])){      //registration time payment
            $initial_payment = $this->_request['initial_payment'];
        }
        if(isset($this->_request['first_payment'])){      //first payment on membership start date
            $first_payment = $this->_request['first_payment'];
        }
        if(isset($this->_request['payment_frequency'])){
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if(isset($this->_request['membership_start_date'])){
            $membership_start_date = $this->_request['membership_start_date'];
        }
        if(isset($this->_request['payment_start_date'])){
            $payment_start_date = $this->_request['payment_start_date'];
        }
        if(isset($this->_request['recurring_start_date'])){
            $recurring_start_date = $this->_request['recurring_start_date'];
        }
        if(isset($this->_request['reg_col1'])){
            $reg_col1 = $this->_request['reg_col1'];
        }
        if(isset($this->_request['reg_col2'])){
            $reg_col2 = $this->_request['reg_col2'];
        }
        if(isset($this->_request['reg_col3'])){
            $reg_col3 = $this->_request['reg_col3'];
        }
        if(isset($this->_request['reg_col4'])){
            $reg_col4 = $this->_request['reg_col4'];
        }
        if(isset($this->_request['reg_col5'])){
            $reg_col5 = $this->_request['reg_col5'];
        }
        if(isset($this->_request['reg_col6'])){
            $reg_col6 = $this->_request['reg_col6'];
        }
        if(isset($this->_request['reg_col7'])){
            $reg_col7 = $this->_request['reg_col7'];
        }
        if(isset($this->_request['reg_col8'])){
            $reg_col8 = $this->_request['reg_col8'];
        }
        if(isset($this->_request['reg_col9'])){
            $reg_col9 = $this->_request['reg_col9'];
        }
        if(isset($this->_request['reg_col10'])){
            $reg_col10 = $this->_request['reg_col10'];
        }
        if(isset($this->_request['upgrade_status'])){
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if(isset($this->_request['prorate_first_payment_flg']) && !empty(trim($this->_request['prorate_first_payment_flg']))){
            $prorate_first_payment_flg = $this->_request['prorate_first_payment_flg'];
        }
        if(isset($this->_request['delay_recurring_payment_start_flg']) && !empty(trim($this->_request['delay_recurring_payment_start_flg']))){
            $delay_recurring_payment_start_flg = $this->_request['delay_recurring_payment_start_flg'];
        }
        if(isset($this->_request['delay_recurring_payment_start_date'])){
            $delay_recurring_payment_start_date = $this->_request['delay_recurring_payment_start_date'];
        }
        if(isset($this->_request['delay_recurring_payment_amount'])){
            $delay_recurring_payment_amount = $this->_request['delay_recurring_payment_amount'];
        }
        if(isset($this->_request['custom_recurring_frequency_period_type'])){
            $custom_recurring_frequency_period_type = $this->_request['custom_recurring_frequency_period_type'];
        }
        if(isset($this->_request['custom_recurring_frequency_period_val'])){
            $custom_recurring_frequency_period_val = $this->_request['custom_recurring_frequency_period_val'];
        }
        if(isset($this->_request['initial_payment_include_membership_fee'])){
            $initial_payment_include_membership_fee = $this->_request['initial_payment_include_membership_fee'];
        }
        if(isset($this->_request['billing_options'])){
            $billing_options = $this->_request['billing_options'];
        }
        if(isset($this->_request['no_of_classes'])){
            $no_of_classes = $this->_request['no_of_classes'];
        }
        if(isset($this->_request['billing_options_expiration_date'])){
            $billing_options_expiration_date = $this->_request['billing_options_expiration_date'];
        }
        if(isset($this->_request['billing_options_deposit_amount'])){
            $billing_options_deposit_amount = $this->_request['billing_options_deposit_amount'];
        }
        if(isset($this->_request['billing_options_no_of_payments'])){
            $billing_options_no_of_payments = $this->_request['billing_options_no_of_payments'];
        }
        if(isset($this->_request['payment_array'])){
            $payment_array = $this->_request['payment_array'];
        }
        if(isset($this->_request['participant_street'])){
            $participant_street = $this->_request['participant_street'];
        }
        if(isset($this->_request['participant_city'])){
            $participant_city = $this->_request['participant_city'];
        }
        if(isset($this->_request['participant_state'])){
            $participant_state = $this->_request['participant_state'];
        }
        if(isset($this->_request['participant_zip'])){
            $participant_zip = $this->_request['participant_zip'];
        }
        if(isset($this->_request['participant_country'])){
            $participant_country = $this->_request['participant_country'];
        }
        if(isset($this->_request['membership_discount_code'])){
            $membership_discount_code = $this->_request['membership_discount_code'];
        }
        if(isset($this->_request['signup_discount_code'])){
            $signup_discount_code = $this->_request['signup_discount_code'];
        }
        if(isset($this->_request['exclude_days_array'])){
            $exclude_days_array = $this->_request['exclude_days_array'];
        }
        if(isset($this->_request['program_start_date'])){
            $program_start_date = $this->_request['program_start_date'];
        }
        if(isset($this->_request['program_end_date'])){
            $program_end_date = $this->_request['program_end_date'];
        }
        if(isset($this->_request['exclude_from_billing_flag'])){
            $exclude_from_billing_flag = $this->_request['exclude_from_billing_flag'];
        }
        if(isset($this->_request['registration_date'])){
            $reg_current_date = $this->_request['registration_date'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['check_number']) && !empty($this->_request['check_number'])){
            $check_number = $this->_request['check_number'];
        }
        if (isset($this->_request['optional_discount'])) {
            if(!empty($this->_request['optional_discount'])){
            $optional_discount = $this->_request['optional_discount'];
            }
        }
        if (isset($this->_request['optional_discount_flag'])) {
            $optional_discount_flag = $this->_request['optional_discount_flag'];
        }
        if (isset($this->_request['token'])) {
            $token = $this->_request['token'];
        }
        if (isset($this->_request['pos_email'])) {
            $pos_email = $this->_request['pos_email'];
        }
        if(isset($this->_request['payment_method_id'])){
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if(isset($this->_request['intent_method_type'])){
            $intent_method_type = $this->_request['intent_method_type'];
        }
        if (isset($this->_request['cc_type'])) {    // cc_type - N - New, E - Existing
            $cc_type = $this->_request['cc_type'];
        }
        if (isset($this->_request['stripe_customer_id'])) {    // cc_type - N - New, E - Existing
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        if($reg_type_user !== 'U'){
            $this->checkpossettings($company_id, '', $pos_email, $token);
        }
        if($reg_type_user === "PP" && (((int)$optional_discount > 0) || $optional_discount_flag === "Y")){
            $error = array('status' => "Failed", "msg" => "Optional discount is not applicable");
            $this->response($this->json($error), 200);
        }
        if($payment_method === "CH" && empty(trim($check_number))){
            $error = array('status' => "Failed", "msg" => "Please provide check number");
            $this->response($this->json($error), 200);
        }
        if(empty($reg_type_user) || ($reg_type_user === 'PP' && !empty(trim($student_id)))){
            $error = array('status' => "Failed", "msg" => "Authentication Error");
            $this->response($this->json($error), 200);
        }
        //Input validations
        if(!empty(trim($payment_frequency)) && !in_array($payment_frequency, $membership_payment_array)){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if ($initial_payment != 0 || $first_payment != 0 || $payment_amount != 0) {
            if ($payment_method === "CC" && empty(trim($payment_method_id))) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 200);
            }
        }
        if(!empty(trim($payment_frequency)) && $payment_frequency=='C' && (empty(trim($custom_recurring_frequency_period_type)) || empty(trim($custom_recurring_frequency_period_val)))){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if(!empty($membership_structure) && $membership_structure=='OE' && $payment_amount>0 && !empty(trim($delay_recurring_payment_start_flg)) && $delay_recurring_payment_start_flg=='Y' && empty(trim($delay_recurring_payment_start_date))){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }        
        if(!empty($membership_structure) && $membership_structure=='NC' && empty($billing_options)){
            if(!empty($billing_options) && $billing_options=='PP' && empty($payment_array)){
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }
        }
        if(!empty($membership_structure) && $membership_structure=='SE' && ((empty($program_start_date) || $program_start_date=='0000-00-00' || empty($program_end_date) || $program_end_date=='0000-00-00') || (!empty($billing_options) && $billing_options=='PP' && $payment_amount > 0 && empty($payment_array)))){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);        
        }
        
        if(!empty($company_id) && !empty($membership_id) && !empty($membership_option_id) && !empty($membership_category_title) && !empty($membership_title) && !empty($membership_desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)){
            $this->stripeMembershipCreateCheckout($company_id, $membership_id, $membership_option_id, $student_id,$participant_id, $upgrade_status, $membership_category_title, $membership_title, $membership_desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state,
                    $processing_fee_type, $processing_fee, $membership_fee, $membership_fee_disc, $signup_fee, $signup_fee_disc, $payment_amount, $initial_payment, $first_payment, $payment_frequency, $payment_start_date, $recurring_start_date, 
                    $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $prorate_first_payment_flg, 
                    $delay_recurring_payment_start_flg, $delay_recurring_payment_start_date, $delay_recurring_payment_amount, $custom_recurring_frequency_period_type, $custom_recurring_frequency_period_val, $initial_payment_include_membership_fee, $membership_structure,
                    $billing_options, $no_of_classes, $billing_options_expiration_date, $billing_options_deposit_amount, $billing_options_no_of_payments, $payment_array,
                    $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $membership_discount_code, $signup_discount_code,$exclude_days_array,$program_start_date,$program_end_date,$exclude_from_billing_flag,$reg_type_user,$reg_version_user,$reg_current_date,$membership_start_date,$payment_method,$check_number,$optional_discount,$optional_discount_flag,$payment_method_id,$intent_method_type,$cc_type,$stripe_customer_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    // End of stripe checkout
    
} 

// Initiiate Library
    $api = new MembershipApi;
    $api->processApi();



?>