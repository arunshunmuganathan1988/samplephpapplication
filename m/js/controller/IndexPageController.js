/*jslint browser: true*/
/*global console, MyApp*/
//www
MyApp.angular.controller('IndexPageController', ['$rootScope', '$scope', '$http', 'urlservice', '$localStorage', 'openurlservice', 'InitService', '$window', function ($rootScope, $scope, $http, urlservice, $localStorage, openurlservice, InitService, $window) {
        'use strict';

        InitService.addEventListener('ready', function ()
        {

            //   Call company details on menu open
            $$('.panel-left').on('panel:opened', function () {
//                $scope.companydetails();
            });

        });

        //No Internet - show alert
        $scope.istatus = function () {
            MyApp.alert('Please turn on internet', 'No Internet');
            $scope.preloaderVisible = false;
            return;
        };

        //  Muthulakshmi
        $scope.studentlist = [];
        $scope.student_name = [];
        $scope.memcarddetails = [];
        $scope.showcardselection = false;
        $scope.shownewcard = true;
        $scope.memfee_optional_discount_applied_flag = 'N';
        $scope.signupfee_optional_discount_applied_flag = 'N';
        $scope.mem_search_member = '';
        $scope.errormemfeeoptionaldiscount = false;
        $scope.memfee_opt_discount = '';
        $scope.signupfee_opt_discount = '';
        $scope.memfee_discount_applied_flag = $scope.signupfee_discount_applied_flag = 'N';
        $scope.signupfee_Discode_Found = $scope.membershipfee_Discode_Found = false;
        $scope.signupfee_DiscountAmount = '';
        $scope.membershipfee_DiscountAmount = '';
        $scope.signupfee_afterdiscount = $scope.membershipfee_afterdiscount = 0;
        $scope.signupfee_DiscountCode = '';
        $scope.membershipfee_DiscountCode = '';
        $scope.membership_payment_method = 'CC';
        $scope.memcarddetails = [];
        $scope.membership_check_number = '';
        $scope.static_signup_discount = '';
        $scope.static_membership_discount = '';
        $scope.wepaystatus = $scope.stripestatus = '';
        $scope.wepay_enabled = $scope.stripe_enabled = $scope.mem_ach_chkbx = $scope.mem_cc_chkbx = $scope.mem_waiver_checked = false;
        $scope.stripe_status='';
        $scope.mem_ach_route_no = $scope.mem_ach_acc_number = $scope.mem_ach_holder_name = $scope.mem_ach_acc_type ='';
        $scope.stripe_card = $scope.stripe = '';
        $scope.stripe_intent_method_type = "PI";
        $scope.payment_method_id = $scope.stripe_customer_id = '';
 
        $scope.initial_load_payment = function () {
            $scope.onetimecheckout = 0;
            $scope.ccyearlist = ["2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037"];
            $scope.ccmonthlist = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
            $scope.ccmonth = '';
            $scope.ccyear = '';
            $scope.country = '';
            if ($scope.wp_level === 'P') {
                $scope.wepay_env = "production";
                $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
            } else if ($scope.wp_level === 'S') {
                $scope.wepay_env = "stage";
                $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
            }
            WePay.set_endpoint($scope.wepay_env); // change to "production" when live  
        };


        // Shortcuts
        var valueById = '';
        var d = document;
        d.id = d.getElementById, valueById = function (id) {
            return d.id(id).value;
        };

        // For those not using DOM libraries
        var addEvent = function (e, v, f) {
            if (!!window.attachEvent) {
                e.attachEvent('on' + v, f);
            } else {
                e.addEventListener(v, f, false);
            }
        };
         // ATTACH THE RETAIL TO THE DOM - WEB VIEW SUBMIT BUTTON
        addEvent(d.id('cc-submit'), 'click', function () {
           $scope.MembershipCheckoutRedirect();
        });
       
       // ATTACH THE RETAIL TO THE DOM - MOBILE VIEW SUBMIT BUTTON
       addEvent(d.id('cc-submit1'), 'click', function () {
            $scope.MembershipCheckoutRedirect();
        });

        // ATTACH THE MEMBERSHIP TO THE DOM
        $scope.MembershipCheckoutRedirect = function(){
            
            var checkout_message1 = '';
            for (var i = 0; i < $scope.current_membership_reg_columns.length; i++) {
                if (parseInt(i) !== 2) {
                    if ($scope.msreg_col_field_name[i] && $scope.msreg_col_field_name[i] !== undefined && $scope.msreg_col_field_name[i] !== '') {

                    } else {
                        if ($scope.current_membership_reg_columns[i].reg_col_mandatory === 'Y') {
                            checkout_message1 += '"' + $scope.current_membership_reg_columns[i].reg_col_name + '"';
                            checkout_message1 += '<br>'
                        }
                        $scope.msreg_col_field_name[i] = '';
                    }
                }
            }
            var birthday = document.getElementById("column2").value;
            if (birthday == '') {
                checkout_message1 += '"Date of birth"<br>';
            }
            if (birthday) {
                var birthdaysplit = birthday.split("-");
                var bmonth = birthdaysplit[1];
                var bday = birthdaysplit[2];
                var byear = birthdaysplit[0];
                if (!bmonth || !bday || !byear) {
                    if (!bmonth) {
                        checkout_message1 += '"DOB Month"<br>';
                    }
                    if (!bday) {
                        checkout_message1 += '"DOB Day"<br>';
                    }
                    if (!byear) {
                        checkout_message1 += '"DOB Year"<br>';
                    }
                }
            }
            if (checkout_message1 !== '') {
                checkout_message1 = 'Participant Info: ' + checkout_message1;
            }
            if ($scope.mems1_firstname === '' || $scope.mems1_firstname === undefined || $scope.mems1_lastname === '' || $scope.mems1_lastname === undefined || $scope.mems1_email === '' || $scope.mems1_email === undefined || $scope.mems1_phone === '' || $scope.mems1_phone === undefined)
            {
                checkout_message1 += "Buyer's Info: ";
                if ($scope.mems1_firstname === '' || $scope.mems1_firstname === undefined) {
                    checkout_message1 += '"First Name"<br>';
                }
                if ($scope.mems1_lastname === '' || $scope.mems1_lastname === undefined) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if ($scope.mems1_email === '' || $scope.mems1_email === undefined) {
                    checkout_message1 += '"Email"<br>';
                }
                if ($scope.mems1_phone === '' || $scope.mems1_phone === undefined) {
                    checkout_message1 += '"Cell Phone"<br>';
                }
            }
            if ($scope.participant_Streetaddress === '' || $scope.participant_Streetaddress === undefined || $scope.participant_City === '' || $scope.participant_City === undefined || $scope.participant_State === '' || $scope.participant_State === undefined || $scope.participant_Zip === '' || $scope.participant_Zip === undefined || $scope.participant_Country === '' || $scope.participant_Country === undefined)
            {
                checkout_message1 += 'Address: ';
                if ($scope.participant_Streetaddress === '' || $scope.participant_Streetaddress === undefined) {
                    checkout_message1 += '"Street address"<br>';
                }
                if ($scope.participant_City === '' || $scope.participant_City === undefined) {
                    checkout_message1 += '"City"<br>';
                }
                if ($scope.participant_State === '' || $scope.participant_State === undefined) {
                    checkout_message1 += '"State"<br>';
                }
                if ($scope.participant_Zip === '' || $scope.participant_Zip === undefined) {
                    checkout_message1 += '"Zip"<br>';
                }
                if ($scope.participant_Country === '' || $scope.participant_Country === undefined) {
                    checkout_message1 += '"Country"<br>';
                }
            }
            if ($scope.membership_payment_method === 'CC') {
                if ($scope.mems1_cardnumber === '' || $scope.mems1_cardnumber === undefined || $scope.mems1_ccmonth === '' || $scope.mems1_ccmonth === undefined || $scope.mems1_ccyear === '' || $scope.mems1_ccyear === undefined || $scope.mems1_cvv === '' || $scope.mems1_cvv === undefined || $scope.mems1_country === '' || $scope.mems1_country === undefined || $scope.mems1_postal_code === '' || $scope.mems1_postal_code === undefined)
                {
                    checkout_message1 += 'Payment Info: ';
                    if ($scope.mems1_cardnumber === '' || $scope.mems1_cardnumber === undefined) {
                        checkout_message1 += '"Card number"<br>';
                    }
                    if ($scope.mems1_ccmonth === '' || $scope.mems1_ccmonth === undefined) {
                        checkout_message1 += '"Expiration Date:Month"<br>';
                    }
                    if ($scope.mems1_ccyear === '' || $scope.mems1_ccyear === undefined) {
                        checkout_message1 += '"Expiration Date:Year"<br>';
                    }
                    if ($scope.mems1_cvv === '' || $scope.mems1_cvv === undefined) {
                        checkout_message1 += '"CVV"<br>';
                    }
                    if ($scope.mems1_country === '' || $scope.mems1_country === undefined) {
                        checkout_message1 += '"Choose country"<br>';
                    }
                    if ($scope.mems1_postal_code === '' || $scope.mems1_postal_code === undefined) {
                        checkout_message1 += '"Zip code"<br>';
                    }
                }
            }
            if (!$scope.mem_waiver_checked) {
                checkout_message1 += "Click the check box to accept Waiver & Agreement<br>";
            }
            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }

            var userName = [valueById('mems1_firstName')] + ' ' + [valueById('mems1_lastName')];
            var user_first_name = valueById('mems1_firstName');
            var user_last_name = valueById('mems1_lastName');
            var email = valueById('mems1_email').trim();
            var phone = valueById('mems1_phone');
            var postal_code = valueById('mems1_postal_code');
            var country = valueById('mems1_country');
            var response = WePay.credit_card.create({
                "client_id": $scope.wp_client_id,
                "user_name": userName,
                "email": valueById('mems1_email').trim(),
                "cc_number": valueById('mems1_cardnumber'),
                "cvv": valueById('mems1_cvv'),
                "expiration_month": valueById('mems1_ccmonth'),
                "expiration_year": valueById('mems1_ccyear'),
                "address": {
                    "country": valueById('mems1_country'),
                    "postal_code": valueById('mems1_postal_code')
                }
            }, function (data) {
                if (data.error) {
                    $scope.preloader = false;
                    $scope.$apply();
                    MyApp.closeModal();
                    MyApp.alert(data.error_description, 'Message');
                } else {
                    if ($scope.onetimecheckout == 0) {
                        $scope.onetimecheckout = 1;
                        $scope.membershipcheckout(data.credit_card_id, data.state, user_first_name, user_last_name, email, phone, postal_code, country);
                    }
                }
            });
        };
       
        // Stripe Membership Checkout 
        $scope.membershipStripeCheckout = function () {
            $scope.preloader = true;
            if($scope.validateStripeForm()){ // Validate Stripe Checkout Form
            
                var birthday = document.getElementById("column2").value;

                if ($scope.membership_payment_method === 'CA') {
                    $scope.membership_check_number = '';
                }

                var curr_date = new Date();
                var reg_current_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);

                var delay_recurring_payment_start_date = '';
                var membership_start_date = $scope.payment_start_date;
                if ($scope.mem_rec_frequency === 'N' && $scope.membership_structure === 'OE') {
                    delay_recurring_payment_start_date = $scope.total_payment_startdate;
                } else if ($scope.membership_structure === 'OE' && $scope.delay_rec_payment_start_flg === 'Y') {
                    delay_recurring_payment_start_date = $scope.delayed_recurring_payment_startdate;
                    $scope.payment_start_date = delay_recurring_payment_start_date;
                } else if ($scope.membership_billing_option === 'PF' && ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' || $scope.membership_structure === 'SE')) {
                    delay_recurring_payment_start_date = $scope.payment_start_date;
                } else {
                    delay_recurring_payment_start_date = $scope.recurring_payment_startdate;
                }

                var exclude_days = [];
                if ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' || $scope.membership_structure === 'SE') {
                    $scope.final_pay = $scope.pay;
                    if ($scope.membership_structure === 'SE') {
                        exclude_days = $scope.mem_billing_exclude_days;
                    } else {
                        exclude_days = [];
                    }
                } else {
                    $scope.final_pay = [];
                    exclude_days = [];
                }
                // Intent method type for card payment
                if($scope.final_membership_payment_amount == 0 && $scope.zerocostview){
                    $scope.stripe_intent_method_type = 'SI';
                }else{
                    $scope.stripe_intent_method_type = "PI";
                }

                $scope.payment_amount = (Math.round((+$scope.final_membership_payment_amount + 0.00001) * 100) / 100).toFixed(2);
                var fee = 0;
                fee = $scope.pay_infull_processingfee;

                $http({
                    method: 'POST',
                    url: urlservice.url + 'stripeMembershipCheckout',
                    data: {
                        companyid: $scope.company_id,
                        membership_id: $scope.membershipcategory_id,
                        membership_option_id: $scope.membershipoption_id,
                        membership_category_title: $scope.MS_CategoryTitle,
                        membership_title: $scope.membershipoptiontitle,
                        membership_desc: $scope.membershipoptiontitle,
                        buyer_first_name: $scope.mems1_firstname,
                        buyer_last_name: $scope.mems1_lastname,
                        email: $scope.mems1_email,
                        phone: $scope.mems1_phone,
                        postal_code: $scope.participant_Zip,
                        country: $scope.participant_Country,
                        payment_amount: $scope.total_mem_amount_view, //reccurring amt
                        processing_fee_type: $scope.current_processing_fee_type,
                        processing_fee: fee,
                        membership_fee: $scope.memfee,
                        membership_fee_disc: $scope.membershipfee_DiscountAmount,
                        signup_fee: $scope.memsignupfee,
                        signup_fee_disc: $scope.signupfee_DiscountAmount,
                        initial_payment: $scope.payment_amount, //First time amt
                        first_payment: $scope.first_payment_amount, //Membership start date payment amount
                        payment_frequency: $scope.mem_rec_frequency,
                        payment_start_date: $scope.payment_start_date, //payment start date
                        membership_start_date: membership_start_date, // Membership start date
                        recurring_start_date: $scope.recurring_payment_startdate, //recurring startdte
                        prorate_first_payment_flg: $scope.prorate_first_payment,
                        delay_recurring_payment_start_flg: $scope.delay_rec_payment_start_flg,
                        delay_recurring_payment_start_date: delay_recurring_payment_start_date, //recurr start date
                        initial_payment_include_membership_fee: $scope.mem_fee_include_status,
                        delay_recurring_payment_amount: $scope.delay_membership_amount_view,
                        custom_recurring_frequency_period_type: $scope.cus_rec_freq_period_type,
                        custom_recurring_frequency_period_val: $scope.cus_rec_freq_period_val,
                        membership_structure: $scope.membership_structure,
                        no_of_classes: $scope.membership_no_of_classes,
                        billing_options_expiration_date: $scope.membership_expiry_date,
                        billing_options: $scope.membership_billing_option,
                        billing_options_deposit_amount: $scope.membership_billing_deposit_amount,
                        billing_options_no_of_payments: $scope.membership_no_of_payments,
                        payment_array: $scope.final_pay,
                        reg_col1: $scope.msreg_col_field_name[0],
                        reg_col2: $scope.msreg_col_field_name[1],
                        reg_col3: birthday,
                        reg_col4: $scope.msreg_col_field_name[3],
                        reg_col5: $scope.msreg_col_field_name[4],
                        reg_col6: $scope.msreg_col_field_name[5],
                        reg_col7: $scope.msreg_col_field_name[6],
                        reg_col8: $scope.msreg_col_field_name[7],
                        reg_col9: $scope.msreg_col_field_name[8],
                        reg_col10: $scope.msreg_col_field_name[9],
                        upgrade_status: $scope.company_status,
                        participant_street: $scope.participant_Streetaddress,
                        participant_city: $scope.participant_City,
                        participant_state: $scope.participant_State,
                        participant_zip: $scope.participant_Zip,
                        participant_country: $scope.participant_Country,
                        signup_discount_code: $scope.signupfee_DiscountCode,
                        membership_discount_code: $scope.membershipfee_DiscountCode,
                        registration_date: reg_current_date,
                        exclude_days_array: exclude_days,
                        program_start_date: $scope.specific_start_date,
                        program_end_date: $scope.specific_end_date,
                        exclude_from_billing_flag: $scope.exclude_from_billing_flag,
                        pos_email: $localStorage.pos_user_email_id,
                        payment_method: $scope.membership_payment_method, //CC - creadit card,CH - check, CA- Cash
                        check_number: $scope.membership_check_number,
                        memfee_optional_discount: $scope.memfee_opt_discount, //memfee optional discount
                        memfee_optional_discount_flag: $scope.memfee_optional_discount_applied_flag,
                        signupfee_optional_discount: $scope.signupfee_opt_discount,
                        signupfee_optional_discount_flag: $scope.signupfee_optional_discount_applied_flag,
                        reg_type_user: $scope.pos_user_type === 'S' ? 'SP' : ($scope.pos_user_type === 'P' ? 'PP' : 'U'),
                        token: $scope.pos_token,
                        payment_method_id: $scope.payment_method_id,
                        intent_method_type: $scope.stripe_intent_method_type, // SI OR PI
                        cc_type: ($scope.shownewcard) ? 'N' : 'E',
                        stripe_customer_id : $scope.stripe_customer_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.onetimecheckout = 0;
                        MyApp.closeModal();
                        $scope.resetCartAfterMembershipPaymentSucceed();
                        $scope.preloader = false;
                        MyApp.alert(response.data.msg, 'Message');
                    } else {
                        $scope.onetimecheckout = 0;
                        if (response.data.upgrade_status && response.data.studio_expiry_level) {
                            $scope.cmpny_email = response.data.studio_email;
                            $scope.company_status = response.data.upgrade_status;
                            $scope.company_expiry_level = response.data.studio_expiry_level;
                        }
                        if (response.data.error) {
                            $scope.preloader = false;
                            $scope.$apply();
                            MyApp.closeModal();
                            MyApp.alert(response.data.error + ' ' + response.data.error_description, 'Message');
                        } else if (($scope.company_status && $scope.company_status === 'F') || ($scope.company_expiry_level && $scope.company_expiry_level === 'L2')) {
                            MyApp.closeModal();
                            // PREVENT FURTHUR EXECUTION & BLOCK THE APP
                            MyApp.modal({
                                title: '',
                                afterText:
                                        '<br>' +
                                        '<div id="studioExpiryText" class="modal-text custom-modal-content">Please contact the business operator to activate this link.</div>' +
                                        '<br>' +
                                        '<button class="button custom-modal-green-button button-text modalresetButton" id="send_expiry_email"></button>'
                            });

                            if ($scope.cmpny_email) {
                                $("#send_expiry_email").text("Send email message");
                            } else {
                                $("#send_expiry_email").text("Ok");
                            }

                            $('#send_expiry_email').click(function () {
                                $scope.$apply(function () {
                                    if ($("#send_expiry_email").text() === 'Send email message') {
                                        $scope.sendExpiryEmailToStudio();
                                    }
                                    MyApp.closeModal();
                                });
                            });

                        } else {
                            MyApp.closeModal();
                            if(response.data.session_status === "Expired"){
                                MyApp.alert('Session Expired','', function () {
                                    $localStorage.pos_user_email_id = "";
                                    $scope.backToPOS();
                                });
                            } else {
                                MyApp.alert(response.data.msg, 'Message');
                            }
                            $scope.preloader = false;
                            $scope.preloaderVisible = false;
                            $scope.$apply();
                        }

                    }
                }, function (response) {
                    $scope.onetimecheckout = 0;
                    $scope.preloaderVisible = false;
                    $scope.preloader = false;
                    MyApp.alert('Connection failed', 'Invalid Server Response');
                });
            }  else{
                $scope.preloader = false;
            }
        };
        //Stripe status 
        //get Stripe status of this company
        $scope.getStripeStatus = function (cmp_id) {
            if ($scope.stripestatus !== 'Y') {
                $scope.wepay_enabled = $scope.stripe_enabled = false;
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getstripestatus',
                    params: {
                        "companyid": cmp_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'},
                        withCredentials: true

                }).then(function (response) {                   
                    if (response.data.status === 'Success') {
                        $scope.stripestatus = response.data.msg;
                        if ($scope.stripestatus !== 'N') {
                            $localStorage.preview_wepaystatus = $scope.wepaystatus = 'Y';
                            if ($scope.stripe_country_support === 'Y') {
                                $scope.stripe_enabled = true;
                                $scope.stripe = Stripe($scope.stripe_publish_key);
                                $scope.getStripeElements();
                            }
                        }else{
                            $scope.getWepayStatus(cmp_id);
                        }      
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        MyApp.alert(response.data.msg, '');
                    } else {
                       if(response.data.msg && response.data.msg !== 'N'){
                            MyApp.alert(response.data.msg, '');
                        }else{
                            $scope.getWepayStatus(cmp_id);
                        }
                    }
                }, function (response) {
                    console.log(response.data);
                    MyApp.alert(response.data.msg, '');
                });
            } else {
                if ($scope.stripestatus !== 'N' && $scope.stripe_country_support === 'Y') {
                    $scope.stripe_enabled = true;
                } else {
                    $scope.wepay_enabled = $scope.stripe_enabled = false;
                    $scope.getWepayStatus(cmp_id);
                }
            }
        };
        
        $scope.validateStripeForm = function(){
            $scope.preloader = true;
            var checkout_message1 = '';
                for (var i = 0; i < $scope.current_membership_reg_columns.length; i++) {
                    if (parseInt(i) !== 2) {
                        if ($scope.msreg_col_field_name[i] && $scope.msreg_col_field_name[i] !== undefined && $scope.msreg_col_field_name[i] !== '') {

                        } else {
                            if ($scope.current_membership_reg_columns[i].reg_col_mandatory === 'Y') {
                                checkout_message1 += '"' + $scope.current_membership_reg_columns[i].reg_col_name + '"';
                                checkout_message1 += '<br>'
                            }
                            $scope.msreg_col_field_name[i] = '';
                        }
                    }
                }
                var birthday = document.getElementById("column2").value;
                if (birthday == '') {
                    checkout_message1 += '"Date of birth"<br>';
                }
                if (birthday) {
                    var birthdaysplit = birthday.split("-");
                    var bmonth = birthdaysplit[1];
                    var bday = birthdaysplit[2];
                    var byear = birthdaysplit[0];
                    if (!bmonth || !bday || !byear) {
                        if (!bmonth) {
                            checkout_message1 += '"DOB Month"<br>';
                        }
                        if (!bday) {
                            checkout_message1 += '"DOB Day"<br>';
                        }
                        if (!byear) {
                            checkout_message1 += '"DOB Year"<br>';
                        }
                    }
                }
                if (checkout_message1 !== '') {
                    checkout_message1 = 'Participant Info: ' + checkout_message1;
                }
                if (!$scope.mems1_firstname === '' || $scope.mems1_firstname === undefined || $scope.mems1_lastname === '' || $scope.mems1_lastname === undefined || $scope.mems1_email === '' || $scope.mems1_email === undefined || $scope.mems1_phone === '' || $scope.mems1_phone === undefined)
                {
                    checkout_message1 += "Buyer's Info: ";
                    if ($scope.mems1_firstname === '' || $scope.mems1_firstname === undefined) {
                        checkout_message1 += '"First Name"<br>';
                    }
                    if ($scope.mems1_lastname === '' || $scope.mems1_lastname === undefined) {
                        checkout_message1 += '"Last Name"<br>';
                    }
                    if ($scope.mems1_email === '' || $scope.mems1_email === undefined) {
                        checkout_message1 += '"Email"<br>';
                    }
                    if ($scope.mems1_phone === '' || $scope.mems1_phone === undefined) {
                        checkout_message1 += '"Cell Phone"<br>';
                    }
                }
                if ($scope.participant_Streetaddress === '' || $scope.participant_Streetaddress === undefined || $scope.participant_City === '' || $scope.participant_City === undefined || $scope.participant_State === '' || $scope.participant_State === undefined || $scope.participant_Zip === '' || $scope.participant_Zip === undefined || $scope.participant_Country === '' || $scope.participant_Country === undefined)
                {
                    checkout_message1 += 'Address: ';
                    if ($scope.participant_Streetaddress === '' || $scope.participant_Streetaddress === undefined) {
                        checkout_message1 += '"Street address"<br>';
                    }
                    if ($scope.participant_City === '' || $scope.participant_City === undefined) {
                        checkout_message1 += '"City"<br>';
                    }
                    if ($scope.participant_State === '' || $scope.participant_State === undefined) {
                        checkout_message1 += '"State"<br>';
                    }
                    if ($scope.participant_Zip === '' || $scope.participant_Zip === undefined) {
                        checkout_message1 += '"Zip"<br>';
                    }
                    if ($scope.participant_Country === '' || $scope.participant_Country === undefined) {
                        checkout_message1 += '"Country"<br>';
                    }
                }
                if($scope.final_membership_payment_amount > 0 || $scope.zerocostview){
                    if (($scope.shownewcard && !$scope.valid_stripe_card && $scope.membership_payment_method === 'CC')
                       || ($scope.pos_user_type === 'S' && $scope.membership_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex)
                        || (($scope.pos_user_type === 'S' || $scope.pos_user_type === 'P') && $scope.membership_payment_method ==='CH' && !$scope.membership_check_number)){
                    
                        checkout_message1 += 'Payment Info:';
                        if($scope.pos_user_type === 'S' && $scope.membership_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex){
                           checkout_message1 += '"Select Card"<br>'; 
                        }
                        if(($scope.pos_user_type === 'S' || $scope.pos_user_type === 'P') && $scope.membership_payment_method ==='CH' && !$scope.membership_check_number){  
                            checkout_message1 += '"Check Number"<br>';
                        }
                        if($scope.shownewcard && !$scope.valid_stripe_card && $scope.membership_payment_method === 'CC'){
                            checkout_message1 += '"Invalid card details"<br>';
                        }
                    }
                }
                if (!$scope.mem_waiver_checked && (($scope.final_membership_payment_amount == 0 && !$scope.zerocostview) 
                        || (($scope.pos_user_type === 'S' || $scope.pos_user_type === 'P')  && ($scope.membership_payment_method === 'CA' || $scope.membership_payment_method === 'CH')))) {
                    checkout_message1 += "Click the check box to accept Waiver & Agreement<br>";
                }
                if (!$scope.mem_cc_chkbx && $scope.membership_payment_method === 'CC' && ($scope.final_membership_payment_amount > 0 || $scope.zerocostview)) {
                    checkout_message1 += '"Click the check box to accept the Credit card agreement"<br>';
                }
                if (checkout_message1 !== '') {
                    MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                    setTimeout(function () {
                        $scope.preloader = false;
                        $scope.$apply();
                    }, 1000);
                    return false;
                }else{
                    return true;
                }
        };
        
        $scope.stripeSubmitForm = function(){
            if($scope.validateStripeForm()){ // Validate Stripe Checkout Form   
                $scope.payment_method_id = "";
                $scope.stripe.createPaymentMethod('card', $scope.stripe_card).then(function (result) {
                    if (result.error) {
                        setTimeout(function () {
                            $scope.preloader = false;
                            $scope.$apply();
                        }, 1000);
                        // Inform the user if there was an error. 
                        MyApp.alert(result.error.message, '');
                        $scope.valid_stripe_card = false;
                    } else { 
                    // Send paymentMethod.id to server 
                        (result.paymentMethod.id)? $scope.payment_method_id = result.paymentMethod.id : $scope.payment_method_id = "";
                        $scope.preloader = true;
                        $scope.membershipStripeCheckout();
                    }
                });
            }else{
                $scope.preloader = false;
            }
        };


        // STRIPE TOKEN CALL
        $scope.getStripeElements = function () {
            // Create a Stripe client
            $scope.valid_stripe_card = false;
            // Create an instance of Elements
            var elements = $scope.stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            var style = {
                base: {
                    color: 'rgb(74,74,74)',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: ($scope.mobiledevice)? '14px':'16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element
            $scope.stripe_card = elements.create('card', {style: style});

            // Add an instance of the card Element into the `card-element` <div>
            $scope.stripe_card.mount('#stripe-card-element');
            
            $scope.valid_stripe_card = false;
            // Handle real-time validation errors from the card Element.
            $scope.stripe_card.addEventListener('change', function (event) {
                var displayError = document.getElementById('stripe-card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                    $scope.valid_stripe_card = false;
                } else {
                    displayError.textContent = '';
                    $scope.valid_stripe_card = true;
                }
            });
        };
        
        // GET WEPAY STATUS
        $scope.getWepayStatus = function (cmp_id) {
            $http({
                method: 'GET',
                url: urlservice.url + 'getwepaystatus',
                params: {
                    "companyid": cmp_id,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user": $scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.wepaystatus = response.data.msg;
                    if($scope.wepaystatus !== 'N'){
                        $scope.wepay_enabled = true;
                    }
                } else {
                    $scope.wepaystatus = 'N';
                    $scope.wepay_enabled = false;
                }
                $scope.preloader = false;
                $scope.preloaderVisible = false;
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
                $scope.preloader = false;
                $scope.preloaderVisible = false;
            });
        };
        
        
        //GET MEMBERSHIP LIST CALL
        $scope.getMembershipList = function (cmp_id, mem_cat_id, mem_opt_id) {
            $scope.fav_company_name = '';
            $scope.company_id_status = $scope.membership_cat_id_status = $scope.membership_opt_id_status = false;
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            $http({
                method: 'GET',
                url: urlservice.url + 'membership',
                params: {
                    "companyid": cmp_id,
                    "category_id": mem_cat_id,
                    "option_id": mem_opt_id,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user": $scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U',
                    "page_from": "M"
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                    $scope.db_MScontent = "";
                    $scope.db_MSno = response.data.status;
                    $scope.db_MScontent = response.data.msg;
                    $scope.company_id = cmp_id;

                    $scope.logoUrl = response.data.company_details.logo_URL;
                    $scope.wp_level = response.data.company_details.wp_level;
                    $scope.wp_client_id = response.data.company_details.wp_client_id;
                    $scope.company_status = response.data.company_details.upgrade_status;
                    $scope.processing_fee_percentage = response.data.company_details.processing_fee_percentage;
                    $scope.processing_fee_transaction = response.data.company_details.processing_fee_transaction;
                    $scope.fav_company_name = response.data.company_details.company_name;
                    $scope.wp_currency_symbol = $scope.cursymbol = response.data.company_details.wp_currency_symbol;
                    $scope.wp_currency_code = response.data.company_details.wp_currency_code;
                    $scope.pos_settings_payment_method = response.data.company_details.pos_mem_payment_method;
                    //stripe set
                    $scope.stripe_upgrade_status = response.data.company_details.stripe_upgrade_status;
                    $scope.stripe_subscription = response.data.company_details.stripe_subscription;
                    $scope.stripe_publish_key = response.data.company_details.stripe_publish_key;
                    $scope.stripe_country_support = response.data.company_details.stripe_country_support;
                    $scope.getStripeStatus(cmp_id);
                    if ($scope.pos_user_type === 'S') {
                        $scope.getstudentlist(cmp_id);
                    }
                    $scope.initial_load_payment();
                    var Selcted_Opt_Index = '';

                    if (cmp_id !== '' && mem_cat_id === '' && mem_opt_id === '') {
                        $scope.db_MScontent = response.data.msg;
                        $scope.dynamic_mem_category_id = '';
                        $scope.dynamic_mem_option_id = '';
                        $scope.company_id_status = true;
                        $scope.membership_cat_id_status = false;
                        $scope.membership_opt_id_status = false;
                        // Load page:                                       
                        memberView.router.load({
                            pageName: 'index'
                        });

                    } else if (cmp_id !== '' && mem_cat_id !== '' && mem_opt_id === '') {
                        $scope.company_id_status = true;
                        $scope.membership_cat_id_status = true;
                        $scope.membership_opt_id_status = false;
                        $scope.dynamic_mem_category_id = mem_cat_id;
                        $scope.dynamic_mem_option_id = '';
                        $scope.db_MScontent = response.data.msg;
                        $scope.MembershipCategory_Details('', response.data.msg);
                    } else {
                        $scope.company_id_status = true;
                        $scope.membership_cat_id_status = true;
                        $scope.membership_opt_id_status = true;
                        $scope.dynamic_mem_category_id = mem_cat_id;
                        $scope.dynamic_mem_option_id = mem_opt_id;
                        $scope.db_MScontent = response.data.msg;
                        $scope.membershipcategory_id = $scope.db_MScontent.membership_id;
                        $scope.MS_CategoryTitle = $scope.db_MScontent.category_title;

                        if ($scope.db_MScontent.membership_options.length > 0) {
                            $scope.MembershipOptions_status = 'Success';
                            $scope.MembersipOptions_List = $scope.db_MScontent.membership_options;
                        } else {
                            $scope.MembershipOptions_status = 'Failure';
                            $scope.MembersipOptions_List = "";
                        }
                        for (var opt_index = 0; opt_index < $scope.MembersipOptions_List.length; opt_index++) {
                            if ($scope.MembersipOptions_List[opt_index].membership_option_id === mem_opt_id) {
                                Selcted_Opt_Index = opt_index;
                            }
                        }
                        if (Selcted_Opt_Index === '') {
                            MyApp.alert('Membership option details not available.', 'Message');
                            $scope.MembershipCategory_Details('', response.data.msg);
                        } else {
                            $scope.MembershipOptions_Description(Selcted_Opt_Index);
                        }
                    }

                } else {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                    $scope.db_MScontent = "";
                    $scope.db_MSno = response.data.status;
                    if (response.data.upgrade_status && response.data.studio_expiry_level) {
                        $scope.cmpny_email = response.data.studio_email;
                        $scope.company_status = response.data.upgrade_status;
                        $scope.company_expiry_level = response.data.studio_expiry_level;
                    }

                    if (($scope.company_status && $scope.company_status === 'F') || ($scope.company_expiry_level && $scope.company_expiry_level === 'L2')) {
                        // PREVENT FURTHUR EXECUTION & BLOCK THE APP
                        MyApp.modal({
                            title: '',
                            afterText:
                                    '<br>' +
                                    '<div id="studioExpiryText" class="modal-text custom-modal-content">Please contact the business operator to activate this link.</div>' +
                                    '<br>' +
                                    '<button class="button custom-modal-green-button button-text modalresetButton" id="send_expiry_email"></button>'
                        });

                        if ($scope.cmpny_email) {
                            $("#send_expiry_email").text("Send email message");
                        } else {
                            $("#send_expiry_email").text("Ok");
                        }

                        $('#send_expiry_email').click(function () {
                            $scope.$apply(function () {
                                if ($("#send_expiry_email").text() === 'Send email message') {
                                    $scope.sendExpiryEmailToStudio(cmp_id);
                                }
                                MyApp.closeModal();
                            });
                        });

                    } else {
                       if(response.data.session_status === "Expired"){
                            MyApp.alert('Session Expired','', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        } else {
                            MyApp.alert(response.data.msg, 'Message');
                        }
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };

        $scope.sendExpiryEmailToStudio = function () {
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            $http({
                method: 'POST',
                url: urlservice.url + 'sendExpiryEmailToStudio',
                data: {
                    "companyid": $scope.company_id,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user": $scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status == 'Success') {
                    MyApp.alert(response.data.msg, '');
                } else {
                    MyApp.alert(response.data.msg, 'Failure');
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };


        $scope.urlredirection = function () {
            $scope.param1 = $scope.param2 = $scope.param3 = $scope.param4 = '';
            var decoded_url = decodeURIComponent(window.location.href);
            var params = decoded_url.split('?=')[1].split('/');

            $scope.param1 = params[0];  //company code
            $scope.param2 = params[1];  //company id
            $scope.param3 = params[2];  //category id
            $scope.param4 = params[3];  //option id

            if (params[5]) {
                if (params[5] === 'spos') { //If staff
                    $scope.pos_user_type = 'S';
                    // ENABLE ADMIN BANNER, SEARCH MEMBER, OPTIONAL DISCOUNTS
                    $('#view-1').addClass('with-banner');
                } else {
                    $scope.pos_user_type = 'P'; //If public
                }
                if (params[6]) { //For access token
                    $scope.pos_token = params[6];
                }
                if (params[7]) {
                    $scope.pos_entry_type = params[7];
                }
                $('#view-1').addClass('from-pos with-footer');
            } else {
                $scope.pos_user_type = 'O';
                $scope.pos_token = $scope.pos_entry_type = '';
            }
//            alert('param1: '+$scope.param1+' param2: '+$scope.param2+' param3: '+$scope.param3+' param4: '+$scope.param4);

            if ($scope.param1 !== undefined && $scope.param2 !== undefined && ($scope.param3 === undefined || $scope.param3 === '') && $scope.param4 === undefined) {
                $scope.getMembershipList($scope.param2, '', '');
            } else if ($scope.param1 !== undefined && $scope.param2 !== undefined && $scope.param3 !== undefined && ($scope.param4 === undefined || $scope.param4 === '')) {
                if (isNaN($scope.param3)) {
                    $scope.getMembershipList($scope.param2, '', '');
                } else {
                    $scope.getMembershipList($scope.param2, $scope.param3, '');         // Is a number
                }
            } else if ($scope.param1 !== undefined && $scope.param2 !== undefined && $scope.param3 !== undefined && $scope.param4 !== undefined) {
                if (isNaN($scope.param4)) {
                    $scope.getMembershipList($scope.param2, $scope.param3, '');
                } else {
                    $scope.getMembershipList($scope.param2, $scope.param3, $scope.param4);         // Is a number
                }
            } else {
                window.location.href = window.location.origin;
            }

        };


        $scope.MembershipCategory_Details = function (Selcted_Index, category_details) {
            $scope.memCategoryDetails = '';
            if (Selcted_Index !== '') {
                $scope.memCategoryDetails = $scope.db_MScontent[Selcted_Index];
            } else {
                $scope.memCategoryDetails = category_details;
                // Load page:  
                memberView.router.load({
                    pageName: 'MembershipsCategory_Description'
                });
            }
            $scope.membershipcategory_id = $scope.memCategoryDetails.membership_id;
            $scope.MS_CategoryTitle = $scope.memCategoryDetails.category_title;
            $scope.MS_Category_img_url = $scope.memCategoryDetails.category_image_url;
            $scope.MS_Category_video_url = $scope.memCategoryDetails.category_video_url;
            $scope.MS_Category_desc_txt = $scope.memCategoryDetails.category_description;
            if ($scope.memCategoryDetails.membership_options.length > 0) {
                $scope.MembershipOptions_status = 'Success';
                $scope.MembersipOptions_List = $scope.memCategoryDetails.membership_options;
            } else {
                $scope.MembershipOptions_status = 'Failure';
                $scope.MembersipOptions_List = "";
            }

        };

        $scope.MembershipOptions_Description = function (Selcted_Index) {

            memberView.router.load({
                pageName: 'MembershipsOptions_Description'
            });
            $scope.MembershipOptions_Content = '';
            $scope.MembershipOptions_Content = $scope.MembersipOptions_List[Selcted_Index];
            $scope.current_membership_reg_columns = $scope.MembershipOptions_Content.reg_columns;
            $scope.membershipoption_id = $scope.MembershipOptions_Content.membership_option_id;
            $scope.membership_structure = $scope.MembershipOptions_Content.membership_structure;
            $scope.membership_structure_datepicker = $scope.MembershipOptions_Content.membership_structure;
            $scope.specific_start_date = $scope.MembershipOptions_Content.specific_start_date;
            $scope.specific_end_date = $scope.MembershipOptions_Content.specific_end_date;
            //  MEMBERSHIP WAIVER CONTENTS
            if ($scope.MembershipOptions_Content.waiver_policies === null || $scope.MembershipOptions_Content.waiver_policies === undefined || $scope.MembershipOptions_Content.waiver_policies === '') {
                $scope.MSoptions_waiver_text = '';
            } else {
                $scope.MSoptions_waiver_text = $scope.MembershipOptions_Content.waiver_policies;
            }

            //  MEMBERSHIP DISCOUNT ARRAY
            if ($scope.MembershipOptions_Content.membership_disc.length > 0) {
                $scope.MSOPT_DiscountList = $scope.MembershipOptions_Content.membership_disc;
                $scope.signupfee_Discode_exist = false;
                $scope.memfee_Discode_exist = false;
                for (var index = 0; index < $scope.MSOPT_DiscountList.length; index++) {
//                      DISCOUNT exists var to hide/show input box in discount popup  
                    if ($scope.MSOPT_DiscountList[index].discount_off === 'S') {
                        $scope.signupfee_Discode_exist = true;
                    } else if ($scope.MSOPT_DiscountList[index].discount_off === 'M') {
                        $scope.memfee_Discode_exist = true;
                    }
                }
            } else {
                $scope.MSOPT_DiscountList = '';
            }
            //Payment form validation purpose
            $scope.mems1_ccmonth = '';
            $scope.mems1_ccyear = '';
            $scope.mems1_country = '';

        };

        $scope.SetMembershipOptions_CartDetails = function () {
            $scope.membershipfee_DiscountAmount = $scope.signupfee_DiscountAmount = 0;
            $scope.signupfee_DiscountCode = $scope.membershipfee_DiscountCode = '';
            $scope.membershipoptiontitle = $scope.MembershipOptions_Content.membership_title;
            $scope.membershipoption_id = $scope.MembershipOptions_Content.membership_option_id;

            $scope.current_membership_manage = $scope.MembershipOptions_Content;

            $scope.show_in_app_flg = $scope.current_membership_manage.show_in_app_flg;
            if ($scope.show_in_app_flg === 'Y') {
                $scope.show_in_app = true;
            } else {
                $scope.show_in_app = false;
            }
            $scope.membership_structure = $scope.current_membership_manage.membership_structure;
            $scope.processing_fee_show = $scope.current_membership_manage.membership_processing_fee_type;
            $scope.memsignupfee = parseFloat($scope.current_membership_manage.membership_signup_fee);
            $scope.memfee = parseFloat($scope.current_membership_manage.membership_fee);
            $scope.mem_fee_include_status = $scope.current_membership_manage.initial_payment_include_membership_fee;

            $scope.mem_rec_frequency = $scope.current_membership_manage.membership_recurring_frequency;

            $scope.membership_structure = $scope.current_membership_manage.membership_structure;
            $scope.membership_billing_option = $scope.current_membership_manage.billing_options;
            $scope.membership_billing_startdate_type = $scope.current_membership_manage.billing_payment_start_date_type;
            $scope.membership_billing_startdate = $scope.current_membership_manage.billing_options_payment_start_date;
            $scope.membership_billing_deposit_amount = parseFloat($scope.current_membership_manage.deposit_amount);
            $scope.membership_no_of_payments = $scope.current_membership_manage.no_of_payments;
            $scope.membership_no_of_classes = parseFloat($scope.current_membership_manage.no_of_classes);
            $scope.membership_expiry_status = $scope.current_membership_manage.expiration_status;
            $scope.membership_expiry_period = $scope.current_membership_manage.expiration_date_type;
            $scope.membership_expiry_value = $scope.current_membership_manage.expiration_date_val;

            if ($scope.mem_rec_frequency === 'B' || $scope.mem_rec_frequency === 'M') {
                $scope.prorate_first_payment = $scope.current_membership_manage.prorate_first_payment_flg;
                $scope.cus_rec_freq_period_type = '';
                $scope.cus_rec_freq_period_val = '';
            } else if ($scope.mem_rec_frequency === 'C') {
                $scope.prorate_first_payment = 'N';
                $scope.cus_rec_freq_period_type = $scope.current_membership_manage.custom_recurring_frequency_period_type;
                $scope.cus_rec_freq_period_val = $scope.current_membership_manage.custom_recurring_frequency_period_val;
            } else {
                $scope.prorate_first_payment = 'N';
                $scope.cus_rec_freq_period_type = '';
                $scope.cus_rec_freq_period_val = '';
            }

            $scope.delay_rec_payment_start_flg = $scope.current_membership_manage.delay_recurring_payment_start_flg;
            if ($scope.delay_rec_payment_start_flg === 'Y') {
                $scope.delayed_recurring_payment_type = $scope.current_membership_manage.delayed_recurring_payment_type;
                $scope.delayed_recurring_payment_val = $scope.current_membership_manage.delayed_recurring_payment_val;
            } else {
                $scope.delayed_recurring_payment_type = '';
                $scope.delayed_recurring_payment_val = '';
            }

            $scope.specific_start_date = $scope.current_membership_manage.specific_start_date;
            $scope.specific_end_date = $scope.current_membership_manage.specific_end_date;
            $scope.specific_payment_frequency = $scope.current_membership_manage.specific_payment_frequency;
            $scope.billing_days = $scope.current_membership_manage.billing_days;
            $scope.exclude_from_billing_flag = $scope.current_membership_manage.exclude_from_billing_flag;
            if ($scope.exclude_from_billing_flag === 'Y') {
                if ($scope.current_membership_manage.mem_billing_exclude.length > 0) {
                    $scope.mem_billing_exclude_days = $scope.current_membership_manage.mem_billing_exclude;
                } else {
                    $scope.mem_billing_exclude_days = [];
                }
            } else {
                $scope.mem_billing_exclude_days = [];
            }


            $scope.payment_start_date_view = $scope.dateString($scope.payment_start_date);
            var dup_date = $scope.safariiosdate($scope.payment_start_date);
            var du_date = dup_date;
            $scope.actual_total_payment_startdate = du_date.getFullYear() + '-' + ('0' + (du_date.getMonth() + 1)).slice(-2) + '-' + ('0' + du_date.getDate()).slice(-2);

            if ($scope.membership_structure === 'SE') { // Has specific start and end date
                $scope.payment_end_date_view = $scope.dateString($scope.specific_end_date);
            } else {
                $scope.payment_end_date_view = "";
            }

            if ($scope.membership_structure === 'OE') {
                if ($scope.delay_rec_payment_start_flg === 'N') { //multiple recurring payment details
                    var dup = dup_date;
                    $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                } else {
                    if ($scope.delayed_recurring_payment_type === 'DM') {
                        dup_date = $scope.addMonths(dup_date, $scope.delayed_recurring_payment_val);
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else {
                        dup_date.setDate(dup_date.getDate() + (+$scope.delayed_recurring_payment_val * 7));
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }
                    $scope.delayed_recurring_payment_startdate = $scope.total_payment_startdate;
                    $scope.delayed_rec_payment_date = $scope.dateString($scope.total_payment_startdate);
                }
            } else if ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C') {

                if ($scope.membership_billing_option === 'PF') {
                    var dup = dup_date;
                    $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                } else if ($scope.membership_billing_option === 'PP') {
                    if ($scope.membership_billing_startdate_type === '1') {          //30 days after registration
                        var cus_curr_date = new Date();
                        cus_curr_date.setDate(cus_curr_date.getDate() + 30);
                        var dup = cus_curr_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else if ($scope.membership_billing_startdate_type === '2') {       //1 week after registration
                        var cus_curr_date = new Date();
                        cus_curr_date.setDate(cus_curr_date.getDate() + 7);
                        var dup = cus_curr_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else if ($scope.membership_billing_startdate_type === '3') {       //2 week after registration
                        var cus_curr_date = new Date();
                        cus_curr_date.setDate(cus_curr_date.getDate() + 14);
                        var dup = cus_curr_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else if ($scope.membership_billing_startdate_type === '4') {       //custom date selection
                        $scope.total_payment_startdate = $scope.membership_billing_startdate;
                        var cus_curr_date = new Date();
                        var cus_current_date = cus_curr_date.getFullYear() + '-' + ('0' + (cus_curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + cus_curr_date.getDate()).slice(-2);
                        if (new Date(cus_current_date) > new Date($scope.membership_billing_startdate)) {
                            MyApp.alert('Payment plan for this membership has already started, please contact studio for assistance.', ' ');
                            return false;
                        }
                    } else if ($scope.membership_billing_startdate_type === '5') {       //on membership start date
                        $scope.total_payment_startdate = $scope.actual_total_payment_startdate;
                    }

                }

            } else if ($scope.membership_structure === 'SE') {
                var dup = dup_date;
                $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
            }

            if (($scope.membership_structure === 'NC' || $scope.membership_structure === 'C') && $scope.membership_expiry_status === 'Y') {
                var dup_date = $scope.safariiosdate($scope.payment_start_date);
                if ($scope.membership_expiry_period === 'M') {
                    dup_date = $scope.addMonths(dup_date, $scope.membership_expiry_value);
                    var dup = dup_date;
                    $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                } else if ($scope.membership_expiry_period === 'W') {
                    dup_date.setDate(dup_date.getDate() + (+$scope.membership_expiry_value * 7));
                    var dup = dup_date;
                    $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                } else if ($scope.membership_expiry_period === 'Y') {
                    dup_date.setFullYear(dup_date.getFullYear() + +$scope.membership_expiry_value);
                    var dup = dup_date;
                    $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                }
            } else {
                $scope.membership_expiry_date = '';
            }


            $scope.current_membership_reg_columns = $scope.current_membership_manage.reg_columns;
            $("#column2").dateDropdowns({
                displayFormat: "mdy",
                required: true
            });
            if ($scope.current_membership_manage.waiver_policies === null) {
                $scope.waiverShow = false;
                $scope.waiverarea = '';
            } else {
                $scope.waiverShow = true;
                $scope.waiverarea = $scope.current_membership_manage.waiver_policies;
            }
            $scope.showMembershipTotal();
            //  LOAD PAGE:
            memberView.router.loadPage({
                pageName: 'MembershipsOptions_Cart'
            });
        };

        $scope.SetDefault_StartDate = function () {
            if ($scope.membership_structure_datepicker === 'SE') {
                var curr_date = new Date();
                var current_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2));
                var specific_start_date = $scope.newSEdateFormat($scope.specific_start_date);
                var specific_end_date = $scope.newSEdateFormat($scope.specific_end_date);
                if (+specific_start_date < +current_date) {
                    var curr_date = new Date();
                    $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);
                } else {
                    var curr_date = $scope.newSEdateFormat($scope.specific_start_date);
                    $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);
                }
                $scope.payment_end_date = specific_end_date.getFullYear() + '-' + ('0' + (specific_end_date.getMonth() + 1)).slice(-2) + '-' + ('0' + specific_end_date.getDate()).slice(-2);
            } else {
                var curr_date = new Date();
                $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);
            }

            var current_day = $scope.newSEdateFormat($scope.payment_start_date);
            var maxdate = $scope.membership_structure_datepicker === 'SE' ? $scope.newSEdateFormat($scope.payment_end_date) : null;
            $scope.setStartEndDate(current_day, maxdate);

            //  LOAD PAGE:
            memberView.router.loadPage({
                pageName: 'MembershipsOptions_DateSelection'
            });
        };

        var calendarInline;

        $scope.setStartEndDate = function (current_day, maxdate) {
            //  INITIALIZING THE MEMBERSHIP DATEPICKER 
            if (calendarInline !== undefined) {
                calendarInline.destroy();
                document.getElementById("calendar-inline-container").innerHTML = '';
            }

            var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var today = new Date(current_day);
            var yesterday = new Date(current_day);
            yesterday.setDate(yesterday.getDate() - 1);
            var enddate = maxdate;

            calendarInline = MyApp.calendar({
                container: '#calendar-inline-container',
//              minDate: yesterday,
                minDate: today,
                maxDate: enddate,
                value: [today],
                weekHeader: true,
                firstDay: 0,
                toolbarTemplate:
                        '<div class="toolbar calendar-custom-toolbar">' +
                        '<div class="toolbar-inner">' +
                        '<div class="left">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
                        '</div>' +
                        '<div class="center"></div>' +
                        '<div class="right">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
                        '</div>' +
                        '</div>' +
                        '</div>',
                onOpen: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                    $$('.calendar-custom-toolbar .left .link').on('click', function () {
                        calendarInline.prevMonth();

                    });
                    $$('.calendar-custom-toolbar .right .link').on('click', function () {
                        calendarInline.nextMonth();
                    });
                },
                onMonthYearChangeStart: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                },
                onDayClick: function (p, dayContainer, year, month, day) {
                    var selecteddate = new Date(year, month, day);
                    $scope.payment_start_date = year + '-' + ('0' + (parseInt(month) + parseInt(1))).slice(-2) + '-' + ('0' + day).slice(-2);
                }
            });
        };

        $$('#memsdiscountcode').on('click', function () {
            if ($scope.actual_membership_cost > 0 && $scope.memfee_Discode_exist) {
                var dismemmodelhtml = '<input placeholder="Discount Membership Cost" id="membershipfee_code" class="modal-text-input" style="text-align:center;" type="text">';
            } else {
                var dismemmodelhtml = ' ';
            }

            if ($scope.actual_signup_cost > 0 && $scope.signupfee_Discode_exist) {
                var dissignmodelhtml = '<input placeholder="Discount Sign-Up Fee" id="signupfee_code" class="modal-text-input"  style="text-align:center;" type="text">';
            } else {
                var dissignmodelhtml = ' ';
            }

            MyApp.modal({
//                  title:'Discount',
//                    bold:true,
//                    text:'Enter Discount Code',
                afterText: '<div class="modal-title">Enter Discount Code</div>' +
                        '<div class="modal-text"></div>' +
                        '<div class="input-field modal-input" style="margin-bottom:-5px;">' +
                        dissignmodelhtml +
                        '</div>' +
                        '<div class="input-field modal-input">' +
                        dismemmodelhtml +
                        '</div>',
                buttons: [
                    {
                        text: 'Cancel',
                        onClick: function () {
                            MyApp.closeModal();
                        }
                    },
                    {
                        text: 'Ok',
                        onClick: function () {
                            $scope.signupfee_Discode = $scope.membershipfee_Discode = '';
                            $scope.memfee_discount_applied_flag = $scope.signupfee_discount_applied_flag = 'N';
                            $scope.signupfee_Discode_Found = $scope.membershipfee_Discode_Found = false;
                            if ($scope.actual_signup_cost > 0 && $scope.signupfee_Discode_exist)
                                $scope.signupfee_Discode = document.getElementById("signupfee_code").value;
                            if ($scope.actual_membership_cost > 0 && $scope.memfee_Discode_exist)
                                $scope.membershipfee_Discode = document.getElementById("membershipfee_code").value;
                            $scope.MSOPT_DiscountCheck($scope.signupfee_Discode, $scope.membershipfee_Discode);
                        }
                    }
                ]
            });

        });
        $scope.MSOPT_DiscountCheck = function (signupfee_Discode, membershipfee_Discode) {
            $scope.memfee_discount_applied_flag = $scope.signupfee_discount_applied_flag = 'N';
            $scope.signupfee_afterdiscount = $scope.membershipfee_afterdiscount = 0;
            $scope.membershipfee_DiscountAmount = $scope.signupfee_DiscountAmount = 0;
            $scope.signupfee_Discode_Found = $scope.membershipfee_Discode_Found = false;
            if ((signupfee_Discode == '') && (membershipfee_Discode == '')) {
                $scope.memfee_discount_applied_flag = $scope.signupfee_discount_applied_flag = 'N';
                $scope.signupfee_Discode_Found = $scope.membershipfee_Discode_Found = false;
                $scope.signupfee_DiscountAmount = '';
                $scope.membershipfee_DiscountAmount = '';
                $scope.signupfee_afterdiscount = $scope.membershipfee_afterdiscount = 0;
                $scope.signupfee_DiscountCode = '';
                $scope.membershipfee_DiscountCode = '';
            } else {
                if (signupfee_Discode !== '') {
                    $scope.signupfee_discount_applied_flag = 'N';
                    $scope.signupfee_Discode_Found = false;
                    for (var index = 0; index < $scope.MSOPT_DiscountList.length; index++) {
//                      SIGNUP FEE DISCOUNT CHECK  
                        if (signupfee_Discode.toUpperCase() === $scope.MSOPT_DiscountList[index].discount_code.toUpperCase()) {
                            if ($scope.MSOPT_DiscountList[index].discount_off === 'S') {
                                $scope.signupfee_Discode_Found = true;
                                $scope.signupfee_discount_applied_flag = 'Y';
                                if ($scope.MSOPT_DiscountList[index].discount_type === 'V') {
                                    $scope.signupfee_DiscountAmount = parseFloat($scope.MSOPT_DiscountList[index].discount_amount);
                                    $scope.signupfee_afterdiscount = +$scope.memsignupfee - +$scope.signupfee_DiscountAmount;
                                } else {
                                    $scope.signupfee_DiscountAmount = +$scope.memsignupfee * (parseFloat($scope.MSOPT_DiscountList[index].discount_amount) / 100);
                                    $scope.signupfee_afterdiscount = +$scope.memsignupfee - +$scope.signupfee_DiscountAmount;
                                }
                            }
                        }
                    }
                    $scope.signupfee_DiscountCode = signupfee_Discode.toUpperCase();
                    if ($scope.signupfee_Discode_Found === false) {
                        $scope.signupfee_DiscountCode = '';
                        $scope.signupfee_discount_applied_flag = 'N';
                        MyApp.alert('Invalid Sign-up Discount Code : ' + signupfee_Discode, '');
                    }
                }

                if (membershipfee_Discode !== '') {
                    $scope.memfee_discount_applied_flag = 'N';
                    $scope.membershipfee_Discode_Found = false;
                    for (var index = 0; index < $scope.MSOPT_DiscountList.length; index++) {
//                      MEMBERSHIP FEE DISCOUNT CHECK  
                        if (membershipfee_Discode.toUpperCase() === $scope.MSOPT_DiscountList[index].discount_code.toUpperCase()) {
                            if ($scope.MSOPT_DiscountList[index].discount_off === 'M') {
                                $scope.membershipfee_Discode_Found = true;
                                $scope.memfee_discount_applied_flag = 'Y';
                                if ($scope.MSOPT_DiscountList[index].discount_type === 'V') {
                                    $scope.membershipfee_DiscountAmount = parseFloat($scope.MSOPT_DiscountList[index].discount_amount);
                                    $scope.membershipfee_afterdiscount = +$scope.register_membership_cost - +$scope.membershipfee_DiscountAmount;
                                } else {
                                    $scope.membershipfee_DiscountAmount = +$scope.register_membership_cost * (parseFloat($scope.MSOPT_DiscountList[index].discount_amount) / 100);
                                    $scope.membershipfee_afterdiscount = +$scope.register_membership_cost - +$scope.membershipfee_DiscountAmount;
                                }
                            }

                        }
                    }
                    $scope.membershipfee_DiscountCode = membershipfee_Discode.toUpperCase();
                    if ($scope.membershipfee_Discode_Found === false) {
                        $scope.membershipfee_DiscountCode = '';
                        $scope.memfee_discount_applied_flag = 'N';
                        MyApp.alert('Invalid Membership Discount Code : ' + membershipfee_Discode, '');
                    }
                }
                $scope.static_signup_discount = $scope.signupfee_DiscountAmount;
                $scope.static_membership_discount = $scope.membershipfee_DiscountAmount;
                $scope.total_payment_amount = +$scope.signupfee_afterdiscount + +$scope.membershipfee_afterdiscount;
                if (parseFloat($scope.total_payment_amount) > 5 || parseFloat($scope.total_payment_amount) == 0) {
                    $scope.final_total_payment_amount = $scope.total_payment_amount;
                    $scope.showMembershipTotal();
                    $scope.$apply();
                } else {
                    $scope.signupfee_DiscountCode = '';
                    $scope.membershipfee_DiscountCode = '';
                    $scope.signupfee_Discode_Found = $scope.membershipfee_Discode_Found = false;
                    $scope.memfee_discount_applied_flag = $scope.signupfee_discount_applied_flag = 'N';
                    MyApp.alert('Discount value exceeds the actual cost and must be atleast ' + $scope.cursymbol + '5', '');
                    return false;
                }

            }
        };

        //DATE TO READABLE FORMAT
        $scope.dateString = function (date) {
            if (date !== '0000-00-00' && date !== '' && date !== undefined) {
                var Displaydate = date.toString();
                Displaydate = date.split("-");
                var year = Displaydate[0];
                var month = Displaydate[1] - 1;
                var date = Displaydate[2];
                var ReadDate = new Date(year, month, date);
                var datesuccess = ReadDate.toString();
                var finaldateformat = datesuccess.split(" ");
                var finalized = finaldateformat[0] + " " + finaldateformat[1] + " " + finaldateformat[2] + " " + finaldateformat[3];
                return finalized;
            } else {
                return "";
            }
        };
        $scope.addMonths = function (adate, months) {
            var ad = adate.getDate();
            adate.setMonth(adate.getMonth() + +months);
            if (adate.getDate() != ad) {
                adate.setDate(0);
            }
            return adate;
        };

        $scope.safariiosdate = function (dat) {

            var dda = dat.toString();
            dda = dda.replace(/-/g, "/");
            var curr_date = new Date(dda);   //  FOR DATE FORMAT IN SAFARI 

            var given_start_year = curr_date.getFullYear();
            var given_start_month = curr_date.getMonth();
            var given_start_day = curr_date.getDate();
            var given_start_month = curr_date.getMonth() + 1;
            var dda = given_start_year + "-" + given_start_month + "-" + given_start_day;
            dda = dda.toString();
            dda = dda.replace(/-/g, "/");
            var dup_date = new Date(dda);  //  FOR DATE FORMAT IN SAFARI 

            return dup_date;
        }


        $scope.SEProcessingFee = function (recurring_amnt) {
            var process_fee_per = 0, process_fee_val = 0, recurring_pro_fee = 0;
            var process_fee_per = parseFloat($scope.processing_fee_percentage);
            var process_fee_val = parseFloat($scope.processing_fee_transaction);
            if (parseFloat(recurring_amnt) >= 0 && parseFloat(recurring_amnt) != '') {
                if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                    if (recurring_amnt > 0) {
                        var process_fee1 = +(parseFloat(recurring_amnt)) + +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                        if (isFinite(recurring_pro_fee) === false) {
                            recurring_pro_fee = 0;
                        }
                    } else {
                        recurring_pro_fee = 0;
                    }
                } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1') {
                    if (recurring_amnt > 0) {
                        var process_fee = +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                        if (isFinite(recurring_pro_fee) === false) {
                            recurring_pro_fee = 0;
                        }
                    } else {
                        recurring_pro_fee = 0;
                    }
                }
            }
            return recurring_pro_fee;
        };


        $scope.SEMembershipFee = function (count) {
            var SEMem_cost = 0;
            var selected_count = $scope.billing_days.match(/1/gi).length;
            if (parseFloat($scope.total_membership_amount_view) >= 0 && parseFloat($scope.total_membership_amount_view) != '') {
                if ($scope.specific_payment_frequency === 'PW' && parseFloat(count) > 0) {
                    SEMem_cost = (parseFloat($scope.total_membership_amount_view) / parseFloat(selected_count)) * parseFloat(count);
                } else if ($scope.specific_payment_frequency === 'PM' && parseFloat(count) > 0) {
                    SEMem_cost = (parseFloat($scope.total_membership_amount_view) / (parseFloat(selected_count) * 4.345)) * parseFloat(count);
                } else {
                    SEMem_cost = 0;
                }
            } else {
                SEMem_cost = 0;
            }

            return SEMem_cost;
        };


        $scope.startandenddate = function (start, end) {
            if (start !== undefined && start !== '' && end !== undefined && end !== '') {
                var check_start_date = start;
                var check_end_date = end;
                $scope.daysarray = $scope.billing_days.split('');
                $scope.checked_days_count = 0;
                for (var check_date = check_start_date; check_date <= check_end_date; check_date.setDate(check_date.getDate() + 1)) {
                    $scope.day_check = check_date.getDay() - 1;
                    if ($scope.day_check === -1) {
                        $scope.day_check = 6;
                    }
                    if ($scope.daysarray[$scope.day_check] === '1') {
                        $scope.checked_days_count += 1;
                    }
                }
                return $scope.checked_days_count;
            }
        };

        $scope.showMembershipTotal = function () {
            $scope.total_amount_view = $scope.total_reg_amount_view = 0;
            $scope.total_membership_amount_view = 0;
            $scope.total_mem_amount_view = 0;
            $scope.total_mem_amount_withfee_view = 0;
            $scope.total_signup_amount_view = 0;
            $scope.processing_fee_value = $scope.first_payment_amount = $scope.payment_amount = 0;
            $scope.recurring_processing_fee_value = $scope.delay_membership_amount_view = 0;
            $scope.final_membership_payment_amount = $scope.pay_infull_processingfee = 0;
            $scope.register_signup_discount_value = $scope.register_membership_discount_value = 0;
            $scope.recurring_payment_startdate_view = $scope.total_payment_startdate_view = '';
            $scope.membership_discount_show = $scope.signup_discount_show = true;
            $scope.zerocostview = true;
            $scope.first_payment_status = false;

            var process_fee_per = 0, process_fee_val = 0;
            var process_fee_per = $scope.processing_fee_percentage;
            var process_fee_val = $scope.processing_fee_transaction;

            if (parseFloat($scope.current_membership_manage.membership_signup_fee) > 0) {
                $scope.actual_signup_cost = parseFloat($scope.current_membership_manage.membership_signup_fee);
                $scope.register_signup_cost = $scope.actual_signup_cost;
            } else {
                $scope.actual_signup_cost = 0;
                $scope.register_signup_cost = 0;
                $scope.signup_discount_show = false;
            }

            if (parseFloat($scope.current_membership_manage.membership_fee) > 0) {
                $scope.actual_membership_cost = parseFloat($scope.current_membership_manage.membership_fee);
                $scope.register_membership_cost = $scope.actual_membership_cost;
            } else {
                $scope.actual_membership_cost = 0;
                $scope.register_membership_cost = 0;
                $scope.membership_discount_show = false;
            }
            $scope.current_processing_fee_type = $scope.current_membership_manage.membership_processing_fee_type;
            if ($scope.signupfee_DiscountAmount > 0) {
                $scope.total_signup_amount_view = (Math.round((+$scope.register_signup_cost - +$scope.signupfee_DiscountAmount + +0.00001) * 100) / 100).toFixed(2);
                $scope.register_signup_discount_value = $scope.signupfee_DiscountAmount;
            } else {
                $scope.total_signup_amount_view = (Math.round((+$scope.register_signup_cost + +0.00001) * 100) / 100).toFixed(2);
                $scope.register_signup_discount_value = 0;
            }

            if ($scope.membershipfee_DiscountAmount > 0) {
                $scope.total_membership_amount_view = +$scope.register_membership_cost - +$scope.membershipfee_DiscountAmount;
                $scope.register_membership_discount_value = $scope.membershipfee_DiscountAmount;
            } else {
                $scope.total_membership_amount_view = +$scope.register_membership_cost;
                $scope.register_membership_discount_value = 0;
            }
            $scope.total_mem_amount_view = $scope.total_membership_amount_view;
            var amount = $scope.total_mem_amount_view;
            if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                if (amount > 0) {
                    var process_fee1 = +amount + +((+amount * +process_fee_per) / 100) + +process_fee_val;
                    var process_fee = +((+process_fee1 * +process_fee_per) / 100) + +process_fee_val;
                    $scope.recurring_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    $scope.recurring_processing_fee_value = 0;
                }
            } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1') {
                if (amount > 0) {
                    var process_fee = +((+amount * +process_fee_per) / 100) + +process_fee_val;
                    $scope.recurring_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    $scope.recurring_processing_fee_value = 0;
                }
            }


            if ($scope.membership_structure === 'OE') {
                var dup_date = $scope.safariiosdate($scope.total_payment_startdate);
                var act_date = $scope.safariiosdate($scope.actual_total_payment_startdate);
                $scope.prorate_includes_recurr_note = false;
                $scope.first_payment_status = false;
                if ($scope.prorate_first_payment === 'Y' && $scope.mem_rec_frequency === 'M') {
                    if ($scope.delay_rec_payment_start_flg === 'Y') {
                        var ld = $scope.safariiosdate($scope.total_payment_startdate);
                        var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                        $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / lastDay.getDate()) * (+lastDay.getDate() + 1 - ld.getDate());
                        $scope.total_membership_amount_view = 0;
                        $scope.total_reg_amt_for_check = +$scope.delay_membership_amount_view;
                        $scope.first_payment_amount = 0;
                        dup_date = $scope.addMonths(dup_date, 1);
                        var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                        $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else {
                        var ld = $scope.safariiosdate($scope.actual_total_payment_startdate);
                        var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                        $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / lastDay.getDate()) * (+lastDay.getDate() + 1 - ld.getDate());
                        if ($scope.mem_fee_include_status === 'N') {
                            $scope.first_payment_status = true;
                            $scope.first_payment_amount = parseFloat($scope.total_membership_amount_view);
                            $scope.first_payment_startdate = $scope.dateString($scope.actual_total_payment_startdate);
                        } else {
                            $scope.first_payment_status = false;
                            $scope.first_payment_amount = 0;
                            $scope.first_payment_startdate = '';
                        }
                        $scope.total_reg_amt_for_check = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                        if ($scope.total_reg_amt_for_check < 5) {
                            $scope.prorate_includes_recurr_note = true;
                            $scope.total_membership_amount_view = +$scope.total_membership_amount_view + +$scope.total_mem_amount_view;
                            if ($scope.mem_fee_include_status === 'N') {
                                dup_date = $scope.addMonths(dup_date, 1);
                                var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                            } else {
                                dup_date = $scope.addMonths(dup_date, 2);
                                var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                            }
                        } else {
                            dup_date = $scope.addMonths(dup_date, 1);
                            var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                            $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }
                    }

                } else if ($scope.prorate_first_payment === 'Y' && $scope.mem_rec_frequency === 'B') {
                    if ($scope.delay_rec_payment_start_flg === 'Y') {
                        if (dup_date.getDate() < 16) {
                            $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / 15) * (+16 - act_date.getDate());
                            $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16';
                        } else {
                            var ld = $scope.safariiosdate($scope.total_payment_startdate);
                            var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                            if (dup_date.getDate() == 16)
                                $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view));
                            else
                                $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / (lastDay.getDate() - 15)) * (+lastDay.getDate() + 1 - act_date.getDate());

                            dup_date = $scope.addMonths(dup_date, 1);
                            var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                            $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }
                        $scope.total_membership_amount_view = 0;
                        $scope.first_payment_amount = 0;
                    } else {
                        if (act_date.getDate() < 16) {
                            $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / 15) * (+16 - act_date.getDate());
                            if ($scope.mem_fee_include_status === 'N') {
                                $scope.first_payment_status = true;
                                $scope.first_payment_amount = parseFloat($scope.total_membership_amount_view);
                                $scope.first_payment_startdate = $scope.dateString($scope.actual_total_payment_startdate);
                            } else {
                                $scope.first_payment_status = false;
                                $scope.first_payment_amount = 0;
                                $scope.first_payment_startdate = '';
                            }
                        } else {
                            var ld = $scope.safariiosdate($scope.actual_total_payment_startdate);
                            var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                            if (dup_date.getDate() == 16)
                                $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view));
                            else
                                $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / (lastDay.getDate() - 15)) * (+lastDay.getDate() + 1 - act_date.getDate());
                            if ($scope.mem_fee_include_status === 'N') {
                                $scope.first_payment_status = true;
                                $scope.first_payment_amount = parseFloat($scope.total_membership_amount_view);
                                $scope.first_payment_startdate = $scope.dateString($scope.actual_total_payment_startdate);
                            } else {
                                $scope.first_payment_status = false;
                                $scope.first_payment_amount = 0;
                                $scope.first_payment_startdate = '';
                            }
                        }

                        if (dup_date.getDate() < 16) {
                            $scope.total_reg_amt_for_check = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                            if ($scope.total_reg_amt_for_check < 5) {
                                $scope.prorate_includes_recurr_note = true;
                                $scope.total_membership_amount_view = +$scope.total_membership_amount_view + +$scope.total_mem_amount_view;
                                if ($scope.mem_fee_include_status === 'N') {
                                    $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16';
                                } else {
                                    dup_date = $scope.addMonths(dup_date, 1);
                                    var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                                }
                            } else {
                                $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16';
                            }
                        } else {
                            $scope.total_reg_amt_for_check = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                            if ($scope.total_reg_amt_for_check < 5) {
                                $scope.prorate_includes_recurr_note = true;
                                $scope.total_membership_amount_view = +$scope.total_membership_amount_view + +$scope.total_mem_amount_view;
                                if ($scope.mem_fee_include_status === 'N') {
                                    dup_date = $scope.addMonths(dup_date, 1);
                                    var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                                } else {
                                    dup_date = $scope.addMonths(dup_date, 1);
                                    var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-16';
                                }
                            } else {
                                dup_date = $scope.addMonths(dup_date, 1);
                                var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                            }
                        }
                    }
                }

                var cus_curr_date = new Date();
                var cus_current_date = cus_curr_date.getFullYear() + '-' + ('0' + (cus_curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + cus_curr_date.getDate()).slice(-2);
                if ($scope.mem_rec_frequency === 'A') {
                    dup_date.setFullYear(dup_date.getFullYear() + 1);
                    var dup = dup_date;
                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    } else {
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'W') {
                    dup_date.setDate(dup_date.getDate() + (+7));
                    var dup = dup_date;
                    //                dup.setDate(dup.getDate() + (7 - dup.getDay()) % 7 + 1);
                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    } else {
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'C') {
                    if ($scope.cus_rec_freq_period_type === 'CM') {
                        dup_date = $scope.addMonths(dup_date, $scope.cus_rec_freq_period_val);
                        var dup = dup_date;
                        $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else {
                        dup_date.setDate(dup_date.getDate() + (+$scope.cus_rec_freq_period_val * 7));
                        var dup = dup_date;
                        $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    } else {
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'B' && $scope.prorate_first_payment !== 'Y') {
                    if (dup_date.getDate() < 16) {
                        $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16';
                    } else {
                        var dup_date = $scope.addMonths(dup_date, 1);
                        var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                        $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    } else {
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'M' && $scope.prorate_first_payment !== 'Y') {
                    dup_date = $scope.addMonths(dup_date, 1);
                    $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-' + ('0' + dup_date.getDate()).slice(-2);
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    } else {
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }

                } else if ($scope.mem_rec_frequency === 'N') {
                    $scope.recurring_payment_startdate = '';
                    if ($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    } else {
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                }


                if ($scope.mem_rec_frequency !== 'N')
                    $scope.recurring_payment_startdate_view = $scope.dateString($scope.recurring_payment_startdate);
                $scope.total_payment_startdate_view = $scope.dateString($scope.total_payment_startdate);


                if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                    $scope.total_membership_amount_view = 0;
                }

            } else if ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C') {
                var dup_date = $scope.safariiosdate($scope.total_payment_startdate);
                var act_date = $scope.safariiosdate($scope.actual_total_payment_startdate);
                var number_of_payments = +$scope.membership_no_of_payments;
                var intervalType = $scope.mem_rec_frequency;
                var billing_option = $scope.membership_billing_option;
                var recurring_amnt = (+$scope.total_membership_amount_view - +$scope.membership_billing_deposit_amount) / (+$scope.membership_no_of_payments);
                var recurring_pro_fee;
                if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                    if (recurring_amnt > 0) {
                        var process_fee1 = +(parseFloat(recurring_amnt)) + +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        recurring_pro_fee = 0;
                    }
                } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1') {
                    if (recurring_amnt > 0) {
                        var process_fee = +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        recurring_pro_fee = 0;
                    }
                }

                $scope.pay = [];
                $scope.recurrent = {};

                if (parseFloat(recurring_amnt) > 0 && billing_option === 'PP') {
                    if (intervalType === 'M') {             //monthly
                        for (var i = 0; i < number_of_payments; i++) {
                            $scope.recurrent = {};
                            if (i === 0) {
                                $scope.recurrent['date'] = $scope.total_payment_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            } else {
                                dup_date = $scope.addMonths(dup_date, 1);
                                var date = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-' + ('0' + dup_date.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            }
                        }
                    } else if (intervalType === 'W') {           //weekly
                        for (var i = 0; i < number_of_payments; i++) {
                            $scope.recurrent = {};
                            if (i === 0) {
                                $scope.recurrent['date'] = $scope.total_payment_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            } else {
                                dup_date.setDate(dup_date.getDate() + (+7));
                                var dup = dup_date;
                                var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            }
                        }
                    } else if (intervalType === 'B') {           //Bi-weekly
                        for (var i = 0; i < number_of_payments; i++) {
                            $scope.recurrent = {};
                            if (i === 0) {
                                $scope.recurrent['date'] = $scope.total_payment_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            } else {
                                dup_date.setDate(dup_date.getDate() + (+14));
                                var dup = dup_date;
                                var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            }
                        }
                    }
                    if ($scope.pay.length > 0) {
                        $scope.recurring_payment_startdate = $scope.pay[0].date;
                        $scope.total_mem_amount_view = $scope.pay[0].amount;
                    }
                } else {
                    $scope.pay = [];
                    $scope.recurring_payment_startdate = '';
                }
            } else if ($scope.membership_structure === 'SE') {
                var dup_date = $scope.safariiosdate($scope.total_payment_startdate);
                var act_date = $scope.safariiosdate($scope.actual_total_payment_startdate);
                var specific_payment_frequency = $scope.specific_payment_frequency;
                var billing_option = $scope.membership_billing_option;
                $scope.TotalMembershipFeeSE = 0;

                $scope.pay = [];
                $scope.recurrent = {};

                if (specific_payment_frequency === 'PM') {             //per monthly
                    var membership_firstDay = $scope.newSEdateFormat($scope.payment_start_date);
                    var membership_lastDay = $scope.newSEdateFormat($scope.specific_end_date);
                    var se_amount = 0;
                    var se_processing_fee = 0;
                    $scope.month_array = [];
                    $scope.total_count_array = [];
                    for (date = membership_firstDay; date <= membership_lastDay; date.setMonth(date.getMonth() + 1, 1)) {
                        var count = 0, exclude_count = 0;
                        var y = date.getFullYear(), m = date.getMonth(), da = date.getDate(), hr = date.getHours(), min = date.getMinutes();
                        var startdate = new Date(y, m, da, hr, min);
                        var enddate = new Date(y, m + 1, 0, hr, min);
                        if (membership_lastDay < enddate) {
                            enddate = membership_lastDay;
                        }
                        count = $scope.startandenddate(startdate, enddate);
                        $scope.total_count_array.push(count);

                        if ($scope.exclude_from_billing_flag === 'Y' && $scope.mem_billing_exclude_days.length > 0) {
                            for (i = 0; i < $scope.mem_billing_exclude_days.length; i++) {
                                var final_exclude_startdate = "";
                                var final_exclude_enddate = "";
                                var exclude_start_date = $scope.newSEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_startdate);
                                var exclude_end_date = $scope.newSEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_enddate);
                                if ((exclude_start_date.getMonth() === date.getMonth()) && (exclude_start_date.getFullYear() === date.getFullYear())) {
                                    var y = exclude_start_date.getFullYear(), m = exclude_start_date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                                    var end_of_month = new Date(y, m + 1, 0, hr, min);
                                    if (date <= exclude_start_date || date <= exclude_end_date) {
                                        if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                            var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                            final_exclude_startdate = new Date(y, m, day, hr, min);
                                            if (exclude_end_date > end_of_month) {
                                                final_exclude_enddate = end_of_month;
                                            } else {
                                                final_exclude_enddate = exclude_end_date;
                                            }
                                        } else {
                                            final_exclude_startdate = exclude_start_date;
                                            if (exclude_end_date > end_of_month) {
                                                final_exclude_enddate = end_of_month;
                                            } else {
                                                final_exclude_enddate = exclude_end_date;
                                            }
                                        }
                                    }
                                } else if ((exclude_end_date.getMonth() === date.getMonth()) && (exclude_end_date.getFullYear() === date.getFullYear())) {
                                    if (date <= exclude_end_date || date <= exclude_start_date) {
                                        if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                            var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                            final_exclude_startdate = new Date(y, m, day, hr, min);
                                            final_exclude_enddate = exclude_end_date;
                                        } else {
                                            var y = exclude_end_date.getFullYear(), m = exclude_end_date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                                            var startdate_of_month = new Date(y, m, 1, hr, min);
                                            final_exclude_enddate = exclude_end_date;
                                            if (exclude_start_date < startdate_of_month) {
                                                final_exclude_startdate = startdate_of_month;
                                            } else {
                                                final_exclude_startdate = exclude_start_date;
                                            }
                                        }
                                    }
                                } else if (((exclude_start_date.getMonth() < date.getMonth() && exclude_start_date.getFullYear() === date.getFullYear()) || (exclude_start_date.getFullYear() < date.getFullYear())) && ((exclude_end_date.getMonth() > date.getMonth() && exclude_end_date.getFullYear() === date.getFullYear()) || (exclude_end_date.getFullYear() > date.getFullYear()))) {
                                    var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                    if (date > exclude_start_date && date < exclude_end_date) {//for date picking in registration
                                        final_exclude_startdate = new Date(y, m, day, hr, min);
                                        final_exclude_enddate = new Date(y, m + 1, 0, hr, min);
                                    } else {
                                        final_exclude_startdate = new Date(y, m, 1, hr, min);
                                        final_exclude_enddate = new Date(y, m + 1, 0, hr, min);
                                    }
                                }
                                if (final_exclude_startdate !== '' && final_exclude_enddate !== '') {
                                    exclude_count = +exclude_count + +$scope.startandenddate(final_exclude_startdate, final_exclude_enddate);

                                }
                            }
                            $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1] - +exclude_count;
                            count = $scope.total_count_array[$scope.total_count_array.length - 1];
                        } else {
                            $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1];
                        }
                        var se_start_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                        $scope.recurrent = {};
                        se_amount = $scope.SEMembershipFee(count);
                        se_processing_fee = $scope.SEProcessingFee(se_amount);
                        $scope.recurrent['date'] = se_start_date;
                        $scope.recurrent['amount'] = se_amount;
                        $scope.recurrent['type'] = 'R';
                        $scope.recurrent['processing_fee'] = se_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    }
                } else if (specific_payment_frequency === 'PW') {           //per weekly
                    var membership_firstDay = $scope.newSEdateFormat($scope.payment_start_date);
                    var membership_lastDay = $scope.newSEdateFormat($scope.specific_end_date);
                    var se_amount = 0, se_processing_fee = 0;
                    $scope.week_array = [];
                    $scope.total_count_array = [];
                    for (date = membership_firstDay; date <= membership_lastDay; date.setDate(date.getDate() + 7)) {
                        var start_date = "";
                        var end_date, da;
                        var count, exclude_count = 0;
                        var first = date.getDate() - (date.getDay() - 1); // start of the week
                        var last = first + 6;//last day of week  
                        var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                        var firstday_of_week = new Date(y, m, first, hr, min);
                        var lastday_of_week = new Date(y, m, last, hr, min);
                        if (date <= membership_firstDay) {
                            start_date = membership_firstDay;
                        } else {
                            start_date = firstday_of_week;
                        }
                        if (lastday_of_week >= membership_lastDay) {
                            end_date = membership_lastDay;
                        } else {
                            end_date = lastday_of_week;
                        }
                        var y = start_date.getFullYear(), m = start_date.getMonth(), da = start_date.getDate(), shr = date.getHours(), smin = date.getMinutes();
                        var startdate = new Date(y, m, da, shr, smin);
                        var ey = end_date.getFullYear(), em = end_date.getMonth(), eda = end_date.getDate(), ehr = date.getHours(), emin = date.getMinutes();
                        var enddate = new Date(ey, em, eda, ehr, emin);
                        count = $scope.startandenddate(startdate, enddate);
                        $scope.total_count_array.push(count);

                        if ($scope.exclude_from_billing_flag === 'Y' && $scope.mem_billing_exclude_days.length > 0) {
                            for (i = 0; i < $scope.mem_billing_exclude_days.length; i++) {
                                var exclude_start_date = $scope.newSEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_startdate);
                                var exclude_end_date = $scope.newSEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_enddate);
                                var final_exclude_startdate = "";
                                var final_exclude_enddate = "";
                                if (date <= exclude_end_date || date <= exclude_start_date) {
                                    if (exclude_start_date >= firstday_of_week && exclude_start_date <= lastday_of_week) {
                                        if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                            var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                            final_exclude_startdate = new Date(y, m, day, hr, min);
                                            if (exclude_end_date > lastday_of_week) {
                                                final_exclude_enddate = lastday_of_week;
                                            } else {
                                                final_exclude_enddate = exclude_end_date;
                                            }
                                        } else {
                                            final_exclude_startdate = exclude_start_date;
                                            if (exclude_end_date > lastday_of_week) {
                                                final_exclude_enddate = lastday_of_week;
                                            } else {
                                                final_exclude_enddate = exclude_end_date;
                                            }
                                        }
                                    } else if (exclude_end_date >= firstday_of_week && exclude_end_date <= lastday_of_week) {
                                        if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                            var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                            final_exclude_startdate = new Date(y, m, day, hr, min);
                                            final_exclude_enddate = exclude_end_date;
                                        } else {
                                            final_exclude_enddate = exclude_end_date;
                                            if (exclude_start_date > firstday_of_week) {
                                                final_exclude_startdate = exclude_start_date;
                                            } else {
                                                final_exclude_startdate = firstday_of_week;
                                            }
                                        }
                                    } else if (exclude_start_date < firstday_of_week && exclude_end_date > lastday_of_week) {
                                        if (date > exclude_start_date && date < exclude_end_date) {//for date picking in registration
                                            var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                            final_exclude_startdate = new Date(y, m, day, hr, min);
                                            final_exclude_enddate = lastday_of_week;
                                        } else {
                                            final_exclude_startdate = firstday_of_week;
                                            final_exclude_enddate = lastday_of_week;

                                        }
                                    }
                                }
                                if (final_exclude_startdate !== '' && final_exclude_enddate !== '') {
                                    exclude_count = parseInt(exclude_count) + parseInt($scope.startandenddate(final_exclude_startdate, final_exclude_enddate));
                                }
                            }
                            $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1] - +exclude_count;
                            count = $scope.total_count_array[$scope.total_count_array.length - 1];
                        } else {
                            $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1];
                        }
                        var se_start_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                        if (date.getDay() !== 1) {
                            date = firstday_of_week;
                        }
                        $scope.recurrent = {};
                        se_amount = $scope.SEMembershipFee(count);
                        se_processing_fee = $scope.SEProcessingFee(se_amount);
                        $scope.recurrent['date'] = se_start_date;
                        $scope.recurrent['amount'] = se_amount;
                        $scope.recurrent['type'] = 'R';
                        $scope.recurrent['processing_fee'] = se_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    }
                }

                for (i = 0; i < $scope.pay.length; i++) {//wepay less than 5 calculation
                    $scope.pay[i].prorate_flag = "F";
                    while ($scope.pay[i].amount < 5) {
                        if ($scope.pay[i].amount === 0) {
                            $scope.pay.splice(i, 1);
                            i--;
                            break;
                        }
                        var j = i + 1;
                        if ($scope.pay[j] === undefined)
                            break;
                        $scope.pay[i].amount = $scope.pay[i].amount + $scope.pay[j].amount;
                        $scope.pay[i].processing_fee = $scope.SEProcessingFee($scope.pay[i].amount);
                        $scope.pay[i].prorate_flag = "T";
                        $scope.pay.splice(j, 1);
                    }
                }

                if (specific_payment_frequency === 'PM' && $scope.pay.length > 0) {  //first and last dates prorate check  
                    var first_date = $scope.newSEdateFormat($scope.pay[0].date);
                    var last_date = $scope.newSEdateFormat($scope.specific_end_date);
                    var ye = last_date.getFullYear(), me = last_date.getMonth(), hre = last_date.getHours(), mine = last_date.getMinutes();
                    var end_of_months = new Date(ye, me + 1, 0, hre, mine);
                    if ($scope.pay[0].prorate_flag !== "T") {
                        $scope.pay[0].prorate_flag = (first_date.getDate() === 1) ? "F" : "T";
                    }
                    if ($scope.pay[$scope.pay.length - 1].prorate_flag !== "T" && $scope.pay.length > 1) {
                        $scope.pay[$scope.pay.length - 1].prorate_flag = (last_date.getDate() === end_of_months.getDate()) ? "F" : "T";
                    }
                } else if (specific_payment_frequency === 'PW' && $scope.pay.length > 0) {
                    var first_date = $scope.newSEdateFormat($scope.pay[0].date);
                    var last_date = $scope.newSEdateFormat($scope.specific_end_date);
                    var checkprorate_start, checkprorate_end = '';
                    $scope.selected_days = $scope.billing_days.split('');
                    if ($scope.pay[0].prorate_flag !== "T") {    //week start
                        for (i = 0; i < $scope.selected_days.length; i++) {
                            if ($scope.selected_days[i] === "1") {
                                checkprorate_start = i;
                                break;
                            }
                        }
                        $scope.days_check_start = first_date.getDay() - 1;
                        $scope.days_check_start = ($scope.days_check_start === -1) ? '6' : $scope.days_check_start;
                        $scope.pay[0].prorate_flag = (checkprorate_start === $scope.days_check_start) ? "F" : "T";
                    }
                    if ($scope.pay[$scope.pay.length - 1].prorate_flag !== "T" && $scope.pay.length > 1) {     //week end
                        for (j = $scope.selected_days.length - 1; j >= 0; j--) {
                            if ($scope.selected_days[j] === "1") {
                                checkprorate_end = j;
                                break;
                            }
                        }
                        $scope.days_check_last = last_date.getDay() - 1;
                        $scope.days_check_last = ($scope.days_check_last === -1) ? '6' : $scope.days_check_last;
                        $scope.pay[$scope.pay.length - 1].prorate_flag = (checkprorate_end === $scope.days_check_last) ? "F" : "T";
                    }
                }

                if (parseInt($scope.pay.length) > 1 && $scope.pay !== undefined && $scope.pay !== '') {
                    if ($scope.pay[$scope.pay.length - 1].amount < 5) {
                        $scope.pay[$scope.pay.length - 2].amount += $scope.pay[$scope.pay.length - 1].amount;
                        $scope.pay[$scope.pay.length - 2].processing_fee = $scope.SEProcessingFee($scope.pay[$scope.pay.length - 2].amount);
                        $scope.pay[$scope.pay.length - 2].prorate_flag = "T";
                        $scope.pay.splice($scope.pay.length - 1, 1);
                    }
                }

                var se_mem_amount = 0;
                for (i = 0; i < $scope.pay.length; i++) {
                    se_mem_amount = parseFloat(se_mem_amount) + parseFloat($scope.pay[i].amount);
                }
                $scope.TotalMembershipFeeSE = se_mem_amount;
                if (billing_option === 'PF') {
                    $scope.total_mem_amount_view = se_mem_amount;
                    $scope.pay = [];
                    $scope.recurring_payment_startdate = '';
                }

                if ($scope.pay.length > 0) {
                    $scope.recurring_payment_startdate = $scope.pay[0].date;
                    $scope.total_mem_amount_view = $scope.pay[0].amount;
                } else {
                    $scope.pay = [];
                    $scope.recurring_payment_startdate = '';
                }
            }

            if ($scope.total_membership_amount_view > 0 || $scope.total_signup_amount_view > 0) {
                $scope.total_amount_view = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                $scope.total_reg_amount_view = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
            } else {
                $scope.membership_discount_show = false;
                $scope.signup_discount_show = false;
            }

            if ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C') {
                var curr_date = new Date();
                var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2));
                var payment_date = new Date($scope.total_payment_startdate);
                var actual_payment_date = new Date($scope.actual_total_payment_startdate);
                if ($scope.membership_billing_option === 'PP' && $scope.pay.length > 0) {
                    if ((+actual_payment_date === +payment_date) && (+server_curr_date === +actual_payment_date)) {
                        $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.membership_billing_deposit_amount + parseFloat($scope.total_mem_amount_view);
                        var amount = $scope.total_amount_view;
                        $scope.final_membership_payment_amount = amount;
                        $scope.pay.shift();
                        $scope.recurring_payment_startdate = ($scope.pay.length > 0) ? $scope.pay[0].date : "";
                    } else {
                        $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.membership_billing_deposit_amount;
                        var amount = $scope.total_amount_view;
                        $scope.final_membership_payment_amount = amount;
                    }
                } else {
                    var amount = +$scope.total_amount_view;
                    $scope.final_membership_payment_amount = amount;
                }
            } else if ($scope.membership_structure === 'SE') {
                var curr_date = new Date();
                var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2));
                var payment_date = new Date($scope.total_payment_startdate);
                var actual_payment_date = new Date($scope.actual_total_payment_startdate);
                if ($scope.membership_billing_option === 'PP' && $scope.pay.length > 0) {
                    if ((+actual_payment_date === +payment_date) && (+server_curr_date === +actual_payment_date)) {
                        $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.pay[0].amount;
                        var amount = $scope.total_amount_view;
                        $scope.final_membership_payment_amount = amount;
                        $scope.pay.shift();
                    } else {
                        if ($scope.mem_fee_include_status === 'Y') {
                            $scope.total_amount_view = +$scope.total_signup_amount_view + $scope.pay[0].amount;
                            $scope.pay.splice(0, 1);
                            var amount = $scope.total_amount_view;
                            $scope.final_membership_payment_amount = amount;
                        } else {
                            $scope.total_amount_view = +$scope.total_signup_amount_view;
                            var amount = $scope.total_amount_view;
                            $scope.final_membership_payment_amount = amount;
                        }
                    }
                    $scope.recurring_payment_startdate = ($scope.pay.length > 0) ? $scope.pay[0].date : "";
                } else {
                    if ($scope.mem_fee_include_status === 'N' && (new Date(server_curr_date) < new Date($scope.actual_total_payment_startdate))) {   // Membership fee doesnot include on first payment
                        $scope.total_amount_view = +$scope.total_signup_amount_view;
                        var amount = $scope.total_amount_view;
                        $scope.final_membership_payment_amount = amount;
                        $scope.membership_fee_include = $scope.total_mem_amount_view;
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    } else {
                        $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.total_mem_amount_view;
                        var amount = $scope.total_amount_view;
                        $scope.final_membership_payment_amount = amount;
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                }
            } else {
                var amount = +$scope.total_amount_view;
                $scope.final_membership_payment_amount = amount;
            }
            // pos
            if ($scope.membership_payment_method === 'CC') {
                if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                    if (amount > 0) {
                        var process_fee1 = +amount + +((+amount * +process_fee_per) / 100) + +process_fee_val;
                        var process_fee = +((+process_fee1 * +process_fee_per) / 100) + +process_fee_val;
                        $scope.processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.processing_fee_value = 0;
                    }
                    $scope.total_amount_view = (Math.round((+amount + +$scope.processing_fee_value + +0.00001) * 100) / 100).toFixed(2);
                } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1') {
                    if (amount > 0) {
                        var process_fee = +((+amount * +process_fee_per) / 100) + +process_fee_val;
                        $scope.processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.processing_fee_value = 0;
                    }
                    $scope.total_amount_view = (Math.round((+amount + +0.00001) * 100) / 100).toFixed(2);
                }
            } else {
                $scope.processing_fee_value = 0;
                $scope.total_amount_view = (Math.round((+amount + +0.00001) * 100) / 100).toFixed(2);
            }
            $scope.pay_infull_processingfee = $scope.processing_fee_value;
            if (parseFloat($scope.total_amount_view) == parseFloat('0')) {
                if (($scope.membership_structure === 'OE' || $scope.membership_structure === 'SE') && ((parseFloat($scope.delay_membership_amount_view) > 0) || (parseFloat($scope.first_payment_amount) > 0))) {
                    $scope.zerocostview = true;
                } else if (($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' || $scope.membership_structure === 'SE') && (parseFloat($scope.total_mem_amount_view) > 0)) {
                    $scope.zerocostview = true;
                } else {
                    $scope.zerocostview = false;
                }
            } else {
                $scope.zerocostview = true;
            }
        };

        $scope.membershipresetForm = function () {
            for (var i = 0; i < $scope.msreg_col_field_name.length; i++) {
                $scope.msreg_col_field_name[i] = '';
            }
            $scope.msreg_col_field_name = [];
            if (document.getElementsByClassName("dob_month")[0] || document.getElementsByClassName("dob_day")[0] || document.getElementsByClassName("dob_year")[0]) {
                document.getElementsByClassName("dob_month")[0].value = "";
                document.getElementsByClassName("dob_day")[0].value = "";
                document.getElementsByClassName("dob_year")[0].value = "";
            }
            $scope.msreg_col_field_name[2] = '';
            $scope.mems1_firstname = '';
            $scope.mems1_lastname = '';
            $scope.mems1_email = '';
            $scope.mems1_phone = '';
            $scope.mems1_cardnumber = '';
            $scope.mems1_ccmonth = '';
            $scope.mems1_ccyear = '';
            $scope.mems1_cvv = '';
            $scope.mems1_country = '';
            $scope.mems1_postal_code = '';
            $scope.mem_waiver_checked = false;
            $scope.participant_Streetaddress = '';
            $scope.participant_City = '';
            $scope.participant_State = '';
            $scope.participant_Zip = '';
            $scope.participant_Country = '';
//            $scope.$apply();

            // muthulakshmi
            $scope.showcardselection = false;
            $scope.shownewcard = true;
            $scope.mem_cc_name = '';
            $scope.memfee_optional_discount_applied_flag = 'N';
            $scope.signupfee_optional_discount_applied_flag = 'N';
            $scope.mem_search_member = '';
            $scope.signupfee_Discode = $scope.membershipfee_Discode = '';
            $scope.memfee_discount_applied_flag = $scope.signupfee_discount_applied_flag = 'N';
            $scope.signupfee_Discode_Found = $scope.membershipfee_Discode_Found = false;
            $scope.memfee_opt_discount = '';
            $scope.signupfee_opt_discount = '';
            $scope.errormemfeeoptionaldiscount = false;
            $scope.membership_payment_method = 'CC';
            $scope.memcarddetails = [];
            $scope.membership_check_number = '';
            $scope.selectedcardindex = '';
            $scope.errormemfee = $scope.errorsignupfee = false;
            if($scope.stripe_card)
                $scope.stripe_card.clear();
            $scope.payment_method_id = '';
            $scope.stripe_customer_id = '';
            $scope.stripe_intent_method_type = "PI";
            $scope.mem_cc_chkbx = false;
            // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
            $('.ph-text').css('display','block');

        };

        $scope.resetCartAfterMembershipPaymentSucceed = function () {

            $scope.membershipresetForm();
            $scope.mems_paymentform1.$setPristine();
            $scope.mems_paymentform1.$setUntouched();
            $scope.final_membership_payment_amount = 0;
            $scope.current_processing_fee_type = '';
            $scope.processing_fee_value = 0;
            $scope.MSoptions_waiver_text = '';
            $('.form-group').removeClass('focused');
            if ($scope.company_id !== '' && $scope.dynamic_mem_category_id === '' && $scope.dynamic_mem_option_id === '') {
                // Load page:                                       
                memberView.router.load({
                    pageName: 'index'
                });
                $scope.getMembershipList($scope.company_id, '', '');
            } else if ($scope.company_id !== '' && $scope.dynamic_mem_category_id !== '' && $scope.dynamic_mem_option_id === '') {
                $scope.getMembershipList($scope.company_id, $scope.dynamic_mem_category_id, '');
            } else if ($scope.company_id !== '' && $scope.dynamic_mem_category_id !== '' && $scope.dynamic_mem_option_id !== '') {
                $scope.getMembershipList($scope.company_id, $scope.dynamic_mem_category_id, $scope.dynamic_mem_option_id);
            }
        };

        $scope.resetMembershipcartdetails = function () {

            MyApp.closePanel();
            MyApp.confirm(' Discard cart items?', 'Alert', function () {

                $scope.membershipresetForm();
                $scope.mems_paymentform1.$setPristine();
                $scope.mems_paymentform1.$setUntouched();
                $scope.SetDefault_StartDate();

                $scope.preloader = false;
                $scope.MSoptions_waiver_text = '';
                $scope.show_in_app_flg = '';
                $scope.show_in_app = '';
                $scope.current_processing_fee_type = '';
                $scope.membershipoptiontitle = '';
                $scope.membershipoption_id = '';
                $scope.membershipfee_DiscountAmount = 0;
                $scope.signupfee_DiscountAmount = 0;
                $scope.final_membership_payment_amount = 0;
                $scope.processing_fee_show = '';
                $scope.memsignupfee = 0;
                $scope.memfee = 0;
                $scope.mem_rec_frequency = '';
                $scope.prorate_first_payment = 0;
                $scope.cus_rec_freq_period_type = '';
                $scope.current_membership_manage = '';
                $scope.delay_rec_payment_start_flg = '';
                $scope.delayed_recurring_payment_type = '';
                $scope.delayed_recurring_payment_val = 0;

                $scope.membership_structure = '';
                $scope.membership_billing_option = '';
                $scope.membership_billing_startdate_type = '';
                $scope.membership_billing_deposit_amount = 0;
                $scope.membership_no_of_payments = 0;
                $scope.membership_no_of_classes = 0;
                $scope.membership_expiry_status = '';
                $scope.membership_expiry_period = '';
                $scope.membership_expiry_value = 0;
                $('.form-group').removeClass('focused');

                // Load page:
                memberView.router.loadPage({
                    pageName: 'MembershipsOptions_Description'
                });
            });


        };


        $scope.membershipcheckout = function (credit_card_id, state, user_first_name, user_last_name, email, phone, postal_code, country) {
            $scope.preloader = true;
            if (!user_first_name) {
                user_first_name = $scope.mems1_firstname;
            }
            if (!user_last_name) {
                user_last_name = $scope.mems1_lastname;
            }
            var checkout_message1 = '';
            for (var i = 0; i < $scope.current_membership_reg_columns.length; i++) {
                if (parseInt(i) !== 2) {
                    if ($scope.msreg_col_field_name[i] && $scope.msreg_col_field_name[i] !== undefined && $scope.msreg_col_field_name[i] !== '') {

                    } else {
                        if ($scope.current_membership_reg_columns[i].reg_col_mandatory === 'Y') {
                            checkout_message1 += '"' + $scope.current_membership_reg_columns[i].reg_col_name + '"';
                            checkout_message1 += '<br>';
                        }
                        $scope.msreg_col_field_name[i] = '';
                    }
                }
            }
            var birthday = document.getElementById("column2").value;
            if (birthday == '') {
                checkout_message1 += '"Date of birth"<br>';
            }
            if (birthday) {
                var birthdaysplit = birthday.split("-");
                var bmonth = birthdaysplit[1];
                var bday = birthdaysplit[2];
                var byear = birthdaysplit[0];
                var byear1 = '';
                if(byear > 1000 && byear < 4000)
                    byear1 = byear;                 
                if (!bmonth || !bday || !byear) {
                    if (!bmonth) {
                        checkout_message1 += '"DOB Month"<br>';
                    }
                    if (!bday) {
                        checkout_message1 += '"DOB Day"<br>';
                    }
                    if (!byear1) {
                        checkout_message1 += '"DOB Year"<br>';
                    }
                }
            }
            if (checkout_message1 !== '') {
                checkout_message1 = 'Participant Info:' + checkout_message1;
            }
            if ($scope.mems1_firstname === '' || $scope.mems1_firstname === undefined || $scope.mems1_lastname === '' || $scope.mems1_lastname === undefined || $scope.mems1_email === '' || $scope.mems1_email === undefined || $scope.mems1_phone === '' || $scope.mems1_phone === undefined)
            {
                checkout_message1 += "Buyer's Info:";
                if ($scope.mems1_firstname === '' || $scope.mems1_firstname === undefined) {
                    checkout_message1 += '"First Name"<br>';
                }
                if ($scope.mems1_lastname === '' || $scope.mems1_lastname === undefined) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if ($scope.mems1_email === '' || $scope.mems1_email === undefined) {
                    checkout_message1 += '"Email"<br>';
                }
                if ($scope.mems1_phone === '' || $scope.mems1_phone === undefined) {
                    checkout_message1 += '"Cell Phone"<br>';
                }
            }
            if ($scope.participant_Streetaddress === '' || $scope.participant_Streetaddress === undefined || $scope.participant_City === '' || $scope.participant_City === undefined || $scope.participant_State === '' || $scope.participant_State === undefined || $scope.participant_Zip === '' || $scope.participant_Zip === undefined || $scope.participant_Country === '' || $scope.participant_Country === undefined)
            {
                checkout_message1 += 'Address:';
                if ($scope.participant_Streetaddress === '' || $scope.participant_Streetaddress === undefined) {
                    checkout_message1 += '"Street address"<br>';
                }
                if ($scope.participant_City === '' || $scope.participant_City === undefined) {
                    checkout_message1 += '"City"<br>';
                }
                if ($scope.participant_State === '' || $scope.participant_State === undefined) {
                    checkout_message1 += '"State"<br>';
                }
                if ($scope.participant_Zip === '' || $scope.participant_Zip === undefined) {
                    checkout_message1 += '"Zip"<br>';
                }
                if ($scope.participant_Country === '' || $scope.participant_Country === undefined) {
                    checkout_message1 += '"Country"<br>';
                }
            }
            if ($scope.pos_user_type === 'S' && $scope.membership_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex && $scope.final_membership_payment_amount > 0) {
                checkout_message1 += '"Select Card"<br>';
            }
            if ($scope.membership_payment_method === 'CH' && !$scope.membership_check_number) {
                checkout_message1 += '"Check Number"<br>';
            }
            if (!$scope.mem_waiver_checked) {
                checkout_message1 += "Click the check box to accept Waiver & Agreement<br>";
            }
            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }

            if ($scope.membership_payment_method === 'CH') {
                credit_card_id = '';
                state = '';
            } else if ($scope.membership_payment_method === 'CA') {
                credit_card_id = '';
                state = '';
                $scope.membership_check_number = '';
            } else {
                $scope.membership_check_number = '';
            }

            var curr_date = new Date();
            var reg_current_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);

            var delay_recurring_payment_start_date = '';
            var membership_start_date = $scope.payment_start_date;
            if ($scope.mem_rec_frequency === 'N' && $scope.membership_structure === 'OE') {
                delay_recurring_payment_start_date = $scope.total_payment_startdate;
            } else if ($scope.membership_structure === 'OE' && $scope.delay_rec_payment_start_flg === 'Y') {
                delay_recurring_payment_start_date = $scope.delayed_recurring_payment_startdate;
                $scope.payment_start_date = delay_recurring_payment_start_date;
            } else if ($scope.membership_billing_option === 'PF' && ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' || $scope.membership_structure === 'SE')) {
                delay_recurring_payment_start_date = $scope.payment_start_date;
            } else {
                delay_recurring_payment_start_date = $scope.recurring_payment_startdate;
            }

            var exclude_days = [];
            if ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' || $scope.membership_structure === 'SE') {
                $scope.final_pay = $scope.pay;
                if ($scope.membership_structure === 'SE') {
                    exclude_days = $scope.mem_billing_exclude_days;
                } else {
                    exclude_days = [];
                }
            } else {
                $scope.final_pay = [];
                exclude_days = [];
            }

            $scope.payment_amount = (Math.round((+$scope.final_membership_payment_amount + 0.00001) * 100) / 100).toFixed(2);
            var fee = 0;
            fee = $scope.pay_infull_processingfee;

            $http({
                method: 'POST',
                url: urlservice.url + 'wepayMembershipCheckout',
                data: {
                    companyid: $scope.company_id,
                    membership_id: $scope.membershipcategory_id,
                    membership_option_id: $scope.membershipoption_id,
                    membership_category_title: $scope.MS_CategoryTitle,
                    membership_title: $scope.membershipoptiontitle,
                    membership_desc: $scope.membershipoptiontitle,
                    buyer_first_name: user_first_name,
                    buyer_last_name: user_last_name,
                    email: email,
                    phone: phone,
                    postal_code: postal_code,
                    country: country,
                    cc_id: credit_card_id,
                    cc_state: state,
                    payment_amount: $scope.total_mem_amount_view, //reccurring amt
                    processing_fee_type: $scope.current_processing_fee_type,
                    processing_fee: fee,
                    membership_fee: $scope.memfee,
                    membership_fee_disc: $scope.membershipfee_DiscountAmount,
                    signup_fee: $scope.memsignupfee,
                    signup_fee_disc: $scope.signupfee_DiscountAmount,
                    initial_payment: $scope.payment_amount, //First time amt
                    first_payment: $scope.first_payment_amount, //Membership start date payment amount
                    payment_frequency: $scope.mem_rec_frequency,
                    payment_start_date: $scope.payment_start_date, //payment start date
                    membership_start_date: membership_start_date, // Membership start date
                    recurring_start_date: $scope.recurring_payment_startdate, //recurring startdte
                    prorate_first_payment_flg: $scope.prorate_first_payment,
                    delay_recurring_payment_start_flg: $scope.delay_rec_payment_start_flg,
                    delay_recurring_payment_start_date: delay_recurring_payment_start_date, //recurr start date
                    initial_payment_include_membership_fee: $scope.mem_fee_include_status,
                    delay_recurring_payment_amount: $scope.delay_membership_amount_view,
                    custom_recurring_frequency_period_type: $scope.cus_rec_freq_period_type,
                    custom_recurring_frequency_period_val: $scope.cus_rec_freq_period_val,
                    membership_structure: $scope.membership_structure,
                    no_of_classes: $scope.membership_no_of_classes,
                    billing_options_expiration_date: $scope.membership_expiry_date,
                    billing_options: $scope.membership_billing_option,
                    billing_options_deposit_amount: $scope.membership_billing_deposit_amount,
                    billing_options_no_of_payments: $scope.membership_no_of_payments,
                    payment_array: $scope.final_pay,
                    reg_col1: $scope.msreg_col_field_name[0],
                    reg_col2: $scope.msreg_col_field_name[1],
                    reg_col3: birthday,
                    reg_col4: $scope.msreg_col_field_name[3],
                    reg_col5: $scope.msreg_col_field_name[4],
                    reg_col6: $scope.msreg_col_field_name[5],
                    reg_col7: $scope.msreg_col_field_name[6],
                    reg_col8: $scope.msreg_col_field_name[7],
                    reg_col9: $scope.msreg_col_field_name[8],
                    reg_col10: $scope.msreg_col_field_name[9],
                    upgrade_status: $scope.company_status,
                    participant_street: $scope.participant_Streetaddress,
                    participant_city: $scope.participant_City,
                    participant_state: $scope.participant_State,
                    participant_zip: $scope.participant_Zip,
                    participant_country: $scope.participant_Country,
                    signup_discount_code: $scope.signupfee_DiscountCode,
                    membership_discount_code: $scope.membershipfee_DiscountCode,
                    registration_date: reg_current_date,
                    exclude_days_array: exclude_days,
                    program_start_date: $scope.specific_start_date,
                    program_end_date: $scope.specific_end_date,
                    exclude_from_billing_flag: $scope.exclude_from_billing_flag,
                    pos_email: $localStorage.pos_user_email_id,
                    payment_method: $scope.membership_payment_method, //CC - creadit card,CH - check, CA- Cash
                    check_number: $scope.membership_check_number,
                    memfee_optional_discount: $scope.memfee_opt_discount, //memfee optional discount
                    memfee_optional_discount_flag: $scope.memfee_optional_discount_applied_flag,
                    signupfee_optional_discount: $scope.signupfee_opt_discount,
                    signupfee_optional_discount_flag: $scope.signupfee_optional_discount_applied_flag,
                    reg_type_user: $scope.pos_user_type === 'S' ? 'SP' : ($scope.pos_user_type === 'P' ? 'PP' : 'U'),
                    token: $scope.pos_token
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.onetimecheckout = 0;
                    MyApp.closeModal();
                    $scope.resetCartAfterMembershipPaymentSucceed();
                    $scope.preloader = false;
                    MyApp.alert(response.data.msg, 'Message');
                } else {
                    $scope.onetimecheckout = 0;
                    if (response.data.upgrade_status && response.data.studio_expiry_level) {
                        $scope.cmpny_email = response.data.studio_email;
                        $scope.company_status = response.data.upgrade_status;
                        $scope.company_expiry_level = response.data.studio_expiry_level;
                    }
                    if (response.data.error) {
                        $scope.preloader = false;
                        $scope.$apply();
                        MyApp.closeModal();
                        MyApp.alert(response.data.error + ' ' + response.data.error_description, 'Message');
                    } else if (($scope.company_status && $scope.company_status === 'F') || ($scope.company_expiry_level && $scope.company_expiry_level === 'L2')) {
                        MyApp.closeModal();
                        // PREVENT FURTHUR EXECUTION & BLOCK THE APP
                        MyApp.modal({
                            title: '',
                            afterText:
                                    '<br>' +
                                    '<div id="studioExpiryText" class="modal-text custom-modal-content">Please contact the business operator to activate this link.</div>' +
                                    '<br>' +
                                    '<button class="button custom-modal-green-button button-text modalresetButton" id="send_expiry_email"></button>'
                        });

                        if ($scope.cmpny_email) {
                            $("#send_expiry_email").text("Send email message");
                        } else {
                            $("#send_expiry_email").text("Ok");
                        }

                        $('#send_expiry_email').click(function () {
                            $scope.$apply(function () {
                                if ($("#send_expiry_email").text() === 'Send email message') {
                                    $scope.sendExpiryEmailToStudio();
                                }
                                MyApp.closeModal();
                            });
                        });

                    } else {
                        MyApp.closeModal();
                        if(response.data.session_status === "Expired"){
                            MyApp.alert('Session Expired','', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        } else {
                            MyApp.alert(response.data.msg, 'Message');
                        }
                        $scope.preloader = false;
                        $scope.preloaderVisible = false;
                        $scope.$apply();
                    }

                }
            }, function (response) {
                $scope.onetimecheckout = 0;
                $scope.preloaderVisible = false;
                $scope.preloader = false;
                MyApp.alert('Connection failed', 'Invalid Server Response');
            });
        };

        $scope.cancelMembership = function (page) {
            $scope.membershipresetForm();
            $scope.mems_paymentform1.$setPristine();
            $scope.mems_paymentform1.$setUntouched();
            $scope.preloader = false;
            $scope.MSoptions_waiver_text = '';
            $scope.show_in_app_flg = '';
            $scope.show_in_app = '';
            $scope.current_processing_fee_type = '';
            $scope.membershipoptiontitle = '';
            $scope.membershipoption_id = '';
            $scope.membershipfee_DiscountAmount = 0;
            $scope.signupfee_DiscountAmount = 0;
            $scope.final_membership_payment_amount = 0;
            $scope.processing_fee_show = '';
            $scope.memsignupfee = 0;
            $scope.memfee = 0;
            $scope.mem_rec_frequency = '';
            $scope.prorate_first_payment = 0;
            $scope.cus_rec_freq_period_type = '';
            $scope.current_membership_manage = '';
            $scope.delay_rec_payment_start_flg = '';
            $scope.delayed_recurring_payment_type = '';
            $scope.delayed_recurring_payment_val = 0;
            $scope.membership_structure = '';
            $scope.membership_billing_option = '';
            $scope.membership_billing_startdate_type = '';
            $scope.membership_billing_deposit_amount = 0;
            $scope.membership_no_of_payments = 0;
            $scope.membership_no_of_classes = 0;
            $scope.membership_expiry_status = '';
            $scope.membership_expiry_period = '';
            $scope.membership_expiry_value = 0;
            $('.form-group').removeClass('focused');
            if (page != 'POS') {
                memberView.router.loadPage({
                    pageName: 'index'
                });
            }
        };

        $scope.checkdatetimevalue = function (dat) {
            if (dat !== '0000-00-00' && dat !== '') {
                var timecheck = dat.split(" ");
                if (timecheck[1] === "00:00:00") {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                var newDate = '';
                return newDate;
            }
        };


        $scope.getstartDate = function (dat) {
            if (dat !== '0000-00-00' && dat !== '') {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari) {
                    var safaritimecheck = dat.split(" ");
                    if (safaritimecheck[1] === "00:00:00") {
                        var ddateonly = new Date(safaritimecheck[0].replace(/-/g, "/"));
                        var dateStr = new Date(ddateonly);
                        return dateStr;
                    } else {
                        var dda = dat.toString();
                        dda = dda.replace(/-/g, "/");
                        var time = new Date(dda).toISOString();   // For date in safari format
                        return time;
                    }
                } else {
                    var datetimecheck = dat.split(" ");
                    if (datetimecheck[1] === "00:00:00") {
                        var ddateonly = datetimecheck[0] + 'T00:00:00+00:00';
                        var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                        return newDate;
                    } else {
                        var ddateonly = datetimecheck[0] + 'T' + datetimecheck[1] + '+00:00';
                        var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                        return newDate;

                    }
                }
            } else {
                var newDate = '';
                return newDate;
            }
        };

        $scope.getPaymentdate = function (dat) {
            if (dat !== '0000-00-00' && dat !== '') {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari) {
                    var ddateonly = new Date(dat.replace(/-/g, "/"));
                    var dateStr = new Date(ddateonly);
                    return dateStr;
                } else {
                    var ddateonly = dat + 'T00:00:00+00:00';
                    var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                    return newDate;
                }
            } else {
                var newDate = '';
                return newDate;
            }
        };

        $scope.formatDate = function (dat) {
            var date = dat.split("-").join("/");
            var dateOut = new Date(date);
            return dateOut;
        };

        $scope.openevents = function (eventurl) {
            MyApp.closePanel();
            if ($rootScope.online != "online") {
                $scope.istatus();
                return;
            }
            if (eventurl === '' && eventurl === undefined && eventurl === null) {
                return false;
            } else if (eventurl !== '' && eventurl !== undefined && eventurl !== null) {
                window.open(eventurl, '_blank');
            }
        };

        $scope.newSEdateFormat = function (SEdate) {
            var sdate = SEdate.split("-");
            var syear = sdate[0];
            var smonth = parseInt(sdate[1], 10) - 1;
            var sdate = sdate[2];
            var sedate = new Date(syear, smonth, sdate);
            return sedate;
        };

        //  Muthulakshmi
        $scope.getstudentlist = function (companyid) {
            $scope.preloader = false;
            $scope.preloaderVisible = false;
            $http({
                method: 'GET',
                url: openurlservice.url + 'getstudent',
                params: {
                    "companyid": companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user": $scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status === 'Success') {
                    $scope.studentlist = response.data.msg.student_details;
                    for (var i = 0; i < $scope.studentlist.length; i++) {
                        $scope.student_name.push($scope.studentlist[i].student_name);
                    }
                } else {
                    if (response.data.session_status === "Expired") {
                        $scope.preloader = false;
                        $scope.preloaderVisible = false;
                        MyApp.alert('Session Expired','', function () {
                            $localStorage.pos_user_email_id = "";
                            $scope.backToPOS();
                        });
                    } else {
//                        MyApp.alert(response.data.msg, 'Message');
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert(response.data.msg, 'Failure');
            });
        };
        var autocompleteDropdownSimple = MyApp.autocomplete({
            input: '#autocomplete-dropdown',
            openIn: 'dropdown',

            source: function (autocomplete, query, render) {
                var results = [];
                if (query.length === 0) {
                    render(results);
                    return;
                }

                // You can find matched items
                for (var i = 0; i < $scope.student_name.length; i++) {
                    if ($scope.student_name[i].toLowerCase().indexOf(query.toLowerCase()) >= 0)
                        results.push($scope.student_name[i]);
                }
                // Display the items by passing array with result items
                render(results);
            }
        });
        $scope.catchMemSearchMember = function () {
            if ($scope.mem_search_member) {
                if ($scope.studentlist.length > 0) {
                    for (var i = 0; i < $scope.studentlist.length; i++) {
                        if ($scope.mem_search_member === $scope.studentlist[i].student_name) {
                            $scope.mem_selected_student_id = $scope.studentlist[i].student_id;
                            $scope.msreg_col_field_name[0] = $scope.studentlist[i].reg_col_1;
                            $scope.msreg_col_field_name[1] = $scope.studentlist[i].reg_col_2;
                            $scope.msreg_col_field_name[2] = $scope.studentlist[i].reg_col_3;
                            if ($scope.msreg_col_field_name[2]) {
                                var bchoose_date = $scope.msreg_col_field_name[2].split("-");
                                if(bchoose_date[1])
                                    document.getElementsByClassName("dob_month")[0].value = bchoose_date[1];
                                if(bchoose_date[2])
                                    document.getElementsByClassName("dob_day")[0].value = bchoose_date[2];
                                if(bchoose_date[0]){
                                    if(bchoose_date[0] > 1000 && bchoose_date[0] < 4000)
                                        document.getElementsByClassName("dob_year")[0].value = bchoose_date[0];
                                    else
                                       document.getElementsByClassName("dob_year")[0].value = ''; 
                                }
                                document.getElementById("column2").value = $scope.msreg_col_field_name[2];
                            }
                            $scope.mems1_firstname = $scope.studentlist[i].buyer_first_name;
                            $scope.mems1_lastname = $scope.studentlist[i].buyer_last_name;
                            $scope.mems1_email = $scope.studentlist[i].buyer_email;
                            $scope.mems1_phone = $scope.studentlist[i].buyer_phone;
                            $scope.participant_Streetaddress = $scope.studentlist[i].participant_street;
                            $scope.participant_City = $scope.studentlist[i].participant_city;
                            $scope.participant_State = $scope.studentlist[i].participant_state;
                            $scope.participant_Zip = $scope.studentlist[i].participant_zip;
                            $scope.participant_Country = $scope.studentlist[i].participant_country;
                            $(".form-group > input.ng-valid").parents('.form-group').addClass('focused');
                            //If no values then remove focus class
                            if(!$scope.mems1_firstname)
                                $("#mems1_firstName").parents('.form-group').removeClass('focused');
                            if(!$scope.mems1_lastname)
                                $("#mems1_lastName").parents('.form-group').removeClass('focused');
                            if(!$scope.mems1_email)
                                $("#mems1_email").parents('.form-group').removeClass('focused');
                            if(!$scope.mems1_phone)
                                $("#mems1_phone").parents('.form-group').removeClass('focused');
                            if(!$scope.participant_Streetaddress)
                                $("#participant_Streetaddress").parents('.form-group').removeClass('focused');
                            if(!$scope.participant_City)
                                $("#participant_City").parents('.form-group').removeClass('focused');
                            if(!$scope.participant_State)
                                $("#participant_State").parents('.form-group').removeClass('focused');
                            if(!$scope.participant_Zip)
                                $("#participant_Zip").parents('.form-group').removeClass('focused');
                            if(!$scope.participant_Country)
                                $("#participant_Country").parents('.form-group').removeClass('focused');
                            
                            for(var j = 0; j < 10; j++){
                                $scope.catchMemInputBlur('column'+j,$scope.msreg_col_field_name[j]); // For Dynamic registration column
                            }
                            //For credit card
                            if ($scope.studentlist[i].card_details && $scope.studentlist[i].card_details.length > 0) {
                                $scope.memcarddetails = $scope.studentlist[i].card_details;
                                $scope.showcardselection = true;
                                $scope.shownewcard = false;
                            } else {
                                $scope.memcarddetails = [];
                                $scope.showcardselection = false;
                                $scope.shownewcard = true;
                                $("#mems1_cardnumber").parents('.form-group').removeClass('focused');
                                $("#mems1_cvv").parents('.form-group').removeClass('focused');
                                $("#mem_cc_name").parents('.form-group').removeClass('focused');
                                $("#mems1_postal_code").parents('.form-group').removeClass('focused');
                            }
                        }
                    }
                }
                // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
                $('.ph-text').css('display','none');
            } else {
                $scope.mem_selected_student_id = '';
                $scope.msreg_col_field_name[0] = '';
                $scope.msreg_col_field_name[1] = '';
                $scope.msreg_col_field_name[2] = '';
                document.getElementsByClassName("dob_month")[0].value = '';
                document.getElementsByClassName("dob_day")[0].value = '';
                document.getElementsByClassName("dob_year")[0].value = '';
                document.getElementById("column2").value = '';
                $scope.mems1_firstname = '';
                $scope.mems1_lastname = '';
                $scope.mems1_email = '';
                $scope.mems1_phone = '';
                $scope.participant_Streetaddress = '';
                $scope.participant_City = '';
                $scope.participant_State = '';
                $scope.participant_Zip = '';
                $scope.participant_Country = '';
                $scope.mem_credit_card_id = '';
                $scope.mem_credit_card_status = '';
                $scope.mem_cc_name = '';
                $scope.mems1_cardnumber = '';
                $scope.mems1_cvv = '';
                $scope.mems1_ccmonth = '';
                $scope.mems1_ccyear = '';
                $scope.mems1_country = '';
                $scope.mems1_postal_code = '';
                $scope.payment_method_id = '';
                $scope.stripe_customer_id = '';
                $(".form-group > input.ng-valid").parents('.form-group').removeClass('focused');
                $scope.showcardselection = false;
                $scope.shownewcard = true;
                $scope.memcarddetails = [];
                // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
                $('.ph-text').css('display','block');
            }
        };
        //For selecting existing card of a user
        $scope.selectMemCardDetail = function (ind) {
            if ($scope.selectedcardindex === 'Add New credit card') {
                $scope.shownewcard = true;
                $scope.mem_credit_card_id = '';
                $scope.mem_credit_card_status = '';
                $scope.mem_cc_name = '';
                $scope.mems1_cardnumber = '';
                $scope.mems1_ccmonth = '';
                $scope.mems1_ccyear = '';
                $scope.mems1_country = '';
                $scope.mems1_postal_code = '';
                $scope.payment_method_id = '';
                $scope.stripe_customer_id = '';
                $("#mems1_cardnumber").parents('.form-group').removeClass('focused');
                $("#mems1_cvv").parents('.form-group').removeClass('focused');
                $("#mem_cc_name").parents('.form-group').removeClass('focused');
                $("#mems1_postal_code").parents('.form-group').removeClass('focused');
            } else {
                $scope.shownewcard = false;
                if($scope.stripe_enabled) {
                    $scope.payment_method_id = $scope.memcarddetails[ind].payment_method_id;
                    $scope.stripe_customer_id = $scope.memcarddetails[ind].stripe_customer_id;
                }else{
                    $scope.mem_credit_card_id = parseInt($scope.memcarddetails[ind].credit_card_id);
                    $scope.mem_credit_card_status = $scope.memcarddetails[ind].credit_card_status;
                    $scope.mems1_cardnumber = $scope.memcarddetails[ind].credit_card_name;
                    $scope.mems1_ccmonth = $scope.memcarddetails[ind].credit_card_expiration_month;
                    $scope.mems1_ccyear = $scope.memcarddetails[ind].credit_card_expiration_year;
                    $scope.mems1_country = $scope.memcarddetails[ind].credit_card_country;
                    $scope.mems1_postal_code = $scope.memcarddetails[ind].postal_code;
                }
            }
        };

        $scope.BackToCart = function () {
            if ($scope.memfee_optional_discount_applied_flag === 'Y' || $scope.signupfee_optional_discount_applied_flag === 'Y' || $scope.signupfee_opt_discount || $scope.memfee_opt_discount) {
                $scope.signupfee_opt_discount = $scope.memfee_opt_discount = "";
                $scope.memfee_optional_discount_applied_flag = $scope.signupfee_optional_discount_applied_flag = 'N';
                $scope.errormemfee = $scope.errorsignupfee = false;
                $scope.signupfee_DiscountAmount = $scope.membershipfee_DiscountAmount = 0;
                $scope.total_payment_amount = $scope.static_total_payment_amount;
                $scope.showMembershipTotal();
            }else if($scope.memfee_discount_applied_flag === 'Y' || $scope.signupfee_discount_applied_flag === 'Y'){
                $scope.signupfee_DiscountAmount = $scope.static_signup_discount;
                $scope.membershipfee_DiscountAmount = $scope.static_membership_discount;
                $scope.total_payment_amount = $scope.static_total_payment_amount;
                $scope.showMembershipTotal();
            }
        };

        $scope.catchOptionalDiscuont = function (disc_type, value) {
            var temp_memfee_amount_view = 0;
            var temp_signupfee_amount_view = 0;
            var total_amount = 0;
            if (disc_type === 'S')
                $scope.signupfee_opt_discount = value;
            else
                $scope.memfee_opt_discount = value;

            $scope.static_total_payment_amount = $scope.final_total_payment_amount;
            $scope.memfee_optional_discount_applied_flag = $scope.signupfee_optional_discount_applied_flag = 'N';
            $scope.errormemfee = $scope.errorsignupfee = false;
//            Muthulakshmi
            if (!$scope.memfee_opt_discount && !$scope.signupfee_opt_discount) { //If No discount for all
                $scope.memfee_optional_discount_applied_flag = $scope.signupfee_optional_discount_applied_flag = 'N';
                $scope.errormemfee = $scope.errorsignupfee = false;
                $scope.total_payment_amount = $scope.static_total_payment_amount;
                if($scope.memfee_discount_applied_flag === 'Y' || $scope.signupfee_discount_applied_flag === 'Y'){
                    $scope.signupfee_DiscountAmount = $scope.static_signup_discount;
                    $scope.membershipfee_DiscountAmount = $scope.static_membership_discount;
                }else{
                    $scope.membershipfee_DiscountAmount = $scope.signupfee_DiscountAmount = 0;
                }
            } else { //If anyone or both

                //For membership
                if ($scope.memfee_opt_discount) { // If Mem discount
                    if (parseFloat($scope.memfee_opt_discount) > 0 && $scope.actual_membership_cost > 0) {
                        if (parseFloat($scope.memfee_opt_discount) <= parseFloat($scope.actual_membership_cost)) {
                            temp_memfee_amount_view = parseFloat($scope.actual_membership_cost) - parseFloat($scope.memfee_opt_discount);
                            if (parseFloat(temp_memfee_amount_view) >= 0) {
                                $scope.membershipfee_DiscountAmount = $scope.memfee_opt_discount;
                                $scope.memfee_optional_discount_applied_flag = 'Y';
                            } else {
                                $scope.membershipfee_DiscountAmount = 0;
                                $scope.memfee_optional_discount_applied_flag = 'N';
                            }
                        } else {
                            $scope.errormemfee = true;
                            $scope.errormembershipmsg = 'Membership discount should not greater than membership fee';
                            $scope.memfee_optional_discount_applied_flag = 'N';
                            $scope.membershipfee_DiscountAmount = 0;
                        }
                    } else {
                        $scope.errormemfee = false;
                        $scope.memfee_optional_discount_applied_flag = 'N';
                        $scope.membershipfee_DiscountAmount = 0;
                    }
                } else {
                    $scope.errormemfee = false;
                    $scope.memfee_optional_discount_applied_flag = 'N';
                    $scope.membershipfee_DiscountAmount = 0;
                }

                //For signup
                if ($scope.signupfee_opt_discount) { // If signup discount
                    if (parseFloat($scope.signupfee_opt_discount) > 0 && $scope.actual_signup_cost > 0) {
                        if (parseFloat($scope.signupfee_opt_discount) <= parseFloat($scope.actual_signup_cost)) {
                            temp_signupfee_amount_view = parseFloat($scope.actual_signup_cost) - parseFloat($scope.signupfee_opt_discount);
                            if (parseFloat(temp_signupfee_amount_view) >= 0) {
                                $scope.signupfee_DiscountAmount = $scope.signupfee_opt_discount;
                                $scope.signupfee_optional_discount_applied_flag = 'Y';
                            } else {
                                $scope.signupfee_DiscountAmount = 0;
                                $scope.signupfee_optional_discount_applied_flag = 'N';
                            }
                        } else {
                            $scope.errorsignupfee = true;
                            $scope.errorsignupmsg = 'signup discount should not greater than signup fee';
                            $scope.signupfee_optional_discount_applied_flag = 'N';
                            $scope.signupfee_DiscountAmount = 0;
                        }
                    } else {
                        $scope.errorsignupfee = false;
                        $scope.signupfee_optional_discount_applied_flag = 'N';
                        $scope.signupfee_DiscountAmount = 0;
                    }
                } else {
                    $scope.errorsignupfee = false;
                    $scope.signupfee_optional_discount_applied_flag = 'N';
                    $scope.signupfee_DiscountAmount = 0;
                }
                // For total
                total_amount = parseFloat(temp_signupfee_amount_view) + parseFloat(temp_memfee_amount_view);
                if (total_amount >= 5) {
                    $scope.total_payment_amount = total_amount;
                } else {
                    $scope.total_payment_amount = $scope.static_total_payment_amount;
                }
            }
            $scope.showMembershipTotal();
        };
        // RESET PAYMENT METHOD SELECTION         
        $scope.selectPaymentMethod = function () {
            $scope.membership_check_number = '';
            $scope.selectedcardindex = '';
            $scope.mem_credit_card_id = '';
            $scope.mem_credit_card_status = '';
            $scope.mems1_cardnumber = '';
            $scope.mems1_ccmonth = '';
            $scope.mems1_ccyear = '';
            $scope.mems1_country = '';
            $scope.mems1_postal_code = '';
            $scope.signupfee_opt_discount = '';
            $scope.memfee_opt_discount = '';
            $scope.mem_cc_chkbx = false;
            $scope.payment_method_id = '';
            $scope.stripe_customer_id = '';
            if ($scope.membership_payment_method === 'CC') {
                if ($scope.memcarddetails && $scope.memcarddetails.length > 0) {
                    $scope.showcardselection = true;
                    $scope.shownewcard = false;
                } else {
                    $scope.showcardselection = false;
                    $scope.shownewcard = true;
                }
            } else {
                $scope.showcardselection = false;
                $scope.shownewcard = false;
            }
            $scope.catchOptionalDiscuont('', '');
        };

        $scope.backToPOS = function (page) {
            // CANCEL MEMBERSHIP ACTIONS
                $scope.cancelMembership('POS');
            // TAKE USER TO POS HOME
            var hosturl;
            if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
               window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                       window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                    var origin = window.location.origin;
                    hosturl = origin;
                }

            } else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            }  else if (window.location.hostname === 'beta.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'https://' + www_url;
                }
            }   else if (window.location.hostname === 'stage.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'localhost') {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            } else {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            }
            var pos_type, pos_string, pos_entry_type;
            if ($scope.pos_user_type === 'S') {
                pos_type = 'spos';
                pos_string = 'e92213e6721525d7spos712627e6db9b98b4';
            } else if ($scope.pos_user_type === 'P') {
                pos_type = 'ppos';
                pos_string = '8f2f44c8580cc72ppos700ff5d197b5a18f0';
            } else {
                pos_type = '';
                pos_string = '';
            }
            if ($scope.pos_entry_type) {
                pos_entry_type = $scope.pos_entry_type;
            } else {
                pos_entry_type = 'l';
            }
            if (pos_type) {
                $window.open(hosturl + '/' + 'pos' + '/?=' + $scope.param1 + '/' + $scope.param2 + '/' + pos_type + '-' + pos_string + '/' + pos_entry_type + '/', '_self');
            }
        };

        $scope.catchMemCheck = function () {
            $scope.membership_check_number = this.membership_check_number;
        };
        $scope.catchMemInputBlur = function (selectedid,inputvalue){
            if (inputvalue === "" || inputvalue === undefined) {
                $('#'+selectedid).parents('.form-group').removeClass('focused');
            } else {
                $('#'+selectedid).parents('.form-group').addClass('focused');
            }
        };
        $scope.catchMemInputFocus = function (selectedid){
                $('#'+selectedid).parents('.form-group').addClass('focused');
        };

        //DOCUMENT READY FUNCTION
        angular.element(document).ready(function () {
            // NEW ANIMATION FORM PLACEHOLDER TO LABELS
            $('input').focus(function () {
                $(this).parents('.form-group').addClass('focused');
            });

            $('input').blur(function () {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.form-group').removeClass('focused');
                } else {
                    $(this).addClass('filled');
                }
            });
            ($(window).width() < 641) ? $scope.mobiledevice = true : $scope.mobiledevice = false;
            $scope.urlredirection();
        });

    }]);

MyApp.angular.filter('date2', function ($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function (input, format) {
        console.clear();
        var dtfilter = $filter('date')(input, format);
        var day = parseInt($filter('date')(input, 'dd'));
        var relevantDigits = (day < 30) ? day % 20 : day % 30;
        var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
        return dtfilter;
    };
});

MyApp.angular.filter("trust", ['$sce', function ($sce) {
        return function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        }
    }]);
MyApp.angular.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
MyApp.angular.directive('compileTemplate', function ($compile, $parse) {
    return {
        link: function (scope, element, attr) {
            var parsed = $parse(attr.ngBindHtml);
            function getStringValue() {
                return (parsed(scope) || '').toString();
            }

            //Recompile if the template changes
            scope.$watch(getStringValue, function () {
                $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
            });
        }
    }
});
//compile-template

//valid number and only 2 digit after decimal
MyApp.angular.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');

                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});

