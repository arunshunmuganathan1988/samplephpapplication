<?php

require_once 'WePay.php';

class wepay_redirect {
    private $wp = '';
    
    function __construct() {
        $this->wp = new wepay();
    }
    
    public function processApi(){
        
        if(isset($_REQUEST)){
            $this->wp->log_info(json_encode($_REQUEST));
        }else{
            $this->wp->log_info("No response");
        }
    }
}
$api = new wepay_redirect();
$api->processApi();