<?php

require_once '../Globals/cont_log.php';
require_once 'WePay.php';
require_once('class.phpmailer.php');
require_once('class.smtp.php');

class wepayipn{
    
    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_pswd = '';
    private $user_agent = '';
    
    public function __construct(){
        require_once '../Globals/config.php';
        $this->wp = new wepay();        
        $this->inputs();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->user_agent = $this->getUserAgent();
        date_default_timezone_set('UTC');
    }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }
    
    private function getUserAgent(){
        if (!function_exists('getallheaders')){
            if(!function_exists ('apache_request_headers')){
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            }else{
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
            }
        }else{
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
        }
        return $user_agent;
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithMail($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
        header("Content-Type:" . $this->_content_type);
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function processApi(){
        $func = strtolower(trim(str_replace("/","",$_REQUEST['run'])));
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)){
            $this->_request = json_decode(file_get_contents('php://input'), true);
 	}
        
        if((int)method_exists($this,$func) > 0){
            $this->$func();
        }else{
            $error = array("status" => "Failed", "msg" => "Page not found.");
            $this->response($this->json($error),404); // If the method not exist with in this class, response would be "Page not found".
        }
    }
    
    private function updateUser(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error),406);
        }
        
        $user_id = '';
        
        if(isset($this->_request['user_id'])){
            $user_id = $this->_request['user_id'];
        }
        
        if(!empty($user_id)){
            $this->getUserDetails($user_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error), 400);
        }
    }
    
    private function getUserDetails($user_id){
        $query = sprintf("SELECT `wp_account_id`, `company_id`, `wp_user_id`, `wp_user_state`, `access_token`, `first_name`, `last_name`, `user_name`, 
                `user_email` FROM `wp_account` WHERE `wp_user_id`='%s'",  mysqli_real_escape_string($this->db, $user_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $access_token = $row['access_token'];
                $dummy_sns=[];
                $temp_password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $temp_password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                $response = $this->wp->accessWepayApi("user",'GET','',$token,$this->user_agent,$dummy_sns);
                if($response['status']=="Success"){
                    $res = $response['msg'];
                    $user_state = $res['state'];
                    
                    $update_query = sprintf("UPDATE `wp_account` SET `wp_user_state`='%s' WHERE `wp_user_id`='%s'", mysqli_real_escape_string($this->db, $user_state),
                            mysqli_real_escape_string($this->db, $user_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "User status updated successfully.");
                        ipn_log_info($this->json($error));
                        $this->response($this->json($error), 200);
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "User doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 401);
            }
        }
    }
    
    private function updateAccount(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error),406);
        }
        
        $account_id = '';
        
        if(isset($this->_request['account_id'])){
            $account_id = $this->_request['account_id'];
        }
        
        if(!empty($account_id)){
            $this->getAccountDetails($account_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error), 400);
        }
    }
    
    private function getAccountDetails($account_id){
        $query = sprintf("SELECT `wp_account_id`, `company_id`, `access_token`, `account_id`, `account_state`, `first_name`, `last_name`, `user_name`, 
                `user_email` FROM `wp_account` WHERE `account_id`='%s' limit 0,1",  mysqli_real_escape_string($this->db, $account_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $company_id = $row['company_id'];
                $wp_account_id = $row['wp_account_id'];
                $access_token = $row['access_token'];
                $dummy_sns=[];
                $temp_password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $temp_password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                
                $json = array("account_id"=>"$account_id");
                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$this->user_agent,$dummy_sns);
                ipn_log_info("wepay_get_account: ".$response['status']);
                if($response['status']=='Success'){
                    $res = $response['msg'];
                    $account_state = $res['state'];
                    
                    $update_query = sprintf("UPDATE `wp_account` SET `account_state`='%s' WHERE `wp_account_id`='%s' and `account_id`='%s'", mysqli_real_escape_string($this->db, $account_state),
                            mysqli_real_escape_string($this->db, $wp_account_id), mysqli_real_escape_string($this->db, $account_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        if($account_state=="active" || $account_state=="action_required"){
                            $wp_status = 'Y';
                        }else{
                            $wp_status = 'N';
                        }
                        $update_company = sprintf("UPDATE `company` SET `wepay_status`='$wp_status' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
                        $update_company_result = mysqli_query($this->db, $update_company);
                        if(!$update_company_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
                            ipn_log_info($this->json($error_log));
                        }
                        $error = array('status' => "Success", "msg" => "Account status updated successfully.");
                        ipn_log_info($this->json($error));
                        $this->response($this->json($error), 200);
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Account details doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
    private function updateCheckout(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error),406);
        }
        
        $checkout_id = $for = '';
        
        if(isset($this->_request['checkout_id'])){
            $checkout_id = $this->_request['checkout_id'];
        }
        if(isset($_REQUEST['for'])){
            $for = $_REQUEST['for'];
        }
        
        if(!empty($checkout_id)){
            if(!empty($for) && $for == "membership"){
                $this->getMembershipCheckoutDetails($checkout_id);
            }else if(!empty($for) && $for == "trial"){
                $this->gettrialCheckoutDetails($checkout_id);
            }else if(!empty($for) && $for == "retail"){
                $this->getretailCheckoutDetails($checkout_id);
            }else if(!empty($for) && $for == "misc"){
                $this->getMiscCheckoutDetails($checkout_id);
            }else{
                $this->getCheckoutDetails($checkout_id);
            }
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error), 400);
        }
    }
    
    private function getCheckoutDetails($checkout_id){
        $query = sprintf("SELECT er.`event_reg_id`, ep.`event_payment_id`, ep.`checkout_id`, ep.`checkout_status`, `access_token`, e.`event_title`, ep.`payment_amount`, ep.`processing_fee`, er.`processing_fee_type`, er.`event_id`, er.`payment_type`
                FROM `event_registration` er 
                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` 
                LEFT JOIN `wp_account` wp ON er.`company_id` = wp.`company_id` 
                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
                WHERE ep.`checkout_id` = '%s' ORDER BY ep.`event_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $checkout_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $event_reg_id = $row['event_reg_id'];
                $access_token = $row['access_token'];
                $event_id = $row['event_id'];
                $payment_id = $row['event_payment_id'];
                $payment_amount = $row['payment_amount'];
                $processing_fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_type = $row['payment_type'];
                $dummy_sns=[];
                $temp_password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $temp_password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                
                $json = array("checkout_id"=>"$checkout_id");
                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi('checkout','POST',$postData,$token,$this->user_agent,$dummy_sns);
                ipn_log_info("wepay_get_checkout: ".$response['status']);
                if($response['status']=='Success'){
                    $res = $response['msg'];
                    $checkout_state = $res['state'];
                    $refund_amt = $res['refund']['amount_refunded'];
                    $chargeback_amt = $res['chargeback']['amount_charged_back'];
                    $schedule_status = "";
                    $send_email = 0;
                    if($checkout_state=='expired' || $checkout_state=='failed' || $checkout_state=='charged back'){
                        $schedule_status = ", `schedule_status`='F'";
                        $send_email = 1;
                    }
                    $update_query = sprintf("UPDATE `event_payment` SET `checkout_status`='%s' $schedule_status WHERE `event_reg_id`='%s' and `event_payment_id`='%s' and `checkout_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $checkout_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query", "ipn" => "1");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        ipn_log_info($this->json($error));
                        
                        
                        if($checkout_state=='released' && $refund_amt==0 && $chargeback_amt==0){
                            $temp_payment_amount = $payment_amount;
                            $onetime_netsales_for_parent_event = 0;

                            $update_event_reg_query = sprintf("UPDATE `event_registration` SET `paid_amount`=`paid_amount`+$payment_amount WHERE `event_reg_id`='%s'",$event_reg_id);
                            $result_update_event_reg_query = mysqli_query($this->db, $update_event_reg_query);
                            if(!$result_update_event_reg_query){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_reg_query", "ipn" => "1");
                                ipn_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }

                            if($payment_type=='CO'){
                                $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_type`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
                                           LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
                                           AND `payment_status` IN ('CC') ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                                $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                                if(!$get_event_details_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query", "ipn" => "1");
                                    ipn_log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }else{
                                    $num_rows2 = mysqli_num_rows($get_event_details_result);
                                    if($num_rows2>0){
                                        $rem_due = 0;
                                        while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                                            if($temp_payment_amount>0){
                                                $event_reg_details_id = $row2['event_reg_details_id'];
                                                $child_id = $row2['event_id'];
                                                $event_parent_id = $row2['parent_id'];
                                                $child_payment = $row2['payment_amount'];
                                                $rem_due = $row2['balance_due'];
                                                $event_type = $row2['event_type'];
                                                $payment_status = $row2['payment_status'];
                                                $child_net_sales = 0;
                                                if($onetime_netsales_for_parent_event==0){
                                                    if($processing_fee_type==2){
                                                        $net_sales = $payment_amount;
                                                    }elseif($processing_fee_type==1){
                                                        $net_sales = $payment_amount-$processing_fee;
                                                    }
                                                    $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
                                                    $result_update_event_query = mysqli_query($this->db, $update_event_query);
                                                    if(!$result_update_event_query){
                                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query", "ipn" => "1");
                                                        ipn_log_info($this->json($error_log));
                                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                        $this->response($this->json($error), 500);
                                                    }
                                                    $onetime_netsales_for_parent_event=1;
                                                }
                                                if($temp_payment_amount>=$child_payment){
                                                    $temp_payment_amount = $temp_payment_amount-$child_payment;
                                                    $child_net_sales = $child_payment;
                                                    $child_payment = 0;
                                                }else{
                                                    $child_payment = $child_payment - $temp_payment_amount;
                                                    $child_net_sales = $temp_payment_amount;
                                                    $temp_payment_amount = 0;
                                                }
                                                $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$child_payment WHERE `event_reg_details_id`='%s'", 
                                                            mysqli_real_escape_string($this->db, $event_reg_details_id));
                                                $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
                                                if(!$result_update_reg_query){
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query", "ipn" => "1");
                                                    ipn_log_info($this->json($error_log));
                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                    $this->response($this->json($error), 500);
                                                }
                                                if($child_id!=$event_parent_id){
                                                    $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
                                                    $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
                                                    if(!$result_update_child_query){
                                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales", "ipn" => "1");
                                                        ipn_log_info($this->json($error_log));
                                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                        $this->response($this->json($error), 500);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }else{
                                $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_type`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
                                           LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
                                           AND `payment_status` = 'RP' ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                                $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                                if(!$get_event_details_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query", "ipn" => "1");
                                    ipn_log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }else{
                                    $num_rows2 = mysqli_num_rows($get_event_details_result);
                                    if($num_rows2>0){
                                        $rem_due = 0;
                                        while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                                            if($temp_payment_amount>0){
                                                $event_reg_details_id = $row2['event_reg_details_id'];
                                                $child_id = $row2['event_id'];
                                                $event_parent_id = $row2['parent_id'];
                                                $rem_due = $row2['balance_due'];
                                                $event_type = $row2['event_type'];
                                                $payment_status = $row2['payment_status'];
                                                $child_net_sales = 0;
                                                if($onetime_netsales_for_parent_event==0){
                                                    if($processing_fee_type==2){
                                                        $net_sales = $payment_amount;
                                                    }elseif($processing_fee_type==1){
                                                        $net_sales = $payment_amount-$processing_fee;
                                                    }
                                                    $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
                                                    $result_update_event_query = mysqli_query($this->db, $update_event_query);
                                                    if(!$result_update_event_query){
                                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query", "ipn" => "1");
                                                        ipn_log_info($this->json($error_log));
                                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                        $this->response($this->json($error), 500);
                                                    }
                                                    $onetime_netsales_for_parent_event=1;
                                                }
                                                if($temp_payment_amount>=$rem_due){
                                                    $temp_payment_amount = $temp_payment_amount-$rem_due;
                                                    $child_net_sales = $rem_due;
                                                    $rem_due = 0;
                                                    $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due, `payment_status`='RC' WHERE `event_reg_details_id`='%s'", 
                                                            mysqli_real_escape_string($this->db, $event_reg_details_id));
                                                }else{
                                                    $rem_due = $rem_due - $temp_payment_amount;
                                                    $child_net_sales = $temp_payment_amount;
                                                    $temp_payment_amount = 0;
                                                    $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due WHERE `event_reg_details_id`='%s'", 
                                                            mysqli_real_escape_string($this->db, $event_reg_details_id));
                                                }
                                                $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
                                                if(!$result_update_reg_query){
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query", "ipn" => "1");
                                                    ipn_log_info($this->json($error_log));
                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                    $this->response($this->json($error), 500);
                                                }else{
                                                    if($child_id!=$event_parent_id){
                                                        $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
                                                        $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
                                                        if(!$result_update_child_query){
                                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales", "ipn" => "1");
                                                            ipn_log_info($this->json($error_log));
                                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                            $this->response($this->json($error), 500);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                                
                        
                        if($send_email==1){
                            $this->responseWithMail($this->json($error), 200);
                            $this->sendFailedOrderReceiptForEventPayment($event_reg_id,$checkout_state,0);
                        }else{
                            $this->response($this->json($error), 200);
                        }
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
                
            }else{
                $error = array('status' => "Failed", "msg" => "Checkout details doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
    protected function sendFailedOrderReceiptForEventPayment($event_reg_id, $checkout_state, $return_value){
       
        $waiver_policies = '';
        $query = sprintf("SELECT er.`company_id`, er.`event_id`, `event_title`, ep.`cc_name`, `waiver_policies`, concat(er.event_registration_column_2, ',', er.event_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `payment_type`, 
            er.`payment_amount` total_due, er.`processing_fee_type`, er.`processing_fee` total_processing_fee, `paid_amount`, ep.`payment_amount`, ep.`processing_fee`, ep.`refunded_amount`, `schedule_date`, `schedule_status`, DATE(ep.`created_dt`) created_dt
            FROM `event_registration` er LEFT JOIN `event_payment` ep ON er.`event_reg_id` = ep.`event_reg_id` AND ep.`schedule_status` IN ('N', 'S', 'PR', 'M')
            LEFT JOIN `event` e ON e.`event_id` = er.`event_id` AND e.`company_id` =  er.`company_id`
            LEFT JOIN `company` c ON c.`company_id` =  er.`company_id`
            WHERE er.`event_reg_id` = '%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
                $event_name = $participant_name = $buyer_name = $buyer_email = $cc_email_list = '';
                $inital_stage = $initial_down_pay = 0;
                $current_plan_details = $event_details = $bill_due_details = $payment_details = [];
                $total_due = $paid_due = $rem_due = 0;
                while($row = mysqli_fetch_assoc($result)){
                    $paid_amt = 0;
                    $event_id = $row['event_id'];
                    $event_name = $row['event_title'];
                    $participant_name = $row['participant_name'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $temp=[];
                    
                    if($row['processing_fee_type']==2){
                        $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                    }else{
                        $temp['amount'] = $row['payment_amount'];
                    }
//                    $temp['amount'] = $row['payment_amount'];
                    $temp['date'] = $row['schedule_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['edit_status'] = 'N';
                    $temp['schedule_status'] = $row['schedule_status'];
                    $temp['processing_fee'] = $row['processing_fee'];
                    $temp['credit_card_name'] = $row['cc_name'];
                    if($row['schedule_status']=='N'){
                        $temp['status'] = 'N';
                    }elseif($row['schedule_status']=='S'){
                        $temp['status'] = 'S';
                        $paid_amt = $temp['amount'];
                        $inital_stage++;
                    }elseif($row['schedule_status']=='PR' || $row['schedule_status']=='M'){
                        $paid_amt = $temp['amount']-$temp['refunded_amount'];
                    }
//                    $total_due = $current_plan_details['total_due'] = $row['total_due'];
//                    $paid_due = $current_plan_details['paid_due'] = $row['paid_amount'];
                    $total_due += $temp['amount'];
                    $current_plan_details['total_due'] = $total_due;
                    $paid_due += $paid_amt;
                    $current_plan_details['paid_due'] = $paid_due;
                    
                    $current_plan_details['success_count'] = $inital_stage;
                    $rem_due = $current_plan_details['rem_due'] = number_format((float)($total_due - $paid_due), 2, '.', '');
//                    $payment_details[] = $temp;
                    if ($row['schedule_status'] == 'N') {
                        $bill_due_details[] = $temp;
                    } elseif ($row['schedule_status'] == 'S') {
                        $payment_details[] = $temp;
                        if ($initial_down_pay > 0) {
                            $bill_due_details[] = $temp;
                        }
                        $initial_down_pay++;
                    } elseif ($row['schedule_status'] == 'M') {
                        $payment_details[] = $temp;
                    }
                }
                
                $studio_name = $studio_mail = $message = '';
                $query2 = sprintf("SELECT c.`company_name`, c.`email_id`, c.`referral_email_list`, e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, `event_end_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` 
                    FROM `event` e LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` 
                    LEFT JOIN `company` c ON e.`company_id` =  c.`company_id`
                    WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' AND `payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", 
                        mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                $result2 = mysqli_query($this->db, $query2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    ipn_log_info($this->json($error_log));
                    if($return_value==1){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    return;
                }else{
                    $temp_quantity = $temp_char_count = 0;
                    while($row2 = mysqli_fetch_assoc($result2)){
                        $temp2 = [];
                        $temp2['title'] = $row2['event_title'];
                        if($temp2['title']>$temp_char_count){
                            $temp_char_count = strlen($temp2['title']);
                        }
                        
                        if (!empty(trim($row2['event_begin_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_begin_dt']) {
                                $start = " - ";
                            } else {
                                $start = $row2['event_begin_dt'];

                                $split_start = explode(" ", $start);
                                $start_date_value = $split_start[0];
                                $start_time_value = $split_start[1];
                                if ("00:00:00" == $start_time_value) {
                                    $start = date('M d, Y', strtotime($start));
                                } else {
                                    $start = date('M d, Y @ h:i A', strtotime($start));
                                }
                            }
                        }
                        
                        if (!empty(trim($row2['event_end_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_end_dt']) {
                                $end = " - ";
                            } else {
                                $end = $row2['event_end_dt'];

                                $split_end = explode(" ", $end);
                                $end_date_value = $split_end[0];
                                $end_time_value = $split_end[1];
                                if ("00:00:00" == $end_time_value) {
                                    $end = date('M d, Y', strtotime($end));
                                } else {
                                    $end = date('M d, Y @ h:i A', strtotime($end));
                                }
                            }
                        }

                        $temp2['start'] = $start;
                        $temp2['end'] = $end;
                        $temp2['event_cost'] = $row2['event_cost'];
                        $temp2['quantity'] = $row2['quantity'];
                        $temp2['event_disc'] = $row2['discount'];
                        $temp2['discounted_price'] = $row2['event_cost'] - $row2['discount'];
                        $temp_quantity += $row2['quantity'];
                        $temp2['payment_amount'] = $row2['payment_amount'];
                        $temp2['paid_due'] = $temp2['payment_amount'] - $row2['balance_due'];
                        $temp2['rem_due'] = $row2['balance_due'];
                        $temp2['state'] = 'N';
                        $event_details[] = $temp2;
                        $studio_name = $row2['company_name'];
                        $studio_mail = $row2['email_id'];
                        $cc_email_list = $row2['referral_email_list'];
                    }
                    $current_plan_details['total_event'] = $temp_quantity;
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                ipn_log_info($this->json($error_log));
                if($return_value==1){
                    $error = array('status' => "Failed", "msg" => "No records found.");
                    $this->response($this->json($error), 200);
                }
                return;
            }
        }
        
        $failed_str = 'failed';
        if($checkout_state=='charged back'){
            $failed_str = 'charged back';
        }
        
        $subject = $event_name." - Payment $failed_str";
                
        $message .= "<b>The following payment for the Buyer $buyer_name $failed_str.</b><br><br>";
        $message .= "<b>Event Details</b><br><br>";
        for($i=0;$i<count($event_details);$i++){
            $message .= "Event: ".$event_details[$i]['title']."<br>Starts: ".$event_details[$i]['start']."<br>Ends: ".$event_details[$i]['end']."<br>Participant: ".$participant_name."<br><br>";
        }

        if($total_due>0){
            $message .= "<b>Billing Details</b><br><br>";
            if(count($bill_due_details)>0){
                $message .= "Total due: $".$total_due."<br>";
                $message .= "Balance due: $".$rem_due."<br>";                
                for($j=0;$j<count($bill_due_details);$j++){
                    $x= $j+1;
                    $amount = $bill_due_details[$j]['amount'];
                    $refunded_amount = $bill_due_details[$j]['refunded_amount'];
                    $final_amt = $amount-$refunded_amount;
                    $schedule_status = $bill_due_details[$j]['schedule_status'];
                    $date = date("M dS, Y", strtotime($bill_due_details[$j]['date']));
                    if($schedule_status=='N'){
                        $paid_str = "due";
                    }
                    $message .= "Bill ".$x.": $".$final_amt." ".$paid_str." ".$date."  <br>";
                }
                
                if(count($payment_details)>0){
                    $message.="<br><br><b>Payemt Details</b><br><br>";
                    $down_pay = $payment_details[0]['amount']-$payment_details[0]['refunded_amount']." paid on ".date("M dS, Y", strtotime($payment_details[0]['date']))."(".$payment_details[0]['credit_card_name'].")";
                    $message .= "Down Payment: $".$down_pay."<br>";
                    for($k=1;$k<count($payment_details);$k++){
                        $x= $k;
                        $amount = $payment_details[$k]['amount'];
                        $refunded_amount = $payment_details[$k]['refunded_amount'];
                        $final_amt = $amount-$refunded_amount;
                        $schedule_status = $payment_details[$k]['schedule_status'];
                        $date = date("M dS, Y", strtotime($payment_details[$k]['date']));
                        $paid_str = "paid on";
                        if($schedule_status=='S'){
                            $pay_type= "(".$payment_details[$k]['credit_card_name'].")";
                        }else{
                            $pay_type="(manual credit applied)";
                        }
                        $message .= "Payment ".$x.": $".$final_amt." ".$paid_str." ".$date." ".$pay_type." <br>";
                    }
                }
            }else{
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $schedule_status = $payment_details[0]['schedule_status'];
                $final_amount = $amount-$refunded_amount;
                $message .= "Total Due: $".$final_amount."<br>Amount Paid: $".$final_amount."(".$payment_details[0]['credit_card_name'].")<br>";
            }
            $message .= "<br><br>";
        }

        $sendEmail_status = $this->sendEmailForEvent('', $studio_mail, $subject, $message, '', '', $cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $studio_mail."   ".$sendEmail_status['mail_status']);
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $log = array('status' => "Success", "msg" => "Event order receipt sent successfully.");
                $this->response($this->json($log), 200);
            }
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $studio_mail);
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Event order receipt sent failed.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    protected function sendEmailForEvent($from, $to, $subject, $message, $cmp_name, $reply_to, $cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
//            if(filter_var($from, FILTER_VALIDATE_EMAIL)){
//                $mail->setFrom($from, $cmp_name);
//                $mail->AddCC($from);
//            }else{
//                $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        ipn_log_info($this->json($error_log));
        return $response;
    }
    
    private function getMembershipCheckoutDetails($checkout_id){
        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_registration_id`, mr.`membership_category_title`, mr.`membership_title`, mp.`membership_payment_id`, mp.`checkout_id`, mp.`checkout_status`, `access_token`, mp.`payment_amount`, mp.`processing_fee`, mr.`processing_fee_type`
                    FROM `membership_registration` mr 
                    LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id`=mp.`membership_registration_id` 
                    LEFT JOIN `wp_account` wp ON mr.`company_id` = wp.`company_id` 
                    WHERE mp.`checkout_id` = '%s' ORDER BY mp.`membership_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $checkout_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $membership_reg_id = $row['membership_registration_id'];
                $access_token = $row['access_token'];
                $payment_id = $row['membership_payment_id'];
                $fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_amount = $row['payment_amount'];
                $company_id = $row['company_id'];
                $membership_id = $row['membership_id'];
                $membership_option_id = $row['membership_option_id'];
                $membership_title = $desc = $row['membership_title'];
                $membership_category_title=$row['membership_category_title'];      
                $dummy_sns=[];
                $temp_password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $temp_password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                
                $json = array("checkout_id"=>"$checkout_id");
                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi('checkout','POST',$postData,$token,$this->user_agent,$dummy_sns);
                ipn_log_info("wepay_get_checkout: ".$response['status']);
                if($response['status']=='Success'){
                    $res = $response['msg'];
                    $checkout_state = $res['state'];
                    $payment_status = "";
                    $send_email = 0;
                    if($checkout_state=='expired' || $checkout_state=='failed' || $checkout_state=='charged back'){
                        $payment_status = ", `payment_status`='F'";
                        $send_email = 1;
                    }
                    
                    if($checkout_state=='released'){
                        $paid_amount = 0;
                        if($processing_fee_type==1){
                            $paid_amount = $payment_amount-$fee;
                        }elseif($processing_fee_type==2){
                            $paid_amount = $payment_amount;
                        }
                        $update_sales_in_reg = sprintf("UPDATE `membership_registration` SET `paid_amount`=`paid_amount`+$paid_amount WHERE `membership_registration_id`='%s'", mysqli_real_escape_string($this->db, $membership_reg_id));
                        $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);
                        
                        if(!$result_sales_in_reg){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                        
                        $update_count = sprintf("UPDATE `membership` SET `category_net_sales`=`category_net_sales`+$paid_amount WHERE `company_id`='%s' AND `membership_id`='%s'",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
                        $result_count = mysqli_query($this->db, $update_count);
                        
                        if(!$result_count){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                        
                        $update_count2 = sprintf("UPDATE `membership_options` SET `options_net_sales` = `options_net_sales`+$paid_amount WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
                        $result_count2 = mysqli_query($this->db, $update_count2);
                        
                        if(!$result_count2){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count2");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }  
                    }
                    
                    $update_query = sprintf("UPDATE `membership_payment` SET `checkout_status`='%s' $payment_status WHERE `membership_registration_id`='%s' and `membership_payment_id`='%s' and `checkout_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $checkout_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        ipn_log_info($this->json($error));
                        if($send_email==1){
                            $update_query2 = sprintf("UPDATE `membership_registration` SET `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'",  mysqli_real_escape_string($this->db, $membership_reg_id));
                            $update_result2 = mysqli_query($this->db, $update_query2);
                            if(!$update_result2){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query2");
                                ipn_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                            $this->responseWithMail($this->json($error), 200);
                            $this->sendFailedOrderReceiptForMembershipPayment($membership_reg_id,0);
                        }else{
                            $this->responseWithMail($this->json($error), 200);
                            $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id, $membership_category_title, $membership_title, '');
                            $this->updateMembershipDimensionsNetSales($company_id, $membership_id, '');
                        }
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
                
            }else{
                $error = array('status' => "Failed", "msg" => "Checkout details doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
    protected function sendFailedOrderReceiptForMembershipPayment($membership_reg_id, $return_value){
       
        $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
        $payment_details = [];
        $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
        $recurring_start_date = $payment_start_date = $payment_frequency = $custom_recurring_frequency_period_type = $msg1 = $msg2 = $msg3 = '';
        $membership_structure = $billing_options = $billing_options_no_of_payments = '';
        
        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, mp.`cc_name`, `waiver_policies`, mr.credit_card_name, 
            mr.`payment_start_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
            `buyer_postal_code`, mp.`checkout_status`,mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, DATE(mp.`created_dt`) created_dt, mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`
            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id`
            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N','F')
            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){                
                while($row = mysqli_fetch_assoc($result)){
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
//                    $company_id = $row['company_id'];
//                    $membership_id = $row['membership_id'];
//                    $membership_option_id = $row['membership_option_id'];
                    $payment_type = $row['payment_type'];
                    $membership_category_title = $row['membership_category_title'];
                    $membership_title = $row['membership_title'];
                    $membership_structure = $row['membership_structure'];
                    $participant_name = $row['participant_name'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $signup_fee = $row['signup_fee'];
                    $signup_fee_disc = $row['signup_fee_disc'];
                    $mem_fee = $row['membership_fee'];
                    $mem_fee_disc = $row['membership_fee_disc'];
                    $initial_payment = $row['initial_payment'];
                    $inital_processing_fee = $row['initial_processing_fee'];
                    $payment_start_date = $row['payment_start_date'];
                    $recurring_start_date = $row['recurring_start_date'];
                    $next_payment_date = $row['next_payment_date'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $recurring_amount = $row['recurring_amount'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
                    $first_payment = $row['first_payment'];
                    $billing_options = $row['billing_options'];
                    $no_of_classes = $row['no_of_classes'];
                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $credit_card_name = $row['credit_card_name'];
                    $checkout_state = $row['checkout_status'];
                    $temp=[];
                    if(!empty($row['membership_payment_id'])){
                        if($recurring_amount == $row['payment_amount']){
                            $recurring_processing_fee = $row['processing_fee'];
                        }
                        if($row['processing_fee_type']==2){
                            $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                        }else{
                            $temp['amount'] = $row['payment_amount'];
                        }
                        $temp['date'] = $row['payment_date'];
                        $temp['created_dt'] = $row['created_dt'];
                        $temp['processing_fee'] = $row['processing_fee'];
                        $temp['payment_status'] = $row['payment_status'];
                        $temp['paytime_type'] = $row['paytime_type'];
                        $temp['cc_name'] = $row['cc_name'];
                        $paid_amt += $temp['amount'];
                        $payment_details[] = $temp;
                    }
                }
                if(($membership_structure=='NC' || $membership_structure=='C') && $billing_options=='PP'){
                    if($billing_options_no_of_payments>1){
                        $already_recurring_records_count = 0;
                        $next_date = '';
                        for($i=0;$i<count($payment_details);$i++){
                            if($payment_details[$i]['paytime_type']=='R'){
                                $already_recurring_records_count++;
                                $next_date = $payment_details[$i]['date'];
                            }
                        }
                        if($billing_options_no_of_payments>$already_recurring_records_count){
                            $count = $billing_options_no_of_payments - $already_recurring_records_count;
                            $future_date = $next_date;
                            for($j=0;$j<$count;$j++){
                                if($payment_frequency=='W'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+1 weeks', strtotime($future_date)));
                                }elseif($payment_frequency=='B'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+14 days', strtotime($future_date)));
                                }elseif($payment_frequency=='M'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('next month', strtotime($future_date)));
                                }
                                
                                $temp=[];
                                if($processing_fee_type==2){
                                    $temp['amount'] = $recurring_amount+$recurring_processing_fee;
                                }else{
                                    $temp['amount'] = $recurring_amount;
                                }
                                $temp['date'] = $next_payment_date2;
                                $temp['processing_fee'] = $recurring_processing_fee;
                                $temp['payment_status'] = 'N';
                                $temp['paytime_type'] = 'R';
                                $temp['cc_name'] = $credit_card_name;
                                $payment_details[] = $temp;
                                $future_date = $next_payment_date2;
                            }
                        }
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                ipn_log_info($this->json($error_log));
                if($return_value==1){
                    $error = array('status' => "Failed", "msg" => "No records found.");
                    $this->response($this->json($error), 200);
                }
                return;
            }
        }

        $failed_str = 'failed';
        if ($checkout_state == 'charged back') {
            $failed_str = 'charged back';
        }

        $subject = $membership_category_title . " - Payment $failed_str";

        $message .= "<b>The following payment for the Buyer $buyer_name failed.</b><br><br>";
        $message .= "<b>Membership Details</b><br><br>";
        $message .= "Membership Start Date: ".date("M dS, Y", strtotime($payment_start_date))."<br>";
        $message .= "Membership: $membership_category_title, $membership_title<br>Participant: ".$participant_name."<br>";
        if($membership_structure=='NC' && $billing_options=='PP' && $billing_options_expiration_date!='0000-00-00'){
            $message .= "$no_of_classes Class Package Expires: ".date("D, M d, Y", strtotime($billing_options_expiration_date))."<br>";
        }
        $message .= "<br>";
            
        $prorated = "";
        if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
            $prorated = "(Pro-rated)";
        }

        if($paid_amt>0){
//            if($initial_payment>0){
                $message .= "<br><b>Registration Details</b><br><br>";
                $message .= "Sign-up Fee: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
                if($processing_fee_type==2){
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment+$inital_processing_fee),2))." (includes $wp_currency_symbol".$inital_processing_fee." administrative fees) $prorated<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }else{
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))." $prorated<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }
//            }
        }

        if(count($payment_details)>0){
            if($payment_frequency!='N' && $payment_type=='R'){
                $freq_str = "";
                if($membership_structure=='OE' || $membership_structure==1){
                    if($payment_frequency=='B'){
                        $freq_str = "Semi-Monthly Recurring: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "Monthly Recurring: ";
                    }elseif($payment_frequency=='W'){
                        $freq_str = "Weekly Recurring: ";
                    }elseif($payment_frequency=='A'){
                        $freq_str = "Annual Recurring: ";
                    }elseif($payment_frequency=='C'){
                        if($custom_recurring_frequency_period_type=='CW'){
                            $freq_str = "Weekly Recurring: ";
                        }elseif($custom_recurring_frequency_period_type=='CM'){
                            $freq_str = "Monthly Recurring: ";
                        }
                    }
                }elseif($billing_options=='PP'){
                    if($payment_frequency=='W'){
                        $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
                    }elseif($payment_frequency=='B'){
                        $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
                    }
                }
                
                $message .= "<br><br><b>Membership Payment Plan</b><br><br>";
                $recur_pf_str = "";
                if($processing_fee_type==2){
                    $recur_pf_str = " (plus $wp_currency_symbol".$recurring_processing_fee." administrative fees)";
                }
                if($freq_str!=""){
                    $message .= "$freq_str $wp_currency_symbol".$recurring_amount.$recur_pf_str."<br>";
                }
                $message .= "Next Payment Date: ".date("M dS, Y", strtotime($next_payment_date))."<br>";
            }
            
            $message.="<br><br><b>Payment Details</b><br><br>";
            for($init_success_payments=0;$init_success_payments<count($payment_details);$init_success_payments++){
                $pf_str = "";
                if($processing_fee_type==2){
                    $pf_str = " (includes $wp_currency_symbol".$payment_details[$init_success_payments]['processing_fee']." administrative fees)";
                }
                if($initial_payment>0 && $payment_details[$init_success_payments]['paytime_type']=='I'){
                    $down_pay = $payment_details[$init_success_payments]['amount'].$pf_str." paid on ".date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']))." (".$payment_details[$init_success_payments]['cc_name'].") " .$prorated;
                    $msg1 .= "Payment: $wp_currency_symbol".$down_pay."<br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='F'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    if($payment_details[$init_success_payments]['payment_status']=='S'){
                        $paid_str = "paid on";
                        $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    }else{
                        $paid_str = "due";
                        $pay_type= "";
                    }
                    $msg2 .= "First Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='S'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "paid on";
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='N' && ($membership_structure=='NC' || $membership_structure=='C') && $billing_options=='PP'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "due";
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
            }
            $message .= $msg1.$msg2.$msg3;
            $message .= "<br><br><br>";
        }
        
        $sendEmail_status = $this->sendEmailForMembership('',$studio_mail, $subject, $message, '', '', $cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $log = array('status' => "Success", "msg" => "Membership order receipt sent successfully.");
                $this->response($this->json($log), 200);
            }
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Membership order receipt sent failed.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    protected function sendEmailForMembership($from, $to, $subject, $message, $cmp_name, $reply_to, $cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
//            if(filter_var($from, FILTER_VALIDATE_EMAIL)){
//                $mail->setFrom($from, $cmp_name);
//                $mail->AddCC($from);
//            }else{
//                $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        ipn_log_info($this->json($error_log));
        return $response;
    }

    protected function clean($string) {
        $str = preg_replace('/\s+/', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    }
    
    protected function updateEvent(){
        $sql = sprintf("SELECT e.event_id, e.event_title, e.company_id, c.company_code FROM `event` e LEFT JOIN `company` c ON e.company_id = c.company_id WHERE `event_type` IN ('M','S')");
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            while($output = mysqli_fetch_assoc($result)){
                $company_id = $output['company_id'];
                $company_code = $output['company_code'];
                $event_id = $output['event_id'];
                $event_title = $output['event_title'];

                $company_code_new = $this->clean($company_code);
                $event_title_new = $this->clean($event_title);
                $event_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/e/?=$company_code_new/$company_id/$event_id";

                $sql3 = sprintf("UPDATE `event` SET `event_url` = '%s' WHERE `event_id` = '%s' ", mysqli_real_escape_string($this->db, $event_url), mysqli_real_escape_string($this->db, $event_id));
                $result3 = mysqli_query($this->db, $sql3);
                if (!$result3) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                    ipn_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }
    }
    
    protected function updateEventWaiver(){
        $count=0;
//        $sql = sprintf("SELECT event_id, company_id, waiver_policies, 'E' as type FROM `event` WHERE waiver_policies!='' UNION
//                    SELECT membership_option_id, company_id, waiver_policies, 'M' as type FROM membership_options WHERE waiver_policies!=''");
        $sql = sprintf("SELECT event_id, company_id, waiver_policies, 'E' as type FROM `event` WHERE waiver_policies!=''");
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($output = mysqli_fetch_assoc($result)){
                    $company_id = $output['company_id'];
                    $type = $output['type'];
                    $event_id = $output['event_id'];
                    $waiver_policies = urldecode($output['waiver_policies']);

                    if($type=='E'){
                        $sql2 = sprintf("UPDATE `event` SET `waiver_policies` = '%s' WHERE company_id='%s' AND `event_id` = '%s' ", mysqli_real_escape_string($this->db, $waiver_policies), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
                    }else{
                        $sql2 = sprintf("UPDATE `membership_options` SET `waiver_policies` = '%s' WHERE company_id='%s' AND `membership_option_id` = '%s' ", mysqli_real_escape_string($this->db, $waiver_policies), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
                    }
                    $result2 = mysqli_query($this->db, $sql2);
                    if (!$result2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $count++;
                    }
                }
                $error = array('status' => "Success", "msg" => "$count records updated.");
                $this->response($this->json($error), 500);
            }else{
                $error = array('status' => "Failed", "msg" => "No records found.");
                $this->response($this->json($error), 500);
            }
        }
    }
//    
//    protected function addMembershipDimensions(){
//        $start = 1; $end = 1000;
//        if(isset($_REQUEST['start'])){
//            $start = $_REQUEST['start'];
//        }
//        if(isset($_REQUEST['end'])){
//            $end = $_REQUEST['end'];
//        }
//        $query1 = sprintf("SELECT m.company_id, m.membership_id, m.category_title, m.created_dt m_cd, mo.membership_option_id, mo.membership_title, mo.created_dt mo_cd
//                FROM `membership` m LEFT JOIN `membership_options` mo ON m.`membership_id` = mo.`membership_id` AND m.`company_id`=mo.`company_id`
//                WHERE m.`company_id` between $start and $end");
//        $result1 = mysqli_query($this->db, $query1);
//        
//        if(!$result1){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
//            ipn_log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 500);
//        }else{
//            $category = $option = $membership_id_arr = $membership_options_id_arr = [];
//            $num_rows = mysqli_num_rows($result1);
//            if($num_rows>0){
//                while($row = mysqli_fetch_assoc($result1)){
//                    $temp = $temp2 = [];
//                    $temp['company_id'] = $temp2['company_id'] = $company_id = $row['company_id'];
//                    $temp['category_id'] = $temp2['category_id'] = $membership_id = $row['membership_id'];
//                    $temp['category_title'] = $temp2['category_title'] = $category_title = $row['category_title'];
//                    $temp['m_cd'] = $row['m_cd'];
//                    $temp2['option_id'] = $membership_option_id = $row['membership_option_id'];
//                    $temp2['option_title'] = $option_title = $row['membership_title'];
//                    $temp2['mo_cd'] = $row['mo_cd'];
//                    
//                    if(!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)){
//                        $membership_id_arr[] = $membership_id;
//                        $category[] = $temp;
//                    }
//                    if(!empty($membership_option_id) && !in_array($membership_option_id, $membership_options_id_arr, true)){
//                        $membership_options_id_arr[] = $membership_option_id;
//                        $option[] = $temp2;
//                    }
//                }
//                for($i=0;$i<count($category);$i++){
//                    $date_init = 0;
//                    for($j=0;$j<1;$j++){
//                        $date_str = "%Y-%m";
//                        $curr_dt = date("Y-m-d");
//                        $every_month_dt = date("Y-m-d", strtotime("first day of +$date_init month", strtotime($category[$i]['m_cd'])));
//                        if($every_month_dt=='0000-00-00' || $every_month_dt=='1970-01-01'){
//                            $every_month_dt = $curr_dt;
//                        }
//                        if($every_month_dt>$curr_dt){
//                            continue;
//                        }
//                        $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')",
//                                $category[$i]['company_id'], $category[$i]['category_id'], $every_month_dt, $date_str);                            
//                        $res1 = mysqli_query($this->db, $sql1);
//                        if(!$res1){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//                            ipn_log_info($this->json($error_log));
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 500);
//                        }else{
//                            if($curr_dt>$every_month_dt){
//                                $date_init++;
//                                $j--;
//                            }
//                            $num_of_rows = mysqli_num_rows($res1);
//                            if($num_of_rows==0){
//                                $sql2 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `category_title`) VALUES('%s', DATE_FORMAT('%s', '%s'),'%s','%s')",
//                                        $category[$i]['company_id'], $every_month_dt, $date_str, $category[$i]['category_id'], mysqli_real_escape_string($this->db, $category[$i]['category_title']));
//                                $res2 = mysqli_query($this->db, $sql2);
//                                if(!$res2){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                                    ipn_log_info($this->json($error_log));
//                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                    $this->response($this->json($error), 500);
//                                }
//                                
//                            }
//                        }
//                    }
//                }
//                for($i=0;$i<count($option);$i++){
//                    $date_init = 0;
//                    for($j=0;$j<1;$j++){
//                        $date_str = "%Y-%m";
//                        $curr_dt = date("Y-m-d");
//                        $every_month_dt = date("Y-m-d", strtotime("first day of +$date_init month", strtotime($option[$i]['mo_cd'])));
//                        if($every_month_dt=='0000-00-00' || $every_month_dt=='1970-01-01'){
//                            $every_month_dt = $curr_dt;
//                        }
//                        if($every_month_dt>$curr_dt){
//                            continue;
//                        }
//                        $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')",
//                                $option[$i]['company_id'], $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);                            
//                        $res1 = mysqli_query($this->db, $sql1);
//                        if(!$res1){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//                            ipn_log_info($this->json($error_log));
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 500);
//                        }else{
//                            if($curr_dt>$every_month_dt){
//                                $date_init++;
//                                $j--;
//                            }
//                        
//                            $num_of_rows = mysqli_num_rows($res1);
//                            if($num_of_rows==0){
//                                $sql2 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`) VALUES('%s', DATE_FORMAT('%s', '%s'),'%s','%s','%s','%s')",
//                                        $option[$i]['company_id'], $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id'], mysqli_real_escape_string($this->db, $option[$i]['category_title']), mysqli_real_escape_string($this->db, $option[$i]['option_title']));
//                                $res2 = mysqli_query($this->db, $sql2);
//                                if(!$res2){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                                    ipn_log_info($this->json($error_log));
//                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                    $this->response($this->json($error), 500);
//                                }
//                                
//                            }
//                        }
//                    }
//                }
//            }else{
//                $error_log = array('status' => "Failed", "msg" => "No records.", "query" => "$query1");
//                ipn_log_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "No records.");
//                $this->response($this->json($error), 204);
//            }
//        }
//    }
//    
//    protected function updateMembershipDimensionsNetSales(){
//        $start = 1;
//        $end = 1000;
//        if(isset($_REQUEST['start'])){
//            $start = $_REQUEST['start'];
//        }
//        if(isset($_REQUEST['end'])){
//            $end = $_REQUEST['end'];
//        }
//        $query1 = sprintf("SELECT mr.company_id, mr.membership_id, mr.membership_option_id, mp.payment_date mp_ud, sum(if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
//                  mp.payment_date FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
//                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
//                  WHERE mr.company_id between $start and $end AND mp.payment_status='S' GROUP BY mr.`membership_id`, mr.`membership_option_id`, month(mp.payment_date)");
//        $result1 = mysqli_query($this->db, $query1);
//        
//        if(!$result1){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
//            ipn_log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 500);
//        }else{
//            $category = $option = $membership_id_arr = $membership_options_id_arr = [];
//            $num_rows = mysqli_num_rows($result1);
//            if($num_rows>0){
//                while($row = mysqli_fetch_assoc($result1)){
//                    $temp = $temp2 = [];
//                    $temp['company_id'] = $temp2['company_id'] = $company_id = $row['company_id'];
//                    $temp['category_id'] = $temp2['category_id'] = $membership_id = $row['membership_id'];
//                    $temp2['option_id'] = $membership_option_id = $row['membership_option_id'];
//                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
//                    $temp2['amount'] = $amount = $row['amount'];
//                    $temp2['mp_ud'] = $row['mp_ud'];
//                    
//                    if(!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)){
//                        $membership_id_arr[] = $membership_id;
//                        $category[] = $temp;
//                    }
////                    $category[count($category)-1]['amount'] = 
////                    if(!empty($membership_option_id) && !in_array($membership_option_id, $membership_options_id_arr, true)){
////                        $membership_options_id_arr[] = $membership_option_id;
//                        $option[] = $temp2;
////                    }
//                }
//                for($i=0;$i<count($option);$i++){
//                    $date_init = 0;
////                    for($j=0;$j<1;$j++){
//                        $date_str = "%Y-%m";
//                        $curr_dt = date("Y-m-d");
//                        $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
////                        if($every_month_dt=='0000-00-00' || $every_month_dt=='1970-01-01'){
////                            $every_month_dt = $curr_dt;
////                        }
////                        if($every_month_dt>$curr_dt){
////                            continue;
////                        }
////                        if($option[$i]['amount']==0){
//////                            $date_init++;
//////                            $j--;
////                            continue;
////                        }
//                        $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')",
//                                $option[$i]['company_id'], $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);                            
//                        $res1 = mysqli_query($this->db, $sql1);
//                        if(!$res1){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//                            ipn_log_info($this->json($error_log));
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 500);
//                        }else{
////                            if($curr_dt>$every_month_dt){
////                                $date_init++;
////                                $j--;
////                            }
//                        
//                            $num_of_rows = mysqli_num_rows($res1);
//                            if($num_of_rows==1){
//                                
//                                $sql2 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`='%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='%s'",
//                                        $option[$i]['amount'], $option[$i]['company_id'], $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id']);
//                                $res2 = mysqli_query($this->db, $sql2);
//                                if(!$res2){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                                    ipn_log_info($this->json($error_log));
//                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                    $this->response($this->json($error), 500);
//                                }else{
//                                    $sql3 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=`sales_amount`+'%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='0'",
//                                        $option[$i]['amount'], $option[$i]['company_id'], $every_month_dt, $date_str, $option[$i]['category_id']);
//                                    $res3 = mysqli_query($this->db, $sql3);
//                                    if(!$res3){
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
//                                        ipn_log_info($this->json($error_log));
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                        $this->response($this->json($error), 500);
//                                    }
//                                }
//                                
//                            }
//                        }
////                    }
//                }
//            }else{
//                $error_log = array('status' => "Failed", "msg" => "No records.", "query" => "$query1");
//                ipn_log_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "No records.");
//                $this->response($this->json($error), 204);
//            }
//        }
//    }
//    
//    protected function updateMembershipDimensionsMembers(){
//        $start = 1;
//        $end = 1000;
//        if(isset($_REQUEST['start'])){
//            $start = $_REQUEST['start'];
//        }
//        if(isset($_REQUEST['end'])){
//            $end = $_REQUEST['end'];
//        }
//        $date_str = "%Y-%m";
//        $first_date_str = "%Y-%m-01";
//        
//        $query1 = sprintf("SELECT 'New' as type, count(*) total, DATE_FORMAT(created_dt,'%s') as month, company_id, membership_id, membership_option_id, DATE_FORMAT(created_dt,'%s') as date  FROM membership_registration 
//                    WHERE company_id between $start and $end AND created_dt>=DATE_FORMAT(CURDATE(), '%s') - INTERVAL 3 MONTH GROUP BY membership_id, membership_option_id, month(created_dt)
//                    UNION
//                    SELECT 'Pause' as type, count(*) total, DATE_FORMAT(hold_date,'%s') as month, company_id, membership_id, membership_option_id, DATE_FORMAT(hold_date,'%s') as date FROM membership_registration 
//                    WHERE company_id between $start and $end AND hold_date>=DATE_FORMAT(CURDATE(), '%s') - INTERVAL 3 MONTH GROUP BY membership_id, membership_option_id, month(hold_date)
//                    UNION
//                    SELECT 'Cancel' as type, count(*) total, DATE_FORMAT(cancelled_date,'%s') as month, company_id, membership_id, membership_option_id, DATE_FORMAT(cancelled_date,'%s') as date FROM membership_registration 
//                    WHERE company_id between $start and $end AND cancelled_date>=DATE_FORMAT(CURDATE(), '%s') - INTERVAL 3 MONTH GROUP BY membership_id, membership_option_id, month(cancelled_date)  
//                    UNION
//                    SELECT 'Active' as type, count(*) total, DATE_FORMAT(created_dt,'%s') as month, company_id, membership_id, membership_option_id, DATE_FORMAT(created_dt,'%s') as date FROM `membership_registration` 
//                    WHERE company_id between $start and $end AND `membership_status`='R' GROUP BY membership_id, membership_option_id, month(created_dt) 
//                    ORDER BY membership_id, membership_option_id, month", $date_str, $first_date_str, $first_date_str, $date_str, $first_date_str, $first_date_str, $date_str, $first_date_str, $first_date_str, $date_str, $first_date_str);
//        $result1 = mysqli_query($this->db, $query1);
//        
//        if(!$result1){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
//            ipn_log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 500);
//        }else{
//            $membership_options_id_arr = $option = $new = $pause = $cancel = $active = [];
//            $num_rows = mysqli_num_rows($result1);
//            if($num_rows>0){
//                while($row = mysqli_fetch_assoc($result1)){
//                    $temp = [];
//                    $temp['company_id'] = $row['company_id'];
//                    $temp['category_id'] = $membership_id = $row['membership_id'];
//                    $temp['option_id'] = $membership_option_id = $row['membership_option_id'];
//                    $temp['month'] = $row['month'];
//                    $temp['type'] = $row['type'];
//                    $temp['total'] = $row['total'];
//                    $temp['date'] = $row['date'];
//                    
//                    if($temp['type']=='Active'){
//                        $active[] = $temp;
//                    }elseif($temp['type']=='New'){
//                        $new[] = $temp;
//                    }elseif($temp['type']=='Pause'){
//                        $pause[] = $temp;
//                    }elseif($temp['type']=='Cancel'){
//                        $cancel[] = $temp;
//                    }
//                }
//                $opt_array = [$active, $new, $pause, $cancel];
//                $col_arr = ['active_members','reg_new','reg_hold','reg_cancelled'];
//                for($k=0;$k<4;$k++){
//                    $option = $opt_array[$k];
//                    $col_str = $col_arr[$k];
//                    for($i=0;$i<count($option);$i++){
//                        
//                        $every_month_dt = date("Y-m-d", strtotime($option[$i]['date']));
//
//                        $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')",
//                                $option[$i]['company_id'], $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);                            
//                        $res1 = mysqli_query($this->db, $sql1);
//                        if(!$res1){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//                            ipn_log_info($this->json($error_log));
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 500);
//                        }else{
//                            $num_of_rows = mysqli_num_rows($res1);
//                            if($num_of_rows==1){
//                                $sql2 = sprintf("UPDATE `membership_dimensions` SET $col_str='%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='%s'",
//                                        $option[$i]['total'], $option[$i]['company_id'], $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id']);
//                                $res2 = mysqli_query($this->db, $sql2);
//                                if(!$res2){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                                    ipn_log_info($this->json($error_log));
//                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                    $this->response($this->json($error), 500);
//                                }else{
//                                    $sql3 = sprintf("UPDATE `membership_dimensions` SET $col_str=$col_str+'%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`=0",
//                                        $option[$i]['total'], $option[$i]['company_id'], $every_month_dt, $date_str, $option[$i]['category_id']);
//                                    $res3 = mysqli_query($this->db, $sql3);
//                                    if(!$res3){
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
//                                        ipn_log_info($this->json($error_log));
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                        $this->response($this->json($error), 500);
//                                    }
//                                }
//
//                            }
//                        }
//                    }
//                }
//            }else{
//                $error_log = array('status' => "Failed", "msg" => "No records.", "query" => "$query1");
//                ipn_log_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "No records.");
//                $this->response($this->json($error), 204);
//            }
//        }
//    }
    
    
    
    public function addMembershipDimensions($company_id, $membership_id, $mem_option_id, $category_title, $option_title, $period) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($mem_option_id)) {
            $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                ipn_log_info($this->json($error_log));
                $reg_id = 1;
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $selectsql = sprintf("SELECT `active_members` from membership_dimensions where company_id='%s' and `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s' ",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $pre_dt));
                    $resultselectsql = mysqli_query($this->db, $selectsql);
                    if (!$resultselectsql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        ipn_log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
                        $num_of_rows2 = mysqli_num_rows($resultselectsql);
                        if ($num_of_rows2 == 0) {
                            $sqlselect1=sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and `membership_option_id`='%s' and  `company_id`= '%s'",  mysqli_real_escape_string($this->db,$membership_id),mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db,$company_id));
                            $resultsqlselect1=mysqli_query($this->db, $sqlselect1);
                            $value2 = mysqli_fetch_object($resultsqlselect1);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id),
                                    mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), mysqli_real_escape_string($this->db,$value2->active));
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                ipn_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        } else {
                            $value = mysqli_fetch_object($resultselectsql);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id),
                                    mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), $value->active_members);
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                ipn_log_info($this->json($error_log));
                                $reg_id = 1;
                            }
                        }
                    }
                }
            }
        }

        $sql2 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            ipn_log_info($this->json($error_log));
            $reg_id = 1;
        } else{
            $num_of_rows3 = mysqli_num_rows($result2);
            if ($num_of_rows3 == 0) {
                $selectsql2 = sprintf("SELECT `active_members` FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $pre_dt));
                $resultselectsql2 = mysqli_query($this->db, $selectsql2);
                if (!$resultselectsql2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql2");
                    ipn_log_info($this->json($error_log));
                    $reg_id = 1;
                } else {
                    $num_of_rows4 = mysqli_num_rows($resultselectsql2);
                    if ($num_of_rows4 == 0) {
                        $sqlselect2 = sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and  `company_id`= '%s'", mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$company_id));
                        $resultsqlselect2 = mysqli_query($this->db, $sqlselect2);
                        $value3 = mysqli_fetch_object($resultsqlselect2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $value3->active));
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            ipn_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    } else {
                        $value1 = mysqli_fetch_object($resultselectsql2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), $value1->active_members);
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            ipn_log_info($this->json($error_log));
                            $reg_id = 1;
                        }
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for addMembershipDimensions(), check log";
            $this->sendEmailForAdmins($msg);
        }
        
    }
    
    public function updateMembershipDimensionsNetSales($company_id, $membership_id, $date) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $mp_last_upd_date=" DATE(CONVERT_TZ(mp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $last_upd_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";
        if(empty($date)){
            $curr_dt = "($currdate_add)";
        }else{
            $curr_dt = "'$date'";
        }
//        $sql = sprintf("SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, sum(if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
//                  mp.payment_date FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
//                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
//                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  MONTH( $mp_last_upd_date) = MONTH($curr_dt)
//                  AND YEAR($mp_last_upd_date) = YEAR($curr_dt) AND mp.payment_status='S' AND mp.checkout_status='released' GROUP BY mr.`membership_id`, mr.`membership_option_id`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
        $date_inc1= "DATE_FORMAT($curr_dt,'%Y-%m')";
        $date_inc2= "DATE_FORMAT($mp_last_upd_date,'%Y-%m')";
        $date_inc3= "DATE_FORMAT($last_upd_date,'%Y-%m')";
        
        $sql = sprintf("SELECT company_id, membership_id, membership_option_id, mp_ud, sum(amount) amount, payment_date FROM (

                  SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount) amount, 
                  mp.payment_date, mp.membership_payment_id FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND (mp.payment_status='S' AND mp.checkout_status='released' || mp.payment_status IN ('MF') || mp.payment_status='M' AND mp.`credit_method`!='MC')
                  AND %s = %s 
                  
                  UNION
                  
                  SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, concat('-',if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
                  mp.payment_date, mp.membership_payment_id FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  (mp.`payment_status`='R' 
                  AND IF(mr.registration_from='S', mp.payment_intent_id NOT IN (SELECT payment_intent_id FROM membership_payment WHERE payment_status='FR' AND %s = %s),
                  mp.checkout_id NOT IN (SELECT checkout_id FROM membership_payment WHERE payment_status='FR' AND %s = %s)) || mp.`payment_status`='MR')
                  
                  AND %s = %s
                  )t1
                  GROUP BY t1.`membership_id`, t1.`membership_option_id`"

                 , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id),
                 $date_inc2, $date_inc1, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id),
                 $date_inc3, $date_inc1, $date_inc3, $date_inc1, $date_inc2, $date_inc1);
        
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            ipn_log_info($this->json($error_log));
            $reg_id = 1;
        } else {
            $category = $membership_id_arr = $membership_options_id_arr = [];
            $number_rows = mysqli_num_rows($result);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $temp = $temp2 = [];
                    $temp['category_id'] = $temp2['category_id'] = $membership_id = $row['membership_id'];
                    $temp2['option_id'] = $membership_option_id = $row['membership_option_id'];
                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
                    $temp2['amount'] = $amount = $row['amount'];
                    $temp2['mp_ud'] = $row['mp_ud'];

                    if (!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)) {
                        $membership_id_arr[] = $membership_id;
                        $category[] = $temp;
                    }
                    $option[] = $temp2;
                }

                for ($i = 0; $i < count($option); $i++) {
                    $date_init = 0;
                   
                    $date_str = "%Y-%m";
                    $curr_dt = date("Y-m-d");
                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
//                     
                    $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')", mysqli_real_escape_string($this->db, $company_id), $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);
                    $res1 = mysqli_query($this->db, $sql1);
                    if (!$res1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        ipn_log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
//                           

                        $num_of_rows = mysqli_num_rows($res1);
                        if ($num_of_rows == 1) {

                            $sql2 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`='%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='%s'", $option[$i]['amount'], mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id']);
                            $res2 = mysqli_query($this->db, $sql2);
                            if (!$res2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                ipn_log_info($this->json($error_log));
                                $reg_id = 1;
                            } else {
                                if($i==0){
                                    $sales_value = "'%s'";
                                }else{
                                    $sales_value = "`sales_amount`+'%s'";
                                }
                                $sql3 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='0'",mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str,mysqli_real_escape_string($this->db, $membership_id));
                                $res3 = mysqli_query($this->db, $sql3);
                                if (!$res3) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                                    ipn_log_info($this->json($error_log));
                                    $reg_id = 1;
                                }
                            }
                        }
                    }
                }
            } else {
                $sql2 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=0 WHERE `company_id`='%s' AND `period`=%s AND `category_id`='%s'", mysqli_real_escape_string($this->db, $company_id),$date_inc1, $membership_id);
                $res2 = mysqli_query($this->db, $sql2);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    ipn_log_info($this->json($error_log));
//                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                $this->response($this->json($error), 200);
                }
                $error = array('status' => "Failed", "msg" => "No records.", "query" => "$sql");
                ipn_log_info($this->json($error));
//                $error = array('status' => "Failed", "msg" => "No records.","query" => "$sql");
//                $this->response($this->json($error), 204);
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for updateMembershipDimensionsNetSales(), check log";
            $this->sendEmailForAdmins($msg);
        }
    }
    
    private function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone='';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
        } else {
            $num_rows = mysqli_num_rows($result_tz);                       
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    private function sendEmailForAdmins($msg){
        $cmp_name = "MyStudio WepayIpn";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "Wepay Ipn Handling Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        ipn_log_info($this->json($error_log));
        return $response;
    }
    
    private function gettrialCheckoutDetails($checkout_id){
        $query = sprintf("SELECT tr.`company_id`, tr.`trial_id`, tr.`trial_reg_id`, t.trial_title, tp.`trial_payment_id`, tp.`checkout_id`, tp.`checkout_status`, `access_token`, tp.`payment_amount`, tp.`processing_fee`, tr.`processing_fee_type`
                    FROM `trial_registration` tr 
                    LEFT JOIN `trial_payment` tp ON tr.`trial_reg_id`=tp.`trial_reg_id`
                    LEFT JOIN `trial` t ON t.`trial_id`=tr.`trial_id`
                    LEFT JOIN `wp_account` wp ON tr.`company_id` = wp.`company_id` 
                    WHERE tp.`checkout_id` = '%s' ORDER BY tp.`trial_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $checkout_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $trial_reg_id = $row['trial_reg_id'];
                $access_token = $row['access_token'];
                $payment_id = $row['trial_payment_id'];
                $fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_amount = $row['payment_amount'];
                $company_id = $row['company_id'];
                $trial_id = $row['trial_id'];
                $trial_title = $desc = $row['trial_title'];
                   
                
                $temp_password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $temp_password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                $dummy_sns=[];
                $json = array("checkout_id"=>"$checkout_id");
                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi('checkout','POST',$postData,$token,$this->user_agent,$dummy_sns);
                ipn_log_info("wepay_get_checkout: ".$response['status']);
                if($response['status']=='Success'){
                    $res = $response['msg'];
                    $checkout_state = $res['state'];
                    $payment_status = "";
                    $send_email = 0;
                    if($checkout_state=='expired' || $checkout_state=='failed' || $checkout_state=='charged back'){
                        $payment_status = ", `payment_status`='F'";
                        $send_email = 1;
                    }
                    
                    if($checkout_state=='released'){
                        $paid_amount = 0;
                        if($processing_fee_type==1){
                            $paid_amount = $payment_amount-$fee;
                        }elseif($processing_fee_type==2){
                            $paid_amount = $payment_amount;
                        }
                        $update_sales_in_reg = sprintf("UPDATE `trial_registration` SET `paid_amount`=`paid_amount`+$paid_amount WHERE `trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_id));
                        $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);
                        
                        if(!$result_sales_in_reg){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                        
                        $update_count = sprintf("UPDATE `trial` SET `net_sales`=`net_sales`+$paid_amount WHERE `company_id`='%s' AND `trial_id`='%s'",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
                        $result_count = mysqli_query($this->db, $update_count);
                        
                        if(!$result_count){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                     }
                    $update_query = sprintf("UPDATE `trial_payment` SET `checkout_status`='%s' $payment_status WHERE `trial_reg_id`='%s' and `trial_payment_id`='%s' and `checkout_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $checkout_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        ipn_log_info($this->json($error));
                        if($send_email==1){
                           $this->responseWithMail($this->json($error), 200);
                        }else{
                            $this->responseWithMail($this->json($error), 200);
                            $this->updateTrialDimensionsNetSales($company_id,$trial_id,'');
                        }
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
                
            }else{
                $error = array('status' => "Failed", "msg" => "Checkout details doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
     public function updateTrialDimensionsNetSales($company_id, $trial_id, $date) {
        $reg_id = 0;
        $sql_add_dim = sprintf("SELECT `trial_id` FROM `trial_registration` WHERE `trial_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $company_id));
        $result_add_dim = mysqli_query($this->db, $sql_add_dim);
        if (!$result_add_dim) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_add_dim");
            log_info($this->json($error_log));
        } else {
            $number_rows = mysqli_num_rows($result_add_dim);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_add_dim)) {
                     $trial_id = $row['trial_id'];
                }
                $this->addTrialDimensions($company_id, $trial_id, '');
            }
        }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $mp_last_upd_date=" DATE(CONVERT_TZ(tp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $last_upd_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";
        if(empty($date)){
            $curr_dt = "($currdate_add)";
        }else{
            $curr_dt = "'$date'";
        }
//        $sql = sprintf("SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, sum(if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
//                  mp.payment_date FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
//                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
//                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  MONTH( $mp_last_upd_date) = MONTH($curr_dt)
//                  AND YEAR($mp_last_upd_date) = YEAR($curr_dt) AND mp.payment_status='S' AND mp.checkout_status='released' GROUP BY mr.`membership_id`, mr.`membership_option_id`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
        $date_inc1= "DATE_FORMAT($curr_dt,'%Y-%m')";
        $date_inc2= "DATE_FORMAT($mp_last_upd_date,'%Y-%m')";
        $date_inc3= "DATE_FORMAT($last_upd_date,'%Y-%m')";
        
        $sql = sprintf("SELECT company_id, trial_id,  mp_ud, sum(amount) amount, payment_date FROM (

                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND (tp.payment_status='S' AND tp.checkout_status='released' || tp.payment_status IN ('MF') || tp.payment_status='M' AND tp.`credit_method`!='MC')
                  AND %s = %s 
                  
                  UNION
                  
                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, concat('-',if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount)) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND  (tp.`payment_status`='R' 
                  AND IF(tr.registration_from='S', tp.payment_intent_id NOT IN (SELECT payment_intent_id FROM trial_payment WHERE payment_status='FR' AND %s = %s),
                  tp.checkout_id NOT IN (SELECT checkout_id FROM trial_payment WHERE payment_status='FR' AND %s = %s)) || tp.`payment_status`='MR')
                  AND %s = %s
                  )t1
                  GROUP BY t1.`trial_id`"

                 , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc2, $date_inc1, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc3, $date_inc1, $date_inc3, $date_inc1, $date_inc2, $date_inc1);
        
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            ipn_log_info($this->json($error_log));
            $reg_id = 1;
        } else {
            $trial = $trial_id_arr = [];
            $number_rows = mysqli_num_rows($result);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $temp = $temp2 = [];
                    $temp['trial_id'] = $temp2['trial_id'] = $trial_id = $row['trial_id'];
                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
                    $temp2['amount'] = $amount = $row['amount'];
                    $temp2['mp_ud'] = $row['mp_ud'];

                    if (!empty($trial_id) && !in_array($trial_id, $trial_id_arr, true)) {
                        $trial_id_arr[] = $trial_id;
                        $trial[] = $temp;
                    }
                    $option[] = $temp2;
                }

                for ($i = 0; $i < count($option); $i++) {
                    $date_init = 0;
                   
                    $date_str = "%Y-%m";
                    $curr_dt = date("Y-m-d");
                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
//                     
                    $sql1 = sprintf("SELECT * FROM `trial_dimensions` WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')", mysqli_real_escape_string($this->db, $company_id), $option[$i]['trial_id'], $every_month_dt, $date_str);
                    $res1 = mysqli_query($this->db, $sql1);
                    if (!$res1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        ipn_log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
//                           

                        $num_of_rows = mysqli_num_rows($res1);
                        if ($num_of_rows == 1) {

                            if($i==0){
                                    $sales_value = "'%s'";
                                }else{
                                    $sales_value = "`sales_amount`+'%s'";
                                }
                                $sql3 = sprintf("UPDATE `trial_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `trial_id`='%s' ",mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str,mysqli_real_escape_string($this->db, $trial_id));
                                $res3 = mysqli_query($this->db, $sql3);
                                if (!$res3) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                                    ipn_log_info($this->json($error_log));
                                    $reg_id = 1;
                                }
                           
                        }
                    }
                }
            } else {
                $sql3 = sprintf("UPDATE `trial_dimensions` SET `sales_amount`=0 WHERE `company_id`='%s' AND `period`=%s AND `trial_id`='%s' ",mysqli_real_escape_string($this->db, $company_id), $date_inc1, mysqli_real_escape_string($this->db, $trial_id));
                $res3 = mysqli_query($this->db, $sql3);
                if (!$res3) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                    log_info($this->json($error_log));
                }
                $error = array('status' => "Failed", "msg" => "No records.", "query" => "$sql");
                ipn_log_info($this->json($error));
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for updateTrialDimensionsNetSales(), check log";
            $this->sendEmailForAdmins($msg);
        }
    }
    private function getretailCheckoutDetails($checkout_id){
        $retail_arr=[];
        $query = sprintf("SELECT rto.`company_id`,rod.`retail_order_detail_id`,rod.`retail_payment_amount`, rod.`retail_product_id`, rto.`retail_order_id`,rod.`retail_parent_id`,rod.`rem_due`, rp.`retail_payment_id`, rp.`checkout_id`, rp.`checkout_status`, `access_token`, rp.`payment_amount`, rp.`processing_fee`, rto.`processing_fee_type`,rto.`retail_discount_id`,rod.`retail_tax_percentage`,rod.`retail_discount`
                    FROM `retail_orders` rto 
                    LEFT JOIN `retail_payment` rp ON rto.`retail_order_id`=rp.`retail_order_id`
                    LEFT JOIN `retail_order_details` rod ON rto.`retail_order_id`=rod.`retail_order_id`
                    LEFT JOIN `wp_account` wp ON rto.`company_id` = wp.`company_id` 
                    WHERE rp.`checkout_id` = '%s' AND rp.`payment_status` IN ('S', 'PR') ORDER BY rp.`retail_payment_id`", mysqli_real_escape_string($this->db, $checkout_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $k=0;
                while($row = mysqli_fetch_assoc($result)){
                    $retail_order_id = $row['retail_order_id'];
                    $access_token = $row['access_token'];
                    $payment_id = $row['retail_payment_id'];
                    $retail_total_processing_fee = $fee = $row['processing_fee'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $payment_amount = $row['payment_amount'];
                    $company_id = $row['company_id'];
//                    $retail_discount_id = $row['retail_discount_id'];
                    $retail_arr[$k]['retail_order_detail_id'] = $row['retail_order_detail_id'];
                    $retail_arr[$k]['retail_product_id'] = $row['retail_product_id'];
                    $retail_arr[$k]['retail_parent_id'] = $row['retail_parent_id'];
                    $retail_arr[$k]['retail_price'] = $row['retail_payment_amount'];
                    $retail_arr[$k]['retail_tax_percentage'] = $row['retail_tax_percentage'];
                    $retail_arr[$k]['retail_discount'] = $row['retail_discount'];
                    $retail_arr[$k]['rem_due'] = $row['rem_due'];
                    $k++;
                }
                
                $temp_password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $temp_password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                $dummy_sns=[];
                $json = array("checkout_id"=>"$checkout_id");
                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi('checkout','POST',$postData,$token,$this->user_agent,$dummy_sns);
                ipn_log_info("wepay_get_checkout: ".$response['status']);
                if($response['status']=='Success'){
                    $res = $response['msg'];
                    $checkout_state = $res['state'];
                    $payment_status = "";
                    $send_email = 0;
                    if($checkout_state=='expired' || $checkout_state=='failed' || $checkout_state=='charged back'){
                        $payment_status = ", `payment_status`='F'";
                        $send_email = 1;
                    }
                    
                    if($checkout_state=='released'){
                        $update_amount = 0;
                        $paid_amount = $payment_amount;
                        if($processing_fee_type==1){
                            $total_amount = $update_amount = $payment_amount;
                        }elseif($processing_fee_type==2){
                            $total_amount = $update_amount = $payment_amount - $fee;
                        }
                        $update_sales_in_reg = sprintf("UPDATE `retail_orders` SET `retail_order_paid_amount`=`retail_order_paid_amount`+$paid_amount WHERE `retail_order_id`='%s'", mysqli_real_escape_string($this->db, $retail_order_id));
                        $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);
                        
                        if(!$result_sales_in_reg){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                        for($i=0;$i<count($retail_arr);$i++){
                            $retail_order_detail_id = $retail_arr[$i]['retail_order_detail_id'];
                            $retail_parent_id=$retail_arr[$i]['retail_parent_id'];
                            $retail_product_id=$retail_arr[$i]['retail_product_id'];
//                            $retail_product_price=$retail_arr[$i]['retail_price'];
                            $rem_due = $retail_arr[$i]['rem_due'];
                            $str = '';

//                            if ($retail_discount_id !== 0) {
//                                if ($retail_arr[$i]['retail_discount'] > 0) {
//                                    $taxpercent = ($retail_arr[$i]['retail_tax_percentage'] / 100) * ($retail_arr[$i]['retail_price'] - $retail_arr[$i]['retail_discount']);
//                                    $retail_product_price = $retail_arr[$i]['retail_price'] - $retail_arr[$i]['retail_discount'] + $taxpercent;
//                                } else {
//                                    $taxpercent = ($retail_arr[$i]['retail_tax_percentage'] / 100) * $retail_arr[$i]['retail_price'];
//                                    $retail_product_price = $retail_arr[$i]['retail_price'] + $taxpercent;
//                                }
//                            } else {
//                                $taxpercent = ($retail_arr[$i]['retail_tax_percentage'] / 100) * $retail_arr[$i]['retail_price'];
//                                $retail_product_price = $retail_arr[$i]['retail_price'] + $taxpercent;
//                            }
                            if($rem_due>0 && $update_amount>0){
                                $due = $sales_due = 0;
                                if($rem_due>$update_amount){
                                    $sales_due = $update_amount;
                                    $due = $rem_due - $update_amount;
                                    $update_amount = 0;
                                    if($due<.5){
                                        $sts = 'S';
                                    }else{
                                        $sts = 'P';
                                    }
                                }else{
                                    $sales_due = $rem_due;
                                    $due = 0;
                                    $sts = 'S';
                                    $update_amount -= $rem_due;
                                }
                                $product_processing_fee = $retail_total_processing_fee * ($retail_arr[$i]['rem_due']/$total_amount);
                                $final_net_update = $sales_due;
                                if($processing_fee_type==1){
                                    $final_net_update = $sales_due-$product_processing_fee;
                                }
                                if (empty($retail_parent_id) || is_null($retail_parent_id)) {
                                    $str = "IN (%s)";
                                    $this->addretailDimensions($company_id, $retail_arr[$i]['retail_product_id'], '', '');
                                    $this->updateRetailDimensionsNetSales($company_id, $retail_arr[$i]['retail_product_id'], '', '', $final_net_update);
                                } else {
                                    $str = "IN (%s,$retail_parent_id)";
                                    $this->addretailDimensions($company_id, $retail_arr[$i]['retail_parent_id'], $retail_arr[$i]['retail_product_id'], '');
                                    $this->updateRetailDimensionsNetSales($company_id, $retail_arr[$i]['retail_parent_id'], $retail_arr[$i]['retail_product_id'], '', $final_net_update);
                                }

                                $update_order_details = sprintf("UPDATE `retail_order_details` SET `rem_due`='%s', `retail_payment_status`='$sts' WHERE `company_id`='%s' AND `retail_order_id`='%s' AND `retail_order_detail_id`='%s'", mysqli_real_escape_string($this->db, $due),
                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $retail_order_detail_id));
                                $result_order_details = mysqli_query($this->db, $update_order_details);
                                if(!$result_order_details){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_order_details");
                                    ipn_log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }

                                $update_count = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`+$final_net_update WHERE `company_id`='%s' AND `retail_product_id` $str",
                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
                                $result_count = mysqli_query($this->db, $update_count);
                                if(!$result_count){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                                    ipn_log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }
                    $update_query = sprintf("UPDATE `retail_payment` SET `checkout_status`='%s', `actual_paid_date`=CURDATE() $payment_status WHERE `retail_order_id`='%s' and `retail_payment_id`='%s' and `checkout_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $checkout_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        ipn_log_info($this->json($error));
                        if($send_email==1){
                           $this->responseWithMail($this->json($error), 200);
                        }else{
                            $this->responseWithMail($this->json($error), 200);
                        }
                    }
                    
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
                
            }else{
                $error = array('status' => "Failed", "msg" => "Checkout details doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
      //retail dashboard
    public function addretailDimensions($company_id, $product_id, $child_id, $period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($child_id)) {
            $sql1 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND  `child_id`='%s' AND child_id !=0  AND `period`='%s' AND `deleted_flg`!='D'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $insertquery = sprintf("INSERT INTO `retail_dimensions`(`company_id`, `period`, `product_id`, `child_id`) VALUES('%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id));
                    $res = mysqli_query($this->db, $insertquery);
                    if (!$res) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        if (!empty($product_id)) {
            $sql2 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s' AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result2 = mysqli_query($this->db, $sql2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows1 = mysqli_num_rows($result2);
                if ($num_of_rows1 == 0) {
                    $insertquery1 = sprintf("INSERT INTO `retail_dimensions`(`company_id`, `period`, `product_id`,  `child_id`) VALUES('%s', '%s','%s',0)",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $product_id));
                    $res2 = mysqli_query($this->db, $insertquery1);
                    if (!$res2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function updateRetailDimensionsNetSales($company_id, $product_id, $child_id, $date, $amount) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        if (!empty($date)) {
            $every_month_dt = date("Y-m", strtotime($date));
        } else {
            $every_month_dt = date("Y-m");
        }

//        $date_init = 0;
//        $date_str = "%Y-%m";
//        $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));

        if (!empty($child_id)) {
            $sql1 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`='%s' AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), $every_month_dt);
            $res1 = mysqli_query($this->db, $sql1);
            if (!$res1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($res1);
                if ($num_of_rows == 1) {
                    $sql2 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`='%s' AND `period`='%s'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), $every_month_dt);
                    $res2 = mysqli_query($this->db, $sql2);
                    if (!$res2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $sql3 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
                        $res3 = mysqli_query($this->db, $sql3);
                        if (!$res3) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }
        } else {
            $sql2 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
            $res2 = mysqli_query($this->db, $sql2);
            if (!$res2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($res2);
                if ($num_of_rows == 1) {
                    $sql3 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `period`='%s' AND `child_id`='0'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
                    $res3 = mysqli_query($this->db, $sql3);
                    if (!$res3) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    private function getMiscCheckoutDetails($checkout_id){
        $query = sprintf("SELECT ms.`company_id`, ms.`misc_order_id`,msp.`misc_payment_id`,msp.`checkout_id`,msp.`checkout_status`, `access_token`,msp.`payment_amount`,msp.`processing_fee`, s.`misc_charge_fee_type`
                    FROM `misc_order` ms 
                    LEFT JOIN `misc_payment` msp ON ms.`misc_order_id`=msp.`misc_order_id`
                    LEFT JOIN `wp_account` wp ON ms.`company_id` = wp.`company_id` 
                    LEFT JOIN `studio_pos_settings` s ON ms.`company_id` = s.`company_id` 
                    WHERE msp.`checkout_id` = '%s' ORDER BY msp.`misc_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $checkout_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $misc_order_id = $row['misc_order_id'];
                $access_token = $row['access_token'];
                $payment_id = $row['misc_payment_id'];
                $fee = $row['processing_fee'];
                $processing_fee_type = $row['misc_charge_fee_type'];
                $payment_amount = $row['payment_amount'];
                $company_id = $row['company_id'];
                   
                
                $temp_password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $temp_password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                $dummy_sns=[];
                $json = array("checkout_id"=>"$checkout_id");
                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi('checkout','POST',$postData,$token,$this->user_agent,$dummy_sns);
                ipn_log_info("wepay_get_checkout: ".$response['status']);
                if($response['status']=='Success'){
                    $res = $response['msg'];
                    $checkout_state = $res['state'];
                    $payment_status = "";
                    $send_email = 0;
                    if($checkout_state=='expired' || $checkout_state=='failed' || $checkout_state=='charged back'){
                        $payment_status = ", `payment_status`='F'";
                        $send_email = 1;
                    }
                    if ($processing_fee_type == 1) {
                        $paid_amount = $payment_amount - $fee;
                    } elseif ($processing_fee_type == 2) {
                        $paid_amount = $payment_amount;
                    }
                    
                    if($checkout_state == 'released'){
                        $update_sales_in_reg = sprintf("UPDATE `misc_order` SET `paid_amount`=`paid_amount`+$paid_amount WHERE `misc_order_id`='%s'", mysqli_real_escape_string($this->db, $misc_order_id));
                        $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);
                        
                        if(!$result_sales_in_reg){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }

                    $update_query = sprintf("UPDATE `misc_payment` SET `checkout_status`='%s' $payment_status WHERE `misc_order_id`='%s' and `misc_payment_id`='%s' and `checkout_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $misc_order_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $checkout_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        ipn_log_info($this->json($error));
                        if($send_email==1){
                           $this->responseWithMail($this->json($error), 200);
                        }else{
                            $this->responseWithMail($this->json($error), 200);
                            if($checkout_state == 'released'){
                                $this->addMiscDimensions($company_id,'',$paid_amount);
                            }
                        }
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
                
            }else{
                $error = array('status' => "Failed", "msg" => "Checkout details doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
    public function addMiscDimensions($company_id, $period,$amount) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if (!empty($period)) {
            $curr_dt = date("Y-m", strtotime($period));
        } else {
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));

        $sql2 = sprintf("SELECT * FROM `misc_dimensions` WHERE `company_id`='%s' AND `period`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows1 = mysqli_num_rows($result2);
            if ($num_of_rows1 == 0) {
                $insertquery1 = sprintf("INSERT INTO `misc_dimensions`(`company_id`, `period`,`sales_amount`) VALUES('%s','%s','0')", 
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $amount));
                $res2 = mysqli_query($this->db, $insertquery1);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    $this->response($this->json($error), 200);
                }
            }
            
            $updatequery1 = sprintf("UPDATE `misc_dimensions` SET `sales_amount`=sales_amount+'%s' where company_id='%s' AND period = '%s'", 
                        mysqli_real_escape_string($this->db, $amount),mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt));
            $res2 = mysqli_query($this->db, $updatequery1);
            if (!$res2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$updatequery1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$updatequery1");
                $this->response($this->json($error), 200);
            }
            
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function updateMembershipDimensionsNetSalesForOneTime(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error),406);
        }

        $company_id = $membership_id = $period = '';
        
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        if(isset($this->_request['membership_id'])){
            $membership_id = $this->_request['membership_id'];
        }
        if(isset($this->_request['period'])){
            $period = $this->_request['period'];
        }
        
        if(!empty($company_id) && !empty($membership_id)){
            $this->updateMembershipDimensionsNetSales($company_id, $membership_id, $period);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error), 400);
        }
    }
    
    public function addTrialDimensions($company_id, $trial_id, $period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if (!empty($period)) {
            $curr_dt = date("Y-m", strtotime($period));
        } else {
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));


        $sql2 = sprintf("SELECT * FROM `trial_dimensions` WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows1 = mysqli_num_rows($result2);
            if ($num_of_rows1 == 0) {
                $insertquery1 = sprintf("INSERT INTO `trial_dimensions`(`company_id`, `period`, `trial_id`) VALUES('%s','%s','%s')", 
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $trial_id));
                $res2 = mysqli_query($this->db, $insertquery1);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    $this->response($this->json($error), 200);
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
}

$api = new wepayipn;
$api->processApi();