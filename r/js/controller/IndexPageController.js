/*jslint browser: true*/
/*global console, MyApp*/
//www
MyApp.angular.controller('IndexPageController', ['$rootScope', '$scope', '$http', 'urlservice', 'InitService', '$localStorage', '$window',   function ($rootScope, $scope, $http, urlservice, InitService ,$localStorage ,$window) {
        'use strict';
      
        
        //No Internet - show alert
        $scope.istatus = function () {
            MyApp.alert('Please turn on internet', 'No Internet');
            $scope.preloaderVisible = false;
            return;
        };
        
        // CART EMPTY INITIALIZE
        $scope.cart_contents = $scope.studentlist = [];
        $scope.server_call_status = false;
        $scope.discount_error_flag = 'N';
        $scope.discount_error_msg = '';
        $scope.price_error_flag = 'N';
        $scope.price_error_msg = '';
        $scope.sel_cat = $scope.sel_cat1 = "";
        $scope.back_prod_type = $scope.back_prod_index = $scope.cart_page_from = "";
        $scope.retail_buyer_Billing_Address = $scope.retail_buyer_Address_Line2 = "";
        $scope.product_not_available = $scope.discount_code_applied = false;
        $scope.initial_load_payment = function () {
            $scope.onetimecheckout = 0;
            $scope.ccyearlist = ["2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037"];
            $scope.ccmonthlist = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
            $scope.ccmonth = '';
            $scope.ccyear = '';
            $scope.country = '';
            if ($scope.wp_level === 'P') {
                $scope.wepay_env = "production";
                $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
            } else if ($scope.wp_level === 'S') {
                $scope.wepay_env = "stage";
                $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
            }
            WePay.set_endpoint($scope.wepay_env); // change to "production" when live  
        }; 
        //      Muthulakshmi
        $scope.retail_payment_method = 'CC';
        $scope.student_name = [];
        $scope.showcardselection = false;
        $scope.retailcarddetails = [];
        $scope.shownewcard = true; 
        $scope.retail_optional_discount = '';
        $scope.retail_check_number = '';
        $scope.retail_cash_amount = '';
        $scope.erroroptionaldiscount = false;
        $scope.selectedcardindex = '';
        $scope.optional_discount_applied = 'N';
        $scope.retail_search_member = '';
        $scope.wepaystatus = $scope.stripestatus = '';
        $scope.wepay_enabled = $scope.stripe_enabled = false;
        $scope.retail_waiver_checked = $scope.retail_ach_chkbx = $scope.retail_cc_chkbx = false;
        $scope.stripe_status='';
        $scope.retail_ach_route_no = $scope.retail_ach_acc_number = $scope.retail_ach_holder_name = $scope.retail_ach_acc_type = '';
        $scope.stripe_card = '';
        
         // Shortcuts
        var valueById = '';
        var d = document;
        d.id = d.getElementById, valueById = function(id) {
            return d.id(id).value;
        };

        // For those not using DOM libraries
        var addEvent = function(e,v,f) {
            if (!!window.attachEvent) { e.attachEvent('on' + v, f); }
            else { e.addEventListener(v, f, false); }
        };

        // ATTACH THE RETAIL TO THE DOM - WEB VIEW SUBMIT BUTTON
        addEvent(d.id('cc-submit-retail'), 'click', function () {
           $scope.retailCheckoutRedirect();
        });
       
       // ATTACH THE RETAIL TO THE DOM - MOBILE VIEW SUBMIT BUTTON
       addEvent(d.id('cc-submit-retail1'), 'click', function () {
            $scope.retailCheckoutRedirect();
        });
        
        $scope.retailCheckoutRedirect = function(){
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            var checkout_message1 = '';
            
            
            if (!$scope.retail_buyer_First_Name || !$scope.retail_buyer_Last_Name || !$scope.retail_buyer_Email || !$scope.retail_buyer_Phone)
            {
                checkout_message1 += "Buyer's Info:";
                if (!$scope.retail_buyer_First_Name) {
                    checkout_message1 += '"First Name"<br>';
                }
                if (!$scope.retail_buyer_Last_Name) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if (!$scope.retail_buyer_Email) {
                    checkout_message1 += '"Email"<br>';
                }
                if (!$scope.retail_buyer_Phone) {
                    checkout_message1 += '"Cell Phone"<br>';
                }
            }
            if (!$scope.retail_buyer_Billing_Address || !$scope.retail_buyer_City  || !$scope.retail_buyer_State || !$scope.retail_buyer_Zipcode || !$scope.retail_buyer_Country )
            {
                checkout_message1 += 'Address:';
                if (!$scope.retail_buyer_Billing_Address) {
                    checkout_message1 += '"Street address"<br>';
                }
                if (!$scope.retail_buyer_City) {
                    checkout_message1 += '"City"<br>';
                }
                if (!$scope.retail_buyer_State) {
                    checkout_message1 += '"State"<br>';
                }
                if (!$scope.retail_buyer_Zipcode) {
                    checkout_message1 += '"Zip"<br>';
                }
                if (!$scope.retail_buyer_Country) {
                    checkout_message1 += '"Country"<br>';
                }
            }
            //Muthulakshmi
            if($scope.retail_payment_method === 'CC'){
                if (!$scope.retail_cc_name || !$scope.retail_cc_number || !$scope.retail_cc_expirymonth  || !$scope.retail_cc_expiryyear || !$scope.retail_cc_cvv  || !$scope.retail_cc_country || !$scope.retail_cc_zipcode)
                {
                    checkout_message1 += 'Payment Info:';
                    if (!$scope.retail_cc_name) {
                        checkout_message1 += '"Name on card"<br>';
                    }
                    if (!$scope.retail_cc_number) {
                        checkout_message1 += '"Card number"<br>';
                    }
                    if (!$scope.retail_cc_expirymonth) {
                        checkout_message1 += '"Expiration Date:Month"<br>';
                    }
                    if (!$scope.retail_cc_expiryyear) {
                        checkout_message1 += '"Expiration Date:Year"<br>';
                    }
                    if (!$scope.retail_cc_cvv) {
                        checkout_message1 += '"CVV"<br>';
                    }
                    if (!$scope.retail_buyer_Country) {
                        checkout_message1 += '"Choose country"<br>';
                    }
                    if (!$scope.retail_cc_zipcode) {
                        checkout_message1 += '"Zip code"<br>';
                    }
                }
            }
            
            if (!$scope.retail_waiver_checked) {
                checkout_message1 += "Click the check box to accept sales agreement<br>";
            }
            if ($scope.stripe_enabled && $scope.retail_payment_method == 'ACH' && !$scope.retail_ach_chkbx) {
                checkout_message1 += '"Click the check box to accept the ACH plugin agreement"<br>';
            }
            if ($scope.stripe_enabled && $scope.retail_payment_method == 'CC' && !$scope.retail_cc_chkbx) {
                checkout_message1 += '"Click the check box to accept the Credit card agreement"<br>';
            }
            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }
            var userName = $scope.retail_buyer_First_Name + ' ' + $scope.retail_buyer_Last_Name;
            var user_first_name = $scope.retail_buyer_First_Name;
            var user_last_name = $scope.retail_buyer_Last_Name;
            var email = valueById('retail_buyer_Email').trim();
            var phone = valueById('retail_buyer_Phone');
            var postal_code = valueById('retail_cc_zipcode');
            var country = valueById('retail_cc_country');
            var response = WePay.credit_card.create({
                "client_id": $scope.wp_client_id,
                "user_name": userName,
                "email": valueById('retail_buyer_Email').trim(),
                "cc_number": valueById('retail_cc_number'),
                "cvv": valueById('retail_cc_cvv'),
                "expiration_month": valueById('retail_cc_expirymonth'),
                "expiration_year": valueById('retail_cc_expiryyear'),
                "address": {
                    "country": valueById('retail_cc_country'),
                    "postal_code": valueById('retail_cc_zipcode')
                }
            }, function (data) {
                if (data.error) {
                    $scope.preloader = false;
                    $scope.$apply();
                    MyApp.closeModal();
                    MyApp.alert(data.error_description, 'Message');
                    // HANDLES ERROR RESPONSE
                } else {
                    $scope.purchaseretailproduct(data.credit_card_id, data.state, user_first_name, user_last_name, email, phone, postal_code, country);
                }
            });
        };
        
        $scope.backToPOS = function () {
            var hosturl;
            if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
               window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                       window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                    var origin = window.location.origin;
                    hosturl = origin;
                }

            } else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            }  else if (window.location.hostname === 'beta.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'https://' + www_url;
                }
            }   else if (window.location.hostname === 'stage.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'localhost') {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            } else {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            }
            var pos_type, pos_string, pos_entry_type;
            if ($scope.pos_user_type === 'S'){
                pos_type = 'spos';
                pos_string = 'e92213e6721525d7spos712627e6db9b98b4';
            }else if($scope.pos_user_type === 'P'){
                pos_type = 'ppos';
                pos_string = '8f2f44c8580cc72ppos700ff5d197b5a18f0';
            }else{
                pos_type = '';
                pos_string = '';
            }
            if($scope.pos_entry_type){
                pos_entry_type = $scope.pos_entry_type;
            }else{
                pos_entry_type = 'l';
            }
            if (pos_type) {
                $window.open(hosturl+'/' + 'pos' + '/?=' + $scope.param1 + '/' + $scope.param2 + '/' + pos_type + '-' + pos_string + '/'+pos_entry_type+'/', '_self');
            }
        };
        
        // GET WEPAY STATUS
        $scope.getWepayStatus = function (companyid) {
            $http({
                method: 'GET',
                url: urlservice.url + 'getwepaystatus',
                params: {
                    "companyid": companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.wepaystatus = response.data.msg;
                    if($scope.wepaystatus !== 'N'){
                        $scope.wepay_enabled = true;
                    }
                } else {
                    $scope.wepaystatus = 'N';
                    $scope.wepay_enabled = false;
                }
                $scope.preloader = false;
                
            }, function (response) {
                $scope.preloader = false;
                
                MyApp.alert('Connection failed', 'Invalid server response');
                $scope.preloader = false;
                
            });
        };
        
        $scope.retailStripeValidation = function(){
            $scope.preloader = true;
            var checkout_message1 = '';
            
         if (!$scope.retail_buyer_First_Name || !$scope.retail_buyer_Last_Name || !$scope.retail_buyer_Email || !$scope.retail_buyer_Phone)
            {
                checkout_message1 += "Buyer's Info:";
                if (!$scope.retail_buyer_First_Name) {
                    checkout_message1 += '"First Name"<br>';
                }
                if (!$scope.retail_buyer_Last_Name) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if (!$scope.retail_buyer_Email) {
                    checkout_message1 += '"Email"<br>';
                }
                if (!$scope.retail_buyer_Phone) {
                    checkout_message1 += '"Cell Phone"<br>';
                }
            }
            if (!$scope.retail_buyer_Billing_Address || !$scope.retail_buyer_City  || !$scope.retail_buyer_State || !$scope.retail_buyer_Zipcode || !$scope.retail_buyer_Country )
            {
                checkout_message1 += 'Address:';
                if (!$scope.retail_buyer_Billing_Address) {
                    checkout_message1 += '"Street address"<br>';
                }
                if (!$scope.retail_buyer_City) {
                    checkout_message1 += '"City"<br>';
                }
                if (!$scope.retail_buyer_State) {
                    checkout_message1 += '"State"<br>';
                }
                if (!$scope.retail_buyer_Zipcode) {
                    checkout_message1 += '"Zip"<br>';
                }
                if (!$scope.retail_buyer_Country) {
                    checkout_message1 += '"Country"<br>';
                }
            }
            if($scope.total_discounted_price > 0){
                if ( ($scope.pos_user_type === 'S' && $scope.retail_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex) 
                        || (($scope.pos_user_type === 'S' || $scope.pos_user_type === 'P') && $scope.retail_payment_method == 'CH' && !$scope.retail_check_number ) 
                        || ($scope.retail_payment_method == 'CC' && $scope.shownewcard && !$scope.valid_stripe_card ) 
                        ||  ($scope.retail_payment_method == 'ACH' && (!$scope.retail_ach_route_no || !$scope.retail_ach_acc_number  || !$scope.retail_ach_holder_name || !$scope.retail_ach_acc_type)) ){
                    checkout_message1 += 'Payment Info:';
                    if(($scope.pos_user_type === 'S' || $scope.pos_user_type === 'P') && $scope.retail_payment_method === 'CH' && !$scope.retail_check_number){
                       checkout_message1 += '"Check Number"<br>'; 
                    }
                    if($scope.pos_user_type === 'S' && $scope.retail_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex){
                       checkout_message1 += '"Select Card"<br>'; 
                    }
                    if($scope.retail_payment_method == 'CC' && $scope.shownewcard  && !$scope.valid_stripe_card){
                        checkout_message1 += '"Invalid card details"<br>';
                    }
                    if($scope.retail_payment_method == 'ACH'){
                        checkout_message1 += '"ACH details"<br>';
                        if (!$scope.retail_ach_route_no) {
                            checkout_message1 += '"Route number"<br>';
                        }
                        if (!$scope.retail_ach_acc_number) {
                            checkout_message1 += '"Account number"<br>';
                        }
                        if (!$scope.retail_ach_holder_name) {
                            checkout_message1 += '"Account holder name"<br>';
                        }
                        if (!$scope.retail_ach_acc_type) {
                            checkout_message1 += '"Account type"<br>';
                        }
                    }
                }
            }
            if (!$scope.retail_waiver_checked && ($scope.total_discounted_price == 0 || $scope.retail_payment_method !== 'CC')) {
                checkout_message1 += "Click the check box to accept Waiver & Agreement<br>";
            }
            if ($scope.total_discounted_price > 0 && $scope.retail_payment_method == 'ACH' && !$scope.retail_ach_chkbx) {
                checkout_message1 += '"Click the check box to accept the ACH plugin agreement"<br>';
            }
            if ($scope.total_discounted_price > 0 && $scope.retail_payment_method == 'CC' && !$scope.retail_cc_chkbx) {
                checkout_message1 += '"Click the check box to accept the Credit card agreement"<br>';
            }
            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }else{
                return true;
            }
        };
        
                
        $scope.retailStripeCheckout = function () {
            if ($scope.retailStripeValidation()) { // stripe form validation
                $scope.preloader = true;
                $http({
                    method: 'POST',
                    url: urlservice.url + 'stripeRetailCheckout',
                    data: {
                        "company_id": $scope.companyid,
                        "pos_email": $localStorage.pos_user_email_id,
                        "token": $scope.pos_token,
                        "studentid": $scope.retail_selected_student_id,
                        "cart_details": $scope.selected_products,
                        "buyer_first_name": $scope.retail_buyer_First_Name,
                        "buyer_last_name": $scope.retail_buyer_Last_Name,
                        "buyer_street": $scope.retail_buyer_Billing_Address + ' ' + $scope.retail_buyer_Address_Line2,
                        "buyer_city": $scope.retail_buyer_City,
                        "buyer_state": $scope.retail_buyer_State,
                        "buyer_zip": $scope.retail_buyer_Zipcode,
                        "email": $scope.retail_buyer_Email,
                        "phone": $scope.retail_buyer_Phone,
                        "country": $scope.retail_buyer_Country,
                        "payment_method_id": ($scope.retail_payment_method == 'CC') ? $scope.payment_method_id : '',
                        "reg_type_user": $scope.pos_user_type === 'S' ? 'SP' : ($scope.pos_user_type === 'P' ? 'PP' : 'U'), // REGISTERED if Staffpos = 'SP, if Publicpos = 'PP' else open URL
                        "discount_code": $scope.retail_discount_code,
                        "payment_method": ($scope.total_discounted_price > 0) ? $scope.retail_payment_method : '', //CC - creadit card,CH - check, CA- Cash
                        "check_number": $scope.retail_check_number,
                        "optional_discount": $scope.optional_discount_applied === 'N' ? 0 : $scope.retail_optional_discount,
                        "optional_discount_flag": $scope.optional_discount_applied,
                        "payment_amount": $scope.total_discounted_price,
                        "intent_method_type": parseFloat($scope.total_discounted_price) > 0 ? "PI" : "", // SI - SETUP INTENT /  PI - PAYMENT INTENT
                        "verification_status": $scope.verification_status,
                        "payment_intent_id": $scope.verification_status === 'success' ? $scope.payment_intent_id : ""
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then(function (response) {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                    if (response.data.status === 'Success') {
                        MyApp.alert(response.data.msg, 'Thank You');
                        $scope.reset_retailForm();
                        $scope.redirect_page();  //  GET RETAIL LIST & REDIRECT TO LISTING SCREEN
                    } else {
                        if (response.data.session_status === "Expired") {
                            MyApp.alert('Session Expired', '', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        } else {
                            MyApp.alert(response.data.msg, 'Failure');
                        }
                    }
                }, function (response) {
                    $scope.preloader = false;
                    MyApp.alert('Connection failed', 'Invalid server response');
                });
            } else {
                $scope.preloader = false;
            }
        };
        // STRIPE TOKEN CALL
        $scope.getStripeElements = function () {
            // Create a Stripe client
            $scope.valid_stripe_card = false;
            // Create an instance of Elements
            var elements = $scope.stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: 'rgb(74,74,74)',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: ($scope.mobiledevice) ? '14px' : '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element
            $scope.stripe_card = elements.create('card', {style: style});

            // Add an instance of the card Element into the `card-element` <div>
            $scope.stripe_card.mount('#stripe-card-element');

            // Handle real-time validation errors from the card Element.
            $scope.stripe_card.addEventListener('change', function (event) {
                var displayError = document.getElementById('stripe-card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                    $scope.valid_stripe_card = false;
                } else {
                    displayError.textContent = '';
                    $scope.valid_stripe_card = true;
                }
            });

        };
        
        // Handle form submission
        $scope.submitretailform = function () {
            if ($scope.retailStripeValidation()) {
                $scope.payment_method_id = "";
                $scope.stripe.createPaymentMethod('card', $scope.stripe_card).then(function (result) {
                    if (result.error) {
                        setTimeout(function () {
                            $scope.preloader = false;
                            $scope.$apply();
                        }, 1000);

                        // Inform the user if there was an error
                        MyApp.alert(result.error.message, '');
                        $scope.valid_stripe_card = false;
                    } else {
                        // Send paymentMethod.id to server 
                        (result.paymentMethod.id) ? $scope.payment_method_id = result.paymentMethod.id : $scope.payment_method_id = "";
                        $scope.preloader = true;
                        $scope.retailStripeCheckout();
                    }
                });
            } else {
                $scope.preloader = false;
            }
        };
        
        //get Stripe status of this company
        $scope.getStripeStatus = function (cmp_id) {
            
            if ($scope.stripestatus !== 'Y') {
                $scope.wepay_enabled = $scope.stripe_enabled = false;
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getstripestatus',
                    params: {
                        "company_id": $scope.companyid,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.stripestatus = response.data.msg;
                        if ($scope.stripestatus !== 'N') {
                            $localStorage.preview_wepaystatus = $scope.wepaystatus = 'Y';
                            if ($scope.stripe_country_support === 'Y') {
                                $scope.stripe_enabled = true;
                                $scope.stripe = Stripe($scope.stripe_publish_key);
                                $scope.getStripeElements();
                            }else{
                                $scope.getWepayStatus(cmp_id);
                            } 
                        }else{
                            $scope.getWepayStatus(cmp_id);
                        }      
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        MyApp.alert(response.data.msg, '');                       
                    } else {
                        if(response.data.msg && response.data.msg !== 'N'){
                            MyApp.alert(response.data.msg, '');
                        }else{
                            $scope.getWepayStatus(cmp_id);
                        }
                    }
                }, function (response) {
                    console.log(response.data);
                    MyApp.alert(response.data, '');
                });
            } else {
                if ($scope.stripestatus !== 'N' && $scope.stripe_country_support === 'Y') {
                    $scope.stripe_enabled = true;
                } else {
                    $scope.getWepayStatus(cmp_id);
                    $scope.wepay_enabled = $scope.stripe_enabled = false;
                }
            }
        };
        
        $scope.sendExpiryEmailToStudio = function(companyid){
            $scope.preloader = true;
            $http({
                method: 'POST',
                url: urlservice.url + 'sendExpiryEmailToStudio',
                data: {
                    "companyid": companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;                
                if (response.data.status == 'Success') {
                    MyApp.alert(response.data.msg, '');
                } else {
                    MyApp.alert(response.data.msg, 'Failure');
                }
            }, function (response) {
                $scope.preloader = false;               
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };
        
        // RESET VARIANT DETAILS
        $scope.resetvariantdetails = function () {
            $scope.product_variant = [];
            $scope.prod_variant_selection = "";
            $scope.quan_exc_limit_msg = "";
            $scope.quan_exc_limit = false;
        };
                   
        //GET RETAIL VARIANT LIST
        $scope.getVariantList = function (companyid, productid) {
            $http({
                method: 'GET',
                url: urlservice.url + 'retailvariants',
                params: {
                    "companyid":$scope.companyid,
                    "product_id": productid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.product_variant = [];
                    $scope.product_variant = response.data.msg;
                    $scope.quan_exc_limit = false;
                    $scope.quan_exc_limit_msg = "";
                    // SETTING DEFAULT DROP DOWN VALUE
                    if ($scope.product_variant && $scope.product_variant.length > 0) {
                        $scope.prod_variant_selection = $scope.product_variant[0].retail_variant_id;
                        $scope.retail_variant_price = $scope.product_variant[0].variant_price;   // SETTING 1ST VARIANT PRICE IN DESCRIPTION PAGE
                        $scope.getproductprice('listing','click'); // CHECK AVALIABILITY & GET VALUES BY ASSUMING QUANTITY 1
                    }
                } else {        
                    $scope.resetvariantdetails();
                    if($scope.cart_page_from === 'PD'){
                        $scope.redirect_page();
                    }else{
                        MyApp.alert(response.data.msg,'Not Available');
                    }
                }
            }, function (response) {
                MyApp.alert(response.data.status, 'Invalid server response');
            });
        };
        
        //GET RETAIL PRODUCT LIST
        $scope.getRetailList = function (companyid, categoryid, productid) {
            $scope.fav_product_name = '';
            $scope.company_id_status = $scope.category_id_status = $scope.product_id_status = false;            
            $scope.dynamic_product_id = $scope.dynamic_category_id = '';
            
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            var prod_id, page_from;
            if(categoryid && productid){
                prod_id = productid;
                page_from = 'CL';
            }else{
                prod_id = categoryid;
                page_from = '';
            }
            $http({
                method: 'GET',
                url: urlservice.url + 'retailproducts',
                params: {
                    "companyid": companyid,
                    "product_id": prod_id,
                    "page_from": page_from,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {

                if (response.data.status === 'Success') {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                    $scope.retailcontent = [];
                    $scope.retail_product_list = [];
                    $scope.initial_load_payment();
                    $scope.retailstatus = response.data.status;
                    $scope.retail_sales_agreement =  response.data.msg.retail_sales_agreement;
                    //  VARIABLES INITIALIZATION
//                    $scope.prod_variant_selection = "";
                    $scope.retai_discount_available = response.data.msg.retai_discount_available;
                    if (companyid !== '' && categoryid ==='' && productid === '') {
                        $scope.retailcontent = response.data.msg.retail_category;
                        $scope.company_id_status = true;
                        // Load page:                                       
                        retailView.router.load({
                            pageName: 'retail_category'
                        });

                    } else if (companyid !== '' && categoryid !=='' && productid === '') {
                        $scope.category_id_status = true;
                        $scope.dynamic_category_id = categoryid;
                        $scope.retailcontent = response.data.msg.retail_category;
                        $scope.fav_product_name = $scope.retailcontent[0].retail_product_title;
                        // check product type is category / standalone to redirect screens
                        if($scope.retailcontent[0].retail_product_type === 'C'){
                            $scope.getproducttype('C',0);
                        }else if($scope.retailcontent[0].retail_product_type === 'S'){
                            $scope.product_id_status = true;
                            $scope.getproducttype('S',0);
                        }
                    } else {
                        $scope.product_id_status = true;
                        $scope.dynamic_product_id = productid;
                        $scope.retail_product_list = response.data.msg.retail_category;
                        $scope.fav_product_name = $scope.retail_product_list[0].retail_product_title;
                        $scope.getproducttype('P',0);
                    }
                    
                    // ALIGN CONTENTS CENTER WHEN MINIMUM DATA  - MAIN LISTING
                    if($scope.retailcontent && $scope.retailcontent.length < 3){
                        if($("span#home-list").parent().get( 0 ).tagName !== "CENTER"){
                            $("span#home-list").wrap("<center />");
                        }
                    }else{
                        if($("span#home-list").parent().get( 0 ).tagName === "CENTER"){
                            $("span#home-list").unwrap("<center />");
                        }
                    }
                    
                    // ALIGN CONTENTS CENTER WHEN MINIMUM DATA  - PRODUCTS LISTING
                    if($scope.retail_product_list && $scope.retail_product_list.length < 3){
                        if($("span#product_list").parent().get( 0 ).tagName !== "CENTER"){
                            $("span#product_list").wrap("<center />");
                        }
                    }else{
                        if($("span#product_list").parent().get( 0 ).tagName === "CENTER"){
                            $("span#product_list").unwrap("<center />");
                        }
                    }
                } else {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                    $scope.retailcontent = [];
                    $scope.retailstatus = response.data.status;
                    if(response.data.session_status === "Expired"){
                        MyApp.alert('Session Expired','', function () {
                            $localStorage.pos_user_email_id = "";
                            $scope.backToPOS();
                        });
                    }else{
                        MyApp.alert(response.data.msg, 'Message');
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };
        
        // GET COMPANY DETAILS
        $scope.getCompanyDetails = function (studio_code,companyid, categoryid, productid) {
            $scope.companyid = companyid;
            $scope.categoryid = categoryid;
            $scope.productid = productid;
            $scope.fav_company_name = "";
            
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            $http({
                method: 'GET',
                url: urlservice.url + 'publiccompanydetails',
                params: {
                    "companyid": companyid,
                    "studio_code":studio_code,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U',
                    "page_from": "R"
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status == 'Success') {
                    $scope.companydetails = response.data.company_details;
                    $scope.logoUrl = $scope.companydetails.logo_URL;
                    $scope.wp_level = $scope.companydetails.wp_level;
                    $scope.wp_client_id = $scope.companydetails.wp_client_id;
                    $scope.company_status = $scope.companydetails.upgrade_status;
                    $scope.processing_fee_percentage = $scope.companydetails.processing_fee_percentage;
                    $scope.processing_fee_transaction = $scope.companydetails.processing_fee_transaction;
                    $scope.fav_company_name = $scope.companydetails.company_name;
                    $scope.wp_currency_symbol = $scope.companydetails.wp_currency_symbol;
                    $scope.wp_currency_code = $scope.companydetails.wp_currency_code;
                    $scope.retail_enable_flag = $scope.companydetails.retail_enable_flag;
                    $scope.retail_processing_fee_type = $scope.companydetails.retail_processing_fee_type;
                    $scope.retail_sales_agreement = $scope.companydetails.retail_sales_agreement;
                    $scope.pos_settings_payment_method = $scope.companydetails.retail_payment_method;
                    //stripe set
                    $scope.stripe_upgrade_status = $scope.companydetails.stripe_upgrade_status;
                    $scope.stripe_subscription = $scope.companydetails.stripe_subscription;
                    $scope.stripe_publish_key = $scope.companydetails.stripe_publish_key;
                    $scope.stripe_country_support = $scope.companydetails.stripe_country_support;
                    $scope.getStripeStatus(companyid);
                    if($scope.pos_user_type === 'S')
                        $scope.getstudentlist(companyid);
                    if(companyid && !categoryid && !productid){
                        $scope.getRetailList(companyid,"","");
                    }else if(companyid && categoryid && !productid){
                        $scope.getRetailList(companyid, categoryid, "");                        
                    }else{
                        $scope.getRetailList(companyid, categoryid, productid);
                    }
                } else {
                   // PREVENT FURTHUR EXECUTION & BLOCK THE URL IF STUDIO EXPIRED
                    
                    if (response.data.upgrade_status && response.data.studio_expiry_level) {
                        $scope.cmpny_email = response.data.studio_email;
                        $scope.company_status = response.data.upgrade_status;
                        $scope.company_expiry_level = response.data.studio_expiry_level;
                    }

                    if (($scope.company_status && $scope.company_status === 'F') || ($scope.company_expiry_level && $scope.company_expiry_level === 'L2')) {
                        // PREVENT FURTHUR EXECUTION & BLOCK THE APP
                        MyApp.modal({
                            title: '',
                            afterText:
                                    '<br>' +
                                    '<div id="studioExpiryText" class="modal-text custom-modal-content">Please contact the business operator to activate this link.</div>' +
                                    '<br>' +
                                    '<button class="button custom-modal-green-button button-text modalresetButton" id="send_expiry_email"></button>'
                        });

                        if ($scope.cmpny_email) {
                            $("#send_expiry_email").text("Send email message");
                        } else {
                            $("#send_expiry_email").text("Ok");
                        }

                        $('#send_expiry_email ').click(function () {
                            $scope.$apply(function () {
                                if ($("#send_expiry_email").text() === 'Send email message') {
                                    $scope.sendExpiryEmailToStudio(companyid);
                                }
                                MyApp.closeModal();
                            });
                        });

                    } else {
                        if(response.data.session_status === "Expired"){
                            MyApp.alert('Session Expired','', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        }else{
                            MyApp.alert(response.data.msg, 'Message');
                        }
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };        
        
        $scope.urlredirection = function () {
            $scope.param1 = $scope.param2 = $scope.param3 = $scope.param4 = '';
            var decoded_url = decodeURIComponent(window.location.href);
            var params = decoded_url.split('?=')[1].split('/');

            $scope.param1 = params[0];
            $scope.param2 = params[1];
            $scope.param3 = params[2];
            $scope.param4 = params[3];
            
            if(params[5]){
                if(params[5] === 'spos'){ //If staff
                    $scope.pos_user_type = 'S';
                    // ENABLE ADMIN BANNER, SEARCH MEMBER, OPTIONAL DISCOUNTS
                    $('#view-1').addClass('with-banner');
                }else{
                    $scope.pos_user_type = 'P'; //If public
                }
                if(params[6]){ //For access token
                    $scope.pos_token = params[6];
                }
                if(params[7]){
                   $scope.pos_entry_type = params[7];
                }
                $('#view-1').addClass('from-pos with-footer');
            }else{
                $scope.pos_user_type = 'O'; // If open URL
                $scope.pos_token = $scope.pos_entry_type = '';
            }
//            alert('param1: '+$scope.param1+' param2: '+$scope.param2+' param3: '+$scope.param3+' param4: '+$scope.param4);

            if ($scope.param1 !== undefined && $scope.param2 !== undefined && ($scope.param3 === undefined || $scope.param3 === '') && $scope.param4 === undefined) {
                $scope.getCompanyDetails($scope.param1,$scope.param2, '', '');
            } else if ($scope.param1 !== undefined && $scope.param2 !== undefined && $scope.param3 !== undefined && ($scope.param4 === undefined || $scope.param4 === '')) {
                if (isNaN($scope.param3)) {
                    $scope.getCompanyDetails($scope.param1,$scope.param2, '', '');
                } else {
                    $scope.getCompanyDetails($scope.param1,$scope.param2, $scope.param3, '');         // Is a number
                }
            } else if ($scope.param1 !== undefined && $scope.param2 !== undefined && $scope.param3 !== undefined && $scope.param4 !== undefined) {
                $scope.getCompanyDetails($scope.param1,$scope.param2, $scope.param3, $scope.param4);
            } else {
                window.location.href = window.location.origin;
            }
        };
       
        // SETTING PRODUCT DETAIL BASED ON TYPE
        $scope.getproducttype = function(type,index){
//            alert(type+' '+index);
            if(type === 'C'){
                $scope.retail_product_list = [];
                $scope.current_retail_category = $scope.retailcontent[index];
                if ($scope.current_retail_category.retail_product_type === 'C' && $scope.current_retail_category.products) {
                    $scope.retail_product_list = $scope.current_retail_category.products;
                    // ALIGN CONTENTS CENTER WHEN MINIMUM DATA  - PRODUCTS LISTING
                    if($scope.retail_product_list && $scope.retail_product_list.length < 3){
                        if($("span#product_list").parent().get( 0 ).tagName !== "CENTER"){
                            $("span#product_list").wrap("<center />");
                        }
                    }else{
                        if($("span#product_list").parent().get( 0 ).tagName === "CENTER"){
                            $("span#product_list").unwrap("<center />");
                        }
                    }
                } 
                $scope.sel_cat = index;;  // TO MAINTAIN PRODUCTS LISTING DROPDOWN
                // Load page:                                       
                retailView.router.load({
                    pageName: 'retail_products'
                });
                $scope.parent_index = index;    // TO MAINTAIN THE PARENT INDEX FOR PRODUCTS
            } else if (type === 'S' || type === 'P') {
                // CLEARING RESPECTIVE VALUES 
                $scope.resettotalprice();
                $scope.prod_variant_quantity = "";
                // GETTING VALUES FROM RESPECTIVE ARRAY
                if(type === 'S'){
                    $scope.current_retail_product = $scope.retailcontent[index];
                }else if(type === 'P'){
                     $scope.current_retail_product = $scope.retail_product_list[index];
                }
                
                // PLACING THE VALUES TO THE RESPECTIVE SCOPES
                if ($scope.current_retail_product) {
                    $scope.retail_product_title = $scope.current_retail_product.retail_product_title;
                    $scope.retail_banner_img_url = $scope.current_retail_product.retail_banner_img_url;
                    $scope.retail_product_desc = $scope.current_retail_product.retail_product_desc;
                    $scope.retail_product_price = $scope.current_retail_product.retail_product_price;
                    $scope.retail_product_compare_price = $scope.current_retail_product.retail_product_compare_price;
                    $scope.retail_variant_flag = $scope.current_retail_product.retail_variant_flag;
                    if ($scope.retail_variant_flag === 'N') {
                        $scope.resetvariantdetails();
                        $scope.getproductprice('listing','click'); // CHECK AVALIABILITY & GET VALUES BY ASSUMING QUANTITY 1
                    }else{
                        // GETTING VARIANT LIST IF EXISTS
                        $scope.getVariantList($scope.companyid, $scope.current_retail_product.retail_product_id);
                    }
                }
                // TO MAINTAIN PRODUCTS DESCRIPTION DROPDOWN
                if(type === 'P'){
                   $scope.sel_cat1 = $scope.parent_index; 
                   
                }else{
                   $scope.sel_cat1 = index; 
                }
            }
            $scope.back_prod_type = type;
            $scope.back_prod_index = index;
        };
        
        $scope.checkproducttypeback = function(){
            if($scope.current_retail_product){
                if($scope.current_retail_product.retail_product_type === 'S'){
                    $scope.redirect_page();
                }else if($scope.current_retail_product.retail_product_type === 'P'){
                    $scope.sel_cat = $scope.parent_index;  // SETTING PRODUCT LIST DROPDOWN VALUE
                    // Load page:                                       
                    retailView.router.load({
                        pageName: 'retail_products'
                    });
                }
            }
        };
        
        $scope.errorflagreset = function(response){
            var retail_array = [];
            retail_array = response.data.retail_array;
            if (retail_array.length > 0 && $scope.cart_contents.length > 0) {
                for (var ret_arr = 0; ret_arr < retail_array.length; ret_arr++) {
                    $scope.cart_contents[ret_arr].quantity_error_flag = retail_array[ret_arr].quantity_error_flag;
                    $scope.cart_contents[ret_arr].quantity_msg = retail_array[ret_arr].quantity_msg;
                    $scope.cart_contents[ret_arr].discount_error_flag = retail_array[ret_arr].discount_error_flag;
                    $scope.cart_contents[ret_arr].discount_msg = retail_array[ret_arr].discount_msg;
                    $scope.cart_contents[ret_arr].is_discount_applied = retail_array[ret_arr].is_discount_applied;
                    $scope.cart_contents[ret_arr].discounted_value = retail_array[ret_arr].discounted_value;
                    $scope.cart_contents[ret_arr].retail_total_product_price = retail_array[ret_arr].retail_total_product_price;
                    $scope.cart_contents[ret_arr].retail_discounted_price = retail_array[ret_arr].retail_discounted_price;
                }
            }
            $scope.quan_exc_limit_msg = "";
            $scope.quan_exc_limit = false;
            $scope.discount_error_flag = response.data.discount_error_flag;
            $scope.optional_discount_error_flag = response.data.optional_discount_error_flag;
            $scope.discount_error_msg = response.data.discount_error_msg;
            $scope.price_error_flag = response.data.price_error_flag;
            $scope.price_error_msg = response.data.price_error_msg;
            $scope.discount_usage_type = response.data.discount_usage_type;
            $scope.cart_contents_copy = [];
            $scope.cart_contents_copy = angular.copy($scope.cart_contents);
        };
           
        // GETTING PRODUCT PRICE & AVAILABLE QUANTITY
        $scope.getproductprice = function (page, action) { //kamal
            //  PREVENT IF SERVER CALL RUNNING ALREADY
            if (action === 'click' && $scope.server_call_status) {
                return;
            }
            
            $scope.selected_products = [];
            $scope.checkout_flag = 'N';
            if (page === 'description' || page === 'listing') {
                // PUSH THE SELECTED ITEM INTO ARRAY
                $scope.selected_products.push({"retail_product_id": $scope.current_retail_product.retail_product_id, "retail_parent_id": ($scope.current_retail_product.retail_parent_id === null || $scope.current_retail_product.retail_parent_id === '') ? '' : $scope.current_retail_product.retail_parent_id, "retail_variant_id": $scope.prod_variant_selection, "quantity": ($scope.prod_variant_quantity === '' || $scope.prod_variant_quantity === undefined )? '1':$scope.prod_variant_quantity, "quantity_error_flag": 'N', "quantity_msg": "", "discount_error_flag": 'N', "discount_msg": "","retail_total_product_price":"","retail_product_compare_price":""});
                $scope.retail_discount_code = "";
            } else if (page === 'cart' || page === 'checkout') {
                if(action === 'click'){
                    $scope.checkout_flag = 'Y';    // CHECKOUT AMOUNT LESS THAN 5 ERROR
                }
                if ($scope.cart_contents && $scope.cart_contents.length > 0) {
                    $scope.selected_products = angular.copy($scope.cart_contents);
                } else {
                    $scope.resettotalprice();
                    return;       // RESET PRICE & PREVENT SERVERCALL WHEN NO CART CONTENTS
                }
            }
            
            $scope.server_call_status = true;
            if (action === 'click') {
                $scope.preloader = true;
                $scope.preloaderVisible = true;
            }
            $http({
                method: 'POST',
                url: urlservice.url + 'checkQuantityOrDiscountOnCheckout',
                data: {
                    "companyid": $scope.companyid,
                    "discount_code": ($scope.optional_discount_applied === 'N' && $scope.discount_code_applied)?$scope.retail_discount_code:'',
                    "cart_details": $scope.selected_products,
                    "checkout_flag": $scope.checkout_flag,
                    "payment_method" :$scope.retail_payment_method, //CC - creadit card,CH - check, CA- Cash,
                    "optional_discount_flag":$scope.optional_discount_applied,
                    "optional_discount":$scope.optional_discount_applied === 'N'? 0:$scope.retail_optional_discount,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                if (action === 'click') {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                }
                if (response.data.status === 'Success') {
                    $scope.quan_exc_limit_msg = "";
                    $scope.quan_exc_limit = false;
                    // CHECKING QUANTIY,DISCOUNT,PRICE ERRORS
                    if (response.data.quantity_error_flag === 'N' && response.data.discount_error_flag === 'N' && response.data.price_error_flag === 'N' && response.data.optional_discount_error_flag === 'N') {
                        if (page === 'description' && action === 'click') {
                            $scope.addproducttocart(response);
                        } else if (page === 'cart' && action === 'click') {
                            // Load page:                                       
                            retailView.router.load({
                                pageName: 'retail_checkout'
                            });
                        } else if (page === 'cart' && action === 'blur') {
                            // Load page:                                       
                            retailView.router.load({
                                pageName: 'retail_cart'
                            });
                        }
                        if($scope.cart_contents && $scope.cart_contents.length > 0){
                            $scope.errorflagreset(response);
                        }
                        $scope.settotalprice(response.data);
                        $scope.handlediscountstatus(response.data);
                    } else {

                    }
                } else {
                    // ENSURE THE PRODUCTS IN CART IS NOT DELETED FROM WEB PORTAL
                    if (response.data.deleted_error_flag && response.data.deleted_error_flag === 'Y' && page === 'cart' && response.data.retail_array && response.data.retail_array.length > 0) {
                        for (var i = 0; i < response.data.retail_array.length; i++) {
                            if (response.data.retail_array[i].deleted_flag === 'Y' && $scope.cart_contents && $scope.cart_contents.length > 0) {
                                $scope.cart_contents.splice(i, 1);
                            }
                        }
                        $scope.product_not_available = true;
                        $scope.updatecartdetails('cart', 'delete', '', '', '', '');
                    } else if (response.data.quantity_error_flag && (response.data.quantity_error_flag === 'Y' || page === 'description') && response.data.retail_array && response.data.retail_array.length > 0) {
                        // DISPLAY QUANTITY ERROR FOR DESCRIPTION PAGE
                        $scope.quan_exc_limit_msg = response.data.retail_array[0].quantity_msg;
                        $scope.quan_exc_limit = true;
                        $scope.resettotalprice();   // RESET PRICE WHEN QUANTITY ERROR
                        $scope.cart_contents = angular.copy($scope.cart_contents);
                    } else if (response.data.discount_error_flag && response.data.discount_error_flag === 'Y' ) {
                        // HANDLE DISCOUNT ERRORS
                        $scope.settotalprice(response.data);
                        $scope.handlediscountstatus(response.data);
                        if (page === 'cart' && action === 'click') {
                            // Load page:                                       
                            retailView.router.load({
                                pageName: 'retail_checkout'
                            });
                        }
                        $scope.cart_contents = angular.copy($scope.cart_contents);
                    }else {
                        $scope.errorflagreset(response);
                        if(response.data.optional_discount_error_flag === 'N'){
                            MyApp.alert(response.data.price_error_msg, 'Failure');
                            $scope.settotalprice(response.data);
                        }
                    }
                    if (page === 'cart') {
                        $scope.errorflagreset(response);
                    }
                }
                $scope.server_call_status = false;
                $scope.pos_settings_payment_method = response.data.pos_settings_payment_method;
                // GO TO PRODUCT DETAIL AFTER SERVER CALL
                if (page === 'listing' && action === 'click') {
                    // Load page:                                       
                    retailView.router.load({
                        pageName: 'retail_product_detail'
                    });
                }
            }, function (response) {
                if (action === 'click') {
                    $scope.preloader = false;
                    $scope.preloaderVisible = false;
                }
                MyApp.alert('Connection failed', 'Invalid server response');
                $scope.server_call_status = false;
            });
        };
        
        // CHANGING INPUT BORDERS BASED ON THE STATUS - VALID / INVALID
        $scope.handleInputValidity = function(selector,status){
            if(status === 'valid'){
                $(selector).removeClass('invalid_input');
                $(selector).addClass('valid_input');
            }else if(status === 'invalid'){
                $(selector).removeClass('valid_input');
                $(selector).addClass('invalid_input');
            } else {
                $(selector).removeClass('invalid_input');
                $(selector).removeClass('valid_input');
            }
        };
        
        $scope.settotalprice = function(data){
            // TOTAL DATAS ASSIGNED HERE
            $scope.total_order_price = data.total_array.total_order_price;
            $scope.total_discounted_price = data.total_array.total_discounted_price;
            $scope.total_processing_fee = data.total_array.total_processing_fee;
            $scope.total_tax_value = data.total_array.total_tax_value;
            $scope.retail_processing_fee_type = data.total_array.processing_fee_type;
            $scope.total_discount_value = data.total_array.total_discount_value;
            $scope.total_order_compare_price = data.total_array.total_order_compare_price;
            $scope.total_quantity =  data.total_array.total_quantity;
            $scope.total_order_price_without_discount = data.total_array.total_order_price_without_discount;
            
            // CALCULATED PRODUCT PRICE AND COMPARE PRICE PUSHED INSIDE THE ARRAY
            $scope.retail_array = data.retail_array;
            if($scope.retail_array.length === $scope.cart_contents.length){
                for(var i=0; i<$scope.retail_array.length;i++){
                    $scope.cart_contents[i].retail_total_product_price = $scope.retail_array[i].retail_total_product_price;
//                    $scope.cart_contents[i].retail_product_compare_price = $scope.retail_array[i].retail_product_compare_price;
                }
            }
        };
        
        $scope.handlediscountstatus = function(data){
            $scope.applied_discount_type = data.discount_type;
            $scope.applied_discount_value = data.discount_value;
            $scope.discount_usage_type = data.discount_usage_type;
            $scope.retail_discount_msg = '';
            if(data.discount_error_flag === 'Y'){
                $scope.retail_discount_msg = data.discount_error_msg;
                $scope.handleInputValidity('.retail-discount-box','invalid');
                $('.retail_discount_error').css('color','rgb(255,4,4)');
            }else if($scope.applied_discount_value && $scope.applied_discount_type){
                if (data.discount_usage_type === 'O') {     // DISCOUNT APPLIED TO OVERALL ORDER
                    if ($scope.applied_discount_type === 'V') {
                        $scope.retail_discount_msg = 'A ' + $scope.wp_currency_symbol + $scope.applied_discount_value + ' discount has been subtracted from the total';
                    } else {
                        $scope.retail_discount_msg = 'A ' + $scope.applied_discount_value + '% discount has been subtracted from the total';
                    }
                } else if (data.discount_usage_type === 'P') {
                    if ($scope.applied_discount_type === 'V') {
                        $scope.retail_discount_msg = 'A ' + $scope.wp_currency_symbol + $scope.applied_discount_value + ' discount applied to applicable item(s).';
                    } else {
                        $scope.retail_discount_msg = 'A ' + $scope.applied_discount_value + '% discount applied to applicable item(s).';
                    }
                }
                
               
                $scope.handleInputValidity('.retail-discount-box','valid'); 
                $('.retail_discount_error').css('color','black');
            }else{
                $scope.retail_discount_msg = '';
                $scope.handleInputValidity('.retail-discount-box','reset'); 
                $('.retail_discount_error').css('color','black');
            }
        };
        
        $scope.resettotalprice = function(){
            $scope.total_discounted_price = $scope.total_order_price = $scope.total_processing_fee = $scope.total_tax_value = $scope.total_discount_value = $scope.total_order_compare_price = $scope.total_quantity = $scope.total_order_price_without_discount = 0;
            $scope.retail_processing_fee_type = '';
            $scope.retail_array = [];
        };
        
        $scope.addproducttocart = function(response){
           // CART VARIABLES 
           $scope.current_product_to_cart = {};
           $scope.current_retail_product_price = $scope.current_product_total = 0;
            
            // IF CURRENT PRODUCT ALREADY IN CART UPDATE IT ELSE ADD IT
            if ($scope.productalreadyincart('description','','')) {
                $scope.updatecartdetails('description','update','',$scope.prod_variant_quantity,$scope.matched_product_index,'');
            }else{
                // GETTING RESPECTIVE PRICE BASED ON VARIANT IF ANY
                if ($scope.current_retail_product.retail_variant_flag === 'Y') {
                    $scope.current_retail_product_price = $scope.current_product_variant_price;   // USE THIS SCOPE TO MAINTAIN  VARIANT PRICE - CALCULATED RESPONSE FROM SERVER
                    $scope.current_product_variant = $scope.product_variant;
                } else if ($scope.current_retail_product.retail_variant_flag === 'N') {
                    $scope.current_retail_product_price = $scope.current_retail_product.retail_product_price;
                    $scope.current_product_variant = "";
                }
                

                if($scope.current_product_variant.length > 0 && $scope.current_retail_product.retail_variant_flag === 'Y'){
                    for (var var_arr = 0; var_arr < $scope.current_product_variant.length; var_arr++) {
                        if($scope.prod_variant_selection === $scope.current_product_variant[var_arr].retail_variant_id){
                            var variant_index = var_arr;
                        }                        
                    }
                }
                // PRODUCT DETAILS IN OBJECT
                $scope.current_product_to_cart = {"retail_product_id": $scope.current_retail_product.retail_product_id, 
                    "retail_parent_id": ($scope.current_retail_product.retail_parent_id === null || $scope.current_retail_product.retail_parent_id === '') ? '' : $scope.current_retail_product.retail_parent_id, 
                    "retail_variant_id": $scope.prod_variant_selection,"retail_variant_index": variant_index, "quantity": ($scope.prod_variant_quantity === '' || $scope.prod_variant_quantity === undefined)?'1':$scope.prod_variant_quantity, 
                    "retail_product_title": $scope.current_retail_product.retail_product_title, "retail_discounted_price": response.data.retail_array[0].retail_discounted_price,
                    "retail_variant_flag":$scope.current_retail_product.retail_variant_flag, "variant_list":$scope.current_product_variant,
                    "quantity_error_flag":'N',"quantity_msg":"","discount_error_flag":'N',"discount_msg":"","is_discount_applied":'N',"discounted_value	":0,"retail_total_product_price":"","retail_product_compare_price":""}; // REMOVE TOTAL PRICE FROM OBJECT ONCE THE SERVER SIDE WORK COMPLETED (TEMPORARY)

                // PUSHING PRODUCT DETAILS INTO CART
                $scope.cart_contents.push($scope.current_product_to_cart);
                // PUSHING VARIANT DETAILS INTO CART IF ANY
                if ($scope.product_variant && parseInt($scope.product_variant.length > 0)) {
                    $scope.cart_contents[$scope.cart_contents.length - 1].product_variant_id = $scope.product_variant;
                }
            }
                // GO TO THE RESPECTIVE PAGE BASED ON PRODUCT TYPE
                $scope.confirmcartredirect();
        };
        
        // CART REDIRECT CONFIRMATION
        $scope.confirmcartredirect = function () {
            MyApp.modal({
                title: '',
                afterText:
                        '<br>'+
                        '<span id="cart_confirm" class="cart_confirm">' +
                        '<div class="modal-text custom-modal-content">Product has been added to your cart</div>' +
                        '<br>' +
                        '<button class="button custom-modal-white-button button-text modalresetButton" id="continueshopping">Continue Shopping</button>'+
                        '<button class="button custom-modal-green-button button-text modalCreateButton" id="checkout">Check Out</button>'+ 
                        '</span>'
            });

            $('#continueshopping').click(function () {
                $scope.$apply(function () {
                   $scope.getRetailList($scope.companyid, "", ""); // REDIRECT TO MAIN LISTING SCREEN
                    MyApp.closeModal();
                });
            });
            
            $('#checkout').click(function () {
                $scope.$apply(function () {
                    $scope.cart_page_from = 'PD';
                    $scope.getproductprice('cart','blur'); // CHECK AVALIABILITY & REDIRECT TO CART SCREEN
                    MyApp.closeModal();
                });
            });
        };
        
        // FUNCTION TO CHECK WHETHER THE CURRENT PRODUCT IS ALREADY IN THE CART
        $scope.productalreadyincart = function (page, object, variant_id) {  // PAGE  - description / cart 
            $scope.current_product_id = $scope.current_variant_id = '';
            if(page === 'description'){
                $scope.current_product_id = $scope.current_retail_product.retail_product_id;
                $scope.current_variant_id = $scope.prod_variant_selection;
            }else if(page === 'cart' && object){
                $scope.current_product_id = object.retail_product_id;
                $scope.current_variant_id = variant_id;
            }
            
            $scope.matched_product_index = $scope.matched_product_id = $scope.matched_variant_id = $scope.matched_product_qty = '';
             if ($scope.cart_contents && $scope.cart_contents.length > 0) {
                for (var i = 0; i < $scope.cart_contents.length; i++) {
                    if($scope.cart_contents[i].retail_variant_flag === 'Y'){     // CHECKING WHETHER THE PRODUCT HAS VARIANTS
                        if (($scope.cart_contents[i].retail_product_id === $scope.current_product_id) && ($scope.cart_contents[i].retail_variant_id === $scope.current_variant_id)) {  // PRODUCT ALREADY ADDED WITH SAME PRODUCT & VARIANT ID
                            $scope.matched_product_index = i;
                            $scope.matched_product_id = $scope.cart_contents[i].retail_product_id;
                            $scope.matched_variant_id = $scope.cart_contents[i].retail_variant_id;
                            $scope.matched_product_qty = $scope.cart_contents[i].quantity;
                            return true;     // PRODUCT & VARIANT MATCHED  - RETURN RESCPECTIVE ID & INDEX
                        }
                    }else if($scope.cart_contents[i].retail_variant_flag === 'N'){
                        if (($scope.cart_contents[i].retail_product_id === $scope.current_product_id)) {  // PRODUCT ALREADY ADDED WITH SAME PRODUCT ID
                            $scope.matched_product_index = i;
                            $scope.matched_product_id = $scope.cart_contents[i].retail_product_id;
                            $scope.matched_product_qty = $scope.cart_contents[i].quantity;
                            return true;     // PRODUCT MATCHED  - RETURN RESCPECTIVE ID & INDEX
                        }
                    }
                } 
            }
            return false;
        };
        
        $scope.updatecartdetails = function(page,type,variant_id,quantity,pindex,vindex){
//            console.log("update quantity "+quantity+"  "+pindex+" Vind : "+vindex);
            if(type === 'delete'){
                if($scope.cart_contents && $scope.cart_contents.length > 0 && $scope.cart_contents[pindex]) {
                    $scope.delete_prod_title = $scope.cart_contents[pindex].retail_product_title;
                    if (!$scope.product_not_available) {
                        MyApp.confirm('Delete item from cart ?', '',
                                function () {
                                    $scope.cart_contents.splice(pindex, 1);
                                    $scope.cart_contents_copy = angular.copy($scope.cart_contents);
//                            MyApp.alert('',$scope.delete_prod_title+' product discarded from cart sucessfully');
                                    $scope.getproductprice('cart', 'blur');
                                    $scope.$apply();
                                },function () {
                                    $scope.cart_contents[pindex].quantity = quantity;
                                    $scope.cart_contents_copy = angular.copy($scope.cart_contents);
                                    $scope.getproductprice('cart', 'blur');
                                    $scope.$apply();
                                }
                        );
                    }else{
                        $scope.cart_contents_copy = angular.copy($scope.cart_contents);
                        $scope.getproductprice('cart', 'blur');
                    }
                    $scope.product_not_available = false;
                }
            }else if(type === 'update'){
                if(quantity){
                    $scope.cart_contents[pindex].quantity = quantity;
                }
                if(variant_id){
                    $scope.cart_contents[pindex].retail_variant_id = variant_id;
                    $scope.cart_contents[pindex].retail_variant_index = vindex;
                }
                $scope.getproductprice(page,'blur');
            }
            
        };
        
        $scope.catchcartproductchange = function(variant_id,quantity,index){
            // TREAT 0 QUANTITY AS DELETE
            if(quantity){
                if(parseInt(quantity) === 0){
                    $scope.updatecartdetails('','delete','',$scope.cart_contents_copy[index].quantity,index,'');
                }
            }else{
                quantity = (quantity == '' || quantity == undefined)?'1':quantity;
            }
                // CHECK IF  PRODUCT ALREADY IN CART WITH SAME PRODUCT ID & VARIANT ID
                if (variant_id) {
                    if ($scope.productalreadyincart('cart', $scope.cart_contents[index], variant_id)) {
                        $scope.cart_contents = angular.copy($scope.cart_contents);
                        MyApp.alert('Already the same product exists in the cart', '');
                    }
                } else {
                    // UPDATE THE CART PRODUCTS WITH THE RESPECTIVE QUANTITY & VARIANTS
                    var vindex = '';
                    var var_list = [];
                    var_list = $scope.cart_contents[index].variant_list;
                    if (variant_id && var_list && var_list.length > 0) {
                        for (var i = 0; i < var_list.length; i++) {
                            if (parseInt(variant_id) == parseInt(var_list[i].retail_variant_id)) {
                                vindex = i;
                            }
                        }
                    }else{
                       variant_id = '';
                    }
                    $scope.updatecartdetails('cart', 'update', variant_id, quantity, index, vindex);
                }
            if(quantity){
                $scope.getproductprice('cart','blur');
            }
        };
        
        
        $scope.redirect_page = function () {
            if ($scope.companyid && !$scope.categoryid && !$scope.productid) {
                $scope.getRetailList($scope.companyid, "", "");
            } else if ($scope.companyid && $scope.categoryid && !$scope.productid) {
                $scope.getRetailList($scope.companyid, $scope.categoryid, "");
            } else {
                $scope.getRetailList($scope.companyid, $scope.categoryid, $scope.productid);
            }
        };
        
        $scope.reset_retailForm = function(){                   
            $scope.retailpaymentform.$setPristine();
            $scope.retailpaymentform.$setUntouched();             
            $scope.retail_buyer_First_Name = '';
            $scope.retail_buyer_Last_Name = '';
            $scope.retail_buyer_Email='';
            $scope.retail_buyer_Phone = '';
            $scope.retail_buyer_Billing_Address = '';
            $scope.retail_buyer_Address_Line2 = '';
            $scope.retail_buyer_City = '';
            $scope.retail_buyer_State='';
            $scope.retail_buyer_Zipcode = '';
            $scope.retail_buyer_Country = '';
            $scope.retail_cc_name = '';
            $scope.retail_cc_number = '';
            $scope.retail_cc_expirymonth = '';
            $scope.retail_cc_expiryyear = '';
            $scope.retail_cc_cvv = '';
            $scope.retail_cc_number = '';
            $scope.retail_cc_country = '';
            $scope.retail_cc_zipcode = '';
            $scope.retail_waiver_checked = $scope.retail_ach_chkbx = $scope.retail_cc_chkbx = false;
        
            $scope.cart_contents = $scope.cart_contents_copy = [];
            $scope.server_call_status = false;
            $scope.discount_error_flag = 'N';
            $scope.discount_error_msg = '';
            $scope.price_error_flag = 'N';
            $scope.price_error_msg = '';
            
            $scope.retail_payment_method = 'CC';
            $scope.showcardselection = false;
            $scope.retailcarddetails = [];
            $scope.shownewcard = true; 
            $scope.retail_optional_discount = '';
            $scope.retail_check_number = '';
            $scope.retail_cash_amount = '';
            $scope.erroroptionaldiscount = false;
            $scope.selectedcardindex = '';
            $scope.optional_discount_applied = 'N';
            $scope.retail_search_member = '';
            // CLEAR STRIPE FORM
            if ($scope.stripe_card) {
                $scope.stripe_card.clear();
            }
            $('.form-group').removeClass('focused');
            // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
            $('.ph-text').css('display','block');
        }
        
        $scope.purchaseretailproduct = function (credit_card_id, credit_card_state, user_first_name, user_last_name, email, phone, postal_code, country) {
            
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            var checkout_message1 = '';            
            
            if (!$scope.retail_buyer_First_Name || !$scope.retail_buyer_Last_Name || !$scope.retail_buyer_Email || !$scope.retail_buyer_Phone)
            {
                checkout_message1 += "Buyer's Info:";
                if (!$scope.retail_buyer_First_Name) {
                    checkout_message1 += '"First Name"<br>';
                }
                if (!$scope.retail_buyer_Last_Name) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if (!$scope.retail_buyer_Email) {
                    checkout_message1 += '"Email"<br>';
                }
                if (!$scope.retail_buyer_Phone) {
                    checkout_message1 += '"Cell Phone"<br>';
                }
            }
            if (!$scope.retail_buyer_Billing_Address || !$scope.retail_buyer_City  || !$scope.retail_buyer_State || !$scope.retail_buyer_Zipcode || !$scope.retail_buyer_Country )
            {
                checkout_message1 += 'Address:';
                if (!$scope.retail_buyer_Billing_Address) {
                    checkout_message1 += '"Street address"<br>';
                }
                if (!$scope.retail_buyer_City) {
                    checkout_message1 += '"City"<br>';
                }
                if (!$scope.retail_buyer_State) {
                    checkout_message1 += '"State"<br>';
                }
                if (!$scope.retail_buyer_Zipcode) {
                    checkout_message1 += '"Zip"<br>';
                }
                if (!$scope.retail_buyer_Country) {
                    checkout_message1 += '"Country"<br>';
                }
            }
            
            if($scope.pos_user_type === 'S' && $scope.retail_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex && $scope.total_discounted_price > 0){
               checkout_message1 += '"Select Card"<br>'; 
            }
            if($scope.retail_payment_method ==='CH' && !$scope.retail_check_number){  
                checkout_message1 += '"Check Number"<br>';
            }
            if (!$scope.retail_waiver_checked) {
                checkout_message1 += "Click the check box to accept sales agreement<br>";
            }
            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }
            
            if($scope.retail_payment_method ==='CH'){                
                credit_card_id = '';
                credit_card_state = '';
            }else if($scope.retail_payment_method ==='CA'){
                credit_card_id = '';
                credit_card_state = '';
                $scope.retail_check_number = '';
            }else{
                $scope.retail_check_number = '';
            }
            
            $http({
                method: 'POST',
                url: urlservice.url + 'wepayretailCheckout',
                data: {
                    "companyid": $scope.companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token":  $scope.pos_token,
                    "studentid": $scope.retail_selected_student_id,
                    "cart_details": $scope.selected_products,
                    "buyer_first_name":user_first_name,
                    "buyer_last_name":user_last_name,
                    "buyer_street":$scope.retail_buyer_Billing_Address+' '+$scope.retail_buyer_Address_Line2,
                    "buyer_city":$scope.retail_buyer_City,
                    "buyer_state":$scope.retail_buyer_State,
                    "buyer_zip":$scope.retail_buyer_Zipcode,
                    "email":email,
                    "phone":phone,
                    "country":$scope.retail_buyer_Country,
                    "cc_id":credit_card_id,
                    "cc_state":credit_card_state,
                    "reg_type_user": $scope.pos_user_type === 'S' ? 'SP' : ($scope.pos_user_type === 'P' ? 'PP' : 'U'),           // REGISTERED if Staffpos = 'SP, if Publicpos = 'PP' else open URL
                    "discount_code" : $scope.retail_discount_code,
                    "payment_method" :$scope.retail_payment_method, //CC - creadit card,CH - check, CA- Cash
                    "check_number":$scope.retail_check_number,
                    "optional_discount":$scope.optional_discount_applied === 'N'? 0:$scope.retail_optional_discount,
                    "optional_discount_flag":$scope.optional_discount_applied,
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status === 'Success') {
                    MyApp.alert(response.data.msg,'Thank You');
                    $scope.reset_retailForm();
                    $scope.redirect_page();  //  GET RETAIL LIST & REDIRECT TO LISTING SCREEN
                } else {
                    if(response.data.session_status === "Expired"){
                        MyApp.alert('Session Expired','', function () {
                            $localStorage.pos_user_email_id = "";
                            $scope.backToPOS();
                        });
                    }else{
                        MyApp.alert(response.data.msg, 'Failure');
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };
        
        
        $scope.backFromCart = function () {
            if($scope.cart_page_from === 'CL') {
                $scope.redirect_page();     // GET RETAIL LIST & REDIRECT TO CATEGORY LISTING SCREEN
            }else if($scope.cart_page_from === 'PD' || $scope.cart_page_from === 'PL' || $scope.cart_page_from === 'CO'){
                if ($scope.cart_contents && $scope.cart_contents.length > 0) {
                   $scope.getproducttype($scope.back_prod_type, $scope.back_prod_index);    // GET PRODUCT LIST / PRODUCT DETAIL SCREEN
                }else{
                    $scope.redirect_page();
                }
            }
        };
        
        $scope.openretailcart = function(page){
            if($scope.cart_contents && $scope.cart_contents.length > 0){
                $scope.cart_page_from = page;
                $scope.getproductprice('cart','blur');
                // Load page:                                       
                retailView.router.load({
                    pageName: 'retail_cart'
                });
            }
        };
        
        $scope.getstudentlist = function(companyid){
           $scope.preloader = false;
            $scope.preloaderVisible = false;
            $http({
                method: 'GET',
                url: urlservice.url + 'getstudent',
                params: {
                    "companyid": companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status === 'Success') {
                    $scope.studentlist = response.data.msg.student_details;
                    for(var i=0;i<$scope.studentlist.length;i++){
                        $scope.student_name.push($scope.studentlist[i].student_name);
                    }
                } else {
                    if(response.data.session_status === "Expired"){
                        $scope.preloader = false;
                        $scope.preloaderVisible = false;
                        MyApp.alert('Session Expired','', function () {
                            $localStorage.pos_user_email_id = "";
                            $scope.backToPOS();
                        });
                    }else{
//                        MyApp.alert(response.data.msg, 'Message');
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert(response.data.msg, 'Failure');
            }); 
        };
        
        $scope.openretailcheckout = function(){
            $(".form-group > input.ng-valid.filled.ng-valid-parse.ng-pristine.ng-untouched").parents('.form-group').addClass('focused');
            $scope.retail_optional_discount = '';   
            $scope.erroroptionaldiscount = false;
            $scope.static_total_price = $scope.total_discounted_price;
            $scope.retailpaymentform.$setPristine();
            $scope.retailpaymentform.$setUntouched(); 
            if($scope.cart_contents && $scope.cart_contents.length > 0){
                $scope.getproductprice('cart','click');
            }
        };
                
        $scope.openevents = function (eventurl) {
               MyApp.closePanel();
            if ($rootScope.online != "online") {
                $scope.istatus();
                return;
            }
            if(eventurl ==='' && eventurl === undefined && eventurl === null){
                return false;
            }else if(eventurl !=='' && eventurl !== undefined && eventurl !== null){
                window.open(eventurl, '_blank');            
            }

        };
    
        $scope.formatDate = function (dat) {
            var date = dat.split("-").join("/");
            var dateOut = new Date(date);
            return dateOut;
        };
        
        // GET SELECTED VARIANT PRICE & COMPARE PRICE FOR DESCRIPTION 
        $scope.getvariantprice = function () {
            if ($scope.product_variant && $scope.product_variant.length > 0) {
                for(var i=0;i<$scope.product_variant.length;i++){
                    if($scope.prod_variant_selection === $scope.product_variant[i].retail_variant_id){
                        $scope.retail_variant_price = $scope.product_variant[i].variant_price;
                        $scope.retail_variant_compare_price = $scope.product_variant[i].variant_compare_price;
                    }
                }
            }
        };
        
        $scope.catchdiscountcode = function(){
            $scope.discount_code_applied = false;
            if(!$scope.retail_discount_code || $scope.retail_discount_code === ''){
                $scope.retail_discount_code = '';
                $scope.getproductprice('cart','blur');
            }
        };
        
        $scope.checkdiscountcode = function(){
            $scope.discount_code_applied = true;
            if(!$scope.retail_discount_code || $scope.retail_discount_code === ''){
                $scope.retail_discount_code = '';
                $scope.discount_code_applied = false;
            }
            if($scope.discount_code_applied){
                $scope.getproductprice('cart','blur');
            }
        };
        // Simple Dropdown
//        var countries  =  ('India Africa Australia NewZealand England WestIndies Scotland Zimbabwe Srilanka Bangladesh').split(' ');
         
        var autocompleteDropdownSimple  =  MyApp.autocomplete ({
            input: '#autocomplete-dropdown',
            openIn: 'dropdown',
            
            source: function (autocomplete, query, render) {
               var results  =  [];
               if (query.length === 0) {
                  render(results);
                  return;
               }
               
               // You can find matched items
               for (var i = 0; i < $scope.student_name.length; i++) {
                  if ($scope.student_name[i].toLowerCase().indexOf(query.toLowerCase()) >= 0) 
                    results.push($scope.student_name[i]);
               }
               // Display the items by passing array with result items
               render(results);
            }
         });
         
         $scope.catchrtailCash = function(){
             $scope.retail_cash_amount = this.retail_cash_amount;
         };
         $scope.catchrtailCheck = function(){
             $scope.retail_check_number = this.retail_check_number;
         };
         $scope.BackToCartView = function(){             
             $scope.retail_optional_discount = '';
             $scope.optional_discount_applied = 'N';
             $scope.getproductprice('cart','blur'); // CHECK AVALIABILITY & REDIRECT TO CART SCREEN
         };
         $scope.catchRetailOptionalDis = function(){ // kamal
             $scope.discount_code_applied = false;
             $scope.retail_optional_discount = this.retail_optional_discount;
             $scope.optional_discount_applied = 'Y';
             $scope.server_call_status = false;
             $scope.getproductprice('checkout','click');
    }; 
          
        $scope.catchSearchMember = function(){
             if($scope.retail_search_member){
                if ($scope.studentlist.length > 0) {
                    for (var i = 0; i < $scope.studentlist.length; i++) { 
                        if ($scope.retail_search_member === $scope.studentlist[i].student_name){
                            $scope.retail_selected_student_id = $scope.studentlist[i].student_id;
                            $scope.retail_buyer_First_Name = $scope.studentlist[i].buyer_first_name;
                            $scope.retail_buyer_Last_Name = $scope.studentlist[i].buyer_last_name;
                            $scope.retail_buyer_Billing_Address = $scope.studentlist[i].participant_street;
                            $scope.retail_buyer_City = $scope.studentlist[i].participant_city;
                            $scope.retail_buyer_State = $scope.studentlist[i].participant_state;
                            $scope.retail_buyer_Zipcode = $scope.studentlist[i].buyer_postal_code;
                            $scope.retail_buyer_Country = $scope.studentlist[i].participant_country;
                            $scope.retail_buyer_Phone = $scope.studentlist[i].buyer_phone;
                            $scope.retail_buyer_Email = $scope.studentlist[i].buyer_email;
                            $(".form-group > input.ng-valid").parents('.form-group').addClass('focused');
                            //If no values then remove focus class
                            if(!$scope.retail_buyer_First_Name)
                                $("#retail_buyer_First_Name").parents('.form-group').removeClass('focused');
                            if(!$scope.retail_buyer_Last_Name)
                                $("#retail_buyer_Last_Name").parents('.form-group').removeClass('focused');
                            if(!$scope.retail_buyer_Billing_Address)
                                $("#retail_buyer_Billing_Address").parents('.form-group').removeClass('focused');
                            if(!$scope.retail_buyer_Address_Line2)
                                $("#retail_buyer_Address_Line2").parents('.form-group').removeClass('focused');
                            if(!$scope.retail_buyer_City)
                                $("#retail_buyer_City").parents('.form-group').removeClass('focused');
                            if(!$scope.retail_buyer_State)
                                $("#retail_buyer_State").parents('.form-group').removeClass('focused');
                            if(!$scope.retail_buyer_Zipcode)
                                $("#retail_buyer_Zipcode").parents('.form-group').removeClass('focused');
                            if(!$scope.retail_buyer_Country)
                                $("#retail_buyer_Country").parents('.form-group').removeClass('focused');
                            if(!$scope.retail_buyer_Phone)
                                $("#retail_buyer_Phone").parents('.form-group').removeClass('focused');
                            if(!$scope.retail_buyer_Email)
                                $("#retail_buyer_Email").parents('.form-group').removeClass('focused');
                            //For credit card
                            if ($scope.studentlist[i].card_details && $scope.studentlist[i].card_details.length > 0){
                                $scope.retailcarddetails = $scope.studentlist[i].card_details;
                            }else{
                                $scope.retailcarddetails = [];
                            } 
                            if($scope.retail_payment_method === 'CC'){ 
                               if ($scope.studentlist[i].card_details && $scope.studentlist[i].card_details.length > 0){
                                    $scope.showcardselection = true;
                                    $scope.shownewcard = false; 
                                } else{
                                    $scope.showcardselection = false;
                                    $scope.shownewcard = true; 
                                }
                            }else{
                                $scope.showcardselection = false;
                                $scope.shownewcard = false; 
                            }
                        }
                    }
                }
                // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
                $('.ph-text').css('display','none');
            }else{
                $scope.retail_selected_student_id = '';
                $scope.retail_buyer_First_Name = '';
                $scope.retail_buyer_Last_Name = '';
                $scope.retail_buyer_Billing_Address = '';
                $scope.retail_buyer_Address_Line2 = '';
                $scope.retail_buyer_City = '';
                $scope.retail_buyer_State = '';
                $scope.retail_buyer_Zipcode = '';
                $scope.retail_buyer_Country = '';
                $scope.retail_buyer_Phone = '';
                $scope.retail_buyer_Email = ''; 
                $scope.retail_cc_name = '';
                $scope.retail_cc_cvv = '';
                $scope.retail_cc_number = '';
                $scope.retail_cc_expirymonth = '';
                $scope.retail_cc_expiryyear = '';
                $scope.retail_cc_country = '';
                $scope.retail_cc_zipcode = '';
                $(".form-group > input.ng-valid").parents('.form-group').removeClass('focused');
                $scope.showcardselection = false;
                $scope.shownewcard = true; 
                $scope.retailcarddetails = [];
                $scope.selectPaymentMethod();
                // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
                $('.ph-text').css('display','block');
            }
         };
         
         // RESET PAYMENT METHOD SELECTION         
         $scope.selectPaymentMethod = function () {
            $scope.retail_check_number = '';   
            $scope.selectedcardindex = '';
            $scope.retail_credit_card_id = '';
            $scope.retail_credit_card_status = '';
            $scope.retail_cc_name = '';
            $scope.retail_cc_cvv = '';
            $scope.retail_cc_number = '';
            $scope.retail_cc_expirymonth = '';
            $scope.retail_cc_expiryyear = '';
            $scope.retail_cc_country = '';
            $scope.retail_cc_zipcode = '';
            if($scope.retail_payment_method === 'CC'){ 
               if ($scope.retailcarddetails && $scope.retailcarddetails.length > 0){
                    $scope.showcardselection = true;
                    $scope.shownewcard = false; 
                } else{
                    $scope.showcardselection = false;
                    $scope.shownewcard = true; 
                }
            }else{
                $scope.showcardselection = false;
                $scope.shownewcard = false; 
            } 
            $scope.getproductprice('checkout','click');
            
        };
         
         //For selecting existing card of a user
        $scope.selectretailCardDetail = function (ind) {
            if ($scope.selectedcardindex === 'Add New credit card') {
                $scope.shownewcard = true;
                $scope.retail_credit_card_id = '';
                $scope.retail_credit_card_status = '';
                $scope.retail_cc_name = '';
                $scope.retail_cc_cvv = '';
                $scope.retail_cc_number = '';
                $scope.retail_cc_expirymonth = '';
                $scope.retail_cc_expiryyear = '';
                $scope.retail_cc_country = '';
                $scope.retail_cc_zipcode = '';
                $("#retail_cc_name").parents('.form-group').removeClass('focused');
                $("#retail_cc_number").parents('.form-group').removeClass('focused');
                $("#retail_cc_zipcode").parents('.form-group').removeClass('focused');
                $("#retail_cc_cvv").parents('.form-group').removeClass('focused');
            } else {
                $scope.shownewcard = false;
                if($scope.stripe_enabled) {
                    $scope.payment_method_id = $scope.retailcarddetails[ind].payment_method_id;
                } else {
                    $scope.retail_credit_card_id = parseInt($scope.retailcarddetails[ind].credit_card_id);
                    $scope.retail_credit_card_status = $scope.retailcarddetails[ind].credit_card_status;
                    $scope.retail_cc_name = $scope.retailcarddetails[ind].credit_card_name;
                    $scope.retail_cc_expirymonth = $scope.retailcarddetails[ind].credit_card_expiration_month;
                    $scope.retail_cc_expiryyear = $scope.retailcarddetails[ind].credit_card_expiration_year;
                    $scope.retail_cc_country = $scope.retailcarddetails[ind].credit_card_countryr;
                    $scope.retail_cc_zipcode = $scope.retailcarddetails[ind].postal_code;
                }
            }
        };
        
        $scope.handleCheckout = function(){
            if($scope.stripe_enabled){
                $scope.retailStripeCheckout();
            }else{
                $scope.purchaseretailproduct('', '', $scope.retail_buyer_First_Name, $scope.retail_buyer_Last_Name, $scope.retail_buyer_Email, $scope.retail_buyer_Phone, $scope.retail_buyer_Zipcode, $scope.retail_buyer_Country);
            }
        };
        
        //DOCUMENT READY FUNCTION
        angular.element(document).ready(function () {
            // NEW ANIMATION FORM PLACEHOLDER TO LABELS
            $('input').focus(function () {
                $(this).parents('.form-group').addClass('focused');
            });

            $('input').blur(function () {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.form-group').removeClass('focused');
                } else {
                    $(this).addClass('filled');
                }
            });
            ($(window).width() < 641) ? $scope.mobiledevice = true : $scope.mobiledevice = false;
            $scope.urlredirection();
        });
    }]);

MyApp.angular.filter('date2', function ($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function (input, format) {
        console.clear();
        console.log(input, format);
        var dtfilter = $filter('date')(input, format);
        if (dtfilter != undefined) {
            var day = parseInt($filter('date')(input, 'dd'));
            var relevantDigits = (day < 30) ? day % 20 : day % 30;
            console.log(day, relevantDigits);
            var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
            return dtfilter;
            //return dtfilter+suffix;
        }
    };
});

MyApp.angular.filter("trust", ['$sce', function ($sce) {
        return function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        }
    }]);
MyApp.angular.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');

                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
MyApp.angular.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
MyApp.angular.directive('compileTemplate', function($compile, $parse){
    return {
          link: function(scope, element, attr){
            var parsed = $parse(attr.ngBindHtml);
          function getStringValue() { return (parsed(scope) || '').toString(); }

            //Recompile if the template changes
          scope.$watch(getStringValue, function() {
                $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
            });
        }
    }
});
//compile-template

