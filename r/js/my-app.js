


var MyApp = new Framework7({
  pushState: false
});


var $$ = Dom7;

var retailView = MyApp.addView('.retail-view', {
  // Because we want to use dynamic navbar, we need to enable it for this view:
  domCache: true, dynamicNavbar: true, animateNavBackIcon:false, animatePages:false
});

MyApp.angular = angular.module('MyApp', ['ngStorage']).run(function($rootScope) {
                                                       $rootScope.online = navigator.onLine ? 'online' : 'offline';
                                                       $rootScope.$apply();

                                                       if (window.addEventListener) {

                                                         window.addEventListener("online", function() {
                                                           $rootScope.online = "online";
                                                           $rootScope.$apply();
                                                         }, true);
                                                         window.addEventListener("offline", function() {
                                                           $rootScope.online = "offline";
                                                           $rootScope.$apply();
                                                         }, true);
                                                       } else {
                                                         document.body.ononline = function() {
                                                           $rootScope.online = "online";
                                                           $rootScope.$apply();
                                                         };
                                                         document.body.onoffline = function() {
                                                           $rootScope.online = "offline";
                                                           $rootScope.$apply();
                                                         };
                                                       }
                                                      }).factory('urlservice',function(){
                                                           var hosturl;
                                                            if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
                                                                       window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                                                                if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                                                                           window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                                                                    var origin = window.location.origin;
                                                                    hosturl = origin;
                                                                }

                                                            } else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                                                                if (window.location.hostname.indexOf("www") > -1) {
                                                                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                                                                } else {
                                                                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                                                                    hosturl = 'http://' + www_url;
                                                                }
                                                            }  else if (window.location.hostname === 'beta.mystudio.academy') {
                                                                if (window.location.hostname.indexOf("www") > -1) {
                                                                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                                                                } else {
                                                                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                                                                    hosturl = 'https://' + www_url;
                                                                }
                                                            } else if (window.location.hostname === 'stage.mystudio.academy') {
                                                                if (window.location.hostname.indexOf("www") > -1) {
                                                                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                                                                } else {
                                                                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                                                                    hosturl = 'http://' + www_url;
                                                                }
                                                            } else if (window.location.hostname === 'localhost') {
                                                                hosturl = 'http://' + window.location.hostname+'/nila/Mystudio_webapp';
                                                            } else {
                                                                hosturl = 'http://' + window.location.hostname;
                                                            }

                                                            return {
                                                                url: hosturl + '/Api/v1/'
//                                                                url: hosturl + '/mystudio_bitbucket/Api/v1/'
                                                            };
                                                        }); 
