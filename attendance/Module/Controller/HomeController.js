
var app = angular.module('home', []);
app.controller("HomeController", ['$scope', '$location', '$http', '$localStorage', 'urlservice', '$route', '$rootScope', '$filter', '$timeout', function ($scope, $location, $http, $localStorage, urlservice, $route, $rootScope, $filter, $timeout) {
    
    if($scope.attendance_token){ 
        $scope.search = "";
        $scope.company_logo = $localStorage.company_logo;
        setInterval( function(){
            $http({
                method: 'POST',
                url: urlservice.url + 'getMembershipStudents',
                data: {
                    "company_id": $localStorage.att_company_id,
                    "token": $scope.attendance_token,
                    "email": $localStorage.att_user_email_id,
                    "membership_id": $localStorage.membership_id,
                    "category": $localStorage.category_type
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $scope.attendance_details = response.data.msg;
                    $scope.attendance_field_array = angular.copy($scope.attendance_details);
                } else {
                    $('#progress-full').hide();
                    console.log(response.data);
                    $("#homeMessageModal").modal('show');
                    $("#homeMessagecontent").text(response.data.msg);
                }
            }, function (response) {
                $('#progress-full').hide();
                console.log(response.data);
            });
         }, 1000 * 60 * 1); 
       
        $scope.menuLockStatus = $localStorage.menuLockStatus;
        $scope.memCategoryTitle = $localStorage.memCategoryTitle;
        $scope.category_type = $localStorage.category_type;
        
        $scope.getMembershipStudents = function () {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'getMembershipStudents',
                data: {
                    "company_id": $localStorage.att_company_id,
                    "token": $scope.attendance_token,
                    "email": $localStorage.att_user_email_id,
                    "membership_id": $localStorage.membership_id,
                    "category": $localStorage.category_type
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $scope.attendance_details = response.data.msg;
                    $scope.attendance_field_array = angular.copy($scope.attendance_details);
                } else {
                    $('#progress-full').hide();
                    console.log(response.data);
                    $("#homeMessageModal").modal('show');
                    $("#homeMessagecontent").text(response.data.msg);
                }
            }, function (response) {
                $('#progress-full').hide();
                console.log(response.data);
            });
        };
        $scope.getMembershipStudents();

        $scope.attendanceCheckin = function(attendance) {
            $('#progress-full').show();
            var participant_id, membership_reg_id, membership_id, membership_option_id, type = '';
            participant_id = attendance.participant_id;
            membership_reg_id = attendance.membership_registration_id;
            membership_id = attendance.membership_id;
            membership_option_id = attendance.membership_option_id;
            type = attendance.type;
            
            $http({
                method: 'POST',
                url: urlservice.url + 'membershipCheckin',
                data: {
                    "company_id": $localStorage.att_company_id,
                    "token": $scope.attendance_token,
                    "email": $localStorage.att_user_email_id,
                    "participant_id": participant_id,
                    "membership_id": membership_id,
                    "membership_option_id": membership_option_id,
                    "membership_reg_id": membership_reg_id,
                    "list_type": $localStorage.membership_id ? "category":"all",
                    "type": type,
                    "category": $localStorage.category_type
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
//                    $("#homeMessageModal").modal('show');
//                    $("#homeMessagecontent").text(response.data.msg);   
                    $scope.attendance_details = response.data.participant_details.msg;
                    $scope.attendance_field_array = angular.copy($scope.attendance_details);
                } else {
                    $('#progress-full').hide();
                    console.log(response.data);
                    $("#homeMessageModal").modal('show');
                    $("#homeMessagecontent").text(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
            });
        };
        
        $scope.attendanceCheckedin = function (attendance_id, participant_name,type) {
            $("#CancelCheckinModal").modal('show');
            $("#CancelCheckincontent").text('Cancel check in for '+participant_name);
            $scope.cancel_atten_id = attendance_id;
            $scope.cancel_type = type;
        };
        
        $scope.confirmCancelCheckin = function(attendance_id , type) {
            $('#progress-full').show();            
            $http({
                method: 'POST',
                url: urlservice.url + 'cancelMembershipCheckin',
                data: {
                    "company_id": $localStorage.att_company_id,
                    "token": $scope.attendance_token,
                    "email": $localStorage.att_user_email_id,
                    "attendance_id": attendance_id,
                    "membership_id": $localStorage.membership_id,
                    "type":type,
                    "category": $localStorage.category_type
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide(); 
                    $scope.cancel_atten_id = $scope.cancel_type = "";
                    $scope.attendance_details = response.data.participant_details.msg;
                    $scope.attendance_field_array = angular.copy($scope.attendance_details);
                } else {
                    $('#progress-full').hide();
                    console.log(response.data);
                    $("#homeMessageModal").modal('show');
                    $("#homeMessagecontent").text(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
            });
        };
        $scope.confirmcancelcheckincancel = function (){
            $scope.cancel_atten_id = "";
            $scope.cancel_type = "";
        };
        
        $scope.HomeMenu = function () {            
            $location.path('/menu');
        };
        
        $scope.showUnlockMenu = function () {
            $scope.clearUnlockForm();
            $("#myModalunlock").modal('show');              
        };
        
        $scope.clearUnlockForm = function () {
            $scope.unlockuseremail = $scope.unlockpassword = '';
            $scope.studio_AuthenticationStatus = true;
            $scope.unlock.$setPristine();
            $scope.unlock.$setUntouched();
        };

        $scope.unlockMenu = function () {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'lockOrUnlockMenuDetails',
                data: {
                    "company_id": $localStorage.att_company_id,
                    "token": $scope.attendance_token,
                    "email": $localStorage.att_user_email_id,
                    "lock_email": $scope.unlockuseremail,
                    "lock_password": $scope.unlockpassword
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $scope.clearUnlockForm();
                    $localStorage.menuLockStatus = 'N';
                    $scope.menuLockStatus = $localStorage.menuLockStatus;
                    console.log(response.data);
                    $scope.studio_AuthenticationStatus = true;
                    $("#myModalunlock").modal('hide');
                } else {
                    $('#progress-full').hide();
                    console.log(response.data);
                    if(response.data.msg === "Session expired."){
                        $("#homeMessageModal").modal('show');
                        $("#homeMessagecontent").text(response.data.msg);
                    }else{                        
                        $scope.studio_AuthenticationStatus = false;
                    }
                    $localStorage.menuLockStatus = 'Y';
                    $scope.menuLockStatus = $localStorage.menuLockStatus;
                }
            }, function (response) {
                console.log(response.data);
                $('#progress-full').hide();
            });
        };
        
        }else{
            $location.path('/');
        }

    }]);


app.filter('dateSuffix', function ($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function (input) {
        if(input !== '0000-00-00'){
            var dtfilter = $filter('date')(input, 'MMMM d');
            var day = parseInt(dtfilter.slice(-2));
            var relevantDigits = (day < 30) ? day % 20 : day % 30;
            var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
            var daysuffix = dtfilter + suffix;

            var currentDate = new Date();
            var current_dtfilter = $filter('date')(currentDate, 'MMMM d');
            var current_day = parseInt(current_dtfilter.slice(-2));
            var current_relevantDigits = (current_day < 30) ? current_day % 20 : current_day % 30;
            var current_suffix = (current_relevantDigits <= 3) ? suffixes[current_relevantDigits] : suffixes[0];
            var current_daysuffix = current_dtfilter + current_suffix;
            return (daysuffix === current_daysuffix) ? 'Today' : daysuffix;
        }else{
            return "";
        }
    };
});

