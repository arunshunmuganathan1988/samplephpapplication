var app = angular.module('menu', []);
app.controller("MenuController", ['$scope', '$location', '$http', '$localStorage', 'urlservice', '$route', '$rootScope', '$filter', '$timeout', function ($scope, $location, $http, $localStorage, urlservice, $route, $rootScope, $filter, $timeout) {

        $scope.mainMenuShow = false;
        $scope.membershipMenuShow = false;
        $scope.classesMenuShow = false;
        $scope.menuLockStatus = $localStorage.menuLockStatus;
        $scope.studio_AuthenticationStatus = true;        
        $scope.menu_Click = "Y";
        $scope.company_logo = $localStorage.company_logo;
        
        if($localStorage.menuLockStatus === 'Y'){
            $location.path('/home');
        }

        $scope.mainMenuView = function () {
            $scope.mainMenuShow = true;
            $scope.membershipMenuShow = false;
            $scope.classesMenuShow = false;
            $scope.current_member_view = "parent";
        };
        $scope.mainMenuView();

        $scope.membershipMenuView = function () {
            $('#progress-full').show();
            $scope.mainMenuShow = false;
            $scope.membershipMenuShow = true;
            $scope.classesMenuShow = false;            
            $scope.current_member_view = "child";
            $http({
                method: 'POST',
                url: urlservice.url + 'getMembershipCategories',
                data: {
                    "company_id": $localStorage.att_company_id,
                    "token":  $scope.attendance_token,
                    "email": $localStorage.att_user_email_id,
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $scope.membership_categories = response.data.msg;
                    $scope.membership_categories_view = angular.copy($scope.membership_categories);
                } else {
                    $('#progress-full').hide();
                    console.log(response.data);
                    $("#messageLockModal").modal('show');
                    $("#messageLockcontent").text(response.data.msg);
                }
            }, function (response) {
                $('#progress-full').hide();
                console.log(response.data);
            });

        };

        $scope.classesMenuView = function () {
            $scope.mainMenuShow = false;
            $scope.membershipMenuShow = false;
            $scope.classesMenuShow = true;
        };  
        
        $scope.clearLockForm = function () {
            $scope.lockuseremail = $scope.lockpassword = '';
            $scope.studio_AuthenticationStatus = true;
            $scope.lock.$setPristine();
            $scope.lock.$setUntouched();
        };
        
        $scope.showlockMenu = function () {
            $scope.clearLockForm();
            $("#myModallock").modal('show');              
        };
        
        $scope.memCategorySelection = function (mem_id, memCategoryTitle, category_status) {
            $localStorage.memCategoryTitle = memCategoryTitle; 
            $localStorage.category_type = category_status;
            $scope.home(mem_id,category_status);
        };        
        
        $scope.MenuBack = function () {            
            if($scope.current_member_view === "child"){                    
                $scope.mainMenuShow = true;
                $scope.membershipMenuShow = false;
                $scope.classesMenuShow = false;
                $scope.current_member_view = "parent";
            }else if($scope.current_member_view === "parent"){
               $location.path('/home');
            }
        };

        $scope.lockMenu = function () {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'lockOrUnlockMenuDetails',
                data: {
                    "company_id": $localStorage.att_company_id,
                    "token":  $scope.attendance_token,
                    "email": $localStorage.att_user_email_id,
                    "lock_email": $scope.lockuseremail,
                    "lock_password": $scope.lockpassword
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $scope.clearLockForm();
                    $localStorage.menuLockStatus = 'Y';
                    $scope.menuLockStatus = $localStorage.menuLockStatus;
                    console.log(response.data);
                    $scope.studio_AuthenticationStatus = true;
                    $("#myModallock").modal('hide');
                    $scope.home($localStorage.membership_id,$localStorage.category_type);                    
                } else {
                    $('#progress-full').hide();
                    console.log(response.data);
                    $localStorage.menuLockStatus = 'N';
                    $scope.menuLockStatus = $localStorage.menuLockStatus;
                    if(response.data.msg === "Session expired."){
                        $("#messageLockModal").modal('show');
                        $("#messageLockcontent").text(response.data.msg);
                    }else{                        
                        $scope.studio_AuthenticationStatus = false;
                    }
                }
            }, function (response) {
                $('#progress-full').hide();
                console.log(response.data);
            });
        };
        
    }]);