//declare modules

angular.module("home", []);
angular.module("menu", []);
angular.module("classesprogram", []);

$('#progress-full').hide();

angular.module("myapp", ['ngRoute', 'home', 'menu', 'classesprogram', 'angular-md5', 'ngStorage', 'dndLists'])

        .config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
                $routeProvider
                        .when('/home', {
                            templateUrl: 'Module/View/Home.html',
                            controller: 'HomeController'
                        })
                        .when('/menu', {
                            templateUrl: 'Module/View/Menu.html',
                            controller: 'MenuController'
                        })
                        .when('/classesprogram', {
                            templateUrl: 'Module/View/ClassesProgram.html',
                            controller: 'ClassesProgramController'
                        })
//                        .otherwise({
//                            templateUrl: 'index.html',
//                            controller: 'IndexPageController'
//                        });

            }]);

angular.module('myapp').controller("myCtrl", ['$scope', '$http', '$location', '$route', '$rootScope', '$localStorage', '$sessionStorage', 'urlservice', function ($scope, $http, $location, $route, $rootScope, $localStorage, $sessionStorage, urlservice) {
        
        $scope.studio_AuthenticationStatus = true;
        $scope.attendance_token = "";
        $scope.studio_Loginmsg = "";
        $scope.studio_LoginStatus = true;

        $scope.home = function (membership_id,category_status) {
            $localStorage.membership_id = membership_id;
            $localStorage.category_type = $scope.category_type = category_status;
            $scope.menuLockStatus = $localStorage.menuLockStatus;
            $location.path('/home');
        };

        $scope.getTokenAccess = function () {
            if ($scope.company_id && $scope.user_email_id) {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'generateStudioAttendanceToken',
                    data: {
                        "company_id": $localStorage.att_company_id,
                        "email": $localStorage.att_user_email_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'}
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.studio_upgrade_status = response.data.upgrade_status;
                        $scope.studio_expiry_level = response.data.studio_expiry_level;
                        if($scope.studio_upgrade_status === 'F' || $scope.studio_upgrade_status === 'B'){
                            $("#messageexpiryModal").modal('show');
                            $("#messageexpirytitle").text('Message');
                            $("#messageexpirycontent").text('This feature is available with our Premium and White Label platform.');
                        }else if($scope.studio_expiry_level === "L2"){
                            $("#messageexpiryModal").modal('show');
                            $("#messageexpirytitle").text('Message');
                            $("#messageexpirycontent").text('Your studio has Expired');
                        }else{
                            $scope.menuLockStatus = $localStorage.menuLockStatus;
                            $scope.attendance_token = response.data.msg;
                            $localStorage.company_logo = response.data.logoURL;
                            $scope.home($scope.membership_id,$scope.category_type);
                        }
                    } else {
                        $('#progress-full').hide();
                        console.log(response.data);
                        $("#homeMessageModal").modal('show');
                        $("#homeMessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    $('#progress-full').hide();
                    console.log(response.data);
                });
            } else {
                $('#progress-full').hide();
                $sessionStorage.isAttLogin = 'N';
                $scope.loginuseremail = $scope.loginpassword = '';
                $scope.studio_LoginStatus = true;
                $scope.login.$setPristine();
                $scope.login.$setUntouched();
                $("#myModallogin").modal('show');
            }
        };

        $scope.attendanceLogin = function () {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'login',
                data: {
                    "email": $scope.loginuseremail,
                    "password": $scope.loginpassword
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $("#myModallogin").modal('hide');
                    $scope.studio_upgrade_status = response.data.msg.upgrade_status;
                    $scope.studio_expiry_level = response.data.msg.studio_expiry_level;
                    if($scope.studio_upgrade_status === 'F' || $scope.studio_upgrade_status === 'B' || $scope.studio_expiry_level === 'L2'){
                        $("#messageexpiryModal").modal('show');
                        $("#messageexpirytitle").text('Message');
                    }else{
                        $sessionStorage.isAttLogin = 'Y';
                        $localStorage.att_company_id = response.data.msg.company_id;
                        $localStorage.att_user_email_id = response.data.msg.user_email;
                        $scope.company_id = $localStorage.att_company_id;
                        $scope.user_email_id = $localStorage.att_user_email_id;
                        $scope.getTokenAccess();
                    }
                } else {
                    $('#progress-full').hide();
                    console.log(response.data);
                    $sessionStorage.isAttLogin = 'N';
                    if (response.data.msg === "Session expired.") {
                        $("#indexMessageModal").modal('show');
                        $("#indexMessagecontent").text(response.data.msg);
                    } else {
                        $scope.studio_Loginmsg = response.data.msg;
                        $scope.studio_LoginStatus = false;
                    }
                }
            }, function (response) {
                $('#progress-full').hide();
                console.log(response.data);
            });
        };

        $scope.attendanceurlredirection = function () {
            $scope.param1 = '';
            var decoded_url = decodeURIComponent(window.location.href);
            if (decoded_url.indexOf('?=') > -1) {
                var params = decoded_url.split('?=')[1].split('/');
                $scope.param1 = params[0];
            } else {
                $localStorage.membership_id = "";
                $localStorage.memCategoryTitle = $localStorage.category_type = "";
                $scope.param1 = "";
            }
            $scope.company_id = $localStorage.att_company_id;
            $scope.user_email_id = $localStorage.att_user_email_id;
            $scope.membership_id = $localStorage.membership_id;
            $scope.category_type = $localStorage.category_type;
            
            if ($scope.param1.indexOf('l') > -1) {
                $scope.getTokenAccess();
            } else {
                if ($sessionStorage.isAttLogin === 'Y') {
                    $scope.getTokenAccess();
                } else {      
                    $('#progress-full').hide();
                    $sessionStorage.isAttLogin = 'N';
                    $scope.loginuseremail = $scope.loginpassword = '';
                    $scope.studio_LoginStatus = true;
                    $scope.login.$setPristine();
                    $scope.login.$setUntouched();              
                    $("#myModallogin").modal('show');
                }
            }
        };
        
        $scope.studioUpgradeNow = function () {
            $localStorage.viewselectplanpage = true;
            var hosturl;
            if (window.location.hostname === 'www.mystudio.academy') {
                hosturl = 'https://www.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'dev.mystudio.academy') {
                 hosturl = 'http://dev.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'dev2.mystudio.academy') {
                 hosturl = 'http://dev2.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'stage.mystudio.academy') {
                hosturl = 'http://stage.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'localhost') {
                hosturl = 'http://' + window.location.hostname + project_location;
            } else {
                hosturl = 'http://' + window.location.hostname + project_location;
            }
            window.open(hosturl,"_self");
        };

    }])

        .run(function ($rootScope) {
            $rootScope.online = navigator.onLine ? 'online' : 'offline';
            $rootScope.$apply();

            if (window.addEventListener) {
                window.addEventListener("online", function () {
                    $rootScope.online = "online";
                    $rootScope.$apply();
                }, true);
                window.addEventListener("offline", function () {
                    $rootScope.online = "offline";
                    $rootScope.$apply();
                }, true);
            } else {
                document.body.ononline = function () {
                    $rootScope.online = "online";
                    $rootScope.$apply();
                };
                document.body.onoffline = function () {
                    $rootScope.online = "offline";
                    $rootScope.$apply();
                };
            }
        })

        .directive('valueRepeatComplete', function () {
            return function (scope, element, attrs) {
                if (scope.$last) {
                    // iteration is complete, do whatever post-processing
                    $('#progress-full').hide();
                } else {
                    $('#progress-full').show();
                }
            }
        })


        .factory('urlservice', function () {
            var hosturl;
            if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
                       window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                           window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                    var origin = window.location.origin;
                    hosturl = origin;
                }

            } else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'beta.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'https://' + www_url;
                }
            } else if (window.location.hostname === 'stage.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'localhost') {
                hosturl = 'http://' + window.location.hostname + attendance_location;
            } else {
                hosturl = 'http://' + window.location.hostname + attendance_location;
            }

            return {
                url: hosturl + '/attendance/Api/'
            };
        });