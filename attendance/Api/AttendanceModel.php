<?php

require_once __DIR__."/../../Globals/cont_log.php";
require_once 'PHPMailer_5.2.1/class.phpmailer.php';
require_once 'PHPMailer_5.2.1/class.smtp.php';

class AttendanceModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    private $server_url;

    public function __construct() {
        require_once __DIR__."/../../Globals/config.php";
        $this->inputs();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'dev.mystudio.academy') !== false){  //Development
            $this->server_url = "http://dev.mystudio.academy";
        }elseif(strpos($host, 'dev2.mystudio.academy') !== false){  //Development
            $this->server_url = "http://dev2.mystudio.academy";
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME'];
        }else{
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    /* login functionality using email_id */
    protected function checklogin($email, $password) {
        $password = md5($password);
        $sql = sprintf("SELECT user_id,user_email,user_password,u.company_id,u.deleted_flag, c.upgrade_status, c.studio_expiry_level
                    from user u LEFT JOIN `company` c ON u.`company_id`=c.`company_id`
                    where user_email='%s' limit 0,1", mysqli_real_escape_string($this->db,$email));
        $result = mysqli_query($this->db, $sql);
        $company_id = "";
        $user_id = "";

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    $out['company_id'] = $row['company_id'];
                    $out['user_email'] = $row['user_email'];
                    $company_id = $row['company_id'];
                    $user_id = $row['user_id'];
                    $pwd = $row['user_password'];
                    $deleted_flag = $row['deleted_flag'];
                    $out['upgrade_status'] = $row['upgrade_status'];
                    $out['studio_expiry_level'] = $row['studio_expiry_level'];
                }
                if($pwd!=$password){
                    $error = array('status' => "Failed", "msg" => "Username or password doesn't match our records.");
                    $this->response($this->json($error), 401);       // If "Company id" not available
                }
                if($deleted_flag=='Y'){
                    $error = array('status' => "Failed", "msg" => "User account doesn't exist.");
                    $this->response($this->json($error), 401);       // If "Company id" not available
                }
                $msg = array('status' => "Success", "msg" => $out);
                $this->response($this->json($msg), 200);       // If "Company id" not available
            } else {
                $error = array('status' => "Login Failed", "msg" => "Username or password doesn't match our records.");
                $this->response($this->json($error), 401);
            }
        }
    }
    
    private function checkStudioAndEmail($company_id, $email){
        $query = sprintf("SELECT * FROM `user` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Studio details.");
                $this->response($this->json($error), 401);
            }
        }
    }
    
    protected function generateToken($company_id, $email){
        $logo_url = '';
        $this->checkStudioAndEmail($company_id, $email);
        $query = sprintf("SELECT * FROM `attendance_token` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            $token = base64_encode(openssl_random_pseudo_bytes(3 * (64 >> 2)));
            if($num_rows>0){
                $query2 = sprintf("UPDATE `attendance_token` SET `token`='%s' WHERE `company_id`='%s' AND `user_email`='%s'", 
                        mysqli_real_escape_string($this->db, $token), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
            }else{
                $query2 = sprintf("INSERT INTO `attendance_token`( `company_id`, `user_email`, `token`) VALUES ('%s', '%s', '%s')", 
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email), mysqli_real_escape_string($this->db, $token));
            }
            $result2 = mysqli_query($this->db, $query2);                
            if(!$result2){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }else{
                $get_logo = sprintf("SELECT  `company_name`, `logo_URL`, upgrade_status, studio_expiry_level  FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
                $res_logo = mysqli_query($this->db, $get_logo);
                if(!$res_logo){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }else{
                    $num_rows2 = mysqli_num_rows($res_logo);
                    if($num_rows2>0){
                        $row = mysqli_fetch_assoc($res_logo);
                        $logo_url = $row['logo_URL'];
                        $upgrade_status = $row['upgrade_status'];
                        $studio_expiry_level = $row['studio_expiry_level'];
                    }
                }
                $msg = array("status"=>"Success", "msg"=>"$token", "logoURL"=>"$logo_url", "upgrade_status"=>"$upgrade_status", "studio_expiry_level"=>$studio_expiry_level);
                $this->response($this->json($msg), 200);
            }
        }
    }
    
    protected function verifyStudioAttendanceToken($company_id, $token, $email) {
        $query = sprintf("SELECT * FROM `attendance_token` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $old_token = $row['token'];
                $token_id = $row['attendance_token_id'];
                if($old_token==$token){
                    $query2 = sprintf("UPDATE `attendance_token` SET `no_of_access`=`no_of_access`+1 WHERE `attendance_token_id`='%s'", mysqli_real_escape_string($this->db, $token_id));
                    $result2 = mysqli_query($this->db, $query2);                
                    if(!$result2){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $msg = array("status"=>"Success", "msg"=>"Session is active.");
                }else{
                    $msg = array("status"=>"Failed", "msg"=>"Session expired.");
                }
                return $msg;
            }else{
                $msg = array("status"=>"Failed", "msg"=>"Session expired.");
                $this->response($this->json($msg), 401);
            }
        }
    }
    
    protected function getMembershipStudentDetailsForAttendance($company_id, $membership_id, $category, $call_back){
        $attendance=$attendance_mem=$attendance_trial = [];
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        
        $checkin_date_time = " CONVERT_TZ(`checkin_datetime`,$tzadd_add,'$new_timezone')";
//        $curr_date = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $given_date_time = "CURDATE()";        
        $start_dt_format = "%Y-%m-01 00:00:00";
        $end_dt_format = "%Y-%m-%d 23:59:59";
        
//        $query1 = sprintf("SELECT mr.`participant_id`, mr.`student_id`, mr. `membership_registration_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_structure`,  mr.`student_id`,  m.`category_title`, mr.`membership_registration_column_1` participant_first_name, mr.`membership_registration_column_2` participant_last_name, CONCAT(mr.`membership_registration_column_1`, ' ', mr.`membership_registration_column_2`) participant_full_name, mr.`membership_registration_column_3` date_of_birth,
//                IFNULL(mrk.`required_attendance_count`, '') required_attendance_count, mo.`no_of_classes`, mo.`attendance_limit_flag`, mo.`attendance_period`, (SELECT `attendance_id` FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `membership_option_id`=mr.`membership_option_id` AND `status`='N' ORDER BY `checkin_datetime` desc limit 0,1) attendance_id,
//                IFNULL((SELECT COUNT(*) FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `membership_option_id`=mr.`membership_option_id` AND `status`='N' 
//                AND IF(mo.membership_structure='NC' , 1, IF(mo.`attendance_limit_flag`='N',1,IF(mo.`attendance_period`='CPW',(checkin_datetime>=DATE_ADD($curr_date, INTERVAL - WEEKDAY($curr_date) DAY)),(checkin_datetime>=SUBDATE($curr_date, INTERVAL 29 DAY)))))), 0) no_of_class_attended,
//                IF(mo.`attendance_limit_flag`='Y', IF(IFNULL((SELECT COUNT(*) FROM `attendance` 
//                WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `membership_option_id`=mr.`membership_option_id` AND `status`='N' 
//                AND IF(mo.membership_structure='NC',1,IF(mo.`attendance_limit_flag`='N',1,IF(mo.`attendance_period`='CPW',(checkin_datetime>=DATE_ADD($curr_date, INTERVAL - WEEKDAY($curr_date) DAY)),(checkin_datetime>=SUBDATE($curr_date, INTERVAL 29 DAY)))))),0)<mo.`no_of_classes`,'N','Y'),'N') class_excd_flg,
//                IFNULL((SELECT SUM(1) FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `checkin_datetime`>=mr.`last_advanced_date` AND `status`='N' GROUP BY `company_id`, `membership_registration_id`),0) actual_attendance,
//                IF(TIMESTAMPDIFF(MINUTE, IFNULL((SELECT `checkin_datetime` FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `status`='N' ORDER BY `checkin_datetime` DESC LIMIT 0,1),'0000-00-00 00:00:00'),NOW())<30, 'Y', 'N') checkin_flag
//                FROM `membership_registration` mr
//                LEFT JOIN `membership` m ON mr.`company_id`=m.`company_id` AND mr.`membership_id`=m.`membership_id`
//                LEFT JOIN `membership_options` mo ON mr.`company_id`=mo.`company_id` AND mr.`membership_id`=mo.`membership_id` AND mr.`membership_option_id`=mo.`membership_option_id`
//                LEFT JOIN `participant` p ON mr.`company_id`=p.`company_id` AND mr.`student_id`=p.`student_id` AND mr.`participant_id`=p.`participant_id`
//                LEFT JOIN `membership_ranks` mrk ON mr.`company_id`=mrk.`company_id` AND mr.`membership_id`=mrk.`membership_id` AND mr.`rank_id`=mrk.`membership_rank_id`
//                WHERE mr.`company_id`='%s' AND IF('%s' IS NULL || '%s'='', 1, m.`membership_id`='%s') AND mr.`membership_status`='R' AND 
//                IF(mr.`membership_structure`='SE',IF(mr.`specific_end_date`>DATE(NOW()),1,0),1) ORDER BY `participant_last_name`", mysqli_real_escape_string($this->db,$company_id),
//                mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$membership_id));
        $query1 = sprintf("SELECT 'membership' as type, mr.`participant_id`, mr.`student_id`, mr. `membership_registration_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_structure`,  m.`category_title`, mr.`membership_registration_column_1` participant_first_name, mr.`membership_registration_column_2` participant_last_name, CONCAT(mr.`membership_registration_column_2`, ', ', mr.`membership_registration_column_1`) participant_full_name, mr.`membership_registration_column_3` date_of_birth,
                IFNULL(mrk.`required_attendance_count`, '') required_attendance_count,IF( mo.`no_of_classes`=0, '', mo.`no_of_classes`) no_of_classes, mo.`attendance_limit_flag`, mo.`attendance_period`, (SELECT `attendance_id` FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `membership_option_id`=mr.`membership_option_id` AND `status`='N' ORDER BY `checkin_datetime` desc limit 0,1) attendance_id,
                IFNULL((SELECT COUNT(*) FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `membership_option_id`=mr.`membership_option_id` AND `status`='N' 
                AND IF(mo.membership_structure='NC' , 1, IF(mo.`attendance_limit_flag`='N',1,IF(mo.`attendance_period`='CPW',($checkin_date_time BETWEEN DATE_SUB(SUBDATE(DATE($given_date_time), INTERVAL WEEKDAY(DATE($given_date_time)) DAY), interval 0 second) AND 
                    DATE_SUB(ADDDATE(DATE($given_date_time), INTERVAL 7-weekday(DATE($given_date_time)) DAY), interval 1 second)),
                    ($checkin_date_time BETWEEN DATE_FORMAT($given_date_time,'%s') AND DATE_FORMAT(LAST_DAY($given_date_time),'%s')))))), 0) no_of_class_attended,
                IF(mo.`attendance_limit_flag`='Y', IF(IFNULL((SELECT COUNT(*) FROM `attendance` 
                WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `membership_option_id`=mr.`membership_option_id` AND `status`='N' 
                AND IF(mo.membership_structure='NC',1,IF(mo.`attendance_limit_flag`='N',1,IF(mo.`attendance_period`='CPW',($checkin_date_time BETWEEN DATE_SUB(SUBDATE(DATE($given_date_time), INTERVAL WEEKDAY(DATE($given_date_time)) DAY), interval 0 second) AND 
                    DATE_SUB(ADDDATE(DATE($given_date_time), INTERVAL 7-weekday(DATE($given_date_time)) DAY), interval 1 second)),
                    ($checkin_date_time BETWEEN DATE_FORMAT($given_date_time,'%s') AND DATE_FORMAT(LAST_DAY($given_date_time),'%s')))))),0)<mo.`no_of_classes`,'N','Y'),'N') class_excd_flg,
                IFNULL((SELECT SUM(1) FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `checkin_datetime`>=mr.`last_advanced_date` AND `status`='N' GROUP BY `company_id`, `membership_registration_id`),0) actual_attendance,
                IF(TIMESTAMPDIFF(MINUTE, IFNULL((SELECT `checkin_datetime` FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `status`='N' ORDER BY `checkin_datetime` DESC LIMIT 0,1),'0000-00-00 00:00:00'),NOW())<30, 'Y', 'N') checkin_flag
                FROM `membership_registration` mr
                LEFT JOIN `membership` m ON mr.`company_id`=m.`company_id` AND mr.`membership_id`=m.`membership_id`
                LEFT JOIN `membership_options` mo ON mr.`company_id`=mo.`company_id` AND mr.`membership_id`=mo.`membership_id` AND mr.`membership_option_id`=mo.`membership_option_id`
                LEFT JOIN `participant` p ON mr.`company_id`=p.`company_id` AND mr.`student_id`=p.`student_id` AND mr.`participant_id`=p.`participant_id`
                LEFT JOIN `membership_ranks` mrk ON mr.`company_id`=mrk.`company_id` AND mr.`membership_id`=mrk.`membership_id` AND mr.`rank_id`=mrk.`membership_rank_id`
                WHERE mr.`company_id`='%s' AND IF('%s' IS NULL || '%s'='', 1, m.`membership_id`='%s') AND mr.`membership_status`='R' AND 
                IF(mr.`membership_structure`='SE',IF(mr.`specific_end_date`>DATE(NOW()),1,0),1) ORDER BY LOWER(`participant_last_name`)", 
                mysqli_real_escape_string($this->db,$start_dt_format), mysqli_real_escape_string($this->db,$end_dt_format), mysqli_real_escape_string($this->db,$start_dt_format), mysqli_real_escape_string($this->db,$end_dt_format), 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$membership_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if($call_back==1){
                return $error;
            }else{
                $this->response($this->json($error), 500);
            }
        }else{
            $num_rows = mysqli_num_rows($result1);
            if($num_rows>0){
                while($row1 = mysqli_fetch_assoc($result1)){
                    $attendance_mem[] = $row1;
                }
            }
        }
        
        $trials=sprintf("SELECT 'trial' as type, tr.`participant_id`, tr.`student_id`, tr. `trial_reg_id` as membership_registration_id, tr.`trial_id` as membership_id,'' membership_option_id,'' membership_structure,t.`trial_title` category_title, tr.`trial_registration_column_1` participant_first_name, tr.`trial_registration_column_2` participant_last_name, CONCAT(tr.`trial_registration_column_2`, ', ', tr.`trial_registration_column_1`) participant_full_name, tr.`trial_registration_column_3` date_of_birth,
                '' required_attendance_count,'' no_of_classes,'' attendance_limit_flag,'' attendance_period,(SELECT `trial_attendance_id` FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` AND `status`='N' ORDER BY `checkin_datetime` desc limit 0,1) attendance_id,
                '' no_of_class_attended,'' class_excd_flg, IFNULL((SELECT SUM(1) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id`  AND `status`='N' GROUP BY `company_id`, `trial_reg_id`),0) actual_attendance,
                IF(TIMESTAMPDIFF(MINUTE, IFNULL((SELECT `checkin_datetime` FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` AND `status`='N' ORDER BY `checkin_datetime` DESC LIMIT 0,1),'0000-00-00 00:00:00'),NOW())<30, 'Y', 'N') checkin_flag                 
                FROM `trial_registration` tr
                LEFT JOIN `trial` t ON tr.`company_id`=t.`company_id` AND tr.`trial_id`=t.`trial_id`
                LEFT JOIN `participant` p ON tr.`company_id`=p.`company_id` AND tr.`student_id`=p.`student_id` AND tr.`participant_id`=p.`participant_id`
                WHERE tr.`company_id`='$company_id' AND IF('$membership_id' IS NULL || '$membership_id'='', 1, t.`trial_id`='$membership_id') AND tr.`trial_status`='A'  ORDER BY LOWER(`participant_last_name`)",
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$membership_id), mysqli_real_escape_string($this->db,$membership_id));
        
        $result_trial = mysqli_query($this->db, $trials);
        if(!$result_trial){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$trials");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if($call_back==1){
                return $error;
            }else{
                $this->response($this->json($error), 500);
            }
        }else{
            $num_rows = mysqli_num_rows($result_trial);
            if($num_rows>0){
                while($row2 = mysqli_fetch_assoc($result_trial)){
                    $attendance_trial[] = $row2;
                }
            }
        }
        if($category=='A'){
            $attendance=array_merge($attendance_mem,$attendance_trial);
            
            if(count($attendance)>0){
                usort($attendance, function($a, $b) {
                    return strtolower($a['participant_last_name']) > strtolower($b['participant_last_name']);
                });
            }
        }elseif ($category=='M'){
            $attendance=$attendance_mem;
        }elseif ($category=='T') {
            $attendance=$attendance_trial;
        }
        if(empty($attendance)){
            $error = array('status' => "Failed", "msg" => "Student details not available.");
            if($call_back==1){
                return $error;
            }else{
                $this->response($this->json($error), 401);
            }
        }else{
            $msg = array("status"=>"Success", "msg"=>$attendance);
            if($call_back==1){
                return $msg;
            }else{
                $this->response($this->json($msg), 200);
            }
        }
        
    }
    
    protected function getMembershipCategoryDetailsForAttendance($company_id){
        $category=[];
        $query = sprintf("SELECT m.`company_id`, m.`membership_id`, m.`category_title` FROM `membership_registration` mr LEFT JOIN `membership` m ON mr.`company_id`=m.`company_id` AND mr.`membership_id`=m.`membership_id`
                WHERE mr.`company_id`='%s' AND mr.`membership_status`='R' AND 
                IF(mr.`membership_structure`='SE',IF(mr.`specific_end_date`>DATE(NOW()),1,0),1) Group by mr.membership_id", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $category[] = $row;
                }
                $msg = array("status"=>"Success", "msg"=>$category);
                $this->response($this->json($msg), 200);
            }else{
                $error = array('status' => "Failed", "msg" => "Category details not available.");
                $this->response($this->json($error), 401);
            }
        }
    }
     /* login functionality using email_id */
    
    
    public function membershipCheckinDetails($company_id, $participant_id, $mem_id, $mem_reg_id, $mem_opt_id, $list_type,$category){
        $getParticularMembershipDetails = $this->getMembershipStudentDetailsForAttendance($company_id, $mem_id, 'M', 1);
        if($getParticularMembershipDetails["status"]!="Success"){
            $this->response($this->json($getParticularMembershipDetails), 500);
        }else{
            $check_reg = 0;
            for($i=0;$i<count($getParticularMembershipDetails['msg']);$i++){
                if($mem_reg_id==$getParticularMembershipDetails['msg'][$i]['membership_registration_id']){
                    $check_reg=1;
                    if($getParticularMembershipDetails['msg'][$i]['class_excd_flg']=='Y'){
                        $error = array('status' => "Failed", "msg" => "You are a rock star and have reached your attendance limit. Please talk to us about adding on more classes.");
                        $this->response($this->json($error), 401);
                    }
                }
            }            
        }
        
        $insert_attendance = sprintf("INSERT INTO `attendance` (`company_id`,`participant_id`, `membership_registration_id`, `membership_id`, `membership_option_id`, `checkin_datetime`)VALUES ('%s','%s','%s','%s','%s',NOW())", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$participant_id), mysqli_real_escape_string($this->db,$mem_reg_id),mysqli_real_escape_string($this->db,$mem_id),mysqli_real_escape_string($this->db,$mem_opt_id));
        $result = mysqli_query($this->db, $insert_attendance);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_attendance");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            if($list_type == 'all'){
                $mem_id = "";
            }
            $participant = $this->getMembershipStudentDetailsForAttendance($company_id, $mem_id, $category, 1);
            $msg = array("status"=>"Success", "msg"=>"Checked In successfully.", "participant_details"=>$participant);
            $this->response($this->json($msg), 200);
        }
    }

    protected function checklockMenuDetails($lock_company_id, $lock_email, $lock_password) {
        $encrypted_password = md5($lock_password);
        $sql = sprintf("SELECT user_id,user_email,user_password,company_id,deleted_flag from user where user_email='%s' limit 0,1", mysqli_real_escape_string($this->db,$lock_email));
        $result = mysqli_query($this->db, $sql);
        $user_email = "";

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
             if($num_of_rows > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    $company_id = $row['company_id'];
                    $user_email = strtolower($row['user_email']);
                    $pwd = $row['user_password'];
                    $deleted_flag = $row['deleted_flag'];
                }
                if($deleted_flag =='Y'){
                    $error = array('status' => "Failed", "msg" => "User account doesn't exist.");
                    $this->response($this->json($error), 401);       // If "Company id" not available
                }
                if(($lock_company_id === $company_id) && (($lock_email === $user_email) && ($encrypted_password === $pwd))){
                    $error = array('status' => "Success", "msg" => "Valid Email and password");
                    $this->response($this->json($error), 200); 
                }else {
                    $error = array('status' => "Invalid", "msg" => "Invalid credentials");
                    $this->response($this->json($error), 401);  
                }               
            } else {
                $error = array('status' => "Invalid", "msg" => "Username or password doesn't match our records.");
                $this->response($this->json($error), 401);
            }
        }
    }

    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone='';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result_tz);                       
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    public function cancelMembershipCheckinDetails($company_id, $attendance_id, $mem_id,$category){
        $query = sprintf("UPDATE `attendance` SET `status`='C' WHERE `attendance_id`='%s' and `company_id`='%s'", mysqli_real_escape_string($this->db, $attendance_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $participant = $this->getMembershipStudentDetailsForAttendance($company_id, $mem_id, $category, 1);
            $msg = array("status"=>"Success", "msg"=>"Checked-In Cancelled Successfully.", "participant_details"=>$participant);
            $this->response($this->json($msg), 200);
        }
    }
    
    public  function  trialCheckinDetails($company_id,$participant_id,$trial_id,$reg_id,$list_type,$category){
        $insert_attendance=sprintf("INSERT INTO `trial_attendance`( `company_id`, `participant_id`, `trial_reg_id`, `trial_id`, `checkin_datetime` ) 
                VALUES ('%s','%s','%s','%s',NOW())",mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db,$participant_id),mysqli_real_escape_string($this->db,$reg_id),
                mysqli_real_escape_string($this->db, $trial_id));
        $result = mysqli_query($this->db, $insert_attendance);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_attendance");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            if($list_type == 'all'){
                $trial_id = "";
            }
            $participant = $this->getMembershipStudentDetailsForAttendance($company_id, $trial_id, $category, 1);
            $msg = array("status"=>"Success", "msg"=>"Checked In successfully.", "participant_details"=>$participant);
            $this->response($this->json($msg), 200);
        }

    }
    
    public function cancelTrialCheckinDetails($company_id, $attendance_id, $trial_id,$category){
        $query = sprintf("UPDATE `trial_attendance` SET `status`='C' WHERE `trial_attendance_id`='%s' and `company_id`='%s'", mysqli_real_escape_string($this->db, $attendance_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $participant = $this->getMembershipStudentDetailsForAttendance($company_id, $trial_id, $category, 1);
            $msg = array("status"=>"Success", "msg"=>"Checked-In Cancelled Successfully.", "participant_details"=>$participant);
            $this->response($this->json($msg), 200);
        }
    }
    
}

?>
