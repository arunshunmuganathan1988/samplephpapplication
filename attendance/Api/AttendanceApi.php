<?php

require_once 'AttendanceModel.php';

class AttendanceApi extends AttendanceModel {
    
    public function __construct(){
        parent::__construct();          // Init parent contructor
    }
    
    public function processApi(){
        $func = strtolower(trim(str_replace("/","",$_REQUEST['run'])));
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)){
            $this->_request = json_decode(file_get_contents('php://input'), true);
 	}
        $company_id = $token = $email = '';
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        if(isset($this->_request['token'])){
            $token = $this->_request['token'];
        }
        if(isset($this->_request['email'])){
            $email = $this->_request['email'];
        }
        if($company_id==6){
            $error = array('status' => "Failed", "msg" => "No participants available.");
            $this->response($this->json($error), 404);
        }
        if((int)method_exists($this,$func) > 0){
            if($_REQUEST['run']=='generateStudioAttendanceToken' || $_REQUEST['run']=='login'){
                $this->$func();
            }else{
                $verify = $this->verifyStudioAttendanceToken($company_id,$token,$email);
                if($verify['status']=='Success'){
                    $this->$func();
                }else{
                    $error = array('status' => "Failed", "msg" => $verify['msg']);
                    $this->response($this->json($error), 401);
                }
            }
        }else{
            $error = array("status" => "Failed", "code" => 404, "msg" => "Page not found.", "func" => $func);
            $this->response($this->json($error),404); // If the method not exist with in this class, response would be "Page not found".
        }
    }
    
    private function login() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $password = $email='';
        if (isset($this->_request['email'])) {
        $email = $this->_request['email'];
        }
        if (isset($this->_request['password'])) {
            $password = $this->_request['password'];
        }

        // Input validations
        if (!empty($email) && !empty($password)) {
            $this->checklogin($email, $password);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function generateStudioAttendanceToken() {
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id = $token = $email = '';
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        if(isset($this->_request['email'])){
            $email = $this->_request['email'];
        }
        
        if(!empty($company_id) && !empty($email)){
            $this->generateToken($company_id, $email);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function getMembershipStudents(){
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id = $membership_id = $category = '';
        
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        if(isset($this->_request['membership_id'])){
            $membership_id = $this->_request['membership_id'];
        }
        if(isset($this->_request['category'])){
            $category = $this->_request['category'];
        }
        if ($category == '' || $category == NULL ) {
            $category = 'A';
        }

        if(!empty($company_id)&& !empty($category)){
            $this->getMembershipStudentDetailsForAttendance($company_id, $membership_id, $category, 0);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function getMembershipCategories(){
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id = '';
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        
        if(!empty($company_id)){
            $this->getMembershipCategoryDetailsForAttendance($company_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    public function membershipCheckin(){
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id = $participant_id = $mem_id = $mem_opt_id = $mem_reg_id = $category = '';
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }        
        if(isset($this->_request['participant_id'])){
            $participant_id = $this->_request['participant_id'];
        }
        if(isset($this->_request['membership_id'])){
            $mem_id = $this->_request['membership_id'];
        }
        if(isset($this->_request['membership_option_id'])){
            $mem_opt_id = $this->_request['membership_option_id'];
        }
        if(isset($this->_request['membership_reg_id'])){
            $mem_reg_id = $this->_request['membership_reg_id'];
        }
        if(isset($this->_request['list_type'])){
            $list_type = $this->_request['list_type'];
        }
        if(isset($this->_request['type'])){
            $type = $this->_request['type'];
        }
        if(isset($this->_request['category'])){
            $category = $this->_request['category'];
        }
        if ($category == '' || $category == NULL) {
            $category = 'A';
        }
        if($type==''||$type==NULL){
            $type='all';
        }
        if($list_type==''||$list_type==Null){
            $list_type='membership';
        }
        if(!empty($company_id) && !empty($participant_id) && !empty($mem_id) && !empty($mem_reg_id) && !empty($mem_opt_id)&& !empty($type)&&$type=='membership'){
            $this-> membershipCheckinDetails($company_id, $participant_id, $mem_id , $mem_reg_id , $mem_opt_id, $list_type,$category);
        }elseif(!empty($company_id) && !empty($participant_id) && !empty($mem_id) && !empty($mem_reg_id) && !empty($type)&&$type=='trial'){
             $this-> trialCheckinDetails($company_id,$participant_id,$mem_id,$mem_reg_id,$list_type,$category);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function lockOrUnlockMenuDetails(){
       if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $lock_company_id = $lock_email = $lock_password = '';
        
        if(isset($this->_request['company_id'])){
            $lock_company_id = $this->_request['company_id'];
        }
        if(isset($this->_request['lock_email'])){
            $lock_email = strtolower($this->_request['lock_email']);
        }
        if(isset($this->_request['lock_password'])){
            $lock_password = $this->_request['lock_password'];
        }
         // Input validations
        if(!empty($lock_company_id) && !empty($lock_email) && !empty($lock_password)){
            $this->checklockMenuDetails($lock_company_id,$lock_email,$lock_password);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function cancelMembershipCheckin() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $attendance_id = $mem_id = '';
        $type='membership';
        $category='A';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['attendance_id'])) {
            $attendance_id = $this->_request['attendance_id'];
        }
        if(isset($this->_request['membership_id'])){
            $mem_id = $this->_request['membership_id'];
        }
        if(isset($this->_request['type'])){
            $type = $this->_request['type'];
        }
        if(isset($this->_request['category'])){
            $category = $this->_request['category'];
        }

        if (!empty($company_id) && !empty($attendance_id)&&$type=='membership') {
            $this->cancelMembershipCheckinDetails($company_id, $attendance_id, $mem_id,$category);
        }elseif (!empty($company_id) && !empty($attendance_id)&&$type=='trial') {
            $this->cancelTrialCheckinDetails($company_id, $attendance_id, $mem_id,$category);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
} 

// Initiiate Library
    $api = new AttendanceApi;
    $api->processApi();



?>