<?php
if (array_key_exists('HTTP_ORIGIN', $_SERVER)) { 
    $origin = $_SERVER['HTTP_ORIGIN']; 
} else if (array_key_exists('HTTP_REFERER', $_SERVER)) { 
    $origin = $_SERVER['HTTP_REFERER']; 
} else { 
    $origin = $_SERVER['REMOTE_ADDR']; 
}
$GLOBALS['origin']=$origin;
if (!isset($_SESSION)) {
    if (strpos($_SERVER['HTTP_HOST'], 'dev.mystudio.academy') !== false) {
        session_set_cookie_params(0, '/', '.dev.mystudio.academy');
    } else if (strpos($_SERVER['HTTP_HOST'], 'dev2.mystudio.academy') !== false) {
        session_set_cookie_params(0, '/', '.dev2.mystudio.academy');
    } else if (strpos($_SERVER['HTTP_HOST'], 'stage.mystudio.academy') !== false) {
        session_set_cookie_params(0, '/', '.stage.mystudio.academy');
    } else if (strpos($_SERVER['HTTP_HOST'], 'mystudio.academy') !== false) {
        session_set_cookie_params(0, '/', '.mystudio.academy');
    }else{
       session_set_cookie_params(0, '/', 'localhost');
    }
    session_start();
}
if (isset($_SESSION['last_activity']) && (time() - $_SESSION['last_activity'] > 3600)) {
    // last request was more than 60 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();   // destroy session data in storage
}
$_SESSION['last_activity'] = time(); // update last activity time stamp
require_once 'PortalModel.php';
require_once 'CurriculumModel.php';
require_once 'EventModel.php';
require_once 'MembershipModel.php';
require_once 'TrialModel.php';
require_once 'LeadsModel.php';
require_once 'CampaignModel.php';
require_once 'AdminModel.php';
require_once 'RetailModel.php';
require_once 'ChatModel.php';
require_once 'WhitelabelModel.php';
$file_from = "VERSION_CHECK";
require_once __DIR__.'/../../../mystudioapp.php';

class PortalApi extends PortalModel {

    private $curr_model, $event_model, $membership_model, $trial_model, $leads_model, $admin_model,$campaign_model,$retail_model,$chat_model,$whitelabel_model;

    public function __construct() {
        parent::__construct();          // Init parent contructor
        $this->curr_model = new CurriculumModel();
        $this->event_model = new EventModel();
        $this->membership_model = new MembershipModel();
        $this->trial_model = new TrialModel();
        $this->admin_model= new AdminModel();
        $this->leads_model = new LeadsModel();
        $this->campaign_model = new CampaignModel();
        $this->retail_model = new RetailModel();
        $this->chat_model = new ChatModel();
        $this->whitelabel_model = new WhitelabelModel();
    }

    /*
     * Public method for access api.
     * This method dynmically call the method based on the query string
    */
    
    public function processApi() {
        $func = strtolower(trim(str_replace("/", "", $_REQUEST['run'])));
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)) {
            $this->_request = json_decode(file_get_contents('php://input'), true);
        }
        if ((int) method_exists($this, $func) > 0) {
            $this->checkVersion();
            if (!isset($_SESSION['admin']) && $_REQUEST['run'] != 'verifySession' && $_REQUEST['run'] != 'login' && $_REQUEST['run'] != 'logout' && $_REQUEST['run'] != 'adminlogin' && $_REQUEST['run'] != 'register' && $_REQUEST['run'] != 'forgotPassword' && $_REQUEST['run'] != 'tokenvalidation' && $_REQUEST['run'] != 'resetPassword' && $_REQUEST['run'] != 'addvideos' && $_REQUEST['run'] != 'adminlogout' && $_REQUEST['run'] != 'verifyUserEmail' && $_REQUEST['run'] != 'verifyCompanyCode' && $_REQUEST['run'] != 'registerWebAppUser' && $_REQUEST['run'] != 'wepayGenerateUserToken' && $_REQUEST['run'] != 'stripeGenerateUserToken' && $_REQUEST['run'] != 'copyRetailFromStudio' && $_REQUEST['run'] != 'changeEmailSubscriptionStatus' && $_REQUEST['run'] != 'getSupportedCountryList') {
                $company_id = '';
                if (isset($this->_request['company_id'])) {
                    $company_id = $this->_request['company_id'];
                }
                if (!isset($_SESSION['user']) || !isset($_SESSION['pwd'])) {
                    $error = array('status' => "Expired", "msg" => "Session Expired. Please login again.");
                    $this->response($this->json($error), 401);
                }
                
                $verify = $this->verifyuser($company_id, $_SESSION['user'], $_SESSION['pwd']);
                if ($verify['status'] == 'Success') {
                    $this->$func();
                } else {
                    $error = array('status' => "Failed", "msg" => $verify['msg']);
                    $this->response($this->json($error), 401);
                }
            } else {
                $this->$func();
            }
        } else {
            $error = array("status" => "Failed", "code" => 404, "msg" => "Page not found.");
            $this->response($this->json($error), 404); // If the method not exist with in this class, response would be "Page not found".
        }
    }

    /*
     * 	Simple login API
     *  Login must be POST method
     *  email : <USER EMAIL>
     */

    private function login() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $password = $email= $login_from = '';
        if (isset($this->_request['email'])) {
            $email = $this->_request['email'];
        }
        if (isset($this->_request['login_from'])) {
            $login_from = $this->_request['login_from'];
        }
        if (isset($_SESSION['admin'])) {
            $password = "dummy";
        } else {
            if (isset($this->_request['password'])) {
                $password = $this->_request['password'];
            }
        }
        if ($login_from == "adminpanel" && isset($_SESSION) && !isset($_SESSION['admin'])) {
            $error = array("status" => "Expired","msg" => "Session Expired. Please login again.");
            $this->response($this->json($error), 406);
        }
        // Input validations
        if (!empty($email) && !empty($password)) {
            $this->checklogin($email, $password);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function checkVersion(){
        $current_path = explode('/', $_SERVER['PHP_SELF']);
        $current_path_ver_num = substr($current_path[1], 1, 2);
        if(strpos($_SERVER['PHP_SELF'],"localhost")!==false && $current_path_ver_num < $GLOBALS['MYSTUDIO_CURRENT_VERSION2']){
            $v = $GLOBALS['MYSTUDIO_CURRENT_VERSION1'].$GLOBALS['MYSTUDIO_CURRENT_VERSION2'];
            $url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME']."/$v/";
            $error = array('status' => "Version", "msg" => "Latest Version-$url");
            $this->response($this->json($error), 401);
        }
        
    }

    private function verifySession() {
        $status = "Success";
        $msg = "Already logged-in.";
        if (isset($_SESSION)) {
            if (!isset($_SESSION['admin']) && !isset($_SESSION['user'])) {
                $status = "Failed";
                $msg = "No session maintained.";
            }
        } else {
            $status = "Failed";
            $msg = "No session maintained.";
        }
        $error = array('status' => $status, "msg" => $msg);
        $this->response($this->json($error), 200);
    }

    private function adminlogin() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $email = $password = '';
        if (isset($this->_request['email'])) {
            $email = $this->_request['email'];
        }
        if (isset($this->_request['password'])) {
            $password = $this->_request['password'];
        }

        // Input validations
        if (!empty($email) && !empty($password)) {
//            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            $this->checkadminlogin($email, $password);
//            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function logout() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $this->logoutUser();
    }

    private function adminlogout() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $this->logoutAdmin();
    }

    private function register() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $email = $password = $company_name = $phone_number = $country = '';

        if (isset($this->_request['emailid'])) {
            $email = $this->_request['emailid'];
        }
        if (isset($this->_request['password'])) {
            $password = $this->_request['password'];
        }
        if (isset($this->_request['company_name'])) {
            $company_name = $this->_request['company_name'];
        }
        if (isset($this->_request['phone_number'])) {
            $phone_number = $this->_request['phone_number'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
//        // Input validations
        if (!empty($email) && !empty($password) && !empty($company_name) && !empty($phone_number) && !empty($country)) {
//            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            $this->registerUserAndCompany($email, $password, $company_name, $phone_number, $country);
//            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function registercompanylogo() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $company_logo = $image_type = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
//        $company_name = $this->_request['company_name'];
        if (isset($this->_request['company_logo'])) {
            $company_logo = $this->_request['company_logo'];
        }
        if (isset($this->_request['image_type'])) {
            $image_type = $this->_request['image_type'];
        }
        $image_url = "";
        if ($company_logo != '') {
            $folder_name = "Company_$company_id";
            $old = getcwd(); // Save the current directory
            $dir_name = "../../../$this->upload_folder_name";
            chdir($dir_name);
            if (!file_exists($folder_name)) {
                $oldmask = umask(0);
                mkdir($folder_name, 0777, true);
                umask($oldmask);
            }
            chdir($folder_name);
            $image_content = base64_decode($company_logo);
            $fileName = 'logo';
            //$fileName = str_replace(' ', '_', $company_name);
            $file = "$fileName.$image_type";
            $logo_img = $file; // logo - your file name
            $ifp = fopen($logo_img, "wb");
            fwrite($ifp, $image_content);
            fclose($ifp);
            chdir($old); // Restore the old working directory
//            $image_url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//            $image_url = implode('/', explode('/', $image_url, -2));
            $image_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/$folder_name/$file";
        }
        // Input validations
        if (!empty($company_id) && !empty($company_logo)) {
            $this->registerLogo($company_id, $company_logo, $image_url);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function registercompanycode() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $company_code = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['company_code'])) {
            $company_code = $this->_request['company_code'];
        }

        // Input validations
        if (!empty($company_id) && !empty($company_code)) {
            $this->registerCompanyCodeInDB($company_id, $company_code);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addevents() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $parent_id = $event_type = $event_status = $event_start_datetime = $event_end_datetime = $event_title = $event_category_subtitle = $event_desc = $event_video_detail_url = $event_cost = $event_compare_price = $event_banner_img_content = $event_banner_img_type = $event_sort_order = $event_template_flag = '';
        $event_capacity = NULL;
        $capacity_text = $list_type = $event_show_status = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['parent_id'])) {
            $parent_id = $this->_request['parent_id'];
        }
        if (isset($this->_request['event_type'])) {
            $event_type = $this->_request['event_type'];
        }
        if (isset($this->_request['event_status'])) {
            $event_status = $this->_request['event_status'];
        }
        if (isset($this->_request['event_start_datetime'])) {
            $event_start_datetime = $this->_request['event_start_datetime'];
        }
        if (isset($this->_request['event_end_datetime'])) {
            $event_end_datetime = $this->_request['event_end_datetime'];
        }
        if (isset($this->_request['event_title'])) {
            $event_title = $this->_request['event_title'];
        }
        if (isset($this->_request['event_category_subtitle'])) {
            $event_category_subtitle = $this->_request['event_category_subtitle'];
        }
        if (isset($this->_request['event_desc'])) {
            $event_desc = $this->_request['event_desc'];
        }
        if (isset($this->_request['event_video_detail_url'])) {
            $event_video_detail_url = $this->_request['event_video_detail_url'];
        }
        if (isset($this->_request['event_cost'])) {
            $event_cost = $this->_request['event_cost'];
        }
        if (isset($this->_request['event_compare_price'])) {
            $event_compare_price = $this->_request['event_compare_price'];
        }
        if (isset($this->_request['event_banner_img_content'])) {
            $event_banner_img_content = $this->_request['event_banner_img_content'];
        }
        if (isset($this->_request['event_banner_img_type'])) {
            $event_banner_img_type = $this->_request['event_banner_img_type'];
        }
        if (isset($this->_request['event_sort_order'])) {
            $event_sort_order = $this->_request['event_sort_order'];
        }
        if (isset($this->_request['event_capacity'])) {
            $event_capacity = $this->_request['event_capacity'];
        }
        if (isset($this->_request['capacity_text'])) {
            $capacity_text = $this->_request['capacity_text'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }
        if (isset($this->_request['event_show_status'])) {
            $event_show_status = $this->_request['event_show_status'];
        }

        // Input validations
        if (!empty($event_title) && !empty($company_id) && !empty($event_type)) {
            if (($event_type == "S") || ($event_type == "M")) {
                $this->event_model->insertEvent($company_id, $parent_id, $event_type, $event_status, $event_start_datetime, $event_end_datetime, $event_title, $event_category_subtitle, $event_desc, $event_video_detail_url, $event_cost, $event_compare_price, $event_banner_img_content, $event_banner_img_type, $event_sort_order, $event_capacity, $capacity_text, $list_type,$event_show_status);
            } else if (($event_type == "C") && !empty($parent_id)) {
                $this->event_model->insertEvent($company_id, $parent_id, $event_type, $event_status, $event_start_datetime, $event_end_datetime, $event_title, $event_category_subtitle, $event_desc, $event_video_detail_url, $event_cost, $event_compare_price, $event_banner_img_content, $event_banner_img_type, $event_sort_order, $event_capacity, $capacity_text, $list_type,$event_show_status);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function geteventdetails() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $parent_id = '';
        $limit = 0;
        $list_type = 'P';
        $from = 'W';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['list_type']) && !empty($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }
        if (isset($this->_request['limit'])){
            $limit = $this->_request['limit'];
        }
        if (isset($this->_request['parent_id'])){
            $parent_id = $this->_request['parent_id'];
        }
        if (isset($this->_request['from']) && !empty($this->_request['from'])){
            $from = $this->_request['from'];
        }
        if (!empty($company_id)){
            if($from === 'I'){ //From iframe or web => I - Iframe (get total list without limit)
                $this->event_model->getEventsDetailByCompany($company_id, $list_type, 0, $from);
            }else if(!empty($parent_id)){
                $this->event_model->getEventsDetailByParent($company_id, $list_type, $parent_id);
            }else{
                $this->event_model->checkEventFirstTime($company_id, $list_type, $limit, $from);
            }
            
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getsingleeventdetails() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_type = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['event_type'])) {
            $event_type = $this->_request['event_type'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }
        if (!empty($company_id) && !empty($event_id) && !empty($event_type) && !empty($list_type)) {
            $this->event_model->getEventsDetailByEventId($company_id, $event_id, $event_type, $list_type,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateeventstatus() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $event_flag = $company_id = $eventbrite_id = $eventbrite_url='';
        if (isset($this->_request['event_flag'])) {
            $event_flag = $this->_request['event_flag'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['eventbrite_id'])) {
            $eventbrite_id = $this->_request['eventbrite_id'];
        }
        if (isset($this->_request['eventbrite_url'])) {
            $eventbrite_url = $this->_request['eventbrite_url'];
        }
        // Input validations
        if (!empty($company_id)) {
            $this->event_model->updateEventStatusInDB($event_flag, $company_id, $eventbrite_id, $eventbrite_url);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteevents() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $event_id = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($company_id) && !empty($event_id)) {
            $this->event_model->deleteEvent($company_id, $event_id, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function copyevents() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $event_id = $copy_event_type = $event_template_flag = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['copy_event_type'])) {
            $copy_event_type = $this->_request['copy_event_type'];
        }
        if (isset($this->_request['event_template_flag'])) {
            $event_template_flag = $this->_request['event_template_flag'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($company_id) && !empty($event_id) && !empty($copy_event_type) && !empty($event_template_flag)) {
            $this->event_model->copyEvent($company_id, $event_id, $copy_event_type, $event_template_flag, $list_type,'');
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addeventsdiscount() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $event_id = $discount_type = $discount_code = $discount_amount = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['discount_type'])) {
            $discount_type = $this->_request['discount_type'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['discount_amount'])) {
            $discount_amount = $this->_request['discount_amount'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($event_id) && !empty($company_id) && !empty($discount_type) && !empty($discount_code) && !empty($discount_amount)) {
            $this->event_model->inserteventsdiscount($company_id, $event_id, $discount_type, $discount_code, $discount_amount, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateeventsdiscount() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $discount_type = $discount_code = $discount_amount = $event_discount_id = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['event_discount_id'])) {
            $event_discount_id = $this->_request['event_discount_id'];
        }
        if (isset($this->_request['discount_type'])) {
            $discount_type = $this->_request['discount_type'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['discount_amount'])) {
            $discount_amount = $this->_request['discount_amount'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($event_id) && !empty($company_id) && !empty($discount_type) && !empty($discount_code) && !empty($discount_amount) && !empty($event_discount_id)) {
            $this->event_model->updateEventSDiscountInDB($company_id, $event_id, $event_discount_id, $discount_type, $discount_code, $discount_amount, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateEventPaymentDetails() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_capacity = $capacity_text = $processing_fees = $event_onetime_payment_flag = $event_recurring_payment_flag = $total_order_text = $total_quantity_text = $deposit_amount = $number_of_payments = $payment_startdate = $payment_frequency = $event_type = $list_type = '';
        $payment_startdate_type = 4;
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
//        if(isset($this->_request['event_capacity']))
//            $event_capacity = $this->_request['event_capacity'];
//        if(isset($this->_request['capacity_text']))
//            $capacity_text = $this->_request['capacity_text'];
        if (isset($this->_request['processing_fees'])) {
            $processing_fees = $this->_request['processing_fees'];
        }
        if (isset($this->_request['event_onetime_payment_flag'])) {
            $event_onetime_payment_flag = $this->_request['event_onetime_payment_flag'];
        }
        if (isset($this->_request['event_recurring_payment_flag'])) {
            $event_recurring_payment_flag = $this->_request['event_recurring_payment_flag'];
        }
        if (isset($this->_request['total_order_text'])) {
            $total_order_text = $this->_request['total_order_text'];
        }
        if (isset($this->_request['total_quantity_text'])) {
            $total_quantity_text = $this->_request['total_quantity_text'];
        }
        if (isset($this->_request['deposit_amount'])) {
            $deposit_amount = $this->_request['deposit_amount'];
        }
        if (isset($this->_request['number_of_payments'])) {
            $number_of_payments = $this->_request['number_of_payments'];
        }
        if (isset($this->_request['payment_startdate_type'])) {
            $payment_startdate_type = $this->_request['payment_startdate_type'];
        }
        if (isset($this->_request['payment_startdate'])) {
            $payment_startdate = $this->_request['payment_startdate'];
        }
        if (isset($this->_request['payment_frequency'])) {
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($event_id) && !empty($company_id)) {
            $this->event_model->updateEventPaymentDetailsInDB($company_id, $event_id, $processing_fees, $event_onetime_payment_flag, $event_recurring_payment_flag, $total_order_text, $total_quantity_text, $deposit_amount, $number_of_payments, $payment_startdate_type, $payment_startdate, $payment_frequency, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteEventDiscount() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_discount_id = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['event_discount_id'])) {
            $event_discount_id = $this->_request['event_discount_id'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($event_id) && !empty($company_id) && !empty($event_discount_id)) {
            $this->event_model->deleteEventDiscountInDB($company_id, $event_id, $event_discount_id, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateWaiver() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $waiver_policies = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['waiver_policies'])) {
            $waiver_policies = $this->_request['waiver_policies'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($event_id) && !empty($company_id)) {
            $this->event_model->updateWaiverDetailsInDB($company_id, $event_id, $waiver_policies, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateEventSortingOrder() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id =$event_id =$event_sort_order ='';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['sort_id'])) {
            $event_sort_order = $this->_request['sort_id'];
        }

        if (!empty($company_id) && !empty($event_id) && !empty($event_sort_order)) {
            $this->event_model->updateEventSortingDetails($event_sort_order, $company_id, $event_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateEventRegistration() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_column_name = $event_column_value = $event_mandatory_column_name = $event_mandatory_column_value = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['event_column_name'])) {
            $event_column_name = $this->_request['event_column_name'];
        }
        if (isset($this->_request['event_column_value'])) {
            $event_column_value = $this->_request['event_column_value'];
        }
        if (isset($this->_request['event_mandatory_column_name'])) {
            $event_mandatory_column_name = $this->_request['event_mandatory_column_name'];
        }
        if (isset($this->_request['event_mandatory_column_value'])) {
            $event_mandatory_column_value = $this->_request['event_mandatory_column_value'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($event_id) && !empty($company_id) && !empty($event_column_name) && !empty($event_column_value)) {
            $this->event_model->updateEventRegistrationDetailsInDB($company_id, $event_id, $event_column_name, $event_column_value, $event_mandatory_column_name, $event_mandatory_column_value, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteEventRegistration() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_column_name = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['event_column_name'])) {
            $event_column_name = $this->_request['event_column_name'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($event_id) && !empty($company_id) && !empty($event_column_name)) {
            $this->event_model->deleteEventRegistrationDetailsInDB($company_id, $event_id, $event_column_name, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function eventRegistrationSorting() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $old_location_id = $new_location_id = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['old_location_id'])) {
            $old_location_id = $this->_request['old_location_id'];
        }
        if (isset($this->_request['new_location_id'])) {
            $new_location_id = $this->_request['new_location_id'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($event_id) && !empty($company_id) && !empty($old_location_id) && !empty($new_location_id)) {
            $this->event_model->eventRegistrationSortingDetailsInDB($company_id, $event_id, $old_location_id, $new_location_id, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getEventTemplate() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $from = 'W';    //W - Web, I - iframe
        $this->event_model->getEventsDetailByCompany("6", 'P', 0, $from);
    }

    private function getsocialdetails() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->getSocialDetailsByCompany($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updatesocialdetails() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $twitter_id = $twitter_url = $facebook_id = $facebook_url = $instagram_id = $instagram_url = $vimeo_id = $vimeo_url = $youtube_id = $youtube_url = $company_id = $google_url = $yelp_url = '';
        if (isset($this->_request['twitter_id'])) {
        $twitter_id = $this->_request['twitter_id'];
        }
        if (isset($this->_request['twitter_url'])) {
        $twitter_url = $this->_request['twitter_url'];
        }
        if (isset($this->_request['facebook_id'])) {
        $facebook_id = $this->_request['facebook_id'];
        }
        if (isset($this->_request['facebook_url'])) {
        $facebook_url = $this->_request['facebook_url'];
        }
        if (isset($this->_request['instagram_id'])) {
        $instagram_id = $this->_request['instagram_id'];
        }
        if (isset($this->_request['instagram_url'])) {
        $instagram_url = $this->_request['instagram_url'];
        }
        if (isset($this->_request['vimeo_id'])) {
        $vimeo_id = $this->_request['vimeo_id'];
        }
        if (isset($this->_request['vimeo_url'])) {
        $vimeo_url = $this->_request['vimeo_url'];
        }
        if (isset($this->_request['youtube_id'])) {
        $youtube_id = $this->_request['youtube_id'];
        }
        if (isset($this->_request['youtube_url'])) {
        $youtube_url = $this->_request['youtube_url'];
        }
        if (isset($this->_request['company_id'])) {
        $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['google_url'])) {
        $google_url = $this->_request['google_url'];
        }
        if (isset($this->_request['yelp_url'])) {
        $yelp_url = $this->_request['yelp_url'];
        }

        // Input validations
        if (!empty($company_id)) {
            $this->updateSocialDetailsInDB($twitter_id, $twitter_url, $facebook_id, $facebook_url, $instagram_id, $instagram_url, $vimeo_id, $vimeo_url, $youtube_id, $youtube_url, $google_url, $yelp_url, $company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updatecompany() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $fileInput = $old_logo_url = $image_type = $image_update = $company_code = $company_name = $web_page = $street_address = $city = $state = $postal_code = '';
        $country = $timezone = $phone_number = $test_email_id = $user_id = $company_id = $class_schedule = $class_schedule_type = $class_schedule_update = $old_schedule_url = $company_type = '';
        if (isset($this->_request['logo_url'])) {
            $fileInput = $this->_request['logo_url'];
        }
        if (isset($this->_request['old_logo_url'])) {
            $old_logo_url = $this->_request['old_logo_url'];
        }
        if (isset($this->_request['image_type'])) {
            $image_type = $this->_request['image_type'];
        }
        if (isset($this->_request['image_update'])) {
            $image_update = $this->_request['image_update'];
        }
        if (isset($this->_request['company_code'])) {
            $company_code = $this->_request['company_code'];
        }
        if (isset($this->_request['company_name'])) {
            $company_name = $this->_request['company_name'];
        }
        if (isset($this->_request['web_page'])) {
            $web_page = $this->_request['web_page'];
        }
        if (isset($this->_request['street_address'])) {
            $street_address = $this->_request['street_address'];
        }
        if (isset($this->_request['city'])) {
            $city = $this->_request['city'];
        }
        if (isset($this->_request['state'])) {
            $state = $this->_request['state'];
        }
        if (isset($this->_request['postal_code'])) {
            $postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['timezone'])) {
            $timezone = $this->_request['timezone'];
        }
        if (isset($this->_request['phone_number'])) {
            $phone_number = $this->_request['phone_number'];
        }
        if (isset($this->_request['email_id'])) {
            $test_email_id = $this->_request['email_id'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['user_id'])) {
            $user_id = $this->_request['user_id'];
        }
        if (isset($this->_request['class_schedule'])) {
            $class_schedule = $this->_request['class_schedule'];
        }
        if (isset($this->_request['class_schedule_type'])) {
            $class_schedule_type = $this->_request['class_schedule_type'];
        }
        if (isset($this->_request['class_schedule_update'])) {
            $class_schedule_update = $this->_request['class_schedule_update'];
        }
        if (isset($this->_request['old_schedule_url'])) {
            $old_schedule_url = $this->_request['old_schedule_url'];
        }
        if (isset($this->_request['company_type'])) {
            $company_type = $this->_request['company_type'];
        }
        $image_url = $cs_file_url = "";
        if ($fileInput != '') {
            if ($image_update === 'Y') {
                $folder_name = "Company_$company_id";
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name";
                chdir($dir_name);
                if (!file_exists($folder_name)) {
                    $oldmask = umask(0);
                    mkdir($folder_name, 0777, true);
                    umask($oldmask);
                }
                chdir($folder_name);
                $image_content = base64_decode($fileInput);
                $fileName = 'logo';
                //$fileName = str_replace(' ', '_', $company_name);
                $file = $fileName . "_" . time() . ".$image_type";
                $logo_img = $file; // logo - your file name
                $ifp = fopen($logo_img, "wb");
                fwrite($ifp, $image_content);
                fclose($ifp);
                chdir($old); // Restore the old working directory
//                $image_url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//                $image_url = implode('/', explode('/', $image_url, -2));
                $image_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/$folder_name/$file";
            }
        } else {
            if ($old_logo_url != '') {
                $old = getcwd(); // Save the current directory
                $folder_name = "Company_$company_id";
                $path_parts = pathinfo($old_logo_url);
                $dir_name = "../../../$this->upload_folder_name/$folder_name";
                $filename = $path_parts['basename'];
                chdir($dir_name);
                if (file_exists($filename)) {
                    unlink($filename);
                }
                chdir($old); // Restore the old working directory
            }
        }
        if ($class_schedule != '') {
            if ($class_schedule_update === 'Y') {
                $folder_name = "Company_$company_id";
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name";
                chdir($dir_name);
                if (!file_exists($folder_name)) {
                    $oldmask = umask(0);
                    mkdir($folder_name, 0777, true);
                    umask($oldmask);
                }
                chdir($folder_name);
                $image_content = base64_decode($class_schedule);
                $file = "cs". "_" . time() ."$class_schedule_type";
                $logo_img = $file; // logo - your file name
                $ifp = fopen($logo_img, "wb");
                fwrite($ifp, $image_content);
                fclose($ifp);
                chdir($old); // Restore the old working directory
//                $cs_file_url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//                $cs_file_url = implode('/', explode('/', $cs_file_url, -2));
                $cs_file_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/$folder_name/$file";
            }
        } else {
            if ($old_schedule_url != '') {
                $old = getcwd(); // Save the current directory
                $folder_name = "Company_$company_id";
                $path_parts = pathinfo($old_schedule_url);
                $dir_name = "../../../$this->upload_folder_name/$folder_name";
                $filename = $path_parts['basename'];
                chdir($dir_name);
                if (file_exists($filename)) {
                    unlink($filename);
                }
                chdir($old); // Restore the old working directory
            }
        }
        if (isset($_SESSION['user'])) {
            $email = $_SESSION['user'];
        }
//        $email = $this->_request['emailid'];
        $verification_code = $this->_request['verification_code'];
        $registration_complete = $this->_request['registration_complete'];

//        // Input validations
        if (!empty($email) && !empty($verification_code) && !empty($registration_complete) && $registration_complete != 'Y') {
            $res = $this->verifyUserRegistrationDetails($email, $verification_code, $company_id, 1);   // 0 - echo result, 1 - return result
        }
        // Input validations
        if (!empty($company_code) && !empty($company_name) && !empty($company_id)) {
            $this->updateCompanyDetails($image_update, $image_url, $fileInput, $company_code, $company_name, $web_page, $street_address, $city, $state, $postal_code, $country, $timezone, $phone_number, $test_email_id, $cs_file_url, $class_schedule_update, $company_type, $company_id, $user_id, $old_logo_url);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateevents() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $event_id = $company_id = $event_start_datetime = $event_end_datetime = $event_title = $event_category_subtitle = $event_desc = $event_video_detail_url = $event_cost = $event_compare_price = $event_banner_img_content = $event_banner_img_type = $event_compare_price = $event_status = $parent_id = $event_type = $event_image_update = $event_sort_order = $list_type = $old_event_banner_img_url = '';
        $capacity_text = $event_show_status = '';
        $event_capacity = NULL;

        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_start_datetime'])){
            $event_start_datetime = $this->_request['event_start_datetime'];
        }
        if (isset($this->_request['event_end_datetime'])){
            $event_end_datetime = $this->_request['event_end_datetime'];
        }
        if (isset($this->_request['event_title'])){
            $event_title = $this->_request['event_title'];
        }
        if (isset($this->_request['event_category_subtitle'])){
            $event_category_subtitle = $this->_request['event_category_subtitle'];
        }
        if (isset($this->_request['event_desc'])){
            $event_desc = $this->_request['event_desc'];
        }
        if (isset($this->_request['event_video_detail_url'])){
            $event_video_detail_url = $this->_request['event_video_detail_url'];
        }
        if (isset($this->_request['event_cost'])){
            $event_cost = $this->_request['event_cost'];
        }
        if (isset($this->_request['event_banner_img_content'])){
            $event_banner_img_content = $this->_request['event_banner_img_content'];
        }
        if (isset($this->_request['event_compare_price'])){
            $event_compare_price = $this->_request['event_compare_price'];
        }
        if (isset($this->_request['event_banner_img_type'])){
            $event_banner_img_type = $this->_request['event_banner_img_type'];
        }
        if (isset($this->_request['event_status'])){
            $event_status = $this->_request['event_status'];
        }
        if (isset($this->_request['parent_id'])){
            $parent_id = $this->_request['parent_id'];
        }
        if (isset($this->_request['event_type'])){
            $event_type = $this->_request['event_type'];
        }
        if (isset($this->_request['event_image_update'])){
            $event_image_update = $this->_request['event_image_update'];
        }
        if (isset($this->_request['event_banner_img_url'])){
            $old_event_banner_img_url = $this->_request['event_banner_img_url'];
        }
        if (isset($this->_request['event_sort_order'])){
            $event_sort_order = $this->_request['event_sort_order'];
        }
        if (isset($this->_request['list_type'])){
            $list_type = $this->_request['list_type'];
        }
        if (isset($this->_request['event_capacity'])){
            $event_capacity = $this->_request['event_capacity'];
        }
        if (isset($this->_request['capacity_text'])){
            $capacity_text = $this->_request['capacity_text'];
        }
        if (isset($this->_request['event_show_status'])){
            $event_show_status = $this->_request['event_show_status'];
        }

        $event_banner_img_url = "";
        if ($event_image_update === 'Y' && $event_banner_img_content != '') {
            $cfolder_name = "Company_$company_id";
            $old = getcwd(); // Save the current directory
            $dir_name = "../../../$this->upload_folder_name";
            chdir($dir_name);
            if (!file_exists($cfolder_name)) {
                $oldmask = umask(0);
                mkdir($cfolder_name, 0777, true);
                umask($oldmask);
            }
            chdir($cfolder_name);
            $folder_name = "Events";
            if (!file_exists($folder_name)) {
                $oldmask = umask(0);
                mkdir($folder_name, 0777, true);
                umask($oldmask);
            }
            chdir($folder_name);
            $image_content = base64_decode($event_banner_img_content);
            $file_name = "$event_id" . "_" . time();
            $file = "$file_name.$event_banner_img_type";
            $img = $file; // logo - your file name
            $ifp = fopen($img, "wb");
            fwrite($ifp, $image_content);
            fclose($ifp);
            chdir($old); // Restore the old working directory
//            $event_banner_img_url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//            $event_banner_img_url = implode('/', explode('/', $event_banner_img_url, -2));
            $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/$cfolder_name/$folder_name/$file";
        } elseif ($event_image_update === 'Y' && $event_banner_img_content == '') {
            $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/Default/default.png";
        }

        // Input validations

        if (!empty($event_title) && !empty($company_id) && !empty($event_type)) {
            if (($event_type == "S") || ($event_type == "M")) {
                $this->event_model->updateEventDetails($event_id, $company_id, $event_type, $event_start_datetime, $event_end_datetime, $event_title, $event_category_subtitle, $event_desc, $event_video_detail_url, $event_cost, $event_compare_price, $event_banner_img_url, $event_status, $parent_id, $event_image_update, $event_sort_order, $list_type, $old_event_banner_img_url, $event_capacity, $capacity_text,$event_show_status);
            } else if (($event_type == "C") && !empty($parent_id)) {
                $this->event_model->updateEventDetails($event_id, $company_id, $event_type, $event_start_datetime, $event_end_datetime, $event_title, $event_category_subtitle, $event_desc, $event_video_detail_url, $event_cost, $event_compare_price, $event_banner_img_url, $event_status, $parent_id, $event_image_update, $event_sort_order, $list_type, $old_event_banner_img_url, $event_capacity, $capacity_text,$event_show_status);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addmessage() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $message_text = $message_link = $company_id = $schedule_date_time = $email_status = $push_status = '';
        if (isset($this->_request['message_text'])) {
            $message_text = $this->_request['message_text'];
        }
        if (isset($this->_request['message_link'])) {
            $message_link = $this->_request['message_link'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['schedule_date_time'])) {
            $schedule_date_time = $this->_request['schedule_date_time'];
        }
        if (isset($this->_request['email_status'])) {
            $email_status = $this->_request['email_status'];
        }
        if (isset($this->_request['push_status'])) {
            $push_status = $this->_request['push_status'];
        }

        // Input validations
        if (!empty($message_text)) {
            $this->insertMessageDetails($message_text, $message_link, $company_id, $schedule_date_time, $email_status, $push_status);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updatemessage() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $message_id = $message_text = $message_link = $company_id = $schedule_date_time = $email_status = $push_status = '';
        if (isset($this->_request['message_id'])) {
            $message_id = $this->_request['message_id'];
        }
        if (isset($this->_request['message_text'])) {
            $message_text = $this->_request['message_text'];
        }
        if (isset($this->_request['message_link'])) {
            $message_link = $this->_request['message_link'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['schedule_date_time'])) {
            $schedule_date_time = $this->_request['schedule_date_time'];
        }
        if (isset($this->_request['email_status'])) {
            $email_status = $this->_request['email_status'];
        }
        if (isset($this->_request['push_status'])) {
            $push_status = $this->_request['push_status'];
        }

        // Input validations
        if (!empty($message_text)) {
//            $this->updateMessageDetails($message_id, $message_text, $message_link, $company_id, $schedule_date_time, $email_status, $push_status);
            $this->insertMessageDetails($message_text, $message_link, $company_id, $schedule_date_time, $email_status, $push_status);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getmessagedetails() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->getMessageDetailByCompany($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addcurriculum() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $created_user_id = $curr_name = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['user_id'])) {
            $created_user_id = $this->_request['user_id'];
        }
        if (isset($this->_request['curriculum_name'])) {
            $curr_name = $this->_request['curriculum_name'];
        }

        // Input validations
        if (!empty($curr_name) && !empty($created_user_id) && !empty($company_id)) {
            $this->curr_model->insertCurriculum($company_id, $curr_name, $created_user_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addcurriculumcontent() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $created_user_id = $created_user_id = $content_title = $content_description = $content_video_url = $content_video_type = $video_id = $content_sort_order = $curr_id = $curr_detail_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['user_id'])) {
            $created_user_id = $this->_request['user_id'];
        }
        if (isset($this->_request['content_title'])) {
            $content_title = $this->_request['content_title'];
        }
        if (isset($this->_request['content_description'])) {
            $content_description = $this->_request['content_description'];
        }
        if (isset($this->_request['content_video_url'])) {
            $content_video_url = $this->_request['content_video_url'];
        }
        if (isset($this->_request['content_video_type'])) {
            $content_video_type = $this->_request['content_video_type'];
        }
        if (isset($this->_request['video_id'])) {
            $video_id = $this->_request['video_id'];
        }
        if (isset($this->_request['content_sort_order'])) {
            $content_sort_order = $this->_request['content_sort_order'];
        }
        if (isset($this->_request['curriculum_id'])) {
            $curr_id = $this->_request['curriculum_id'];
        }
        if (isset($this->_request['curriculum_detail_id'])) {
            $curr_detail_id = $this->_request['curriculum_detail_id'];
        }
        // Input validations
        if (!empty($company_id) && !empty($created_user_id) && !empty($content_video_type) && !empty($curr_id) && !empty($curr_detail_id)) {
            $this->curr_model->insertCurriculumContentDetails($company_id, $created_user_id, $content_title, $content_description, $content_video_url, $content_video_type, $video_id, $curr_id, $curr_detail_id, $content_sort_order);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getcurriculumdetails() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->curr_model->getCurriculumDetailByCompany($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getcurriculumcontentdetails() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $curr_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['curriculum_id'])) {
            $curr_id = $this->_request['curriculum_id'];
        }

        if (!empty($company_id) && !empty($curr_id)) {
            $this->curr_model->getCurriculumContentDetail($curr_id, $company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updatecurriculum() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $curr_id = $curr_name = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['curriculum_id'])) {
            $curr_id = $this->_request['curriculum_id'];
        }
        if (isset($this->_request['curriculum_name'])) {
            $curr_name = $this->_request['curriculum_name'];
        }

        // Input validations
        if (!empty($curr_name) && !empty($curr_id) && !empty($company_id)) {
            $this->curr_model->updateCurriculumDet($company_id, $curr_id, $curr_name);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updatecurriculumcontent() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $content_id = $content_title = $content_description = $content_video_url = $content_video_type = $video_id = $curr_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['content_id'])) {
            $content_id = $this->_request['content_id'];
        }
        if (isset($this->_request['content_title'])) {
            $content_title = $this->_request['content_title'];
        }
        if (isset($this->_request['content_description'])) {
            $content_description = $this->_request['content_description'];
        }
        if (isset($this->_request['content_video_url'])) {
            $content_video_url = $this->_request['content_video_url'];
        }
        if (isset($this->_request['content_video_type'])) {
            $content_video_type = $this->_request['content_video_type'];
        }
        if (isset($this->_request['video_id'])) {
            $video_id = $this->_request['video_id'];
        }
        if (isset($this->_request['curriculum_id'])) {
            $curr_id = $this->_request['curriculum_id'];
        }
        // Input validations
        if (!empty($company_id) && !empty($curr_id) && !empty($content_id)) {
            $this->curr_model->updateCurriculumContentDetails($company_id, $content_id, $content_title, $content_description, $content_video_url, $content_video_type, $video_id, $curr_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateCurriculumContentSortingOrder() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $content_id = $curriculum_id = $curriculum_sort_order = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['content_id'])) {
            $content_id = $this->_request['content_id'];
        }
        if (isset($this->_request['curriculum_id'])) {
            $curriculum_id = $this->_request['curriculum_id'];
        }
        if (isset($this->_request['sort_id'])) {
            $curriculum_sort_order = $this->_request['sort_id'];
        }

        if (!empty($content_id) && !empty($company_id) && !empty($curriculum_sort_order) && !empty($curriculum_id)) {
            $this->curr_model->updateCurriculumContentSortingDetails($content_id, $curriculum_sort_order, $company_id, $curriculum_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateCurriculumSortingOrder() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $curriculum_id = $curriculum_sort_order = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['curriculum_id'])) {
            $curriculum_id = $this->_request['curriculum_id'];
        }
        if (isset($this->_request['sort_id'])) {
            $curriculum_sort_order = $this->_request['sort_id'];
        }

        if (!empty($company_id) && !empty($curriculum_sort_order) && !empty($curriculum_id)) {
            $this->curr_model->updateCurriculumSortingDetails($curriculum_sort_order, $company_id, $curriculum_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deletecurriculumcontent() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $content_id = $curriculum_id = $company_id = '';
        if (isset($this->_request['content_id'])) {
            $content_id = $this->_request['content_id'];
        }
        if (isset($this->_request['curriculum_id'])) {
            $curriculum_id = $this->_request['curriculum_id'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        // Input validations
        if (!empty($content_id) && !empty($curriculum_id) && !empty($company_id)) {
            $this->curr_model->deleteCurriculumContentDetails($company_id, $content_id, $curriculum_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deletecurriculum() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $curriculum_id = $company_id = '';
        if (isset($this->_request['curriculum_id'])) {
            $curriculum_id = $this->_request['curriculum_id'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        // Input validations
        if (!empty($curriculum_id) && !empty($company_id)) {
            $this->curr_model->deleteCurriculumDetails($company_id, $curriculum_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getstudent() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() == "GET" || $this->get_request_method() == "POST") {
           $company_id= $search = $draw_table = $length_table = $start = $sorting = '';
            $for = '';
//        $data_limit = 0;
            if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
            }
            if (isset($this->_request['for'])) {
                $for = $this->_request['for'];
            }
            if (isset($this->_request['search'])) {
                $search = $this->_request['search'];
            }
            if (isset($this->_request['draw'])) {
                $draw_table = $this->_request['draw'];
            }
            if (isset($this->_request['length'])) {
                $length_table = $this->_request['length'];
            }
            if (isset($this->_request['start'])) {
                $start = $this->_request['start'];
            }

            if (isset($this->_request['order'])) {
                $sorting = $this->_request['order'];
            }

            if (!empty($company_id)) {
                if (empty($for)) {
                    $this->getStudentDetails($company_id, $search, $draw_table, $length_table, $start, $sorting);
                } else if ($for == 'membership' || $for == 'event' || $for == 'trial') {
                    $this->getStudentPaymentInfoDetails($company_id, 0);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
    }

    private function updatestudent() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $student_id = $student_name = $student_lastname ='';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['student_name'])) {
            $student_name = $this->_request['student_name'];
        }
        if (isset($this->_request['student_lastname'])) {
            $student_lastname = $this->_request['student_lastname'];
        }
        if (isset($this->_request['student_email'])) {
            $student_email = $this->_request['student_email'];
        }

        if (!empty($company_id) && !empty($student_id) && !empty($student_name) && !empty($student_lastname)) {
            $this->updateStudentDetails($company_id, $student_id, $student_name, $student_lastname, $student_email);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateeventbrite() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $eventbrite_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['eventbrite_id'])) {
            $eventbrite_id = $this->_request['eventbrite_id'];
        }

        if (!empty($company_id) && !empty($eventbrite_id)) {
            $this->event_model->updateEventbriteDetails($company_id, $eventbrite_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getEventbrite() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->event_model->getEventbriteDetails($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getTotalUsers() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->getTotalUserDetails($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getuploadedvideos() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->getvideos($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addvideos() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $title = $category_name = $description = $thum_nail_url = $video_url = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['title'])) {
            $title = $this->_request['title'];
        }
        if (isset($this->_request['category_name'])) {
            $category_name = $this->_request['category_name'];
        }
        if (isset($this->_request['description'])) {
            $description = $this->_request['description'];
        }
        if (isset($this->_request['thum_nail_url'])) {
            $thum_nail_url = $this->_request['thum_nail_url'];
        }
        if (isset($this->_request['video_url'])) {
            $video_url = $this->_request['video_url'];
        }
        // Input validations
        if (!empty($category_name) && !empty($video_url) && !empty($thum_nail_url)) {
            $this->insertVideoDetails($company_id, $title, $category_name, $description, $thum_nail_url, $video_url);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updatecompanycode() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $company_code = $user_id = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['company_code'])) {
            $company_code = $this->_request['company_code'];
        }
        if (isset($this->_request['user_id'])) {
            $user_id = $this->_request['user_id'];
        }

        if (!empty($company_id) && !empty($company_code)) {
            $this->updateCompanyCodeInDB($company_id, $company_code, $user_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function forgotPassword() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $email = '';
        if (isset($this->_request['email'])) {
            $email = $this->_request['email'];
        }

        // Input validations
        if (!empty($email)) {
//            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            $this->forgotPasswordRequest($email);
//            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function tokenvalidation() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $token = '';
        if (isset($this->_request['token'])) {
            $token = $this->_request['token'];
        }

        // Input validations
        if (!empty($token)) {
            $this->checkTokenIsValid($token);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function resetPassword() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $token = $password = $retypepassword = '';
        if (isset($this->_request['token'])) {
            $token = $this->_request['token'];
        }
        if (isset($this->_request['password'])) {
            $password = $this->_request['password'];
        }
        if (isset($this->_request['retypepassword'])) {
            $retypepassword = $this->_request['retypepassword'];
        }

        // Input validations
        if (!empty($token) && !empty($password) && !empty($retypepassword)) {
            $this->resetPasswordForUser($token, $password, $retypepassword);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteMessages() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $message_id = $company_id = '';

        if (isset($this->_request['message_id'])) {
            $message_id = $this->_request['message_id'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        // Input validations
        if (!empty($message_id) && !empty($company_id)) {
            $this->deleteMessage($company_id, $message_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function testemail() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $to = $this->_request['to'];
        if (isset($this->_request['subject'])) {
            $subject = $this->_request['subject'];
        } else {
            $subject = "subject";
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        } else {
            $message = "Message";
        }
        echo 'test';
        // Input validations
        if (!empty($to)) {
            echo $this->sendEmail($to, $subject, $message);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMenuNames() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $curriculum_name = $event_name = $message_name = $membership_name = $trial_name = $leads_name = $retail_name = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['curriculum_name'])) {
            $curriculum_name = $this->_request['curriculum_name'];
        }
        if (isset($this->_request['event_name'])) {
            $event_name = $this->_request['event_name'];
        }
        if (isset($this->_request['message_name'])) {
            $message_name = $this->_request['message_name'];
        }
        if(isset($this->_request['trial_name'])){
            $trial_name = $this->_request['trial_name'];
        }
        if(isset($this->_request['membership_name'])){
            $membership_name = $this->_request['membership_name'];
        }
        if(isset($this->_request['leads_name'])){
            $leads_name = $this->_request['leads_name'];
        }
        if(isset($this->_request['retail_name'])){
            $retail_name = $this->_request['retail_name'];
        }

        if (!empty($company_id) && !empty($curriculum_name) && !empty($event_name) && !empty($message_name) && !empty($trial_name) && !empty($membership_name) && !empty($leads_name) && !empty($retail_name)) {
            $this->updateMenuNamesInDB($company_id, $curriculum_name, $event_name, $message_name, $trial_name, $membership_name, $leads_name,$retail_name);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getMenuNames() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->getMenuNamesFromDB($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteStudent() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $student_id = $company_id = '';
        
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        // Input validations
        if (!empty($student_id) && !empty($company_id)) {
            $this->deleteStudentInDB($company_id, $student_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getReferralDetails() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $student_id = $start_date = $end_date = '';
        $company_id = $this->_request['company_id'];
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['start_date'])) {
            $start_date = $this->_request['start_date'];
        }
        if (isset($this->_request['end_date'])) {
            $end_date = $this->_request['end_date'];
        }

        // Input validations
        if (!empty($company_id)) {
            $this->getReferralDetailsFromDB($company_id, $student_id, $start_date, $end_date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateReferralMessage() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $ref_msg = $user_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['referral_message'])) {
            $ref_msg = $this->_request['referral_message'];
        }
        if (isset($this->_request['user_id'])) {
            $user_id = $this->_request['user_id'];
        }

        // Input validations
        if (!empty($company_id) && !empty($ref_msg)) {
            $this->updateReferralMessageDetails($company_id, $ref_msg, $user_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function verifyUserRegistration() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $verification_code = '';
        if (isset($_SESSION['user'])) {
            $email = $_SESSION['user'];
        }
//        $email = $this->_request['emailid'];
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['verification_code'])) {
            $verification_code = $this->_request['verification_code'];
        }

//        // Input validations
        if (!empty($email) && !empty($verification_code)) {
//            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            $this->verifyUserRegistrationDetails($email, $verification_code, $company_id, 0);   // 0 - echo result, 1 - return result
//            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function resendverificationlink() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        if (isset($_SESSION['user'])) {
            $email = $_SESSION['user'];
        }
//        $email = $this->_request['emailid'];
//        // Input validations
        if (!empty($email)) {
            $this->VerificationLinkresend($email);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function checkverification() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (!empty($company_id))
            $this->getVerifiedStatus($company_id);
        else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getDefaultLogo() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $response = $this->getDefaultLogoImage();
        $res = array('status' => "Success", "msg" => "$response");
        $this->response($this->json($res), 200);
    }

    private function getToken() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $this->getcheckouttoken();
    }

    private function getCreditCardAccounts() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id='';
        
        if (isset($this->_request['company_id'])) {
        $company_id = $this->_request['company_id'];
        }
        if (!empty($company_id)) {
            $this->getCreditCardAccountDetails($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function payment() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $firstname = $lastname = $email = $phone = $address = $city = $state = $country = $zip = '';
        $company_id = $user_id = $customer_id = $account_id = $payment_type = $payment_amount = $payee_type = '';
        $setup_fee = 0;
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['user_id'])) {
            $user_id = $this->_request['user_id'];
        }
        if (isset($this->_request['customer_id'])) {
            $customer_id = $this->_request['customer_id'];
        }
        if (isset($this->_request['account_id'])) {
            $account_id = $this->_request['account_id'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['payee_type'])) {
            $payee_type = $this->_request['payee_type'];
        }

        if (isset($this->_request['FirstName'])) {
            $firstname = $this->_request['FirstName'];
        }
        if (isset($this->_request['LastName'])) {
            $lastname = $this->_request['LastName'];
        }
        if (isset($this->_request['Email'])) {
            $email = $this->_request['Email'];
        }
        if (isset($this->_request['Phone'])) {
            $phone = $this->_request['Phone'];
        }
        if (isset($this->_request['StreetAddress'])) {
            $address = $this->_request['StreetAddress'];
        }
        if (isset($this->_request['City'])) {
            $city = $this->_request['City'];
        }
        if (isset($this->_request['StateCode'])) {
            $state = $this->_request['StateCode'];
        }
        if (isset($this->_request['Country'])) {
            $country = $this->_request['Country'];
        }
        if (isset($this->_request['BillingZipcode'])) {
            $zip = $this->_request['BillingZipcode'];
        }
        if (isset($this->_request['setup_fee'])) {
            $setup_fee = $this->_request['setup_fee'];
        }


        if (!empty($company_id) && !empty($user_id) && !empty($customer_id) && !empty($account_id) && !empty($payment_type) && !empty($payment_amount) && $payment_amount > 0) {
            $this->paymentPS($company_id, $user_id, $customer_id, $account_id, $payment_type, $payment_amount, $setup_fee, $payee_type, $firstname, $lastname, $email, $phone, $address, $city, $state, $country, $zip);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getCurrentPayment() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id='';
        
        if (isset($this->_request['company_id'])) {
        $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->currentPaymentDetails($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getRecurringBySchedule() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $schedule_id = '';
        if (isset($this->_request['schedule_id'])) {
            $schedule_id = $this->_request['schedule_id'];
        }
        if (!empty($schedule_id)) {
            $this->getRecurringDetails($schedule_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updatePassword() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id=$emailid=$old_password=$new_password='';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($_SESSION['user'])) {
            $emailid = $_SESSION['user'];
        }
//        $emailid = $this->_request['emailid'];
        if (isset($this->_request['old_password'])) {
            $old_password = $this->_request['old_password'];
        }
        if (isset($this->_request['new_password'])) {
            $new_password = $this->_request['new_password'];
        }

        if ($old_password == $new_password) {
            $error = array('status' => "Failed", "msg" => "New password should not be same as previous password.");
            $this->response($this->json($error), 200);
        }

        if (!empty($company_id) && !empty($emailid) && !empty($old_password) && !empty($new_password)) {
            $this->updateUserPassword($company_id, $emailid, $old_password, $new_password);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updatepayment() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $firstname = $lastname = $email = $phone = $address = $city = $state = $country = $zip = '';
        $company_id = $user_id = $customer_id = $account_id = $payment_type = $payment_amount = $payee_type = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['user_id'])) {
            $user_id = $this->_request['user_id'];
        }
        if (isset($this->_request['customer_id'])) {
            $customer_id = $this->_request['customer_id'];
        }
        if (isset($this->_request['account_id'])) {
            $account_id = $this->_request['account_id'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['payee_type'])) {
            $payee_type = $this->_request['payee_type'];
        }

        if (isset($this->_request['FirstName'])) {
            $firstname = $this->_request['FirstName'];
        }
        if (isset($this->_request['LastName'])) {
            $lastname = $this->_request['LastName'];
        }
        if (isset($this->_request['Email'])) {
            $email = $this->_request['Email'];
        }
        if (isset($this->_request['Phone'])) {
            $phone = $this->_request['Phone'];
        }
        if (isset($this->_request['StreetAddress'])) {
            $address = $this->_request['StreetAddress'];
        }
        if (isset($this->_request['City'])) {
            $city = $this->_request['City'];
        }
        if (isset($this->_request['StateCode'])) {
            $state = $this->_request['StateCode'];
        }
        if (isset($this->_request['Country'])) {
            $country = $this->_request['Country'];
        }
        if (isset($this->_request['BillingZipcode'])) {
            $zip = $this->_request['BillingZipcode'];
        }


        if (!empty($company_id) && !empty($user_id) && !empty($customer_id) && !empty($account_id) && !empty($payment_type) && !empty($payment_amount) && $payment_amount > 0) {
            $this->paymentUpdatePS($company_id, $user_id, $customer_id, $account_id, $payment_type, $payment_amount, $payee_type, $firstname, $lastname, $email, $phone, $address, $city, $state, $country, $zip);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateReferralEmailList() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $email_list = '';

        if (isset($this->_request['company_id'])){
        $company_id = $this->_request['company_id'];        
        }
        if (isset($this->_request['email_list'])){
            $email_list = $this->_request['email_list'];
        }

        if (!empty($company_id)) {
            $this->updateReferralEmails($company_id, $email_list);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getCompanyInfo() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $user_id='';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['user_id'])) {
            $user_id = $this->_request['user_id'];
        }
        if (!empty($company_id)) {
            $this->getcompanydetails($company_id, $user_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteUserAccount() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $email ='';
        if (isset($this->_request['email'])) {
            $email = $this->_request['email'];
        }

        if (!empty($email)) {
            $this->deleteAccountDetailsForUser($email);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function geteventparticipants() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $data_limit = 0;
        $company_id = $event_id = $event_status_tab = '';
        $search = $draw_table = $length_table = $start = $sorting = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }

        if (isset($this->_request['event_status_tab'])) {
            $event_status_tab = $this->_request['event_status_tab'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = $this->_request['start'];
        }

        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }



        if ($event_status_tab != 'O' && $event_status_tab != 'C') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        if (!empty($company_id) && !empty($event_id) && !empty($event_status_tab)) {
            $this->event_model->getEventParticipantDetails($company_id, $event_id, $event_status_tab, $search, $draw_table, $length_table, $start, $sorting);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function sendEventPush() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $message = $type = $push_for = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['push_for'])) {
            $push_for = $this->_request['push_for'];
        }

        if (!empty($company_id) && !empty($event_id) && !empty($message) && !empty($type) && !empty($push_for)) {
            if ($type == 'push') {
                $this->event_model->sendPushMessageForEvent($company_id, $event_id, $message, $push_for);
            } elseif ($type == 'mail') {
                $this->event_model->sendPushMailForEvent($company_id, $event_id, $message, $push_for);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getwepaystatus() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->event_model->wepayStatus($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getstripestatus() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->stripeStatus($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function wepayGenerateToken() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $company_name = $company_desc = $country = $code = $redirect_uri = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['company_name'])) {
            $company_name = $this->_request['company_name'];
        }
        if (isset($this->_request['company_desc'])) {
            $company_desc = $this->_request['company_desc'];
        }
        if (isset($this->_request['code'])) {
            $code = $this->_request['code'];
        }
        if (isset($this->_request['redirect_uri'])) {
            $redirect_uri = $this->_request['redirect_uri'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }

        if (!empty($company_id) && !empty($company_name) && !empty($company_desc) && !empty($code) && !empty($redirect_uri)) {
            $this->event_model->wepayTokenGeneration($company_id, $company_name, $company_desc, $code, $redirect_uri, $country);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function wepayGenerateUserToken() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $first_name = $last_name = $email = $type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['first_name'])) {
            $first_name = $this->_request['first_name'];
        }
        if (isset($this->_request['last_name'])) {
            $last_name = $this->_request['last_name'];
        }
        if (isset($this->_request['email'])) {
            $email = $this->_request['email'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }

        if (!empty($company_id) && !empty($first_name) && !empty($last_name) && !empty($email) && !empty($type)) {
            $this->event_model->wepayUserTokenGeneration($company_id, $first_name, $last_name, $email, $type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function stripeGenerateUserToken() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $studio_code = $email = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['studio_code'])) {
            $studio_code = $this->_request['studio_code'];
        }
        if (isset($this->_request['email'])) {
            $email = $this->_request['email'];
        }

        if (!empty($company_id) && !empty($email)) {
            $this->createstripeconnectedaccount($company_id, $studio_code, $email, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

//    private function addParticipantDetails(){
//        if($this->get_request_method() != "POST"){
//            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
//            $this->response($this->json($error),406);
//        }
//        
//        $company_id = $event_id = $parent_id = $event_name = $desc = $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = $processing_fee_type = $event_type = '';
//        $student_id = $fee = $dicount = $registration_amount = $payment_amount = 0;
//        $quantity = 1;
//        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
//                
//        if(isset($this->_request['company_id'])){
//            $company_id = $this->_request['company_id'];
//        }
//        if(isset($this->_request['event_id'])){
//            $event_id = $this->_request['event_id'];
//        }
//        if(isset($this->_request['parent_id'])){
//            $parent_id = $this->_request['parent_id'];
//        }
//        if(isset($this->_request['student_id'])){
//            $student_id = $this->_request['student_id'];
//        }
//        if(isset($this->_request['event_name'])){
//            $event_name = $this->_request['event_name'];
//        }
//        if(isset($this->_request['event_type'])){
//            $event_type = $this->_request['event_type'];
//        }
//        if(isset($this->_request['desc'])){
//            $desc = $this->_request['desc'];
//        }
//        if(isset($this->_request['buyer_name'])){
//            $buyer_name = $this->_request['buyer_name'];
//        }
//        if(isset($this->_request['email'])){
//            $buyer_email = $this->_request['email'];
//        }
//        if(isset($this->_request['phone'])){
//            $buyer_phone = $this->_request['phone'];
//        }
//        if(isset($this->_request['postal_code'])){
//            $buyer_postal_code = $this->_request['postal_code'];
//        }
//        if(isset($this->_request['registration_amount'])){
//            $registration_amount = $this->_request['registration_amount'];
//        }
//        if(isset($this->_request['payment_amount'])){
//            $payment_amount = $this->_request['payment_amount'];
//        }
//        if(isset($this->_request['processing_fee_type'])){
//            $processing_fee_type = $this->_request['processing_fee_type'];
//        }
//        if(isset($this->_request['fee'])){
//            $fee = $this->_request['fee'];
//        }
//        if(isset($this->_request['reg_col1'])){
//            $reg_col1 = $this->_request['reg_col1'];
//        }
//        if(isset($this->_request['reg_col2'])){
//            $reg_col2 = $this->_request['reg_col2'];
//        }
//        if(isset($this->_request['reg_col3'])){
//            $reg_col3 = $this->_request['reg_col3'];
//        }
//        if(isset($this->_request['reg_col4'])){
//            $reg_col4 = $this->_request['reg_col4'];
//        }
//        if(isset($this->_request['reg_col5'])){
//            $reg_col5 = $this->_request['reg_col5'];
//        }
//        if(isset($this->_request['reg_col6'])){
//            $reg_col6 = $this->_request['reg_col6'];
//        }
//        if(isset($this->_request['reg_col7'])){
//            $reg_col7 = $this->_request['reg_col7'];
//        }
//        if(isset($this->_request['reg_col8'])){
//            $reg_col8 = $this->_request['reg_col8'];
//        }
//        if(isset($this->_request['reg_col9'])){
//            $reg_col9 = $this->_request['reg_col9'];
//        }
//        if(isset($this->_request['reg_col10'])){
//            $reg_col10 = $this->_request['reg_col10'];
//        }
//        if(isset($this->_request['discount'])){
//            $discount = $this->_request['discount'];
//        }
//        if(isset($this->_request['quantity'])){
//            $quantity = $this->_request['quantity'];
//        }
//        
//        
//        if(!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($buyer_name) && !empty($buyer_email)){
//            $this->addParticipantDetailsForFreeEvent($company_id, $event_id, $parent_id, $student_id, $event_name, $desc, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_type);
////            $this->wepayCreateCheckout($company_id, $event_id, $student_id, $sub_events_id, $event_name, $desc, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity);
//        }else{
//            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
//            $this->response($this->json($error), 200);
//        }
//    }
//    private function wepaycheckout(){
//        if($this->get_request_method() != "POST"){
//            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
//            $this->response($this->json($error),406);
//        }
//        
//        $company_id = $student_name = $event_id = $parent_id = $event_name = $desc = $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = '';
//        $student_id = $fee = $dicount = $registration_amount = $payment_amount = 0;
//        $quantity = 1;
//        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
//        $country = 'US';
//                
//        if(isset($this->_request['company_id'])){
//            $company_id = $this->_request['company_id'];
//        }
//        if(isset($this->_request['event_id'])){
//            $event_id = $this->_request['event_id'];
//        }
//        if(isset($this->_request['parent_id'])){
//            $parent_id = $this->_request['parent_id'];
//        }
//        if(isset($this->_request['student_id'])){
//            $student_id = $this->_request['student_id'];
//        }
//        if(isset($this->_request['student_name'])){
//            $student_name = $this->_request['student_name'];
//        }
//        if(isset($this->_request['event_name'])){
//            $event_name = $this->_request['event_name'];
//        }
//        if(isset($this->_request['desc'])){
//            $desc = $this->_request['desc'];
//        }
//        if(isset($this->_request['buyer_name'])){
//            $buyer_name = $this->_request['buyer_name'];
//        }
//        if(isset($this->_request['email'])){
//            $buyer_email = $this->_request['email'];
//        }
//        if(isset($this->_request['phone'])){
//            $buyer_phone = $this->_request['phone'];
//        }
//        if(isset($this->_request['postal_code'])){
//            $buyer_postal_code = $this->_request['postal_code'];
//        }
//        if(isset($this->_request['country'])){
//            $country = $this->_request['country'];
//        }
//        if(isset($this->_request['cc_id'])){
//            $cc_id = $this->_request['cc_id'];
//        }
//        if(isset($this->_request['cc_state'])){
//            $cc_state = $this->_request['cc_state'];
//        }
//        if(isset($this->_request['registration_amount'])){
//            $registration_amount = $this->_request['registration_amount'];
//        }
//        if(isset($this->_request['payment_amount'])){
//            $payment_amount = $this->_request['payment_amount'];
//        }
//        if(isset($this->_request['processing_fee_type'])){
//            $processing_fee_type = $this->_request['processing_fee_type'];
//        }
//        if(isset($this->_request['fee'])){
//            $fee = $this->_request['fee'];
//        }
//        if(isset($this->_request['reg_col1'])){
//            $reg_col1 = $this->_request['reg_col1'];
//        }
//        if(isset($this->_request['reg_col2'])){
//            $reg_col2 = $this->_request['reg_col2'];
//        }
//        if(isset($this->_request['reg_col3'])){
//            $reg_col3 = $this->_request['reg_col3'];
//        }
//        if(isset($this->_request['reg_col4'])){
//            $reg_col4 = $this->_request['reg_col4'];
//        }
//        if(isset($this->_request['reg_col5'])){
//            $reg_col5 = $this->_request['reg_col5'];
//        }
//        if(isset($this->_request['reg_col6'])){
//            $reg_col6 = $this->_request['reg_col6'];
//        }
//        if(isset($this->_request['reg_col7'])){
//            $reg_col7 = $this->_request['reg_col7'];
//        }
//        if(isset($this->_request['reg_col8'])){
//            $reg_col8 = $this->_request['reg_col8'];
//        }
//        if(isset($this->_request['reg_col9'])){
//            $reg_col9 = $this->_request['reg_col9'];
//        }
//        if(isset($this->_request['reg_col10'])){
//            $reg_col10 = $this->_request['reg_col10'];
//        }
//        if(isset($this->_request['discount'])){
//            $discount = $this->_request['discount'];
//        }
//        if(isset($this->_request['quantity'])){
//            $quantity = $this->_request['quantity'];
//        }
//        
//        
//        if(!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($cc_id) && !empty($buyer_name) && !empty($buyer_email)){
//            $this->event_model->wepayCreateCheckout($company_id, $event_id, $parent_id, $student_id, $student_name, $event_name, $desc, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity);
//        }else{
//            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
//            $this->response($this->json($error), 200);
//        }
//    }

    private function getParticipantDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_reg_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['event_reg_id'])) {
            $event_reg_id = $this->_request['event_reg_id'];
        }

        if (!empty($company_id) && !empty($event_id) && !empty($event_reg_id)) {
            $this->event_model->getEventParticipantDetailsForRefund($company_id, $event_id, $event_reg_id, 0);  //0 - call back -> echo & exit
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function processPaymentPlanRefund() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_reg_id = $cancel_flag = '';
        $event_array = $plan_array = [];
        $total_due = 0;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_reg_id'])) {
            $event_reg_id = $this->_request['event_reg_id'];
        }
        if (isset($this->_request['event_array'])) {
            $event_array = $this->_request['event_array'];
        }
        if (isset($this->_request['plan_array'])) {
            $plan_array = $this->_request['plan_array'];
        }
        if (isset($this->_request['total_due'])) {
            $total_due = $this->_request['total_due'];
        }
        // process --> C - Process cancellation, WR - Cancellation with Refund, WOR - Cancellation without Refund, CP - Cancellation & Keep Current Plan, NP - Cancellation & New Payment Plan
        if (isset($this->_request['cancel_flag'])) {
            $cancel_flag = $this->_request['cancel_flag'];
        }

        if (!empty($company_id) && !empty($event_reg_id) && !empty($event_array) && !empty($cancel_flag) && ($cancel_flag=='C' || !empty($plan_array))) {
            $this->event_model->processPaymentRefund($company_id, $event_reg_id, $event_array, $plan_array, $total_due, $cancel_flag);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateEventSortTest() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $this->event_model->eventSortTest();
    }

//    private function updateParticipantName(){
//        // Cross validation if the request method is GET else it will return "Not Acceptable" status
//        if($this->get_request_method() != "POST"){
//            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
//            $this->response($this->json($error),406);
//        }
//        
//        $company_id = $event_id = $event_reg_id = $pfirst_name = $plast_name = '';
//        
//        if(isset($this->_request['company_id'])){
//            $company_id = $this->_request['company_id'];
//        }
//        if(isset($this->_request['event_id'])){
//            $event_id = $this->_request['event_id'];
//        }
//        if(isset($this->_request['event_reg_id'])){
//            $event_reg_id = $this->_request['event_reg_id'];
//        }
//        if(isset($this->_request['pfirst_name'])){
//            $pfirst_name = $this->_request['pfirst_name'];
//        }
//        if(isset($this->_request['plast_name'])){
//            $plast_name = $this->_request['plast_name'];
//        }
//        
//        if(!empty($company_id)&&!empty($event_reg_id)&&!empty($pfirst_name)&&!empty($plast_name)){
//            $this->event_model->updateParticipantDetails($company_id,$event_id,$event_reg_id,$pfirst_name,$plast_name);
//        }else{
//           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
//           $this->response($this->json($error), 200); 
//        }
//        
//    }

    private function sendEventOrderReceipt() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_reg_id = '';
        $return_value = 1;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_reg_id'])) {
            $event_reg_id = $this->_request['event_reg_id'];
        }

        if (!empty($company_id) && !empty($event_reg_id)) {
            $this->event_model->sendOrderReceiptForEventPayment($event_reg_id, $return_value);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function exportData() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $export_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['export_type'])) {
            $export_type = $this->_request['export_type'];
        }

        if (!empty($company_id) && !empty($event_id) && !empty($export_type)) {
            if ($export_type == "P") {
                $this->event_model->createPDF($company_id, $event_id);
            } else {
                $this->event_model->createCSV($company_id, $event_id);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function eventPaymentMarkedAsPaid() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $event_reg_id = $event_payment_id = '';
        $credit_amount = 0;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_reg_id'])) {
            $event_reg_id = $this->_request['event_reg_id'];
        }
        if (isset($this->_request['event_payment_id'])) {
            $event_payment_id = $this->_request['event_payment_id'];
        }
        if (isset($this->_request['credit_amount'])) {
            $credit_amount = $this->_request['credit_amount'];
        }
        if (isset($this->_request['payment_Count'])) {
            $payment_Count = $this->_request['payment_Count'];
        }
        if (isset($this->_request['credit_method'])) {
            $credit_method = $this->_request['credit_method'];
        }
        if (isset($this->_request['check_number'])) {
            $check_number = $this->_request['check_number'];
        }
        


        // Input validations
        if (!empty($company_id) && !empty($event_reg_id) && !empty($event_payment_id)) {
            $this->event_model->updateEventPaymentDetailsAsPaid($company_id, $event_reg_id, $event_payment_id, $credit_amount, $payment_Count, $credit_method, $check_number);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

//    private function updateEventPaymentMethod(){
//         // Cross validation if the request method is POST else it will return "Not Acceptable" status
//        if($this->get_request_method() != "POST"){
//            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
//            $this->response($this->json($error),406);
//        }
//        $company_id=$event_reg_id=$event_payment_id='';
//        $new_cc_id=$cc_state=$buyer_name=$buyer_email=$postal_code='';
//        
//        if(isset($this->_request['company_id'])){
//            $company_id = $this->_request['company_id'];
//        }
//        if(isset($this->_request['event_reg_id'])){
//            $event_reg_id = $this->_request['event_reg_id'];
//        }
//        if(isset($this->_request['buyer_name'])){
//            $buyer_name = $this->_request['buyer_name'];
//        }
//        if(isset($this->_request['email'])){
//            $buyer_email = $this->_request['email'];
//        }
//        if(isset($this->_request['postal_code'])){
//            $postal_code = $this->_request['postal_code'];
//        }
//        if(isset($this->_request['cc_id'])){
//            $new_cc_id = $this->_request['cc_id'];
//        }
//        if(isset($this->_request['cc_state'])){
//            $cc_state = $this->_request['cc_state'];
//        }
//        
//           // Input validations
//        if (!empty($company_id) && !empty($event_reg_id) && !empty($new_cc_id) && !empty($buyer_name) && !empty($buyer_email)) {
//            $this->event_model->updateEventPaymentMethodDetails($event_reg_id,$new_cc_id,$cc_state,$buyer_name,$buyer_email,$postal_code);
//        } else {
//            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
//            $this->response($this->json($error), 200);
//        }
//    }

    private function wepayPortalCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_name = $sub_events_id = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = $payment_frequency = '';
        $student_id = $participant_id = $fee = $discount = $registration_amount = $payment_amount = $total_order_quantity = $total_order_amount = 0;
        $quantity = 1;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $event_array = $payment_array = [];
        $upgrade_status = 'F';
        $country = 'US';
        $reg_type_user = 'P';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = $version;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['event_name'])) {
            $event_name = $this->_request['event_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if (isset($this->_request['event_array'])) {
            $event_array = $this->_request['event_array'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['cc_id'])) {
            $cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_state'])) {
            $cc_state = $this->_request['cc_state'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['fee'])) {
            $fee = $this->_request['fee'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
        if (isset($this->_request['quantity'])) {
            $quantity = $this->_request['quantity'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['reg_col4'])) {
            $reg_col4 = $this->_request['reg_col4'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['payment_frequency'])) {
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if (isset($this->_request['event_array'])) {
            $event_array = $this->_request['event_array'];
        }
        if (isset($this->_request['payment_array'])) {
            $payment_array = $this->_request['payment_array'];
        }
        if (isset($this->_request['event_type'])) {
            $event_type = $this->_request['event_type'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['total_order_amount'])) {
            $total_order_amount = $this->_request['total_order_amount'];
        }
        if (isset($this->_request['total_order_quantity'])) {
            $total_order_quantity = $this->_request['total_order_quantity'];
        }
        if ($event_type == 'S' && $payment_type == 'onetimesingle') {
            $event_array = [];
            $payment_array = [];
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($cc_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
                $this->event_model->wepayCreatePortalCheckout($company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, 4, 'CO', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } elseif ($event_type == 'S' && $payment_type == 'singlerecurring') {
            $event_array = [];
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($cc_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($payment_array)) {
                $this->event_model->wepayCreatePortalCheckout($company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, 'RE', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } elseif ($event_type == 'M' && $payment_type == 'onetimemultiple') {
            $payment_array = [];
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($cc_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($event_array)) {
                $this->event_model->wepayCreatePortalCheckout($company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, 4, 'CO', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } elseif ($event_type == 'M' && $payment_type == 'multiplerecurring') {
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($cc_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($payment_array) && !empty($event_array)) {
                $this->event_model->wepayCreatePortalCheckout($company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, 'RE', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addParticipantDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_name = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $processing_fee_type = $event_type = '';
        $student_id = $participant_id = $fee = $dicount = $registration_amount = $payment_amount = 0;
        $quantity = 1;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $event_array = [];
        $reg_type_user = 'P';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = $version;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['event_name'])) {
            $event_name = $this->_request['event_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['fee'])) {
            $fee = $this->_request['fee'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['reg_col4'])) {
            $reg_col4 = $this->_request['reg_col4'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
        if (isset($this->_request['quantity'])) {
            $quantity = $this->_request['quantity'];
        }
        if (isset($this->_request['event_type'])) {
            $event_type = $this->_request['event_type'];
        }
        if (isset($this->_request['event_array'])) {
            $event_array = $this->_request['event_array'];
        }


        if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($event_type)) {
            if ($event_type != 'M' && $event_type != 'S') {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            } else {
                if ($event_type == 'M' && empty($event_array)) {
                    $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 200);
                } else {
                    $this->event_model->addParticipantDetailsForFreeEvent($company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_type, $event_array, $reg_type_user, $reg_version_user);
                }
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
//    Membership open tabs Initially
    private function openMembershipTabs() {

        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (!empty($company_id)) {
            $this->membership_model->getopenMembershipTabs($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    //Membership related functions
    private function getAllMembershipCategory() {

        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';
        $list_type = 'P';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['list_type']) && !empty($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        if (!empty($company_id) && !empty($list_type) && ($list_type=='P' || $list_type=='S')) {
            $this->membership_model->getAllMembershipDetails($company_id, $list_type, 0, '');
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getMembershipCategory() {

        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }

        if (!empty($company_id) && !empty($membership_id)) {
            $this->membership_model->getMembershipDetails($company_id, $membership_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addMembershipCategory() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $category_status = $category_title = $category_subtitle = $category_banner_img_type = $category_banner_img_content = $category_video_url = $category_description = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['category_status'])) {
            $category_status = $this->_request['category_status'];
        }
        if (isset($this->_request['category_title'])) {
            $category_title = $this->_request['category_title'];
        }
        if (isset($this->_request['category_subtitle'])) {
            $category_subtitle = $this->_request['category_subtitle'];
        }
        if (isset($this->_request['category_banner_img_type'])) {
            $category_banner_img_type = $this->_request['category_banner_img_type'];
        }
        if (isset($this->_request['category_banner_img_content'])) {
            $category_banner_img_content = $this->_request['category_banner_img_content'];
        }
        if (isset($this->_request['category_video_url'])) {
            $category_video_url = $this->_request['category_video_url'];
        }
        if (isset($this->_request['category_description'])) {
            $category_description = $this->_request['category_description'];
        }

        // Input validations
        if (!empty($company_id) && !empty($category_title) && !empty($category_status)) {
            $this->membership_model->addMembershipDetails($company_id, $category_status, $category_title, $category_subtitle, $category_banner_img_type, $category_banner_img_content, $category_video_url, $category_description);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipCategory() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $membership_id = $company_id = $category_status = $category_title = $category_subtitle = $category_banner_img_type = $category_banner_img_content = $category_video_url = $category_description = $category_image_update = $old_category_image_url = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['category_status'])) {
            $category_status = $this->_request['category_status'];
        }
        if (isset($this->_request['category_title'])) {
            $category_title = $this->_request['category_title'];
        }
        if (isset($this->_request['category_subtitle'])) {
            $category_subtitle = $this->_request['category_subtitle'];
        }
        if (isset($this->_request['category_banner_img_type'])) {
            $category_banner_img_type = $this->_request['category_banner_img_type'];
        }
        if (isset($this->_request['category_banner_img_content'])) {
            $category_banner_img_content = $this->_request['category_banner_img_content'];
        }
        if (isset($this->_request['category_video_url'])) {
            $category_video_url = $this->_request['category_video_url'];
        }
        if (isset($this->_request['category_description'])) {
            $category_description = $this->_request['category_description'];
        }
        if (isset($this->_request['category_image_update'])) {
            $category_image_update = $this->_request['category_image_update'];
        }
        if (isset($this->_request['old_category_image_url'])) {
            $old_category_image_url = $this->_request['old_category_image_url'];
        }

        $category_banner_img_url = $old_category_image_url;
        if ($category_image_update === 'Y' && $category_banner_img_content != '') {
            $cfolder_name = "Company_$company_id";
            $old = getcwd(); // Save the current directory
            $dir_name = "../../../$this->upload_folder_name";
            chdir($dir_name);
            if (!file_exists($cfolder_name)) {
                $oldmask = umask(0);
                mkdir($cfolder_name, 0777, true);
                umask($oldmask);
            }
            chdir($cfolder_name);
            $folder_name = "Membership";
            if (!file_exists($folder_name)) {
                $oldmask = umask(0);
                mkdir($folder_name, 0777, true);
                umask($oldmask);
            }
            chdir($folder_name);
            $image_content = base64_decode($category_banner_img_content);
            $file_name = "$membership_id" . "_" . time();
            $file = "$file_name.$category_banner_img_type";
            $img = $file; // logo - your file name
            $ifp = fopen($img, "wb");
            fwrite($ifp, $image_content);
            fclose($ifp);
            chdir($old); // Restore the old working directory

            $category_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/" . $this->membership_model->local_upload_folder_name . "/$cfolder_name/$folder_name/$file";
        } elseif ($category_image_update === 'Y' && $category_banner_img_content == '') {
            $category_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/" . $this->membership_model->local_upload_folder_name . "/Default/default.png";
        }

        // Input validations

        if (!empty($company_id) && !empty($category_title) && !empty($category_status)) {
            $this->membership_model->updateMembershipDetails($company_id, $membership_id, $category_status, $category_title, $category_subtitle, $category_banner_img_url, $category_video_url, $category_description, $category_image_update, $old_category_image_url);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function copyMembershipCategory() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_id = $membership_template_flag = '';
        $list_type = 'P';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_template_flag'])) {
            $membership_template_flag = $this->_request['membership_template_flag'];
        }
        if (isset($this->_request['list_type']) && !empty($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($membership_template_flag) && !empty($list_type) && ($list_type=='P' || $list_type=='S')) {
            $this->membership_model->copyMembershipDetails($company_id, $membership_id, $membership_template_flag, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteMembershipCategory() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_id = '';
        $list_type = 'P';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['list_type']) && !empty($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($list_type) && ($list_type=='P' || $list_type=='S')) {
            $this->membership_model->deleteMembershipDetails($company_id, $membership_id, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipCategorySortOrder() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_id = $sort_id = '';
        $list_type = 'P';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['sort_id'])) {
            $sort_id = $this->_request['sort_id'];
        }
        if (isset($this->_request['list_type']) && !empty($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($sort_id)) {
            $this->membership_model->updateMembershipCategorySortingOrder($company_id, $membership_id, $sort_id, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addMembershipRank() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $rank_name = $required_attendance_count = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['rank_name'])) {
            $rank_name = $this->_request['rank_name'];
        }
        if (isset($this->_request['required_attendance'])) {
            $required_attendance_count = $this->_request['required_attendance'];
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($rank_name)) {
            $this->membership_model->addMembershipRankDetails($company_id, $membership_id, $rank_name, $required_attendance_count);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipRank() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_id = $membership_rank_id = $rank_name = $required_attendance_count = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_rank_id'])) {
            $membership_rank_id = $this->_request['membership_rank_id'];
        }
        if (isset($this->_request['rank_name'])) {
            $rank_name = $this->_request['rank_name'];
        }
        if (isset($this->_request['required_attendance'])) {
            $required_attendance_count = $this->_request['required_attendance'];
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($membership_rank_id) && !empty($rank_name)) {
            $this->membership_model->updateMembershipRankDetails($company_id, $membership_id, $membership_rank_id, $rank_name, $required_attendance_count);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteMembershipRank() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_rank_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_rank_id'])) {
            $membership_rank_id = $this->_request['membership_rank_id'];
        }


        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($membership_rank_id)) {
            $this->membership_model->deleteMembershipRank($company_id, $membership_id, $membership_rank_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipRankSortOrder() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_id = $membership_rank_id = $sort_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_rank_id'])) {
            $membership_rank_id = $this->_request['membership_rank_id'];
        }
        if (isset($this->_request['sort_id'])) {
            $sort_id = $this->_request['sort_id'];
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($sort_id)) {
            $this->membership_model->updateMembershipRankSortingOrder($company_id, $membership_id, $membership_rank_id, $sort_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addMembershipOptions() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_title = $membership_subtitle = $membership_description = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_title'])) {
            $membership_title = $this->_request['membership_title'];
        }
        if (isset($this->_request['membership_subtitle'])) {
            $membership_subtitle = $this->_request['membership_subtitle'];
        }
        if (isset($this->_request['membership_description'])) {
            $membership_description = $this->_request['membership_description'];
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($membership_title)) {
            $this->membership_model->addMembershipOptionDetails($company_id, $membership_id, $membership_title, $membership_subtitle, $membership_description);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipOptions() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_option_id = $membership_title = $membership_subtitle = $membership_description = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['membership_title'])) {
            $membership_title = $this->_request['membership_title'];
        }
        if (isset($this->_request['membership_subtitle'])) {
            $membership_subtitle = $this->_request['membership_subtitle'];
        }
        if (isset($this->_request['membership_description'])) {
            $membership_description = $this->_request['membership_description'];
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($membership_title)) {
            $this->membership_model->updateMembershipOptionDetails($company_id, $membership_id, $membership_option_id, $membership_title, $membership_subtitle, $membership_description);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function copyMembershipOptions() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_id = $membership_option_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($membership_option_id)) {

            $this->membership_model->copyMembershipOptionDetails($company_id, $membership_id, $membership_option_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteMembershipOptions() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_id = $membership_option_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id)) {
            $this->membership_model->deleteMembershipOptionDetails($company_id, $membership_id, $membership_option_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipOptionsSortOrder() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_id = $sort_id = $membership_option_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['sort_id'])) {
            $sort_id = $this->_request['sort_id'];
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($membership_option_id) && !empty($sort_id)) {
            $this->membership_model->updateMembershipOptionsSortingOrder($company_id, $membership_id, $membership_option_id, $sort_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipPaymentDetails() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_option_id = '';
        $recurring_membership_payment_frequency = $custom_recurring_frequency_period_type = $custom_recurring_frequency_period_val = $delayed_recurring_payment_type = $delayed_recurring_payment_val = '';
        $prorate_first_payment_period_flg = $delay_recurring_payment_flg = 'N';
        $processing_fee = 2;
        $signup_fee = $membership_fee = $no_of_classes = $expiration_date_val = $deposit_amount = $no_of_payments = 0;
        $expiration_date_type = $billing_options = $billing_payment_start_date_type = $billing_options_payment_start_date = '';
        $membership_payment_array = ['N', 'W', 'B', 'M', 'A', 'C'];          //  membership frequency
        $membership_structure = 'OE';
        $initial_payment_include_membership_fee = $expiration_status = 'Y';
        $specific_start_date = $specific_end_date = '';
        $specific_payment_frequency = 'PW';
        $exclude_from_billing_flag = 'N';
        $week_days_order = "mon,tue,wed,thu,fri,sat,sun";
        $billing_days = "1111100";
        $attendance_limit_flag = 'N';
        $attendance_period_type = 'CPW';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['membership_structure'])) {
            $membership_structure = $this->_request['membership_structure'];
            if ($membership_structure == 1) {
                $membership_structure = 'OE';
            }
        }
        if (isset($this->_request['processing_fee'])) {
            $processing_fee = $this->_request['processing_fee'];
        }
        if (isset($this->_request['signup_fee'])) {
            $signup_fee = $this->_request['signup_fee'];
        }
        if (isset($this->_request['membership_fee'])) {
            $membership_fee = $this->_request['membership_fee'];
        }
        if (isset($this->_request['recurring_membership_payment_frequency'])) {
            $recurring_membership_payment_frequency = $this->_request['recurring_membership_payment_frequency'];
        }
        if (isset($this->_request['prorate_first_payment_period_flg']) && !empty($this->_request['prorate_first_payment_period_flg'])) {
            $prorate_first_payment_period_flg = $this->_request['prorate_first_payment_period_flg'];
        }
        if (isset($this->_request['custom_recurring_frequency_period_val'])) {
            $custom_recurring_frequency_period_val = $this->_request['custom_recurring_frequency_period_val'];
        }
        if (isset($this->_request['custom_recurring_frequency_period_type'])) {
            $custom_recurring_frequency_period_type = $this->_request['custom_recurring_frequency_period_type'];
        }
        if (isset($this->_request['delay_recurring_payment_flg'])) {
            $delay_recurring_payment_flg = $this->_request['delay_recurring_payment_flg'];
        }
        if (isset($this->_request['delayed_recurring_payment_type'])) {
            $delayed_recurring_payment_type = $this->_request['delayed_recurring_payment_type'];
        }
        if (isset($this->_request['delayed_recurring_payment_val'])) {
            $delayed_recurring_payment_val = $this->_request['delayed_recurring_payment_val'];
        }
        if (isset($this->_request['initial_payment_include_membership_fee'])) {
            $initial_payment_include_membership_fee = $this->_request['initial_payment_include_membership_fee'];
        }
        if (isset($this->_request['billing_options'])) {
            $billing_options = $this->_request['billing_options'];
        }
        if (isset($this->_request['no_of_classes'])) {
            $no_of_classes = $this->_request['no_of_classes'];
        }
        if (isset($this->_request['expiration_date_type'])) {
            $expiration_date_type = $this->_request['expiration_date_type'];
        }
        if (isset($this->_request['expiration_date_val'])) {
            $expiration_date_val = $this->_request['expiration_date_val'];
        }
        if (isset($this->_request['expiration_status'])) {
            $expiration_status = $this->_request['expiration_status'];
        }
        if (isset($this->_request['deposit_amount'])) {
            $deposit_amount = $this->_request['deposit_amount'];
        }
        if (isset($this->_request['no_of_payments'])) {
            $no_of_payments = $this->_request['no_of_payments'];
        }
        if (isset($this->_request['billing_payment_start_date_type'])) {
            $billing_payment_start_date_type = $this->_request['billing_payment_start_date_type'];
        }
        if (isset($this->_request['billing_options_payment_start_date'])) {
            $billing_options_payment_start_date = $this->_request['billing_options_payment_start_date'];
        }
        if (isset($this->_request['specific_start_date'])) {
            $specific_start_date = $this->_request['specific_start_date'];
        }
        if (isset($this->_request['specific_end_date'])) {
            $specific_end_date = $this->_request['specific_end_date'];
        }
        if (isset($this->_request['specific_payment_frequency'])) {
            $specific_payment_frequency = $this->_request['specific_payment_frequency'];
        }
        if (isset($this->_request['exclude_from_billing_flag'])) {
            $exclude_from_billing_flag = $this->_request['exclude_from_billing_flag'];
        }
        if (isset($this->_request['week_days_order'])) {
            $week_days_order = $this->_request['week_days_order'];
        }
        if (isset($this->_request['billing_days'])) {
            $billing_days = $this->_request['billing_days'];
        }
        if (isset($this->_request['attendance_limit_flag'])) {
            $attendance_limit_flag = $this->_request['attendance_limit_flag'];
        }
        if (isset($this->_request['attendance_period_type'])) {
            $attendance_period_type = $this->_request['attendance_period_type'];
        }

        if (!empty($recurring_membership_payment_frequency) && !in_array($recurring_membership_payment_frequency, $membership_payment_array)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($recurring_membership_payment_frequency) && $recurring_membership_payment_frequency == 'C' && (empty($custom_recurring_frequency_period_type) || empty($custom_recurring_frequency_period_val))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($membership_structure) && ($membership_structure == 'OE' || $membership_structure == 1) && !empty($delay_recurring_payment_flg) && $delay_recurring_payment_flg == 'Y' && (empty($delayed_recurring_payment_type) || empty($delayed_recurring_payment_val))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($membership_structure) && $membership_structure == 'NC' && $expiration_status == 'Y' && (empty($billing_options) || empty($billing_payment_start_date_type) || empty($expiration_date_val) || empty($expiration_date_type))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($membership_structure) && $membership_structure == 'SE' && (empty($billing_options) || empty($specific_start_date) || empty($specific_end_date) || empty($specific_payment_frequency) || empty($exclude_from_billing_flag) || empty($week_days_order) || empty($billing_days) || (strlen($billing_days) != 7))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($membership_option_id) && !empty($membership_structure) && !empty($recurring_membership_payment_frequency)) {
            $this->membership_model->updateMembershipOptionPaymentDetails($company_id, $membership_id, $membership_option_id, $membership_structure, $processing_fee, $signup_fee, $membership_fee, $recurring_membership_payment_frequency, $prorate_first_payment_period_flg, $custom_recurring_frequency_period_type, $custom_recurring_frequency_period_val, $delay_recurring_payment_flg, $delayed_recurring_payment_type, $delayed_recurring_payment_val, $initial_payment_include_membership_fee, $billing_options, $no_of_classes, $expiration_date_type, $expiration_date_val, $expiration_status, $deposit_amount, $no_of_payments, $billing_payment_start_date_type, $billing_options_payment_start_date, $specific_start_date, $specific_end_date, $specific_payment_frequency, $exclude_from_billing_flag, $week_days_order, $billing_days, $attendance_limit_flag, $attendance_period_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addMembershipDiscount() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_id = $membership_option_id = $discount_type = $discount_code = $discount_amount = $discount_off = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['discount_off'])) {
            $discount_off = $this->_request['discount_off'];
        }
        if (isset($this->_request['discount_type'])) {
            $discount_type = $this->_request['discount_type'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['discount_amount'])) {
            $discount_amount = $this->_request['discount_amount'];
        }

        // Input validations
        if (!empty($membership_id) && !empty($company_id) && !empty($membership_option_id) && !empty($discount_off) && !empty($discount_type) && !empty($discount_code) && !empty($discount_amount)) {
            $this->membership_model->addMembershipDiscountDetails($company_id, $membership_id, $membership_option_id, $discount_off, $discount_type, $discount_code, $discount_amount);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipDiscount() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_id = $membership_option_id = $discount_type = $discount_code = $discount_amount = $discount_off = $membership_discount_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['membership_discount_id'])) {
            $membership_discount_id = $this->_request['membership_discount_id'];
        }
        if (isset($this->_request['discount_off'])) {
            $discount_off = $this->_request['discount_off'];
        }
        if (isset($this->_request['discount_type'])) {
            $discount_type = $this->_request['discount_type'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['discount_amount'])) {
            $discount_amount = $this->_request['discount_amount'];
        }

        // Input validations
        if (!empty($membership_id) && !empty($company_id) && !empty($membership_option_id) && !empty($discount_off) && !empty($discount_type) && !empty($discount_code) && !empty($discount_amount)) {
            $this->membership_model->updateMembershipDiscountDetails($company_id, $membership_id, $membership_option_id, $membership_discount_id, $discount_off, $discount_type, $discount_code, $discount_amount);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteMembershipDiscount() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_option_id = $membership_discount_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['membership_discount_id'])) {
            $membership_discount_id = $this->_request['membership_discount_id'];
        }

        // Input validations
        if (!empty($membership_id) && !empty($membership_option_id) && !empty($company_id) && !empty($membership_discount_id)) {
            $this->membership_model->deleteMembershipDiscountDetails($company_id, $membership_id, $membership_option_id, $membership_discount_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipWaiver() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $waiver_policies = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['waiver_policies'])) {
            $waiver_policies = $this->_request['waiver_policies'];
        }

        // Input validations
        if (!empty($membership_id) && !empty($company_id) && !empty($waiver_policies)) {
            $this->membership_model->updateMembershipWaiverDetails($company_id, $membership_id, $waiver_policies);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipRegistration() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_column_name = $membership_column_value = $membership_mandatory_column_name = $membership_mandatory_column_value = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_column_name'])) {
            $membership_column_name = $this->_request['membership_column_name'];
        }
        if (isset($this->_request['membership_column_value'])) {
            $membership_column_value = $this->_request['membership_column_value'];
        }
        if (isset($this->_request['membership_mandatory_column_name'])) {
            $membership_mandatory_column_name = $this->_request['membership_mandatory_column_name'];
        }
        if (isset($this->_request['membership_mandatory_column_value'])) {
            $membership_mandatory_column_value = $this->_request['membership_mandatory_column_value'];
        }


        // Input validations
        if (!empty($membership_id) && !empty($company_id) && !empty($membership_column_name) && !empty($membership_column_value)) {
            $this->membership_model->updateMembershipRegistrationDetails($company_id, $membership_id, $membership_column_name, $membership_column_value, $membership_mandatory_column_name, $membership_mandatory_column_value);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteMembershipRegistration() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_column_name = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_column_name'])) {
            $membership_column_name = $this->_request['membership_column_name'];
        }


        // Input validations
        if (!empty($membership_id) && !empty($company_id) && !empty($membership_column_name)) {
            $this->membership_model->deleteMembershipRegistrationDetails($company_id, $membership_id, $membership_column_name);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function membershipOptionsRegistrationSorting() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $old_location_id = $new_location_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['old_location_id'])) {
            $old_location_id = $this->_request['old_location_id'];
        }
        if (isset($this->_request['new_location_id'])) {
            $new_location_id = $this->_request['new_location_id'];
        }

        // Input validations
        if (!empty($membership_id) && !empty($company_id) && !empty($old_location_id) && !empty($new_location_id)) {
            $this->membership_model->membershipOptionsRegistrationDetailsSorting($company_id, $membership_id, $old_location_id, $new_location_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function showMembershipOptionInApp() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_option_id = $show_in_app_flg = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['show_in_app_flg'])) {
            $show_in_app_flg = $this->_request['show_in_app_flg'];
        }

        // Input validations
        if (!empty($membership_id) && !empty($membership_option_id) && !empty($company_id) && !empty($show_in_app_flg)) {
            $this->membership_model->showMembershipOptionDetailsInApp($company_id, $membership_id, $membership_option_id, $show_in_app_flg);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function wepayMembershipCheckout() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_option_id = $membership_category_title = $membership_title = $membership_desc = $recurring_start_date = '';
        $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = $payment_frequency = $membership_start_date = $payment_start_date = $delay_recurring_payment_start_date = $custom_recurring_frequency_period_type = '';
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $student_id = $participant_id = $payment_amount = $processing_fee = $membership_fee = $membership_fee_disc = $signup_fee = $signup_fee_disc = $initial_payment = $custom_recurring_frequency_period_val = 0;
        $upgrade_status = 'F';
        $country = 'US';
        $prorate_first_payment_flg = $delay_recurring_payment_start_flg = 'N';
        $membership_payment_array = ['N', 'W', 'B', 'M', 'A', 'C'];
        $payment_array = [];
        $no_of_classes = $billing_options_deposit_amount = $billing_options_no_of_payments = $delay_recurring_payment_amount = $first_payment = 0;
        $billing_options = $billing_options_expiration_date = '';
        $membership_structure = 'OE';
        $initial_payment_include_membership_fee = 'Y';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        $rank_id = $rank_name = $registration_type = $old_membership_reg_id = '';
        $reg_type_user = 'P';
        $temp1 = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp1[1];
        $reg_version_user = $version;
        $exclude_from_billing_flag = 'N';
        $exclude_days_array = [];
        $program_start_date = $program_end_date = $reg_current_date = '';


        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['membership_category_title'])) {
            $membership_category_title = $this->_request['membership_category_title'];
        }
        if (isset($this->_request['membership_title'])) {
            $membership_title = $this->_request['membership_title'];
        }
        if (isset($this->_request['membership_desc'])) {
            $membership_desc = $this->_request['membership_desc'];
        }
        if (isset($this->_request['membership_structure'])) {
            $membership_structure = $this->_request['membership_structure'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['cc_id'])) {
            $cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_state'])) {
            $cc_state = $this->_request['cc_state'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['processing_fee'])) {
            $processing_fee = $this->_request['processing_fee'];
        }
        if (isset($this->_request['membership_fee'])) {
            $membership_fee = $this->_request['membership_fee'];
        }
        if (isset($this->_request['membership_fee_disc'])) {
            $membership_fee_disc = $this->_request['membership_fee_disc'];
        }
        if (isset($this->_request['signup_fee'])) {
            $signup_fee = $this->_request['signup_fee'];
        }
        if (isset($this->_request['signup_fee_disc'])) {
            $signup_fee_disc = $this->_request['signup_fee_disc'];
        }
        if (isset($this->_request['initial_payment'])) {      //registration time payment
            $initial_payment = $this->_request['initial_payment'];
        }
        if (isset($this->_request['first_payment'])) {      //first payment on membership start date
            $first_payment = $this->_request['first_payment'];
        }
        if (isset($this->_request['payment_frequency'])) {
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if(isset($this->_request['membership_start_date'])){
            $membership_start_date = $this->_request['membership_start_date'];
        }
        if (isset($this->_request['payment_start_date'])) {
            $payment_start_date = $this->_request['payment_start_date'];
        }
        if (isset($this->_request['recurring_start_date'])) {
            $recurring_start_date = $this->_request['recurring_start_date'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['reg_col4'])) {
            $reg_col4 = $this->_request['reg_col4'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['prorate_first_payment_flg']) && !empty(trim($this->_request['prorate_first_payment_flg']))) {
            $prorate_first_payment_flg = $this->_request['prorate_first_payment_flg'];
        }
        if (isset($this->_request['delay_recurring_payment_start_flg']) && !empty(trim($this->_request['delay_recurring_payment_start_flg']))) {
            $delay_recurring_payment_start_flg = $this->_request['delay_recurring_payment_start_flg'];
        }
        if (isset($this->_request['delay_recurring_payment_start_date'])) {
            $delay_recurring_payment_start_date = $this->_request['delay_recurring_payment_start_date'];
        }
        if (isset($this->_request['delay_recurring_payment_amount'])) {
            $delay_recurring_payment_amount = $this->_request['delay_recurring_payment_amount'];
        }
        if (isset($this->_request['custom_recurring_frequency_period_type'])) {
            $custom_recurring_frequency_period_type = $this->_request['custom_recurring_frequency_period_type'];
        }
        if (isset($this->_request['custom_recurring_frequency_period_val'])) {
            $custom_recurring_frequency_period_val = $this->_request['custom_recurring_frequency_period_val'];
        }
        if (isset($this->_request['initial_payment_include_membership_fee'])) {
            $initial_payment_include_membership_fee = $this->_request['initial_payment_include_membership_fee'];
        }
        if (isset($this->_request['billing_options'])) {
            $billing_options = $this->_request['billing_options'];
        }
        if (isset($this->_request['no_of_classes'])) {
            $no_of_classes = $this->_request['no_of_classes'];
        }
        if (isset($this->_request['billing_options_expiration_date'])) {
            $billing_options_expiration_date = $this->_request['billing_options_expiration_date'];
        }
        if (isset($this->_request['billing_options_deposit_amount'])) {
            $billing_options_deposit_amount = $this->_request['billing_options_deposit_amount'];
        }
        if (isset($this->_request['billing_options_no_of_payments'])) {
            $billing_options_no_of_payments = $this->_request['billing_options_no_of_payments'];
        }
        if (isset($this->_request['payment_array'])) {
            $payment_array = $this->_request['payment_array'];
        }
        if (isset($this->_request['participant_street'])) {
            $participant_street = $this->_request['participant_street'];
        }
        if (isset($this->_request['participant_city'])) {
            $participant_city = $this->_request['participant_city'];
        }
        if (isset($this->_request['participant_state'])) {
            $participant_state = $this->_request['participant_state'];
        }
        if (isset($this->_request['participant_zip'])) {
            $participant_zip = $this->_request['participant_zip'];
        }
        if (isset($this->_request['participant_country'])) {
            $participant_country = $this->_request['participant_country'];
        }
        if (isset($this->_request['rank_id'])) {
            $rank_id = $this->_request['rank_id'];
        }
        if (isset($this->_request['rank_name'])) {
            $rank_name = $this->_request['rank_name'];
        }
        if (isset($this->_request['registration_type'])) {
            $registration_type = $this->_request['registration_type'];
        }
        if (isset($this->_request['old_membership_reg_id'])) {
            $old_membership_reg_id = $this->_request['old_membership_reg_id'];
        }
        if (isset($this->_request['exclude_days_array'])) {
            $exclude_days_array = $this->_request['exclude_days_array'];
        }
        if (isset($this->_request['program_start_date'])) {
            $program_start_date = $this->_request['program_start_date'];
        }
        if (isset($this->_request['program_end_date'])) {
            $program_end_date = $this->_request['program_end_date'];
        }
        if (isset($this->_request['exclude_from_billing_flag'])) {
            $exclude_from_billing_flag = $this->_request['exclude_from_billing_flag'];
        }
        if (isset($this->_request['reg_current_date'])) {
            $reg_current_date = $this->_request['reg_current_date'];
        }

        //Input validations
        if (!empty(trim($payment_frequency)) && !in_array($payment_frequency, $membership_payment_array)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty(trim($payment_frequency)) && $payment_frequency == 'C' && (empty(trim($custom_recurring_frequency_period_type)) || empty(trim($custom_recurring_frequency_period_val)))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($membership_structure) && $membership_structure == 'OE' && $payment_amount > 0 && !empty(trim($delay_recurring_payment_start_flg)) && $delay_recurring_payment_start_flg == 'Y' && empty(trim($delay_recurring_payment_start_date))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($membership_structure) && $membership_structure == 'NC' && empty($billing_options)) {
            if (!empty($billing_options) && $billing_options == 'PP' && empty($payment_array)) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        }
        if (!empty($membership_structure) && $membership_structure == 'SE' && ((empty($program_start_date) || $program_start_date == '0000-00-00' || empty($program_end_date) || $program_end_date == '0000-00-00') || (!empty($billing_options) && $billing_options == 'PP' && $payment_amount > 0 && empty($payment_array)))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        if (!empty($company_id) && !empty($membership_id) && !empty($membership_option_id) && !empty($membership_category_title) && !empty($membership_title) && !empty($membership_desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
            $this->membership_model->membershipCheckout($company_id, $membership_id, $membership_option_id, $student_id, $participant_id, $upgrade_status, $membership_category_title, $membership_title, $membership_desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $processing_fee_type, $processing_fee, $membership_fee, $membership_fee_disc, $signup_fee, $signup_fee_disc, $payment_amount, $initial_payment, $first_payment, $payment_frequency, $payment_start_date, $recurring_start_date, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $prorate_first_payment_flg, $delay_recurring_payment_start_flg, $delay_recurring_payment_start_date, $delay_recurring_payment_amount, $custom_recurring_frequency_period_type, $custom_recurring_frequency_period_val, $initial_payment_include_membership_fee, $membership_structure, $billing_options, $no_of_classes, $billing_options_expiration_date, $billing_options_deposit_amount, $billing_options_no_of_payments, $payment_array, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $rank_id, $rank_name, $registration_type, $old_membership_reg_id, $exclude_days_array, $program_start_date, $program_end_date, $exclude_from_billing_flag, $reg_type_user, $reg_version_user, $reg_current_date, $membership_start_date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getMembershipParticipants() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_option_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }

        if (!empty($company_id) && !empty($membership_id) && !empty($membership_option_id)) {
            $this->membership_model->getMembershipParticipantDetails($company_id, $membership_id, $membership_option_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function sendMembershipPush() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_option_id = $message = $type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }

        if (!empty($company_id) && !empty($membership_id) && !empty($membership_option_id) && !empty($message) && !empty($type)) {
            if ($type == 'push') {
                $this->membership_model->sendPushMessageForMembership($company_id, $membership_id, $membership_option_id, $message);
            } elseif ($type == 'mail') {
                $this->membership_model->sendPushMailForMembership($company_id, $membership_id, $membership_option_id, $message);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getMembershipTemplate() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->membership_model->getAllMembershipDetails(6, 'P', 0, '');
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function Customers() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_status = $membership_startdate_limit = $membership_enddate_limit = $draw_table = $length_table = $start = $storting = $storting_type =$notattendeddays=$notattendeddays1= '';
        $status_array = ['R', 'P', 'C'];
        $membership_days_type = 5;
        $register_type=1;
        $search = [];
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_status'])) {
            $membership_status = $this->_request['membership_status'];
        }
//        if (isset($this->_request['data_limit'])) {
//            $data_limit = $this->_request['data_limit'];
//        }
        if (isset($this->_request['membership_startdate_limit'])) {
            $membership_startdate_limit = $this->_request['membership_startdate_limit'];
        }
        if (isset($this->_request['membership_enddate_limit'])) {
            $membership_enddate_limit = $this->_request['membership_enddate_limit'];
        }
        if (isset($this->_request['membership_days_type'])) {
            $membership_days_type = $this->_request['membership_days_type'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = $this->_request['start'];
        }

        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }
        
        if (isset($this->_request['register_type'])) {
            $register_type = $this->_request['register_type'];
        }
        if (isset($this->_request['notattendeddays'])) {
            $notattendeddays = $this->_request['notattendeddays'];
        }
        if (isset($this->_request['notattendeddays1'])) {
            $notattendeddays1 = $this->_request['notattendeddays1'];
        }
        


        if (!empty($company_id) && !empty($membership_status)) {
            if (!in_array($membership_status, $status_array)) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }            
            if ($membership_days_type == 4 && (empty(trim($membership_startdate_limit)) || empty(trim($membership_enddate_limit)))) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 200);
            }
//            if(($register_type==2 && (empty($notattendeddays))) || ($register_type==3 && (empty($notattendeddays) || empty($notattendeddays1)))){
//                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
//                $this->response($this->json($error), 200);
//            }
//            $this->membership_model->getCustomerDetails($company_id, $membership_status,$data_limit);
            $this->membership_model->getmembershipDetailsByFilterType($company_id, $membership_days_type, $membership_startdate_limit, $membership_enddate_limit, $membership_status, $search, $length_table, $draw_table, $start, $sorting,$register_type,$notattendeddays,$notattendeddays1);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getcustomersdetailsbyfilter() {

        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_days_type = $membership_startdate_limit = $membership_enddate_limit = $membership_status = $data_limit = '';
        $status_array = ['R', 'P', 'C'];

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_days_type'])) {
            $membership_days_type = $this->_request['membership_days_type'];
        }
        if (isset($this->_request['membership_startdate_limit'])) {
            $membership_startdate_limit = $this->_request['membership_startdate_limit'];
        }
        if (isset($this->_request['membership_enddate_limit'])) {
            $membership_enddate_limit = $this->_request['membership_enddate_limit'];
        }
        if (isset($this->_request['membership_status'])) {
            $membership_status = $this->_request['membership_status'];
        }
        if (isset($this->_request['data_limit'])) {
            $data_limit = $this->_request['data_limit'];
        }

        if (!empty($company_id) && !empty($membership_status) && !empty($membership_days_type)) {
            if (!in_array($membership_status, $status_array)) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
            if ($membership_days_type == 4 && (empty(trim($membership_startdate_limit)) || empty(trim($membership_enddate_limit)))) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
            $this->membership_model->getmembershipDetailsByFilterType($company_id, $membership_days_type, $membership_startdate_limit, $membership_enddate_limit, $membership_status, $data_limit);
        } else {

            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getMembersByMembershipId() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_opt_id = '';
        $status_array = ['R', 'P', 'C'];
        $membership_status = 'R';
        $search = $draw_table = $length_table = $start = $sorting = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_opt_id'])) {
            $membership_opt_id = $this->_request['membership_opt_id'];
        }
        if (isset($this->_request['membership_status'])) {
            $membership_status = $this->_request['membership_status'];
        }

        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = $this->_request['start'];
        }

        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }



        if (!in_array($membership_status, $status_array)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($company_id) && !empty($membership_id) && !empty($membership_opt_id) && !empty($membership_status)) {
            $this->membership_model->getMemberDetailsByMembershipId($company_id, $membership_id, $membership_opt_id, $membership_status, $search, $draw_table, $length_table, $start, $sorting);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function sendMembershipOrderReceipt() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_reg_id = '';
        $return_value = 1;
        $label_for_email_subject = 'P';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $membership_reg_id = $this->_request['membership_reg_id'];
        }
        if (isset($this->_request['label_for_email_subject'])) {
            $label_for_email_subject = $this->_request['label_for_email_subject'];
        }

        if (!empty($company_id) && !empty($membership_reg_id)) {
            $this->membership_model->sendOrderReceiptForMembershipPayment($company_id, $membership_reg_id, $label_for_email_subject, $return_value);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

//    private function updateMembershipPaymentMethod(){
//         // Cross validation if the request method is POST else it will return "Not Acceptable" status
//        if($this->get_request_method() != "POST"){
//            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
//            $this->response($this->json($error),406);
//        }
//        $company_id=$membership_reg_id=$event_payment_id='';
//        $new_cc_id=$cc_state=$buyer_name=$buyer_email=$postal_code=$page_key='';
//        
//        if(isset($this->_request['company_id'])){
//            $company_id = $this->_request['company_id'];
//        }
//        if(isset($this->_request['membership_reg_id'])){
//            $membership_reg_id = $this->_request['membership_reg_id'];
//        }
//        if(isset($this->_request['buyer_name'])){
//            $buyer_name = $this->_request['buyer_name'];
//        }
//        if(isset($this->_request['email'])){
//            $buyer_email = $this->_request['email'];
//        }
//        if(isset($this->_request['postal_code'])){
//            $postal_code = $this->_request['postal_code'];
//        }
//        if(isset($this->_request['cc_id'])){
//            $new_cc_id = $this->_request['cc_id'];
//        }
//        if(isset($this->_request['cc_state'])){
//            $cc_state = $this->_request['cc_state'];
//        }
//        if(isset($this->_request['page_key'])){
//            $page_key = $this->_request['page_key'];
//        }
//        
//           // Input validations
//        if (!empty($company_id) && !empty($membership_reg_id) && !empty($new_cc_id) && !empty($buyer_name) && !empty($buyer_email) && !empty($page_key)) {
//            $this->membership_model->updateMembershipPaymentMethodDetails($company_id,$membership_reg_id,$new_cc_id,$cc_state,$buyer_name,$buyer_email,$postal_code,$page_key);
//        } else {
//            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
//            $this->response($this->json($error), 200);
//        }
//    }

    private function updateMembershipParticipantName() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_reg_id = $membership_option_id = $pfirst_name = $plast_name = $membership_status = $buyer_first_name = $buyer_last_name = '';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = $participant_birth_date = $participant_phone = $participant_email = '';
        $membership_registration_Cvalue_4 = $membership_registration_Cvalue_5 = $membership_registration_Cvalue_6 = $membership_registration_Cvalue_7 = $membership_registration_Cvalue_8 = $membership_registration_Cvalue_9 = $membership_registration_Cvalue_10 = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $membership_reg_id = $this->_request['membership_reg_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['membership_status'])) {
            $membership_status = $this->_request['membership_status'];
        }
        if (isset($this->_request['pfirst_name'])) {
            $pfirst_name = $this->_request['pfirst_name'];
        }
        if (isset($this->_request['plast_name'])) {
            $plast_name = $this->_request['plast_name'];
        }
        if (isset($this->_request['bfirst_name'])) {
            $buyer_first_name = $this->_request['bfirst_name'];
        }
        if (isset($this->_request['blast_name'])) {
            $buyer_last_name = $this->_request['blast_name'];
        }
        if (isset($this->_request['participant_street'])) {
            $participant_street = $this->_request['participant_street'];
        }
        if (isset($this->_request['participant_city'])) {
            $participant_city = $this->_request['participant_city'];
        }
        if (isset($this->_request['participant_state'])) {
            $participant_state = $this->_request['participant_state'];
        }
        if (isset($this->_request['participant_zip'])) {
            $participant_zip = $this->_request['participant_zip'];
        }
        if (isset($this->_request['participant_country'])) {
            $participant_country = $this->_request['participant_country'];
        }
        if (isset($this->_request['participant_birth_date'])) {
            $participant_birth_date = $this->_request['participant_birth_date'];
        }
        if (isset($this->_request['participant_phone'])) {
            $participant_phone = $this->_request['participant_phone'];
        }
        if (isset($this->_request['participant_email'])) {
            $participant_email = $this->_request['participant_email'];
        }
        if (isset($this->_request['membership_registration_Cvalue_4'])) {
            $membership_registration_Cvalue_4 = $this->_request['membership_registration_Cvalue_4'];
        }
        if (isset($this->_request['membership_registration_Cvalue_5'])) {
            $membership_registration_Cvalue_5 = $this->_request['membership_registration_Cvalue_5'];
        }
        if (isset($this->_request['membership_registration_Cvalue_6'])) {
            $membership_registration_Cvalue_6 = $this->_request['membership_registration_Cvalue_6'];
        }
        if (isset($this->_request['membership_registration_Cvalue_7'])) {
            $membership_registration_Cvalue_7 = $this->_request['membership_registration_Cvalue_7'];
        }
        if (isset($this->_request['membership_registration_Cvalue_8'])) {
            $membership_registration_Cvalue_8 = $this->_request['membership_registration_Cvalue_8'];
        }
        if (isset($this->_request['membership_registration_Cvalue_9'])) {
            $membership_registration_Cvalue_9 = $this->_request['membership_registration_Cvalue_9'];
        }
        if (isset($this->_request['membership_registration_Cvalue_10'])) {
            $membership_registration_Cvalue_10 = $this->_request['membership_registration_Cvalue_10'];
        }


        if (!empty($company_id) && !empty($membership_id) && !empty($membership_reg_id) && !empty($membership_option_id) && !empty($pfirst_name) && !empty($plast_name) && !empty($buyer_first_name) && !empty($buyer_last_name)) {
            $this->membership_model->updateMembershipParticipantNameInDb($company_id, $membership_reg_id, $membership_status, $pfirst_name, $plast_name, $buyer_first_name, $buyer_last_name, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $participant_birth_date, $participant_phone, $participant_email, $membership_registration_Cvalue_4, $membership_registration_Cvalue_5, $membership_registration_Cvalue_6, $membership_registration_Cvalue_7, $membership_registration_Cvalue_8, $membership_registration_Cvalue_9, $membership_registration_Cvalue_10);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipParticipantStatus() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_reg_id = $membership_option_id = $membership_status = $category_title = $option_title = $updated_amount = $next_date = $payment_pause_flag = $payment_pause_date = '';
        $pay_now = 'N';
        $resume_without_date_flg = 'N';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $membership_reg_id = $this->_request['membership_reg_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['membership_status'])) {
            $membership_status = $this->_request['membership_status'];
        }
        if (isset($this->_request['category_title'])) {
            $category_title = $this->_request['category_title'];
        }
        if (isset($this->_request['option_title'])) {
            $option_title = $this->_request['option_title'];
        }
        if (isset($this->_request['updated_amount'])) {
            $updated_amount = $this->_request['updated_amount'];
        }
        if (isset($this->_request['next_date'])) {
            $next_date = $this->_request['next_date'];
        }
        if (isset($this->_request['pay_now_flg'])) {
            $pay_now = $this->_request['pay_now_flg'];
        }
        if (isset($this->_request['resume_without_date_flg'])) {
            $resume_without_date_flg = $this->_request['resume_without_date_flg'];
        }
       if (isset($this->_request['scheduled_cancel_flag'])) {
           $scheduled_cancel_flag = $this->_request['scheduled_cancel_flag'];
        }
        if (isset($this->_request['cancellation_date'])) {
           $cancellation_date = $this->_request['cancellation_date'];
        }
        if (isset($this->_request['payment_pause_flag'])) {
           $payment_pause_flag = $this->_request['payment_pause_flag'];
        }
        if (isset($this->_request['payment_pause_date'])) {
           $payment_pause_date = $this->_request['payment_pause_date'];
        }
        if (!empty($company_id) && !empty($membership_id) && !empty($membership_reg_id) && !empty($membership_option_id) && !empty($membership_status)) {
            $this->membership_model->updateMembershipParticipantStatusInDb($company_id, $membership_id, $membership_reg_id, $membership_option_id, $membership_status, $category_title, $option_title, $updated_amount, $next_date, $pay_now, $resume_without_date_flg,$scheduled_cancel_flag,$cancellation_date,$payment_pause_flag,$payment_pause_date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateParticipantMembershipRank() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $rank_id = $membership_reg_id = $membership_rank = $membership_status = '';
//        $advance_date_update = 'Y';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $membership_reg_id = $this->_request['membership_reg_id'];
        }
        if (isset($this->_request['membership_status'])) {
            $membership_status = $this->_request['membership_status'];
        }
        if (isset($this->_request['rank_id'])) {
            $rank_id = $this->_request['rank_id'];
        }
        if (isset($this->_request['membership_rank'])) {
            $membership_rank = $this->_request['membership_rank'];
        }
        if (isset($this->_request['advance_date'])) {
            $advance_date_update = $this->_request['advance_date'];
        }
//        if(isset($this->_request['advance_date_update']) && !empty($this->_request['advance_date_update'])){
//            $advance_date_update = $this->_request['advance_date_update'];
//        }

        if (!empty($company_id) && !empty($membership_reg_id) && !empty($rank_id) && !empty($membership_rank)) {
            $this->membership_model->updateParticipantMembershipRankInDb($company_id, $membership_reg_id, $membership_status, $rank_id, $membership_rank, $advance_date_update);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function sendIndividualPush() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $subject = $message = $mail_id = $membership_registration_id = $file_name = $attached_file = '';
        $type = 'M';
        $to_send = 'A';     //to_send -> A - All(Push & email), E - Email, P - Push
        $type_array = ['M', 'E', 'S'];

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['subject'])) {
            $subject = $this->_request['subject'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['mail_id'])) {
            $mail_id = $this->_request['mail_id'];
        }
        if (isset($this->_request['attached_file'])) {
            $attached_file = $this->_request['attached_file'];
        }
        if (isset($this->_request['file_name'])) {
            $file_name = $this->_request['file_name'];
        }
        if (isset($this->_request['type_id'])) {
            $type_id = $this->_request['type_id'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['to_send']) && !empty($this->_request['to_send'])) {
            $to_send = $this->_request['to_send'];
        }

        if (!empty($type) && !in_array($type, $type_array)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($company_id) && !empty($message) && !empty($mail_id) && !empty($type_id)) {
            $this->membership_model->sendIndividualPushMail($company_id, $subject, $message, $mail_id, $type_id, $type, $attached_file, $file_name, $to_send);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getPaymentDetails() {       // Forward to getPaymentDetailsByFilterType
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $payment_type = $payment_startdate_limit = $payment_enddate_limit = '';
        $category_type = 'A';
        $payment_days_type = 1;
        $search = $draw_table  = $start = $sorting = '';
        $length_table=72;
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['category_type']) && !empty(trim($this->_request['category_type']))) {
            $category_type = $this->_request['category_type'];
        }

        if ($payment_type == 'P') {
            $payment_days_type = 5;
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])&& is_numeric($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = $this->_request['start'];
        }
        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }

        if (!empty($company_id) && !empty($payment_type)) {
//            $this->getPaymentDetailsByCompany($company_id,$list_flag,$category_type,$data_limit);
            $this->getPaymentDetailsByFilterType($company_id, $payment_type, $payment_days_type, $payment_startdate_limit, $payment_enddate_limit, $category_type, $search, $draw_table, $length_table, $start, $sorting);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getPaymentDetailsbyfilter() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $payment_type = $payment_days_type = $payment_startdate_limit = $payment_enddate_limit = $search = $draw_table = $length_table = $start = $sorting = '';

        $category_type = 'A';

        if (isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['payment_type'])){
            $payment_type = $this->_request['payment_type'];
        }
//        if(isset($this->_request['payment_option_type']))
//            $payment_option_type = $this->_request['payment_option_type'];
        if (isset($this->_request['payment_days_type'])){
            $payment_days_type = $this->_request['payment_days_type'];
        }
        if (isset($this->_request['payment_startdate_limit'])){
            $payment_startdate_limit = $this->_request['payment_startdate_limit'];
        }
        if (isset($this->_request['payment_enddate_limit'])){
            $payment_enddate_limit = $this->_request['payment_enddate_limit'];
        }
        if (isset($this->_request['category_type']) && !empty(trim($this->_request['category_type']))) {
            $category_type = $this->_request['category_type'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = $this->_request['start'];
        }

        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }

        // Input validations
        if (!empty($company_id) && !empty($payment_type) && !empty($payment_days_type)) {
            if ($payment_days_type == 4 && (empty($payment_startdate_limit) || empty($payment_enddate_limit))) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
            $this->getPaymentDetailsByFilterType($company_id, $payment_type, $payment_days_type, $payment_startdate_limit, $payment_enddate_limit, $category_type, $search, $draw_table, $length_table, $start, $sorting);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function sendOrderReceipt() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $reg_id = '';
        $return_value = 1;
        $category_type = 'E';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['category_type']) && !empty($this->_request['category_type'])) {
            $category_type = $this->_request['category_type'];
        }

        if (!empty($company_id) && !empty($reg_id)) {
            if ($category_type == 'M') {
                $this->membership_model->sendOrderReceiptForMembershipPayment($company_id, $reg_id, $return_value);
            } else {
                $this->event_model->sendOrderReceiptForEventPayment($company_id, $reg_id, $return_value);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updatePaymentMethod() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $reg_id = $buyer_first_name = $buyer_last_name = '';
        $new_cc_id = $cc_state = $buyer_name = $buyer_email = $postal_code = $page_key = '';
        $category = 'E';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['postal_code'])) {
            $postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['cc_id'])) {
            $new_cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_state'])) {
            $cc_state = $this->_request['cc_state'];
        }
        if (isset($this->_request['category_type']) && !empty($this->_request['category_type'])) {
            $category = $this->_request['category_type'];
        }
        if (isset($this->_request['page_key'])) {
            $page_key = $this->_request['page_key'];
        }
        $type= '';
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if($type == "CC" && (empty($reg_id) || empty($new_cc_id) || empty($buyer_first_name) || empty($buyer_last_name) || empty($buyer_email))){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        // Input validations
        if (!empty($company_id) ) {
            if ($category == 'M') {
                $this->membership_model->updateMembershipPaymentMethodDetails($company_id, $reg_id, $new_cc_id, $cc_state, $buyer_first_name, $buyer_last_name, $buyer_email, $postal_code, $page_key,$type);
            } else if ($category == 'T') {
                $this->trial_model->updateTrialPaymentMethodDetails($company_id, $reg_id, $new_cc_id, $cc_state, $buyer_first_name, $buyer_last_name, $buyer_email, $postal_code);
            } else {
                $this->event_model->updateEventPaymentMethodDetails($reg_id, $new_cc_id, $cc_state, $buyer_first_name, $buyer_last_name, $buyer_email, $postal_code, $page_key,$type);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function reRunPayment() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $reg_id = $payment_id = $category = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['payment_id'])) {
            $payment_id = $this->_request['payment_id'];
        }
        if (isset($this->_request['category_type']) && !empty($this->_request['category_type'])) {
            $category = $this->_request['category_type'];
        }

        // Input validations
        if (!empty($company_id) && !empty($reg_id) && !empty($payment_id)) {
            if ($category == 'M') {
                $this->membership_model->reRunMembershipPayment($company_id, $reg_id, $payment_id);
            } else if ($category == 'E') {
                $this->event_model->reRunEventPayment($company_id, $reg_id, $payment_id);
            } else if ($category == 'T'){
                $this->trial_model->reRunTrialPayment($company_id, $reg_id, $payment_id);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function exportMembershipData() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_id = $membership_option_id = $type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if ($type == 'O' && empty(trim($membership_option_id))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        if (!empty($company_id) && !empty($membership_id) && !empty($type)) {
            $this->membership_model->createMembershipCSV($company_id, $membership_id, $membership_option_id, $type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function processMembershipPaymentPlanRefund() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_reg_id = $membership_payment_id = $membership_id = '';


        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $membership_reg_id = $this->_request['membership_reg_id'];
        }
        if (isset($this->_request['membership_payment_id'])) {
            $membership_payment_id = $this->_request['membership_payment_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['allow_manual_credit_refund'])) {
            $allowmanualcreditrefund = $this->_request['allow_manual_credit_refund'];
        }
        
        if (!empty($company_id) && !empty($membership_reg_id) && !empty($membership_payment_id)) {
            if($allowmanualcreditrefund == true){ 
                $this->membership_model->processcashcheckPaymentRefund($company_id, $membership_reg_id, $membership_payment_id);
            }else if($allowmanualcreditrefund == false){
                $this->membership_model->processMembershipPaymentRefund($company_id, $membership_reg_id, $membership_payment_id);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function editMembershipFirstPayment() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_reg_id = $first_payment_date = $updated_amount = $upgrade_status = '';
        $pay_now_flg = $delete_payment = 'N';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $membership_reg_id = $this->_request['membership_reg_id'];
        }
        if (isset($this->_request['first_payment_date'])) {
            $first_payment_date = $this->_request['first_payment_date'];
        }
        if (isset($this->_request['updated_amount'])) {
            $updated_amount = $this->_request['updated_amount'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['pay_now_flg'])) {
            $pay_now_flg = $this->_request['pay_now_flg'];
        }
        if (isset($this->_request['delete_payment'])) {
            $delete_payment = $this->_request['delete_payment'];
        }

        if (!empty($company_id) && !empty($membership_reg_id) && !empty($first_payment_date) && !empty($updated_amount) && !empty($upgrade_status)) {
            $this->membership_model->editMembershipFirstPaymentDetails($company_id, $membership_reg_id, $first_payment_date, $updated_amount, $upgrade_status, $pay_now_flg, $delete_payment);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function editMembership() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_reg_id = $next_date = $updated_amount = $upgrade_status = $membership_start_date = '';
        $pay_now_flg = 'N';


        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $membership_reg_id = $this->_request['membership_reg_id'];
        }
        if (isset($this->_request['next_date'])) {
            $next_date = $this->_request['next_date'];
        }
        if (isset($this->_request['updated_amount'])) {
            $updated_amount = $this->_request['updated_amount'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['pay_now_flg'])) {
            $pay_now_flg = $this->_request['pay_now_flg'];
        }
        if (isset($this->_request['membership_start_date'])) {
            $membership_start_date = $this->_request['membership_start_date'];
        }

        if (!empty($company_id) && !empty($membership_reg_id) && !empty($next_date) && !empty($updated_amount) && !empty($upgrade_status)) {
            $this->membership_model->editMembershipDetails($company_id, $membership_reg_id, $next_date, $updated_amount, $upgrade_status, $pay_now_flg, $membership_start_date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function dashboard() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $activemembers_flag = $member = $netsales = $netsales_flag = $rentation_flag = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['n_flag'])) {
            $netsales_flag = $this->_request['n_flag'];
        }
        if (isset($this->_request['mem'])) {
            $member = $this->_request['mem'];
        }
        if (isset($this->_request['net'])) {
            $netsales = $this->_request['net'];
        }
        if (isset($this->_request['a_flag'])) {
            $activemembers_flag = $this->_request['a_flag'];
        }
        if (isset($this->_request['r_flag'])) {
            $rentation_flag = $this->_request['r_flag'];
        }
        if (isset($this->_request['sales_period'])) {
            $sales_period = $this->_request['sales_period'];
        }

        if (!empty($company_id)) {
            $this->getDashboardDetails($company_id, $netsales_flag, $member, $netsales, $activemembers_flag, $rentation_flag, $sales_period);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addMembershipdashboard() {

        $company_id = $membership_id = $category_title = $option_title = $date = '';
        $mem_option_id = 0;

        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['mem_option_id'])) {
            $mem_option_id = $this->_request['mem_option_id'];
        }
        if (isset($this->_request['category_title'])) {
            $category_title = $this->_request['category_title'];
        }
        if (isset($this->_request['option_title'])) {
            $option_title = $this->_request['option_title'];
        }
        if (isset($this->_request['date'])) {
            $date = $this->_request['date'];
        }
        if (!empty($company_id) && !empty($membership_id)) {
            $this->membership_model->addMembershipDimensions($company_id, $membership_id, $mem_option_id, $category_title, $option_title, $date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipNetsales() {
        $company_id = $membership_id = $date = '';

        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['date'])) {
            $date = $this->_request['date'];
        }

        if (!empty($company_id) && !empty($membership_id)) {
            $this->membership_model->updateMembershipDimensionsNetSales($company_id, $membership_id, $date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipmembers() {
        $company_id = $membership_id = $date = '';

        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['date'])) {
            $date = $this->_request['date'];
        }

        if (!empty($company_id) && !empty($membership_id)) {
            $this->membership_model->updateMembershipDimensionsMembers($company_id, $membership_id, $date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function sendMembershipGroupPush() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $subject = $message = $type = $attached_file = $file_name = '';
        $also_send_mail = 'N';
        $mem_reg_id = [];
        $register_type=1;
        $all_select_flag = 'N';
        $membership_status = $membership_days_type = $membership_startdate_limit = $membership_enddate_limit = $membership_option_id=$from=$search=$notattendeddays=$notattendeddays1='';
        
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['subject'])) {
            $subject = $this->_request['subject'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['selected_membership_list'])) {
            $mem_reg_id = $this->_request['selected_membership_list'];
        }
        if (isset($this->_request['also_send_mail'])) {
            $also_send_mail = $this->_request['also_send_mail'];
        }
        if (isset($this->_request['attached_file'])) {
            $attached_file = $this->_request['attached_file'];
        }
        if (isset($this->_request['file_name'])) {
            $file_name = $this->_request['file_name'];
        }
        if (isset($this->_request['all_select_flag'])) {
            $all_select_flag = $this->_request['all_select_flag'];
        }
        if (isset($this->_request['membership_days_type'])) {
            $membership_days_type = $this->_request['membership_days_type'];
        }
        if (isset($this->_request['membership_startdate_limit'])) {
            $membership_startdate_limit = $this->_request['membership_startdate_limit'];
        }
        if (isset($this->_request['membership_enddate_limit'])) {
            $membership_enddate_limit = $this->_request['membership_enddate_limit'];
        }
        if (isset($this->_request['membership_status'])) {
            $membership_status = $this->_request['membership_status'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['from'])) {
            $from = $this->_request['from'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        
        if (isset($this->_request['register_type'])) {
            $register_type = $this->_request['register_type'];
        }
        if (isset($this->_request['notattendeddays'])) {
            $notattendeddays = $this->_request['notattendeddays'];
        }
        if (isset($this->_request['notattendeddays1'])) {
            $notattendeddays1 = $this->_request['notattendeddays1'];
        }
        if(isset($this->_request['unsubscribe_email'])){
            $unsubscribe_email = $this->_request['unsubscribe_email'];
        } 
        
        if ($type != 'subscribe') {
            if (!empty($company_id) && !empty($message) && !empty($type) && ((!empty($mem_reg_id) && $all_select_flag == 'N') || $all_select_flag == 'Y') && !empty($membership_status)) {
                if ($type == 'push') {
                    $this->membership_model->sendGroupPushMessageForMembership($company_id, $subject, $message, $mem_reg_id, $also_send_mail, $attached_file, $file_name, $membership_status, $all_select_flag, $membership_days_type, $membership_startdate_limit, $membership_enddate_limit, $membership_option_id, $search, $register_type, $notattendeddays, $notattendeddays1);
                } elseif ($type == 'mail') {
                    $this->membership_model->sendGroupPushMailForMembership($company_id, $subject, $message, $mem_reg_id, '', $attached_file, $file_name, $membership_status, $all_select_flag, $membership_days_type, $membership_startdate_limit, $membership_enddate_limit, $membership_option_id, $search, $register_type, $notattendeddays, $notattendeddays1);
                } else {
                    $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $this->activationlinkCreationForEmail($unsubscribe_email, $subject,  $company_id);
        }
    }

    private function createMemSelectonCSV() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        $mem_reg_id = [];
        $all_select_flag = 'N';
         $register_type=1;
        $membership_status = $membership_days_type = $membership_startdate_limit = $membership_enddate_limit = $membership_option_id = $from =$notattendeddays=$notattendeddays1= '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['selected_membership_list'])) {
            $mem_reg_id = $this->_request['selected_membership_list'];
        }
        if (isset($this->_request['all_select_flag'])) {
            $all_select_flag = $this->_request['all_select_flag'];
        }
        if (isset($this->_request['membership_days_type'])) {
            $membership_days_type = $this->_request['membership_days_type'];
        }
        if (isset($this->_request['membership_startdate_limit'])) {
            $membership_startdate_limit = $this->_request['membership_startdate_limit'];
        }
        if (isset($this->_request['membership_enddate_limit'])) {
            $membership_enddate_limit = $this->_request['membership_enddate_limit'];
        }
        if (isset($this->_request['membership_status'])) {
            $membership_status = $this->_request['membership_status'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['from'])) {
            $from = $this->_request['from'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
         if (isset($this->_request['register_type'])) {
            $register_type = $this->_request['register_type'];
        }
        if (isset($this->_request['notattendeddays'])) {
            $notattendeddays = $this->_request['notattendeddays'];
        }
        if (isset($this->_request['notattendeddays1'])) {
            $notattendeddays1 = $this->_request['notattendeddays1'];
        }
        
        if(!empty($company_id) && ((!empty($mem_reg_id) && $all_select_flag == 'N') || $all_select_flag == 'Y')){
            $this->membership_model->createMembershipSelectonCSV($company_id,$mem_reg_id,$all_select_flag,$membership_days_type,$membership_startdate_limit,$membership_enddate_limit,$membership_status,$membership_option_id,$search,$register_type,$notattendeddays,$notattendeddays1);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipParticipantNameByID() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_reg_id = $pfirst_name = $plast_name = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $membership_reg_id = $this->_request['membership_reg_id'];
        }
        if (isset($this->_request['pfirst_name'])) {
            $pfirst_name = $this->_request['pfirst_name'];
        }
        if (isset($this->_request['plast_name'])) {
            $plast_name = $this->_request['plast_name'];
        }
        if (!empty($company_id) && !empty($membership_reg_id) && !empty($pfirst_name) && !empty($plast_name) && !empty($membership_id)) {
            $this->membership_model->updateMembershipParticipantNameByIdInDb($company_id, $membership_id, $membership_reg_id, $pfirst_name, $plast_name);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function membersRegDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $mem_reg_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['selected_membership_id'])) {
            $mem_reg_id = $this->_request['selected_membership_id'];
        }
        if (!empty($company_id) && !empty($mem_reg_id)) {
            $this->membership_model->getMembersRegDetails($company_id, $mem_reg_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function participantInfoDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $mem_reg_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['selected_membership_id'])) {
            $mem_reg_id = $this->_request['selected_membership_id'];
        }
        if (!empty($company_id) && !empty($mem_reg_id)) {
            $this->membership_model->getparticipantInfoDetails($company_id, $mem_reg_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function membershipHistoryDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $mem_reg_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['selected_membership_id'])) {
            $mem_reg_id = $this->_request['selected_membership_id'];
        }
        if (!empty($company_id) && !empty($mem_reg_id)) {
            $this->membership_model->getMembershipHistoryDetails($company_id, $mem_reg_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function paymentHistoryDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $reg_id = $category = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }        
        if (isset($this->_request['category'])) {
            $category = $this->_request['category'];
        }
        if (!empty($company_id) && !empty($reg_id) && !empty($category)) {
            if ($category == 'membership') {
                $this->membership_model->getpaymentHistoryDetails($company_id, $reg_id, 0);
            } elseif ($category == 'event') {
                $this->event_model->getpaymentHistoryDetails($company_id, $reg_id, 0);
            } elseif ($category == 'trial'){
                 $this->trial_model->getpaymentHistoryDetails($company_id, $reg_id, 0);
            }elseif ($category == 'retail'){
                 $this->retail_model->getRetailOrderDetails($company_id, $reg_id, 0);
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipHistoryDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $membership_status = 'R';
        $company_id = $student_id = $membership_reg_id = $transferred_from = $activity_name = $activity_text = '';
        $membership_fee = $next_payment_date = $payment_method = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $membership_reg_id = $this->_request['membership_reg_id'];
        }
        if (isset($this->_request['activity_name'])) {
            $activity_name = $this->_request['activity_name'];
        }
        if (isset($this->_request['activity_text'])) {
            $activity_text = $this->_request['activity_text'];
        }
        if (isset($this->_request['membership_fee'])) {
            $membership_fee = $this->_request['membership_fee'];
        }
        if (isset($this->_request['next_payment_date'])) {
            $next_payment_date = $this->_request['next_payment_date'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (!empty($company_id) && !empty($membership_reg_id) && !empty($activity_name)) {
            $this->membership_model->updateMembershipHistoryDetailsInDb($company_id, $membership_reg_id, $membership_status, $activity_name, $activity_text, $membership_fee, $next_payment_date, $payment_method);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateMembershipPaymentHistoryDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $payment_id = $membership_reg_id = $activity_name = $activity_text = $payment_status = $refunded_amount = '';
        $refunded_date = $paytime_type = $cc_id = $cc_name = $membership_status = $membership_structure = $processing_fee_type = $prorate_first_payment_flag = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['payment_id'])) {
            $payment_id = $this->_request['payment_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $membership_reg_id = $this->_request['membership_reg_id'];
        }
        if (isset($this->_request['activity_name'])) {
            $activity_name = $this->_request['activity_name'];
        }
        if (isset($this->_request['activity_text'])) {
            $activity_text = $this->_request['activity_text'];
        }
        if (isset($this->_request['payment_status'])) {
            $payment_status = $this->_request['payment_status'];
        }
        if (isset($this->_request['refunded_amount'])) {
            $refunded_amount = $this->_request['refunded_amount'];
        }
        if (isset($this->_request['refunded_date'])) {
            $refunded_date = $this->_request['refunded_date'];
        }
        if (isset($this->_request['paytime_type'])) {
            $paytime_type = $this->_request['paytime_type'];
        }
        if (isset($this->_request['cc_id'])) {
            $cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_name'])) {
            $cc_name = $this->_request['cc_name'];
        }
        if (isset($this->_request['membership_status'])) {
            $membership_status = $this->_request['membership_status'];
        }
        if (isset($this->_request['membership_structure'])) {
            $membership_structure = $this->_request['membership_structure'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['prorate_first_payment_flag'])) {
            $prorate_first_payment_flag = $this->_request['prorate_first_payment_flag'];
        }


        if (!empty($company_id) && !empty($membership_reg_id) && !empty($activity_name)) {
            $this->membership_model->updateMembershipPaymentHistoryDetailsInDb($company_id, $payment_id, $membership_reg_id, $activity_name, $activity_text, $payment_status, $refunded_amount, $refunded_date, $paytime_type, $cc_id, $cc_name, $membership_status, $membership_structure, $processing_fee_type, $prorate_first_payment_flag);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function membershipPaymentMarkedAsPaid() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $reg_id = $payment_id = $credit_method ='';
        $credit_amount =$check_number =0;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['payment_id'])) {
            $payment_id = $this->_request['payment_id'];
        }
        if (isset($this->_request['credit_amount'])) {
            $credit_amount = $this->_request['credit_amount'];
        }

        if (isset($this->_request['credit_method'])) {
            $credit_method = $this->_request['credit_method'];
        }
        
        if (isset($this->_request['check_number'])) {
            $check_number = $this->_request['check_number'];
        }
        if($credit_method == 'CH' && empty(trim($check_number))){
            $error = array('status' => "Failed", "msg" => "Check Number Mandatory.");
            $this->response($this->json($error), 200);
        }
        
        // Input validations
        if (!empty($company_id) && !empty($reg_id) && !empty($payment_id) && !empty($credit_amount)) {
            $this->membership_model->updateMembershipPaymentDetailsAsPaid($company_id, $reg_id, $payment_id, $credit_amount, $credit_method, $check_number );
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function membershipHistoryNotes() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $reg_id = $student_id = $status = $history_id = $note_text = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['status'])) {
            $status = $this->_request['status'];
        }
        if (isset($this->_request['history_id'])) {
            $history_id = $this->_request['history_id'];
        }
        if (isset($this->_request['note_text'])) {
            $note_text = $this->_request['note_text'];
        }

        if ($status != 'add' && $status != 'update' && $status != 'delete') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } elseif (($status == 'delete' || $status == 'update') && empty($history_id)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } elseif (($status == 'add' || $status == 'update') && empty($note_text)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        // Input validations
        if (!empty($company_id) && !empty($reg_id) && !empty($status)) {
            $this->membership_model->membershipHistoryNoteDetails($company_id, $reg_id, $student_id, $status, $history_id, $note_text);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function editMembershipDatesForClasses() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $reg_id = $membership_structure = $membership_start_date = $membership_end_date = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['membership_structure'])) {
            $membership_structure = $this->_request['membership_structure'];
        }
        if (isset($this->_request['membership_start_date'])) {
            $membership_start_date = $this->_request['membership_start_date'];
        }
        if (isset($this->_request['membership_end_date'])) {
            $membership_end_date = $this->_request['membership_end_date'];
        }

        if (($membership_structure != 'NC' && $membership_structure != 'C' && $membership_structure != 'SE' ) || ($membership_structure == 'NC' && empty($membership_end_date))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        // Input validations
        if (!empty($company_id) && !empty($reg_id) && !empty($membership_structure) && !empty($membership_start_date)) {
            $this->membership_model->editMembershipDatesForClasses($company_id, $reg_id, $membership_structure, $membership_start_date, $membership_end_date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function editPaymentFromHistory() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $reg_id = $payment_id = $payment_date = '';
        $payment_amount = 0;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['payment_id'])) {
            $payment_id = $this->_request['payment_id'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['payment_date'])) {
            $payment_date = $this->_request['payment_date'];
        }if (isset($this->_request['category'])) {
            $category = $this->_request['category'];
        }

        // Input validations
        if (!empty($company_id) && !empty($reg_id) && !empty($payment_id) && !empty($payment_date)) {
            if ($category == 'membership' && !empty($payment_amount)) {
                $this->membership_model->editMembershipPaymentDetailsFromHistory($company_id, $reg_id, $payment_id, $payment_amount, $payment_date);
            } elseif ($category == 'events') {
                $this->event_model->editeventPaymentDetailsFromHistory($company_id, $reg_id, $payment_id, $payment_date);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function exportPaymentDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $payment_type = $category = $payment_days_type = $payment_startdate_limit = $payment_enddate_limit = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['category'])) {
            $category = $this->_request['category'];
        }
        if (isset($this->_request['payment_days_type'])) {
            $payment_days_type = $this->_request['payment_days_type'];
        }
        if (isset($this->_request['payment_startdate_limit'])) {
            $payment_startdate_limit = $this->_request['payment_startdate_limit'];
        }
        if (isset($this->_request['payment_enddate_limit'])) {
            $payment_enddate_limit = $this->_request['payment_enddate_limit'];
        }

        if (!empty($company_id) && !empty($payment_type) && !empty($category) && !empty($payment_days_type)) {
            $this->createCsvPaymentDetails($company_id, $payment_type, $category, $payment_days_type, $payment_startdate_limit, $payment_enddate_limit);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function sendPaymentHistory() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $mem_reg_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $mem_reg_id = $this->_request['membership_reg_id'];
        }
        if (isset($this->_request['category'])) {
            $category = $this->_request['category'];
        }
        if (!empty($company_id) && !empty($mem_reg_id) && !empty($category)) {
            if ($category == 'membership') {
                $this->membership_model->sendMembershipPaymentHistoryDetails($company_id, $mem_reg_id);
            } else if ($category == 'events') {
                $this->event_model->sendEventPaymentHistoryDetails($company_id, $mem_reg_id);
            }else if($category == 'trial'){
                $this->trial_model->sendTrialPaymentHistoryDetails($company_id, $mem_reg_id);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function exportStudentDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $search='';
        $select_all_flag='N';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['selected_student_list'])) {
            $selected_student_list = $this->_request['selected_student_list'];
        }
        if (isset($this->_request['select_all_flag'])) {
            $select_all_flag = $this->_request['select_all_flag'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        
        if (!empty($company_id)   && ((!empty($selected_student_list) && $select_all_flag == 'N') || $select_all_flag == 'Y')) {
            $this->createCsvStudentDetails($company_id,$selected_student_list,$select_all_flag,$search);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function sendStudentGroupPush() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $subject = $message = $type =$attached_file=$file_name=$search= $unsubscribe_email= '';
        $also_send_mail = 'N';
        $student_id = [];
        $select_all_flag = 'N';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['subject'])) {
            $subject = $this->_request['subject'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['selected_student_list'])) {
            $student_id = $this->_request['selected_student_list'];
        }
        if (isset($this->_request['also_send_mail'])) {
            $also_send_mail = $this->_request['also_send_mail'];
        }
        if (isset($this->_request['attached_file'])) {
            $attached_file = $this->_request['attached_file'];
        }
        if (isset($this->_request['file_name'])) {
            $file_name = $this->_request['file_name'];
        }
        if (isset($this->_request['select_all_flag'])) {
            $select_all_flag = $this->_request['select_all_flag'];
        }
        if(isset($this->_request['search'])){
            $search = $this->_request['search'];
        }
        if(isset($this->_request['unsubscribe_email'])){
            $unsubscribe_email = $this->_request['unsubscribe_email'];
        }//deepak
        if($type != 'subscribe') {
            if (!empty($company_id) && !empty($message) && !empty($type) && ((!empty($student_id) && $select_all_flag == 'N') || $select_all_flag == 'Y')) {
                if ($type == 'push') {
                    $this->sendGroupPushMessageForStudent($company_id, $subject, $message, $student_id, $also_send_mail, $attached_file, $file_name, $select_all_flag, $search);
                } elseif ($type == 'mail') {
                    $this->sendGroupPushMailForStudent($company_id, $subject, $message, $student_id, '', $attached_file, $file_name, $select_all_flag, $search);
                } else {
                    $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
           $this->activationlinkCreationForEmail($unsubscribe_email, $subject,  $company_id);
        }
    }

    private function sendPaymentIndividualPush() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $subject = $message = $type = $attached_file = $file_name = '';
        $also_send_mail = 'N';
        $selected_payment_list = [];

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['subject'])) {
            $subject = $this->_request['subject'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['selected_payment_list'])) {
            $selected_payment_list = $this->_request['selected_payment_list'];
        }
        if (isset($this->_request['also_send_mail'])) {
            $also_send_mail = $this->_request['also_send_mail'];
        }
        if (isset($this->_request['attached_file'])) {
            $attached_file = $this->_request['attached_file'];
        }
        if (isset($this->_request['file_name'])) {
            $file_name = $this->_request['file_name'];
        }
      if(isset($this->_request['unsubscribe_email'])){
            $unsubscribe_email = $this->_request['unsubscribe_email'];
        }//deepak
        
        if ($type != 'subscribe') {
            if (!empty($company_id) && !empty($message) && !empty($type) && !empty($selected_payment_list)) {
                if ($type == 'push') {
                    $this->sendIndividualPushMessageForPaymentUser($company_id, $subject, $message, $selected_payment_list, $also_send_mail, $attached_file, $file_name);
                } elseif ($type == 'mail') {
                    $this->sendIndividualPushMailForPaymentUser($company_id, $subject, $message, $selected_payment_list, '', $attached_file, $file_name);
                } else {
                    $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $this->activationlinkCreationForEmail($unsubscribe_email, $subject, $company_id);
        }
    }

    private function sendEventGroupPush() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $subject = $message = $type =$attached_file=$file_name=$event_id= $from=$status=$search='';
        $also_send_mail = 'N';
        $event_reg_id = [];
        $all_select_flag = 'N';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['subject'])) {
            $subject = $this->_request['subject'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['selected_event_list'])) {
            $event_reg_id = $this->_request['selected_event_list'];
        }
        if (isset($this->_request['also_send_mail'])) {
            $also_send_mail = $this->_request['also_send_mail'];
        }
        if (isset($this->_request['attached_file'])) {
            $attached_file = $this->_request['attached_file'];
        }
        if (isset($this->_request['file_name'])) {
            $file_name = $this->_request['file_name'];
        }
        if (isset($this->_request['all_select_flag'])) {
            $all_select_flag = $this->_request['all_select_flag'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['from'])) {
            $from = $this->_request['from'];
        }
        if (isset($this->_request['status'])) {
            $status = $this->_request['status'];
        }
        if(isset($this->_request['search'])){
            $search = $this->_request['search'];
        }
        if(isset($this->_request['unsubscribe_email'])){
            $unsubscribe_email = $this->_request['unsubscribe_email'];
        } 
        
        if ($type != 'subscribe') {
            if (!empty($company_id) && !empty($message) && !empty($type) && ((!empty($event_reg_id) && $all_select_flag == 'N') || $all_select_flag == 'Y')) {
                if ($type == 'push') {
                    $this->event_model->sendGroupPushMessageForEvent($company_id, $subject, $message, $event_reg_id, $also_send_mail, $attached_file, $file_name, $all_select_flag, $event_id, $status, $search);
                } elseif ($type == 'mail') {
                    $this->event_model->sendGroupPushMailForEvent($company_id, $subject, $message, $event_reg_id, '', $attached_file, $file_name, $all_select_flag, $event_id, $status, $search);
                } else {
                    $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $this->activationlinkCreationForEmail($unsubscribe_email, $subject, $company_id);
        }
    }

    private function sendCompletePush() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $membership_reg_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_reg_id'])) {
            $membership_reg_id = $this->_request['membership_reg_id'];
        }
        if (!empty($company_id) && !empty($membership_reg_id)) {
            $this->membership_model->sendReceiptForMembershipCompletion($company_id, $membership_reg_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    //event history 
    private function getEventHistory() {

        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $event_reg_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_reg_id'])) {
            $event_reg_id = $this->_request['event_reg_id'];
        }
        if (!empty($company_id) && !empty($event_reg_id)) {
            $this->event_model->getEventHistoryModal($company_id, $event_reg_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function EventHistoryNotes() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $reg_id = $student_id = $status = $history_id = $note_text = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $event_reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['status'])) {
            $status = $this->_request['status'];
        }
        if (isset($this->_request['history_id'])) {
            $history_id = $this->_request['history_id'];
        }
        if (isset($this->_request['note_text'])) {
            $note_text = $this->_request['note_text'];
        }

        if ($status != 'add' && $status != 'update' && $status != 'delete') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } elseif (($status == 'delete' || $status == 'update') && empty($history_id)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } elseif (($status == 'add' || $status == 'update') && empty($note_text)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        // Input validations
        if (!empty($company_id) && !empty($event_reg_id) && !empty($status)) {
            $this->event_model->addorupdateEventHistoryNote($company_id, $event_reg_id, $student_id, $status, $history_id, $note_text);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    //event history-end



    private function getEventParticipantdetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $reg_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $event_reg_id = $this->_request['reg_id'];
        }
        if (!empty($company_id) && !empty($event_reg_id)) {
            $this->event_model->geteventParticipantinfo($company_id, $event_reg_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateEventParticipantName() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_reg_id = $pfirst_name = $plast_name = $buyer_first_name = $buyer_last_name = $buyer_email = '';
        $event_field_3 = $event_field_4 = $event_field_5 = $event_field_6 = $event_field_7 = $event_field_8 = $event_field_9 = $event_field_10 = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $event_reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['pfirst_name'])) {
            $pfirst_name = $this->_request['pfirst_name'];
        }
        if (isset($this->_request['plast_name'])) {
            $plast_name = $this->_request['plast_name'];
        }
        if (isset($this->_request['bfirst_name'])) {
            $buyer_first_name = $this->_request['bfirst_name'];
        }
        if (isset($this->_request['blast_name'])) {
            $buyer_last_name = $this->_request['blast_name'];
        }
        if (isset($this->_request['buyer_email'])) {
            $buyer_email = $this->_request['buyer_email'];
        }
        if (isset($this->_request['buyer_phone'])) {
            $buyer_phone = $this->_request['buyer_phone'];
        }

        if (isset($this->_request['event_field_3'])) {
            $event_field_3 = $this->_request['event_field_3'];
        }
        if (isset($this->_request['event_field_4'])) {
            $event_field_4 = $this->_request['event_field_4'];
        }
        if (isset($this->_request['event_field_5'])) {
            $event_field_5 = $this->_request['event_field_5'];
        }
        if (isset($this->_request['event_field_6'])) {
            $event_field_6 = $this->_request['event_field_6'];
        }
        if (isset($this->_request['event_field_7'])) {
            $event_field_7 = $this->_request['event_field_7'];
        }
        if (isset($this->_request['event_field_8'])) {
            $event_field_8 = $this->_request['event_field_8'];
        }
        if (isset($this->_request['event_field_9'])) {
            $event_field_9 = $this->_request['event_field_9'];
        }
        if (isset($this->_request['event_field_10'])) {
            $event_field_10 = $this->_request['event_field_10'];
        }

        if (!empty($company_id) && !empty($event_reg_id) && !empty($buyer_first_name) && !empty($buyer_last_name)) {
            $this->event_model->updateParticipantDetails($company_id, $event_reg_id, $pfirst_name, $plast_name, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $event_field_3, $event_field_4, $event_field_5, $event_field_6, $event_field_7, $event_field_8, $event_field_9, $event_field_10);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function membershipExcludeDays() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_billing_exclude_id = $membership_option_id = $status = $exclude_start_date = $exclude_end_date = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['membership_billing_exclude_id'])) {
            $membership_billing_exclude_id = $this->_request['membership_billing_exclude_id'];
        }
        if (isset($this->_request['status'])) {
            $status = $this->_request['status'];
        }
        if (isset($this->_request['exclude_start_date'])) {
            $exclude_start_date = $this->_request['exclude_start_date'];
        }
        if (isset($this->_request['exclude_end_date'])) {
            $exclude_end_date = $this->_request['exclude_end_date'];
        }

        if ($status != 'add' && $status != 'update' && $status != 'delete') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } elseif (($status == 'delete' || $status == 'update') && empty($membership_billing_exclude_id)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } elseif (($status == 'add' || $status == 'update') && empty($exclude_start_date) && empty($exclude_end_date)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        // Input validations
        if (!empty($company_id) && !empty($membership_id) && !empty($membership_option_id) && !empty($status)) {
            $this->membership_model->membershipExcludeDaysDetails($company_id, $membership_id, $membership_option_id, $membership_billing_exclude_id, $status, $exclude_start_date, $exclude_end_date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getAllRankDetails() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }

        if (!empty($company_id)) {
            $this->membership_model->getAllRankDetailsForStudio($company_id, $membership_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function registerStudioInSalesforce() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $company_name = $email = $phone_number = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['company_name'])) {
            $company_name = $this->_request['company_name'];
        }
        if (isset($this->_request['email'])) {
            $email = $this->_request['email'];
        }
        if (isset($this->_request['phone_number'])) {
            $phone_number = $this->_request['phone_number'];
        }

        if (!empty($company_id) && !empty($company_name) && !empty($email) && !empty($phone_number)) {
            $this->registerCompanyinSalesforceApi($company_id, $company_name, $email, $phone_number);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateStudioInSalesforce() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->updateCompanyDetailsinSalesforceApi($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function getAttendanceDetail() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $reg_id = $company_id = $category = '';
        $callback = 0;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['category'])) {
            $category = $this->_request['category'];
        }
        if (!empty($company_id) && !empty($reg_id) && (!empty($category)&&$category =='M')) {
            $this->membership_model->getAttendanceDetails($company_id, $reg_id, $callback);
        }else if (!empty($company_id) && !empty($reg_id) && (!empty($category)&&$category =='T')) {
            $this->trial_model->getTrialAttendanceDetails($company_id, $reg_id, $callback);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function deleteorinsertAttendance() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $reg_id = $company_id = $category = $flag = $attendance_id = $attendance_date_time = '';
        $override_flag = 'N';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['category'])) {
            $category = $this->_request['category'];
        }
        if (isset($this->_request['flag'])) {
            $flag = $this->_request['flag'];
        }
        if (isset($this->_request['attendance_id'])) {
            $attendance_id = $this->_request['attendance_id'];
        }
        if (isset($this->_request['attendance_date_time'])) {
            $attendance_date_time = $this->_request['attendance_date_time'];
        }
        if (isset($this->_request['override_flag'])) {
            $override_flag = $this->_request['override_flag'];
        }

        if (!empty($company_id) && !empty($flag) && ($flag == 'A' || ($flag == 'D' && !empty($attendance_id)))&&(!empty($category)&&$category =='M')) {
            $this->membership_model->deleteOrAddAttendanceDetails($company_id, $reg_id, $flag, $attendance_id, $attendance_date_time, $override_flag);
        }else if (!empty($company_id) && !empty($flag) && ($flag == 'A' || ($flag == 'D' && !empty($attendance_id))) && (!empty($category)&&$category =='T')) {
            $this->trial_model->deleteOrAddTrialAttendanceDetails($company_id, $reg_id, $flag, $attendance_id, $attendance_date_time, $override_flag);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getAttendanceDateOverrideValidation(){            
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $reg_id = $selected_date = '';
        $call_back = 0;
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['selected_date'])) {
            $selected_date = $this->_request['selected_date'];
        }

        if (!empty($company_id) && !empty($reg_id) && !empty($selected_date)) {
            $this->membership_model->getAttendanceOverrideValidation($company_id, $reg_id, "'$selected_date'", $call_back);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getAttendanceDashboard(){            
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->getAttendanceDashboardDetails($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function updateAttendanceReportFrequency(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $attendance_report_flag = $attendance_report_frequency = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['attendance_report_flag'])) {
            $attendance_report_flag = $this->_request['attendance_report_flag'];
        }
        if (isset($this->_request['attendance_report_frequency'])) {
            $attendance_report_frequency = $this->_request['attendance_report_frequency'];
        }

        if (!empty($company_id) && !empty($attendance_report_flag) && !empty($attendance_report_frequency)) {
            $this->updateAttendanceReportFrequencyDetails($company_id, $attendance_report_flag, $attendance_report_frequency);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function downloadAttendanceReportCSV(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->downloadAttendanceReportAsCSV($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function updateMembershipHistoryinDB() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $membership_registration_id = $company_id = $type = $cancelled_date = '';
        $callback = 0;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['selected_membership_id'])) {
            $membership_registration_id = $this->_request['selected_membership_id'];
        }
        if (isset($this->_request['history_id'])) {
            $history_id = $this->_request['history_id'];
        }
        if (isset($this->_request['cancellation_date'])) {
            $cancelled_date = $this->_request['cancellation_date'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (!empty($company_id) && !empty($membership_registration_id) && !empty($history_id) && (!empty($type) && ($type == 'edit_cancellation' || $type == 'delete_cancellation' ))) {
            $this->membership_model->updateScheduledCancellationDateinDB($company_id, $membership_registration_id, $history_id, $cancelled_date, $type);
        } elseif (!empty($company_id) && !empty($membership_registration_id) && !empty($history_id) && (!empty($type) && ($type == 'delete_hold' ))) {
            $this->membership_model->deleteScheduledHoldResumeDateinDB($company_id, $membership_registration_id, $history_id, $type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 200);
        }
    }

    private function addTrialCategory() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $trial_status = $trial_title = $trial_subtitle = $trial_banner_img_type = $trial_banner_img_content = $trial_welcome_url = $trial_description = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_category_type'])) {
            $trial_status = $this->_request['trial_category_type'];
        }
        if (isset($this->_request['trial_category_title'])) {
            $trial_title = $this->_request['trial_category_title'];
        }
        if (isset($this->_request['trial_subcategory_title'])) {
            $trial_subtitle = $this->_request['trial_subcategory_title'];
        }
        if (isset($this->_request['trial_banner_img_type'])) {
            $trial_banner_img_type = $this->_request['trial_banner_img_type'];
        }
        if (isset($this->_request['trial_category_image'])) {
            $trial_banner_img_content = $this->_request['trial_category_image'];
        }
        if (isset($this->_request['trial_conversion_url'])) {
            $trial_welcome_url = $this->_request['trial_conversion_url'];
        }
        if (isset($this->_request['trial_category_desc'])) {
            $trial_description = $this->_request['trial_category_desc'];
        }

        // Input validations
        if (!empty($company_id) && !empty($trial_title) && !empty($trial_status)) {
            $this->trial_model->addTrialProgramDetails($company_id, $trial_status, $trial_title, $trial_subtitle,  $trial_banner_img_content, $trial_welcome_url, $trial_description,$trial_banner_img_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
     private function updateTrialPaymentDetails() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_price = $trial_processingfeetype = $trial_programlengthperiod = $trial_programlengthquantity  = '';
//        $payment_startdate_type = 4;
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
//        if(isset($this->_request['event_capacity']))
//            $event_capacity = $this->_request['event_capacity'];
//        if(isset($this->_request['capacity_text']))
//            $capacity_text = $this->_request['capacity_text'];
        if (isset($this->_request['trial_price'])) {
            $trial_price = $this->_request['trial_price'];
        }
        if (isset($this->_request['trial_processingfeetype'])) {
            $trial_processingfeetype = $this->_request['trial_processingfeetype'];
        }
        if (isset($this->_request['trial_programlengthperiod'])) {
            $trial_programlengthperiod = $this->_request['trial_programlengthperiod'];
        }
        if (isset($this->_request['trial_programlengthquantity'])) {
            $trial_programlengthquantity = $this->_request['trial_programlengthquantity'];
        }
//      

        // Input validations
        if (!empty($trial_id) && !empty($company_id)) {
            $this->trial_model->updateTrialPaymentDetailsInDB($company_id, $trial_id, $trial_price, $trial_processingfeetype, $trial_programlengthperiod, $trial_programlengthquantity );
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
   private function updateTrialRegistration() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_column_name = $trial_column_value = $trial_mandatory_column_name = $trial_mandatory_column_value = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['trial_column_name'])) {
            $trial_column_name = $this->_request['trial_column_name'];
        }
        if (isset($this->_request['trial_column_value'])) {
            $trial_column_value = $this->_request['trial_column_value'];
        }
        if (isset($this->_request['trial_mandatory_column_name'])) {
            $trial_mandatory_column_name = $this->_request['trial_mandatory_column_name'];
        }
        if (isset($this->_request['trial_mandatory_column_value'])) {
            $trial_mandatory_column_value = $this->_request['trial_mandatory_column_value'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($trial_id) && !empty($company_id) && !empty($trial_column_name) && !empty($trial_column_value)) {
            $this->trial_model->updateTrialRegistrationDetailsInDB($company_id, $trial_id, $trial_column_name, $trial_column_value, $trial_mandatory_column_name, $trial_mandatory_column_value, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
     private function deleteTrialRegistration() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_column_name = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['trial_column_name'])) {
            $trial_column_name = $this->_request['trial_column_name'];
        }

        // Input validations
        if (!empty($trial_id) && !empty($company_id) && !empty($trial_column_name)) {
            $this->trial_model->deleteTrialRegistrationDetailsInDB($company_id, $trial_id, $trial_column_name, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function TrialRegistrationSorting() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $old_location_id = $new_location_id = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['old_location_id'])) {
            $old_location_id = $this->_request['old_location_id'];
        }
        if (isset($this->_request['new_location_id'])) {
            $new_location_id = $this->_request['new_location_id'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($trial_id) && !empty($company_id) && !empty($old_location_id) && !empty($new_location_id)) {
            $this->trial_model->trialRegistrationSortingDetailsInDB($company_id, $trial_id, $old_location_id, $new_location_id, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function updateTrialLeadSource() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_leadsource_name = $trial_leadsource_value =  $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['trial_leadsource_name'])) {
            $trial_leadsource_name = $this->_request['trial_leadsource_name'];
        }
        if (isset($this->_request['trial_leadsource_value'])) {
            $trial_leadsource_value = $this->_request['trial_leadsource_value'];
        }
        
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($trial_id) && !empty($company_id) && !empty($trial_leadsource_name) && !empty($trial_leadsource_value)) {
            $this->trial_model->updateTrialLeadSourceDetailsInDB($company_id, $trial_id, $trial_leadsource_name, $trial_leadsource_value,  $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
     private function deleteTrialLeadSource() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_leadsource_name = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['trial_leadsource_name'])) {
            $trial_leadsource_name = $this->_request['trial_leadsource_name'];
        }

        // Input validations
        if (!empty($trial_id) && !empty($company_id) && !empty($trial_leadsource_name)) {
            $this->trial_model->deleteTrialLeadSourceDetailsInDB($company_id, $trial_id, $trial_leadsource_name, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function TrialLeadsourceSorting() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $old_location_id = $new_location_id = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['old_location_id'])) {
            $old_location_id = $this->_request['old_location_id'];
        }
        if (isset($this->_request['new_location_id'])) {
            $new_location_id = $this->_request['new_location_id'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($trial_id) && !empty($company_id) && !empty($old_location_id) && !empty($new_location_id)) {
            $this->trial_model->trialLeadsourceSortingDetailsInDB($company_id, $trial_id, $old_location_id, $new_location_id, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function updateTrialWaiver() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $waiver_policies = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['waiver_policies'])) {
            $waiver_policies = $this->_request['waiver_policies'];
        }

        // Input validations
        if (!empty($trial_id) && !empty($company_id) && !empty($waiver_policies)) {
            $this->trial_model->updateTrialWaiverDetails($company_id, $trial_id, $waiver_policies);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function addtrialdiscount() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $trial_id = $discount_type = $discount_code = $discount_amount = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
       if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['discount_type'])) {
            $discount_type = $this->_request['discount_type'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['discount_amount'])) {
            $discount_amount = $this->_request['discount_amount'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($trial_id) && !empty($company_id) && !empty($discount_type) && !empty($discount_code) && !empty($discount_amount)) {
            $this->trial_model->inserttrialdiscount($company_id, $trial_id, $discount_type, $discount_code, $discount_amount, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function updatetrialdiscount() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $discount_type = $discount_code = $discount_amount = $trial_discount_id = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['trial_discount_id'])) {
            $trial_discount_id = $this->_request['trial_discount_id'];
        }
        if (isset($this->_request['discount_type'])) {
            $discount_type = $this->_request['discount_type'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['discount_amount'])) {
            $discount_amount = $this->_request['discount_amount'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($trial_id) && !empty($company_id) && !empty($discount_type) && !empty($discount_code) && !empty($discount_amount) && !empty($trial_discount_id)) {
            $this->trial_model->updatetrialDiscountInDB($company_id, $trial_id, $trial_discount_id, $discount_type, $discount_code, $discount_amount, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
     private function deletetrialDiscount() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_discount_id = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['trial_discount_id'])) {
            $trial_discount_id = $this->_request['trial_discount_id'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($trial_id) && !empty($company_id) && !empty($trial_discount_id)) {
            $this->trial_model->deletetrialDiscountInDB($company_id, $trial_id, $trial_discount_id, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
     private function updatetrialcategory() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_status = $trial_title = $trial_subtitle = $trial_banner_img_type = $trial_banner_img_content = $trial_welcome_url = $trial_description = $trial_image_update=$old_trial_banner_img_url='';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['trial_category_type'])) {
            $trial_status = $this->_request['trial_category_type'];
        }
        if (isset($this->_request['trial_category_title'])) {
            $trial_title = $this->_request['trial_category_title'];
        }
        if (isset($this->_request['trial_subcategory_title'])) {
            $trial_subtitle = $this->_request['trial_subcategory_title'];
        }
        if (isset($this->_request['trial_banner_img_type'])) {
            $trial_banner_img_type = $this->_request['trial_banner_img_type'];
        }
        if (isset($this->_request['trial_category_image'])) {
            $trial_banner_img_content = $this->_request['trial_category_image'];
        }
        if (isset($this->_request['trial_conversion_url'])) {
            $trial_welcome_url = $this->_request['trial_conversion_url'];
        }
        if (isset($this->_request['trial_category_desc'])) {
            $trial_description = $this->_request['trial_category_desc'];
        }
        if (isset($this->_request['trial_image_update'])) {
            $trial_image_update = $this->_request['trial_image_update'];
        }   
        if (isset($this->_request['old_trial_banner_img_url'])) {
            $old_trial_banner_img_url = $this->_request['old_trial_banner_img_url'];
        }
         $trial_banner_img_url = "";
        if ($trial_image_update === 'Y' && $trial_banner_img_content != '') {
             $timestamp = time();
            $cfolder_name = "Company_$company_id";
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name";
                chdir($dir_name);
                if (!file_exists($cfolder_name)) {
                    $oldmask = umask(0);
                    mkdir($cfolder_name, 0777, true);
                    umask($oldmask);
                }
                chdir($cfolder_name);
                $folder_name = "Trial";
                if (!file_exists($folder_name)) {
                    $oldmask = umask(0);
                    mkdir($folder_name, 0777, true);
                    umask($oldmask);
                }
                chdir($folder_name);
                $image_content = base64_decode($trial_banner_img_content);
                $file_name = $company_id . "-" . $trial_id."[".$timestamp."]";
                $file = "$file_name.$trial_banner_img_type";
                $img = $file; // logo - your file name
                $ifp = fopen($img, "wb");
                fwrite($ifp, $image_content);
                fclose($ifp);
                chdir($old); // Restore the old working directory
//            $event_banner_img_url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//            $event_banner_img_url = implode('/', explode('/', $event_banner_img_url, -2));
            $trial_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/$cfolder_name/$folder_name/$file";
        } elseif ($trial_image_update === 'Y' && $trial_banner_img_content == '') {
            $trial_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/Default/default.png";
        }

        // Input validations

        if (!empty($trial_title) && !empty($company_id) && !empty($trial_status) && !empty($trial_id)) {
                 $this->trial_model->updatetrialDetails($trial_id, $company_id, $trial_status, $trial_title, $trial_subtitle,  $trial_banner_img_content, $trial_welcome_url, $trial_description,$old_trial_banner_img_url,$trial_banner_img_url,$trial_image_update);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function gettrialdetails() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        $list_type = 'P';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if(isset($this->_request['list_type'])){
            $list_type = $this->_request['list_type'];
        }

        if (!empty($company_id) && !empty($list_type)) {
            $this->trial_model->getTrialDetailByCompany($company_id, $list_type, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function deleteTrialCategory() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $trial_id = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($company_id) && !empty($trial_id)) {
            $this->trial_model->deleteTrial($company_id, $trial_id, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function gettrialparticipants() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $data_limit = 0;
        $company_id = $trial_id = $trial_status_tab = '';
        $search = $draw_table = $length_table = $start = $sorting = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }

        if (isset($this->_request['trial_status_tab'])) {
            $trial_status_tab = $this->_request['trial_status_tab'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = $this->_request['start'];
        }

        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }



        if ($trial_status_tab != 'A' && $trial_status_tab != 'E' && $trial_status_tab != 'C' && $trial_status_tab != 'D') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }

        if (!empty($company_id) && !empty($trial_id) && !empty($trial_status_tab)) {
            $this->trial_model->gettrialParticipantDetails($company_id, $trial_id, $trial_status_tab, $search, $draw_table, $length_table, $start, $sorting);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
     private function wepayTrialprogramCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_name = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = $program_length_type = $payment_type =$start_date='';
        $student_id = $participant_id = $fee = $discount = $registration_amount = $payment_amount =  $program_length = 0;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $upgrade_status = 'F';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        $country = 'US';
        $reg_type_user = 'P';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = $version;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['trial_name'])) {
            $trial_name = $this->_request['trial_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['cc_id'])) {
            $cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_state'])) {
            $cc_state = $this->_request['cc_state'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
         if (isset($this->_request['payment_startdate'])) {
            $start_date = $this->_request['payment_startdate'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['lead_source'])) {
            $reg_col4 = $this->_request['lead_source'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['program_length_type'])) {
            $program_length_type = $this->_request['program_length_type'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['program_length'])) {
            $program_length = $this->_request['program_length'];
        }
        if (isset($this->_request['trial_participant_Streetaddress'])) {
            $participant_street = $this->_request['trial_participant_Streetaddress'];
        }
        if (isset($this->_request['trial_participant_City'])) {
            $participant_city = $this->_request['trial_participant_City'];
        }
        if (isset($this->_request['trial_participant_State'])) {
            $participant_state = $this->_request['trial_participant_State'];
        }
        if (isset($this->_request['trial_participant_Zip'])) {
            $participant_zip = $this->_request['trial_participant_Zip'];
        }
        if (isset($this->_request['trial_participant_Country'])) {
            $participant_country = $this->_request['trial_participant_Country'];
        }
       
       if (!empty($company_id) && !empty($trial_id) && !empty($trial_name) &&  (!empty($cc_id) || $payment_amount == 0) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
                $this->trial_model->trialProgramCheckout($company_id, $trial_id, $student_id, $participant_id, $trial_name, $desc,  $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount,  $processing_fee_type, $discount,  $payment_type, $upgrade_status,  $reg_type_user, $reg_version_user,$program_length,$program_length_type,$start_date,$participant_street, $participant_city, $participant_state, $participant_zip, $participant_country);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 400);
            }
        
    }
    
    private function copytrialprogram() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $trial_id = $trial_template_flag = $list_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['trial_template_flag'])) {
            $trial_template_flag = $this->_request['trial_template_flag'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        // Input validations
        if (!empty($company_id) && !empty($trial_id) &&  !empty($trial_template_flag)) {
            $this->trial_model->copyTrial($company_id, $trial_id, $trial_template_flag, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function exporttrialdata() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id  = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
         if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (!empty($company_id) && !empty($trial_id)) {
            $this->trial_model->createtrialCSV($company_id, $trial_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
        
        
    }
    
    private function getTrialprogramTemplate() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $list_type = 'P';
        $this->trial_model->getTrialDetailByCompany("6",$list_type,0);
    }
    
    private function updateTrialCategorySortOrder() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id =$trial_id =$trial_sort_order ='';
        $list_type = 'P';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['sort_id'])) {
            $trial_sort_order = $this->_request['sort_id'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        if (!empty($company_id) && !empty($trial_id) && !empty($trial_sort_order)) {
            $this->trial_model->updatetrialSortingDetails($trial_sort_order, $company_id, $trial_id, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
     private function updateTrialParticipantStatus() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_reg_id = $trial_status = $next_date = $student_id='';
        

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['trial_reg_id'])) {
            $trial_reg_id = $this->_request['trial_reg_id'];
        }
         if (isset($this->_request['trial_status'])) {
            $trial_status = $this->_request['trial_status'];
        }
        if (isset($this->_request['next_date'])) {
            $next_date = $this->_request['next_date'];
        }
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (!empty($company_id) && !empty($trial_id) && !empty($trial_reg_id) && !empty($trial_status)) {
            $this->trial_model->updateTrialParticipantStatusInDb($company_id, $student_id, $trial_id, $trial_reg_id,  $trial_status, $next_date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function trialRegDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $trial_reg_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_reg_id'])) {
            $trial_reg_id = $this->_request['trial_reg_id'];
        }
        if (!empty($company_id) && !empty($trial_reg_id)) {
            $this->trial_model->getTrialRegDetails($company_id, $trial_reg_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }

     private function trialparticipantInfoDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
}
        $company_id = $trial_reg_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_reg_id'])) {
            $trial_reg_id = $this->_request['trial_reg_id'];
        }
        if (!empty($company_id) && !empty($trial_reg_id)) {
            $this->trial_model->getparticipantInfoDetails($company_id, $trial_reg_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }

    private function trialHistoryDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $trial_reg_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_reg_id'])) {
            $trial_reg_id = $this->_request['trial_reg_id'];
        }
        if (!empty($company_id) && !empty($trial_reg_id)) {
            $this->trial_model->getTrialHistoryDetails($company_id, $trial_reg_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
      private function sendTrialGroupPush() {//kumar
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $subject = $message = $type = $attached_file = $file_name = $trial_startdate_limit = $trial_enddate_limit = $trial_days_type ='';
        $also_send_mail = 'N';
        $trial_reg_id = [];
        $all_select_flag = 'N';
        $trial_status = $trial_id = $from=$search = $trial_id='';
        
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['subject'])) {
            $subject = $this->_request['subject'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['selected_trial_list'])) {
            $trial_reg_id = $this->_request['selected_trial_list'];
        }
        if (isset($this->_request['also_send_mail'])) {
            $also_send_mail = $this->_request['also_send_mail'];
        }
        if (isset($this->_request['attached_file'])) {
            $attached_file = $this->_request['attached_file'];
        }
        if (isset($this->_request['file_name'])) {
            $file_name = $this->_request['file_name'];
        }
        if (isset($this->_request['all_select_flag'])) {
            $all_select_flag = $this->_request['all_select_flag'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['trial_status'])) {
            $trial_status = $this->_request['trial_status'];
        }
       if (isset($this->_request['from'])) {
            $from = $this->_request['from'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['trial_startdate_limit'])) {
            $trial_startdate_limit = $this->_request['trial_startdate_limit'];
        }
        if (isset($this->_request['trial_enddate_limit'])) {
            $trial_enddate_limit = $this->_request['trial_enddate_limit'];
        }
        if (isset($this->_request['trial_days_type'])) {
            $trial_days_type = $this->_request['trial_days_type'];
        }
        if(isset($this->_request['unsubscribe_email'])){
            $unsubscribe_email = $this->_request['unsubscribe_email'];
        }//deepak
        if($type != 'subscribe') {
        if(!empty($company_id) && !empty($message) && !empty($type)  && ((!empty($trial_reg_id) && $all_select_flag == 'N') || $all_select_flag == 'Y')  && !empty($trial_status)){
            if($type=='push'){
                $this->trial_model->sendGroupPushMessageForTrial($company_id, $subject, $message, $trial_reg_id, $also_send_mail, $attached_file, $file_name, $trial_status, $all_select_flag,$search,$trial_id,$trial_startdate_limit,$trial_enddate_limit,$trial_days_type);
            }elseif($type=='mail'){
                $this->trial_model->sendGroupPushMailForTrial($company_id, $subject, $message, $trial_reg_id,'',$attached_file,$file_name, $trial_status, $all_select_flag,$search,$trial_id,$trial_startdate_limit,$trial_enddate_limit,$trial_days_type);
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 400);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
        }else{
            $this->activationlinkCreationForEmail($unsubscribe_email, $subject,  $company_id);
        }
    }
    
    private function createTrialSelectonCSV() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
         $company_id = $type = $trial_id = $attached_file = $file_name = $trial_startdate_limit = $trial_enddate_limit = $trial_days_type ='';
        
        $trial_reg_id = [];
        $all_select_flag = 'N';
        $trial_status = $trial_id = $from=$search='';
        
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
       if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['selected_trial_list'])) {
            $trial_reg_id = $this->_request['selected_trial_list'];
        }
       
        if (isset($this->_request['attached_file'])) {
            $attached_file = $this->_request['attached_file'];
        }
        if (isset($this->_request['file_name'])) {
            $file_name = $this->_request['file_name'];
        }
        if (isset($this->_request['all_select_flag'])) {
            $all_select_flag = $this->_request['all_select_flag'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['trial_status'])) {
            $trial_status = $this->_request['trial_status'];
        }
       if (isset($this->_request['from'])) {
            $from = $this->_request['from'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['trial_startdate_limit'])) {
            $trial_startdate_limit = $this->_request['trial_startdate_limit'];
        }
        if (isset($this->_request['trial_enddate_limit'])) {
            $trial_enddate_limit = $this->_request['trial_enddate_limit'];
        }
        if (isset($this->_request['trial_days_type'])) {
            $trial_days_type = $this->_request['trial_days_type'];
        }
        
        if(!empty($company_id) && ((!empty($trial_reg_id) && $all_select_flag == 'N') || $all_select_flag == 'Y')){
            $this->trial_model->createTrialSelectionCSV($company_id,$trial_reg_id, $trial_status, $all_select_flag,$search,$trial_id,$trial_startdate_limit,$trial_enddate_limit,$trial_days_type);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
       private function TrialHistoryNotes() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_reg_id = $student_id = $status = $history_id = $note_text = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_reg_id'])) {
            $trial_reg_id = $this->_request['trial_reg_id'];
        }
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['status'])) {
            $status = $this->_request['status'];
        }
        if (isset($this->_request['history_id'])) {
            $history_id = $this->_request['history_id'];
        }
        if (isset($this->_request['note_text'])) {
            $note_text = $this->_request['note_text'];
        }

        if ($status != 'add' && $status != 'update' && $status != 'delete') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } elseif (($status == 'delete' || $status == 'update') && empty($history_id)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } elseif (($status == 'add' || $status == 'update') && empty($note_text)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        // Input validations
        if (!empty($company_id) && !empty($trial_reg_id) && !empty($status)) {
            $this->trial_model->addorupdateTrialHistoryNote($company_id, $trial_reg_id, $student_id, $status, $history_id, $note_text);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function updateTrialParticipantName() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_reg_id = $pfirst_name = $plast_name = $buyer_name = $buyer_email = $buyer_first_name = $buyer_last_name = '';
        $dob = $trial_field_5 = $trial_field_6 = $trial_field_7 = $trial_field_8 = $trial_field_9 = $trial_field_10 = '';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $trial_reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['pfirst_name'])) {
            $pfirst_name = $this->_request['pfirst_name'];
        }
        if (isset($this->_request['plast_name'])) {
            $plast_name = $this->_request['plast_name'];
        }
        if (isset($this->_request['bfirst_name'])) {
            $buyer_first_name = $this->_request['bfirst_name'];
        }
        if (isset($this->_request['blast_name'])) {
            $buyer_last_name = $this->_request['blast_name'];
        }
        if (isset($this->_request['buyer_email'])) {
            $buyer_email = $this->_request['buyer_email'];
        }
        if (isset($this->_request['buyer_phone'])) {
            $buyer_phone = $this->_request['buyer_phone'];
        }

        if (isset($this->_request['date_of_birth'])) {
            $dob = $this->_request['date_of_birth'];
        }
        if (isset($this->_request['trial_field_5'])) {
            $trial_field_5 = $this->_request['trial_field_5'];
        }
        if (isset($this->_request['trial_field_6'])) {
            $trial_field_6 = $this->_request['trial_field_6'];
        }
        if (isset($this->_request['trial_field_7'])) {
            $trial_field_7 = $this->_request['trial_field_7'];
        }
        if (isset($this->_request['trial_field_8'])) {
            $trial_field_8 = $this->_request['trial_field_8'];
        }
        if (isset($this->_request['trial_field_9'])) {
            $trial_field_9 = $this->_request['trial_field_9'];
        }
        if (isset($this->_request['trial_field_10'])) {
            $trial_field_10 = $this->_request['trial_field_10'];
        }
        if (isset($this->_request['trial_participant_street'])) {
            $participant_street = $this->_request['trial_participant_street'];
        }
        if (isset($this->_request['trial_participant_city'])) {
            $participant_city = $this->_request['trial_participant_city'];
        }
        if (isset($this->_request['trial_participant_state'])) {
            $participant_state = $this->_request['trial_participant_state'];
        }
        if (isset($this->_request['trial_participant_zip'])) {
            $participant_zip = $this->_request['trial_participant_zip'];
        }
        if (isset($this->_request['trial_participant_country'])) {
            $participant_country = $this->_request['trial_participant_country'];
        }

        if (!empty($company_id) && !empty($trial_reg_id) && !empty($buyer_first_name) && !empty($buyer_last_name)) {
            $this->trial_model->updateParticipantDetails($company_id, $trial_reg_id, $pfirst_name, $plast_name, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $dob, $trial_field_5, $trial_field_6, $trial_field_7, $trial_field_8, $trial_field_9, $trial_field_10,$participant_street, $participant_city, $participant_state, $participant_zip, $participant_country);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
     private function trialcustomers() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $data_limit = 0;
        $company_id = $trial_id = $trial_status_tab = $trial_startdate_limit= $trial_enddate_limit='';
        $search = $draw_table = $length_table = $start = $sorting = $trial_days_type='';
        $register_type='1';
        $notattendeddays=$notattendeddays1='';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
//          if (isset($this->_request['trial_id'])) {
//            $trial_id = $this->_request['trial_id'];
//        }

        if (isset($this->_request['trial_startdate_limit'])) {
            $trial_startdate_limit = $this->_request['trial_startdate_limit'];
        }
        if (isset($this->_request['trial_enddate_limit'])) {
            $trial_enddate_limit = $this->_request['trial_enddate_limit'];
        }
        if (isset($this->_request['trial_days_type'])) {
            $trial_days_type = $this->_request['trial_days_type'];
        }
        if (isset($this->_request['trial_status_tab'])) {
            $trial_status_tab = $this->_request['trial_status_tab'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = $this->_request['start'];
        }

        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }
        if (isset($this->_request['trialregister_type'])) {
            $register_type = $this->_request['trialregister_type'];
        }
        if (isset($this->_request['trialnotattendeddays'])) {
            $notattendeddays = $this->_request['trialnotattendeddays'];
        }
        if (isset($this->_request['trialnotattendeddays1'])) {
            $notattendeddays1 = $this->_request['trialnotattendeddays1'];
        }

        if ($trial_status_tab != 'A' && $trial_status_tab != 'E' && $trial_status_tab != 'C' && $trial_status_tab != 'D' && empty($trial_days_type)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
//        if(empty($company_id) && empty($trial_id)){
//            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
//                $this->response($this->json($error), 200);
//        }
        if ($trial_days_type == 4 && (empty(trim($trial_startdate_limit)) || empty(trim($trial_enddate_limit)))) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 200);
            }
        if (!empty($company_id) && !empty($trial_status_tab)) {
            $this->trial_model->gettrialcustomerDetailsbyfilter($company_id, $trial_status_tab, $search, $draw_table, $length_table, $start, $sorting,$trial_startdate_limit,$trial_enddate_limit,$trial_days_type,$register_type,$notattendeddays,$notattendeddays1);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
     private function processTrialPaymentRefund() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_reg_id = $trial_payment_id = $trial_id = '';


        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_reg_id'])) {
            $trial_reg_id = $this->_request['trial_reg_id'];
        }
        if (isset($this->_request['trial_payment_id'])) {
            $trial_payment_id = $this->_request['trial_payment_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (!empty($company_id) && !empty($trial_reg_id) && !empty($trial_payment_id)) {
            $this->trial_model->processTrialPaymentRefund($company_id, $trial_reg_id, $trial_payment_id, $trial_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getTrialdashboard() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $leadsource_flag = $conversion_flag = $sales_period = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['l_flag'])) {
            $leadsource_flag = $this->_request['l_flag'];
        }
        
        if (isset($this->_request['c_flag'])) {
            $conversion_flag = $this->_request['c_flag'];
        }
        if (isset($this->_request['sales_period'])) {
            $sales_period = $this->_request['sales_period'];
        }

        if (!empty($company_id)) {
            $this->trial_model->getTrialDashboardDetails($company_id, $leadsource_flag, $conversion_flag, $sales_period);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
        
    private  function sendverificationcode(){
         if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $email = $company_id = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (isset($this->_request['email'])) {
            $email = $this->_request['email'];
        }
        if (!empty($company_id) &&!empty($email)) {
            $this->sendVerficationDetailsEmail($company_id, $email);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 200);
        }
    }

// NEW REGISTER FUNCTIONS
    
    private function verifyUserEmail() {

        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $user_email = '';

        //REQUESTING THE PARAMETERS
        if (isset($this->_request['user_email'])) {
            $user_email = $this->_request['user_email'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($user_email)) {
            $this->verifyUserEmailInDb($user_email,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
        }
    }

    private function verifyCompanyCode() {
        
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        // INITIALIZATION OF VARIABLES
        $company_code = '';
        
        //REQUESTING THE PARAMETERS
        if (isset($this->_request['company_code'])) {
            $company_code = $this->_request['company_code'];
        }
        if (!empty($company_code)) {
            $this->verifyCompanyCodeInDb($company_code,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function registerWebAppUser(){
        
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        // INITIALIZATION OF VARIABLES
        $user_email = $user_password = $country = $user_firstname = $user_lastname = $company_name = $web_page = $phone_number = $company_type = $company_code = $user_phone="";
        $user_phone_country ="";        

        //REQUESTING THE PARAMETERS
        if (isset($this->_request['user_email'])) {
            $user_email = $this->_request['user_email'];
}
        if(isset($this->_request['user_password'])){
            $user_password = $this->_request['user_password'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['user_firstname'])) {
            $user_firstname = $this->_request['user_firstname'];
        }
        if (isset($this->_request['user_lastname'])) {
            $user_lastname = $this->_request['user_lastname'];
        }
        if (isset($this->_request['company_name'])) {
            $company_name = $this->_request['company_name'];
        }
        if (isset($this->_request['web_page'])) {
            $web_page = $this->_request['web_page'];
        }
        if(isset($this->_request['phone_number'])){
            $phone_number = $this->_request['phone_number'];
        }
        if (isset($this->_request['company_type'])) {
            $company_type = $this->_request['company_type'];
        }
        if (isset($this->_request['company_code'])) {
            $company_code = $this->_request['company_code'];
        }
        if (isset($this->_request['user_phone'])) {
            $user_phone = $this->_request['user_phone'];
        }
        if (isset($this->_request['user_phone_country'])) {
            $user_phone_country = $this->_request['user_phone_country'];
        }
        
        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($user_email) && !empty($user_password) && !empty($country) && !empty($user_firstname) && !empty($user_lastname) && !empty($company_name)&& !empty($web_page)&& !empty($phone_number)&& !empty($company_type)&& !empty($user_phone) && !empty($company_code)) {
            $this->registerWebAppUserInDb($user_email,$user_password,$user_firstname,$user_lastname,$country,$company_name,$web_page,$phone_number,$company_type,$user_phone,$company_code,$user_phone_country);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getFromAccountForClone() {
        
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_email =$company_id= '';

        if (isset($this->_request['email'])) {
            $company_email = $this->_request['email'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_email)) {
            $this->admin_model->getFromAccountDetailsForClone($company_email);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getToAccountForClone() {
        
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_email =$company_id= '';

        if (isset($this->_request['email'])) {
            $company_email = $this->_request['email'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_email)) {
            $this->admin_model->getToAccountDetailsForClone($company_email);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getCloneDetailsFromUser() {
        
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
         $event_details=$membership_details=$curriculum_details=$trial_details=[];
                $company_id=$event_clone_flag=$membership_clone_flag=$curriculum_clone_flag=$trial_clone_flag=$from_company_id=$to_company_id=$from_user_id=$to_user_id='';

//        if (isset($this->_request['email'])) {
//            $company_email = $this->_request['email'];
//        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['from_company_id'])) {
            $from_company_id = $this->_request['from_company_id'];
        }
        if (isset($this->_request['to_company_id'])) {
            $to_company_id = $this->_request['to_company_id'];
        }
        if (isset($this->_request['from_user_id'])) {
            $from_user_id = $this->_request['from_user_id'];
        }
        if (isset($this->_request['to_user_id'])) {
            $to_user_id = $this->_request['to_user_id'];
        }        
        
        if (isset($this->_request['event_details'])) {
            $event_details = $this->_request['event_details'];
        }
        if (isset($this->_request['membership_details'])) {
            $membership_details = $this->_request['membership_details'];
        }
        if (isset($this->_request['curriculum_details'])) {
            $curriculum_details = $this->_request['curriculum_details'];
        }
        if (isset($this->_request['trial_details'])) {
            $trial_details = $this->_request['trial_details'];
        }
        if (isset($this->_request['event_clone_flag'])) {
            $event_clone_flag = $this->_request['event_clone_flag'];
        }
        if (isset($this->_request['membership_clone_flag'])) {
            $membership_clone_flag = $this->_request['membership_clone_flag'];
        }
        if (isset($this->_request['curriculum_clone_flag'])) {
            $curriculum_clone_flag = $this->_request['curriculum_clone_flag'];
        }
        if (isset($this->_request['trial_clone_flag'])) {
            $trial_clone_flag = $this->_request['trial_clone_flag'];
        }
        if($from_company_id==$to_company_id){
           $error = array('status' => "Failed", "msg" => "The source and destination studio should not be the same ");
           $this->response($this->json($error), 200); 
        }
        if (!empty($event_clone_flag) && !empty($membership_clone_flag) && !empty($curriculum_clone_flag) && !empty($trial_clone_flag)) {
            $this->admin_model->getStudioCloneDetailsFromUser($from_company_id,$to_company_id,$from_user_id,$to_user_id,$event_clone_flag,$membership_clone_flag,$curriculum_clone_flag,$trial_clone_flag,$event_details,$membership_details,$curriculum_details,$trial_details);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
     private function editTrialDetails() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $reg_id = $lead_source = $trial_start_date = $trial_end_date = $student_id = '';
        $trial_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['trial_reg_id'])) {
            $reg_id = $this->_request['trial_reg_id'];
        }
        if (isset($this->_request['trial_lead_source'])) {
            $lead_source = $this->_request['trial_lead_source'];
        }
        if (isset($this->_request['trial_start_date'])) {
            $trial_start_date = $this->_request['trial_start_date'];
        }
        if (isset($this->_request['trial_end_date'])) {
            $trial_end_date = $this->_request['trial_end_date'];
        }
         if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }

        if (empty($student_id) || (empty($trial_end_date) || empty($trial_start_date))){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        // Input validations
        if (!empty($company_id) && !empty($reg_id) && !empty($lead_source)) {
            $this->trial_model->editTrialDatesandlead($company_id, $student_id, $reg_id, $lead_source, $trial_start_date, $trial_end_date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
     private function getMembershipforaddparticipant() {

        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->membership_model->getMembershipDetailsforaddparticipant($company_id,0);
            } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    
    //Leads task
    private function addLeadsMember() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $p_firstname = $p_lastname = '';
        $include_campaign_email = 'Y';
        $leads_status = 'A';
        $reg_type_user = 'P';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = $version;
        $reg_entry_type = 'I';
       
       if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['pi_id'])) {
            $program_id = $this->_request['pi_id'];
        }
        if (isset($this->_request['ls_id'])) {
            $source_id = $this->_request['ls_id'];
        }
         if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
         if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['participant_first_name'])) {
            $p_firstname = $this->_request['participant_first_name'];
        }
        if (isset($this->_request['participant_last_name'])) {
            $p_lastname = $this->_request['participant_last_name'];
        }
        if (isset($this->_request['include_campaign_email']) && !empty($this->_request['include_campaign_email'])) {
            $include_campaign_email = $this->_request['include_campaign_email'];
        }
        if (isset($this->_request['leads_status'])) {
            $leads_status = $this->_request['leads_status'];
        }
        if(empty($program_id)){
             $program_id = 1;
        }
        if(empty($source_id)){
             $source_id = 1;
        }
       
        if (!empty($company_id) && (!empty($buyer_first_name) || !empty($buyer_last_name)) && (!empty($buyer_email) || !empty($buyer_phone))) {
            $this->leads_model->addIndividualLeadsMember($company_id, $program_id, $source_id, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $p_firstname, $p_lastname, $leads_status, $include_campaign_email, $reg_type_user, $reg_version_user, $reg_entry_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
        
    }
    
    private function leadscustomers() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $data_limit = 0;
        $company_id = $leads_status_tab = $leads_startdate_limit= $leads_enddate_limit='';
        $search = $draw_table = $length_table = $start = $sorting = $leads_days_type='';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['leads_startdate_limit'])) {
            $leads_startdate_limit = $this->_request['leads_startdate_limit'];
        }
        if (isset($this->_request['leads_enddate_limit'])) {
            $leads_enddate_limit = $this->_request['leads_enddate_limit'];
        }
        if (isset($this->_request['leads_days_type'])) {
            $leads_days_type = $this->_request['leads_days_type'];
        }
        if (isset($this->_request['leads_status_tab'])) {
            $leads_status_tab = $this->_request['leads_status_tab'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = $this->_request['start'];
        }

        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }
       

        if ($leads_status_tab != 'A' && $leads_status_tab != 'E' && $leads_status_tab != 'NI' && empty($leads_days_type)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
         if ($leads_days_type == 4 && (empty(trim($leads_startdate_limit)) || empty(trim($leads_enddate_limit)))) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 200);
            }
        if (!empty($company_id) && !empty($leads_status_tab)) {
            $this->leads_model->getleadscustomerDetailsbyfilter($company_id, $leads_status_tab, $search, $draw_table, $length_table, $start, $sorting,$leads_startdate_limit,$leads_enddate_limit,$leads_days_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function moveanddeletefield() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $id =  $type = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['id'])) {
            $id = $this->_request['id'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['to_be_deleted'])) {
            $old_lead = $this->_request['to_be_deleted'];
        }

        if (!empty($company_id) && !empty($id) && (!empty($type) && ($type == 'pi' || $type == 'ls'))) {
            $this->leads_model->moveanddeletefieldindb($company_id, $id, $type, $old_lead);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function getleadCSV() {//header
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (!empty($company_id)) {
            $this->leads_model->createLeadsCSV($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function leadImportCSVCheck() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $uploadedCSV_file = '';
        $call_back = 0;
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['uploadedCSV_file'])) {
            $uploadedCSV_file = $this->_request['uploadedCSV_file'];
        }
        if (!empty($company_id)&&!empty($uploadedCSV_file)) {
            $this->leads_model->createLeadImportCSV($company_id,$uploadedCSV_file, $call_back);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function ConfirmImportCSV() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $uploadedCSV_file = '';
        $include_campaign_email = 'Y';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['uploadedCSV_file'])) {
            $uploadedCSV_file = $this->_request['uploadedCSV_file'];
        }
        if (isset($this->_request['include_campaign_email']) && !empty($this->_request['include_campaign_email'])) {
            $include_campaign_email = $this->_request['include_campaign_email'];
        }
        
        if (!empty($company_id)&&!empty($uploadedCSV_file)) {
            $this->leads_model->InsertImportedCSV($company_id,$uploadedCSV_file,$include_campaign_email,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function leadsRegDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $leads_reg_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['leads_reg_id'])) {
            $leads_reg_id = $this->_request['leads_reg_id'];
        }
        if (!empty($company_id) && !empty($leads_reg_id)) {
            $this->leads_model->getLeadsRegDetails($company_id, $leads_reg_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function leadsHistoryDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $leads_reg_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['leads_reg_id'])) {
            $leads_reg_id = $this->_request['leads_reg_id'];
        }
        if (!empty($company_id) && !empty($leads_reg_id)) {
            $this->leads_model->getLeadsHistoryDetails($company_id, $leads_reg_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function editLeadsDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $leads_reg_id = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $pfirst_name = $plast_name = $program_interest_id = $source_id = '';
         
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['leads_id'])) {
            $leads_reg_id = $this->_request['leads_id'];
        }
        
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
         if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['participant_first_name'])) {
            $pfirst_name = $this->_request['participant_first_name'];
        }
        if (isset($this->_request['participant_last_name'])) {
            $plast_name = $this->_request['participant_last_name'];
        }
        if (isset($this->_request['program_interest_id'])) {
            $program_interest_id = $this->_request['program_interest_id'];
        }
        if (isset($this->_request['source_id'])) {
            $source_id = $this->_request['source_id'];
        }
        if (!empty($company_id) && !empty($leads_reg_id)) {
            $this->leads_model->updateLeadsDetails($company_id, $leads_reg_id, $buyer_first_name, $buyer_last_name, $buyer_phone, $buyer_email, $pfirst_name, $plast_name, $program_interest_id, $source_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function LeadsHistoryNotes() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $leads_reg_id = $status = $history_id = $note_text = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['leads_reg_id'])) {
            $leads_reg_id = $this->_request['leads_reg_id'];
        }
        if (isset($this->_request['status'])) {
            $status = $this->_request['status'];
        }
        if (isset($this->_request['history_id'])) {
            $history_id = $this->_request['history_id'];
        }
        if (isset($this->_request['note_text'])) {
            $note_text = $this->_request['note_text'];
        }

        if ($status != 'add' && $status != 'update' && $status != 'delete') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } elseif (($status == 'delete' || $status == 'update') && empty($history_id)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } elseif (($status == 'add' || $status == 'update') && empty($note_text)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        // Input validations
        if (!empty($company_id) && !empty($leads_reg_id) && !empty($status)) {
            $this->leads_model->addorupdateLeadsHistoryNote($company_id, $leads_reg_id, $status, $history_id, $note_text);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    
   private function sendleadsGroupPush() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $subject = $message = $type = $attached_file = $file_name = $leads_startdate_limit = $leads_enddate_limit = $leads_days_type ='';
        $leads_reg_id = [];
        $all_select_flag = 'N';
        $leads_status = $from=$search ='';
        
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['subject'])) {
            $subject = $this->_request['subject'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['selected_leads_list'])) {
            $leads_reg_id = $this->_request['selected_leads_list'];
        }
       if (isset($this->_request['attached_file'])) {
            $attached_file = $this->_request['attached_file'];
        }
        if (isset($this->_request['file_name'])) {
            $file_name = $this->_request['file_name'];
        }
        if (isset($this->_request['all_select_flag'])) {
            $all_select_flag = $this->_request['all_select_flag'];
        }
        if (isset($this->_request['leads_status'])) {
            $leads_status = $this->_request['leads_status'];
        }
       if (isset($this->_request['from'])) {
            $from = $this->_request['from'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['leads_startdate_limit'])) {
            $leads_startdate_limit = $this->_request['leads_startdate_limit'];
        }
        if (isset($this->_request['leads_enddate_limit'])) {
            $leads_enddate_limit = $this->_request['leads_enddate_limit'];
        }
        if (isset($this->_request['leads_days_type'])) {
            $leads_days_type = $this->_request['leads_days_type'];
        }
        if(isset($this->_request['unsubscribe_email'])){
            $unsubscribe_email = $this->_request['unsubscribe_email'];
        } 
        
        if ($type != 'subscribe') {
            if (!empty($company_id) && !empty($message) && ((!empty($leads_reg_id) && $all_select_flag == 'N') || $all_select_flag == 'Y') && !empty($leads_status)) {
                $this->leads_model->sendGroupPushMailForLead($company_id, $subject, $message, $attached_file, $file_name, $from, $leads_status, $leads_reg_id, $all_select_flag, $search, $leads_startdate_limit, $leads_enddate_limit, $leads_days_type);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 400);
            }
        } else {
            $this->activationlinkCreationForEmail($unsubscribe_email, $subject, $company_id);
        }
    }

    private function createleadsSelectonCSV() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
         $company_id = $subject = $message = $type = $attached_file = $file_name = $leads_startdate_limit = $leads_enddate_limit = $leads_days_type ='';
        $lead_reg_id = [];
        $all_select_flag = 'N';
        $leads_status = $from=$search ='';
        
         if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['subject'])) {
            $subject = $this->_request['subject'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['selected_leads_list'])) {
            $lead_reg_id = $this->_request['selected_leads_list'];
        }
       if (isset($this->_request['all_select_flag'])) {
            $all_select_flag = $this->_request['all_select_flag'];
        }
        if (isset($this->_request['leads_status'])) {
            $leads_status = $this->_request['leads_status'];
        }
       if (isset($this->_request['from'])) {
            $from = $this->_request['from'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['leads_startdate_limit'])) {
            $leads_startdate_limit = $this->_request['leads_startdate_limit'];
        }
        if (isset($this->_request['leads_enddate_limit'])) {
            $leads_enddate_limit = $this->_request['leads_enddate_limit'];
        }
        if (isset($this->_request['leads_days_type'])) {
            $leads_days_type = $this->_request['leads_days_type'];
        }
        
        if(!empty($company_id) && ((!empty($lead_reg_id) && $all_select_flag == 'N') || $all_select_flag == 'Y')){
            $this->leads_model->createLeadSelectionCSV($company_id,$lead_reg_id, $leads_status, $all_select_flag,$search,$leads_startdate_limit,$leads_enddate_limit,$leads_days_type);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function updateleadsParticipantStatus() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $leads_reg_id = $leads_status = $next_date = '';
        

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['leads_reg_id'])) {
            $leads_reg_id = $this->_request['leads_reg_id'];
        }
         if (isset($this->_request['leads_status'])) {
            $leads_status = $this->_request['leads_status'];
        }
        if (isset($this->_request['next_date'])) {
            $next_date = $this->_request['next_date'];
        }
        if (!empty($company_id) && !empty($leads_reg_id) && !empty($leads_status)) {
            $this->leads_model->updateleadsParticipantStatusInDb($company_id, $leads_reg_id,  $leads_status, $next_date);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function getleadsdashboard() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $leadsource_flag = $conversion_flag = $sales_period = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['l_flag'])) {
            $leadsource_flag = $this->_request['l_flag'];
        }
        if (isset($this->_request['c_flag'])) {
            $conversion_flag = $this->_request['c_flag'];
        }
        if (isset($this->_request['sales_period'])) {
            $sales_period = $this->_request['sales_period'];
        }

        if (!empty($company_id)) {
            $this->leads_model->getLeadsDashboardDetails($company_id, $leadsource_flag, $conversion_flag, $sales_period);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function updateprograminterest() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $program_name = $program_id = $type = '';
        $call_back = 0;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['pi_name'])) {
            $program_name = $this->_request['pi_name'];
        }
        if (isset($this->_request['leads_pi_id'])) {
            $program_id = $this->_request['leads_pi_id'];
        }

        // Input validations
        if (!empty($type) && ($type == 'add' || ($type == 'update' && !empty($program_id)))) {
            if (!empty($company_id) && !empty($program_name)) {
                $this->leads_model->updateprograminterestdetails($company_id, $program_name, $program_id, $type, $call_back);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 400);
            }
        } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 400);
        }
    }
    
    private function updateleadsource() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $source_name = $source_id = $type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['ls_name'])) {
            $source_name = $this->_request['ls_name'];
        }
        if (isset($this->_request['leads_ls_id'])) {
            $source_id = $this->_request['leads_ls_id'];
        }

        // Input validations
        if (!empty($type) && ($type == 'add' || ($type == 'update' && !empty($source_id)))) {
            if (!empty($company_id) && !empty($source_name)) {
                $this->leads_model->updateleadsourcedetails($company_id, $source_name, $source_id, $type, 0);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 400);
            }
        } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 400);
        }
    }
    
    private function getleadssettings() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->leads_model->getleadsProgramDetails($company_id,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function sortsettingslist() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $id = $sort_order = $type = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['id'])) {
            $id = $this->_request['id'];
        }
        if (isset($this->_request['sort_id'])) {
            $sort_order = $this->_request['sort_id'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }

        if (!empty($company_id) && !empty($id) && !empty($sort_order) && (!empty($type) && ($type == 'pi' || $type == 'ls'))) {
            $this->leads_model->updatesortlistDetails($sort_order, $company_id, $id, $type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function checkparticipantexist() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $id =  $type = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['id'])) {
            $id = $this->_request['id'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }

        if (!empty($company_id) && !empty($id) && (!empty($type) && ($type == 'pi' || $type == 'ls'))) {
            $this->leads_model->checkparticipantexistandDelete($company_id, $id, $type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function unblockbounceemail(){
        if($this->get_request_method() !="POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id=$bounced_email='';
        
        if (isset($this->_request['bounced_email'])) {
            $bounced_email = $this->_request['bounced_email'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        
        if(!empty($bounced_email) && !empty($company_id) ){
            $this->unblockbouncedemail($company_id,$bounced_email);
        }else{
             $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    //Admin Panel task
    //getadminmembers
    private function getadminaccounts() {

        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
         if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = $this->_request['start'];
        }

        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }
        
        $this->admin_model->getadminstudioaccounts($sorting,$start,$length_table,$draw_table,$search);
    }
    
     private function getadminWLdetails() {

        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $filter = $android_filter = "";
         if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = $this->_request['start'];
        }
        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }
        if (isset($this->_request['filter'])) {
            $filter = $this->_request['filter'];
        }
        if (isset($this->_request['android_filter'])) {
            $android_filter = $this->_request['android_filter'];
        }
        
        $this->whitelabel_model->getadminWLdetailsINDB($sorting,$start,$length_table,$draw_table,$search,$filter,$android_filter);
    }
    
    private function getStudioSalesFromAdminPanel(){
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }        
        
        if(!empty($company_id) ){
            $this->admin_model->getStudioSalesDetails($company_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }        
    }
    
    private function updatestudiodetail(){
        if($this->get_request_method() !="POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $studio_name = $lead_source = $owner_name = $user_id = $user_firstname = $user_lastname = $user_phone = $industry = $website = $studio_phone = $company_code = $login_email = '';
        $street_address = $city = $state = $postal_code = $country = "";
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['studio_name'])) {
            $studio_name = $this->_request['studio_name'];
        }
        if (isset($this->_request['lead_source'])) {
            $lead_source = $this->_request['lead_source'];
        }
        if (isset($this->_request['owner_name'])) {
            $owner_name = $this->_request['owner_name'];
        }
        if (isset($this->_request['user_id'])) {
            $user_id = $this->_request['user_id'];
        }
        if (isset($this->_request['user_firstname'])) {
            $user_firstname = $this->_request['user_firstname'];
        }
        if (isset($this->_request['user_lastname'])) {
            $user_lastname = $this->_request['user_lastname'];
        }
        if (isset($this->_request['user_phone'])) {
            $user_phone = $this->_request['user_phone'];
        }
        if (isset($this->_request['industry'])) {
            $industry = $this->_request['industry'];
        }
        if (isset($this->_request['website'])) {
            $website = $this->_request['website'];
        }
        if (isset($this->_request['studio_phone'])) {
            $studio_phone = $this->_request['studio_phone'];
        }
        if (isset($this->_request['login_email'])) {
            $login_email = $this->_request['login_email'];
        }
        if (isset($this->_request['company_code'])) {
            $company_code = $this->_request['company_code'];
        }
        if (isset($this->_request['street_address'])) {
            $street_address = $this->_request['street_address'];
        }
        if (isset($this->_request['city'])) {
            $city = $this->_request['city'];
        }
        if (isset($this->_request['state'])) {
            $state = $this->_request['state'];
        }
        if (isset($this->_request['postal_code'])) {
            $postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        $emailavailblecheck = $this->admin_model->verifyUserEmailInDbForAdminPanel($login_email,$company_id,1);
        if($emailavailblecheck == 'exist'){
            $error = array('status' => "Failed", "msg" => "Email ID already Exist");
            $this->response($this->json($error), 200);
        }
        $studiocodeavailblecheck= $this->admin_model->verifyCompanyCodeInDbForAdminPanel($company_code,$company_id,1);
        if($studiocodeavailblecheck == 'N'){
            $error = array('status' => "Failed", "msg" => "Studio Code already Exist");
            $this->response($this->json($error), 200);
        }
        
        if(!empty($company_id)&&!empty($studio_name)&&!empty($company_code)&&!empty($login_email)&&!empty($user_id)){
            $this->admin_model->updatestudiodetails($company_id,$studio_name,$lead_source,$owner_name,$user_id,$user_firstname,$user_lastname,$user_phone,$industry,$website,$studio_phone,$login_email,$company_code,$street_address,$city,$state,$postal_code,$country);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function verifyUserEmailForAdminPanel() {

        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $user_email = $company_id = '';

        //REQUESTING THE PARAMETERS
        if (isset($this->_request['user_email'])) {
            $user_email = $this->_request['user_email'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($user_email) && !empty($company_id)) {
            $this->admin_model->verifyUserEmailInDbForAdminPanel($user_email,$company_id,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function verifyCompanyCodeForAdminPanel() {
        
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        // INITIALIZATION OF VARIABLES
        $company_code = $company_id = '';
        
        //REQUESTING THE PARAMETERS
        if (isset($this->_request['company_code'])) {
            $company_code = $this->_request['company_code'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        
        if (!empty($company_code) && !empty($company_id)) {
            $this->admin_model->verifyCompanyCodeInDbForAdminPanel($company_code,$company_id,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
        
    //get admin dashboard details
    private function getadmindashboarddetails() {
        
        $this->admin_model->getadmindashboarddetailsForAdminPanel();
        
    }
    
    private function createCsvForadmin() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $search = '';
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
       
            $this->admin_model->adminstudioaccountscsv($search);
        
    }
    
    //Campaign task
    private function getCampaignDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->campaign_model->getAllCampaignDetails($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getEmailListForCampaign() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $campaign_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['campaign_id'])) {
            $campaign_id = $this->_request['campaign_id'];
        }

        if (!empty($company_id) && !empty($campaign_id)) {
            $this->campaign_model->getAllEmailListForCampaign($company_id,$campaign_id,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function updateCampaignSortingOrder() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id =$campaign_id =$campaign_sort_order ='';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['campaign_id'])) {
            $campaign_id = $this->_request['campaign_id'];
        }
        if (isset($this->_request['sort_id'])) {
            $campaign_sort_order = $this->_request['sort_id'];
        }

        if (!empty($company_id) && !empty($campaign_id) && !empty($campaign_sort_order)) {
            $this->campaign_model->updateCampaignSortingDetails($company_id, $campaign_id, $campaign_sort_order);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    
    private function updateCampaignStatus() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $campaign_id = $status = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['campaign_id'])) {
            $campaign_id = $this->_request['campaign_id'];
        }
        if (isset($this->_request['status'])) {
            $status = $this->_request['status'];
        }

        if (!empty($company_id) && !empty($campaign_id) &&  !empty($status)) {
            $this->campaign_model->changeCampaignStatus($company_id,$campaign_id,$status);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function updateEmailCampaign() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $parent_id = $campaign_id = $status = $type = $subject = $message = $email_banner_image = $email_banner_image_type = $show_banner_image = $banner_image_update_status = $email_banner_old_image_url = $button_text = $button_url = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['campaign_id'])) {
            $campaign_id = $this->_request['campaign_id'];
        }
        if (isset($this->_request['parent_id'])) {
            $parent_id = $this->_request['parent_id'];
        }
        if (isset($this->_request['status'])) {
            $status = $this->_request['status'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['subject'])) {
            $subject = $this->_request['subject'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['email_banner_image'])) {
            $email_banner_image = $this->_request['email_banner_image'];
        }
        if (isset($this->_request['email_banner_image_type'])) {
            $email_banner_image_type = $this->_request['email_banner_image_type'];
        }
        if (isset($this->_request['banner_image_update_status'])) {
            $banner_image_update_status = $this->_request['banner_image_update_status'];
        }
        if (isset($this->_request['email_banner_old_image_url'])) {
            $email_banner_old_image_url = $this->_request['email_banner_old_image_url'];
        }
        if (isset($this->_request['show_banner_image'])) {
            $show_banner_image = $this->_request['show_banner_image'];
        }
        if (isset($this->_request['button_text'])) {
            $button_text = $this->_request['button_text'];
        }
         if (isset($this->_request['button_url'])) {
            $button_url = $this->_request['button_url'];
        }
        
        
        if (!empty($company_id) && !empty($campaign_id) && !empty($parent_id) &&  !empty($status) &&  !empty($type) && !empty($subject) && !empty($message) && !empty($show_banner_image) && !empty($banner_image_update_status)) {
            $this->campaign_model->updateEmailCampaignInDb($company_id,$parent_id,$campaign_id,$type,$status,$subject,$message,$email_banner_image,$email_banner_image_type,$show_banner_image,$banner_image_update_status,$email_banner_old_image_url, $button_text,$button_url);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function sendTestEmailCampaign() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $test_email_id = $subject = $message = $email_banner_image = $email_banner_image_type = $show_banner_image = $email_banner_old_image_url = $banner_image_update_status = $button_text = $button_url = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['email_to'])) {
            $test_email_id = $this->_request['email_to'];
        }
        if (isset($this->_request['subject'])) {
            $subject = $this->_request['subject'];
        }
        if (isset($this->_request['message'])) {
            $message = $this->_request['message'];
        }
        if (isset($this->_request['email_banner_image'])) {
            $email_banner_image = $this->_request['email_banner_image'];
        }
        if (isset($this->_request['email_banner_image_type'])) {
            $email_banner_image_type = $this->_request['email_banner_image_type'];
        }
        if (isset($this->_request['show_banner_image'])) {
            $show_banner_image = $this->_request['show_banner_image'];
        }
        if (isset($this->_request['banner_image_update_status'])) {
            $banner_image_update_status = $this->_request['banner_image_update_status'];
        }
        if (isset($this->_request['email_banner_old_image_url'])) {
            $email_banner_old_image_url = $this->_request['email_banner_old_image_url'];
        }
        if (isset($this->_request['button_text'])) {
            $button_text = $this->_request['button_text'];
        }
         if (isset($this->_request['button_url'])) {
            $button_url = $this->_request['button_url'];
        }
        
        
        if (!empty($company_id) && !empty($test_email_id) && !empty($subject) && !empty($message) && !empty($show_banner_image)  && !empty($banner_image_update_status)) {
            $this->campaign_model->sendTestEmailForCampaign($company_id,$test_email_id,$subject,$message,$email_banner_image,$email_banner_image_type,$show_banner_image, $banner_image_update_status,$email_banner_old_image_url,$button_text,$button_url);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    //Stripe & My Account Section Task
    private function getPlanDetails(){
       if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $payment_type = '';
        $call_back=0;
        $payment_plan_list = ['W', 'WM', 'P', 'M', 'B'];
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if(!in_array($payment_type, $payment_plan_list)){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        
        if (!empty($company_id) && !empty($payment_type)) {
            $this->getSubscriptionPlanDetails($company_id, $payment_type, $call_back);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function updateStudioSubscription(){
       if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $payment_type = $payee_type = $user_id = $charge_flag=$lead_source= '';
        $referer_name='N/A';
        $payment_plan_list = ['W', 'WM', 'P', 'M', 'B'];
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['payee_type'])) {
            $payee_type = $this->_request['payee_type'];
        }       
        if (isset($this->_request['user_id'])) {
            $user_id = $this->_request['user_id'];
        }
        if(isset($this->_request['lead_source'])){
            $lead_source = $this->_request['lead_source'];
        }
        if(isset($this->_request['referer_name']) && !empty($this->_request['referer_name'])){
            $referer_name = $this->_request['referer_name'];
        }
        if(isset($this->_request['charge_flag'])){
            $charge_flag = $this->_request['charge_flag'];
        }
        if(!in_array($payment_type, $payment_plan_list)){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        if (!empty($company_id)&& !empty($user_id) && !empty($payment_type) && !empty($payee_type)&& !empty($charge_flag)) {
            $this->updateStudioSubscriptionPlan($company_id, $user_id, $payment_type, $payee_type, $charge_flag, $lead_source, $referer_name);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function updateStudioSubscriptionCard(){
       if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $firstname = $lastname = $address = $city = $state = $country = $zip = '';
        $company_id = $payment_type = $payment_amount = $payee_type = $token_id =$stripe_referal_name=$charge_flag= '';
        $setup_fee = $processing_fee = 0;
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['setup_fee'])) {
            $setup_fee = $this->_request['setup_fee'];
        }
        if (isset($this->_request['processing_fee'])) {
            $processing_fee = $this->_request['processing_fee'];
        }
        if (isset($this->_request['payee_type'])) {
            $payee_type = $this->_request['payee_type'];
        }
        if (isset($this->_request['FirstName'])) {
            $firstname = $this->_request['FirstName'];
        }
        if (isset($this->_request['LastName'])) {
            $lastname = $this->_request['LastName'];
        }
        if (isset($this->_request['StreetAddress'])) {
            $address = $this->_request['StreetAddress'];
        }
        if (isset($this->_request['City'])) {
            $city = $this->_request['City'];
        }
        if (isset($this->_request['StateCode'])) {
            $state = $this->_request['StateCode'];
        }
        if (isset($this->_request['Country'])) {
            $country = $this->_request['Country'];
        }
        if (isset($this->_request['BillingZipcode'])) {
            $zip = $this->_request['BillingZipcode'];
        }        
        if (isset($this->_request['token_id'])) {
            $token_id = $this->_request['token_id'];
        }        
        if(isset($this->_request['charge_flag'])){
            $charge_flag = $this->_request['charge_flag'];
        }
        if($payee_type!=='registration' && $payee_type!=='update_card'){
            $error = array('status' => "Failed", "msg" => "Invalid payment type, Server error.");
            $this->response($this->json($error), 200);
        }
        

        if (!empty($company_id)&& !empty($payment_type) &&  !empty($token_id)&& !empty($payee_type)&& !empty($charge_flag)) {
            $this->updateStudioSubscriptionCardDetails($company_id, $payment_type, $payee_type,$charge_flag, $firstname, $lastname, $address, $city, $state, $country, $zip, $token_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function reRunSubscription(){
       if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $payment_type = $payment_amount = $payee_type =$user_id=$charge_flag= '';
        $call_back= 0;
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['payee_type'])) {
            $payee_type = $this->_request['payee_type'];
        }
        if(isset($this->_request['charge_flag'])){
            $charge_flag = $this->_request['charge_flag'];
        }

        if (!empty($company_id) && !empty($payee_type)&& !empty($charge_flag)) {
            $this->reRunSubscriptionPayment($company_id, $call_back);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
        
    private function getStripeCreditCardAccounts() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id='';
        
        if (isset($this->_request['company_id'])) {
        $company_id = $this->_request['company_id'];
        }
        if (!empty($company_id)) {
            $this->getStripeCreditCardAccountDetails($company_id,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function downgradeStudioSubscription(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $payment_plan = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['payment_plan'])) {
            $payment_plan = $this->_request['payment_plan'];
        }
        
        if (!empty($company_id) && !empty($payment_plan)) {
            $this->admin_model->downgradeStudioSubscriptionStatus($company_id, $payment_plan);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    //Zapier
    private function getCustomAPIKeyDetails(){
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
        $company_id = $this->_request['company_id'];
        }
        
        if (!empty($company_id)) {
            $this->getCustomAPIkeydetailsInDB($company_id,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getZapierDetails(){
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (!empty($company_id)) {
            $this->getzapierdetailsInDB($company_id,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function createCustomAPIkey(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $custom_api_name = $api_type = '';
        
        if (isset($this->_request['company_id'])) {
        $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['custom_api_name'])) {
        $custom_api_name = $this->_request['custom_api_name'];
        }
        if (isset($this->_request['api_type'])) {
        $api_type = $this->_request['api_type'];
        }
        
        if (!empty($company_id)&& !empty($custom_api_name)&& !empty($api_type)) {
            $this->createCustomAPIkeyInDB($company_id,$custom_api_name,$api_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function deleteCustomAPIkey(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $delete_api_key = $delete_api_name = '';
        
        if (isset($this->_request['company_id'])) {
        $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['delete_api_key'])) {
        $delete_api_key = $this->_request['delete_api_key'];
        }
        if (isset($this->_request['delete_api_name'])) {
        $delete_api_name = $this->_request['delete_api_name'];
        }
        
        if (!empty($company_id)&& !empty($delete_api_key) && !empty($delete_api_name)) {
            $this->deleteCustomAPIkeyInDB($company_id,$delete_api_key,$delete_api_name);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function addZapierTrigger(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $target_url = $zap_event_type = $zap_category_type = $zap_api_key = $zap_name = '';
        
        if (isset($this->_request['company_id'])) {
        $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['target_url'])) {
        $target_url = $this->_request['target_url'];
        }
        if (isset($this->_request['zap_name'])) {
        $zap_name = $this->_request['zap_name'];
        }
        if (isset($this->_request['zap_event_type'])) {
        $zap_event_type = $this->_request['zap_event_type'];
        }
        if (isset($this->_request['zap_category_type'])) {
        $zap_category_type = $this->_request['zap_category_type'];
        }
        if (isset($this->_request['zap_api_key'])) {
        $zap_api_key = $this->_request['zap_api_key'];
        }
        
        if (!empty($company_id) && !empty($target_url) && !empty($zap_name) && !empty($zap_event_type) && !empty($zap_category_type) && !empty($zap_api_key)) {
            $this->updateZapierTrigger($company_id,$target_url,$zap_name,$zap_event_type,$zap_category_type,$zap_api_key);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    private function testZapTrigger(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $test_target_url = '';
        
        if (isset($this->_request['company_id'])) {
        $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['test_target_url'])) {
        $test_target_url = $this->_request['test_target_url'];
        }
        
        if (!empty($company_id)&& !empty($test_target_url)) {
            $this->testZapTriggerInDB($company_id,$test_target_url);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    private function deleteZapierTrigger(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $del_target_url = '';
        
        if (isset($this->_request['company_id'])) {
        $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['del_target_url'])) {
        $del_target_url = $this->_request['del_target_url'];
        }
        
        if (!empty($company_id)&& !empty($del_target_url)) {
            $this->deleteZapierTriggerInDB($company_id,$del_target_url);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function trialPaymentMarkedAsPaid() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $reg_id = $payment_id = $credit_method = '';
        $credit_amount = 0;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['payment_id'])) {
            $payment_id = $this->_request['payment_id'];
        }
        if (isset($this->_request['credit_amount'])) {
            $credit_amount = $this->_request['credit_amount'];
        }
        if (isset($this->_request['credit_method'])) {
            $credit_method = $this->_request['credit_method'];
        }

        // Input validations
        if (!empty($company_id) && !empty($reg_id) && !empty($payment_id) && !empty($credit_amount) && (!empty($credit_method) && ($credit_method == 'MC' || $credit_method == 'CA' || $credit_method == 'CH'))) {
            $this->trial_model->updateTrialPaymentDetailsAsPaid($company_id, $reg_id, $payment_id, $credit_amount, $credit_method);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function switchRetail(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id ='';
        $retail_flag='N';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['retail_flag'])) {
            $retail_flag = $this->_request['retail_flag'];
        }
        if(!empty($company_id )&& !empty($retail_flag)&&($retail_flag=='Y'||$retail_flag=='N')){
            $this->retail_model->switchRetailinDb($company_id,$retail_flag);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    //get category list
    private function getretailcategorylist(){
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if(!empty($company_id)){
            $this->retail_model->getcategorylistinDB($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function updateRetail(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $retail_parent_id = $retail_product_type = $retail_product_status = $retail_product_title = $retail_product_subtitle = $retail_product_desc  = $retail_banner_img_content = $retail_banner_img_type = '';
        $retail_product_price = $retail_product_compare_price = $retail_product_image_update = $old_retail_banner_img_url = $tax_rate = $variant_flag = $retail_novariant_inventory = "";
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['retail_parent_id'])) {
            $retail_parent_id = $this->_request['retail_parent_id'];
        }
        if (isset($this->_request['retail_product_type'])) {
            $retail_product_type = $this->_request['retail_product_type'];
        }
        if (isset($this->_request['retail_product_status'])) {
            $retail_product_status = $this->_request['retail_product_status'];
        }
        if (isset($this->_request['retail_product_title'])) {
            $retail_product_title = $this->_request['retail_product_title'];
        }
        if (isset($this->_request['retail_product_subtitle'])) {
            $retail_product_subtitle = $this->_request['retail_product_subtitle'];
        }
        if (isset($this->_request['retail_product_desc'])) {
            $retail_product_desc  = $this->_request['retail_product_desc'];
        }
        if (isset($this->_request['retail_banner_img_content'])) {
            $retail_banner_img_content = $this->_request['retail_banner_img_content'];
        }
        if (isset($this->_request['retail_banner_img_type'])) {
            $retail_banner_img_type = $this->_request['retail_banner_img_type'];
        }
        if (isset($this->_request['retail_id'])) {
            $retail_id = $this->_request['retail_id'];
        }
        if (isset($this->_request['retail_product_price'])){
            $retail_product_price = $this->_request['retail_product_price'];
        }
        if (isset($this->_request['retail_product_compare_price'])){
            $retail_product_compare_price = $this->_request['retail_product_compare_price'];
        }
        if (isset($this->_request['retail_product_image_update'])){
            $retail_product_image_update = $this->_request['retail_product_image_update'];
        }
        if (isset($this->_request['add_or_update'])){
            $add_or_update = $this->_request['add_or_update'];
        }
        if (isset($this->_request['tax_rate'])){
            $tax_rate = $this->_request['tax_rate'];
        }
        if (isset($this->_request['variant_flag'])){
            $variant_flag = $this->_request['variant_flag'];
        }
        if (isset($this->_request['retail_novariant_inventory'])){
            $retail_novariant_inventory = $this->_request['retail_novariant_inventory'];
        }
        if (isset($this->_request['retail_old_image_url'])){
            $old_retail_banner_img_url = $this->_request['retail_old_image_url'];
        }
        //update checks
            $retail_banner_img_url = "";
            if (($add_or_update == "update" && $retail_product_image_update === 'Y') || ($add_or_update == "add")) {
                if($retail_banner_img_content != ''){
                    $cfolder_name = "Company_$company_id";
                    $old = getcwd(); // Save the current directory
                    $dir_name = "../../../$this->upload_folder_name";
                    chdir($dir_name);
                    if (!file_exists($cfolder_name)) {
                        $oldmask = umask(0);
                        mkdir($cfolder_name, 0777, true);
                        umask($oldmask);
                    }
                    chdir($cfolder_name);
                    $folder_name = "Retail";
                    if (!file_exists($folder_name)) {
                        $oldmask = umask(0);
                        mkdir($folder_name, 0777, true);
                        umask($oldmask);
                    }
                    chdir($folder_name);
                    $image_content = base64_decode($retail_banner_img_content);
                    $file_name = "$retail_id" . "_" . time();
                    $file = "$file_name.$retail_banner_img_type";
                    $img = $file; // logo - your file name
                    $ifp = fopen($img, "wb");
                    fwrite($ifp, $image_content);
                    fclose($ifp);
                    chdir($old);
                    $retail_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/$cfolder_name/$folder_name/$file";
                }else{
                    $retail_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/Default/default.png";
                }
            }

        // Input validations
        if (!empty($retail_product_title) && !empty($company_id) && !empty($retail_product_type)) {
            if ($retail_product_type == "P" && empty($retail_parent_id) ) {
                $error = array('status' => "Failed", "msg" => "Failed to add product to category");
                $this->response($this->json($error), 200);                
            } else {
                if($add_or_update == "add"){
                    $this->retail_model->insertRetailCategory($company_id, $retail_parent_id, $retail_product_type, $retail_product_status, $retail_product_title, $retail_product_subtitle, $retail_product_desc,$tax_rate,$variant_flag,$retail_novariant_inventory,$retail_product_price,$retail_product_compare_price,$retail_banner_img_url);
                }else if($add_or_update == "update"){
                    $this->retail_model->updateRetailCategoryDetails($retail_id,$company_id,$retail_parent_id,$retail_product_type,$retail_product_title,$retail_product_subtitle,$retail_product_desc,$retail_product_price,$retail_product_compare_price,$retail_banner_img_url,$retail_product_status,$retail_product_image_update,"live",$old_retail_banner_img_url,$tax_rate,$variant_flag,$retail_novariant_inventory);
                }
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        
    }
    
    private function addOrUpdateRetailProductDiscount() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $discount_code = $discount_id = $selected_products = $end_date = $discount_type = $discount_amount = $start_date = $min_purchase_flag = $min_purchase_amount = $discount_limit_flag = $discount_limit_value = $type = '';
        $min_array = array('N', 'V', 'Q');
        $retail_discount_array = array('V', 'P');

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['discount_id'])) {
            $discount_id = $this->_request['discount_id'];
        }
        if (isset($this->_request['start_date'])) {
            $start_date = $this->_request['start_date'];
        }
        if (isset($this->_request['end_date'])) {
            $end_date = $this->_request['end_date'];
        }
        if (isset($this->_request['discount_type'])) {
            $discount_type = $this->_request['discount_type'];
        }
        if (isset($this->_request['discount_amount'])) {
            $discount_amount = $this->_request['discount_amount'];
        }
        if (isset($this->_request['min_purchase_flag'])) {
            $min_purchase_flag = $this->_request['min_purchase_flag'];
        }
        if (isset($this->_request['min_purchase_amount'])) {
            $min_purchase_amount = $this->_request['min_purchase_amount'];
        }
        if (isset($this->_request['discount_limit_flag'])) {
            $discount_limit_flag = $this->_request['discount_limit_flag'];
        }
        if (isset($this->_request['discount_limit'])) {
            $discount_limit_value = $this->_request['discount_limit'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['applies_entire_or_specific'])) {
            $applies_entire_or_specific = $this->_request['applies_entire_or_specific'];
        }
        if (isset($this->_request['selected_products'])) {
            $selected_products = $this->_request['selected_products'];
        }
        if ((!is_array($selected_products) || (is_array($selected_products) && count($selected_products) == 0)) && $applies_entire_or_specific == "P"){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        // Input validations
        if (!in_array($min_purchase_flag, $min_array) || !in_array($discount_type, $retail_discount_array)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        if (!empty($company_id) && !empty($discount_code) && !empty($discount_amount) && (!empty($min_purchase_amount) || $min_purchase_flag == 'N') && !empty($discount_type) && !empty($type)) {
            if (($type === 'add' && empty($discount_id)) || ($type === 'update' && !empty($discount_id))) {
                $this->retail_model->addRetailDiscountDetails($company_id, $discount_code, $discount_id, $discount_type, $discount_amount, $min_purchase_amount, $min_purchase_flag, $start_date, $end_date, $discount_limit_flag, $discount_limit_value, $type,$applies_entire_or_specific,$selected_products);
            }else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getRetailDiscounts() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $data_limit = 0;
        $company_id = $discount_tab = '';
        $search = $draw_table = $length_table = $start = $sorting = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
         if (isset($this->_request['discount_tab'])) {
            $discount_tab = $this->_request['discount_tab'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = $this->_request['start'];
        }
        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }
        if ($discount_tab != 'active' && $discount_tab != 'scheduled' && $discount_tab != 'expired' && $discount_tab != 'all') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        if (!empty($company_id) && !empty($discount_tab)) {
            $this->retail_model->getRetailDiscountDetails($company_id, $discount_tab, $draw_table, $length_table, $start, $sorting);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function deleteRetailDiscounts() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $selected_id_list=[];
        $company_id = $discount_tab = $select_all_flag = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
         if (isset($this->_request['discount_tab'])) {
            $discount_tab = $this->_request['discount_tab'];
        }
        if (isset($this->_request['selected_id_list']) && !empty($this->_request['selected_id_list'])) {
            $selected_id_list = $this->_request['selected_id_list'];
        }
        if (isset($this->_request['select_all_flag'])) {
            $select_all_flag = $this->_request['select_all_flag'];
        }

        if ($discount_tab != 'active' && $discount_tab != 'scheduled' && $discount_tab != 'expired' && $discount_tab != 'all') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        if (!empty($company_id) && !empty($discount_tab) && (!empty($select_all_flag) || !empty($selected_id_list))) {
            $this->retail_model->updateRetailDiscountDetails($company_id, $discount_tab, $selected_id_list, $select_all_flag);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getretailfulfillment() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $search = $draw_table = $length_table = $sorting = $retail_fulfillment_tab = '';
        $start = 0;
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['retail_fulfillment_tab'])) {
            $retail_fulfillment_tab = $this->_request['retail_fulfillment_tab'];
        }
        if (isset($this->_request['draw'])) {
            $draw_table = $this->_request['draw'];
        }
        if (isset($this->_request['length'])) {
            $length_table = $this->_request['length'];
        }
        if (isset($this->_request['start'])) {
            $start = (int)$this->_request['start'];
        }
        if (isset($this->_request['order'])) {
            $sorting = $this->_request['order'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if ($retail_fulfillment_tab != 'all' && $retail_fulfillment_tab != 'unfulfilled' && $retail_fulfillment_tab != 'fulfilled' && $retail_fulfillment_tab != 'cancelled') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($company_id) && !empty($retail_fulfillment_tab)) {
            $this->retail_model->getRetailFulfillmentdetails($company_id, $retail_fulfillment_tab, $draw_table, $length_table, $start, $sorting,$search);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function addorupdatedeletevariants() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $retail_id = $variant_name = $variant_inventory_count = $variant_id = $type = $variant_price = $variant_compare_price = '';
        $type_list = ['add', 'update', 'delete'];
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['retail_id'])) {
            $retail_id = $this->_request['retail_id'];
        }
        if (isset($this->_request['variant_name'])) {
            $variant_name = $this->_request['variant_name'];
        }
        if (isset($this->_request['variant_inventory_count'])) {
            $variant_inventory_count = $this->_request['variant_inventory_count'];
        }
        if (isset($this->_request['variant_price'])) {
            $variant_price = $this->_request['variant_price'];
        }
        if (isset($this->_request['variant_compare_price'])) {
            $variant_compare_price = $this->_request['variant_compare_price'];
        }
        if (isset($this->_request['variant_id'])) {
            $variant_id = $this->_request['variant_id'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }

        // Input validations
//        if ($delete_all_variants_flag=='Y') {
//            if (empty($company_id) || empty($retail_id)) {
//                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
//                $this->response($this->json($error), 200);
//            }else{
//                $this->retail_model->deleteallvariantsindb($company_id, $retail_id);
//            }
//        } else {
            if (empty($company_id) || empty($retail_id) || !in_array($type, $type_list) || ($type != "delete" && empty($variant_name)) || ($type != "add" && empty($variant_id))) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            } else {
                if ($type == "add") {
                    $this->retail_model->addretailvariantdetails($company_id, $retail_id, $variant_name, $variant_inventory_count, $variant_price,$variant_compare_price);
                } else {
                    $this->retail_model->updateretailvariantdetails($company_id, $retail_id, $variant_name, $variant_inventory_count, $variant_id, $type, $variant_price,$variant_compare_price);
                }
            }
//        }
    }
    
    private function getretailvariants() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $retail_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['retail_id'])) {
            $retail_id = $this->_request['retail_id'];
        }

        // Input validations
        if (!empty($company_id) && !empty($retail_id)) {
            $this->retail_model->getretailvariantdetails($company_id, $retail_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getretailmanagedetails() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $list_type = $parent_id ='';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['list_type'])) {
            $list_type= $this->_request['list_type'];
        }
        if (isset($this->_request['parent_id'])) {
            $parent_id = $this->_request['parent_id'];
        }

        // Input validations
        if (empty($company_id) || empty($list_type)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } else {
            $this->retail_model->getretailmanagelistdetails($company_id, $list_type, $parent_id, 0, '');    
        }
    }
    
    private function getRetailSettings() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id ='';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        // Input validations
        if (!empty($company_id)) {
            $this->retail_model->getretailsettingdetails($company_id,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function saveretailagreement() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $retail_status = $retail_processing_type = $retail_agreement = $update_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['retail_status'])) {
            $retail_status = $this->_request['retail_status'];
        }
        if (isset($this->_request['retail_proceesing_type'])) {
            $retail_processing_type = $this->_request['retail_proceesing_type'];
        }
        if (isset($this->_request['retail_agreement_text'])) {
            $retail_agreement = $this->_request['retail_agreement_text'];
        }
        if (isset($this->_request['update_type'])) {
            $update_type = $this->_request['update_type'];
        }

        // Input validations
        if (empty($company_id) || empty($update_type)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } else {
            $this->retail_model->updateRetailSettings($company_id,$retail_status, $retail_processing_type,$retail_agreement,$update_type);    
        }
    }
    
    private function getproducts() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $search_term = "";
        $limit = $discount_amount = 0;
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['limit'])) {
            $limit = $this->_request['limit'];
        }
        if (isset($this->_request['search_term'])) {
            $search_term = $this->_request['search_term'];
        }
        if (isset($this->_request['discount_amount'])) {
            $discount_amount = $this->_request['discount_amount'];
        }
        if (!empty($company_id) ) {
            $this->retail_model->getproductsinDB($company_id,$limit,$search_term,$discount_amount,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function addDiscountProducts(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id=$discount_id='';
        $product_id=[];
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['discount_id'])) {
            $discount_id = $this->_request['discount_id'];
        }
        if (isset($this->_request['product_id'])) {
            $product_id = $this->_request['product_id'];
        }
        if (!is_array($product_id) || (is_array($product_id) && count($product_id)==0)){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($company_id) && !empty($discount_id) && !empty($product_id)) {
            $this->retail_model->addDiscountProductsInDb($company_id,$discount_id,$product_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getIndividualRetail(){
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id= $retail_product_id = $retail_product_type='';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['retail_product_id'])) {
            $retail_product_id = $this->_request['retail_product_id'];
        }
        if (isset($this->_request['retail_product_type'])) {
            $retail_product_type = $this->_request['retail_product_type'];
        }
        if (!empty($company_id) && !empty($retail_product_id)) {
        $this->retail_model->getretailindividualdetail($company_id,$retail_product_id,0,'','');
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function copyRetail(){
        if($this->get_request_method()!=="POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $retail_product_id = $list_type = $retail_template_flag = '';
        $level = 'L1';
        $from = 'L';        //from : L - List page, T - Template, C - Copy for first time
        
        if(isset($this->_request['company_id'])){
            $company_id=$this->_request['company_id'];
        }
        if(isset($this->_request['retail_product_id'])){
            $retail_product_id=$this->_request['retail_product_id'];
        }
        if(isset($this->_request['list_type'])){
            $list_type=$this->_request['list_type'];
        }
        if(isset($this->_request['level']) && !empty($this->_request['level'])){
            $level=$this->_request['level'];
        }
        if(isset($this->_request['retail_template_flag'])){
            $retail_template_flag = $this->_request['retail_template_flag'];
        }
        if(isset($this->_request['from']) && !empty($this->_request['from'])){
            $from=$this->_request['from'];
        }
        
        if(!empty($company_id) && !empty($retail_product_id) && !empty($list_type)){
            $this->retail_model->copyRetailDetails($company_id, $retail_product_id, $list_type, $retail_template_flag, $level, 0, $from);  //last parameter callback => 0 - echo & exit, 1 - return
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updateRetailSortingOrder() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $retail_product_id = $retail_product_sort_order = $parent_id = '';
        $list_type = 'P';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['retail_product_id'])) {
            $retail_product_id = $this->_request['retail_product_id'];
        }
        if (isset($this->_request['retail_product_sort_order'])) {
            $retail_product_sort_order = $this->_request['retail_product_sort_order'];
        }
        if (isset($this->_request['parent_id'])) {
            $parent_id = $this->_request['parent_id'];
        }
        if (isset($this->_request['list_type']) && !empty($this->_request['list_type'])) {
            $list_type = $this->_request['list_type'];
        }

        if (!empty($company_id) && !empty($retail_product_id) && !empty($retail_product_sort_order) && !empty($list_type)) {
            $this->retail_model->updateRetailSortingDetails($company_id, $retail_product_id, $retail_product_sort_order, $parent_id, $list_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
//    delete retail product
    private function deleteretailproduct() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $retail_product_id = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['retail_product_id'])) {
            $retail_product_id = $this->_request['retail_product_id'];
        }

        if (!empty($company_id) && !empty($retail_product_id)) {
            $this->retail_model->deleteretailproductinDB($company_id, $retail_product_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function createRetailFulfillmentCSV() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $search = $tab = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['search'])) {
            $search = $this->_request['search'];
        }
        if (isset($this->_request['fulfillment_tab'])) {
            $tab = $this->_request['fulfillment_tab'];
        }
        if ($tab != 'all' && $tab != 'unfulfilled' && $tab != 'fulfilled' && $tab != 'cancelled') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($company_id) && !empty($tab)) {
            $this->retail_model->createRetailFulfillmentCSVINDB($company_id, $tab, $search);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function changefulfillmentstatus() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $fulfillment_id = $checkout_id = $note = $fulfillment_status = $item_name = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['fulfillment_id'])) {
            $fulfillment_id = $this->_request['fulfillment_id'];
        }
        if (isset($this->_request['note'])) {
            $note = trim($this->_request['note']);
        }
        if (isset($this->_request['checkout_id'])) {
            $checkout_id = $this->_request['checkout_id'];
        }
        if (isset($this->_request['fulfillment_status'])) {
            $fulfillment_status = $this->_request['fulfillment_status'];
        }
        if (isset($this->_request['item_name'])) {
            $item_name = $this->_request['item_name'];
        }
        if (!empty($company_id) && !empty($checkout_id) && !empty($fulfillment_id)) {
            $this->retail_model->changefulfillmentstatusINDB($company_id,$checkout_id,$fulfillment_id,$note,$fulfillment_status,$item_name);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
//    private function getorderdetails() {
//        if ($this->get_request_method() != "POST") {
//            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
//            $this->response($this->json($error), 406);
//        }
//
//        $company_id = $order_id ='';
//
//        if (isset($this->_request['company_id'])) {
//            $company_id = $this->_request['company_id'];
//        }
//        if (isset($this->_request['order_id'])) {
//            $order_id = $this->_request['order_id'];
//        }
//        if (!empty($company_id) && !empty($order_id)) {
//            $this->retail_model->getorderdetailsINDB($company_id,$order_id);
//        } else {
//            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
//            $this->response($this->json($error), 200);
//        }
//    }
    
    private function updatefulfillmentnote() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $checkout_id = $note = $type = $note_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['note'])) {
            $note = $this->_request['note'];
        }
        if (isset($this->_request['checkout_id'])) {
            $checkout_id = $this->_request['checkout_id'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['note_id'])) {
            $note_id = $this->_request['note_id'];
        }
        if (!empty($company_id) && !empty($type)) {
            $this->retail_model->updatefulfillmentnoteINDB($company_id,$checkout_id,$note,$type,$note_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getRetailOrderDetails(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $order_id = '';
        $call_back = 0;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['order_id'])) {
            $order_id = $this->_request['order_id'];
        }
        
        if(!empty($company_id) && !empty($order_id)){
            $this->retail_model->getRetailOrderDetails($company_id, $order_id, $call_back);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        
    }
    
    private function retailEditorderDetails(){
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        // INITIALIZATION OF VARIABLES
        $company_id = '';
        $retail_order_details = $retail_order = [];
        $callBack = 0;

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['order'])) {
            $retail_order = json_decode($this->_request['order'],true);
        }
        if (isset($this->_request['order_details'])) {
            $retail_order_details = json_decode($this->_request['order_details'],true);
        }
        
        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($retail_order) && !empty($retail_order_details)) {
            $this->retail_model->retailEditorderDetailsInDb($company_id, $retail_order, $retail_order_details, $callBack);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function refundRetailOrders(){
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        // INITIALIZATION OF VARIABLES
        $company_id = '';
        $retail_order_details = $retail_order = [];

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['order'])) {
            $retail_order = json_decode($this->_request['order'],true);
        }
        if (isset($this->_request['order_details'])) {
            $retail_order_details = json_decode($this->_request['order_details'],true);
        }
        
        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($retail_order) && !empty($retail_order_details)) {
            $this->retail_model->refundRetailOrdersInDb($company_id, $retail_order, $retail_order_details);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function updateVariantSortOrder(){
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        // INITIALIZATION OF VARIABLES
        $company_id = $sort_id=$variant_id=$retail_id='';
        // REQUESTING THE PARAMETERS
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['sort_id'])) {
            $sort_id = $this->_request['sort_id'];
        }
        if (isset($this->_request['variant_id'])) {
            $variant_id = $this->_request['variant_id'];
        }
        if (isset($this->_request['retail_id'])) {
            $retail_id = $this->_request['retail_id'];
        }
        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($sort_id) && !empty($variant_id)&& !empty($retail_id)) {
            $this->retail_model->updateVariantSortOrder($company_id, $sort_id, $variant_id,$retail_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function getRetailProductTemplates() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = '';
        $template_type = 'C';
                
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if(isset($this->_request['template_type']) && !empty($this->_request['template_type'])){
            $template_type = $this->_request['template_type'];
        }
        
        $this->retail_model->getretailmanagelistdetails($company_id, 'P', '', 1, $template_type);   //1 - from template flag true, 0 - from template flag false
    }
    
    //Retail open tabs Initially
    private function openRetailTabs() {

        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (!empty($company_id)) {
            $this->retail_model->getRetailTabs($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    //Copy from one to another studio retail details
    private function copyRetailFromStudio(){
        
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $from_company = $company_id = '';

        if (isset($this->_request['from_company_id'])) {
            $from_company = $this->_request['from_company_id'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        
        if (!empty($from_company) && !empty($company_id)) {
            $this->retail_model->copyRetailDetailsForStudio($from_company, $company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function checkbundleid(){
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (!empty($company_id)) {
            $this->whitelabel_model->checkbundleidINDB($company_id,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getwldashboarddetails(){
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (!empty($company_id)) {
            $this->whitelabel_model->getwldashboarddetailsINDB($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function buildtheapp(){
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $app_id ='';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id= $this->_request['app_id'];
        }
        if (!empty($company_id)) {
            $this->whitelabel_model->buildtheappINDB($company_id,$app_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function updatebundleid(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $bundle_id ='';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['bundle_id'])) {
            $bundle_id = $this->_request['bundle_id'];
        }
        if (!empty($bundle_id) && !empty($company_id)) {
            $this->whitelabel_model->updatebundleidINDB($company_id,$bundle_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function uploadwhitelabelcert(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $ios_development_cer = $ios_development_cer_decoded = $ios_account_type = '';        
        $ios_distribution_cer =$aps_development_cer = $aps_Production_cer=$AppName_development_cer= $AppName_distribution_cer= "";
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['ios_development_cer'])) {
            $ios_development_cer = $this->_request['ios_development_cer'];
        }
        if (isset($this->_request['ios_distribution_cer'])) {
            $ios_distribution_cer = $this->_request['ios_distribution_cer'];
        }
        if (isset($this->_request['aps_development_cer'])) {
            $aps_development_cer = $this->_request['aps_development_cer'];
        }
        if (isset($this->_request['aps_Production_cer'])) {
            $aps_Production_cer = $this->_request['aps_Production_cer'];
        }
        if (isset($this->_request['AppName_development_cer'])) {
            $AppName_development_cer = $this->_request['AppName_development_cer'];
        }
        if (isset($this->_request['AppName_distribution_cer'])) {
            $AppName_distribution_cer = $this->_request['AppName_distribution_cer'];
        }
        $ios_distribution_cer_name =$aps_development_cer_name = $aps_Production_cer_name=$AppName_development_cer_name= $AppName_distribution_cer_name=$ios_development_cer_name= "";
        //names
        if (isset($this->_request['ios_development_cer_name'])) {
            $ios_development_cer_name = $this->_request['ios_development_cer_name'];
        }
        if (isset($this->_request['ios_distribution_cer_name'])) {
            $ios_distribution_cer_name = $this->_request['ios_distribution_cer_name'];
        }
        if (isset($this->_request['aps_development_cer_name'])) {
            $aps_development_cer_name = $this->_request['aps_development_cer_name'];
        }
        if (isset($this->_request['aps_Production_cer_name'])) {
            $aps_Production_cer_name = $this->_request['aps_Production_cer_name'];
        }
        if (isset($this->_request['AppName_development_cer_name'])) {
            $AppName_development_cer_name = $this->_request['AppName_development_cer_name'];
        }
        if (isset($this->_request['AppName_distribution_cer_name'])) {
            $AppName_distribution_cer_name = $this->_request['AppName_distribution_cer_name'];
        }
        if (isset($this->_request['ios_account_type'])) {
            $ios_account_type = $this->_request['ios_account_type'];
        }
        $this->whitelabel_model->uploadwhitelabelcertINDB($company_id,$ios_development_cer,$ios_distribution_cer,$aps_development_cer,$aps_Production_cer,$AppName_development_cer,$AppName_distribution_cer,$ios_distribution_cer_name,$aps_development_cer_name,$aps_Production_cer_name,$AppName_development_cer_name,$AppName_distribution_cer_name,$ios_development_cer_name,$ios_account_type);
        
    }
    
    //get student list for chat
    private function getChatList(){
        
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $limit = $search_term = $from_tab = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['limit'])) {
            $limit = $this->_request['limit'];
        }
        if (isset($this->_request['searchterm'])) {
            $search_term = $this->_request['searchterm'];
        }
        if (isset($this->_request['from_tab'])) {
            $from_tab = $this->_request['from_tab'];
        }
        if (!empty($company_id) && !empty($from_tab) && $from_tab !== "S") {
            $this->chat_model->getChatListINDB($company_id,$limit,$search_term,$from_tab);
        }else if(!empty($company_id) && !empty($from_tab) && $from_tab === "S" && !empty($search_term)){ 
            $this->chat_model->getSentListINDB($company_id,$search_term);
        }else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getStudentListForChat(){
        
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id  ='';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (!empty($company_id)) {
            $this->chat_model->getStudentListForChatINDB($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getChatConversation(){
        
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $student_id = $limit  = '';
        $last_view_message_id = $last_view_message_datetime = 0;
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['limit'])) {
            $limit = $this->_request['limit'];
        }
        if (isset($this->_request['last_view_message_id'])) {
            $last_view_message_id = $this->_request['last_view_message_id'];
        }
        if (isset($this->_request['last_view_message_datetime'])) {
            $last_view_message_datetime = $this->_request['last_view_message_datetime'];
        }
        if (!empty($company_id) && !empty($student_id) ) {
            $this->chat_model->getChatConversationINDB($company_id,$student_id,$last_view_message_id,$last_view_message_datetime);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function sendchatmessage(){

        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $student_id =  $message_from = $message_text = '';
        $last_message_id = $last_messagedatetime = 0;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['message_from'])) {
            $message_from = $this->_request['message_from'];
        }
        if (isset($this->_request['message_text'])) {
            $message_text = $this->_request['message_text'];
        }
        if (isset($this->_request['last_message_id'])) {
            $last_message_id = $this->_request['last_message_id'];
        }
        if (isset($this->_request['last_messagedatetime'])) {
            $last_messagedatetime = $this->_request['last_messagedatetime'];
        }
        
        if (!empty($company_id) && !empty($student_id) && !empty($message_text) && !empty($message_from)) {
            $this->chat_model->sendchatmessageINDB($company_id,$student_id,$message_text,$message_from,$last_message_id,$last_messagedatetime);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function communicationIntervalCall(){
        
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $current_page =  $selected_student_id = $read_count = $search_term =$from_tab = '';
        $last_msg_id = $last_messagedatetime = 0;
        $unread_count =$student_last_msg_id =0;

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['conv_current_page'])) {
            $current_page = $this->_request['conv_current_page'];
        }
        if (isset($this->_request['selected_student_id'])) {
            $selected_student_id = $this->_request['selected_student_id'];
        }
        if (isset($this->_request['last_msg_id'])) {
            $last_msg_id = $this->_request['last_msg_id'];
        }
        if (isset($this->_request['last_messagedatetime'])) {
            $last_messagedatetime = $this->_request['last_messagedatetime'];
        }
        if (isset($this->_request['student_last_msg_id'])) {
            $student_last_msg_id = $this->_request['student_last_msg_id'];
        }
        if (isset($this->_request['unread_count'])) {
            $unread_count = $this->_request['unread_count'];
        }
        if (isset($this->_request['search_term'])) {
            $search_term = $this->_request['search_term'];
        }
        if (isset($this->_request['from_tab'])) {
            $from_tab = $this->_request['from_tab'];
        }
        
        if (!empty($company_id) && !empty($from_tab)) {
            $this->chat_model->communicationIntervalCallINDB($company_id,$current_page,$selected_student_id,$last_msg_id,$last_messagedatetime,$unread_count,$student_last_msg_id,$search_term,$from_tab);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function sendmultiplechatmessage(){
        
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $student_id =  $message_from = $message_text = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['student_id'])) {
            $student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['message_from'])) {
            $message_from = $this->_request['message_from'];
        }
        if (isset($this->_request['message_text'])) {
            $message_text = $this->_request['message_text'];
        }
        
        if (!empty($company_id) && !empty($student_id) && !empty($message_text) && !empty($message_from)) {
            $this->chat_model->sendmultiplechatmessageINDB($company_id,$student_id,$message_text,$message_from);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }

    private function changeEmailSubscriptionStatus() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $email = $status =  $company_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['email'])) {
            $email = $this->_request['email'];
        }
        if (isset($this->_request['status'])) {
            $status = $this->_request['status'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($email) && !empty($status)) {
            $this->updateEmailSubscriptionStatus($email,$status,$company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function updateEventEnabledStatus() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_enabled_status = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_enabled_status'])) {
            $event_enabled_status = $this->_request['event_enabled_status'];
        }

        // Input validations
        if (empty($company_id) || empty($event_enabled_status)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        } else {
            $this->event_model->updateEventEnabledStatusInDB($company_id,$event_enabled_status);
        }
    }
    
    private function savepossettings() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $miscstatus = $retailstatus =  $company_id = $eventstatus = $membershipstatus =  $trialstatus = $leadsstatus = $misc_processingfee_type = '';
        $leads_email_status = $misc_payment_code =  $retail_payment_code = $event_payment_code = $membership_payment_code =  $trial_payment_code = $leads_payment_code = '';
        $misc_agreement = $type = "";
        // REQUESTING THE PARAMETERS
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['miscstatus'])) {
            $miscstatus = $this->_request['miscstatus'];
        }
        if (isset($this->_request['eventstatus'])) {
            $eventstatus = $this->_request['eventstatus'];
        }
        if (isset($this->_request['retailstatus'])) {
            $retailstatus = $this->_request['retailstatus'];
        }
        if (isset($this->_request['membershipstatus'])) {
            $membershipstatus = $this->_request['membershipstatus'];
        }
        if (isset($this->_request['trialstatus'])) {
            $trialstatus = $this->_request['trialstatus'];
        }
        if (isset($this->_request['leadsstatus'])) {
            $leadsstatus = $this->_request['leadsstatus'];
        }
        if (isset($this->_request['misc_processingfee_type'])) {
            $misc_processingfee_type = $this->_request['misc_processingfee_type'];
        }
        if (isset($this->_request['leads_email_status'])) {
            $leads_email_status = $this->_request['leads_email_status'];
        }
        if (isset($this->_request['misc_payment_code'])) {
            $misc_payment_code = $this->_request['misc_payment_code'];
        }
        if (isset($this->_request['retail_payment_code'])) {
            $retail_payment_code = $this->_request['retail_payment_code'];
        }
        if (isset($this->_request['event_payment_code'])) {
            $event_payment_code = $this->_request['event_payment_code'];
        }
        if (isset($this->_request['membership_payment_code'])) {
            $membership_payment_code = $this->_request['membership_payment_code'];
        }
        if (isset($this->_request['trial_payment_code'])) {
            $trial_payment_code = $this->_request['trial_payment_code'];
        }
        if (isset($this->_request['leads_payment_code'])) {
            $leads_payment_code = $this->_request['leads_payment_code'];
        }
        if (isset($this->_request['misc_agreement'])) {
            $misc_agreement = $this->_request['misc_agreement'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($type) && !empty($leads_payment_code) && !empty($trial_payment_code) && !empty($membership_payment_code) && !empty($event_payment_code) && !empty($retail_payment_code) && !empty($misc_payment_code) && !empty($leads_email_status) && !empty($misc_processingfee_type) && !empty($leadsstatus) && !empty($trialstatus) && !empty($membershipstatus) && !empty($retailstatus) && !empty($eventstatus) && !empty($miscstatus) && !empty($company_id)) {
            $this->savepossettingsINDB($company_id, $leads_payment_code, $trial_payment_code, $membership_payment_code, $event_payment_code, $retail_payment_code, $misc_payment_code, $leads_email_status
                    , $misc_processingfee_type, $leadsstatus, $trialstatus, $membershipstatus, $retailstatus, $eventstatus, $miscstatus,$type,$misc_agreement);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function getpossettingdetails() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = '';

        // REQUESTING THE PARAMETERS
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id)) {
            $this->getpossettingdetailsINDB($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    // miscellaneous
     private function getmiscpaymentHistoryDetails() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $misc_order_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['misc_order_id'])) {
            $misc_order_id = $this->_request['misc_order_id'];
        }        
        if (!empty($company_id) && !empty($misc_order_id)) {
           $this->getmiscpaymenthistory($company_id, $misc_order_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function applypaymentcreditformisc() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $misc_payment_id = $applycredit_payment_method = $misc_order_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['misc_payment_id'])) {
            $misc_payment_id = $this->_request['misc_payment_id'];
        }
        if (isset($this->_request['applycredit_payment_method'])) {
            $applycredit_payment_method = $this->_request['applycredit_payment_method'];
        }
        if (isset($this->_request['misc_order_id'])) {
            $misc_order_id = $this->_request['misc_order_id'];
        }
        if (!empty($company_id) && !empty($misc_payment_id) &&!empty($applycredit_payment_method) && !empty($misc_order_id)) {
           $this->applypaymentcreditformiscINDB($company_id, $misc_payment_id,$applycredit_payment_method,$misc_order_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function refundmisc() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $misc_payment_id = $refund_payment_method = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['misc_payment_id'])) {
            $misc_payment_id = $this->_request['misc_payment_id'];
        }
        if (isset($this->_request['refund_payment_method'])) {
            $refund_payment_method = $this->_request['refund_payment_method'];
        }
        if (isset($this->_request['misc_order_id'])) {
            $misc_order_id = $this->_request['misc_order_id'];
        }
        if (!empty($company_id) && !empty($misc_payment_id) &&!empty($refund_payment_method) && !empty($misc_order_id)) {
           $this->refundmiscINDB($company_id, $misc_payment_id,$refund_payment_method,$misc_order_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    
    private function reRunMiscPayment() {
       // Cross validation if the request method is POST else it will return "Not Acceptable" status
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id = $reg_id = $payment_id = '';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['reg_id'])) {
           $reg_id = $this->_request['reg_id'];
       }
       if (isset($this->_request['payment_id'])) {
           $payment_id = $this->_request['payment_id'];
       }

       // Input validations
       if (!empty($company_id) && !empty($reg_id) && !empty($payment_id)) {
               $this->reRunMiscPaymentINDB($company_id, $reg_id, $payment_id);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   //   update note misc
   private function updatemiscnotes() {
       // Cross validation if the request method is POST else it will return "Not Acceptable" status
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id = $note_id = $misc_order_id = $note_text =  '';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['note_id'])) {
           $note_id = $this->_request['note_id'];
       }
       if (isset($this->_request['type'])) {
           $type = $this->_request['type'];
       }
       if (isset($this->_request['misc_order_id'])) {
           $misc_order_id = $this->_request['misc_order_id'];
       }
       if (isset($this->_request['note_text'])) {
           $note_text = $this->_request['note_text'];
       }

       // Input validations
       if (!empty($company_id) && !empty($type) && !empty($misc_order_id)) {
               $this->updatemiscnotesINDB($company_id, $type, $misc_order_id,$note_text,$note_id);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function getmiscnotes() {
       // Cross validation if the request method is POST else it will return "Not Acceptable" status
       if ($this->get_request_method() != "GET") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $misc_order_id =  '';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['misc_order_id'])) {
           $misc_order_id = $this->_request['misc_order_id'];
       }

       // Input validations
       if (!empty($company_id)  && !empty($misc_order_id) ) {
               $this->getmiscnotesINDB($company_id,$misc_order_id);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function appinfotomystudio(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $ios_developerid =  $ios_teamid =  '';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['ios_developerid'])) {
           $ios_developerid = $this->_request['ios_developerid'];
       }
       if (isset($this->_request['ios_teamid'])) {
           $ios_teamid = $this->_request['ios_teamid'];
       }

       // Input validations
       if (!empty($company_id)) {
               $this->whitelabel_model->saveappinfo($company_id,$ios_developerid,$ios_teamid);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function updateaccounttype(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $wl_type = $bundle_id = '';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['wl_type'])) {
           $wl_type = $this->_request['wl_type'];
       }
       if (isset($this->_request['bundle_id'])) {
           $bundle_id = $this->_request['bundle_id'];
       }

       // Input validations
       if (!empty($company_id)) {
               $this->whitelabel_model->updateaccounttypeINDB($company_id,$wl_type,$bundle_id);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function geticonurl(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
       if ($this->get_request_method() != "GET") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id = $scheme_name = $bundle_id='';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['scheme_name'])) {
           $scheme_name = $this->_request['scheme_name'];
       }
       if (isset($this->_request['bundle_id'])) {
           $bundle_id = $this->_request['bundle_id'];
       }

       // Input validations
       if (!empty($company_id)) {
               $this->whitelabel_model->geticonurlINDB($company_id,$scheme_name,$bundle_id,0);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function updateappinfo(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $appiconname = $bundle_id = $appiconnameandroid = $type =$app_id =  '';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['appiconname'])) {
           $appiconname = $this->_request['appiconname'];
       }
       if (isset($this->_request['appiconnameandroid'])) {
           $appiconnameandroid = $this->_request['appiconnameandroid'];
       }
       if (isset($this->_request['bundle_id'])) {
           $bundle_id = $this->_request['bundle_id'];
       }
       if (isset($this->_request['type'])) {
           $type = $this->_request['type'];
       }
       if (isset($this->_request['app_id'])) {
           $app_id = $this->_request['app_id'];
       }
       // Input validations
       if (!empty($company_id)) {
               $this->whitelabel_model->updateappinfoINDB($company_id,$appiconname,$bundle_id,$appiconnameandroid,$type,$app_id);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function getandroidstoreinfo(){
       if ($this->get_request_method() != "GET") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       
       $company_id =  $app_id = $short_desc = $full_desc= $title= '';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['app_id'])) {
           $app_id = $this->_request['app_id'];
       }
       if (!empty($company_id) ) {
               $this->whitelabel_model->getandroidstoreinfoINDB($company_id,$app_id);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function saveandroidstoreinfo(){
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       
       $company_id =  $app_id = $short_desc = $full_desc= $title=$json_file= $type ='';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['app_id'])) {
           $app_id = $this->_request['app_id'];
       }
       if (isset($this->_request['short_desc'])) {
           $short_desc = $this->_request['short_desc'];
       }
        if (isset($this->_request['full_desc'])) {
           $full_desc = $this->_request['full_desc'];
       }
        if (isset($this->_request['title'])) {
           $title = $this->_request['title'];
       }
       if (isset($this->_request['json_file'])) {
           $json_file = $this->_request['json_file'];
       }
       if (isset($this->_request['type'])) {
           $type = $this->_request['type'];
       }
       if (!empty($company_id)  && !empty($app_id) &&!empty($type) ) {
               $this->whitelabel_model->saveandroidstoreinfoINDB($company_id,$app_id,$short_desc,$full_desc,$title,$json_file,$type);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function savefirebase(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $firebase = $dynamic_url = $bundle_id= $server_key='';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['firebase'])) {
           $firebase = $this->_request['firebase'];
       }
       if (isset($this->_request['dynamic_url'])) {
           $dynamic_url = $this->_request['dynamic_url'];
       }
        if (isset($this->_request['bundle_id'])) {
           $bundle_id = $this->_request['bundle_id'];
       }
       if (isset($this->_request['server_key'])) {
           $server_key = $this->_request['server_key'];
       }
       $service_json = $type = "";
       if (isset($this->_request['service_json'])) {
           $service_json = $this->_request['service_json'];
       }
       if (isset($this->_request['type'])) {
           $type = $this->_request['type'];
       }
       if($type == 'json'){
           $service_json_temp = explode("base64,",$service_json);
           $str = (base64_decode($service_json_temp[1]));
           $json = json_decode($str, true);
           if(!isset($json["client"])){
               $error = array('status' => "Failed", "msg" => "Invalid file!");
                $this->response($this->json($error), 200);
            }
           if (($json["client"][0]['client_info']['android_client_info']['package_name'] != $bundle_id) ) {
                $error = array('status' => "Failed", "msg" => "Bundle ID mismatch!");
                $this->response($this->json($error), 200);
            }
        }
       
       // Input validations
       if (!empty($company_id)  && !empty($bundle_id) ) {
               $this->whitelabel_model->savefirebaseINDB($company_id,$firebase,$dynamic_url,$bundle_id,$server_key,$service_json,$type);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function savemanagerdetails(){
       // Cross validation if the request method is POST else it will return "Not Acceptable" status
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $ios_username =  $ios_password =  $ios_account_type = '';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['ios_username'])) {
           $ios_username = $this->_request['ios_username'];
       }
       if (isset($this->_request['ios_password'])) {
           $ios_password = $this->_request['ios_password'];
       }
       if (isset($this->_request['ios_account_type'])) {
           $ios_account_type = $this->_request['ios_account_type'];
       }

       // Input validations
       if (!empty($company_id)) {
               $this->whitelabel_model->updatemanagerdetails($company_id,$ios_username,$ios_password,$ios_account_type);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }

   private function getstripeemail() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->getStripeEmailFromDB($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function setupIntent(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $buyer_email = $student_id = $payment_method_id = $actual_student_id = $stripe_customer_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['buyer_email'])) {
            $buyer_email = $this->_request['buyer_email'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['student_id'])) {
            $actual_student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['stripe_customer_id'])) {
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        
        if (!empty($company_id) && !empty($payment_method_id) && !empty($buyer_email) && !empty($buyer_first_name) && !empty($buyer_last_name)) {
            $this->setupIntentCreation($company_id, $actual_student_id, $payment_method_id, $buyer_email, $buyer_first_name, $buyer_last_name, $stripe_customer_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function stripeTrialprogramCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_name = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $payment_method_id = $processing_fee_type = $program_length_type = $payment_type =$start_date='';
        $student_id = $participant_id = $fee = $discount = $registration_amount = $payment_amount =  $program_length = 0;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $upgrade_status = 'F';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = $payment_method = '';
        $country = 'US';
        $reg_type_user = 'P';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = $version;
        $intent_method_type = $verification_status = $payment_intent_id = $cc_type = $stripe_customer_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['trial_name'])) {
            $trial_name = $this->_request['trial_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
         if (isset($this->_request['payment_startdate'])) {
            $start_date = $this->_request['payment_startdate'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['lead_source'])) {
            $reg_col4 = $this->_request['lead_source'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['program_length_type'])) {
            $program_length_type = $this->_request['program_length_type'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['program_length'])) {
            $program_length = $this->_request['program_length'];
        }
        if (isset($this->_request['trial_participant_Streetaddress'])) {
            $participant_street = $this->_request['trial_participant_Streetaddress'];
        }
        if (isset($this->_request['trial_participant_City'])) {
            $participant_city = $this->_request['trial_participant_City'];
        }
        if (isset($this->_request['trial_participant_State'])) {
            $participant_state = $this->_request['trial_participant_State'];
        }
        if (isset($this->_request['trial_participant_Zip'])) {
            $participant_zip = $this->_request['trial_participant_Zip'];
        }
        if (isset($this->_request['trial_participant_Country'])) {
            $participant_country = $this->_request['trial_participant_Country'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['intent_method_type'])) {
            $intent_method_type = $this->_request['intent_method_type'];
        }
        if (isset($this->_request['verification_status'])) {
            $verification_status = $this->_request['verification_status'];
        }
        if (isset($this->_request['payment_intent_id'])) {
            $payment_intent_id = $this->_request['payment_intent_id'];
        } 
        if (isset($this->_request['cc_type'])) {    // cc_type - N - New, E - Existing
            $cc_type = $this->_request['cc_type'];
        }
        if (isset($this->_request['stripe_customer_id'])) {    // cc_type - N - New, E - Existing
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        
        if (!empty($company_id) && !empty($trial_id) && !empty($trial_name) &&  (!empty($payment_method_id) || $payment_amount == 0) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) ) {
                $this->trial_model->stripetrialProgramCheckout($company_id, $trial_id, $student_id, $participant_id, $trial_name, $desc,  $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $payment_method_id, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount,  $processing_fee_type, $discount,  $upgrade_status,  $reg_type_user, $reg_version_user,$program_length,$program_length_type,$start_date,$participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $payment_method,$intent_method_type,$verification_status,$payment_intent_id,$cc_type,$stripe_customer_id);
        } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 400);
        }
    }
   
   private function updateappstoreinformation(){
       // Cross validation if the request method is POST else it will return "Not Acceptable" status
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $app_id =   '';

       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['app_id'])) {
           $app_id = $this->_request['app_id'];
       }
       if (isset($this->_request['type'])) {
           $type = $this->_request['type'];
       }
       $localizable_info_name = $localizable_info_policy_url=$localizable_info_subtitle = "";
       if (isset($this->_request['localizable_info_name'])) {
           $localizable_info_name = $this->_request['localizable_info_name'];
       }
       if (isset($this->_request['localizable_info_policy_url'])) {
           $localizable_info_policy_url = $this->_request['localizable_info_policy_url'];
       }
       if (isset($this->_request['localizable_info_subtitle'])) {
           $localizable_info_subtitle = $this->_request['localizable_info_subtitle'];
       }
       $general_info_apple_id =$general_info_description =$general_info_promotional_text =$general_info_support_url = $general_info_marketing_url=$general_info_keywords= "";
       if (isset($this->_request['general_info_apple_id'])) {
           $general_info_apple_id = $this->_request['general_info_apple_id'];
       }
       if (isset($this->_request['general_info_description'])) {
           $general_info_description = $this->_request['general_info_description'];
       }
       if (isset($this->_request['general_info_promotional_text'])) {
           $general_info_promotional_text = $this->_request['general_info_promotional_text'];
       }
       if (isset($this->_request['general_info_support_url'])) {
           $general_info_support_url = $this->_request['general_info_support_url'];
       }
       if (isset($this->_request['general_info_marketing_url'])) {
           $general_info_marketing_url = $this->_request['general_info_marketing_url'];
       }
       if (isset($this->_request['general_info_keywords'])) {
           $general_info_keywords = $this->_request['general_info_keywords'];
       }
       $copyright = "";
       if (isset($this->_request['copyright'])) {
           $copyright = $this->_request['copyright'];
       }
       $trade_rep_contact_info_fname = $trade_rep_contact_info_lname=$trade_rep_contact_info_address =$trade_rep_contact_info_suite =$trade_rep_contact_info_city = $trade_rep_contact_info_state =
              $trade_rep_contact_info_postal_code = $trade_rep_contact_info_phone=$trade_rep_contact_info_country = $trade_rep_contact_info_email= "";
       if (isset($this->_request['trade_rep_contact_info_fname'])) {
           $trade_rep_contact_info_fname = $this->_request['trade_rep_contact_info_fname'];
       }
       if (isset($this->_request['trade_rep_contact_info_lname'])) {
           $trade_rep_contact_info_lname = $this->_request['trade_rep_contact_info_lname'];
       }
       if (isset($this->_request['trade_rep_contact_info_address'])) {
           $trade_rep_contact_info_address = $this->_request['trade_rep_contact_info_address'];
       }
       if (isset($this->_request['trade_rep_contact_info_suite'])) {
           $trade_rep_contact_info_suite = $this->_request['trade_rep_contact_info_suite'];
       }
       if (isset($this->_request['trade_rep_contact_info_city'])) {
           $trade_rep_contact_info_city = $this->_request['trade_rep_contact_info_city'];
       }
       if (isset($this->_request['trade_rep_contact_info_state'])) {
           $trade_rep_contact_info_state = $this->_request['trade_rep_contact_info_state'];
       }
       if (isset($this->_request['trade_rep_contact_info_postal_code'])) {
           $trade_rep_contact_info_postal_code = $this->_request['trade_rep_contact_info_postal_code'];
       }
       if (isset($this->_request['trade_rep_contact_info_phone'])) {
           $trade_rep_contact_info_phone = $this->_request['trade_rep_contact_info_phone'];
       }
       if (isset($this->_request['trade_rep_contact_info_country'])) {
           $trade_rep_contact_info_country = $this->_request['trade_rep_contact_info_country'];
       }
       if (isset($this->_request['trade_rep_contact_info_email'])) {
           $trade_rep_contact_info_email = $this->_request['trade_rep_contact_info_email'];
       }
       $app_review_info_username = $app_review_info_password= $app_review_info_fname= $app_review_info_lname= $app_review_info_email=$app_review_info_note = $app_review_info_phone="";
       if (isset($this->_request['app_review_info_username'])) {
           $app_review_info_username = $this->_request['app_review_info_username'];
       }
       if (isset($this->_request['app_review_info_password'])) {
           $app_review_info_password = $this->_request['app_review_info_password'];
       }
       if (isset($this->_request['app_review_info_fname'])) {
           $app_review_info_fname = $this->_request['app_review_info_fname'];
       }
       if (isset($this->_request['app_review_info_lname'])) {
           $app_review_info_lname = $this->_request['app_review_info_lname'];
       }
       if (isset($this->_request['app_review_info_email'])) {
           $app_review_info_email = $this->_request['app_review_info_email'];
       }
       if (isset($this->_request['app_review_info_phone'])) {
           $app_review_info_phone = $this->_request['app_review_info_phone'];
       }
       if (isset($this->_request['app_review_info_note'])) {
           $app_review_info_note = $this->_request['app_review_info_note'];
       }
       if (isset($this->_request['company_code'])) {
           $company_code = $this->_request['company_code'];
       }
       
       
       // Input validations
       if (!empty($app_id)) {
                   $this->whitelabel_model->updateappstoreinformationINDB($company_id,$app_id,$localizable_info_name , $localizable_info_policy_url,$localizable_info_subtitle,
                       $general_info_apple_id ,$general_info_description ,$general_info_promotional_text ,$general_info_support_url ,
                       $general_info_marketing_url,$general_info_keywords,$copyright,
                       $trade_rep_contact_info_fname ,$trade_rep_contact_info_lname,$trade_rep_contact_info_address,$trade_rep_contact_info_suite,
                       $trade_rep_contact_info_city,$trade_rep_contact_info_state,
                        $trade_rep_contact_info_postal_code,$trade_rep_contact_info_phone,$trade_rep_contact_info_country ,$trade_rep_contact_info_email,
                       $app_review_info_username ,$app_review_info_password,$app_review_info_fname,$app_review_info_lname,$app_review_info_email,
                       $app_review_info_note,$app_review_info_phone,$company_code,$type);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function getappstoreinfo(){
        if ($this->get_request_method() != "GET") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $app_id = '';
       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['app_id'])) {
           $app_id = $this->_request['app_id'];
       }
       if(!empty($company_id) && !empty($company_id)){
             $this->whitelabel_model->getappstoreinfofromdb($company_id,$app_id,"I");
       }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
       }
   }
   
   private function getappstoreinfoandroid(){
        if ($this->get_request_method() != "GET") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $app_id = '';
       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['app_id'])) {
           $app_id = $this->_request['app_id'];
       }
       if(!empty($company_id) && !empty($company_id)){
             $this->whitelabel_model->getappstoreinfofromdb($company_id,$app_id,"A");
       }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
       }
   }
   
   private function getiosscreenshoturl(){
        if ($this->get_request_method() != "GET") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $scheme_name = $bundle_id='';
       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['scheme_name'])) {
           $scheme_name = $this->_request['scheme_name'];
       }
       if (isset($this->_request['type'])) {
           $type = $this->_request['type'];
       }
       if (isset($this->_request['bundle_id'])) {
           $bundle_id = $this->_request['bundle_id'];
       }
       if(!empty($company_id) && !empty($company_id)){
             $this->whitelabel_model->getiosscreenshoturlS3($scheme_name,$type,$bundle_id);
       }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
       }
   }
   
   private function uploadandroidscreenshots(){
        if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $scheme_name = '';
       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['scheme_name'])) {
           $scheme_name = $this->_request['scheme_name'];
       }
       $and_phone_file1 = $and_phone_file2 = $and_phone_file3 = $and_phone_file4 = "";
       if (isset($this->_request['and_phone_file1'])) {
           $and_phone_file1 = $this->_request['and_phone_file1'];
       }
       if (isset($this->_request['and_phone_file2'])) {
           $and_phone_file2 = $this->_request['and_phone_file2'];
       }
       if (isset($this->_request['and_phone_file3'])) {
           $and_phone_file3 = $this->_request['and_phone_file3'];
       }
       if (isset($this->_request['and_phone_file4'])) {
           $and_phone_file4 = $this->_request['and_phone_file4'];
       }
       $and_tab_file1 = $and_tab_file2 = $and_tab_file3 = $and_tab_file4 = "";
       if (isset($this->_request['and_tab_file1'])) {
           $and_tab_file1 = $this->_request['and_tab_file1'];
       }
       if (isset($this->_request['and_tab_file2'])) {
           $and_tab_file2 = $this->_request['and_tab_file2'];
       }
       if (isset($this->_request['and_tab_file3'])) {
           $and_tab_file3 = $this->_request['and_tab_file3'];
       }
       if (isset($this->_request['and_tab_file4'])) {
           $and_tab_file4 = $this->_request['and_tab_file4'];
       }
       
       if(!empty($company_id) && !empty($scheme_name)){
             $this->whitelabel_model->uploadandroidscreenshotsINDB($company_id,$scheme_name,
                     $and_phone_file1,$and_phone_file2,$and_phone_file3,$and_phone_file4,
                     $and_tab_file1,$and_tab_file2,$and_tab_file3,$and_tab_file4);
       }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
       }
   }
   
   private function uploadiosscreenshots(){
        if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $scheme_name = '';
       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['scheme_name'])) {
           $scheme_name = $this->_request['scheme_name'];
       }
       $_5inch_file1 = $_5inch_file2 = $_5inch_file3 = $_5inch_file4 = "";
       if (isset($this->_request['5inch_file1'])) {
           $_5inch_file1 = $this->_request['5inch_file1'];
       }
       if (isset($this->_request['5inch_file2'])) {
           $_5inch_file2 = $this->_request['5inch_file2'];
       }
       if (isset($this->_request['5inch_file3'])) {
           $_5inch_file3 = $this->_request['5inch_file3'];
       }
       if (isset($this->_request['5inch_file4'])) {
           $_5inch_file4 = $this->_request['5inch_file4'];
       }
       $_6inch_file1 = $_6inch_file2 = $_6inch_file3 = $_6inch_file4 = "";
       if (isset($this->_request['6inch_file1'])) {
           $_6inch_file1 = $this->_request['6inch_file1'];
       }
       if (isset($this->_request['6inch_file2'])) {
           $_6inch_file2 = $this->_request['6inch_file2'];
       }
       if (isset($this->_request['6inch_file3'])) {
           $_6inch_file3 = $this->_request['6inch_file3'];
       }
       if (isset($this->_request['6inch_file4'])) {
           $_6inch_file4 = $this->_request['6inch_file4'];
       }
       $ipad_file1 = $ipad_file2 = $ipad_file3 = $ipad_file4 = "";
       if (isset($this->_request['ipad_file1'])) {
           $ipad_file1 = $this->_request['ipad_file1'];
       }
       if (isset($this->_request['ipad_file2'])) {
           $ipad_file2 = $this->_request['ipad_file2'];
       }
       if (isset($this->_request['ipad_file3'])) {
           $ipad_file3 = $this->_request['ipad_file3'];
       }
       if (isset($this->_request['ipad_file4'])) {
           $ipad_file4 = $this->_request['ipad_file4'];
       }
       
       if(!empty($company_id) && !empty($scheme_name)){
             $this->whitelabel_model->uploadiosscreenshotsINDB($company_id,$scheme_name,$_5inch_file1,$_5inch_file2,$_5inch_file3,$_5inch_file4,$_6inch_file1,$_6inch_file2,$_6inch_file3,$_6inch_file4
                     ,$ipad_file1,$ipad_file2,$ipad_file3,$ipad_file4);
       }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
       }
   }
   
   private function iosimageupload(){
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id =  $ios_zip = $splash_flag = $hex_value = $icon_content = $bundle_id = $type = $scheme_name= '';
       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['ios_zip'])) {
           $ios_zip = $this->_request['ios_zip'];
       }
       if (isset($this->_request['splash_flag'])) {
           $splash_flag = $this->_request['splash_flag'];
       }
       if (isset($this->_request['hex_value'])) {
           $hex_value = $this->_request['hex_value'];
       }
       if (isset($this->_request['icon_content'])) {
           $icon_content = $this->_request['icon_content'];
       }
       if (isset($this->_request['ios_bundle_id'])) {
           $bundle_id = $this->_request['ios_bundle_id'];
       }
       if (isset($this->_request['type'])) {
           $type = $this->_request['type'];
       }
       if (isset($this->_request['scheme_name'])) {
           $scheme_name = $this->_request['scheme_name'];
       }
       if (!empty($company_id)  && !empty($icon_content)) {
               $this->whitelabel_model->createiconsandsplashios($company_id,$splash_flag,$icon_content,$hex_value,$ios_zip,$bundle_id,$type,$scheme_name);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function processskeletonapk(){
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id = $app_id='';
       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['app_id'])) {
           $app_id = $this->_request['app_id'];
       }
       if (!empty($company_id)) {
               $this->whitelabel_model->processskeletonapkINDB($company_id,$app_id);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function catchmanualmeta(){
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id = $app_id='';
       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['app_id'])) {
           $app_id = $this->_request['app_id'];
       }
       if (isset($this->_request['type'])) {
           $type = $this->_request['type'];
       }
       if (isset($this->_request['flag'])) {
           $flag = $this->_request['flag'];
       }
       if (!empty($company_id)) {
               $this->whitelabel_model->catchmanualmetaINDB($company_id,$app_id,$type,$flag);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
   
   private function catchmanualscreen(){
       if ($this->get_request_method() != "POST") {
           $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
           $this->response($this->json($error), 406);
       }
       $company_id = $app_id='';
       if (isset($this->_request['company_id'])) {
           $company_id = $this->_request['company_id'];
       }
       if (isset($this->_request['app_id'])) {
           $app_id = $this->_request['app_id'];
       }
       if (isset($this->_request['type'])) {
           $type = $this->_request['type'];
       }
       if (isset($this->_request['flag'])) {
           $flag = $this->_request['flag'];
       }
       if (!empty($company_id)) {
               $this->whitelabel_model->catchmanualscreenINDB($company_id,$app_id,$type,$flag);
       } else {
           $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
           $this->response($this->json($error), 200);
       }
   }
    // Update stripe payment method
    private function updateStripePayoutMethod() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $method_type = $card_token_value = $user_id = '';
        $routing_no = $account_no = $acc_holder_name = $acc_holder_type = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['user_id'])) {
            $user_id = $this->_request['user_id'];
        }
        if (isset($this->_request['method_type'])) {
            $method_type = $this->_request['method_type'];
        }
        if (isset($this->_request['card_token_value'])) {
            $card_token_value = $this->_request['card_token_value'];
        }
        if (isset($this->_request['routing_no'])) {
            $routing_no = $this->_request['routing_no'];
        }
        if (isset($this->_request['account_no'])) {
            $account_no = $this->_request['account_no'];
        }
        if (isset($this->_request['acc_holder_name'])) {
            $acc_holder_name = $this->_request['acc_holder_name'];
        }
        if (isset($this->_request['acc_holder_type'])) {
            $acc_holder_type = $this->_request['acc_holder_type'];
        }

        if (!empty($company_id) && !empty($method_type)) {
            if ($method_type == 'C') { // card
                if (!empty($card_token_value)) {
                    $this->updatestripeExternalCardAccount($company_id,$user_id,$card_token_value);
                }else{
                    $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 200);
                }
            }else if($method_type == 'B'){ // Bank Account
                if (!empty($routing_no) && !empty($account_no) && !empty($acc_holder_name) && !empty($acc_holder_type)) {
                    $this->createstripeExternalBankAccount($company_id,$user_id,$routing_no,$account_no,$acc_holder_name,$acc_holder_type);
                }else{
                    $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 200);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Method Type");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    //  Get stripe payout details
    private function getStripePayoutDetails() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (!empty($company_id)) {
            $this->getStripePayoutDetailsfromDB($company_id,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    // Stripe event portal checkout
    private function stripeEventPortalCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_name = $sub_events_id = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $processing_fee_type = $payment_frequency = '';
        $student_id = $participant_id = $fee = $discount = $registration_amount = $payment_amount = $total_order_quantity = $total_order_amount = 0;
        $quantity = 1;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $event_array = $payment_array = [];
        $upgrade_status = 'F';
        $country = 'US';
        $reg_type_user = 'P';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = $version;
        $payment_method_id = $stripe_customer_id = $cc_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['event_name'])) {
            $event_name = $this->_request['event_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if (isset($this->_request['event_array'])) {
            $event_array = $this->_request['event_array'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['fee'])) {
            $fee = $this->_request['fee'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
        if (isset($this->_request['quantity'])) {
            $quantity = $this->_request['quantity'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['reg_col4'])) {
            $reg_col4 = $this->_request['reg_col4'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['payment_frequency'])) {
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if (isset($this->_request['event_array'])) {
            $event_array = $this->_request['event_array'];
        }
        if (isset($this->_request['payment_array'])) {
            $payment_array = $this->_request['payment_array'];
        }
        if (isset($this->_request['event_type'])) {
            $event_type = $this->_request['event_type'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['total_order_amount'])) {
            $total_order_amount = $this->_request['total_order_amount'];
        }
        if (isset($this->_request['total_order_quantity'])) {
            $total_order_quantity = $this->_request['total_order_quantity'];
        }
        if(isset($this->_request['payment_method_id'])){
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['cc_type'])) {    // cc_type - N - New, E - Existing
            $cc_type = $this->_request['cc_type'];
        }
        if (isset($this->_request['stripe_customer_id'])) {    // cc_type - N - New, E - Existing
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        if ($event_type == 'S' && $payment_type == 'onetimesingle') {
            $event_array = [];
            $payment_array = [];
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($payment_method_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
                $this->event_model->stripeCreatePortalCheckout($company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, 4, 'CO', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user,$payment_method_id,$cc_type,$stripe_customer_id);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } elseif ($event_type == 'S' && $payment_type == 'singlerecurring') {
            $event_array = [];
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($payment_method_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($payment_array)) {
                $this->event_model->stripeCreatePortalCheckout($company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, 'RE', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user,$payment_method_id,$cc_type,$stripe_customer_id);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } elseif ($event_type == 'M' && $payment_type == 'onetimemultiple') {
            $payment_array = [];
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($payment_method_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($event_array)) {
                $this->event_model->stripeCreatePortalCheckout($company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, 4, 'CO', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user,$payment_method_id,$cc_type,$stripe_customer_id);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } elseif ($event_type == 'M' && $payment_type == 'multiplerecurring') {
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($payment_method_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($payment_array) && !empty($event_array)) {
                $this->event_model->stripeCreatePortalCheckout($company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, 'RE', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user,$payment_method_id,$cc_type,$stripe_customer_id);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function stripeMembershipCheckout() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_option_id = $membership_category_title = $membership_title = $membership_desc = $recurring_start_date = '';
        $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code  = $processing_fee_type = $payment_frequency = $membership_start_date = $payment_start_date = $delay_recurring_payment_start_date = $custom_recurring_frequency_period_type = '';
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $student_id = $participant_id = $payment_amount = $processing_fee = $membership_fee = $membership_fee_disc = $signup_fee = $signup_fee_disc = $initial_payment = $custom_recurring_frequency_period_val = 0;
        
        $upgrade_status = 'F';
        $country = '';
        $prorate_first_payment_flg = $delay_recurring_payment_start_flg = 'N';
        $membership_payment_array = ['N', 'W', 'B', 'M', 'A', 'C'];
        $payment_array = [];
        $no_of_classes = $billing_options_deposit_amount = $billing_options_no_of_payments = $delay_recurring_payment_amount = $first_payment = 0;
        $billing_options = $billing_options_expiration_date = '';
        $membership_structure = 'OE';
        $initial_payment_include_membership_fee = 'Y';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        $rank_id = $rank_name = $registration_type = $old_membership_reg_id = '';
        $reg_type_user = 'P';
        $temp1 = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp1[1];
        $reg_version_user = $version;
        $exclude_from_billing_flag = 'N';
        $exclude_days_array = [];
        $program_start_date = $program_end_date = $reg_current_date = '';
        $payment_method_id = $stripe_customer_id = $cc_type = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['membership_category_title'])) {
            $membership_category_title = $this->_request['membership_category_title'];
        }
        if (isset($this->_request['membership_title'])) {
            $membership_title = $this->_request['membership_title'];
        }
        if (isset($this->_request['membership_desc'])) {
            $membership_desc = $this->_request['membership_desc'];
        }
        if (isset($this->_request['membership_structure'])) {
            $membership_structure = $this->_request['membership_structure'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['processing_fee'])) {
            $processing_fee = $this->_request['processing_fee'];
        }
        if (isset($this->_request['membership_fee'])) {
            $membership_fee = $this->_request['membership_fee'];
        }
        if (isset($this->_request['membership_fee_disc'])) {
            $membership_fee_disc = $this->_request['membership_fee_disc'];
        }
        if (isset($this->_request['signup_fee'])) {
            $signup_fee = $this->_request['signup_fee'];
        }
        if (isset($this->_request['signup_fee_disc'])) {
            $signup_fee_disc = $this->_request['signup_fee_disc'];
        }
        if (isset($this->_request['initial_payment'])) {      //registration time payment
            $initial_payment = $this->_request['initial_payment'];
        }
        if (isset($this->_request['first_payment'])) {      //first payment on membership start date
            $first_payment = $this->_request['first_payment'];
        }
        if (isset($this->_request['payment_frequency'])) {
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if(isset($this->_request['membership_start_date'])){
            $membership_start_date = $this->_request['membership_start_date'];
        }
        if (isset($this->_request['payment_start_date'])) {
            $payment_start_date = $this->_request['payment_start_date'];
        }
        if (isset($this->_request['recurring_start_date'])) {
            $recurring_start_date = $this->_request['recurring_start_date'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['reg_col4'])) {
            $reg_col4 = $this->_request['reg_col4'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['prorate_first_payment_flg']) && !empty(trim($this->_request['prorate_first_payment_flg']))) {
            $prorate_first_payment_flg = $this->_request['prorate_first_payment_flg'];
        }
        if (isset($this->_request['delay_recurring_payment_start_flg']) && !empty(trim($this->_request['delay_recurring_payment_start_flg']))) {
            $delay_recurring_payment_start_flg = $this->_request['delay_recurring_payment_start_flg'];
        }
        if (isset($this->_request['delay_recurring_payment_start_date'])) {
            $delay_recurring_payment_start_date = $this->_request['delay_recurring_payment_start_date'];
        }
        if (isset($this->_request['delay_recurring_payment_amount'])) {
            $delay_recurring_payment_amount = $this->_request['delay_recurring_payment_amount'];
        }
        if (isset($this->_request['custom_recurring_frequency_period_type'])) {
            $custom_recurring_frequency_period_type = $this->_request['custom_recurring_frequency_period_type'];
        }
        if (isset($this->_request['custom_recurring_frequency_period_val'])) {
            $custom_recurring_frequency_period_val = $this->_request['custom_recurring_frequency_period_val'];
        }
        if (isset($this->_request['initial_payment_include_membership_fee'])) {
            $initial_payment_include_membership_fee = $this->_request['initial_payment_include_membership_fee'];
        }
        if (isset($this->_request['billing_options'])) {
            $billing_options = $this->_request['billing_options'];
        }
        if (isset($this->_request['no_of_classes'])) {
            $no_of_classes = $this->_request['no_of_classes'];
        }
        if (isset($this->_request['billing_options_expiration_date'])) {
            $billing_options_expiration_date = $this->_request['billing_options_expiration_date'];
        }
        if (isset($this->_request['billing_options_deposit_amount'])) {
            $billing_options_deposit_amount = $this->_request['billing_options_deposit_amount'];
        }
        if (isset($this->_request['billing_options_no_of_payments'])) {
            $billing_options_no_of_payments = $this->_request['billing_options_no_of_payments'];
        }
        if (isset($this->_request['payment_array'])) {
            $payment_array = $this->_request['payment_array'];
        }
        if (isset($this->_request['participant_street'])) {
            $participant_street = $this->_request['participant_street'];
        }
        if (isset($this->_request['participant_city'])) {
            $participant_city = $this->_request['participant_city'];
        }
        if (isset($this->_request['participant_state'])) {
            $participant_state = $this->_request['participant_state'];
        }
        if (isset($this->_request['participant_zip'])) {
            $participant_zip = $this->_request['participant_zip'];
        }
        if (isset($this->_request['participant_country'])) {
            $participant_country = $this->_request['participant_country'];
        }
        if (isset($this->_request['rank_id'])) {
            $rank_id = $this->_request['rank_id'];
        }
        if (isset($this->_request['rank_name'])) {
            $rank_name = $this->_request['rank_name'];
        }
        if (isset($this->_request['registration_type'])) {
            $registration_type = $this->_request['registration_type'];
        }
        if (isset($this->_request['old_membership_reg_id'])) {
            $old_membership_reg_id = $this->_request['old_membership_reg_id'];
        }
        if (isset($this->_request['exclude_days_array'])) {
            $exclude_days_array = $this->_request['exclude_days_array'];
        }
        if (isset($this->_request['program_start_date'])) {
            $program_start_date = $this->_request['program_start_date'];
        }
        if (isset($this->_request['program_end_date'])) {
            $program_end_date = $this->_request['program_end_date'];
        }
        if (isset($this->_request['exclude_from_billing_flag'])) {
            $exclude_from_billing_flag = $this->_request['exclude_from_billing_flag'];
        }
        if (isset($this->_request['reg_current_date'])) {
            $reg_current_date = $this->_request['reg_current_date'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['stripe_customer_id'])) {
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        if (isset($this->_request['cc_type'])) {
            $cc_type = $this->_request['cc_type'];
        }
        //Input validations
        if (!empty(trim($payment_frequency)) && !in_array($payment_frequency, $membership_payment_array)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty(trim($payment_frequency)) && $payment_frequency == 'C' && (empty(trim($custom_recurring_frequency_period_type)) || empty(trim($custom_recurring_frequency_period_val)))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($membership_structure) && $membership_structure == 'OE' && $payment_amount > 0 && !empty(trim($delay_recurring_payment_start_flg)) && $delay_recurring_payment_start_flg == 'Y' && empty(trim($delay_recurring_payment_start_date))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if (!empty($membership_structure) && $membership_structure == 'NC' && empty($billing_options)) {
            if (!empty($billing_options) && $billing_options == 'PP' && empty($payment_array)) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        }
        if (!empty($membership_structure) && $membership_structure == 'SE' && ((empty($program_start_date) || $program_start_date == '0000-00-00' || empty($program_end_date) || $program_end_date == '0000-00-00') || (!empty($billing_options) && $billing_options == 'PP' && $payment_amount > 0 && empty($payment_array)))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }

        if (!empty($company_id) && !empty($membership_id) && !empty($membership_option_id) && !empty($membership_category_title) && !empty($membership_title) && !empty($membership_desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
            $this->membership_model->stripemembershipCheckout($company_id, $membership_id, $membership_option_id, $student_id, $participant_id, $upgrade_status, $membership_category_title, $membership_title, $membership_desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $processing_fee_type, $processing_fee, $membership_fee, $membership_fee_disc, $signup_fee, $signup_fee_disc, $payment_amount, $initial_payment, $first_payment, $payment_frequency, $payment_start_date, $recurring_start_date, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $prorate_first_payment_flg, $delay_recurring_payment_start_flg, $delay_recurring_payment_start_date, $delay_recurring_payment_amount, $custom_recurring_frequency_period_type, $custom_recurring_frequency_period_val, $initial_payment_include_membership_fee, $membership_structure, $billing_options, $no_of_classes, $billing_options_expiration_date, $billing_options_deposit_amount, $billing_options_no_of_payments, $payment_array, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $rank_id, $rank_name, $registration_type, $old_membership_reg_id, $exclude_days_array, $program_start_date, $program_end_date, $exclude_from_billing_flag, $reg_type_user, $reg_version_user, $reg_current_date, $membership_start_date, $payment_method_id, $stripe_customer_id, $cc_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function updatestripePaymentMethod() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $reg_id = $buyer_first_name = $buyer_last_name = '';
        $payment_method_id = $buyer_name = $buyer_email = $page_key = '';
        $category = 'E';        
        $type= '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['reg_id'])) {
            $reg_id = $this->_request['reg_id'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['category_type']) && !empty($this->_request['category_type'])) {
            $category = $this->_request['category_type'];
        }
        if (isset($this->_request['page_key'])) {
            $page_key = $this->_request['page_key'];
        }
        if (isset($this->_request['payment_method'])){
            $payment_method_id = $this->_request['payment_method'];
        }

        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        
        if($type == "CC" && (empty($reg_id) || empty($payment_method_id) || empty($buyer_first_name) || empty($buyer_last_name) || empty($buyer_email))){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        // Input validations
        if (!empty($company_id) ) {
            if ($category == 'M') {
                $this->membership_model->updateMembershipstripePaymentMethodDetails($company_id, $reg_id, $payment_method_id, $buyer_first_name, $buyer_last_name, $buyer_email, $page_key,$type);
            } else if ($category == 'T') {
                $this->trial_model->updateTrialstripePaymentMethodDetails($company_id, $reg_id, $payment_method_id, $buyer_first_name, $buyer_last_name, $buyer_email);
            } else {
                $this->event_model->updateEventstripePaymentMethodDetails($reg_id, $payment_method_id, $buyer_first_name, $buyer_last_name, $buyer_email, $page_key,$type);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getSupportedCountryList(){
        $this->getCountryList();
    }
    
    
}
// Initiiate Library
$api = new PortalApi;
$api->processApi();
