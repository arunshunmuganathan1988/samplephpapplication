
<?php

header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: origin, x-requested-with, Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
header('Access-Control-Allow-Credentials: true');

require_once '../../../Globals/cont_log.php';
require_once __DIR__.'/../../../aws/aws.phar';
	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;
class WhitelabelModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $sf;
    private $ps;
    private $list_id = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_redirect_uri = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $stripe_pk = '', $stripe_sk = '', $mystudio_account = '';
    private $env;
    private $server_url;

    public function __construct() {
        require_once '../../../Globals/config.php';
        require_once '../../../Globals/stripe_props.php';
        $this->inputs();
        $this->sf = new Salesforce();
        $this->ps = new PaySimple();
        $this->wp = new wepay();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->list_id = $this->sf->list_id;
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = "../../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'dev.mystudio.academy') !== false || strpos($host, 'dev2.mystudio.academy') !== false){
            $this->env = 'D';
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->env = 'P';
        }else{
            $this->env = 'D';
        }
        if (strpos($host, 'dev.mystudio.academy') !== false) {
            $this->server_url = "http://dev.mystudio.academy";
        } elseif (strpos($host, 'dev2.mystudio.academy') !== false) {
            $this->server_url = "http://dev2.mystudio.academy";
        } elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'];
        } else {
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithPaysimple($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithSalesforce($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithCampaignEmail($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
        header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');
        
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public  function geticonurlINDB($company_id,$scheme_name,$bundle_id,$callback){ 
        
        $sql1 = sprintf("SELECT `ios_hex_value`,`android_hex_value` From  `white_label_apps` WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                $row1 = mysqli_fetch_assoc($result1);
            }
        }
        
        $ios_url = $android_url = "null";
        $ios_portrait  = $ios_landscape = $android_portait = $android_landscape = "";
        $bucketName = 'dev.app-files.mystudio.app';
	$IAM_KEY = 'AKIAIGUWYNJGAHDPJGIA';
	$IAM_SECRET = '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';
        $file_exist_flag = false;
	// Connect to AWS
	try {
		$s3 = S3Client::factory(
			array(
				'credentials' => array(
					'key' => $IAM_KEY,
					'secret' => $IAM_SECRET
				),
				'version' => 'latest',
				'region'  => 'us-east-1'
			)
		);
	} catch (Exception $e) {
            log_info($e->getMessage());
                $error = array('status' => "Failed", "msg" => "Failed to get connection");
                $this->response($this->json($error), 200);
	}
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$scheme_name.appiconset/Icon-App-1024x1024@1x.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$scheme_name.appiconset/Icon-App-1024x1024@1x.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $ios_url = (string) $request->getUri();
                }
                
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$scheme_name.launchimage/Default~iphone.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$scheme_name.launchimage/Default~iphone.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $ios_portrait = (string) $request->getUri();
                }
                
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$scheme_name.launchimage/Default-Landscape-736h.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$scheme_name.launchimage/Default-Landscape-736h.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $ios_landscape = (string) $request->getUri();
                }
                
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/MyStudioWhiteLabel/platforms/android/res-$scheme_name/drawable-port-hdpi/screen.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/MyStudioWhiteLabel/platforms/android/res-$scheme_name/drawable-port-hdpi/screen.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $android_portait = (string) $request->getUri();
                }
                
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/MyStudioWhiteLabel/platforms/android/res-$scheme_name/drawable-land-hdpi/screen.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/MyStudioWhiteLabel/platforms/android/res-$scheme_name/drawable-land-hdpi/screen.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $android_landscape = (string) $request->getUri();
                }
                
                
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/MyStudioWhiteLabel/platforms/android/res-$scheme_name/drawable-hdpi/icon.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/MyStudioWhiteLabel/platforms/android/res-$scheme_name/drawable-hdpi/icon.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $android_url = (string) $request->getUri();
                }
                if($callback == 1){
                    $error = array("ios_icon_url" => "$ios_url", "android_icon_url" => "$android_url");
                    return $error;
                }else{
                    $error = array('status' => "Success", "ios_icon_url" => "$ios_url", "android_icon_url" => "$android_url","hex"=>$row1,"ios_portrait"=>$ios_portrait ,"ios_landscape" => $ios_landscape,"android_portait" => $android_portait,"android_landscape" => $android_landscape);
                    $this->response($this->json($error), 200);
                    
                }
    }
    
    public function uploadwhitelabelcertINDB($company_id,$ios_development_cer,$ios_distribution_cer,$aps_development_cer,$aps_Production_cer,$AppName_development_cer,$AppName_distribution_cer,$ios_distribution_cer_name,$aps_development_cer_name,$aps_Production_cer_name,$AppName_development_cer_name,$AppName_distribution_cer_name,$ios_development_cer_name,$ios_account_type) {
        
        $ios_development_cer_enocded = explode("base64,", $ios_development_cer);
        $ios_distribution_cer_enocded = explode("base64,", $ios_distribution_cer);
        $aps_development_cer_enocded = explode("base64,", $aps_development_cer);
        $aps_Production_cer_enocded = explode("base64,", $aps_Production_cer);
        $AppName_development_cer_enocded = explode("base64,", $AppName_development_cer);
        $AppName_distribution_cer_enocded = explode("base64,", $AppName_distribution_cer);
        
        $sql1 = sprintf("SELECT `ios_teamid`,`ios_bundle_id` From  `white_label_apps` WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                $row1 = mysqli_fetch_assoc($result1);
                $teamid = $row1['ios_teamid'];
                $bundle_id = $row1['ios_bundle_id'];
            }
        }

        if($ios_account_type == "I" && (!empty($ios_development_cer) || !empty($ios_distribution_cer) || !empty($aps_development_cer)
                 || !empty($aps_Production_cer) || !empty($AppName_development_cer) || !empty($AppName_distribution_cer))){
//      starts
        $profile_types = ["ICD","ICP","IPD","IPP"];
            
        for ($i = 0; $i < count($profile_types); $i++) {
            $file_content = "";
            $file_content = ($profile_types[$i] == "ICD" ? $ios_development_cer_enocded : (($profile_types[$i] == "ICP" ? $ios_distribution_cer_enocded : (($profile_types[$i] == "IPD" ? $AppName_development_cer_enocded : ($AppName_distribution_cer_enocded) )) )) );
            $file_content_name = "";
            
            if(empty(trim($file_content[0]))){
                continue;
            }
            $file_content_decoded = base64_decode($file_content[1]);
            if ($profile_types[$i] == "ICD") {
                $file_content_name = (explode("fakepath\\", $ios_development_cer_name));
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $textfile = "temp.txt";
                $textfileopen = fopen($textfile, "wb");
                fwrite($textfileopen, $file_content_decoded);
                fclose($textfileopen);
                if( exec("grep 'iPhone Developer' temp.txt")) {
                    unlink("$textfile");
                    $file_content_name[1] = "ios_development.cer";
                    chdir($old);
                }else{
                    unlink($textfile);
                    chdir($old);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
            } else if ($profile_types[$i] == "ICP") {
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $file_content_name = (explode("fakepath\\", $ios_distribution_cer_name));
                $textfile = "temp.txt";
                $textfileopen = fopen($textfile, "wb");
                fwrite($textfileopen, $file_content_decoded);
                fclose($textfileopen);
                if( exec("grep 'iPhone Distribution' temp.txt")) {
                    unlink("$textfile");
                    $file_content_name[1] = "ios_distribution.cer";
                    chdir($old);
                }else{
                    unlink($textfile);
                    chdir($old);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
            } else if ($profile_types[$i] == "IPD") {
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $file_content_name = (explode("fakepath\\", $AppName_development_cer_name));
                $textfile = "temp.mobileprovision";
                $textfileopen = fopen("temp.mobileprovision", "wb");
                fwrite($textfileopen, $file_content_decoded);
                fclose($textfileopen);
                exec("openssl smime -inform der -verify -noverify -in $textfile > tmp.plist");
                $xml=simplexml_load_file("tmp.plist") or die("Error: Cannot create object");
                $jsonxml = json_encode($xml);
                $jsondecoded = json_decode($jsonxml,true);
                if($jsondecoded['dict']['dict']['string'][0] == "development"){
                    unlink("tmp.plist");
                    unlink($textfile);
                    $file_content_name[1] = str_replace(" ","_",$jsondecoded['dict']['string'][0]);
                }else{
                    unlink("tmp.plist");
                    unlink($textfile);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
                chdir($old);
            } else {
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $file_content_name = (explode("fakepath\\", $AppName_distribution_cer_name));
                $textfile = "temp.mobileprovision";
                $textfileopen = fopen("temp.mobileprovision", "wb");
                fwrite($textfileopen, $file_content_decoded);
                fclose($textfileopen);
                exec("openssl smime -inform der -verify -noverify -in $textfile > tmp.plist");
                $xml=simplexml_load_file("tmp.plist") or die("Error: Cannot create object");
                $jsonxml = json_encode($xml);
                $jsondecoded = json_decode($jsonxml,true);
                if($jsondecoded['dict']['dict']['string'][0] == "production"){
                    unlink("tmp.plist");
                    unlink($textfile);
                    $file_content_name[1] = str_replace(" ","_",$jsondecoded['dict']['string'][0]);
                }else{
                    unlink("tmp.plist");
                    unlink($textfile);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
                chdir($old);
            }
            //validation
            $old = getcwd(); // Save the current directory
            $dir_name = "../../../$this->upload_folder_name/whitelabel";
            chdir($dir_name);
            $file = $file_content_name[1];
            $ifp = fopen($file, "wb");
            fwrite($ifp, $file_content_decoded);
            fclose($ifp);
            if($profile_types[$i] == "ICP" || $profile_types[$i] == "ICD"){
                $textfile =(explode(".", $file));
                $textfile = $textfile[0] . ".txt";
                $textfileopen = fopen($textfile, "wb");
                fwrite($textfileopen, $file_content_decoded);
                if( exec("grep '".escapeshellarg("$teamid") . "' ./$textfile")) {
                    fclose($textfileopen);
                    unlink("$textfile");
                }else{
                    fclose($textfileopen);
                    unlink($file_content_name[1]);//delete file
                    unlink("$textfile");
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] and Team ID mismatch");
                    $this->response($this->json($error), 200);
                }
            }
            $exp_date_ssl = $exp_date_ssl_format=$day= $mon = $year= $currentdate= $currentplus15days=$team_id="";
            if($profile_types[$i] == "ICD" || $profile_types[$i] == "ICP"){
                exec("openssl x509 -in $file_content_name[1] -inform DER -out aps.pem -outform PEM");
                exec("openssl pkcs12 -export -inkey app.key -in aps.pem -out aps.p12 -password pass:mystudio");
                exec("openssl pkcs12 -in aps.p12 -out prod.pem -nodes -clcerts -password pass:mystudio");
                $exp_date_ssl = exec('openssl x509 -enddate -noout -in prod.pem');
                log_info($file_content_name[1] . " : $exp_date_ssl");
                $date = substr($exp_date_ssl,9);
                $exp_date_ssl = strtotime($date);
            }else{
                exec("openssl smime -inform der -verify -noverify -in $file_content_name[1] > tmp.plist");
                $xml=simplexml_load_file("tmp.plist") or die("Error: Cannot create object");
                $jsonxml = json_encode($xml);
                $jsondecoded = json_decode($jsonxml,true);
                $team_id = $jsondecoded['dict']['array'][0]['string'];
                $bundle_id_fromxml = $jsondecoded['dict']['dict']['string'][1];
                $bundle_id_fromxml = trim(str_replace($team_id.".","",$bundle_id_fromxml));
                if($bundle_id_fromxml != $bundle_id) {
                    $error = array('status' => "Failed", "msg" => "Bundle ID and certificate mismatch");
                    $this->response($this->json($error), 200);
                }
                log_info($file_content_name[1] . " : " . $jsondecoded['dict']['date'][1]);
                $exp_date_ssl = strtotime($jsondecoded['dict']['date'][1]);
                unlink("tmp.plist");
//                $exp_date_ssl = exec("/usr/libexec/PlistBuddy -c 'Print DeveloperCertificates:0' /dev/stdin <<< $(security cms -D -i $file_content_name[1]) | openssl x509 -inform DER -noout -enddate");
            }
            $exp_date = date("Y-m-d", $exp_date_ssl);
//            log_info("Exp date : ".$exp_date_ssl);
            unlink($file_content_name[1]);//delete file
            if($team_id != $teamid && ($profile_types[$i] == "IPD" || $profile_types[$i] == "IPP")){
                $error = array('status' => "Failed", "msg" => "Team ID & certificates doesn't match.");
                $this->response($this->json($error), 200);
            }
            if($profile_types[$i] == "ICD" || $profile_types[$i] == "ICP"){
                unlink("aps.p12");//delete .p12 file
                unlink("aps.pem");//delete aps.pem
                unlink("prod.pem");//delete prod.pem file
            }
            if($profile_types[$i] == "ICD" || $profile_types[$i] == "ICP"){
                $currentplus15days = date("Y-m-d", strtotime("+15 days"));
            }else{
                $currentplus15days = date("Y-m-d", strtotime("+30 days"));
            }
            $currentplus15days = strtotime($currentplus15days);
            if($exp_date_ssl<$currentplus15days){
                $error = array('status' => "Failed", "msg" => "$file_content_name[1] is expired. Please upload new file!");
                $this->response($this->json($error), 200);
            }
            chdir($old); // Restore the old working directory
            
            $sql1 = sprintf("SELECT * FROM `white_labels_profile` WHERE `ios_bundle_id`='%s' and profile_type='%s'", mysqli_real_escape_string($this->db, $bundle_id), mysqli_real_escape_string($this->db, $profile_types[$i]));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if (mysqli_num_rows($result) == 0) {
                    $sql2 = sprintf("INSERT INTO `white_labels_profile` (`app_id`,`ios_bundle_id`,`ios_app_name`,`profile_type`,`profile_name`,`app_profile`, `profile_expiry_date`) SELECT `app_id`,`ios_bundle_id`,`ios_app_name`,'$profile_types[$i]','$file_content_name[1]','$file_content[1]', '$exp_date' from `white_label_apps` where `ios_bundle_id` = '$bundle_id' ");
                    $result1 = mysqli_query($this->db, $sql2);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                } else {
                    $sql2 = sprintf("UPDATE `white_labels_profile` SET profile_name='%s',app_profile='%s', `profile_expiry_date` = '%s' where `ios_bundle_id` = '%s' and profile_type='%s'", mysqli_real_escape_string($this->db, $file_content_name[1]), mysqli_real_escape_string($this->db, $file_content[1]), mysqli_real_escape_string($this->db, $exp_date), mysqli_real_escape_string($this->db, $bundle_id), mysqli_real_escape_string($this->db, $profile_types[$i]));
                    $result1 = mysqli_query($this->db, $sql2);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        
        //ssl prod  and dev
        $ssl_types = ["dev","prod"];
        $file_contents_array = [];
        $file_contents_array[0]['content'] = "";
        $file_contents_array[0]['expdate'] = "";
        $file_contents_array[1]['content'] = "";
        $file_contents_array[1]['expdate'] = "";
        for($j = 0 ;$j < count($ssl_types) ; $j++){
            $file_contents = [];
            $file_contents = ($ssl_types[$j] == "dev" ? $aps_development_cer_enocded : $aps_Production_cer_enocded);
            $file_content_name = "";
            if(empty(trim($file_contents[0]))){
                continue;
            }
            $file_contents_decoded = base64_decode($file_contents[1]);
            if ($ssl_types[$j] == "dev") {
                $file_content_name = (explode("fakepath\\", $aps_development_cer_name));
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $textfile = "temp.txt";
                $textfileopen = fopen($textfile, "wb");
                fwrite($textfileopen, $file_contents_decoded);
                fclose($textfileopen);
                if( exec("grep 'Apple Development IOS Push Services' temp.txt")) {
                    unlink("$textfile");
                    $file_content_name[1] = "aps_development.cer";
                    chdir($old);
                }else{
                    unlink($textfile);
                    chdir($old);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
            }else{
                $file_content_name = (explode("fakepath\\", $aps_Production_cer_name));
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $textfile = "temp.txt";
                $textfileopen = fopen($textfile, "wb");
                fwrite($textfileopen, $file_contents_decoded);
                fclose($textfileopen);
                if( exec("grep 'Apple Push Services' temp.txt")) {
                    unlink("$textfile");
                    $file_content_name[1] = "aps_production.cer";
                    chdir($old);
                }else{
                    unlink($textfile);
                    chdir($old);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
            }
            //validation
            $old = getcwd(); // Save the current directory
            $dir_name = "../../../$this->upload_folder_name/whitelabel";
            chdir($dir_name);
            $file = $file_content_name[1];
            $ifp = fopen($file, "wb");
            fwrite($ifp, $file_contents_decoded);
            fclose($ifp);
            $exp_date_ssl = $exp_date_ssl_format=$day= $mon = $year= $currentdate= $currentplus15days="";
            exec("openssl x509 -in $file_content_name[1] -inform DER -out aps.pem -outform PEM");
            exec("openssl pkcs12 -export -inkey app.key -in aps.pem -out aps.p12 -password pass:mystudio");
            exec("openssl pkcs12 -in aps.p12 -out prod.pem -nodes -clcerts -password pass:mystudio");
            $exp_date_ssl = exec('openssl x509 -enddate -noout -in prod.pem');
//                exec("security cms -D -i $file_content_name[1] > tmp.plist");
            $file_contents_array[$j]['content'] = file_get_contents("prod.pem");//pem files array
            unlink($file_content_name[1]);//delete file
            unlink("aps.p12");//delete .p12 file
            unlink("aps.pem");//delete aps.pem
            unlink("prod.pem");//delete prod.pem file
            $date = substr($exp_date_ssl,9);
//            $mon = substr($exp_date_ssl,9,3);
//            $day = substr($exp_date_ssl,13,2);
//            $year = substr($exp_date_ssl,25,4);
            $exp_date_ssl = strtotime($date);
            $exp_date2 = date("Y-m-d", $exp_date_ssl);
             $file_contents_array[$j]['expdate'] = $exp_date2; // EXPIRATION DATE 
            $currentplus15days = date("Y-m-d", strtotime("+30 days"));
            $currentplus15days = strtotime($currentplus15days);
            if($exp_date_ssl<$currentplus15days){
                $error = array('status' => "Failed", "msg" => "$file_content_name[1] is expired. Please upload new file!");
                $this->response($this->json($error), 200);
            }
            chdir($old); // Restore the old working directory
        }
        $sql1 = sprintf("SELECT * FROM `mobile_push_certificate` WHERE `bundle_id`='%s'", mysqli_real_escape_string($this->db, $bundle_id));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if (mysqli_num_rows($result) == 0) {
                    $sql2 = sprintf("INSERT INTO `mobile_push_certificate` (`type`,`bundle_id`,`dev_pem`,`prod_pem`,`dev_pem_expiry`, `prod_pem_expiry`) values ('I','%s','%s','%s','%s','%s') ", mysqli_real_escape_string($this->db, $bundle_id), mysqli_real_escape_string($this->db, $file_contents_array[0]['content']), mysqli_real_escape_string($this->db, $file_contents_array[1]['content']), mysqli_real_escape_string($this->db, $file_contents_array[0]['expdate']), mysqli_real_escape_string($this->db, $file_contents_array[1]['expdate']));
                    $result1 = mysqli_query($this->db, $sql2);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                } else {
                    if(!empty($file_contents_array[0]['content'])){
                    $sql2 = sprintf("UPDATE `mobile_push_certificate` SET dev_pem='%s', dev_pem_expiry = '%s' where `bundle_id` = '%s'", mysqli_real_escape_string($this->db, $file_contents_array[0]['content']), mysqli_real_escape_string($this->db, $file_contents_array[0]['expdate']), mysqli_real_escape_string($this->db, $bundle_id));
                    $result1 = mysqli_query($this->db, $sql2);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                    }
                    if(!empty($file_contents_array[1]['content'])){
                    $sql3 = sprintf("UPDATE `mobile_push_certificate` SET prod_pem='%s', prod_pem_expiry = '%s' where `bundle_id` = '%s'", mysqli_real_escape_string($this->db, $file_contents_array[1]['content']), mysqli_real_escape_string($this->db, $file_contents_array[1]['expdate']), mysqli_real_escape_string($this->db, $bundle_id));
                    $result2 = mysqli_query($this->db, $sql3);
                    if (!$result2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                    }
                }
            }
        }
        
        $all_updated_status = $this->checkbundleidINDB($company_id,1);
        if($all_updated_status['ios_dev_status'] == "Y" && $all_updated_status['ios_prod_status'] == "Y" && $all_updated_status['provision_dev_status'] == "Y" && 
           $all_updated_status['provision_prod_status'] == "Y" && $all_updated_status['aps_dev_status'] == "Y" && $all_updated_status['aps_prod_status'] == "Y"){
            //update flag
            $sql3 = sprintf("UPDATE `white_label_apps` SET all_files_uploaded_flag='R' where `ios_bundle_id` = '%s'",mysqli_real_escape_string($this->db, $bundle_id));
            $result2 = mysqli_query($this->db, $sql3);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
            $this->sendEmailForAdmins('W',"All files for Company id : $company_id , Bundle ID : $bundle_id uploaded successfully");
        }
        
        $error = array('status' => "Success", "msg" => "Certificates updated successfully");
        $this->response($this->json($error), 200);
        
    }
    
    public function sendEmailForAdmins($subject,$msg){
        if($this->env=='P'){
            $env = "Production";
        }else{
            $env = "Development";
        }
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com,ravi@technogemsinc.com,deepak@technogemsinc.com,jeeva@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if($subject == "W"){
                $mail->Subject = "$env - Whitelabel Files upload status.";
            }else{
                $mail->Subject = "$env - Stripe Subscription Handling Error.";
            }
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    
    public function getwldashboarddetailsINDB($company_id) {
        $sql1 = sprintf("SELECT  w.exclude_android_server_key,w.mystudio_legacy_app,c.company_code,w.ios_latest_version latest_version,IF(w.android_store_key = (select android_store_key from white_label_apps where app_id = 1),'Y','N') mystudio_google_app,w.error_msg,w.app_store_error_msg,w.app_id,w.skeleton_apk_status,w.android_server_key,w.google_services_json,w.android_store_key json_file,w.ios_bundle_id,c.upgrade_status,IF(w.ios_bundle_id!='com.mystudio.app' AND w.ios_username='chennai@technogemsinc.com', '', w.ios_username) as ios_username,IF(w.ios_bundle_id!='com.mystudio.app' AND w.ios_username='chennai@technogemsinc.com', '', w.ios_password) as ios_password,IF(w.ios_bundle_id!='com.mystudio.app' AND w.ios_username='chennai@technogemsinc.com', '',w.ios_teamid) as ios_teamid,w.ios_developerid,w.ios_account_type,w.all_files_uploaded_flag,w.ios_target_name,w.firebase_snippet,w.dynamic_url,c.stripe_status,c.wepay_status,w.application_status,w.current_build_number,w.android_version_number,w.android_application_status,w.build_status app_store_status,w.new_build_version,w.bundleid_error_flag,w.credential_error_flag,w.teamid_error_flag,w.metadata_error,w.ios_icon_error,w.android_icon_error,w.ios_screenshot_error,w.android_screenshot_error
            FROM `white_label_apps` w LEFT JOIN white_labels_profile wp USING(ios_bundle_id) LEFT JOIN android_store_information gsi on w.app_id=gsi.app_id  RIGHT JOIN company c 
                ON c.company_id=w.company_id  AND w.`studio_code`=(SELECT company_code from company where company_id='%s')  WHERE c.`company_id`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql1);
//        log_info($sql1);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            $this->response($this->json($error), 200);
        } else {
            $rows = [];
            if (mysqli_num_rows($result) > 0) {
                $rows = mysqli_fetch_assoc($result);
            }
            $artwork = [];
            $bundle_id = $rows['ios_bundle_id'];
            if(count($rows)>0){
                $artwork = $this->geticonurlINDB($company_id,$rows['ios_target_name'],$bundle_id,1);
            }
            $error = array('status' => "Success", "msg" => $rows,"artwork"=>$artwork);
            $this->response($this->json($error), 200);
        }
    }
    
    
    
    
    public function checkbundleidINDB($company_id,$callback) {
//        $this->savezipinS3();
        $sql1 = sprintf("SELECT c.whilelabel_setup_page_access,w.android_server_key,w.all_files_uploaded_flag,c.stripe_status,c.wepay_status,w.application_status,w.current_build_number,w.android_version_number,w.app_store_status,w.new_build_number,w.bundleid_error_flag,w.credential_error_flag,w.teamid_error_flag,w.metadata_error,w.ios_icon_error,w.android_icon_error,w.ios_screenshot_error,w.android_screenshot_error
            ,w.android_app_name appiconnameandroid,w.app_id,w.ios_target_name,w.firebase_snippet,w.dynamic_url,w.ios_app_name,w.ios_account_type,m.dev_pem,m.prod_pem,m.dev_pem_expiry,m.prod_pem_expiry,wp.profile_type,wp.profile_expiry_date,w.ios_bundle_id,c.upgrade_status,IF(w.ios_bundle_id!='com.mystudio.app' AND w.ios_username='chennai@technogemsinc.com', '', w.ios_username) as ios_username,IF(w.ios_bundle_id!='com.mystudio.app' AND w.ios_username='chennai@technogemsinc.com', '', w.ios_password) as ios_password,IF(w.ios_bundle_id!='com.mystudio.app' AND w.ios_username='chennai@technogemsinc.com', '',w.ios_teamid) as ios_teamid,w.ios_developerid,w.ios_account_type FROM `white_label_apps` w LEFT JOIN white_labels_profile wp USING(ios_bundle_id) LEFT JOIN mobile_push_certificate m on w.ios_bundle_id=m.bundle_id  LEFT JOIN apple_store_information asi on w.app_id=asi.app_id LEFT JOIN android_store_information gsi on w.app_id=gsi.app_id RIGHT JOIN company c 
                ON c.company_id=w.company_id  AND w.`studio_code`=(SELECT company_code from company where company_id='%s')  WHERE c.`company_id`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql1);
//        log_info($sql1);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            $this->response($this->json($error), 200);
        } else {
                $out['ios_dev_status'] = 'N';
                $out['ios_prod_status'] = 'N';
                $out['provision_dev_status'] = 'N';
                $out['provision_prod_status'] = 'N';
                $out['aps_dev_status'] = 'N';
                $out['aps_prod_status'] = 'N';
            if (mysqli_num_rows($result) > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $outs[] = $rows;
                    if($rows['profile_type'] == "ICD"){
                        $out['ios_dev_status'] = 'Y';
                        $out['ios_dev_expiry_date'] = $rows['profile_expiry_date'];
                    }else if($rows['profile_type'] == "ICP"){
                        $out['ios_prod_status'] = 'Y';
                        $out['ios_prod_expiry_date'] = $rows['profile_expiry_date'];
                    }else if($rows['profile_type'] == "IPD"){
                        $out['provision_dev_status'] = 'Y';
                        $out['provision_dev_expiry_date'] = $rows['profile_expiry_date'];
                    }else if($rows['profile_type'] == "IPP"){
                        $out['provision_prod_status'] = 'Y';
                        $out['provision_prod_expiry_date'] = $rows['profile_expiry_date'];
                    }
                    if(!empty($rows['dev_pem']) && !is_null($rows['dev_pem_expiry']) && !empty($rows['dev_pem_expiry'])){
                        $out['aps_dev_status'] = 'Y';
                        $out['dev_pem_expiry'] = $rows['dev_pem_expiry'];
                    }
                    if(!empty($rows['prod_pem']) && !is_null($rows['prod_pem_expiry']) && !empty($rows['prod_pem_expiry'])){
                        $out['aps_prod_status'] = 'Y';
                        $out['prod_pem_expiry'] = $rows['prod_pem_expiry'];
                    }
                }
                if($callback == 0){
                    if(empty($outs[0]['ios_bundle_id']) || is_null($outs[0]['ios_bundle_id'])){
                        $output = array('status' => "Success", "bundle_id_status" => "N","upgrade_status"=>$outs[0]['upgrade_status'],"bundle_id"=>'',"msg"=>$outs[0]);
                    }else{
                        if(!is_null($outs[0]['ios_password']) && !empty($outs[0]['ios_password'])){
                            $outs[0]['ios_password'] = $this->decryptPassword($outs[0]['ios_password']);
                        }else{
                            $outs[0]['ios_password'] = '';
                        }
                        $output = array('status' => "Success", "bundle_id_status" => "Y","bundle_id"=>$outs[0]['ios_bundle_id'],"ios_account_type"=>$outs[0]['ios_account_type'],"upgrade_status"=>$outs[0]['upgrade_status'],"msg"=>$outs[0],"msgs"=>"Bundle id updated successfully.","file_flag"=>$out);
                    }
                }else{
                    return $out;
                }
            }else{
                if($callback == 0){
                    $output = array('status' => "Success", "bundle_id_status" => "N");
                }else{
                    return $out;
                }
            }
            $this->response($this->json($output), 200);
        }
    }
    
    public function updatebundleidINDB($company_id,$bundle_id) {
        
        $sql1 = sprintf("SELECT * FROM `white_label_apps` WHERE `ios_bundle_id`='%s'", mysqli_real_escape_string($this->db, $bundle_id));
        $result = mysqli_query($this->db, $sql1);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) == 1) {
//                $sql2 = sprintf("UPDATE `white_label_apps` SET company_id= '%s',`studio_code`=(SELECT company_code from company where company_id='%s') WHERE `ios_bundle_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $bundle_id));
//                $result2 = mysqli_query($this->db, $sql2);
//                if (!$result2) {
//                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                    log_info($this->json($error_log));
//                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                    $this->response($this->json($error), 200);
//                }else{
//                    $this->checkbundleidINDB($company_id,0);
//                }
                    $error = array('status' => "Failed", "msg" => "Bundle ID already exist. Please contact administrator");
                    $this->response($this->json($error), 200);
            }else{
                $scheme_name = trim(str_replace(".","",$bundle_id));
                
                $sql1 = sprintf("SELECT ios_bundle_id,ios_target_name, (SELECT android_latest_version from white_label_apps where app_id=1) android_latest_version, (SELECT ios_latest_version from white_label_apps where app_id=1) ios_latest_version FROM `white_label_apps` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
                $result = mysqli_query($this->db, $sql1);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    if (mysqli_num_rows($result) == 1) {
                        $rows = mysqli_fetch_assoc($result);
                        $ios_latest_version = $rows['ios_latest_version'];
                        $android_latest_version = $rows['android_latest_version'];
                        $old_iios_target_name = $rows['ios_target_name'];
                        if (!empty(trim($old_iios_target_name))) {
                            $this->deletescreenshots($old_iios_target_name);
                            $this->copyanddelicons($old_iios_target_name,$scheme_name,$rows['ios_bundle_id'],$bundle_id);
                        }
                    }
                }
                $sql2 = sprintf("UPDATE `white_label_apps` SET android_latest_version = '$android_latest_version',ios_latest_version='$ios_latest_version',ios_bundle_id = '%s',android_bundle_id = '%s',ios_target_name = '%s',android_flavor_name='%s',skeleton_apk_status='P' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $bundle_id), mysqli_real_escape_string($this->db, $bundle_id), mysqli_real_escape_string($this->db, $scheme_name), mysqli_real_escape_string($this->db, $scheme_name), mysqli_real_escape_string($this->db, $company_id));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $select = sprintf("SELECT * FROM mobile_push_certificate WHERE bundle_id='%s'", mysqli_real_escape_string($this->db, $bundle_id));
                    $result_select = mysqli_query($this->db, $select);
                    if (!$result_select) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        if (mysqli_num_rows($result_select) == 0) {
                            $sql1 = sprintf("INSERT INTO mobile_push_certificate (bundle_id) VALUES ('%s')", mysqli_real_escape_string($this->db, $bundle_id));
                            $result = mysqli_query($this->db, $sql1);
                            if (!$result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                    }
                    $this->copyscreenshots($scheme_name);
                    $this->checkbundleidINDB($company_id, 0);
                }
            }
        }
    }

    public function saveappinfo($company_id, $ios_developerid, $ios_teamid) {
        $app_id = '';
        $sql1 = sprintf("SELECT `app_id`, `ios_app_name` ,`ios_developerid`, `ios_teamid`,ios_username From  `white_label_apps` WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                $row1 = mysqli_fetch_assoc($result1);
                $app_id = $row1['app_id'];
                $user_name = $row1['ios_username'];
                $string = "";
                if($user_name == 'chennai@technogemsinc.com'){
                    $string = ",ios_username = ''";
                }
                if(!empty($ios_developerid)){
                    $sql2 = sprintf("UPDATE `white_label_apps` SET ios_developerid= '%s', studio_code=(SELECT company_code from company where company_id='%s') $string where `app_id` = '%s' ", 
                        mysqli_real_escape_string($this->db, $ios_developerid), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $app_id));
                    $result2 = mysqli_query($this->db, $sql2);
                }
                if(!empty($ios_teamid)){
                    $sql2 = sprintf("UPDATE `white_label_apps` SET ios_teamid = '%s', studio_code=(SELECT company_code from company where company_id='%s') where `app_id` = '%s' ",mysqli_real_escape_string($this->db, $ios_teamid), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $app_id));
                    $result2 = mysqli_query($this->db, $sql2);
                }
//                $sql2 = sprintf("UPDATE `white_label_apps` SET ios_developerid= '%s', ios_teamid = '%s', studio_code=(SELECT company_code from company where company_id='%s') where `app_id` = '%s' ", 
//                        mysqli_real_escape_string($this->db, $ios_developerid), mysqli_real_escape_string($this->db, $ios_teamid), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $app_id));
//                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
//                    if(mysqli_affected_rows($this->db) > 0) {
//                        $this->sendappinfotomystudio($app_id, $company_id);
//                    }else{
//                        $error = array('status' => "Failed", "msg" => "No changes made in App info");
//                        $this->response($this->json($error), 200);
//                    }
                    $this->sendappinfotomystudio($app_id, $company_id);
                }
            } else {
                $sql = sprintf("INSERT INTO `white_label_apps` (`company_id`, `ios_developerid`, `ios_teamid`, `studio_code`) VALUES ('%s', '%s', '%s', (SELECT company_code from company where company_id='%s'))"
                        , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $ios_developerid), mysqli_real_escape_string($this->db, $ios_teamid), mysqli_real_escape_string($this->db, $company_id));
                $result = mysqli_query($this->db, $sql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    $this->response($this->json($error), 200);
                } else {
                    $app_id = mysqli_insert_id($this->db);
                    if(mysqli_affected_rows($this->db) > 0) {
                        $this->sendappinfotomystudio($app_id, $company_id);
                    }else{
                        $error = array('status' => "Failed", "msg" => "Your studio is not linked to any white label app. Please contact MyStudio.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
    }
    
    public function updateaccounttypeINDB($company_id,$wl_type,$bundle_id) {
        $sql2 = sprintf("UPDATE `white_label_apps` SET ios_account_type= '%s' where `company_id` = '%s' ", 
                mysqli_real_escape_string($this->db, $wl_type), mysqli_real_escape_string($this->db, $company_id));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
                $error = array('status' => "Success", "msg" => "Updated Successfully");
                $this->response($this->json($error), 200);
            }
    }
    
    public function savefirebaseINDB($company_id,$firebase,$dynamic_url,$bundle_id,$server_key,$service_json,$type){
        if(!empty($firebase)){
              $firebase = str_replace(array("\n","\r"," "), '', $firebase);
        }
        $service_json_temp = [];
        $service_json_temp = explode("base64,",$service_json);
        if($type == 'json'){
            $sql2 = sprintf("UPDATE `white_label_apps` SET google_services_json= '%s' where `ios_bundle_id` = '%s' ", 
                mysqli_real_escape_string($this->db, $service_json_temp[1]), mysqli_real_escape_string($this->db, $bundle_id)); 
        }else{
            if($type == 'build'){
                $sql2 = sprintf("UPDATE `white_label_apps` LEFT JOIN `mobile_push_certificate` ON white_label_apps.ios_bundle_id = mobile_push_certificate.bundle_id SET white_label_apps.firebase_snippet= '%s',white_label_apps.application_status='T',white_label_apps.dynamic_url= '%s',white_label_apps.android_server_key='%s',mobile_push_certificate.android_props='%s' where white_label_apps.ios_bundle_id = '%s'", 
                mysqli_real_escape_string($this->db, $firebase), mysqli_real_escape_string($this->db, $dynamic_url), mysqli_real_escape_string($this->db, $server_key), mysqli_real_escape_string($this->db, $server_key), mysqli_real_escape_string($this->db, $bundle_id)); 
        
            }else{
                $sql2 = sprintf("UPDATE `white_label_apps` LEFT JOIN `mobile_push_certificate` ON white_label_apps.ios_bundle_id = mobile_push_certificate.bundle_id SET white_label_apps.firebase_snippet= '%s',white_label_apps.dynamic_url= '%s',white_label_apps.android_server_key='%s',mobile_push_certificate.android_props='%s' where white_label_apps.ios_bundle_id = '%s'", 
                mysqli_real_escape_string($this->db, $firebase), mysqli_real_escape_string($this->db, $dynamic_url), mysqli_real_escape_string($this->db, $server_key), mysqli_real_escape_string($this->db, $server_key), mysqli_real_escape_string($this->db, $bundle_id)); 
        
            }
        }
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            $this->response($this->json($error), 200);
        } else {
                if($type == 'json'){
                    $error = array('status' => "Success", "msg" => "Updated Successfully");
                }else{
                    $error = array('status' => "Success", "msg" => "Updated Successfully","firebase"=>$firebase,"dynamic_url"=>$dynamic_url,"server_key"=>$server_key);
                }
                $this->response($this->json($error), 200);
        }
    }
    
    public  function getandroidstoreinfoINDB($company_id,$app_id){     
         $sql1 = sprintf("SELECT an.android_app_name,an.android_full_desc,an.android_title,an.android_short_desc,w.skeleton_apk_status,w.android_store_key json_file,w.android_metadata_manual_check,w.android_screenshot_manual_check from android_store_information an LEFT JOIN white_label_apps w USING(app_id) where app_id ='%s'",mysqli_real_escape_string($this->db, $app_id));
        $result = mysqli_query($this->db, $sql1);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $rows = mysqli_fetch_assoc($result);
                $error = array('status' => "Success", "msg" => $rows);
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public  function processskeletonapkINDB($company_id,$app_id){     
        $sql1 = sprintf("UPDATE white_label_apps SET skeleton_apk_status='P' where company_id ='%s'",mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql1);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            $this->response($this->json($error), 200);
        } else {
                $error = array('status' => "Success","skeleton_apk_status"=>"P");
                $this->response($this->json($error), 200);
        }
    }
    
    public  function saveandroidstoreinfoINDB($company_id,$app_id,$short_desc,$full_desc,$title,$json_file,$type){    
        $query = sprintf("SELECT * from android_store_information where `app_id` = '%s' ", mysqli_real_escape_string($this->db, $app_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                if($type == "NF"){
                    $sql2 = sprintf("UPDATE `android_store_information` SET android_title= '%s',android_short_desc= '%s',android_full_desc= '%s' where `app_id` = '%s' ", 
                mysqli_real_escape_string($this->db, $title),mysqli_real_escape_string($this->db, $short_desc),
                    mysqli_real_escape_string($this->db, $full_desc),mysqli_real_escape_string($this->db, $app_id));
                }
            }else{
                if($type == "NF"){
                $sql2 = sprintf("INSERT INTO `android_store_information` (android_title,android_short_desc,android_full_desc,app_id) values ('%s','%s','%s','%s') ", 
                mysqli_real_escape_string($this->db, $title),mysqli_real_escape_string($this->db, $short_desc),
                    mysqli_real_escape_string($this->db, $full_desc),mysqli_real_escape_string($this->db, $app_id));
                }
            }
            if($type == "F") {
                $json_file_encoded = explode("base64,", $json_file);
                $sql2 = sprintf("UPDATE `white_label_apps` SET android_store_key= '%s' where `app_id` = '%s' ", mysqli_real_escape_string($this->db, $json_file_encoded[1]), mysqli_real_escape_string($this->db, $app_id));
            }
            $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $error = array('status' => "Success", "msg" => "Updated Successfully");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function updateappinfoINDB($company_id,$appiconname,$bundle_id,$appiconnameandroid,$type,$app_id) {
        if($type == "I"){
            $sql2 = sprintf("UPDATE `white_label_apps` SET ios_app_name= '%s' where `app_id` = '%s' ", 
                mysqli_real_escape_string($this->db, $appiconname), mysqli_real_escape_string($this->db, $app_id));
        }else if($type == "A"){
            $sql2 = sprintf("UPDATE `white_label_apps` SET android_app_name= '%s' where `app_id` = '%s' ", 
                mysqli_real_escape_string($this->db, $appiconnameandroid), mysqli_real_escape_string($this->db, $app_id));
        }
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
                $error = array('status' => "Success", "msg" => "Updated Successfully","appiconname"=>$appiconname,"appiconnameandroid"=>$appiconnameandroid);
                $this->response($this->json($error), 200);
            }
    }
    
    public function updatemanagerdetails($company_id, $ios_username, $ios_password, $ios_account_type) {
        $roletext = '';
        if($ios_account_type === 'I'){
            $roletext = 'Admin user details updated successfully.';
        }else if($ios_account_type === 'E'){
            $roletext = 'App manager details updated successfully.';
        }
        $encrypt_pass = $this->encryptPassword($ios_password);
        $sql1 = sprintf("SELECT `ios_username`, `ios_password` ,`ios_account_type` From  `white_label_apps` WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                if(!empty($ios_username)){
                    $sql = sprintf("UPDATE `white_label_apps` SET ios_username = '%s' where `company_id` = '%s' ", mysqli_real_escape_string($this->db, $ios_username), mysqli_real_escape_string($this->db, $company_id));
                    $result = mysqli_query($this->db, $sql);
                }
                if(!empty($ios_password)){
                    $sql = sprintf("UPDATE `white_label_apps` SET ios_password = '%s' where `company_id` = '%s' ", mysqli_real_escape_string($this->db, $encrypt_pass), mysqli_real_escape_string($this->db, $company_id));
                    $result = mysqli_query($this->db, $sql);
                }
//                $sql = sprintf("UPDATE `white_label_apps` SET ios_username = '%s', ios_password = '%s', ios_account_type = '%s' where `company_id` = '%s' ", mysqli_real_escape_string($this->db, $ios_username), mysqli_real_escape_string($this->db, $encrypt_pass), mysqli_real_escape_string($this->db, $ios_account_type), mysqli_real_escape_string($this->db, $company_id));
                $result = mysqli_query($this->db, $sql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $error = array('status' => "Success", "msg" => $roletext);
                    $this->response($this->json($error), 200);
                }
            } else {
                $sql = sprintf("INSERT INTO `white_label_apps` (`ios_username`,`ios_password`, `ios_account_type`) VALUES ('%s', '%s', '%s')", mysqli_real_escape_string($this->db, $ios_username), mysqli_real_escape_string($this->db, $encrypt_pass), mysqli_real_escape_string($this->db, $ios_account_type));
                $result = mysqli_query($this->db, $sql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if (mysqli_affected_rows($this->db) > 0) {
                        $error = array('status' => "Success", "msg" => $roletext);
                        $this->response($this->json($error), 200);
                    } else {
                        $error = array('status' => "Failed", "msg" => $roletext);
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
    }
    
    public function uploadiosscreenshotsINDB($company_id, $scheme_name, $_5inch_file1, $_5inch_file2, $_5inch_file3, $_5inch_file4, $_6inch_file1, $_6inch_file2, $_6inch_file3, $_6inch_file4
    , $ipad_file1, $ipad_file2, $ipad_file3, $ipad_file4) {
        
        $old = getcwd(); // Save the current directory
        $dir_name = "../../../$this->upload_folder_name/whitelabel";
        chdir($dir_name);
        if (is_dir($scheme_name)) {
            system('rm -rf ' . escapeshellarg("$scheme_name"), $retval);
        }
        mkdir($scheme_name);
        chmod($scheme_name,0777);
        chdir($scheme_name);
        mkdir("ios");
        chmod("ios",0777);
        chdir("ios");
        if (strpos($_5inch_file1, 'base64') !== false) {
           $this->writescreenshot("For 5.5 inch IPHONE only — Preview 1.png",$_5inch_file1);
        }
        if (strpos($_5inch_file2, 'base64') !== false) {
        $this->writescreenshot("For 5.5 inch IPHONE only — Preview 2.png",$_5inch_file2);
        }
        if (strpos($_5inch_file3, 'base64') !== false) {
        $this->writescreenshot("For 5.5 inch IPHONE only — Preview 3.png",$_5inch_file3);
        }
        if (strpos($_5inch_file4, 'base64') !== false) {
        $this->writescreenshot("For 5.5 inch IPHONE only — Preview 4.png",$_5inch_file4);
        }
        
        if (strpos($_6inch_file1, 'base64') !== false) {
        $this->writescreenshot("For 6.5 inch IPHONE only — Preview 1.png",$_6inch_file1);
        }
        if (strpos($_6inch_file2, 'base64') !== false) {
        $this->writescreenshot("For 6.5 inch IPHONE only — Preview 2.png",$_6inch_file2);
        }
        if (strpos($_6inch_file3, 'base64') !== false) {
        $this->writescreenshot("For 6.5 inch IPHONE only — Preview 3.png",$_6inch_file3);
        }
        if (strpos($_6inch_file4, 'base64') !== false) {
        $this->writescreenshot("For 6.5 inch IPHONE only — Preview 4.png",$_6inch_file4);
        }
        
        if (strpos($ipad_file1, 'base64') !== false) {
        $this->writescreenshot("For IPAD only — Preview 1.png",$ipad_file1);
        $this->writescreenshot("ipadPro129.png",$ipad_file1);
        }
        if (strpos($ipad_file2, 'base64') !== false) {
        $this->writescreenshot("For IPAD only — Preview 2.png",$ipad_file2);
        $this->writescreenshot("ipadPro1292.png",$ipad_file2);
        }
        if (strpos($ipad_file3, 'base64') !== false) {
        $this->writescreenshot("For IPAD only — Preview 3.png",$ipad_file3);
        $this->writescreenshot("ipadPro1293.png",$ipad_file3);
        }
        if (strpos($ipad_file4, 'base64') !== false) {
        $this->writescreenshot("For IPAD only — Preview 4.png",$ipad_file4);
        $this->writescreenshot("ipadPro129 4.png",$ipad_file4);
        }
        
        $bucketName = 'dev.app-files.mystudio.app';
        $IAM_KEY = 'AKIAIGUWYNJGAHDPJGIA';
        $IAM_SECRET = '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';
        // Connect to AWS
        try {
            $s3 = S3Client::factory(
                            array(
                                'credentials' => array(
                                    'key' => $IAM_KEY,
                                    'secret' => $IAM_SECRET
                                ),
                                'version' => 'latest',
                                'region' => 'us-east-1'
                            )
            );
        } catch (Exception $e) {
            log_info($e->getMessage());
            $error = array('status' => "Failed", "msg" => "Failed to get connection");
                $this->response($this->json($error), 200);
        }

        // Where the files will be source from
        $source = getcwd();
        // Where the files will be transferred to
        $dest = "s3://$bucketName/aws_assets/Screenshots/$scheme_name/ios/en-US";
        // Create a transfer object
        $manager = new \Aws\S3\Transfer($s3, $source, $dest);
        // Perform the transfer synchronously
        $manager->transfer();
        chdir($old);
        chdir($dir_name);
        if (is_dir($scheme_name)) {
            system('rm -rf ' . escapeshellarg("$scheme_name"), $retval);
        }
        chdir($old);
        $error = array('status' => "Success", "msg" => "Updated Successfully");
        $this->response($this->json($error), 200);
    }
    
    public function uploadandroidscreenshotsINDB($company_id,$scheme_name,$and_phone_file1,$and_phone_file2,$and_phone_file3,$and_phone_file4,
                     $and_tab_file1,$and_tab_file2,$and_tab_file3,$and_tab_file4){
        $old = getcwd(); // Save the current directory
        $dir_name = "../../../$this->upload_folder_name/whitelabel";
        chdir($dir_name);
        if (is_dir($scheme_name)) {
            system('rm -rf ' . escapeshellarg("$scheme_name"), $retval);
        }
        mkdir($scheme_name);
        chmod($scheme_name,0777);
        chdir($scheme_name);
        mkdir("android");
        chmod("android",0777);
        chdir("android");
        mkdir("phoneScreenshots");
        chmod("phoneScreenshots",0777);
        chdir("phoneScreenshots");
        if (strpos($and_phone_file1, 'base64') !== false) {
           $this->writescreenshot("Preview 1.png",$and_phone_file1);
        }
        if (strpos($and_phone_file2, 'base64') !== false) {
        $this->writescreenshot("Preview 2.png",$and_phone_file2);
        }
        if (strpos($and_phone_file3, 'base64') !== false) {
        $this->writescreenshot("Preview 3.png",$and_phone_file3);
        }
        if (strpos($and_phone_file4, 'base64') !== false) {
        $this->writescreenshot("Preview 4.png",$and_phone_file4);
        }
             
        chdir($old);
        chdir($dir_name);
        chdir($scheme_name);
        chdir("android");
        mkdir("sevenInchScreenshots");
        chmod("sevenInchScreenshots",0777);
        chdir("sevenInchScreenshots");
        if (strpos($and_tab_file1, 'base64') !== false) {
        $this->writescreenshot("Preview 1.png",$and_tab_file1);
        }
        if (strpos($and_tab_file2, 'base64') !== false) {
        $this->writescreenshot("Preview 2.png",$and_tab_file2);
        }
        if (strpos($and_tab_file3, 'base64') !== false) {
        $this->writescreenshot("Preview 3.png",$and_tab_file3);
        }
        if (strpos($and_tab_file4, 'base64') !== false) {
        $this->writescreenshot("Preview 4.png",$and_tab_file4);
        }
        chdir($old);
        $bucketName = 'dev.app-files.mystudio.app';
        $IAM_KEY = 'AKIAIGUWYNJGAHDPJGIA';
        $IAM_SECRET = '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';
        // Connect to AWS
        try {
            $s3 = S3Client::factory(
                            array(
                                'credentials' => array(
                                    'key' => $IAM_KEY,
                                    'secret' => $IAM_SECRET
                                ),
                                'version' => 'latest',
                                'region' => 'us-east-1'
                            )
            );
        } catch (Exception $e) {
            log_info($e->getMessage());
            $error = array('status' => "Failed", "msg" => "Failed to get connection");
                $this->response($this->json($error), 200);
        }

        // Where the files will be source from
        $source = "../../../$this->upload_folder_name/whitelabel/$scheme_name/android";
        // Where the files will be transferred to
        $dest = "s3://$bucketName/aws_assets/Screenshots/$scheme_name/android/en-US/images";
        // Create a transfer object
        $manager = new \Aws\S3\Transfer($s3, $source, $dest);
        // Perform the transfer synchronously
        $manager->transfer();
        chdir($old);
        chdir($dir_name);
        if (is_dir($scheme_name)) {
            system('rm -rf ' . escapeshellarg("$scheme_name"), $retval);
        }
        chdir($old);
        $error = array('status' => "Success", "msg" => "Updated Successfully");
        $this->response($this->json($error), 200);
    }
    
    public  function createiconsandsplashios($company_id,$splash_flag,$icon_content,$hex_value,$ios_zip,$bundle_id,$type,$scheme_name){    
        if($type == 'B' || $type == 'I') {
            $sql3 = sprintf("UPDATE `white_label_apps` SET `ios_hex_value` = '$hex_value' WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
            $result3 = mysqli_query($this->db, $sql3);
            if (!$result3) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        if($type == 'B' || $type == 'A') {
            $sql3 = sprintf("UPDATE `white_label_apps` SET `android_hex_value` = '$hex_value' WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
            $result3 = mysqli_query($this->db, $sql3);
            if (!$result3) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        $old = getcwd(); // Save the current directory
        chdir("../../../$this->upload_folder_name/whitelabel");
        if (is_dir($bundle_id)) {
            system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
        }
        chdir($old);
        $red = hexdec("$hex_value[1]$hex_value[2]"); 
        $green = hexdec("$hex_value[3]$hex_value[4]"); 
        $blue = hexdec("$hex_value[5]$hex_value[6]"); 
        if($type == "B" || $type == "A"){
        $this->createandviewimagesandroid($company_id,$splash_flag,$icon_content,$hex_value,$ios_zip,$bundle_id,$type);
        if($type == "A"){
            chdir($old);
            $this->savezipinS3($bundle_id, $type,$scheme_name);
            chdir("../../../$this->upload_folder_name/whitelabel");
            system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
            chdir($old);
            $error = array('status' => "Success", "msg" => "Uploaded successfully");
            $this->response($this->json($error), 200);
            }
        }else{
            $whitelabel_dir_name = "../../../$this->upload_folder_name/whitelabel";
            chdir($whitelabel_dir_name);
            
            mkdir("$bundle_id");
            chmod($bundle_id, 0777);
            chdir($bundle_id); //go bundle id dir
        }
        
        chdir($old);
        if($type == "I" || $type == "B"){
        $whitelabel_dir_name = "../../../$this->upload_folder_name/whitelabel/$bundle_id";
        chdir($whitelabel_dir_name);
        
        $filename = "icon.png";
        $icon_content_encoded = explode("base64,", $icon_content);
        $file_content_decoded = base64_decode($icon_content_encoded[1]);
        $ifp = fopen($filename, "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
        //logo image
            list($width,$height) = getimagesize($filename);
            $imagetruecolor = imagecreatetruecolor(300, 300);
            $white = imagecolorallocate($imagetruecolor, 255, 255, 255);//white background
            imagefill($imagetruecolor, 0, 0, $white);
            $newimage = imagecreatefrompng($filename);
            imagecopyresampled($imagetruecolor, $newimage, 0, 0, 0, 0, 300, 300, $width, $height);
            imagepng($imagetruecolor, "$bundle_id.png", 9);
        
        
        mkdir("ios");
        chmod("ios",0777);
        chdir("ios");//go bundle id dir
        mkdir("icons");
        chmod("icons",0777);
        chdir("icons");
        chdir($old);
        chdir("../../../$this->upload_folder_name/whitelabel");
        copy("Contents.json", "$bundle_id/ios/icons/Contents.json");
        chdir("$bundle_id/ios/icons");
        $filename = "icon.png";
        $icon_content_encoded = explode("base64,", $icon_content);
        $file_content_decoded = base64_decode($icon_content_encoded[1]);
        $ifp = fopen($filename, "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
               
        
        $resolution_array = [20,40,60,29,58,87,40,80,120,57,114,60,120,180,72,144,76,152,228,167,50,100,1024];
        for($i = 0 ; $i < count($resolution_array) ; $i++){
            list($width,$height) = getimagesize($filename);
            $imagetruecolor = imagecreatetruecolor($resolution_array[$i], $resolution_array[$i]);
            $white = imagecolorallocate($imagetruecolor, 255, 255, 255);//white background
            imagefill($imagetruecolor, 0, 0, $white);
            $newimage = imagecreatefrompng($filename);
            imagecopyresampled($imagetruecolor, $newimage, 0, 0, 0, 0, $resolution_array[$i], $resolution_array[$i], $width, $height);
            if($i == 0){
            imagepng($imagetruecolor, "Icon-App-20x20@1x.png", 9);
            }else if($i == 1){
            imagepng($imagetruecolor, "Icon-App-20x20@2x.png", 9);
            }else if($i == 2){
            imagepng($imagetruecolor, "Icon-App-20x20@3x.png", 9);
            }else if($i == 3){
            imagepng($imagetruecolor, "Icon-App-29x29@1x.png", 9);
            }else if($i == 4){
            imagepng($imagetruecolor, "Icon-App-29x29@2x.png", 9);
            }else if($i == 5){
            imagepng($imagetruecolor, "Icon-App-29x29@3x.png", 9);
            }else if($i == 6){
            imagepng($imagetruecolor, "Icon-App-40x40@1x.png", 9);
            }else if($i == 7){
            imagepng($imagetruecolor, "Icon-App-40x40@2x.png", 9);
            }else if($i == 8){
            imagepng($imagetruecolor, "Icon-App-40x40@3x.png", 9);
            }else if($i == 9){
            imagepng($imagetruecolor, "Icon-App-57x57@1x.png", 9);
            }else if($i == 10){
            imagepng($imagetruecolor, "Icon-App-57x57@2x.png", 9);
            }else if($i == 11){
            imagepng($imagetruecolor, "Icon-App-60x60@1x.png", 9);
            }else if($i == 12){
            imagepng($imagetruecolor, "Icon-App-60x60@2x.png", 9);
            }else if($i == 13){
            imagepng($imagetruecolor, "Icon-App-60x60@3x.png", 9);
            }else if($i == 14){
            imagepng($imagetruecolor, "Icon-App-72x72@1x.png", 9);
            }else if($i == 15){
            imagepng($imagetruecolor, "Icon-App-72x72@2x.png", 9);
            }else if($i == 16){
            imagepng($imagetruecolor, "Icon-App-76x76@1x.png", 9);
            }else if($i == 17){
            imagepng($imagetruecolor, "Icon-App-76x76@2x.png", 9);
            }else if($i == 18){
            imagepng($imagetruecolor, "Icon-App-76x76@3x.png", 9);
            }else if($i == 19){
            imagepng($imagetruecolor, "Icon-App-83.5x83.5@2x.png", 9);
            }else if($i == 20){
            imagepng($imagetruecolor, "Icon-Small-50x50@1x.png", 9);
            }else if($i == 21){
            imagepng($imagetruecolor, "Icon-Small-50x50@2x.png", 9);
            }else if($i == 22){
            imagepng($imagetruecolor, "Icon-App-1024x1024@1x.png", 9);
            }
        }
//        unlink("icon.png");
        chdir($old);
        if($splash_flag == "G"){
        chdir($whitelabel_dir_name);//go whitelabel folder
        chdir("ios");
        mkdir("launch_screen");
        chmod("launch_screen",0777);
        chdir("launch_screen");
        $filename = "icon.png";
        $icon_content_encoded = explode("base64,", $icon_content);
        $file_content_decoded = base64_decode($icon_content_encoded[1]);
        $ifp = fopen($filename, "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
//        for 640 * 1136
        $this->createsplashimage(170,418,300,300,640,1136,"Default-568h@2x~iphone",$filename,$red,$green,$blue);
        $this->createsplashimage(225,502,300,300,750,1334,"Default-667h",$filename,$red,$green,$blue);
        $this->createsplashimage(170,330,300,300,640,960,"Default@2x~iphone",$filename,$red,$green,$blue);
//        for iphone x
        $this->createsplashimage(262.5,918,600,600,1125,2436,"iphonex",$filename,$red,$green,$blue);
//        for 1242 x 2208
        $this->createsplashimage(321,804,600,600,1242,2208,"Default-736h",$filename,$red,$green,$blue);
        // for ipad portrait 768 x 1024
        $this->createsplashimage(234,362,300,300,768,1024,"Default-Portrait~ipad",$filename,$red,$green,$blue);
        // for ipad portrait @2x 1536 x 2048
        $this->createsplashimage(468,724,600,600,1536,2048,"Default-Portrait@2x~ipad",$filename,$red,$green,$blue);
        // for ipad landscape 2208 x 1242
        $this->createsplashimage(804,321,600,600,2208,1242,"Default-Landscape-736h",$filename,$red,$green,$blue);
        // for ipad landscape 2048 x 1536
        $this->createsplashimage(724,468,600,600,2048,1536,"Default-Landscape@2x~ipad",$filename,$red,$green,$blue);
        // for ipad landscape 1024 x 768
        $this->createsplashimage(362,234,300,300,1024,768,"Default-Landscape~ipad",$filename,$red,$green,$blue);
        // for DEAFULT iphone 320 x 480
        $this->createsplashimage(60,140,200,200,320,480,"Default~iphone",$filename,$red,$green,$blue);
        // for iphonex landscape 2436 x 1125
        $this->createsplashimage(918,262.5,600,600,2436,1125,"iphonex-lanscape",$filename,$red,$green,$blue);
        unlink("icon.png");
        }else{
            $this->ioszipextract($bundle_id,$ios_zip);
        }
        chdir($old);
        }
        $this->savezipinS3($bundle_id,$type,$scheme_name);
        
        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
        chdir($old);
        $error = array('status' => "Success", "msg" => "Uploaded successfully");
        $this->response($this->json($error), 200);
    }
    
    public function createsplashimage($x,$y,$img_height,$img_width,$res_width,$res_height,$name,$filename,$red,$green,$blue){
        list($width, $height) = getimagesize($filename);
            $imagetruecolor = imagecreatetruecolor($res_width, $res_height);
            $white = imagecolorallocate($imagetruecolor, $red, $green, $blue);
            imagefill($imagetruecolor, 0, 0, $white);
            $newimage = imagecreatefrompng($filename);
            imagecopyresampled($imagetruecolor, $newimage, $x, $y, 0, 0, $img_width, $img_height, $width, $height);
            imagepng($imagetruecolor, "$name.png", 9);
    }
    
    public function createiconandroid($filename,$res,$name){
        list($width, $height) = getimagesize($filename);
        $imagetruecolor = imagecreatetruecolor($res, $res);
        $white = imagecolorallocate($imagetruecolor, 255, 255, 255); //white background
        imagefill($imagetruecolor, 0, 0, $white);
        $newimage = imagecreatefrompng($filename);
        imagecopyresampled($imagetruecolor, $newimage, 0, 0, 0, 0, $res, $res, $width, $height);
        imagepng($imagetruecolor, $name, 9);
    }
    public  function createandviewimagesandroid($company_id,$splash_flag,$icon_content,$hex_value,$android_zip,$bundle_id,$type){
        $red = hexdec("$hex_value[1]$hex_value[2]"); 
        $green = hexdec("$hex_value[3]$hex_value[4]"); 
        $blue = hexdec("$hex_value[5]$hex_value[6]"); 
        $old = getcwd(); // Save the current directory
        $whitelabel_dir_name = "../../../$this->upload_folder_name/whitelabel";
        chdir($whitelabel_dir_name);
        mkdir("$bundle_id");
        chmod($bundle_id,0777);
        chdir($bundle_id);//go bundle id dir
        if($type == 'A'){
            $filename = "icon.png";
            $icon_content_encoded = explode("base64,", $icon_content);
            $file_content_decoded = base64_decode($icon_content_encoded[1]);
            $ifp = fopen($filename, "wb");
            fwrite($ifp, $file_content_decoded);
            fclose($ifp);
            //logo image
            list($width, $height) = getimagesize($filename);
            $imagetruecolor = imagecreatetruecolor(300, 300);
            $white = imagecolorallocate($imagetruecolor, 255, 255, 255); //white background
            imagefill($imagetruecolor, 0, 0, $white);
            $newimage = imagecreatefrompng($filename);
            imagecopyresampled($imagetruecolor, $newimage, 0, 0, 0, 0, 300, 300, $width, $height);
            imagepng($imagetruecolor, "$bundle_id.png", 9);
        }
        mkdir("android");
        chmod("android",0777);
        chdir("android");//go bundle id dir
        $filename = "icon.png";
        $icon_content_encoded = explode("base64,", $icon_content);
        $file_content_decoded = base64_decode($icon_content_encoded[1]);
        $ifp = fopen($filename, "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
        $this->createiconandroid($filename,72,"icon_temp.png");
        mkdir("drawable-hdpi");
        chmod("drawable-hdpi",0777);
        copy('icon_temp.png', 'drawable-hdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,144,"icon_temp.png");
        mkdir("drawable-xxhdpi");
        chmod("drawable-xxhdpi",0777);
        copy('icon_temp.png', 'drawable-xxhdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,48,"icon_temp.png");
        mkdir("drawable-mdpi");
        chmod("drawable-mdpi",0777);
        copy('icon_temp.png', 'drawable-mdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,96,"icon_temp.png");
        mkdir("drawable-xhdpi");
        chmod("drawable-xhdpi",0777);
        copy('icon_temp.png', 'drawable-xhdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,72,"icon_temp.png");
        mkdir("mipmap-hdpi");
        chmod("mipmap-hdpi",0777);
        copy('icon_temp.png', 'mipmap-hdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,36,"icon_temp.png");
        mkdir("mipmap-ldpi");
        chmod("mipmap-ldpi",0777);
        copy('icon_temp.png', 'mipmap-ldpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,48,"icon_temp.png");
        mkdir("mipmap-mdpi");
        chmod("mipmap-mdpi",0777);
        copy('icon_temp.png', 'mipmap-mdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,96,"icon_temp.png");
        mkdir("mipmap-xhdpi");
        chmod("mipmap-xhdpi",0777);
        copy('icon_temp.png', 'mipmap-xhdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,144,"icon_temp.png");
        mkdir("mipmap-xxhdpi");
        chmod("mipmap-xxhdpi",0777);
        copy('icon_temp.png', 'mipmap-xxhdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,192,"icon_temp.png");
        mkdir("mipmap-xxxhdpi");
        chmod("mipmap-xxxhdpi",0777);
        copy('icon_temp.png', 'mipmap-xxxhdpi/icon.png');
        unlink('icon_temp.png');
        unlink('icon.png');
        chdir($old);
        //splash
//        if($splash_flag == "G"){
        chdir($whitelabel_dir_name."/".$bundle_id."/android");
        $filename = "icon.png";
        $icon_content_encoded = explode("base64,", $icon_content);
        $file_content_decoded = base64_decode($icon_content_encoded[1]);
        $ifp = fopen($filename, "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
        $this->createsplashimage(300,140,200,200,800,480,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-land-hdpi");
        chmod("drawable-land-hdpi",0777);
        copy('screen.png', 'drawable-land-hdpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(110,50,100,100,320,200,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-land-ldpi");
        chmod("drawable-land-ldpi",0777);
        copy('screen.png', 'drawable-land-ldpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(160,440,400,400,720,1280,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-port-xhdpi");
        chmod("drawable-port-xhdpi",0777);
        copy('screen.png', 'drawable-port-xhdpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(140,60,200,200,480,320,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-land-mdpi");
        chmod("drawable-land-mdpi",0777);
        copy('screen.png', 'drawable-land-mdpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(440,160,400,400,1280,720,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-land-xhdpi");
        chmod("drawable-land-xhdpi",0777);
        copy('screen.png', 'drawable-land-xhdpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(140,300,200,200,480,800,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-port-hdpi");
        chmod("drawable-port-hdpi",0777);
        copy('screen.png', 'drawable-port-hdpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(50,110,100,100,200,320,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-port-ldpi");
        chmod("drawable-port-ldpi",0777);
        copy('screen.png', 'drawable-port-ldpi/screen.png');
        unlink('screen.png');
        chdir($old);
        if($splash_flag == "U"){
            $this->androidzipextract($android_zip,$bundle_id);
        }
//        unlink('icon.png');
    }
    
    public function androidzipextract($android_zip,$bundle_id){
        $old = getcwd(); // Save the current directory
        $dir_name = "../../../$this->upload_folder_name/whitelabel/$bundle_id";
        chdir($dir_name);
        $ios_zip_enocded = explode("base64,", $android_zip);
        $file_content_decoded = base64_decode($ios_zip_enocded[1]);
        $ifp = fopen('android.zip', "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
        mkdir("androidzip");
        chmod("androidzip",0777);
        $zip = new ZipArchive;
        $res = $zip->open('android.zip');
        if ($res === TRUE) {
            $zip->extractTo('androidzip/');
            $zip->close();
        } else {
            chdir("../../../$this->upload_folder_name/whitelabel");
            system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
            $error = array('status' => "Failed", "msg" => "Invalid zip file");
            $this->response($this->json($error), 200);
        }
        unlink('android.zip');
        $directory = 'androidzip';
        $folder_count = 7;
        if (!is_dir($directory)) {
            chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
            $error = array('status' => "Failed", "msg" => "Invalid diretory path");
            $this->response($this->json($error), 200);
        }
        chdir($directory);
        if (is_dir("__MACOSX")) {
            chmod("__MACOSX",0777);
            $folder_count = 8;
        }
        chdir($old);
        chdir("../../../$this->upload_folder_name/whitelabel/$bundle_id");
        $folder = array();
        foreach (scandir($directory) as $file) {
            if ('.' === $file) continue;
            if ('..' === $file) continue;

            $folder[] = $file;
        }
//        for ios image count error
        if(count($folder) != $folder_count){
            chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
            $error = array('status' => "Failed", "msg" => "Some of the images in zip file is missing");
            $this->response($this->json($error), 200);
        }
        $android_folder_names = ["drawable-land-hdpi","drawable-land-ldpi","drawable-land-mdpi","drawable-land-xhdpi",
            "drawable-port-hdpi","drawable-port-ldpi","drawable-port-xhdpi"];
        for($i = 0; $i< count($android_folder_names); $i++) {
           if (in_array($android_folder_names[$i], $folder)) {
                 chdir("androidzip");
                 chdir("$android_folder_names[$i]");
                 $width = $height = 0;
                 if($android_folder_names[$i] == "drawable-land-hdpi"){
                     list($width, $height) = getimagesize("screen.png");
                 if($width != 800 && $height != 480){
                        chdir($old);
                        chdir("../../../$this->upload_folder_name/whitelabel");
                        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$android_folder_names[$i] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                    copy('screen.png', __DIR__."/../../../$this->upload_folder_name/whitelabel/$bundle_id/android/drawable-land-hdpi/screen.png");
                    chdir("..");
                    chdir("..");
                 }
                 
                 if($android_folder_names[$i] == "drawable-land-ldpi"){
                     list($width, $height) = getimagesize("screen.png");
                 if($width != 320 && $height != 200){
                     chdir($old);
                        chdir("../../../$this->upload_folder_name/whitelabel");
                        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$android_folder_names[$i] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                    copy('screen.png', __DIR__."/../../../$this->upload_folder_name/whitelabel/$bundle_id/android/drawable-land-ldpi/screen.png");
                    chdir("..");
                    chdir("..");
                 }
                 
                 if($android_folder_names[$i] == "drawable-land-mdpi"){
                     list($width, $height) = getimagesize("screen.png");
                 if($width != 480 && $height != 320){
                     chdir($old);
                        chdir("../../../$this->upload_folder_name/whitelabel");
                        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$android_folder_names[$i] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                    copy('screen.png', __DIR__."/../../../$this->upload_folder_name/whitelabel/$bundle_id/android/drawable-land-mdpi/screen.png");
                    chdir("..");
                    chdir("..");
                 }
                 
                 if($android_folder_names[$i] == "drawable-land-xhdpi"){
                     list($width, $height) = getimagesize("screen.png");
                 if($width != 1280 && $height != 720){
                     chdir($old);
                        chdir("../../../$this->upload_folder_name/whitelabel");
                        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$android_folder_names[$i] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                    copy('screen.png', __DIR__."/../../../$this->upload_folder_name/whitelabel/$bundle_id/android/drawable-land-xhdpi/screen.png");
                    chdir("..");
                    chdir("..");
                 }
                 
                 if($android_folder_names[$i] == "drawable-port-hdpi"){
                     list($width, $height) = getimagesize("screen.png");
                 if($width != 480 && $height != 800){
                     chdir($old);
                        chdir("../../../$this->upload_folder_name/whitelabel");
                        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$android_folder_names[$i] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                    copy('screen.png', __DIR__."/../../../$this->upload_folder_name/whitelabel/$bundle_id/android/drawable-port-hdpi/screen.png");
                    chdir("..");
                    chdir("..");
                 }
                 
                 if($android_folder_names[$i] == "drawable-port-ldpi"){
                     list($width, $height) = getimagesize("screen.png");
                 if($width != 200 && $height != 320){
                     chdir($old);
                        chdir("../../../$this->upload_folder_name/whitelabel");
                        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$android_folder_names[$i] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                    copy('screen.png', __DIR__."/../../../$this->upload_folder_name/whitelabel/$bundle_id/android/drawable-port-ldpi/screen.png");
                    chdir("..");
                    chdir("..");
                 }
                 
                 if($android_folder_names[$i] == "drawable-port-xhdpi"){
                     list($width, $height) = getimagesize("screen.png");
                 if($width != 720 && $height != 1280){
                     chdir($old);
                        chdir("../../../$this->upload_folder_name/whitelabel");
                        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$android_folder_names[$i] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                    copy('screen.png', __DIR__."/../../../$this->upload_folder_name/whitelabel/$bundle_id/android/drawable-port-xhdpi/screen.png");
                   chdir("..");
                    chdir("..");
                 }
            }else{
                chdir($old);
                chdir("../../../$this->upload_folder_name/whitelabel");
                 system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "Some folders in the zip file are missing!");
                        $this->response($this->json($error), 200);
            }
        }
        chdir($old);
    }
    
    public function ioszipextract($bundle_id,$ioszip_file){
        $old = getcwd(); // Save the current directory
        $dir_name = "../../../$this->upload_folder_name/whitelabel/$bundle_id";
        chdir($dir_name);
        $ios_zip_enocded = explode("base64,", $ioszip_file);
        $file_content_decoded = base64_decode($ios_zip_enocded[1]);
        $ifp = fopen('ios.zip', "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
        mkdir("launch_screen");
        chmod("launch_screen",0777);
        $zip = new ZipArchive;
        $res = $zip->open('ios.zip');
        if ($res === TRUE) {
            $zip->extractTo('launch_screen/');
            $zip->close();
        } else {
            chdir("../../../$this->upload_folder_name/whitelabel");
            system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
            $error = array('status' => "Failed", "msg" => "Invalid zip file");
            $this->response($this->json($error), 200);
        }
        unlink('ios.zip');
        $directory = 'launch_screen';
        $files_count = 12;
        if (! is_dir($directory)) {
            chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
            $error = array('status' => "Failed", "msg" => "Invalid diretory path");
            $this->response($this->json($error), 200);
        }
        chdir($directory);
        if (is_dir("__MACOSX")) {
            chmod("__MACOSX",0777);
            $files_count = 13;
        }
        chdir($old);
        chdir($dir_name);
        $files = array();
        foreach (scandir($directory) as $file) {
            if ('.' === $file) continue;
            if ('..' === $file) continue;

            $files[] = $file;
        }
//        for ios image count error
        if(count($files) != $files_count){
            chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
            $error = array('status' => "Failed", "msg" => "Some of the images in zip file is missing");
            $this->response($this->json($error), 200);
        }
        $ios_splash_file_names = ["Default-568h@2x~iphone.png","Default-667h.png","Default-736h.png","Default-Landscape-736h.png",
            "Default-Landscape@2x~ipad.png","Default-Landscape~ipad.png","Default-Portrait@2x~ipad.png","Default-Portrait~ipad.png",
            "Default@2x~iphone.png","iphonex.png","Default~iphone.png","iphonex-Landscape.png"];
        chdir("launch_screen");
        for( $j=1 ; $j < count($files) ; $j++ ){//skip 1st index (hidden file)
            if( $files[$j] != "__MACOSX"){
            if (in_array(($files[$j]), $ios_splash_file_names)) {
                $width = $height = 0;
                if($files[$j] == "Default-568h@2x~iphone.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 640 && $height != 1136){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default@2x~iphone.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 640 && $height != 960){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-667h.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 750 && $height != 1334){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "iphonex.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 1125 && $height != 2436){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-736h.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 1242 && $height != 2208){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-Portrait~ipad.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 768 && $height != 1024){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-Portrait@2x~ipad.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 1536 && $height != 2048){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-Landscape-736h.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 2208 && $height != 1242){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-Landscape@2x~ipad.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 2048 && $height != 1536){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-Landscape~ipad.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 1024 && $height != 768){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default~iphone.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 320 && $height != 480){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "iphonex-Landscape.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 2436 && $height != 1125){
                        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }
            } else {
                chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
                $error = array('status' => "Failed", "msg" => "$files[$j] file is not valid");
                $this->response($this->json($error), 200);
            }
            }
        }
        chdir($old);
    }
    
    public function savezipinS3($bundle_id,$type,$scheme_name){
        // AWS Info
	$bucketName = 'dev.app-files.mystudio.app';
	$IAM_KEY = 'AKIAIGUWYNJGAHDPJGIA';
	$IAM_SECRET = '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';
	// Connect to AWS
	try {
		$s3 = S3Client::factory(
			array(
				'credentials' => array(
					'key' => $IAM_KEY,
					'secret' => $IAM_SECRET
				),
				'version' => 'latest',
				'region'  => 'us-east-1'
			)
		);
	} catch (Exception $e) {
            log_info($e->getMessage());
//		$error = array('status' => "Failed", "msg" => $e->getMessage());
//                $this->response($this->json($error), 200);
	}
	$old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel/$bundle_id";
                chdir($dir_name);
	// For this, I would generate a unqiue random string for the key name. But you can do whatever.
	// single file upload
                if($type == "B" || $type== "I" ){
                    try {
                            $s3->putObject(
                                    array(
                                            'Bucket'=>$bucketName,
                                            'Key' =>  "aws_assets/MyStudioWhiteLabel/platforms/ios/www/$bundle_id.png",
                                            'SourceFile' => "$bundle_id.png",
                                            'StorageClass' => 'REDUCED_REDUNDANCY'
                                    )
                            );
                    } catch (S3Exception $e) {
                        log_info($e->getMessage());
            //		$error = array('status' => "Failed", "msg" => $e->getMessage());
            //                $this->response($this->json($error), 200);
                    } catch (Exception $e) {
                        log_info($e->getMessage());
            //		$error = array('status' => "Failed", "msg" => $e->getMessage());
            //                $this->response($this->json($error), 200);
                    }
                }
                
                if($type == "B" || $type== "A" ){
                    try {
                            $s3->putObject(
                                    array(
                                            'Bucket'=>$bucketName,
                                            'Key' =>  "aws_assets/MyStudioWhiteLabel/platforms/android/assets/www/$bundle_id.png",
                                            'SourceFile' => "$bundle_id.png",
                                            'StorageClass' => 'REDUCED_REDUNDANCY'
                                    )
                            );
                    } catch (S3Exception $e) {
                        log_info($e->getMessage());
            //		$error = array('status' => "Failed", "msg" => $e->getMessage());
            //                $this->response($this->json($error), 200);
                    } catch (Exception $e) {
                        log_info($e->getMessage());
            //		$error = array('status' => "Failed", "msg" => $e->getMessage());
            //                $this->response($this->json($error), 200);
                    }
                }
                
                if($type == "B" || $type == "A"){
                //-------move to android folder-------
                // Where the files will be source from
                $source = $dir_name."/android";
                // Where the files will be transferred to
                $dest = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/android/res-$scheme_name";
                // Create a transfer object
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                // Perform the transfer synchronously
                $manager->transfer();
                
                // Where the files will be source from
                $source = "../../../$this->upload_folder_name/whitelabel/android_default_files";
                // Where the files will be transferred to
                $dest = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/android/res-$scheme_name";
                // Create a transfer object
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                // Perform the transfer synchronously
                $manager->transfer();
                }
                if($type == "I" || $type == "B"){
                // Where the files will be source from
                $source = $dir_name."/ios/icons";
                // Where the files will be transferred to
                $dest = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$scheme_name.appiconset";
                // Create a transfer object
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                // Perform the transfer synchronously
                $manager->transfer();
                
                //-------move to ios splash folder-------
                // Where the files will be source from
                $source = $dir_name."/ios/launch_screen";
                // Where the files will be transferred to
                $dest = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$scheme_name.launchimage";
                // Create a transfer object
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                // Perform the transfer synchronously
                $manager->transfer();
                }
                chdir($old);
    }
    
    public function deletescreenshots($scheme_name){
        $bucketName = 'dev.app-files.mystudio.app';
        $IAM_KEY = 'AKIAIGUWYNJGAHDPJGIA';
        $IAM_SECRET = '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';
        // Connect to AWS
        try {
            $s3 = S3Client::factory(
                            array(
                                'credentials' => array(
                                    'key' => $IAM_KEY,
                                    'secret' => $IAM_SECRET
                                ),
                                'version' => 'latest',
                                'region' => 'us-east-1'
                            )
            );
        } catch (Exception $e) {
            log_info($e->getMessage());
            $error = array('status' => "Failed", "msg" => "Failed to get connection");
                $this->response($this->json($error), 200);
        }
        $response = $s3->getIterator(
                'ListObjects', [
            'Bucket' => $bucketName,
            'Prefix' => "aws_assets/Screenshots/$scheme_name/ios/en-US"
                ]
        );
        //delete each 
        foreach ($response as $object) {
            $fileName = $object['Key'];
            $s3->deleteObject([
                'Bucket' => $bucketName,
                'Key' => $fileName
            ]);
        }//foreach
    }
    
    public function copyanddelicons($old_scheme_name,$new_scheme_name,$old_bundle_id,$new_bundle_id){
        $old=getcwd();
        $bucketName = 'dev.app-files.mystudio.app';
        $IAM_KEY = 'AKIAIGUWYNJGAHDPJGIA';
        $IAM_SECRET = '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';
        try {
            $s3 = S3Client::factory(
                            array(
                                'credentials' => array(
                                    'key' => $IAM_KEY,
                                    'secret' => $IAM_SECRET
                                ),
                                'version' => 'latest',
                                'region' => 'us-east-1'
                            )
            );
        } catch (Exception $e) {
            log_info($e->getMessage());
            $error = array('status' => "Failed", "msg" => "Failed to get connection");
                $this->response($this->json($error), 200);
        }       
        //ios
        $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$old_scheme_name.launchimage/Default~iphone.png");
        if ($file_exist_flag) {
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                if (is_dir($new_scheme_name.".appiconset")) {
                    system('rm -rf ' . escapeshellarg($new_scheme_name.".appiconset"), $retval);
                }
                mkdir($new_scheme_name.".appiconset");
                chmod($new_scheme_name.".appiconset",0777);
                chdir($new_scheme_name.".appiconset");
                //app icons
                $source = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$old_scheme_name.appiconset";
                $dest = getcwd();
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                $manager->transfer();
                //app screenshoyts
                chdir("..");
                if (is_dir($new_scheme_name.".launchimage")) {
                    system('rm -rf ' . escapeshellarg($new_scheme_name.".launchimage"), $retval);
                }
                mkdir($new_scheme_name.".launchimage");
                chmod($new_scheme_name.".launchimage",0777);
                chdir($new_scheme_name.".launchimage");
                $source = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$old_scheme_name.launchimage";
                $dest = getcwd();
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                $manager->transfer();
                chdir("..");
//                copy new scheme folder to s3
                //launch image
                $dest = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$new_scheme_name.launchimage";
                $source = getcwd()."/".$new_scheme_name.".launchimage/";
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                $manager->transfer();
                //icons
                $dest = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$new_scheme_name.appiconset";
                $source = getcwd()."/".$new_scheme_name.".appiconset/";
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                $manager->transfer();     
                
                //download 3008 300 ios
                $source = "aws_assets/MyStudioWhiteLabel/platforms/ios/www/$old_bundle_id.png";
                $result = $s3->getObject(array(
                    'Bucket' => $bucketName,
                    'Key'    => $source,
                    'SaveAs' => getcwd()."/$old_bundle_id.png",
                ));
                //upload 300 * 300 ios
                try {
                            $s3->putObject(
                                    array(
                                            'Bucket'=>$bucketName,
                                            'Key' =>  "aws_assets/MyStudioWhiteLabel/platforms/ios/www/$new_bundle_id.png",
                                            'SourceFile' => "$old_bundle_id.png",
                                            'StorageClass' => 'REDUCED_REDUNDANCY'
                                    )
                            );
                    } catch (S3Exception $e) {
                        log_info($e->getMessage());
            //		$error = array('status' => "Failed", "msg" => $e->getMessage());
            //                $this->response($this->json($error), 200);
                    } catch (Exception $e) {
                        log_info($e->getMessage());
            //		$error = array('status' => "Failed", "msg" => $e->getMessage());
            //                $this->response($this->json($error), 200);
                    }
                    //delete old 300 x 300
                    $s3->deleteObject([
                        'Bucket' => $bucketName,
                        'Key' => "aws_assets/MyStudioWhiteLabel/platforms/ios/www/$old_bundle_id.png"
                    ]);

            //delete old scheme foler for icon
        $response = $s3->getIterator(
                'ListObjects', [
            'Bucket' => $bucketName,
            'Prefix' => "aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$old_scheme_name.appiconset"
                ]
        );
        //delete each 
        foreach ($response as $object) {
            $fileName = $object['Key'];
            $s3->deleteObject([
                'Bucket' => $bucketName,
                'Key' => $fileName
            ]);
        }//foreach
        
        //delete old scheme foler for launchimage
        $response = $s3->getIterator(
                'ListObjects', [
            'Bucket' => $bucketName,
            'Prefix' => "aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/$old_scheme_name.launchimage"
                ]
        );
        //delete each 
        foreach ($response as $object) {
            $fileName = $object['Key'];
            $s3->deleteObject([
                'Bucket' => $bucketName,
                'Key' => $fileName
            ]);
        }//foreach
        if (is_dir($new_scheme_name . ".appiconset")) {
            system('rm -rf ' . escapeshellarg($new_scheme_name . ".appiconset"), $retval);
        }
        if (is_dir($new_scheme_name . ".launchimage")) {
            system('rm -rf ' . escapeshellarg($new_scheme_name . ".launchimage"), $retval);
        }
        if (file_exists("$old_bundle_id.png"))  
        { 
            unlink("$old_bundle_id.png");
        }
        chdir($old);
        }
        
        //android
        $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/MyStudioWhiteLabel/platforms/android/res-$old_scheme_name/drawable-hdpi/icon.png");
        if($file_exist_flag){
            $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $androidfoldeename= "res-$new_scheme_name";
                if (is_dir($androidfoldeename)) {
                    system('rm -rf ' . escapeshellarg($androidfoldeename), $retval);
                }
                mkdir($androidfoldeename);
                chmod($androidfoldeename,0777);
                chdir($androidfoldeename);
                //app icons
                $source = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/android/res-$old_scheme_name";
                $dest = getcwd();
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                $manager->transfer();
//                copy new scheme folder to s3
                $source = getcwd()."/";
                $dest = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/android/res-$new_scheme_name";
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                $manager->transfer(); 
                chdir("..");
                //download 3008 300 ios
                $source = "aws_assets/MyStudioWhiteLabel/platforms/android/assets/www/$old_bundle_id.png";
                $result = $s3->getObject(array(
                    'Bucket' => $bucketName,
                    'Key'    => $source,
                    'SaveAs' => getcwd()."/$old_bundle_id.png",
                ));
                //upload 300 * 300 ios
                try {
                            $s3->putObject(
                                    array(
                                            'Bucket'=>$bucketName,
                                            'Key' =>  "aws_assets/MyStudioWhiteLabel/platforms/android/assets/www/$new_bundle_id.png",
                                            'SourceFile' => "$old_bundle_id.png",
                                            'StorageClass' => 'REDUCED_REDUNDANCY'
                                    )
                            );
                    } catch (S3Exception $e) {
                        log_info($e->getMessage());
            //		$error = array('status' => "Failed", "msg" => $e->getMessage());
            //                $this->response($this->json($error), 200);
                    } catch (Exception $e) {
                        log_info($e->getMessage());
            //		$error = array('status' => "Failed", "msg" => $e->getMessage());
            //                $this->response($this->json($error), 200);
                    }
                    //delete old 300 x 300
                    $s3->deleteObject([
                        'Bucket' => $bucketName,
                        'Key' => "aws_assets/MyStudioWhiteLabel/platforms/android/assets/www/$old_bundle_id.png",
                    ]);

            //delete old scheme foler for icon
        $response = $s3->getIterator(
                'ListObjects', [
            'Bucket' => $bucketName,
            'Prefix' => "aws_assets/MyStudioWhiteLabel/platforms/android/res-$old_scheme_name"
                ]
        );
        //delete each 
        foreach ($response as $object) {
            $fileName = $object['Key'];
            $s3->deleteObject([
                'Bucket' => $bucketName,
                'Key' => $fileName
            ]);
        }//foreach
        
        if (is_dir($androidfoldeename)) {
            system('rm -rf ' . escapeshellarg($androidfoldeename), $retval);
        }
        if (file_exists("$old_bundle_id.png"))  
        { 
            unlink("$old_bundle_id.png");
        }
        }
        
        
        
        chdir($old);
    }
    
    public function copyscreenshots($scheme_name){
        $bucketName = 'dev.app-files.mystudio.app';
        $IAM_KEY = 'AKIAIGUWYNJGAHDPJGIA';
        $IAM_SECRET = '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';
        // Connect to AWS
        try {
            $s3 = S3Client::factory(
                            array(
                                'credentials' => array(
                                    'key' => $IAM_KEY,
                                    'secret' => $IAM_SECRET
                                ),
                                'version' => 'latest',
                                'region' => 'us-east-1'
                            )
            );
        } catch (Exception $e) {
            log_info($e->getMessage());
            $error = array('status' => "Failed", "msg" => "Failed to get connection");
                $this->response($this->json($error), 200);
        }
                $dir_name = "../../../$this->upload_folder_name/whitelabel/default_screenshots";
                $source = $dir_name."/ios/";
                // Where the files will be transferred to
                $dest = "s3://$bucketName/aws_assets/Screenshots/$scheme_name/ios/en-US";
                // Create a transfer object
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                // Perform the transfer synchronously
                $manager->transfer();
    }
    
    public function getiosscreenshoturlS3($scheme_name,$type,$bundle_id){
        $skeleton_apk_url = "";
        $screenshot_url_array = [];
        $screenshot_url_array['_5inch_1'] = ""; 
        $screenshot_url_array['_5inch_2'] = ""; 
        $screenshot_url_array['_5inch_3'] = ""; 
        $screenshot_url_array['_5inch_4'] = ""; 
        $screenshot_url_array['_6inch_1'] = ""; 
        $screenshot_url_array['_6inch_2'] = ""; 
        $screenshot_url_array['_6inch_3'] = ""; 
        $screenshot_url_array['_6inch_4'] = ""; 
        $screenshot_url_array['ipad_1'] = ""; 
        $screenshot_url_array['ipad_2'] = ""; 
        $screenshot_url_array['ipad_3'] = ""; 
        $screenshot_url_array['ipad_4'] = "";
        
        //android
        $androidscreenshot_url_array['phone1'] = ""; 
        $androidscreenshot_url_array['phone2'] = ""; 
        $androidscreenshot_url_array['phone3'] = ""; 
        $androidscreenshot_url_array['phone4'] = ""; 
        $androidscreenshot_url_array['tab_1'] = ""; 
        $androidscreenshot_url_array['tab_2'] = ""; 
        $androidscreenshot_url_array['tab_3'] = ""; 
        $androidscreenshot_url_array['tab_4'] = "";
        $bucketName = 'dev.app-files.mystudio.app';
	$IAM_KEY = 'AKIAIGUWYNJGAHDPJGIA';
	$IAM_SECRET = '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';
	// Connect to AWS
	try {
		$s3 = S3Client::factory(
			array(
				'credentials' => array(
					'key' => $IAM_KEY,
					'secret' => $IAM_SECRET
				),
				'version' => 'latest',
				'region'  => 'us-east-1'
			)
		);
	} catch (Exception $e) {
            log_info($e->getMessage());
                $error = array('status' => "Failed", "msg" => "Failed to get connection");
                $this->response($this->json($error), 200);
	}
        
        $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/SkeletonApk/$bundle_id/output-release.apk");
        if ($file_exist_flag) {
            $cmd = $s3->getCommand('GetObject', [
                'Bucket' => "$bucketName",
                'Key' => "aws_assets/SkeletonApk/$bundle_id/output-release.apk"
            ]);
            $request = $s3->createPresignedRequest($cmd, '+60 minutes');
            $skeleton_apk_url = (string) $request->getUri();
        }


        if($type == "I"){
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For 5.5 inch IPHONE only — Preview 1.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For 5.5 inch IPHONE only — Preview 1.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['_5inch_1'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For 5.5 inch IPHONE only — Preview 2.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For 5.5 inch IPHONE only — Preview 2.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['_5inch_2'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For 5.5 inch IPHONE only — Preview 3.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For 5.5 inch IPHONE only — Preview 3.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['_5inch_3'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For 5.5 inch IPHONE only — Preview 4.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For 5.5 inch IPHONE only — Preview 4.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['_5inch_4'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For 6.5 inch IPHONE only — Preview 1.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For 6.5 inch IPHONE only — Preview 1.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['_6inch_1'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For 6.5 inch IPHONE only — Preview 2.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For 6.5 inch IPHONE only — Preview 2.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['_6inch_2'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For 6.5 inch IPHONE only — Preview 3.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For 6.5 inch IPHONE only — Preview 3.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['_6inch_3'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For 6.5 inch IPHONE only — Preview 4.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For 6.5 inch IPHONE only — Preview 4.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['_6inch_4'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For IPAD only — Preview 1.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For IPAD only — Preview 1.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['ipad_1'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For IPAD only — Preview 2.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For IPAD only — Preview 2.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['ipad_2'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For IPAD only — Preview 3.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For IPAD only — Preview 3.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['ipad_3'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/ios/en-US/For IPAD only — Preview 4.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/ios/en-US/For IPAD only — Preview 4.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $screenshot_url_array['ipad_4'] = (string) $request->getUri();
                }
                
                $error = array('status' => "Success", "screenshot_url" => $screenshot_url_array);
                $this->response($this->json($error), 200);
    }else if($type == "A"){
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/android/en-US/images/phoneScreenshots/Preview 1.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/android/en-US/images/phoneScreenshots/Preview 1.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $androidscreenshot_url_array['phone1'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/android/en-US/images/phoneScreenshots/Preview 2.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/android/en-US/images/phoneScreenshots/Preview 2.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $androidscreenshot_url_array['phone2'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/android/en-US/images/phoneScreenshots/Preview 3.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/android/en-US/images/phoneScreenshots/Preview 3.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $androidscreenshot_url_array['phone3'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/android/en-US/images/phoneScreenshots/Preview 4.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/android/en-US/images/phoneScreenshots/Preview 4.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $androidscreenshot_url_array['phone4'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/android/en-US/images/sevenInchScreenshots/Preview 1.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/android/en-US/images/sevenInchScreenshots/Preview 1.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $androidscreenshot_url_array['tab_1'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/android/en-US/images/sevenInchScreenshots/Preview 2.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/android/en-US/images/sevenInchScreenshots/Preview 2.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $androidscreenshot_url_array['tab_2'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/android/en-US/images/sevenInchScreenshots/Preview 3.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/android/en-US/images/sevenInchScreenshots/Preview 3.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $androidscreenshot_url_array['tab_3'] = (string) $request->getUri();
                }
                
                $file_exist_flag = $s3->doesObjectExist($bucketName, "aws_assets/Screenshots/$scheme_name/android/en-US/images/sevenInchScreenshots/Preview 4.png");
                if($file_exist_flag){
                $cmd = $s3->getCommand('GetObject', [
                    'Bucket' => "$bucketName",
                    'Key' => "aws_assets/Screenshots/$scheme_name/android/en-US/images/sevenInchScreenshots/Preview 4.png"
                ]);
                $request = $s3->createPresignedRequest($cmd, '+60 minutes');
                $androidscreenshot_url_array['tab_4'] = (string) $request->getUri();
                }
                
                $error = array('status' => "Success", "screenshot_url" => $androidscreenshot_url_array,"skeleton_apk_url"=>$skeleton_apk_url);
                $this->response($this->json($error), 200);
    }
    }
    
    public function writescreenshot($filename,$file_content){
        $icon_content_encoded = [];
        $icon_content_encoded = explode("base64,", $file_content);
        $file_content_decoded = base64_decode($icon_content_encoded[1]);
        $ifp = fopen($filename, "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
    }
    
    public function getappstoreinfofromdb($company_id,$app_id,$type){
        if($type == "I"){
        $sql1 = sprintf("SELECT a.*,w.ios_metadata_manual_check,w.ios_screenshot_manual_check FROM apple_store_information a LEFT JOIN white_label_apps w USING(app_id) WHERE `app_id`='%s'", mysqli_real_escape_string($this->db, $app_id));
        }else if($type == "A"){
        $sql1 = sprintf("SELECT * FROM android_store_information WHERE `app_id`='%s'", mysqli_real_escape_string($this->db, $app_id));
        }
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $output = [];
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                while($rows = mysqli_fetch_assoc($result1)){
                    $output[] = $rows;
                }
            }
            if(empty($output[0])){
                $output[0] = "";
            }
            $error = array('status' => "Success", "msg" => $output[0]);
            $this->response($this->json($error), 200);
        }
    }
    
    
    public function buildtheappINDB($company_id,$app_id){
        $sql1 = sprintf("UPDATE white_label_apps SET application_status='T',bundleid_error_flag='N',credential_error_flag='N',teamid_error_flag='N',ios_icon_error='N',android_icon_error='N',ios_screenshot_error='N',android_screenshot_error='N',android_application_status='T' WHERE `app_id`='%s'", mysqli_real_escape_string($this->db, $app_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $error = array('status' => "Success");
            $this->response($this->json($error), 200);
        }
    }
    
    public function catchmanualmetaINDB($company_id,$app_id,$type,$flag){
        if($type == 'A'){
            $sql1 = sprintf("UPDATE white_label_apps SET android_metadata_manual_check='%s' WHERE `app_id`='%s'", mysqli_real_escape_string($this->db, $flag), mysqli_real_escape_string($this->db, $app_id));
        }else if($type == 'I'){
            $sql1 = sprintf("UPDATE white_label_apps SET ios_metadata_manual_check='%s' WHERE `app_id`='%s'", mysqli_real_escape_string($this->db, $flag), mysqli_real_escape_string($this->db, $app_id));
        }
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            
        $sql1 = sprintf("select android_metadata_manual_check,ios_metadata_manual_check from  white_label_apps WHERE `app_id`='%s'",mysqli_real_escape_string($this->db, $app_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $rows = mysqli_fetch_assoc($result1);
            $error = array('status' => "Success","msg"=>$rows);
            $this->response($this->json($error), 200);
        }
        }
    }
    
    public function catchmanualscreenINDB($company_id,$app_id,$type,$flag){
        if($type == 'A'){
            $sql1 = sprintf("UPDATE white_label_apps SET android_screenshot_manual_check='%s' WHERE `app_id`='%s'", mysqli_real_escape_string($this->db, $flag), mysqli_real_escape_string($this->db, $app_id));
        }else if($type == 'I'){
            $sql1 = sprintf("UPDATE white_label_apps SET ios_screenshot_manual_check='%s' WHERE `app_id`='%s'", mysqli_real_escape_string($this->db, $flag), mysqli_real_escape_string($this->db, $app_id));
        }
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            
        $sql1 = sprintf("select android_screenshot_manual_check,ios_screenshot_manual_check from  white_label_apps WHERE `app_id`='%s'",mysqli_real_escape_string($this->db, $app_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $rows = mysqli_fetch_assoc($result1);
            $error = array('status' => "Success","msg"=>$rows);
            $this->response($this->json($error), 200);
        }
        }
    }
    
    public function updateappstoreinformationINDB($company_id,$app_id,$localizable_info_name , $localizable_info_policy_url,$localizable_info_subtitle,
                       $general_info_apple_id ,$general_info_description ,$general_info_promotional_text ,$general_info_support_url ,
                       $general_info_marketing_url,$general_info_keywords,$copyright,
                       $trade_rep_contact_info_fname ,$trade_rep_contact_info_lname,$trade_rep_contact_info_address,$trade_rep_contact_info_suite,
                       $trade_rep_contact_info_city,$trade_rep_contact_info_state,
                        $trade_rep_contact_info_postal_code,$trade_rep_contact_info_phone,$trade_rep_contact_info_country ,$trade_rep_contact_info_email,
                       $app_review_info_username ,$app_review_info_password,$app_review_info_fname,$app_review_info_lname,$app_review_info_email,
                       $app_review_info_note,$app_review_info_phone,$company_code,$type) {
        
        $sql1 = sprintf("SELECT `app_id` From  `apple_store_information` WHERE `app_id` = '%s'", mysqli_real_escape_string($this->db, $app_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                if($type == "R"){
                $sql = sprintf("UPDATE `apple_store_information` SET updated_status='Y',localizable_info_name = '%s', localizable_info_policy_url = '%s', localizable_info_subtitle = '%s' 
                        ,general_info_description = '%s',general_info_promotional_text = '%s',general_info_support_url = '%s',general_info_marketing_url = '%s',general_info_keywords = '%s',copyright = '%s',
                        
                        trade_rep_contact_info_fname = '%s',trade_rep_contact_info_lname = '%s',trade_rep_contact_info_address = '%s',trade_rep_contact_info_suite = '%s',trade_rep_contact_info_city = '%s',
                        trade_rep_contact_info_state = '%s',trade_rep_contact_info_postal_code = '%s',trade_rep_contact_info_phone = '%s',trade_rep_contact_info_country = '%s',trade_rep_contact_info_email = '%s',
                        
                        app_review_info_username = '%s',app_review_info_password = '%s',app_review_info_fname = '%s',app_review_info_lname = '%s',app_review_info_email = '%s',app_review_info_note = '%s',app_review_info_phone = '%s'
                        
                        where `app_id` = '%s'", mysqli_real_escape_string($this->db, $localizable_info_name), mysqli_real_escape_string($this->db, $localizable_info_policy_url), mysqli_real_escape_string($this->db, $localizable_info_subtitle)
                        ,  mysqli_real_escape_string($this->db, $general_info_description)
                        , mysqli_real_escape_string($this->db, $general_info_promotional_text), mysqli_real_escape_string($this->db, $general_info_support_url)
                        , mysqli_real_escape_string($this->db, $general_info_marketing_url), mysqli_real_escape_string($this->db, $general_info_keywords), mysqli_real_escape_string($this->db, $copyright)
                        
                        , mysqli_real_escape_string($this->db, $trade_rep_contact_info_fname), mysqli_real_escape_string($this->db, $trade_rep_contact_info_lname)
                        , mysqli_real_escape_string($this->db, $trade_rep_contact_info_address), mysqli_real_escape_string($this->db, $trade_rep_contact_info_suite)
                        , mysqli_real_escape_string($this->db, $trade_rep_contact_info_city), mysqli_real_escape_string($this->db, $trade_rep_contact_info_state)
                        , mysqli_real_escape_string($this->db, $trade_rep_contact_info_postal_code), mysqli_real_escape_string($this->db, $trade_rep_contact_info_phone)
                        , mysqli_real_escape_string($this->db, $trade_rep_contact_info_country), mysqli_real_escape_string($this->db, $trade_rep_contact_info_email)
                        
                        , mysqli_real_escape_string($this->db, $app_review_info_username), mysqli_real_escape_string($this->db, $app_review_info_password)
                        , mysqli_real_escape_string($this->db, $app_review_info_fname), mysqli_real_escape_string($this->db, $app_review_info_lname)
                        , mysqli_real_escape_string($this->db, $app_review_info_email), mysqli_real_escape_string($this->db, $app_review_info_note), mysqli_real_escape_string($this->db, $app_review_info_phone)
                        
                        , mysqli_real_escape_string($this->db, $app_id));
                }else if($type == "I"){
                    $sql = sprintf("UPDATE `apple_store_information` SET updated_status='Y',general_info_apple_id='%s' 
                        where `app_id` = '%s'", mysqli_real_escape_string($this->db, $general_info_apple_id), mysqli_real_escape_string($this->db, $app_id));
                }
                $result = mysqli_query($this->db, $sql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    $this->response($this->json($error), 200);
                } else {
                        $this->getappstoreinfofromdb($company_id,$app_id,"I");
                }
            } else {
                if($type == 'R'){
                $sql = sprintf("INSERT INTO `apple_store_information` (`localizable_info_name`,`localizable_info_policy_url`, `localizable_info_subtitle`,general_info_description,general_info_promotional_text,general_info_support_url,general_info_marketing_url,general_info_keywords,copyright,
                        trade_rep_contact_info_fname,trade_rep_contact_info_lname,trade_rep_contact_info_address,trade_rep_contact_info_suite,trade_rep_contact_info_city,
                        trade_rep_contact_info_state,trade_rep_contact_info_postal_code,trade_rep_contact_info_phone,trade_rep_contact_info_country,trade_rep_contact_info_email,
                        app_review_info_username,app_review_info_password,app_review_info_fname,app_review_info_lname,app_review_info_email,app_review_info_note,app_review_info_phone,app_id
                        ) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') "
                        , mysqli_real_escape_string($this->db, $localizable_info_name), mysqli_real_escape_string($this->db, $localizable_info_policy_url), mysqli_real_escape_string($this->db, $localizable_info_subtitle)
                        , mysqli_real_escape_string($this->db, $general_info_description)
                        , mysqli_real_escape_string($this->db, $general_info_promotional_text), mysqli_real_escape_string($this->db, $general_info_support_url)
                        , mysqli_real_escape_string($this->db, $general_info_marketing_url), mysqli_real_escape_string($this->db, $general_info_keywords), mysqli_real_escape_string($this->db, $copyright)
                        
                        , mysqli_real_escape_string($this->db, $trade_rep_contact_info_fname), mysqli_real_escape_string($this->db, $trade_rep_contact_info_lname)
                        , mysqli_real_escape_string($this->db, $trade_rep_contact_info_address), mysqli_real_escape_string($this->db, $trade_rep_contact_info_suite)
                        , mysqli_real_escape_string($this->db, $trade_rep_contact_info_city), mysqli_real_escape_string($this->db, $trade_rep_contact_info_state)
                        , mysqli_real_escape_string($this->db, $trade_rep_contact_info_postal_code), mysqli_real_escape_string($this->db, $trade_rep_contact_info_phone)
                        , mysqli_real_escape_string($this->db, $trade_rep_contact_info_country), mysqli_real_escape_string($this->db, $trade_rep_contact_info_email)
                        
                        , mysqli_real_escape_string($this->db, $app_review_info_username), mysqli_real_escape_string($this->db, $app_review_info_password)
                        , mysqli_real_escape_string($this->db, $app_review_info_fname), mysqli_real_escape_string($this->db, $app_review_info_lname)
                        , mysqli_real_escape_string($this->db, $app_review_info_email), mysqli_real_escape_string($this->db, $app_review_info_note), mysqli_real_escape_string($this->db, $app_review_info_phone)
                        
                        , mysqli_real_escape_string($this->db, $app_id));
                }else if($type == "I"){
                    $sql = sprintf("INSERT INTO `apple_store_information` (`general_info_apple_id`) VALUES ('%s') "
                        , mysqli_real_escape_string($this->db, $general_info_apple_id)
                        , mysqli_real_escape_string($this->db, $app_id));
                }
                $result = mysqli_query($this->db, $sql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    $this->response($this->json($error), 200);
                } else {
                    if (mysqli_affected_rows($this->db) > 0) {
                        $this->getappstoreinfofromdb($company_id,$app_id,"I");
                    }
                }
            }
        }
    }
    
    public function sendappinfotomystudio($app_id, $company_id) {
        $app_name = $ios_developerid = $ios_teamid = $user_emailid = $subject = $message = "";
        $query = sprintf("SELECT `ios_app_name`, `ios_developerid`, `ios_teamid` FROM `white_label_apps` WHERE `app_id` = '%s' AND `company_id` = '%s' ", mysqli_real_escape_string($this->db, $app_id),  mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $app_name = $row['ios_app_name'];
                $ios_developerid = $row['ios_developerid'];
                $ios_teamid = $row['ios_teamid'];
//                $user_emailid = 'james@technogemsinc.com';
                $user_emailid = 'support@mystudio.academy';
                $subject = "App transfer details";
                $message .= "<h1 style='font-family: avenir;'>Apple Developer account details<h1/>";
                $message .= '<label style="font-size: 16px;font-family: avenir;"><b>App Name</b>         : ' . $app_name . '</label><br>';
                $message .= '<label style="font-size: 16px;font-family: avenir;"><b>IOS Developer Id</b> : ' . $ios_developerid . '</label><br>';
                $message .= '<label style="font-size: 16px;font-family: avenir;"><b>IOS Team ID</b>      : ' . $ios_teamid . '</label><br>';
                $sendEmail_status = $this->sendEmail($user_emailid, $subject, $message, '', '', '', '', '', $company_id, '', '');
                if ($sendEmail_status['status'] == "true") {
                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                    log_info($this->json($error_log));
                    $error = array('status' => "Success", "msg" => "Developer account details updated successfully.");
                    $this->response($this->json($error), 200);
                } else {
                    $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Developer account details update failed.");
                    $this->response($this->json($error), 200);
                }
            }
        }
    }
    
    public function encryptPassword($pass){
        if (!function_exists('getallheaders')){
            if(!function_exists ('apache_request_headers')){
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            }else{
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
            }
        }else{
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
        }
        $ip = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR');   
        
        $time = time();
        $password = $this->wp_pswd;
        $method = 'aes-256-cbc';
        // Must be exact 32 chars (256 bit)
        $password = substr(hash('sha256', $password, true), 0, 32);
        // IV must be exact 16 chars (128 bit)
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        $encrypted_password = base64_encode(openssl_encrypt($pass, $method, $password, OPENSSL_RAW_DATA, $iv));
        return $encrypted_password;
    }
    
    public function decryptPassword($encrypted_pass){
        if (!function_exists('getallheaders')){
            if(!function_exists ('apache_request_headers')){
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            }else{
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
            }
        }else{
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
        }
        $ip = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR');   
        
        $time = time();
        $password = $this->wp_pswd;
        $method = 'aes-256-cbc';
        // Must be exact 32 chars (256 bit)
        $password = substr(hash('sha256', $password, true), 0, 32);
        // IV must be exact 16 chars (128 bit)
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        $decrypted_pass = openssl_decrypt(base64_decode($encrypted_pass), $method, $password, OPENSSL_RAW_DATA, $iv);
        return $decrypted_pass;
    }
    
    public function sendEmail($to, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file,$cc_email_list,$company_id,$type,$footer_detail) {//
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }            
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            if (!empty($company_id) && $type != 'subscribe') {
            
                $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                $curr_loc = implode('/', explode('/', $curr_loc, -4));
//                $tomail = $to;
                if (!empty(trim($cc_email_list))) {
                    $tomail = base64_encode($to . "," . "$cc_email_list");
                } else {
                    $tomail = base64_encode($to);
                }
                $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/EmailSubscription.php?mail=$tomail?comp_id=$company_id?type=U";
                $message = "
            <html>
            <body>
                <p>$message</p><br><br>
                    <footer style='font-size: 13px;color: #746f6f;'>
                    <center>
                    <p>$footer_detail
                    </p>
                    <a href='$activation_link' target='_blank' style='color: #746f6f; !important;text-decoration: none !important;'><u style='color: #746f6f;'>Unsubscribe $to</u></a>
                    </center>
                    </footer>
            </body>
            </html>
            ";
            }
            $mail->Body = $message;
            if(!empty($file_name)){
                $mail->AddStringAttachment(base64_decode($attached_file), $file_name, 'base64');
            }
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    if (filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)) {
                        $verify_email_for_bounce = $this->verifyBouncedEmailForCC($cc_addresses[$init], $company_id);
                       if($verify_email_for_bounce==1){
                            $mail->AddCC($cc_addresses[$init]);
                        }else if($verify_email_for_bounce==0 && $type == 'subscribe'){
                            $mail->AddCC($cc_addresses[$init]);
                        }
                    }
                }
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    public function getadminWLdetailsINDB($sorting,$start,$length_table,$draw_table,$search,$filter,$android_filter) {
        if($filter == 'A'){
            $filter_string = "";
        }else{
           $filter_string  = "where build_status='$filter'";
        }
        if($android_filter == 'A'){
            $android_filter_string = "";
        }else{
            if($filter == 'A'){
                $android_filter_string  = "where android_application_status IN ($android_filter)";
            }else{
                $android_filter_string  = "AND android_application_status IN ($android_filter)";
            }
        }
         $output = [];
         $out=[];
        $sort = $sorting[0]['column'];
        $sort_type = $sorting[0]['dir'];
        $search_value = mysqli_real_escape_string($this->db,$search['value']);
         if (!empty($search_value)) {
             if(empty($filter_string) && empty($android_filter_string)){
                 $s_text = "where (user_email like  '%$search_value%'  or user_email like  '%$search_value%'  or company_name like  '%$search_value%'  or w.ios_bundle_id like  '%$search_value%' or  w.android_bundle_id like '%$search_value%'
                    or `ios_app_name` like  '%$search_value%'  
                     or  new_build_version  like  '%$search_value%' or  android_version_number  like  '%$search_value%')";
             }else{
                 $s_text = "and (user_email like  '%$search_value%'  or user_email like  '%$search_value%'  or company_name like  '%$search_value%'  or w.ios_bundle_id like  '%$search_value%' or  w.android_bundle_id like '%$search_value%'
                    or `ios_app_name` like  '%$search_value%'  
                     or  new_build_version  like  '%$search_value%' or  android_version_number  like  '%$search_value%')";
             }
        } else {
            $s_text = "";
        }

        $column = array(0 => "ios_app_name",1 => "company_name",2=>"company_code",3=>"user_email", 4 => "ios_bundle_id", 5 => "android_bundle_id",  6 => "new_build_version", 8 => "android_version_number");
        
        $sql = sprintf("SELECT w.ios_log_file_url,w.android_log_file_url,u.user_email,c.company_code,c.company_name,w.company_id,w.ios_bundle_id,w.android_bundle_id,ios_app_name,new_build_version,android_version_number,if(android_application_status = 'C','Published',(IF(android_application_status = 'F','Failed',(IF(android_application_status='N','Pending Submission',('Processing')))))) android_application_status,build_status,app_store_error_msg from white_label_apps w left join company c using(company_id) Left JOIN user u USING(company_id) $filter_string $android_filter_string %s
                    ORDER BY $column[$sort] $sort_type LIMIT $start, $length_table ",$s_text);

        $result = mysqli_query($this->db, $sql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $row['ios_url'] = "";
                    $row['android_url'] = "";
                    if(!empty($row['ios_log_file_url'])){
                        $ios_key = explode('s3://whitelabellog/',$row['ios_log_file_url']);
                        $row['ios_url'] = $this->getlogurlWL($ios_key[1]);
                    }
                    if(!empty($row['android_log_file_url'])){
                        $android_key = explode('s3://whitelabellog/',$row['android_log_file_url']);
                        $row['android_url'] = $this->getlogurlWL($android_key[1]);
                    }
                    $output[] = $row;
                }
                $out['draw'] = $draw_table;
                $out['data'] = $output;
                $this->response($this->json($out), 200);
            }else{
                $out['draw'] = $draw_table;
                $out['data'] = $output;
                $this->response($this->json($out), 200); // If no records "No Content" status
            }
        }
    }
    
    public function getlogurlWL($key){
        $bucketName = 'whitelabellog';
        $IAM_KEY = 'AKIAIGUWYNJGAHDPJGIA';
        $IAM_SECRET = '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';
        $file_exist_flag = false;
        $url = "";
        // Connect to AWS
        try {
            $s3 = S3Client::factory(
                            array(
                                'credentials' => array(
                                    'key' => $IAM_KEY,
                                    'secret' => $IAM_SECRET
                                ),
                                'version' => 'latest',
                                'region' => 'us-east-1'
                            )
            );
        } catch (Exception $e) {
            log_info($e->getMessage());
            $error = array('status' => "Failed", "msg" => "Failed to get connection");
            $this->response($this->json($error), 200);
        }
        $file_exist_flag = $s3->doesObjectExist($bucketName, "$key");
        if ($file_exist_flag) {
            $cmd = $s3->getCommand('GetObject', [
                'Bucket' => "$bucketName",
                'Key' => "$key"
            ]);
            $request = $s3->createPresignedRequest($cmd, '+60 minutes');
            $url = (string) $request->getUri();
        }
        return $url;
    }
    
}