<?php

class PaySimple {

    private $api_username = '';
    private $api_secret = '';
    private $url = '';
    private $file_name = '';
    
    public function __construct() {
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'dev.mystudio.academy') !== false || strpos($host, 'dev2.mystudio.academy') !== false){
            $this->file_name = "../../../Globals/paysimple_dev.props";
            $this->url = "https://sandbox-api.paysimple.com/v4/";
        }elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->file_name = "../../../Globals/paysimple_prod.props";
            $this->url = "https://api.paysimple.com/v4/";
        }else{
            $this->file_name = "../../../Globals/paysimple_dev.props";
            $this->url = "https://sandbox-api.paysimple.com/v4/";
        }
        
        $this->getUserandSecret();
    }
    
    private function getUserandSecret(){
        $file = explode( PHP_EOL, file_get_contents($this->file_name));
        $i=0;
        foreach( $file as $line ) {
            if($i<=1 && $line!=""){
                $api = explode("=", $line);
                if($i==0){
                    $this->api_username = $api[1];
                    $i++;
                }elseif($i==1){
                    $this->api_secret = $api[1];
                    $i++;
                }
            }
        }
    }
    
    public function accessPaySimpleApi($method,$request_type,$params){
        $timestamp = gmdate("c");
        $hmac = hash_hmac("sha256", $timestamp, $this->api_secret, true); //note the raw output parameter
        $hmac = base64_encode($hmac);
        $auth[] = "Accept: application/json";
        $auth[] = "Content-Type: application/json";
        $auth[] = "Authorization: PSSERVER AccessId = $this->api_username; Timestamp = $timestamp; Signature = $hmac";
        
        $url = $this->url.$method;
        if(strtoupper($request_type)=='GET'){
            $body='{}';
            $post_option = false;
            $url = $url.$params;
        }else{
            $body = $params;
            if(strtoupper($request_type)=='POST'){
                $post_option = true;
            }else{
                $post_option = false;
            }
        }
        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "$url");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "$request_type");
        curl_setopt($ch, CURLOPT_POST, $post_option);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $auth);

        $result = curl_exec($ch);
        $resultArr = json_decode($result,true);
        $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        if(isset($resultArr)&&!empty($resultArr)&&!empty($resultArr['Meta']['HttpStatusCode'])&&$resultArr['Meta']['HttpStatusCode']<300){
            $succ_log = array("Curl : " => "Success($returnCode)", "Method: " => $method, "Type: " => $request_type, "Url: " => $this->url, "Params: " => $params, "Result: " => $result);
            $this->log_info($this->json($succ_log));
            $res['status'] = "Success";
            $res['msg'] = $resultArr;
            $res['curl_status'] = $returnCode;
        }else{
            $error_log = array("Curl error: " => curl_error($ch)."($returnCode)", "Method: " => $method, "Type: " => $request_type, "Url: " => $this->url, "Params: " => $params, "Result : " => $result);
            $this->log_info($this->json($error_log));
            $res['status'] = "Failed";
            $res['msg'] = $resultArr;
            $res['curl_status'] = $returnCode;
        }
        curl_close($ch);
        return $res;
    }
    
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    private function log_info($msg) {        
        $today = gmdate("d.m.Y");
        $filename = "../../../Log/Paysimple/$today.txt";
        $fd = fopen($filename, "a");
        $str = "[" . gmdate("d/m/Y H:i:s") . "] " . $msg;
        fwrite($fd, $str . PHP_EOL . PHP_EOL);
        fclose($fd);
    }
    
}