<?php

 header('Access-Control-Allow-Origin:'.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');

require_once '../../../Globals/cont_log.php';
require_once 'Salesforce.php';
require_once 'PaySimple.php';
require_once 'WePay.php';
require_once('../../PHPMailer_5.2.1/class.phpmailer.php');
require_once('../../PHPMailer_5.2.1/class.smtp.php');
require_once 'tfpdf.php';
//require_once '../../../aws/aws-autoloader.php';
require_once __DIR__.'/../../../aws/aws.phar';

use Aws\Sns\SnsClient;
use Aws\Sqs\SqsClient;
class EventModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $sf;
    private $ps;
    private $list_id = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_redirect_uri = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $server_url;

    public function __construct() {
        require_once '../../../Globals/config.php';
        require_once '../../../Globals/stripe_props.php';
        $this->inputs();
        $this->sf = new Salesforce();
        $this->ps = new PaySimple();
        $this->wp = new wepay();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->list_id = $this->sf->list_id;
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = "../../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'dev.mystudio.academy') !== false) {
            $this->server_url = "http://dev.mystudio.academy";
        }elseif(strpos($host, 'dev2.mystudio.academy') !== false) {
            $this->server_url = "http://dev2.mystudio.academy";
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME'];
        }else{
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithPaysimple($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithSalesforce($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
        header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    protected function clean($string) {
        $str = preg_replace('/\s+/', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    }

    //Add New Event
    public function insertEvent($company_id,$parent_id,$event_type,$event_status,$event_start_datetime,$event_end_datetime,$event_title,$event_category_subtitle,$event_desc,$event_video_detail_url,$event_cost,$event_compare_price,$event_banner_img_content,$event_banner_img_type,$event_sort_order,$event_capacity,$capacity_text,$list_type,$event_show_status) {

        $event_id = $return_event_id = '';
        $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->upload_folder_name/Default/default.png";
        
        if(empty(trim($event_compare_price))){      
            $event_compare_price = 'null';   
        }
        if($event_capacity==''){
            $event_capacity='NULL';
        }
        if($event_type=='M'){
            $sql1 = sprintf("INSERT INTO `event` (`company_id`, `event_type`, `event_status`, `event_title`, `event_desc`, `event_video_detail_url`, `event_category_subtitle`, `event_banner_img_url`, `event_sort_order`, `event_show_status`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', NextVal('event_sort_order_seq'), '%s')", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_type), mysqli_real_escape_string($this->db,$event_status), mysqli_real_escape_string($this->db,$event_title), mysqli_real_escape_string($this->db,$event_desc), mysqli_real_escape_string($this->db,$event_video_detail_url), mysqli_real_escape_string($this->db,$event_category_subtitle), mysqli_real_escape_string($this->db,$event_banner_img_url),mysqli_real_escape_string($this->db,$event_show_status));
        }else{
            if(empty(trim($parent_id))){
                $parent_id = 'null';
            }
            $sql1 = sprintf("INSERT INTO `event` (`company_id`, `parent_id`, `event_type`, `event_status`, `event_begin_dt`, `event_end_dt`, `event_title`, `event_desc`, `event_video_detail_url`, `event_cost`, `event_compare_price`, `event_sort_order`, `event_banner_img_url`, `event_capacity`, `capacity_text`,`event_show_status`) VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, NextVal('event_sort_order_seq'), '%s', %s, '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$parent_id), mysqli_real_escape_string($this->db,$event_type), mysqli_real_escape_string($this->db,$event_status), mysqli_real_escape_string($this->db,$event_start_datetime), mysqli_real_escape_string($this->db,$event_end_datetime), mysqli_real_escape_string($this->db,$event_title), mysqli_real_escape_string($this->db,$event_desc), mysqli_real_escape_string($this->db,$event_video_detail_url), mysqli_real_escape_string($this->db,$event_cost), mysqli_real_escape_string($this->db,$event_compare_price), mysqli_real_escape_string($this->db,$event_banner_img_url), mysqli_real_escape_string($this->db,$event_capacity), mysqli_real_escape_string($this->db,$capacity_text), mysqli_real_escape_string($this->db,$event_show_status));
        }
        $result1 = mysqli_query($this->db, $sql1);

        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $event_id = mysqli_insert_id($this->db);
            if(!empty(trim($parent_id)) && $parent_id!='null'){
                $return_event_id = $parent_id;
            }else{
                $return_event_id = $event_id;
            }
//            if ($event_type == 'M' || $event_type == 'S') {
                $sql2 = sprintf("SELECT  `company_code` FROM  `company` WHERE  `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $output_value = mysqli_fetch_assoc($result2);
                    $company_code = $output_value['company_code'];
                }
                $company_code_new = $this->clean($company_code);
                $event_title_new = $this->clean($event_title);
                $event_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/e/?=$company_code_new/$company_id/$event_id";

                $sql3 = sprintf("UPDATE `event` SET `event_url` = '$event_url' WHERE `event_id` = '%s' ", mysqli_real_escape_string($this->db, $event_id));
                $result3 = mysqli_query($this->db, $sql3);
                if (!$result3) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
//            }
            if ($event_banner_img_content != '') {
                $event_banner_img_url = "";
                $cfolder_name = "Company_$company_id";
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name";
                chdir($dir_name);
                if (!file_exists($cfolder_name)) {
                    $oldmask = umask(0);
                    mkdir($cfolder_name, 0777, true);
                    umask($oldmask);
                }
                chdir($cfolder_name);
                $folder_name = "Events";
                if (!file_exists($folder_name)) {
                    $oldmask = umask(0);
                    mkdir($folder_name, 0777, true);
                    umask($oldmask);
                }
                chdir($folder_name);
                $image_content = base64_decode($event_banner_img_content);
                $file_name = $company_id . "-" . $event_id;
                $file = "$file_name.$event_banner_img_type";
                $img = $file; // logo - your file name
                $ifp = fopen($img, "wb");
                fwrite($ifp, $image_content);
                fclose($ifp);
                chdir($old); // Restore the old working directory

                $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->upload_folder_name/$cfolder_name/$folder_name/$file";

                $update_event = sprintf("UPDATE `event` SET `event_banner_img_url`='%s' WHERE `event_id`='%s'", mysqli_real_escape_string($this->db,$event_banner_img_url), $event_id);
                $result = mysqli_query($this->db, $update_event);
                
                //Print Company details after login success
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
//            $this->getEventsDetailByCompany($company_id);
            $this->getEventsDetailByEventId($company_id, $return_event_id, $event_type, $list_type, 0);            
        }
    }

    //Get Event details by added event
    public function getEventsDetailByEventId($company_id, $event_id, $type, $list_type, $call_back) {//call_back, 0 -> returned & exit, 1 -> return response
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = "SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        
        if($type=='M' || $type=='C'){
            $parent_check = "or `parent_id`=$event_id";
        }else{
            $parent_check = '';
        }
        $sql = sprintf("SELECT  `event_id` ,  `company_id` ,  `parent_id` ,  `event_type` ,  `event_status` , `event_show_status` ,  `event_begin_dt` ,  
            `event_end_dt` , `event_title` ,CONCAT(`event_url`,'///',UNIX_TIMESTAMP()) event_url,  `event_category_subtitle` ,  `event_desc` ,  `event_more_detail_url`,  `event_video_detail_url` ,  `event_cost` , 
            `event_compare_price` ,  `event_capacity`, `capacity_text`, `processing_fees`, `event_onetime_payment_flag`, 
            `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, 
            `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `registrations_count`, `net_sales`, 
            `event_sort_order` ,  `event_banner_img_url`, `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, 
            `event_registration_column_2`, `event_reg_col_2_mandatory_flag`,`event_registration_column_3`, `event_reg_col_3_mandatory_flag`,
            `event_registration_column_4`, `event_reg_col_4_mandatory_flag`,`event_registration_column_5`, `event_reg_col_5_mandatory_flag`,
            `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, `event_reg_col_7_mandatory_flag`,
            `event_registration_column_8`, `event_reg_col_8_mandatory_flag`,`event_registration_column_9`, `event_reg_col_9_mandatory_flag`,
            `event_registration_column_10`, `event_reg_col_10_mandatory_flag`,IF( `event_status` = 'S', 'draft', 
            IF( `event_type`='M', 'past', IF( DATE(`event_begin_dt`) <= ($currdate_add), 
            IF(DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00', 'live', 'past' ) , 'live' ))) list_type
            FROM  `event` 
            WHERE  `company_id` = '%s' AND (`event_id` =  '%s' %s) AND  `event_status` !=  'D'
            ORDER BY  `parent_id` ASC, `event_begin_dt` ASC", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$parent_check));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if($call_back==1){
                return $error;
            }else{
                $this->response($this->json($error), 200);
            }
        } else {
            $child = [];
            $disc = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                if($type=='M' || $type=='C'){
                    while($row = mysqli_fetch_assoc($result)) {
                        $disc = $this->getDiscountDetails($company_id, $row['event_id']);
                        $row['discount'] = $disc['discount'];
                        $reg_field_name_array = [];
                        $temp_reg_field = "event_registration_column_";
                        for($z=1;$z<=10;$z++){ //collect registration fields as array
                            $y=$z-1;
                            $reg_field = $temp_reg_field."$z";      //for filed names
                            $reg_field_mandatory = "event_reg_col_".$z."_mandatory_flag";
                            if($row[$reg_field]!=''){
                                $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                            }
                        }
                        $row['reg_columns'] = $reg_field_name_array;
                        if($row['event_type']=='M'){
                            $row['child_events'] = [];
                            $response = $row;
                        }else{
                            $child[] = $row;
                        }
                    }
                }else{
                    $row = mysqli_fetch_assoc($result);
                    $disc = $this->getDiscountDetails($company_id, $event_id);
                    $row['discount'] = $disc['discount'];
                    $reg_field_name_array = [];
                    $temp_reg_field = "event_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "event_reg_col_".$z."_mandatory_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row['reg_columns'] = $reg_field_name_array;
                    $response = $row;
                }
                if(count($child)>0){
                    $check_child_type = 0;                    
                    for($i=0;$i<count($child);$i++){
                        if($list_type == '' || $list_type == $child[$i]['list_type']){
                            if($check_child_type==0){
                                $response['list_type'] = $child[$i]['list_type'];
                                $check_child_type++;
                            }
                            $response['child_events'][] = $child[$i];
                        }
                    }
                }else{
                    if($type=='M' && $response['event_status']=='P'){
                        $response['list_type'] = 'live';
                    }
                    $response['child_events']=[];
                }
                $out = array('status' => "Success", 'msg' => $response);
                // If success everythig is good send header as "OK" and user details
                if($call_back==1){
                    return $out;
                }else{
                    $this->response($this->json($out), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Event details doesn't exist.");
                if($call_back==1){
                    return $error;
                }else{
                    $this->response($this->json($error), 200);
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    //Get Discount details by Event ID
    public function getDiscountDetails($company_id, $event_id){
        $sql = sprintf("SELECT `event_discount_id` , `discount_type` , `discount_code` , `discount_amount` FROM `event_discount`
                WHERE `company_id` = '%s' AND `event_id`='%s' AND `is_expired`!='Y'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id));
        $result = mysqli_query($this->db, $sql);
        $output = $response = [];
        if($result){
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $output[] = $row;
                }
                $response['status'] = "Success";
            }else{
                $response['status'] = "Failed";
            }
        }else{
            $response['status'] = "Failed";
        }
        $response['discount'] = $output;
        return $response;
    }
    
    //Get Social details by Company ID
    public function copyEvent($company_id,$event_id,$copy_event_type,$event_template_flag, $list_type,$for_initial) { //For_initial_loading Y-For initial
        $parent_id = $company_code = $event_compare_price=$event_capacity=$last_inserted_event_id1="";
        
        if($event_template_flag == "Y"){
            $comp_id = "6";
        } else {
            $comp_id = $company_id;
        }
        $sql_company_code = sprintf("SELECT  `company_code` FROM  `company` WHERE  `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result_company_code = mysqli_query($this->db, $sql_company_code);
        if (!$result_company_code) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_company_code");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $output_value = mysqli_fetch_assoc($result_company_code);
            $company_code = $output_value['company_code'];
        }
        
        $sql = sprintf("SELECT `parent_id`, `event_type`, `event_status`, `event_title`, 
            `event_category_subtitle`, `event_desc`, `event_more_detail_url`,  `event_video_detail_url`, `event_cost`, 
            `event_compare_price`, `event_capacity`, `capacity_text`, `processing_fees`, `event_onetime_payment_flag`, 
            `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, 
            `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `event_banner_img_content`, 
            `event_banner_img_url`, `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, `event_registration_column_2`, 
            `event_reg_col_2_mandatory_flag`, `event_registration_column_3`, `event_reg_col_3_mandatory_flag`, 
            `event_registration_column_4`, `event_reg_col_4_mandatory_flag`, `event_registration_column_5`, 
            `event_reg_col_5_mandatory_flag`, `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, 
            `event_registration_column_7`, `event_reg_col_7_mandatory_flag`, `event_registration_column_8`, 
            `event_reg_col_8_mandatory_flag`, `event_registration_column_9`, `event_reg_col_9_mandatory_flag`, 
            `event_registration_column_10`, `event_reg_col_10_mandatory_flag` FROM `event` WHERE `company_id`='%s' AND 
            (`event_id`='%s' OR `parent_id`='%s') AND `event_status`!='D' order by event_type desc", $comp_id, $event_id, $event_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                if (is_null($output[0]['event_capacity'])) {
                    $event_capacity = 'NULL';
                } else {
                    $event_capacity = $output[0]['event_capacity'];
                }
                if(empty(trim($output[0]['event_compare_price']))||is_null($output[0]['event_compare_price'])){
                    $event_compare_price = 'NULL';
                }else{
                    $event_compare_price = $output[0]['event_compare_price'];
                }
                if($for_initial == 'Y'){
                    $e_status = 'P';
                    $copy_string = '';
                }else{
                    $e_status = 'S';
                    $copy_string = ' copy';
                }
                            
                if (empty(trim($output[0]['parent_id']))) {
                    $parent_id = 'null';
                    $sql1 = sprintf("INSERT INTO `event` (`company_id`, `parent_id`, `event_type`, `event_status`, `event_title`, 
                        `event_category_subtitle`, `event_desc`, `event_more_detail_url`,  `event_video_detail_url`, `event_cost`, 
                        `event_compare_price`, `event_capacity`, `capacity_text`, `processing_fees`, `event_onetime_payment_flag`, 
                        `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, 
                        `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, 
                        `event_banner_img_content`, `event_registration_column_1`, 
                        `event_reg_col_1_mandatory_flag`, `event_registration_column_2`, `event_reg_col_2_mandatory_flag`, 
                        `event_registration_column_3`, `event_reg_col_3_mandatory_flag`, `event_registration_column_4`, 
                        `event_reg_col_4_mandatory_flag`, `event_registration_column_5`, `event_reg_col_5_mandatory_flag`, 
                        `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, 
                        `event_reg_col_7_mandatory_flag`, `event_registration_column_8`, `event_reg_col_8_mandatory_flag`, 
                        `event_registration_column_9`, `event_reg_col_9_mandatory_flag`, `event_registration_column_10`, 
                        `event_reg_col_10_mandatory_flag`, `event_sort_order`) VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                         %s,%s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                        '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', 
                        NextVal('event_sort_order_seq'))", mysqli_real_escape_string($this->db, $company_id), 
                            mysqli_real_escape_string($this->db, $parent_id), 
                            mysqli_real_escape_string($this->db, $output[0]['event_type']), 
                            mysqli_real_escape_string($this->db, $e_status), 
                            mysqli_real_escape_string($this->db, $output[0]['event_title'] .$copy_string), 
                            mysqli_real_escape_string($this->db, $output[0]['event_category_subtitle'] .$copy_string), 
                            mysqli_real_escape_string($this->db, $output[0]['event_desc']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_more_detail_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_video_detail_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_cost']), 
                            mysqli_real_escape_string($this->db, $event_compare_price), 
                            mysqli_real_escape_string($this->db, $event_capacity), 
                            mysqli_real_escape_string($this->db, $output[0]['capacity_text']), 
                            mysqli_real_escape_string($this->db, $output[0]['processing_fees']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_onetime_payment_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_recurring_payment_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['total_order_text']), 
                            mysqli_real_escape_string($this->db, $output[0]['total_quantity_text']), 
                            mysqli_real_escape_string($this->db, $output[0]['deposit_amount']), 
                            mysqli_real_escape_string($this->db, $output[0]['number_of_payments']), 
                            mysqli_real_escape_string($this->db, $output[0]['payment_startdate_type']), 
                            mysqli_real_escape_string($this->db, $output[0]['payment_startdate']), 
                            mysqli_real_escape_string($this->db, $output[0]['payment_frequency']), 
                            mysqli_real_escape_string($this->db, $output[0]['waiver_policies']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_banner_img_content']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_1']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_1_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_2']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_2_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_3']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_3_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_4']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_4_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_5']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_5_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_6']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_6_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_7']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_7_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_8']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_8_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_9']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_9_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_10']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_10_mandatory_flag']));
                } else{
                    $parent_id = $output[0]['parent_id'];
                    $sql1 = sprintf("INSERT INTO `event` (`company_id`, `parent_id`, `event_type`, `event_status`, `event_title`, 
                        `event_category_subtitle`, `event_desc`, `event_more_detail_url`,  `event_video_detail_url`, `event_cost`, 
                        `event_compare_price`, `event_capacity`, `capacity_text`, `processing_fees`, `event_onetime_payment_flag`, 
                        `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, 
                        `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, 
                        `event_banner_img_content`, `event_registration_column_1`, 
                        `event_reg_col_1_mandatory_flag`, `event_registration_column_2`, `event_reg_col_2_mandatory_flag`, 
                        `event_registration_column_3`, `event_reg_col_3_mandatory_flag`, `event_registration_column_4`, 
                        `event_reg_col_4_mandatory_flag`, `event_registration_column_5`, `event_reg_col_5_mandatory_flag`, 
                        `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, 
                        `event_reg_col_7_mandatory_flag`, `event_registration_column_8`, `event_reg_col_8_mandatory_flag`, 
                        `event_registration_column_9`, `event_reg_col_9_mandatory_flag`, `event_registration_column_10`, 
                        `event_reg_col_10_mandatory_flag`, `event_sort_order`) VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                        %s,%s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                        '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', 
                        NextVal('event_sort_order_seq'))", mysqli_real_escape_string($this->db, $company_id), 
                            mysqli_real_escape_string($this->db, $parent_id), 
                            mysqli_real_escape_string($this->db, $output[0]['event_type']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_status']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_title'] .$copy_string), 
                            mysqli_real_escape_string($this->db, $output[0]['event_category_subtitle'] .$copy_string), 
                            mysqli_real_escape_string($this->db, $output[0]['event_desc']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_more_detail_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_video_detail_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_cost']), 
                            mysqli_real_escape_string($this->db, $event_compare_price), 
                            mysqli_real_escape_string($this->db, $event_capacity), 
                            mysqli_real_escape_string($this->db, $output[0]['capacity_text']), 
                            mysqli_real_escape_string($this->db, $output[0]['processing_fees']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_onetime_payment_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_recurring_payment_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['total_order_text']), 
                            mysqli_real_escape_string($this->db, $output[0]['total_quantity_text']), 
                            mysqli_real_escape_string($this->db, $output[0]['deposit_amount']), 
                            mysqli_real_escape_string($this->db, $output[0]['number_of_payments']), 
                            mysqli_real_escape_string($this->db, $output[0]['payment_startdate_type']), 
                            mysqli_real_escape_string($this->db, $output[0]['payment_startdate']), 
                            mysqli_real_escape_string($this->db, $output[0]['payment_frequency']), 
                            mysqli_real_escape_string($this->db, $output[0]['waiver_policies']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_banner_img_content']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_1']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_1_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_2']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_2_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_3']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_3_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_4']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_4_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_5']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_5_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_6']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_6_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_7']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_7_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_8']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_8_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_9']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_9_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_10']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_10_mandatory_flag'])); 
                }

//                $sql1 = sprintf("INSERT INTO `event` (`company_id`, `parent_id`, `event_type`, `event_status`, `event_begin_dt`, `event_end_dt`, `event_title`, `event_desc`, `event_more_detail_url`,  `event_video_detail_url`, `event_cost`, `event_compare_price`, `event_sort_order`, `event_banner_img_url`) VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', NextVal('event_sort_order_seq'), '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $parent_id), mysqli_real_escape_string($this->db, $output[0]['event_type']), mysqli_real_escape_string($this->db, $output[0]['event_status']), mysqli_real_escape_string($this->db, $output[0]['event_begin_dt']), mysqli_real_escape_string($this->db, $output[0]['event_end_dt']), mysqli_real_escape_string($this->db, $output[0]['event_title'] . " copy"), mysqli_real_escape_string($this->db, $output[0]['event_desc']), mysqli_real_escape_string($this->db, $output[0]['event_more_detail_url']), mysqli_real_escape_string($this->db, $output[0]['event_video_detail_url']), mysqli_real_escape_string($this->db, $output[0]['event_cost']), mysqli_real_escape_string($this->db, $output[0]['event_compare_price']), mysqli_real_escape_string($this->db, $output[0]['event_banner_img_url']));
                $result1 = mysqli_query($this->db, $sql1);

                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $last_inserted_event_id = mysqli_insert_id($this->db);
                    if($output[0]['event_type'] == 'M' || $output[0]['event_type'] == 'S'|| $output[0]['event_type'] == 'C'){
                        $company_code_new = $this->clean($company_code);
//                        $event_title_new = $this->clean($output[0]['event_title']);
                        $event_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/e/?=$company_code_new/$company_id/$last_inserted_event_id";
                        $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->upload_folder_name/Default/default.png";
                    if($output[0]['event_banner_img_url'] !== $event_banner_img_url){
                        $file_name = $company_id . "-" . $last_inserted_event_id;
                        $cfolder_name = "Company_$company_id";
                          $old = getcwd(); // Save the current directory
                        $dir_name = "../../../$this->upload_folder_name";
                        chdir($dir_name);
                        if (!file_exists($cfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($cfolder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($cfolder_name);
                        $folder_name = "Events";
                        if (!file_exists($folder_name)) {
                            $oldmask = umask(0);
                            mkdir($folder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($folder_name);
                        $file = $file_name.'.png' ;
                       $check = copy($output[0]['event_banner_img_url'],$file);
                       chdir($old);
                       if($check){
                        $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->upload_folder_name/$cfolder_name/$folder_name/$file";
                       }else{
                            $error_log = array('status' => "Failed", "msg" => "Image copy failed", "image_url" => $output[0]['event_banner_img_url']);
                            log_info($this->json($error_log)); 
                       }
                    }

                        $sql_update_url = sprintf("UPDATE `event` SET `event_url` = '$event_url',`event_banner_img_url` = '$event_banner_img_url' WHERE `event_id` = '%s' ", mysqli_real_escape_string($this->db, $last_inserted_event_id));
                        $result_update_url = mysqli_query($this->db, $sql_update_url);
                        if (!$result_update_url) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update_url");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    if ($num_of_rows > 1) {
                        
                        for ($i = 1; $i < count($output); $i++) {
                            if(is_null($output[$i]['event_capacity'])){
                                $capacity = 'NULL';
                            }else{
                                $capacity = $output[$i]['event_capacity'];
                            }
                            if(empty(trim($output[$i]['event_compare_price']))||is_null($output[$i]['event_compare_price'])){      
                                $event_compare_price = 'NULL';   
                            }else{
                                $event_compare_price=$output[$i]['event_compare_price'];
                            }
                            $sql2 = sprintf("INSERT INTO `event` (`company_id`, `parent_id`, `event_type`, `event_status`, `event_title`, 
                                `event_category_subtitle`, `event_desc`, `event_more_detail_url`,  `event_video_detail_url`, `event_cost`, 
                                `event_compare_price`, `event_capacity`, `capacity_text`, `processing_fees`, `event_onetime_payment_flag`, 
                                `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, 
                                `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, 
                                `event_banner_img_content`, `event_banner_img_url`, `event_registration_column_1`, 
                                `event_reg_col_1_mandatory_flag`, `event_registration_column_2`, `event_reg_col_2_mandatory_flag`, 
                                `event_registration_column_3`, `event_reg_col_3_mandatory_flag`, `event_registration_column_4`, 
                                `event_reg_col_4_mandatory_flag`, `event_registration_column_5`, `event_reg_col_5_mandatory_flag`, 
                                `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, 
                                `event_reg_col_7_mandatory_flag`, `event_registration_column_8`, `event_reg_col_8_mandatory_flag`, 
                                `event_registration_column_9`, `event_reg_col_9_mandatory_flag`, `event_registration_column_10`, 
                                `event_reg_col_10_mandatory_flag`, `event_sort_order`) VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                                %s, %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                                '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', 
                                NextVal('event_sort_order_seq'))", mysqli_real_escape_string($this->db, $company_id), 
                                mysqli_real_escape_string($this->db, $last_inserted_event_id), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_type']), 
                                mysqli_real_escape_string($this->db, $e_status), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_title'] .$copy_string), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_category_subtitle'] .$copy_string), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_desc']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_more_detail_url']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_video_detail_url']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_cost']), 
                                mysqli_real_escape_string($this->db, $event_compare_price), 
                                mysqli_real_escape_string($this->db, $capacity), 
                                mysqli_real_escape_string($this->db, $output[$i]['capacity_text']), 
                                mysqli_real_escape_string($this->db, $output[$i]['processing_fees']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_onetime_payment_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_recurring_payment_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['total_order_text']), 
                                mysqli_real_escape_string($this->db, $output[$i]['total_quantity_text']), 
                                mysqli_real_escape_string($this->db, $output[$i]['deposit_amount']), 
                                mysqli_real_escape_string($this->db, $output[$i]['number_of_payments']), 
                                mysqli_real_escape_string($this->db, $output[$i]['payment_startdate_type']), 
                                mysqli_real_escape_string($this->db, $output[$i]['payment_startdate']), 
                                mysqli_real_escape_string($this->db, $output[$i]['payment_frequency']), 
                                mysqli_real_escape_string($this->db, $output[$i]['waiver_policies']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_banner_img_content']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_banner_img_url']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_1']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_1_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_2']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_2_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_3']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_3_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_4']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_4_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_5']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_5_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_6']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_6_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_7']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_7_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_8']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_8_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_9']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_9_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_10']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_10_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_banner_img_url']));
                            $result2 = mysqli_query($this->db, $sql2);

                            if (!$result2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            } else {
                                $last_inserted_event_id1 = mysqli_insert_id($this->db);
                                if ($output[$i]['event_type'] == 'M' || $output[$i]['event_type'] == 'S'|| $output[$i]['event_type'] == 'C') {
                                    $company_code_new = $this->clean($company_code);
                                    $event_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/e/?=$company_code_new/$company_id/$last_inserted_event_id1";
                                    $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/Default/default.png";
                                    if ($output[$i]['event_banner_img_url'] !== $event_banner_img_url) {
                                        $file_name = $company_id . "-" . $last_inserted_event_id1;
                                        $cfolder_name = "Company_$company_id";
                                        $old = getcwd(); // Save the current directory
                                        $dir_name = "../../../$this->upload_folder_name";
                                        chdir($dir_name);
                                        if (!file_exists($cfolder_name)) {
                                            $oldmask = umask(0);
                                            mkdir($cfolder_name, 0777, true);
                                            umask($oldmask);
                                        }
                                        chdir($cfolder_name);
                                        $folder_name = "Events";
                                        if (!file_exists($folder_name)) {
                                            $oldmask = umask(0);
                                            mkdir($folder_name, 0777, true);
                                            umask($oldmask);
                                        }
                                        chdir($folder_name);
                                        $file = $file_name . '.png';
                                        $check = copy($output[$i]['event_banner_img_url'], $file);
                                        chdir($old);
                                        if ($check) {
                                            $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/$cfolder_name/$folder_name/$file";
                                        } else {
                                            $error_log = array('status' => "Failed", "msg" => "Image copy failed", "image_url" => $output[$i]['event_banner_img_url']);
                                            log_info($this->json($error_log));
                                        }
                                    }

                                    $sql_update_url = sprintf("UPDATE `event` SET `event_url` = '$event_url',`event_banner_img_url` = '$event_banner_img_url' WHERE `event_id` = '%s' ", mysqli_real_escape_string($this->db, $last_inserted_event_id1));
                                    $result_update_url = mysqli_query($this->db, $sql_update_url);
                                    if (!$result_update_url) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update_url");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    }
                                }
                            }
                        }
                    } 
                }
                if(empty($for_initial)){
                    if($copy_event_type === "Main"){ 
                        if($event_template_flag == "Y"){
                            $out = array('status' => "Success", "msg" => "Template copied successfully");
                            $this->response($this->json($out), 200); // If no records "No Content" status
                        }else{
                            $this->getEventsDetailByCompany($company_id, 'S', 0 ,'');
                        }
                    } else if($copy_event_type === "Child"){ // only for child copy
                        if($list_type == 'draft'){
                            $new_list_type = 'S';
                        }else if($list_type == 'past'){
                            $new_list_type = 'D';
                        }else{
                            $new_list_type = 'P';
                        }
                        $this->getEventsDetailByParent($company_id, $new_list_type,$parent_id);
                    }else {
                        $this->getEventsDetailByEventId($company_id, $parent_id, "C", $list_type, 0);
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Event details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
    //Get Social details by Company ID
    public function eventSortTest() {

        $sql = sprintf("SELECT * FROM `event` order by `event_begin_dt` asc");
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                
                $length = count($output);
                for ($i = 0; $i < $length; $i++) {
                    $j = $i + 1;
                    $update_event = sprintf("UPDATE `event` SET `event_sort_order`= $j WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $output[$i]['event_id']));
                    $result = mysqli_query($this->db, $update_event);

                    //Print Company details after login success
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Event details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
    public function getEventParticipantDetails($company_id, $event_id, $event_status_tab, $search,$draw_table,$length_table,$start,$sorting){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        
//        $end_limit=200;
        $sort = $sorting[0]['column'];
        $sort_type = $sorting[0]['dir'];
        $search_value = $search['value'];
        $out=$output=[];
        $out['cancel']=$out['order']=$count['recent'] = $count['cancelled'] = $count['recent_filtered']=$count['cancelled_filtered']= 0;
        $date_format = "'%b %d, %Y'";
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $last_updt_date = " DATE(CONVERT_TZ(erd.last_updt_dt,$tzadd_add,'$new_timezone'))";
        
        
        if(!empty($search_value)){
             $s_text=" and ( buyer_name like  '%$search_value%'  or buyer_email like  '%$search_value%' or erd.`balance_due`  like  '%$search_value%'   or `buyer_phone` like  '%$search_value%' or  event_reg_type_user like '%$search_value%'
                    or CONCAT(`event_registration_column_2`, ', ', `event_registration_column_1`) like  '%$search_value%' or   DATE_FORMAT(($last_updt_date), '%b %d, %Y')  like  '%$search_value%' 
                    or  DATE_FORMAT(`registration_date`, '%b %d, %Y')  like  '%$search_value%' or  erd.`payment_amount` like  '%$search_value%'  )";
         }else{
             $s_text='';
         }
        
         if($event_status_tab=='O'){
             $column=array(2=>"buyer_name",3=>"participant_name",4=>"participant_email",5=>"participant_phone",6=>"cast(quantity AS unsigned)",7=>"cast(paid_amount AS unsigned)",8=>"cast(balance_due AS unsigned)",9=>"date(registration_date)",10=>"event_reg_type_user");
         }else{
             $column=array(2=>"buyer_name",3=>"participant_name",4=>"participant_email",5=>"participant_phone",6=>"cast(quantity AS unsigned)",7=>"date(registration_date)",8=>"event_reg_type_user",9=>"date(erd.last_updt_dt)");
         }
        
        if($event_status_tab == 'O'){ //ORDERS TAB
            $event_tab = "erd.`payment_status` in ('CC','RC','RP')";
        }else{
            $event_tab = "erd.`payment_status` in ('CP','RF')";
        }
        
        $query = sprintf("SELECT er.`event_reg_id` as id, er.`company_id`, erd.`event_id`, erd.`event_parent_id`, er.`student_id`, `buyer_name`, CONCAT(`event_registration_column_2`, ', ', `event_registration_column_1`) participant_name,
                IF(event_reg_type_user='P', 'Control Panel', IF(event_reg_type_user='M', 'Mobile App', IF(event_reg_type_user='U', 'Website URL', IF(event_reg_type_user='SP', 'Staff POS', IF(event_reg_type_user='PP', 'Public POS', ''))))) event_reg_type_user,
                if(`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(`registration_date`,%s) registration_date, `payment_type`, erd.`quantity`, erd.`payment_amount`, erd.`balance_due`, 
                (erd.`payment_amount` - erd.`balance_due`) paid_amount, erd.`payment_status`, IF(erd.`payment_status`='RF',concat('-',erd.`payment_amount`),0) refunded_amount, DATE_FORMAT(($last_updt_date),%s) last_updt_dt,
                if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon
                FROM `event_registration` er LEFT JOIN `event_reg_details` erd ON er.`event_reg_id` = erd.`event_reg_id`
                AND erd.`event_id`='%s' 
                LEFT JOIN student s1 on er.`student_id` = s1.student_id and er.company_id = s1.company_id
                LEFT JOIN student s2 on er.`buyer_email` = s2.student_email and er.company_id = s2.company_id
                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                WHERE er.`company_id`='%s' AND er.`event_id` IN (SELECT IFNULL(`event_parent_id`, `event_id`) FROM `event_reg_details` WHERE `event_id`='%s') AND erd.`event_id`='%s'  %s AND $event_tab ORDER BY $column[$sort] $sort_type LIMIT $start, $length_table ", $date_format,$date_format,
                mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$event_id),$s_text);
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            $sql = sprintf("SELECT count(*) status_count, payment_status ,'wo' as search from `event_reg_details` where event_id='%s' group by payment_status
                        union
                       SELECT count(*) status_count,payment_status,'w' as search   FROM `event_reg_details` erd LEFT JOIN `event_registration` er ON er.`event_reg_id` = erd.`event_reg_id`
                        AND erd.`event_id`='%s' 
                       LEFT JOIN student s1 on er.`student_id` = s1.student_id and er.company_id = s1.company_id
                       LEFT JOIN student s2 on er.`buyer_email` = s2.student_email and er.company_id = s2.company_id
                       LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                       where erd.event_id='%s' %s group by payment_status ",
                    mysqli_real_escape_string($this->db,$event_id),mysqli_real_escape_string($this->db,$event_id), 
                    mysqli_real_escape_string($this->db,$event_id),$s_text);
            $result_sql = mysqli_query($this->db, $sql);
            if(!$result_sql){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }else{
                $numsql_rows = mysqli_num_rows($result_sql);
                if($numsql_rows>0){
                     while($rows1= mysqli_fetch_assoc($result_sql)){
                        $payment_status = $rows1['payment_status'];
                        $status_count = $rows1['status_count'];
                        if($rows1['search'] == 'wo') {
                            if ($payment_status == 'CC' || $payment_status == 'RC' || $payment_status == 'RP') {
                                $count['recent'] += $status_count;
                                $out['order'] = $count['recent'];
                            } elseif ($payment_status == 'RF' || $payment_status == 'CP') {
                                $count['cancelled'] += $status_count;
                                $out['cancel'] = $count['cancelled'];
                            }
                            if ($event_status_tab == 'O') {
                                $out['recordsTotal'] = $out['order'];
                            } else {
                                $out['recordsTotal'] = $out['cancel'];
                            }
                        } elseif ($rows1['search'] == 'w') {
                            if ($payment_status == 'CC' || $payment_status == 'RC' || $payment_status == 'RP') {
                                $count['recent_filtered'] += $status_count;
//                                $out['order'] = $count['recent_filtered'];
                            } elseif ($payment_status == 'RF' || $payment_status == 'CP') {
                                $count['cancelled_filtered'] += $status_count;
//                                $out['cancel'] = $count['cancelled'];
                            }
                            if ($event_status_tab == 'O') {
                                $out['recordsFiltered'] = $count['recent_filtered'];
                            } else {
                                $out['recordsFiltered'] = $count['cancelled_filtered'];
                            }
                        }
                    }
                }                
              
            }
            if($num_rows>0){
//                $output['recent'] = $output['cancelled'] = [];
                while($row=  mysqli_fetch_assoc($result)){
                    if($event_status_tab=='O'){
                        $output[] = $row;
                    }else{
                        $output[]= $row;
                    }
                }    
                $out['draw']=$draw_table;
                $out['data']=$output;
//                if(!empty($search_value)){
//                   $out['recordsFiltered'] = $num_rows;
//               }
//                $res = array("status" => "Success", "msg" => $output, "count" => $count);
                $this->response($this->json($out), 200);
            }else{
                $out['draw']=$draw_table;
                $out['data']=$output;
//                $error = array('status' => "Failed", "msg" => "No participants registered yet.", "count" => $count);
                $this->response($this->json($out), 200); // If no records "No Content" status
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
//    private function getProcessingFee($upgrade_status){
//        $temp=[];
//        $x=0;
//        $file_handle = fopen($this->processing_fee_file_name, "r");
//        while (!feof($file_handle)) {
//            $lines = fgets($file_handle);
//            $line_contents = explode(",", $lines);
//            for($i=0;$i<count($line_contents);$i++){                
//                $cnt = explode("=", $line_contents[$i]);
//                $temp[$x][$cnt[0]] = $cnt[1];
//            }
//            $x++;
//        }
//        fclose($file_handle);
//        
//        for($i=0;$i<count($temp);$i++){
//            if($temp[$i]['level']==$upgrade_status){
//                return $temp[$i];
//            }
//        }
//    }
    
    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }
        
   
    //Get Event details by Company ID
    public function getEventsDetailByCompany($company_id, $list_type, $limit, $from) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        
//      Get Event Status
        $selecsql=sprintf("SELECT event_enabled FROM `company`  WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $selectresult= mysqli_query($this->db,$selecsql);
        if (!$selectresult) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else{
            if (mysqli_num_rows($selectresult) > 0) {
                $statusoutput = mysqli_fetch_assoc($selectresult);
            }
        }
//      Get Event Status
        
        $event_list_url_init = 0;
        $event_list_url = $company_code = '';
        $event_count['P'] = $event_count['D'] = $event_count['S'] = 0;      // P - Live, S - Draft, D - Past Events type
        $sql_count = sprintf("SELECT COUNT(*) CNT, `event_status`, 'draft' as list_type FROM `event` WHERE `event_status`='S' AND `event_type` IN ('M', 'S') AND `company_id` = '%s'
                    UNION 
                    SELECT COUNT(*) CNT, `event_status`, 'live' as list_type FROM `event` WHERE `company_id` = '%s' AND (`event_status`='P' AND `event_type`='S' AND (DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00')) OR 
                    (`event_type`='M' AND `event_id` IN (SELECT `parent_id` FROM `event` WHERE `company_id` = '%s' AND `event_status`='P' AND `event_type`='C' AND (DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00')) OR
                    (`event_id` IN (SELECT event_id FROM `event` WHERE `event_status`='P' AND `event_type`='M' AND `company_id` = '%s'
                    AND (`event_id` NOT IN (SELECT e1.`event_id` FROM `event` e1 INNER JOIN `event` e2 ON e2.parent_id = e1.event_id AND e2.company_id = e1.company_id
                    WHERE e1.`event_status`='P' AND e2.`event_status`!='D' AND e1.`company_id` = '%s')))))
                    UNION 
                    SELECT COUNT(*) CNT, `event_status`, 'past' as list_type FROM `event` WHERE `company_id` = '%s' AND (`event_status`='P' AND `event_type`='S' AND (DATE(`event_end_dt`) <= ($currdate_add) AND DATE(`event_end_dt`)!='0000-00-00')) OR 
                    (`event_type`='M' AND `event_id` IN (SELECT `parent_id` FROM `event` WHERE `company_id` = '%s' AND `event_status`='P' AND `event_type`='C' AND (DATE(`event_end_dt`) <= ($currdate_add) && DATE(`event_end_dt`)!='0000-00-00')))",
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$company_id));
        $result_count = mysqli_query($this->db, $sql_count);
        if (!$result_count) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_count");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if(mysqli_num_rows($result_count)>0){
                while($row_count = mysqli_fetch_assoc($result_count)){
                    if($row_count['event_status']=='S'){
                        $event_count['S'] = $row_count['CNT'];
                    }elseif($row_count['event_status']=='P'){
                        if($row_count['list_type']=='live'){
                            $event_count['P'] = $row_count['CNT'];
                        }elseif($row_count['list_type']=='past'){
                            $event_count['D'] = $row_count['CNT'];
                        }
                    }
                }
            }
        }
        if($list_type == 'S'){
            $str = "AND (event_id IN (SELECT event_id FROM `event` WHERE `event_status`='S' AND `event_type` IN ('M', 'S')) OR parent_id IN (SELECT event_id FROM `event` WHERE `event_status`='S' AND `event_type` IN ('M')))";
        }elseif($list_type == 'P'){
            $str = "AND (event_id IN (SELECT event_id FROM `event` WHERE `event_status`='P' AND `event_type` IN ('S', 'C') AND (DATE(`event_end_dt`) >= $currdate_add || DATE(`event_end_dt`)='0000-00-00')) OR
                    `event_id` IN (SELECT `parent_id` FROM `event` WHERE `event_status`='P' AND `event_type`='C' AND (DATE(`event_end_dt`) >= $currdate_add || DATE(`event_end_dt`)='0000-00-00')) OR
                    `event_id` IN (SELECT event_id FROM `event` WHERE `event_status`='P' AND `event_type`='M' AND (
                    `event_id` NOT IN (SELECT e1.`event_id` FROM `event` e1 INNER JOIN `event` e2 ON e2.parent_id = e1.event_id
                                   WHERE e1.`event_status`='P' AND e2.`event_status`!='D'))))";
        }elseif($list_type == 'D'){
            $str = "AND (event_id IN (SELECT event_id FROM `event` WHERE `event_status`='P' AND `event_type` IN ('S', 'C') AND (DATE(`event_end_dt`) <= $currdate_add && DATE(`event_end_dt`)!='0000-00-00')) OR
                    `event_id` IN (SELECT `parent_id` FROM `event` WHERE `event_status`='P' AND `event_type`='C' AND (DATE(`event_end_dt`) <= $currdate_add && DATE(`event_end_dt`)!='0000-00-00')))";
        }
//        IF( `event_status` = 'S', 'draft', 
//            IF( `event_type`='M', 'past', IF( DATE(`event_begin_dt`) <= ($currdate_add), 
//            IF(DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00', 'live', 'past' ) , 'live' ))) list_type

//        $sql = sprintf("SELECT  `event_id` ,  e.`company_id`, `company_code`,  `parent_id` ,  `event_type` ,  `event_status` ,  `event_begin_dt` ,  `event_end_dt` ,  `event_title` ,CONCAT(`event_url`,'///',UNIX_TIMESTAMP()) event_url,  `event_category_subtitle` ,  
//            `event_desc` ,  `event_more_detail_url`,  `event_video_detail_url` , `event_cost` ,  `event_compare_price` ,  `registrations_count` ,  `event_capacity` ,  `capacity_text` ,  `processing_fees`, `event_onetime_payment_flag`, 
//            `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `net_sales` ,  `event_sort_order` ,  
//            `event_banner_img_url`, `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, 
//            `event_registration_column_2`, `event_reg_col_2_mandatory_flag`,`event_registration_column_3`, `event_reg_col_3_mandatory_flag`,
//            `event_registration_column_4`, `event_reg_col_4_mandatory_flag`,`event_registration_column_5`, `event_reg_col_5_mandatory_flag`,
//            `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, `event_reg_col_7_mandatory_flag`,
//            `event_registration_column_8`, `event_reg_col_8_mandatory_flag`,`event_registration_column_9`, `event_reg_col_9_mandatory_flag`,
//            `event_registration_column_10`, `event_reg_col_10_mandatory_flag`, IF('%s'='S', 'draft', IF('%s'='P', 'live', 'past')) list_type
//            FROM  `event` e LEFT JOIN `company` c ON e.`company_id` = c.`company_id`
//            WHERE  e.`company_id` = '%s' AND event_type IN ('M', 'S')
//            AND  `event_status` !=  'D' %s ORDER BY `event_sort_order` desc limit %s, 20",mysqli_real_escape_string($this->db,$list_type),mysqli_real_escape_string($this->db,$list_type), mysqli_real_escape_string($this->db,$company_id), $str,mysqli_real_escape_string($this->db,$limit));
        if($from!='I'){
//            $limit_str = "limit ".mysqli_real_escape_string($this->db,$limit).", 20";
            $limit_str = "";
        }else{
            $limit_str = "";
        }
        $sql = sprintf("SELECT  `event_id` ,  e.`company_id`, `company_code`,  `parent_id` ,  `event_type` ,  `event_status` , `event_show_status`,  `event_begin_dt` ,  `event_end_dt` ,  `event_title` ,CONCAT(`event_url`,'///',UNIX_TIMESTAMP()) event_url,  `event_category_subtitle` ,  
            `event_cost` ,  `event_compare_price` , `registrations_count` ,  `event_capacity` ,  `capacity_text` ,  `processing_fees`, `event_onetime_payment_flag`, 
            `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, `event_banner_img_url`,
            `event_registration_column_2`, `event_reg_col_2_mandatory_flag`,`event_registration_column_3`, `event_reg_col_3_mandatory_flag`,
            `event_registration_column_4`, `event_reg_col_4_mandatory_flag`,`event_registration_column_5`, `event_reg_col_5_mandatory_flag`,
            `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, `event_reg_col_7_mandatory_flag`,
            `event_registration_column_8`, `event_reg_col_8_mandatory_flag`,`event_registration_column_9`, `event_reg_col_9_mandatory_flag`,
            `event_registration_column_10`, `event_reg_col_10_mandatory_flag`,`event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `net_sales` ,  `event_sort_order` ,  
            IF('%s'='S', 'draft', IF('%s'='P', 'live', 'past')) list_type
            FROM  `event` e LEFT JOIN `company` c ON e.`company_id` = c.`company_id`
            WHERE  e.`company_id` = '%s' AND `event_status` !=  'D' %s ORDER BY `event_sort_order` desc $limit_str",
                mysqli_real_escape_string($this->db,$list_type),mysqli_real_escape_string($this->db,$list_type), mysqli_real_escape_string($this->db,$company_id), $str);
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else { 
            $output = $parent_mapping = $parent_integrated = $response['live'] = $response['draft'] = $response['past'] = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    if($event_list_url_init==0){
                        $company_code = $row['company_code'];
                        $event_list_url_init++;
                        $event_list_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/e/?=$company_code/$company_id".'///'.time();
                    }
//                    $disc = $this->getDiscountDetails($company_id, $row['event_id']);
//                    $row['discount'] = $disc['discount'];
                    $row['child_events']=[];
                    $reg_field_name_array = [];
                    $temp_reg_field = "event_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "event_reg_col_".$z."_mandatory_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row['reg_columns'] = $reg_field_name_array;
                    if($row['event_type']=='M'){
                        $parent_mapping[$row['event_id']] = count($parent_integrated);
                        $parent_integrated[] = $row;
                    }
                    if($row['event_type']=='S' && empty($row['parent_id'])){
                        $parent_integrated[] = $row;
                    }
                    $output[] = $row;
                }
                for($i=0;$i<count($output);$i++){//integrate the child event with parent event for multiple event
                    if(!empty(trim($output[$i]['parent_id']))){
                        $parent_integrated[$parent_mapping[$output[$i]['parent_id']]]['child_events'][] = $output[$i];
//                        if($output[$i]['list_type']=='live'){//default parent status - past, when atleast a child event is in live then parent status changed to live
//                            $parent_integrated[$parent_mapping[$output[$i]['parent_id']]]['list_type'] = 'live';
//                        }
                    }
                }
                $parent_count = count($parent_integrated);
                for($i=0;$i<$parent_count;$i++){//split-up by live, past & draft
                    $child_count = count($parent_integrated[$i]['child_events']);
                    if ($child_count > 0) {
                        usort($parent_integrated[$i]['child_events'], function($a, $b) {
                            return $a['event_begin_dt'] > $b['event_begin_dt'];
                        });
                        
                        $check_type_conflict = $cevent_capacity =$cevent_regcount=$cevent_net_sales= 0;
                        $inital_child_type = $parent_integrated[$i]['child_events'][0]['list_type'];
                        $parent_integrated[$i]['list_type'] = $inital_child_type;
                        
                        for($j=0;$j<$child_count;$j++){
                            $current_child_type = $parent_integrated[$i]['child_events'][$j]['list_type'];
                            if($j>0 && $current_child_type != $inital_child_type){
                                if($check_type_conflict==0){
                                    $check_type_conflict = 1;
                                    $parent_integrated[] = $parent_integrated[$i];
                                    $parent_count++;
                                    $parent_integrated[count($parent_integrated)-1]['list_type'] = $current_child_type;
//                                    $parent_integrated[count($parent_integrated)-1]['child_events'] = [];
//                                    $parent_integrated[count($parent_integrated)-1]['child_events'][] = $parent_integrated[$i]['child_events'][$j];
                                    array_splice($parent_integrated[$i]['child_events'],$j, 1);
                                    $j--;
                                    $child_count--;
                                }else{
//                                    $parent_integrated[count($parent_integrated)-1]['child_events'][] = $parent_integrated[$i]['child_events'][$j];
                                    array_splice($parent_integrated[$i]['child_events'], $j, 1);
                                    $child_count--;
                                    $j--;
                                }
                            }else{
                                $cevent_capacity += $parent_integrated[$i]['child_events'][$j]['event_capacity'];  
                                $cevent_regcount += $parent_integrated[$i]['child_events'][$j]['registrations_count']; 
                                $cevent_net_sales += $parent_integrated[$i]['child_events'][$j]['net_sales']; 
                            }
                        }
                        $parent_integrated[$i]['event_capacity'] = $cevent_capacity;
                        $parent_integrated[$i]['registrations_count'] = $cevent_regcount;
                        $parent_integrated[$i]['net_sales'] = $cevent_net_sales;
                        
                    }
                    if($parent_integrated[$i]['list_type']=='live'){
                        $response['live'][] = $parent_integrated[$i];
                    }elseif($parent_integrated[$i]['list_type']=='draft'){
                        $response['draft'][] = $parent_integrated[$i];
                    }elseif($parent_integrated[$i]['list_type']=='past'){
                        $response['past'][] = $parent_integrated[$i];
                    }
                }
                usort( $response['live'], function($a, $b) {
                    return $a['event_sort_order'] < $b['event_sort_order'];
                });
                
                $out = array('status' => "Success", 'msg' => $response, 'event_list_url' => $event_list_url, "count"=>$event_count, "event_status"=>$statusoutput);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Event details doesn't exist.", "count"=>$event_count,"event_status"=>$statusoutput);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
     public function getEventsDetailByParent($company_id, $list_type,$parent_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = "SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
//        For Event Count
        $event_count['P'] = $event_count['D'] = $event_count['S'] = 0;      // P - Live, S - Draft, D - Past Events type
        $sql_count = sprintf("SELECT COUNT(*) CNT, `event_status`, 'draft' as list_type FROM `event` WHERE `event_status`='S' AND `event_type` IN ('M', 'S') AND `company_id` = '%s'
                    UNION 
                    SELECT COUNT(*) CNT, `event_status`, 'live' as list_type FROM `event` WHERE `company_id` = '%s' AND (`event_status`='P' AND `event_type`='S' AND (DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00')) OR 
                    (`event_type`='M' AND `event_id` IN (SELECT `parent_id` FROM `event` WHERE `company_id` = '%s' AND `event_status`='P' AND `event_type`='C' AND (DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00')) OR
                    (`event_id` IN (SELECT event_id FROM `event` WHERE `event_status`='P' AND `event_type`='M' AND `company_id` = '%s'
                    AND (`event_id` NOT IN (SELECT e1.`event_id` FROM `event` e1 INNER JOIN `event` e2 ON e2.parent_id = e1.event_id AND e2.company_id = e1.company_id
                    WHERE e1.`event_status`='P' AND e2.`event_status`!='D' AND e1.`company_id` = '%s')))))
                    UNION 
                    SELECT COUNT(*) CNT, `event_status`, 'past' as list_type FROM `event` WHERE `company_id` = '%s' AND (`event_status`='P' AND `event_type`='S' AND (DATE(`event_end_dt`) <= ($currdate_add) AND DATE(`event_end_dt`)!='0000-00-00')) OR 
                    (`event_type`='M' AND `event_id` IN (SELECT `parent_id` FROM `event` WHERE `company_id` = '%s' AND `event_status`='P' AND `event_type`='C' AND (DATE(`event_end_dt`) <= ($currdate_add) && DATE(`event_end_dt`)!='0000-00-00')))",
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$company_id));
        $result_count = mysqli_query($this->db, $sql_count);
        if (!$result_count) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_count");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if(mysqli_num_rows($result_count)>0){
                while($row_count = mysqli_fetch_assoc($result_count)){
                    if($row_count['event_status']=='S'){
                        $event_count['S'] = $row_count['CNT'];
                    }elseif($row_count['event_status']=='P'){
                        if($row_count['list_type']=='live'){
                            $event_count['P'] = $row_count['CNT'];
                        }elseif($row_count['list_type']=='past'){
                            $event_count['D'] = $row_count['CNT'];
                        }
                    }
                }
            }
        }
        $out = [];
        // For selection
        $sql1 = sprintf("SELECT  `event_id` ,  e.`company_id`, `company_code`,  `parent_id` ,  `event_type` ,  `event_status` , `event_show_status`, `event_begin_dt` ,  `event_end_dt` ,  `event_title` ,CONCAT(`event_url`,'///',UNIX_TIMESTAMP()) event_url,  `event_category_subtitle` ,   `event_banner_img_url`,
            `event_cost` ,  `event_compare_price` , `registrations_count` ,  `event_capacity` ,  `capacity_text` ,  `processing_fees`, `event_onetime_payment_flag`, 
            `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `net_sales` ,  `event_sort_order` ,  
         IF('%s'='S', 'draft', IF('%s'='P', 'live', 'past')) list_type, IF( `event_status` = 'S', 'S', 
        IF( `event_type`='M', 'D', IF( DATE(`event_begin_dt`) <= ($currdate_add), 
        IF(DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00', 'P', 'D' ) , 'P' ))) l_type
            FROM  `event` e LEFT JOIN `company` c ON e.`company_id` = c.`company_id`
            WHERE  e.`company_id` = '%s' AND  `event_status` !=  'D' AND  `parent_id` =  '%s'  ORDER BY `event_sort_order` desc",mysqli_real_escape_string($this->db,$list_type),mysqli_real_escape_string($this->db,$list_type), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$parent_id));
         $result1 = mysqli_query($this->db, $sql1);
        if(!$result1){
            $error_log1 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log1));
            $error1 = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error1), 200);
        }else{
            $num_of_rows1 = mysqli_num_rows($result1);
            if($num_of_rows1>0){
                while($row1 = mysqli_fetch_assoc($result1)){
                    if($list_type==$row1['l_type']){
                        $out[] = $row1;
                    }
                }
            }
            usort($out, function($a, $b) {
                return $a['event_begin_dt'] > $b['event_begin_dt'];
            });
            $output = array('status' => "Success", "msg" => $out, "count"=>$event_count);
            $this->response($this->json($output), 200);
        }
    }
    public function checkEventFirstTime($company_id,$list_type,$limit,$from) {
        $event_id = '';
        $query = sprintf("SELECT * FROM `event` where company_id='%s' ORDER BY event_id", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if(mysqli_num_rows($result)==0){
                $query2 = sprintf("SELECT `event_id`, `event_status` FROM `event` where company_id='6' AND event_type IN ('S', 'M') AND event_status='P' ORDER BY event_id");
                $result2 = mysqli_query($this->db, $query2);
                if (!$result2) {
                    $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log2));
                    $error2 = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error2), 200);
                } else {
                    $num_row = mysqli_num_rows($result2);
                    if ($num_row > 0) {
                        while ($row = mysqli_fetch_assoc($result2)) {
                            $event_id = $row['event_id'];
                            $event_status = $row['event_status'];
                            $this->copyEvent($company_id, $event_id, '','Y',$event_status,'Y');
                        }
                        // UPDATE POS EVENT STATUS TO ENABLED
                        $query3 = sprintf("UPDATE studio_pos_settings SET `event_status` = 'E' WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
                        $result3 = mysqli_query($this->db, $query3);
                        if (!$result3) {
                            $error_log3 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                            log_info($this->json($error_log3));
                            $error3 = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error3), 200);
                        }
                    }else{
                        $error = array('status' => "Failed", "msg" => "Event details does not exist.");
                        $this->response($this->json($error), 200);
                    }
                    $this->getEventsDetailByCompany($company_id, 'P', 0, $from);
                }
            }else{
                $this->getEventsDetailByCompany($company_id, $list_type, $limit, $from);
            }
        }
    }
    
    //Delete Event
    public function deleteEvent($company_id, $event_id, $list_type) {
        $check_event_registrations = sprintf("SELECT * FROM `event_reg_details` WHERE (`event_id`='%s' OR `event_parent_id`='%s') AND `payment_status` NOT IN ('RF', 'CP')", mysqli_real_escape_string($this->db, $event_id),  mysqli_real_escape_string($this->db, $event_id));
        $result_event_reg = mysqli_query($this->db, $check_event_registrations);
        if(!$result_event_reg){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_event_registrations");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($result_event_reg);
            if($num_of_rows>0){
                $error = array('status' => "Failed", "msg" => "This event cannot be canceled since there are registered users. Please cancel and refund all registered participants to continue");
                $this->response($this->json($error), 200);
            }
        }
        
        $get_event = sprintf("select `parent_id`, `event_type` from `event` where `event_id`='%s' AND `company_id`='%s'",  mysqli_real_escape_string($this->db, $event_id),  mysqli_real_escape_string($this->db, $company_id));
        $get_result = mysqli_query($this->db, $get_event);
        if(!$get_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($get_result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($get_result);
                $event_type = $row['event_type'];
                $parent_id = $row['parent_id'];
                
                $update_event = sprintf("Update `event` SET `event_status`='D' WHERE (`event_id`='%s' or `parent_id`='%s') and `company_id`='%s'", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
                $result = mysqli_query($this->db, $update_event);
                
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if($event_type=='C' && !empty($parent_id)){
                        $this->getEventsDetailByEventId($company_id,$parent_id,$event_type, $list_type, 0);
                    }else{
                        $out = array('status' => "Success", 'msg' => "Event Deleted Successfully.");
                        // If success everythig is good send header as "OK" and user details
                        $this->response($this->json($out), 200);
                    }
                }
            }else{
                $out = array('status' => "Success", 'msg' => "No Event found to delete.");
                    // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            }
        }
    }
    
    //Add New Event discount
    public function inserteventsdiscount($company_id, $event_id, $discount_type, $discount_code, $discount_amount, $list_type) {
        
        $parent_id=$return_event_id=$event_type="";
        $query = sprintf("SELECT * FROM `event_discount` WHERE `company_id`='%s' AND `event_id`='%s' AND `discount_code`='%s' AND `is_expired`='N'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$discount_code));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($result);
            if($num_of_rows>0){
                $error = array('status' => "Failed", "msg" => "Discount Code already available for this event.");
                $this->response($this->json($error), 417);
            }
        }
        $sql1 = sprintf("INSERT INTO `event_discount` (`company_id`, `event_id`, `discount_type`, `discount_code`, `discount_amount`) VALUES ('%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$discount_type), mysqli_real_escape_string($this->db,$discount_code), mysqli_real_escape_string($this->db,$discount_amount));      
        $result1 = mysqli_query($this->db, $sql1);

        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $get_event = sprintf("select `parent_id`, `event_type` from `event` where `event_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
            $get_result = mysqli_query($this->db, $get_event);
            if (!$get_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($get_result);
                if ($num_rows > 0) {
                    $row = mysqli_fetch_assoc($get_result);
                    $event_type = $row['event_type'];
                    $parent_id = $row['parent_id'];
                }
            }
            
            if (empty(trim($parent_id))) {
                $return_event_id = $event_id;
            } else {
                $return_event_id = $parent_id;
            }
            $this->getEventsDetailByEventId($company_id, $return_event_id, $event_type, $list_type, 0);            
        }
    }
    
    //Update Event discount details
    public function updateEventSDiscountInDB($company_id,$event_id,$event_discount_id,$discount_type,$discount_code,$discount_amount, $list_type) {

        $parent_id=$return_event_id=$event_type="";
        $query = sprintf("SELECT * FROM `event_discount` WHERE `company_id`='%s' AND `event_id`='%s' AND `discount_code`='%s' AND `event_discount_id`!='%s' AND `is_expired`='N'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$discount_code), mysqli_real_escape_string($this->db,$event_discount_id));
        $query_result = mysqli_query($this->db, $query);
        if(!$query_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($query_result);
            if($num_of_rows>0){
                $error = array('status' => "Failed", "msg" => "Discount Code already available for this event.");
                $this->response($this->json($error), 417);
            }
        }
        
        $update_company = sprintf("UPDATE `event_discount` SET `discount_type`='%s',`discount_code`='%s',`discount_amount`='%s' WHERE `company_id`='%s' AND `event_id`='%s' AND `event_discount_id`='%s'", mysqli_real_escape_string($this->db,$discount_type), mysqli_real_escape_string($this->db,$discount_code), mysqli_real_escape_string($this->db,$discount_amount), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$event_discount_id));
        $result = mysqli_query($this->db, $update_company);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $get_event = sprintf("select `parent_id`, `event_type` from `event` where `event_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
            $get_result = mysqli_query($this->db, $get_event);
            if (!$get_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($get_result);
                if ($num_rows > 0) {
                    $row = mysqli_fetch_assoc($get_result);
                    $event_type = $row['event_type'];
                    $parent_id = $row['parent_id'];
                }
            }
            
            if (empty(trim($parent_id))) {
                $return_event_id = $event_id;
            } else {
                $return_event_id = $parent_id;
            }

            $this->getEventsDetailByEventId($company_id, $return_event_id, $event_type, $list_type, 0);
        }
    }
    
    //Update Event discount details
    public function updateEventPaymentDetailsInDB($company_id,$event_id,$processing_fees,$event_onetime_payment_flag,$event_recurring_payment_flag,$total_order_text,$total_quantity_text,$deposit_amount,$number_of_payments,$payment_startdate_type,$payment_startdate,$payment_frequency, $list_type) {

        $return_event_id=$parent_id=$event_type="";
        $update_company = sprintf("UPDATE `event` SET `processing_fees`='%s',`event_onetime_payment_flag`='%s',`event_recurring_payment_flag`='%s',`total_order_text`='%s',`total_quantity_text`='%s',`deposit_amount`='%s',`number_of_payments`='%s',`payment_startdate_type`='%s',payment_startdate='%s',`payment_frequency`='%s' WHERE `company_id`='%s' AND `event_id`='%s'", mysqli_real_escape_string($this->db,$processing_fees), mysqli_real_escape_string($this->db,$event_onetime_payment_flag), mysqli_real_escape_string($this->db,$event_recurring_payment_flag), mysqli_real_escape_string($this->db,$total_order_text), mysqli_real_escape_string($this->db,$total_quantity_text), mysqli_real_escape_string($this->db,$deposit_amount), mysqli_real_escape_string($this->db,$number_of_payments), mysqli_real_escape_string($this->db, $payment_startdate_type), mysqli_real_escape_string($this->db,$payment_startdate), mysqli_real_escape_string($this->db,$payment_frequency), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id));
        $result = mysqli_query($this->db, $update_company);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $get_event = sprintf("select `parent_id`, `event_type` from `event` where `event_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
            $get_result = mysqli_query($this->db, $get_event);
            if (!$get_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($get_result);
                if ($num_rows > 0) {
                    $row = mysqli_fetch_assoc($get_result);
                    $event_type = $row['event_type'];
                    $parent_id = $row['parent_id'];
                }
            }
            
            if (empty(trim($parent_id))) {
                $return_event_id = $event_id;
            } else {
                $return_event_id = $parent_id;
            }
            $this->getEventsDetailByEventId($company_id, $return_event_id, $event_type, $list_type, 0); 
        }
    }
    
    //Delete Event discount details
    public function deleteEventDiscountInDB($company_id,$event_id,$event_discount_id, $list_type) {

        $parent_id=$return_event_id=$event_type="";
        $update_company = sprintf("UPDATE `event_discount` SET `is_expired`='Y' WHERE `company_id`='%s' AND `event_id`='%s' AND `event_discount_id`='%s'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$event_discount_id));
        $result = mysqli_query($this->db, $update_company);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $get_event = sprintf("select `parent_id`, `event_type` from `event` where `event_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
            $get_result = mysqli_query($this->db, $get_event);
            if (!$get_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($get_result);
                if ($num_rows > 0) {
                    $row = mysqli_fetch_assoc($get_result);
                    $event_type = $row['event_type'];
                    $parent_id = $row['parent_id'];
                }
            }
            
            if (empty(trim($parent_id))) {
                $return_event_id = $event_id;
            } else {
                $return_event_id = $parent_id;
            }
            $this->getEventsDetailByEventId($company_id, $return_event_id, $event_type, $list_type, 0); 
        }
    }   
    
     //Update Event discount details
    public function updateWaiverDetailsInDB($company_id,$event_id,$waiver_policies, $list_type) {

        $return_event_id=$parent_id=$event_type="";
        $update_company = sprintf("UPDATE `event` SET `waiver_policies`='%s' WHERE `company_id`='%s' AND `event_id`='%s'", mysqli_real_escape_string($this->db,$waiver_policies), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id));
        $result = mysqli_query($this->db, $update_company);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $get_event = sprintf("select `parent_id`, `event_type` from `event` where `event_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
            $get_result = mysqli_query($this->db, $get_event);
            if (!$get_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($get_result);
                if ($num_rows > 0) {
                    $row = mysqli_fetch_assoc($get_result);
                    $event_type = $row['event_type'];
                    $parent_id = $row['parent_id'];
                }
            }
            
            if (empty(trim($parent_id))) {
                $return_event_id = $event_id;
            } else {
                $return_event_id = $parent_id;
            }
            $this->getEventsDetailByEventId($company_id, $return_event_id, $event_type, $list_type, 0); 
        }
    }
    
     //Update Event discount details
    public function updateEventRegistrationDetailsInDB($company_id,$event_id,$event_column_name,$event_column_value,$event_mandatory_column_name,$event_mandatory_column_value, $list_type) {

        $return_event_id=$parent_id=$event_type="";
        $update_company = sprintf("UPDATE `event` SET $event_column_name='%s',$event_mandatory_column_name='%s' WHERE `company_id`='%s' AND `event_id`='%s'", mysqli_real_escape_string($this->db,$event_column_value), mysqli_real_escape_string($this->db,$event_mandatory_column_value), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id));
        $result = mysqli_query($this->db, $update_company);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $get_event = sprintf("select `parent_id`, `event_type` from `event` where `event_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
            $get_result = mysqli_query($this->db, $get_event);
            if (!$get_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($get_result);
                if ($num_rows > 0) {
                    $row = mysqli_fetch_assoc($get_result);
                    $event_type = $row['event_type'];
                    $parent_id = $row['parent_id'];
                }
            }
            
            if (empty(trim($parent_id))) {
                $return_event_id = $event_id;
            } else {
                $return_event_id = $parent_id;
            }
            $this->getEventsDetailByEventId($company_id, $return_event_id, $event_type, $list_type, 0); 
        }
    }
    
    //Delete Event Registration
    public function deleteEventRegistrationDetailsInDB($company_id,$event_id,$event_column_name, $list_type) {
        
        $column_array = array("event_registration_column_1","event_registration_column_2","event_registration_column_3",
            "event_registration_column_4","event_registration_column_5","event_registration_column_6",
            "event_registration_column_7","event_registration_column_8",
            "event_registration_column_9","event_registration_column_10");
        
        $index = array_search($event_column_name, $column_array);
        
        $column_string = '';
        for($i=$index; $i<10; $i++){
            if($i!=9){
                $j=$i+1;
                $column_string .= "$column_array[$i] = $column_array[$j] , ";
            }else{
                $column_string .= "$column_array[$i] = ''";                
            }
        }
 
        $return_event_id=$parent_id=$event_type="";
        $update_event = sprintf("UPDATE `event` SET $column_string WHERE `company_id`='%s' AND `event_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
        $result = mysqli_query($this->db, $update_event);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {

            $update_event_registration = sprintf("UPDATE `event_registration` SET $column_string WHERE `company_id`='%s' AND `event_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
            $result = mysqli_query($this->db, $update_event_registration);

            //Print Company details after login success
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_registration");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $get_event = sprintf("select `parent_id`, `event_type` from `event` where `event_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
                $get_result = mysqli_query($this->db, $get_event);
                if (!$get_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_rows = mysqli_num_rows($get_result);
                    if ($num_rows > 0) {
                        $row = mysqli_fetch_assoc($get_result);
                        $event_type = $row['event_type'];
                        $parent_id = $row['parent_id'];
                    }
                }

                if (empty(trim($parent_id))) {
                    $return_event_id = $event_id;
                } else {
                    $return_event_id = $parent_id;
                }
                $this->getEventsDetailByEventId($company_id, $return_event_id, $event_type, $list_type, 0);
            }
        }
    }
    
    //Event Registration Sorting 
    public function eventRegistrationSortingDetailsInDB($company_id,$event_id,$old_location_id,$new_location_id, $list_type) {
        
        $return_event_id=$event_old_location_value=$event_old_location_mandatory_value=$event_reg_old_location_value=$parent_id=$event_type=$column_string=$column_mandatory_string=$index="";
        
        $column_array = array("event_registration_column_1","event_registration_column_2","event_registration_column_3",
            "event_registration_column_4","event_registration_column_5","event_registration_column_6",
            "event_registration_column_7","event_registration_column_8",
            "event_registration_column_9","event_registration_column_10");
        
        $column_mandatory_array = array("event_reg_col_1_mandatory_flag","event_reg_col_2_mandatory_flag","event_reg_col_3_mandatory_flag",
            "event_reg_col_4_mandatory_flag","event_reg_col_5_mandatory_flag","event_reg_col_6_mandatory_flag",
            "event_reg_col_7_mandatory_flag","event_reg_col_8_mandatory_flag",
            "event_reg_col_9_mandatory_flag","event_reg_col_10_mandatory_flag");
        
        $old_location_id = $old_location_id-1;
        $new_location_id = $new_location_id-1;
        
        $get_event_column = sprintf("select $column_array[$old_location_id],$column_mandatory_array[$old_location_id] from `event` where `event_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
        $get_event_result = mysqli_query($this->db, $get_event_column);
        if (!$get_event_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_column");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($get_event_result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($get_event_result);
                $event_old_location_value = $row[$column_array[$old_location_id]];
                $event_old_location_mandatory_value = $row[$column_mandatory_array[$old_location_id]];
            }else{
                $error = array('status' => "Failed", "msg" => "Event details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }
        
        $get_event_reg_column = sprintf("select $column_array[$old_location_id] from `event_registration` where `event_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
        $get_event_reg_result = mysqli_query($this->db, $get_event_reg_column);
        if (!$get_event_reg_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_reg_column");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($get_event_reg_result);
            if ($num_rows1 > 0) {
                $row = mysqli_fetch_assoc($get_event_reg_result);
                $event_reg_old_location_value = $row[$column_array[$old_location_id]];
            } 
//            else {
//                $error = array('status' => "Failed", "msg" => "Event registration details doesn't exist.");
//                $this->response($this->json($error), 204);
//            }
        }
        
        if ($old_location_id < $new_location_id) {
            for ($i = $old_location_id; $i < $new_location_id; $i++) {               
                $j = $i + 1;
                $column_string .= "$column_array[$i] = $column_array[$j] , ";
                $column_mandatory_string .= "$column_mandatory_array[$i] = $column_mandatory_array[$j] , ";
            }
        } else {          
            for ($i = $old_location_id; $i > $new_location_id; $i--) {  
                $j = $i - 1;
                $column_string .= "$column_array[$i] = $column_array[$j] , "; 
                $column_mandatory_string .= "$column_mandatory_array[$i] = $column_mandatory_array[$j] , ";
            }
        }

        $update_event = sprintf("UPDATE `event` SET $column_string $column_mandatory_string $column_array[$new_location_id]='%s', $column_mandatory_array[$new_location_id]='%s' WHERE `company_id`='%s' AND `event_id`='%s'", mysqli_real_escape_string($this->db, $event_old_location_value), mysqli_real_escape_string($this->db, $event_old_location_mandatory_value), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
        $result = mysqli_query($this->db, $update_event);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {

            $update_event_registration = sprintf("UPDATE `event_registration` SET $column_string $column_array[$new_location_id]='%s' WHERE `company_id`='%s' AND `event_id`='%s'", mysqli_real_escape_string($this->db, $event_reg_old_location_value), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
            $result = mysqli_query($this->db, $update_event_registration);

            //Print Company details after login success
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_registration");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $get_event = sprintf("select `parent_id`, `event_type` from `event` where `event_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
                $get_result = mysqli_query($this->db, $get_event);
                if (!$get_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_rows = mysqli_num_rows($get_result);
                    if ($num_rows > 0) {
                        $row = mysqli_fetch_assoc($get_result);
                        $event_type = $row['event_type'];
                        $parent_id = $row['parent_id'];
                    }
                }

                if (empty(trim($parent_id))) {
                    $return_event_id = $event_id;
                } else {
                    $return_event_id = $parent_id;
                }
                $this->getEventsDetailByEventId($company_id, $return_event_id, $event_type, $list_type, 0);
            }
        }
    }

    //Update Events
    public function  updateEventDetails($event_id,$company_id,$event_type,$event_start_datetime,$event_end_datetime,$event_title,$event_category_subtitle,$event_desc,$event_video_detail_url,$event_cost,$event_compare_price,$event_banner_img_url,$event_status,$parent_id,$event_image_update,$event_sort_order,$list_type,$old_event_banner_img_url,$event_capacity,$capacity_text,$event_show_status) {
        $sort_order_value = '';
        if(($event_type=='M' || $event_type=='S') && $event_status=='P' && $list_type!='live'){
            $sort_order_value = ", `event_sort_order` = NextVal('event_sort_order_seq')";
        }
        
        if(empty(trim($event_compare_price))){      
            $event_compare_price = 'null';   
        }
        
        if($event_capacity==''){
            $event_capacity='NULL';
        }
        if ($event_image_update === 'Y') {
            $update_event = sprintf("UPDATE `event` SET `event_begin_dt`='%s', `event_end_dt`='%s', `event_title`='%s',  `event_category_subtitle`='%s', `event_desc`='%s', `event_video_detail_url`='%s', `event_cost`='%s', `event_compare_price`=%s, `event_banner_img_url`='%s', `event_status`='%s', `event_show_status`='%s', `event_capacity`=%s, `capacity_text`='%s' $sort_order_value WHERE `event_id`='%s' and `company_id`='%s'", $event_start_datetime, $event_end_datetime, mysqli_real_escape_string($this->db,$event_title), mysqli_real_escape_string($this->db,$event_category_subtitle), mysqli_real_escape_string($this->db,$event_desc), mysqli_real_escape_string($this->db,$event_video_detail_url), $event_cost, $event_compare_price, mysqli_real_escape_string($this->db,$event_banner_img_url), $event_status, $event_show_status, mysqli_real_escape_string($this->db,$event_capacity), mysqli_real_escape_string($this->db,$capacity_text), $event_id, $company_id);
        } else {
            $update_event = sprintf("UPDATE `event` SET `event_begin_dt`='%s', `event_end_dt`='%s', `event_title`='%s',  `event_category_subtitle`='%s', `event_desc`='%s', `event_video_detail_url`='%s', `event_cost`='%s', `event_compare_price`=%s, `event_status`='%s', `event_show_status`='%s', `event_capacity`=%s, `capacity_text`='%s' $sort_order_value WHERE `event_id`='%s' and `company_id`='%s'", $event_start_datetime, $event_end_datetime, mysqli_real_escape_string($this->db,$event_title), mysqli_real_escape_string($this->db,$event_category_subtitle), mysqli_real_escape_string($this->db,$event_desc), mysqli_real_escape_string($this->db,$event_video_detail_url), $event_cost, $event_compare_price, $event_status, $event_show_status, mysqli_real_escape_string($this->db,$event_capacity), mysqli_real_escape_string($this->db,$capacity_text), $event_id, $company_id);
        }
        $result = mysqli_query($this->db, $update_event);
        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if($event_image_update==='Y' && !empty($old_event_banner_img_url) && strpos($old_event_banner_img_url, 'default.png') === false){
                $cfolder_name = "Company_$company_id";
                $folder_name = "Events";
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/$cfolder_name/$folder_name";
                chdir($dir_name);
                $temp_old_img_url = stripslashes($old_event_banner_img_url);
                $split = explode("$cfolder_name/$folder_name/",$temp_old_img_url);
                $filename = $split[1];                
                if(file_exists($filename)){
                    unlink($filename);
                }
                chdir($old); // Restore the old working directory
            }
            if($event_type=='M'){
                $update_child_status = sprintf("UPDATE `event` SET `event_status`='%s' WHERE `parent_id`='%s' AND `company_id`='%s' AND `event_status`!='D'", mysqli_real_escape_string($this->db,$event_status), mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$company_id));
                $result_child_status = mysqli_query($this->db, $update_child_status);
                if (!$result_child_status) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_status");
                    log_info($this->json($error_log));
                }
            }
            if(!empty(trim($parent_id))){
                $return_event_id = $parent_id;
            }else{
                $return_event_id = $event_id;
            }
            $this->getEventsDetailByEventId($company_id,$return_event_id,$event_type, $list_type, 0);
        }
    }
    
    protected function sendEmail($to, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file,$cc_email_list,$company_id,$type,$footer_detail) {
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }            
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            if (!empty($company_id) && $type != 'subscribe') {
                $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                $curr_loc = implode('/', explode('/', $curr_loc, -4));
//                $tomail = $to;
                if (!empty(trim($cc_email_list))) {
                    $tomail = base64_encode($to . "," . "$cc_email_list");
                } else {
                    $tomail = base64_encode($to);
                }
                $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/EmailSubscription.php?mail=$tomail?comp_id=$company_id?type=U";
                $message = "
            <html>
            <body>
                <p>$message</p><br><br>
                    <footer style='font-size: 13px;color: #746f6f;'>
                    <center>
                    <p>$footer_detail
                    </p>
                    <a href='$activation_link' target='_blank' style='color: #746f6f; !important;text-decoration: none !important;'><u style='color: #746f6f;'>Unsubscribe $to</u></a>
                    </center>
                    </footer>
            </body>
            </html>
            ";
            }
            $mail->Body = $message;
            if(!empty($file_name)){
                $mail->AddStringAttachment(base64_decode($attached_file), $file_name, 'base64');
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                         $verify_email_for_bounce = $this->verifyBouncedEmailForCC($cc_addresses[$init], $company_id);
                         if($verify_email_for_bounce==1){
                               $mail->AddCC($cc_addresses[$init]);
                         }
                    }
                }
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    public function updateEventbriteDetails($company_id,$eventbrite_id) {

        $sql = sprintf("UPDATE `company` SET `eventbrite_id`='%s' WHERE `company_id`='%s'", $eventbrite_id, $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
           $out = array('status' => "Success", 'msg' => "Eventbrite id updated successfully.");
                // If success everythig is good send header as "OK" and user details
           $this->response($this->json($out), 200);
        }
    }
    
    //Get Eventbrite details by company
    public function getEventbriteDetails($company_id) {

        $sql = sprintf("SELECT `eventbrite_id` FROM `company` WHERE company_id = '%s'", $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output = $row;
                }
                $out = array('status' => "Success", 'msg' => $output);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Eventbrite details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
    public function updateEventSortingDetails($event_sort_order, $company_id, $event_id){
            
        $update_message = sprintf("UPDATE `event` SET `event_sort_order`='%s' WHERE `event_id`='%s' AND `company_id`='%s'", $event_sort_order, $event_id, $company_id);
        $result = mysqli_query($this->db, $update_message);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => $update_message);
            $this->response($this->json($error), 200);
        }
        $out = array('status' => "Success", 'msg' => 'Successfully updated');
                // If success everythig is good send header as "OK" and user details
        $this->response($this->json($out), 200);
    }
    
    public function sendEmailForEvent($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list,$student_cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if(!empty(trim($student_cc_email_list))){
                $student_cc_addresses = explode(',', $student_cc_email_list);
                for($init_1=0;$init_1<count($student_cc_addresses);$init_1++){
                    if(filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($student_cc_addresses[$init_1]);
                    }
                }
            }
            
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    public function wepayStatus($company_id) {
        $query = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if (!empty(trim($curr))) {
                    $currency = explode(",", trim($curr));
                }
                if ($row['account_state'] == 'active') {
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                } else {
                    $res = array('status' => "Success", "msg" => "A", "info" => "Deleted/Disabled/Action needed.");
                }
            } else {
                $res = array('status' => "Success", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }

    public function wepayTokenGeneration($company_id, $company_name, $company_desc, $code, $redirect_uri, $country) {
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg4=array("buyer_email"=>$company_id,"membership_title"=>$company_name,"gmt_date"=>$gmt_date);
        $user_id = $access_token = $first_name = $last_name = $user_name = $email = $account_id = $account_state = $action_reasons = $disabled_reasons = '';
        $account_address = $account_phone = $account_mail = [];
        $website = $url = $social = '';
        $query_studio_details = sprintf("SELECT * FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_studio_details = mysqli_query($this->db, $query_studio_details);
        if (!$result_studio_details) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_studio_details");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $time = time();
            $num_rows = mysqli_num_rows($result_studio_details);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result_studio_details);
                if (!empty(trim($row['postal_code']))) {
                    $temp = [];
                    $account_address['receive_time'] = $time;
                    $account_address['type'] = "address";
                    $account_address['source'] = "user";

                    if (!empty(trim($row['street_address']))) {
                        $temp['address1'] = $row['street_address'];
                    } else {
                        $temp['address1'] = "N/A";
                    }
                    if(!empty(trim($row['city']))){
                        $temp['city']=$row['city'];
                    }else{
                        $temp['city']="N/A";
                    }
                    if(!empty(trim($row['postal_code']))){
                        $temp['zip']=$row['postal_code'];
                    }else{
                        $temp['zip']="N/A";
                    }
                    if (!empty(trim($row['country']))) {
                        if (strtolower($row['country']) == 'ca' || strtolower($row['country']) == 'canada'){
                            $temp['country'] = "CA";
                        } else {
                            $temp['country'] = "US";
                        }
                    }else{
                        $temp['country'] = "US";
                    }
                    $country = $temp['country'];
                    if (!empty(trim($row['web_page']))) {
                        $website = $row['web_page'];
                    }
                    if (!empty(trim($row['facebook_url']))) {
                        $social = "facebook";
                        $url = $row['facebook_url'];
                    } elseif (!empty(trim($row['twitter_url']))) {
                        $social = "twitter";
                        $url = $row['twitter_url'];
                    }
                    $account_address['properties']['address'] = $temp;
                }
                if (!empty(trim($row['phone_number']))) {
                    $account_phone['receive_time'] = $time;
                    $account_phone['type'] = "phone";
                    $account_phone['source'] = "user";
                    $account_phone['properties']['phone'] = $row['phone_number'];
                    $account_phone['properties']['type'] = "work";
                } else {
                    $account_phone['receive_time'] = $time;
                    $account_phone['type'] = "phone";
                    $account_phone['source'] = "user";
                    $account_phone['properties']['phone'] = "N/A";
                    $account_phone['properties']['type'] = "work";
                }
                if (!empty(trim($row['email_id']))) {
                    $account_mail['receive_time'] = $time;
                    $account_mail['type'] = "email";
                    $account_mail['source'] = "user";
                    $account_mail['properties']['email'] = $row['email_id'];
                } else {
                    $account_mail['receive_time'] = $time;
                    $account_mail['type'] = "email";
                    $account_mail['source'] = "user";
                    $account_mail['properties']['email'] = "N/A";
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Studio details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }

        $temp_password = $this->wp_pswd;
        $method = 'aes-256-cbc';
        // Must be exact 32 chars (256 bit)
        $password = substr(hash('sha256', $temp_password, true), 0, 32);
        // IV must be exact 16 chars (128 bit)
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);


//// My secret message 1234
//$decrypted = openssl_decrypt(base64_decode($encrypted), $method, $password, OPENSSL_RAW_DATA, $iv);

        if (!function_exists('getallheaders')) {
            if (!function_exists('apache_request_headers')) {
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            } else {
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
            }
        } else {
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
        }
//        $ip = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR');


        $json = array("client_id" => "$this->wp_client_id", "client_secret" => "$this->wp_client_secret", "code" => "$code", "redirect_uri" => "$redirect_uri");
        $postData = json_encode($json);
        $sns_msg4['call'] = "OAuth Token";
        $sns_msg4['type'] = "Token generation";
        $response = $this->wp->accessWepayApi("oauth2/token", 'POST', $postData, '', $user_agent,$sns_msg4);
        $this->wp->log_info("wepay_oauth2_token: " . $response['status']);
        if ($response['status'] == "Success") {
            $user_id = $response['msg']['user_id'];
            $access_token = $response['msg']['access_token'];
            if (!empty($user_id)) {
                $sns_msg4['call'] = "User";
                $sns_msg4['type'] = "User details";
                $response2 = $this->wp->accessWepayApi("user", 'GET', '', $access_token, $user_agent,$sns_msg4);
                $this->wp->log_info("wepay_getuser: " . $response2['status']);
                if ($response2['status'] == "Success") {
                    $res = $response2['msg'];
                    $first_name = $res['first_name'];
                    $last_name = $res['last_name'];
                    $user_name = $res['user_name'];
                    $email = $res['email'];
                    $user_state = $res['state'];
                    $user_callback_url = $this->server_url."/Wepay2/updateUser";
                    $json1 = array("callback_uri"=>"$user_callback_url");
                    $postData1 = json_encode($json1);
                    $sns_msg4['call'] = "User";
                    $sns_msg4['type'] = "Updating user details";
                    $resp = $this->wp->accessWepayApi("user/modify", 'POST', $postData1, $access_token, $user_agent,$sns_msg4);
                    $this->wp->log_info("wepay_user_modify: " . $resp['status']);
                } else {
                    $log = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                    $this->response($this->json($log), 200);
                }
            }

            $acc_callback_url = $this->server_url."/Wepay2/updateAccount";
            if (strtolower($country) == "canada" || strtolower($country) == "ca") {
                $json3 = array("name" => "$company_name", "description" => "$company_desc", "callback_uri" => "$acc_callback_url", "country_options" => array("debit_opt_in" => true), "currencies" => array("CAD"));
            } else {
                $json3 = array("name" => "$company_name", "description" => "$company_desc", "callback_uri" => "$acc_callback_url");
            }
            $postData3 = json_encode($json3);
            $sns_msg4['call'] = "Account";
            $sns_msg4['type'] = "Account Creation";
            $response3 = $this->wp->accessWepayApi("account/create", 'POST', $postData3, $access_token, $user_agent,$sns_msg4);
            $this->wp->log_info("wepay_account_create: " . $response3['status']);
            if ($response3['status'] == "Success") {
                $res = $response3['msg'];
                $account_id = $res['account_id'];
                $account_state = $res['state'];
                $action_reasons = implode(",", $res['action_reasons']);
                $disabled_reasons = implode(",", $res['disabled_reasons']);
                $currency = implode(",", $res['currencies']);
                $this->rbitCallForAccount($access_token, $user_agent, $account_id, $company_name, $time, $account_address, $account_mail, $account_phone);
            } else {
                $log = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                $this->response($this->json($log), 200);
            }

            $encrypted_access_token = base64_encode(openssl_encrypt($access_token, $method, $password, OPENSSL_RAW_DATA, $iv));
            $query = sprintf("INSERT INTO `wp_account`(`company_id`, `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `first_name`, `last_name`, `user_name`, `user_email`, `action_reasons`, `disabled_reasons`, `currency`)
                    VALUE ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $user_id), mysqli_real_escape_string($this->db, $user_state), mysqli_real_escape_string($this->db, $encrypted_access_token),
                    mysqli_real_escape_string($this->db, $account_id), mysqli_real_escape_string($this->db, $account_state), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name),
                    mysqli_real_escape_string($this->db, $user_name), mysqli_real_escape_string($this->db, $email), mysqli_real_escape_string($this->db, $action_reasons), mysqli_real_escape_string($this->db, $disabled_reasons), mysqli_real_escape_string($this->db, $currency));
            $result = mysqli_query($this->db, $query);

            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
           $log = array("status" => "Success", "msg" => "Wepay user account created successfully.");
            $this->response($this->json($log),200); 
        }else{
            $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
            $this->response($this->json($log),200);
        }
    }
    
    public function wepayUserTokenGeneration($company_id, $user_first_name, $user_last_name, $user_email, $type) {
        $user_id = $access_token = $first_name = $last_name = $user_name = $email = $account_id = $account_state = $action_reasons = $disabled_reasons = $country = $company_name = $contact_id = $list_item_id = '';
        $account_address = $account_phone = $account_mail = [];
        $website = $url = $social = $currency_code = $currency_symbol = '';
        $gmt_date = gmdate(DATE_RFC822);
        $sns_msg5 = array("buyer_email" => $company_id, "membership_title" => $user_email, "gmt_date" => $gmt_date);
        if (!function_exists('getallheaders')){
            if(!function_exists ('apache_request_headers')){
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            }else{
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
            }
        }else{
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
        }
        $ip = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR');   
        
        $time = time();
        $temp_password = $this->wp_pswd;
        $method = 'aes-256-cbc';
        // Must be exact 32 chars (256 bit)
        $password = substr(hash('sha256', $temp_password, true), 0, 32);
        // IV must be exact 16 chars (128 bit)
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        
        $query_check_wepay = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result_check_wepay = mysqli_query($this->db, $query_check_wepay);
        if(!$result_check_wepay){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_check_wepay");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result_check_wepay)>0){
                $error = array('status' => "Failed", "msg" => "Wepay user account details already available to this studio. Please contact administrator to reset Wepay account.");
                $this->response($this->json($error), 200);
            }
        }
        if($type=='L'){
            $query_check_email = sprintf("SELECT * FROM `wp_account` WHERE `user_email`='%s' ORDER BY `wp_account_id` DESC limit 0,1", mysqli_real_escape_string($this->db, $user_email));
            $result_check_mail = mysqli_query($this->db, $query_check_email);
            if(!$result_check_mail){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_check_email");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                $numrows = mysqli_num_rows($result_check_mail);
                if($numrows>0){
                    $row = mysqli_fetch_assoc($result_check_mail);
//                    if($row['first_name']==$user_first_name && $row['last_name']==$user_last_name){
                        if($row['company_id']==$company_id){
                            $error = array('status' => "Failed", "msg" => "Same Wepay user details available to this studio.");
                            $this->response($this->json($error), 200);
                        }else{
                            $user_id = $row['wp_user_id'];
                            $token = $row['access_token'];
                            $access_token = openssl_decrypt(base64_decode($token), $method, $password, OPENSSL_RAW_DATA, $iv);
                        }
//                    }else{
//                        $error = array('status' => "Failed", "msg" => "Wepay user details mismatched.");
//                        $this->response($this->json($error), 200);
//                    }
                }else{
                    $error = array('status' => "Failed", "msg" => "No user account created via Mystudio app.");
                    $this->response($this->json($error), 200);
                }
            }
        }else{
            $json = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","email"=>"$user_email","scope"=>"manage_accounts,collect_payments,view_user,send_money,preapprove_payments",
                "first_name"=>"$user_first_name","last_name"=>"$user_last_name","original_ip"=>"$ip","original_device"=>"$user_agent","tos_acceptance_time"=>$time);
            $postData = json_encode($json);            
            $sns_msg5['call'] = "User";
            $sns_msg5['type'] = "User registration";
            $response = $this->wp->accessWepayApi("user/register",'POST',$postData,'',$user_agent,$sns_msg5);
            $this->wp->log_info("wepay_user_create: ".$response['status']);
            if($response['status'] == "Success"){
                $res = $response['msg'];
                $user_id = $res['user_id'];
                $access_token = $res['access_token'];
                if(isset($res['expires_in']) && !empty($res['expires_in'])){
                    $json5 = array("email_subject"=>"MyStudio - Verify your WePay account");
                    $postData5 = json_encode($json5);
                    $sns_msg5['call'] = "User";
                    $sns_msg5['type'] = "Sending User confirmation email";
                    $response5 = $this->wp->accessWepayApi("user/send_confirmation",'POST',$postData5,$access_token,$user_agent,$sns_msg5);
                    if($response5['status'] == "Success"){

                    }else{
                        $log = array("status" => "Failed", "msg" => $response5['msg']['error_description']);
                        $this->response($this->json($log),200);
                    }
                }
            }else{
                $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
                $this->response($this->json($log),200);
            }
        }     
        
        
//        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
        
        $query_studio_details = sprintf("SELECT * FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_studio_details = mysqli_query($this->db, $query_studio_details);
        if(!$result_studio_details){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_studio_details");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $time = time();
            $num_rows = mysqli_num_rows($result_studio_details);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result_studio_details);
                $company_name = $row['company_name'];
                $contact_id = $row['contact_id'];
                $list_item_id = $row['list_item_id'];
                $currency_code = $row['wp_currency_code'];
                $currency_symbol = $row['wp_currency_symbol'];
                if(!empty(trim($row['country']))){
                    if (strtolower($row['country']) == 'ca' || strtolower($row['country']) == 'canada'){
                        $country="CA";
                    }elseif (strtolower($row['country']) == 'uk' || strtolower($row['country']) == 'gb' || strtolower($row['country']) == 'united kingdom'){
                        $country="GB";
                    }else{
                        $country="US";
                    }
                }else{
                    $country = "US";
                }
                if(!empty(trim($row['postal_code']))){
                    $temp=[];
                    $account_address['receive_time'] = $time;
                    $account_address['type'] = "address";
                    $account_address['source'] = "user";
                    if(!empty(trim($row['street_address']))){
                        $temp['address1']=$row['street_address'];
                    }else{
                        $temp['address1']="N/A";
                    }
                    if(!empty(trim($row['city']))){
                        $temp['city']=$row['city'];
                    }else{
                        $temp['city']="N/A";
                    }
                    if(!empty(trim($row['postal_code']))){
                        $temp['zip']=$row['postal_code'];
                    }else{
                        $temp['zip']="N/A";
                    }
                    $temp['country'] = $country;
                    if(!empty(trim($row['web_page']))){
                        $website = $row['web_page'];
                    }
                    if(!empty(trim($row['facebook_url']))){
                        $social = "facebook";
                        $url = $row['facebook_url'];
                    }elseif(!empty(trim($row['twitter_url']))){
                        $social = "twitter";
                        $url = $row['twitter_url'];
                    }
                    $account_address['properties']['address'] = $temp;
                }
                if(!empty(trim($row['phone_number']))){
                    $account_phone['receive_time'] = $time;
                    $account_phone['type'] = "phone";
                    $account_phone['source'] = "user";
                    $account_phone['properties']['phone'] = $row['phone_number'];
                    $account_phone['properties']['type'] = "work";
                }else{
                    $account_phone['receive_time'] = $time;
                    $account_phone['type'] = "phone";
                    $account_phone['source'] = "user";
                    $account_phone['properties']['phone'] = "N/A";
                    $account_phone['properties']['type'] = "work";
                }
                if(!empty(trim($row['email_id']))){
                    $account_mail['receive_time'] = $time;
                    $account_mail['type'] = "email";
                    $account_mail['source'] = "user";
                    $account_mail['properties']['email'] = $row['email_id'];
                }else{
                    $account_mail['receive_time'] = $time;
                    $account_mail['type'] = "email";
                    $account_mail['source'] = "user";
                    $account_mail['properties']['email'] = "N/A";
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Studio details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }

//// My secret message 1234
//$decrypted = openssl_decrypt(base64_decode($encrypted), $method, $password, OPENSSL_RAW_DATA, $iv);

        
                    
//        $json = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","code"=>"$code","redirect_uri"=>"$redirect_uri");
//        $postData = json_encode($json);
//        $response = $this->wp->accessWepayApi("oauth2/token",'POST',$postData,'',$user_agent);
//        $this->wp->log_info("wepay_oauth2_token: ".$response['status']);
//        if($response['status'] == "Success"){
//            $user_id = $response['msg']['user_id'];
//            $access_token = $response['msg']['access_token'];
//            if(!empty($user_id)){
                $sns_msg5['call'] = "User";
                $sns_msg5['type'] = "User details";
                $response2 = $this->wp->accessWepayApi("user",'GET','',$access_token,$user_agent,$sns_msg5);
                $this->wp->log_info("wepay_getuser: ".$response2['status']);
                if($response2['status'] == "Success"){
                    $res = $response2['msg'];
                    $first_name = $res['first_name'];
                    $last_name = $res['last_name'];
                    $user_name = $res['user_name'];
                    $email = $res['email'];
                    $user_state = $res['state'];
                    $user_callback_url = $this->server_url."/Wepay2/updateUser";
                    $json1 = array("callback_uri"=>"$user_callback_url");
                    $postData1 = json_encode($json1);
                    $sns_msg5['call'] = "User";
                    $sns_msg5['type'] = "Updating User details";
                    $resp = $this->wp->accessWepayApi("user/modify",'POST',$postData1,$access_token,$user_agent,$sns_msg5);
                    $this->wp->log_info("wepay_user_modify: ".$resp['status']);
                }else{
                    $log = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                    $this->response($this->json($log),200);
                }
//            }
            
            $acc_callback_url = $this->server_url."/Wepay2/updateAccount";
            if(strtolower($country)=="canada" || strtolower($country)=="ca"){
                $json3 = array("name"=>"$company_name","description"=>"$company_name","callback_uri"=>"$acc_callback_url","country"=>"CA","country_options"=>array("debit_opt_in"=> true),"currencies"=>array("$currency_code"));
            }elseif(strtolower($row['country']) == 'uk' || strtolower($row['country']) == 'gb' || strtolower($row['country']) == 'united kingdom'){
                $json3 = array("name"=>"$company_name","description"=>"$company_name","callback_uri"=>"$acc_callback_url","country"=>"GB","currencies"=>array("$currency_code"));
            }else{
                $json3 = array("name"=>"$company_name","description"=>"$company_name","callback_uri"=>"$acc_callback_url");
            }
            $postData3 = json_encode($json3);
            $sns_msg5['call'] = "Account";
            $sns_msg5['type'] = "Account Creation";
            $response3 = $this->wp->accessWepayApi("account/create",'POST',$postData3,$access_token,$user_agent,$sns_msg5);
            $this->wp->log_info("wepay_account_create: ".$response3['status']);
            if($response3['status'] == "Success"){
                $res = $response3['msg'];
                $account_id = $res['account_id'];
                $account_state = $res['state'];
                $action_reasons = implode(",", $res['action_reasons']);
                $disabled_reasons = implode(",", $res['disabled_reasons']);
                $currency = implode(",", $res['currencies']);
                $this->rbitCallForAccount($access_token, $user_agent, $account_id, $company_name, $time, $account_address, $account_mail, $account_phone, $website, $social, $url);
            }else{
                $log = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                $this->response($this->json($log),200);
            }
            
            $encrypted_access_token = base64_encode(openssl_encrypt($access_token, $method, $password, OPENSSL_RAW_DATA, $iv));
            $query = sprintf("INSERT INTO `wp_account`(`company_id`, `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `first_name`, `last_name`, `user_name`, `user_email`, `action_reasons`, `disabled_reasons`, `currency`)
                    VALUE ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $user_id), mysqli_real_escape_string($this->db, $user_state), mysqli_real_escape_string($this->db, $encrypted_access_token),
                    mysqli_real_escape_string($this->db, $account_id), mysqli_real_escape_string($this->db, $account_state), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name),
                    mysqli_real_escape_string($this->db, $user_name), mysqli_real_escape_string($this->db, $email), mysqli_real_escape_string($this->db, $action_reasons), mysqli_real_escape_string($this->db, $disabled_reasons), mysqli_real_escape_string($this->db, $currency));
            $result = mysqli_query($this->db, $query);
            
            if(!$result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
            
            if(!empty($account_state) && $account_state!="disabled" && $account_state!="deleted"){
                if($account_state=="pending"){
                    $status = 'P';
                }else{
                    $status = 'Y';
                }
                $update_company = sprintf("UPDATE `company` SET `wepay_status`='%s' WHERE `company_id`='%s'", $status, mysqli_real_escape_string($this->db, $company_id));
                $result_update_company = mysqli_query($this->db, $update_company);
                if(!$result_update_company){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                if(!empty($list_item_id)&&!empty($contact_id)){
                    $wp_status = "1";
                    $json = array("contactIds"=>array("$contact_id"),"fieldValues"=>array("24"=>array(array("raw"=>"$wp_status"))));
                    $postData = json_encode($json);
                    $sf_response = $this->sf->accessSalesforceApi("lists/$this->list_id/listitems/$list_item_id",'PUT',$postData);
                    log_info("updateCompanyDetailsinSalesforceApi_ListItemUpdation: ".$sf_response['status']);
                }
            }
            $log = array("status" => "Success", "msg" => "Wepay user account created successfully. Please check your email for the confirmation to link your bank account.");
            $this->response($this->json($log),200); 
//        }else{
//            $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//            $this->response($this->json($log),200);
//        }
    }
    
    public function rbitCallForAccount($token, $user_agent, $account_id, $company_name, $time, $account_address, $account_mail, $account_phone, $website, $social, $url) {
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg6=array("buyer_email"=>$account_id,"membership_title"=>$company_name,"gmt_date"=>$gmt_date,"call"=>"Account","type"=>"rbit");
        if (empty($account_address) && empty($account_mail) && empty($account_phone)) {
            $error_log = array('status' => "Failed", "msg" => "Studio details are empty.");
            log_info($this->json($error_log));
        } else {
            for ($i = 1; $i < 5; $i++) {
                $json = [];
                $postData = '';
                switch ($i) {
                    case 1:
                        $json['associated_object_type'] = "account";
                        $json['associated_object_id'] = $account_id;
                        $json['receive_time'] = $time;
                        $json['type'] = "person";
                        $json['source'] = "user";
                        $json['properties']['name'] = "$company_name";
                        $temp = [];
                        if (!empty($account_address)) {
                            array_push($temp, $account_address);
                        }
                        if (!empty($account_mail)) {
                            array_push($temp, $account_mail);
                        }
                        if (!empty($account_phone)) {
                            array_push($temp, $account_phone);
                        }
                        $json['related_rbits'] = $temp;
                        break;
                    case 2:
                        $json = array("associated_object_type" => "account", "associated_object_id" => $account_id, "receive_time" => $time, "type" => "business_name", "source" => "user", "properties" => array("business_name" => "$company_name"));
                        break;
                    case 3:
                        $json = array("associated_object_type" => "account", "associated_object_id" => $account_id, "receive_time" => $time, "type" => "business_description", "source" => "user", "properties" => array("business_description" => "Martial arts studio"));
                        break;
                    case 4:
                        if (!empty(trim($website))) {
                            $json = array("associated_object_type" => "account", "associated_object_id" => $account_id, "receive_time" => $time, "type" => "external_account", "source" => "user",
                                "properties" => array("is_partner_account" => "no", "account_type" => "$company_name", "uri" => "$website"));
                        } else {
                            continue 2;
                        }
                        break;
//                    case 5:
//                        if (!empty(trim($url))) {
//                            $json = array("associated_object_type" => "account", "associated_object_id" => $account_id, "receive_time" => $time, "type" => "external_account", "source" => "user",
//                                "properties" => array("is_partner_account" => "no", "account_type" => "$social", "uri" => "$url"));
//                        } else {
//                            continue 2;
//                        }
//                        break;
                    default :
                        break;
                }

                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi("rbit/create", 'POST', $postData, $token, $user_agent,$sns_msg6);
                $this->wp->log_info("wepay_rbit_create: " . $response['status']);
                if ($response['status'] == "Success") {
                    continue;
                } else {
                    $error = array("status" => "Failed", "msg" => $response['msg']['error_description']);
                    $this->response($this->json($error), 200);
                }
            }
        }
    }
    
//    public function wepayCreateCheckout($company_id, $event_id, $parent_id, $stud_id, $student_name, $event_name, $desc, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity){
//        $user_timezone = $this->getUserTimezone($company_id);
//        $curr_time_zone = date_default_timezone_get();
//        $new_timezone=$user_timezone['timezone'];
//        date_default_timezone_set($new_timezone);
////        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
//        
//        $student_id = $stud_id;
//        if($stud_id==0){
//            $name = $reg_col1." ".$reg_col2;
//            if(!empty($student_name)){
//                $name = $student_name;
//            }
//            $squery = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' order by `last_login_dt` desc limit 0,1",  mysqli_real_escape_string($this->db, $buyer_email));
//            $sresult = mysqli_query($this->db, $squery);
//            
//            if(!$sresult){
//                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$squery");
//                log_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                $this->response($this->json($error), 200);
//            }else{
//                if(mysqli_num_rows($sresult)>0){
//                    $row = mysqli_fetch_assoc($sresult);
//                    $student_id = $row['student_id'];
////                    $error = array('status' => "Failed", "msg" => "Student already registered.");
////                    $this->response($this->json($error), 200);
//                }else{
//                    $iquery = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_email`) VALUES ('%s','%s','%s')", mysqli_real_escape_string($this->db,$company_id),
//                            mysqli_real_escape_string($this->db,$name), mysqli_real_escape_string($this->db,$buyer_email));
//                    $iresult = mysqli_query($this->db, $iquery);
//                    
//                    if(!$iresult){
//                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$iquery");
//                        log_info($this->json($error_log));
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                        $this->response($this->json($error), 200);
//                    }else{
//                        $student_id = mysqli_insert_id($this->db);
//                    }
//                }
////                if($stud_id==$student_id){
////                    $error = array('status' => "Failed", "msg" => "Student cannot be added at this time, try again later.");
////                    $this->response($this->json($error), 200);
////                }
//            }
//        }
//        if(empty($parent_id)){
//            $reg_event = $event_id;
//            $reg_parent = 'null';
//        }else{
//            $reg_event = $parent_id;
//            $reg_parent = $parent_id;
//        }
//        
//        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency` FROM `wp_account` WHERE `company_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id));
//        $result1 = mysqli_query($this->db, $query1);
//        if(!$result1){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//        }else{
//            $num_rows1 = mysqli_num_rows($result1);
//            if($num_rows1>0){
//                $row = mysqli_fetch_assoc($result1);
//                $access_token = $row['access_token'];
//                $account_id = $row['account_id'];
//                $curr_db = $row['currency'];
//                $user_state = $row['wp_user_state'];
//                $acc_state = $row['account_state'];
//                if($user_state=='deleted'){
//                    $log = array("status" => "Failed", "msg" => "You may delete your account yourselves, or the account may be deleted by WePay customer support.");
//                    $this->response($this->json($log),200);
//                }
//                
//                if(!empty($access_token) && !empty($account_id)){
//                    if (!function_exists('getallheaders')){
//                        if(!function_exists ('apache_request_headers')){
//                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
//                        }else{
//                            $headers = apache_request_headers();
//                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
//                        }
//                    }else{
//                        $headers = getallheaders();
//                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
//                    }
//                    $password = $this->wp_pswd;
//                    $method = 'aes-256-cbc';
//                    // Must be exact 32 chars (256 bit)
//                    $password = substr(hash('sha256', $password, true), 0, 32);
//                    // IV must be exact 16 chars (128 bit)
//                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
//        
//                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
//                    
//                    $json = array("account_id"=>"$account_id");
//                    $postData = json_encode($json);
//                    $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent);
//                    $this->wp->log_info("wepay_get_account: ".$response['status']);
//                    if($response['status']=='Success'){
//                        $res = $response['msg'];
//                        $account_state = $res['state'];
//                        $currency = $res['currencies'][0];
//                        $curr_acc = implode(",", $res['currencies']);
//                        $disabled_reasons = implode(",", $res['disabled_reasons']);
//                        if($account_state=='deleted'){
//                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
//                            $this->response($this->json($log),200);
//                        }elseif($account_state=='disabled'){
//                            $log = array("status" => "Failed", "msg" => "The account has been disabled for following reason(s) - ".$disabled_reasons." by WePay and can no longer accept payments.");
//                            $this->response($this->json($log),200);
//                        }
//                    }else{
//                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                        $this->response($this->json($log),200);
//                    }
//                    
//                    $gross = 0;
//                    $checkout_id = $checkout_state = '';
//                    if($payment_amount>0){
//                        $checkout_callback_url = $this->server_url."/Wepay/updateCheckout";
//                        $unique_id = $company_id."_".$event_id."_".time();
//                        $date = date("Y-m-d_H:i:s");
//                        $ref_id = $company_id."_".$event_id."_".$date;
////                        if($processing_fee_type==2){
////                            $fee_payer = "payer_from_app";
////                        }elseif($processing_fee_type==1){
//                            $fee_payer = "payee_from_app";
////                        }
//                        if($processing_fee_type==2){
//                            $w_payment_amount =$payment_amount + $fee;
//                        }elseif($processing_fee_type==1){
//                            $w_payment_amount = $payment_amount;
//                        }
//                        $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_payment_amount","currency"=>"$currency",
//                            "fee"=>array("app_fee"=>$fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
//                            "email_message"=>array("to_payee"=>"Payment has been added for $event_name","to_payer"=>"Payment has been made for $event_name"),
//                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url");
//                        $postData2 = json_encode($json2);
//                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent);
//                        $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
//                        if($response2['status']=="Success"){
//                            $res2 = $response2['msg'];
//                            $checkout_id = $res2['checkout_id'];
//                            $checkout_state = $res2['state'];
//                            $gross = $res2['gross'];
//                            $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_payment_amount, $currency);
//                        }else{
//                            if($response2['msg']['error_code']==1008){
//                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent);
//                                $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
//                                if($response2['status']=="Success"){
//                                    $res2 = $response2['msg'];
//                                    $checkout_id = $res2['checkout_id'];
//                                    $checkout_state = $res2['state'];
//                                    $gross = $res2['gross'];
//                                    $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_payment_amount, $currency);
//                                }else{
//                                    if($response2['msg']['error_code']==1008){
//                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
//                                        $this->response($this->json($error),200);
//                                    }
//                                }
//                            }else{
//                                if($stud_id==0){
//                                    $error = array("status" => "Failed", "msg" => $response2['msg']['error_description'], "student_id"=>$student_id);
//                                }else{
//                                    $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                                }
//                                $this->response($this->json($error),200);
//                            }
//                        }
//                    }
//                    
//                    $cc_name = $cc_month = $cc_year = '';
//                    $cc_status = $cc_state;
//                    $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
//                    $postData3 = json_encode($json3);
//                    $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent);
//                    $this->wp->log_info("wepay_credit_card: ".$response3['status']);
//                    if($response3['status']=="Success"){
//                        $res3 = $response3['msg'];
//                        $cc_name = $res3['credit_card_name'];
//                        $cc_new_state = $res3['state'];
//                        if($cc_state!=$cc_new_state){
//                            $cc_status = $cc_new_state;
//                        }
//                        $cc_month = $res3['expiration_month'];
//                        $cc_year = $res3['expiration_year'];
//                    }else{
//                        $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
//                        $this->response($this->json($error),200);
//                    }
//                    
//                    
//                    $current_date = date("Y-m-d");
//                    $payment_type = 'CO';
//                    $payment_frequency = 4;
//                    
//                    $query = sprintf("INSERT INTO `event_registration`(`company_id`, `event_id`, `student_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
//                        `payment_type`, `payment_amount`, `paid_amount`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `event_registration_column_1`, `event_registration_column_2`,
//                        `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`, `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`)
//                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_event), mysqli_real_escape_string($this->db, $student_id), 
//                            mysqli_real_escape_string($this->db, $buyer_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code),
//                            mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_type), 
//                            mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name),
//                            mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4),
//                            mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10));
//                    $result = mysqli_query($this->db, $query);
//                    if(!$result){
//                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
//                        log_info($this->json($error_log));
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                        $this->response($this->json($error), 200);
//                    }else{
//                        $event_reg_id = mysqli_insert_id($this->db);
//                        $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_status`, `processing_fee`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')",
//                                mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), 
//                                mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name));
//                        $payment_result = mysqli_query($this->db, $payment_query);
//                        if(!$payment_result){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
//                            log_info($this->json($error_log));
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 200);
//                        }
//                        
//                        $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `student_id`, `event_id`, `event_parent_id`, `event_cost`, `discount`, `payment_amount`, `quantity`) VALUES ('%s','%s','%s',%s,'%s','%s','%s','%s')",
//                                mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $reg_parent), 
//                                mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $quantity));
//                        $reg_det_result = mysqli_query($this->db, $reg_det_query);
//                        if(!$reg_det_result){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
//                            log_info($this->json($error_log));
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 200);
//                        }
//                        
//                        $sales_amount = $payment_amount;
//                        if($processing_fee_type==1){
//                            $sales_amount = $payment_amount-$fee;
//                        }
//                        
//                        $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity, `net_sales` = `net_sales`+$sales_amount WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
//                        $update_event_result = mysqli_query($this->db, $update_event_query);
//                        if(!$update_event_result){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
//                            log_info($this->json($error_log));
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 200);
//                        }
//                        
//                        if($reg_parent != 'null'){
//                            $parent_sales_amount=$payment_amount;
//                            if($processing_fee_type==1){
//                                $parent_sales_amount = $payment_amount-$fee;
//                            }
////                            $update_event_parent = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity, `net_sales` = `net_sales`+$parent_sales_amount WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $reg_parent));
//                            $update_event_parent = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $reg_parent));
//                            $update_event_parent_result = mysqli_query($this->db, $update_event_parent);
//                            if(!$update_event_parent_result){
//                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_parent");
//                                log_info($this->json($error_log));
//                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                $this->response($this->json($error), 200);
//                            }
//                        }
//                        
//                        $log = array("status" => "Success", "msg" => "Registration successful. Confirmation of purchase has been sent to your email.");
//                        $this->responseWithWepay($this->json($log),200);
//                        $this->sendOrderReceiptForEventPayment($company_id,$event_reg_id,0); // 0 - return value off, 1 - return value on
//                    }
//                }
//            }else{
//                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
//                $this->response($this->json($error),200);
//            }
//        }
//        date_default_timezone_set($curr_time_zone);
//    }
    
    public function rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $amount, $currency){
        $time = time();
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg7=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
        $json = array("associated_object_type"=>"checkout","associated_object_id"=>$checkout_id,"receive_time"=>$time,"type"=>"person","source"=>"user","properties"=>array("name"=>"$buyer_name"), 
            "related_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code","country"=>"$country")))));
        $postData = json_encode($json);
        $response = $this->wp->accessWepayApi("rbit/create",'POST',$postData,$token,$user_agent,$sns_msg7);
        $this->wp->log_info("wepay_rbit_create: ".$response['status']);
        if($response['status']=="Success"){
            $json2 = array("associated_object_type"=>"checkout","associated_object_id"=>$checkout_id,"receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$amount, "quantity"=>1, "amount"=>$amount, "currency"=>"$currency"))));
            $postData2 = json_encode($json2);
            $response2 = $this->wp->accessWepayApi("rbit/create",'POST',$postData2,$token,$user_agent,$sns_msg7);
            $this->wp->log_info("wepay_rbit_create: ".$response2['status']);
            if($response2['status']=="Success"){
                
            }else{
                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                $this->response($this->json($error),200);
            }
        }else{
                    
            $error = array("status" => "Failed", "msg" => $response['msg']['error_description']);
            $this->response($this->json($error),200);
        }
    }
    
    public function getEventParticipantDetailsForRefund($company_id, $event_id, $event_reg_id, $call_back){//call_back, 0 -> returned & exit, 1 -> return response
        $query = sprintf("SELECT er.`company_id`, er.`student_id`, er.`event_reg_id`, `event_payment_id`, er.`event_id`, er.`reg_event_type`, concat(`buyer_first_name`,' ',`buyer_last_name`) buyer_name, concat(`event_registration_column_1`, ' ', `event_registration_column_2`) as participant_name, `buyer_email`, `buyer_phone`, `buyer_postal_code`, 
                `registration_amount`, ep.`processing_fee`, `payment_type`, `payment_frequency`, er.`processing_fee_type`, er.`payment_amount` total_due, `paid_amount`, er.`refunded_amount` total_refunded, `credit_card_id`, if(er.`registration_from`='S',er.`stripe_card_name`, er.`credit_card_name`) as `credit_card_name`, `credit_card_status`, `total_order_amount`, `total_order_quantity`, IFNULL(`payment_method`, 'CC') payment_method, IF(`payment_method`='CA', 'Cash', IF(`payment_method`='CH', 'Check', 'Credit card')) payment_method_name, er.check_number reg_check_number,
                ep.`checkout_id`, ep.`checkout_status`, ep.`payment_amount`, ep.`schedule_date`, ep.`schedule_status`, ep.`refunded_amount`, ep.`checkout_status`, ep.credit_method FROM `event_registration` er 
                LEFT JOIN event_payment ep ON er.`event_reg_id`=ep.`event_reg_id` AND ep.`schedule_status` IN ('N', 'S', 'PR', 'CR', 'M', 'MP') 
                WHERE er.`company_id`='%s' AND er.`event_id` in (SELECT IFNULL(`event_parent_id`,`event_id`) FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND er.`event_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));        
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if($call_back==1){
                return $error;
            }else{
                $this->response($this->json($error), 200);
            }
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $pending_state = 0;
                $inital_stage = $paid_processing_fee =  $manual_credit = 0;
                $current_plan_details = $event_details = [];
                $paid_due = $rem_due = $refunded_amt = $total_refunded = 0;
                $payment_id = $payment_status = $type = $event_id_list = $reg_event_type = '';
                while($row = mysqli_fetch_assoc($result)){
                    $buyer_name=$row['buyer_name'];
                    $participant_name=$row['participant_name'];
                    $reg_event_type = $row['reg_event_type'];
                    $payment_type = $row['payment_type'];
                    $fee_type = $row['processing_fee_type'];
                    $credit_card_id = $row['credit_card_id'];
                    $credit_card_name = $row['credit_card_name'];
                    $credit_card_status = $row['credit_card_status'];
                    $event_payment_id = $row['event_payment_id'];
                    $payment_id = $row['checkout_id'];
                    $payment_status = $row['checkout_status'];
                    $event_id_1 = $row['event_id'];
                    $total_refunded = $row['total_refunded'];
                    $refunded_amt += $row['refunded_amount'];
                    $temp=[];
//                    $current_plan_details['total_event'] = $num_of_events;
                    $current_plan_details['payment_type'] = $payment_type;
                    
                    $temp['event_payment_id'] = $event_payment_id;
                    $temp['amount'] = $row['payment_amount'];
                    $temp['date'] = $row['schedule_date'];
                    $temp['edit_status'] = 'N';
                    $temp['process_fee'] = $row['processing_fee'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['credit_method'] = $row['credit_method'];
                    if($row['schedule_status']=='N'){
                        $temp['status'] = 'N';
                    }elseif($row['schedule_status']=='S'){
                        if($row['checkout_status']!='released' && $row['checkout_status']!=''){
                            $pending_state = 1;
                        }
                        $temp['status'] = 'S';
                        $paid_processing_fee += $row['processing_fee'];
                        $inital_stage++;
                    }elseif($row['schedule_status']=='PR'){
                        $temp['status'] = 'S';
                        $paid_processing_fee += $row['processing_fee'];
                        $inital_stage++;
                    }elseif($row['schedule_status']=='M' || $row['schedule_status']=='MP'){
                        $temp['status'] = 'M';
                        $current_plan_details['manual_credit_details'][] = $temp;
                        $manual_credit += $temp['amount']-$temp['refunded_amount'];
                    }
                    $current_plan_details['credit_card_id'] = $credit_card_id;
                    $current_plan_details['credit_card_name'] = $credit_card_name;
                    $current_plan_details['credit_card_status'] = $credit_card_status;
                    $current_plan_details['payment_method'] = $row['payment_method'];
                    $current_plan_details['payment_method_name'] = $row['payment_method_name'];
                    $current_plan_details['reg_check_number'] = $row['reg_check_number'];
                    $current_plan_details['manual_credit'] = $manual_credit;
                    $current_plan_details['total_order_amount'] = $row['total_order_amount'];
                    $current_plan_details['total_order_quantity'] = $row['total_order_quantity'];
                    $current_plan_details['success_count'] = $inital_stage;
                    $current_plan_details['processing_fee_type'] = $fee_type;
                    $current_plan_details['paid_processing_fee'] = $paid_processing_fee;
                    $current_plan_details['total_due'] = $row['total_due'];
                    $current_plan_details['total_refunded'] = $total_refunded;
                    $current_plan_details['paid_due'] = $row['paid_amount'];
                    $current_plan_details['rem_due'] = number_format((float)($row['total_due'] - $row['paid_amount']), 2, '.', '');
                    if($row['schedule_status']!='M' && $row['schedule_status']!='MP' && $row['schedule_status']!='CR'){
                        $current_plan_details['due_details'][] = $temp;
                    }
                }
                $current_plan_details['pending_payment_state'] = $pending_state;
                                
                $get_event_details_query = sprintf("SELECT e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, erd.`event_parent_id`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, erd.`payment_status`, `balance_due`, `manual_payment_amount` FROM `event` e 
                           LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' AND erd.`payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id_1), mysqli_real_escape_string($this->db, $event_id_1), mysqli_real_escape_string($this->db, $event_reg_id));
                $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                if(!$get_event_details_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    if($call_back==1){
                        return $error;
                    }else{
                        $this->response($this->json($error), 200);
                    }
                }else{
                    $num_rows2 = mysqli_num_rows($get_event_details_result);
                    if($num_rows2>0){
                        $temp_quantity = 0;
                        while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                            $temp2 = [];
                            $temp2['event_reg_details_id'] = $row2['event_reg_details_id'];
                               $temp2['ev_id'] = $row2['event_id']; 
                            $temp2['event_parent_id'] = $row2['event_parent_id'];
                            $temp2['title'] = $row2['event_title'];
                            $temp2['event_cost'] = $row2['event_cost'];
                            $temp2['quantity'] = $row2['quantity'];
                            if($row2['payment_amount']==0){
                                $temp2['event_disc'] = $row2['discount'] = $row2['event_cost'];
                            }else{
                                $temp2['event_disc'] = $row2['discount'];
                            }
                            $temp2['discounted_price'] = number_format((float)($row2['payment_amount'] / $row2['quantity']), 2, '.', '');
                            $temp_quantity += $row2['quantity'];
                            $temp2['payment_amount'] = $row2['payment_amount'];
                            $temp2['old_manual_payment_amount'] = $row2['manual_payment_amount'];
                            $temp2['manual_payment_amount'] = $row2['manual_payment_amount'];
                            $temp2['paid_due'] = $temp2['payment_amount'] - $row2['balance_due'];
                            $temp2['old_paid_due'] = $temp2['paid_due'];
                            $temp2['rem_due'] = $row2['balance_due'];
                            $temp2['state'] = 'N';
                            $event_details[] = $temp2;
                        }                        
                        $current_plan_details['total_event'] = $temp_quantity;
                    }else{
                        $error = array("status" => "Failed","event_id" =>$event_id, "msg" => "Participant details doesn't exist.");
                        if($call_back==1){
                            return $error;
                        }else{
                            $this->response($this->json($error),200);
                        }
                    }
                }
            }else{
                $error = array("status" => "Failed","event_id" =>$event_id, "msg" => "Participant details doesn't exist.");
                if($call_back==1){
                    return $error;
                }else{
                    $this->response($this->json($error),200);
                }
            }
        }
        $res = array("status" => "Success", "info" => "payment plan", "msg" => array("event_type" => $reg_event_type,"buyer_name"=> $buyer_name,
                  "participant_name"=>  $participant_name, "current_plan_details" => $current_plan_details, "event_details" => $event_details),"event_id" =>$event_id);
        if($call_back==1){
            return $res;
        }else{
            $this->response($this->json($res),200);
        }
    }
//    
//    public function processSingleEventRegistrationChanges($company_id, $event_reg_id, $reg_event_type, $event_array, $plan_array, $total_due, $cancel_flag) {
//        $user_timezone = $this->getUserTimezone($company_id);
//        $curr_time_zone = date_default_timezone_get();
//        $new_timezone=$user_timezone['timezone'];
//        date_default_timezone_set($new_timezone);
//        $curr_date = gmdate("Y-m-d");
//        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
//        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, wp.`account_id`, `account_state`, `action_reasons`, `disabled_reasons`, wp.`currency`, `upgrade_status`, er.`student_id`, er.`payment_type`, er.`credit_card_id`, er.`credit_card_name`, er.`processing_fee_type`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, e.`event_type`, e.`event_title` FROM `wp_account` wp
//                LEFT JOIN `company` c ON c.`company_id`=wp.`company_id` 
//                LEFT JOIN `event_registration` er ON er.`company_id`=wp.`company_id` 
//                LEFT JOIN `event` e ON er.`event_id`=e.`event_id` WHERE c.`company_id`='%s' AND `event_reg_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id));
//        $result1 = mysqli_query($this->db, $query1);
//        if(!$result1){
//            log_info("processSingleEventRegistrationChanges-1 : $query1");
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//        }else{
//            $num_rows1 = mysqli_num_rows($result1);
//            if($num_rows1>0){
//                $row = mysqli_fetch_assoc($result1);
//                $access_token = $row['access_token'];
//                $account_id = $row['account_id'];
//                $currency = $row['currency'];
//                $user_state = $row['wp_user_state'];
//                $acc_state = $row['account_state'];
//                $upgrade_status = $row['upgrade_status'];
//                $student_id = $row['student_id'];
//                $event_type = $row['event_type'];
//                $event_name = $row['event_title'];
//                $payment_type = $row['payment_type'];
//                $cc_id = $row['credit_card_id'];
//                $cc_name = $row['credit_card_name'];
//                $processing_fee_type = $row['processing_fee_type'];
//                $buyer_name = $row['buyer_name'];
//                $buyer_email = $row['buyer_email'];
//                $buyer_phone = $row['buyer_phone'];
//                $buyer_postal_code = $row['buyer_postal_code'];
////                $country = $row['country'];
//                $gmt_date=gmdate(DATE_RFC822);
//                $sns_msg8=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
//                if($user_state=='deleted'){
//                    $log = array("status" => "Failed", "msg" => "You may delete your account yourselves, or the account may be deleted by WePay customer support.");
//                    $this->response($this->json($log),200);
//                }
//                
//                if($acc_state=='deleted'){
//                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
//                    $this->response($this->json($log),200);
//                }elseif($acc_state=='disabled'){
//                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
//                    $this->response($this->json($log),200);
//                }
//
//                if($event_type!=$reg_event_type || count($event_array)!=1){
//                    $log = array("status" => "Failed", "msg" => "Event details mismatched.");
//                    $this->response($this->json($log),200);
//                }
//                
//                if(!empty($access_token) && !empty($account_id)){
//                    if (!function_exists('getallheaders')){
//                        if(!function_exists ('apache_request_headers')){
//                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
//                        }else{
//                            $headers = apache_request_headers();
//                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
//                        }
//                    }else{
//                        $headers = getallheaders();
//                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
//                    }
//                    $password = $this->wp_pswd;
//                    $method = 'aes-256-cbc';
//                    // Must be exact 32 chars (256 bit)
//                    $password = substr(hash('sha256', $password, true), 0, 32);
//                    // IV must be exact 16 chars (128 bit)
//                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
//
//                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
//                }
//            }else{
//                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
//                $this->response($this->json($error),200);
//            }
//        }
//        
//        $update_zero_cost_payment_record = 0;
//        $update_quantity_array = [];
//        
//        if($payment_type=='F'){
//            
//            for($i=0; $i<count($event_array); $i++){
//                $update_quantity_array[$i] = 0;
//                $event_id = $event_array[$i]['ev_id'];
//                $child_title = $event_array[$i]['title'];
//                $cancelled_quantity = $event_array[$i]['cancelled_quantity'];
//                $discounted_price = $event_array[$i]['discounted_price'];
//                $quantity = $event_array[$i]['quantity'];
//                $update_quantity = 0;
//                if($event_array[$i]['state']=='U'){
//                    if(isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
//                        $update_quantity_array[$i] += -$event_array[$i]['cancelled_quantity'];
//                    }
//                    if(isset($event_array[$i]['added_quantity']) && $event_array[$i]['added_quantity']>0){
//                        $update_quantity_array[$i] += $event_array[$i]['added_quantity'];
//                    }
//                    
//                    if(isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
//                        $update_reg_details = sprintf("UPDATE `event_reg_details` SET `quantity`= IF('%s'=0,`quantity`,'%s'), `payment_status`=IF('%s'=0,'CP',`payment_status`) WHERE `event_reg_details_id`='%s'", 
//                                mysqli_real_escape_string($this->db, $event_array[$i]['quantity']), mysqli_real_escape_string($this->db, $event_array[$i]['quantity']), mysqli_real_escape_string($this->db, $event_array[$i]['quantity']), mysqli_real_escape_string($this->db, $event_array[$i]['event_reg_details_id']));
//                        $result_update_reg_details = mysqli_query($this->db, $update_reg_details);
//                        if(!$result_update_reg_details){
//                            log_info("processSingleEventRegistrationChanges-2 : $update_reg_details");
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 200);
//                        }
//                        
//                        if($event_array[$i]['quantity']==0 && $event_array[$i]['discounted_price']==0){
//                            $update_plan = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$i]['event_payment_id']));
//                            $result_update = mysqli_query($this->db, $update_plan);
//                            if(!$result_update){
//                                log_info("processSingleEventRegistrationChanges-3 : $update_plan");
//                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                $this->response($this->json($error), 200);
//                            }
//                        }elseif($event_array[$i]['quantity']>0 && $event_array[$i]['discounted_price']==0){
//                            $update_reg_details2 = sprintf("INSERT INTO `event_reg_details`( `event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `manual_payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) 
//                                    SELECT `event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `manual_payment_amount`, '%s', `balance_due`, `processing_fee`, 'CP' FROM `event_reg_details` WHERE `event_reg_details_id`='%s'", 
//                                    mysqli_real_escape_string($this->db, $event_array[$i]['cancelled_quantity']), mysqli_real_escape_string($this->db, $event_array[$i]['event_reg_details_id']));
//                            $result_update_reg_details2 = mysqli_query($this->db, $update_reg_details2);
//                            if(!$result_update_reg_details2){
//                                log_info("processSingleEventRegistrationChanges-4 : $update_reg_details2");
//                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                $this->response($this->json($error), 200);
//                            }
//                        }
//                        $cancelled_cost = $discounted_price*$cancelled_quantity;
//                        $cancel_text = "Cancelled $event_name $child_title, quantity $cancelled_quantity. Total Cost $cancelled_cost."; 
//                        $update_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`) VALUES ('%s','%s','%s','%s','%s')",
//                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 
//                                mysqli_real_escape_string($this->db, 'cancel'), mysqli_real_escape_string($this->db, $cancel_text));
//                        $result_update_history = mysqli_query($update_history);
//                        if(!$result_update_history){
//                            log_info("processSingleEventRegistrationChanges-5 : $update_history");
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 200);
//                        }
//                    }
//                    
//                    $update_event_count = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+%s WHERE `event_id`=%s", $update_quantity_array[$i], $event_array[$i]['ev_id']);
//                    $result_event_count = mysqli_query($this->db, $update_event_count);
//                    if(!$result_event_count){
//                        log_info("processSingleEventRegistrationChanges-6 : $update_event_count");
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                        $this->response($this->json($error), 200);
//                    }
//                }elseif($event_array[$i]['state']=='N'){
//                    
//                }
//            }
//        }elseif($payment_type=='CO'){               ///need check & work for Manual refund
//            
//            $p_f = $this->getProcessingFee($company_id,$upgrade_status);
//            $process_fee_per = $p_f['PP'];
//            $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);
//            
//            $exceed_amount = 0;
//            if($cancel_flag=='WR'){
//                for($j=0;$j<count($plan_array);$j++){
//                    $payment_amount = $plan_array[$j]['amount'];
//                    
//                    if(isset($plan_array[$j]['event_payment_id'])&&!empty($plan_array[$j]['event_payment_id'])){
//                        $get_payment_query = sprintf("SELECT credit_method,`checkout_id`,`checkout_status`, `refunded_amount`, `processing_fee`, 
//                                (select sum(processing_fee) from `event_payment` ep2 where ep2.`event_reg_id`='%s' AND ep2.`checkout_id`=ep1.`checkout_id` AND ep1.`checkout_id`!='' AND `event_payment_id`!='%s') refunded_processing_fee 
//                                FROM `event_payment` ep1 WHERE `event_payment_id`='%s' ", mysqli_real_escape_string($this->db,$event_reg_id), mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']), mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
//                        $result_payment_query = mysqli_query($this->db,$get_payment_query);
//                        if(!$result_payment_query){
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 200);
//                        }else{
//                            $row_res = mysqli_fetch_assoc($result_payment_query);
//                            $payment_id = $row_res['checkout_id'];
//                            $payment_status = $row_res['checkout_status'];
//                            $db_refunded_amt = $row_res['refunded_amount'];
//                            $db_processing_fee = $row_res['processing_fee'];
//                            $db_refunded_processing_fee = $row_res['refunded_processing_fee'];
//                            $stop_wepay = false;
//                            if($row_res['credit_method'] == "CH" || $row_res['credit_method'] == "CA"){
//                                $stop_wepay = true;
//                            }
//                        }
//                    }else{
//                        $error = array('status' => "Failed", "msg" => "Payment details mismatch error.");
//                        $this->response($this->json($error), 200);
//                    }
//                    
//                    if($plan_array[$j]['edit_status']=='U'){
//                        if(isset($plan_array[$j]['to_refund']) && $plan_array[$j]['to_refund']>0){
//                            if($plan_array[$j]['to_refund']==$plan_array[$j]['amount']){
//                                if(empty(trim($payment_id)) && empty(trim($payment_status))){
//                                    $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
//                                    $result_update_query2 = mysqli_query($this->db, $update_query2);
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "5- $update_query2");
//                                    log_info($this->json($error_log));
//                                    if(!$result_update_query2){
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "5- $update_query2");
//                                        log_info($this->json($error_log));
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                        $this->response($this->json($error), 200);
//                                    }
//                                }else{
//                                    if(!$stop_wepay){
//                                    if($payment_status=='new' || $payment_status=='authorized' || $payment_status=='reserved'){
//                                        $sns_msg['call'] = "Checkout";
//                                        $sns_msg['type'] = "Checkout Cancel";
//                                        $call_method = "checkout/cancel";
//                                        $json = array("checkout_id"=>$payment_id,"cancel_reason"=>"Participant ");
//                                    }elseif($payment_status=='released' || $payment_status=='captured'){
//                                        $sns_msg8['call'] = "Checkout";
//                                        $sns_msg8['type'] = "Checkout Refund";
//                                        $call_method = "checkout/refund";
//                                        $json = array("checkout_id"=>$payment_id,"refund_reason"=>"Participant ");
//                                    }else{
//                                        $error = array("status" => "Failed", "msg" => "Participant payment status is in $payment_status status.");
//                                        $this->response($this->json($error),200);
//                                    }
//
//                                    $postData = json_encode($json);
//                                    $response = $this->wp->accessWepayApi($call_method,'POST',$postData,$token,$user_agent,$sns_msg);
//                                    $this->wp->log_info("wepay_checkout_refund: ".$response['status']);
//                                    }
//                                    if((isset($response['status']) && $response['status']=="Success") || $stop_wepay){
//                                        $state = $response['msg']['state'];
//                                        if($state=='cancelled'){
//                                            $status = "C";
//                                            $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `refunded_amount`='%s' WHERE `event_payment_id`='%s'", 
//                                                mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $plan_array[$j]['to_refund']), mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
//                                        }elseif($state=='refunded'){
//                                            $status = "R";
//                                            $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `refunded_amount`='%s', `refunded_date`='$curr_date' WHERE `event_payment_id`='%s'", 
//                                                mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $plan_array[$j]['to_refund']), mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
//                                        }
//                                        $result_update_query2 = mysqli_query($this->db, $update_query2);
//                                        if(!$result_update_query2){
//                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "6- $update_query2");
//                                            log_info($this->json($error_log));
//                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                            $this->response($this->json($error), 200);
//                                        }
//                                    }else{
//                                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                                        $this->response($this->json($log),200);
//                                    }
//                                }
//                            }elseif($plan_array[$j]['to_refund']<$plan_array[$j]['amount']){
//                            
//                                if(empty(trim($payment_id)) && empty(trim($payment_status))){
//                                    $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
//                                    $result_update_query2 = mysqli_query($this->db, $update_query2);
//                                    if(!$result_update_query2){
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "7- $update_query2");
//                                        log_info($this->json($error_log));
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                        $this->response($this->json($error), 200);
//                                    }
//                                }else{
//                                    $payment_refunded = 0;
//                                    $refund = $plan_array[$j]['to_refund'];
//                                    $temp_payment_amount = $plan_array[$j]['amount']-$db_refunded_amt;
//                                    $fee = $db_processing_fee;
//                                    if(isset($plan_array[$j]['to_refund']) && $plan_array[$j]['to_refund']>0){
//                                        $temp_amount = $temp_payment_amount - $plan_array[$j]['to_refund'];
//                                        if($temp_amount>0){
//                                            $r1_fee = $temp_amount*($process_fee_per/100) + $process_fee_val;
//                                            $r2_fee = ($temp_amount + $r1_fee)*($process_fee_per/100) + $process_fee_val;
//                                            if($processing_fee_type==1){
//                                                $trfee = number_format((float)($r1_fee), 2, '.', '');
//                                            }else{
//                                                $trfee = number_format((float)($r2_fee), 2, '.', '');
//                                            }
//                                            $rfee = $fee - $trfee;
//                                        }else{
//                                            $rfee = $fee;
//                                        }
//                                    }
//                                    $paid_amount -= $refund;
//                                    $r_amt = 0;
//                                    $rem_amt = $temp_payment_amount-$refund;
//                                    if($processing_fee_type==1){
//                                        $r_amt = $refund;
//                                    }else{
//                                        $r_amt = $refund+$rfee;
//                                    }
//                                    if(!$stop_wepay){
//                                    if($payment_status=='new' || $payment_status=='authorized' || $payment_status=='reserved'){
//                                        $sns_msg8['call'] = "Checkout";
//                                        $sns_msg8['type'] = "Checkout Cancel";
//                                        $call_method = "checkout/cancel";
//                                        $json = array("checkout_id"=>$payment_id,"cancel_reason"=>"Participant ");
//                                    }elseif($payment_status=='released' || $payment_status=='captured'){
//                                        $paid_amount -= $refund;
//                                        $sns_msg8['call'] = "Checkout";
//                                        $sns_msg8['type'] = "Checkout Refund";
//                                        $call_method = "checkout/refund";
//                                        $json = array("checkout_id"=>$payment_id,"amount"=>$r_amt,"app_fee"=>$rfee,"refund_reason"=>"Participant ");
//                                    }else{
//                                        $error = array("status" => "Failed", "msg" => "Participant payment status is in $payment_status status.");
//                                        $this->response($this->json($error),200);
//                                    }
//
//                                    $postData = json_encode($json);
//                                    $response = $this->wp->accessWepayApi($call_method,'POST',$postData,$token,$user_agent,$sns_msg8);
//                                    $this->wp->log_info("wepay_checkout_refund: ".$response['status']);
//                                    }
//                                    if((isset($response['status']) && $response['status']=="Success") || $stop_wepay){
//            //                            $res = $response['msg'];
//                                        $state = $response['msg']['state'];
//
//                                        $json2 = array("checkout_id"=>"$payment_id");
//                                        $postData2 = json_encode($json2);
//                                        $sns_msg8['call'] = "Checkout";
//                                        $sns_msg8['type'] = "Checkout details";
//                                        $response2 = $this->wp->accessWepayApi("checkout",'POST',$postData2,$token,$user_agent,$sns_msg8);
//                                        $this->wp->log_info("wepay_checkout_get: ".$response2['status']);
//                                        if($response2['status']=="Success"){
//                                            $res2 = $response2['msg'];
//                                            $checkout_id = $res2['checkout_id'];
//                                            $checkout_state = $res2['state'];
//                                        }else{
//                                            $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                                            $this->response($this->json($error),200);
//                                        }
//
//                                        $total_refund_amount += $plan_array[$j]['to_refund'];
//                                        if($response2['msg']['refund']['amount_refunded']>0){
//                                            if($processing_fee_type==1){
//                                                $payment_refunded = $response2['msg']['refund']['amount_refunded'];
//                                            }else{
//                                                $payment_refunded = $response2['msg']['refund']['amount_refunded']-($db_refunded_processing_fee+$rfee);
//                                            }
//                                        }
//                                        if($state=='cancelled'){
//                                            $rand = mt_rand(1,99999);
//                                            $ip = $this->getRealIpAddr();
//                                            $status = "C";
//                                            $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout";
//                                            $unique_id = $company_id."_".$event_id."_".$ip."_".$rand."_".time();
//                                            $date = date("Y-m-d_H:i:s");
//                                            $ref_id = $company_id."_".$event_id."_".$date;
//                                            $fee_payer = "payee_from_app";
//
//                                            if ($processing_fee_type == 2) {
//                                                $w_rem_amt = $rem_amt + $trfee;
//                                            } elseif ($processing_fee_type == 1) {
//                                                $w_rem_amt = $rem_amt;
//                                            }
//                                            $time = time();
//                                            $json2 = array("account_id"=>"$account_id","short_description"=>"$event_name","type"=>"event","amount"=>"$w_rem_amt","currency"=>"$currency",
//                                                    "fee"=>array("app_fee"=>$trfee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
//                                                    "email_message"=>array("to_payee"=>"Payment has been added for $event_name","to_payer"=>"Payment has been made for $event_name"),
//                                                    "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
//                                                    "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
//                                                        array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
//                                                        array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))),
//                                                    "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
//                                                        "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$w_rem_amt, "quantity"=>1, "amount"=>$w_rem_amt, "currency"=>"$currency"))))));
//                                                $postData2 = json_encode($json2);
//                                                $sns_msg8['call'] = "Checkout";
//                                                $sns_msg8['type'] = "Checkout Creation";
//                                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg8);
//                                                $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
//                                                if($response2['status']=="Success"){
//                                                    $res2 = $response2['msg'];
//                                                    $checkout_id = $res2['checkout_id'];
//                                                    $checkout_state = $res2['state'];
//                                                }else{
//                                                    if($response2['msg']['error_code']==1008){
//                                                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg8);
//                                                        $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
//                                                        if($response2['status']=="Success"){
//                                                            $res2 = $response2['msg'];
//                                                            $checkout_id = $res2['checkout_id'];
//                                                            $checkout_state = $res2['state'];
//                                                        }else{
//                                                            if($response2['msg']['error_code']==1008){
//                                                                $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
//                                                                $this->response($this->json($error),200);
//                                                            }else{
//                                                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                                                                $this->response($this->json($error),200);
//                                                            }
//                                                        }
//                                                    }else{                                                
//                                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                                                        $this->response($this->json($error),200);
//                                                    }
//                                                }
//
//                                                $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `processing_fee`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
//                                                        mysqli_real_escape_string($this->db,$event_reg_id), mysqli_real_escape_string($this->db,$student_id), mysqli_real_escape_string($this->db,$checkout_id), mysqli_real_escape_string($this->db,$checkout_state), mysqli_real_escape_string($this->db,$rem_amt),
//                                                        mysqli_real_escape_string($this->db,$curr_date), mysqli_real_escape_string($this->db,'S'), mysqli_real_escape_string($this->db,$trfee), mysqli_real_escape_string($this->db,$cc_id), mysqli_real_escape_string($this->db,$cc_name));
//                                                $result_insert = mysqli_query($this->db, $insert_payment);
//                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "8- $insert_payment");
//                                                log_info($this->json($error_log));
//                                                if (!$result_insert) {
//                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "8- $insert_payment");
//                                                    log_info($this->json($error_log));
//                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                                    $this->response($this->json($error), 200);
//                                                }
//                                        }elseif($state=='refunded'){
//                                            $status = "R";
//                                            $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `refunded_date`, `processing_fee`, `cc_id`, `cc_name`)
//                                                    SELECT `event_reg_id`, `student_id`, `checkout_id`, 'refunded', $refund, `schedule_date`, 'R', '$curr_date', $rfee, $cc_id, '$cc_name'  FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
//                                            $result_insert_query = mysqli_query($this->db, $insert_payment);
//                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "9- $insert_payment");
//                                            log_info($this->json($error_log));
//                                            if(!$result_insert_query){
//                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "9- $insert_payment");
//                                                log_info($this->json($error_log));
//                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                                $this->response($this->json($error), 200);
//                                            }
//                                        }elseif($payment_refunded>0){
//                                            $status = "PR";
//                                            $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `refunded_date`, `processing_fee`, `cc_id`, `cc_name`)
//                                                    SELECT `event_reg_id`, `student_id`, `checkout_id`, 'refunded', $refund, `schedule_date`, 'R', '$curr_date', $rfee, $cc_id, '$cc_name'  FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
//                                            $result_insert_query = mysqli_query($this->db, $insert_payment);
//                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "10- $insert_payment");
//                                            log_info($this->json($error_log));
//                                            if(!$result_insert_query){
//                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "10- $insert_payment");
//                                                log_info($this->json($error_log));
//                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                                $this->response($this->json($error), 200);
//                                            }
//                                        }
//
//                                        $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`=IF($temp_amount=0,'CR','%s'), `refunded_amount`=$payment_refunded, `refunded_date`='$curr_date', `processing_fee`=IF($temp_amount=0,0,'$trfee') WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
//                                        $result_update_query2 = mysqli_query($this->db, $update_query2);
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "11- $update_query2");
//                                        log_info($this->json($error_log));
//                                        if(!$result_update_query2){
//                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "11- $update_query2");
//                                            log_info($this->json($error_log));
//                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                            $this->response($this->json($error), 200);
//                                        }
//                                    }else{
//                                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                                        $this->response($this->json($log),200);
//                                    }
//                                }
//                            }
//                        }else{
//                            if(isset($plan_array[$j]['event_payment_id']) && !empty($plan_array[$j]['event_payment_id'])){
//                                $update_plan = sprintf("UPDATE `event_payment` SET `payment_amount`='%s', `schedule_status`='%s', `processing_fee`='%s' WHERE `event_payment_id`='%s'", 
//                                        mysqli_real_escape_string($this->db,$plan_array[$j]['amount']), mysqli_real_escape_string($this->db,$plan_array[$j]['status']), mysqli_real_escape_string($this->db,$plan_array[$j]['process_fee']), mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
//                                $result_update = mysqli_query($this->db, $update_plan);
//                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "12- $update_plan");
//                                log_info($this->json($error_log));
//                                if(!$result_update){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "12- $update_plan");
//                                    log_info($this->json($error_log));
//                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                    $this->response($this->json($error), 200);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            
//            for($i=0; $i<count($event_array); $i++){
//                $update_quantity_array[$i] = 0;
//                $event_id = $event_array[$i]['ev_id'];
//                $child_title = $event_array[$i]['title'];
//                $discounted_price = $event_array[$i]['discounted_price'];
//                $quantity = $event_array[$i]['quantity'];
//                $cancelled_quantity = $event_array[$i]['cancelled_quantity'];
//                $cancelled_cost = $discounted_price*$cancelled_quantity;
//                
//                if($cancel_flag=='WOR'){
//                    $exceed_amount += $cancelled_cost;
//                }
//                
//                if($event_array[$i]['state']=='U'){
//                    $actual_cost = ($quantity+$cancelled_quantity)*$discounted_price;
//                    $remaining_event_cost = $quantity*$discounted_price;
//                    
//                    $ac1_fee = $actual_cost*($process_fee_per/100) + $process_fee_val;
//                    $rc1_fee = $remaining_event_cost*($process_fee_per/100) + $process_fee_val;                    
//                    if($processing_fee_type==1){
//                        $actual_cost_fee = number_format((float)($ac1_fee), 2, '.', '');
//                        $remaining_event_cost_fee = number_format((float)($rc1_fee), 2, '.', '');
//                    }else{
//                        $ac2_fee = ($actual_cost + $ac1_fee)*($process_fee_per/100) + $process_fee_val;
//                        $rc2_fee = ($remaining_event_cost + $rc1_fee)*($process_fee_per/100) + $process_fee_val;
//                        $actual_cost_fee = number_format((float)($ac2_fee), 2, '.', '');
//                        $remaining_event_cost_fee = number_format((float)($rc2_fee), 2, '.', '');
//                    }
//                    $cancelled_cost_fee = $actual_cost_fee - $remaining_event_cost_fee;
//                    
//                    if(isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
//                        $update_quantity_array[$i] += -$event_array[$i]['cancelled_quantity'];
//                    }
//                    if(isset($event_array[$i]['added_quantity']) && $event_array[$i]['added_quantity']>0){
//                        $update_quantity_array[$i] += $event_array[$i]['added_quantity'];
//                    }
//                    
//                    if(isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
//                        $update_reg_details = sprintf("UPDATE `event_reg_details` SET `quantity`= IF('%s'=0,`quantity`,'%s'), `payment_status`=IF('%s'=0,'RF',`payment_status`) WHERE `event_reg_details_id`='%s'", 
//                                mysqli_real_escape_string($this->db, $event_array[$i]['quantity']), mysqli_real_escape_string($this->db, $event_array[$i]['quantity']), mysqli_real_escape_string($this->db, $event_array[$i]['quantity']), mysqli_real_escape_string($this->db, $event_array[$i]['event_reg_details_id']));
//                        $result_update_reg_details = mysqli_query($this->db, $update_reg_details);
//                        if(!$result_update_reg_details){
//                            log_info("processSingleEventRegistrationChanges-2 : $update_reg_details");
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 200);
//                        }
//                        
//                        if($event_array[$i]['quantity']==0){
//                            $update_plan = sprintf("UPDATE `event_payment` SET `schedule_status`=if('$cancel_flag'='WR','R','C') WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$i]['event_payment_id']));
//                            $result_update = mysqli_query($this->db, $update_plan);
//                            if(!$result_update){
//                                log_info("processSingleEventRegistrationChanges-3 : $update_plan");
//                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                $this->response($this->json($error), 200);
//                            }
//                        }elseif($event_array[$i]['quantity']>0){
//                            $update_reg_details2 = sprintf("INSERT INTO `event_reg_details`( `event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) 
//                                    SELECT `event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, $cancelled_cost, $cancelled_quantity, `balance_due`, $cancelled_cost_fee, if('$cancel_flag'='WR','RF','CP') FROM `event_reg_details` WHERE `event_reg_details_id`='%s'", 
//                                    mysqli_real_escape_string($this->db, $event_array[$i]['event_reg_details_id']));
//                            $result_update_reg_details2 = mysqli_query($this->db, $update_reg_details2);
//                            if(!$result_update_reg_details2){
//                                log_info("processSingleEventRegistrationChanges-4 : $update_reg_details2");
//                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                $this->response($this->json($error), 200);
//                            }
//                        }
//                        
//                        $cancel_text = "Cancelled $event_name, quantity $cancelled_quantity. Total Cost $cancelled_cost."; 
//                        $update_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`) VALUES ('%s','%s','%s','%s','%s')",
//                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 
//                                mysqli_real_escape_string($this->db, 'cancel'), mysqli_real_escape_string($this->db, $cancel_text));
//                        $result_update_history = mysqli_query($update_history);
//                        if(!$result_update_history){
//                            log_info("processSingleEventRegistrationChanges-5 : $update_history");
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 200);
//                        }
//                    }
//                    
//                    $update_event_count = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+%s WHERE `event_id`=%s", $update_quantity_array[$i], $event_array[$i]['ev_id']);
//                    $result_event_count = mysqli_query($this->db, $update_event_count);
//                    if(!$result_event_count){
//                        log_info("processSingleEventRegistrationChanges-6 : $update_event_count");
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                        $this->response($this->json($error), 200);
//                    }
//                }
//            }
//        }elseif($payment_type=='RE'){
//            
//        }else{
//            $log = array("status" => "Failed", "msg" => "Invalid payment type.");
//            $this->response($this->json($log),200);
//        }
//        
//        $event_details = $this->getEventsDetailByEventId($company_id, $event_id, $event_type, 'live', 1);
//        $msg = array("status" => "Success", "msg" => "Cancellation Successfully Processed.", "event_details"=>$event_details);
//        $this->response($this->json($msg), 200);
////                    $log = array("status" => "Failed", "msg" => "Invalid payment plan details error.");
////                    $this->response($this->json($log),200);
////                }
////            }elseif($payment_type=='CO'){
////                
////            }elseif($payment_type=='RE'){
////                
////            }else{
////                $log = array("status" => "Failed", "msg" => "Invalid payment type error.");
////                $this->response($this->json($log),200);
////            }
////        }elseif($event_array[$i]['state']=='N'){
////            $log = array("status" => "Failed", "msg" => "No changes found.");
////            $this->response($this->json($log),200);
////        }else{
////            ######Waiting for access
////            $log = array("status" => "Failed", "msg" => "For future use.");
////            $this->response($this->json($log),200);
////        }
//        
//            
//    }
    
    public function processPaymentRefund($company_id, $event_reg_id, $event_array, $plan_array, $total_due, $cancel_flag){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        
        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
        $stop_wepay = false;
//        $country = 'US';
        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, wp.`account_id`, c.`wp_currency_symbol`, `account_state`, `action_reasons`, `disabled_reasons`, wp.`currency`, `upgrade_status`, er.`student_id`, er.`payment_type`, er.`credit_card_id`, er.`credit_card_name`, er.`processing_fee_type`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, e.`event_type`, e.`event_title` ,er.`registration_from` FROM `company` c
                LEFT JOIN `wp_account` wp ON c.`company_id`=wp.`company_id` 
                LEFT JOIN `event_registration` er ON er.`company_id`=c.`company_id` 
                LEFT JOIN `event` e ON er.`event_id`=e.`event_id` WHERE c.`company_id`='%s' AND `event_reg_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id));
        $result1 = mysqli_query($this->db, $query1);
        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "1- $query1");
        log_info($this->json($error_log));
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "1- $query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $wp_currency_symbol = $row['wp_currency_symbol'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                $upgrade_status = $row['upgrade_status'];
                $student_id = $row['student_id'];
                $event_type = $row['event_type'];
                $event_name = $row['event_title'];
                $payment_type = $row['payment_type'];
                $cc_id = $row['credit_card_id'];
                $cc_name = $row['credit_card_name'];
                $processing_fee_type = $row['processing_fee_type'];
                $buyer_name = $row['buyer_name'];
                $buyer_email = $row['buyer_email'];
                $buyer_phone = $row['buyer_phone'];
                $buyer_postal_code = $row['buyer_postal_code'];
                $registration_from = $row['registration_from'];
                if ($registration_from == 'S'){
                    $this->processstripePaymentRefund($company_id, $event_reg_id, $event_array, $plan_array, $total_due, $cancel_flag);
                }  
//                $country = $row['country'];
                 $gmt_date=gmdate(DATE_RFC822);
                $sns_msg8=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
                if($user_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "You may delete your account yourselves, or the account may be deleted by WePay customer support.");
                    $this->response($this->json($log),200);
                }else if(empty(trim($user_state)) || is_null($user_state)){
                    $log = array("status" => "Failed", "msg" => "You currntly do not have any Wepay account.");
                    $this->response($this->json($log),200);
                }
                
                if($acc_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log),200);
                }elseif($acc_state=='disabled'){
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log),200);
                }

                if(!empty($access_token) && !empty($account_id)){
                    if (!function_exists('getallheaders')){
                        if(!function_exists ('apache_request_headers')){
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        }else{
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    }else{
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);

//                    $json = array("account_id"=>"$account_id");
//                    $postData = json_encode($json);
//                    $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent,$sns_msg8);
//                    $this->wp->log_info("wepay_get_account: ".$response['status']);
//                    if($response['status']=='Success'){
//                        $res = $response['msg'];
//                        $account_state = $res['state'];
//                        $currency = $res['currencies'][0];
//                        $curr_acc = implode(",", $res['currencies']);
//                        $disabled_reasons = implode(",", $res['disabled_reasons']);
//                        if($account_state=='deleted'){
//                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
//                            $this->response($this->json($log),200);
//                        }elseif($account_state=='disabled'){
//                            $log = array("status" => "Failed", "msg" => "Wepay Account requires ".$disabled_reasons." update. Please complete the action(s) before accept payments.");
//                            $this->response($this->json($log),200);
//                        }
//                    }else{
//                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                        $this->response($this->json($log),200);
//                    }
                }
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error),200);
            }
        }
        
        $paid_amount = $parent_qty = $total_refund_amount = $update_event_remaining_dues = $update_event_amount = $event_refund_sales = $update_refund_sales_flag = $update_zero_cost_payment_record = 0;
        $curr_date = date("Y-m-d");
        $parent_net_sales = 0;
        $manual_refund_amount = 0;
        
        if(count($event_array)>0){
            for($i=0; $i<count($event_array); $i++){
                if($event_array[$i]['state']=='U'){
                    if(isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
                        $parent_qty += $event_array[$i]['cancelled_quantity'];
                    }
                    if($event_array[$i]['quantity']==0){
                        if($event_array[$i]['discounted_price']==0 && $payment_type=='F' && count($event_array)==1){
                            $update_zero_cost_payment_record = 1;
                        }
                    }
                }
            }
        }

        if($cancel_flag=='WOR'){
            if(count($plan_array)>0){
                for($j=0; $j<count($plan_array);$j++){
                    if(isset($plan_array[$j]['event_payment_id'])&&!empty($plan_array[$j]['event_payment_id'])&&$plan_array[$j]['edit_status']=='D'){
                        $update_plan = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                        $result_update = mysqli_query($this->db, $update_plan);
                        if(!$result_update){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "4- $update_plan");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }

                        $curr_date_time = gmdate("Y-m-d H:i:s");
                        $activity_text = "Cancelled payment $wp_currency_symbol".$plan_array[$j]['amount']."($cc_name)";

                        $insert_payment_history1 = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $plan_array[$j]['event_payment_id'], 'refund', mysqli_real_escape_string($this->db, $activity_text), $curr_date_time);
                        $result_insert_payment_history1 = mysqli_query($this->db, $insert_payment_history1);
                        if (!$result_insert_payment_history1) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $insert_payment_history1");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }
        }elseif($cancel_flag=='WR' || $cancel_flag=='C' || $cancel_flag=='NP'){
            if(count($plan_array)>0){
                for($j=0; $j<count($plan_array);$j++){
                    $make_new_cancel_payment_record = 0;
                    $rfee = $trfee = $payment_id = 0;
                    $payment_amount = $plan_array[$j]['amount'];
                    $p_f = $this->getProcessingFee($company_id,$upgrade_status);
                    $process_fee_per = $p_f['PP'];
                    $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);
                    if(isset($plan_array[$j]['event_payment_id'])&&!empty($plan_array[$j]['event_payment_id'])&&$plan_array[$j]['status']!='M'){
    //                $get_payment_query = sprintf("SELECT `checkout_id`,`checkout_status`, `refunded_amount`, `processing_fee` FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                        $get_payment_query = sprintf("SELECT `checkout_id`,`checkout_status`, `refunded_amount`, `processing_fee`, `credit_method`,
                                (select sum(processing_fee) from `event_payment` ep2 where ep2.`event_reg_id`='%s' AND ep2.`checkout_id`=ep1.`checkout_id` AND ep1.`checkout_id`!='' AND `event_payment_id`!='%s') refunded_processing_fee ,`last_updt_dt`
                                FROM `event_payment` ep1 WHERE `event_payment_id`='%s' ", mysqli_real_escape_string($this->db,$event_reg_id), mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']), mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                        $result_payment_query = mysqli_query($this->db,$get_payment_query);
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $get_payment_query");
                        log_info($this->json($error_log));
                        if(!$result_payment_query){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $get_payment_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else{
                            $row_res = mysqli_fetch_assoc($result_payment_query);
                            $payment_id = $row_res['checkout_id'];
                            $payment_status = $row_res['checkout_status'];
                            $db_refunded_amt = $row_res['refunded_amount'];
                            $db_processing_fee = $row_res['processing_fee'];
                            $db_refunded_processing_fee = $row_res['refunded_processing_fee'];
                            $old_last_updt_dt = $row_res['last_updt_dt'];
                            $stop_wepay = false;
                            if($row_res['credit_method'] == "CH" || $row_res['credit_method'] == "CA"){
                                $stop_wepay = true;
                            }
                        }
    //                    $p_f = $this->getProcessingFee($upgrade_status);
    //                    $process_fee_per = $p_f['PP'];
    //                    $process_fee_val = $p_f['PT'];
        //                $p1_fee = $payment_amount*($process_fee_per/100) + $process_fee_val;
        //                $p2_fee = ($payment_amount + $p1_fee)*($process_fee_per/100) + $process_fee_val;
        //                if($processing_fee_type==1){
        //                    $fee = number_format((float)($p1_fee), 2, '.', '');
        //                }else{
        //                    $fee = number_format((float)($p2_fee), 2, '.', '');
        //                }
        //                if(isset($plan_array[$j]['to_refund']) && $plan_array[$j]['to_refund']>0){
        //                    if($plan_array[$j]['to_refund']==$plan_array[$j]['amount']){
        //                        $temp_amount = $plan_array[$j]['amount'];
        //                    }else{
        //                        $temp_amount = $plan_array[$j]['amount'] - $plan_array[$j]['to_refund'];
        //                    }
        //                    $r1_fee = $temp_amount*($process_fee_per/100) + $process_fee_val;
        //                    $r2_fee = ($temp_amount + $r1_fee)*($process_fee_per/100) + $process_fee_val;
        //                    if($processing_fee_type==1){
        //                        $trfee = number_format((float)($r1_fee), 2, '.', '');
        //                    }else{
        //                        $trfee = number_format((float)($r2_fee), 2, '.', '');
        //                    }
        //                    $rfee = $fee - $trfee;
        //                    if($plan_array[$j]['to_refund']==$plan_array[$j]['amount']){
        //                        if($processing_fee_type==1){
        //                            $parent_net_sales += $plan_array[$j]['to_refund']-$trfee;
        //                            $event_refund_sales += $plan_array[$j]['to_refund']-$trfee;
        //                        }else{
        //                            $parent_net_sales += $plan_array[$j]['to_refund'];
        //                            $event_refund_sales += $plan_array[$j]['to_refund'];
        //                        }
        //                    }else{
        //                        if($processing_fee_type==1){
        //                            $parent_net_sales += $plan_array[$j]['to_refund']-$rfee;
        //                            $event_refund_sales += $plan_array[$j]['to_refund']-$rfee;
        //                        }else{
        //                            $parent_net_sales += $plan_array[$j]['to_refund'];
        //                            $event_refund_sales += $plan_array[$j]['to_refund'];
        //                        }
        //                    }                    
        //                    $update_refund_sales_flag = 1;
        //                }
                        $temp_payment_amount = $plan_array[$j]['amount']-$db_refunded_amt;
                        $fee = $db_processing_fee;
                        if(isset($plan_array[$j]['to_refund']) && $plan_array[$j]['to_refund']>0){
                            $temp_amount = $temp_payment_amount - $plan_array[$j]['to_refund'];
                            if($temp_amount>0){
                                $r1_fee = $temp_amount*($process_fee_per/100) + $process_fee_val;
                                $r2_fee = ($temp_amount + $r1_fee)*($process_fee_per/100) + $process_fee_val;
                                if($processing_fee_type==1){
                                    $trfee = number_format((float)($r1_fee), 2, '.', '');
                                }else{
                                    $trfee = number_format((float)($r2_fee), 2, '.', '');
                                }
                                $rfee = $fee - $trfee;
                            }else{
                                $rfee = $fee;
                                $trfee = $fee;
                            }
                            if($plan_array[$j]['credit_method']=='CC'){
                                if($plan_array[$j]['to_refund']==$plan_array[$j]['amount']){
                                    if($processing_fee_type==1){
                                        $parent_net_sales += $plan_array[$j]['to_refund']-$trfee;
                                        $event_refund_sales += $plan_array[$j]['to_refund']-$trfee;
                                    }else{
                                        $parent_net_sales += $plan_array[$j]['to_refund'];
                                        $event_refund_sales += $plan_array[$j]['to_refund'];
                                    }
                                }else{
                                    if($processing_fee_type==1){
                                        $parent_net_sales += $plan_array[$j]['to_refund']-$rfee;
                                        $event_refund_sales += $plan_array[$j]['to_refund']-$rfee;
                                    }else{
                                        $parent_net_sales += $plan_array[$j]['to_refund'];
                                        $event_refund_sales += $plan_array[$j]['to_refund'];
                                    }
                                }
                            }else{
                                $parent_net_sales += $plan_array[$j]['to_refund'];
                                $event_refund_sales += $plan_array[$j]['to_refund'];
                            }
                            $update_refund_sales_flag = 1;
                        }
                    }else{
                        $stop_wepay = false;
                        if($plan_array[$j]['status']=='M'){
                            $manual_refund_amount += $plan_array[$j]['to_refund'];
                        }
                        $p1_fee = $payment_amount*($process_fee_per/100) + $process_fee_val;
                        $p2_fee = ($payment_amount + $p1_fee)*($process_fee_per/100) + $process_fee_val;
                        if($processing_fee_type==1){
                            $fee = number_format((float)($p1_fee), 2, '.', '');
                        }else{
                            $fee = number_format((float)($p2_fee), 2, '.', '');
                        }
                    }
                    if($update_zero_cost_payment_record == 1 && count($plan_array)==1 && $payment_amount==0){
                        $update_plan = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                        $result_update = mysqli_query($this->db, $update_plan);
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $update_plan");
                        log_info($this->json($error_log));
                        if(!$result_update){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $update_plan");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }

                    if($plan_array[$j]['edit_status']=='D'){
                        $update_plan = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                        $result_update = mysqli_query($this->db, $update_plan);
                        if(!$result_update){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "4- $update_plan");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                        
                        $curr_date_time = gmdate("Y-m-d H:i:s");
                        $activity_text = "Cancelled payment $wp_currency_symbol".$plan_array[$j]['amount']."($cc_name)";
                
                        $insert_payment_history1 = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $plan_array[$j]['event_payment_id'], 'refund', mysqli_real_escape_string($this->db, $activity_text), $curr_date_time);
                        $result_insert_payment_history1 = mysqli_query($this->db, $insert_payment_history1);
                        if (!$result_insert_payment_history1) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $insert_payment_history1");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }elseif($plan_array[$j]['edit_status']=='U'){
                        if(isset($plan_array[$j]['to_refund']) && $plan_array[$j]['to_refund']>0){
    //                        $event_refund_sales += $plan_array[$j]['to_refund'];
    //                        $update_refund_sales_flag = 1;
    //                        $get_payment_query = sprintf("SELECT `checkout_id`,`checkout_status`, `refunded_amount`, `processing_fee` FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
    //                        $result_payment_query = mysqli_query($this->db,$get_payment_query);
    //                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "4- $get_payment_query");
    //                        log_info($this->json($error_log));
    //                        if(!$result_payment_query){
    //                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "4- $get_payment_query");
    //                            log_info($this->json($error_log));
    //                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
    //                            $this->response($this->json($error), 200);
    //                        }else{
    //                            $row_res = mysqli_fetch_assoc($result_payment_query);
    //                            $payment_id = $row_res['checkout_id'];
    //                            $payment_status = $row_res['checkout_status'];
    //                            $db_refunded_amt = $row_res['refunded_amount'];
    //                            $db_processing_fee = $row_res['processing_fee'];
    //                        }
                            if($plan_array[$j]['to_refund']==$plan_array[$j]['amount']){
                                if(empty(trim($payment_id)) && empty(trim($payment_status))){
                                    $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                    $result_update_query2 = mysqli_query($this->db, $update_query2);
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "5- $update_query2");
                                    log_info($this->json($error_log));
                                    if(!$result_update_query2){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "5- $update_query2");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    }
                                }else{
                                    if(!$stop_wepay){
                                        if($payment_status=='new' || $payment_status=='authorized' || $payment_status=='reserved'){
                                            $sns_msg8['call'] = "Checkout";
                                            $sns_msg8['type'] = "Checkout Cancel";
                                            $call_method = "checkout/cancel";
                //                            $json = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","checkout_id"=>$payment_id,"cancel_reason"=>"Participant ");
                                            $json = array("checkout_id"=>$payment_id,"cancel_reason"=>"Participant ");
                                        }elseif($payment_status=='released' || $payment_status=='captured'){
                                            $sns_msg8['call'] = "Checkout";
                                            $sns_msg8['type'] = "Checkout Refund";
                                            $call_method = "checkout/refund";
                                            $json = array("checkout_id"=>$payment_id,"refund_reason"=>"Participant ");
                                        }else{
                                            $error = array("status" => "Failed", "msg" => "Participant payment status is in $payment_status status.");
                                            $this->response($this->json($error),200);
                                        }

                                        $postData = json_encode($json);
                                        $response = $this->wp->accessWepayApi($call_method,'POST',$postData,$token,$user_agent,$sns_msg8);
                                        $this->wp->log_info("wepay_checkout_refund: ".$response['status']);
                                    }
                                    if((isset($response['status']) && $response['status']=="Success") || $stop_wepay){
                                        $total_refund_amount += $plan_array[$j]['to_refund'];
                                        $paid_amount -= $payment_amount;
            //                            $res = $response['msg'];
                                        $state = $response['msg']['state'];
                                        
                                        $insert_new_refunded_query = sprintf("INSERT INTO `event_payment`( `event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `schedule_date`, `schedule_status`, `cc_id`, `cc_name`, `created_dt`, `last_updt_dt`) 
                                                    SELECT `event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `schedule_date`, 'FR', `cc_id`, `cc_name`, `created_dt`, '$old_last_updt_dt' FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                        $result_insert_new_refunded_query = mysqli_query($this->db, $insert_new_refunded_query);
                                        if (!$result_insert_new_refunded_query) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $insert_new_refunded_query");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 200);
                                        }
                                        
                                        if($state=='cancelled'){
                                            $status = "C";
                                            $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `refunded_amount`='%s' WHERE `event_payment_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $plan_array[$j]['to_refund']), mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                        }elseif($state=='refunded'){
                                            $status = "R";
                                            $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `refunded_amount`='%s', `refunded_date`='$curr_date' WHERE `event_payment_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $plan_array[$j]['to_refund']), mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                        }

                                        $result_update_query2 = mysqli_query($this->db, $update_query2);
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "6- $update_query2");
                                        log_info($this->json($error_log));
                                        if(!$result_update_query2){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "6- $update_query2");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 200);
                                        }else{
                                            $ref_amount=0;
                                            if ($processing_fee_type == 1) {
                                                $ref_amount = $plan_array[$j]['to_refund'];
//                                                 $ref_amount = $plan_array[$j]['to_refund'];
                                            } else {
                                                $ref_amount = $plan_array[$j]['to_refund']+ $db_processing_fee;
                                            }
                                            
                                            $curr_date_time = gmdate("Y-m-d H:i:s");
                                            $activity_text = "Refunded $wp_currency_symbol".$ref_amount." ($cc_name)";
                
                                            $insert_payment_history2 = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $plan_array[$j]['event_payment_id'], 'refund', mysqli_real_escape_string($this->db, $activity_text), $curr_date_time);
                                            $result_insert_payment_history2 = mysqli_query($this->db, $insert_payment_history2);
                                            if (!$result_insert_payment_history2) {
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $insert_payment_history2");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            }
                                        }
                                    }else{
                                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
                                        $this->response($this->json($log),200);
                                    }
                                }

    //                            $update_plan = sprintf("UPDATE `event_payment` SET `schedule_status`='R' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
    //                            $result_update = mysqli_query($this->db, $update_plan);
    //                            if(!$result_update){
    //                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_plan");
    //                                log_info($this->json($error_log));
    //                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
    //                                $this->response($this->json($error), 200);
    //                            }
                            }elseif($plan_array[$j]['to_refund']<$plan_array[$j]['amount']){

                                if(empty(trim($payment_id)) && empty(trim($payment_status))){
                                    $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                    $result_update_query2 = mysqli_query($this->db, $update_query2);
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "7- $update_query2");
                                    log_info($this->json($error_log));
                                    if(!$result_update_query2){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "7- $update_query2");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    }
                                }else{
                                    $make_new_cancel_payment_record = 1;
                                    $payment_refunded = 0;
                                    $refund = $plan_array[$j]['to_refund'];
    //                                $payment_amount = $plan_array[$j]['amount'];
                                    $temp_payment_amount = $plan_array[$j]['amount']-$db_refunded_amt;
    //                                $p1_fee = $temp_payment_amount*($process_fee_per/100) + $process_fee_val;
    //                                $p2_fee = ($temp_payment_amount + $p1_fee)*($process_fee_per/100) + $process_fee_val;
    //                                if($processing_fee_type==1){
    //                                    $fee = number_format((float)($p1_fee), 2, '.', '');
    //                                }else{
    //                                    $fee = number_format((float)($p2_fee), 2, '.', '');
    //                                }
                                    $fee = $db_processing_fee;
                                    if(isset($plan_array[$j]['to_refund']) && $plan_array[$j]['to_refund']>0){
                                        $temp_amount = $temp_payment_amount - $plan_array[$j]['to_refund'];
                                        if($temp_amount>0){
                                            $r1_fee = $temp_amount*($process_fee_per/100) + $process_fee_val;
                                            $r2_fee = ($temp_amount + $r1_fee)*($process_fee_per/100) + $process_fee_val;
                                            if($processing_fee_type==1){
                                                $trfee = number_format((float)($r1_fee), 2, '.', '');
                                            }else{
                                                $trfee = number_format((float)($r2_fee), 2, '.', '');
                                            }
                                            $rfee = $fee - $trfee;
                                        }else{
                                            $rfee = $fee;
                                        }
                                    }
//                                    $paid_amount -= $refund;
                                    $r_amt = 0;
                                    $rem_amt = $temp_payment_amount-$refund;
                                    if($processing_fee_type==1){
                                        $r_amt = $refund;
                                    }else{
                                        $r_amt = $refund+$rfee;
                                    }
                                    if(!$stop_wepay){
                                        if($payment_status=='new' || $payment_status=='authorized' || $payment_status=='reserved'){
                                            $sns_msg8['call'] = "Checkout";
                                            $sns_msg8['type'] = "Checkout Cancel";
                                            $call_method = "checkout/cancel";
                //                            $json = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","checkout_id"=>$payment_id,"cancel_reason"=>"Participant ");
                                            $json = array("checkout_id"=>$payment_id,"cancel_reason"=>"Participant ");
                                        }elseif($payment_status=='released' || $payment_status=='captured'){
                                            $paid_amount -= $refund;
                                            $sns_msg8['call'] = "Checkout";
                                            $sns_msg8['type'] = "Checkout Refund";
                                            $call_method = "checkout/refund";
                                            $json = array("checkout_id"=>$payment_id,"amount"=>$r_amt,"app_fee"=>$rfee,"refund_reason"=>"Participant ");
                                        }else{
                                            $error = array("status" => "Failed", "msg" => "Participant payment status is in $payment_status status.");
                                            $this->response($this->json($error),200);
                                        }

                                        $postData = json_encode($json);
                                        $response = $this->wp->accessWepayApi($call_method,'POST',$postData,$token,$user_agent,$sns_msg8);
                                        $this->wp->log_info("wepay_checkout_refund: ".$response['status']);
                                    }
                                    if((isset($response['status']) && $response['status']=="Success") || $stop_wepay){
            //                            $res = $response['msg'];
                                        $state = $response['msg']['state'];

                                        $json2 = array("checkout_id"=>"$payment_id");
                                        $postData2 = json_encode($json2);
                                        $sns_msg8['call'] = "Checkout";
                                        $sns_msg8['type'] = "Checkout details";
                                        $response2 = $this->wp->accessWepayApi("checkout",'POST',$postData2,$token,$user_agent,$sns_msg8);
                                        $this->wp->log_info("wepay_checkout_get: ".$response2['status']);
                                        if($response2['status']=="Success"){
                                            $res2 = $response2['msg'];
                                            $checkout_id = $res2['checkout_id'];
                                            $checkout_state = $res2['state'];
                                        }else{
                                            $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                            $this->response($this->json($error),200);
                                        }

                                        $total_refund_amount += $plan_array[$j]['to_refund'];
                                        if($response2['msg']['refund']['amount_refunded']>0){
                                            if($processing_fee_type==1){
                                                $payment_refunded = $response2['msg']['refund']['amount_refunded'];
                                            }else{
                                                $payment_refunded = $response2['msg']['refund']['amount_refunded']-($db_refunded_processing_fee+$rfee);
                                            }
                                        }
                                        if($state=='cancelled'){
                                            $rand = mt_rand(1,99999);
                                            $ip = $this->getRealIpAddr();
                                            $status = "C";
                                            $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout";
                                            $unique_id = $company_id."_".$event_name."_".$ip."_".$rand."_".time();
                                            $date = date("Y-m-d_H:i:s");
                                            $ref_id = $company_id."_".$event_name."_".$date;
    //                                            if($processing_fee_type==2){
    //                                                $fee_payer = "payer_from_app";
    //                                            }elseif($processing_fee_type==1){
                                                    $fee_payer = "payee_from_app";
    //                                            }

                                            if ($processing_fee_type == 2) {
                                                $w_rem_amt = $rem_amt + $trfee;
                                            } elseif ($processing_fee_type == 1) {
                                                $w_rem_amt = $rem_amt;
                                            }
                                            $time = time();
                                            $json2 = array("account_id"=>"$account_id","short_description"=>"$event_name","type"=>"event","amount"=>"$w_rem_amt","currency"=>"$currency",
                                                    "fee"=>array("app_fee"=>$trfee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                                                    "email_message"=>array("to_payee"=>"Payment has been added for $event_name","to_payer"=>"Payment has been made for $event_name"),
                                                    "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                                                    "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                                        array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                                        array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))),
                                                    "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                                        "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$w_rem_amt, "quantity"=>1, "amount"=>$w_rem_amt, "currency"=>"$currency"))))));
                                                $postData2 = json_encode($json2);
                                                $sns_msg8['call'] = "Checkout";
                                                $sns_msg8['type'] = "Checkout Creation";
                                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg8);
                                                $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                                                if($response2['status']=="Success"){
                                                    $res2 = $response2['msg'];
                                                    $checkout_id = $res2['checkout_id'];
                                                    $checkout_state = $res2['state'];
    //                                                $parent_net_sales += $rem_amt+$trfee;
    //                                                $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_rem_amt, $currency);
                                                }else{
                                                    if($response2['msg']['error_code']==1008){
                                                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg8);
                                                        $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                                                        if($response2['status']=="Success"){
                                                            $res2 = $response2['msg'];
                                                            $checkout_id = $res2['checkout_id'];
                                                            $checkout_state = $res2['state'];
    //                                                        $parent_net_sales += $rem_amt+$trfee;
    //                                                        $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_rem_amt, $currency);
                                                        }else{
                                                            if($response2['msg']['error_code']==1008){
                                                                $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                                                $this->response($this->json($error),200);
                                                            }else{
                                                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                                                $this->response($this->json($error),200);
                                                            }
                                                        }
                                                    }else{                                                
                                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                                        $this->response($this->json($error),200);
                                                    }
                                                }

                                                $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `processing_fee`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                                        mysqli_real_escape_string($this->db,$event_reg_id), mysqli_real_escape_string($this->db,$student_id), mysqli_real_escape_string($this->db,$checkout_id), mysqli_real_escape_string($this->db,$checkout_state), mysqli_real_escape_string($this->db,$rem_amt),
                                                        mysqli_real_escape_string($this->db,$curr_date), mysqli_real_escape_string($this->db,'S'), mysqli_real_escape_string($this->db,$trfee), mysqli_real_escape_string($this->db,$cc_id), mysqli_real_escape_string($this->db,$cc_name));
                                                $result_insert = mysqli_query($this->db, $insert_payment);
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "8- $insert_payment");
                                                log_info($this->json($error_log));
                                                if (!$result_insert) {
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "8- $insert_payment");
                                                    log_info($this->json($error_log));
                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                    $this->response($this->json($error), 200);
                                                }
                                        }elseif($state=='refunded'){
                                            $status = "R";
                                            $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `refunded_date`, `processing_fee`, `cc_id`, `cc_name`)
                                                    SELECT `event_reg_id`, `student_id`, `checkout_id`, 'refunded', $refund, `schedule_date`, 'R', '$curr_date', $rfee, $cc_id, '$cc_name'  FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                            $result_insert_query = mysqli_query($this->db, $insert_payment);
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "9- $insert_payment");
                                            log_info($this->json($error_log));
                                            if(!$result_insert_query){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "9- $insert_payment");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            }
                                        }elseif($payment_refunded>0){
                                            $status = "PR";
                                            $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `refunded_date`, `processing_fee`, `cc_id`, `cc_name`)
                                                    SELECT `event_reg_id`, `student_id`, `checkout_id`, 'refunded', $refund, `schedule_date`, 'R', '$curr_date', $rfee, $cc_id, '$cc_name'  FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                            $result_insert_query = mysqli_query($this->db, $insert_payment);
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "10- $insert_payment");
                                            log_info($this->json($error_log));
                                            if(!$result_insert_query){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "10- $insert_payment");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            }
                                        }

                                        $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`=IF($temp_amount=0,'CR','%s'), `refunded_amount`=$payment_refunded, `refunded_date`='$curr_date', `processing_fee`=IF($temp_amount=0,0,'$trfee') WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                        $result_update_query2 = mysqli_query($this->db, $update_query2);
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "11- $update_query2");
                                        log_info($this->json($error_log));
                                        if(!$result_update_query2){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "11- $update_query2");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 200);
                                        }
                                        if ($processing_fee_type == 1) {
                                                $ref_amount1 = $plan_array[$j]['to_refund'];
                                            } else {
                                                $ref_amount1 = $plan_array[$j]['to_refund']+ $rfee;
                                            }
                                            $curr_date_time = gmdate("Y-m-d H:i:s");
                                            $activity_text = "Refunded $wp_currency_symbol".$ref_amount1." ($cc_name)";
                
                                            $insert_payment_history2 = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $plan_array[$j]['event_payment_id'], 'refund', mysqli_real_escape_string($this->db, $activity_text), $curr_date_time);
                                            $result_insert_payment_history2 = mysqli_query($this->db, $insert_payment_history2);
                                            if (!$result_insert_payment_history2) {
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $insert_payment_history2");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            }
                                    }else{
                                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
                                        $this->response($this->json($log),200);
                                    }
                                }
                            }
                        }else{
                            if(isset($plan_array[$j]['event_payment_id']) && !empty($plan_array[$j]['event_payment_id'])){
                                $update_plan = sprintf("UPDATE `event_payment` SET `payment_amount`='%s', `schedule_status`='%s', `processing_fee`='%s' WHERE `event_payment_id`='%s'", 
                                        mysqli_real_escape_string($this->db,$plan_array[$j]['amount']), mysqli_real_escape_string($this->db,$plan_array[$j]['status']), mysqli_real_escape_string($this->db,$plan_array[$j]['process_fee']), mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                                $result_update = mysqli_query($this->db, $update_plan);
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "12- $update_plan");
                                log_info($this->json($error_log));
                                if(!$result_update){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "12- $update_plan");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
                    }elseif($plan_array[$j]['edit_status']=='N' && empty($plan_array[$j]['event_payment_id']) && $plan_array[$j]['status']=='N'){
                        $checkout_id = $checkout_state = '';
                        if($payment_amount>0){
                            if((isset($plan_array[$j]['credit_method'])&&$plan_array[$j]['credit_method']=='CC') || !isset($plan_array[$j]['credit_method'])){
                                if($processing_fee_type==1){
                                    $event_refund_sales -= ($payment_amount-$fee);
                                    $parent_net_sales -= ($payment_amount-$fee);
                                }else{
                                    $event_refund_sales -= $payment_amount;
                                    $parent_net_sales -= $payment_amount;
                                }
                                $update_refund_sales_flag = 1;
                                $rand = mt_rand(1,99999);
                                $ip = $this->getRealIpAddr();
                                $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout";
                                $unique_id = $company_id."_".$event_name."_".$ip."_".$rand."_".time();
                                $date = date("Y-m-d_H:i:s");
                                $curr_date = date("Y-m-d");
                                $ref_id = $company_id."_".$event_name."_".$date;
        //                        if($processing_fee_type==2){
        //                            $fee_payer = "payer_from_app";
        //                        }elseif($processing_fee_type==1){
                                    $fee_payer = "payee_from_app";
        //                        }

                                if ($processing_fee_type == 2) {
                                    $w_payment_amount = $payment_amount + $fee;
                                } elseif ($processing_fee_type == 1) {
                                    $w_payment_amount = $payment_amount;
                                }
                                $time = time();
                                $json2 = array("account_id"=>"$account_id","short_description"=>"$event_name","type"=>"event","amount"=>"$w_payment_amount","currency"=>"$currency",
                                    "fee"=>array("app_fee"=>$fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                                    "email_message"=>array("to_payee"=>"Payment has been added for $event_name","to_payer"=>"Payment has been made for $event_name"),
                                    "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                                    "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                        array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                        array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))),
                                    "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                        "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$w_payment_amount, "quantity"=>1, "amount"=>$w_payment_amount, "currency"=>"$currency"))))));
                                $postData2 = json_encode($json2);
                                $sns_msg8['call'] = "Checkout";
                                $sns_msg8['type'] = "Checkout Creation";
                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg8);
                                $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                                if($response2['status']=="Success"){
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
        //                            $paid_amount += $payment_amount;
                                    $update_event_remaining_dues = 1;
        //                            $update_event_amount = $payment_amount;
        //                            $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_payment_amount, $currency);
                                }else{
                                    if($response2['msg']['error_code']==1008){
                                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg8);
                                        $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                                        if($response2['status']=="Success"){
                                            $res2 = $response2['msg'];
                                            $checkout_id = $res2['checkout_id'];
                                            $checkout_state = $res2['state'];
        //                                    $paid_amount += $payment_amount;
                                            $update_event_remaining_dues = 1;
        //                                    $update_event_amount = $payment_amount;
        //                                    $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_payment_amount, $currency);
                                        }else{
                                            if($response2['msg']['error_code']==1008){
                                                $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                                $this->response($this->json($error),200);
                                            }else{
                                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                                $this->response($this->json($error),200);
                                            }
                                        }
                                    }else{                                                
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }

                                $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `processing_fee`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db,$event_reg_id), mysqli_real_escape_string($this->db,$student_id), mysqli_real_escape_string($this->db,$checkout_id), mysqli_real_escape_string($this->db,$checkout_state), mysqli_real_escape_string($this->db,$payment_amount),
                                        mysqli_real_escape_string($this->db,$curr_date), mysqli_real_escape_string($this->db,'S'), mysqli_real_escape_string($this->db,$fee), mysqli_real_escape_string($this->db,$cc_id), mysqli_real_escape_string($this->db,$cc_name));
                                $result_insert = mysqli_query($this->db, $insert_payment);
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "13- $insert_payment");
                                log_info($this->json($error_log));
                                if (!$result_insert) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "13- $insert_payment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }else{
                                $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `payment_amount`, `schedule_date`, `schedule_status`, `credit_method`) VALUES ('%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db,$event_reg_id), mysqli_real_escape_string($this->db,$student_id), mysqli_real_escape_string($this->db,$payment_amount),
                                        mysqli_real_escape_string($this->db,$curr_date), mysqli_real_escape_string($this->db,'M'), mysqli_real_escape_string($this->db,$plan_array[$j]['credit_method']));
                                $result_insert = mysqli_query($this->db, $insert_payment);
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "13- $insert_payment");
                                log_info($this->json($error_log));
                                if (!$result_insert) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "13- $insert_payment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                                $update_event_remaining_dues = 1;
                                $parent_net_sales -= $payment_amount;
                                $event_refund_sales -= $payment_amount;
                                $paid_amount += $payment_amount;
                                $update_event_amount = $payment_amount;
                            }
                        }
                    }elseif($plan_array[$j]['edit_status']=='MU'){
                        if(isset($plan_array[$j]['to_refund']) && $plan_array[$j]['to_refund']>0){
                            $credit_method_2 = $plan_array[$j]['credit_method'];
                            $method_str = "";
                            if($credit_method_2=='CA'){
                                $method_str = " (Cash)";
                            }elseif($credit_method_2=='CH'){
                                $method_str = " (Check)";
                            }elseif($credit_method_2=='MC'){
                                $method_str = " (Credit)";
                            }
                            $update_event_remaining_dues = 1;
                            $update_refund_sales_flag = 1;
                            $m_refund = $plan_array[$j]['to_refund'];
                            $total_refund_amount += $m_refund;
                            $paid_amount -= $m_refund;
                            $parent_net_sales += $plan_array[$j]['to_refund'];
                            $event_refund_sales += $plan_array[$j]['to_refund'];
                            if($plan_array[$j]['to_refund']==$plan_array[$j]['amount']){
                                $s_status = 'MF';
                                $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `refunded_date`, `credit_method`, `check_number`)
                                        SELECT `event_reg_id`, `student_id`, `checkout_id`, 'manual refunded', $m_refund, `schedule_date`, 'MR', '$curr_date', `credit_method`, `check_number` FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                $result_insert_query = mysqli_query($this->db, $insert_payment);
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "14- $insert_payment");
                                log_info($this->json($error_log));
                                if(!$result_insert_query){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "14- $insert_payment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }else{
                                $s_status = 'MP';
                                $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `refunded_date`, `credit_method`, `check_number`)
                                        SELECT `event_reg_id`, `student_id`, `checkout_id`, 'manual refunded', $m_refund, `schedule_date`, 'MR', '$curr_date', `credit_method`, `check_number` FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                $result_insert_query = mysqli_query($this->db, $insert_payment);
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "14- $insert_payment");
                                log_info($this->json($error_log));
                                if(!$result_insert_query){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "14- $insert_payment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                            $update_plan = sprintf("UPDATE `event_payment` SET `schedule_status`='$s_status', `refunded_amount`=`refunded_amount`+$m_refund WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                            $result_update = mysqli_query($this->db, $update_plan);
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "15- $update_plan");
                            log_info($this->json($error_log));
                            if(!$result_update){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "15- $update_plan");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                            
                            $activity_text = "Refunded $wp_currency_symbol".$m_refund.$method_str;
                            $insert_payment_history2 = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s',NOW())",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $plan_array[$j]['event_payment_id'], 'refund', mysqli_real_escape_string($this->db, $activity_text));
                            $result_insert_payment_history2 = mysqli_query($this->db, $insert_payment_history2);
                            if (!$result_insert_payment_history2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "15- $insert_payment_history2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                    }
                }
            }
        }
        if(count($event_array)>0){
            $update_event_amount2 = $update_event_amount;
            for($i=0; $i<count($event_array); $i++){
                if($event_array[$i]['state']=='U'){
                    $c_qty = $make_new_cancel_record = $old_qty = 0;
                    if(isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
                        $c_qty = $event_array[$i]['cancelled_quantity'];
                    }
                    if($event_array[$i]['quantity']==0){
                        $old_qty = 0;
                        if($event_array[$i]['discounted_price']==0 && $payment_type=='F'){
                            $update_zero_cost_payment_record = 1;
                        }
                        $status = 'CP';
                        $event_array[$i]['payment_amount'] = $event_array[$i]['discounted_price']*$c_qty;
                        $event_array[$i]['quantity']=$c_qty;
                    }else{
                        $old_qty = $event_array[$i]['quantity']+$event_array[$i]['cancelled_quantity'];
                        if($c_qty>0){
                            $make_new_cancel_record = 1;
                        }
                        if($event_array[$i]['payment_amount']>0){
                            if($event_array[$i]['paid_due']==$event_array[$i]['payment_amount']){
                                if($payment_type=='CO'){
                                    $status = 'CC';
                                }else{
                                    $status = 'RC';
                                }
                            }else{
                                if($event_array[$i]['rem_due']>0){
                                    $status = 'RP';
                                }else{
                                    if($payment_type=='CO'){
                                        $status = 'CC';
                                    }else{
                                        $status = 'RC';
                                    }
                                }
                            }
                        }else{
                            $status = 'CC';
                        }
                    }
                    
                    if($make_new_cancel_record==1){
                        $insert_event = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`) 
                                  SELECT `event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, (`event_cost`-`discount`)*$c_qty, $c_qty, IF(`payment_status`='CC' || `payment_status`='RC', IF(`payment_amount`>0,'RF','CP'), 'CP') FROM `event_reg_details` WHERE `event_reg_details_id`='%s'",
                            mysqli_real_escape_string($this->db,$event_array[$i]['event_reg_details_id']));
                        $result_insert_event = mysqli_query($this->db, $insert_event);
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "16- $insert_event");
                        log_info($this->json($error_log));
                        if(!$result_insert_event){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "16- $insert_event");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    $temp_rem_due = $event_array[$i]['rem_due'];
                    if($update_event_remaining_dues==1 && $update_event_amount>0 && $old_qty>0){
                        if($temp_rem_due>$update_event_amount){
                            $temp_rem_due = $temp_rem_due-$update_event_amount;
                            $update_event_amount=0;
                            $status = 'RP';
                        }else{
                            $update_event_amount = $update_event_amount-$temp_rem_due;
                            $temp_rem_due=0;
                            $status = 'RC';
                        }
                    }
                    log_info(json_encode($event_array[$i]));
                    $man_calc = $event_array[$i]['manual_payment_amount']-$event_array[$i]['old_manual_payment_amount'];
                    $update_event = sprintf("UPDATE `event_reg_details` SET `payment_amount`='%s',`manual_payment_amount`=`manual_payment_amount`+'%s',`quantity`='%s',`balance_due`='%s',`payment_status`=IF('$status'!='CP', '%s', IF(`payment_status`='CC' || `payment_status`='RC', IF(`payment_amount`>0,'RF','CP'), 'CP')) WHERE `event_reg_details_id`='%s'",
                        mysqli_real_escape_string($this->db,$event_array[$i]['payment_amount']), mysqli_real_escape_string($this->db,$man_calc), mysqli_real_escape_string($this->db,$event_array[$i]['quantity']), 
                        mysqli_real_escape_string($this->db,$temp_rem_due), mysqli_real_escape_string($this->db,$status), mysqli_real_escape_string($this->db,$event_array[$i]['event_reg_details_id']));
                    $result_update_event = mysqli_query($this->db, $update_event);
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "17- $update_event");
                    log_info($this->json($error_log));
                    if(!$result_update_event){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "17- $update_event");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        $parent_id = 0;
        if(count($event_array)>0){
            $child_rem_manual_amt = 0;
            $temp_manual_refund_amount=$manual_refund_amount;
            for($i=0; $i<count($event_array); $i++){
                $current_rem_val = 0;
                if($event_array[$i]['quantity']==0){
                    $old_qty = 0;
                }else{
                    $old_qty = $event_array[$i]['quantity']+$event_array[$i]['cancelled_quantity'];
                }
                $temp_rem_due = $event_array[$i]['rem_due'];
                if($update_event_remaining_dues==1 && $update_event_amount2>0 && $old_qty>0){
                    if($temp_rem_due>$update_event_amount2){
                        $current_rem_val = $temp_rem_due-$update_event_amount2;
                    }else{
                        $update_event_amount2 = $update_event_amount2-$temp_rem_due;
                        $current_rem_val = $temp_rem_due;
                    }
                }
                if($event_array[$i]['state']=='U' && isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
                    $parent_str = $parent_sales_str = "";
                    $net_sales = 0;
                    $c_qty = $event_array[$i]['cancelled_quantity'];
                    $event_id = $event_array[$i]['ev_id'];
                    $event_list = "$event_id";
                    
                    if($update_refund_sales_flag == 1){
                        if($event_refund_sales>0){
                            $temp_event_amount = $event_array[$i]['cancelled_quantity']*$event_array[$i]['discounted_price'];
                            if($event_refund_sales>$temp_event_amount){
                                $net_sales = $temp_event_amount;
                                $event_refund_sales = $event_refund_sales - $temp_event_amount;
                                $temp_event_amount = 0;
                            }else{
                                $net_sales = $event_refund_sales;
                                $temp_event_amount = $temp_event_amount - $event_refund_sales;
                                $event_refund_sales = 0;
                            }
                        }else{
                            $net_sales = $event_refund_sales;
                            $event_refund_sales = 0;
                        }
                    }
                    
                    if(isset($event_array[$i]['event_parent_id']) && !empty($event_array[$i]['event_parent_id'])){
                        $parent_id = $event_array[$i]['event_parent_id'];
//                        $event_list .= ", $parent_id";
//                        $parent_str = "WHEN '$parent_id' THEN  `registrations_count`-$c_qty ";
//                        $parent_sales_str = "WHEN '$parent_id' THEN  `net_sales`-$parent_net_sales ";
                    }
                    if(count($event_array)==1){
                        $update_event_count = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`-$c_qty, `net_sales` = IF('$cancel_flag'='WR' || '$cancel_flag'='C' || '$cancel_flag'='NP', `net_sales`-$net_sales, `net_sales`)
                                WHERE `event_id` IN (%s)", $event_list);
                        $result_event_count = mysqli_query($this->db, $update_event_count);                    
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "18- $update_event_count");
                        log_info($this->json($error_log));
                        if(!$result_event_count){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "18- $update_event_count");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }else{
                        $child_rem_due = ($event_array[$i]['old_paid_due'] - $event_array[$i]['paid_due']) - ($event_array[$i]['old_manual_payment_amount']-$event_array[$i]['manual_payment_amount']);
                        $update_event_count = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`-$c_qty, `net_sales` = IF('$cancel_flag'='WR' || '$cancel_flag'='C' || '$cancel_flag'='NP', `net_sales`-$child_rem_due+$current_rem_val, `net_sales`)
                                WHERE `event_id` IN (%s)", $event_list);
                        $result_event_count = mysqli_query($this->db, $update_event_count);                    
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "19- $update_event_count");
                        log_info($this->json($error_log));
                        if(!$result_event_count){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "19- $update_event_count");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }else{
//                    if($event_array[$i]['old_paid_due'] != $event_array[$i]['paid_due'] || $event_array[$i]['old_manual_payment_amount'] != $event_array[$i]['manual_payment_amount']){
                    $child_rem_due = ($event_array[$i]['old_paid_due'] - $event_array[$i]['paid_due']) - ($event_array[$i]['old_manual_payment_amount']-$event_array[$i]['manual_payment_amount']);
                    $update_event_count = sprintf("UPDATE `event` SET `net_sales` = IF('$cancel_flag'='WR' || '$cancel_flag'='C' || '$cancel_flag'='NP', `net_sales`-$child_rem_due+$current_rem_val, `net_sales`)
                            WHERE `event_id` IN (%s)", $event_array[$i]['ev_id']);
                    $result_event_count = mysqli_query($this->db, $update_event_count);                    
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "20- $update_event_count");
                    log_info($this->json($error_log));
                    if(!$result_event_count){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "20- $update_event_count");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        if($parent_id!=0){
            $update_event_count = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`-$parent_qty,`net_sales` = IF('$cancel_flag'='WR' || '$cancel_flag'='C' || '$cancel_flag'='NP', `net_sales`-$parent_net_sales, `net_sales`)
                            WHERE `event_id` IN (%s)", $parent_id);
            $result_event_count = mysqli_query($this->db, $update_event_count);                    
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "21- $update_event_count");
            log_info($this->json($error_log));
            if(!$result_event_count){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "21- $update_event_count");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        if($total_due>0){
//            $paid_text = $refund_text = "";
//            if($paid_amount>0){
                if($payment_type=='CO'){
                    $paid_text = ", `paid_amount`=$total_due";
                }else{
                    $paid_text = ", `paid_amount`=`paid_amount`+ $paid_amount";
                }
                    $refund_text = ", `refunded_amount` = `refunded_amount` + $total_refund_amount";
//                }
//            }
            $update_reg = sprintf("UPDATE `event_registration` SET `payment_amount`='%s' %s %s WHERE `event_reg_id`='%s'", mysqli_real_escape_string($this->db,$total_due), $paid_text, $refund_text, mysqli_real_escape_string($this->db,$event_reg_id));
            $result_update_reg = mysqli_query($this->db, $update_reg);
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "22- $update_reg");
            log_info($this->json($error_log));
            if (!$result_update_reg) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "22- $update_reg");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        if(count($event_array)>0){
            $parent_title = '';
            $curr_date = gmdate("Y-m-d H:i:s");
            for($i=0; $i<count($event_array); $i++){
                if($i==0){
                    if(!empty($event_array[$i]['event_parent_id'])||$event_array[$i]['event_parent_id']!='null'){
                        $query_parent_title = sprintf("SELECT event_title FROM `event` WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_array[$i]['event_parent_id']));
                        $result_query_parent_title = mysqli_query($this->db, $query_parent_title);
                        if(!$result_query_parent_title){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else{
                            $num = mysqli_num_rows($result_query_parent_title);
                            if($num>0){
                                $row_title = mysqli_fetch_assoc($result_query_parent_title);
                                $parent_title = $row_title['event_title'];
                            }
                        }
                    }else{
                        $parent_title = $event_array[$i]['title'];
                    }
                }
                if($event_array[$i]['state']=='U'){
                    if(isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
                        if($cancel_flag=='WOR' || $cancel_flag=='CP'){
                            $issue_refund = " (no refund issued)";
                        }else{
                            $issue_refund = " ";
                        }
                        if($payment_type!='F'){
                            $cancellation_text = "Total cancellation cost -$wp_currency_symbol".($event_array[$i]['cancelled_quantity']*$event_array[$i]['discounted_price']).$issue_refund;
                        }else{
                            $cancellation_text = '';
                        }
                        if(!empty($event_array[$i]['event_parent_id'])||$event_array[$i]['event_parent_id']!='null'){
                            $activity_text = "Cancelled $parent_title ".$event_array[$i]['title'].", quantity ".$event_array[$i]['cancelled_quantity'].". $cancellation_text";;
                        }else{
                            $activity_text = "Cancelled ".$event_array[$i]['title'].", quantity ".$event_array[$i]['cancelled_quantity'].". $cancellation_text";
                        }
                        $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", 
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id),
                                    'Cancellation', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                        $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                        if (!$result_insert_event_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                            log_info($this->json($error_log));
                        }
                    }
                }
            }
        }
        if(isset($event_id) && !empty($event_id)){
            $event_details = $this->getEventsDetailByEventId($company_id, $event_id, $event_type, 'live', 1);
            $reg_details = $this->getEventParticipantDetailsForRefund($company_id, $event_id, $event_reg_id, 1);                
            $msg = array("status" => "Success", "msg" => "Cancellation Successfully Processed.", "event_details"=>$event_details, "reg_details" => $reg_details);
        }else{
            $msg = array("status" => "Failed", "msg" => "No changes found.");
        }
        $this->response($this->json($msg), 200);
        date_default_timezone_set($curr_time_zone);
    }


//    public function updateParticipantDetails($company_id,$event_id,$event_reg_id,$pfirst_name,$plast_name) {
//
//        $sql = sprintf("UPDATE `event_registration` SET `event_registration_column_1`='%s', `event_registration_column_2`='%s' WHERE `company_id`='%s' AND `event_reg_id`='%s'", mysqli_real_escape_string($this->db,$pfirst_name), mysqli_real_escape_string($this->db,$plast_name), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_reg_id));
//        $result = mysqli_query($this->db, $sql);
//
//        if (!$result) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//        } else {
//           $this->getEventParticipantDetails($company_id, $event_id);
//        }
//    }
    
    public function sendOrderReceiptForEventPayment($company_id,$event_reg_id, $return_value){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $created_date =" DATE(CONVERT_TZ(ep.created_dt,$tzadd_add,'$new_timezone'))";
       
        $waiver_policies = '';
        $query = sprintf("SELECT er.`company_id`, er.`event_id`, `event_title`, if(ep.`payment_from`='S',ep.`stripe_card_name`,ep.`cc_name`) as cc_name, `waiver_policies`, concat(er.event_registration_column_2, ',', er.event_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `payment_type`, 
            er.`payment_amount` total_due, er.`processing_fee_type`, er.`processing_fee` total_processing_fee, `paid_amount`, ep.`payment_amount`, ep.`processing_fee`, ep.`refunded_amount`, `schedule_date`, `schedule_status`, DATE($created_date) created_dt
            FROM `event_registration` er LEFT JOIN `event_payment` ep ON er.`event_reg_id` = ep.`event_reg_id` AND ep.`schedule_status` IN ('N', 'S', 'PR', 'M')
            LEFT JOIN `event` e ON e.`event_id` = er.`event_id` AND e.`company_id` =  er.`company_id` 
            WHERE er.`event_reg_id` = '%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
            exit();
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
                $event_name = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol =$processing_fee_type= '';
                $inital_stage = $initial_down_pay = 0;
                $current_plan_details = $event_details = $bill_due_details = $payment_details = [];
                $total_due = $paid_due = $rem_due = 0;
                while($row = mysqli_fetch_assoc($result)){
                    $paid_amt = 0;
                    $company_id = $row['company_id'];
                    $event_id = $row['event_id'];
                    $event_name = $row['event_title'];
                    $participant_name = $row['participant_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $temp=[];
                    
                    if($row['processing_fee_type']==2){
                        $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                    }else{
                        $temp['amount'] = $row['payment_amount'];
                    }
//                    $temp['amount'] = $row['payment_amount'];
                    $temp['date'] = $row['schedule_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['edit_status'] = 'N';
                    $temp['schedule_status'] = $row['schedule_status'];
                    $temp['processing_fee'] = $row['processing_fee']; 
                    $temp['cc_name'] = $row['cc_name'];
                    if($row['schedule_status']=='N'){
                        $temp['status'] = 'N';
                    }elseif($row['schedule_status']=='S'){
                        $temp['status'] = 'S';
                        $paid_amt = $temp['amount'];
                        $inital_stage++;
                    }elseif($row['schedule_status']=='PR' || $row['schedule_status']=='M'){
                        $paid_amt = $temp['amount']-$temp['refunded_amount'];
                    }
//                    $total_due = $current_plan_details['total_due'] = $row['total_due'];
//                    $paid_due = $current_plan_details['paid_due'] = $row['paid_amount'];
                    $total_due += $temp['amount'];
                    $current_plan_details['total_due'] = $total_due;
                    $paid_due += $paid_amt;
                    $current_plan_details['paid_due'] = $paid_due;
                    
                    $current_plan_details['success_count'] = $inital_stage;
                    $rem_due = $current_plan_details['rem_due'] = number_format((float)($total_due - $paid_due), 2, '.', '');
//                    $payment_details[] = $temp;
                    if ($row['schedule_status'] == 'N') {
                        $bill_due_details[] = $temp;
                    } elseif ($row['schedule_status'] == 'S') {
                        $payment_details[] = $temp;
                        if ($initial_down_pay > 0) {
                            $bill_due_details[] = $temp;
                        }
                        $initial_down_pay++;
                    } elseif ($row['schedule_status'] == 'M') {
                        $payment_details[] = $temp;
                    }
                }
                
                $query2 = sprintf("SELECT c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`, e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, `event_end_dt`, erd.`event_cost`, `discount`, erd.`payment_amount`, `quantity`, erd.`payment_status`, `balance_due`, s.`student_cc_email`
                    FROM `event` e LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` LEFT JOIN `event_registration` er ON er.`event_reg_id`= erd.`event_reg_id`
                    LEFT JOIN `company` c ON e.`company_id` =  c.`company_id` LEFT JOIN `student` s ON s.`company_id`= er.`company_id` AND s.`student_id`= er.`student_id`
                    WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND erd.`event_reg_id`='%s' AND erd.`payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", 
                        mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                $result2 = mysqli_query($this->db, $query2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    if($return_value==1){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                    exit();
                }else{
                    $temp_quantity = $temp_char_count = 0;
                    while($row2 = mysqli_fetch_assoc($result2)){
                        $temp2 = [];
                        $temp2['title'] = $row2['event_title'];
                        if($temp2['title']>$temp_char_count){
                            $temp_char_count = strlen($row2['event_title']);
                        }
                        
                        if (!empty(trim($row2['event_begin_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_begin_dt']) {
                                $start = " - ";
                            } else {
                                $start = $row2['event_begin_dt'];

                                $split_start = explode(" ", $start);
                                $start_date_value = $split_start[0];
                                $start_time_value = $split_start[1];
                                if ("00:00:00" == $start_time_value) {
                                    $start = date('M d, Y', strtotime($start));
                                } else {
                                    $start = date('M d, Y @ h:i A', strtotime($start));
                                }
                            }
                        }
                        
                        if (!empty(trim($row2['event_end_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_end_dt']) {
                                $end = " - ";
                            } else {
                                $end = $row2['event_end_dt'];

                                $split_end = explode(" ", $end);
                                $end_date_value = $split_end[0];
                                $end_time_value = $split_end[1];
                                if ("00:00:00" == $end_time_value) {
                                    $end = date('M d, Y', strtotime($end));
                                } else {
                                    $end = date('M d, Y @ h:i A', strtotime($end));
                                }
                            }
                        }

                        $temp2['start'] = $start;
                        $temp2['end'] = $end;
                        $temp2['event_cost'] = $row2['event_cost'];
                        $temp2['quantity'] = $row2['quantity'];
                        $temp2['event_disc'] = $row2['discount'];
                        $temp2['discounted_price'] = $row2['event_cost'] - $row2['discount'];
                        $temp_quantity += $row2['quantity'];
                        $temp2['payment_amount'] = $row2['payment_amount'];
                        $temp2['paid_due'] = $temp2['payment_amount'] - $row2['balance_due'];
                        $temp2['rem_due'] = $row2['balance_due'];
                        $temp2['state'] = 'N';
                        $event_details[] = $temp2;
                        $studio_name = $row2['company_name'];
                        $studio_mail = $row2['email_id'];
                        $cc_email_list = $row2['referral_email_list'];
                        $wp_currency_code = $row2['wp_currency_code'];
                        $wp_currency_symbol = $row2['wp_currency_symbol'];
                        $student_cc_email_list = $row2['student_cc_email'];
                    }
                    $current_plan_details['total_event'] = $temp_quantity;
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                log_info($this->json($error_log));
                if($return_value==1){
                    $error = array('status' => "Failed", "msg" => "No records found.");
                    $this->response($this->json($error), 200);
                }
                exit();
            }
        }
        
        $subject = $event_name." Order Confirmation";
        
        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
        $message .= "<b>Event Details</b><br><br>";
        for($i=0;$i<count($event_details);$i++){
            $message .= "Event: ".$event_details[$i]['title']."<br>Starts: ".$event_details[$i]['start']."<br>Ends: ".$event_details[$i]['end']."<br>Participant: ".$participant_name."<br><br>";
        }

        if($total_due>0){
            $message .= "<b>Billing Details</b><br><br>";
            if(count($bill_due_details)>0){
                $message .= "Total due: $wp_currency_symbol".$total_due."<br>";
                $message .= "Balance due: $wp_currency_symbol".$rem_due."<br>";                
                for($j=0;$j<count($bill_due_details);$j++){
                    $x= $j+1;
                    $amount = $bill_due_details[$j]['amount'];
                    $refunded_amount = $bill_due_details[$j]['refunded_amount'];
                    $final_amt = $amount-$refunded_amount;
                    $schedule_status = $bill_due_details[$j]['schedule_status'];
                    $date = date("M dS, Y", strtotime($bill_due_details[$j]['date']));
                    $recurring_processing_fee = $bill_due_details[$j]['processing_fee'];
                    if($schedule_status=='N'){
                        $paid_str = "due";
                    }
                    $recur_pf_str = "";
                    if($processing_fee_type==2){
                        $recur_pf_str = " (plus $wp_currency_symbol".$recurring_processing_fee." administrative fees)";
                    }
                    $message .= "Bill ".$x.": $wp_currency_symbol".$final_amt." ".$paid_str." ".$date." " .$recur_pf_str."  <br>";
                }
                
                if(count($payment_details)>0){
                    $pf_str=$pf_str1 = "";
                    $message.="<br><br><b>Payment Details</b><br><br>";
                    $pr_fee = $payment_details[0]['processing_fee'];
                    if($processing_fee_type==2){
                        $pf_str = " (includes $wp_currency_symbol".$pr_fee." administrative fees)";
                    }
                    $down_pay = $payment_details[0]['amount']-$payment_details[0]['refunded_amount'].$pf_str." paid on ".date("M dS, Y", strtotime($payment_details[0]['date']))."(".$payment_details[0]['cc_name'].")";
                    
                    $message .= "Down Payment: $wp_currency_symbol".$down_pay."<br>";
                    for($k=1;$k<count($payment_details);$k++){
                        $x= $k;                                                
                        $amount = $payment_details[$k]['amount'];
                        $refunded_amount = $payment_details[$k]['refunded_amount'];
                        $final_amt = $amount-$refunded_amount;
                        $schedule_status = $payment_details[$k]['schedule_status'];
                        $date = date("M dS, Y", strtotime($payment_details[$k]['date']));
                        $paid_str = "paid on";
                        $pr_fee1 = $payment_details[$k]['processing_fee'];
                        if($processing_fee_type==2){
                            $pf_str1 = " (includes $wp_currency_symbol".$pr_fee1." administrative fees)";
                        }
                        if($schedule_status=='S'){
                            $pay_type= "(".$payment_details[$k]['cc_name'].")";
                        }else{
                            $pay_type="(manual credit applied)";
                        }
                        $message .= "Payment ".$x.": $wp_currency_symbol".$final_amt." ".$pf_str1." ".$paid_str." ".$date." ".$pay_type." <br>";
                    }
                }
            }else{
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $schedule_status = $payment_details[0]['schedule_status'];
                $final_amount = $amount-$refunded_amount;
                $pr_fee2 = $payment_details[0]['processing_fee'];
                $pf_str2="";
                if($processing_fee_type==2){
                    $pf_str2 = " (includes $wp_currency_symbol".$pr_fee2." administrative fees)";
                }
                $message .= "Total Due: $wp_currency_symbol".$final_amount."<br>Amount Paid: $wp_currency_symbol".$final_amount.$pf_str2."(".$payment_details[0]['cc_name'].")<br>";
            }
            $message .= "<br><br>";
        }
        
        $waiver_present = 0;
        $file = '';
        if(!empty(trim($waiver_policies))){
            $waiver_present = 1;
            $file_name_initialize = 10;
            for($z=0; $z<$file_name_initialize; $z++){
                $dt = time();
                $file = "../../../uploads/".$dt."_waiver_policies.html";        
                if(file_exists($file)){
                    sleep(1);
                    $file_name_initialize++;
                }else{
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }

        $sendEmail_status = $this->sendEmailForEvent($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list,$student_cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            log_info($this->json($error_log));
            if($return_value==1){
                $log = array('status' => "Success", "msg" => "Event order receipt sent successfully.");
                $this->response($this->json($log), 200);
            }
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Event order receipt sent failed.");
                $this->response($this->json($error), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
   
    public function createPDF($company_id,$event_id){
         $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        
        $header_array = array('', 'Participant', 'Buyer', 'Phone');
        $parent_id = $event_type = '';
        $title = $subtitle = $start = $end = $cus_field_1 = $cus_field_2 = $cus_field_3 = $cus_field_4 = "";
        $select_query = sprintf("SELECT e.`parent_id`, e.`event_type`, e.`event_title`, e.`event_category_subtitle`, e.`event_begin_dt`, e.`event_end_dt`, 
            e.`event_registration_column_3` as cus_field_1, e.`event_registration_column_4` as cus_field_2, e.`event_registration_column_5` as cus_field_3, 
            e.`event_registration_column_6` as cus_field_4, e.`event_registration_column_7` as cus_field_5, e.`event_registration_column_8` as cus_field_6, 
            e.`event_registration_column_9` as cus_field_7, e.`event_registration_column_10` as cus_field_8, 
            e2.`event_title` as parent_title, e2.`event_registration_column_3` as custom_field_1, e2.`event_registration_column_4` as custom_field_2, 
            e2.`event_registration_column_5` as custom_field_3, e2.`event_registration_column_6` as custom_field_4 ,e2.`event_registration_column_7` as custom_field_5,
            e2.`event_registration_column_8` as custom_field_6, e2.`event_registration_column_9` as custom_field_7, e2.`event_registration_column_10` as custom_field_8 
            FROM `event` e LEFT JOIN `event` 
            e2 ON e.parent_id = e2.event_id 
            WHERE e.`company_id` = '%s' AND e.`event_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
        $result_select = mysqli_query($this->db, $select_query);
        if(!$result_select){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result_select);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result_select);
                if(!empty($row['parent_id'])){
                    $parent_id = $row['parent_id'];
                }
                $event_type = $row['event_type'];
                if(!empty(trim($row['event_begin_dt']))){
                    if("0000-00-00 00:00:00" == $row['event_begin_dt']){
                        $start = "";
                    } else {
                        $start = $row['event_begin_dt'];
                        
                        $split_start = explode(" ", $start);
                        $start_date_value = $split_start[0];
                        $start_time_value = $split_start[1];
                        if("00:00:00" == $start_time_value){
                            $start = date('M d, Y', strtotime($start));
                            $start = "Start Time: ".$start;
                        } else {
                            $start = date('M d, Y @ h:i A', strtotime($start));
                            $start = "Start Time: ".$start;
                        }
                        
                    }
                }
                
                if(!empty(trim($row['event_end_dt']))){
                    if("0000-00-00 00:00:00" == $row['event_end_dt']){
                        $end = "";
                    } else {
                        $end = $row['event_end_dt'];
                        
                        $split_end = explode(" ", $end);
                        $end_date_value = $split_end[0];
                        $end_time_value = $split_end[1];
                        if("00:00:00" == $end_time_value){
                            $end = date('M d, Y', strtotime($end));
                            $end = "End Time: ".$end;
                        } else {
                            $end = date('M d, Y @ h:i A', strtotime($end));
                            $end = "End Time: ".$end;
                        }
                    }
                }
                
                if($event_type=='S' || $event_type=='M'){
                    
                    $title = $row['event_title'];
                    $subtitle = $row['event_category_subtitle'];
                
                    if(!empty(trim($row['cus_field_1']))){
                        $cus_field_1 = $row['cus_field_1'];
                        $header_array[] = $cus_field_1;
                    }
                    if(!empty(trim($row['cus_field_2']))){
                        $cus_field_2 = $row['cus_field_2'];
                        $header_array[] = $cus_field_2;
                    }
                    if(!empty(trim($row['cus_field_3']))){
                        $cus_field_3 = $row['cus_field_3'];
                        $header_array[] = $cus_field_3;
                    }
                    if(!empty(trim($row['cus_field_4']))){
                        $cus_field_4 = $row['cus_field_4'];
                        $header_array[] = $cus_field_4;
                    }
                    if(!empty(trim($row['cus_field_5']))){
                        $cus_field_5 = $row['cus_field_5'];
                        $header_array[] = $cus_field_5;
                    }
                    if(!empty(trim($row['cus_field_6']))){
                        $cus_field_6 = $row['cus_field_6'];
                        $header_array[] = $cus_field_6;
                    }
                    if(!empty(trim($row['cus_field_7']))){
                        $cus_field_7 = $row['cus_field_7'];
                        $header_array[] = $cus_field_7;
                    }
                    if(!empty(trim($row['cus_field_8']))){
                        $cus_field_8 = $row['cus_field_8'];
                        $header_array[] = $cus_field_8;
                    }
                }else{
                    
                    $title = $row['parent_title'];
                    $subtitle = $row['event_title'];
                
                    if(!empty(trim($row['custom_field_1']))){
                        $cus_field_1 = $row['custom_field_1'];
                        $header_array[] = $cus_field_1;
                    }
                    if(!empty(trim($row['custom_field_2']))){
                        $cus_field_2 = $row['custom_field_2'];
                        $header_array[] = $cus_field_2;
                    }
                    if(!empty(trim($row['custom_field_3']))){
                        $cus_field_3 = $row['custom_field_3'];
                        $header_array[] = $cus_field_3;
                    }
                    if(!empty(trim($row['custom_field_4']))){
                        $cus_field_4 = $row['custom_field_4'];
                        $header_array[] = $cus_field_4;
                    }
                    if(!empty(trim($row['custom_field_5']))){
                        $cus_field_5 = $row['custom_field_5'];
                        $header_array[] = $cus_field_5;
                    }
                    if(!empty(trim($row['custom_field_6']))){
                        $cus_field_6 = $row['custom_field_6'];
                        $header_array[] = $cus_field_6;
                    }
                    if(!empty(trim($row['custom_field_7']))){
                        $cus_field_7 = $row['custom_field_7'];
                        $header_array[] = $cus_field_7;
                    }
                    if(!empty(trim($row['custom_field_8']))){
                        $cus_field_8 = $row['custom_field_8'];
                        $header_array[] = $cus_field_8;
                    }
                }
            }else{
                $error = array("status" => "Failed", "msg" => "Event details doesn't exist.");
                $this->response($this->json($error),200);
            }
        }
        $header_count = count($header_array);
        $event_text = $event_table_text = '';
        if($event_type=='S'){
            $event_table_text = " LEFT JOIN `event` e ON e.event_id = er.event_id LEFT JOIN `event_reg_details` erd ON er.event_reg_id = erd.event_reg_id ";
            $event_text = " AND er.`event_id`='$event_id'";
        }elseif($event_type=='M'){
            $event_table_text = " LEFT JOIN `event_reg_details` erd ON er.event_reg_id = erd.event_reg_id AND er.`event_id` = erd.`event_parent_id` LEFT JOIN `event` e ON e.event_id = erd.event_id ";
            $event_text = " AND (er.`event_id`='$event_id' AND er.`event_id`=erd.`event_parent_id`) ";
        }elseif($event_type=='C'){
            $event_table_text = " LEFT JOIN `event_reg_details` erd ON er.event_reg_id = erd.event_reg_id AND er.`event_id` = erd.`event_parent_id` AND erd.`event_id`='$event_id' LEFT JOIN `event` e ON e.event_id = erd.event_id ";
            $event_text = "";
        }
        
        $header_lines = 0;
        if (!empty(trim($title))) {
            $header_lines++;
        }
        if (!empty(trim($subtitle))) {
            $header_lines++;
        }
        if (!empty(trim($start))) {
            $header_lines++;
        }
        if (!empty(trim($end))) {
            $header_lines++;
        }
        $startY = 5 + $header_lines * 5 + 10 + 10;
        if($header_count>5){
            $row_size = 270;
        }else{
            $row_size = 175;
        }

        $pdf = new tFPDF();
        $pdf->SetAutoPageBreak(false);
        $pdf->event_title = urldecode($title);
        $pdf->event_sub_title = urldecode($subtitle);
        $pdf->event_begin_date = urldecode($start);
        $pdf->event_end_date = urldecode($end);
        $pdf->header_array = $header_array;
        $pdf->SetDrawColor(177, 177, 177);
        $pdf->SetFillColor(236, 236, 236);
        $final_row_size = $row_size;
        $pdf->row_size = $final_row_size;
        if($header_count>5){
            $pdf->AddPage("L","A4");
            $finalY = 205;
        }else{
            $pdf->AddPage("P","A4");
            $finalY = 290;
        }

        $query = sprintf("SELECT e.`event_title`, e.`event_category_subtitle`, e.`event_begin_dt`, e.`event_end_dt`,
            er.`event_registration_column_1`, er.`event_registration_column_2`, er.`buyer_name`, er.`buyer_phone`,
            er.`event_registration_column_3`, er.`event_registration_column_4`, er.`event_registration_column_5`, er.`event_registration_column_6`,
            er.`event_registration_column_7`, er.`event_registration_column_8`, er.`event_registration_column_9`, er.`event_registration_column_10` 
            FROM `event_registration` er %s
            WHERE e.`company_id` = '%s' %s AND erd.payment_status NOT In('CP','RF') order by er.`event_registration_column_2` ASC", $event_table_text, $company_id, $event_text);
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $number_of_rows = mysqli_num_rows($result);
            if($number_of_rows>0){
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                
                $col_array = array("event_registration_column_1", "event_registration_column_2", "buyer_name", "buyer_phone", "event_registration_column_3", "event_registration_column_4", "event_registration_column_5",
                    "event_registration_column_6", "event_registration_column_7", "event_registration_column_8", "event_registration_column_9", "event_registration_column_10");
                
                for($i=0;$i<$header_count;$i++){
                    $column_array[$i] = $col_array[$i];
                }
                $first_col_size = 5;
                $rem_col_size = floor($final_row_size/($header_count-1));
                $pdf->SetFont('DejaVu','',8);
                $finalX = $pdf->row_size+15;
//                $c = $pdf->GetY();
//                $pdf->Text(5, 5, "gY - $c, sY - $startY, iY = $initialY");
//                $initialY = $startY;
                $initialY = $pdf->GetY();
                
                for($row_index=0;$row_index<$number_of_rows;$row_index++){
                    if($row_index%2==1){
                        $fill_value = false;
                    }else{
                        $fill_value = false;
                    }
                    $initialX = 15;
//                    $tempY = $initialY+10;
                    $normal_height = $max_row_height = $final_max_row_height = 5;
                    $line_height_array = [];
                    $line_height_array[] = $final_max_row_height;
                    
                    for($column_index=1;$column_index<$header_count;$column_index++){
                        if($column_index==1){
                            $first_name_str_value = $output[$row_index][$column_array[0]];
                            $last_name_str_value = $output[$row_index][$column_array[$column_index]];                            
                            $str_value_check = $last_name_str_value." , ".$first_name_str_value;
                            $str_length = ceil($pdf->GetStringWidth($str_value_check))+1;
                        }else{
                            $str_value_check = $output[$row_index][$column_array[$column_index]];
                            $str_length = ceil($pdf->GetStringWidth($str_value_check))+1;
                        }
                        $divide_value= (int) ($str_length / $rem_col_size);
                        $remaining_value=$str_length % $rem_col_size;
                        if ($divide_value<1) {
                            $max_row_height=5;
                        }else{
                            if($remaining_value>1){
                                $max_row_height=$normal_height*($divide_value+2);
                            }else{
                                $max_row_height=$normal_height*($divide_value+1);
                            }
                        }
                        $line_height_array[] = $max_row_height;
                        
                        if($final_max_row_height < $max_row_height){
                            $final_max_row_height=$max_row_height;
                        }
                    }
                    $line_height_array[0] = $final_max_row_height;
                    
                    
                    if($initialY + $final_max_row_height>=$finalY){
                        if($header_count>5){
                            $pdf->AddPage("L","A4");
                            $finalY = 205;
                        }else{
                            $pdf->AddPage("P","A4");
                            $finalY = 290;
                        }
                        $initialY = $pdf->GetY();
                    }
                    
                    $pdf->Line($initialX, $initialY, $finalX, $initialY);
                    $pdf->Line($initialX, $initialY+$final_max_row_height, $finalX, $initialY+$final_max_row_height);
                    $pdf->Line($initialX, $initialY, $initialX, $initialY+$final_max_row_height);
                    
                    for($column_index=0;$column_index<$header_count;$column_index++){
                        $pdf->SetY($initialY);
                        $pdf->SetX($initialX);
                        $current_col_width = $line_height_array[$column_index];
                        
                        if($column_index==0){
                            $col_width = $first_col_size;
                            $col_height = $final_max_row_height;
                            $initialX = $initialX + $first_col_size;
                            $str_value = '';
                        } else if($column_index==1){
                            $col_width = $rem_col_size;
                            $first_name_str_value = $output[$row_index][$column_array[0]];
                            $last_name_str_value = $output[$row_index][$column_array[$column_index]];
                            $str_value = $last_name_str_value." , ".$first_name_str_value;
                            $initialX = $initialX + $rem_col_size;
                            
                            if($current_col_width==$final_max_row_height){
                                $col_height = $normal_height;
                            }else{
                                if($current_col_width==$normal_height){
                                    $col_height = $final_max_row_height;
                                }else{
                                    $col_height = $final_max_row_height/($current_col_width/$normal_height);
                                }
                            }
                        } else {
                            $col_width = $rem_col_size;
                            $str_value = $output[$row_index][$column_array[$column_index]];
                            if($current_col_width==$final_max_row_height){
                                $col_height = $normal_height;
                            }else{
                                if($current_col_width==$normal_height){
                                    $col_height = $final_max_row_height;
                                }else{
                                    $col_height = $final_max_row_height/($current_col_width/$normal_height);
                                }
                            }
                            $initialX = $initialX + $rem_col_size;
                            
                        }
                        $pdf->MultiCell($col_width,$col_height,$str_value,0,'C',$fill_value);
//                        $pdf->MultiAlignCell($col_width,$col_height,$str_value,$initialX,0,'C',$fill_value);
                        $pdf->Line($initialX, $initialY, $initialX, $initialY+$final_max_row_height);
                        $pdf->SetY($initialY);                          
                    }
//                    $pdf->Line($initialX, $initialY, $initialX, $tempY);
                    $pdf->SetX($initialX);
                    $initialY = $initialY + $final_max_row_height;
                    $nextY = 0;
                    
                    if ($row_index + 1 < $number_of_rows) {
                        $nextY = $initialY + $final_max_row_height;
                    } else {
                        $nextY = $initialY;
                    }

                    if($nextY>=$finalY){
//                           $pdf->AddPage();
                        if($header_count>5){
                             $pdf->AddPage("L","A4");
                             $finalY = 205;
                        }else{
                             $pdf->AddPage("P","A4");
                             $finalY = 290;
                        }
                        $initialY = $pdf->GetY();
                    }
                }
            }
            
            ob_end_clean();
            $file = "$title.pdf";
            $pdf->Output($file, 'I');
        }
        date_default_timezone_set($curr_time_zone);
    }
   
//    public function createPDF($company_id,$event_id){
//        
//        $header_array = array('', 'Participant', 'Buyer', 'Phone');
//        $parent_id = $event_type = '';
//        $title = $subtitle = $start = $end = $cus_field_1 = $cus_field_2 = $cus_field_3 = $cus_field_4 = "";
//        $select_query = sprintf("SELECT e.`parent_id`, e.`event_type`, e.`event_title`, e.`event_category_subtitle`,            
//            e.`event_begin_dt`, e.`event_end_dt`, e.`event_registration_column_3` as cus_field_1, 
//            e.`event_registration_column_4` as cus_field_2, e.`event_registration_column_5` as cus_field_3, 
//            e.`event_registration_column_6` as cus_field_4, e.`event_registration_column_7` as cus_field_5,
//            e.`event_registration_column_8` as cus_field_6, e.`event_registration_column_9` as cus_field_7,
//            e.`event_registration_column_10` as cus_field_8, e2.`event_title` as parent_title, e2.`event_registration_column_3` 
//            as custom_field_1, e2.`event_registration_column_4` as custom_field_2, e2.`event_registration_column_5` 
//            as custom_field_3, e2.`event_registration_column_6` as custom_field_4 ,e2.`event_registration_column_7` as custom_field_5,
//            e2.`event_registration_column_8` as custom_field_6, e2.`event_registration_column_9` as custom_field_7,
//            e2.`event_registration_column_10` as custom_field_8 FROM `event` e LEFT JOIN `event` 
//            e2 ON e.parent_id = e2.event_id WHERE e.`company_id` = '%s' AND e.`event_id`='%s'", 
//                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
//        $result_select = mysqli_query($this->db, $select_query);
//        if(!$result_select){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_query");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//        }else{
//            $num_rows = mysqli_num_rows($result_select);
//            if($num_rows>0){
//                $row = mysqli_fetch_assoc($result_select);
//                if(!empty($row['parent_id'])){
//                    $parent_id = $row['parent_id'];
//                }
//                $event_type = $row['event_type'];
//                if(!empty(trim($row['event_begin_dt']))){
//                    if("0000-00-00 00:00:00" == $row['event_begin_dt']){
//                        $start = "";
//                    } else {
//                        $start = $row['event_begin_dt'];
//                        
//                        $split_start = explode(" ", $start);
//                        $start_date_value = $split_start[0];
//                        $start_time_value = $split_start[1];
//                        if("00:00:00" == $start_time_value){
//                            $start = date('M d, Y', strtotime($start));
//                            $start = "Start Time: ".$start;
//                        } else {
//                            $start = date('M d, Y @ h:i A', strtotime($start));
//                            $start = "Start Time: ".$start;
//                        }
//                        
//                    }
//                }
//                
//                if(!empty(trim($row['event_end_dt']))){
//                    if("0000-00-00 00:00:00" == $row['event_end_dt']){
//                        $end = "";
//                    } else {
//                        $end = $row['event_end_dt'];
//                        
//                        $split_end = explode(" ", $end);
//                        $end_date_value = $split_end[0];
//                        $end_time_value = $split_end[1];
//                        if("00:00:00" == $end_time_value){
//                            $end = date('M d, Y', strtotime($end));
//                            $end = "End Time: ".$end;
//                        } else {
//                            $end = date('M d, Y @ h:i A', strtotime($end));
//                            $end = "End Time: ".$end;
//                        }
//                    }
//                }
//                
//                if($event_type=='S' || $event_type=='M'){
//                    
//                    $title = $row['event_title'];
//                    $subtitle = $row['event_category_subtitle'];
//                
//                    if(!empty(trim($row['cus_field_1']))){
//                        $cus_field_1 = $row['cus_field_1'];
//                        $header_array[] = $cus_field_1;
//                    }
//                    if(!empty(trim($row['cus_field_2']))){
//                        $cus_field_2 = $row['cus_field_2'];
//                        $header_array[] = $cus_field_2;
//                    }
//                    if(!empty(trim($row['cus_field_3']))){
//                        $cus_field_3 = $row['cus_field_3'];
//                        $header_array[] = $cus_field_3;
//                    }
//                    if(!empty(trim($row['cus_field_4']))){
//                        $cus_field_4 = $row['cus_field_4'];
//                        $header_array[] = $cus_field_4;
//                    }
//                    if(!empty(trim($row['cus_field_5']))){
//                        $cus_field_5 = $row['cus_field_5'];
//                        $header_array[] = $cus_field_5;
//                    }
//                    if(!empty(trim($row['cus_field_6']))){
//                        $cus_field_6 = $row['cus_field_6'];
//                        $header_array[] = $cus_field_6;
//                    }
//                    if(!empty(trim($row['cus_field_7']))){
//                        $cus_field_7 = $row['cus_field_7'];
//                        $header_array[] = $cus_field_7;
//                    }
//                    if(!empty(trim($row['cus_field_8']))){
//                        $cus_field_8 = $row['cus_field_8'];
//                        $header_array[] = $cus_field_8;
//                    }
//                }else{
//                    
//                    $title = $row['parent_title'];
//                    $subtitle = $row['event_title'];
//                
//                    if(!empty(trim($row['custom_field_1']))){
//                        $cus_field_1 = $row['custom_field_1'];
//                        $header_array[] = $cus_field_1;
//                    }
//                    if(!empty(trim($row['custom_field_2']))){
//                        $cus_field_2 = $row['custom_field_2'];
//                        $header_array[] = $cus_field_2;
//                    }
//                    if(!empty(trim($row['custom_field_3']))){
//                        $cus_field_3 = $row['custom_field_3'];
//                        $header_array[] = $cus_field_3;
//                    }
//                    if(!empty(trim($row['custom_field_4']))){
//                        $cus_field_4 = $row['custom_field_4'];
//                        $header_array[] = $cus_field_4;
//                    }
//                    if(!empty(trim($row['custom_field_5']))){
//                        $cus_field_5 = $row['custom_field_5'];
//                        $header_array[] = $cus_field_5;
//                    }
//                    if(!empty(trim($row['custom_field_6']))){
//                        $cus_field_6 = $row['custom_field_6'];
//                        $header_array[] = $cus_field_6;
//                    }
//                    if(!empty(trim($row['custom_field_7']))){
//                        $cus_field_7 = $row['custom_field_7'];
//                        $header_array[] = $cus_field_7;
//                    }
//                    if(!empty(trim($row['custom_field_8']))){
//                        $cus_field_8 = $row['custom_field_8'];
//                        $header_array[] = $cus_field_8;
//                    }
//                }
//            }else{
//                $error = array("status" => "Failed", "msg" => "Event details doesn't exist.");
//                $this->response($this->json($error),200);
//            }
//        }
//        $header_count = count($header_array);
//        $event_text = $event_table_text = '';
//        if($event_type=='S'){
//            $event_table_text = " LEFT JOIN `event` e ON e.event_id = er.event_id LEFT JOIN `event_reg_details` erd ON er.event_reg_id = erd.event_reg_id ";
//            $event_text = " AND er.`event_id`='$event_id'";
//        }elseif($event_type=='M'){
//            $event_table_text = " LEFT JOIN `event_reg_details` erd ON er.event_reg_id = erd.event_reg_id AND er.`event_id` = erd.`event_parent_id` LEFT JOIN `event` e ON e.event_id = erd.event_id ";
//            $event_text = " AND (er.`event_id`='$event_id' AND er.`event_id`=erd.`event_parent_id`) ";
//        }elseif($event_type=='C'){
//            $event_table_text = " LEFT JOIN `event_reg_details` erd ON er.event_reg_id = erd.event_reg_id AND er.`event_id` = erd.`event_parent_id` AND erd.`event_id`='$event_id' LEFT JOIN `event` e ON e.event_id = erd.event_id ";
//            $event_text = "";
//        }
//        
//        $header_lines = 0;
//        if (!empty(trim($title))) {
//            $header_lines++;
//        }
//        if (!empty(trim($subtitle))) {
//            $header_lines++;
//        }
//        if (!empty(trim($start))) {
//            $header_lines++;
//        }
//        if (!empty(trim($end))) {
//            $header_lines++;
//        }
//        $startY = 5 + $header_lines * 5 + 10 + 10;
////        $row_size = [175, 174, 174, 176, 175, 174, 175, 176];
//        if($header_count>5){
////        $row_size = [260, 260, 260, 260, 260, 260, 260, 260, 260, 260, 260, 260];
//         $row_size = [275, 275, 275, 275, 275, 275, 275, 275, 275, 275, 275, 275];
//        }else{
//            $row_size = [175, 174, 174, 176, 175, 174, 175, 176, 171, 175, 174, 175];
//        }
//
//        $pdf = new tFPDF();
//        $pdf->SetAutoPageBreak(false);
//        $pdf->event_title = urldecode($title);
//        $pdf->event_sub_title = urldecode($subtitle);
//        $pdf->event_begin_date = urldecode($start);
//        $pdf->event_end_date = urldecode($end);
//        $pdf->header_array = $header_array;
//        $pdf->SetDrawColor(177, 177, 177);
//        $pdf->SetFillColor(236, 236, 236);
//                $final_row_size = $row_size[$header_count - 1];
//        $pdf->row_size = $final_row_size;
////           $pdf->AddPage();
//        if($header_count>5){
//             $pdf->AddPage("L","A4");
//             $finalY = 205;
////            $row_size = [260, 260, 260,260, 260, 260, 260, 260, 260, 260, 260, 260];
//        }else{
//             $pdf->AddPage("P","A4");
//             $finalY = 290;
////            $row_size = [175, 174, 174, 176, 175, 174, 175, 176, 171, 175, 174, 175];
//        }
//
//        $query = sprintf("SELECT e.`event_title`,e.`event_category_subtitle`,e.`event_begin_dt`,e.`event_end_dt`,
//            er.`event_registration_column_1`,er.`event_registration_column_2`,er.`buyer_name`,er.`buyer_phone`,
//            er.`event_registration_column_3`,er.`event_registration_column_4`,er.`event_registration_column_5`,er.`event_registration_column_6`,
//            er.`event_registration_column_7`,er.`event_registration_column_8`,er.`event_registration_column_9`,er.`event_registration_column_10` 
//            FROM `event_registration` er %s
//            WHERE e.`company_id` = '%s' %s AND erd.payment_status NOT In('CP','RF') order by er.`event_registration_column_2` ASC",$event_table_text,$company_id,$event_text);
//        $result = mysqli_query($this->db, $query);
//        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
//        log_info($this->json($error_log));
//        if (!$result) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//        } else {
//            $number_of_rows = mysqli_num_rows($result);
//            if($number_of_rows>0){
//                
//                while ($row = mysqli_fetch_assoc($result)) {
//                    $output[] = $row;
//                }
//                
//                $col_array = array("event_registration_column_1", "event_registration_column_2", "buyer_name", "buyer_phone",
//                    "event_registration_column_3", "event_registration_column_4", "event_registration_column_5",
//                    "event_registration_column_6", "event_registration_column_7", "event_registration_column_8",
//                    "event_registration_column_9", "event_registration_column_10");
//                for($i=0;$i<$header_count;$i++){
//                    $column_array[$i] = $col_array[$i];
//                }
//                $first_col_size = 5;
//                $rem_col_size = floor($final_row_size/($header_count-1));
////                $rem_col_size = 25;
////                $this->SetFont('Arial','',9);
//                $pdf->SetFont('DejaVu','',8);
//                $finalX = $pdf->row_size+15;
//                $finalY = 290;
//                $initialY = $startY;
//                for($row_index=0;$row_index<$number_of_rows;$row_index++){
//                    if($row_index%2==1){
//                        $fill_value = true;
//                    }else{
//                        $fill_value = false;
//                    }
//                    $initialX = 15;
//                    $tempY = $initialY+10;
//                    $final_max_row_height=5;
//                    $max_row_height=5;
//                    for($column_index=1;$column_index<$header_count;$column_index++){
//                        if($column_index==1){
//                            $first_name_str_value = $output[$row_index][$column_array[0]];
//                            $last_name_str_value = $output[$row_index][$column_array[$column_index]];                            
//                            $str_value_check = $last_name_str_value." , ".$first_name_str_value;
//                            $str_length = $pdf->GetStringWidth($str_value_check);
//                        }else{
//                            $str_value_check = $output[$row_index][$column_array[$column_index]];
//                            $str_length = $pdf->GetStringWidth($str_value_check);
//                        }
//                        $divide_value=$str_length / $rem_col_size;
//                        $remaining_value=$str_length % $rem_col_size;
//                        if ($divide_value<1) {
//                            $max_row_height=5;
//                        }else{
//                            if($remaining_value>1){
//                                $max_row_height=$max_row_height*($divide_value+1);
//                            }else{
//                                $max_row_height=$max_row_height*($divide_value);
//                            }
//                        }
//                        if($final_max_row_height>$max_row_height){
//                            $final_max_row_height=$max_row_height;
//                        }
//                    }
////                    $pdf->Line($initialX ,$tempY, $finalX, $tempY);
//                    for($column_index=0;$column_index<$header_count;$column_index++){
//                        $pdf->SetY($initialY);
//                        $pdf->SetX($initialX);
////                        $pdf->Line($initialX, $initialY, $initialX, $tempY);
//                        
//                        if($column_index==0){
////                             print_r($final_max_row_height);
//                            $pdf->MultiCell($first_col_size,$final_max_row_height,'',1,'C',$fill_value);
////                            $pdf->MultiAlignCell($first_col_size, 10, '', '', 1, 'C', $fill_value);
//                            $initialX = $initialX + $first_col_size;
//                        } else if($column_index==1){
//                            
//                            $first_name_str_value = $output[$row_index][$column_array[0]];
//                            $last_name_str_value = $output[$row_index][$column_array[$column_index]];
//                            
//                            $str_value = $last_name_str_value." , ".$first_name_str_value;
//                            $str_length = $pdf->GetStringWidth($str_value);
//                            
//                            if ($str_length <= $rem_col_size) {
//                                $height_index_value = "10";
//                            } else {
//                                $str_length_cutoff_value = substr($str_value, 0, $rem_col_size-1);
//                                $str_value = $str_length_cutoff_value;
//                                $height_index_value = "5";
//                            }
//                            $pdf->MultiCell($rem_col_size,$final_max_row_height,$str_value,1,'C',$fill_value);
////                            $pdf->MultiAlignCell($rem_col_size, $height_index_value, $str_value, '', 1, 'C', $fill_value);
//                            $initialX = $initialX + $rem_col_size;
//                        } else {
//                            
//                            $str_value = $output[$row_index][$column_array[$column_index]];
//                            $str_length = $pdf->GetStringWidth($str_value);
//                            
//                            if ($str_length <= $rem_col_size) {
//                                $height_index_value = "10";
//                            } else {
//                                $str_length_cutoff_value = substr($str_value, 0, $rem_col_size-1);
//                                $str_value = $str_length_cutoff_value;
//                                $height_index_value = "5";
//                            }
//                            $pdf->MultiCell($rem_col_size,$final_max_row_height,$str_value,1,'C',$fill_value);
////                            $pdf->MultiAlignCell($rem_col_size,  $height_index_value, $str_value, '', 1, 'C', $fill_value);
//                            $initialX = $initialX + $rem_col_size;
//                            
//                        }
//                                                  
//                        $pdf->SetY($initialY);                          
//                    }
////                    $pdf->Line($initialX, $initialY, $initialX, $tempY);
//                    $pdf->SetX($initialX);
//                    $initialY = $initialY + 10;
//                    $nextY = 0;
//                    
//                    if ($row_index + 1 < $number_of_rows) {
//                        $nextY = $initialY + 10;
//                    } else {
//                        $nextY = $initialY;
//                    }
//
//                    if($nextY>=$finalY){
////                           $pdf->AddPage();
//                    if($header_count>5){
//                         $pdf->AddPage("L","A4");
//                         $finalY = 205;
////                        $row_size = [260, 260, 260, 260, 260, 260, 260, 260, 260, 260, 260, 260];
//                    }else{
//                         $pdf->AddPage("P","A4");
//                         $finalY = 290;
////                        $row_size = [175, 174, 174, 176, 175, 174, 175, 176, 171, 175, 174, 175];
//                    }
//                        $initialY = $startY;
//                    }
//                }
//            } else {
//                
//            }
//            
//            ob_end_clean();
//            $file = "$title.pdf";
//            $pdf->Output($file, 'I');
//        }
//    }
    
    public function createCSV($company_id,$event_id){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);        
        $parent_id = $event_type = '';
        $title = $subtitle = $start = $end = "";
        $cus_field_1 = $cus_field_2 = $cus_field_3 = $cus_field_4 = $cus_field_5 = $cus_field_6 = $cus_field_7 = $cus_field_8 = $cus_field_9 = $cus_field_10 = "";
        $select_query = sprintf("SELECT e.`parent_id`, e.`event_type`, e.`event_title`, e.`event_category_subtitle`, e.`event_begin_dt`, e.`event_end_dt`,
            e.`event_registration_column_1` as cus_field_1,e.`event_registration_column_2` as cus_field_2,
            e.`event_registration_column_3` as cus_field_3,e.`event_registration_column_4` as cus_field_4,
            e.`event_registration_column_5` as cus_field_5,e.`event_registration_column_6` as cus_field_6,
            e.`event_registration_column_7` as cus_field_7,e.`event_registration_column_8` as cus_field_8,
            e.`event_registration_column_9` as cus_field_9,e.`event_registration_column_10` as cus_field_10, 
            e2.`event_registration_column_1` as custom_field_1,e2.`event_registration_column_2` as custom_field_2,
            e2.`event_registration_column_3` as custom_field_3,e2.`event_registration_column_4` as custom_field_4,
            e2.`event_registration_column_5` as custom_field_5,e2.`event_registration_column_6` as custom_field_6,
            e2.`event_registration_column_7` as custom_field_7,e2.`event_registration_column_8` as custom_field_8,
            e2.`event_registration_column_9` as custom_field_9,e2.`event_registration_column_10` as custom_field_10
            FROM `event` e 
            LEFT JOIN `event` e2 ON e.parent_id = e2.event_id
            WHERE e.`company_id` = '%s' AND e.`event_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
        $result_select = mysqli_query($this->db, $select_query);
        if(!$result_select){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result_select);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result_select);
                $event_details = $row;
                if(!empty($row['parent_id'])){
                    $parent_id = $row['parent_id'];
                }
                $event_type = $row['event_type'];
                $title = $row['event_title'];
                $subtitle = $row['event_category_subtitle'];
                $start = $row['event_begin_dt'];
                $end = $row['event_end_dt'];
                
            }else{
                $error = array("status" => "Failed", "msg" => "Event details doesn't exist.");
                $this->response($this->json($error),200);
            }
        }
        
        $event_text = $event_table_text = '';
        if($event_type=='S'){
            $event_table_text = " LEFT JOIN `event_reg_details` erd ON er.event_reg_id = erd.event_reg_id LEFT JOIN `event` e ON e.event_id = er.event_id ";
            $event_text = " AND er.`event_id`='$event_id'";
        }elseif($event_type=='M'){
            $event_table_text = " LEFT JOIN `event_reg_details` erd ON er.event_reg_id = erd.event_reg_id AND er.`event_id` = erd.`event_parent_id` LEFT JOIN `event` e ON e.event_id = erd.event_id ";
            $event_text = " AND (er.`event_id`='$event_id' AND er.`event_id`=erd.`event_parent_id`) ";
        }elseif($event_type=='C'){
            $event_table_text = " LEFT JOIN `event_reg_details` erd ON er.event_reg_id = erd.event_reg_id AND er.`event_id` = erd.`event_parent_id` AND erd.`event_id`='$event_id' LEFT JOIN `event` e ON e.event_id = erd.event_id ";
            $event_text = "";
        }
        
        $header = $body = '';
        $header = "Event Title,Event Start Date,Event End Date,";
        
        if ($event_type == 'C') {
            $temp_reg_field = "custom_field_";
        } else {
            $temp_reg_field = "cus_field_";
        }

        for ($z = 1; $z <= 10; $z++) { //collect registration fields as array
            $reg_field = $temp_reg_field . "$z";
            if ($event_details[$reg_field] != '') {
                  $header = $header .'"'. str_replace('"', "''", $event_details[$reg_field]) .'"'. ",";
            }
        }

        $header = $header . "Buyer Name,Buyer Email,Buyer Phone Number,Buyer Postal code,Registration Date,Event Cost,"
                . "Discounts,Qty Ordered,Total Paid by User to date,Balance Remaining,Number of Payments,Payment Frequency\r\n";

        $query = sprintf("SELECT e.`event_title`,e.`event_category_subtitle`,e.`event_begin_dt`,e.`event_end_dt`,
            er.`event_registration_column_1`,er.`event_registration_column_2`,er.`event_registration_column_3`,
            er.`event_registration_column_4`,er.`event_registration_column_5`,er.`event_registration_column_6`,
            er.`event_registration_column_7`,er.`event_registration_column_8`,er.`event_registration_column_9`,
            er.`event_registration_column_10`,concat(er.`buyer_last_name`,', ',er.`buyer_first_name`) buyer_name,er.`buyer_phone`,er.`buyer_email`,er.`buyer_postal_code`,
            er.`registration_date`,erd.`event_cost`,erd.`discount`,erd.`quantity`,erd.`payment_amount`,erd.`balance_due`,
            IF(er.`payment_type`='RE', er.`number_of_payments` , '') number_of_payments, er.`payment_frequency`,IF(er.`payment_type`='RE', er.`payment_startdate` , '') payment_startdate
            FROM `event_registration` er %s
            WHERE e.`company_id` = '%s' %s AND erd.payment_status NOT In('CP','RF') group by erd.event_reg_id, erd.event_id order by er.`event_registration_column_2` ASC",$event_table_text, $company_id, $event_text);
        $result = mysqli_query($this->db, $query);
        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $number_of_rows = mysqli_num_rows($result);
            if($number_of_rows>0){               
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                
                for ($row_index = 0; $row_index < $number_of_rows; $row_index++) {
                    if(!empty(trim($output[$row_index]['event_title']))){
                        $event_title = str_replace('"', "''", $output[$row_index]['event_title']);
                    } else {
                        $event_title = "";
                    }
//                    if(!empty(trim($output[$row_index]['event_category_subtitle']))){
//                        $event_sub_title = $output[$row_index]['event_category_subtitle'];
//                    } else {
//                        $event_sub_title = "";
//                    }
                    if(!empty(trim($output[$row_index]['event_begin_dt']))){
                        if("0000-00-00 00:00:00" == $output[$row_index]['event_begin_dt']){
                            $event_begin_date = "";
                        } else {
                            $event_begin_date = $output[$row_index]['event_begin_dt'];
                            
                            $split_start = explode(" ", $event_begin_date);
                            $start_date_value = $split_start[0];
                            $start_time_value = $split_start[1];
                            if ("00:00:00" == $start_time_value) {
                                $event_begin_date = date('m/d/Y', strtotime($event_begin_date));
                            } else {
                                $event_begin_date = date('m/d/Y h:i A', strtotime($event_begin_date));
                            }
                        }
                    } else {
                        $event_begin_date = "";
                    }
                    if(!empty(trim($output[$row_index]['event_end_dt']))){
                        if("0000-00-00 00:00:00" == $output[$row_index]['event_end_dt']){
                            $event_end_date = "";
                        } else {
                            $event_end_date = $output[$row_index]['event_end_dt'];
                            
                            $split_end = explode(" ", $event_end_date);
                            $end_date_value = $split_end[0];
                            $end_time_value = $split_end[1];
                            if ("00:00:00" == $end_time_value) {                               
                                $event_end_date = date('m/d/Y', strtotime($event_end_date));
                            } else {                               
                                $event_end_date = date('m/d/Y h:i A', strtotime($event_end_date));
                            }
                        }
                    } else {
                        $event_end_date = "";
                    }
                    $body = $body .'"'. $event_title .'"'. "," . $event_begin_date . "," . $event_end_date . ",";
                    $temp_reg_field_value = "event_registration_column_";
                    for ($z = 1; $z <= 10; $z++) { //collect registration fields as array
                        $reg_field = $temp_reg_field . "$z";
                        $reg_field_value = $temp_reg_field_value . "$z";//for filed names
                        if ($event_details[$reg_field] != '') {
                            $body = $body .'"'. str_replace('"', "''", $output[$row_index][$reg_field_value]) .'"'. ",";
                        }
                    }
                    
                    $body = $body .'"'. str_replace('"', "''", $output[$row_index]['buyer_name']) .'"'. ",";
                    $body = $body .'"'. str_replace('"', "''", $output[$row_index]['buyer_email']) .'"'. ",";
                    $body = $body .'"'. str_replace('"', "''", $output[$row_index]['buyer_phone']) .'"'. ",";
                    $body = $body .'"'. str_replace('"', "''", $output[$row_index]['buyer_postal_code']) .'"'. ",";
                    
                    if (!empty(trim($output[$row_index]['registration_date']))) {
                        if ("0000-00-00" == $output[$row_index]['registration_date']) {
                            $registration_date = "";
                        } else {
                            $registration_date = $output[$row_index]['registration_date'];
                            $registration_date = date('Y-m-d', strtotime($registration_date));
                        }
                    } else {
                        $registration_date = "";
                    }
                    
                    $body = $body . $registration_date . ",";

                    $body = $body . $output[$row_index]['event_cost'] . ",";
                    $body = $body . $output[$row_index]['discount'] . ",";
                    $body = $body . $output[$row_index]['quantity'] . ",";
                    
                    $total_paid_by_userToDate = $output[$row_index]['payment_amount'] - $output[$row_index]['balance_due'];
                    $body = $body . $total_paid_by_userToDate . ",";
                    
                    $body = $body . $output[$row_index]['balance_due'] . ",";
                    $body = $body . $output[$row_index]['number_of_payments'] . ",";
                    
                    $frequency = $output[$row_index]['payment_frequency'];
                    if ($frequency == "1") {
                        $payment_frequency = "Monthly";
                    } else if ($frequency == "2") {
                        $payment_frequency = "Weekly";
                    } else if ($frequency == "3") {
                        $payment_frequency = "Bi-Weekly";
                    } else {
                        $payment_frequency = "";
                    } 
                    $body = $body . $payment_frequency . "\r\n";
                    
                }
                                                
            } else {
                
            }
            
            ob_clean();
            ob_start();
            $file = "../../../uploads/test.csv";
            $ifp = fopen($file, "w");
            fwrite($ifp, $header);
            fwrite($ifp, $body);
            fclose($ifp);
            header('Content-Type: application/x-download; charset=utf-8');
            header('Content-Disposition: attachment; filename="' . basename($file) . '"');
            header('Cache-Control: private, max-age=0, must-revalidate');
            header('Pragma: public');
            readfile($file);
            unlink($file);
            exit();
        }
        date_default_timezone_set($curr_time_zone);
    }   

    public function updateEventPaymentDetailsAsPaid($company_id,$event_reg_id,$event_payment_id,$credit_amount,$payment_Count,$credit_method,$check_number){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $credit_method_and_check_string = '';
        $check_string = '';
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
       
        $curr_date = date("Y-m-d");
        $select_query = sprintf("SELECT ep.*, (SELECT c.`upgrade_status` FROM `company` c LEFT JOIN `event_registration` er ON c.`company_id`=er.`company_id` WHERE er.`event_reg_id`='%s') upgrade_status,
            (SELECT c.wp_currency_symbol FROM `company` c LEFT JOIN `event_registration` er ON c.`company_id`=er.`company_id` WHERE er.`event_reg_id`='%s') wp_currency_symbol , 
            (SELECT er.`processing_fee_type` FROM `company` c LEFT JOIN `event_registration` er ON c.`company_id`=er.`company_id` WHERE er.`event_reg_id`='%s') processing_fee_type
                   FROM `event_payment` ep WHERE ep.`event_reg_id`='%s' AND ep.`event_payment_id`='%s'", 
                mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_payment_id));
        $result_select_query = mysqli_query($this->db, $select_query);
//        log_info($select_query);
        if (!$result_select_query) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result_select_query);
            if($num_rows>0){
                $original_record_status = 'N';
                $row = mysqli_fetch_assoc($result_select_query);
                $status = $row['schedule_status'];
                $amount_in_db = $row['payment_amount'];
                $date_in_db = $row['schedule_date'];
                $upgrade_status = $row['upgrade_status'];
                $processing_fee_type = $row['processing_fee_type'];
                $wp_currency_symbol=$row['wp_currency_symbol'];
                $rem_amt = $amount_in_db-$credit_amount;
                $updated_amt = $amount_in_db;
                $original_record_date = $date_in_db;
                $original_record_fee = 0;
                if($status!='N' && $status!='F'){
                    if($status=='C'){
                        $msg = array('status' => "Failed", "msg" => "Selected Payment has been cancelled already.");
                        $this->response($this->json($msg), 200);
                    }elseif($status=='R' || $status=='CR' ){
                        $msg = array('status' => "Failed", "msg" => "Selected Payment has already been paid and refunded.");
                        $this->response($this->json($msg), 200);
                    }elseif($status=='PR'){
                        $msg = array('status' => "Failed", "msg" => "Selected payment has already been paid and partially refunded.");
                        $this->response($this->json($msg), 200);
                    }elseif($status=='S'){
                        $msg = array('status' => "Failed", "msg" => "Selected payment has been paid already.");
                        $this->response($this->json($msg), 200);
                    }else{
                        $msg = array('status' => "Failed", "msg" => "Invalid schedule status.");
                        $this->response($this->json($msg), 200);
                    }
                }
                if($credit_amount>$amount_in_db){
                    $msg = array('status' => "Failed", "msg" => "Credit amount should not be greater than bill amount.");
                    $this->response($this->json($msg), 200);
                }else{
                    if($rem_amt!=0){
                        if($credit_amount<5 || $rem_amt<5){
                            $msg = array('status' => "Failed", "msg" => "Credit amount or remaining bill amount should not be lower than $5.");
                            $this->response($this->json($msg), 200);
                        }else{
                            if ($credit_method == 'CH') {
                                $check_string = "'".$check_number."'";
                            }else{
                                $check_string = "NULL";
                            }
                            $updated_amt = $rem_amt;
                            $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                            $process_fee_per = $process_fee['PP'];
                            $process_fee_val = preg_replace('~[\r\n]+~', '', $process_fee['PT']);
                            $p1_fee = $updated_amt*($process_fee_per/100) + $process_fee_val;
                            $p2_fee = ($updated_amt + $p1_fee)*($process_fee_per/100) + $process_fee_val;
                            if($processing_fee_type==1){
                                $fee = number_format((float)($p1_fee), 2, '.', '');
                            }else{
                                $fee = number_format((float)($p2_fee), 2, '.', '');
                            }
                            $original_record_fee = $fee;
                            $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `credit_method`, `check_number`)
                                                SELECT `event_reg_id`, `student_id`, 'manual', $credit_amount, '%s', 'M', '$credit_method', $check_string FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $date_in_db), mysqli_real_escape_string($this->db, $event_payment_id));
                            $result_insert_query = mysqli_query($this->db, $insert_payment);
                            if (!$result_insert_query) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_payment");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                    }else{
                        $original_record_status = 'M';
                        $original_record_date = $curr_date;
                        if ($credit_method == 'CH') {
                            $check_string = "'".$check_number."'";
                        }else{
                            $check_string = "NULL";
                        }
                        $credit_method_and_check_string = "`credit_method` = '$credit_method', `check_number` =$check_string,";
                    }
                }
                
                $update_query = sprintf("UPDATE `event_payment` SET $credit_method_and_check_string `schedule_status`='$original_record_status', `schedule_date`='$date_in_db', `payment_amount`='$updated_amt', `processing_fee`='$original_record_fee' WHERE `event_reg_id`='%s' AND `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_payment_id));
                $result_update_query = mysqli_query($this->db, $update_query);
                if (!$result_update_query) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                
                $update_reg = sprintf("UPDATE `event_registration` SET `paid_amount`=`paid_amount`+$credit_amount WHERE `event_reg_id`='%s'", mysqli_real_escape_string($this->db, $event_reg_id));
                $result_update_reg = mysqli_query($this->db, $update_reg);
                if (!$result_update_reg) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                    
                $temp_credit_amount = $credit_amount;
                $get_event_details_query = sprintf("SELECT e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, erd.`event_parent_id`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, erd.`payment_status`, `balance_due` FROM `event` e 
                           LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_reg_id`='%s') AND erd.`payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_reg_id));
                $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                if(!$get_event_details_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $num_rows2 = mysqli_num_rows($get_event_details_result);
                    if($num_rows2>0){
                        while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                            $update_rem_due = 0;
                            if($temp_credit_amount>0){
                                $event_reg_details_id = $row2['event_reg_details_id'];
                                $amount = $row2['payment_amount'];
                                $rem_due = $row2['balance_due'];
                                if($rem_due>0){
                                    if($temp_credit_amount>=$rem_due){
                                        $temp_credit_amount = $temp_credit_amount-$rem_due;
                                        $update_event_reg = sprintf("UPDATE `event_reg_details` SET `payment_status`='RC', `balance_due`=$update_rem_due, `manual_payment_amount`=`manual_payment_amount`+$rem_due WHERE `event_reg_details_id`='%s'", mysqli_real_escape_string($this->db, $event_reg_details_id));
                                        $result_update_event_reg = mysqli_query($this->db, $update_event_reg);
                                        if(!$result_update_event_reg){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_reg");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 200);
                                        }
                                    }else{
                                        $update_rem_due = $rem_due-$temp_credit_amount;
                                        $update_event_reg = sprintf("UPDATE `event_reg_details` SET `balance_due`=$update_rem_due, `manual_payment_amount`=`manual_payment_amount`+$temp_credit_amount WHERE `event_reg_details_id`='%s'", mysqli_real_escape_string($this->db, $event_reg_details_id));
                                        $result_update_event_reg = mysqli_query($this->db, $update_event_reg);
                                        if(!$result_update_event_reg){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_reg");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 200);
                                        }
                                        $temp_credit_amount = 0;
                                    }
                                }
                            }
                        }
                    }else{
                        $error = array("status" => "Failed", "msg" => "Participant details doesn't exist.");
                        $this->response($this->json($error),200);
                    }
                    if ($credit_method == 'CA') {
                        $activity_type_string = "Cash Credit Applied";
                        $activity_text = "Cash Credit Applied: ";
                    }else if ($credit_method == 'CH'){
                        $activity_type_string = "Check Credit Applied";
                        $activity_text = "Check Credit Applied: ";
                    }else if ($credit_method == 'MC'){
                        $activity_type_string = "Manual Credit Applied";
                        $activity_text = "Manual Credit Applied: ";
                    }                     
                    $curr_date = gmdate("Y-m-d H:i:s");
                    $text = $activity_text.$wp_currency_symbol.$credit_amount.' to '.$payment_Count .' on ';
                    $insert_history = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_payment_id),$activity_type_string, $text, $curr_date);
                    $result_history = mysqli_query($this->db, $insert_history);
                    if (!$result_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                    $payment_history = $this->getpaymentHistoryDetails($company_id, $event_reg_id, 1);
                    if ($credit_method == 'CH'){
                        $msg_text = 'Check Credit applied.';
                    }else if (($credit_method == 'CA') ){
                        $msg_text = 'Cash Credit applied.';
                    }else if (($credit_method == 'MC') ){
                        $msg_text = 'Manual Credit applied.';
                    }
                    $log = array("status" => "Success", "msg" => $msg_text,  "payment" => $payment_history);
                    $this->response($this->json($log),200);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "No records found.");
                $this->response($this->json($error), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function updateEventPaymentMethodDetails($event_reg_id,$new_cc_id,$cc_state,$buyer_first_name,$buyer_last_name,$buyer_email,$postal_code,$page_key,$type){
        
        $gmt_date=gmdate(DATE_RFC822);
        $bactivity_type = $bactivity_text = "";
        $sns_msg9=array("buyer_email"=>$buyer_email,"membership_title"=>$event_reg_id,"gmt_date"=>$gmt_date);
        $query1 = sprintf("SELECT er.payment_method,er.company_id, `wp_user_id`,`wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency`, 
                  er.`student_id`, er.`event_id`, er.`payment_type`, max(ep.`schedule_date`) s_date, er.`buyer_first_name`,er.`buyer_last_name` 
                  FROM `event_registration` er LEFT JOIN `wp_account` wp ON er.`company_id`=wp.`company_id` 
                  LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` WHERE er.`event_reg_id`='%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                $event_id = $row['event_id'];
                $payment_type = $row['payment_type'];
                $schedule_date = $row['s_date'];
                $company_id = $row['company_id'];
                $student_id = $row['student_id'];
                $old_buyer_first_name = $row['buyer_first_name'];
                $old_buyer_last_name = $row['buyer_last_name'];
                if($type == "CC"){
                    if ($old_buyer_first_name !== $buyer_first_name && $old_buyer_last_name !== $buyer_last_name) {
                        $bactivity_type = "buyer name edit";
                        $bactivity_text = "buyer name is changed from $old_buyer_first_name $old_buyer_last_name to $buyer_first_name $buyer_last_name.";
                    } elseif ($old_buyer_first_name !== $buyer_first_name) {
                        $bactivity_type = "buyer name edit";
                        $bactivity_text = "buyer first name is changed from $old_buyer_first_name to $buyer_first_name";
                    } elseif ($old_buyer_last_name !== $buyer_last_name) {
                        $bactivity_type = "buyer name edit";
                        $bactivity_text = "buyer last name is changed from $old_buyer_last_name to $buyer_last_name";
                    }
                }else{
                    if($type==$row['payment_method']){
                        $log = array("status" => "Failed", "msg" => "Cannot update payment method");
                        $this->response($this->json($log),200);
                    }
                    $bactivity_type = "Payment method update";
//                    $from_method = ($row['payment_method'] == "CA" ? "Cash" : "Check");
//                    $to_method = ($type == "CA" ? "Cash":"Check");
//                    .$from_method." to ".$to_method
                    $bactivity_text = "Payment method changed to Manual";
                }
                if($user_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "You may delete your account yourselves, or the account may be deleted by WePay customer support.");
                    $this->response($this->json($log),200);
                }
                
                if($acc_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log),200);
                }elseif($acc_state=='disabled'){
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log),200);
                }
                if ($type == "CC") {
                    $query1 = sprintf("SELECT ep.`event_payment_id`,ep.`payment_amount`, er.`processing_fee_type` FROM `event_payment` ep LEFT join `event_registration` er USING (`event_reg_id`) WHERE `event_reg_id`='%s' AND `schedule_status` IN ('N','P','F')", mysqli_real_escape_string($this->db, $event_reg_id));
                    $result1 = mysqli_query($this->db, $query1);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $num_rows1 = mysqli_num_rows($result1);
                        if ($num_rows1 > 0) {
                            $sts = $this->getStudioSubscriptionStatus($company_id);
                            $upgrade_status = $sts['upgrade_status'];
                            while ($row = mysqli_fetch_assoc($result1)) {
                                $p_f = $this->getProcessingFee($company_id, $upgrade_status);
                                $process_fee_per = $p_f['PP'];
                                $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);
                                $initial_payment = $row['payment_amount'];
                                $processing_fee_type = $row['processing_fee_type'];
                                $event_payment_id = $row['event_payment_id'];
                                if ($initial_payment <= 0) {
                                    $initial_payment_processing_fee = 0;
                                } else {
                                    $initial_payment_processing_fee = $this->calculateProcessingFee($initial_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                }
                                $update_query = sprintf("UPDATE `event_payment` SET `processing_fee`='%s' WHERE `event_reg_id`='%s' AND `event_payment_id`='%s' AND `schedule_status` IN ('N','P','F')", mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_payment_id));
                                $result_update = mysqli_query($this->db, $update_query);
                                if (!$result_update) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
                    }
                }
                if(!empty($access_token) && !empty($account_id) || ($type == "CA" || $type == "CH")){
                    if($type == "CC"){
                        if (!function_exists('getallheaders')){
                            if(!function_exists ('apache_request_headers')){
                                $user_agent = $_SERVER['HTTP_USER_AGENT'];
                            }else{
                                $headers = apache_request_headers();
                                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                            }
                        }else{
                            $headers = getallheaders();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                        $temp_password = $this->wp_pswd;
                        $method = 'aes-256-cbc';
                        // Must be exact 32 chars (256 bit)
                        $password = substr(hash('sha256', $temp_password, true), 0, 32);
                        // IV must be exact 16 chars (128 bit)
                        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);

    //                    $json = array("account_id"=>"$account_id");
    //                    $postData = json_encode($json);
    //                    $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent,$sns_msg9);
    //                    $this->wp->log_info("wepay_get_account: ".$response['status']);
    //                    if($response['status']=='Success'){
    //                        $res = $response['msg'];
    //                        $account_state = $res['state'];
    //                        $currency = $res['currencies'][0];
    //                        $curr_acc = implode(",", $res['currencies']);
    //                        $disabled_reasons = implode(",", $res['disabled_reasons']);
    //                        if($account_state=='deleted'){
    //                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
    //                            $this->response($this->json($log),200);
    //                        }elseif($account_state=='disabled'){
    //                            $log = array("status" => "Failed", "msg" => "The account has been disabled for following reason(s) - ".$disabled_reasons." by WePay and can no longer accept payments.");
    //                            $this->response($this->json($log),200);
    //                        }
    //                    }else{
    //                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
    //                        $this->response($this->json($log),200);
    //                    }

                        $cc_name = $cc_month = $cc_year = '';
                        $cc_status = $cc_state;
                        $json2 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$new_cc_id,"account_id"=>$account_id);
                        $postData2 = json_encode($json2);
                        $sns_msg9['call'] = "Credit Card";
                        $sns_msg9['type'] = "Credit Card Authorization";
                        $response2 = $this->wp->accessWepayApi("credit_card/authorize",'POST',$postData2,$token,$user_agent,$sns_msg9);
                        $this->wp->log_info("wepay_credit_card_authorize: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $cc_name = $res2['credit_card_name'];
                            $cc_new_state = $res2['state'];
                            if($cc_state!=$cc_new_state){
                                $cc_status = $cc_new_state;
                            }
                            $cc_month = $res2['expiration_month'];
                            $cc_year = $res2['expiration_year'];
                        }else{
                            $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                            $this->response($this->json($error),200);
                        }
                    
//                    if($payment_type=='RE'){
//                        if(date("Y", strtotime($schedule_date))<$cc_year || (date("Y", strtotime($schedule_date))==$cc_year && date("m", strtotime($schedule_date))<=$cc_month)){
//                            
//                        }else{
//                            $error = array("status" => "Failed", "msg" => "Credit card is not valid till the last recurring payment.");
//                            $this->response($this->json($error),200);
//                        }
//                    }
                    }
                    if($type == "CC"){
                        $update_query = sprintf("UPDATE `event_registration` SET payment_method='%s',`buyer_first_name`='%s',`buyer_last_name`='%s',`buyer_email`='%s',`buyer_postal_code`='%s',`credit_card_id`='%s',
                                `credit_card_status`='%s',`credit_card_name`='%s',`credit_card_expiration_month`='%s',`credit_card_expiration_year`='%s' WHERE `event_reg_id`='%s'", mysqli_real_escape_string($this->db, $type), mysqli_real_escape_string($this->db, $buyer_first_name),mysqli_real_escape_string($this->db, $buyer_last_name),
                                mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $postal_code), mysqli_real_escape_string($this->db, $new_cc_id),
                                mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month),
                                mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $event_reg_id));
                    }else{
                         $update_query = sprintf("UPDATE `event_registration` SET `credit_card_id`='', `credit_card_name`='',`payment_method`='%s' WHERE `event_reg_id`='%s'",
                                mysqli_real_escape_string($this->db, $type), mysqli_real_escape_string($this->db, $event_reg_id));   
                    }
                    $result_update = mysqli_query($this->db, $update_query);
                    if(!$result_update){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }else{
                        $curr_date = gmdate("Y-m-d H:i:s");
                        if (!empty($bactivity_type)) {
                            $insert_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')", 
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $student_id, $bactivity_type, $bactivity_text, $curr_date);
                            $result_history = mysqli_query($this->db, $insert_history);
                            if (!$result_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                        if($type == "CC"){
                            $update_query2 = sprintf("UPDATE `event_payment` SET `cc_id`='%s', `cc_name`='%s',credit_method='%s' WHERE `event_reg_id`='%s' AND `schedule_status` IN ('N','F')",
                                    mysqli_real_escape_string($this->db, $new_cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $type), mysqli_real_escape_string($this->db, $event_reg_id));
                        }else{
                            $update_query2 = sprintf("UPDATE `event_payment` SET `cc_id`='', `cc_name`='',`processing_fee`=0,credit_method='%s' WHERE `event_reg_id`='%s' AND `schedule_status` IN ('N','F')",
                                    mysqli_real_escape_string($this->db, $type), mysqli_real_escape_string($this->db, $event_reg_id));
                        }
                        
                        $result_update2 = mysqli_query($this->db, $update_query2);
                        if(!$result_update2){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query2");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    if($type == "CC"){
                        $activity_text = "Payment method changed " . $cc_name;
                        $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", 
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'payment method change',
                                mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                        $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                        if (!$result_insert_event_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                            log_info($this->json($error_log));
                        }
                    }
                    if($page_key == 'ED'){ //ED - Event Detail call
                        $reg_details = $this->getEventParticipantDetailsForRefund($company_id, $event_id, $event_reg_id, 1);
                        $msg = array('status' => "Success", "msg" => "New Payment method successfully updated.", "reg_details" => $reg_details);
                        $this->response($this->json($msg), 200);
                    }else{ // MM - Manage Membership call PP-Payment Page
                        $msg = array('status' => "Success", "msg" => "New Payment method successfully updated.");
                        $this->response($this->json($msg), 200);
                    }
                    
                }else{
                    $error = array('status' => "Failed", "msg" => "Wepay details not exist.");
                    $this->response($this->json($error), 200);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Particpant payment details mismatch.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    private function calculateProcessingFee($amount, $fee_type, $process_fee_per, $process_fee_val) {
        $p1_fee = ($amount * ($process_fee_per / 100)) + $process_fee_val;
        $p2_fee = ($amount + $p1_fee) * ($process_fee_per / 100) + $process_fee_val;
        if ($fee_type === 2 || $fee_type === '2') {
            $p_fee = $p2_fee;
        } else {
            $p_fee = $p1_fee;
        }
        return $p_fee;
    }
                       
    public function wepayCreatePortalCheckout($company_id, $event_id, $actual_student_id,$actual_participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, $payment_type, $upgrade_status, $total_order_amount, $total_order_quantity, $event_type,$reg_type_user,$reg_version_user){
        $this->getStudioSubscriptionStatus($company_id);
        $this->checkQuantityDuringCheckout($company_id,$event_id,$event_array,$quantity, $event_type);
        $buyer_name = $buyer_first_name.' '.$buyer_last_name;  
            if (is_null($actual_student_id) || $actual_student_id == 0) {
                $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Wepay', '');
                $student_id = $stud_array['student_id'];
            } else {
            $student_id = $actual_student_id;
            }
            if (is_null($actual_participant_id) || $actual_participant_id == 0) {
                $participant_id = $this->getParticipantIdForEvent($company_id, $student_id, $reg_col1, $reg_col2);
            } else {
                $participant_id = $actual_participant_id;
            }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        
        $balance_due = $deposit_amt = $pr_fee = $temp_pr_fee = 0;
        $no_of_payments = $authorize_only = 0;
        $payment_startdate = $current_date = date("Y-m-d");
//        $check_deposit_failure = 0;
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg1=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
        $p_f = $this->getProcessingFee($company_id, $upgrade_status);
        $process_fee_per = $p_f['PP'];
        $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);
        if($fee==0 && $processing_fee_type==1){
            $fee = number_format((float)($payment_amount*$process_fee_per/100+$process_fee_val), 2, '.', '');
        }
        
        if(count($payment_array)>0){
            $no_of_payments = count($payment_array)-1;
            for($i=0;$i<count($payment_array);$i++){
                if(isset($payment_array[$i]['date']) && empty($payment_array[$i]['date'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),200);
                }            
                if(isset($payment_array[$i]['amount']) && empty($payment_array[$i]['amount'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),200);
                }
                if(isset($payment_array[$i]['type']) && empty($payment_array[$i]['type'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),200);
                }
                
                if($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount']>0){
//                    $balance_due = $payment_array[$i]['amount'];
                    $deposit_amt = $payment_array[$i]['amount'];
                    $pr_fee = $payment_array[$i]['processing_fee'];
                }elseif($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount'] == 0){
                    $authorize_only = 1;
                    $no_of_payments = count($payment_array);
                }
                $temp_pr_fee += $payment_array[$i]['processing_fee'];
            }
            $fee = $temp_pr_fee;
        }else{
            $pr_fee = $fee;
        }
        
        usort($event_array, function($a, $b) {
            return $a['event_begin_dt'] > $b['event_begin_dt'];
        });
        
        if(count($event_array)>0){
            for($i=0;$i<count($event_array);$i++){
                if(isset($event_array[$i]['event_id']) && empty($event_array[$i]['event_id'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),200);
                }            
                if(isset($event_array[$i]['event_quantity']) && empty($event_array[$i]['event_quantity'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),200);
                }
                if(!isset($event_array[$i]['event_cost'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),200);
                }
                if(!isset($event_array[$i]['event_discount_amount'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),200);
                }            
                if(!isset($event_array[$i]['event_payment_amount'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),200);
                }            
                if(!isset($event_array[$i]['event_begin_dt'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),200);
                }
                if(count($payment_array)>0){
                    if($balance_due>0){
                        $amt = $event_array[$i]['event_payment_amount'];
                        if($balance_due>=$event_array[$i]['event_payment_amount']){
                            if($balance_due==$amt){
                                $t_amt = $amt;
                            }else{
                                $t_amt = $balance_due - $amt;
                            }
                            $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($t_amt, $processing_fee_type, $process_fee_per, $process_fee_val);
                            $event_array[$i]['balance_due'] = 0;
                            $balance_due = $balance_due - $amt;
                        }else{
                            $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($balance_due, $processing_fee_type, $process_fee_per, $process_fee_val);
                            $event_array[$i]['balance_due'] = $amt - $balance_due;
                            $balance_due = 0;
                        }
                    }elseif($balance_due==0){
                        $event_array[$i]['processing_fee'] = 0;
                        $event_array[$i]['balance_due'] = $event_array[$i]['event_payment_amount'];
                    }
                }else{
                    $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($event_array[$i]['event_payment_amount'], $processing_fee_type, $process_fee_per, $process_fee_val);
                    $event_array[$i]['balance_due'] = 0;
                }
            }
        }
        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency` FROM `wp_account` WHERE `company_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                if($user_state=='deleted'){
                    $msg = "Wepay user registered was deleted.";
                    $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
                    $this->response($this->json($log),200);
                }
                
                if($acc_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log),200);
                }elseif($acc_state=='disabled'){
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log),200);
                }
                
                if(!empty($access_token) && !empty($account_id)){
                    if (!function_exists('getallheaders')){
                        if(!function_exists ('apache_request_headers')){
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        }else{
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    }else{
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        
                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                    
//                    $json = array("account_id"=>"$account_id");
//                    $postData = json_encode($json);
//                    $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent,$sns_msg1);
//                    $this->wp->log_info("wepay_get_account: ".$response['status']);
//                    if($response['status']=='Success'){
//                        $res = $response['msg'];
//                        $account_state = $res['state'];
//                        $currency = $res['currencies'][0];
//                        $curr_acc = implode(",", $res['currencies']);
//                        $disabled_reasons = implode(",", $res['disabled_reasons']);
//                        if($account_state=='deleted'){
//                            $msg = "Wepay Account associated with this MyStudio account was deleted.";
//                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
//                            $this->response($this->json($log),200);
//                        }elseif($account_state=='disabled'){
//                            $msg = "Wepay Account associated with this MyStudio account was disabled(For ".$disabled_reasons.").";
//                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
//                            $this->response($this->json($log),200);
//                        }
//                    }else{
//                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                        $this->response($this->json($log),200);
//                    }
                    
                    $paid_amount = $paid_fee = $gross = 0;
                    $checkout_id = $checkout_state = '';                    
                    $cc_name = $cc_month = $cc_year = '';
                    $cc_status = $cc_state;
                    
                    if($payment_amount>0 && $authorize_only==0){
                        $checkout_id_flag = 1;
                        $rand = mt_rand(1,99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout";
                        $unique_id = $company_id."_".$event_id."_".$ip."_".$rand."_".time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id."_".$event_id."_".$date;
//                        if($processing_fee_type==2){
//                            $fee_payer = "payer_from_app";
//                        }elseif($processing_fee_type==1){
                            $fee_payer = "payee_from_app";
//                        }
                        $paid_amount = $payment_amount;
                        $paid_fee = $fee;
                        if(count($payment_array)>0){
                            for($i=0;$i<count($payment_array);$i++){
                                if($payment_array[$i]['type']=='D'){
                                    $paid_amount = $payment_array[$i]['amount'];
                                    $paid_fee = $payment_array[$i]['processing_fee'];
                                    break;
                                }
                            }
                        }
                        
                        if($processing_fee_type==2){
                            $w_paid_amount = $payment_amount + $fee;
                        }elseif($processing_fee_type==1){
                            $w_paid_amount = $payment_amount;
                        }
                        if (count($payment_array) > 0) {
                            for ($i = 0; $i < count($payment_array); $i++) {
                                if ($payment_array[$i]['type'] == 'D') {
                                    if ($processing_fee_type == 2) {
                                        $w_paid_amount = $payment_array[$i]['amount'] + $payment_array[$i]['processing_fee'];
                                    } elseif ($processing_fee_type == 1) {
                                        $w_paid_amount = $payment_array[$i]['amount'];
                                    }
                                    break;
                                }
                            }
                        }
                        
                        $time = time();
                        $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                            "fee"=>array("app_fee"=>$paid_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                            "email_message"=>array("to_payee"=>"Payment has been added for $event_name","to_payer"=>"Payment has been made for $event_name"),
                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                            "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code","country"=>"$country")))),
                            "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                        $postData2 = json_encode($json2);
                        $sns_msg1['call'] = "Checkout";
                        $sns_msg1['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                        $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $gross = $res2['gross'];
//                            $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                        }else{
                            if($response2['msg']['error_code']==1008){
                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                                $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                                if($response2['status']=="Success"){
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $gross = $res2['gross'];
//                                    $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                                }else{
                                    if($response2['msg']['error_code']==1008){
                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error),200);
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error),200);
                            }
                        }
                    }else{
                        $checkout_id_flag = 0;
                        if(!empty(trim($cc_id))){
                            $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                            $postData3 = json_encode($json3);
                            $sns_msg1['call'] = "Credit Card";
                            $sns_msg1['type'] = "Credit card details";
                            $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                            $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                            if($response3['status']=="Success"){
                                $res3 = $response3['msg'];
                                if($res3['state']=='new'){
                                    $json2 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id,"account_id"=>$account_id);
                                    $postData2 = json_encode($json2);                                    
                                    $sns_msg1['call'] = "Credit Card";
                                    $sns_msg1['type'] = "Credit Card Authorization";
                                    $response2 = $this->wp->accessWepayApi("credit_card/authorize",'POST',$postData2,$token,$user_agent,$sns_msg1);
                                    $this->wp->log_info("wepay_credit_card_authorize: ".$response2['status']);
                                    if($response2['status']=="Success"){
                                        $res2 = $response2['msg'];
                                        $cc_name = $res2['credit_card_name'];
                                        $cc_new_state = $res2['state'];
                                        if($cc_state!=$cc_new_state){
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res2['expiration_month'];
                                        $cc_year = $res2['expiration_year'];
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }elseif($res3['state']=='authorized'){
                                    $cc_name = $res3['credit_card_name'];
                                    $cc_new_state = $res3['state'];
                                    $cc_month = $res3['expiration_month'];
                                    $cc_year = $res3['expiration_year'];
                                }else{
                                    if($res3['state']=='expired'){
                                        $msg = "Given credit card is expired.";
                                    }elseif($res3['state']=='deleted'){
                                        $msg = "Given credit card was deleted.";
                                    }else{
                                        $msg = "Given credit card details is invalid.";
                                    }
                                    $error = array("status" => "Failed", "msg" => $msg);
                                    $this->response($this->json($error),200);
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                $this->response($this->json($error),200);
                            }
                        }
//                        $json2 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id,"account_id"=>$account_id);
//                        $postData2 = json_encode($json2);
//                        $response2 = $this->wp->accessWepayApi("credit_card/authorize",'POST',$postData2,$token,$user_agent);
//                        $this->wp->log_info("wepay_credit_card_authorize: ".$response2['status']);
//                        if($response2['status']=="Success"){
//                            $res2 = $response2['msg'];
//                            $cc_name = $res2['credit_card_name'];
//                            $cc_new_state = $res2['state'];
//                            if($cc_state!=$cc_new_state){
//                                $cc_status = $cc_new_state;
//                            }
//                            $cc_month = $res2['expiration_month'];
//                            $cc_year = $res2['expiration_year'];
//                        }else{
//                            $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                            $this->response($this->json($error),200);
//                        }
                    }
                    
                    if(!empty(trim($cc_id))){
                        $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                        $postData3 = json_encode($json3);
                        $sns_msg1['call'] = "Credit Card";
                        $sns_msg1['type'] = "Credit Card details";
                        $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                        $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                        if($response3['status']=="Success"){
                            $res3 = $response3['msg'];
                            $cc_name = $res3['credit_card_name'];
                            $cc_new_state = $res3['state'];
                            if($cc_state!=$cc_new_state){
                                $cc_status = $cc_new_state;
                            }
                            $cc_month = $res3['expiration_month'];
                            $cc_year = $res3['expiration_year'];
                        }else{
                            for($k=0;$k<2;$k++){
                                if($response3['msg']['error_code'] == 503){
                                    sleep(2);
                                    $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                                    $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                                    if($response3['status']=="Success"){
                                        $res3 = $response3['msg'];
                                        $cc_name = $res3['credit_card_name'];
                                        $cc_new_state = $res3['state'];
                                        if($cc_state!=$cc_new_state){
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res3['expiration_month'];
                                        $cc_year = $res3['expiration_year'];
                                        break;
                                    }
                                    if($k==1){
                                        if($checkout_id_flag == 1){
                                            $type = $response3['msg']['error'];
                                            $description =  $response3['msg']['error_description']." Buyer Name:".$buyer_name." Buyer Email:".$buyer_email." Checkout Id:".$checkout_id." Participant Name:".$reg_col1." ".$reg_col2;      
                                            $insert_alertquery = sprintf("INSERT INTO `wepay_critical_alert`(`company_id`, `type`, `description`) VALUES ('%s','%s','%s')",mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $type),mysqli_real_escape_string($this->db, $description));
                                            $result_alert = mysqli_query($this->db, $insert_alertquery);
                                            if(!$result_alert){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_alertquery");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            } 
                                            $type1 = $response3['msg']['error']." Regarding Credit Card";
                                            $description1 = "Fetching Credit Card details failed"."\n"." Buyer Name:".$buyer_name."\n"." Buyer Email:".$buyer_email."\n"." Checkout Id:".$checkout_id."\n"." Participant Name:".$reg_col1." ".$reg_col2;     
                                            $this->sendSnsforfailedcheckout($type1,$description1);
                                        }
                                        $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }else{
                                    $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                    $this->response($this->json($error),200);
                                }
                            }
                        }
                    }
                    
                    
                    
                    $query = sprintf("INSERT INTO `event_registration`(`company_id`, `event_id`, `reg_event_type`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                        `payment_type`, `payment_amount`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `deposit_amount`, `number_of_payments`, `payment_startdate`, `payment_frequency`, `total_order_amount`, `total_order_quantity`, 
                        `event_registration_column_1`, `event_registration_column_2`, `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`, `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`,`event_reg_type_user`,`event_reg_version_user`)
                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_type),
                            mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db,$participant_id),mysqli_real_escape_string($this->db, $buyer_first_name),mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code),
                            mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_type), 
                            mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name),
                            mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $deposit_amt), mysqli_real_escape_string($this->db, $no_of_payments), mysqli_real_escape_string($this->db, $payment_startdate), mysqli_real_escape_string($this->db, $payment_frequency), 
                            mysqli_real_escape_string($this->db, $total_order_amount), mysqli_real_escape_string($this->db, $total_order_quantity), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4),
                            mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user));
                    $result = mysqli_query($this->db, $query);
                    if(!$result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }else{
                        $event_reg_id = mysqli_insert_id($this->db);
                        
                        if(count($payment_array)>0){
                            $temp1 = $temp4 = 0;
                            for($i=0;$i<count($payment_array);$i++){
                                $pdate = $payment_array[$i]['date'];
                                $pamount = $payment_array[$i]['amount'];
                                $ptype = $payment_array[$i]['type'];
                                $pfee = $payment_array[$i]['processing_fee'];
                                if($ptype!='D'){
                                    $checkout_id = '';
                                    $checkout_state = '';
                                    $pstatus = 'N';
                                }else{
                                    $pstatus = 'S';
                                } 
                                $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_date`, `schedule_status`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), 
                                        mysqli_real_escape_string($this->db, $pfee), mysqli_real_escape_string($this->db, $pamount), mysqli_real_escape_string($this->db, $pdate), mysqli_real_escape_string($this->db, $pstatus), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name));
                                $payment_result = mysqli_query($this->db, $payment_query);
                                if(!$payment_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                                    log_info($this->json($error_log));
                                    $temp1 += 1;
                                }
                            }
                            if($temp1>0){
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp1);
                                $this->response($this->json($error), 200);
                            }
                        }else{
                            $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_status`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), 
                                    mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name));
                            $payment_result = mysqli_query($this->db, $payment_query);
                            if(!$payment_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                        
                        if(count($event_array)>0){
                            $qty = $net_sales = $parent_net_sales = 0;
                            $temp = $temp2= $temp3 = 0;
                            for($i=0;$i<count($event_array);$i++){
                                $cevent_id = $event_array[$i]['event_id'];
                                $cquantity =  $event_array[$i]['event_quantity'];
                                $cevent_cost = $event_array[$i]['event_cost'];
                                $cdiscount = $event_array[$i]['event_discount_amount'];
                                $cpayment_amount = $event_array[$i]['event_payment_amount'];
                                $cbalance = $event_array[$i]['balance_due'];
                                $cprocessing_fee = $event_array[$i]['processing_fee'];
                                if(count($payment_array)>0){
                                    if($cbalance>0){
                                        $cstatus = 'RP';
                                    }else{
                                        $cstatus = 'RC';
                                    }
                                }else{
                                    $cstatus = 'CC';
                                }
                        
                                $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $cevent_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cevent_cost), 
                                        mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cbalance), mysqli_real_escape_string($this->db, $cprocessing_fee), mysqli_real_escape_string($this->db, $cstatus));
                                $reg_det_result = mysqli_query($this->db, $reg_det_query);
                                if(!$reg_det_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                                    log_info($this->json($error_log));
                                    $temp += 1;
                                }
                                
                                $pd_amount = $cpayment_amount - $cbalance;
                                
                                if(count($payment_array)>0){
                                    if($processing_fee_type==2){
                                        $net_sales += $pd_amount;
                                        $parent_net_sales += $pd_amount;
                                    }elseif($processing_fee_type==1){
                                        $net_sales += $pd_amount-$cprocessing_fee;
                                        $parent_net_sales += $pd_amount-$cprocessing_fee;
                                    }
                                }else{
                                    if($processing_fee_type==2){
                                        $parent_net_sales = $payment_amount;
                                    }else{
                                        $parent_net_sales = $payment_amount-$fee;
                                    }
                                }
                                $qty += $cquantity;
//                                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                                $update_event_result = mysqli_query($this->db, $update_event_query);
                                if(!$update_event_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                                    log_info($this->json($error_log));
                                    $temp2 += 1;
                                }
                            }
                            if($temp>0){
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp);
                                $this->response($this->json($error), 200);
                            }
                            if($temp2>0){
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp2);
                                $this->response($this->json($error), 200);
                            }
                            
                            $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$qty WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                            $update_event_result = mysqli_query($this->db, $update_event_query);
                            if(!$update_event_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }else{
//                            $sbalance = $payment_amount - $paid_amount;
                            $sbalance = $payment_amount;
                            if($sbalance>0){
                                $fee = $pr_fee;
                            }
                            if($processing_fee_type==1){
                                $paid_amount -= $fee;
                            }
                            if(count($payment_array)>0){
                                if($sbalance>0){
                                    $sstatus = 'RP';
                                }else{
                                    $sstatus = 'RC';
                                }
                            }else{
                                $sstatus = 'CC';
                            }
                            $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $registration_amount), 
                                    mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $quantity), mysqli_real_escape_string($this->db, $sbalance), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $sstatus));
                            $reg_det_result = mysqli_query($this->db, $reg_det_query);
                            if(!$reg_det_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                            
                            $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                            $update_event_result = mysqli_query($this->db, $update_event_query);
                            if(!$update_event_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                        
                        $selectcurrency=sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'",  mysqli_escape_string($this->db, $company_id));
                        $resultselectcurrency=  mysqli_query($this->db, $selectcurrency);
                        if(!$resultselectcurrency){
                             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                            log_info($this->json($error_log));
                        }else{
                            $row = mysqli_fetch_assoc($resultselectcurrency);
                            $wp_currency_symbol=$row['wp_currency_symbol'];
                        }
                        
                        $curr_date = gmdate("Y-m-d H:i:s");
                        if ($event_type == 'S') {
                            if ($discount!=0 || !empty($discount)) {
                                $activity_text = "Registered for $event_name, quantity $quantity. Total cost $wp_currency_symbol".$payment_amount.". Discount amount used $wp_currency_symbol.$discount";
                            } else {
                                $activity_text = "Registered for $event_name, quantity $quantity. Total cost $wp_currency_symbol".$payment_amount;
                            }
                            $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id),
                                    'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                            $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                            if (!$result_insert_event_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                                log_info($this->json($error_log));
                            }
                        } else {
                            for ($i = 0; $i < count($event_array); $i++) {
                                $event_title = $event_array[$i]['event_title'];
                                $event_quantity = $event_array[$i]['event_quantity'];
                                $event_cost = $event_array[$i]['event_payment_amount'];
                                $event_discount_amount = $event_array[$i]['event_discount_amount'];
//                                log_info("event_cost".$event_cost."event_discount_amount".$event_discount_amount);
                                if ($event_discount_amount!=0 || !empty($event_discount_amount)) {
                                    $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol".($event_cost).". Discount amount used $wp_currency_symbol".($event_discount_amount);
                                } else {
                                    $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol".$event_cost;
                                }
                                $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id),
                                        'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                                $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                                if (!$result_insert_event_history) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                                    log_info($this->json($error_log));
                                }
                            }
                        }
                        $event_details = $this->getEventsDetailByEventId($company_id, $event_id, $event_type, 'live', 1);
                        $msg = array("status" => "Success", "msg" => "Event registration was successful. Email confirmation sent.", "event_details"=>$event_details);
                        $this->responseWithWepay($this->json($msg),200);
                        $this->sendOrderReceiptForEventPayment($company_id,$event_reg_id,0);
                    }
                }
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error),200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function addParticipantDetailsForFreeEvent($company_id, $event_id, $actual_student_id,$actual_participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_type, $event_array,$reg_type_user,$reg_version_user) {
        $this->getStudioSubscriptionStatus($company_id);
        $this->checkQuantityDuringCheckout($company_id,$event_id,$event_array,$quantity, $event_type);
        $buyer_name = $buyer_first_name.' '.$buyer_last_name;  
        if (is_null($actual_student_id) || $actual_student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Wepay', '');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $actual_student_id;
        }
        if (is_null($actual_participant_id) || $actual_participant_id == 0) {
            $participant_id = $this->getParticipantIdForEvent($company_id, $student_id, $reg_col1, $reg_col2);
        } else {
            $participant_id = $actual_participant_id;
        }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";        
//        $student_id = $stud_id;
//        if ($stud_id == 0) {
//            $squery = sprintf("SELECT * FROM `student` WHERE `student_email`='%s'  order by `last_login_dt` desc limit 0,1", mysqli_real_escape_string($this->db, $buyer_email));
//            $sresult = mysqli_query($this->db, $squery);
//
//            if (!$sresult) {
//                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$squery");
//                log_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                $this->response($this->json($error), 200);
//            } else {
//                if (mysqli_num_rows($sresult) > 0) {
//                    $row = mysqli_fetch_assoc($sresult);
//                    $student_id = $row['student_id'];
////                    $error = array('status' => "Failed", "msg" => "Student already registered.");
////                    $this->response($this->json($error), 200);
//                } else {
//                    $iquery = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_email`) VALUES ('%s','%s','%s')",
//                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $buyer_name), mysqli_real_escape_string($this->db, $buyer_email));
//                    $iresult = mysqli_query($this->db, $iquery);
//
//                    if (!$iresult) {
//                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$iquery");
//                        log_info($this->json($error_log));
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                        $this->response($this->json($error), 200);
//                    } else {
//                        $student_id = mysqli_insert_id($this->db);
//                    }
//                }
////                if($stud_id==$student_id){
////                    $error = array('status' => "Failed", "msg" => "Student cannot be added at this time, try again later.");
////                    $this->response($this->json($error), 200);
////                }
//            }
//        }
        $current_date = date("Y-m-d");
        $payment_type = 'F';
        $checkout_id = $checkout_state = '';
        $payment_method = 'CC';
        $query = sprintf("INSERT INTO `event_registration`(`company_id`, `event_id`,`reg_event_type`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
            `payment_type`, `payment_amount`, `paid_amount`, `event_registration_column_1`, `event_registration_column_2`,
            `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`, `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`,`event_reg_type_user`,`event_reg_version_user`,`payment_method`)
                VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_type),
                mysqli_real_escape_string($this->db, $student_id),mysqli_real_escape_string($this->db, $participant_id), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code),
                mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_type),
                mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4),
                mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10),
                mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $payment_method));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $event_reg_id = mysqli_insert_id($this->db);
            $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_status`) VALUES ('%s','%s','%s','%s','%s','%s')",
                    mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state),
                    mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, 'S'));
            $payment_result = mysqli_query($this->db, $payment_query);
            if (!$payment_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }

            if (count($event_array) > 0) {
                $qty = $net_sales = 0;
                $temp = $temp2 = $temp3 = 0;
                for ($i = 0; $i < count($event_array); $i++) {
                    $cevent_id = $event_array[$i]['event_id'];
                    $cquantity = $event_array[$i]['event_quantity'];
                    $cevent_cost = $event_array[$i]['event_cost'];
                    $cdiscount = $event_array[$i]['event_discount_amount'];
                    $cpayment_amount = $event_array[$i]['event_payment_amount'];
                    $cstatus = 'CC';

                    $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $cevent_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cevent_cost),
                            mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cstatus));
                    $reg_det_result = mysqli_query($this->db, $reg_det_query);
                    if (!$reg_det_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                        mlog_info($this->json($error_log));
                        $temp += 1;
                    }

                    $qty += $cquantity;

                    $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                    $update_event_result = mysqli_query($this->db, $update_event_query);
                    if (!$update_event_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                        mlog_info($this->json($error_log));
                        $temp2 += 1;
                    }
                }
                if ($temp > 0) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp);
                    $this->response($this->json($error), 200);
                }
                if ($temp2 > 0) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp2);
                    $this->response($this->json($error), 200);
                }

                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$qty WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                $update_event_result = mysqli_query($this->db, $update_event_query);
                if (!$update_event_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            } else {

                $sstatus = 'CC';
                $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $quantity), mysqli_real_escape_string($this->db, $sstatus));
                $reg_det_result = mysqli_query($this->db, $reg_det_query);
                if (!$reg_det_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }

                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                $update_event_result = mysqli_query($this->db, $update_event_query);
                if (!$update_event_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
            $selectcurrency = sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'", mysqli_escape_string($this->db, $company_id));
            $resultselectcurrency = mysqli_query($this->db, $selectcurrency);
            if (!$resultselectcurrency) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                log_info($this->json($error_log));
            } else {
                $row = mysqli_fetch_assoc($resultselectcurrency);
                $wp_currency_symbol = $row['wp_currency_symbol'];
            }

            $curr_date = gmdate("Y-m-d H:i:s");
            if ($event_type == 'S') {
                if ($discount!=0 || !empty($discount)) {
                    $activity_text = "Registered for $event_name, quantity $quantity. Total cost $wp_currency_symbol".$payment_amount.". Discount amount used $wp_currency_symbol.$discount";
                } else {
                    $activity_text = "Registered for $event_name, quantity $quantity. Total cost $wp_currency_symbol".$payment_amount;
                }
                $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                if (!$result_insert_event_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                    log_info($this->json($error_log));
                }
            } else {
                for ($i = 0; $i < count($event_array); $i++) {
                    $event_title = $event_array[$i]['event_title'];
                    $event_quantity = $event_array[$i]['event_quantity'];
                    $event_cost = $event_array[$i]['event_cost'];
                    $event_discount_amount = $event_array[$i]['event_discount_amount'];
                    if ($event_discount_amount!=0 || !empty($event_discount_amount)) {
                        $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol".$event_cost.". Discount amount used $wp_currency_symbol".$event_discount_amount;
                    } else {
                        $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol".$event_cost;
                    }
                    $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                    $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                    if (!$result_insert_event_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                        log_info($this->json($error_log));
                    }
                }
            }

            $event_details = $this->getEventsDetailByEventId($company_id, $event_id, $event_type, 'live', 1);
            $msg = array("status" => "Success", "msg" => "Event registration was successful. Email confirmation sent.", "event_details" => $event_details);
            $this->responseWithWepay($this->json($msg), 200);
            $this->sendOrderReceiptForEventPayment($company_id, $event_reg_id, 0);
        }
        date_default_timezone_set($curr_time_zone);
    }

    protected function rbitCallForReRunCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $event_name, $amount, $currency){
        $time = time();
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg2=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
        $json = array("associated_object_type"=>"checkout","associated_object_id"=>$checkout_id,"receive_time"=>$time,"type"=>"person","source"=>"user","properties"=>array("name"=>"$buyer_name"), 
            "related_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))));
        $postData = json_encode($json);
        $response = $this->wp->accessWepayApi("rbit/create",'POST',$postData,$token,$user_agent,$sns_msg2);
        $this->wp->log_info("wepay_rbit_create: ".$response['status']);
        if($response['status']=="Success"){
            $json2 = array("associated_object_type"=>"checkout","associated_object_id"=>$checkout_id,"receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$amount, "quantity"=>1, "amount"=>$amount, "currency"=>"$currency"))));
            $postData2 = json_encode($json2);
            $response2 = $this->wp->accessWepayApi("rbit/create",'POST',$postData2,$token,$user_agent,$sns_msg2);
            $this->wp->log_info("wepay_rbit_create: ".$response2['status']);
            if($response2['status']=="Success"){
                $error = array("status" => "Success", "msg" => "Checkout rbit info created successfully.");
//                $this->response($this->json($error), 200);
                log_info($this->json($error));
            }else{
                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                $this->response($this->json($error), 200);
                log_info($this->json($error));
            }
        }else{            
            $error = array("status" => "Failed", "msg" => $response['msg']['error_description']);
            log_info($this->json($error));
           
        }
    }
    
    public function reRunEventPayment($company_id,$reg_id,$payment_id){
//        log_info("runCronJobForEventPreapproval() - Started");  
        $query = sprintf("SELECT  `registration_from` FROM `event_registration` 
                    WHERE `company_id`='%s' AND `event_reg_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $rows = mysqli_fetch_assoc($result);
            $registration_from = $rows['registration_from'];
            if ($registration_from == 'S'){
                $this->reRunstripeEventPayment($company_id, $reg_id, $payment_id);
            }    
        }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;        

        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
        
        $query = sprintf("SELECT `access_token`, wp.`account_id`, wp.`account_state`, wp.`currency`, er.`company_id`, er.`student_id`, e.`event_title`, er.`event_reg_id`, `event_payment_id`, er.`event_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, 
                `registration_amount`, ep.`processing_fee`, `payment_type`, er.`payment_frequency`, er.`payment_amount` total_due, `paid_amount`, `credit_card_id`, `credit_card_name`, `credit_card_status`, `total_order_amount`, `total_order_quantity`, 
                ep.`payment_amount`, ep.`schedule_date`, ep.`schedule_status`, er.`processing_fee_type`, ep.`checkout_id`, ep.`checkout_status`,c.`wp_currency_symbol` 
                FROM `event_registration` er 
                LEFT JOIN `wp_account` wp ON er.`company_id` = wp.`company_id` 
                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` AND (ep.`schedule_status`='F' || (ep.`schedule_status`='N' && ep.`schedule_date`< ($currdate_add)))
                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
                LEFT JOIN `company` c ON er.`company_id` = c.`company_id` 
                WHERE   er.`event_reg_id` = $reg_id  AND `event_payment_id` = $payment_id  AND ep.`payment_amount`>0");
//         WHERE ep.`schedule_date`=now() AND `payment_type`='RE' AND ep.`payment_amount`>0 ORDER BY er.`company_id`
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $company_list = [];
                while($row = mysqli_fetch_assoc($result)){
                    $company_id = $row['company_id'];
                    $access_token = $row['access_token'];
                    $account_id = $row['account_id'];
                    $acc_state = $row['account_state'];
                    $currency = $row['currency'];
                    $event_reg_id = $row['event_reg_id'];
                    $event_payment_id = $row['event_payment_id'];
                    $old_checkout_id = $row['checkout_id'];
                    $old_checkout_status = $row['checkout_status'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $buyer_postal_code = $row['buyer_postal_code'];
                    $cc_id = $row['credit_card_id'];
                    $cc_name = $row['credit_card_name'];
                
                    if($acc_state=='deleted'){
                        $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                        $this->response($this->json($log),200);
                    }elseif($acc_state=='disabled'){
                        $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                        $this->response($this->json($log),200);
                    }
                    
                    if(!empty($access_token)){
                        $failure = $success = 0;
                        $event_id = $row['event_id'];
                        $event_name = $desc = $row['event_title'];
                        $payment_amount = $row['payment_amount'];
                        $processing_fee = $row['processing_fee'];
                        $processing_fee_type = $row['processing_fee_type'];
                        $wp_currency_symbol = $row['wp_currency_symbol'];
                        $schedule_date = $row['schedule_date'];
                        $gmt_date=gmdate(DATE_RFC822);
                        $sns_msg3=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
                
//                        if (!function_exists('getallheaders')){
//                            if(!function_exists ('apache_request_headers')){
//                                $user_agent = $_SERVER['HTTP_USER_AGENT'];
//                            }else{
//                                $headers = apache_request_headers();
//                                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
//                            }
//                        }else{
//                            $headers = getallheaders();
//                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
//                        }
                        $user_agent = "mystudio.academy";
                        $temp_password = $this->wp_pswd;
                        $method = 'aes-256-cbc';
                        // Must be exact 32 chars (256 bit)
                        $password = substr(hash('sha256', $temp_password, true), 0, 32);
                        // IV must be exact 16 chars (128 bit)
                        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                        
//                        $json = array("account_id"=>"$account_id");
//                        $postData = json_encode($json);
//                        $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent,$sns_msg3);
//                        log_info("wepay_get_account: ".$response['status']);
//                        if($response['status']=='Success'){
//                            $res = $response['msg'];
//                            $account_state = $res['state'];
//                            $currency = $res['currencies'][0];
//                            $action_reasons = implode(",", $res['action_reasons']);
//                            $disabled_reasons = implode(",", $res['disabled_reasons']);
//                            $curr_acc = implode(",", $res['currencies']);
//                            if($account_state=='deleted'){
//                                $error = array('status' => "Failed", "msg" => "Wepay Account has been deleted.");
//                                $this->response($this->json($error), 200);
//                            }elseif($account_state=='disabled'){
//                                $error = array('status' => "Failed", "msg" => "The account has been disabled for following reason(s) - ".$disabled_reasons." by WePay and can no longer accept payments.");
//                                $this->response($this->json($error), 200);
//                            }
//                        }else{
//                            $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
//                            $this->response($this->json($error), 200);
//                        }
                        
                        $rand = mt_rand(1,99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout";
                        $unique_id = $company_id."_".$event_id."_".$ip."_".$rand."_".time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id."_".$event_id."_".$date;
//                        if($processing_fee_type==2){
//                            $fee_payer = "payer_from_app";
//                        }elseif($processing_fee_type==1){
                            $fee_payer = "payee_from_app";
//                        }
                        if($processing_fee_type==2){
                            $w_paid_amount = $payment_amount + $processing_fee;
                        }elseif($processing_fee_type==1){
                            $w_paid_amount = $payment_amount;
                        }
                        $time = time();
                        $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                            "fee"=>array("app_fee"=>$processing_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                            "email_message"=>array("to_payee"=>"Payment has been added for $event_name","to_payer"=>"Payment has been made for $event_name"),
                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                            "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))),
                            "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                "properties"=>array("itemized_receipt"=>array(array("description"=>"$event_name", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                        $postData2 = json_encode($json2);                        
                        $sns_msg3['call'] = "Checkout";
                        $sns_msg3['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg3);
                        log_info("wepay_checkout_create: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $success=1;
//                            $this->rbitCallForReRunCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $event_name, $w_paid_amount, $currency);
                        }else{
                            if($response2['msg']['error_code']==1008){
                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg3);
                                log_info("wepay_checkout_create: ".$response2['status']);
                                if($response2['status']=="Success"){
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $success=1;
//                                    $this->rbitCallForReRunCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $event_name, $w_paid_amount, $currency);
                                }else{
                                    if($response2['msg']['error_code']==1008){
                                        $error = array('status' => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error), 200);
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }
                            }else{
                                $error = array('status' => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error), 200);
                            }
                        }
                        
                        $payment_query = sprintf("UPDATE `event_payment` SET `checkout_id`='%s', `checkout_status`='%s', `schedule_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name' WHERE `event_payment_id`='%s'",
                                mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, 'S'),
                                mysqli_real_escape_string($this->db, $event_payment_id));
                        $payment_result = mysqli_query($this->db, $payment_query);
                        if(!$payment_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else{
//                            $num_affected_rows = mysqli_affected_rows($this->db);
//                            if ($num_affected_rows > 0) {
                                $activity_date_time = gmdate("Y-m-d H:i:s");
                                $activity_type = "Rerun payment";
//                            $text_date = date("M d, Y",strtotime($activity_date_time));
                                if (empty($schedule_date)) {
                                    $text_date1 = "";
                                } else {
                                    $text_date1 = date("M d, Y", strtotime($schedule_date));
                                }
                                $activity_text = "Rerun payment $wp_currency_symbol" . "$w_paid_amount" . " past due " . "$text_date1 paid";
                                $insert_pay_history = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) values ('%s','%s','%s','%s','%s','%s')", mysqli_escape_string($this->db, $company_id), mysqli_escape_string($this->db, $reg_id)
                                        , mysqli_escape_string($this->db, $event_payment_id), mysqli_escape_string($this->db, $activity_type), mysqli_escape_string($this->db, $activity_text), mysqli_escape_string($this->db, $activity_date_time)); //work
                                $result_pay_history = mysqli_query($this->db, $insert_pay_history);

                                if (!$result_pay_history) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_pay_history");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                                $error = array('status' => "Success", "msg" => "Payment Successful.");
                                $this->response($this->json($error), 200);

//                                if(empty(trim($old_checkout_id)) && empty(trim($old_checkout_status))){
//                                    $temp_payment_amount = $payment_amount;
//                                    $onetime_netsales_for_parent_event = 0;
//
//                                    $update_event_reg_query = sprintf("UPDATE `event_registration` SET `paid_amount`=`paid_amount`+$payment_amount WHERE `event_reg_id`='%s'",$event_reg_id);
//                                    $result_update_event_reg_query = mysqli_query($this->db, $update_event_reg_query);
//                                    if(!$result_update_event_reg_query){
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_reg_query");
//                                        log_info($this->json($error_log));
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                        $this->response($this->json($error), 200);
//                                    }
//
//                                    $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
//                                               LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
//                                               AND `payment_status` = 'RP' ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
//                                    $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
//                                    if(!$get_event_details_result){
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query");
//                                        log_info($this->json($error_log));
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                        $this->response($this->json($error), 200);
//                                    }else{
//                                        $num_rows2 = mysqli_num_rows($get_event_details_result);
//                                        if($num_rows2>0){
//                                            $rem_due = 0;
//                                            while($row2 = mysqli_fetch_assoc($get_event_details_result)){
//                                                if($temp_payment_amount>0){
//                                                    $event_reg_details_id = $row2['event_reg_details_id'];
//                                                    $child_id = $row2['event_id'];
//                                                    $event_parent_id = $row2['parent_id'];
//                                                    $rem_due = $row2['balance_due'];
//                                                    $child_net_sales = 0;
//                                                    if($onetime_netsales_for_parent_event==0){
//                                                        if($processing_fee_type==2){
//                                                            $net_sales = $payment_amount;
//                                                        }elseif($processing_fee_type==1){
//                                                            $net_sales = $payment_amount-$processing_fee;
//                                                        }
//                                                        $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
//                                                        $result_update_event_query = mysqli_query($this->db, $update_event_query);
//                                                        if(!$result_update_event_query){
//                                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
//                                                            log_info($this->json($error_log));
//                                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                                            $this->response($this->json($error), 200);
//                                                        }
//                                                        $onetime_netsales_for_parent_event=1;
//                                                    }
//                                                    if($temp_payment_amount>=$rem_due){
//                                                        $temp_payment_amount = $temp_payment_amount-$rem_due;
//                                                        $child_net_sales = $rem_due;
//                                                        $rem_due = 0;
//                                                        $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due, `payment_status`='RC' WHERE `event_reg_details_id`='%s'", 
//                                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
//                                                    }else{
//                                                        $rem_due = $rem_due - $temp_payment_amount;
//                                                        $child_net_sales = $temp_payment_amount;
//                                                        $temp_payment_amount = 0;
//                                                        $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due WHERE `event_reg_details_id`='%s'", 
//                                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
//                                                    }
//                                                    $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
//                                                    if(!$result_update_reg_query){
//                                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query");
//                                                        log_info($this->json($error_log));
//                                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                                        $this->response($this->json($error), 200);
//                                                    }else{
//                                                        if($child_id!=$event_parent_id){
//                                                            $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
//                                                            $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
//                                                            if(!$result_update_child_query){
//                                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales");
//                                                                log_info($this->json($error_log));
//                                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                                                $this->response($this->json($error), 200);
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                            }
//                                        }else{
//                                            $error = array('status' => "Failed", "msg" => "Participant details doesn't exist.");
//                                            $this->response($this->json($error), 200);
//                                        }
//                                    }
//                                }else{
//                                    $error = array('status' => "Success", "msg" => "Payment Successful.");
//                                    $this->response($this->json($error), 200);
//                                }
//                            } else{
//                                $payment_history = $this->getpaymentHistoryDetails($company_id, $reg_id, 1);
//                                $error = array('status' => "Success", "msg" => "Payment Successful.", "payment"=>$payment_history);
//                                $this->response($this->json($error), 200);
//                            }
                        }

                    }else{
                        $error = array('status' => "Failed", "msg" => "Access token error. Cannot be accept payments anymore.");
                        $this->response($this->json($error), 200);
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Event payment details mismatched.");
                $this->response($this->json($error), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone='';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_tz);                       
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }

    public function checkQuantityDuringCheckout($company_id, $event_id, $event_array, $quantity, $event_type) {
        $event_capacity = $registration_count = $remaining_qty = $child_qty = 0;
        if ($event_type == "M") {
            $capacity_query = sprintf("SELECT `event_id`,`event_capacity`,`registrations_count`,`event_title` FROM `event` WHERE `parent_id`='%s' and `company_id`='%s' ", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
        } else if ($event_type == "S") {
            $capacity_query = sprintf("SELECT `event_id`,`event_capacity`,`registrations_count`,`event_title` FROM `event` WHERE `event_id`='%s' and `company_id`='%s' ", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
        }
        $cap_result = mysqli_query($this->db, $capacity_query);
        if (!$cap_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$capacity_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($cap_result) > 0) {
                while ($row_1 = mysqli_fetch_assoc($cap_result)) {
                    $output_1[] = $row_1;
                }
                if ($event_type == "S") {
                    $event_capacity = $output_1[0]['event_capacity'];
                    $registration_count = $output_1[0]["registrations_count"];
                    if (isset($event_capacity) && $event_capacity > 0) {
                        $remaining_qty = $event_capacity - $registration_count;
                        if ($remaining_qty == 0) {
                            $error = array('status' => "Failed", "msg" => "Event Sold Out.");
                            $this->response($this->json($error), 200);
                        }
                        if ($remaining_qty < $quantity) {
                            $error = array('status' => "Failed", "msg" => "Only $remaining_qty Capacity is Available  .");
                            $this->response($this->json($error), 200);
                        }
                    }elseif (isset($event_capacity) && $event_capacity == 0){
                        $error = array('status' => "Failed", "msg" => "Event Sold Out.");
                        $this->response($this->json($error), 200); 
                    }
                } elseif ($event_type == "M") {
                    $failure_flag=0;
                    $error_msg='';
                    for ($idx = 0; $idx < count($event_array); $idx++) {
                        $event_capacity = $registration_count = $remaining_qty = $child_qty = 0;
                        $event_title='';
                        $child_qty = $event_array[$idx]["event_quantity"]; 
                        for ($idx_1 = 0; $idx_1 < count($output_1); $idx_1++) {
                            if($output_1[$idx_1]['event_id']==$event_array[$idx]['event_id']){
                                $event_capacity = $output_1[$idx_1]['event_capacity'];
                                $registration_count = $output_1[$idx_1]["registrations_count"];
                                $event_title= $output_1[$idx_1]["event_title"];
                                if (isset($event_capacity) && $event_capacity > 0) {
                                    $remaining_qty = $event_capacity - $registration_count;
                                    if ($remaining_qty == 0) {
                                        $failure_flag=1;
                                        $error_msg .= "Event $event_title Sold Out. ";
                                    }
                                    if ($remaining_qty < $child_qty) {
                                        $failure_flag=1;
                                        $error_msg .= "Only $remaining_qty Capacity is available for $event_title. ";
                                    }
                                }elseif (isset($event_capacity) && $event_capacity == 0){
                                    $error = array('status' => "Failed", "msg" => "Event $event_title Sold Out.");
                                    $this->response($this->json($error), 200); 
                                }
                            }
                        }
                    }
                    if($failure_flag==1){
                        $error = array('status' => "Failed", "msg" => $error_msg);
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
    }
    
    public function sendGroupPushMessageForEvent($company_id, $subject, $message, $event_reg_id, $also_send_mail, $attached_file, $file_name,$all_select_flag,$event_id,$status,$search){
        $output1 = $ios = $android = [];
        if($all_select_flag=='Y'){
            
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
       
        $date_format = "'%b %d, %Y'";
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $last_updt_date = " DATE(CONVERT_TZ(erd.last_updt_dt,$tzadd_add,'$new_timezone'))";
        
        
        if(!empty($search)){
             $s_text=" and ( buyer_name like  '%$search%'  or buyer_email like  '%$search%' or erd.`balance_due`  like  '%$search%'   or `buyer_phone` like  '%$search%' or  event_reg_type_user like '%$search%'
                    or `event_registration_column_1` like  '%$search%' or event_registration_column_2 like  '%$search%' or   DATE_FORMAT(($last_updt_date), '%b %d, %Y')  like  '%$search%' 
                    or  DATE_FORMAT(`registration_date`, '%b %d, %Y')  like  '%$search%' or  erd.`payment_amount` like  '%$search%'  )";
         }else{
             $s_text='';
         }
        
        
        
        if($status == 'O'){ //ORDERS TAB
            $event_tab = "erd.`payment_status` in ('CC','RC','RP')";
        }else{
            $event_tab = "erd.`payment_status` in ('CP','RF')";
        }
        
        $sql_event_reg_id = sprintf("SELECT er.`event_reg_id` as id, er.`company_id`, erd.`event_id`, erd.`event_parent_id`, er.`student_id`, `buyer_name`, CONCAT(`event_registration_column_1`, ' ', `event_registration_column_2`) participant_name,
                IF(event_reg_type_user='P', 'Control Panel', IF(event_reg_type_user='M', 'Mobile App', IF(event_reg_type_user='U', 'Website URL', ''))) event_reg_type_user,
                `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(`registration_date`,%s) registration_date, `payment_type`, erd.`quantity`, erd.`payment_amount`, erd.`balance_due`, 
                (erd.`payment_amount` - erd.`balance_due`) paid_amount, erd.`payment_status`, IF(erd.`payment_status`='RF',concat('-',erd.`payment_amount`),0) refunded_amount, DATE_FORMAT(($last_updt_date),%s) last_updt_dt
                FROM `event_registration` er LEFT JOIN `event_reg_details` erd ON er.`event_reg_id` = erd.`event_reg_id` AND erd.`event_id`='%s' 
                WHERE er.`company_id`='%s' AND er.`event_id` IN (SELECT IFNULL(`event_parent_id`, `event_id`) FROM `event_reg_details` WHERE `event_id`='%s') AND erd.`event_id`='%s'  %s AND $event_tab ", $date_format,$date_format,
                mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$event_id),$s_text);
      
            
//            
//        if($status == 'O'){ //ORDERS TAB
//            $event_tab = "erd.`payment_status` in ('CC','RC','RP')";
//        }else{
//            $event_tab = "erd.`payment_status` in ('CP','RF')";
//        }
//         $sql_event_reg_id = sprintf("SELECT er.`event_reg_id` as id
//                FROM `event_registration` er LEFT JOIN `event_reg_details` erd ON er.`event_reg_id` = erd.`event_reg_id` AND erd.`event_id`='%s' 
//                WHERE er.`company_id`='%s' AND er.`event_id` IN (SELECT IFNULL(`event_parent_id`, `event_id`) FROM `event_reg_details` WHERE `event_id`='%s') AND erd.`event_id`='%s'  AND $event_tab ", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id));
            $result_event_r_id = mysqli_query($this->db, $sql_event_reg_id);
            if (!$result_event_r_id) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_event_reg_id");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num = mysqli_num_rows($result_event_r_id);
                if ($num > 0) {
                    while ($r = mysqli_fetch_assoc($result_event_r_id)) {
                        $event_reg_id[] = $r;
                    }
                }
            }
        }


        for($i=0; $i<count($event_reg_id); $i++){
            $output1[] = $event_reg_id[$i]['id'];
        }
        $reg_id_list = implode(",", $output1);
            
        $query = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id)
                WHERE `company_id`='%s' AND (`student_id` IN 
                (SELECT `student_id` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s))
                OR `student_email` IN 
                (SELECT `buyer_email` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                AND push_device_id IS NOT NULL AND trim(push_device_id)!='' order by app_id)t1 
                LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", 
               mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list),
               mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list), mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $query);
        
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $student_id = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                    if(!is_null($row['student_id']) && !empty($row['student_id'])){
                        $student_id[] = $row['student_id'];
                    }
                }
                $msg_id = $this->insertPushMessage($company_id,$student_id,$message);
                $update_message = sprintf("UPDATE `message` SET `push_delivered_date`=NOW() WHERE `message_id`='%s' and `company_id`='%s'", $msg_id, $company_id);
                $result_update_message = mysqli_query($this->db, $update_message);
                if (!$result_update_message) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                $query2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                        (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                        IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id)
                        WHERE `company_id`='%s' AND (`student_id` IN 
                        (SELECT `student_id` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s))
                        OR `student_email` IN 
                        (SELECT `buyer_email` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                        AND push_device_id IS NOT NULL AND trim(push_device_id)!='' order by app_id)t1 
                        LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                        where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", 
                       mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list),
                       mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list), mysqli_real_escape_string($this->db,$company_id));
                $result2 = mysqli_query($this->db, $query2);

                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $num_of_rows2 = mysqli_num_rows($result2);
                    if ($num_of_rows2 > 0) {
                        $output = [];
                        while ($row2 = mysqli_fetch_assoc($result2)) {
                            $output[] = $row2;
                        }
                    }
                }
                $r_rand = mt_rand(1,9999);
                $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();  //for creating pem file from db
                if (!file_exists($rfolder_name)) {
                    $oldmask = umask(0);
                    mkdir($rfolder_name, 0777, true);
                    umask($oldmask);
                }else{ // to make directory once.
                    $r = 0;
                    while ($r < 100) {
                        $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();
                        if (!file_exists($rfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($rfolder_name, 0777, true);
                            umask($oldmask);
                            break;
                        }
                     $r++;
                    }
                }
                $ios_socket_url ='';
                $ios_app_id_array = [];
                $host = $_SERVER['HTTP_HOST'];
                if (strpos($host, 'mystudio.academy') !== false) {
//                if (strpos($host, 'www.mystudio.academy') !== false || strpos($host, 'stage.mystudio.academy') !== false) {
                    //production
                    $ios_socket_url = "ssl://gateway.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];
                        
                        if ($devicetype == 'I') {                            
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['prod_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }else{
                    //development
                    $ios_socket_url = "ssl://gateway.sandbox.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];                        
                        
                        if ($devicetype == 'I') {
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['dev_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }
                $send_count = $this->sendpush($message, $ios, $android, $ios_socket_url);
                $update_message = sprintf("UPDATE `message` SET `no_of_users`='%s' WHERE `message_id`='%s' and `company_id`='%s'", $send_count, $msg_id, $company_id);
                $result_update_message = mysqli_query($this->db, $update_message);
                if (!$result_update_message) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                $check = $this->deleteDirectory("$rfolder_name");// delete directory with content
                $out = array('status' => "Success", 'msg' => "Push Message sent successfully. ");
                if($also_send_mail!='Y'){
                    $this->response($this->json($out), 200);
                }else{
                    $this->sendGroupPushMailForEvent($company_id, $subject, $message, $event_reg_id, $out['msg'], $attached_file, $file_name,$all_select_flag,$event_id,$status,$search);
                }
//                return $out;
            }else{
                $this->insertPushMessageForNoSelectedUsers($company_id, $message);
                $out = array('status' => "Failed", 'msg' => "Push Message sent successfully ");
                if($also_send_mail!='Y'){
                    $this->response($this->json($out), 200);
                }else{
                    $this->sendGroupPushMailForEvent($company_id, $subject, $message, $event_reg_id, $out['msg'], $attached_file, $file_name,$all_select_flag,$event_id,$status,$search);
                }
//                return $out;
            }
        }
    }
    
    public function insertPushMessageForNoSelectedUsers($company_id, $message_text){
        
        $sql1 = sprintf("INSERT INTO `message` (`message_text`, `company_id`, `message_to`, `push_from`) VALUES ('%s', '%s', 'A', 'EP')", mysqli_real_escape_string($this->db,$message_text), $company_id);
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
    }
    
    public function insertPushMessage($company_id, $student_id, $message_text){
        $message_id = '';
//        if(count($student_id)==1){
//            $message_to = 'I';
//            $stud_id = $student_id[0];
//        }else
        if(count($student_id)==0){
            $message_to = 'A';
            $stud_id = 'NULL';
        }else{
            $message_to = 'G';
            $stud_id = 'NULL';
        }
        
        $sql1 = sprintf("INSERT INTO `message` (`message_text`, `company_id`, `message_to`, `student_id`, `push_from`) VALUES ('%s', '%s', '%s', %s, 'EP')", mysqli_real_escape_string($this->db,$message_text), $company_id, $message_to, $stud_id);
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $message_id = mysqli_insert_id($this->db);
            if($message_id>0 && $message_to!='I'){
                $this->sendGroupMessage($company_id, $message_id, $student_id);
            }
        }
        return $message_id;
    }
    
    //send group message
    protected function sendGroupMessage($company_id, $message_id, $student_id){
        if(empty($student_id)){
            $sql = sprintf("SELECT student_id FROM `student` WHERE `company_id`='%s' AND `deleted_flag`!='Y'
                        AND student_id NOT IN (SELECT student_id FROM message_mapping WHERE message_id='%s')", 
                    mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$message_id));
            $result = mysqli_query($this->db, $sql);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $student_id=[];
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows > 0) {
                    while ($row = mysqli_fetch_assoc($result)) {
                        $student_id[] = $row['student_id'];
                    }
                }
            }
        }
        
        for($i=0; $i<count($student_id);$i++){
            $insert_query = sprintf("INSERT INTO `message_mapping`(`company_id`, `message_id`, `student_id`) VALUES('%s', '%s', '%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $message_id), mysqli_real_escape_string($this->db, $student_id[$i]));
            $insert_result = mysqli_query($this->db, $insert_query);
            if(!$insert_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function sendGroupPushMailForEvent($company_id, $subject, $message, $event_reg_id, $push_msg, $attached_file, $file_name,$all_select_flag,$event_id,$status,$search){
        if(empty($subject)){
            $subject = "Message";
        }
        $output1 = [];
        if($all_select_flag=='Y'){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
       
        $date_format = "'%b %d, %Y'";
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $last_updt_date = " DATE(CONVERT_TZ(erd.last_updt_dt,$tzadd_add,'$new_timezone'))";
        
        
        if(!empty($search)){
             $s_text=" and ( buyer_name like  '%$search%'  or buyer_email like  '%$search%' or erd.`balance_due`  like  '%$search%'   or `buyer_phone` like  '%$search%' or  event_reg_type_user like '%$search%'
                    or `event_registration_column_1` like  '%$search%' or event_registration_column_2 like  '%$search%' or   DATE_FORMAT(($last_updt_date), '%b %d, %Y')  like  '%$search%' 
                    or  DATE_FORMAT(`registration_date`, '%b %d, %Y')  like  '%$search%' or  erd.`payment_amount` like  '%$search%'  )";
         }else{
             $s_text='';
         }
        
        if($status == 'O'){ //ORDERS TAB
            $event_tab = "erd.`payment_status` in ('CC','RC','RP')";
        }else{
            $event_tab = "erd.`payment_status` in ('CP','RF')";
        }
        
        $sql_event_reg_id = sprintf("SELECT er.`event_reg_id` as id, er.`company_id`, erd.`event_id`, erd.`event_parent_id`, er.`student_id`, `buyer_name`, CONCAT(`event_registration_column_1`, ' ', `event_registration_column_2`) participant_name,
                IF(event_reg_type_user='P', 'Control Panel', IF(event_reg_type_user='M', 'Mobile App', IF(event_reg_type_user='U', 'Website URL', ''))) event_reg_type_user,
                `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(`registration_date`,%s) registration_date, `payment_type`, erd.`quantity`, erd.`payment_amount`, erd.`balance_due`, 
                (erd.`payment_amount` - erd.`balance_due`) paid_amount, erd.`payment_status`, IF(erd.`payment_status`='RF',concat('-',erd.`payment_amount`),0) refunded_amount, DATE_FORMAT(($last_updt_date),%s) last_updt_dt
                FROM `event_registration` er LEFT JOIN `event_reg_details` erd ON er.`event_reg_id` = erd.`event_reg_id` AND erd.`event_id`='%s' 
                WHERE er.`company_id`='%s' AND er.`event_id` IN (SELECT IFNULL(`event_parent_id`, `event_id`) FROM `event_reg_details` WHERE `event_id`='%s') AND erd.`event_id`='%s'  %s AND $event_tab ", $date_format,$date_format,
                mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_id), mysqli_real_escape_string($this->db,$event_id),$s_text);
        
//         $sql_event_reg_id = sprintf("SELECT er.`event_reg_id` as id
//                FROM `event_registration` er LEFT JOIN `event_reg_details` erd ON er.`event_reg_id` = erd.`event_reg_id` AND erd.`event_id`='%s' 
//                WHERE er.`company_id`='%s' AND er.`event_id` IN (SELECT IFNULL(`event_parent_id`, `event_id`) FROM `event_reg_details` WHERE `event_id`='%s') AND erd.`event_id`='%s'   AND $event_tab ", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id));
            $result_event_r_id = mysqli_query($this->db, $sql_event_reg_id);
            if (!$result_event_r_id) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_event_reg_id");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num = mysqli_num_rows($result_event_r_id);
                if ($num > 0) {
                    while ($r = mysqli_fetch_assoc($result_event_r_id)) {
                        $event_reg_id[] = $r;
                    }
                }
            }
        }
        for($i=0; $i<count($event_reg_id); $i++){
            $output1[] = $event_reg_id[$i]['id'];
        }
        $reg_id_list = implode(",", $output1);
        $countForBouncedEmail=$this->checkBouncedEmailCount($reg_id_list);
        $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, t1.* from
                (select s.`student_email`,s.`student_cc_email` 
                FROM  `student` s  LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
                WHERE `student_id` in (SELECT `student_id` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s)) and s.`deleted_flag`!='Y' and  s.`student_email` != IFNULL(b.`bounced_mail`, '')  GROUP BY student_email
                UNION 
                SELECT `buyer_email` as student_email,'' student_cc_email FROM `student` s 
                LEFT JOIN `event_registration` er ON er.`company_id` = s.`company_id`  and er.`student_id` = s.`student_id`
                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
                WHERE er.`company_id`='%s' and s.`deleted_flag`!='Y'  AND `event_reg_id` in (%s) and  buyer_email != IFNULL(b.`bounced_mail`, '')  GROUP BY buyer_email ) t1,company c WHERE c.`company_id`='%s'  group by student_email", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list), mysqli_real_escape_string($this->db,$company_id));
//        $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, s.`student_email`,s.`student_cc_email` 
//                FROM  `student` s LEFT JOIN  `company` c ON c.`company_id` = s.`company_id` 
//                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email`
//                WHERE s.`student_email` != IFNULL(b.`bounced_mail`, '') and  `student_id` in (SELECT `student_id` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s)) and s.`deleted_flag`!='Y' GROUP BY student_email", 
//                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list));
        $result = mysqli_query($this->db, $sql);
    
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
              $send_count=0;
              $failed_count = $countForBouncedEmail['failed_count'];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $cmp_name = $reply_to = '';
                $initial_check = 0;
                while ($row = mysqli_fetch_assoc($result)) {
                    if($initial_check==0){
                        $cmp_name = $row['company_name'];
                        $reply_to = $row['email_id'];
                        $initial_check++;
                    }
                    $output[] = $row;
                }
                $footer_detail = $this->getunsubscribefooter($company_id);
                for ($idx = 0; $idx < count($output); $idx++) {
                    $obj = (Array) $output[$idx];
                    $user_emailid = $obj["student_email"];
                    $cc_email_list = $obj['student_cc_email'];
                    //echo "******count******".$idx;
                    $sendEmail_status = $this->sendEmail($user_emailid, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file,$cc_email_list,$company_id,'',$footer_detail);
                    if($sendEmail_status['status']=="true"){
                         $send_count++;
                        $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200);
                    }else{
                        $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                        $failed_count++;
                        log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200); // If no records "No Content" status
                    }
                }
                 if($failed_count== 0){
                    $push_msg="Email(s) Sent Successfully.";
                }
                elseif($send_count==0){
                    $push_msg="Email(s) failed due to invalid address.";
                }else{
//                    $push_msg=$countForBouncedEmail['Total_count']-$send_count. ' of ' .$countForBouncedEmail['Total_count'].' email sending failed. Others sent successfully';
                  
                    $push_msg=$send_count ." sent. ".$failed_count." failed due to invalid address.";
                   // $push_msg=$send_count ." sent. ".$failed_count." failed due to invalid address." ;
                   
                }
//                if(empty($push_msg)){
//                    $push_msg="Email Sent Successfully";
//                }
                $msg = array('status' => "Success", "msg" => $push_msg);
                $this->response($this->json($msg), 200);
            } else {
              if($send_count==0){
                    $push_msg="Email(s) failed due to invalid address.";
                }
                $error = array('status' => "Failed", "msg" => $push_msg);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }

    public function getEventHistoryModal($company_id, $event_reg_id, $call_back) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $activity_dt_tm = " IF(TIME(`activity_date_time`)!='00:00:00', DATE(CONVERT_TZ(`activity_date_time`,$tzadd_add,'$new_timezone')), DATE(`activity_date_time`))";
        date_default_timezone_set($curr_time_zone);
        $output=[];
        $selectsql = sprintf("SELECT `activity_text`,`event_history_id`,`activity_type`, $activity_dt_tm as activity_date FROM `event_history` WHERE `company_id`='%s' and `event_reg_id`='%s' AND `deleted_flag`!='D' AND `activity_type`!='buyer name edit' ORDER BY `activity_date_time`, `event_history_id`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id));
        $resultselectsql = mysqli_query($this->db, $selectsql);
        if (!$resultselectsql) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($resultselectsql);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($resultselectsql)) {
                    $event_history[] = $row;
                }
               
                $out = array('status' => "Success", 'msg' => $event_history);
                if ($call_back == 0) {
                    $this->response($this->json($out), 200);
                } else {
                    return $out;
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Event history details doesn't exist.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200); // If no records "No Content" status  
                } else {
                    return $error;
                }
            }
        }
    }
    
    public function addorupdateEventHistoryNote($company_id,$event_reg_id,$student_id,$status,$history_id,$note_text){
         $curr_date = gmdate('Y-m-d H:i:s');
        if($status=='add'){
            $query = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, 'note'), 
                    mysqli_real_escape_string($this->db, $note_text), mysqli_real_escape_string($this->db, $curr_date));
            $res_msg = "Note added successfully.";
        }else{
            $sql = sprintf("SELECT * FROM `event_history` WHERE `event_history_id`='%s' AND `company_id`='%s' AND `event_reg_id`='%s'",
                    mysqli_real_escape_string($this->db, $history_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id));
            $res = mysqli_query($this->db, $sql);
            if (!$res) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.","query" => "$sql");
                $this->response($this->json($error), 200);
            }else{
                $num_rows = mysqli_num_rows($res);
                if($num_rows==0){
                    $error = array('status' => "Failed", "msg" => "History note details not available.");
                    $this->response($this->json($error), 200);
                }
            }
            if($status=='update'){
                $query = sprintf("UPDATE `event_history` SET `activity_text`='%s', `activity_date_time`='%s' WHERE `event_history_id`='%s' AND `company_id`='%s' AND `event_reg_id`='%s'",
                        mysqli_real_escape_string($this->db, $note_text), mysqli_real_escape_string($this->db, $curr_date),
                        mysqli_real_escape_string($this->db, $history_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id));
                $res_msg = "Note updated successfully.";
            }elseif($status=='delete'){
                $query = sprintf("UPDATE `event_history` SET `deleted_flag`='D' WHERE `event_history_id`='%s' AND `company_id`='%s' AND `event_reg_id`='%s'",
                        mysqli_real_escape_string($this->db, $history_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id));
                $res_msg = "Note deleted successfully.";
            }
        }
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $event_history = $this->getEventHistoryModal($company_id, $event_reg_id,1);
            $log = array("status" => "Success", "msg" => $res_msg, "event_history" => $event_history);
            $this->response($this->json($log),200);
        }
    }
    
    public function geteventParticipantinfo($company_id,$event_reg_id,$call_index) {
        $output = []; 
        $sql_reg = sprintf("SELECT e.`event_registration_column_1` heading_1, e.`event_reg_col_1_mandatory_flag` heading_1_mf, e.`event_registration_column_2` heading_2, e.`event_reg_col_2_mandatory_flag` heading_2_mf, e.`event_registration_column_3` heading_3, e.`event_reg_col_3_mandatory_flag` heading_3_mf, e.`event_registration_column_4` heading_4, e.`event_reg_col_4_mandatory_flag` heading_4_mf, 
                e.`event_registration_column_5` heading_5, e.`event_reg_col_5_mandatory_flag` heading_5_mf, e.`event_registration_column_6` heading_6, e.`event_reg_col_6_mandatory_flag` heading_6_mf,
                e.`event_registration_column_7` heading_7, e.`event_reg_col_7_mandatory_flag` heading_7_mf, e.`event_registration_column_8` heading_8, e.`event_reg_col_8_mandatory_flag` heading_8_mf, 
                e.`event_registration_column_9` heading_9, e.`event_reg_col_9_mandatory_flag` heading_9_mf, e.`event_registration_column_10` heading_10, e.`event_reg_col_10_mandatory_flag` heading_10_mf,
                er.`event_registration_column_1`, er.`event_registration_column_2`, er.`event_registration_column_3`,
                er.`event_registration_column_4`, er.`event_registration_column_5`, er.`event_registration_column_6`, er.`event_registration_column_7`,
                er.`event_registration_column_8`, er.`event_registration_column_9`, er.`event_registration_column_10`,
                concat(er.`buyer_first_name`,' ',`buyer_last_name`) buyer_name, er.`buyer_phone`, er.`buyer_email`,er.`buyer_first_name`,er.`buyer_last_name` 
                FROM `event` e LEFT JOIN `event_registration` er ON e.`event_id` = er.`event_id` AND e.`company_id` = er.`company_id`   WHERE er.company_id='%s' AND
                er.`event_reg_id`= '%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id));
        $result_reg_details = mysqli_query($this->db, $sql_reg);
        if (!$result_reg_details) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_reg");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_reg_details);
            if ($num_rows > 0) {
                $output = mysqli_fetch_assoc($result_reg_details);
                $out = array('status' => "Success", 'msg' => $output);
                if($call_index==0){
                    $this->response($this->json($out), 200);
                }elseif($call_index==1){
                    return $out;
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Participant details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status  
            }
        }
    }
    
    public function updateParticipantDetails($company_id,$event_reg_id,$pfirst_name,$plast_name,$buyer_first_name,$buyer_last_name,$buyer_email,$buyer_phone,$event_field_3,$event_field_4,$event_field_5,$event_field_6,$event_field_7,$event_field_8,$event_field_9,$event_field_10) {
    $activity_type = $activity_text = "";
    $curr_date = gmdate('Y-m-d H:i:s');
        $check_buyer = sprintf("SELECT * FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id`='%s'",
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_reg_id));
        $result_buyer = mysqli_query($this->db, $check_buyer);
        if (!$result_buyer) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_buyer");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $nums_rows = mysqli_num_rows($result_buyer);
            if ($nums_rows > 0) {
                $old_reg = mysqli_fetch_object($result_buyer);
                $old_buyer_first_name = $old_reg->buyer_first_name;
                $old_buyer_last_name = $old_reg->buyer_last_name;
                $student_id = $old_reg->student_id;
                if ($old_buyer_first_name !== $buyer_first_name && $old_buyer_last_name !== $buyer_last_name) {
                    $activity_type = "buyer name edit";
                    $activity_text = "buyer name is changed from $old_buyer_first_name $old_buyer_last_name to $buyer_first_name $buyer_last_name.";
                } elseif ($old_buyer_first_name !== $buyer_first_name) {
                    $activity_type = "buyer name edit";
                    $activity_text = "buyer first name is changed from $old_buyer_first_name to $buyer_first_name";
                } elseif ($old_buyer_last_name !== $buyer_last_name) {
                    $activity_type = "buyer name edit";
                    $activity_text = "buyer last name is changed from $old_buyer_last_name to $buyer_last_name";
                }
            } else {
                $error = array('status' => "Failed", "msg" => "No Participant Found.");
                $this->response($this->json($error), 200);
            }
        }

        $sql = sprintf("UPDATE `event_registration` SET `buyer_first_name`='%s',`buyer_last_name`='%s',`buyer_email`='%s',`buyer_phone`='%s',`event_registration_column_1`='%s', `event_registration_column_2`='%s',`event_registration_column_3`='%s',
              `event_registration_column_4`='%s',`event_registration_column_5`='%s',`event_registration_column_6`='%s',`event_registration_column_7`='%s',`event_registration_column_8`='%s',`event_registration_column_9`='%s',`event_registration_column_10`='%s' WHERE `company_id`='%s' AND `event_reg_id`='%s'", 
                mysqli_real_escape_string($this->db,$buyer_first_name), mysqli_real_escape_string($this->db,$buyer_last_name),mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$buyer_phone), mysqli_real_escape_string($this->db,$pfirst_name), mysqli_real_escape_string($this->db,$plast_name), mysqli_real_escape_string($this->db,$event_field_3),mysqli_real_escape_string($this->db,$event_field_4),mysqli_real_escape_string($this->db,$event_field_5),
                mysqli_real_escape_string($this->db,$event_field_6),mysqli_real_escape_string($this->db,$event_field_7),mysqli_real_escape_string($this->db,$event_field_8),mysqli_real_escape_string($this->db,$event_field_9),mysqli_real_escape_string($this->db,$event_field_10), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_reg_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if(!empty($activity_type)){
                $insert_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $student_id, $activity_type, $activity_text, $curr_date);
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
           $participant_details= $this->geteventParticipantinfo($company_id, $event_reg_id,1);
           $msg = array('status' => "Success", "msg" => "Participant details updated successfully.", "participant_details" => $participant_details);
           $this->response($this->json($msg), 200);
        }
    }
    
     public function getpaymentHistoryDetails($company_id, $reg_id, $call_back) {
        
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $payment_date = " IF(TIME(ep.schedule_date)!='00:00:00', DATE(CONVERT_TZ(ep.schedule_date,$tzadd_add,'$new_timezone')), DATE(ep.schedule_date)) ";
        $created_date = " IF(TIME(ep.created_dt)!='00:00:00', DATE(CONVERT_TZ(ep.created_dt,$tzadd_add,'$new_timezone')), DATE(ep.created_dt)) ";
        $activity_dt_tm = " IF(TIME(`activity_date_time`)!='00:00:00', DATE(CONVERT_TZ(`activity_date_time`,$tzadd_add,'$new_timezone')), DATE(`activity_date_time`))";
        date_default_timezone_set($curr_time_zone);
        $output=[];
        $selectsql = sprintf("SELECT *, 'events' category FROM (
                SELECT ep.check_number,'payment' as type, '' history_id, ep.`event_payment_id` payment_id, ep.`event_reg_id` reg_id, ep.credit_method, ep.checkout_status, IF(er.`processing_fee_type`=2,IF(er.`payment_type`='RE'&&(ep.`schedule_status`in('N','F')), ep.`payment_amount`, ep.`payment_amount`+if(ep.`schedule_status`='PR'||ep.`schedule_status`='CR',(SELECT SUM(`processing_fee`) FROM `event_payment` WHERE `event_reg_id`=ep.`event_reg_id` AND `checkout_id`=ep.`checkout_id` AND `schedule_status` IN ('FR','PR')),ep.`processing_fee`)),ep.`payment_amount`) payment_amount,er.payment_type,er.`processing_fee_type`,er.`event_id` reg_event_id,
                IF(ep.`schedule_status`='N', IF(ep.`schedule_date`>=CURDATE(),'upcoming','past'), '') current_status,
                ep.`payment_amount` payment_amount_without_pf, if(ep.`schedule_status`='PR'||ep.`schedule_status`='CR',(SELECT SUM(`processing_fee`) FROM `event_payment` WHERE `event_reg_id`=ep.`event_reg_id` AND `checkout_id`=ep.`checkout_id` AND `schedule_status` IN ('FR','PR')),ep.`processing_fee`) processing_fee,ifnull(DATE($payment_date),$created_date) as payment_date,ep.`schedule_status`,ep.`refunded_amount`,ep.`refunded_date`,if(ep.`payment_from`='S',ep.`stripe_card_name`,ep.`cc_name`) as cc_name,DATE(ep.`last_updt_dt`) as last_updt_dt, er.deposit_amount,'' activity_text
                FROM `event_payment` ep LEFT JOIN `event_registration` er ON ep.`event_reg_id` = er.`event_reg_id` 
                WHERE er.company_id='%s' AND er.event_reg_id= '%s' AND ep.`schedule_status` NOT IN ('C','R','MR') 
                UNION
                SELECT '' check_number,'history' as type ,`event_payment_history_id` history_id,`event_payment_id` payment_id,`event_reg_id` reg_id,'' credit_method,'' checkout_status,'' payment_amount,''payment_type,''processing_fee_type,'' reg_event_id,'' current_status,''payment_amount_without_pf,'' processing_fee, $activity_dt_tm payment_date,'' schedule_status,
                ''refunded_amount,''refunded_date,'' cc_name,DATE(`last_updt_date`) as last_updt_dt,'' deposit_amount ,activity_text  from event_payment_history where company_id ='%s' and event_reg_id='%s' and trim(activity_type) not in ('Manual Credit Applied','Check Credit Applied','Cash Credit Applied')) t
                order by t.`payment_id`, payment_date", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id));
        $result = mysqli_query($this->db, $selectsql);
//        log_info("history  ".$selectsql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $nums_rows = mysqli_num_rows($result);
            if ($nums_rows > 0) {
                $i =1;
                $down_check=0;
                $event_id='';
                while ($row = mysqli_fetch_assoc($result)) {
                    if(!empty($row['reg_event_id'])){
                        $event_id=$row['reg_event_id'];
                    }
                    if($row['type']=='payment'){
                        if ($row['payment_type'] == 'RE' && ($row['payment_amount_without_pf'] == $row['deposit_amount']) && $down_check==0) {
                            $row['payment_count'] = "Down payment";
                            $output[]= $row;
                            $down_check=1;
                            $i--;
                        } else if ($row['payment_type'] == 'RE' && ($row['schedule_status'] == 'N' || $row['schedule_status'] == 'F')) {
                            $row['payment_count'] = "Payment $i";
                            $output[]= $row;
                        } else if ($row['payment_type'] == 'RE' && ($row['schedule_status'] == 'S' || $row['schedule_status'] == 'PR' || $row['schedule_status'] == 'M' || $row['schedule_status'] == 'MP') ) {
                            $row['payment_count'] = "Payment $i";
                            $output[]= $row;
                        } else if ($row['payment_type'] == 'RE' && $row['schedule_status'] == 'R') {
                            $row['payment_count'] = "Refunded payment";
                            $output[]= $row;
                        } else if ($row['payment_type'] == 'CO') {
                            $row['payment_count'] = 'Payment';
                            $output[]= $row;
                        }
                       
                    }else if($row['type']=='history'){
                        $row['payment_count'] = 'Manual';
                          $output[]= $row;
                          $i--;
                    }
                     $i++;
//                    $output[]= $row;
                }
                $output2['upcoming'] =$output2['history'] = [];
                for ($i = 0; $i < count($output); $i++) {
                    if ($output[$i]['payment_type'] == 'RE' && ($output[$i]['schedule_status']=='N'  || $output[$i]['schedule_status']=='F')) {
                        $output2['upcoming'][] = $output[$i];
                    }else{
                         $output2['history'][] = $output[$i];
                    }
                }
                $reg_details = $this->getEventParticipantDetailsForRefund($company_id, $event_id, $reg_id, 1);
                $out = array('status' => "Success", 'msg' => $output2, "reg_details" => $reg_details);
                if ($call_back == 0) {
                    $this->response($this->json($out), 200);
                } else {
                    return $out;
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Event payment history doesn't exist.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200); // If no records "No Content" status  
                } else {
                    return $error;
                }
            }
        }
    }
    
    public function sendEventPaymentHistoryDetails($company_id, $event_reg_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $payment_date = " IF(TIME(ep.schedule_date)!='00:00:00', DATE(CONVERT_TZ(ep.schedule_date,$tzadd_add,'$new_timezone')), DATE(ep.schedule_date)) ";
        $created_date = " IF(TIME(ep.created_dt)!='00:00:00', DATE(CONVERT_TZ(ep.created_dt,$tzadd_add,'$new_timezone')), DATE(ep.created_dt)) ";
        date_default_timezone_set($curr_time_zone);
        $currentDateTime = date('Y-m-d');
        $waiver_policies = $paid_str='';
        $query = sprintf("SELECT er.`company_id`, er.`event_id`, `event_title`,if(ep.`payment_from`='S',ep.`stripe_card_name`, ep.`cc_name`) as cc_name , `waiver_policies`, concat(er.event_registration_column_2, ',', er.event_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `payment_type`, 
            er.`payment_amount` total_due, er.`processing_fee_type`, er.`processing_fee` total_processing_fee, `paid_amount`, er.payment_method, ep.credit_method, ep.check_number, ep.`payment_amount`, ep.`processing_fee`, ep.`refunded_amount`, `schedule_date`, `schedule_status`, DATE($created_date) created_dt
            FROM `event_registration` er LEFT JOIN `event_payment` ep ON er.`event_reg_id` = ep.`event_reg_id` AND ep.`schedule_status` IN ('N', 'S', 'PR', 'M', 'MP')
            LEFT JOIN `event` e ON e.`event_id` = er.`event_id` AND e.`company_id` =  er.`company_id`
            WHERE er.`event_reg_id` = '%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            if ($return_value == 1) {
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
            exit();
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {

                $event_name = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $student_cc_email_list = $wp_currency_code = $wp_currency_symbol = $processing_fee_type = '';
                $inital_stage = $initial_down_pay = 0;
                $current_plan_details = $event_details = $bill_due_details = $payment_details = [];
                $total_due = $paid_due = $rem_due = 0;
                while ($row = mysqli_fetch_assoc($result)) {
                    $paid_amt = 0;
                    $company_id = $row['company_id'];
                    $event_id = $row['event_id'];
                    $event_name = $row['event_title'];
                    $participant_name = $row['participant_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $payment_method = $row['payment_method'];
                    $temp = [];

                    if ($row['processing_fee_type'] == 2) {
                        $temp['amount'] = $row['payment_amount'] + $row['processing_fee'];
                    } else {
                        $temp['amount'] = $row['payment_amount'];
                    }
//                    $temp['amount'] = $row['payment_amount'];
                    $temp['date'] = $row['schedule_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['edit_status'] = 'N';
                    $temp['schedule_status'] = $row['schedule_status'];
                    $temp['processing_fee'] = $row['processing_fee'];
                    $temp['cc_name'] = $row['cc_name'];
                    $temp['credit_method'] = $row['credit_method'];
                    $temp['check_number'] = $row['check_number'];
                    if ($row['schedule_status'] == 'N') {
                        $temp['status'] = 'N';
                    } elseif ($row['schedule_status'] == 'S') {
                        $temp['status'] = 'S';
                        $paid_amt = $temp['amount'];
                        $inital_stage++;
                    } elseif ($row['schedule_status'] == 'PR' || $row['schedule_status'] == 'M' || $row['schedule_status'] == 'MP') {
                        $paid_amt = $temp['amount'] - $temp['refunded_amount'];
                    }
//                    $total_due = $current_plan_details['total_due'] = $row['total_due'];
//                    $paid_due = $current_plan_details['paid_due'] = $row['paid_amount'];
                    $total_due += $temp['amount'];
                    $current_plan_details['total_due'] = $total_due;
                    $paid_due += $paid_amt;
                    $current_plan_details['paid_due'] = $paid_due;

                    $current_plan_details['success_count'] = $inital_stage;
                    $rem_due = $current_plan_details['rem_due'] = number_format((float) ($total_due - $paid_due), 2, '.', '');
//                    $payment_details[] = $temp;
                    if ($row['schedule_status'] == 'N') {
                        $bill_due_details[] = $temp;
                    } elseif ($row['schedule_status'] == 'S' || $row['schedule_status'] == 'PR') {
                        $payment_details[] = $temp;
                        if ($initial_down_pay > 0) {
                            $bill_due_details[] = $temp;
                        }
                        $initial_down_pay++;
                    } elseif ($row['schedule_status'] == 'M' || $row['schedule_status'] == 'MP') {
                        $payment_details[] = $temp;
                    }
                }
            }
        }


        $query2 = sprintf("SELECT c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`, e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, `event_end_dt`, erd.`event_cost`, `discount`, erd.`payment_amount`, `quantity`, erd.`payment_status`, `balance_due`,s.`student_cc_email` 
                    FROM `event` e LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` LEFT JOIN `event_registration` er ON er.`event_reg_id`= erd.`event_reg_id`
                    LEFT JOIN `company` c ON e.`company_id` =  c.`company_id` LEFT JOIN `student` s ON s.`company_id`= er.`company_id` AND s.`student_id`= er.`student_id`
                    WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND erd.`event_reg_id`='%s' AND erd.`payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
        $result2 = mysqli_query($this->db, $query2);
        $error_log = array('status' => "check", "msg" => "Internal Server Error.", "query" => "$query2");
        log_info($this->json($error_log));
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
            log_info($this->json($error_log));
            if ($return_value == 1) {
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
            exit();
        } else {
            $temp_quantity = $temp_char_count = 0;
            while ($row2 = mysqli_fetch_assoc($result2)) {
                $temp2 = [];
                $temp2['title'] = $row2['event_title'];
                if ($temp2['title'] > $temp_char_count) {
                    $temp_char_count = strlen($temp2['title']);
                }

                if (!empty(trim($row2['event_begin_dt']))) {
                    if ("0000-00-00 00:00:00" == $row2['event_begin_dt']) {
                        $start = " - ";
                    } else {
                        $start = $row2['event_begin_dt'];

                        $split_start = explode(" ", $start);
                        $start_date_value = $split_start[0];
                        $start_time_value = $split_start[1];
                        if ("00:00:00" == $start_time_value) {
                            $start = date('M d, Y', strtotime($start));
                        } else {
                            $start = date('M d, Y @ h:i A', strtotime($start));
                        }
                    }
                }

                if (!empty(trim($row2['event_end_dt']))) {
                    if ("0000-00-00 00:00:00" == $row2['event_end_dt']) {
                        $end = " - ";
                    } else {
                        $end = $row2['event_end_dt'];

                        $split_end = explode(" ", $end);
                        $end_date_value = $split_end[0];
                        $end_time_value = $split_end[1];
                        if ("00:00:00" == $end_time_value) {
                            $end = date('M d, Y', strtotime($end));
                        } else {
                            $end = date('M d, Y @ h:i A', strtotime($end));
                        }
                    }
                }

                $temp2['start'] = $start;
                $temp2['end'] = $end;
                $temp2['event_cost'] = $row2['event_cost'];
                $temp2['quantity'] = $row2['quantity'];
                $temp2['event_disc'] = $row2['discount'];
                $temp2['discounted_price'] = $row2['event_cost'] - $row2['discount'];
                $temp_quantity += $row2['quantity'];
                $temp2['payment_amount'] = $row2['payment_amount'];
                $temp2['paid_due'] = $temp2['payment_amount'] - $row2['balance_due'];
                $temp2['rem_due'] = $row2['balance_due'];
                $temp2['state'] = 'N';
                $event_details[] = $temp2;
                $studio_name = $row2['company_name'];
                $studio_mail = $row2['email_id'];
                $cc_email_list = $row2['referral_email_list'];
                $wp_currency_code = $row2['wp_currency_code'];
                $wp_currency_symbol = $row2['wp_currency_symbol'];
                $student_cc_email_list = $row2['student_cc_email'];
            }
            $current_plan_details['total_event'] = $temp_quantity;
        }
        $subject = $event_name . " Event Payment History";
        $message .= "<b>Event Details</b><br><br>";
        for ($i = 0; $i < count($event_details); $i++) {
            $message .= "Event: " . $event_details[$i]['title'] . "<br>Starts: " . $event_details[$i]['start'] . "<br>Ends: " . $event_details[$i]['end'] . "<br>Participant: " . $participant_name . "<br><br>";
        }

        if ($total_due > 0) {
            $message .= "<b>Billing Details</b><br><br>";
            if (count($bill_due_details) > 0) {
                $message .= "Total due: $wp_currency_symbol" . $total_due . "<br>";
                $message .= "Balance due: $wp_currency_symbol" . $rem_due . "<br>";
                for ($j = 0; $j < count($bill_due_details); $j++) {
                    $x = $j + 1;
                    $amount = $bill_due_details[$j]['amount'];
                    $refunded_amount = $bill_due_details[$j]['refunded_amount'];
                    $final_amt = $amount - $refunded_amount;
                    $schedule_status = $bill_due_details[$j]['schedule_status'];
                    $date = date("M dS, Y", strtotime($bill_due_details[$j]['date']));
                    $recurring_processing_fee = $bill_due_details[$j]['processing_fee'];
                    if ($schedule_status == 'N') {
                        $paid_str = "due";
                    }
                    $recur_pf_str = "";
                    if ($processing_fee_type == 2) {
                        $recur_pf_str = " (plus $wp_currency_symbol" . $recurring_processing_fee . " administrative fees)";
                    }
                    $message .= "Bill " . $x . ": $wp_currency_symbol" . $final_amt . " " . $paid_str . " " . $date . " " . $recur_pf_str . "  <br>";
                }

                if (count($payment_details) > 0) {

                    $initial_check = 0;
                    $payment_history = [];
                    $schedule_status = $credit_method = $check_number = $type = $processing_fees = $processing_fee_type = $refunded_amount = $wp_currency_symbol = $event_title = $payment_dt = $payment_amount = $cc_name = $studio_name = $buyer_email = $studio_mail = '';
                    $selectsql = sprintf("SELECT 'payment' as type, '' history_id, ep.`event_payment_id` payment_id,e.event_title as title,c.wp_currency_symbol ,c.`company_name`, c.`email_id`, ep.`event_reg_id` reg_id,er.`processing_fee_type`,er.buyer_email, IF(er.`processing_fee_type`=1, ep.`payment_amount`, ep.`payment_amount`+ep.`processing_fee`) payment_amount,er.payment_type,
                     ep.`processing_fee`,ifnull($payment_date,$created_date) as payment_date,ep.`schedule_status`,ep.credit_method, ep.check_number,ep.`refunded_amount`,ep.`refunded_date`,if(ep.`payment_from`='S',ep.`stripe_card_name`,ep.`cc_name`) as cc_name,DATE(ep.`last_updt_dt`) as last_updt_dt
                     ,er.deposit_amount FROM `event_payment` ep LEFT JOIN `event_registration` er  on ep.`event_reg_id` = er.`event_reg_id`  LEFT JOIN event e on  e.event_id=er.event_id  left join company c on c.company_id=er.company_id WHERE er.company_id='%s' AND er.event_reg_id= '%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id));
                    $result = mysqli_query($this->db, $selectsql);
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $nums_rows = mysqli_num_rows($result);
                        if ($nums_rows > 0) {
                            $i = 0;
                            while ($row = mysqli_fetch_assoc($result)) {
                                $payment_history[] = $row;
                                $schedule_status = $row['schedule_status'];
                                $type = $row['type'];
                                $processing_fees = $row['processing_fee'];
                                $processing_fee_type = $row['processing_fee_type'];
                                $refunded_amount = $row['refunded_amount'];
                                $wp_currency_symbol = $row['wp_currency_symbol'];
                                $event_title = $row['title'];
                                $payment_dt = $row['payment_date'];
                                $payment_amount = $row['payment_amount'];
                                $cc_name = $row['cc_name'];
                                $studio_name = $row['company_name'];
                                $buyer_email = $row['buyer_email'];
                                $studio_mail = $row['email_id'];
                                $credit_method = $row['credit_method'];
                                $check_number = $row['check_number'];

                                if ($row['payment_type'] == 'RE') {
                                    $payment_count = "Payment $i";
                                    $i++;
                                } else {
                                    $payment_count = 'Payment';
                                }
                                if ($initial_check == 0) {
//                                    $subject = $event_title . " Event Payment History";
                                    $message .= "<br>";
                                    $message .= "<b>Payment Details </b><br><br>";
                                }
                                if ($type == 'history') {
                                    $ms_str = '';
                                    if($credit_method=='CA'){
                                        $ms_str = " (Cash)";
                                    }elseif($credit_method=='CH'){
                                        $ms_str = " (Check)";
                                    }
                                    $message .= date("M dS, Y", strtotime($payment_dt)).$ms_str;
                                    $message .= "<br>";
                                } else {
                                    if ($row['payment_type'] == 'RE') {
                                        if ($payment_count == 'Payment 0') {
                                            $message .= "down Payment : ";
                                        } else {
                                            $message .= "$payment_count : ";
                                        }
                                    } else {
                                        $message .= "$payment_count : ";
                                    }

                                    $message .= "$wp_currency_symbol";

                                    if ($schedule_status == 'R') {
                                        if ($processing_fee_type == '2') {
                                            $message .= $refunded_amount + $processing_fees;
                                        } else {
                                            $message .= "$refunded_amount ";
                                        }
                                    } else {
                                        if ($processing_fee_type == '2') {
                                            $message .= $payment_amount;
                                        } else {
                                            $message .= "$payment_amount ";
                                        }
                                    }

                                    if ($processing_fee_type == '2') {
                                        $message .= " (includes $wp_currency_symbol$processing_fees administrative fees) ";
                                    }
                                    if (isset($cc_name)) {
                                        $message .= "($cc_name) ";
                                    }
                                    if ($schedule_status == 'S') {
                                        $message .= "paid on ";
                                    } else if ($schedule_status == 'F') {
                                        $message .= "past due on ";
                                    } else if ($schedule_status == 'R') {
                                        $message .= "refunded on ";
                                    } else if ($schedule_status == 'M') {
                                        $message .= " Mannual Credit Applied: ";
                                    } else if ($schedule_status == 'PR') {
                                        $message .= " Partial Refunded: ";
                                    } else if ($schedule_status == 'N') {
                                        if($currentDateTime<$payment_dt){
                                             $message .= "  schedule for: ";
                                        }else{
                                             $message .= "  past due ";
                                        }
                                    } else if ($schedule_status == 'CR') {
                                        $message .= " Complete Refunded: ";
                                    } else if ($schedule_status == 'C') {
                                        $message .= "Cancelled: ";
                                    }

                                    $message .= date("M dS, Y", strtotime($payment_dt));
                                    $message .= "<br>";
                                }
                                $initial_check++;
                            }
                        } else {
                            $error = array('status' => "Failed", "msg" => "event details doesn't exist.");
                            $this->response($this->json($error), 200); // If no records "No Content" status  
                        }
                    }
                }
            } else {
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $schedule_status = $payment_details[0]['schedule_status'];
                $final_amount = $amount - $refunded_amount;
                $pr_fee2 = $payment_details[0]['processing_fee'];
                $pf_str2 = $credit_name = "";
                if($payment_details[0]['credit_method']=='CC'){
                    if ($processing_fee_type == 2) {
                        $pf_str2 = " (includes $wp_currency_symbol" . $pr_fee2 . " administrative fees)";
                    }
                    $credit_name = "(" . $payment_details[0]['cc_name'] . ")";
                }elseif($payment_details[0]['credit_method']=='CA'){
                    $credit_name = "(Cash)";
                }elseif($payment_details[0]['credit_method']=='CH'){
                    $credit_name = "(Check)";
                }
                $message .= "Total Due: $wp_currency_symbol" . $final_amount . "<br>Amount Paid: $wp_currency_symbol" . $final_amount . $pf_str2 . $credit_name."<br>";
            }
            $message .= "<br><br>";
        }

        $waiver_present = 0;
        $file = '';
        if (!empty(trim($waiver_policies))) {
            $waiver_present = 1;
            $file_name_initialize = 10;
            for ($z = 0; $z < $file_name_initialize; $z++) {
                $dt = time();
                $file = "../../../uploads/" . $dt . "_waiver_policies.html";
                if (file_exists($file)) {
                    sleep(1);
                    $file_name_initialize++;
                } else {
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }

        $sendEmail_status = $this->sendEmailForEvent('', $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, '',$student_cc_email_list);
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $success = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            $msg = array('status' => "Success", "msg" => "Email sent successfully.");
            $this->response($this->json($msg), 200);
            log_info($this->json($success));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }

        date_default_timezone_set($curr_time_zone);
    }

    public function editeventPaymentDetailsFromHistory($company_id,$reg_id,$payment_id,$payment_date){
        if(!$this->validateDate($payment_date)){
            $error = array("status" => "Failed", "msg" => "Payment date is not a valid date.");
            $this->response($this->json($error), 200);
        }
        $wp_currency_symbol = $payment_amount_in_db = $payment_date_in_db = '';
        $query = sprintf("SELECT *, (SELECT `wp_currency_symbol` FROM `company` WHERE `company_id`='%s') wp_currency_symbol FROM `event_payment`   WHERE  `event_reg_id`='%s' AND `event_payment_id`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $payment_id));
        $result = mysqli_query($this->db, $query);
        log_info($query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $wp_currency_symbol = $row['wp_currency_symbol'];
                $payment_amount_in_db = $row['payment_amount'];
                $payment_date_in_db = $row['schedule_date'];
            }else{
                $error = array('status' => "Failed", "msg" => "Event payment details mismatched.");
                $this->response($this->json($error), 200);
            }
        }
        
        $sql = sprintf("UPDATE `event_payment` SET `schedule_date`='%s' WHERE  `event_reg_id`='%s' AND `event_payment_id`='%s'", 
                mysqli_real_escape_string($this->db, $payment_date), mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $payment_id));
        $res = mysqli_query($this->db, $sql);
        log_info($sql);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $curr_date = gmdate("Y-m-d H:i:s");
//            if($payment_amount!=$payment_amount_in_db){
//                $text = "Payment amount changed to $wp_currency_symbol".$payment_amount;            
//                $insert_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s')",
//                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), 'classes payment edit', $text, $curr_date);
//                $result_history = mysqli_query($this->db, $insert_history);
//                if (!$result_history) {
//                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
//                    log_info($this->json($error_log));
//                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                    $this->response($this->json($error), 200);
//                }
//            }
            if($payment_date!=$payment_date_in_db){
                $text = "Payment date changed to ". date("F j, Y", strtotime($payment_date));            
                $insert_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), 'classes payment edit', $text, $curr_date);
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
            
            $payment_history = $this->getpaymentHistoryDetails($company_id, $reg_id, 1);
            $msg = array('status' => "Success", "msg" => "event payment details updated successfully.", "payment"=>$payment_history);
            $this->response($this->json($msg), 200);
        }
    }
    
    private function getRealIpAddr(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet        
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    private function sendSnsforfailedcheckout($sub,$msg){

            $topicarn = 'arn:aws:sns:us-east-1:241334827363:mystudio';
         
                    $subject = $sub;
    //                $subject = $sns_sub . " -transaction failed";
                    $message = $msg;

                    $credentials = new Aws\Credentials\Credentials('AKIAIGUWYNJGAHDPJGIA', 'w4oKbP0UhJZr1W0hGuULhkdEoxEHY2YxEq/6uycT');               
                    $sns = SnsClient::factory([
                        'credentials' => $credentials,
                        'region' => 'us-east-1',
                        'version' => 'latest'
                    ]);
    //                log_info("object cretaed");
                    $result1 = $sns->publish(array(
                        'TopicArn' => $topicarn,
                        // Message is required
                        'Message' => $message,
                        'Subject' => $subject,
                        //'MessageStructure' => 'string',
                        'MessageAttributes' => array(
                            // Associative array of custom 'String' key names
                            'String' => array(
                                // DataType is required
        //            'DataType' => '',
                                'DataType' => 'String',
                                'StringValue' => '200',
                            ),
                        // ... repeated
                        ),
                    ));
               
    }
    public function getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $payment_processor, $studio_name) {
        $select_student = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$company_id));
        $result_select_student = mysqli_query($this->db, $select_student);
        if (!$result_select_student) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student);
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result_select_student) > 0) {
                $r = mysqli_fetch_assoc($result_select_student);
                $student_id = $r['student_id'];
                if(is_null($r['stripe_customer_id']) || empty(trim($r['stripe_customer_id']))){
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = $r['stripe_customer_id'];
                    }
                }else{
                    $stripe_customer_id = $r['stripe_customer_id'];
                }
            } else {
                $insert_student = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_lastname`, `student_email`) VALUES('%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), 
                        mysqli_real_escape_string($this->db,$buyer_first_name), mysqli_real_escape_string($this->db,$buyer_last_name), mysqli_real_escape_string($this->db,$buyer_email));
                $result_insert_student = mysqli_query($this->db, $insert_student);
                if (!$result_insert_student) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student);
                    $this->response($this->json($error), 200);
                } else {
                    $student_id = mysqli_insert_id($this->db);
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = '';
                    }
                }
            }
            return array("student_id" => $student_id, "stripe_customer_id" => $stripe_customer_id);
        }
    }
    public function getParticipantIdForEvent($company_id, $student_id, $first_name, $last_name) {

        $select_participant2 = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name));
        $result_select_participant2 = mysqli_query($this->db, $select_participant2);
        if (!$result_select_participant2) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_participant2);
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result_select_participant2) > 0) {
                $r = mysqli_fetch_assoc($result_select_participant2);
                $participant_id = $r['participant_id'];
                $deleted_flag = $r['deleted_flag'];
                if($deleted_flag=='D'){
                    $update_participant = sprintf("UPDATE `participant` SET `deleted_flag`='N' WHERE `participant_id`='%s'", mysqli_real_escape_string($this->db, $participant_id));
                    $result_update_participant = mysqli_query($this->db, $update_participant);
                    if (!$result_update_participant) {
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_participant);
                        $this->response($this->json($error), 200);
                    }
                }
            } else {
                $insert_participant2 = sprintf("INSERT INTO `participant`(`company_id`, `student_id`, `participant_first_name`, `participant_last_name`) VALUES('%s', '%s', '%s', '%s')",
                        $company_id, $student_id, mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name));
                $result_insert_participant2 = mysqli_query($this->db, $insert_participant2);
                if (!$result_insert_participant2) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_participant2);
                    $this->response($this->json($error), 200);
                } else {
                    $participant_id = mysqli_insert_id($this->db);
                }
            }
            return $participant_id;
        }
    }
    
    protected function sendpush($data, $ios, $android, $socket_url) {
        $send_count = 0;
        //ios
        if (!empty($ios)) {
//            $old_fle_content = '';
            $ctx = stream_context_create();
            for ($r = 0; $r < count($ios['device_token']); $r++) {
//                $token_arr = [];
                $app_id = $ios['app_id'][$r];
                $deviceToken = $ios['device_token'][$r];
                $unread_msg_count = $ios['unread_msg_count'][$r];
//                $new_file_content = file_get_contents($ios['pem'][$r]);
//                if ((!empty($old_fle_content) && $old_fle_content !== $new_file_content) || $r == 0) {
//                    if ($r !== 0) {
//                        fclose($fp);
//                    }
                stream_context_set_option($ctx, 'ssl', 'local_cert', $ios['pem'][$r]);
                stream_context_set_option($ctx, 'ssl', 'passphrase', 'mystudio');
                $fp = stream_socket_client($socket_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                if (!$fp) {
                    $error_log = array('status' => "Failed", "msg" => "Message not delivered(I)[Error - $errstr($err)].", "App id" => $app_id, "Token" => "$deviceToken");
                    log_info($this->json($error_log));
                    continue;
                }
//                }
                // Build the binary notification
                $token_arr = explode(",", $deviceToken);
                $unread_msg_arr = explode(",", $unread_msg_count);
                if (count($token_arr) > 0) {
                    $chunk_token_arr = array_chunk($token_arr, 8);
                    $chunk_bagde_count = array_chunk($unread_msg_arr, 8);
                    $flag = 1;
                    for ($j = 0; $j < count($chunk_token_arr); $j++) {
                        if ($j !== 0) {
                            if ($flag == 1) {
                                fclose($fp);
                            }
                            $deviceToken = implode(",", $chunk_token_arr[$j]);
                            stream_context_set_option($ctx, 'ssl', 'local_cert', $ios['pem'][$r]);
                            stream_context_set_option($ctx, 'ssl', 'passphrase', 'mystudio');
                            $fp = stream_socket_client($socket_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                            if (!$fp) {
                                $error_log = array('status' => "Failed", "msg" => "Message not delivered(I)[Error - $errstr($err)].", "App id" => $app_id, "Token" => "$deviceToken");
                                log_info($this->json($error_log));
                                $flag = 0;
                                continue;
                            } else {
                                $flag = 1;
                            }
                        }
                        for ($c = 0; $c < count($chunk_token_arr[$j]); $c++) {                            
                            // Create the payload body
                            $body['aps'] = array(
                                'alert' => array(
                                    'title' => 'New Message received',
                                    'body' => $data
                                ),
                                'sound' => 'default',
                                'badge' => (int)$chunk_bagde_count[$j][$c]
                            );
                            // Encode the payload as JSON
                            $payload = json_encode($body);
                            $msg = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $chunk_token_arr[$j][$c])) . pack("n", strlen($payload)) . $payload;
                            $result = fwrite($fp, $msg, strlen($msg));
                            // Send it to the server
                            if (!$result) {
                                $error_log = array('status' => "Failed", "msg" => "Message not delivered(I).", "Token" => $chunk_token_arr[$j][$c], "PL" => $payload);
                                log_info($this->json($error_log));
                            } else {
                                $error_log = array('status' => "Success", "msg" => "Message successfully delivered(I).", "Token" => $chunk_token_arr[$j][$c], "PL" => $payload);
                                log_info($this->json($error_log));
                                $send_count++;
                            }
                        }
                    }
                    fclose($fp);
                }
//                $old_fle_content = file_get_contents($ios['pem'][$r]);
            }
        }

        if (!empty($android)) {
            //Android	
            $url = 'https://fcm.googleapis.com/fcm/send';
//            for ($r = 0; $r < count($android['device_token']); $r++) {
            foreach ($android['device_token'] as $point => $values) {
                $key = $android['key'][$point];
                $deviceToken = array_chunk($values, 900);
                for ($j = 0; $j < count($deviceToken); $j++) {
                      $ch = curl_init();
                    $devicetoken = implode(",", $deviceToken[$j]);
                    $fields = array(
                        'registration_ids' => $deviceToken[$j],
                        'data' => array(
                            "message" => $data,
                            "title" => 'New Message',
                            "badge" => 1
                        )
                    );
                    $fields = json_encode($fields);
                    $headers = array(
                        'Authorization: key=' . $key,
                        'Content-Type: application/json'
                    );
                    if ($url) {
                        // Set the url, number of POST vars, POST data
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        // Disabling SSL Certificate support temporarly
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        if ($fields) {
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                        }

                        // Execute post
                        $result = curl_exec($ch);
                        $resultArr = json_decode($result, true);
                        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        if ($result === FALSE || $resultArr['success'] == 0) {
                            //die('Curl failed: ' . curl_error($ch));
                            $error_log = array('status' => "Failed", "msg" => "Message not delivered(A).", "Token" => "$devicetoken", "StatusCode" => $returnCode, "Result" => json_encode($resultArr));
                            log_info($this->json($error_log));
                        } else {
                            $error_log = array('status' => "Success", "msg" => "Message successfully delivered(A).", "Token" => "$devicetoken", "StatusCode" => $returnCode, "Result" => json_encode($resultArr));
                            log_info($this->json($error_log));
                            $send_count += $resultArr['success'];
                        }
                    }
                    curl_close($ch);
                }
            }
        }
        return $send_count;
    }
    
    public function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
     
    public function verifyBouncedEmailForCC($cc_address, $company_id){
        $selecsql=sprintf("SELECT * FROM `bounce_email_verify` b WHERE b.`bounced_mail`='%s' AND (b.`company_id`=0 OR b.`company_id`='%s')", mysqli_real_escape_string($this->db,$cc_address), mysqli_real_escape_string($this->db,$company_id));
        $result= mysqli_query($this->db,$selecsql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
          if(mysqli_num_rows($result)>0){
              return 0;
          }else{
               return 1;
          }
        }
    }
    
    public function checkBouncedEmailCount($reg_id_list){
        $sql=sprintf("SELECT count(*) as b_count , count(`bounced_mail`) as Bounced_count, sum(if(e.`buyer_email` !=s.`student_email`,1,0)) as s_count, sum(if(s.deleted_flag='Y',1,0)) as deleted_count FROM `event_registration` e left JOIN `student` s ON  s.`student_id` = e.`student_id`
            and s.`company_id` = e.`company_id`left JOIN `bounce_email_verify` b ON  e.`buyer_email` = b.`bounced_mail` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
            where e.event_reg_id in (%s);",mysqli_real_escape_string($this->db,$reg_id_list));
        $result= mysqli_query($this->db, $sql);
//        log_info($sql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
             if(mysqli_num_rows($result)>0){
                 $rows = mysqli_fetch_assoc($result);
                  $rows['failed_count']=$rows['Bounced_count']+$rows['deleted_count'];
                 return $rows;
             }
        }
    }

    public function validateDate($date, $format = 'Y-m-d'){
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }
    
    public function getStudioSubscriptionStatus($company_id){
        $query = sprintf("SELECT email_id,upgrade_status,subscription_status,wepay_status,stripe_status,stripe_subscription,studio_expiry_level,expiry_dt FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_email = $row['email_id'];
                $upgrade_status = $row['upgrade_status'];
                $subscription_status = $row['subscription_status'];
                $wepay_status = $row['wepay_status'];
                $stripe_status = $row['stripe_status'];
                $stripe_subscription = $row['stripe_subscription'];
                $studio_expiry_level = $row['studio_expiry_level'];
                $expiry_dt = $row['expiry_dt'];
                if (!empty(trim($upgrade_status)) && $subscription_status=='N' && ($studio_expiry_level=='L2' || $upgrade_status=='F')) {
                    $res = array('status' => "Failed", "msg" => "Please contact the business operator to activate this link.", "studio_email"=>$studio_email, "upgrade_status"=>$upgrade_status, "studio_expiry_level" => $studio_expiry_level);
                    $this->response($this->json($res), 200);
                }else{
                    $res = array('status' => "Success", "msg" => "Studio subscription status success", "upgrade_status" => $upgrade_status);
                    return $res;
                }
                
            } else {
                $res = array('status' => "Failed", "msg" => "Studio Details not found.");
                $this->response($this->json($res), 200);
            }
        }
    }
    
      public function sqs() {
                 $queueUrl='https://sqs.us-east-1.amazonaws.com/241334827363/ms-bill-processing-dev';
        $credentials = new Aws\Credentials\Credentials('AKIAISVTETUMTCHAJINQ', 'dPUMmPYwce4nBH3mCp5JCM7+jGjonX6q1bn3rmWx');
        $client = SqsClient::factory([
                    'credentials' => $credentials,
                    'region' => 'us-east-1',
                    'version' => 'latest'
        ]);

        try {
            $result = $client->receiveMessage(array(
                'AttributeNames' => ['SentTimestamp'],
                'MaxNumberOfMessages' => 1,
                'MessageAttributeNames' => ['All'],
                'QueueUrl' => $queueUrl, // REQUIRED
                'WaitTimeSeconds' => 20,
            ));
            var_dump($result);
        } catch (AwsException $e) {
            // output error message if fails
            error_log($e->getMessage());
        }
        echo json_encode($result);
    }
    protected function getunsubscribefooter($company_id) {
        $sql = sprintf("SELECT company_name, CONCAT(street_address, IF(street_address = '' , city, CONCAT(', ',city)),IF(street_address = '' OR city = '', state, CONCAT(', ',state)),IF(street_address = '' OR city = '' OR state = '', postal_code, CONCAT(' ',postal_code))) address 
                    from company where company_id = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_name = $row['company_name'];
                $studio_address = trim($row['address']);
                if(!empty($studio_address)){
                    $studio_address = ($studio_address[strlen($studio_address) - 1] === "," ? substr($studio_address, 0, -1) : $studio_address);
                }
                $footer_detail = $studio_name . ($studio_address === "" ? '' : " | $studio_address");
                return $footer_detail;
            }
        }
    }
    
    public function updateEventEnabledStatusInDB($company_id, $event_enabled_status){
        //For Pos toggle off
        if($event_enabled_status === "N") {
            $update_settings = sprintf("UPDATE `company` c,`studio_pos_settings` pos SET c.`event_enabled`='%s',pos.`event_status`='D' WHERE c.`company_id`=pos.company_id AND c.`company_id`='%s'", mysqli_real_escape_string($this->db, $event_enabled_status), mysqli_real_escape_string($this->db, $company_id));
        }else{
            $update_settings = sprintf("UPDATE `company` SET `event_enabled`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $event_enabled_status), mysqli_real_escape_string($this->db, $company_id));
        }
        $result = mysqli_query($this->db, $update_settings);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_settings");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $selecsql=sprintf("SELECT event_enabled FROM `company`  WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
            $result1= mysqli_query($this->db,$selecsql);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else{
                $num_of_rows = mysqli_num_rows($result1);
                if ($num_of_rows > 0) {
                    $output[] = mysqli_fetch_assoc($result1);
                }
                $out = array('status' => "Success", 'msg' => $output);
                $this->response($this->json($out),200);
            }
        }
    }
    // stripe start
    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
    public function setup_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$studio_name,$buyer_email,$stripe_account_id,$p_type,$cc_type){
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        if ($cc_type == 'N') {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }
        try {
            $setup_intent_create = \Stripe\SetupIntent::create([
                'payment_method' => $payment_method_id,
                'customer' => $stripe_customer_id,
                'confirm' => true,
                'return_url' => $this->server_url."/stripeUpdation.php?type=authenticate&c_id=$company_id&p_type=$p_type&s_id=$student_id",
                'metadata' =>['c_id' => $company_id,'p_type' => $p_type,'s_id' => $student_id ,'s_name' => $studio_name],
                'description' => 'Payment Intent for ' . $buyer_email,
                'payment_method_types' => ['card'],
                'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                'on_behalf_of' => $stripe_account_id,
            ]);
            $body3 = json_encode($setup_intent_create);
            stripe_log_info("Stripe Setup Intent Creation : $body3");
            $return_array['temp_payment_status'] = $setup_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded'){
                $return_array['payment_intent_id'] = $setup_intent_create->id; // return response
                $return_array['setup_intent_client_secret'] = $setup_intent_create->client_secret;
            }else if ($return_array['temp_payment_status'] == 'requires_action'){
                $return_array['payment_intent_id'] = $setup_intent_create->id; // return response
                $return_array['setup_intent_client_secret'] = $setup_intent_create->client_secret;
                $return_array['next_action'] = $setup_intent_create->next_action;
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }
    
     public function payment_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$amount_in_dollars,$stripe_currency,$studio_name,$buyer_email,$stripe_account_id,$dest_amount_in_dollars,$p_type,$cc_type){
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");   
         if ($cc_type == 'N') {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }
        try {
            $payment_intent_create = \Stripe\PaymentIntent::create([
                'payment_method' => $payment_method_id,
                'customer' => $stripe_customer_id,
                'amount' => $amount_in_dollars,
                'currency' => $stripe_currency,
                'confirmation_method' => 'automatic',
                'capture_method' => 'automatic',
                'confirm' => true,
                'return_url' => $this->server_url."/stripeUpdation.php?type=authenticate&c_id=$company_id&p_type=$p_type&s_id=$student_id",
                'metadata' =>['c_id' => $company_id,'p_type' => $p_type,'s_id' => $student_id ,'s_name' => $studio_name],
                'setup_future_usage' => 'off_session',
                'description' => 'Payment Intent for ' . $buyer_email,
                'payment_method_types' => ['card'],
                'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                'receipt_email' => $buyer_email,
                'transfer_data' => ['destination' => $stripe_account_id, 'amount' => $dest_amount_in_dollars],
                'on_behalf_of' => $stripe_account_id,
            ]);
            $body3 = json_encode($payment_intent_create);
            stripe_log_info("Stripe Payment Creation : $body3");
            $return_array['temp_payment_status'] = $payment_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['cc_month'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_month;
                $return_array['cc_year'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_year;
                $brand = $payment_intent_create->charges->data[0]->payment_method_details->card->brand;
                $last4 = $payment_intent_create->charges->data[0]->payment_method_details->card->last4;
                $temp = ' xxxxxx';
                $return_array['stripe_card_name'] = $brand . $temp . $last4;
            }else if ($return_array['temp_payment_status'] == 'requires_action'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['payment_intent_client_secret'] = $payment_intent_create->client_secret;
                $return_array['next_action'] = $payment_intent_create->next_action; 
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }
    
    public function createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name){
        $stripe_customer_id = '';
        $query = sprintf("SELECT `stripe_customer_id` FROM `student` WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                $stripe_customer_id = $row['stripe_customer_id'];
            }
        }
        if(is_null($stripe_customer_id) || empty(trim($stripe_customer_id))){
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");

            try {
                $stripe_customer_create = \Stripe\Customer::create([
                            "email" => $buyer_email,
                            "description" => "Customer created with $buyer_email for $studio_name($company_id)",
                ]);
                $body = json_encode($stripe_customer_create);
                stripe_log_info("Customer_new : $body");
                $body_arr = json_decode($body, true);
                $stripe_customer_id = $body_arr['id'];

                //update stripe customer id to db - arun 
                $query2 = sprintf("UPDATE `student` SET `stripe_customer_id` = '%s' WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $student_id));
                $result2 = mysqli_query($this->db, $query2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }

            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
        
        return $stripe_customer_id;
    }
    
    public function getCompanyStripeAccount($company_id, $call_back){
        $query = sprintf("SELECT * FROM `stripe_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result) >0){
                $row = mysqli_fetch_assoc($result);
                if($call_back == 1){
                    return $row;
                }else{
                    $msg = array("status" => "Success", "msg" => "Stripe Account details", "stripe_details" => "$row");
                    $this->response($this->json($msg), 200);
                }
            }else{
                if($call_back == 1){
                    return '';
                }else{
                    $msg = array("status" => "Failed", "msg" => "Stripe Account details", "stripe_details" => "");
                    $this->response($this->json($msg), 200);
                }
            }
        }
    }
    
//    Stripe create checkout
    public function stripeCreatePortalCheckout($company_id, $event_id, $actual_student_id,$actual_participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, $payment_type, $upgrade_status, $total_order_amount, $total_order_quantity, $event_type,$reg_type_user,$reg_version_user,$payment_method_id,$cc_type,$stripe_customer_id){
        
        $payment_from = '';
        $registration_from = $payment_intent_id = '';
        $stripe_account_id = $stripe_currency = $studio_name = '';
        $w_paid_amount = $paid_fee = $paid_amount = 0;
        $sts = $this->getStudioSubscriptionStatus($company_id);
        
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
        if ((!empty($stripe_account) && $stripe_account['charges_enabled'] == 'Y' && $payment_amount > 0) || $payment_amount == 0) {  // ALLOWS FOR CASH & CHECK, IF CC IT CHECK THE CHARGES ENABLED TO BE 'Y'
            $upgrade_status = $sts['upgrade_status'];
            $querypf = sprintf("SELECT c.`company_name`, sa.`account_id`, sa.`currency` FROM `company` c 
                        LEFT JOIN `stripe_account` sa ON c.`company_id`=sa.`company_id`
                        WHERE c.`company_id`='%s' ", mysqli_real_escape_string($this->db, $company_id));
            $result = mysqli_query($this->db, $querypf);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$querypf");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $stripe_account_id = $row['account_id'];
                    $stripe_currency = $row['currency'];
                    $studio_name = $row['company_name'];
                }
            }
            $this->checkQuantityDuringCheckout($company_id,$event_id,$event_array,$quantity, $event_type);
            $buyer_name = $buyer_first_name.' '.$buyer_last_name;  
            if (is_null($actual_student_id) || $actual_student_id == 0) {
                $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Stripe', $studio_name);
                $student_id = $stud_array['student_id'];
                $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
            } else {
                $student_id = $actual_student_id;
                if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                    $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                }
            }
            if (is_null($actual_participant_id) || $actual_participant_id == 0) {
                $participant_id = $this->getParticipantIdForEvent($company_id, $student_id, $reg_col1, $reg_col2);
            } else {
                $participant_id = $actual_participant_id;
            }
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone=$user_timezone['timezone'];
            date_default_timezone_set($new_timezone);

            $balance_due = $deposit_amt = $pr_fee = $temp_pr_fee = 0;
            $no_of_payments = $authorize_only = 0;
            $payment_startdate = $current_date = date("Y-m-d");
            $gmt_date=gmdate(DATE_RFC822);
            $sns_msg1=array("buyer_email"=>$buyer_email,"membership_title"=>$event_name,"gmt_date"=>$gmt_date);
            $p_f = $this->getProcessingFee($company_id, $upgrade_status);
            $process_fee_per = $p_f['PP'];
            $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);
            if($fee==0 && $processing_fee_type==1){
                $fee = number_format((float)($payment_amount*$process_fee_per/100+$process_fee_val), 2, '.', '');
            }
            if(count($payment_array)>0){
                $no_of_payments = count($payment_array)-1;
                for($i=0;$i<count($payment_array);$i++){
                    if(isset($payment_array[$i]['date']) && empty($payment_array[$i]['date'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),200);
                    }            
                    if(isset($payment_array[$i]['amount']) && empty($payment_array[$i]['amount'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),200);
                    }
                    if(isset($payment_array[$i]['type']) && empty($payment_array[$i]['type'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),200);
                    }

                    if($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount']>0){
    //                    $balance_due = $payment_array[$i]['amount'];
                        $deposit_amt = $payment_array[$i]['amount'];
                        $pr_fee = $payment_array[$i]['processing_fee'];
                    }elseif($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount'] == 0){
                        $authorize_only = 1;
                        $no_of_payments = count($payment_array);
                    }
                    $temp_pr_fee += $payment_array[$i]['processing_fee'];
                }
                $fee = $temp_pr_fee;
            }else{
                $pr_fee = $fee;
            }
        
            usort($event_array, function($a, $b) {
                return $a['event_begin_dt'] > $b['event_begin_dt'];
            });
            
            if(count($event_array)>0){
                for($i=0;$i<count($event_array);$i++){
                    if(isset($event_array[$i]['event_id']) && empty($event_array[$i]['event_id'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),200);
                    }            
                    if(isset($event_array[$i]['event_quantity']) && empty($event_array[$i]['event_quantity'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),200);
                    }
                    if(!isset($event_array[$i]['event_cost'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),200);
                    }
                    if(!isset($event_array[$i]['event_discount_amount'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),200);
                    }            
                    if(!isset($event_array[$i]['event_payment_amount'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),200);
                    }            
                    if(!isset($event_array[$i]['event_begin_dt'])){
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error),200);
                    }
                    if(count($payment_array)>0){
                        if($balance_due>0){
                            $amt = $event_array[$i]['event_payment_amount'];
                            if($balance_due>=$event_array[$i]['event_payment_amount']){
                                if($balance_due==$amt){
                                    $t_amt = $amt;
                                }else{
                                    $t_amt = $balance_due - $amt;
                                }
                                $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($t_amt, $processing_fee_type, $process_fee_per, $process_fee_val);
                                $event_array[$i]['balance_due'] = 0;
                                $balance_due = $balance_due - $amt;
                            }else{
                                $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($balance_due, $processing_fee_type, $process_fee_per, $process_fee_val);
                                $event_array[$i]['balance_due'] = $amt - $balance_due;
                                $balance_due = 0;
                            }
                        }elseif($balance_due==0){
                            $event_array[$i]['processing_fee'] = 0;
                            $event_array[$i]['balance_due'] = $event_array[$i]['event_payment_amount'];
                        }
                    }else{
                        $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($event_array[$i]['event_payment_amount'], $processing_fee_type, $process_fee_per, $process_fee_val);
                        $event_array[$i]['balance_due'] = 0;
                    }
                }
            }
            // Amount calculation
            $paid_amount = $payment_amount;
            $paid_fee = $fee;
            $deposit_amount = 0; // MM

            if($processing_fee_type==2){
                $w_paid_amount = $payment_amount + $fee;
            }elseif($processing_fee_type==1){
                $w_paid_amount = $payment_amount;
            }
            if (count($payment_array) > 0) {
                for ($i = 0; $i < count($payment_array); $i++) {
                    if ($payment_array[$i]['type'] == 'D') {
                        $paid_amount = $payment_array[$i]['amount'];
                        $paid_fee = $payment_array[$i]['processing_fee'];
                        $deposit_amount = $paid_amount;
                        if ($processing_fee_type == 2) {
                            $w_paid_amount = $payment_array[$i]['amount'] + $payment_array[$i]['processing_fee'];
                        } elseif ($processing_fee_type == 1) {
                            $w_paid_amount = $payment_array[$i]['amount'];
                        }
                        break;
                    }
                }
            }
            
//            Find setup intent or payment intent
            if($deposit_amount == 0 && (count($payment_array) > 0)){
                $intent_method_type = "SI";
            }else{
                $intent_method_type = "PI";
            }

            // STRIPE ACTION START
            $amount_in_dollars = $dest_amount = $dest_amount_in_dollars = $w_paid_amount_round = $dest_amount_round = 0;
            $checkout_state = $cc_month = $cc_year = $stripe_card_name = $payment_status = $payment_intent_id = '';
            $next_action_type = $next_action_type2 = $button_url = '';
            $has_3D_secure = 'N';
            if (!empty($stripe_customer_id) && !(is_null($stripe_customer_id)) && !empty($payment_method_id)) {
                    
                if ($payment_amount > 0) {
                    $w_paid_amount_round = round($w_paid_amount, 2);
                    $dest_amount = $w_paid_amount - $paid_fee;
                    $dest_amount_round = round($dest_amount, 2);
                    $dest_amount_in_dollars = $dest_amount_round * 100;
                    $amount_in_dollars = $w_paid_amount_round * 100;
                    
                    $checkout_type = 'event';
                    if($intent_method_type == "SI"){ // If it is setup intent creation
                       $payment_intent_result = $this->setup_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $studio_name, $buyer_email, $stripe_account_id, $checkout_type,$cc_type); 
                    }else{ //If it is payment intent creation
                        $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $checkout_type,$cc_type);
                    }
                    if ($payment_intent_result['temp_payment_status'] == 'succeeded'){
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'released';
                    }elseif($payment_intent_result['temp_payment_status'] == 'requires_action') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'requires_action';
                        $has_3D_secure = 'Y';
                        $next_action_type = $payment_intent_result['next_action']['type'];
                        if($next_action_type != "redirect_to_url"){
                            # Payment not supported
                            $out['status'] = 'Failed';
                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                            $this->response($this->json($out), 200);
                        }
                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                    } else {
                        # Invalid status
                        $out['status'] = 'Failed';
                        $out['msg'] = 'Invalid SetupIntent status.';
                        $out['error'] = 'Invalid SetupIntent status';
                        $this->response($this->json($out), 200);
                    }
                    $payment_status = 'S';
                }
            } 
            if(!empty($payment_method_id)) {

                try {
                    $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                    $brand = $retrive_payment_method->card->brand;
                    $cc_month = $retrive_payment_method->card->exp_month;
                    $cc_year = $retrive_payment_method->card->exp_year;
                    $last4 = $retrive_payment_method->card->last4;
                    $temp = ' xxxxxx';
                    $stripe_card_name = $brand . $temp . $last4;
                } catch (\Stripe\Error\Card $e) {
                    // Since it's a decline, \Stripe\Error\Card will be caught     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\RateLimit $e) {
                    // Too many requests made to the API too quickly     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\InvalidRequest $e) {
                    // Invalid parameters were supplied to Stripe's API     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Authentication $e) {
                    // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\ApiConnection $e) {
                    // Network communication with Stripe failed     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Base $e) {
                    // Display a very generic error to the user, and maybe send yourself an email     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (Exception $e) {
                    // Something else happened, completely unrelated to Stripe     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                }
                if (isset($err) && !empty($err)) {
                    $error = array('status' => "Failed", "msg" => $err['message']);
                    $this->response($this->json($error), 200);
                }
            }
            $payment_method = 'CC';
            $payment_from = 'S';
            $registration_from = 'S';
            $query = sprintf("INSERT INTO `event_registration`(`company_id`, `event_id`, `reg_event_type`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                `payment_type`, `payment_amount`, `deposit_amount`, `number_of_payments`, `payment_startdate`, `payment_frequency`, `total_order_amount`, `total_order_quantity`, 
                `event_registration_column_1`, `event_registration_column_2`, `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`, `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`,`event_reg_type_user`,`event_reg_version_user`,`payment_method_id`,`stripe_card_name`,`stripe_customer_id`,`registration_from`, `credit_card_expiration_month`, `credit_card_expiration_year`, `payment_method`)
                    VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_type),
                    mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db,$participant_id),mysqli_real_escape_string($this->db, $buyer_first_name),mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code),
                    mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_type), 
                    mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $deposit_amt), mysqli_real_escape_string($this->db, $no_of_payments), mysqli_real_escape_string($this->db, $payment_startdate), mysqli_real_escape_string($this->db, $payment_frequency), 
                    mysqli_real_escape_string($this->db, $total_order_amount), mysqli_real_escape_string($this->db, $total_order_quantity), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4),
                    mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user),
                    mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name),mysqli_real_escape_string($this->db, $stripe_customer_id),mysqli_real_escape_string($this->db, $registration_from),mysqli_real_escape_string($this->db, $cc_month),mysqli_real_escape_string($this->db, $cc_year),mysqli_real_escape_string($this->db, $payment_method));
            $result = mysqli_query($this->db, $query);
            if(!$result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                $event_reg_id = mysqli_insert_id($this->db);

                if(count($payment_array)>0){
                    $temp1 = $temp4 = 0;
                    for($i=0;$i<count($payment_array);$i++){
                        $pdate = $payment_array[$i]['date'];
                        $pamount = $payment_array[$i]['amount'];
                        $ptype = $payment_array[$i]['type'];
                        $pfee = $payment_array[$i]['processing_fee'];
                        if($ptype!='D'){
                            $checkout_state = '';
                            $pstatus = 'N';
                            $pi_id = '';
                        }else{
                            $pi_id = $payment_intent_id;
                            $pstatus = 'S';
                        }

                        $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_date`, `schedule_status`, `payment_method_id`,`stripe_card_name`,`payment_intent_id`,`payment_from`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_state), 
                                mysqli_real_escape_string($this->db, $pfee), mysqli_real_escape_string($this->db, $pamount), mysqli_real_escape_string($this->db, $pdate), mysqli_real_escape_string($this->db, $pstatus), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $pi_id), mysqli_real_escape_string($this->db, $payment_from));
                        $payment_result = mysqli_query($this->db, $payment_query);
                        if(!$payment_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                            log_info($this->json($error_log));
                            $temp1 += 1;
                        }
                    }
                    if($temp1>0){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp1);
                        $this->response($this->json($error), 200);
                    }
                }else{
                    $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_status`, `payment_method_id`,`stripe_card_name`,`payment_intent_id`,`payment_from`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_state), 
                            mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $payment_from));
                    $payment_result = mysqli_query($this->db, $payment_query);
                    if(!$payment_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }

                if(count($event_array)>0){
                    $qty = $net_sales = $parent_net_sales = 0;
                    $temp = $temp2= $temp3 = 0;
                    for($i=0;$i<count($event_array);$i++){
                        $cevent_id = $event_array[$i]['event_id'];
                        $cquantity =  $event_array[$i]['event_quantity'];
                        $cevent_cost = $event_array[$i]['event_cost'];
                        $cdiscount = $event_array[$i]['event_discount_amount'];
                        $cpayment_amount = $event_array[$i]['event_payment_amount'];
                        $cbalance = $event_array[$i]['balance_due'];
                        $cprocessing_fee = $event_array[$i]['processing_fee'];
                        if(count($payment_array)>0){
                            if($cbalance>0){
                                $cstatus = 'RP';
                            }else{
                                $cstatus = 'RC';
                            }
                        }else{
                            $cstatus = 'CC';
                        }

                        $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $cevent_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cevent_cost), 
                                mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cbalance), mysqli_real_escape_string($this->db, $cprocessing_fee), mysqli_real_escape_string($this->db, $cstatus));
                        $reg_det_result = mysqli_query($this->db, $reg_det_query);
                        if(!$reg_det_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                            log_info($this->json($error_log));
                            $temp += 1;
                        }

                        $pd_amount = $cpayment_amount - $cbalance;

                        if(count($payment_array)>0){
                            if($processing_fee_type==2){
                                $net_sales += $pd_amount;
                                $parent_net_sales += $pd_amount;
                            }elseif($processing_fee_type==1){
                                $net_sales += $pd_amount-$cprocessing_fee;
                                $parent_net_sales += $pd_amount-$cprocessing_fee;
                            }
                        }else{
                            if($processing_fee_type==2){
                                $parent_net_sales = $payment_amount;
                            }else{
                                $parent_net_sales = $payment_amount-$fee;
                            }
                        }
                        $qty += $cquantity;
//                                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                        $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                        $update_event_result = mysqli_query($this->db, $update_event_query);
                        if(!$update_event_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                            log_info($this->json($error_log));
                            $temp2 += 1;
                        }
                    }
                    if($temp>0){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp);
                        $this->response($this->json($error), 200);
                    }
                    if($temp2>0){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp2);
                        $this->response($this->json($error), 200);
                    }

                    $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$qty WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                    $update_event_result = mysqli_query($this->db, $update_event_query);
                    if(!$update_event_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }else{
                    $sbalance = $payment_amount;
                    if($sbalance>0){
                        $fee = $pr_fee;
                    }
                    if($processing_fee_type==1){
                        $paid_amount -= $fee;
                    }
                    if(count($payment_array)>0){
                        if($sbalance>0){
                            $sstatus = 'RP';
                        }else{
                            $sstatus = 'RC';
                        }
                    }else{
                        $sstatus = 'CC';
                    }
                    $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $registration_amount), 
                            mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $quantity), mysqli_real_escape_string($this->db, $sbalance), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $sstatus));
                    $reg_det_result = mysqli_query($this->db, $reg_det_query);
                    if(!$reg_det_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }

                    $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                    $update_event_result = mysqli_query($this->db, $update_event_query);
                    if(!$update_event_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
                //update cc information if release state
                if ($checkout_state == 'released') {
                     $this->updateStripeCCDetailsAfterRelease($payment_intent_id);
                }
                //End update cc information if release state
                
                $selectcurrency=sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'",  mysqli_escape_string($this->db, $company_id));
                $resultselectcurrency=  mysqli_query($this->db, $selectcurrency);
                if(!$resultselectcurrency){
                     $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                    log_info($this->json($error_log));
                }else{
                    $row = mysqli_fetch_assoc($resultselectcurrency);
                    $wp_currency_symbol=$row['wp_currency_symbol'];
                }

                $curr_date = gmdate("Y-m-d H:i:s");
                if ($event_type == 'S') {
                    if ($discount!=0 || !empty($discount)) {
                        $activity_text = "Registered for $event_name, quantity $quantity. Total cost $wp_currency_symbol".$payment_amount.". Discount amount used $wp_currency_symbol.$discount";
                    } else {
                        $activity_text = "Registered for $event_name, quantity $quantity. Total cost $wp_currency_symbol".$payment_amount;
                    }
                    $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id),
                            'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                    $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                    if (!$result_insert_event_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                        log_info($this->json($error_log));
                    }
                } else {
                    for ($i = 0; $i < count($event_array); $i++) {
                        $event_title = $event_array[$i]['event_title'];
                        $event_quantity = $event_array[$i]['event_quantity'];
                        $event_cost = $event_array[$i]['event_payment_amount'];
                        $event_discount_amount = $event_array[$i]['event_discount_amount'];
//                                log_info("event_cost".$event_cost."event_discount_amount".$event_discount_amount);
                        if ($event_discount_amount!=0 || !empty($event_discount_amount)) {
                            $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol".($event_cost).". Discount amount used $wp_currency_symbol".($event_discount_amount);
                        } else {
                            $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol".$event_cost;
                        }
                        $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id),
                                'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                        $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                        if (!$result_insert_event_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                            log_info($this->json($error_log));
                        }
                    }
                }
                $event_details = $this->getEventsDetailByEventId($company_id, $event_id, $event_type, 'live', 1);
                if ($has_3D_secure == 'N') {
                    $text = "Event registration was successful. Email confirmation sent.";
                } else {
                    $text = "Event charge was processed and waiting for customer action. Kindly review and authenticate the payment.";
                }
                $msg = array("status" => "Success", "msg" => $text, "event_details"=>$event_details);
                $this->responseWithWepay($this->json($msg),200);
                if ($has_3D_secure == 'N') {
                    $this->sendOrderReceiptForEventPayment($company_id,$event_reg_id,0);
                } else {
                    $email_msg = 'Hi '.$buyer_first_name.' '.$buyer_last_name.',<br>';
                    $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">This is an authentication email for Event '.$event_name.' registration. Kindly click the launch button for redirecting to Stripe.</p>';
                    $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">Complete a required action for this payment. Once the redirect is completed for this PaymentIntent, the payment will proceed.</p>';
                    $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg);
                }
            }
        }else {
            $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
            $this->response($this->json($error), 200);
        }
        date_default_timezone_set($curr_time_zone);
    }
    // End of stripe checkout
    // 3D secure authentication email
    public function sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg){
        
        // EMAIL CONTENT
        $button_text = 'Launch';
        $subject = 'Stripe authenticate';
        $message = $email_msg.'<br>'.'<center><a href="'.$button_url.'"><button type="button" style="width: 190px;border-radius: 5px;background: #009a61;border:1px solid 009a61;color: #ffffff;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 45px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;text-transform: none;overflow: visible;box-sizing: border-box;cursor: pointer;">'.$button_text.'</button></a></center>';
        $sendEmail_status = $this->sendEmail($buyer_email, $subject, $message, $studio_name, '', '', '', '', $company_id, '', '');
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
    }
    public function updateStripeCCDetailsAfterRelease($payment_intent_id){
        $event_reg_id = $event_id = $payment_id = $processing_fee_type = $payment_type = '';
        $payment_amount = 0;
        
        $query = sprintf("SELECT er.company_id, er.`event_reg_id`, ep.`event_payment_id`, e.`event_title`, ep.`payment_amount`, ep.`processing_fee`, er.`processing_fee_type`, er.`event_id`, er.`payment_type`
                FROM `event_registration` er 
                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` 
                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
                WHERE ep.`payment_intent_id` = '%s' ORDER BY ep.`event_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        log_info($this->json($query));
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $event_reg_id = $row['event_reg_id'];
                $event_id = $row['event_id'];
                $payment_id = $row['event_payment_id'];
                $payment_amount = $row['payment_amount'];
                $processing_fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_type = $row['payment_type'];
                
                $temp_payment_amount = $payment_amount;
                $onetime_netsales_for_parent_event = 0;

                $update_event_reg_query = sprintf("UPDATE `event_registration` SET `paid_amount`=`paid_amount`+$payment_amount WHERE `event_reg_id`='%s'",$event_reg_id);
                $result_update_event_reg_query = mysqli_query($this->db, $update_event_reg_query);
                if(!$result_update_event_reg_query){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_reg_query", "ipn" => "1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }

                if($payment_type=='CO'){
                    $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_type`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
                               LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
                               AND `payment_status` IN ('CC') ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                    $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                    if(!$get_event_details_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query", "ipn" => "1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $num_rows2 = mysqli_num_rows($get_event_details_result);
                        if($num_rows2>0){
                            $rem_due = 0;
                            while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                                if($temp_payment_amount>0){
                                    $event_reg_details_id = $row2['event_reg_details_id'];
                                    $child_id = $row2['event_id'];
                                    $event_parent_id = $row2['parent_id'];
                                    $child_payment = $row2['payment_amount'];
                                    $rem_due = $row2['balance_due'];
                                    $event_type = $row2['event_type'];
                                    $payment_status = $row2['payment_status'];
                                    $child_net_sales = 0;
                                    if($onetime_netsales_for_parent_event==0){
                                        if($processing_fee_type==2){
                                            $net_sales = $payment_amount;
                                        }elseif($processing_fee_type==1){
                                            $net_sales = $payment_amount-$processing_fee;
                                        }
                                        $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
                                        $result_update_event_query = mysqli_query($this->db, $update_event_query);
                                        if(!$result_update_event_query){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query", "ipn" => "1");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        }
                                        $onetime_netsales_for_parent_event=1;
                                    }
                                    if($temp_payment_amount>=$child_payment){
                                        $temp_payment_amount = $temp_payment_amount-$child_payment;
                                        $child_net_sales = $child_payment;
                                        $child_payment = 0;
                                    }else{
                                        $child_payment = $child_payment - $temp_payment_amount;
                                        $child_net_sales = $temp_payment_amount;
                                        $temp_payment_amount = 0;
                                    }
                                    $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$child_payment WHERE `event_reg_details_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
                                    $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
                                    if(!$result_update_reg_query){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query", "ipn" => "1");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 500);
                                    }
                                    if($child_id!=$event_parent_id){
                                        $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
                                        $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
                                        if(!$result_update_child_query){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales", "ipn" => "1");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_type`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
                               LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
                               AND `payment_status` = 'RP' ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                    $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                    if(!$get_event_details_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query", "ipn" => "1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $num_rows2 = mysqli_num_rows($get_event_details_result);
                        if($num_rows2>0){
                            $rem_due = 0;
                            while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                                if($temp_payment_amount>0){
                                    $event_reg_details_id = $row2['event_reg_details_id'];
                                    $child_id = $row2['event_id'];
                                    $event_parent_id = $row2['parent_id'];
                                    $rem_due = $row2['balance_due'];
                                    $event_type = $row2['event_type'];
                                    $payment_status = $row2['payment_status'];
                                    $child_net_sales = 0;
                                    if($onetime_netsales_for_parent_event==0){
                                        if($processing_fee_type==2){
                                            $net_sales = $payment_amount;
                                        }elseif($processing_fee_type==1){
                                            $net_sales = $payment_amount-$processing_fee;
                                        }
                                        $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
                                        $result_update_event_query = mysqli_query($this->db, $update_event_query);
                                        if(!$result_update_event_query){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query", "ipn" => "1");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        }
                                        $onetime_netsales_for_parent_event=1;
                                    }
                                    if($temp_payment_amount>=$rem_due){
                                        $temp_payment_amount = $temp_payment_amount-$rem_due;
                                        $child_net_sales = $rem_due;
                                        $rem_due = 0;
                                        $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due, `payment_status`='RC' WHERE `event_reg_details_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
                                    }else{
                                        $rem_due = $rem_due - $temp_payment_amount;
                                        $child_net_sales = $temp_payment_amount;
                                        $temp_payment_amount = 0;
                                        $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due WHERE `event_reg_details_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
                                    }
                                    $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
                                    if(!$result_update_reg_query){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query", "ipn" => "1");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 500);
                                    }else{
                                        if($child_id!=$event_parent_id){
                                            $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
                                            $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
                                            if(!$result_update_child_query){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales", "ipn" => "1");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 500);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $payment_status = "";
                $checkout_state = "released";
                
                $update_query = sprintf("UPDATE `event_payment` SET `checkout_status`='%s' $payment_status WHERE `event_reg_id`='%s' and `event_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                        mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                $update_result = mysqli_query($this->db, $update_query);
                if(!$update_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query", "ipn" => "1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }
    }
    
    public function updateEventstripePaymentMethodDetails($event_reg_id, $payment_method_id, $buyer_first_name, $buyer_last_name, $buyer_email, $page_key, $type) {
        $stripe_card_name = '';
        $stripe_customer_id = '';
        $bactivity_type = $bactivity_text = "";
        $query1 = sprintf("SELECT er.payment_method,er.company_id, 
                  er.`student_id`, er.`event_id`, er.`payment_type`, max(ep.`schedule_date`) s_date, er.`buyer_first_name`,er.`buyer_last_name`,sa.`charges_enabled`,s.`stripe_customer_id` 
                  FROM `event_registration` er  
                  LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` LEFT JOIN `student` s ON er.`student_id` = s.`student_id` LEFT JOIN `stripe_account` sa on sa.`company_id` = s.`company_id` WHERE er.`event_reg_id`='%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result1 = mysqli_query($this->db, $query1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                $row = mysqli_fetch_assoc($result1);
                $event_id = $row['event_id'];
                $stripe_customer_id = $row['stripe_customer_id'];
                $payment_type = $row['payment_type'];
                $schedule_date = $row['s_date'];
                $company_id = $row['company_id'];
                $student_id = $row['student_id'];
                $old_buyer_first_name = $row['buyer_first_name'];
                $old_buyer_last_name = $row['buyer_last_name'];
                $stripe_charges_enabled = $row['charges_enabled'];
                if ($type == "CC") {
                    if ($old_buyer_first_name !== $buyer_first_name && $old_buyer_last_name !== $buyer_last_name) {
                        $bactivity_type = "buyer name edit";
                        $bactivity_text = "buyer name is changed from $old_buyer_first_name $old_buyer_last_name to $buyer_first_name $buyer_last_name.";
                    } elseif ($old_buyer_first_name !== $buyer_first_name) {
                        $bactivity_type = "buyer name edit";
                        $bactivity_text = "buyer first name is changed from $old_buyer_first_name to $buyer_first_name";
                    } elseif ($old_buyer_last_name !== $buyer_last_name) {
                        $bactivity_type = "buyer name edit";
                        $bactivity_text = "buyer last name is changed from $old_buyer_last_name to $buyer_last_name";
                    }
                } else {
                    if ($type == $row['payment_method']) {
                        $log = array("status" => "Failed", "msg" => "Cannot update payment method");
                        $this->response($this->json($log), 200);
                    }
                    $bactivity_type = "Payment method update";
//                    $from_method = ($row['payment_method'] == "CA" ? "Cash" : "Check");
//                    $to_method = ($type == "CA" ? "Cash":"Check");
//                    .$from_method." to ".$to_method
                    $bactivity_text = "Payment method changed to Manual";
                }

                if (($stripe_charges_enabled == 'Y') || ($type == "CA" || $type == "CH")) {
                    if ($type == "CC" && !empty($payment_method_id)) {
                        $this->getStripeKeys();
                        \Stripe\Stripe::setApiKey($this->stripe_sk);
                        \Stripe\Stripe::setApiVersion("2019-05-16");
                        try {
                            $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                            $payment_method->attach(['customer' => $stripe_customer_id]);
                        } catch (\Stripe\Error\Card $e) {
                            // Since it's a decline, \Stripe\Error\Card will be caught       
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\RateLimit $e) {
                            // Too many requests made to the API too quickly      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\InvalidRequest $e) {
                            // Invalid parameters were supplied to Stripe's API      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Authentication $e) {
                            // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\ApiConnection $e) {
                            // Network communication with Stripe failed      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Base $e) {
                            // Display a very generic error to the user, and maybe send yourself an email      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (Exception $e) {
                            // Something else happened, completely unrelated to Stripe      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        }
                        if (isset($err) && !empty($err)) {
                            $error = array('status' => "Failed", "msg" => $err['message']);
                            $this->response($this->json($error), 200);
                        }

                        //Retrieving payment method with payment method id for card details
                        try {
                            $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                            $brand = $retrive_payment_method->card->brand;
                            $cc_month = $retrive_payment_method->card->exp_month;
                            $cc_year = $retrive_payment_method->card->exp_year;
                            $last4 = $retrive_payment_method->card->last4;
                            $temp = ' xxxxxx';
                            $stripe_card_name = $brand . $temp . $last4;
                        } catch (\Stripe\Error\Card $e) {
                            // Since it's a decline, \Stripe\Error\Card will be caught     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\RateLimit $e) {
                            // Too many requests made to the API too quickly     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\InvalidRequest $e) {
                            // Invalid parameters were supplied to Stripe's API     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Authentication $e) {
                            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\ApiConnection $e) {
                            // Network communication with Stripe failed     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Base $e) {
                            // Display a very generic error to the user, and maybe send yourself an email     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (Exception $e) {
                            // Something else happened, completely unrelated to Stripe     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        }
                        if (isset($err) && !empty($err)) {
                            $error = array('status' => "Failed", "msg" => $err['message']);
                            $this->response($this->json($error), 200);
                        }
                    }
                    if ($type == "CC") {
                        $update_query = sprintf("UPDATE `event_registration` SET payment_method='%s',`buyer_first_name`='%s',`buyer_last_name`='%s',`buyer_email`='%s',`payment_method_id`='%s',
                            `stripe_card_name`='%s',`credit_card_expiration_month`='%s',`credit_card_expiration_year`='%s' WHERE `event_reg_id`='%s'", mysqli_real_escape_string($this->db, $type), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $event_reg_id));
                    } else {
                        $update_query = sprintf("UPDATE `event_registration` SET `payment_method`='%s' WHERE `event_reg_id`='%s'", mysqli_real_escape_string($this->db, $type), mysqli_real_escape_string($this->db, $event_reg_id));
                    }
                    $result_update = mysqli_query($this->db, $update_query);
                    if (!$result_update) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $curr_date = gmdate("Y-m-d H:i:s");
                        if (!empty($bactivity_type)) {
                            $insert_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $student_id, $bactivity_type, $bactivity_text, $curr_date);
                            $result_history = mysqli_query($this->db, $insert_history);
                            if (!$result_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                        if ($type == "CC") {
                            $update_query2 = sprintf("UPDATE `event_payment` SET `payment_method_id`='%s', `stripe_card_name`='%s' WHERE `event_reg_id`='%s' AND `schedule_status` IN ('N','F')", mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $event_reg_id));
                        } else {
                            $update_query2 = sprintf("UPDATE `event_payment` SET `payment_method_id`='', `stripe_card_name`='',credit_method='%s' WHERE `event_reg_id`='%s' AND `schedule_status` IN ('N','F')", mysqli_real_escape_string($this->db, $type), mysqli_real_escape_string($this->db, $event_reg_id));
                        }

                        $result_update2 = mysqli_query($this->db, $update_query2);
                        if (!$result_update2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query2");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    if ($type == "CC") {
                        $activity_text = "Payment method changed " . $stripe_card_name;
                        $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'payment method change', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                        $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                        if (!$result_insert_event_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                            log_info($this->json($error_log));
                        }
                    }
                    if ($page_key == 'ED') { //ED - Event Detail call
                        $reg_details = $this->getEventParticipantDetailsForRefund($company_id, $event_id, $event_reg_id, 1);
                        $msg = array('status' => "Success", "msg" => "New Payment method successfully updated.", "reg_details" => $reg_details);
                        $this->response($this->json($msg), 200);
                    } else { // MM - Manage Membership call PP-Payment Page
                        $msg = array('status' => "Success", "msg" => "New Payment method successfully updated.");
                        $this->response($this->json($msg), 200);
                    }
                } else {
                    $error = array('status' => "Failed", "msg" => "Please Enable Your Stripe Payments");
                    $this->response($this->json($error), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Particpant payment details mismatch.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function reRunstripeEventPayment($company_id, $reg_id, $payment_id) {
//        log_info("runCronJobForEventPreapproval() - Started");     
        $stripe_card_name = '';
        $has_3D_secure = 'N';
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";

        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';

        $query = sprintf("SELECT er.`company_id`, er.`student_id`, e.`event_title`, er.`event_reg_id`, `event_payment_id`, er.`event_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, 
                `registration_amount`, ep.`processing_fee`, `payment_type`, er.`payment_frequency`, er.`payment_amount` total_due, `paid_amount`, ep.`payment_method_id`, ep.`stripe_card_name`, `total_order_amount`, `total_order_quantity`, 
                ep.`payment_amount`, ep.`schedule_date`, ep.`schedule_status`, er.`processing_fee_type`, ep.`payment_intent_id`, ep.`checkout_status`,c.`wp_currency_symbol`,c.`company_name`,s.`currency`,s.`account_id`,er.`stripe_customer_id`
                FROM `event_registration` er 
                LEFT JOIN `wp_account` wp ON er.`company_id` = wp.`company_id` 
                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` AND (ep.`schedule_status`='F' || (ep.`schedule_status`='N' && ep.`schedule_date`< ($currdate_add)))
                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
                LEFT JOIN `company` c ON er.`company_id` = c.`company_id` 
                LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` 
                WHERE   er.`event_reg_id` = $reg_id  AND `event_payment_id` = $payment_id  AND ep.`payment_amount`>0");
//         WHERE ep.`schedule_date`=now() AND `payment_type`='RE' AND ep.`payment_amount`>0 ORDER BY er.`company_id`
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $company_list = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $company_id = $row['company_id'];
                    $student_id = $row['student_id'];
                    $stripe_account_id = $row['account_id'];
                    $stripe_currency = $row['currency'];
                    $event_reg_id = $row['event_reg_id'];
                    $event_payment_id = $row['event_payment_id'];
                    $old_payment_intent_id = $row['payment_intent_id'];
                    $old_checkout_status = $row['checkout_status'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $buyer_postal_code = $row['buyer_postal_code'];
                    $payment_method_id = $row['payment_method_id'];
                    $stripe_card_name = $row['stripe_card_name'];
                    $stripe_customer_id = $row['stripe_customer_id'];
                    $studio_name = $row['company_name'];

                    if (!empty($payment_method_id)) {
                        $failure = $success = 0;
                        $event_id = $row['event_id'];
                        $event_name = $desc = $row['event_title'];
                        $payment_amount = $row['payment_amount'];
                        $processing_fee = $row['processing_fee'];
                        $processing_fee_type = $row['processing_fee_type'];
                        $wp_currency_symbol = $row['wp_currency_symbol'];
                        $schedule_date = $row['schedule_date'];

                        $this->getStripeKeys();
                        \Stripe\Stripe::setApiKey($this->stripe_sk);
                        \Stripe\Stripe::setApiVersion("2019-05-16");
                        if (!empty($stripe_customer_id) && !empty($payment_method_id)) {
                            $paid_amount = $payment_amount;
                            if ($processing_fee_type == 2) {
                                $w_paid_amount = $payment_amount + $processing_fee;
                            } elseif ($processing_fee_type == 1) {
                                $w_paid_amount = $payment_amount;
                            }
                            $w_paid_amount_round = round($w_paid_amount, 2);
                            $dest_amount = $w_paid_amount - $processing_fee;
                            $dest_amount_round = round($dest_amount, 2);
                            $dest_amount_in_dollars = $dest_amount_round * 100;
                            $amount_in_dollars = $w_paid_amount_round * 100;
                            $checkout_type = 'event';
                            $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $checkout_type, '');
                            if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                                $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                $checkout_state = 'released';
                            } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {
                                $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                $checkout_state = 'requires_action';
                                $has_3D_secure = 'Y';
                                $next_action_type = $payment_intent_result['next_action']['type'];
                                if($next_action_type != "redirect_to_url"){
                                    # Payment not supported
                                    $out['status'] = 'Failed';
                                    $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                                    $this->response($this->json($out), 200);
                                }
                                $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                            } else {
                                # Invalid status
                                $out['status'] = 'Failed';
                                $out['msg'] = 'Invalid PaymentIntent status.';
                                $out['error'] = 'Invalid PaymentIntent status';
                                $this->response($this->json($out), 200);
                            }
                            $payment_status = 'S';
                        }

                        if (!empty($payment_method_id)) {
                            try {
                                $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                                $brand = $retrive_payment_method->card->brand;
                                $cc_month = $retrive_payment_method->card->exp_month;
                                $cc_year = $retrive_payment_method->card->exp_year;
                                $last4 = $retrive_payment_method->card->last4;
                                $temp = ' xxxxxx';
                                $stripe_card_name = $brand . $temp . $last4;
                            } catch (\Stripe\Error\Card $e) {
                                // Since it's a decline, \Stripe\Error\Card will be caught     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\RateLimit $e) {
                                // Too many requests made to the API too quickly     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\InvalidRequest $e) {
                                // Invalid parameters were supplied to Stripe's API     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Authentication $e) {
                                // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\ApiConnection $e) {
                                // Network communication with Stripe failed     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Base $e) {
                                // Display a very generic error to the user, and maybe send yourself an email     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (Exception $e) {
                                // Something else happened, completely unrelated to Stripe     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            }
                            if (isset($err) && !empty($err)) {
                                $error = array('status' => "Failed", "msg" => $err['message']);
                                $this->response($this->json($error), 200);
                            }
                        }

                        $payment_query = sprintf("UPDATE `event_payment` SET `payment_intent_id`='%s', `checkout_status`='%s', `schedule_status`='%s', `payment_method_id`='$payment_method_id', `stripe_card_name`='$stripe_card_name' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $event_payment_id));
                        $payment_result = mysqli_query($this->db, $payment_query);
                        if (!$payment_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
//                            $num_affected_rows = mysqli_affected_rows($this->db);
//                            if ($num_affected_rows > 0) {
                            $activity_date_time = gmdate("Y-m-d H:i:s");
                            $activity_type = "Rerun payment";
//                            $text_date = date("M d, Y",strtotime($activity_date_time));
                            if (empty($schedule_date)) {
                                $text_date1 = "";
                            } else {
                                $text_date1 = date("M d, Y", strtotime($schedule_date));
                            }
                            $activity_text = "Rerun payment $wp_currency_symbol" . "$w_paid_amount" . " past due " . "$text_date1 paid";
                            $insert_pay_history = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) values ('%s','%s','%s','%s','%s','%s')", mysqli_escape_string($this->db, $company_id), mysqli_escape_string($this->db, $reg_id)
                                    , mysqli_escape_string($this->db, $event_payment_id), mysqli_escape_string($this->db, $activity_type), mysqli_escape_string($this->db, $activity_text), mysqli_escape_string($this->db, $activity_date_time)); //work
                            $result_pay_history = mysqli_query($this->db, $insert_pay_history);

                            if (!$result_pay_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_pay_history");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                            $payment_history = $this->getpaymentHistoryDetails($company_id, $reg_id, 1);
                            if($has_3D_secure=='Y'){
                                 $email_msg = 'Hi '.$buyer_name.',<br>';
                                 $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">This is an authentication email for Event '.$event_name.' registration rerun payment. Kindly click the launch button for redirecting to Stripe.</p>';
                                 $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">Complete a required action for this payment. Once the redirect is completed for this PaymentIntent, the payment will proceed.</p>';
                                 $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name, $email_msg);
                            }
                            $error = array('status' => "Success", "msg" => "Payment Successful.");
                            $this->response($this->json($error), 200);
                        }
                    } else {
                        $error = array('status' => "Failed", "msg" => "Payment Method error. Cannot be accept payments anymore.");
                        $this->response($this->json($error), 200);
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Trial payment details mismatched.");
                $this->response($this->json($error), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
        
    public function processstripePaymentRefund($company_id, $event_reg_id, $event_array, $plan_array, $total_due, $cancel_flag){
        $payment_id_arr = $charge_retrieve_arr = [];
        $payment_id_arr_string = '';
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        
        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
        $stop_stripe = false;
//        $country = 'US';
        $query1 = sprintf("SELECT c.`company_name`,c.`wp_currency_symbol`, er.`student_id`, er.`payment_type`, er.`payment_method_id`, er.`stripe_card_name`, er.`processing_fee_type`, c.`buyer_name`, c.`buyer_email`, c.`buyer_phone`, c.`buyer_postal_code`, e.`event_type`, e.`event_title`, er.`stripe_customer_id`, s.`account_id`, s.`currency`,c.`upgrade_status`,s.`stripe_refund_flag` FROM company c
                LEFT JOIN `event_registration` er ON er.`company_id`=c.`company_id` 
                LEFT JOIN `event` e ON er.`event_id`=e.`event_id` 
                LEFT JOIN `stripe_account` s ON s.`company_id`=c.`company_id`
                WHERE c.`company_id`='%s' AND `event_reg_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id));
        $result1 = mysqli_query($this->db, $query1);
        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "1- $query1");
        log_info($this->json($error_log));
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "1- $query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $row = mysqli_fetch_assoc($result1);
                $wp_currency_symbol = $row['wp_currency_symbol'];
                $upgrade_status = $row['upgrade_status'];
                $student_id = $row['student_id'];
                $event_type = $row['event_type'];
                $event_name = $row['event_title'];
                $payment_type = $row['payment_type'];
                $payment_method_id = $row['payment_method_id'];
                $stripe_card_name = $row['stripe_card_name'];
                $processing_fee_type = $row['processing_fee_type'];
                $buyer_name = $row['buyer_name'];
                $buyer_email = $row['buyer_email'];
                $buyer_phone = $row['buyer_phone'];
                $buyer_postal_code = $row['buyer_postal_code'];
                $stripe_customer_id = $row['stripe_customer_id'];
                $stripe_account_id = $row['account_id'];
                $stripe_currency = $row['currency'];
                $studio_name = $row['company_name']; 
                $stripe_refund_flag = $row['stripe_refund_flag'];
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error),200);
            }
        }
        
        $paid_amount = $parent_qty = $total_refund_amount = $update_event_remaining_dues = $update_event_amount = $event_refund_sales = $update_refund_sales_flag = $update_zero_cost_payment_record = 0;
        $curr_date = date("Y-m-d");
        $parent_net_sales = 0;
        $manual_refund_amount = 0;
        
        if($stripe_refund_flag == 'N'){
            $error = array("status" => "Failed", "msg" => "Connected account does not have sufficient fund to cover the refund");
            $this->response($this->json($error), 200);
        }
        
        if(count($event_array)>0){
            for($i=0; $i<count($event_array); $i++){
                if($event_array[$i]['state']=='U'){
                    if(isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
                        $parent_qty += $event_array[$i]['cancelled_quantity'];
                    }
                    if($event_array[$i]['quantity']==0){
                        if($event_array[$i]['discounted_price']==0 && $payment_type=='F' && count($event_array)==1){
                            $update_zero_cost_payment_record = 1;
                        }
                    }
                }
            }
        }

        if($cancel_flag=='WOR'){
            if(count($plan_array)>0){
                for($j=0; $j<count($plan_array);$j++){
                    if(isset($plan_array[$j]['event_payment_id'])&&!empty($plan_array[$j]['event_payment_id'])&&$plan_array[$j]['edit_status']=='D'){
                        $update_plan = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                        $result_update = mysqli_query($this->db, $update_plan);
                        if(!$result_update){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "4- $update_plan");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }

                        $curr_date_time = gmdate("Y-m-d H:i:s");
                        $activity_text = "Cancelled payment $wp_currency_symbol".$plan_array[$j]['amount']."($stripe_card_name)";

                        $insert_payment_history1 = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $plan_array[$j]['event_payment_id'], 'refund', mysqli_real_escape_string($this->db, $activity_text), $curr_date_time);
                        $result_insert_payment_history1 = mysqli_query($this->db, $insert_payment_history1);
                        if (!$result_insert_payment_history1) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $insert_payment_history1");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }
        }elseif($cancel_flag=='WR' || $cancel_flag=='C' || $cancel_flag=='NP'){
            if(count($plan_array)>0){
                for($a=0; $a<count($plan_array);$a++){
                     if(isset($plan_array[$a]['event_payment_id'])&&!empty($plan_array[$a]['event_payment_id'])&&$plan_array[$a]['status']!='M'){
                        $payment_id_arr[]  = $plan_array[$a]['event_payment_id'];
                     }
                }
                $payment_id_arr_string = implode(',',$payment_id_arr);
                $get_payment_query1 = sprintf("SELECT `schedule_status`,`checkout_status`                    
                                FROM `event_payment` WHERE `event_payment_id` in ($payment_id_arr_string)");
                $result_payment_query1 = mysqli_query($this->db,$get_payment_query1);
                if(!$result_payment_query1){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $get_payment_query1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    while ($rows = mysqli_fetch_assoc($result_payment_query1)) {
                        $payment_status_check = $rows['schedule_status'];
                        $checkout_status_check = $rows['checkout_status'];
                        if ($payment_status_check == 'S' && $checkout_status_check == 'requires_action') {
                            $msg = array("status" => "Failed", "msg" => "Please authenticate your payments to proceed refunds.");
                            $this->response($this->json($msg), 200);
                        }
                    }
                }

                for($j = count($plan_array)-1; $j>=0; $j--){
                    $make_new_cancel_payment_record = 0;
                    $rfee = $trfee = $payment_intent_id = 0;
                    $payment_amount = $plan_array[$j]['amount'];
                    $p_f = $this->getProcessingFee($company_id,$upgrade_status);
                    $process_fee_per = $p_f['PP'];
                    $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);
                    if(isset($plan_array[$j]['event_payment_id'])&&!empty($plan_array[$j]['event_payment_id'])&&$plan_array[$j]['status']!='M'){
    //                $get_payment_query = sprintf("SELECT `checkout_id`,`checkout_status`, `refunded_amount`, `processing_fee` FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                        $get_payment_query = sprintf("SELECT `payment_intent_id`,`checkout_status`, `refunded_amount`, `processing_fee`, `credit_method`,
                                (select sum(processing_fee) from `event_payment` ep2 where ep2.`event_reg_id`='%s' AND ep2.`payment_intent_id`=ep1.`payment_intent_id` AND ep1.`payment_intent_id`!='' AND `event_payment_id`!='%s') refunded_processing_fee ,`last_updt_dt`
                                FROM `event_payment` ep1 WHERE `event_payment_id`='%s' ", mysqli_real_escape_string($this->db,$event_reg_id), mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']), mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                        $result_payment_query = mysqli_query($this->db,$get_payment_query);
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $get_payment_query");
                        log_info($this->json($error_log));
                        if(!$result_payment_query){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $get_payment_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else{
                            $row_res = mysqli_fetch_assoc($result_payment_query);
                            $payment_id = $payment_intent_id = $row_res['payment_intent_id'];
                            $payment_status = $row_res['checkout_status'];
                            $db_refunded_amt = $row_res['refunded_amount'];
                            $db_processing_fee = $row_res['processing_fee'];
                            $db_refunded_processing_fee = $row_res['refunded_processing_fee'];
                            $old_last_updt_dt = $row_res['last_updt_dt'];
                            $stop_stripe = false;
                        }
                        
                        $temp_payment_amount = $plan_array[$j]['amount']-$db_refunded_amt;
                        $fee = $db_processing_fee;
                        if(isset($plan_array[$j]['to_refund']) && $plan_array[$j]['to_refund']>0){
                            $temp_amount = $temp_payment_amount - $plan_array[$j]['to_refund'];
                            if($temp_amount>0){
                                $r1_fee = $temp_amount*($process_fee_per/100) + $process_fee_val;//doubt
                                $r2_fee = ($temp_amount + $r1_fee)*($process_fee_per/100) + $process_fee_val;
                                if($processing_fee_type==1){
                                    $trfee = number_format((float)($r1_fee), 2, '.', '');
                                }else{
                                    $trfee = number_format((float)($r2_fee), 2, '.', '');
                                }
                                $rfee = $fee - $trfee;
                            }else{
                                $rfee = $fee;
                                $trfee = $fee;
                            }
                            if($plan_array[$j]['credit_method']=='CC'){
                                if($plan_array[$j]['to_refund']==$plan_array[$j]['amount']){
                                    if($processing_fee_type==1){
                                        $parent_net_sales += $plan_array[$j]['to_refund']-$trfee;
                                        $event_refund_sales += $plan_array[$j]['to_refund']-$trfee;
                                    }else{
                                        $parent_net_sales += $plan_array[$j]['to_refund'];
                                        $event_refund_sales += $plan_array[$j]['to_refund'];
                                    }
                                }else{
                                    if($processing_fee_type==1){
                                        $parent_net_sales += $plan_array[$j]['to_refund']-$rfee;
                                        $event_refund_sales += $plan_array[$j]['to_refund']-$rfee;
                                    }else{
                                        $parent_net_sales += $plan_array[$j]['to_refund'];
                                        $event_refund_sales += $plan_array[$j]['to_refund'];
                                    }
                                }
                            }else{
                                $parent_net_sales += $plan_array[$j]['to_refund'];
                                $event_refund_sales += $plan_array[$j]['to_refund'];
                            }
                            $update_refund_sales_flag = 1;
                        }
                    }else{
                        $stop_stripe = false;
                        if($plan_array[$j]['status']=='M'){
                            $stop_stripe = true;
                            $manual_refund_amount += $plan_array[$j]['to_refund'];
                        }
                        $p1_fee = $payment_amount*($process_fee_per/100) + $process_fee_val;
                        $p2_fee = ($payment_amount + $p1_fee)*($process_fee_per/100) + $process_fee_val;
                        if($processing_fee_type==1){
                            $fee = number_format((float)($p1_fee), 2, '.', '');
                        }else{
                            $fee = number_format((float)($p2_fee), 2, '.', '');
                        }
                    }
                    if($update_zero_cost_payment_record == 1 && count($plan_array)==1 && $payment_amount==0){
                        $update_plan = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                        $result_update = mysqli_query($this->db, $update_plan);
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $update_plan");
                        log_info($this->json($error_log));
                        if(!$result_update){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $update_plan");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }

                    if($plan_array[$j]['edit_status']=='D'){
                        $update_plan = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                        $result_update = mysqli_query($this->db, $update_plan);
                        if(!$result_update){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "4- $update_plan");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                        
                        $curr_date_time = gmdate("Y-m-d H:i:s");
                        $activity_text = "Cancelled payment $wp_currency_symbol".$plan_array[$j]['amount']."($stripe_card_name)";
                
                        $insert_payment_history1 = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $plan_array[$j]['event_payment_id'], 'refund', mysqli_real_escape_string($this->db, $activity_text), $curr_date_time);
                        $result_insert_payment_history1 = mysqli_query($this->db, $insert_payment_history1);
                        if (!$result_insert_payment_history1) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $insert_payment_history1");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }elseif($plan_array[$j]['edit_status']=='U'){
                        if(isset($plan_array[$j]['to_refund']) && $plan_array[$j]['to_refund']>0){

                            if($plan_array[$j]['to_refund']==$plan_array[$j]['amount']){
                                if(empty(trim($payment_id)) && empty(trim($payment_status))){
                                    $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                    $result_update_query2 = mysqli_query($this->db, $update_query2);
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "5- $update_query2");
                                    log_info($this->json($error_log));
                                    if(!$result_update_query2){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "5- $update_query2");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    }
                                }else{
                                    if(!$stop_stripe){
                                        if($payment_status=='released'){
                                            
                                        if($plan_array[$j]['credit_method'] == 'CC' && !empty($payment_intent_id)){ // for retrieve charge id
                                            $charge_retrieve_arr = $this->stripeChargeIdRetrieve($payment_intent_id);
                                            $charge_array_initial = json_decode(json_encode($charge_retrieve_arr),true);
                                            $charge_array = $charge_array_initial['charges']['data'];
                                            $charge_id = $charge_array[0]['id'];
                                        }
                                        if($plan_array[$j]['credit_method'] == 'CC' && !empty($charge_id)){ // for refund
                                            try {
                                                $create_refund = \Stripe\Refund::create([
                                                    "charge" => $charge_id,
                                                    "reverse_transfer" => true]);
                                            } catch (\Stripe\Error\Card $e) {
                                                // Since it's a decline, \Stripe\Error\Card will be caught       
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (\Stripe\Error\RateLimit $e) {
                                                // Too many requests made to the API too quickly      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (\Stripe\Error\InvalidRequest $e) {
                                                // Invalid parameters were supplied to Stripe's API      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (\Stripe\Error\Authentication $e) {
                                                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (\Stripe\Error\ApiConnection $e) {
                                                // Network communication with Stripe failed      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (\Stripe\Error\Base $e) {
                                                // Display a very generic error to the user, and maybe send yourself an email      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (Exception $e) {
                                                // Something else happened, completely unrelated to Stripe      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            }
                                            if (isset($err) && !empty($err)) {
                                                $error = array('status' => "Failed", "msg" => $err['message']);
                                                $this->response($this->json($error), 200);
                                            }
                                            $retail_refund_arr = json_decode(json_encode($create_refund),true);
                                            if ($retail_refund_arr['status'] == 'succeeded'){

                                            }else{
                                                $error = array("status" => "Failed", "msg" => "Can't able to refund amount.");
                                                $this->response($this->json($error), 200);
                                            }
                                        }
                                        }else{
                                            $error = array("status" => "Failed", "msg" => "Participant payment status is in $payment_status status.");
                                            $this->response($this->json($error),200);
                                        }

                                    }
//                                    if((isset($response['status']) && $response['status']=="Success") || $stop_wepay){
                                        $total_refund_amount += $plan_array[$j]['to_refund'];
                                        $paid_amount -= $payment_amount;
            //                            $res = $response['msg'];
                                 //       $state = $response['msg']['state'];
                                        
                                        $insert_new_refunded_query = sprintf("INSERT INTO `event_payment`( `event_reg_id`, `student_id`, `payment_intent_id`,`payment_from`, `checkout_status`, `payment_amount`, `processing_fee`, `schedule_date`, `schedule_status`, `payment_method_id`, `stripe_card_name`, `created_dt`, `last_updt_dt`) 
                                                    SELECT `event_reg_id`, `student_id`, `payment_intent_id`, `payment_from`, `checkout_status`, `payment_amount`, `processing_fee`, `schedule_date`, 'FR', `payment_method_id`, `stripe_card_name`, `created_dt`, '$old_last_updt_dt' FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                        $result_insert_new_refunded_query = mysqli_query($this->db, $insert_new_refunded_query);
                                        if (!$result_insert_new_refunded_query) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $insert_new_refunded_query");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 200);
                                        }
                                        
                                         $status = "R";
                                            $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`='%s', `refunded_amount`='%s', `refunded_date`='$curr_date' WHERE `event_payment_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $plan_array[$j]['to_refund']), mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));

                                        $result_update_query2 = mysqli_query($this->db, $update_query2);
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "6- $update_query2");
                                        log_info($this->json($error_log));
                                        if(!$result_update_query2){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "6- $update_query2");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 200);
                                        }else{
                                            $ref_amount=0;
                                            if ($processing_fee_type == 1) {
                                                $ref_amount = $plan_array[$j]['to_refund'];
//                                                 $ref_amount = $plan_array[$j]['to_refund'];
                                            } else {
                                                $ref_amount = $plan_array[$j]['to_refund']+ $db_processing_fee;
                                            }
                                            
                                            $curr_date_time = gmdate("Y-m-d H:i:s");
                                            $activity_text = "Refunded $wp_currency_symbol".$ref_amount." ($stripe_card_name)";
                
                                            $insert_payment_history2 = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $plan_array[$j]['event_payment_id'], 'refund', mysqli_real_escape_string($this->db, $activity_text), $curr_date_time);
                                            $result_insert_payment_history2 = mysqli_query($this->db, $insert_payment_history2);
                                            if (!$result_insert_payment_history2) {
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $insert_payment_history2");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            }
                                        }
//                                    }else{
//                                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                                        $this->response($this->json($log),200);
//                                    }
                                }

                            }elseif($plan_array[$j]['to_refund']<$plan_array[$j]['amount']){

                                if(empty(trim($payment_id)) && empty(trim($payment_status))){
                                    $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`='C' WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                    $result_update_query2 = mysqli_query($this->db, $update_query2);
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "7- $update_query2");
                                    log_info($this->json($error_log));
                                    if(!$result_update_query2){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "7- $update_query2");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    }
                                }else{
                                    $make_new_cancel_payment_record = 1;
                                    $payment_refunded = 0;
                                    $refund = $plan_array[$j]['to_refund'];
                                    $temp_payment_amount = $plan_array[$j]['amount']-$db_refunded_amt;
                                    $fee = $db_processing_fee;
                                    if(isset($plan_array[$j]['to_refund']) && $plan_array[$j]['to_refund']>0){
                                        $temp_amount = $temp_payment_amount - $plan_array[$j]['to_refund'];
                                        if($temp_amount>0){
                                            $r1_fee = $temp_amount*($process_fee_per/100) + $process_fee_val;
                                            $r2_fee = ($temp_amount + $r1_fee)*($process_fee_per/100) + $process_fee_val;
                                            if($processing_fee_type==1){
                                                $trfee = number_format((float)($r1_fee), 2, '.', '');
                                            }else{
                                                $trfee = number_format((float)($r2_fee), 2, '.', '');
                                            }
                                            $rfee = $fee - $trfee;
                                        }else{
                                            $rfee = $fee;
                                        }
                                    }
                                    $r_amt = $stripe_refund_check = 0;
                                    if($processing_fee_type==1){
                                        $r_amt = $refund;
                                        $stripe_refund_check = $temp_payment_amount;
                                    }else{
                                        $r_amt = $refund+$rfee;
                                        $stripe_refund_check = $temp_payment_amount+$fee;
                                    }
                                    if(!$stop_stripe){
                                        if($payment_status=='released'){
                                        if($plan_array[$j]['credit_method'] == 'CC' && !empty($payment_intent_id)){ // for retrieve charge id
                                            $charge_retrieve_arr = $this->stripeChargeIdRetrieve($payment_intent_id);
                                            $charge_array_initial = json_decode(json_encode($charge_retrieve_arr),true);
                                            $charge_array = $charge_array_initial['charges']['data'];
                                            $charge_id = $charge_array[0]['id'];
                                        }
                                        if($plan_array[$j]['credit_method'] == 'CC' && !empty($charge_id)){ // for refund
                                            try {
                                                $round_refund_amount = $r_amt * 100;
                                                $create_refund = \Stripe\Refund::create([
                                                    "charge" => $charge_id,
                                                    "amount" => $round_refund_amount,
                                                    "reverse_transfer" => true]);
                                            } catch (\Stripe\Error\Card $e) {
                                                // Since it's a decline, \Stripe\Error\Card will be caught       
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (\Stripe\Error\RateLimit $e) {
                                                // Too many requests made to the API too quickly      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (\Stripe\Error\InvalidRequest $e) {
                                                // Invalid parameters were supplied to Stripe's API      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (\Stripe\Error\Authentication $e) {
                                                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (\Stripe\Error\ApiConnection $e) {
                                                // Network communication with Stripe failed      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (\Stripe\Error\Base $e) {
                                                // Display a very generic error to the user, and maybe send yourself an email      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            } catch (Exception $e) {
                                                // Something else happened, completely unrelated to Stripe      
                                                $body = $e->getJsonBody();
                                                $err = $body['error'];
                                                $err['status_code'] = $e->getHttpStatus();
                                            }
                                            if (isset($err) && !empty($err)) {
                                                $error = array('status' => "Failed", "msg" => $err['message']);
                                                $this->response($this->json($error), 200);
                                            }
                                            $retail_refund_arr = json_decode(json_encode($create_refund),true);
                                            if ($retail_refund_arr['status'] == 'succeeded'){

                                            }else{
                                                $error = array("status" => "Failed", "msg" => "Can't able to refund amount.");
                                                $this->response($this->json($error), 200);
                                            }
                                        }
                                            $paid_amount -= $refund;
                                        }else{
                                            $error = array("status" => "Failed", "msg" => "Participant payment status is in $payment_status status.");
                                            $this->response($this->json($error),200);
                                        }

                                    }
            //                            $res = $response['msg'];

                                       // $json2 = array("checkout_id"=>"$payment_id");

                                        $total_refund_amount += $plan_array[$j]['to_refund'];
                                        if($retail_refund_arr['amount']>0){
                                            if($processing_fee_type==1){
                                                $payment_refunded = ($retail_refund_arr['amount']/100);
                                            }else{
                                                $payment_refunded = ($retail_refund_arr['amount']/100)-($db_refunded_processing_fee+$rfee);
                                            }
                                        }
                                        
                                        if($retail_refund_arr['amount'] == $stripe_refund_check){ // validate for full refund
                                            $status = "R";
                                            $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `paymentt_intent_id`,`payment_from`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `refunded_date`, `processing_fee`, `payment_method_id`, `stripe_card_name`)
                                                    SELECT `event_reg_id`, `student_id`, `payment_intent_id`,`payment_from`, 'refunded', $refund, `schedule_date`, 'R', '$curr_date', $rfee, '$payment_method_id', '$stripe_card_name'  FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                            $result_insert_query = mysqli_query($this->db, $insert_payment);
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "9- $insert_payment");
                                            log_info($this->json($error_log));
                                            if(!$result_insert_query){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "9- $insert_payment");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            }
                                        }elseif($payment_refunded>0){
                                            $status = "PR";
                                            $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `payment_intent_id`,`payment_from`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `refunded_date`, `processing_fee`, `payment_method_id`, `stripe_card_name`)
                                                    SELECT `event_reg_id`, `student_id`, `payment_intent_id`,`payment_from`, 'refunded', $refund, `schedule_date`, 'R', '$curr_date', $rfee, '$payment_method_id', '$stripe_card_name'  FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                            $result_insert_query = mysqli_query($this->db, $insert_payment);
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "10- $insert_payment");
                                            log_info($this->json($error_log));
                                            if(!$result_insert_query){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "10- $insert_payment");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            }
                                        }

                                        $update_query2 = sprintf("UPDATE `event_payment` SET `schedule_status`=IF($temp_amount=0,'CR','%s'), `refunded_amount`=$payment_refunded, `refunded_date`='$curr_date', `processing_fee`=IF($temp_amount=0,0,'$trfee') WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                        $result_update_query2 = mysqli_query($this->db, $update_query2);
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "11- $update_query2");
                                        log_info($this->json($error_log));
                                        if(!$result_update_query2){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "11- $update_query2");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 200);
                                        }
                                        if ($processing_fee_type == 1) {
                                                $ref_amount1 = $plan_array[$j]['to_refund'];
                                            } else {
                                                $ref_amount1 = $plan_array[$j]['to_refund']+ $rfee;
                                            }
                                            $curr_date_time = gmdate("Y-m-d H:i:s");
                                            $activity_text = "Refunded $wp_currency_symbol".$ref_amount1." ($stripe_card_name)";
                
                                            $insert_payment_history2 = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $plan_array[$j]['event_payment_id'], 'refund', mysqli_real_escape_string($this->db, $activity_text), $curr_date_time);
                                            $result_insert_payment_history2 = mysqli_query($this->db, $insert_payment_history2);
                                            if (!$result_insert_payment_history2) {
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $insert_payment_history2");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            }
                                   
                                }
                            }
                        }else{
                            if(isset($plan_array[$j]['event_payment_id']) && !empty($plan_array[$j]['event_payment_id'])){
                                $update_plan = sprintf("UPDATE `event_payment` SET `payment_amount`='%s', `schedule_status`='%s', `processing_fee`='%s' WHERE `event_payment_id`='%s'", 
                                        mysqli_real_escape_string($this->db,$plan_array[$j]['amount']), mysqli_real_escape_string($this->db,$plan_array[$j]['status']), mysqli_real_escape_string($this->db,$plan_array[$j]['process_fee']), mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                                $result_update = mysqli_query($this->db, $update_plan);
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "12- $update_plan");
                                log_info($this->json($error_log));
                                if(!$result_update){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "12- $update_plan");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
                    }elseif($plan_array[$j]['edit_status']=='N' && empty($plan_array[$j]['event_payment_id']) && $plan_array[$j]['status']=='N'){
                        $checkout_id = $checkout_state = '';
                        if($payment_amount>0){
                            if((isset($plan_array[$j]['credit_method'])&&$plan_array[$j]['credit_method']=='CC') || !isset($plan_array[$j]['credit_method'])){
                                if($processing_fee_type==1){
                                    $event_refund_sales -= ($payment_amount-$fee);
                                    $parent_net_sales -= ($payment_amount-$fee);
                                }else{
                                    $event_refund_sales -= $payment_amount;
                                    $parent_net_sales -= $payment_amount;
                                }
                                
                                if ($processing_fee_type == 2) {
                                    $amount_in_dollars = $payment_amount + $fee;
                                    $dest_amount_in_dollars = $payment_amount;
                                } elseif ($processing_fee_type == 1) {
                                    $amount_in_dollars = $payment_amount;
                                    $dest_amount_in_dollars = $payment_amount - $fee;
                                }
                                $amount_in_dollars *= 100;
                                $dest_amount_in_dollars *= 100;
                                
                                $p_type = 'event';
                                $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $p_type,'');
                                if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                                    $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                    $checkout_state = 'released';
                                    $update_event_remaining_dues = 1;
                                } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {// doubt
                                    $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                    $checkout_state = 'requires_action';
                                    $has_3D_secure = 'Y';
                                    $next_action_type = $payment_intent_result['next_action']['type'];
                                    if($next_action_type != "redirect_to_url"){
                                        # Payment not supported
                                        $out['status'] = 'Failed';
                                        $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                                        $this->response($this->json($out), 200);
                                    }
                                    $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                                } else {
                                    # Invalid status
                                    $out['status'] = 'Failed';
                                    $out['msg'] = 'Invalid SetupIntent status.';
                                    $out['error'] = 'Invalid SetupIntent status';
                                    $this->response($this->json($out), 200);
                                }
                                
                                
                             $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `payment_intent_id`,`payment_from`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `processing_fee`, `payment_method_id`, `stripe_card_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db,$event_reg_id), mysqli_real_escape_string($this->db,$student_id), mysqli_real_escape_string($this->db,$payment_intent_id),'S', mysqli_real_escape_string($this->db,$checkout_state), mysqli_real_escape_string($this->db,$payment_amount),
                                        mysqli_real_escape_string($this->db,$curr_date), mysqli_real_escape_string($this->db,'S'), mysqli_real_escape_string($this->db,$fee), mysqli_real_escape_string($this->db,$payment_method_id), mysqli_real_escape_string($this->db,$stripe_card_name));
                                $result_insert = mysqli_query($this->db, $insert_payment);
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "13- $insert_payment");
                                log_info($this->json($error_log));
                                if (!$result_insert) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "13- $insert_payment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }else{
                                $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `payment_amount`, `schedule_date`, `schedule_status`, `credit_method`, `payment_from`) VALUES ('%s','%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db,$event_reg_id), mysqli_real_escape_string($this->db,$student_id), mysqli_real_escape_string($this->db,$payment_amount),
                                        mysqli_real_escape_string($this->db,$curr_date), mysqli_real_escape_string($this->db,'M'), mysqli_real_escape_string($this->db,$plan_array[$j]['credit_method']),'S');
                                $result_insert = mysqli_query($this->db, $insert_payment);
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "13- $insert_payment");
                                log_info($this->json($error_log));
                                if (!$result_insert) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "13- $insert_payment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                                $update_event_remaining_dues = 1;
                                $parent_net_sales -= $payment_amount;
                                $event_refund_sales -= $payment_amount;
                                $paid_amount += $payment_amount;
                                $update_event_amount = $payment_amount;
                            }
                        }
                    }elseif($plan_array[$j]['edit_status']=='MU'){
                        if(isset($plan_array[$j]['to_refund']) && $plan_array[$j]['to_refund']>0){
                            $credit_method_2 = $plan_array[$j]['credit_method'];
                            $method_str = "";
                            if($credit_method_2=='CA'){
                                $method_str = " (Cash)";
                            }elseif($credit_method_2=='CH'){
                                $method_str = " (Check)";
                            }elseif($credit_method_2=='MC'){
                                $method_str = " (Credit)";
                            }
                            $update_event_remaining_dues = 1;
                            $update_refund_sales_flag = 1;
                            $m_refund = $plan_array[$j]['to_refund'];
                            $total_refund_amount += $m_refund;
                            $paid_amount -= $m_refund;
                            $parent_net_sales += $plan_array[$j]['to_refund'];
                            $event_refund_sales += $plan_array[$j]['to_refund'];
                            if($plan_array[$j]['to_refund']==$plan_array[$j]['amount']){
                                $s_status = 'MF';
                                $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `payment_intent_id`,`payment_from`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `refunded_date`, `credit_method`, `check_number`)
                                        SELECT `event_reg_id`, `student_id`, `payment_intent_id`,`S`, 'manual refunded', $m_refund, `schedule_date`, 'MR', '$curr_date', `credit_method`, `check_number` FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                $result_insert_query = mysqli_query($this->db, $insert_payment);
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "14- $insert_payment");
                                log_info($this->json($error_log));
                                if(!$result_insert_query){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "14- $insert_payment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }else{
                                $s_status = 'MP';
                                $insert_payment = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `payment_intent_id`,`payment_from`, `checkout_status`, `payment_amount`, `schedule_date`, `schedule_status`, `refunded_date`, `credit_method`, `check_number`)
                                        SELECT `event_reg_id`, `student_id`, `payment_intent_id`,'S', 'manual refunded', $m_refund, `schedule_date`, 'MR', '$curr_date', `credit_method`, `check_number` FROM `event_payment` WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db, $plan_array[$j]['event_payment_id']));
                                $result_insert_query = mysqli_query($this->db, $insert_payment);
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "14- $insert_payment");
                                log_info($this->json($error_log));
                                if(!$result_insert_query){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "14- $insert_payment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                            $update_plan = sprintf("UPDATE `event_payment` SET `schedule_status`='$s_status', `refunded_amount`=`refunded_amount`+$m_refund WHERE `event_payment_id`='%s'", mysqli_real_escape_string($this->db,$plan_array[$j]['event_payment_id']));
                            $result_update = mysqli_query($this->db, $update_plan);
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "15- $update_plan");
                            log_info($this->json($error_log));
                            if(!$result_update){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "15- $update_plan");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                            
                            $activity_text = "Refunded $wp_currency_symbol".$m_refund.$method_str;
                            $insert_payment_history2 = sprintf("INSERT INTO `event_payment_history`(`company_id`, `event_reg_id`, `event_payment_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s',NOW())",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), $plan_array[$j]['event_payment_id'], 'refund', mysqli_real_escape_string($this->db, $activity_text));
                            $result_insert_payment_history2 = mysqli_query($this->db, $insert_payment_history2);
                            if (!$result_insert_payment_history2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "15- $insert_payment_history2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                    }
                }
            }
        }
        if(count($event_array)>0){
            $update_event_amount2 = $update_event_amount;
            for($i=0; $i<count($event_array); $i++){
                if($event_array[$i]['state']=='U'){
                    $c_qty = $make_new_cancel_record = $old_qty = 0;
                    if(isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
                        $c_qty = $event_array[$i]['cancelled_quantity'];
                    }
                    if($event_array[$i]['quantity']==0){
                        $old_qty = 0;
                        if($event_array[$i]['discounted_price']==0 && $payment_type=='F'){
                            $update_zero_cost_payment_record = 1;
                        }
                        $status = 'CP';
                        $event_array[$i]['payment_amount'] = $event_array[$i]['discounted_price']*$c_qty;
                        $event_array[$i]['quantity']=$c_qty;
                    }else{
                        $old_qty = $event_array[$i]['quantity']+$event_array[$i]['cancelled_quantity'];
                        if($c_qty>0){
                            $make_new_cancel_record = 1;
                        }
                        if($event_array[$i]['payment_amount']>0){
                            if($event_array[$i]['paid_due']==$event_array[$i]['payment_amount']){
                                if($payment_type=='CO'){
                                    $status = 'CC';
                                }else{
                                    $status = 'RC';
                                }
                            }else{
                                if($event_array[$i]['rem_due']>0){
                                    $status = 'RP';
                                }else{
                                    if($payment_type=='CO'){
                                        $status = 'CC';
                                    }else{
                                        $status = 'RC';
                                    }
                                }
                            }
                        }else{
                            $status = 'CC';
                        }
                    }
                    
                    if($make_new_cancel_record==1){
                        $insert_event = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`) 
                                  SELECT `event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, (`event_cost`-`discount`)*$c_qty, $c_qty, IF(`payment_status`='CC' || `payment_status`='RC', IF(`payment_amount`>0,'RF','CP'), 'CP') FROM `event_reg_details` WHERE `event_reg_details_id`='%s'",
                            mysqli_real_escape_string($this->db,$event_array[$i]['event_reg_details_id']));
                        $result_insert_event = mysqli_query($this->db, $insert_event);
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "16- $insert_event");
                        log_info($this->json($error_log));
                        if(!$result_insert_event){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "16- $insert_event");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    $temp_rem_due = $event_array[$i]['rem_due'];
                    if($update_event_remaining_dues==1 && $update_event_amount>0 && $old_qty>0){
                        if($temp_rem_due>$update_event_amount){
                            $temp_rem_due = $temp_rem_due-$update_event_amount;
                            $update_event_amount=0;
                            $status = 'RP';
                        }else{
                            $update_event_amount = $update_event_amount-$temp_rem_due;
                            $temp_rem_due=0;
                            $status = 'RC';
                        }
                    }
                    log_info(json_encode($event_array[$i]));
                    $man_calc = $event_array[$i]['manual_payment_amount']-$event_array[$i]['old_manual_payment_amount'];
                    $update_event = sprintf("UPDATE `event_reg_details` SET `payment_amount`='%s',`manual_payment_amount`=`manual_payment_amount`+'%s',`quantity`='%s',`balance_due`='%s',`payment_status`=IF('$status'!='CP', '%s', IF(`payment_status`='CC' || `payment_status`='RC', IF(`payment_amount`>0,'RF','CP'), 'CP')) WHERE `event_reg_details_id`='%s'",
                        mysqli_real_escape_string($this->db,$event_array[$i]['payment_amount']), mysqli_real_escape_string($this->db,$man_calc), mysqli_real_escape_string($this->db,$event_array[$i]['quantity']), 
                        mysqli_real_escape_string($this->db,$temp_rem_due), mysqli_real_escape_string($this->db,$status), mysqli_real_escape_string($this->db,$event_array[$i]['event_reg_details_id']));
                    $result_update_event = mysqli_query($this->db, $update_event);
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "17- $update_event");
                    log_info($this->json($error_log));
                    if(!$result_update_event){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "17- $update_event");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        $parent_id = 0;
        if(count($event_array)>0){
            $child_rem_manual_amt = 0;
            $temp_manual_refund_amount=$manual_refund_amount;
            for($i=0; $i<count($event_array); $i++){
                $current_rem_val = 0;
                if($event_array[$i]['quantity']==0){
                    $old_qty = 0;
                }else{
                    $old_qty = $event_array[$i]['quantity']+$event_array[$i]['cancelled_quantity'];
                }
                $temp_rem_due = $event_array[$i]['rem_due'];
                if($update_event_remaining_dues==1 && $update_event_amount2>0 && $old_qty>0){
                    if($temp_rem_due>$update_event_amount2){
                        $current_rem_val = $temp_rem_due-$update_event_amount2;
                    }else{
                        $update_event_amount2 = $update_event_amount2-$temp_rem_due;
                        $current_rem_val = $temp_rem_due;
                    }
                }
                if($event_array[$i]['state']=='U' && isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
                    $parent_str = $parent_sales_str = "";
                    $net_sales = 0;
                    $c_qty = $event_array[$i]['cancelled_quantity'];
                    $event_id = $event_array[$i]['ev_id'];
                    $event_list = "$event_id";
                    
                    if($update_refund_sales_flag == 1){
                        if($event_refund_sales>0){
                            $temp_event_amount = $event_array[$i]['cancelled_quantity']*$event_array[$i]['discounted_price'];
                            if($event_refund_sales>$temp_event_amount){
                                $net_sales = $temp_event_amount;
                                $event_refund_sales = $event_refund_sales - $temp_event_amount;
                                $temp_event_amount = 0;
                            }else{
                                $net_sales = $event_refund_sales;
                                $temp_event_amount = $temp_event_amount - $event_refund_sales;
                                $event_refund_sales = 0;
                            }
                        }else{
                            $net_sales = $event_refund_sales;
                            $event_refund_sales = 0;
                        }
                    }
                    
                    if(isset($event_array[$i]['event_parent_id']) && !empty($event_array[$i]['event_parent_id'])){
                        $parent_id = $event_array[$i]['event_parent_id'];
//                        $event_list .= ", $parent_id";
//                        $parent_str = "WHEN '$parent_id' THEN  `registrations_count`-$c_qty ";
//                        $parent_sales_str = "WHEN '$parent_id' THEN  `net_sales`-$parent_net_sales ";
                    }
                    if(count($event_array)==1){
                        $update_event_count = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`-$c_qty, `net_sales` = IF('$cancel_flag'='WR' || '$cancel_flag'='C' || '$cancel_flag'='NP', `net_sales`-$net_sales, `net_sales`)
                                WHERE `event_id` IN (%s)", $event_list);
                        $result_event_count = mysqli_query($this->db, $update_event_count);                    
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "18- $update_event_count");
                        log_info($this->json($error_log));
                        if(!$result_event_count){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "18- $update_event_count");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }else{
                        $child_rem_due = ($event_array[$i]['old_paid_due'] - $event_array[$i]['paid_due']) - ($event_array[$i]['old_manual_payment_amount']-$event_array[$i]['manual_payment_amount']);
                        $update_event_count = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`-$c_qty, `net_sales` = IF('$cancel_flag'='WR' || '$cancel_flag'='C' || '$cancel_flag'='NP', `net_sales`-$child_rem_due+$current_rem_val, `net_sales`)
                                WHERE `event_id` IN (%s)", $event_list);
                        $result_event_count = mysqli_query($this->db, $update_event_count);                    
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "19- $update_event_count");
                        log_info($this->json($error_log));
                        if(!$result_event_count){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "19- $update_event_count");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }else{
//                    if($event_array[$i]['old_paid_due'] != $event_array[$i]['paid_due'] || $event_array[$i]['old_manual_payment_amount'] != $event_array[$i]['manual_payment_amount']){
                    $child_rem_due = ($event_array[$i]['old_paid_due'] - $event_array[$i]['paid_due']) - ($event_array[$i]['old_manual_payment_amount']-$event_array[$i]['manual_payment_amount']);
                    $update_event_count = sprintf("UPDATE `event` SET `net_sales` = IF('$cancel_flag'='WR' || '$cancel_flag'='C' || '$cancel_flag'='NP', `net_sales`-$child_rem_due+$current_rem_val, `net_sales`)
                            WHERE `event_id` IN (%s)", $event_array[$i]['ev_id']);
                    $result_event_count = mysqli_query($this->db, $update_event_count);                    
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "20- $update_event_count");
                    log_info($this->json($error_log));
                    if(!$result_event_count){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "20- $update_event_count");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        if($parent_id!=0){
            $update_event_count = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`-$parent_qty,`net_sales` = IF('$cancel_flag'='WR' || '$cancel_flag'='C' || '$cancel_flag'='NP', `net_sales`-$parent_net_sales, `net_sales`)
                            WHERE `event_id` IN (%s)", $parent_id);
            $result_event_count = mysqli_query($this->db, $update_event_count);                    
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "21- $update_event_count");
            log_info($this->json($error_log));
            if(!$result_event_count){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "21- $update_event_count");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        if($total_due>0){
//            $paid_text = $refund_text = "";
//            if($paid_amount>0){
                if($payment_type=='CO'){
                    $paid_text = ", `paid_amount`=$total_due";
                }else{
                    $paid_text = ", `paid_amount`=`paid_amount`+ $paid_amount";
                }
                    $refund_text = ", `refunded_amount` = `refunded_amount` + $total_refund_amount";
//                }
//            }
            $update_reg = sprintf("UPDATE `event_registration` SET `payment_amount`='%s' %s %s WHERE `event_reg_id`='%s'", mysqli_real_escape_string($this->db,$total_due), $paid_text, $refund_text, mysqli_real_escape_string($this->db,$event_reg_id));
            $result_update_reg = mysqli_query($this->db, $update_reg);
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "22- $update_reg");
            log_info($this->json($error_log));
            if (!$result_update_reg) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "22- $update_reg");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        if(count($event_array)>0){
            $parent_title = '';
            $curr_date = gmdate("Y-m-d H:i:s");
            for($i=0; $i<count($event_array); $i++){
                if($i==0){
                    if(!empty($event_array[$i]['event_parent_id'])||$event_array[$i]['event_parent_id']!='null'){
                        $query_parent_title = sprintf("SELECT event_title FROM `event` WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_array[$i]['event_parent_id']));
                        $result_query_parent_title = mysqli_query($this->db, $query_parent_title);
                        if(!$result_query_parent_title){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else{
                            $num = mysqli_num_rows($result_query_parent_title);
                            if($num>0){
                                $row_title = mysqli_fetch_assoc($result_query_parent_title);
                                $parent_title = $row_title['event_title'];
                            }
                        }
                    }else{
                        $parent_title = $event_array[$i]['title'];
                    }
                }
                if($event_array[$i]['state']=='U'){
                    if(isset($event_array[$i]['cancelled_quantity']) && $event_array[$i]['cancelled_quantity']>0){
                        if($cancel_flag=='WOR' || $cancel_flag=='CP'){
                            $issue_refund = " (no refund issued)";
                        }else{
                            $issue_refund = " ";
                        }
                        if($payment_type!='F'){
                            $cancellation_text = "Total cancellation cost -$wp_currency_symbol".($event_array[$i]['cancelled_quantity']*$event_array[$i]['discounted_price']).$issue_refund;
                        }else{
                            $cancellation_text = '';
                        }
                        if(!empty($event_array[$i]['event_parent_id'])||$event_array[$i]['event_parent_id']!='null'){
                            $activity_text = "Cancelled $parent_title ".$event_array[$i]['title'].", quantity ".$event_array[$i]['cancelled_quantity'].". $cancellation_text";;
                        }else{
                            $activity_text = "Cancelled ".$event_array[$i]['title'].", quantity ".$event_array[$i]['cancelled_quantity'].". $cancellation_text";
                        }
                        $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", 
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id),
                                    'Cancellation', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                        $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                        if (!$result_insert_event_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                            log_info($this->json($error_log));
                        }
                    }
                }
            }
        }
        if(isset($event_id) && !empty($event_id)){
            $event_details = $this->getEventsDetailByEventId($company_id, $event_id, $event_type, 'live', 1);
            $reg_details = $this->getEventParticipantDetailsForRefund($company_id, $event_id, $event_reg_id, 1);                
            $msg = array("status" => "Success", "msg" => "Cancellation Successfully Processed.", "event_details"=>$event_details, "reg_details" => $reg_details);
        }else{
            $msg = array("status" => "Failed", "msg" => "No changes found.");
        }
        $this->response($this->json($msg), 200);
        date_default_timezone_set($curr_time_zone);
    }
    
    public function stripeChargeIdRetrieve($payment_intent_id) {
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        if (!empty($payment_intent_id)) {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $charge_id_retrieve = \Stripe\PaymentIntent::retrieve($payment_intent_id);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
            return $charge_id_retrieve;
        }
    }

}

?>