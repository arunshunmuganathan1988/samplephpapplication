<?php

        header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');

require_once '../../../Globals/cont_log.php';
require_once 'Salesforce.php';
require_once 'PaySimple.php';
require_once 'WePay.php';
require_once('../../PHPMailer_5.2.1/class.phpmailer.php');
require_once('../../PHPMailer_5.2.1/class.smtp.php');
require_once 'tfpdf.php';

class CurriculumModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $sf;
    private $ps;
    private $list_id = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_redirect_uri = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;

    public function __construct() {
        require_once '../../../Globals/config.php';
        $this->inputs();
        $this->sf = new Salesforce();
        $this->ps = new PaySimple();
        $this->wp = new wepay();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->list_id = $this->sf->list_id;
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = "../../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithPaysimple($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithSalesforce($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
        header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    public function insertCurriculum($company_id, $curr_name, $created_user_id) {

        $sql = sprintf("INSERT INTO `curriculum`(`company_id`, `curr_name`, `created_user_id`) VALUES ('%s','%s','%s')", $company_id, mysqli_real_escape_string($this->db,$curr_name), $created_user_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $curr_id = mysqli_insert_id($this->db);

            $sql_sort_order = sprintf("SELECT `curriculum_id` FROM `curriculum` WHERE company_id='%s'", $company_id);
            $result_sort_order = mysqli_query($this->db, $sql_sort_order);
            if (!$result_sort_order) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_sort_order");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $topic_srt_ord = 0;
                $num_of_rows = mysqli_num_rows($result_sort_order);
                if ($num_of_rows >= 0) {
                    $topic_srt_ord = $num_of_rows + 1;
                }
            }

            $this->insertCurriculumDetails($company_id, $curr_id, $curr_name, $topic_srt_ord);
        }
    }

    public function insertCurriculumDetails($company_id, $curr_id, $curr_name, $topic_srt_ord) {

        $sql = sprintf("INSERT INTO `curriculum_detail`(`curriculum_id`, `topic_srt_ord`, `topic_name`) VALUES ('%s','%s','%s')", $curr_id, $topic_srt_ord, mysqli_real_escape_string($this->db,$curr_name));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            //$out = array('status' => "Success", 'msg'=>"Curriculum details added successfully.");
            // If success everythig is good send header as "OK" and user details            
            //$this->response($this->json($out), 200);
            $this->getCurriculumDetailByCompany($company_id);
        }
    }

    public function insertCurriculumContentDetails($company_id,$created_user_id,$content_title,$content_description,$content_video_url,$content_video_type,$video_id,$curr_id,$curr_detail_id,$content_sort_order) {

        $sql = sprintf("INSERT INTO `content_definition`(`content_title`, `content_description`, `ContentVideoUrl`, `video_type`, `video_id`, `created_user_id`, `creator_company_id`) VALUES ('%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db,$content_title), mysqli_real_escape_string($this->db,$content_description), $content_video_url, $content_video_type, $video_id, $created_user_id, $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $content_id = mysqli_insert_id($this->db);
            $this->insertCurriculumContent($curr_id, $curr_detail_id, $content_sort_order, $content_id, $company_id);
        }
    }

    public function insertCurriculumContent($curr_id, $curr_detail_id, $content_sort_order, $content_id, $company_id) {
        $sql = sprintf("INSERT INTO `curriculum_content`(`curriculum_id`, `curriculum_det_id`, `content_srt_ord`, `contentId`) VALUES ('%s','%s','%s','%s')", $curr_id, $curr_detail_id, $content_sort_order, $content_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getCurriculumContentDetail($curr_id, $company_id);
        }
    }

    //Get Curriculum details by Company ID
    public function getCurriculumDetailByCompany($company_id) {

        $sql = sprintf("SELECT c.`curriculum_id`, c.`curr_name`, c.`begin_dt`, c.`end_dt`, c.`created_user_id`, cd.`curr_detail_id`, cd.`topic_srt_ord`, cd.`topic_name`, 
                    cd.`topic_img_url`, cd.`topic_img_content` FROM `curriculum` c, `curriculum_detail` cd 
                    WHERE c.`company_id`= '%s' AND c.`curriculum_id`=cd.`curriculum_id` ORDER BY `topic_srt_ord` ", $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output['curriculum'][] = $row;
                }
                
                $sql1 = sprintf("SELECT curriculum_name from company where company_id='%s'", $company_id);
                $result1 = mysqli_query($this->db, $sql1);

                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows1 = mysqli_num_rows($result1);
                    if ($num_of_rows1 > 0) {
                        while ($row1 = mysqli_fetch_assoc($result1)) {
                            $db_curriculum_name = $row1['curriculum_name'];
                            if(!empty($db_curriculum_name)){
                                
                            }else{
                                $db_curriculum_name = "Curriculum";
                            }
                        }
                    } else {
                        $error = array('status' => "Failed", "msg" => "Curriculum details doesn't exist.");
                        $this->response($this->json($error), 200); // If no records "No Content" status
                    }
                }
                
                $out = array('status' => "Success", 'curriculum_name' => $db_curriculum_name, 'msg' => $output);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Curriculum details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }

    //Get Curriculum Content details
    public function getCurriculumContentDetail($curr_id, $company_id) {
        $sql = sprintf("SELECT * FROM (SELECT cc.`curr_content_id`, cc.`curriculum_id`, cc.`curriculum_det_id`, cc.`content_srt_ord`, cc.`contentId`, cd.`content_id`, 
                    cd.`content_title`, cd.`created_user_id`, cd.`creator_company_id`, cd.`content_description`, cd.`ContentVideoUrl`, cd.`video_type`, cd.`video_id`
                    FROM `curriculum_content` cc, `content_definition` cd WHERE cc.`curriculum_id`='%s' AND `creator_company_id`='%s' AND cc.`contentId`=cd.`content_id` ORDER BY `content_srt_ord`) t1 LEFT JOIN video_content t2 ON t2.video_id =  t1.video_id ORDER BY `content_srt_ord`", $curr_id, $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $db_video_url = $row['video_url'];
                    $db_thum_nail_url = $row['thum_nail_url'];
                    if(!empty($db_video_url)){
                        $row['signed_url'] = $this->changeToSignedURL($db_video_url);
                    }else{
                        $row['signed_url'] = "";
                    }
                    
                    if(!empty($db_thum_nail_url)){
                        $row['signed_thum_nail_url'] = $this->changeToSignedURL($db_thum_nail_url);
                    }else{
                        $row['signed_thum_nail_url'] = "";
                    }
                    $output['curriculum_content'][] = $row;
                }
                $out = array('status' => "Success", 'msg' => $output);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Curriculum content details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }

    public function updateCurriculumDet($company_id, $curr_id, $curr_name) {

        $sql = sprintf("UPDATE `curriculum` SET `curr_name`='%s' WHERE `company_id`='%s' AND `curriculum_id`='%s'", mysqli_real_escape_string($this->db,$curr_name), $company_id, $curr_id);
        $result = mysqli_query($this->db, $sql);
        
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query1" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $sql1 = sprintf("UPDATE `curriculum_detail` SET `topic_name`='%s' WHERE `curriculum_id`='%s'", mysqli_real_escape_string($this->db,$curr_name), $curr_id);
            $result1 = mysqli_query($this->db, $sql1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } 
            $this->updateCurriculumDetails($company_id, $curr_id, $curr_name);
        }
    }

    public function updateCurriculumDetails($company_id, $curr_id, $curr_name) {

        $sql = sprintf("UPDATE `curriculum_detail` SET `topic_name`='%s' WHERE `curriculum_id`='%s'", mysqli_real_escape_string($this->db,$curr_name), $curr_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getCurriculumDetailByCompany($company_id);
        }
    }

    public function updateCurriculumContentDetails($company_id,$content_id,$content_title,$content_description,$content_video_url,$content_video_type,$video_id,$curr_id) {

        $sql = sprintf("UPDATE `content_definition` SET `content_title`='%s', `content_description`='%s', `ContentVideoUrl`='%s', `video_type`='%s', `video_id`='%s' WHERE `creator_company_id`='%s' AND `content_id`='%s'", mysqli_real_escape_string($this->db,$content_title), mysqli_real_escape_string($this->db,$content_description), $content_video_url, $content_video_type, $video_id, $company_id, $content_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getCurriculumContentDetail($curr_id, $company_id);
        }
    }

    //Delete Curriculum Content
    public function deleteCurriculumContentDetails($company_id, $content_id, $curriculum_id) {

        $select_sort_order = sprintf("SELECT `content_srt_ord` FROM `curriculum_content` WHERE `contentId`='%s' and `curriculum_id`='%s'", $content_id, $curriculum_id);
        $result_sort_order = mysqli_query($this->db, $select_sort_order);
        $sort_order_id = 0;
        
        if (!$result_sort_order) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_sort_order");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result_sort_order);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_sort_order)) {
                    $sort_order_id = $row['content_srt_ord'];
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Message doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }

        $delete_curriculum_content = sprintf("DELETE FROM `curriculum_content` WHERE `contentId`='%s' and `curriculum_id`='%s'", $content_id, $curriculum_id);
        $result = mysqli_query($this->db, $delete_curriculum_content);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$delete_curriculum_content");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {

            $delete_curriculum_definition = sprintf("DELETE FROM `content_definition` WHERE `content_id`='%s' and `creator_company_id`='%s'", $content_id, $company_id);
            $result1 = mysqli_query($this->db, $delete_curriculum_definition);

            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$delete_curriculum_definition");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if((!empty($sort_order_id))&&(is_numeric ($sort_order_id))&&($sort_order_id!=0)){
                    $update_message = sprintf("UPDATE `curriculum_content` SET `content_srt_ord`=`content_srt_ord`-1 WHERE `curriculum_id`='%s' and `content_srt_ord`>'%s'", $curriculum_id, $sort_order_id);
                    $result2 = mysqli_query($this->db, $update_message);
                    
                    //Print Company details after login success
                    if (!$result2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        
                    }
                }
                $out = array('status' => "Success", 'msg' => "successfully deleted.");
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            }
        }
    }

    //Delete Curriculum Content
    public function deleteCurriculumDetails($company_id, $curriculum_id) {

        $sql = sprintf("SELECT `contentId` FROM `curriculum_content` WHERE curriculum_id = '%s'", $curriculum_id);
        $result = mysqli_query($this->db, $sql);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {

            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $content_id = $row['contentId'];

                    $delete_curriculum_content = sprintf("DELETE FROM `curriculum_content` WHERE `contentId`='%s' and `curriculum_id`='%s'", $content_id, $curriculum_id);
                    $result1 = mysqli_query($this->db, $delete_curriculum_content);

                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$delete_curriculum_content");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {

                        $delete_curriculum_definition = sprintf("DELETE FROM `content_definition` WHERE `content_id`='%s' and `creator_company_id`='%s'", $content_id, $company_id);
                        $result2 = mysqli_query($this->db, $delete_curriculum_definition);

                        if (!$result2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$delete_curriculum_definition");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            
                        }
                    }
                }
            } else {
//                $error = array('status' => "Failed", "msg" => "Curriculum content details doesn't exist.");
//                $this->response($this->json($error), 200); // If no records "No Content" status
            }
            
            $delete_curriculum_detail = sprintf("DELETE FROM `curriculum_detail` WHERE `curriculum_id`='%s'", $curriculum_id);
            $result3 = mysqli_query($this->db, $delete_curriculum_detail);

            if (!$result3) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$delete_curriculum_detail");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {

                $delete_curriculum = sprintf("DELETE FROM `curriculum` WHERE `curriculum_id`='%s' and `company_id`='%s'", $curriculum_id, $company_id);
                $result4 = mysqli_query($this->db, $delete_curriculum);

                if (!$result4) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$delete_curriculum");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $out = array('status' => "Success", 'msg' => 'Successfully updated');
                // If success everythig is good send header as "OK" and user details
                   $this->response($this->json($out), 200);
                }
            }
        }
    }
    Public function updateCurriculumContentSortingDetails($content_id,$curriculum_sort_order,$company_id,$curriculum_id){
        for($i=0; $i<count($curriculum_sort_order); $i++){
            
            $update_message = sprintf("UPDATE `curriculum_content` SET `content_srt_ord`='%s' WHERE `curr_content_id`='%s'", $curriculum_sort_order[$i], $content_id[$i]);
            $result = mysqli_query($this->db, $update_message);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => $update_message);
                $this->response($this->json($error), 200);
            }
            
        }
//        $this->getCurriculumContentDetail($curriculum_id, $company_id);
        
        $out = array('status' => "Success", 'msg' => 'Successfully updated');
                // If success everythig is good send header as "OK" and user details
        $this->response($this->json($out), 200);
    }
    
    public function updateCurriculumSortingDetails($curriculum_sort_order,$company_id,$curriculum_id){
        for($i=0; $i<count($curriculum_sort_order); $i++){
            
            $update_message = sprintf("UPDATE `curriculum_detail` SET `topic_srt_ord`='%s' WHERE `curriculum_id`='%s'", $curriculum_sort_order[$i], $curriculum_id[$i]);
            $result = mysqli_query($this->db, $update_message);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => $update_message);
                $this->response($this->json($error), 200);
            }
            
        }
        $out = array('status' => "Success", 'msg' => 'Successfully updated');
                // If success everythig is good send header as "OK" and user details
        $this->response($this->json($out), 200);
    }

    public function aws_s3_link($access_key, $secret_key, $bucket, $canonical_uri, $expires, $region, $extra_headers = array()) {

        $encoded_uri = str_replace('%2F', '/', rawurlencode($canonical_uri));
        $signed_headers = array();
        foreach ($extra_headers as $key => $value) {
            $signed_headers[strtolower($key)] = $value;
        }
        if (!array_key_exists('host', $signed_headers)) {
            $signed_headers['host'] = ($region == 'us-east-1') ? "$bucket.s3.amazonaws.com" : "$bucket.s3-$region.amazonaws.com";
        }
        ksort($signed_headers);
        $header_string = '';
        foreach ($signed_headers as $key => $value) {
            $header_string .= $key . ':' . trim($value) . "\n";
        }
        $signed_headers_string = implode(';', array_keys($signed_headers));
        $timestamp = time();
        $date_text = gmdate('Ymd', $timestamp);
        $time_text = $date_text . 'T000000Z';
        $algorithm = 'AWS4-HMAC-SHA256';
        $scope = "$date_text/$region/s3/aws4_request";
        $x_amz_params = array(
            'X-Amz-Algorithm' => $algorithm,
            'X-Amz-Credential' => $access_key . '/' . $scope,
            'X-Amz-Date' => $time_text,
            'X-Amz-SignedHeaders' => $signed_headers_string
        );
        if ($expires > 0)
            $x_amz_params['X-Amz-Expires'] = $expires;
        ksort($x_amz_params);
        $query_string_items = array();
        foreach ($x_amz_params as $key => $value) {
            $query_string_items[] = rawurlencode($key) . '=' . rawurlencode($value);
        }
        $query_string = implode('&', $query_string_items);
        $canonical_request = "GET\n$encoded_uri\n$query_string\n$header_string\n$signed_headers_string\nUNSIGNED-PAYLOAD";
        $string_to_sign = "$algorithm\n$time_text\n$scope\n" . hash('sha256', $canonical_request, false);
        $signing_key = hash_hmac('sha256', 'aws4_request', hash_hmac('sha256', 's3', hash_hmac('sha256', $region, hash_hmac('sha256', $date_text, 'AWS4' . $secret_key, true), true), true), true);
        $signature = hash_hmac('sha256', $string_to_sign, $signing_key);
        $url = 'http://' . $signed_headers['host'] . $encoded_uri . '?' . $query_string . '&X-Amz-Signature=' . $signature;
        return $url;
    }
//    
    public function changeToSignedURL($url){
        $stringArray = explode('/', $url);
        //$bucket = "mystudio-test-private";
        $bucket = "mystudio-test.technogemsinc.com";
        $stringArray = explode('/', $url);
        $canonical_uri = "/" . $stringArray[4] . "/" . $stringArray[5];
        $extra_headers = array();
        $expires = "86400";
        $signed_url = $this->aws_s3_link('AKIAI3W3VQSR2VDGYZHA', 'hURk+Od5sZ2+SYKL/IDJ+PDolKSaVFY2JIrKC0GI', $bucket, $canonical_uri, $expires, 'us-east-1', $extra_headers);
        return $signed_url;
    }
        
}

?>