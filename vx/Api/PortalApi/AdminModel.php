<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token');
header('Access-Control-Allow-Credentials: true');

require_once '../../../Globals/cont_log.php';
require_once '../../Stripe/init.php';
require_once('../../PHPMailer_5.2.1/class.phpmailer.php');
require_once('../../PHPMailer_5.2.1/class.smtp.php');

class AdminModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $local_upload_folder_name = '';
    public $upload_folder_name = '';
    private $server_url;
     private $stripe_pk = '', $stripe_sk = '', $mystudio_account = '';

    public function __construct() {
        require_once '../../../Globals/config.php';
        require_once '../../../Globals/stripe_props.php';
        $this->inputs();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->local_upload_folder_name = $GLOBALS['local_uploads_folder'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'dev.mystudio.academy') !== false) {
            $this->server_url = "http://dev.mystudio.academy";
        } elseif (strpos($host, 'dev2.mystudio.academy') !== false) {
            $this->server_url = "http://dev2.mystudio.academy";
        } elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'];
        } else {
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
        header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    protected function clean($string) {
        $str = preg_replace('/\s+/', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    }
    
    private function getStripeKeys(){        
            $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
            $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
        
    public function getFromAccountDetailsForClone($company_email) {
        $select_company_email = sprintf("Select c.`company_id`,u.`user_id`,c.`company_code`,c.`company_name` from `user` u LEFT JOIN company c ON u.`company_id`=c.`company_id` where `user_email`='%s'", mysqli_real_escape_string($this->db, $company_email));
        $result_select_company_email = mysqli_query($this->db, $select_company_email);

        if (!$result_select_company_email) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_company_email");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $event =$membership= $trial = $curriculum=[];
            $num_rows = mysqli_num_rows($result_select_company_email);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result_select_company_email);
                $company_id = $row['company_id'];
                $company_name = $row['company_name'];
                $company_code = $row['company_code'];
                $user_id = $row['user_id'];
                $event = $this->getEventIdforClone($company_id);
                $membership = $this->getMembershipIdforClone($company_id);
                $trial=$this->getTrialIdforClone($company_id);
                $curriculum = $this->getCurriculumIdforClone($company_id, $user_id);
                
                $error_log = array('status' => "Success", "msg" => "Studio Details", "company_name" => $company_name, "company_id" => $company_id,"user_id"=>$user_id, "company_code" => $company_code, "event" => $event, "membership" => $membership, "curriculum" => $curriculum,"trial"=>$trial);
                $this->response($this->json($error_log), 200);
            } else {
                $error_log = array('status' => "Failed", "msg" => "Studio Details Not Available");
                $this->response($this->json($error_log), 200);
            }
        }
    }

    public function getEventIdforClone($company_id) {
        $select_event_id = sprintf("select * from event where company_id='%s' and event_status!='D' and `event_type`!='C' order by event_id ", mysqli_real_escape_string($this->db, $company_id));
        $result_select_event_id = mysqli_query($this->db, $select_event_id);

        if (!$result_select_event_id) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_event_id");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $event = [];
            $num_rows = mysqli_num_rows($result_select_event_id);
            if ($num_rows > 0) {
                $p = 0;
                while ($row = mysqli_fetch_assoc($result_select_event_id)) {
                    $event[$p]['event_id'] = $row['event_id'];
                    $event[$p]['event_title'] = $row['event_title'];
                    $p++;
                }
                return $event;
            } else {
                $error_log = array('status' => "Failed", "msg" => "Event Details Not Available");
                log_info($this->json($error_log));
                return $event;
            }
        }
    }

    public function getMembershipIdforClone($company_id) {
        $select_membership_id = sprintf("Select * from `membership` where `company_id`='%s' and deleted_flg!='D' order by membership_id ",
                mysqli_real_escape_string($this->db, $company_id));
        $result_select_membership_id = mysqli_query($this->db, $select_membership_id);

        if (!$result_select_membership_id) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_membership_id");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $membership = [];
            $num_rows = mysqli_num_rows($result_select_membership_id);
            if ($num_rows > 0) {
                $r = 0;
                while ($row = mysqli_fetch_assoc($result_select_membership_id)) {
                    $membership[$r]['membership_id'] = $row['membership_id'];
                    $membership[$r]['category_title'] = $row['category_title'];
                    $r++;
                }
                return $membership;
            } else {
                $error_log = array('status' => "Failed", "msg" => "Membership Details Not Available");
                log_info($this->json($error_log));
                return $membership;
            }
        }
    }

    public function getCurriculumIdforClone($company_id, $user_id) {
        $select_curr_id = sprintf("select * from `curriculum` where `company_id`='%s' and `created_user_id`='%s' ",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $user_id));
        $result_select_curr_id = mysqli_query($this->db, $select_curr_id);

        if (!$result_select_curr_id) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_curr_id");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $curriculum = [];
            $num_rows = mysqli_num_rows($result_select_curr_id);
            if ($num_rows > 0) {
                $s = 0;
                while ($row = mysqli_fetch_assoc($result_select_curr_id)) {
                    $curriculum[$s]['curriculum_id'] = $row['curriculum_id'];
                    $curriculum[$s]['curriculum_name'] = $row['curr_name'];
                    $s++;
                }
                return $curriculum;
            } else {
                $error_log = array('status' => "Failed", "msg" => "Curiculum Details Not Available");
                log_info($this->json($error_log));
                return $curriculum;
            }
        }
    }
    
    public function getTrialIdforClone($company_id) {
        $select_trial_id = sprintf("Select * from `trial` where `company_id`='%s' and trial_status!='D' order by trial_id ",
                mysqli_real_escape_string($this->db, $company_id));
        $result_select_trial_id = mysqli_query($this->db, $select_trial_id);

        if (!$result_select_trial_id) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_trial_id");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $trial = [];
            $num_rows = mysqli_num_rows($result_select_trial_id);
            if ($num_rows > 0) {
                $s = 0;
                while ($row = mysqli_fetch_assoc($result_select_trial_id)) {
                    $trial[$s]['trial_id'] = $row['trial_id'];
                    $trial[$s]['trial_title'] = $row['trial_title'];
                    $s++;
                }
                return $trial;
            } else {
                $error_log = array('status' => "Failed", "msg" => "Trial Details Not Available");
                log_info($this->json($error_log));
                return $trial;
            }
        }
    }

    public function getToAccountDetailsForClone($company_email) {
        $select_company_email = sprintf("Select c.`company_id`,u.`user_id`,c.`company_code`,c.`company_name` from `user` u LEFT JOIN company c ON u.`company_id`=c.`company_id` where `user_email`='%s'", mysqli_real_escape_string($this->db, $company_email));
        $result_select_company_email = mysqli_query($this->db, $select_company_email);

        if (!$result_select_company_email) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_company_email");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_select_company_email);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result_select_company_email);
                $company_id = $row['company_id'];
                $user_id= $row['user_id'];
                $company_name = $row['company_name'];
                $company_code = $row['company_code'];               
                $error_log = array('status' => "Success", "msg" => "Studio Details","company_id"=>$company_id,"user_id"=>$user_id, "company_name" => $company_name, "company_code" => $company_code);
                $this->response($this->json($error_log), 200);
            } else {
                $error_log = array('status' => "Failed", "msg" => "Studio Details Not Available");
                $this->response($this->json($error_log), 200);
            }
        }
    }

    public function getStudioCloneDetailsFromUser($company_id,$new_company_id,$user_id,$new_user_id,$event_clone_flag,$membership_clone_flag,$curriculum_clone_flag,$trial_clone_flag, $event_details, $membership_details, $curriculum_details,$trial_details) {
        $var1=$var2=$var3=$var4=0;
        if (isset($curriculum_details)) {
            if ($curriculum_clone_flag == 1) {
                $curr_id = "''";
            } elseif ($curriculum_clone_flag == 2) {
                $curr_id = "(SELECT `curriculum_id` from curriculum where 1)";
            } else {
                if (!empty($curriculum_details)) {
                    for ($c = 0; $c < count($curriculum_details); $c++) {
                        $curr_id_arr[] = json_decode($curriculum_details[$c]['curriculum_id']);
                    }
                    $curr_id = implode(",", $curr_id_arr);
                } else {
                    $curr_id = "''";
                }
            }


            $query1 = sprintf("SELECT * FROM `curriculum` WHERE `company_id`='%s' AND `created_user_id`='%s' AND `curriculum_id`IN (%s)",
                    mysqli_escape_string($this->db, $company_id), mysqli_escape_string($this->db, $user_id), $curr_id);
            $result_query = mysqli_query($this->db, $query1);
            if (!$result_query) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $numrows = mysqli_num_rows($result_query);
                if ($numrows > 0) {
                    $var1=1;
                    $initial_check = 0;
                    while ($rows = mysqli_fetch_assoc($result_query)) {
                        $curriculam_id = $rows['curriculum_id'];
                        $cur_name = $rows['curr_name'];
                        $begin_dt = $rows['begin_dt'];
                        $end_dt = $rows['end_dt'];
                        $this->clonecuriculamdetail($company_id, $new_company_id, $user_id, $new_user_id, $curriculam_id, $cur_name, $begin_dt, $end_dt, $initial_check);
                        $initial_check++;
                    }
                }
            }
        }

        if (isset($event_details)) {
            if ($event_clone_flag == 1) {
                $event_id = "''";
            } elseif ($event_clone_flag == 2) {
                $event_id = "(SELECT `event_id` from event where 1)";
            } else {
                if(!empty($event_details)){
                    for($e=0;$e<count($event_details);$e++){
                    $event_id_arr[] = json_decode($event_details[$e]['event_id']);
                    }
                    $event_id = implode(",", $event_id_arr);
                    }else{
                     $event_id = "''";
                    }
            }
            
            $query2 = sprintf("SELECT * FROM event  where company_id='%s' AND event_id IN (%s) AND event_status!='D' ORDER BY event_id",
                    mysqli_escape_string($this->db, $company_id),$event_id);

            $result2 = mysqli_query($this->db, $query2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_row2 = mysqli_num_rows($result2);
                if ($num_row2 > 0) {
                    $var2=1;
                    while ($row2 = mysqli_fetch_assoc($result2)) {
                        $event_id = $row2['event_id'];
                        $type_check = $row2['event_type'];
                        if ($type_check !== 'C') {
                            $this->cloneEventDetails($company_id, $new_company_id, $event_id);
                        }
                    }
                }
            }
        }





        if (isset($membership_details)) {
            if ($membership_clone_flag == 1) {
                $membership_id = "''";
            } elseif ($membership_clone_flag == 2) {
                $membership_id = "(SELECT `membership_id` from membership where 1)";
            } else {
                if(!empty($membership_details)){
                    for($m=0;$m<count($membership_details);$m++){
                    $membership_id_arr[] = json_decode($membership_details[$m]['membership_id']);
                    }
                    $membership_id = implode(",", $membership_id_arr);
                    }else{
                       $membership_id = "''"; 
                    }
            }


            $query3 = sprintf("SELECT * FROM membership  WHERE  company_id='%s' AND membership_id IN (%s) AND deleted_flg!='D'",
                    mysqli_escape_string($this->db, $company_id),$membership_id);


            $result3 = mysqli_query($this->db, $query3);
            if (!$result3) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows3 = mysqli_num_rows($result3);
                if ($num_rows3 > 0) {
                    $var3=1;
                    while ($row3 = mysqli_fetch_assoc($result3)) {
                        $membership_id = $row3['membership_id'];
                        $this->copyMembershipDetails($company_id, $membership_id, '', $new_company_id);
                    }
                }
            }
        }
        
        if (isset($trial_details)) {
            if ($trial_clone_flag == 1) {
                $trial_id = "''";
            } elseif ($trial_clone_flag == 2) {
                $trial_id = "(SELECT `trial_id` from trial where 1)";
            } else {
                if(!empty($trial_details)){
                for($t=0;$t<count($trial_details);$t++){
                    $trial_id_arr[] = json_decode($trial_details[$t]['trial_id']);
                    }
                    $trial_id = implode(",", $trial_id_arr);
                }else{
                    $trial_id = "''"; 
                }
            }

            $query4 = sprintf("SELECT * FROM trial WHERE company_id='%s' AND trial_id IN (%s) AND trial_status!='D'",
                    mysqli_escape_string($this->db, $company_id),$trial_id);


            $result4 = mysqli_query($this->db, $query4);
            if (!$result4) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query4");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows4 = mysqli_num_rows($result4);
                if ($num_rows4 > 0) {
                    $var4=1;
                    while ($row4 = mysqli_fetch_assoc($result4)) {
                        $trial_id = $row4['trial_id'];
                        $this->copyTrialDetails($company_id, $new_company_id,'',$trial_id);
                    }
                }
            }
        }
        if($var1||$var2||$var3||$var4==1){
            $success = array('status' => "Success", "msg" => "Studio Details Cloned Successfully");
                $this->response($this->json($success), 200);
        }else{
            $error = array('status' => "Failed", "msg" => "No Studio Details Available to Clone");
                $this->response($this->json($error), 200);
        }
    }

    private function clonecuriculamdetail($company_id, $new_company_id, $user_id, $new_user_id, $curriculam_id, $cur_name, $begin_dt, $end_dt, $initial_check) {
        static $new_content_id = [];
        static $result2 = true;
        
        if ((is_null($begin_dt) || $begin_dt == '0000-00-00') && (is_null($end_dt) || $end_dt == '0000-00-00')) {
            $begin = "null";
            $end = "null";
        } else {
            $begin = "$begin_dt";
            $end = "$end_dt";
        }
        $sql = sprintf("INSERT INTO `curriculum`(`company_id`, `curr_name`,`begin_dt`,`end_dt`, `created_user_id`) VALUES ('%s','%s',%s,%s,'%s')",
                mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $cur_name), $begin, $end, mysqli_real_escape_string($this->db, $new_user_id));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error1.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 500);
        } else {
            $curr_id = mysqli_insert_id($this->db);
            log_info("new curriculam id  $curr_id");
//            echo"$curr_id curr_id"."<br>";
            $sql_sort_order = sprintf("SELECT `curriculum_id` FROM `curriculum` WHERE company_id='%s'", $new_company_id);
            $result_sort_order = mysqli_query($this->db, $sql_sort_order);
            if (!$result_sort_order) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error2.", "query" => "$sql_sort_order");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error2.");
                $this->response($this->json($error), 500);
            } else {
                $topic_srt_ord = 0;
                $num_of_rows = mysqli_num_rows($result_sort_order);
                if ($num_of_rows >= 0) {
                    $topic_srt_ord = $num_of_rows + 1;
                }
            }
            $cur_sql = sprintf("SELECT * FROM `curriculum_detail` WHERE `curriculum_id`='%s'", $curriculam_id);
            $cur_result = mysqli_query($this->db, $cur_sql);
            if (!$cur_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error3.", "query" => "$cur_sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error3.");
                $this->response($this->json($error), 500);
            } else {
                $nowrows1 = mysqli_num_rows($cur_result);
                if ($nowrows1 > 0) {
                    while ($rows1 = mysqli_fetch_assoc($cur_result)) {
                        if ((is_null($rows1['topic_img_url']) || $rows1['topic_img_url'] == '') && (is_null($rows1['topic_img_content']) || $rows1['topic_img_content'] == '')) {
                            $img_url = "null";
                            $img_content = "null";
                        } else {
                            $temp_url = $rows1['topic_img_url'];
                            $img_url = "$temp_url";
                            $temp_content = $rows1['topic_img_content'];
                            $img_content = "$temp_content";
                        }
                        $sql1 = sprintf("INSERT INTO `curriculum_detail` (`curriculum_id`,`topic_srt_ord`,`topic_name`,`topic_img_url`,`topic_img_content`) VALUES ('%s','%s','%s',%s,%s)",
                                $curr_id, $topic_srt_ord, mysqli_real_escape_string($this->db, $rows1['topic_name']), mysqli_real_escape_string($this->db, $img_url), mysqli_real_escape_string($this->db, $img_content));
                        $result1 = mysqli_query($this->db, $sql1);
                        if (!$result1) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error4.", "query" => "$sql1");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error4.");
                            $this->response($this->json($error), 500);
                        } else {
                            $curr_detailid = mysqli_insert_id($this->db);
                            log_info("new curriculam detailid  $curr_detailid");
//                            echo"new curriculam detailid  $curr_detailid"."<br>";
                            $content_sql = sprintf("select * from `content_definition` where `creator_company_id`='%s' and `created_user_id`='%s'", mysqli_escape_string($this->db, $company_id), mysqli_escape_string($this->db, $user_id));
                            $content_result = mysqli_query($this->db, $content_sql);
                            if (!$content_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error5.", "query" => "$content_sql");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error5.");
                                $this->response($this->json($error), 500);
                            } else {
                                $numrows2 = mysqli_num_rows($content_result);
                                if ($numrows2 > 0) {
                                    while ($rows2 = mysqli_fetch_assoc($content_result)) {
                                        if ($initial_check == 0) {
                                            $sql2 = sprintf("INSERT INTO `content_definition`(`content_title`, `content_description`, `ContentVideoUrl`, `video_type`, `video_id`, `created_user_id`, `creator_company_id`) VALUES ('%s','%s','%s','%s','%s','%s','%s')",
                                                    mysqli_real_escape_string($this->db, $rows2['content_title']), mysqli_real_escape_string($this->db, $rows2['content_description']), mysqli_real_escape_string($this->db, $rows2['ContentVideoUrl']), mysqli_real_escape_string($this->db, $rows2['video_type']),
                                                    mysqli_real_escape_string($this->db, $rows2['video_id']), mysqli_real_escape_string($this->db, $new_user_id), mysqli_real_escape_string($this->db, $new_company_id));
                                            $result2 = mysqli_query($this->db, $sql2);
                                        }
                                        if (!$result2) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error6.", "query" => "$sql2");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error6.");
                                            $this->response($this->json($error), 500);
                                        } else {
                                            if ($initial_check == 0) {
                                                $content_id = mysqli_insert_id($this->db);
                                                $new_content_id[$rows2['content_id']] = $content_id;
                                            }
                                            $cur_content_sql = sprintf("select * from `curriculum_content` where curriculum_id='%s' and curriculum_det_id='%s' and contentId='%s'",
                                                    mysqli_escape_string($this->db, $curriculam_id), mysqli_escape_string($this->db, $rows1['curr_detail_id']), mysqli_escape_string($this->db, $rows2['content_id']));
                                            $cur_content_result = mysqli_query($this->db, $cur_content_sql);
                                            if (!$cur_content_result) {
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error7.", "query" => "$cur_content_sql");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error7.");
                                                $this->response($this->json($error), 500);
                                            } else {
                                                $numrows3 = mysqli_num_rows($cur_content_result);
                                                if ($numrows3 > 0) {
                                                    while ($rows3 = mysqli_fetch_assoc($cur_content_result)) {
                                                        $latest_content_id = $new_content_id[$rows2['content_id']];
                                                        $sql3 = sprintf("INSERT INTO `curriculum_content`(`curriculum_id`, `curriculum_det_id`, `content_srt_ord`, `contentId`) VALUES ('%s','%s','%s','%s')",
                                                                mysqli_escape_string($this->db, $curr_id), mysqli_escape_string($this->db, $curr_detailid), mysqli_escape_string($this->db, $rows3['content_srt_ord']), mysqli_escape_string($this->db, $latest_content_id));
                                                        $result3 = mysqli_query($this->db, $sql3);
                                                        if (!$result3) {
                                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error8.", "query" => "$sql3");
                                                            log_info($this->json($error_log));
                                                            $error = array('status' => "Failed", "msg" => "Internal Server Error8.");
                                                            $this->response($this->json($error), 500);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function cloneEventDetails($company_id, $new_company_id, $event_id) {
        $parent_id = $company_code = $last_inserted_event_id = $last_inserted_event_id1= $event_compare_price="";

        $sql_company_code = sprintf("SELECT  `company_code` FROM  `company` WHERE  `company_id` = '%s'", mysqli_real_escape_string($this->db, $new_company_id));
        $result_company_code = mysqli_query($this->db, $sql_company_code);
        if (!$result_company_code) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_company_code");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $output_value = mysqli_fetch_assoc($result_company_code);
            $company_code = $output_value['company_code'];
        }

        $sql = sprintf("SELECT `parent_id`, `event_type`, `event_status`, `event_title`, 
            `event_category_subtitle`, `event_desc`, `event_more_detail_url`,  `event_video_detail_url`, `event_cost`, 
            `event_compare_price`, `event_capacity`, `capacity_text`, `processing_fees`, `event_onetime_payment_flag`, 
            `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, 
            `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `event_banner_img_content`, 
            `event_banner_img_url`, `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, `event_registration_column_2`, 
            `event_reg_col_2_mandatory_flag`, `event_registration_column_3`, `event_reg_col_3_mandatory_flag`, 
            `event_registration_column_4`, `event_reg_col_4_mandatory_flag`, `event_registration_column_5`, 
            `event_reg_col_5_mandatory_flag`, `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, 
            `event_registration_column_7`, `event_reg_col_7_mandatory_flag`, `event_registration_column_8`, 
            `event_reg_col_8_mandatory_flag`, `event_registration_column_9`, `event_reg_col_9_mandatory_flag`, 
            `event_registration_column_10`, `event_reg_col_10_mandatory_flag`,(SELECT IF(c.`wepay_status`='N','S',`event_status`) FROM `company` c WHERE c.`company_id`='%s')event_status_new FROM `event` e WHERE e.`company_id`='%s' AND 
            (`event_id`='%s' OR `parent_id`='%s') AND `event_status`!='D' order by event_type desc", $new_company_id,$company_id, $event_id, $event_id);
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }

                if (empty(trim($output[0]['parent_id']))) {
                    $parent_id = 'null';
                    if (is_null($output[0]['event_capacity'])) {
                        $capacity = 'NULL';
                    } else {
                        $capacity = $output[0]['event_capacity'];
                    }
                    if(empty(trim($output[0]['event_compare_price']))||is_null($output[0]['event_compare_price'])){      
                        $event_compare_price = 'NULL';   
                    }else{
                        $event_compare_price=$output[0]['event_compare_price'];
                    }
                    $sql1 = sprintf("INSERT INTO `event` (`company_id`, `parent_id`, `event_type`, `event_status`, `event_title`, 
                        `event_category_subtitle`, `event_desc`, `event_more_detail_url`,  `event_video_detail_url`, `event_cost`, 
                        `event_compare_price`, `event_capacity`, `capacity_text`, `processing_fees`, `event_onetime_payment_flag`, 
                        `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, 
                        `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, 
                        `event_banner_img_content`,`event_registration_column_1`, 
                        `event_reg_col_1_mandatory_flag`, `event_registration_column_2`, `event_reg_col_2_mandatory_flag`, 
                        `event_registration_column_3`, `event_reg_col_3_mandatory_flag`, `event_registration_column_4`, 
                        `event_reg_col_4_mandatory_flag`, `event_registration_column_5`, `event_reg_col_5_mandatory_flag`, 
                        `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, 
                        `event_reg_col_7_mandatory_flag`, `event_registration_column_8`, `event_reg_col_8_mandatory_flag`, 
                        `event_registration_column_9`, `event_reg_col_9_mandatory_flag`, `event_registration_column_10`, 
                        `event_reg_col_10_mandatory_flag`, `event_sort_order`) VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                        '%s',%s, %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                        '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', 
                        NextVal('event_sort_order_seq'))", mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $parent_id),
                            mysqli_real_escape_string($this->db, $output[0]['event_type']), mysqli_real_escape_string($this->db, $output[0]['event_status_new']),mysqli_real_escape_string($this->db, $output[0]['event_title']),                            
                            mysqli_real_escape_string($this->db, $output[0]['event_category_subtitle']), mysqli_real_escape_string($this->db, $output[0]['event_desc']),mysqli_real_escape_string($this->db, $output[0]['event_more_detail_url']),
                            mysqli_real_escape_string($this->db, $output[0]['event_video_detail_url']), mysqli_real_escape_string($this->db, $output[0]['event_cost']),mysqli_real_escape_string($this->db,$event_compare_price),
                            mysqli_real_escape_string($this->db, $capacity), mysqli_real_escape_string($this->db, $output[0]['capacity_text']),mysqli_real_escape_string($this->db, $output[0]['processing_fees']),
                            mysqli_real_escape_string($this->db, $output[0]['event_onetime_payment_flag']), mysqli_real_escape_string($this->db, $output[0]['event_recurring_payment_flag']),mysqli_real_escape_string($this->db, $output[0]['total_order_text']),
                            mysqli_real_escape_string($this->db, $output[0]['total_quantity_text']), mysqli_real_escape_string($this->db, $output[0]['deposit_amount']),mysqli_real_escape_string($this->db, $output[0]['number_of_payments']),mysqli_real_escape_string($this->db, $output[0]['payment_startdate_type']),                            
                            mysqli_real_escape_string($this->db, $output[0]['payment_startdate']), mysqli_real_escape_string($this->db, $output[0]['payment_frequency']),mysqli_real_escape_string($this->db, $output[0]['waiver_policies']), mysqli_real_escape_string($this->db, $output[0]['event_banner_img_content']),                            
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_1']), mysqli_real_escape_string($this->db, $output[0]['event_reg_col_1_mandatory_flag']), mysqli_real_escape_string($this->db, $output[0]['event_registration_column_2']),
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_2_mandatory_flag']), mysqli_real_escape_string($this->db, $output[0]['event_registration_column_3']), mysqli_real_escape_string($this->db, $output[0]['event_reg_col_3_mandatory_flag']),
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_4']), mysqli_real_escape_string($this->db, $output[0]['event_reg_col_4_mandatory_flag']), mysqli_real_escape_string($this->db, $output[0]['event_registration_column_5']),
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_5_mandatory_flag']), mysqli_real_escape_string($this->db, $output[0]['event_registration_column_6']), mysqli_real_escape_string($this->db, $output[0]['event_reg_col_6_mandatory_flag']),
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_7']), mysqli_real_escape_string($this->db, $output[0]['event_reg_col_7_mandatory_flag']), mysqli_real_escape_string($this->db, $output[0]['event_registration_column_8']),
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_8_mandatory_flag']), mysqli_real_escape_string($this->db, $output[0]['event_registration_column_9']), mysqli_real_escape_string($this->db, $output[0]['event_reg_col_9_mandatory_flag']),
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_10']), mysqli_real_escape_string($this->db, $output[0]['event_reg_col_10_mandatory_flag']));
                }
                $result1 = mysqli_query($this->db, $sql1);
                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } else {
                    $last_inserted_event_id = mysqli_insert_id($this->db);
                    if ($output[0]['event_type'] == 'M' || $output[0]['event_type'] == 'S'|| $output[0]['event_type'] == 'C') {
                        $company_code_new = $this->clean($company_code);
                        $event_url = "https://www.mystudio.academy/e/?=$company_code_new/$new_company_id/$last_inserted_event_id";
                        $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->local_upload_folder_name/Default/default.png";
                        if($output[0]['event_banner_img_url'] !== $event_banner_img_url){
                            $file_name = $new_company_id . "-" . $last_inserted_event_id;
                            $cfolder_name = "Company_$new_company_id";
                              $old = getcwd(); // Save the current directory
                            $dir_name = "../../../$this->upload_folder_name";
                            chdir($dir_name);
                            if (!file_exists($cfolder_name)) {
                                $oldmask = umask(0);
                                mkdir($cfolder_name, 0777, true);
                                umask($oldmask);
                            }
                            chdir($cfolder_name);
                            $folder_name = "Events";
                            if (!file_exists($folder_name)) {
                                $oldmask = umask(0);
                                mkdir($folder_name, 0777, true);
                                umask($oldmask);
                            }
                            chdir($folder_name);
                            $file = $file_name.'.png' ;
                           $check = copy($output[0]['event_banner_img_url'],$file);
                           chdir($old);
                           if($check){
                            $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->local_upload_folder_name/$cfolder_name/$folder_name/$file";
                           }else{
                                $error_log = array('status' => "Failed", "msg" => "Image copy failed", "image_url" => $output[0]['event_banner_img_url']);
                                log_info($this->json($error_log)); 
                           }
                        }
                        $sql_update_url = sprintf("UPDATE `event` SET `event_url` = '$event_url',event_banner_img_url='$event_banner_img_url' WHERE `event_id` = '%s' ", mysqli_real_escape_string($this->db, $last_inserted_event_id));
                        $result_update_url = mysqli_query($this->db, $sql_update_url);
                        if (!$result_update_url) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update_url");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }
                    if ($num_of_rows > 1) {

                        for ($i = 1; $i < count($output); $i++) {
                            if (is_null($output[$i]['event_capacity'])) {
                                $capacity = 'NULL';
                            } else {
                                $capacity = $output[$i]['event_capacity'];
                            }
                            if(empty(trim($output[$i]['event_compare_price']))||is_null($output[$i]['event_compare_price'])){      
                                $event_compare_price = 'NULL';   
                            }else{
                                $event_compare_price=$output[$i]['event_compare_price'];
                            }
        
                            $sql2 = sprintf("INSERT INTO `event` (`company_id`, `parent_id`, `event_type`, `event_status`, `event_title`, 
                                `event_category_subtitle`, `event_desc`, `event_more_detail_url`,  `event_video_detail_url`, `event_cost`, 
                                `event_compare_price`, `event_capacity`, `capacity_text`, `processing_fees`, `event_onetime_payment_flag`, 
                                `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, 
                                `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, 
                                `event_banner_img_content`, `event_registration_column_1`, 
                                `event_reg_col_1_mandatory_flag`, `event_registration_column_2`, `event_reg_col_2_mandatory_flag`, 
                                `event_registration_column_3`, `event_reg_col_3_mandatory_flag`, `event_registration_column_4`, 
                                `event_reg_col_4_mandatory_flag`, `event_registration_column_5`, `event_reg_col_5_mandatory_flag`, 
                                `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, 
                                `event_reg_col_7_mandatory_flag`, `event_registration_column_8`, `event_reg_col_8_mandatory_flag`, 
                                `event_registration_column_9`, `event_reg_col_9_mandatory_flag`, `event_registration_column_10`, 
                                `event_reg_col_10_mandatory_flag`, `event_sort_order`) VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                                '%s', %s, %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                                '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', 
                                NextVal('event_sort_order_seq'))", mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $last_inserted_event_id),
                                    mysqli_real_escape_string($this->db, $output[$i]['event_type']), mysqli_real_escape_string($this->db, $output[$i]['event_status_new']), mysqli_real_escape_string($this->db, $output[$i]['event_title']),
                                    mysqli_real_escape_string($this->db, $output[$i]['event_category_subtitle']), mysqli_real_escape_string($this->db, $output[$i]['event_desc']), mysqli_real_escape_string($this->db, $output[$i]['event_more_detail_url']),
                                    mysqli_real_escape_string($this->db, $output[$i]['event_video_detail_url']), mysqli_real_escape_string($this->db, $output[$i]['event_cost']), mysqli_real_escape_string($this->db,$event_compare_price),
                                    mysqli_real_escape_string($this->db, $capacity), mysqli_real_escape_string($this->db, $output[$i]['capacity_text']), mysqli_real_escape_string($this->db, $output[$i]['processing_fees']),
                                    mysqli_real_escape_string($this->db, $output[$i]['event_onetime_payment_flag']), mysqli_real_escape_string($this->db, $output[$i]['event_recurring_payment_flag']), mysqli_real_escape_string($this->db, $output[$i]['total_order_text']),
                                    mysqli_real_escape_string($this->db, $output[$i]['total_quantity_text']), mysqli_real_escape_string($this->db, $output[$i]['deposit_amount']), mysqli_real_escape_string($this->db, $output[$i]['number_of_payments']),
                                    mysqli_real_escape_string($this->db, $output[$i]['payment_startdate_type']), mysqli_real_escape_string($this->db, $output[$i]['payment_startdate']), mysqli_real_escape_string($this->db, $output[$i]['payment_frequency']),
                                    mysqli_real_escape_string($this->db, $output[$i]['waiver_policies']), mysqli_real_escape_string($this->db, $output[$i]['event_banner_img_content']),
                                    mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_1']), mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_1_mandatory_flag']), mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_2']),
                                    mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_2_mandatory_flag']), mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_3']), mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_3_mandatory_flag']),
                                    mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_4']), mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_4_mandatory_flag']), mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_5']),
                                    mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_5_mandatory_flag']), mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_6']), mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_6_mandatory_flag']),
                                    mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_7']), mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_7_mandatory_flag']), mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_8']),
                                    mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_8_mandatory_flag']), mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_9']), mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_9_mandatory_flag']),
                                    mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_10']), mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_10_mandatory_flag']));
                            $result2 = mysqli_query($this->db, $sql2);
                            if (!$result2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }else{
                                $last_inserted_event_id1 = mysqli_insert_id($this->db);
                                if ($output[$i]['event_type'] == 'M' || $output[$i]['event_type'] == 'S'|| $output[$i]['event_type'] == 'C') {
                                    $company_code_new = $this->clean($company_code);
                                    $event_url = "https://www.mystudio.academy/e/?=$company_code_new/$new_company_id/$last_inserted_event_id1";
                                    $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->local_upload_folder_name/Default/default.png";
                                    if ($output[$i]['event_banner_img_url'] !== $event_banner_img_url) {
                                        $file_name = $new_company_id . "-" . $last_inserted_event_id1;
                                        $cfolder_name = "Company_$new_company_id";
                                        $old = getcwd(); // Save the current directory
                                        $dir_name = "../../../$this->upload_folder_name";
                                        chdir($dir_name);
                                        if (!file_exists($cfolder_name)) {
                                            $oldmask = umask(0);
                                            mkdir($cfolder_name, 0777, true);
                                            umask($oldmask);
                                        }
                                        chdir($cfolder_name);
                                        $folder_name = "Events";
                                        if (!file_exists($folder_name)) {
                                            $oldmask = umask(0);
                                            mkdir($folder_name, 0777, true);
                                            umask($oldmask);
                                        }
                                        chdir($folder_name);
                                        $file = $file_name . '.png';
                                        $check = copy($output[$i]['event_banner_img_url'], $file);
                                        chdir($old);
                                        if ($check) {
                                            $event_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->local_upload_folder_name/$cfolder_name/$folder_name/$file";
                                        } else {
                                            $error_log = array('status' => "Failed", "msg" => "Image copy failed", "image_url" => $output[$i]['event_banner_img_url']);
                                            log_info($this->json($error_log));
                                        }
                                    }
                                    $sql_update_url = sprintf("UPDATE `event` SET `event_url` = '$event_url',event_banner_img_url='$event_banner_img_url' WHERE `event_id` = '%s' ", mysqli_real_escape_string($this->db, $last_inserted_event_id1));
                                    $result_update_url = mysqli_query($this->db, $sql_update_url);
                                    if (!$result_update_url) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update_url");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 500);
                                    }
                                }
                            }
                        }
                    }
                }
                if (!empty($last_inserted_event_id)) {
                    $get_event_discount = sprintf("SELECT * FROM `event_discount` WHERE `company_id`='%s' AND `event_id`='%s' AND `is_expired`='N'", $company_id, $event_id);
                    $get_event_discount_result = mysqli_query($this->db, $get_event_discount);
                    if (!$get_event_discount_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_discount");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        if (mysqli_num_rows($get_event_discount_result) > 0) {
                            while ($row_disc = mysqli_fetch_assoc($get_event_discount_result)) {
                                $insert_disc_query = sprintf("INSERT INTO `event_discount`(`company_id`, `event_id`, `discount_type`, `discount_code`, `discount_amount`) VALUES ('%s', '%s', '%s', '%s', '%s')",
                                        $new_company_id, $last_inserted_event_id, $row_disc['discount_type'], $row_disc['discount_code'], $row_disc['discount_amount']);
                                $insert_disc_result = mysqli_query($this->db, $insert_disc_query);
                                if (!$insert_disc_result) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_disc_query");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Event details doesn't exist.");
                $this->response($this->json($error), 401); // If no records "No Content" status
            }
        }
    }

    //Copy membership details
    private function copyMembershipDetails($company_id, $membership_id, $membership_template_flag, $new_company_id) {
        $num_of_rows1 = $num_of_rows2 = $num_of_rows3 = $num_of_rows_exclude = $num_of_rows_discount = 0;
        if ($membership_template_flag == "Y") {
            $comp_id = "6";
        } else {
            $comp_id = $company_id;
        }

        $sql_membership_category = sprintf("SELECT * ,(SELECT IF(c.`wepay_status`='N','S',`category_status`) FROM `company` c WHERE c.`company_id`='%s')category_status_new FROM `membership` m WHERE m.`company_id`='%s' AND `membership_id`='%s' AND m.`deleted_flg`!='D'",
                mysqli_real_escape_string($this->db, $new_company_id),mysqli_real_escape_string($this->db, $comp_id), mysqli_real_escape_string($this->db, $membership_id));
        $result1 = mysqli_query($this->db, $sql_membership_category);

        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_membership_category");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows1 = mysqli_num_rows($result1);
            if ($num_of_rows1 > 0) {
                $output1 = mysqli_fetch_assoc($result1);                    
            } else {
                $error = array('status' => "Failed", "msg" => "Membership details doesn't exist.");
                $this->response($this->json($error), 401);
            }
        }

        $sql_membership_rank = sprintf("SELECT * FROM `membership_ranks` WHERE `company_id`='%s' AND `membership_id`='%s' AND `rank_deleted_flg`!='D' ORDER BY `rank_order`",
                mysqli_real_escape_string($this->db, $comp_id), mysqli_real_escape_string($this->db, $membership_id));
        $result2 = mysqli_query($this->db, $sql_membership_rank);

        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_membership_rank");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows2 = mysqli_num_rows($result2);
            if ($num_of_rows2 > 0) {
                while ($row2 = mysqli_fetch_assoc($result2)) {
                    $output2[] = $row2;
                }
            }
        }

        $sql_membership_options = sprintf("SELECT  * FROM `membership_options` WHERE `company_id`='%s' AND `membership_id`='%s' AND `deleted_flg`!='D' ORDER BY `option_sort_order`",
                mysqli_real_escape_string($this->db, $comp_id), mysqli_real_escape_string($this->db, $membership_id));
        $result3 = mysqli_query($this->db, $sql_membership_options);

        if (!$result3) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_membership_options");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows3 = mysqli_num_rows($result3);
            if ($num_of_rows3 > 0) {
                while ($row3 = mysqli_fetch_assoc($result3)) {
                    $output3[] = $row3;
                }
            }
        }

        $new_membership_id = 0;
        if ($num_of_rows1 > 0) {
            $sql1 = sprintf("INSERT INTO `membership` (`company_id`, `category_status`, `category_title`, `category_subtitle`, `category_video_url`, `category_description`, `category_sort_order`,`waiver_policies`, 
                    `membership_registration_column_1`, `membership_reg_col_1_mandatory_flag`, `membership_registration_column_2`, `membership_reg_col_2_mandatory_flag`, `membership_registration_column_3`, `membership_reg_col_3_mandatory_flag`, 
                    `membership_registration_column_4`, `membership_reg_col_4_mandatory_flag`, `membership_registration_column_5`, `membership_reg_col_5_mandatory_flag`, `membership_registration_column_6`, `membership_reg_col_6_mandatory_flag`, 
                    `membership_registration_column_7`, `membership_reg_col_7_mandatory_flag`, `membership_registration_column_8`, `membership_reg_col_8_mandatory_flag`, `membership_registration_column_9`, `membership_reg_col_9_mandatory_flag`, 
                    `membership_registration_column_10`, `membership_reg_col_10_mandatory_flag` )
                        VALUES ( '%s', '%s', '%s', '%s', '%s', '%s', NextVal('membership_sort_order_seq'), '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                    mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $output1['category_status_new']), mysqli_real_escape_string($this->db, $output1['category_title']), mysqli_real_escape_string($this->db, $output1['category_subtitle']),
                    mysqli_real_escape_string($this->db, $output1['category_video_url']), mysqli_real_escape_string($this->db, $output1['category_description']), mysqli_real_escape_string($this->db, $output1['waiver_policies']),
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_1']), mysqli_real_escape_string($this->db, $output1['membership_reg_col_1_mandatory_flag']), mysqli_real_escape_string($this->db, $output1['membership_registration_column_2']),
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_2_mandatory_flag']), mysqli_real_escape_string($this->db, $output1['membership_registration_column_3']), mysqli_real_escape_string($this->db, $output1['membership_reg_col_3_mandatory_flag']),
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_4']), mysqli_real_escape_string($this->db, $output1['membership_reg_col_4_mandatory_flag']), mysqli_real_escape_string($this->db, $output1['membership_registration_column_5']),
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_5_mandatory_flag']), mysqli_real_escape_string($this->db, $output1['membership_registration_column_6']), mysqli_real_escape_string($this->db, $output1['membership_reg_col_6_mandatory_flag']),
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_7']), mysqli_real_escape_string($this->db, $output1['membership_reg_col_7_mandatory_flag']), mysqli_real_escape_string($this->db, $output1['membership_registration_column_8']),
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_8_mandatory_flag']), mysqli_real_escape_string($this->db, $output1['membership_registration_column_9']), mysqli_real_escape_string($this->db, $output1['membership_reg_col_9_mandatory_flag']),
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_10']), mysqli_real_escape_string($this->db, $output1['membership_reg_col_10_mandatory_flag']));
            $result4 = mysqli_query($this->db, $sql1);

            if (!$result4) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $new_membership_id = mysqli_insert_id($this->db);

                $get_studio_code = sprintf("SELECT `company_code` FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $new_company_id));
                $result_studio_code = mysqli_query($this->db, $get_studio_code);
                if (!$result_studio_code) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_studio_code");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } else {
                    $output_value = mysqli_fetch_assoc($result_studio_code);
                    $company_code = $output_value['company_code'];
                }
                $company_code_new = $this->clean($company_code);
                $category_url = "https://www.mystudio.academy/m/?=$company_code_new/$new_company_id/$new_membership_id";
                $category_image_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->local_upload_folder_name/Default/default.png";
                    if($output1['category_image_url'] !== $category_image_url){
                        $file_name = $new_company_id . "-" . $new_membership_id;
                        $cfolder_name = "Company_$new_company_id";
                          $old = getcwd(); // Save the current directory
                        $dir_name = "../../../$this->upload_folder_name";
                        chdir($dir_name);
                        if (!file_exists($cfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($cfolder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($cfolder_name);
                        $folder_name = "Membership";
                        if (!file_exists($folder_name)) {
                            $oldmask = umask(0);
                            mkdir($folder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($folder_name);
                        $file = $file_name.'.png' ;
                       $check = copy($output1['category_image_url'],$file);
                       chdir($old);
                       if($check){
                        $category_image_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->local_upload_folder_name/$cfolder_name/$folder_name/$file";
                       }else{
                          $error_log = array('status' => "Failed", "msg" => "Image copy failed", "image_url" => $output1['category_image_url']);
                            log_info($this->json($error_log));  
                       }
                    }

                $sql_url_update = sprintf("UPDATE `membership` SET `membership_category_url` = '$category_url',category_image_url='$category_image_url' WHERE `membership_id` = '%s' ", mysqli_real_escape_string($this->db, $new_membership_id));
                $result_url_update = mysqli_query($this->db, $sql_url_update);
                if (!$result_url_update) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_url_update");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }

        if ($new_membership_id > 0) {
            if ($num_of_rows2 > 0) {
                for ($j = 0; $j < $num_of_rows2; $j++) {
                    $sql2 = sprintf("INSERT INTO `membership_ranks` (`company_id`, `membership_id`,`rank_name`,`rank_order`) VALUES ('%s', '%s', '%s', NextVal('membership_rank_sort_order_seq'))",
                            mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $new_membership_id), mysqli_real_escape_string($this->db, $output2[$j]['rank_name']));
                    $result5 = mysqli_query($this->db, $sql2);

                    if (!$result5) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
            }

            if ($num_of_rows3 > 0) { //ravi
                for ($i = 0; $i < $num_of_rows3; $i++) {
                    $output_exclude = $output_discount = [];
                    $exclude_query = sprintf("SELECT * FROM `membership_billing_exclude` WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s' AND `deleted_flag`!='D' order by `membership_billing_exclude_id`",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $output3[$i]['membership_id']), mysqli_real_escape_string($this->db, $output3[$i]['membership_option_id']));
                    $result_exclude = mysqli_query($this->db, $exclude_query);
                    if (!$result_exclude) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$exclude_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $num_of_rows_exclude = mysqli_num_rows($result_exclude);
                        if ($num_of_rows_exclude > 0) {
                            while ($exclude_row = mysqli_fetch_assoc($result_exclude)) {
                                $output_exclude[] = $exclude_row;
                            }
                        }
                    }
                    $discount_query = sprintf("SELECT * FROM `membership_discount` WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s' AND `is_expired`!='Y'  order by `membership_discount_id`",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $output3[$i]['membership_id']), mysqli_real_escape_string($this->db, $output3[$i]['membership_option_id']));
                    $result_discount = mysqli_query($this->db, $discount_query);
                    if (!$result_discount) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$discount_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $num_of_rows_discount = mysqli_num_rows($result_discount);
                        if ($num_of_rows_discount > 0) {
                            while ($discount_row = mysqli_fetch_assoc($result_discount)) {
                                $output_discount[] = $discount_row;
                            }
                        }
                    }
                    $sql3 = sprintf("INSERT INTO `membership_options` (`company_id`, `membership_id`,`membership_title`,`membership_subtitle`,`membership_description`,
                            `membership_structure`, `membership_processing_fee_type`, `membership_signup_fee`, `membership_fee`, `show_in_app_flg`, `membership_recurring_frequency`, `custom_recurring_frequency_period_type`, 
                            `custom_recurring_frequency_period_val`, `prorate_first_payment_flg`, `delay_recurring_payment_start_flg`, `delayed_recurring_payment_type`, `delayed_recurring_payment_val`, `initial_payment_include_membership_fee`,
                            `billing_options`, `no_of_classes`, `expiration_date_type`, `expiration_date_val`, `deposit_amount`, `no_of_payments`, `billing_payment_start_date_type`,`specific_start_date`,`specific_end_date`,`specific_payment_frequency`,`week_days_order`,`billing_days`,`exclude_from_billing_flag`, `option_sort_order`)
                             VALUES ('%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s',                     
                             NextVal('membership_option_sort_order_seq'))", mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $new_membership_id), mysqli_real_escape_string($this->db, $output3[$i]['membership_title']),
                            mysqli_real_escape_string($this->db, $output3[$i]['membership_subtitle']), mysqli_real_escape_string($this->db, $output3[$i]['membership_description']), mysqli_real_escape_string($this->db, $output3[$i]['membership_structure']),
                            mysqli_real_escape_string($this->db, $output3[$i]['membership_processing_fee_type']), mysqli_real_escape_string($this->db, $output3[$i]['membership_signup_fee']), mysqli_real_escape_string($this->db, $output3[$i]['membership_fee']),
                            mysqli_real_escape_string($this->db, $output3[$i]['show_in_app_flg']), mysqli_real_escape_string($this->db, $output3[$i]['membership_recurring_frequency']), mysqli_real_escape_string($this->db, $output3[$i]['custom_recurring_frequency_period_type']),
                            mysqli_real_escape_string($this->db, $output3[$i]['custom_recurring_frequency_period_val']), mysqli_real_escape_string($this->db, $output3[$i]['prorate_first_payment_flg']), mysqli_real_escape_string($this->db, $output3[$i]['delay_recurring_payment_start_flg']),
                            mysqli_real_escape_string($this->db, $output3[$i]['delayed_recurring_payment_type']), mysqli_real_escape_string($this->db, $output3[$i]['delayed_recurring_payment_val']), mysqli_real_escape_string($this->db, $output3[$i]['initial_payment_include_membership_fee']),
                            mysqli_real_escape_string($this->db, $output3[$i]['billing_options']), mysqli_real_escape_string($this->db, $output3[$i]['no_of_classes']), mysqli_real_escape_string($this->db, $output3[$i]['expiration_date_type']), mysqli_real_escape_string($this->db, $output3[$i]['expiration_date_val']),
                            mysqli_real_escape_string($this->db, $output3[$i]['deposit_amount']), mysqli_real_escape_string($this->db, $output3[$i]['no_of_payments']), mysqli_real_escape_string($this->db, $output3[$i]['billing_payment_start_date_type']), mysqli_real_escape_string($this->db, $output3[$i]['specific_start_date']),
                            mysqli_real_escape_string($this->db, $output3[$i]['specific_end_date']), mysqli_real_escape_string($this->db, $output3[$i]['specific_payment_frequency']), mysqli_real_escape_string($this->db, $output3[$i]['week_days_order']), mysqli_real_escape_string($this->db, $output3[$i]['billing_days']),
                            mysqli_real_escape_string($this->db, $output3[$i]['exclude_from_billing_flag']));
                    $result6 = mysqli_query($this->db, $sql3);

                    if (!$result6) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $current_option_id = mysqli_insert_id($this->db);

                        $option_url = "https://www.mystudio.academy/m/?=$company_code_new/$new_company_id/$new_membership_id/$current_option_id";
                        $sql_url_update2 = sprintf("UPDATE `membership_options` SET `membership_option_url` = '$option_url' WHERE `membership_option_id` = '%s'", mysqli_real_escape_string($this->db, $current_option_id));
                        $result_url_update2 = mysqli_query($this->db, $sql_url_update2);
                        if (!$result_url_update2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_url_update2");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                        if ($num_of_rows_exclude > 0) {
                            for ($j = 0; $j < $num_of_rows_exclude; $j++) {
                                $exclude_query1 = sprintf("INSERT INTO `membership_billing_exclude`(`company_id`, `membership_id`, `membership_option_id`, `billing_exclude_startdate`, `billing_exclude_enddate`, `deleted_flag`) VALUES ('%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $new_membership_id), mysqli_real_escape_string($this->db, $current_option_id), mysqli_real_escape_string($this->db, $output_exclude[$j]['billing_exclude_startdate']),
                                        mysqli_real_escape_string($this->db, $output_exclude[$j]['billing_exclude_enddate']), mysqli_real_escape_string($this->db, $output_exclude[$j]['deleted_flag']));
                                $result_exclude1 = mysqli_query($this->db, $exclude_query1);
                                if (!$result_exclude1) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$exclude_query1");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                        if ($num_of_rows_discount > 0) {
                            for ($j = 0; $j < $num_of_rows_discount; $j++) {
                                $discount_query1 = sprintf("INSERT INTO `membership_discount`(`company_id`, `membership_id`, `membership_option_id`,`discount_off`,`discount_type`,`discount_code`,`discount_amount`,`is_expired`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $new_membership_id), mysqli_real_escape_string($this->db, $current_option_id), mysqli_real_escape_string($this->db, $output_discount[$j]['discount_off']),
                                        mysqli_real_escape_string($this->db, $output_discount[$j]['discount_type']), mysqli_real_escape_string($this->db, $output_discount[$j]['discount_code']), mysqli_real_escape_string($this->db, $output_discount[$j]['discount_amount']), mysqli_real_escape_string($this->db, $output_discount[$j]['is_expired']));
                                $result_discount1 = mysqli_query($this->db, $discount_query1);

                                if (!$result_discount1) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$discount_query1");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    public function copyTrialDetails($company_id,$new_company_id,$trial_template_flag, $trial_id){
        $company_code = "";        
             if($trial_template_flag == "Y"){
            $comp_id = "6";
        } else {
            $comp_id = $company_id;
        }
        
        $sql_company_code = sprintf("SELECT  `company_code` FROM  `company` WHERE  `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result_company_code = mysqli_query($this->db, $sql_company_code);
        if (!$result_company_code) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_company_code");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $output_value = mysqli_fetch_assoc($result_company_code);
            $company_code = $output_value['company_code'];
        }
        
        $sql = sprintf("SELECT `trial_status`, `trial_title`, `trial_subtitle`, `trial_desc`, `trial_prog_url`, `trial_welcome_url`, `trial_banner_img_url`, `trial_banner_img_content`, `trial_waiver_policies`, `registrations_count`, `net_sales`, `processing_fee_type`, `price_amount`, 
            `program_length_type`, `program_length`, `trial_sort_order`, `trial_registration_column_1`, `trial_registration_column_1_flag`, `trial_registration_column_2`, `trial_registration_column_2_flag`, `trial_registration_column_3`, `trial_registration_column_3_flag`, 
            `trial_registration_column_4`, `trial_registration_column_4_flag`, `trial_registration_column_5`, `trial_registration_column_5_flag`, `trial_registration_column_6`, `trial_registration_column_6_flag`, `trial_registration_column_7`, `trial_registration_column_7_flag`, `trial_registration_column_8`,
            `trial_registration_column_8_flag`, `trial_registration_column_9`, `trial_registration_column_9_flag`, `trial_registration_column_10`, `trial_registration_column_10_flag`, `trial_lead_source_1`, `trial_lead_source_2`, `trial_lead_source_3`, `trial_lead_source_4`, `trial_lead_source_5`,
            `trial_lead_source_6`, `trial_lead_source_7`, `trial_lead_source_8`, `trial_lead_source_9`, `trial_lead_source_10`, `created_dt`, `last_updt_dt`,(SELECT IF(c.`wepay_status`='N','S',`trial_status`) FROM `company` c WHERE c.`company_id`='%s')trial_status_new FROM `trial` WHERE `company_id`='%s' AND `trial_id`='%s'
             AND `trial_status`!='D'", mysqli_real_escape_string($this->db,$new_company_id), mysqli_real_escape_string($this->db,$comp_id), mysqli_real_escape_string($this->db,$trial_id));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                
                        $sql1 = sprintf("INSERT INTO `trial`(`company_id`, `trial_status`, `trial_title`, `trial_subtitle`, `trial_desc`, 
                                `trial_prog_url`, `trial_welcome_url`,  `trial_banner_img_content`, `trial_waiver_policies`, `processing_fee_type`, `price_amount`, `program_length_type`, `program_length`, `trial_registration_column_1`, `trial_registration_column_1_flag`, `trial_registration_column_2`, 
                                `trial_registration_column_2_flag`, `trial_registration_column_3`, `trial_registration_column_3_flag`, `trial_registration_column_4`, `trial_registration_column_4_flag`, `trial_registration_column_5`, `trial_registration_column_5_flag`, `trial_registration_column_6`, `trial_registration_column_6_flag`, `trial_registration_column_7`, `trial_registration_column_7_flag`, `trial_registration_column_8`, `trial_registration_column_8_flag`, 
                                `trial_registration_column_9`, `trial_registration_column_9_flag`, `trial_registration_column_10`, `trial_registration_column_10_flag`, `trial_lead_source_1`, `trial_lead_source_2`, `trial_lead_source_3`, `trial_lead_source_4`, `trial_lead_source_5`, `trial_lead_source_6`, `trial_lead_source_7`, `trial_lead_source_8`, `trial_lead_source_9`, `trial_lead_source_10`,`trial_sort_order`) 
                                VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s',
                                NextVal('trial_sort_order_seq'))", mysqli_real_escape_string($this->db, $new_company_id), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_status_new']),  
                            mysqli_real_escape_string($this->db, $output[0]['trial_title']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_subtitle']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_desc']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_prog_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_welcome_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_banner_img_content']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_waiver_policies']), 
                            mysqli_real_escape_string($this->db, $output[0]['processing_fee_type']), 
                            mysqli_real_escape_string($this->db, $output[0]['price_amount']), 
                            mysqli_real_escape_string($this->db, $output[0]['program_length_type']), 
                            mysqli_real_escape_string($this->db, $output[0]['program_length']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_1']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_1_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_2']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_2_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_3']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_3_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_4']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_4_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_5']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_5_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_6']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_6_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_7']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_7_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_8']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_8_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_9']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_9_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_10']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_10_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_1']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_2']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_3']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_4']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_5']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_6']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_7']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_8']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_9']),
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_10']));

                 $result1 = mysqli_query($this->db, $sql1);//discount

                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $last_inserted_trial_id = mysqli_insert_id($this->db);
                     $company_code_new = $this->clean($company_code);
                     $trial_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/t/?=$company_code_new/$new_company_id/$last_inserted_trial_id";
                    $trial_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->local_upload_folder_name/Default/default.png";
                    if($output[0]['trial_banner_img_url'] !== $trial_banner_img_url) {
                        $file_name = $new_company_id . "-" . $last_inserted_trial_id;
                        $cfolder_name = "Company_$new_company_id";
                        $old = getcwd(); // Save the current directory
                        $dir_name = "../../../$this->upload_folder_name";
                        chdir($dir_name);
                        if (!file_exists($cfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($cfolder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($cfolder_name);
                        $folder_name = "Trial";
                        if (!file_exists($folder_name)) {
                            $oldmask = umask(0);
                            mkdir($folder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($folder_name);
                        $file = $file_name . '.png';
                        $check = copy($output[0]['trial_banner_img_url'], $file);
                        chdir($old);
                        if ($check) {
                            $trial_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->local_upload_folder_name/$cfolder_name/$folder_name/$file";
                        } else {
                            $error_log = array('status' => "Failed", "msg" => "Image copy failed", "image_url" => $output[0]['trial_banner_img_url']);
                            log_info($this->json($error_log));
                        }
                    }
                    $sql_update_url = sprintf("UPDATE `trial` SET `trial_prog_url` = '$trial_url',`trial_banner_img_url`='$trial_banner_img_url' WHERE `trial_id` = '%s' ", mysqli_real_escape_string($this->db, $last_inserted_trial_id));
                        $result_update_url = mysqli_query($this->db, $sql_update_url);
                        if (!$result_update_url) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update_url");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else {
                        $discount = $this->getTrialDiscountDetails($company_id, $trial_id);
                        $disc = $discount['discount'];
                        if ($discount['status'] == 'Success') {
                            for ($i = 0; $i < count($disc); $i++) {
                                $sql_disc = sprintf("INSERT INTO `trial_discount`(`company_id`, `trial_id`, `discount_type`, `discount_code`, `discount_amount`) VALUES ('%s', '%s', '%s', '%s', '%s')", 
                                        mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $last_inserted_trial_id), mysqli_real_escape_string($this->db, $disc[$i]['discount_type']), 
                                        mysqli_real_escape_string($this->db, $disc[$i]['discount_code']), mysqli_real_escape_string($this->db, $disc[$i]['discount_amount']));
                                $result_disc = mysqli_query($this->db, $sql_disc);
                                if (!$result_disc) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_disc");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Trial program details doesn't exist.");
//                $this->response($this->json($error), 200); // If no records "No Content" status
                log_info($this->json($error));
            }
        }
    }
        public function getTrialDiscountDetails($company_id, $trial_id){
        $sql = sprintf("SELECT `trial_discount_id` , `discount_type` , `discount_code` , `discount_amount` FROM `trial_discount`
                WHERE `company_id` = '%s' AND `trial_id`='%s' AND `is_expired`!='Y'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id));
        $result = mysqli_query($this->db, $sql);
        $output = $response = [];
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $output[] = $row;
                }
                $response['status'] = "Success";
            }else{
                $response['status'] = "Failed";
            }
        }
        $response['discount'] = $output;
        return $response;
    }
    
    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }
    
//    getting admin studioaccounts
    public function getadminstudioaccounts($sorting,$start,$length_table,$draw_table,$search){
         $output = [];
         $out=[];
        $sort = $sorting[0]['column'];
        $sort_type = $sorting[0]['dir'];
        $search_value = mysqli_real_escape_string($this->db,$search['value']);
        $datestring = "%m/%d/%Y";
        $datesearch_string = "'".$datestring."'";
         if (!empty($search_value)) {
            $s_text = " and ( company_name like  '%$search_value%'  or if(`upgrade_status` = 'T','Trial',if(`upgrade_status` = 'F','Inactive',if(`upgrade_status` = 'M','Premium Plan Monthly',if(`upgrade_status` = 'P','Premium Plan Yearly',if(`upgrade_status` = 'W','White Label',''))))) like  '%$search_value%' or if(`studio_payment_status` = 'A','Active',if(`studio_payment_status` = 'F','Failed',if(`studio_payment_status` = 'C','Cancelled',if(`studio_payment_status` = '','N/A','N/A')))) like  '%$search_value%' or  DATE_FORMAT(c.`created_date`,$datesearch_string) like '%$search_value%'
                    or `lead_source` like  '%$search_value%' or `owner_name` like  '%$search_value%' or   user_firstname  like  '%$search_value%' 
                     or  user_lastname  like  '%$search_value%' or  user_phone  like  '%$search_value%' or  u.`user_email` like  '%$search_value%'  or  company_code like  '%$search_value%' or company_type like '%$search_value%' or DATE_FORMAT(`expiry_dt`,$datesearch_string) like '%$search_value%' or street_address like '%$search_value%' or city like '%$search_value%' or state like '%$search_value%' or postal_code like '%$search_value%' or country like '%$search_value%' or phone_number like '%$search_value%' or web_page like '%$search_value%')";
        } else {
            $s_text = '';
        }

        $column = array(0 => "company_name", 1 => "payment_plan", 2 => "studio_payment_status", 3 => "registered_date", 4 => "lead_source", 5 => "owner_name", 6 => "stripe_account", 8 => "total_app_users", 9 => "total_event_sales", 10=>"total_membership_sales", 11=>"user_firstname", 12=>"user_lastname", 13=>"user_phone", 14=>"email_for_log_in", 15=>"company_code", 16=>"Industry", 17=>"next_payment_date", 18=>"next_payment_amount", 19=>"last_login", 20=>"studio_address", 21=>"studio_phone", 22=>"web_site");
        
        $query_count = sprintf("SELECT  count(*) as total_record FROM `company` c LEFT JOIN user u on c.company_id = u.company_id 
                    where c.`company_id`!= '6' AND  u.`user_type`='A' AND c.`deleted_flag` != 'Y'");
        $result_count = mysqli_query($this->db, $query_count);
        if (!$result_count) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", 'query' => "$query_count");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows_count=mysqli_num_rows($result_count);
            if($num_rows_count > 0){
                $row_count = mysqli_fetch_assoc($result_count);
                $total_record= $row_count['total_record'] ;
//                $out['recordsTotal'] = $recordsTotal;
//                $out['recordsFiltered'] = $recordsTotal;
                $out['recordsTotal'] = $total_record;
                $out['recordsFiltered'] = $total_record;
            }
        }
        $sql = sprintf("SELECT t1.*, t2.plan_fee next_payment_amount FROM
                    (SELECT `company_name`,c.`company_id`,`upgrade_status`,`street_address`,`city`,`state`,`postal_code`,`country`,
                    concat(`street_address`,if(trim(`street_address`)!='',', ',''),`city`,if(trim(`city`)!='',', ',''),`state`,
                    if(trim(`state`)!='',', ',''),`postal_code`,if(trim(`postal_code`)!='',', ',''),`country`) studio_address,
                    IFNULL(`company_code`,'') company_code, IF(`referer_name` IS NULL OR TRIM(`referer_name`)='', `lead_source`, concat(`lead_source`, ' - ', `referer_name`)) lead_source,
                    if(`studio_payment_status` = 'A','Active',if(`studio_payment_status` = 'F','Failed',if(`studio_payment_status` = 'C','Cancelled',if(`studio_payment_status` = '','N/A','N/A')))) payment_status,
                    if(`upgrade_status` = 'T','Trial',if(`upgrade_status` = 'F','Inactive',if(`upgrade_status` = 'M','Premium Plan Monthly',if(`upgrade_status` = 'P','Premium Plan Yearly',if(`upgrade_status` = 'W','White Label Yearly',if(`upgrade_status` = 'WM','White Label Monthly',if(`upgrade_status` = 'B','Basic',''))))))) payment_plan, 
                    DATE_FORMAT(c.`created_date`,'%s') registered_date, `owner_name` , if(`stripe_subscription` = 'Y','Yes',if(`stripe_subscription` = 'N','NO','')) stripe_account, 
                    ( select count(*) from student where (`company_id` = c.`company_id` AND `deleted_flag` != 'D') ) total_app_users,
                    (select IFNULL(DATE_FORMAT(`last_login_dt`,'%s'),'') from `user` us where `company_id` = c.`company_id` AND  us.`user_type`='A') last_login,
                    `company_type` Industry,`web_page` web_site,`phone_number` studio_phone, IFNULL(DATE_FORMAT(`expiry_dt`,'%s'),'') next_payment_date, 
                    u.user_id, u.`user_firstname`, u.`user_lastname`,u.`user_email` email_for_log_in,u.`user_phone` user_phone 
                    FROM `company` c LEFT JOIN user u on c.company_id = u.company_id
                    where c.`company_id`!= '6' AND  u.`user_type`='A' AND c.`deleted_flag` != 'Y'  %s)t1
                    LEFT JOIN 
                    (SELECT ssf.company_id, ssf.`plan_fee`, ssf.`premium_type` FROM `studio_subscription_fee` ssf) t2
                    ON t1.upgrade_status=t2.premium_type
                    WHERE IF((SELECT count(*) count FROM `studio_subscription_fee`  WHERE  `company_id`=t1.`company_id`)=0,
                    t2.`company_id`=0, t2.`company_id`=t1.`company_id` and `premium_type` in (select `premium_type`from studio_subscription_fee WHERE t2.`company_id`=t1.company_id))
                    ORDER BY $column[$sort] $sort_type LIMIT $start, $length_table ",$datestring,$datestring,$datestring,$s_text);
//        $sql = sprintf("SELECT `company_name`,c.`company_id`,`upgrade_status`,`street_address`,`city`,`state`,`postal_code`,`country`,
//                    concat(`street_address`,if(trim(`street_address`)!='',', ',''),`city`,if(trim(`city`)!='',', ',''),`state`,
//                    if(trim(`state`)!='',', ',''),`postal_code`,if(trim(`postal_code`)!='',', ',''),`country`) studio_address,
//                    IFNULL(`company_code`,'') company_code, IF(`referer_name` IS NULL OR TRIM(`referer_name`)='', `lead_source`, concat(`lead_source`, ' - ', `referer_name`)) lead_source,
//                    if(`studio_payment_status` = 'A','Active',if(`studio_payment_status` = 'F','Failed',if(`studio_payment_status` = 'C','Cancelled',if(`studio_payment_status` = '','N/A','N/A')))) payment_status,
//                    if(`upgrade_status` = 'T','Trial',if(`upgrade_status` = 'F','Inactive',if(`upgrade_status` = 'M','Premium Plan Monthly',if(`upgrade_status` = 'P','Premium Plan Yearly',if(`upgrade_status` = 'W','White Label Yearly',if(`upgrade_status` = 'WM','White Label Monthly',if(`upgrade_status` = 'B','Basic',''))))))) payment_plan, DATE_FORMAT(c.`created_date`,'%s') registered_date, `owner_name` , if(`stripe_subscription` = 'Y','Yes',if(`stripe_subscription` = 'N','NO','')) stripe_account, 
//                    ( select count(*) from student where (`company_id` = c.`company_id` AND `deleted_flag` != 'D') ) total_app_users,
//                    (select IFNULL(DATE_FORMAT(`last_login_dt`,'%s'),'') from `user` where `company_id` = c.`company_id`) last_login,
//                    `company_type` Industry,`web_page` web_site,`phone_number` studio_phone, IFNULL(DATE_FORMAT(`expiry_dt`,'%s'),'') next_payment_date, 
//                    u.user_id, u.`user_firstname`, u.`user_lastname`,u.`user_email` email_for_log_in,u.`user_phone` user_phone, 
//                    (SELECT ssf.`plan_fee` FROM `studio_subscription_fee` ssf WHERE ssf.premium_type=c.upgrade_status AND IF(ssf.`company_id`=c.company_id,ssf.`company_id`=c.company_id,ssf.`company_id`=0)) next_payment_amount
//                    FROM `company` c LEFT JOIN user u on c.company_id = u.company_id
//                    where c.`company_id`!= '6' AND  u.`user_type`='A' AND c.`deleted_flag` != 'Y' %s  ORDER BY $column[$sort] $sort_type LIMIT $start, $length_table ",$datestring,$datestring,$datestring,$s_text);
//               where c.`company_id`!= '6' AND  u.`user_type`='A' AND c.`deleted_flag` != 'Y' AND IF(ssf.company_id = c.company_id,ssf.company_id = ssf.company_id,ssf.company_id = 0) AND ssf.premium_type=c.upgrade_status   %s  ORDER BY $column[$sort] $sort_type LIMIT $start, $length_table ",$datestring,$datestring,$datestring,$s_text);

        $result = mysqli_query($this->db, $sql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $row['cc_processing'] = $this->getProcessingFee($row["company_id"],$row["upgrade_status"]);
                    $output[] = $row;
                }
                $out['draw'] = $draw_table;
                $out['data'] = $output;
                $this->response($this->json($out), 200);
            }else{
                $out['draw'] = $draw_table;
                $out['data'] = $output;
                $this->response($this->json($out), 200); // If no records "No Content" status
            }
        }
    }
    
    public function updatestudiodetails($company_id,$studio_name,$lead_source,$owner_name,$user_id,$user_firstname,$user_lastname,$user_phone,$industry,$website,$studio_phone,$login_email,$company_code,$street_address,$city,$state,$postal_code,$country){
        $updatequery = sprintf("UPDATE `company`,`user` SET company.`company_name` = '%s',company.`lead_source`='%s',company.`owner_name`='%s',
                user.`user_firstname`='%s',user.`user_lastname`='%s',user.`user_phone`='%s',company.`company_type`='%s',company.`web_page`='%s',
                company.`phone_number`='%s',company.`company_code`='%s',user.`user_email`='%s',company.`street_address`='%s',company.`city`='%s',
                company.`state`='%s',company.`postal_code`='%s',company.`country`='%s' 
                WHERE user.`company_id` = company.`company_id` AND company.`company_id` = '%s' AND user.`user_id`='%s'",
                mysqli_real_escape_string($this->db, $studio_name), mysqli_real_escape_string($this->db, $lead_source), 
                mysqli_real_escape_string($this->db, $owner_name), mysqli_real_escape_string($this->db, $user_firstname), 
                mysqli_real_escape_string($this->db, $user_lastname), mysqli_real_escape_string($this->db, $user_phone), 
                mysqli_real_escape_string($this->db, $industry), mysqli_real_escape_string($this->db, $website),
                mysqli_real_escape_string($this->db, $studio_phone),mysqli_real_escape_string($this->db, $company_code),mysqli_real_escape_string($this->db, $login_email),
                mysqli_real_escape_string($this->db, $street_address),mysqli_real_escape_string($this->db, $city),mysqli_real_escape_string($this->db, $state),
                mysqli_real_escape_string($this->db, $postal_code),mysqli_real_escape_string($this->db, $country),
                mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $user_id));
        $result = mysqli_query($this->db, $updatequery);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatequery");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
           $out = array('status' => "Success", 'msg' => 'Studio Details Updated Successfully');
           $this->response($this->json($out), 200);
        }
    }
    
     public function getadmindashboarddetailsForAdminPanel(){
        $sql = sprintf("SELECT count(*) count, upgrade_status from company where deleted_flag!='Y' and company_id!=6 
                    and company_id NOT IN (select company_id from user where `user_type`='S' ) and studio_payment_status != 'F' group by upgrade_status union SELECT count(*) count, 'Failed' as upgrade_status from `company` WHERE studio_payment_status = 'F' UNION  SELECT count(*) count, 'Cancelled_account' as upgrade_status from `company` WHERE studio_payment_status = 'C' ");
        $result = mysqli_query($this->db, $sql);
        $output = [];
        $output['total_members'] = $output['Inactive'] = $output['trial'] = $output['basic'] = $output['premium_yearly'] = $output['white_lable_yearly'] = $output['premium_monthly'] = $output['white_lable_monthly'] = 0;
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $total_members = 0;
                while($row = mysqli_fetch_assoc($result)){
                    if ($row['upgrade_status'] == 'F') {
                        $output['Inactive'] = $row['count'];
                    } elseif ($row['upgrade_status'] == 'T') {
                        $output['trial'] = $row['count'];
                    } elseif ($row['upgrade_status'] == 'B') {
                        $output['basic'] = $row['count'];
                    } elseif ($row['upgrade_status'] == 'P') {
                        $output['premium_yearly'] = $row['count'];
                    } elseif ($row['upgrade_status'] == 'W') {
                        $output['white_lable_yearly'] = $row['count'];
                    } elseif ($row['upgrade_status'] == 'M') {
                        $output['premium_monthly'] = $row['count'];
                    } elseif ($row['upgrade_status'] == 'WM') {
                        $output['white_lable_monthly'] = $row['count'];
                    } elseif ($row['upgrade_status'] == 'Failed') {
                        $output['failed_payments'] = $row['count'];
                    } elseif ($row['upgrade_status'] == 'Cancelled_account') {
                        $output['Cancelled_account'] = $row['count'];
                    }
                    $output['total_members'] += $row['count'];
                }
                $out = array('status' => "Success", 'msg' => $output);
                $this->response($this->json($out), 200);
            }else{
                $error = array('status' => "Failed", "msg" => "No data available.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function adminstudioaccountscsv($search_value){
        $curr_time_zone = date_default_timezone_get();
        $datestring = "%m/%d/%Y";
        $datesearch_string = "'".$datestring."'";
         if (!empty($search_value)) {
            $s_text = " and ( company_name like  '%$search_value%'  or if(`upgrade_status` = 'T','Trial',if(`upgrade_status` = 'F','Inactive',if(`upgrade_status` = 'M','Premium Plan Monthly',if(`upgrade_status` = 'P','Premium Plan Yearly',if(`upgrade_status` = 'W','White Label',''))))) like  '%$search_value%' or if(`studio_payment_status` = 'A','Active',if(`studio_payment_status` = 'F','Failed',if(`studio_payment_status` = 'C','Cancelled',if(`studio_payment_status` = '','N/A','N/A')))) like  '%$search_value%' or  DATE_FORMAT(c.`created_date`,$datesearch_string) like '%$search_value%'
                    or `lead_source` like  '%$search_value%' or `owner_name` like  '%$search_value%' or   user_firstname  like  '%$search_value%' 
                     or  user_lastname  like  '%$search_value%' or  user_phone  like  '%$search_value%' or  u.`user_email` like  '%$search_value%'  or  company_code like  '%$search_value%' or company_type like '%$search_value%' or DATE_FORMAT(`expiry_dt`,$datesearch_string) like '%$search_value%' or street_address like '%$search_value%' or city like '%$search_value%' or state like '%$search_value%' or postal_code like '%$search_value%' or country like '%$search_value%' or phone_number like '%$search_value%' or web_page like '%$search_value%')";
        } else {
            $s_text = '';
        }
        
        $sql = sprintf("SELECT `company_name`,c.`company_id`,`upgrade_status`,`street_address`,`city`,`state`,`postal_code`,`country`,
                    concat(`street_address`,if(trim(`street_address`)!='',', ',''),`city`,if(trim(`city`)!='',', ',''),`state`,
                    if(trim(`state`)!='',', ',''),`postal_code`,if(trim(`postal_code`)!='',', ',''),`country`) studio_address,
                    IFNULL(`company_code`,'') company_code,`lead_source`,
                    if(`studio_payment_status` = 'A','Active',if(`studio_payment_status` = 'F','Failed',if(`studio_payment_status` = 'C','Cancelled',if(`studio_payment_status` = '','N/A','N/A')))) as payment_status,
                    if(`upgrade_status` = 'T','Trial', if(`upgrade_status` = 'F','Inactive',if(`upgrade_status` = 'M','Premium Plan Monthly',if(`upgrade_status` = 'P','Premium Plan Yearly',if(`upgrade_status` = 'W','White Label',if(`upgrade_status` = 'B','Basic','')))))) payment_plan, DATE_FORMAT(c.`created_date`,'%s') registered_date, `owner_name` , if(`stripe_subscription` = 'Y','Yes',if(`stripe_subscription` = 'N','NO','')) stripe_account, 
                    ( select count(*) from student where (`company_id` = c.`company_id` AND `deleted_flag` != 'D') ) total_app_users,
                    IFNULL((select SUM(IF( mr.processing_fee_type = 2, mp.`payment_amount` + mp.`processing_fee`, mp.`payment_amount` ))  
                    from `membership_payment` mp LEFT JOIN  membership_registration mr ON mp.`membership_registration_id` = mr.`membership_registration_id` and  mp.company_id = mr.company_id 
                    WHERE mp.`company_id` = c.`company_id` AND mp.`payment_status`='S' ),0) total_membership_sales, 
                    IFNULL(( SELECT SUM(if(er.processing_fee_type=2,(ep.payment_amount+if(ep.schedule_status='PR',(SELECT SUM(processing_fee) FROM event_payment WHERE event_reg_id=ep.event_reg_id AND checkout_id=ep.checkout_id AND schedule_status IN ('R','PR')),ep.processing_fee)),ep.payment_amount)) Event_Sales
                    from event_payment ep LEFT JOIN event_registration er ON ep.event_reg_id = er.event_reg_id 
                    WHERE er.`company_id` = c.`company_id` AND ep.schedule_status IN ('S', 'PR')),0) total_event_sales, 
                    (select IFNULL(DATE_FORMAT(`last_login_dt`,'%s'),'') from `user` where `company_id` = c.`company_id`) last_login,
                    `company_type` Industry,`web_page` web_site,`phone_number` studio_phone, IFNULL(DATE_FORMAT(`expiry_dt`,'%s'),'') next_payment_date, 
                    u.user_id, u.`user_firstname`, u.`user_lastname`,u.`user_email` email_for_log_in,u.`user_phone` user_phone,
                    ssf.`plan_fee` next_payment_amount
                    FROM `company` c LEFT JOIN user u on c.company_id = u.company_id 
                    LEFT JOIN `studio_subscription_fee` ssf on 1
                    where c.`company_id`!= '6' AND  u.`user_type`='A' AND c.`deleted_flag` != 'Y' AND IF(ssf.company_id = c.company_id,ssf.company_id = ssf.company_id,ssf.company_id = 0) AND ssf.premium_type=c.upgrade_status   %s  ",$datestring,$datestring,$datestring,$s_text);
        $result = mysqli_query($this->db, $sql);
        $output = [];
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
//                    $row['cc_processing'] = $this->getProcessingFeeForAdmin($row["company_id"],$row["upgrade_status"]);
                    $output[] = $row;
                }
            }
        }
        ob_clean();
        ob_start();
        $header = $body = '';
        $header .= "Studio Name,Payment Plan,Registered Date,Account Owner,Stripe Account,Total App Users,Total Event Sales,Total Membership Sales,First Name,Last Name,Phone,Email for Log In,Studio Code,Industry,Next Payment Date,Next Payment Amount,Last Login,Studio Address,Studio Phone,Studio Website\r\n";
        
        for ($row_index = 0; $row_index < $num_rows; $row_index++) {
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['company_name']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['payment_plan']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['registered_date']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['owner_name']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['stripe_account']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['total_app_users']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['total_event_sales']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['total_membership_sales']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['user_firstname']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['user_lastname']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['user_phone']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['email_for_log_in']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['company_code']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['Industry']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['next_payment_date']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['next_payment_amount']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['last_login']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['studio_address']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['studio_phone']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['web_site']) . '"' . "\r\n";
        }
        
        $file = "../../../uploads/fake1.csv";
        $ifp = fopen($file, "w");
        fwrite($ifp, $header);
        fwrite($ifp, $body);
        fclose($ifp);
        header('Content-Type: application/x-download; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        readfile($file);
        unlink($file);
        date_default_timezone_set($curr_time_zone);
        exit();      
    }
    
    public function verifyUserEmailInDbForAdminPanel($user_email,$company_id,$call_back) {
        $query = sprintf("SELECT  user_email from user where user_email='%s' and (company_id!='%s' or user_type!='A')", mysqli_real_escape_string($this->db, $user_email),
                mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
             $num_of_rows = mysqli_num_rows($result);
             if ($num_of_rows > 0) {
                $error = array('status' => "Failed", "msg" => "User email already exists.");
                if($call_back==0){
                    $this->response($this->json($error), 200);
                }else{
                    return 'exist';
                }
            } else {
                $out = array('status' => "Success", 'msg' => "User email available.");
                if($call_back==0){
                    $this->response($this->json($out), 200);
                }else{
                    return 'not_exist';
                }
            }
        }
    }
    

    public function verifyCompanyCodeInDbForAdminPanel($company_code,$company_id,$call_back) {
        $sql = sprintf("SELECT company_code from company where company_code='%s' and company_id!='%s'", mysqli_real_escape_string($this->db, $company_code),
                mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                if ($call_back == 0) {
                    $error = array('status' => "Failed", "msg" => "Company code already exists.");
                    $this->response($this->json($error), 200);
                } else {
                    return 'N';
                }
               
            } else {
                if ($call_back == 0) {
                    $out = array('status' => "Success", 'msg' => "Company code available.");
                    $this->response($this->json($out), 200);
                } else {
                    return 'Y' ;
                }
                
            }
        }
    }
    
    public function downgradeStudioSubscriptionStatus($company_id,$payment_type){
        $premium_monthly_planarr=['B'];
        $premium_yearly_planarr=['B','M'];
        $whitelabel_monthly_planarr=['B','M','P'];
        $whitelabel_yearly_planarr=['B','M','P','WM'];
        $err=0;
        $this->getStripeKeys();
        $curr_date = date("Y-m-d");
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $current_date = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $future_payment_date=$curr_date;
        $stripe_payment_status='old_credit';
        $charge_type='free';
        $stripe_charge_id =$stripe_old_card_id ='';
        $new_calc_pay_amount=$calculated_payment_amount=$temp_outstanding_balance=$outstanding_credit=0;
        $sql = sprintf("select `company_name`, `web_page`, date(`expiry_dt`) expiry, `subscription_status`, `upgrade_status`, `list_item_id`, `contact_id`, `stripe_currency_code`, `stripe_currency_symbol`, s.`stripe_customer_id`, s.`stripe_source_id`,s.`sss_id`,
                IF( ( `upgrade_status` =  'P' || `upgrade_status` =  'W' || `upgrade_status` =  'WM' || `upgrade_status` =  'B' || `upgrade_status` =  'T' || `upgrade_status` =  'F' || `upgrade_status` =  'M' ) ,  DATEDIFF( c.`expiry_dt` , ($current_date) ) , IF( c.`expiry_dt` IS NULL || c.`expiry_dt` =  '0000-00-00 00:00:00', 30 - DATEDIFF( ($current_date) , c.`created_date` ) , DATEDIFF( c.`expiry_dt` , ($current_date) ) ) ) remaining_days,
                s.`outstanding_balance`, s.`next_payment_date`, s.`last_successfull_payment_date`, s.`expiry_date`,c.stripe_last_success_payment
                from `company` c LEFT JOIN `stripe_studio_subscription` s ON c.`company_id` = s.`company_id`
                where c.`company_id`='%s' AND s.deleted_flag!='Y' AND c.`stripe_subscription`='Y' AND s.`subscription_end_date` IS NULL ORDER BY s.sss_id DESC LIMIT 0,1",mysqli_real_escape_string($this->db,$company_id));
        $sql_result = mysqli_query($this->db, $sql);
        if(!$sql_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $sql_num_rows = mysqli_num_rows($sql_result);
            if($sql_num_rows>0){
                $sql_row = mysqli_fetch_assoc($sql_result);
                $company_name = $sql_row['company_name'];
                $expiry = $sql_row['expiry'];
                $sub_status = $sql_row['subscription_status'];
                $upgrade_status = $sql_row['upgrade_status'];
                $stripe_currency_code = $sql_row['stripe_currency_code'];
                $stripe_currency_symbol = $sql_row['stripe_currency_symbol'];
                $customer_id = $sql_row['stripe_customer_id'];
                $card_id = $sql_row['stripe_source_id'];
                $stripe_reg_id = $sql_row['sss_id'];
                $remaining_days = $sql_row['remaining_days'];
                $outstanding_balance = $sql_row['outstanding_balance'];
                $next_payment_date =$sql_row['next_payment_date'];
                $last_successfull_payment_date=$sql_row['last_successfull_payment_date'];
                $st_expiry_date=$sql_row['expiry_date'];
                $stripe_last_success_payment = $sql_row['stripe_last_success_payment'];
            }else{
                $error = array('status' => "Failed", "msg" => "Studio subscription details not found via stripe.");
                $this->response($this->json($error), 200);
            }  
            
            if (!empty($payment_type) && $payment_type == $upgrade_status) {
                $error = array('status' => "Failed", "msg" => "Cannot downgrade to existiing status. Please choose the different plan.");
                $this->response($this->json($error), 200);
            }else if ($upgrade_status == 'M' && !in_array($payment_type, $premium_monthly_planarr)) {                    
                $err=1;
            }else if ($upgrade_status == 'P' && !in_array($payment_type, $premium_yearly_planarr)) {
                $err=1;
            }else if ($upgrade_status == 'WM' && !in_array($payment_type, $whitelabel_monthly_planarr)) {
                $err=1;
            }else if ($upgrade_status == 'W' && !in_array($payment_type, $whitelabel_yearly_planarr)) {
                $err=1;
            }
            if($err == 1) {
                $error = array('status' => "Failed", "msg" => "You cannot upgrade the plan via admin panel. Please choose the different plan to downgrade.");
                $this->response($this->json($error), 200);
            }
            
            //upgrade to new plan first time
            $new_plan_details = $this->getSubscriptionPlanDetails($company_id,$payment_type,1);
            if (!empty($new_plan_details)) {
                $calculated_payment_amount = $new_plan_details['total_due'];
                $future_payment_date = $new_plan_details['next_payment_date'];
                $payment_desc = $new_plan_details['payment_desc'];
                $new_plan_fee = $new_plan_details['plan_fee'];
                $new_plan_setup_fee = $new_plan_details['setup_fee'];
                $outstanding_credit = $new_plan_details['outstanding_credit'];
            } else {
                $error = array('status' => "Failed", "msg" => $new_plan_details['msg']);
                $this->response($this->json($error), 200);
            }
                
            $temp_outstanding_balance = $outstanding_balance+$outstanding_credit;
            if($calculated_payment_amount>1){
                if($temp_outstanding_balance<0){
                    if(-1*$temp_outstanding_balance>$calculated_payment_amount){
                        $new_calc_pay_amount = 0;
                    }else{
                        $new_calc_pay_amount = $calculated_payment_amount+$temp_outstanding_balance;
                    }
                }else{
                    $new_calc_pay_amount = $calculated_payment_amount+$temp_outstanding_balance;
                }
            }else{
                $new_calc_pay_amount = $calculated_payment_amount;
            }
            
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            try {
                if($new_calc_pay_amount>=1){
                    // Use Stripe's library to make requests...            
                    $stripe_charge_create = \Stripe\Charge::create([
                        "amount" => ($new_calc_pay_amount*100),
                        "currency" => "$stripe_currency_code",
                        "customer" => "$customer_id", 
                        "description" => "$payment_desc",
                        "statement_descriptor" => substr($payment_desc,0,21)
                    ]);            
                    $body3 = json_encode($stripe_charge_create);
                    stripe_log_info("Customer_old_update : $body3");
                    $body3_arr = json_decode($body3, true);
                    $stripe_charge_id = $body3_arr['id'];
                    $stripe_old_card_id=$body3_arr['source']['id'];
                    $charge_type=$body3_arr['source']['object'];
                    $stripe_payment_status=$body3_arr['status'];
                    if ($temp_outstanding_balance < 0) {
                        if (-1 * $temp_outstanding_balance > $new_calc_pay_amount) {
                            $temp_outstanding_balance += $new_calc_pay_amount;
                        } else {
                            $temp_outstanding_balance = 0;
                        }
                    } else {
                        $temp_outstanding_balance = 0;
                    }
                }
            } catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
              // Too many requests made to the API too quickly     
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
              // Invalid parameters were supplied to Stripe's API     
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
              // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
              // Network communication with Stripe failed     
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
              // Display a very generic error to the user, and maybe send yourself an email     
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
              // Something else happened, completely unrelated to Stripe     
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
            }
            if(isset($err) && !empty($err)){
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);// If no records "No Content" status
            }
        
            if($stripe_payment_status=="succeeded"||$stripe_payment_status=="captured"||$stripe_payment_status=="old_credit"){
                $insert_table='`stripe_studio_subscription_payments`';
                $last_successfull_pay_date = $curr_date;
                $pay_type=$payment_type;
                $stripe_last_success_payment=$curr_date;
                if($stripe_payment_status=="old_credit"){
                    $temp_outstanding_balance += $new_calc_pay_amount;
                }
            }else{
                $insert_table='`stripe_studio_subscription_payments_history`';
                $pay_type=$upgrade_status;
                $last_successfull_pay_date = $last_successfull_payment_date;
                $temp_outstanding_balance += $new_calc_pay_amount;
            }            
            
            $sql2 = sprintf("update `company` set `expiry_dt`='%s', `studio_expiry_level`='A', `stripe_last_success_payment`='$stripe_last_success_payment', `subscription_status`='%s', `upgrade_status`='%s' where `company_id`='%s'",
                    mysqli_real_escape_string($this->db,$future_payment_date),'Y',mysqli_real_escape_string($this->db,$pay_type),mysqli_real_escape_string($this->db,$company_id));
            $sql_result2 = mysqli_query($this->db, $sql2);
            if(!$sql_result2){
                $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($err_log));
                $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($res), 200);
            }
            
            $update_payment = sprintf("INSERT INTO `stripe_studio_subscription`(`company_id`,`user_id`, `status`, `expiry_date`, `premium_type`, `payment_amount`,`stripe_card_status`,`card_exp_year`,`card_exp_month`, 
                `stripe_customer_id`, `stripe_source_id`, `firstname`, `lastname`, `email`, `phone`, `streetaddress`, `city`, `state`, `country`, `zip`,`stripe_card_name`)
                SELECT `company_id`,`user_id`, `status`, `expiry_date`, `premium_type`, `payment_amount`,`stripe_card_status`,`card_exp_year`,`card_exp_month`, 
                `stripe_customer_id`, `stripe_source_id`, `firstname`, `lastname`, `email`, `phone`, `streetaddress`, `city`, `state`, `country`, `zip`,`stripe_card_name` FROM `stripe_studio_subscription`
                WHERE `sss_id`='%s' AND `stripe_customer_id`='%s' AND `company_id`='%s' AND IF(`premium_type` IN('T','F'),'',`subscription_end_date` IS NULL ) AND `deleted_flag`!='Y'
                GROUP BY `stripe_customer_id`  ORDER BY sss_id DESC LIMIT 0,1", mysqli_real_escape_string($this->db, $stripe_reg_id),mysqli_real_escape_string($this->db, $customer_id),mysqli_real_escape_string($this->db, $company_id));
            $result_update_payment = mysqli_query($this->db, $update_payment);
            if(!$result_update_payment){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_payment");
                log_info($this->json($error_log));
                $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($res), 200);
            }else{
                $new_sss_id = mysqli_insert_id($this->db);
                $update_query = sprintf("UPDATE `stripe_studio_subscription` SET `subscription_end_date`='%s' WHERE `sss_id`='%s' AND `stripe_customer_id`='%s' AND `company_id`='%s' AND `deleted_flag`!='Y' ",
                    $curr_date,mysqli_real_escape_string($this->db, $stripe_reg_id), mysqli_real_escape_string($this->db, $customer_id), mysqli_real_escape_string($this->db, $company_id));
                $result_update_query = mysqli_query($this->db, $update_query);
                if(!$result_update_query){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                    log_info($this->json($error_log));
                    $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($res), 200);
                }else{
                    if(mysqli_affected_rows($this->db)>0){

                       $update_plan=sprintf("UPDATE `stripe_studio_subscription` SET `expiry_date`='%s', `premium_type`='%s',`payment_amount`='%s',`setup_fee`='%s',`first_payment`='%s',`first_payment_date`='%s',`outstanding_balance`='%s',`next_payment_date`='%s',`last_successfull_payment_date`='%s' WHERE `sss_id`='%s' AND `company_id`='%s' ",
                               mysqli_real_escape_string($this->db, $future_payment_date),mysqli_real_escape_string($this->db, $pay_type), mysqli_real_escape_string($this->db, $new_plan_fee),
                               mysqli_real_escape_string($this->db, $new_plan_setup_fee),mysqli_real_escape_string($this->db, $calculated_payment_amount),mysqli_real_escape_string($this->db, $curr_date),mysqli_real_escape_string($this->db, $temp_outstanding_balance), mysqli_real_escape_string($this->db, $future_payment_date),
                               mysqli_real_escape_string($this->db,$last_successfull_pay_date),mysqli_real_escape_string($this->db, $new_sss_id),mysqli_real_escape_string($this->db, $company_id)); 
                        $result_update_plan = mysqli_query($this->db, $update_plan);
                        if(!$result_update_plan){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_plan");
                            log_info($this->json($error_log));
                            $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($res), 200);
                        }else{
                            if(!empty($new_sss_id && $new_sss_id!==0)){
                                $insert_query2 = sprintf("INSERT INTO $insert_table (`company_id`, `sss_id`, `source_id`, `charge_id`,`charge_amount`, `payment_amount`, `charge_status`,`charge_type`) 
                                VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $new_sss_id),
                                        mysqli_real_escape_string($this->db, $stripe_old_card_id), mysqli_real_escape_string($this->db, $stripe_charge_id),
                                        mysqli_real_escape_string($this->db, $new_calc_pay_amount), mysqli_real_escape_string($this->db, $new_plan_fee), mysqli_real_escape_string($this->db, $stripe_payment_status), mysqli_real_escape_string($this->db, $charge_type));
                                $result_query2 = mysqli_query($this->db, $insert_query2);
                                if (!$result_query2) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query2");
                                    log_info($this->json($error_log));
                                    $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($res), 200);
                                }else{
                                    $res = array("status" => "Success", "msg" => "Subscription Successfully Updated.", "new_upgrade_status"=>$pay_type);
                                    log_info($this->json($res));
                                    $this->response($this->json($res), 200);

                                }
                            }else{
                                $res = array("status" => "Failed", "msg" => " Registration Details Not Updated .");
                                log_info($this->json($res));
                                $this->response($this->json($res), 200);
                            }
                        }
                    }else{
                        $res = array("status" => "Failed", "msg" => " Registration Details Not Updated .");
                        log_info($this->json($res));
                        $this->response($this->json($res), 200);
                    }
                }
            }
        }
    }
    
    //send payment details to Stripe API
    protected function getSubscriptionPlanDetails($company_id, $user_payment_type, $call_back){
        
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        
        $curr_date = date("Y-m-d");
        $to_upgrade = 0;
        $upgrade_to=[];
        $sql = sprintf("select date(`expiry_dt`) expiry, `subscription_status`, `upgrade_status` from `company` c where c.`company_id`='%s'",mysqli_real_escape_string($this->db,$company_id));
        $sql_result = mysqli_query($this->db, $sql);
        if(!$sql_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 0) {
                $this->response($this->json($error), 200);
            } else if ($call_back == 1) {
                return $error;
            }
        }else{
            $sql_num_rows = mysqli_num_rows($sql_result);
            if($sql_num_rows>0){
                $sql_row = mysqli_fetch_assoc($sql_result);
                $expiry = $sql_row['expiry'];
                $sub_status = $sql_row['subscription_status'];
                $upgrade_status = $sql_row['upgrade_status'];

                $result_arr=$upgrade_plan=$plan_arr=[];
                $payment_type='';
                $upgrade_to[] = $user_payment_type;

                $subscription_fee=$this->getSubscriptionPlanFee($company_id,$upgrade_status);
                for ($init = 0; $init < count($upgrade_to); $init++) {
                    $payment_type=$upgrade_to[$init];
                    $per_day_payment_sss=$rem_days_sss=$total_days_in_prev_plan=$total_days_in_curr_plan=$plan_fee=$setup_fee=$total_days_in_new_plan=$new_plan_fee=0;
                    $rem_day_payment_sss=0;
                    $new_plan_setup_fee=$credited_days=$calculated_payment_amount_temp=$calculated_payment_amount_temp1=$monthly_rem_day_payment=$monthly_rem_days=$monthly_per_day_payment=0;
                    $new_plan_premium_type=$future_payment_date='';

                    if(!empty($subscription_fee)){
                        $plan_fee=$subscription_fee['current_plan_fee'];
                        $setup_fee=$subscription_fee['current_plan_setup_fee'];
                        $total_days_in_prev_plan=$subscription_fee['current_plan_days'];

                        for($i=0;$i<count($subscription_fee['plans']);$i++){                                
                            if($payment_type == $subscription_fee['plans'][$i]['premium_type']){
                                $new_plan_premium_type = $subscription_fee['plans'][$i]['premium_type'];
                                $new_plan_fee = $subscription_fee['plans'][$i]['plan_fee'];
                                $total_days_in_new_plan = $subscription_fee['plans'][$i]['plan_days'];
                                $new_plan_setup_fee = $subscription_fee['plans'][$i]['setup_fee'];
                                break;
                            }
                        }
                    }else{
                       $error = array('status' => "Failed", "msg" => "Unable to get the payment plan details.");
                       $this->response($this->json($error), 200); 
                    }

                    if ($upgrade_status!=='F' && $upgrade_status!=='T') {  //UPDATE PLAN BEFORE EXPIRY DATE
                            //UPGRADE FROM OLD PLAN
                        $rem_days_sss = (strtotime($expiry) - strtotime($curr_date)) / 86400;
                        $per_day_payment_sss = number_format((float) ($plan_fee / $total_days_in_prev_plan), 2, '.', '');
                        $rem_day_payment_sss = number_format((float) ($per_day_payment_sss * $rem_days_sss), 2, '.', '');


                        if ($payment_type == 'P') {        // P - Premium Plan Yearly from [W,WM]   288-69=219
                            if ( $rem_day_payment_sss > 0) {
                                $calculated_payment_amount_temp = number_format($new_plan_fee - $rem_day_payment_sss, 2, '.', '');// 288-69=219  or 288-588=-300
                                if($calculated_payment_amount_temp < 1) {
                                    $calculated_payment_amount = 0;
                                    $outstanding_credit = number_format($calculated_payment_amount_temp, 2, '.', '');
                                }else {
                                    $outstanding_credit=0;
                                    $calculated_payment_amount = number_format($calculated_payment_amount_temp, 2, '.', '');//69-20=49
                                }
//                                $plan_arr['prorated_fee']=number_format($new_plan_fee-$calculated_payment_amount_temp, 2, '.', '');
                            } else {
                                $calculated_payment_amount_temp = -1*$rem_day_payment_sss+$new_plan_fee;
                                $calculated_payment_amount= number_format($calculated_payment_amount_temp, 2, '.', '');
//                                $plan_arr['prorated_fee']=0;
                            }
                            $future_payment_date = date('Y-m-d', strtotime($curr_date . "+1 year"));
                            $payment_desc = "MyStudio Premium Yearly Membership";
                            $plan_arr['setup_fee']=0;
                        } elseif ($payment_type == 'M'||$payment_type == 'WM'||$payment_type == 'B') {        // P - Premium Plan Monthly   // DOWNGRADE TO PREMIUM MONTHLY
                            $curr_day = date("j");
                            $no_of_days_in_current_month = date("t");           //[P,W,WM]
                            if ($curr_day != '1') {
                                $monthly_rem_days = $no_of_days_in_current_month - $curr_day + 1;//288-69=
                                $monthly_per_day_payment = number_format((float) ($new_plan_fee / 30), 2, '.', '');
                                $monthly_rem_day_payment = number_format((float) ($monthly_per_day_payment * $monthly_rem_days), 2, '.', '');
                                $temp_payment_amount = $monthly_rem_day_payment;
                                $r_prorate = ($new_plan_fee-$monthly_rem_day_payment); //rem days credit=20      //old credit=30=-10
                            } else {
                                $r_prorate = 0;
                                $temp_payment_amount = $new_plan_fee;
                            }

                            //AMOUNT CREDITED FROM PREVIOUS PLAN  
                            if ($rem_day_payment_sss > 0) {//check upgrade Premium Plan Yearly from Premium Plan Monthly
                                $calculated_payment_amount_temp = $temp_payment_amount - $rem_day_payment_sss;    //29-25=4   or 29-250=-221 or 14-250=-236(prorated)  or 0-250=-250 or 29-10=19 or 0.9-0.4=0.5 or 0.87-0.90=-0.03
                                if ($calculated_payment_amount_temp < 1) {
                                    $calculated_payment_amount = 0;      //0.9-30=29.1 ==>29.1+29=1.1
                                    $outstanding_credit=number_format($calculated_payment_amount_temp, 2, '.', '');
                                }else {
                                    $outstanding_credit=0;
                                    $calculated_payment_amount = number_format($calculated_payment_amount_temp, 2, '.', '');
                                }
                                $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
                                $final_prorate = $r_prorate+$rem_day_payment_sss;
//                                $plan_arr['prorated_fee']=number_format($final_prorate, 2, '.', '');
                            } else {                    //AMOUNT CREDITED FROM PREVIOUS PLAN LESS THAN 0
                                $calculated_payment_amount_temp =number_format($rem_day_payment_sss + $temp_payment_amount, 2, '.', '');//0.97
                                if($calculated_payment_amount_temp < 1){
                                    $calculated_payment_amount= number_format($calculated_payment_amount_temp+$new_plan_fee, 2, '.', '');//basic 5-0=
//                                      $calculated_payment_amount = 0;      //0.9-30=29.1 ==>29.1+29=1.1
                                    $outstanding_credit=0;
                                    $future_payment_date = date('Y-m-d', strtotime('first day of second month'));
                                }else{
                                    $outstanding_credit=0;
                                    $calculated_payment_amount= number_format($calculated_payment_amount_temp, 2, '.', '');                                        
//                                    $plan_arr['prorated_fee']=$r_prorate;
                                    $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
                                }                                    
                            }                                
                            $payment_desc = "MyStudio Premium Monthly Membership";
                            $plan_arr['setup_fee']=0;
                        }

                    }else{
                        $error = array('status' => "Failed", "msg" => "Something went wrong in your subscription plan. Please contact your administrator.");
                        $this->response($this->json($error), 200);
                    }                                
                    $plan_arr['plan_type']=$payment_type;
                    $plan_arr['plan_fee']=$new_plan_fee;
                    $plan_arr['total_due']=$calculated_payment_amount;
                    $plan_arr['next_payment_date']=$future_payment_date;
                    $plan_arr['payment_desc']=$payment_desc;
                    $plan_arr['outstanding_credit']=$outstanding_credit;
                    if($payment_type=='W'||$payment_type=='P'||$payment_type=='B'){
                        $upgrade_plan['yearly']=$plan_arr;
                    }else{
                        $upgrade_plan['monthly']=$plan_arr;
                    }
                    $result_arr=$plan_arr;
                }
                if(!empty($upgrade_plan)){
                    if($call_back == 0) {
                        $out = array('status' => "Success", "msg" => $upgrade_plan, "current_status" => $upgrade_status);
                        $this->response($this->json($out), 200);
                    } else if ($call_back == 1 && !empty($result_arr)) {
                        return $result_arr;
                    }else{
                        $error = array('status' => "Failed", "msg" => "Something went wrong in your subscription plan Due Calculation. Please contact your administrator.");
                        return $error;
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => "Something went wrong in your subscription plan Due Calculation. Please contact your administrator.");
                    if($call_back == 0) {                            
                        $this->response($this->json($error), 200);
                    } else if ($call_back == 1) {
                        return $error;
                    }
                }
                
            }else{
                $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
                if ($call_back == 0) {                    
                    $this->response($this->json($error), 200); // If no records "No Content" status
                } else if ($call_back == 1) {
                    return $error;
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
//    protected function getSubscriptionPlanDetails($company_id,$user_payment_type){
//        
//        $user_timezone = $this->getUserTimezone($company_id);
//        $curr_time_zone = date_default_timezone_get();
//        $new_timezone = $user_timezone['timezone'];
//        date_default_timezone_set($new_timezone);
//        $host = $_SERVER['HTTP_HOST'];
//        if (strpos($host, 'mystudio.academy') !== false){
//            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
//        } else {
//            $tzadd_add = "'" . $curr_time_zone . "'";
//        }
//        $current_date = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
//        
//        $curr_date = date("Y-m-d");
//        $upgrade_during_subscription_period = 'N';
//        $to_upgrade = 0;
//        $upgrade_to=[];
//        $sql = sprintf("select `company_name`, `web_page`, date(`expiry_dt`) expiry, `subscription_status`, `upgrade_status`, `list_item_id`, `contact_id`, `stripe_currency_code`, `stripe_currency_symbol`,
//            IF( `subscription_status` =  'Y' && ( `upgrade_status` =  'P' || `upgrade_status` =  'W' || `upgrade_status` =  'WM' || `upgrade_status` =  'B' || `upgrade_status` =  'T' || `upgrade_status` =  'F' || `upgrade_status` =  'M' ) ,  DATEDIFF( c.`expiry_dt` , ($current_date) ) , IF( c.`expiry_dt` IS NULL || c.`expiry_dt` =  '0000-00-00 00:00:00', 30 - DATEDIFF( ($current_date) , c.`created_date` ) , DATEDIFF( c.`expiry_dt` , ($current_date) ) ) ) remaining_days
//                from `company` c 
//                where c.`company_id`='%s'",mysqli_real_escape_string($this->db,$company_id));
//        $sql_result = mysqli_query($this->db, $sql);
//        if(!$sql_result){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//            
//        }else{
//            $sql_num_rows = mysqli_num_rows($sql_result);
//            if($sql_num_rows>0){
//                $sql_row = mysqli_fetch_assoc($sql_result);
//                $expiry = $sql_row['expiry'];
//                $sub_status = $sql_row['subscription_status'];
//                $upgrade_status = $sql_row['upgrade_status'];
//
//                
//                if($to_upgrade==0){
//                    $upgrade_during_subscription_period = 'N';
//                    $result_arr=$upgrade_plan=$plan_arr=[];
//                    $payment_type='';
//                   
//                        $upgrade_to = $user_payment_type;                    
//
//                    $subscription_fee=$this->getSubscriptionPlanFee($company_id,$upgrade_status);
//
//                    for ($init = 0; $init < count($upgrade_to); $init++) {
//                        $payment_type=$upgrade_to[$init];
//                        $per_day_payment_sss=$rem_days_sss=$total_days_in_prev_plan=$total_days_in_curr_plan=$plan_fee=$setup_fee=$total_days_in_new_plan=$new_plan_fee=$outstanding_credit=0;
//                        $rem_day_payment_sss=-1;
//                        $temp_plan_fee=$temp_plan_setup_fee=$temp_plan_days=$new_plan_setup_fee=$credited_days=$calculated_payment_amount_temp=$calculated_payment_amount_temp1=$monthly_rem_day_payment=$monthly_rem_days=$monthly_per_day_payment=0;
//                        $temp_plan_type=$new_plan_premium_type=$future_payment_date='';
//                        
//                        if(!empty($subscription_fee)){
//                            $plan_fee=$subscription_fee['current_plan_fee'];
//                            $setup_fee=$subscription_fee['current_plan_setup_fee'];
//                            $total_days_in_prev_plan=$subscription_fee['current_plan_days'];
//                            
//                            for($i=0;$i<count($subscription_fee['plans']);$i++){
//                                $temp_plan_fee=$subscription_fee['plans'][$i]['plan_fee'];
//                                $tempt_plan_setup_fee=$subscription_fee['plans'][$i]['setup_fee'];
//                                $temp_plan_type=$subscription_fee['plans'][$i]['premium_type'];
//                                $temp_plan_days=$subscription_fee['plans'][$i]['plan_days'];
//                                
//                                if($temp_plan_type==$payment_type){
//                                    $new_plan_fee=$temp_plan_fee;
//                                    $total_days_in_new_plan=$temp_plan_days;
//                                    $new_plan_premium_type=$temp_plan_type;
//                                    $new_plan_setup_fee=$tempt_plan_setup_fee;
//                                    break;
//                                }
//                            }
//                        }else{
//                           $error = array('status' => "Failed", "msg" => "Unable to get the payment plan details.");
//                           $this->response($this->json($error), 200); 
//                        }
//                                    
//                         if ($upgrade_status!=='T' && $upgrade_during_subscription_period=='N') {  //UPDATE PLAN BEFORE EXPIRY DATE
//                                //Downgrade FROM OLD PLAN
//                            $rem_days_sss = (strtotime($expiry) - strtotime($curr_date)) / 86400;
//                            log_info("remaining days in old plan : ".$rem_days_sss." ".$expiry);
//                            $per_day_payment_sss = (float) ($plan_fee / $total_days_in_prev_plan);
//                            $rem_day_payment_sss = number_format((float) ($per_day_payment_sss * $rem_days_sss), 2, '.', '');
//                            log_info("old credit : ".$rem_day_payment_sss);
//                           
//                            
//                                if ($payment_type == 'P') {        // P - Premium Plan Yearly  [W,WM]
//                                if ( $rem_day_payment_sss > 0) {
//                                    $calculated_payment_amount_temp = number_format($new_plan_fee - $rem_day_payment_sss, 2, '.', '');// 288-69=219  or 288-588=-300
//                                    if($calculated_payment_amount_temp < 1) {
////                                          $calculated_payment_amount = number_format($calculated_payment_amount_temp + $new_plan_fee, 2, '.', '');
//                                            $calculated_payment_amount = 0;
//                                            $outstanding_credit=number_format($calculated_payment_amount_temp, 2, '.', '');
//                                        }
////                                        else if($calculated_payment_amount_temp < 1 && $calculated_payment_amount_temp>-1) {
////                                            $calculated_payment_amount = 0;
////                                        }
//                                        else {
//                                            $outstanding_credit=0;
//                                            $calculated_payment_amount = number_format($calculated_payment_amount_temp + $new_plan_fee, 2, '.', '');//69-20=49
//                                        }
//                                    $plan_arr['prorated_fee']=number_format($new_plan_fee -$calculated_payment_amount_temp, 2, '.', '');
//                                } else {
//                                    $calculated_payment_amount_temp = $rem_day_payment_sss+$new_plan_fee;
//                                    $calculated_payment_amount= number_format($calculated_payment_amount_temp, 2, '.', '');
//                                    $plan_arr['prorated_fee']=0;
//                                }
//                                $future_payment_date = date('Y-m-d', strtotime($curr_date . "+1 year"));
//                                $payment_desc = "MyStudio Premium Yearly Membership";
//                                $plan_arr['setup_fee']=0;
//                            } elseif ($payment_type == 'M'||$payment_type == 'WM'||$payment_type == 'B') {        // P - Premium Plan Monthly   // DOWNGRADE TO PREMIUM MONTHLY
//                                $curr_day = date("j");
//                                $no_of_days_in_current_month = date("t");           //[P,W,WM]
//                                if ($curr_day != '1') {
//                                    $monthly_rem_days = $no_of_days_in_current_month - $curr_day + 1;//288-69=
//                                    $monthly_per_day_payment = (float) ($new_plan_fee / 30);
//                                    $monthly_rem_day_payment = number_format((float) ($monthly_per_day_payment * $monthly_rem_days), 2, '.', '');
//                                    $temp_payment_amount = $monthly_rem_day_payment;
//                                    $r_prorate = ($new_plan_fee-$monthly_rem_day_payment); //rem days credit=20      //old credit=30=-10
//                                } else {
//                                    $r_prorate = 0;
//                                    $temp_payment_amount = $new_plan_fee;
//                                }                             //AMOUNT CREDITED FROM PREVIOUS PLAN  
//                                     if ($rem_day_payment_sss > 0) {//check upgrade Premium Plan Yearly from Premium Plan Monthly
//                                        $calculated_payment_amount_temp = $temp_payment_amount - $rem_day_payment_sss;    //29-25=4   or 29-250=-221 or 14-250=-236(prorated)  or 0-250=-250 or 29-10=19 or 0.9-0.4=0.5 or 0.87-0.90=-0.03
//                                        log_info("CPAT : ".$calculated_payment_amount_temp." "."temp pay amt curr plan : ".$temp_payment_amount." "."old credit : ".$rem_day_payment_sss);
//                                        if ($calculated_payment_amount_temp < 1) {
////                                          $calculated_payment_amount = number_format($calculated_payment_amount_temp + $new_plan_fee, 2, '.', '');
//                                            $calculated_payment_amount = 0;      //0.9-30=29.1 ==>29.1+29=1.1
//                                            $outstanding_credit=number_format($calculated_payment_amount_temp, 2, '.', '');
//                                        }else {
//                                            $outstanding_credit=0;
//                                            $calculated_payment_amount = number_format($calculated_payment_amount_temp, 2, '.', '');
//                                        }
//                                        $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
//                                        $final_prorate = $r_prorate+$rem_day_payment_sss;
//                                        $plan_arr['prorated_fee']=number_format($final_prorate, 2, '.', '');
//                                } else {                    //AMOUNT CREDITED FROM PREVIOUS PLAN LESS THAN 0
//                                    $calculated_payment_amount_temp =number_format($temp_payment_amount, 2, '.', '');//0.97
//                                    if($calculated_payment_amount_temp < 1){
//                                        $calculated_payment_amount= number_format($calculated_payment_amount_temp+$new_plan_fee, 2, '.', '');//basic 5-0=
////                                      $calculated_payment_amount = 0;      //0.9-30=29.1 ==>29.1+29=1.1
//                                        $outstanding_credit=0;
//                                        $future_payment_date = date('Y-m-d', strtotime('first day of second month'));
//                                    }else{
//                                        $outstanding_credit=0;
//                                        $calculated_payment_amount= number_format($calculated_payment_amount_temp, 2, '.', '');                                        
//                                        $plan_arr['prorated_fee']=$r_prorate;
//                                        $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
//                                    }                                    
//                                }                                
//                                $payment_desc = "MyStudio Premium Monthly Membership";
//                                $plan_arr['setup_fee']=0;
//                            }
////                            elseif ($payment_type == 'WM') {        //WM - White Label Plan Monthly //need to work
////                                $curr_day = date("j");
////                                $no_of_days_in_current_month = date("t");
////                                $monthly_per_day_payment = (float) ($new_plan_fee / 30);
////                                if ($curr_day != '1') {
////                                    $monthly_rem_days = $no_of_days_in_current_month - $curr_day + 1;
////                                    $monthly_rem_day_payment = number_format((float) ($monthly_per_day_payment * $monthly_rem_days), 2, '.', '');
////                                    $temp_payment_amount = $monthly_rem_day_payment;
////                                    $r_prorate = ($new_plan_fee-$monthly_rem_day_payment); 
////                                } else {
////                                    $r_prorate= 0;
////                                    $temp_payment_amount = $new_plan_fee;
////                                }
////                                if ($rem_day_payment_sss > 0) {   //DOWNGRADE NEW PLAN IN BETWEEN DAYS OF MONTH
////
////                                    $calculated_payment_amount_temp1 = $temp_payment_amount - $rem_day_payment_sss; ///(30+199)-250=-21     //else (30+199)-100=129
////
////                                    if($calculated_payment_amount_temp1<1){
//////                                        $per_day_cost=$new_plan_fee/$total_days_in_new_plan;//69/30=2.3
////                                        $credit_days=abs($calculated_payment_amount_temp1)/$monthly_per_day_payment;//21/2.3 = 8.69days
////                                        $prorate_days=$total_days_in_new_plan-$credit_days;
////                                        $prorate_days_payment = number_format((float) ($monthly_per_day_payment * $prorate_days), 2, '.', '');
////                                        $calculated_payment_amount_temp11=$prorate_days_payment+ $new_plan_setup_fee;
////
////                                        $calculated_payment_amount= number_format($calculated_payment_amount_temp11, 2, '.', '');
////                                        $future_payment_date = date('Y-m-d', strtotime('first day of second month'));
////                                        $final_prorate =$r_prorate + $rem_day_payment_sss;
////                                        $plan_arr['prorated_fee']=number_format($final_prorate, 2, '.', '');
////                                    }else{
////                                        $calculated_payment_amount_temp11 = number_format($calculated_payment_amount_temp1+$new_plan_setup_fee, 2, '.', ''); 
////
////                                        $calculated_payment_amount= number_format($calculated_payment_amount_temp11, 2, '.', '');
////                                        $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
////                                        $final_prorate = $r_prorate + $rem_day_payment_sss;
////                                        $plan_arr['prorated_fee']=number_format($final_prorate, 2, '.', '');
////                                    }
//////                                    $plan_arr['prorated_fee']=number_format($new_plan_fee + $new_plan_setup_fee -$calculated_payment_amount_temp11, 2, '.', '');
////                                } else {                                    //UPGRADE NEW PLAN IN FIRST DAY OF MONTH
////                                    $calculated_payment_amount_temp1 = $temp_payment_amount  ;
////
////                                    if($calculated_payment_amount_temp1<1){
//////                                        $per_day_cost=$new_plan_fee/$total_days_in_new_plan;
////                                        $credit_days=abs($calculated_payment_amount_temp1)/$monthly_per_day_payment;
////                                        $prorate_days=$total_days_in_new_plan-$credit_days;
////                                        $prorate_days_payment = number_format((float) ($monthly_per_day_payment * $prorate_days), 2, '.', '');
////                                        $calculated_payment_amount_temp11=$prorate_days_payment+ $new_plan_setup_fee;
////
////                                        $calculated_payment_amount= number_format($calculated_payment_amount_temp11, 2, '.', '');
////                                        $future_payment_date = date('Y-m-d', strtotime('first day of second month'));
////                                        $final_prorate = $r_prorate + $rem_day_payment_sss;
////                                        $plan_arr['prorated_fee']=number_format($final_prorate, 2, '.', '');
////                                    }else{
////                                        $calculated_payment_amount_temp11 = number_format($calculated_payment_amount_temp1+$new_plan_setup_fee, 2, '.', '');
////
////                                        $calculated_payment_amount= number_format($calculated_payment_amount_temp11, 2, '.', '');
////                                        $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
////                                        $final_prorate = $r_prorate + $rem_day_payment_sss;
////                                        $plan_arr['prorated_fee']=number_format($final_prorate, 2, '.', '');
////                                    }
////                                }
////
////                                $payment_desc = "MyStudio Whitelabel Monthly Membership";
////                                $plan_arr['setup_fee']=$new_plan_setup_fee;
//                                 
////                            }else if ($payment_type == 'B') {        // P - Premium Plan Monthly
////                                $curr_day = date("j");
////                                $no_of_days_in_current_month = date("t");
////                                if ($curr_day != '1') {
////                                    $monthly_rem_days = $no_of_days_in_current_month - $curr_day + 1;
////                                    $monthly_per_day_payment = (float) ($new_plan_fee / 30);
////                                    $monthly_rem_day_payment = number_format((float) ($monthly_per_day_payment * $monthly_rem_days), 2, '.', '');
////                                    $calculated_payment_amount_temp = $monthly_rem_day_payment;
////                                    if ($calculated_payment_amount_temp < 1) {
////                                        $calculated_payment_amount = number_format($calculated_payment_amount_temp + $new_plan_fee, 2, '.', '');
////                                        $future_payment_date = date('Y-m-d', strtotime('first day of second month'));
////                                    } else {
////                                        $calculated_payment_amount = number_format($calculated_payment_amount_temp, 2, '.', '');
////                                        $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
////                                    }
////                                    $plan_arr['prorated_fee'] = number_format($new_plan_fee - $calculated_payment_amount_temp, 2, '.', '');
////                                } else {
////                                    $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
////                                    $calculated_payment_amount = $new_plan_fee;
////                                    $plan_arr['prorated_fee'] = 0;
////                                }
////                                $payment_desc = "MyStudio Premium Monthly Membership";
////                                $plan_arr['setup_fee'] = 0;
////                            }
//                        }else{
//                            $error = array('status' => "Failed", "msg" => "Something went wrong in your subscription plan. Please contact your administrator.");
//                            $this->response($this->json($error), 200);
//                        }                                
//                        $plan_arr['plan_type']=$payment_type;
//                        $plan_arr['plan_fee']=$new_plan_fee;
//                        $plan_arr['total_due']=$calculated_payment_amount;
//                        $plan_arr['next_payment_date']=$future_payment_date;
//                        $plan_arr['payment_desc']=$payment_desc;
//                        $plan_arr['outstanding_credit']=$outstanding_credit;
//                        if($payment_type=='W'||$payment_type=='P'||$payment_type=='B'){
//                            $upgrade_plan['yearly']=$plan_arr;
//                        }else{
//                            $upgrade_plan['monthly']=$plan_arr;
//                        }
//                        $result_arr=$plan_arr;
//                    }
//                    if(!empty($upgrade_plan)){
//                        log_info(json_encode($result_arr));
//                        return $result_arr;
////                            $out = array('status' => "Success", "msg" => $result_arr, "current_status" => $upgrade_status);
////                            $this->response($this->json($out), 200);                        
//                    }else{
//                            $error = array('status' => "Failed", "msg" => "Something went wrong in your subscription plan Due Calculation. Please contact your administrator.");
//                            $this->response($this->json($error), 200);
//                        
//                    }
//                }else{
//                        $error = array('status' => "Failed", "msg" => "Cannot upgrade plan. Please contact your administrator.");
//                        $this->response($this->json($error), 200);
//                    
//                }
//            }else{
//                    $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
//                    $this->response($this->json($error), 200); // If no records "No Content" status
//                
//            }
//        }
//        date_default_timezone_set($curr_time_zone);
//    }
    
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
     private function getSubscriptionPlanFee($company_id,$upgrade_status){
        $plan_fee_arr=$plan_fee_arr_final=[];
        $plan_fee_arr_final['current_plan_fee']=$plan_fee_arr_final['current_plan_days']=$plan_fee_arr_final['current_plan_setup_fee']=0;
            $select_fee=sprintf("SELECT * FROM `studio_subscription_fee` WHERE 
                IF((SELECT count(*) count FROM `studio_subscription_fee` WHERE `company_id`='%s')=0,`company_id`=0,
                   (`company_id`='%s' or (`company_id`=0 and `premium_type` not in (select `premium_type` from studio_subscription_fee WHERE `company_id`='%s' AND `fee_end_date` IS NULL))))",
                    mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $company_id));
        $result=mysqli_query($this->db,$select_fee);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_fee");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $nums_rows = mysqli_num_rows($result);
            if ($nums_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $plan_fee_arr['plan_fee'] = $rows['plan_fee']+0;
                    $plan_fee_arr['setup_fee'] = $rows['setup_fee']+0;
                    $plan_fee_arr['premium_type'] = $rows['premium_type'];
                    $plan_fee_arr['plan_days'] = $rows['plan_days'];
                    if($plan_fee_arr['premium_type']==$upgrade_status){
                        $plan_fee_arr_final['current_plan_fee']=$plan_fee_arr['plan_fee'];
                        $plan_fee_arr_final['current_plan_days']=$plan_fee_arr['plan_days'];
                        $plan_fee_arr_final['current_plan_setup_fee']=$plan_fee_arr['setup_fee'];
                    }
                    $plan_fee_arr_final['plans'][]=$plan_fee_arr;
                }
            }
        }        
        return $plan_fee_arr_final;
    }
    
    public function getStudioSalesDetails($company_id){
        $sql = sprintf("SELECT c.`wp_currency_symbol`, IFNULL((select SUM(IF( mr.processing_fee_type = 2, mp.`payment_amount` + mp.`processing_fee`, mp.`payment_amount` ))  
                    from `membership_payment` mp LEFT JOIN  membership_registration mr ON mp.`membership_registration_id` = mr.`membership_registration_id` and  mp.company_id = mr.company_id 
                    WHERE mp.`company_id` = c.`company_id` AND mp.`payment_status`='S' ),0) total_membership_sales, 
                    IFNULL(( SELECT SUM(if(er.processing_fee_type=2,(ep.payment_amount+if(ep.schedule_status='PR',(SELECT SUM(processing_fee) FROM event_payment WHERE event_reg_id=ep.event_reg_id AND checkout_id=ep.checkout_id AND schedule_status IN ('R','PR')),ep.processing_fee)),ep.payment_amount)) Event_Sales
                    from event_payment ep LEFT JOIN event_registration er ON ep.event_reg_id = er.event_reg_id 
                    WHERE er.`company_id` = c.`company_id` AND ep.schedule_status IN ('S', 'PR')),0) total_event_sales FROM `company` c 
                    WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows= mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $msg = array('status' => "Success", 'msg' => $row);
                $this->response($this->json($msg), 200);
            }else{
                $error = array("status" => "Failed", "msg" => "Company details does not exist.");
                $this->response($this->json($error), 200);
            }
        }
    }
}

?>