<?php

header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: origin, x-requested-with, Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
header('Access-Control-Allow-Credentials: true');

require_once '../../../Globals/cont_log.php';
require_once 'Salesforce.php';
require_once 'PaySimple.php';
require_once 'WePay.php';
require_once '../../Stripe/init.php';
require_once('../../PHPMailer_5.2.1/class.phpmailer.php');
require_once('../../PHPMailer_5.2.1/class.smtp.php');
require_once 'tfpdf.php';
require_once __DIR__.'/../../../aws/aws.phar';
	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;
class PortalModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $sf;
    private $ps;
    private $list_id = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_redirect_uri = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $stripe_pk = '', $stripe_sk = '', $mystudio_account = '';
    private $env;
    private $server_url;

    public function __construct() {
        require_once '../../../Globals/config.php';
        require_once '../../../Globals/stripe_props.php';
        $this->inputs();
        $this->sf = new Salesforce();
        $this->ps = new PaySimple();
        $this->wp = new wepay();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->list_id = $this->sf->list_id;
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = "../../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'dev.mystudio.academy') !== false || strpos($host, 'dev2.mystudio.academy') !== false){
            $this->env = 'D';
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->env = 'P';
        }else{
            $this->env = 'D';
        }
        if (strpos($host, 'dev.mystudio.academy') !== false) {
            $this->server_url = "http://dev.mystudio.academy";
        } elseif (strpos($host, 'dev2.mystudio.academy') !== false) {
            $this->server_url = "http://dev2.mystudio.academy";
        } elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'];
        } else {
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithPaysimple($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithSalesforce($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithCampaignEmail($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
        header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');
        
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    /* login functionality using email_id */

    protected function checklogin($email, $password) {

        if(isset($_SESSION['admin'])){
           $sql = sprintf("SELECT user_id,user_email,user_password,company_id,deleted_flag from user where user_email='%s' limit 0,1", mysqli_real_escape_string($this->db,$email));
        }else{
            $password = md5($password);
            $sql = sprintf("SELECT user_id,user_email,user_password,company_id,deleted_flag from user where user_email='%s' and user_password='%s' limit 0,1", mysqli_real_escape_string($this->db,$email), mysqli_real_escape_string($this->db,$password));
        }
        $result = mysqli_query($this->db, $sql);
        $company_id = "";
        $user_id = "";

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    $company_id = $row['company_id'];
                    $user_id = $row['user_id'];
                    $pwd = $row['user_password'];
                    $deleted_flag = $row['deleted_flag'];
                }
                if($deleted_flag=='Y'){
                    $error = array('status' => "Failed", "msg" => "User account doesn't exist.");
                    $this->response($this->json($error), 200);       // If "Company id" not available
                }
                $_SESSION['user'] = $email;
                $_SESSION['pwd'] = $pwd;
                $error_log = array('status' => "Session", "msg" => $_SESSION['user']);
                log_info($this->json($error_log));
                if (!empty($company_id)) {
                    $update_lastlogin = sprintf("UPDATE `user` SET `last_login_dt`= `new_login_dt`,`new_login_dt`=NOW() WHERE `user_email`='%s'", mysqli_real_escape_string($this->db,$email));
                    $update_result = mysqli_query($this->db, $update_lastlogin);
                    //Print Company details after login success
                    $this->getcompanydetails($company_id, $user_id, 0); //0 - No call to Salesforce Api
                } else {
                    $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
                    $this->response($this->json($error), 200);       // If "Company id" not available
                }
            } else {
                $error = array('status' => "Login Failed", "msg" => "We could not find that account. Incorrect email and / or password");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    protected function checkadminlogin($email, $password) {
//        session_start();
        $password = md5($password);
        $current_time = gmdate("Y-m-d H:i:s");
        $sql = sprintf("SELECT user_id,user_email,user_password,user.company_id,user.deleted_flag,company.`company_name` from user LEFT JOIN `company` ON user.company_id = company.company_id where user_email='%s' and user_password='%s' and user_type='S' limit 0,1", mysqli_real_escape_string($this->db,$email), mysqli_real_escape_string($this->db,$password));
        $result = mysqli_query($this->db, $sql);
        
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $deleted_flag = $row['deleted_flag'];
                $company_id=$row['company_id'];
                $admin_company_name=$row['company_name'];
                if($deleted_flag=='Y'){
                    $error = array('status' => "Failed", "msg" => "User account doesn't exist.");
                    $this->response($this->json($error), 200);       // If "Company id" not available
                }
                $_SESSION['admin'] = $email;
                $error_log = array('status' => "Session", "msg" => $_SESSION['admin']);
                log_info($this->json($error_log));
                $update_lastlogin = sprintf("UPDATE `user` SET `last_login_dt`= `new_login_dt`,`new_login_dt`=NOW() WHERE `user_email`='%s'", mysqli_real_escape_string($this->db,$email));
                $update_result = mysqli_query($this->db, $update_lastlogin);
                $error = array('status' => "Success", "msg" => "Login success","company_id"=>$company_id,"company_name"=>"$admin_company_name");
                $this->response($this->json($error), 200);
            } else {
                $error = array('status' => "Login Failed", "msg" => "We could not find that account. Incorrect email and / or password");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    protected function logoutUser() {
        
        if(isset($_SESSION['user'])){
            $log = array('status' => "Success", "msg" => "Logged-out Successfully.", "sess" => $_SESSION['user']);
            log_info($this->json($log));
            if(isset($_SESSION['admin'])){
                unset($_SESSION['user']);
                unset($_SESSION['pwd']);
            }else{
                session_unset();     // unset $_SESSION variable for the run-time 
                session_destroy();   // destroy session data in storage
            }
            $error = array('status' => "Success", "msg" => "Logged-out Successfully.");
            $this->response($this->json($error), 200);
        }else{
            $log = array('status' => "Success", "msg" => "Session already expired.");
            log_info($this->json($log));
            $error = array('status' => "Success", "msg" => "Logged-out Successfully.");
            $this->response($this->json($error), 200);
        }
    }
    
    protected function logoutAdmin() {
        if(isset($_SESSION['admin'])){
            $log = array('status' => "Success", "msg" => "Logged-out Successfully.", "sess" => $_SESSION['admin']);
            log_info($this->json($log));
            session_unset();     // unset $_SESSION variable for the run-time 
            session_destroy();   // destroy session data in storage
            $error = array('status' => "Success", "msg" => "Logged-out Successfully.");
            $this->response($this->json($error), 200);
        }else{
            $log = array('status' => "Success", "msg" => "Session already expired.");
            log_info($this->json($log));
            $error = array('status' => "Success", "msg" => "Logged-out Successfully.");
            $this->response($this->json($error), 200);
        }
    }

    // Get Company Details By Id
    protected function getcompanydetails($company_id, $c_user_id, $checkSFUpdate) {   //$checkSFUpdate => 0 - No call to Salesforce Api, 1 - call to Salesforce Api
        $this->getStripeKeys();
        $stripe_publish_key=$this->stripe_pk;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $upgrade_status = '';
        $current_date = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $sql = sprintf("SELECT c.whilelabel_setup_page_access,w.bundleid_error_flag,w.credential_error_flag,w.teamid_error_flag,w.metadata_error,w.ios_icon_error,w.android_icon_error,w.ios_screenshot_error,w.android_screenshot_error
            ,c.`company_id`,c.`company_name`, `logo_URL`, `logo_content`,       UNIX_TIMESTAMP(c.`created_date`) created_date ,DATE(c.`created_date`) AS studio_created_date , DATE(c.`expiry_dt`)as expiry_dt, `company_code`, `event_flag`, `twitter_id`, `twitter_url`,
            `facebook_id`, `facebook_url`, `instagram_id`, `instagram_url`, `eventbrite_id`, `eventbrite_url`, `vimeo_id`, `vimeo_url`, 
            `youtube_id`, `youtube_url`,`web_page`,`email_id`,`phone_number`,`street_address`,c.`city`,c.`state`,c.`postal_code`,c.`country`,c.`timezone`,
            `curriculum_name`,`event_name`,`message_name`,`trial_name`,`membership_name`,`referral_email_list`,`referral_message`,`google_url`,`yelp_url`,`class_schedule`,`company_type`,`registration_complete`,
            `subscription_status`, `upgrade_status`, 'Y' as verification_complete,
            IF( `subscription_status` =  'Y' && ( `upgrade_status` =  'P' || `upgrade_status` =  'W' || `upgrade_status` =  'M'|| `upgrade_status` =  'WM'|| `upgrade_status` =  'B' ) ,  DATEDIFF( c.`expiry_dt` , ($current_date) ) , IF( c.`expiry_dt` IS NULL || c.`expiry_dt` =  '0000-00-00 00:00:00', 30 - DATEDIFF( ($current_date) , c.`created_date` ) , DATEDIFF( c.`expiry_dt` , ($current_date) ) ) ) remaining_days,
            IF( `subscription_status` =  'Y' && ( `upgrade_status` =  'P' || `upgrade_status` =  'W' || `upgrade_status` =  'M'|| `upgrade_status` =  'WM'|| `upgrade_status` =  'B' ) ,  'N', IF( IF( c.`expiry_dt` IS NULL || c.`expiry_dt` =  '0000-00-00 00:00:00', DATEDIFF( ($current_date) , c.`created_date` ) , IF( DATEDIFF( c.`expiry_dt` , ($current_date) )>=0, DATEDIFF( c.`expiry_dt` , ($current_date) ), 31) ) >30,  'Y',  'N' ) ) is_expired,
            IFNULL(ua.`last_payment_status`,'Success') last_payment_status, wp.`wp_user_state`, wp.`account_state`, wp.`action_reasons`, wp.`disabled_reasons`, expiry_msg_flag, wp_currency_code, wp_currency_symbol,c.`studio_expiry_level`,
            DATE(sss.`created_date`)as plan_start_date, sss.`premium_type` as stripe_upgrade_status, sss.`outstanding_balance`,c.`wepay_status`,c.`stripe_status`,c.`stripe_subscription`,DATE(IFNULL(u.last_login_dt,CURDATE())) as last_login_date, c.`owner_name`, sa.`charges_enabled` as stripe_charges_enabled 
            FROM `company` c 
            LEFT JOIN `user_account` ua ON ua.`company_id` = c.`company_id` AND DATE(c.`expiry_dt`) = ua.`expiry_dt` AND ua.`status` = 'Active'
            LEFT JOIN `wp_account` wp ON wp.`company_id` = c.`company_id`             
            LEFT JOIN `stripe_studio_subscription` sss ON sss.`company_id` = c.`company_id` AND DATE(c.`expiry_dt`) = sss.`expiry_date` AND sss.`status` = 'A' AND sss.`subscription_end_date` IS NULL 
            LEFT JOIN `user` u ON u.`company_id`= c.`company_id`
            LEFT JOIN `stripe_account` sa ON sa.`company_id`= c.`company_id`
            LEFT JOIN `white_label_apps` w ON w.`company_id`= c.`company_id`
            WHERE c.`company_id`='%s' AND u.user_id='%s'", $company_id, mysqli_real_escape_string($this->db,$c_user_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $output = mysqli_fetch_assoc($result);
                $expiry_dt = $output['expiry_dt'];
                $upgrade_status = $output['upgrade_status'];
                $expiry_msg_flag = $output['expiry_msg_flag'];
                $stripe_subscription = $output['stripe_subscription'];
                $country = $output['country'];
                if($upgrade_status=='T' || $upgrade_status=='F'){
                    $update_expiry_msg = sprintf("UPDATE `company` SET `expiry_msg_flag` = IF(`expiry_msg_flag`>=10,`expiry_msg_flag`,`expiry_msg_flag`+1) WHERE company_id='%s'", $company_id);
                    $result_expiry_msg = mysqli_query($this->db, $update_expiry_msg);
                    if(!$result_expiry_msg){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_expiry_msg");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
                
                $studio_discount_available = 'N';
                $check_offer = sprintf("SELECT * FROM `studio_subscription_fee` WHERE plan_offer_fee>0 AND 
                        IF((SELECT count(*) count FROM `studio_subscription_fee` WHERE `company_id`='%s')=0,`company_id`=0, 
                        (`company_id`='%s' or (`company_id`=0 and `premium_type` not in (select `premium_type` from studio_subscription_fee WHERE `company_id`='%s' AND `fee_end_date` IS NULL))))",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
                $result_offer = mysqli_query($this->db, $check_offer);
                if(!$result_offer){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_offer");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $offer_num_rows = mysqli_num_rows($result_offer);
                    if($offer_num_rows>0 && $upgrade_status=='T'){
                        $studio_discount_available = 'Y';
                    }
                }
//                if (($expiry_dt === NULL)||($expiry_dt >= $current_date)) {
                    $sql1 = sprintf("SELECT student_id from student where company_id='%s' AND `deleted_flag`!='Y'  limit 0,1", $company_id);
                    $result1 = mysqli_query($this->db, $sql1);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $num_of_rows1 = mysqli_num_rows($result1);
                        if ($num_of_rows1 > 0) {
                            $student_output = mysqli_fetch_assoc($result1);
                            $output['student_id'] = $student_output['student_id'];
                        } else {
                            $output['student_id'] = "failure";
//                           $error = array('status' => "Failed", "msg" => "Company expired, please contact your administrator.");
//                           $this->response($this->json($error), 200);
                        }
                    }
                    $output['wp_client_id'] = $this->wp_client_id;
                    $output['wp_level'] = $this->wp_level;
                    $process_fee = $this->getProcessingFee($company_id,$output['upgrade_status']);
                    $subscription_fee = $this->getSubscriptionPlanFee($company_id,$output['upgrade_status']);
                    $country_payment_specific = $this->checkCountryPaymentSupport($company_id,$country,1); // callback - 1/0
                    $output['processing_fee_percentage'] = $process_fee['PP'];
                    $output['processing_fee_transaction'] = $process_fee['PT'];
                    $output['user_id'] = $c_user_id;
                    $output['plan_processing_fee']=$process_fee;
                    $output['subscription_fee']=$subscription_fee;
                    $output['stripe_publish_key']=$stripe_publish_key;
                    $output['studio_discount_available'] = $studio_discount_available;
                    $output['stripe_subscription'] = $stripe_subscription;
                    $output['stripe_country_support'] = $stripe_country_support = ($country_payment_specific['status'] == 'Success' && $country_payment_specific['support_status'] =='Y' && $country_payment_specific['payment_support'] =='S') ? "Y":"N";  // CHECK COUNTRY SUPPORT STATUS FOR STRIPE
                    //set default logo url when Logo is empty
                    if(empty($output['logo_URL'])){
                        $output['logo_URL'] = $this->getDefaultLogoImage();
                    }
                    $out = array('status' => "Success", 'msg' => $output);
                    if($checkSFUpdate==1){
                        $this->responseWithSalesforce($this->json($out), 200);
                        $this->updateCompanyDetailsinSalesforceApi($company_id);
                    }else{
                        $this->responseWithCampaignEmail($this->json($out), 200);
                    }
                    if($upgrade_status=='F' || $upgrade_status=='B'){
                        $this->checkAndUpdateCampaignEmailStatusForUpgradeStatus($company_id);
                    }
//                } else {
//                    $error = array('status' => "Failed", "msg" => "Company expired, please contact your administrator.");
//                    $this->response($this->json($error), 200);
//                }
                //print_r($output);
                // If success everythig is good send header as "OK" and user details
            } else {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
            date_default_timezone_set($curr_time_zone);
        }
    }
 
//    private function getProcessingFee($upgrade_status){
//        $temp=[];
//        $x=0;
//        $file_handle = fopen($this->processing_fee_file_name, "r");
//        while (!feof($file_handle)) {
//            $lines = fgets($file_handle);
//            $line_contents = explode(",", $lines);
//            for($i=0;$i<count($line_contents);$i++){                
//                $cnt = explode("=", $line_contents[$i]);
//                $temp[$x][$cnt[0]] = $cnt[1];
//            }
//            $x++;
//        }
//        fclose($file_handle);
//        
//        for($i=0;$i<count($temp);$i++){
//            if($temp[$i]['level']==$upgrade_status){
//                return $temp[$i];
//            }
//        }
//    }
    
    private function getProcessingFee($company_id, $upgrade_status) {
        $temp = [];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction`,`level` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                $sql1 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction`,`level` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result1 = mysqli_query($this->db, $sql1);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result1);
                    if ($num_of_rows > 0) {
                        $pf_all = [];
                        while ($row_1 = mysqli_fetch_assoc($result1)) {
                            $level = $row_1['level'];
                            if ($level == 'F') {
                                $pf_all['Free_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Free_processing_transaction'] = $row_1['cc_processing_transaction'];
                                if($level==$upgrade_status){
                                    $pf_all['Free_processing_percentage'] = $temp['PP'];
                                    $pf_all['Free_processing_transaction'] = $temp['PT'];
                                }
                            }else if ($level == 'B') {
                                $pf_all['Basic_monthly_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Basic_monthly_processing_transaction'] = $row_1['cc_processing_transaction'];
                                if($level==$upgrade_status){
                                    $pf_all['Basic_monthly_processing_percentage'] = $temp['PP'];
                                    $pf_all['Basic_monthly_processing_transaction'] = $temp['PT'];
                                }
                            } else if ($level == 'M') {
                                $pf_all['Premium_monthly_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Premium_monthly_processing_transaction'] = $row_1['cc_processing_transaction'];
                                if($level==$upgrade_status){
                                    $pf_all['Premium_monthly_processing_percentage'] = $temp['PP'];
                                    $pf_all['Premium_monthly_processing_transaction'] = $temp['PT'];
                                }
                            } else if ($level == 'P') {
                                $pf_all['Premium_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Premium_processing_transaction'] = $row_1['cc_processing_transaction'];
                                if($level==$upgrade_status){
                                    $pf_all['Premium_processing_percentage'] = $temp['PP'];
                                    $pf_all['Premium_processing_transaction'] = $temp['PT'];
                                }
                            } else if ($level == 'T') {
                                $pf_all['Trail_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Trail_processing_transaction'] = $row_1['cc_processing_transaction'];
                                if($level==$upgrade_status){
                                    $pf_all['Trail_processing_percentage'] = $temp['PP'];
                                    $pf_all['Trail_processing_transaction'] = $temp['PT'];
                                }
                            }else if ($level == 'WM') {
                                $pf_all['Whitelabel_monthly_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Whitelabel_monthly_processing_transaction'] = $row_1['cc_processing_transaction'];
                                if($level==$upgrade_status){
                                    $pf_all['Whitelabel_monthly_processing_percentage'] = $temp['PP'];
                                    $pf_all['Whitelabel_monthly_processing_transaction'] = $temp['PT'];
                                }
                            } else if ($level == 'W') {
                                $pf_all['Whitelabel_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Whitelabel_processing_transaction'] = $row_1['cc_processing_transaction'];
                                if($level==$upgrade_status){
                                    $pf_all['Whitelabel_processing_percentage'] = $temp['PP'];
                                    $pf_all['Whitelabel_processing_transaction'] = $temp['PT'];
                                }
                            }
                        }
                        $temp['all_processing_fee'] = $pf_all;
                    }
                }

                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction`,`level` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        $pf_all = [];
                        while ($row_1 = mysqli_fetch_assoc($result2)) {
                            $level = $row_1['level'];
                            if ($level == 'F') {
                                $pf_all['Free_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Free_processing_transaction'] = $row_1['cc_processing_transaction'];
                            }else if ($level == 'B') {
                                $pf_all['Basic_monthly_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Basic_monthly_processing_transaction'] = $row_1['cc_processing_transaction'];
                            } else if ($level == 'M') {
                                $pf_all['Premium_monthly_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Premium_monthly_processing_transaction'] = $row_1['cc_processing_transaction'];
                            } else if ($level == 'P') {
                                $pf_all['Premium_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Premium_processing_transaction'] = $row_1['cc_processing_transaction'];
                            } else if ($level == 'T') {
                                $pf_all['Trail_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Trail_processing_transaction'] = $row_1['cc_processing_transaction'];
                            }else if ($level == 'WM') {
                                $pf_all['Whitelabel_monthly_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Whitelabel_monthly_processing_transaction'] = $row_1['cc_processing_transaction'];
                            } else if ($level == 'W') {
                                $pf_all['Whitelabel_processing_percentage'] = $row_1['cc_processing_percentage'];
                                $pf_all['Whitelabel_processing_transaction'] = $row_1['cc_processing_transaction'];
                            }
                            
                            if($level==$upgrade_status){
                                $temp['PP'] = $row_1['cc_processing_percentage'];
                                $temp['PT'] = $row_1['cc_processing_transaction'];
                            }
                        }
                        $temp['all_processing_fee'] = $pf_all;
                    }
                }
                return $temp;
            }
        }
    }

    //Update Company
    protected function updateCompanyDetails($image_update, $image_url, $fileInput, $company_code, $company_name, $web_page, $street_address, $city, $state, $postal_code, $country,$timezone, $phone_number, $email_id, $cs_file_url, $class_schedule_update, $company_type, $company_id,$user_id, $old_logo_url) {
        if($email_id == '' || is_null($email_id)) {
            $error = array('status' => "Failed", "msg" => "Email address should not be empty");
            $this->response($this->json($error), 200);
        }
        if ($image_update === 'Y' || $class_schedule_update === 'Y') {
            if ($image_update === 'Y') {
                if ($fileInput != '') {
                    $update_company = sprintf("UPDATE `company` SET `company_name`='%s', `logo_URL`='%s',`logo_content`='%s', `company_code`='%s',`web_page`='%s', `phone_number`='%s',`email_id`='%s', `street_address`='%s',`city`='%s', `state`='%s',`postal_code`='%s', `country`='%s' ,`timezone`='%s', `company_type`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_name), $image_url, $fileInput, mysqli_real_escape_string($this->db, $company_code), mysqli_real_escape_string($this->db, $web_page), $phone_number, mysqli_real_escape_string($this->db, $email_id), mysqli_real_escape_string($this->db, $street_address), mysqli_real_escape_string($this->db, $city), mysqli_real_escape_string($this->db, $state), $postal_code, mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $timezone), mysqli_real_escape_string($this->db, $company_type), $company_id);
                    $result = mysqli_query($this->db, $update_company);
                } else {
                    $update_company = sprintf("UPDATE `company` SET `company_name`='%s', `logo_URL`=NULL, `logo_content`=NULL, `company_code`='%s',`web_page`='%s', `phone_number`='%s',`email_id`='%s', `street_address`='%s',`city`='%s', `state`='%s',`postal_code`='%s', `country`='%s' ,`timezone`='%s', `company_type`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_name), mysqli_real_escape_string($this->db, $company_code), mysqli_real_escape_string($this->db, $web_page), $phone_number, mysqli_real_escape_string($this->db, $email_id), mysqli_real_escape_string($this->db, $street_address), mysqli_real_escape_string($this->db, $city), mysqli_real_escape_string($this->db, $state), $postal_code, mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $timezone), mysqli_real_escape_string($this->db, $company_type), $company_id);
                    $result = mysqli_query($this->db, $update_company);
                }
            }

            if ($class_schedule_update === 'Y') {

                if ($cs_file_url != '') {
                    $update_company = sprintf("UPDATE `company` SET `company_name`='%s', `class_schedule`='%s', `company_code`='%s',`web_page`='%s', `phone_number`='%s',`email_id`='%s', `street_address`='%s',`city`='%s', `state`='%s',`postal_code`='%s', `country`='%s',`timezone`='%s', `company_type`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_name), $cs_file_url, mysqli_real_escape_string($this->db, $company_code), mysqli_real_escape_string($this->db, $web_page), $phone_number, mysqli_real_escape_string($this->db, $email_id), mysqli_real_escape_string($this->db, $street_address), mysqli_real_escape_string($this->db, $city), mysqli_real_escape_string($this->db, $state), $postal_code, mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $timezone), mysqli_real_escape_string($this->db, $company_type), $company_id);
                    $result = mysqli_query($this->db, $update_company);
                } else {
                    $update_company = sprintf("UPDATE `company` SET `company_name`='%s', `class_schedule`=NULL, `company_code`='%s',`web_page`='%s', `phone_number`='%s',`email_id`='%s', `street_address`='%s',`city`='%s', `state`='%s',`postal_code`='%s', `country`='%s' ,`timezone`='%s', `company_type`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_name), mysqli_real_escape_string($this->db, $company_code), mysqli_real_escape_string($this->db, $web_page), $phone_number, mysqli_real_escape_string($this->db, $email_id), mysqli_real_escape_string($this->db, $street_address), mysqli_real_escape_string($this->db, $city), mysqli_real_escape_string($this->db, $state), $postal_code, mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $timezone), mysqli_real_escape_string($this->db, $company_type), $company_id);
                    $result = mysqli_query($this->db, $update_company);
                }
            }
        } else {
            $update_company = sprintf("UPDATE `company` SET `company_name`='%s', `company_code`='%s',`web_page`='%s', `phone_number`='%s',`email_id`='%s', `street_address`='%s',`city`='%s', `state`='%s',`postal_code`='%s', `country`='%s' ,`timezone`='%s', `company_type`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_name), mysqli_real_escape_string($this->db, $company_code), mysqli_real_escape_string($this->db, $web_page), $phone_number, mysqli_real_escape_string($this->db, $email_id), mysqli_real_escape_string($this->db, $street_address), mysqli_real_escape_string($this->db, $city), mysqli_real_escape_string($this->db, $state), $postal_code, mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $timezone), mysqli_real_escape_string($this->db, $company_type), $company_id);
            $result = mysqli_query($this->db, $update_company);
        }
        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if ($image_update === 'Y' && $old_logo_url!='' && strpos($old_logo_url, 'yourlogo.png') === false){
                $old = getcwd(); // Save the current directory
                $folder_name = "Company_$company_id";
                $path_parts = pathinfo($old_logo_url);
                $dir_name = "../../../$this->upload_folder_name/$folder_name";
                $filename = $path_parts['basename'];
                chdir($dir_name);
                if(file_exists($filename)){
                    unlink($filename);
                }
                chdir($old); // Restore the old working directory
            }
            $this->getcompanydetails($company_id,$user_id,1); //1 - call to Salesforce Api
        }
    }

    //Get Social details by Company ID
    protected function getSocialDetailsByCompany($company_id) {

        $sql = sprintf("SELECT `twitter_id`, `twitter_url`, `facebook_id`, `facebook_url`, `instagram_id`, `instagram_url`, 
                            `vimeo_id`, `vimeo_url`, `youtube_id`, `youtube_url`, `google_url`, `yelp_url` FROM `company` WHERE `company_id`='%s'", $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                $out = array('status' => "Success", 'msg' => $output);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Event details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }

    //Update Social
    protected function updateSocialDetailsInDB($twitter_id, $twitter_url, $facebook_id, $facebook_url, $instagram_id, $instagram_url, $vimeo_id, $vimeo_url, $youtube_id, $youtube_url, $google_url, $yelp_url, $company_id) {

        $update_company = sprintf("UPDATE `company` SET `twitter_id`='%s', `twitter_url`='%s', `facebook_id`='%s', `facebook_url`='%s',`instagram_id`='%s', `instagram_url`='%s', `vimeo_id`='%s', `vimeo_url`='%s', `youtube_id`='%s', `youtube_url`='%s', `google_url`='%s', `yelp_url`='%s' WHERE `company_id`='%s'", $twitter_id, $twitter_url, $facebook_id, $facebook_url, $instagram_id, $instagram_url, $vimeo_id, $vimeo_url, $youtube_id, $youtube_url, $google_url, $yelp_url, $company_id);
        $result = mysqli_query($this->db, $update_company);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getSocialDetailsByCompany($company_id);
        }
    }

    //Add New Message
    protected function insertMessageDetails($message_text, $message_link, $company_id, $schedule_date_time, $email_status, $push_status) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        
        if ($schedule_date_time != '') {
            $schedule_date_time = gmdate('Y-m-d H:i:s', strtotime($schedule_date_time));
            $sql1 = sprintf("INSERT INTO `message` (`message_text`, `message_link`, `company_id`, `message_delivery_date`, `email_status`, `message_from`, `push_from`) VALUES ('%s', '%s', '%s', '%s', '%s', 'P', 'CCA')", mysqli_real_escape_string($this->db,$message_text), $message_link, $company_id, $schedule_date_time, $email_status);
        } else {
            $sql1 = sprintf("INSERT INTO `message` (`message_text`, `message_link`, `company_id`, `email_status`, `message_from`, `push_from`, `push_delivered_date`) VALUES ('%s', '%s', '%s', '%s', 'P', 'CCA', NOW())", mysqli_real_escape_string($this->db,$message_text), $message_link, $company_id, $email_status);
        }
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $message_id = mysqli_insert_id($this->db);
            $student_id = [];
            $this->sendGroupMessage($company_id, $message_id, $student_id);
            if (($schedule_date_time === '') && ($push_status === 'Y')) {
                $trimmed_message = substr($message_text, 0, 230);
                $array = $this->preparepush($company_id, $trimmed_message);
                $response_status = $array['status'];
                $user_count = $array['msg'];
                if ($response_status === "Success") {

//                    $user_timezone = $this->getUserTimezone($company_id);
//                    $curr_time_zone = date_default_timezone_get();
//                    $new_timezone=$user_timezone['timezone'];
//                    date_default_timezone_set($new_timezone);
                    
//                    $current_time_tz = date("Y-m-d H:i:s");
                    $update_message = sprintf("UPDATE `message` SET `no_of_users`='%s' WHERE `message_id`='%s' and `company_id`='%s'", $user_count, $message_id, $company_id);
                    $result = mysqli_query($this->db, $update_message);

                    //Print Company details after login success
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }else {
                    if($response_status=='Failure'){

//                        $user_timezone = $this->getUserTimezone($company_id);
//                        $curr_time_zone = date_default_timezone_get();
//                        $new_timezone=$user_timezone['timezone'];
//                        date_default_timezone_set($new_timezone);
                    
//                        $current_time_tz = date("Y-m-d H:i:s");
                        $updt_message = sprintf("UPDATE `message` SET `no_of_users`='0' WHERE `message_id`='%s' and `company_id`='%s'", $message_id, $company_id);
                        $result2 = mysqli_query($this->db, $updt_message);

                        //Print Company details after login success
                        if (!$result2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updt_message");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    
                    }
                    $error = array('status' => "Failed", "msg" => "Students doesn't exist to send message for this company.");
                    $this->response($this->json($error), 200);
                }
            }
            
            if ($schedule_date_time === '' && $email_status === 'Y') {
                if(!empty(trim($message_link))){
                    $message = $message_text."<br><a href='$message_link'>Learn More</a>";
                }else{
                    $message = $message_text;
                }
                $sql = sprintf("SELECT c.`company_name` , c.`email_id` , c.`upgrade_status` , s.`student_email`,s.`student_cc_email` 
                        FROM  `student` s
                        LEFT JOIN  `company` c ON c.`company_id` = s.`company_id` 
                        LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
                        WHERE c.`company_id`='%s'AND s.`deleted_flag`!='Y' and s.`student_email` != IFNULL(b.`bounced_mail`, '')", $company_id);
                $result = mysqli_query($this->db, $sql);

                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result);
                    if ($num_of_rows > 0) {
                        $initial_check = 0;
                        $cmp_name = $reply_to = '';
                        while ($row = mysqli_fetch_assoc($result)) {
                            if($initial_check==0){
                                $cmp_name = $row['company_name'];
                                $reply_to = $row['email_id'];
                                $initial_check++;
                            }
                            $output[] = $row;
                        }
                        $footer_detail = $this->getunsubscribefooter($company_id);
                        for ($idx = 0; $idx < count($output); $idx++) {
                            $obj = (Array) $output[$idx];
                            $user_emailid = $obj["student_email"];
                            $cc_email_list = $obj['student_cc_email'];
                            //echo "******count******".$idx;
                            $sendEmail_status = $this->sendEmail($user_emailid, "Message", $message, $cmp_name, $reply_to,'','',$cc_email_list,$company_id,'',$footer_detail);
                            if($sendEmail_status['status']=="true"){
                                $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                                log_info($this->json($error_log));
                            }else{
                                $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                                log_info($this->json($error_log));
                            }
                            
                        }
//                        $out = array('status' => "Success", 'msg' => "");
//                        // If success everythig is good send header as "OK" and user details
//                        $this->response($this->json($out), 200);
                        
                    } else {
                        $error = array('status' => "Failed", "msg" => "Students doesn't exist to send message for this company.");
                        $this->response($this->json($error), 200); // If no records "No Content" status
                    }
                }
            }
            if ($schedule_date_time != '') {
               
                $out = array('status' => "Success", 'msg' => "Message Successfully Scheduled");
                
            } else if($schedule_date_time == '') {
               
                $out = array('status' => "Success", 'msg' => "Message Successfully Sent");
                
            }
            date_default_timezone_set($curr_time_zone);
//            $out = array('status' => "Success", 'msg' => "Message Successfully Sent");
            // If success everythig is good send header as "OK" and user details
            $this->response($this->json($out), 200);
        }
    }

    //Add New Event
    protected function updateMessageDetails($message_id, $message_text, $message_link, $company_id, $schedule_date_time, $email_status, $push_status) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        
        if ($schedule_date_time != '') {
            $schedule_date_time = gmdate('Y-m-d H:i:s', strtotime($schedule_date_time));
            $sql1 = sprintf("UPDATE  `message` SET  `message_text` =  '%s', `message_link` =  '%s', `message_delivery_date`='%s',  `email_status`='%s', `read_status`='U' WHERE  `message_id` ='%s' AND `company_id` ='%s';", mysqli_real_escape_string($this->db,$message_text), $message_link, $schedule_date_time, $email_status, $message_id, $company_id);
        } else {
            $sql1 = sprintf("UPDATE  `message` SET  `message_text` =  '%s', `message_link` =  '%s',  `email_status`='%s', `read_status`='U', `push_delivered_date`=NOW() WHERE  `message_id` ='%s' AND `company_id` ='%s';", mysqli_real_escape_string($this->db,$message_text), $message_link, $email_status, $message_id, $company_id);
        }
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $student_id = [];
            $this->sendGroupMessage($company_id, $message_id, $student_id);
            if (($schedule_date_time === '') && ($push_status === 'Y')) {
                $trimmed_message = substr($message_text, 0, 230);
                $array = $this->preparepush($company_id, $trimmed_message);
                $response_status = $array['status'];
                $user_count = $array['msg'];
                if ($response_status === "Success") {

//                    $user_timezone = $this->getUserTimezone($company_id);
//                    $curr_time_zone = date_default_timezone_get();
//                    $new_timezone=$user_timezone['timezone'];
//                    date_default_timezone_set($new_timezone);
                    
//                    $current_timezone_tz2 = date("Y-m-d H:i:s");
                    $update_message = sprintf("UPDATE `message` SET `no_of_users`='%s' WHERE `message_id`='%s' and `company_id`='%s'", $user_count, $message_id, $company_id);
                    $result = mysqli_query($this->db, $update_message);

                    //Print Company details after login success
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } 
                }else {
                    if($response_status=='Failure'){

//                        $user_timezone = $this->getUserTimezone($company_id);
//                        $curr_time_zone = date_default_timezone_get();
//                        $new_timezone=$user_timezone['timezone'];
//                        date_default_timezone_set($new_timezone);
                    
//                        $current_timezone_tz2 = date("Y-m-d H:i:s");
                        $updt_message = sprintf("UPDATE `message` SET `no_of_users`='0' WHERE `message_id`='%s' and `company_id`='%s'", $message_id, $company_id);
                        $result2 = mysqli_query($this->db, $updt_message);

                        //Print Company details after login success
                        if (!$result2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updt_message");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    $error = array('status' => "Failed", "msg" => "Students doesn't exist to send message for this company.");
                    $this->response($this->json($error), 200);
                }
            }
            
           if (($schedule_date_time === '') && ($email_status === 'Y')) {
               if(!empty(trim($message_link))){
                    $message = $message_text."<br><a href='$message_link'>Learn More</a>";
                }else{
                    $message = $message_text;
                }
                $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, s.`student_email`, s.`student_cc_email` 
                        FROM  `student` s
                        LEFT JOIN  `company` c ON c.`company_id` = s.`company_id` 
                        LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
                        WHERE c.`company_id`='%s' AND s.`deleted_flag`!='Y' and  s.`student_email` != IFNULL(b.`bounced_mail`, '')", $company_id);
                $result = mysqli_query($this->db, $sql);

                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result);
                    if ($num_of_rows > 0) {
                        $cmp_name = $reply_to = '';
                        $initial_check = 0;
                        while ($row = mysqli_fetch_assoc($result)) {
                            if($initial_check==0){
                                $cmp_name = $row['company_name'];
                                $reply_to = $row['email_id'];
                                $initial_check++;
                            }
                            $output[] = $row;
                        }
                        $footer_detail = $this->getunsubscribefooter($company_id);
                        for ($idx = 0; $idx < count($output); $idx++) {
                            $obj = (Array) $output[$idx];
                            $user_emailid = $obj["student_email"];
                            $cc_email_list = $obj['student_cc_email'];
                            //echo "******count******".$idx;
                            
                            $sendEmail_status = $this->sendEmail($user_emailid, "Message", $message, $cmp_name, $reply_to,'','',$cc_email_list,$company_id,'',$footer_detail);
                            if($sendEmail_status['status']=="true"){
                                $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                                log_info($this->json($error_log));
                            }else{
                                $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                                log_info($this->json($error_log));
                            }
                        }
                        
                    } else {
                        $error = array('status' => "Failed", "msg" => "Students doesn't exist to send email for this company.");
                        $this->response($this->json($error), 200); // If no records "No Content" status
                    }
                }
            }
            if ($schedule_date_time != '') {
                $out = array('status' => "Success", 'msg' => "Message Successfully Scheduled");
            } else {
                $out = array('status' => "Success", 'msg' => "Message Successfully Sent");
            }
            date_default_timezone_set($curr_time_zone);
            // If success everythig is good send header as "OK" and user details
            $this->response($this->json($out), 200);
        }
    }
    
    //send group message
    protected function sendGroupMessage($company_id, $message_id, $student_id){
        if(empty($student_id)){
            $sql = sprintf("SELECT student_id FROM `student` WHERE `company_id`='%s' AND `deleted_flag`!='Y'
                        AND student_id NOT IN (SELECT student_id FROM message_mapping WHERE message_id='%s')", 
                    mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$message_id));
            $result = mysqli_query($this->db, $sql);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $student_id=[];
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows > 0) {
                    while ($row = mysqli_fetch_assoc($result)) {
                        $student_id[] = $row['student_id'];
                    }
                }
            }
        }
        
        for($i=0; $i<count($student_id);$i++){
            $insert_query = sprintf("INSERT INTO `message_mapping`(`company_id`, `message_id`, `student_id`) VALUES('%s', '%s', '%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $message_id), mysqli_real_escape_string($this->db, $student_id[$i]));
            $insert_result = mysqli_query($this->db, $insert_query);
            if(!$insert_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
    }

    //Get Messgage Details
    protected function getMessageDetailByCompany($company_id) {
        
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $tz_str = str_replace("_"," ",explode("/", $new_timezone))[1]." time (GMT ".DATE("P").")";
        $tz = date('T');
        
        $message_delivery_date = "CONVERT_TZ(`message_delivery_date`,$tzadd_add,'$new_timezone')";
        $push_delivered_date = "CONVERT_TZ(`push_delivered_date`,$tzadd_add,'$new_timezone')";

        $sql = sprintf("SELECT `message_id`, `company_id`, `message_text`, `message_link`, $message_delivery_date message_delivery_date, $push_delivered_date push_delivered_date, `email_status`, `no_of_users` FROM `message` WHERE `company_id`='%s' AND `message_to` IN ('A', 'G') ORDER BY `message_id` desc", $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                $out = array('status' => "Success", 'msg' => $output, "tz" => $tz, "tz_offset" => $tz_str);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Message details doesn't exist.", "tz" => $tz, "tz_offset" => $tz_str);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

    protected function preparepush($company_id, $message) {
        $android = $ios = [];
        $sql2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
            (SELECT s.company_id,s.student_id,lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
               IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props  FROM `student` s
            LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id)
            WHERE `company_id`='%s' AND `deleted_flag`!='Y' AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
            AND push_device_id IS NOT NULL AND trim(push_device_id)!='' order by app_id)t1 
            LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
            where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows2 = mysqli_num_rows($result2);
            if ($num_of_rows2 > 0) {
                while ($row = mysqli_fetch_assoc($result2)) {
                    $output[] = $row;
                }
                 $r_rand = mt_rand(1,9999);
                $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();  //for creating pem file from db
                if (!file_exists($rfolder_name)) {
                    $oldmask = umask(0);
                    mkdir($rfolder_name, 0777, true);
                    umask($oldmask);
                }else{ // to make directory once.
                    $r = 0;
                    while ($r < 100) {
                        $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();
                        if (!file_exists($rfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($rfolder_name, 0777, true);
                            umask($oldmask);
                            break;
                        }
                     $r++;
                    }
                }
                $ios_socket_url ='';
                $ios_app_id_array = [];
                $host = $_SERVER['HTTP_HOST'];
                if (strpos($host, 'mystudio.academy') !== false) {
//                if (strpos($host, 'www.mystudio.academy') !== false || strpos($host, 'stage.mystudio.academy') !== false) {
                    //production
                    $ios_socket_url = "ssl://gateway.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];
                        
                        if ($devicetype == 'I') {                            
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['prod_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }else{
                    //development
                    $ios_socket_url = "ssl://gateway.sandbox.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];                        
                        
                        if ($devicetype == 'I') {
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['dev_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }
                $sent_count = $this->sendpush($message, $ios, $android, $ios_socket_url);
                $check = $this->deleteDirectory("$rfolder_name");// delete directory with content
                $out = array('status' => "Success", 'msg' => $sent_count);
                return $out;
            } else {
                $out = array('status' => "Failure", 'msg' => 0);
                return $out;
            }
        }
    }
    
    protected function sendpush($data, $ios, $android, $socket_url) {
        $send_count = 0;
        //ios
        if (!empty($ios)) {
            $ctx = stream_context_create();
            for ($r = 0; $r < count($ios['device_token']); $r++) {
//                $token_arr = [];
                $app_id = $ios['app_id'][$r];
                $deviceToken = $ios['device_token'][$r];
                $unread_msg_count = $ios['unread_msg_count'][$r];
//                $new_file_content = file_get_contents($ios['pem'][$r]);
//                if ((!empty($old_fle_content) && $old_fle_content !== $new_file_content) || $r == 0) {
//                    if ($r !== 0) {
//                        fclose($fp);
//                    }
                stream_context_set_option($ctx, 'ssl', 'local_cert', $ios['pem'][$r]);
                stream_context_set_option($ctx, 'ssl', 'passphrase', 'mystudio');
                $fp = stream_socket_client($socket_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                if (!$fp) {
                    $error_log = array('status' => "Failed", "msg" => "Message not delivered(I)[Error - $errstr($err)].", "App id" => $app_id, "Token" => "$deviceToken");
                    log_info($this->json($error_log));
                    continue;
                }
//                }
                // Build the binary notification
                $token_arr = explode(",", $deviceToken);
                $unread_msg_arr = explode(",", $unread_msg_count);
                if (count($token_arr) > 0) {
                    $chunk_token_arr = array_chunk($token_arr, 8);
                    $chunk_bagde_count = array_chunk($unread_msg_arr, 8);
                    $flag = 1;
                    for ($j = 0; $j < count($chunk_token_arr); $j++) {
                        if ($j !== 0) {
                            if ($flag == 1) {
                                fclose($fp);
                            }
                            $deviceToken = implode(",", $chunk_token_arr[$j]);
                            stream_context_set_option($ctx, 'ssl', 'local_cert', $ios['pem'][$r]);
                            stream_context_set_option($ctx, 'ssl', 'passphrase', 'mystudio');
                            $fp = stream_socket_client($socket_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                            if (!$fp) {
                                $error_log = array('status' => "Failed", "msg" => "Message not delivered(I)[Error - $errstr($err)].", "App id" => $app_id, "Token" => "$deviceToken");
                                log_info($this->json($error_log));
                                $flag = 0;
                                continue;
                            } else {
                                $flag = 1;
                            }
                        }
                        for ($c = 0; $c < count($chunk_token_arr[$j]); $c++) {
                            // Create the payload body
                            $body['aps'] = array(
                                'alert' => array(
                                    'title' => 'New Message received',
                                    'body' => $data
                                ),
                                'sound' => 'default',
                                'badge' => (int)$chunk_bagde_count[$j][$c]
                            );
                            // Encode the payload as JSON
                            $payload = json_encode($body);
                            $msg = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $chunk_token_arr[$j][$c])) . pack("n", strlen($payload)) . $payload;
                            $result = fwrite($fp, $msg, strlen($msg));
                            // Send it to the server
                            if (!$result) {
                                $error_log = array('status' => "Failed", "msg" => "Message not delivered(I).", "Token" => $chunk_token_arr[$j][$c], "PL" => $payload);
                                log_info($this->json($error_log));
                            } else {
                                $error_log = array('status' => "Success", "msg" => "Message successfully delivered(I).", "Token" => $chunk_token_arr[$j][$c], "PL" => $payload);
                                log_info($this->json($error_log));
                                $send_count++;
                            }
                        }
                    }
                    fclose($fp);
                }
//                $old_fle_content = file_get_contents($ios['pem'][$r]);
            }
        }

        if (!empty($android)) {
            //Android	
            $url = 'https://fcm.googleapis.com/fcm/send';
//            for ($r = 0; $r < count($android['device_token']); $r++) {
            foreach ($android['device_token'] as $point => $values) {
                $key = $android['key'][$point];
                $deviceToken = array_chunk($values, 900);
                for ($j = 0; $j < count($deviceToken); $j++) {
                    $ch = curl_init();
                    $devicetoken = implode(",", $deviceToken[$j]);
                    $fields = array(
                        'registration_ids' => $deviceToken[$j],
                        'data' => array(
                            "message" => $data,
                            "title" => 'New Message',
                            "badge"=> 1
                        )
                    );
                    $fields = json_encode($fields);
                    $headers = array(
                        'Authorization: key=' . $key,
                        'Content-Type: application/json'
                    );
                    if ($url) {
                        // Set the url, number of POST vars, POST data
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        // Disabling SSL Certificate support temporarly
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        if ($fields) {
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                        }

                        // Execute post
                        $result = curl_exec($ch);
                        $resultArr = json_decode($result, true);
                        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        if ($result === FALSE || $resultArr['success'] == 0) {
                            //die('Curl failed: ' . curl_error($ch));
                            $error_log = array('status' => "Failed", "msg" => "Message not delivered(A).", "Token" => "$devicetoken", "StatusCode" => $returnCode, "Result" => json_encode($resultArr));
                            log_info($this->json($error_log));
                        } else {
                            $error_log = array('status' => "Success", "msg" => "Message successfully delivered(A).", "Token" => "$devicetoken", "StatusCode" => $returnCode, "Result" => json_encode($resultArr));
                            log_info($this->json($error_log));
                            $send_count += $resultArr['success'];
                        }
                    }
                    curl_close($ch);
                }
            }
        }
        return $send_count;
    }

    //Verify user in every method along with companyid, emailid and password.
    protected function verifyuser($company_id, $emailid, $password) {
        
        if(isset($_SESSION['admin'])){
           $sql = sprintf("SELECT company_id, deleted_flag from user where user_email='%s' and company_id='%s' limit 0,1", mysqli_real_escape_string($this->db,$emailid), $company_id);
        }else{
//            $password = md5($password);
            $sql = sprintf("SELECT company_id, deleted_flag from user where user_email='%s' and user_password='%s' and company_id='%s' limit 0,1", mysqli_real_escape_string($this->db,$emailid), mysqli_real_escape_string($this->db,$password), $company_id);
        }
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if($num_of_rows > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    $cmpny_id = $row['company_id'];
                    $deleted_flag = $row['deleted_flag'];
                }
                
                if($deleted_flag=='Y'){
                    $error = array('status' => "Failed", "msg" => "User account doesn't exist.");
                    $this->response($this->json($error), 200);       // If "Company id" not available
                }
                if ($company_id != $cmpny_id) {
                    $error = array('status' => "Failed", "msg" => "User doesn't belong to the company.");
                    $this->response($this->json($error), 200);
                } else {
                    $current_date = gmdate("Y-m-d");
//                    $sql1 = sprintf("SELECT company_id from company where company_id='%s' and ((date(`expiry_dt`) >='%s')||(`expiry_dt` is null))", $cmpny_id, $current_date);
//                    $result1 = mysqli_query($this->db, $sql1);
//                    if (!$result1) {
//                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
//                        log_info($this->json($error_log));
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                        $this->response($this->json($error), 200);
//                    } else {
//                        $num_of_rows1 = mysqli_num_rows($result1);
//                        if ($num_of_rows1 > 0) {
                            $out = array('status' => "Success", 'msg' => "user details verified successfully.");
                            return $out;
//                        } else {
//                            $error = array('status' => "Failed", "msg" => "Company expired, please contact your administrator.");
//                            $this->response($this->json($error), 200);
//                        }
//                    }


                    // If success everythig is good send header as "OK" and user details
                    //$this->response($this->json($out), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "User doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }
    }

    //Get Student details
    protected function getStudentDetails($company_id,$search,$draw_table,$length_table,$start,$sorting) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        $search_value=$search['value'];
        $sort=$sorting[0]['column'];
        $sort_type=$sorting[0]['dir'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        
        if(!empty($search_value)){
             $s_text=" and (student_name like  '%$search_value%'  or student_lastname like  '%$search_value%' or student_email like  '%$search_value%'  or first_login_dt like  '%$search_value%'  or  last_login_dt like  '%$search_value%' or student_username like  '%$search_value%')";
         }else{
             $s_text='';
         }
         
         $column=array(2=>"student_name",3=>"student_lastname",4=>"student_username",5=>"student_email",6=>"first_login_dt",7=>"last_login_dt");
        $first_login = " CONVERT_TZ(`first_login_dt`,$tzadd_add,'$new_timezone')";
        $last_login = " CONVERT_TZ(`last_login_dt`,$tzadd_add,'$new_timezone')";
        
//        $end_limit = 1000;
        $output=[];
        $recordsTotal=0;
        $sql_count= sprintf("SELECT count(*) total_record ,'wo' as search, `last_login_dt`
                FROM `student` WHERE company_id = '%s' AND `deleted_flag`!='Y'
                union 
                SELECT count(*) total_record, 'w' as search, `last_login_dt`
                FROM `student` WHERE company_id = '%s' AND `deleted_flag`!='Y' %s ORDER BY `last_login_dt` desc ", $company_id,$company_id,$s_text);
        $result_count=  mysqli_query($this->db, $sql_count);
         if (!$result_count) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_count");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
             $num_of_rows1 = mysqli_num_rows($result_count);
             if($num_of_rows1>0){
                 while($row_count = mysqli_fetch_assoc($result_count)){
                    if($row_count['search']=='wo'){
                       $recordsTotal= $row_count['total_record'];
                       $out['recordsTotal'] = $recordsTotal;
                    }elseif($row_count['search']=='w'){
                        $recordsTotal= $row_count['total_record'];
                        $out['recordsFiltered'] = $recordsTotal;
                    }
                 }
             }
        }
        
        $sql = sprintf("SELECT `student_id` as id,`student_name`,`student_lastname`,student_username,`student_email`, $first_login first_login_dt, $last_login last_login_dt, IF(`push_device_id` !='' AND `push_device_id` IS NOT NULL, 'Y', 'N') show_icon,b.`bounced_mail`, if(s.`student_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`
                ,b.`type`,'Bad email. Please edit.' as error FROM `student` s  LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)  WHERE s.`company_id` = '%s' AND `deleted_flag`!='Y' %s  ORDER BY $column[$sort] $sort_type LIMIT $start , $length_table ", $company_id,$s_text);
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) { 
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                
                $out['data'] = $output;
                $out['draw'] = $draw_table;
//               if(!empty($search_value)){
//                  $out['recordsFiltered'] = $num_of_rows;
//               } 
               
//                $out = array('status' => "Success", 'msg' => $output);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                  $out['data'] = $output;
                  $out['draw'] = $draw_table;
                  $out['recordsTotal'] = $recordsTotal;
                  $out['recordsFiltered'] = $recordsTotal;   
//                $error = array('status' => "Failed", "msg" => "Student details doesn't exist.");
                $this->response($this->json($out), 200); // If no records "No Content" status
            }
        }
    }
    
     protected function updateStudentDetails($company_id, $student_id, $student_name, $student_lastname, $student_email) {

        
         $sql1 = sprintf("SELECT `student_id` FROM `student` WHERE  `company_id`='%s' and `student_email` = '%s' ", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_email));

        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $rows = mysqli_num_rows($result1);
            if ($rows > 0) {
                $row = mysqli_fetch_assoc($result1);
                if ($row['student_id'] != $student_id) {
                    $error = array('status' => "Failed", "msg" => "Email already registered for the studio.");
                    $this->response($this->json($error), 200);
                }
            }
        }

        $selectsql = sprintf("SELECT `student_email` FROM `student` WHERE `student_id` ='%s' and `company_id`='%s'", mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
        $resultselect = mysqli_query($this->db, $selectsql);
        if (!$resultselect) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($resultselect) > 0) {
                $obj = mysqli_fetch_object($resultselect);
                $sql = sprintf("UPDATE `student` s LEFT JOIN `membership_registration` m ON m.`student_id`  =  s.`student_id`  and  m.`company_id`= s.`company_id` and m.`buyer_email`  =  s.`student_email`
                                  LEFT JOIN `event_registration` e on e.`student_id`  =  s.`student_id`  and  e.`company_id`= s.`company_id`  and  e.`buyer_email`  =  s.`student_email`
                                  LEFT JOIN `trial_registration` t on t.`student_id`  =  s.`student_id`  and  t.`company_id`= s.`company_id`  and  t.`buyer_email`  =  s.`student_email`
                                  SET  m.`buyer_email` = '%s', s.`student_email` ='%s', e.`buyer_email`='%s', t.`buyer_email`='%s', s.`student_name`='%s', s.`student_lastname`='%s' WHERE
                                  s.`student_id`='%s' and s.company_id='%s'  and  s.`student_email`='%s'", mysqli_real_escape_string($this->db, $student_email), mysqli_real_escape_string($this->db, $student_email),
                        mysqli_real_escape_string($this->db, $student_email), mysqli_real_escape_string($this->db, $student_email), mysqli_real_escape_string($this->db,$student_name), mysqli_real_escape_string($this->db,$student_lastname), 
                        mysqli_real_escape_string($this->db,$student_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db, $obj->student_email));
                
                $result = mysqli_query($this->db, $sql);
                
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $out = array('status' => "Success", 'msg' => 'Updated successfully');
//           $this->getStudentDetails($company_id,0);
                    $this->response($this->json($out), 200);
                }
            }
        }
    }

    //Get Total Users details by company
    protected function getTotalUserDetails($company_id) {

        $current_time = gmdate("Y-m-d");
        $onemonthagodate = gmdate("Y-m-d", strtotime("-30 days"));

        $sql = sprintf("SELECT COUNT( * ) as total_students FROM `student` WHERE company_id = '%s' AND `deleted_flag`!='Y' ", $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output['total_students'] = $row;
                }

                $sql1 = sprintf("SELECT COUNT( * ) as total_active_students FROM `student` WHERE company_id = '%s' and last_login_dt between '%s' and '%s' AND `deleted_flag`!='Y' ", $company_id, $onemonthagodate, $current_time);
                $result1 = mysqli_query($this->db, $sql1);

                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows1 = mysqli_num_rows($result1);
                    if ($num_of_rows1 > 0) {
                        while ($row = mysqli_fetch_assoc($result1)) {
                            $output['total_active_students'] = $row;
                        }
                        
                        $sql1 = sprintf("SELECT `created_date` FROM `company` WHERE company_id = '%s'", $company_id);
                        $result1 = mysqli_query($this->db, $sql1);

                        if (!$result1) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            $num_of_rows1 = mysqli_num_rows($result1);
                            if ($num_of_rows1 > 0) {
                                while ($row = mysqli_fetch_assoc($result1)) {
                                    $output['created_date'] = $row;
                                }
                                $out = array('status' => "Success", 'msg' => $output);
                                // If success everythig is good send header as "OK" and user details
                                $this->response($this->json($out), 200);
                            } else {
                                // If no records "No Content" status
                            }
                        }
                    } else {
                        // If no records "No Content" status
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Total Student details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
    //Get Eventbrite details by company
    protected function getvideos($company_id) {
        $zero_companyid = "0";
        $sql = sprintf("SELECT `video_id`,`title`,`category_name`,`description`,`thum_nail_url`,`video_url` FROM `video_content` WHERE company_id = '%s'", $zero_companyid);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $cat_name=$video_content_array=[];
                while ($row = mysqli_fetch_assoc($result)) {
                    $db_video_url = $row['video_url'];
                    $db_thum_nail_url = $row['thum_nail_url'];
                    
                    if(!empty($db_video_url)){
                        $row['signed_url'] = $this->changeToSignedURL($db_video_url);
                    }else{
                        $row['signed_url'] = "";
                    }
                    
                    if(!empty($db_thum_nail_url)){
                        $row['signed_thum_nail_url'] = $this->changeToSignedURL($db_thum_nail_url);
                    }else{
                        $row['signed_thum_nail_url'] = "";
                    }
                    $output['video_content'][] = $row;
                }
                $count = count($output['video_content']);
                for($i=0;$i<$count;$i++){
                    if(!in_array($output['video_content'][$i]['category_name'], $cat_name))
                       $cat_name[]=$output['video_content'][$i]['category_name'];
                }
                for($j=0;$j<$count;$j++){
                    $index = array_search($output['video_content'][$j]['category_name'], $cat_name);
                    $video_content_array['video_content'][$index]['category_name']=$output['video_content'][$j]['category_name'];
                    $video_content_array['video_content'][$index][] = $output['video_content'][$j];
                }
                $out = array('status' => "Success", 'msg' => $video_content_array);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failure", "msg" => "Video URL doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
    //Register User and Company
    protected function registerUserAndCompany($email, $password, $company_name, $phone_number, $country) {

        $query = sprintf("SELECT user_id, user_email, user_password from user where user_email='%s' limit 0,1", mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
               $error = array('status' => "Failed", "msg" => "User already exists.");
                $this->response($this->json($error), 200);
            }else{
                $this->registerCompany($email, $password, $company_name,$phone_number,$country);
                
//                $response = $this->sendVerificationLink($email);
//                if($response['status']=="true"){
//                    $error_log = array('status' => "Success", "msg" => "Verification Email sent successfully.", "Email_id" => $email, "Email Status Msg" => $response['mail_status']);
//                    log_info($this->json($error_log));
//                    $this->registerCompany($email, $password, $company_name,$phone_number);
//                }else{
//                    $error_log = array('status' => "Failed", "msg" => "Verification Email not sent.", "Email_id" => $email, "Email Status Msg" => $response['mail_status']);
//                    log_info($this->json($error_log));
//                    $out = array('status' => "Failed", 'msg'=>$response['mail_status']);
//                    // If email failed, return the reason
//                    $this->response($this->json($out), 200);
//                }
            }
        }
    }
    
    //send verification link during registration
    protected function sendVerificationLink($email){
        $salt = "MyStudio";
        $user_email_md5 = md5($salt.$email);
        $subject = "MyStudio Verification Code";
        $message = "Please copy and paste this code to complete the registration of your free MyStudio account: $user_email_md5";
        $sendEmail_status = $this->sendEmail($email, $subject, $message, '', '','','','','','','');//last two parameters are empty for company_name & reply-to addr(only for Whitelabel and notifications)
        return $sendEmail_status;
    }
    
    //check verification status
    protected function getVerifiedStatus($company_id){
//        $query = sprintf("SELECT  `registration_complete` , IF(  `registration_complete` =  'Y',  'Y', IF( TIMEDIFF( CURRENT_TIMESTAMP( ) , created_date ) / ( 60 *60 ) >24,  'U',  'N' ) ) verification_complete
        $query = sprintf("SELECT  `registration_complete` , 'Y' as verification_complete
            FROM  `company` 
            WHERE  `company_id` ='%s' limit 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output = $row;
                }
                $out = array('status' => "Success", 'msg' => $output);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            }else{
                $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
                $this->response($this->json($error), 200);       // If "Company id" not available
            }
        }
    }

    //re-send verification link during registration
    protected function verificationLinkresend($email){
        $this->sendBounceEmail_check($email);
        $response = $this->sendVerificationLink($email);
        if($response['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Verification Email sent successfully.", "Email_id" => $email, "Email Status Msg" => $response['mail_status']);
            log_info($this->json($error_log));
            $out = array('status' => "Success", 'msg' => "Verification link re-sent.");
            $this->response($this->json($out), 200);
        }else{
            $error_log = array('status' => "Failed", "msg" => "Verification Email not sent.", "Email_id" => $email, "Email Status Msg" => $response['mail_status']);
            log_info($this->json($error_log));
            $out = array('status' => "Failed", 'msg'=>$response['mail_status']);
            // If email failed, return the reason
            $this->response($this->json($out), 200);
        }
    }
    
    //verify user registration details
    protected function verifyUserRegistrationDetails($email, $verification_code, $company_id, $return){    // $return -> 0 - echo result, 1 - return result
        $query = sprintf("SELECT user_id, user_email from user where user_email='%s' limit 0,1", mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                    $salt = "MyStudio";
                    $user_email_md5 = md5($salt.$email);
                    if($user_email_md5 != $verification_code){
                        $error = array('status' => "Failed", "msg" => "Verification code doesn't match.");
                        $this->response($this->json($error), 200);// Invalid request parameters
                    }else{
                        $sql = sprintf("UPDATE `company` SET `registration_complete`='Y'  WHERE `company_id`='%s'",mysqli_real_escape_string($this->db, $company_id));
                        $result2 = mysqli_query($this->db, $sql);
                        if (!$result2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else{
                            $out = array('status' => "Success", 'msg' => "Verification success.");
                            if($return==1){
                                return $out;
                            }else{
                                $this->response($this->json($out), 200);
                            }
                        }
                    }
                    //$this->registerCompany($email, $password, $company_name);
            }else{
                $error = array('status' => "Failed", "msg" => "User email doesn't match.");
                $this->response($this->json($error), 200);// If no records "No Content" status
            }
        }
    }
    
    protected function registerCompany($email,$password,$company_name,$phone_number,$country) {
        $currency_code = $currency_symbol = '';
        if(strtoupper($country)=='CA'){
            $currency_code = 'CAD';
            $currency_symbol = 'CAD$';
        }elseif(strtoupper($country)=='UK'){
            $currency_code = 'GBP';
            $currency_symbol = '£';
        }else{
            $currency_code = 'USD';
            $currency_symbol = '$';
        }
        $sql = sprintf("INSERT INTO `company` (`company_name`,`phone_number`,`country`,`wp_currency_code`,`wp_currency_symbol`,`expiry_dt`,`email_id`) VALUES ('%s','%s','%s','%s','%s',DATE_ADD(NOW(), INTERVAL 14 DAY),'%s')", mysqli_real_escape_string($this->db, $company_name), mysqli_real_escape_string($this->db, $phone_number), mysqli_real_escape_string($this->db, $country), $currency_code, $currency_symbol,mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $sql);
        $company_id="";

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $company_id = mysqli_insert_id($this->db);
            $this->registerUser($email,$password,$company_id,$company_name,$phone_number);
        }
    }
    
    protected function registerUser($email,$password,$company_id,$company_name,$phone_number) {
        $type="A";
        $password = md5($password);
        $sql = sprintf("INSERT INTO `user` (`user_email`, `user_password`, `company_id`,`last_login_dt`) VALUES ('%s','%s','%s',NOW())", mysqli_real_escape_string($this->db, $email), mysqli_real_escape_string($this->db, $password), $company_id);
     //   $sql = sprintf("UPDATE `user` SET `user_password`='%s', `company_id`='%s', `user_type`='%s', `last_login_dt`= NOW() WHERE `user_email`='%s'", mysqli_real_escape_string($this->db, $password), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $type), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $output['company_id'] = $company_id;
            $output['phone_number'] = $phone_number;
            if (empty($company_id)) {
                $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
                $this->response($this->json($error), 200);       // If "Company id" not available
            }
            $_SESSION['user'] = $email;
            $_SESSION['pwd'] = $password;
            $out = array('status' => "Success", 'msg' => $output);
            $this->responseWithSalesforce($this->json($out), 200);
            $this->registerCompanyinSalesforceApi($company_id, $company_name, $email, $phone_number, '', '', '', '', '');
        }
    }
    
    //Update Logo in Company
    protected function registerLogo($company_id,$company_logo,$image_url) {

        $update_company = sprintf("UPDATE `company` SET `logo_content`='%s',`logo_URL`='%s' WHERE `company_id`='%s'", $company_logo, mysqli_real_escape_string($this->db, $image_url), $company_id);
        $result = mysqli_query($this->db, $update_company);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $out = array('status' => "Success", 'msg' => "Company logo updated successfully.");
            $this->response($this->json($out), 200);
        }
    }
    
    //Update Company Code in Company Table
    protected function registerCompanyCodeInDB($company_id, $company_code) {

        $sql = sprintf("SELECT company_code from company where company_code='%s' limit 0,1", mysqli_real_escape_string($this->db, $company_code));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $error = array('status' => "Failed", "msg" => "Company code already exist.");
                $this->response($this->json($error), 200);
            } else {
                $update_company = sprintf("UPDATE `company` SET `company_code`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_code), $company_id);
                $result = mysqli_query($this->db, $update_company);

                //Print Company details after login success
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $out = array('status' => "Success", 'msg' => "Company code updated successfully.");
                    $this->response($this->json($out), 200);
                }
            }
        }
    }
    
    protected function sendEmail($to, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file,$cc_email_list,$company_id,$type,$footer_detail) {//deepak
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }            
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            if (!empty($company_id) && $type != 'subscribe') {
            
                $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                $curr_loc = implode('/', explode('/', $curr_loc, -4));
//                $tomail = $to;
                if (!empty(trim($cc_email_list))) {
                    $tomail = base64_encode($to . "," . "$cc_email_list");
                } else {
                    $tomail = base64_encode($to);
                }
                $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/EmailSubscription.php?mail=$tomail?comp_id=$company_id?type=U";
                $message = "
            <html>
            <body>
                <p>$message</p><br><br>
                    <footer style='font-size: 13px;color: #746f6f;'>
                    <center>
                    <p>$footer_detail
                    </p>
                    <a href='$activation_link' target='_blank' style='color: #746f6f; !important;text-decoration: none !important;'><u style='color: #746f6f;'>Unsubscribe $to</u></a>
                    </center>
                    </footer>
            </body>
            </html>
            ";
            }
            $mail->Body = $message;
            if(!empty($file_name)){
                $mail->AddStringAttachment(base64_decode($attached_file), $file_name, 'base64');
            }
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    if (filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)) {
                        $verify_email_for_bounce = $this->verifyBouncedEmailForCC($cc_addresses[$init], $company_id);
                       if($verify_email_for_bounce==1){
                            $mail->AddCC($cc_addresses[$init]);
                        }else if($verify_email_for_bounce==0 && $type == 'subscribe'){
                            $mail->AddCC($cc_addresses[$init]);
                        }
                    }
                }
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }

    protected function insertVideoDetails($company_id,$title,$category_name,$description,$thum_nail_url,$video_url) {
        $sql = sprintf("INSERT INTO `video_content`(`title`, `category_name`, `description`, `thum_nail_url`, `video_url`, `company_id`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db,$title), mysqli_real_escape_string($this->db,$category_name), mysqli_real_escape_string($this->db,$description), $thum_nail_url, $video_url, $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $video_id = mysqli_insert_id($this->db);
            $out = array('status' => "Success", 'msg'=>"Video Content details added successfully.", 'video_id'=>$video_id);
            // If success everythig is good send header as "OK" and user details
            $this->response($this->json($out), 200);
        }
    }
    
    protected function updateCompanyCodeInDB($company_id,$company_code,$user_id) {

	$sql = sprintf("SELECT company_code from company where company_code='%s' and company_id != '%s' limit 0,1", mysqli_real_escape_string($this->db, $company_code), $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $error = array('status' => "Failed", "msg" => "Company code already exist.");
                $this->response($this->json($error), 200);
            }else{
                $sql2 = sprintf("UPDATE `company` SET `company_code`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_code), $company_id);
                $result2 = mysqli_query($this->db, $sql2);

                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $this->getcompanydetails($company_id,$user_id,0); //0 - No call to Salesforce Api
                }
            }
        }
    }
    
    //Forgot Password Request
    protected function forgotPasswordRequest($email) {
            $this->sendBounceEmail_check($email);
        $sql = sprintf("SELECT c.`company_name` , c.`email_id` , c.`upgrade_status` , u.`user_email` , u.`last_login_dt` 
            FROM  `user` u
            LEFT JOIN  `company` c ON c.`company_id` = u.`company_id` 
            WHERE user_email = '%s' limit 0,1", mysqli_real_escape_string($this->db,$email));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    if($row['upgrade_status']=='W'){
                        $company_name = $row['company_name'];
                    }else{
                        $company_name = "MyStudio";
                    }
                    $user_email = md5($row['user_email']);
                    $last_login_dt = md5($row['last_login_dt']);
                    $current_dt = gmdate("Y-m-d-H:i:s");                    
                    $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                    $curr_loc = implode('/', explode('/', $curr_loc, -3));
                    
                    $img_url = $msg = "";
                    
                    $url_1 = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://')."$curr_loc/WebPortal/#/resetpassword?token=$user_email-tgs-$last_login_dt-tgs-$current_dt";
                    $url = sprintf('<a href="%s">%s</a>', $url_1, $url_1);
                    $img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://')."$curr_loc/WebPortal/image/resetpassword_email_poster.png";
                    $msg .= "<html>";
                    $msg .= "<head>";
                    $msg .= "<title>Reset Password Email</title>";
                    $msg .= "<meta charset='UTF-8'>";
                    $msg .= "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
                    $msg .= "<style type='text/css'>";
                    $msg .= "body{font-family:avenir;font-weight:normal;letter-spacing:normal;line-height:normal;padding:20px;width:600px;margin: auto;}";
                    $msg .= "img{width:400px;}";
                    $msg .= "p{font-size:16px;font-weight:500;margin:0;}";
                    $msg .= "h1{font-size:22px;font-weight:800;line-height:1.3;letter-spacing:0.5px;margin:30px 0;}";
                    $msg .= "a{color:#009a61;text-decoration: none;}";
                    $msg .= "button{height: 50px;width: 400px;border-radius: 5px;background: #009a61;border:1px solid #009a61;color:white;font-size: 16px;font-weight:600;cursor:pointer;}";
                    $msg .= "</style>";
                    $msg .= "</head>";
                    $msg .= "<body style='font-family:avenir !important;font-weight:normal;letter-spacing:normal;line-height:normal;padding:20px;width:600px;margin: auto;color:#111111 !important;'>";
                    $msg .= "<center>";
                    $msg .= "<img id='cover_img' src='$img_url' alt='coverImage' style='width:400px;'><br><br>";
                    $msg .= "<h1 style='font-family:avenir !important;color:#111111 !important;'>Forgot your password?</h1>";
                    $msg .= "<p style='font-family:avenir !important;color:#111111 !important;'>Not to worry, we got you!<br>Let’s get you a new password.</p>";
                    $msg .= "<br><br><br><br>";
                    $msg .= "<a href='$url_1' target='_blank'><button style='height: 50px;width: 400px;border-radius: 5px;background: #009a61;border: 1px solid #009a61;color: white;font-size: 16px;font-weight: 600;cursor: pointer;'>Reset password</button></a>";
                    $msg .= "</center>";
                    $msg .= "</body>";
                    $msg .= "</html>";
                    
                    $sendEmail_status = $this->sendEmail($row['user_email'], "$company_name - Reset password", $msg, '', '','','','','','','');//last two parameters are empty for company_name & reply-to addr(only for Whitelabel and notifications)
                    if($sendEmail_status['status']=="true"){
                        $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $row['user_email'], "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
                        $out = array('status' => "Success", 'msg'=>"Email sent successfully.");
                        // If success everythig is good send header as "OK" and user details
                        $this->response($this->json($out), 200);
                    }else{
                        $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $row['user_email'], "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
                        $out = array('status' => "Failed", 'msg'=>$sendEmail_status['mail_status']);
                        // If success everythig is good send header as "OK" and user details
                        $this->response($this->json($out), 200);
                    }
                }
                
            } else {
                $error = array('status' => "Failed", "msg" => "Email id doestn't exist.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    //Check Token Is Valid or Not
    protected function checkTokenIsValid($token) {
        
        $stringArray = explode('-tgs-', $token);
        $emailid = $stringArray[0];
        $last_login_dt = $stringArray[1];
        $received_server_dt = $stringArray[2];
        $received_dt_Array = explode('-', $received_server_dt);
        $received_dt = $received_dt_Array[3];
        date_default_timezone_set('UTC');
        if(!strtotime($received_dt)){
            $error = array('status' => "Failed", "msg" => "Invalid/Expired URL - Please contact your Administrator.");
            $this->response($this->json($error), 200);
        }
        $sql = sprintf("SELECT user_email, last_login_dt from user WHERE MD5( `user_email` ) = '%s' limit 0,1", mysqli_real_escape_string($this->db,$emailid));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output = $row;
                    $last_login_dt_in_db = md5($row['last_login_dt']);
                    if($last_login_dt!=$last_login_dt_in_db){
                        $error = array('status' => "Failed", "msg" => "URL Expired, Please regenerate the new url.");
                        $this->response($this->json($error), 200);
                    }
                    date_default_timezone_set('UTC');
                    $datetime1 = new DateTime($received_dt);
                    $datetime2 = new DateTime(gmdate("Y-m-d H:i:s"));
                    $interval = $datetime1->diff($datetime2);
                    $hours_value = $interval->format('%h');
                    if (2<=$hours_value){
                        $error = array('status' => "Failed", "msg" => "Token expired.");
                        $this->response($this->json($error), 200);
                    }
                }
                $out = array('status' => "Success", 'msg'=>$output);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "URL Expired, Please regenerate the new url.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    //Reset Password 
    protected function resetPasswordForUser($token,$password,$retypepassword) {
        
        $stringArray = explode('-tgs-', $token);
        
        $emailid = $stringArray[0];
        $last_login_dt = $stringArray[1];
        $received_server_dt = $stringArray[2];
        $received_dt_Array = explode('-', $received_server_dt);
        $received_dt = $received_dt_Array[3];

        $sql = sprintf("SELECT user_email, last_login_dt from user WHERE MD5( `user_email` ) = '%s' limit 0,1", mysqli_real_escape_string($this->db,$emailid));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output = $row;
                    $last_login_dt_in_db = md5($row['last_login_dt']);
                    if($last_login_dt!=$last_login_dt_in_db){
                        $error = array('status' => "Failed", "msg" => "URL Expired, Please regenerate the new url.");
                        $this->response($this->json($error), 200);
                    }
                    date_default_timezone_set('UTC');
                    $datetime1 = new DateTime($received_dt);
                    $datetime2 = new DateTime(gmdate("Y-m-d H:i:s"));
                    $interval = $datetime1->diff($datetime2);
                    $hours_value = $interval->format('%h');
                    $minss_value = $interval->format('%i');
                    if (2<=$hours_value){
                        $error = array('status' => "Failed", "msg" => "Token expired.");
                        $this->response($this->json($error), 200);
                    }else{
                        
                        if($password === $retypepassword){
                            
                            $password = md5($password);
                            $update_company = sprintf("UPDATE `user` SET `user_password`='%s' WHERE MD5( `user_email` ) = '%s'", mysqli_real_escape_string($this->db,$password), mysqli_real_escape_string($this->db,$emailid));
                            $result = mysqli_query($this->db, $update_company);

                            //Print Company details after login success
                            if (!$result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            } else {
                                $update_lastlogin = sprintf("UPDATE `user` SET `last_login_dt`= NOW() WHERE MD5( `user_email` ) = '%s'", mysqli_real_escape_string($this->db,$emailid));
                                $update_result = mysqli_query($this->db, $update_lastlogin);
                    
                                $out = array('status' => "Success", 'msg' => "Password updated successfully.");
                                $this->response($this->json($out), 200);
                            }
                        }else{
                            $error = array('status' => "Failed", "msg" => "Password and Retype password doesn't match.");
                            $this->response($this->json($error), 200);
                        }
                        
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "The data doesn't match with our records.");
                $this->response($this->json($error), 200);
            }
        }
    }

    public function aws_s3_link($access_key, $secret_key, $bucket, $canonical_uri, $expires, $region, $extra_headers = array()) {

        $encoded_uri = str_replace('%2F', '/', rawurlencode($canonical_uri));
        $signed_headers = array();
        foreach ($extra_headers as $key => $value) {
            $signed_headers[strtolower($key)] = $value;
        }
        if (!array_key_exists('host', $signed_headers)) {
            $signed_headers['host'] = ($region == 'us-east-1') ? "$bucket.s3.amazonaws.com" : "$bucket.s3-$region.amazonaws.com";
        }
        ksort($signed_headers);
        $header_string = '';
        foreach ($signed_headers as $key => $value) {
            $header_string .= $key . ':' . trim($value) . "\n";
        }
        $signed_headers_string = implode(';', array_keys($signed_headers));
        $timestamp = time();
        $date_text = gmdate('Ymd', $timestamp);
        $time_text = $date_text . 'T000000Z';
        $algorithm = 'AWS4-HMAC-SHA256';
        $scope = "$date_text/$region/s3/aws4_request";
        $x_amz_params = array(
            'X-Amz-Algorithm' => $algorithm,
            'X-Amz-Credential' => $access_key . '/' . $scope,
            'X-Amz-Date' => $time_text,
            'X-Amz-SignedHeaders' => $signed_headers_string
        );
        if ($expires > 0)
            $x_amz_params['X-Amz-Expires'] = $expires;
        ksort($x_amz_params);
        $query_string_items = array();
        foreach ($x_amz_params as $key => $value) {
            $query_string_items[] = rawurlencode($key) . '=' . rawurlencode($value);
        }
        $query_string = implode('&', $query_string_items);
        $canonical_request = "GET\n$encoded_uri\n$query_string\n$header_string\n$signed_headers_string\nUNSIGNED-PAYLOAD";
        $string_to_sign = "$algorithm\n$time_text\n$scope\n" . hash('sha256', $canonical_request, false);
        $signing_key = hash_hmac('sha256', 'aws4_request', hash_hmac('sha256', 's3', hash_hmac('sha256', $region, hash_hmac('sha256', $date_text, 'AWS4' . $secret_key, true), true), true), true);
        $signature = hash_hmac('sha256', $string_to_sign, $signing_key);
        $url = 'http://' . $signed_headers['host'] . $encoded_uri . '?' . $query_string . '&X-Amz-Signature=' . $signature;
        return $url;
    }
    
    protected function changeToSignedURL($url){
        $stringArray = explode('/', $url);
        //$bucket = "mystudio-test-private";
        $bucket = "mystudio-test.technogemsinc.com";
        $stringArray = explode('/', $url);
        $canonical_uri = "/" . $stringArray[4] . "/" . $stringArray[5];
        $extra_headers = array();
        $expires = "86400";
        $signed_url = $this->aws_s3_link('AKIAI3W3VQSR2VDGYZHA', 'hURk+Od5sZ2+SYKL/IDJ+PDolKSaVFY2JIrKC0GI', $bucket, $canonical_uri, $expires, 'us-east-1', $extra_headers);
        return $signed_url;
    }
    
    //Delete Message
    protected function deleteMessage($company_id, $message_id) {
        $del_msg = sprintf("DELETE FROM message_mapping WHERE `message_id`='%s' and `company_id`='%s'", mysqli_real_escape_string($this->db, $message_id), mysqli_real_escape_string($this->db, $company_id));
        $del_res = mysqli_query($this->db, $del_msg);
        if (!$del_res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$del_msg");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
        
        $update_event = sprintf("DELETE FROM `message` WHERE `message_id`='%s' and `company_id`='%s'", mysqli_real_escape_string($this->db, $message_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $update_event);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $out = array('status' => "Success", 'msg' => "Message Deleted Successfully.");
                // If success everythig is good send header as "OK" and user details
            $this->response($this->json($out), 200);
        }
    }
    
    protected function updateMenuNamesInDB($company_id,$curriculum_name,$event_name,$message_name,$trial_name,$membership_name,$leads_name,$retail_name) {

       $sql = sprintf("UPDATE `company` SET `curriculum_name`='%s',`event_name`='%s',`message_name`='%s',`trial_name`='%s',`membership_name`='%s',`leads_name`='%s',`retail_name`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$curriculum_name), mysqli_real_escape_string($this->db,$event_name), mysqli_real_escape_string($this->db,$message_name), mysqli_real_escape_string($this->db,$trial_name),mysqli_real_escape_string($this->db,$membership_name),mysqli_real_escape_string($this->db,$leads_name),mysqli_real_escape_string($this->db,$retail_name),$company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
           $out = array('status' => "Success", 'msg' => "Menu Headings Updated Successfully.");
                // If success everythig is good send header as "OK" and user details
           $this->response($this->json($out), 200);
        }
    }
    
    //Get Menu Headings From company
    protected function getMenuNamesFromDB($company_id) {

        $sql = sprintf("SELECT `curriculum_name`,`event_name`,`message_name`,`trial_name`,`membership_name`,`leads_name`,`retail_name` FROM `company` WHERE company_id = '%s'", $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $curriculum_name = $row['curriculum_name'];
                    $event_name = $row['event_name'];
                    $message_name = $row['message_name'];
                    $trial_name = $row['trial_name'];
                    $membership_name = $row['membership_name'];
                    $leads_name = $row['leads_name'];
                    $retail_name = $row['retail_name'];
                    
                    if (!empty($curriculum_name)) {
                        
                    } else {
                        $row['curriculum_name'] = "Curriculum";
                    }

                    if (!empty($event_name)) {
                        
                    } else {
                        $row['event_name'] = "Events";
                    }

                    if (!empty($message_name)) {
                        
                    } else {
                        $row['message_name'] = "Messages";
                    }
                    if (!empty($trial_name)) {
                        
                    } else {
                        $row['trial_name'] = "Trial Program";
                    }
                    if (!empty($membership_name)) {
                        
                    } else {
                        $row['membership_name'] = "Memberships";
                    }
                    if (!empty($leads_name)) {
                        
                    } else {
                        $row['leads_name'] = "Leads";
                    }
                    if (!empty($retail_name)) {
                        
                    } else {
                        $row['retail_name'] = "Retail";
                    }
                    $output = $row;
                }
                $out = array('status' => "Success", 'msg' => $output);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Company doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
    //Delete Student
    protected function deleteStudentInDB($company_id, $student_id) {

        $select_student = sprintf("SELECT * FROM `student` WHERE `student_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
        $result_student = mysqli_query($this->db, $select_student);
        if (!$result_student) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_student");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_student);
            if($num_rows>0){
                $del_flag_student = sprintf("UPDATE `student` SET `deleted_flag`='Y' WHERE `student_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
                $result_updation = mysqli_query($this->db, $del_flag_student);
                if (!$result_updation) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$del_flag_student");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $out = array('status' => "Success", 'msg' => "Student Deleted Successfully.");
                    $this->response($this->json($out), 200);
                }
            }else{
                $out = array('status' => "Success", 'msg' => "Student Details mismatched.");
                    // if student already exists set D flag to the student
                $this->response($this->json($out), 200);
            }
        }
//        $check_participation=sprintf("SELECT e.`event_end_date`,e.`event_type`,e.`event_status`,er.`event_reg_id`,er.`event_id`,'events' as category
//                                      FROM `event_registration` er
//                                      LEFT JOIN event e on er.`event_id`=  IF(e.`event_type`='C',e.`parent_id` ,IF(e.`event_type`='S',er.`event_id`,'') )  WHERE er.`company_id`='%s' AND er.`student_id`='%s'
//                                      UNION
//                                      SELECT `membership_structure`,`membership_status`,`membership_registration_id`,`membership_id`,'membership' as category FROM `membership_registration`  WHERE `company_id`='%s' AND `student_id`='%s'");
//        $check_event_participation = sprintf("SELECT * FROM `event_registration` WHERE `company_id`='%s' AND `student_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id) );
//        $check_participation = sprintf("SELECT `event_reg_id` FROM `event_registration` WHERE `company_id`='%s' AND `student_id`='%s'
//                                        UNION
//                                        SELECT `membership_registration_id` FROM `membership_registration`  WHERE `company_id`='%s' AND `student_id`='%s'",
//                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
//
//        $result_check_participation = mysqli_query($this->db, $check_participation);
//        if (!$result_check_participation) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_participation");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//        } else {
//            $num_rows = mysqli_num_rows($result_check_participation);
//            if ($num_rows > 0) {
//                $del_flag_student = sprintf("UPDATE student SET `deleted_flag`='Y' WHERE `student_id`='%s' AND `company_id`='%s'  ", mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
//                $result_updation = mysqli_query($this->db, $del_flag_student);
//                if (!$result_updation) {
//                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$del_flag_student");
//                    log_info($this->json($error_log));
//                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                    $this->response($this->json($error), 200);
//                } else {
//                    $out = array('status' => "Success", 'msg' => "Student Deleted Successfully1.");
//                    // if student already exists set D flag to the student
//                    $this->response($this->json($out), 200);
//                }
////                 }else{
////                $error = array('status' => "Failed", "msg" => "Student is already registered with an event or membership. So unable to remove the student.");
////                $this->response($this->json($error), 200);
////                }
//            } else {
//                $delete_student = sprintf("DELETE FROM `student` WHERE `student_id`='%s' and `company_id`='%s' AND `deleted_flag`!='Y' ", $student_id, $company_id);
//                $result = mysqli_query($this->db, $delete_student);
//
//                if (!$result) {
//                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$delete_student");
//                    log_info($this->json($error_log));
//                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                    $this->response($this->json($error), 200);
//                } else {
//                    $out = array('status' => "Success", 'msg' => "Student Deleted Successfully2.");
//                    // If success everythig is good send header as "OK" and success message
//                    $this->response($this->json($out), 200);
//                }
//            }
//        }
    }

    //Referral Details
    protected function getReferralDetailsFromDB($company_id,$student_id,$start_date,$end_date){
        $include_student = $stud_id = $include_start = $st_date = $include_end = $e_date = "";
        if($student_id!=''){
            $include_student = "AND `student_id`=";
            $stud_id = "'$student_id'";
        }
        if(!empty($start_date)){
            $include_start = "AND DATE(`referred_date_time`)>=";
            $st_date = "'$start_date'";
        }
        if(!empty($end_date)){
            $include_end = "AND DATE(`referred_date_time`)<=";
            $e_date = "'$end_date'";
        }
        $query = sprintf("SELECT concat(s.`student_name`,' ',s.`student_lastname`) student_name, s.`student_id`, r.* FROM `student` s, `referral` r WHERE s.`company_id`=r.`company_id` AND s.`student_id`=r.`student_id` AND r.`company_id`='%s' %s %s %s %s %s %s AND s.`deleted_flag`!='Y' ",$company_id,$include_student,$stud_id,$include_start,$st_date,$include_end,$e_date);
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output['referral_details'][] = $row;
                }
                $out = array('status' => "Success", "msg" => $output);
                // If success everythig is good send header as "OK" and success message
                $this->response($this->json($out), 200);
            }else{
                $error = array('status' => "Failed", "msg" => "No referrals yet.");
                $this->response($this->json($error), 200);// If no records "No Content" status
            }
        }
    }

    //Update Referral Message
    protected function updateReferralMessageDetails($company_id,$ref_msg,$user_id){
        
        $update_query = sprintf("UPDATE `company` SET `referral_message`='%s' WHERE company_id = '%s'", mysqli_real_escape_string($this->db,$ref_msg), $company_id);
        $update_result = mysqli_query($this->db, $update_query);

        if (!$update_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getcompanydetails($company_id,$user_id,0); //0 - No call to Salesforce Api
        }
    }
    
    //delete user account by admin login
    protected function deleteAccountDetailsForUser($email){
        $query1 = sprintf("SELECT user_id,user_email,user_password,company_id,deleted_flag from user where user_email='%s' limit 0,1", mysqli_real_escape_string($this->db,$email));
        $result1 = mysqli_query($this->db, $query1);
        
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows1 = mysqli_num_rows($result1);
            if ($num_of_rows1 > 0) {
                $row1 = mysqli_fetch_assoc($result1);
                $company_id = $row1['company_id'];
                $flag = $row1['deleted_flag'];
                
                if($flag=='Y'){
                    $error_log = array('status' => "Failed", "msg" => "User already deleted.", "query" => "$query1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "User account already deleted.");
                    $this->response($this->json($error), 200);
                }
                $query2 = sprintf("UPDATE user SET deleted_flag='Y', deleted_date=now() where user_email='%s'", mysqli_real_escape_string($this->db,$email));
                $result2 = mysqli_query($this->db, $query2);
                
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if(mysqli_affected_rows($this->db)>0){
                        $query3 = sprintf("SELECT company_name, deleted_flag from company where company_id='%s' limit 0,1", mysqli_real_escape_string($this->db,$company_id));
                        $result3 = mysqli_query($this->db, $query3);

                        if (!$result3) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            $num_of_rows2 = mysqli_num_rows($result3);
                            if ($num_of_rows2 > 0) {
                                $row2 = mysqli_fetch_assoc($result3);
                                $flag = $row2['deleted_flag'];
                                if($flag=='Y'){
                                    $return = array('status' => "Success", "msg" => "User account deleted successfully.");
                                    $this->response($this->json($return), 200);
                                }else{
                                    $query4 = sprintf("UPDATE company SET deleted_flag='Y', deleted_date=now() where company_id='%s'", mysqli_real_escape_string($this->db,$company_id));
                                    $result4 = mysqli_query($this->db, $query4);

                                    if (!$result4) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query4");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    } else {
                                        if(mysqli_affected_rows($this->db)>0){
                                            $return = array('status' => "Success", "msg" => "User account deleted successfully.");
                                            $this->response($this->json($return), 200);
                                        }else{
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query4");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 200);
                                        }
                                    }
                                }
                            }else{
                                $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
                                $this->response($this->json($error), 200);
                            }
                        }
                    }else{
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "User doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }
        
    }
    
//    //register Company in Salesforce API
    protected function registerCompanyinSalesforceApi($company_id,$company_name,$email,$phone_number, $user_firstname, $user_lastname, $user_phone, $company_type, $country){
        $company_type_arr = ["Martial Arts", "Dance", "Gymnastics", "Crossfit", "Nonprofit", "Church", "Consulting", "Yoga", "Summer Camps"];
        $contact_id = $account_id = $list_item_id = '';
        if($country=='US' || $country=='CA'){
            $country_std_code = '+1';
        }elseif($country=='UK'){
            $country_std_code = '+44';
        }elseif($country=='AU'){
            $country_std_code = '+61';
        }else{
            $country_std_code = '';
        }
        $user_phone_with_std_code = $country_std_code.$user_phone;
        $json = array("properties"=>array("name"=>array(array("value"=>$company_name)),"email"=>array(array("value"=>$email,"metadata"=>array("stype"=>"work","primary"=>"true"))),"phone"=>array(array("value"=>"$user_phone_with_std_code","metadata"=>array("stype"=>"work","primary"=>"true","raw"=>"$user_phone_with_std_code")))));
        $postData = json_encode($json);
        $sf_response = $this->sf->accessSalesforceApi('contacts','POST',$postData);
        log_info("registerCompanyinSalesforceApi_ContactCreation: ".$sf_response['status']);
        if($sf_response['status']=='Success'){
            $sf_output = $sf_response['msg'];
            $contact_id = $sf_output['id'];
        }
        if(in_array($company_type, $company_type_arr)){
            $c_type = array_search($company_type, $company_type_arr);
        }else{
            $c_type = 9;
        }
        $json3 = array("name"=>$company_name,"contactIds"=>array("$contact_id"),"fieldValues"=>array("process_status"=>array(array("raw"=>"0")), "3"=>array(array("raw"=>"0")), "5"=>array(array("raw"=>"0")), "7"=>array(array("raw"=>"$phone_number")), "17"=>array(array("raw"=>"1")), "24"=>array(array("raw"=>"0")),
            "30"=>array(array("raw"=>"$c_type")), "36"=>array(array("raw"=>"$user_firstname")), "37"=>array(array("raw"=>"$user_lastname"))));
        $postData3 = json_encode($json3);
        $sf_response3 = $this->sf->accessSalesforceApi("lists/$this->list_id/listitems",'POST',$postData3);
        log_info("registerCompanyinSalesforceApi_ListItemCreation: ".$sf_response3['status']);
        if($sf_response3['status']=='Success'){
            $sf_output3 = $sf_response3['msg'];
            $list_item_id = $sf_output3['id'];
        }
        
//        if($check_already_available=='N'){
            $sf_sql = sprintf("UPDATE `company` set `contact_id`='%s', `list_item_id`='%s' where company_id='%s'",$contact_id,$list_item_id,$company_id);
            $sf_result = mysqli_query($this->db, $sf_sql);
            if (!$sf_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sf_sql");
                log_info($this->json($error_log));
            }else{
                $succ_log = array('status' => "Success", "msg" => "Updated Successfully.", "query" => "$sf_sql");
                log_info($this->json($succ_log));
            }
//        }
        exit();
    }
    
    //update Company Details in Salesforce API
    protected function updateCompanyDetailsinSalesforceApi($company_id){
        $contact_id = $account_id = $list_item_id = '';
        $company_details = [];
        $sql = sprintf("SELECT c.*, u.`user_email` FROM `company` c, `user` u WHERE c.`company_id`='%s' AND c.`company_id`=u.`company_id`", $company_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $company_details = mysqli_fetch_assoc($result);
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records.", "query" => "$sql");
                log_info($this->json($error_log));
            }
        }
        if(isset($company_details['contact_id']) && !empty($company_details['contact_id'])){
            $contact_id = $company_details['contact_id'];
//            $account_id = $company_details['account_id'];
            $list_item_id = $company_details['list_item_id'];
              
            $email = $company_details['user_email'];
            $company_name = $company_details['company_name'];
            $phone_number = "+".$company_details['phone_number'];
            $address = $company_details['street_address'].", ".$company_details['city'].", ".$company_details['state']." ".$company_details['postal_code'].", ".$company_details['country'];
            $web_page = $company_details['web_page'];
            
            $json = array("id"=>"$contact_id","properties"=>array("name"=>array(array("value"=>$company_name)),"email"=>array(array("value"=>$email,"metadata"=>array("stype"=>"work","primary"=>"true"))),"address"=>array(array("value"=>"$address","metadata"=>new stdClass)),"phone"=>array(array("value"=>"$phone_number","metadata"=>array("stype"=>"work","primary"=>"true","raw"=>"$phone_number")))));
            $postData = json_encode($json);
            $sf_response = $this->sf->accessSalesforceApi("contacts/$contact_id",'PUT',$postData);
            log_info("updateCompanyDetailsinSalesforceApi_ContactsUpdation: ".$sf_response['status']);
            
            if(!empty($list_item_id)&&!empty($web_page)){
                if($company_details['wepay_status']=='Y'){
                    $wp_status = "1";
                }else{
                    $wp_status = "0";
                }
                $json3 = array("contactIds"=>array("$contact_id"),"fieldValues"=>array("6"=>array(array("raw"=>"$web_page")), "24"=>array(array("raw"=>"$wp_status"))));                
                $postData3 = json_encode($json3);
                $sf_response3 = $this->sf->accessSalesforceApi("lists/$this->list_id/listitems/$list_item_id",'PUT',$postData3);
                log_info("updateCompanyDetailsinSalesforceApi_ListItemUpdation: ".$sf_response3['status']);
            }
        
        }
        exit();
    }
    
    //Default logo image from server folder
    protected function getDefaultLogoImage(){
        $filename = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->upload_folder_name/Default/yourlogo.png";
        return $filename;
    }
    
    //get CC details for provided company_id & customer_id
    protected function getCreditCardAccountDetails($company_id){
        $cust_id = '';
        
        $query = sprintf("select ps_customer_id from user_account where company_id='%s' and `flag`!='D' group by `ps_customer_id`",mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $query);        
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while($row = mysqli_fetch_assoc($result)){
                    $cust_id = $row['ps_customer_id'];
                    if(!empty(trim($cust_id))){
                        $card_details[] = $this->getCreditCardDetails($cust_id);
                    }
                }
                $this->response($this->json($card_details), 200); //send card details response with OK
            }else{
                $error = array('status' => "Failed", "msg" => "No card details added to this Company.");
                $this->response($this->json($error), 200);// If no records "No Content" status
            }
        }
    }
    
    //get CC details from Paysimple API by requesting customer_id
    private function getCreditCardDetails($customer_id){
        $ps_response = $this->ps->accessPaySimpleApi("customer/$customer_id/creditcardaccounts",'GET','');
        log_info("getCreditCardDetails: ".$ps_response['status']);
//        $ps_response = stripslashes($ps_response);
        return $ps_response;
//        $this->response($this->json($ps_response), 200); //send Paysimple API response with OK
    }
    
    //get checkout token from Paysimple API for given company_id
    protected function getcheckouttoken(){
        $ps_response = $this->ps->accessPaySimpleApi("checkouttoken",'POST','');
        log_info("getcheckouttoken: ".$ps_response['status']);
//        $ps_response = stripslashes($ps_response);
        $this->response($this->json($ps_response), 200); //send Paysimple API response with OK
    }
    
    //send payment details to Paysimple API
    protected function paymentPS($company_id,$user_id,$customer_id,$account_id,$payment_type,$payment_amount,$setup_fee,$payee_type,$firstname,$lastname,$email,$phone,$address,$city,$state,$country,$zip){
//        $current_date = gmdate("Y-m-d H:i:s");
        $curr_date = gmdate("Y-m-d");
        $upgrade_during_subscription_period = 'N';
        $rem_day_payment_ua = -1;
        $to_upgrade = 0;
        
        if($payment_type!='P' && $payment_type!='W' && $payment_type!='M'){
            $error_log = array('status' => "Failed", "msg" => "Invalid Payment Parameters.", "payment_type" => "$payment_type");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Invalid Payment Parameters. Payment type error.");
            $this->response($this->json($error), 200);
        }
        
        $sql = sprintf("select `company_name`, `web_page`, date(`expiry_dt`) expiry, `subscription_status`, `upgrade_status`, `list_item_id`, `contact_id` from `company` where `company_id`='%s'",mysqli_real_escape_string($this->db,$company_id));
        $sql_result = mysqli_query($this->db, $sql);
        if(!$sql_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $sql_num_rows = mysqli_num_rows($sql_result);
            if($sql_num_rows>0){
                $sql_row = mysqli_fetch_assoc($sql_result);
                $company_name = $sql_row['company_name'];
                $website = $sql_row['web_page'];
                $expiry = $sql_row['expiry'];
                $sub_status = $sql_row['subscription_status'];
                $upgrade_status = $sql_row['upgrade_status'];
                $list_item_id = $sql_row['list_item_id'];
                $contact_id = $sql_row['contact_id'];
                
                if($expiry>=$curr_date && $sub_status=='Y'){
                    if($upgrade_status=='W'){
                        if($payment_type=='M'){
                            $error = array('status' => "Failed", "msg" => "You can't change White Label Plan to Premium Plan Monthly via app. Please contact your administrator.");
                            $this->response($this->json($error), 200);// Unauthorised
                        }elseif($payment_type=='P'){
                            $error = array('status' => "Failed", "msg" => "You can't change White Label Plan to Premium Plan Yearly via app. Please contact your administrator.");
                            $this->response($this->json($error), 200);// Unauthorised
                        }elseif($payment_type=='W'){
                            $error = array('status' => "Failed", "msg" => "You are already in White Label Plan. Please try payment after the company validity is expired.");
                            $this->response($this->json($error), 200);// Unauthorised
                        }
                    }elseif($upgrade_status=='P'){
                        if($payment_type=='M'){
                            $error = array('status' => "Failed", "msg" => "You can't change Premium Plan Yearly to Premium Plan Monthly via app. Please contact your administrator.");
                            $this->response($this->json($error), 200);// Unauthorised
                        }elseif($payment_type=='P'){
                            $error = array('status' => "Failed", "msg" => "You are already in Premium Plan Yearly. Please try payment after the company validity is expired.");
                            $this->response($this->json($error), 200);// Unauthorised
                        }elseif($payment_type=='W'){
                            $to_upgrade=1;
                        }
                    }elseif($upgrade_status=='M'){
                        if($payment_type=='M'){
                            $error = array('status' => "Failed", "msg" => "You are already in Premium Plan Monthly. Please try payment after the company validity is expired.");
                            $this->response($this->json($error), 200);// Unauthorised
                        }else{
                            $to_upgrade=1;
                        }
                    }
                        
                    if($to_upgrade==1){
                        $upgrade_during_subscription_period = 'Y';
                        $sql_ua = sprintf("SELECT `user_account_id`, `ps_customer_id`, `schedule_id`, `premium_type`, `payment_amount`, `expiry_dt`, `status` FROM `user_account` WHERE `company_id`='%s' AND `premium_type`='%s' AND `status`='Active' order by `user_account_id` desc limit 0,1",
                                mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$upgrade_status));
                        $result_ua = mysqli_query($this->db, $sql_ua);
                        if(!$result_ua){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_ua");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else{
                            $sql_ua_numrows = mysqli_num_rows($result_ua);
                            if($sql_ua_numrows>0){
                                $row_ua = mysqli_fetch_assoc($result_ua);
                                $ua_id = $row_ua['user_account_id'];
                                $premium_amount = $row_ua['payment_amount'];
                                $expiry_ua = $row_ua['expiry_dt'];
                                $sch_id = $row_ua['schedule_id'];

                                if($expiry!=$expiry_ua){
                                    $error = array('status' => "Failed", "msg" => "Company and Schedule expiry doesn't match. Please contact administrator.");
                                    $this->response($this->json($error), 200);
                                }
                                if($upgrade_status=='M'){
                                    $total_days_in_prev_plan = 30;
                                }elseif($upgrade_status=='P'){
                                    $total_days_in_prev_plan = 365;
                                }
                                if($expiry_ua>=$curr_date){
                                    $rem_days_ua = (strtotime($expiry_ua)-strtotime($curr_date))/86400;
                                    $per_day_payment_ua = (float)($premium_amount/$total_days_in_prev_plan);
                                    $rem_day_payment_ua = number_format((float)($per_day_payment_ua*$rem_days_ua), 2, '.', '');
                                }
                            }else{
                                $error_log = array('status' => "Failed", "msg" => "Schedule details not available.", "query" => "$sql_ua");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Previous payment details not available. Please contact your administrator.");
                                $this->response($this->json($error), 200);
                            }
                        }
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
                $this->response($this->json($error), 200);// If no records "No Content" status
            }
        }
        
        if($payee_type=='new'){
            $cust_json = array("Company"=>"$company_name","FirstName"=>"$firstname","LastName"=>"$lastname","Email"=>"$email","Phone"=>"$phone","Website"=>"$website","Id"=>$customer_id);
//            $cust_json = array("BillingAddress"=>array("StreetAddress1"=>"$address","StreetAddress2"=>"","City"=>"$city","StateCode"=>"$state","ZipCode"=>"$zip","Country"=>"$country"),
//                "ShippingSameAsBilling"=>"true","Company"=>"$company_name","FirstName"=>"$firstname","LastName"=>"$lastname","Email"=>"$email","Phone"=>"$phone","Website"=>"$website","Id"=>$customer_id);
            $cust_PostData = json_encode($cust_json);
            $ps_cust_response = $this->ps->accessPaySimpleApi("customer",'PUT',$cust_PostData);
            log_info("paymentPS: ".$ps_cust_response['status']);
            if($ps_cust_response['status'] == "Success"){
                $ps_cust_res = $ps_cust_response['msg'];
                $status_code = $ps_cust_res['Meta']['HttpStatusCode'];
                if($status_code!=200){
                    $this->response($this->json($ps_cust_response), 200);
                }
    ////            $this->responseWithPaysimple($this->json($ps_response), 200);
//            }
            }else{
                $this->response($this->json($ps_cust_response), 200);
            }
        }
        
        
        if($payment_type=='W'){         // W - White Label
            if($upgrade_during_subscription_period=='Y' && $rem_day_payment_ua>0){   //check upgrade White Label Plan from Premium Plan
                $first_payment_amount = $payment_amount+$setup_fee-$rem_day_payment_ua;
            }else{
                $first_payment_amount = $payment_amount+$setup_fee;
            }
            $execution_frequency = 9;
            $start_date = date('Y-m-d', strtotime($curr_date."+1 year"));
            $payment_desc = "MyStudio White Label Membership";
        }elseif($payment_type=='P'){        // P - Premium Plan Yearly
            if($upgrade_during_subscription_period=='Y' && $rem_day_payment_ua>0){   //check upgrade Premium Plan Yearly from Premium Plan Monthly
                $first_payment_amount = $payment_amount-$rem_day_payment_ua;
            }else{
                $first_payment_amount = $payment_amount;
            }
            $execution_frequency = 9;
            $start_date = date('Y-m-d', strtotime($curr_date."+1 year"));
            $payment_desc = "MyStudio Premium Yearly Membership";
        }elseif($payment_type=='M'){        // P - Premium Plan Monthly
            $curr_day = gmdate("j");
            $no_of_days_in_current_month = gmdate("t");
            if($curr_day!='1'){
                $monthly_rem_days = $no_of_days_in_current_month - $curr_day + 1;
                $monthly_per_day_payment = (float)($payment_amount/30);
                $monthly_rem_day_payment = number_format((float)($monthly_per_day_payment*$monthly_rem_days), 2, '.', '');
                $first_payment_amount = $monthly_rem_day_payment;
            }else{
                $first_payment_amount = $payment_amount;
            }
            $execution_frequency = 4;
            $start_date = gmdate('Y-m-d',  strtotime('first day of next month'));
            $payment_desc = "MyStudio Premium Monthly Membership";
        }
            
        if($first_payment_amount == $payment_amount){
            $json = array("AccountId"=>$account_id,"paymentAmount"=>$payment_amount,"StartDate"=>"$curr_date","ExecutionFrequencyType"=>$execution_frequency,"Description"=>"$payment_desc");
        }else{
            $json = array("AccountId"=>$account_id,"paymentAmount"=>$payment_amount,"FirstPaymentDate"=>"$curr_date","FirstPaymentAmount"=>$first_payment_amount,
                "StartDate"=>"$start_date","ExecutionFrequencyType"=>$execution_frequency,"Description"=>"$payment_desc");
        }
        
        $postData = json_encode($json);
        $ps_response = $this->ps->accessPaySimpleApi("recurringpayment",'POST',$postData);
        log_info("paymentPS: ".$ps_response['status']);
        if($ps_response['status'] == "Success"){
            $ps_res = $ps_response['msg'];
            $scheduleId = $ps_res['Response']['Id'];
            $next_schedule_with_time = gmdate('Y-m-d H:i:s', strtotime($ps_res['Response']['NextScheduleDate']));
            $next_schedule = gmdate('Y-m-d', strtotime($ps_res['Response']['NextScheduleDate']));
            $status = $ps_res['Response']['ScheduleStatus'];
//            $this->responseWithPaysimple($this->json($ps_response), 200);
        }else{
            $this->response($this->json($ps_response), 200);
        }
        $res = $ps_response;
        
        if($upgrade_during_subscription_period=='Y' && isset($sch_id) && !empty(trim($sch_id))){
            //if Annual plan payment success then suspend the old Monthly plan by schedule id
            $ps_response2 = $this->ps->accessPaySimpleApi("recurringpayment/$sch_id/suspend",'PUT','{}');
            
            if($ps_response2['curl_status']==204){
                log_info("paymentPS: Success");
                $ps_response3 = $this->ps->accessPaySimpleApi("recurringpayment/$sch_id",'GET','');
                log_info("paymentPS: ".$ps_response3['status']);
                if($ps_response3['status'] == 'Success'){
                    $ps_res3 = $ps_response3['msg'];
                    $scheduleId_suspend = $ps_res3['Response']['Id'];
                    $next_schedule_suspend = gmdate('Y-m-d', strtotime($ps_res3['Response']['NextScheduleDate']));
                    $status_suspend = $ps_res3['Response']['ScheduleStatus'];
                    if($status_suspend=='Suspended' && isset($ua_id) && !empty($ua_id)){
                        $update_suspend = sprintf("update `user_account` set `expiry_dt`='%s', `status`='%s' where `user_account_id`='%s'",
                                mysqli_real_escape_string($this->db,$curr_date),mysqli_real_escape_string($this->db,$status_suspend),$ua_id);
                        $result_suspend = mysqli_query($this->db, $update_suspend);
                        if(!$result_suspend){
                            $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_suspend");
                            log_info($this->json($err_log));
                            $res['com_status']= "Failed";
                            $res['com_msg']= "Internal Server Error.";
                            $this->response($this->json($res), 200);
                        }
                    }
                }
            }elseif($ps_response2['status'] == "Success"){
                log_info("paymentPS: Success");
                $ps_res2 = $ps_response2['msg'];
                $scheduleId_suspend = $ps_res2['Response']['Id'];
                $next_schedule_suspend = gmdate('Y-m-d', strtotime($ps_res2['Response']['NextScheduleDate']));
                $status_suspend = $ps_res2['Response']['ScheduleStatus'];
                if($status_suspend=='Suspended' && isset($ua_id) && !empty($ua_id)){
                    $update_suspend = sprintf("update `user_account` set `expiry_dt`='%s', `status`='%s' where `user_account_id`='%s'",
                            mysqli_real_escape_string($this->db,$curr_date),mysqli_real_escape_string($this->db,$status_suspend),$ua_id);
                    $result_suspend = mysqli_query($this->db, $update_suspend);
                    if(!$result_suspend){
                        $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_suspend");
                        log_info($this->json($err_log));
                        $res['com_status']= "Failed";
                        $res['com_msg']= "Internal Server Error.";
                        $this->response($this->json($res), 200);
                    }
                }
    //            $this->responseWithPaysimple($this->json($ps_response), 200);
            }else{
                $this->response($this->json($ps_response2), 200);
            }
        }
        
        $sql2 = sprintf("update `company` set `expiry_dt`='%s', `subscription_status`='%s', `upgrade_status`='%s' where `company_id`='%s'",
                mysqli_real_escape_string($this->db,$next_schedule_with_time),'Y',mysqli_real_escape_string($this->db,$payment_type),mysqli_real_escape_string($this->db,$company_id));
        $sql_result2 = mysqli_query($this->db, $sql2);
        if(!$sql_result2){
            $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($err_log));
            $res['com_status']= "Failed";
            $res['com_msg']= "Internal Server Error.";
            $this->response($this->json($res), 200);
        }
        
        $query = sprintf("select * from `user_account` where `company_id`='%s' and `ps_customer_id`='%s' order by `user_account_id` desc limit 0,1",
                mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$customer_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $res['user_acc_status']= "Failed";
            $res['user_acc_msg']= "Internal Server Error.";
            $this->response($this->json($res), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                if(!empty($payee_type) && $payee_type=="old"){
                    $query2 = sprintf("INSERT INTO `user_account`(`user_id`, `company_id`, `firstname`, `lastname`, `email`, `phone`, `streetaddress`, `city`, `state`, `country`, `zip`,
                        `ps_customer_id`, `schedule_id`, `premium_type`, `payment_amount`, `setup_fee`, `expiry_dt`, `status`, `last_payment_date`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                            mysqli_real_escape_string($this->db,$user_id),mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$row['firstname']),
                            mysqli_real_escape_string($this->db,$row['lastname']),mysqli_real_escape_string($this->db,$row['email']),mysqli_real_escape_string($this->db,$row['phone']),
                            mysqli_real_escape_string($this->db,$row['streetaddress']),mysqli_real_escape_string($this->db,$row['city']),mysqli_real_escape_string($this->db,$row['state']),
                            mysqli_real_escape_string($this->db,$row['country']),mysqli_real_escape_string($this->db,$row['zip']),mysqli_real_escape_string($this->db,$customer_id),mysqli_real_escape_string($this->db,$scheduleId),
                            mysqli_real_escape_string($this->db,$payment_type),mysqli_real_escape_string($this->db,$payment_amount),mysqli_real_escape_string($this->db,$setup_fee),
                            mysqli_real_escape_string($this->db,$next_schedule),mysqli_real_escape_string($this->db,$status),mysqli_real_escape_string($this->db,$next_schedule));
                }else{
                    $query2 = sprintf("INSERT INTO `user_account`(`user_id`, `company_id`, `firstname`, `lastname`, `email`, `phone`, `streetaddress`, `city`, `state`, `country`, `zip`,
                    `ps_customer_id`, `schedule_id`, `premium_type`, `payment_amount`, `setup_fee`, `expiry_dt`, `status`, `last_payment_date`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                        mysqli_real_escape_string($this->db,$user_id),mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$firstname),
                        mysqli_real_escape_string($this->db,$lastname),mysqli_real_escape_string($this->db,$email),mysqli_real_escape_string($this->db,$phone),
                        mysqli_real_escape_string($this->db,$address),mysqli_real_escape_string($this->db,$city),mysqli_real_escape_string($this->db,$state),
                        mysqli_real_escape_string($this->db,$country),mysqli_real_escape_string($this->db,$zip),mysqli_real_escape_string($this->db,$customer_id),mysqli_real_escape_string($this->db,$scheduleId),
                        mysqli_real_escape_string($this->db,$payment_type),mysqli_real_escape_string($this->db,$payment_amount),mysqli_real_escape_string($this->db,$setup_fee),
                        mysqli_real_escape_string($this->db,$next_schedule),mysqli_real_escape_string($this->db,$status),mysqli_real_escape_string($this->db,$next_schedule));
                }
            }else{
                $query2 = sprintf("INSERT INTO `user_account`(`user_id`, `company_id`, `firstname`, `lastname`, `email`, `phone`, `streetaddress`, `city`, `state`, `country`, `zip`,
                    `ps_customer_id`, `schedule_id`, `premium_type`, `payment_amount`, `setup_fee`, `expiry_dt`, `status`, `last_payment_date`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                        mysqli_real_escape_string($this->db,$user_id),mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$firstname),
                        mysqli_real_escape_string($this->db,$lastname),mysqli_real_escape_string($this->db,$email),mysqli_real_escape_string($this->db,$phone),
                        mysqli_real_escape_string($this->db,$address),mysqli_real_escape_string($this->db,$city),mysqli_real_escape_string($this->db,$state),
                        mysqli_real_escape_string($this->db,$country),mysqli_real_escape_string($this->db,$zip),mysqli_real_escape_string($this->db,$customer_id),mysqli_real_escape_string($this->db,$scheduleId),
                        mysqli_real_escape_string($this->db,$payment_type),mysqli_real_escape_string($this->db,$payment_amount),mysqli_real_escape_string($this->db,$setup_fee),
                        mysqli_real_escape_string($this->db,$next_schedule),mysqli_real_escape_string($this->db,$status),mysqli_real_escape_string($this->db,$next_schedule));
            }
            $result2 = mysqli_query($this->db,$query2);
            if(!$result2){
                $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                log_info($this->json($error_log2));
                $res['user_acc_status']= "Failed";
                $res['user_acc_msg']= "Internal Server Error.";
                $this->response($this->json($res), 200);
            }else{
                if(!empty($list_item_id)){
                    if($payment_type=='W'){
                        $stat = 4;
                    }elseif($payment_type=='P'){
                        $stat = 7;                  
                    }elseif($payment_type=='M'){
                        $stat = 10;                  
                    }
                    $json3 = array("contactIds"=>array("$contact_id"),"fieldValues"=>array("process_status"=>array(array("raw"=>"$stat"))));  
                    $postData3 = json_encode($json3);
                    $sf_response3 = $this->sf->accessSalesforceApi("lists/$this->list_id/listitems/$list_item_id",'PUT',$postData3);
                    log_info("updateCompanyDetailsinSalesforceApi_ListItemValidityUpdation: ".$sf_response3['status']);
                }
                $this->response($this->json($ps_response), 200); //send Paysimple API response with OK
            }
        }
    }
    
    //update payment card details for existing customer
    protected function paymentUpdatePS($company_id,$user_id,$customer_id,$account_id,$payment_type,$payment_amount,$payee_type,$firstname,$lastname,$email,$phone,$address,$city,$state,$country,$zip){
        $curr_date = $expiry = gmdate("Y-m-d");
        $no_of_payments_done = 0;
        $update_expiry = 'N';
        
        if($payment_type!='P' && $payment_type!='W' && $payment_type!='M'){
            $error_log = array('status' => "Failed", "msg" => "Invalid Payment Parameters.", "payment_type" => "$payment_type");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Invalid Payment Parameters. Payment type error.");
            $this->response($this->json($error), 200);
        }
        if($payment_type=='W'){             // W - White Label
            $payment_desc = "MyStudio White Label Membership";
            $freq_parameter = 9;
        }elseif($payment_type=='P'){        // P - Premium Plan Yearly
            $payment_desc = "MyStudio Premium Yearly Membership";
            $freq_parameter = 9;
        }elseif($payment_type=='M'){        // P - Premium Plan Monthly
            $payment_desc = "MyStudio Premium Monthly Membership";
            $freq_parameter = 4;
        }
        
        $sql = sprintf("select `company_name`, `web_page`, date(`expiry_dt`) expiry, `subscription_status`, `upgrade_status` from `company` where `company_id`='%s'",mysqli_real_escape_string($this->db,$company_id));
        $sql_result = mysqli_query($this->db, $sql);
        if(!$sql_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $sql_num_rows = mysqli_num_rows($sql_result);
            if($sql_num_rows>0){
                $sql_row = mysqli_fetch_assoc($sql_result);
                $company_name = $sql_row['company_name'];
                $website = $sql_row['web_page'];
                $expiry = $sql_row['expiry'];
                $sub_status = $sql_row['subscription_status'];
                $upgrade_status = $sql_row['upgrade_status'];
                
                if($upgrade_status!=$payment_type){
                    $error_log = array('status' => "Failed", "msg" => "Payment Parameters mismatch.", "payment_type" => "$payment_type");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Payment Parameters mismatch. Payment type error.");
                    $this->response($this->json($error), 200);
                }
                
                $sql_ua = sprintf("SELECT `user_account_id`, `ps_customer_id`, `schedule_id`, `premium_type`, `payment_amount`, `expiry_dt`, `status` FROM `user_account` WHERE `company_id`='%s' AND `premium_type`='%s' AND `status`='Active'",
                        mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$upgrade_status));
                $result_ua = mysqli_query($this->db, $sql_ua);
                if(!$result_ua){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_ua");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $sql_ua_numrows = mysqli_num_rows($result_ua);
                    if($sql_ua_numrows>0){
                        $row_ua = mysqli_fetch_assoc($result_ua);
                        $ua_id = $row_ua['user_account_id'];
                        $premium_amount = $row_ua['payment_amount'];
                        $expiry_ua = $row_ua['expiry_dt'];
                        $sch_id = $row_ua['schedule_id'];
                        
                        if($expiry!=$expiry_ua){
                            $error = array('status' => "Failed", "msg" => "Company and Schedule expiry doesn't match. Please contact administrator.");
                            $this->response($this->json($error), 200);
                        }
                        
                        if(!empty(trim($sch_id)) && $sch_id!=0){
                            $ps_response = $this->ps->accessPaySimpleApi("recurringpayment/$sch_id",'GET','');
                            log_info("currentPaymentDetails: ".$ps_response['status']);
                            if($ps_response['status'] == "Success"){
                                $ps_res = $ps_response['msg'];
                                $acc_id = $ps_res['Response']['AccountId'];
                                if(!empty(trim($acc_id)) && $acc_id==$account_id){
                                    $error = array('status' => "Failed", "msg" => "Same card details given again during payment update.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
                        
                    }else{
                        $error_log = array('status' => "Failed", "msg" => "Schedule details not available.", "query" => "$sql_ua");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Previous payment details not available. Please contact your administrator.");
                        $this->response($this->json($error), 200);
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "Company details doesn't exist/match.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
                $this->response($this->json($error), 200);// If no records "No Content" status
            }
        }
        
        if($payee_type=='update_new'){
            $cust_json = array("Company"=>"$company_name","FirstName"=>"$firstname","LastName"=>"$lastname","Email"=>"$email","Phone"=>"$phone","Website"=>"$website","Id"=>$customer_id);
//            $cust_json = array("BillingAddress"=>array("StreetAddress1"=>"$address","StreetAddress2"=>"","City"=>"$city","StateCode"=>"$state","ZipCode"=>"$zip","Country"=>"$country"),
//                "ShippingSameAsBilling"=>"true","Company"=>"$company_name","FirstName"=>"$firstname","LastName"=>"$lastname","Email"=>"$email","Phone"=>"$phone","Website"=>"$website","Id"=>$customer_id);
            $cust_PostData = json_encode($cust_json);
            $ps_cust_response = $this->ps->accessPaySimpleApi("customer",'PUT',$cust_PostData);
            log_info("paymentPS: ".$ps_cust_response['status']);
            if($ps_cust_response['status'] == "Success"){
                $ps_cust_res = $ps_cust_response['msg'];
                $status_code = $ps_cust_res['Meta']['HttpStatusCode'];
                if($status_code!=200){
                    $this->response($this->json($ps_cust_response), 200);
                }
    ////            $this->responseWithPaysimple($this->json($ps_response), 200);
//            }
            }else{
                $this->response($this->json($ps_cust_response), 200);
            }
        }
        
        if($expiry<=$curr_date){
            $start_date = $curr_date;
        }else{
            $start_date = $expiry;
        }
            
        $json = array("AccountId"=>$account_id,"paymentAmount"=>$payment_amount,"StartDate"=>"$start_date","ExecutionFrequencyType"=>$freq_parameter,"Description"=>"$payment_desc");
        $postData = json_encode($json);
        $ps_response = $this->ps->accessPaySimpleApi("recurringpayment",'POST',$postData);
        log_info("paymentPS: ".$ps_response['status']);
        if($ps_response['status'] == "Success"){
            $ps_res = $ps_response['msg'];
            $scheduleId = $ps_res['Response']['Id'];
            $next_schedule_with_time = gmdate('Y-m-d H:i:s', strtotime($ps_res['Response']['NextScheduleDate']));
            $next_schedule = gmdate('Y-m-d', strtotime($ps_res['Response']['NextScheduleDate']));
            $no_of_payments_done = $ps_res['Response']['NumberOfPaymentsMade'];
            if($no_of_payments_done==1){
                $update_expiry='Y';
            }
            $status = $ps_res['Response']['ScheduleStatus'];
//            $this->responseWithPaysimple($this->json($ps_response), 200);
        }else{
            $this->response($this->json($ps_response), 200);
        }
        $res = $ps_response;
        
        if(isset($sch_id) && !empty(trim($sch_id)) && $sch_id!=0){
            //if Annual plan payment success then suspend the old Monthly plan by schedule id
            $ps_response2 = $this->ps->accessPaySimpleApi("recurringpayment/$sch_id/suspend",'PUT','{}');
            
            if($ps_response2['curl_status']==204){
                log_info("paymentPS: Success");
                $ps_response3 = $this->ps->accessPaySimpleApi("recurringpayment/$sch_id",'GET','');
                log_info("paymentPS: ".$ps_response3['status']);
                if($ps_response3['status'] == 'Success'){
                    $ps_res3 = $ps_response3['msg'];
                    $scheduleId_suspend = $ps_res3['Response']['Id'];
                    $next_schedule_suspend = gmdate('Y-m-d', strtotime($ps_res3['Response']['NextScheduleDate']));
                    $status_suspend = $ps_res3['Response']['ScheduleStatus'];
                    if($status_suspend=='Suspended' && isset($ua_id) && !empty($ua_id)){
                        $update_suspend = sprintf("update `user_account` set `status`='%s', `flag`='D' where `user_account_id`='%s'",
                                mysqli_real_escape_string($this->db,$status_suspend),$ua_id);
                        $result_suspend = mysqli_query($this->db, $update_suspend);
                        if(!$result_suspend){
                            $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_suspend");
                            log_info($this->json($err_log));
                            $res['com_status']= "Failed";
                            $res['com_msg']= "Internal Server Error.";
                            $this->response($this->json($res), 200);
                        }
                    }
                }
            }elseif($ps_response2['status'] == "Success"){
                log_info("paymentPS: Success");
                $ps_res2 = $ps_response2['msg'];
                $scheduleId_suspend = $ps_res2['Response']['Id'];
                $next_schedule_suspend = gmdate('Y-m-d', strtotime($ps_res2['Response']['NextScheduleDate']));
                $status_suspend = $ps_res2['Response']['ScheduleStatus'];
                if($status_suspend=='Suspended' && isset($ua_id) && !empty($ua_id)){
                    $update_suspend = sprintf("update `user_account` set `status`='%s', `flag`='D' where `user_account_id`='%s'",
                            mysqli_real_escape_string($this->db,$status_suspend),$ua_id);
                    $result_suspend = mysqli_query($this->db, $update_suspend);
                    if(!$result_suspend){
                        $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_suspend");
                        log_info($this->json($err_log));
                        $res['com_status']= "Failed";
                        $res['com_msg']= "Internal Server Error.";
                        $this->response($this->json($res), 200);
                    }
                }
    //            $this->responseWithPaysimple($this->json($ps_response), 200);
            }else{
                $this->response($this->json($ps_response2), 200);
            }
        }
        
        if($update_expiry=='Y'){
            $sql2 = sprintf("update `company` set `expiry_dt`='%s' where `company_id`='%s'",
                    mysqli_real_escape_string($this->db,$next_schedule_with_time),mysqli_real_escape_string($this->db,$company_id));
            $sql_result2 = mysqli_query($this->db, $sql2);
            if(!$sql_result2){
                $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($err_log));
                $res['com_status']= "Failed";
                $res['com_msg']= "Internal Server Error.";
                $this->response($this->json($res), 200);
            }
        }
        
        $query = sprintf("select * from `user_account` where `company_id`='%s' and `ps_customer_id`='%s' order by `user_account_id` desc limit 0,1",
                mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$customer_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $res['user_acc_status']= "Failed";
            $res['user_acc_msg']= "Internal Server Error.";
            $this->response($this->json($res), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                if(!empty($payee_type) && $payee_type=="update_old"){
                    $query2 = sprintf("INSERT INTO `user_account`(`user_id`, `company_id`, `firstname`, `lastname`, `email`, `phone`, `streetaddress`, `city`, `state`, `country`, `zip`,
                    `ps_customer_id`, `schedule_id`, `premium_type`, `payment_amount`, `expiry_dt`, `status`, `last_payment_date`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                        mysqli_real_escape_string($this->db,$user_id),mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$row['firstname']),
                        mysqli_real_escape_string($this->db,$row['lastname']),mysqli_real_escape_string($this->db,$row['email']),mysqli_real_escape_string($this->db,$row['phone']),
                        mysqli_real_escape_string($this->db,$row['streetaddress']),mysqli_real_escape_string($this->db,$row['city']),mysqli_real_escape_string($this->db,$row['state']),
                        mysqli_real_escape_string($this->db,$row['country']),mysqli_real_escape_string($this->db,$row['zip']),mysqli_real_escape_string($this->db,$customer_id),mysqli_real_escape_string($this->db,$scheduleId),
                        mysqli_real_escape_string($this->db,$payment_type),mysqli_real_escape_string($this->db,$payment_amount),mysqli_real_escape_string($this->db,$next_schedule),mysqli_real_escape_string($this->db,$status),mysqli_real_escape_string($this->db,$next_schedule));
                }else{
                    $query2 = sprintf("INSERT INTO `user_account`(`user_id`, `company_id`, `firstname`, `lastname`, `email`, `phone`, `streetaddress`, `city`, `state`, `country`, `zip`,
                    `ps_customer_id`, `schedule_id`, `premium_type`, `payment_amount`, `expiry_dt`, `status`, `last_payment_date`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                        mysqli_real_escape_string($this->db,$user_id),mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$firstname),
                        mysqli_real_escape_string($this->db,$lastname),mysqli_real_escape_string($this->db,$email),mysqli_real_escape_string($this->db,$phone),
                        mysqli_real_escape_string($this->db,$address),mysqli_real_escape_string($this->db,$city),mysqli_real_escape_string($this->db,$state),
                        mysqli_real_escape_string($this->db,$country),mysqli_real_escape_string($this->db,$zip),mysqli_real_escape_string($this->db,$customer_id),mysqli_real_escape_string($this->db,$scheduleId),
                        mysqli_real_escape_string($this->db,$payment_type),mysqli_real_escape_string($this->db,$payment_amount),mysqli_real_escape_string($this->db,$next_schedule),mysqli_real_escape_string($this->db,$status),mysqli_real_escape_string($this->db,$next_schedule));
                }
            }else{
                $query2 = sprintf("INSERT INTO `user_account`(`user_id`, `company_id`, `firstname`, `lastname`, `email`, `phone`, `streetaddress`, `city`, `state`, `country`, `zip`,
                    `ps_customer_id`, `schedule_id`, `premium_type`, `payment_amount`, `expiry_dt`, `status`, `last_payment_date`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                        mysqli_real_escape_string($this->db,$user_id),mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$firstname),
                        mysqli_real_escape_string($this->db,$lastname),mysqli_real_escape_string($this->db,$email),mysqli_real_escape_string($this->db,$phone),
                        mysqli_real_escape_string($this->db,$address),mysqli_real_escape_string($this->db,$city),mysqli_real_escape_string($this->db,$state),
                        mysqli_real_escape_string($this->db,$country),mysqli_real_escape_string($this->db,$zip),mysqli_real_escape_string($this->db,$customer_id),mysqli_real_escape_string($this->db,$scheduleId),
                        mysqli_real_escape_string($this->db,$payment_type),mysqli_real_escape_string($this->db,$payment_amount),mysqli_real_escape_string($this->db,$next_schedule),mysqli_real_escape_string($this->db,$status),mysqli_real_escape_string($this->db,$next_schedule));
            }
            $result2 = mysqli_query($this->db,$query2);
            if(!$result2){
                $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                log_info($this->json($error_log2));
                $res['user_acc_status']= "Failed";
                $res['user_acc_msg']= "Internal Server Error.";
                $this->response($this->json($res), 200);
            }else{
                $succ_msg = array('status' => "Success", "msg" => "Card details updated successfully. Your next payment will be debited from this new card.");
                $this->response($this->json($succ_msg), 200); //send Paysimple API response with OK
            }
        }
    }
    
    //get current payment details by company_id
    protected function currentPaymentDetails($company_id) {
        $query = sprintf("select * from `user_account` where `company_id`='%s' and `status`='Active' order by `last_updt_dt` desc limit 0,1",$company_id);
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $schedule_id = $row['schedule_id'];
                if(!empty(trim($schedule_id)) && $schedule_id!=0){
                    $ps_response = $this->ps->accessPaySimpleApi("recurringpayment/$schedule_id",'GET','');
                    log_info("currentPaymentDetails: ".$ps_response['status']);
                    if($ps_response['status'] == "Success"){
                        $ps_res = $ps_response['msg'];
                        $acc_id = $ps_res['Response']['AccountId'];
                        if(!empty(trim($acc_id))){
                            $ps_response2 = $this->ps->accessPaySimpleApi("account/creditcard/$acc_id",'GET','');
                            log_info("currentPaymentDetails: ".$ps_response2['status']);
                            if($ps_response2['status'] == "Success"){
                                $ps_res = $ps_response2['msg'];
                                $this->response($this->json($ps_response2), 200); //send card details response with OK
                            }else{
                                $this->response($this->json($ps_response2), 200);// If no records "No Content" status
                            }
                        }else{
//                            $error = array('status' => "Failed", "msg" => "Account details not found.");
                            $this->response($this->json($ps_response), 200);// If no records "No Content" status
                        }
                    }else{
                        $this->response($this->json($ps_response), 200);
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => "No card details added to this Company.");
                    $this->response($this->json($error), 200);// If no records "No Content" status
                }
            }else{
                $error = array('status' => "Failed", "msg" => "No card details added to this Company.");
                $this->response($this->json($error), 200);// If no records "No Content" status
            }
        }
    }
    
    //get recurring payment details by schedule_id
    protected function getRecurringDetails($schedule_id){
        $ps_response = $this->ps->accessPaySimpleApi("recurringpayment/$schedule_id",'GET','');
        log_info("paymentPS: ".$ps_response['status']);
        if($ps_response['status']=='Success'){
            $acc_d = $ps_response['msg']['Response']['AccountId'];
            echo $acc_d;
        }else{
            echo "Failure";
            $err = $ps_response['Meta']['Errors']['ErrorCode'];
            echo $err;
        }
//        $this->response($this->json($ps_response), 200); //send Paysimple API response with OK
    }
    
    //update referral email list
    protected function updateReferralEmails($company_id,$email_list){
        $query = sprintf("update `company` set `referral_email_list`='%s' where `company_id`='%s'",str_replace(' ','',$email_list),$company_id);
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $error = array('status' => "Success", "msg" => "Successfully updated.", "email_list" => "$email_list");
            $this->response($this->json($error), 200);
        }
    }

    //update user password
    protected function updateUserPassword($company_id,$emailid,$old_password,$new_password){
        $old_password = md5($old_password);
        $new_password = md5($new_password);
        $query = sprintf("SELECT * FROM  `user` WHERE  `user_email` =  '%s'",$emailid);
        $result = mysqli_query($this->db, $query);
        
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $pswd = $row['user_password'];
                $cmpny_id = $row['company_id'];
                if($cmpny_id!=$company_id){
                    $error = array('status' => "Failed", "msg" => "User doesn't belong to the company.");
                    $this->response($this->json($error), 200);
                }
                if($pswd!=$old_password){
                    $error = array('status' => "Failed", "msg" => "Old password doesn't match with our records.");
                    $this->response($this->json($error), 200);
                }
                $query2 = sprintf("UPDATE `user` SET `user_password`='%s' WHERE `user_email`='%s' AND `user_password`='%s'",$new_password,$emailid,$old_password);
                $result2 = mysqli_query($this->db, $query2);

                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    if(mysqli_affected_rows($this->db)>0){
                        $_SESSION['pwd']=$new_password;
                        $status = array('status' => "Success", "msg" => "Password updated successfully.");
                        $this->response($this->json($status), 200);
                    }else{
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "User doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    protected function wepayStatus($company_id){
        $query = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if(!empty(trim($curr))){
                    $currency = explode(",", trim($curr));
                }
                if($row['account_state']=='active'){
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                }else{
                    $res = array('status' => "Success", "msg" => "A", "info" => "Deleted/Disabled/Action needed.");
                }
            }else{
                $res = array('status' => "Failed", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }
    
    protected function stripeStatus($company_id){
        $query = sprintf("SELECT * FROM `stripe_account` where `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        
        $account_state = '';
        
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if(!empty(trim($curr))){
                    $currency = explode(",", trim($curr));
                }
                if($row['account_state'] !== 'N'){
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                }else{
                    $res = array('status' => "Success", "msg" => "Y", "info" => "Deleted/Disabled/Action needed.");
                }
            }else{
                $res = array('status' => "Success", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }
    
    
    protected function getPaymentDetailsByCompany($company_id,$list_flag,$category,$data_limit) {

        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $current_date = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $ep_lastupdt_date = " DATE(CONVERT_TZ(ep.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $mp_lastupdt_date = " DATE(CONVERT_TZ(mp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $payment_array['Approved']=$payment_array['Past']=$payment_array['Upcoming']=[];
        $approvedtotal = 0;
        $pasttotal = 0;
        $upcomingtotal = 0;    
        $union_add = '';
        $end_limit = 2000;
        if($list_flag=='A'){
            if($category=='A'){
                $union_add = "UNION
                SELECT mr.membership_registration_id,m.membership_id, mr.student_id, mp.cc_name credit_card_name, m.category_title as event_title, mr.buyer_name , 
                mr.payment_type, CONCAT(mr.membership_registration_column_1,' ',mr.membership_registration_column_2) as Participant ,     
                IF(mp.payment_status = 'R' , concat('-',if(mr.processing_fee_type=2,(mp.payment_amount+mp.processing_fee),mp.payment_amount)), 
                if(mr.processing_fee_type=2 ,(mp.payment_amount+mp.processing_fee),mp.payment_amount)) payment_amount, 
                IF(mp.payment_status = 'R' , concat('-',if(mr.processing_fee_type=1,(mp.payment_amount-mp.processing_fee),mp.payment_amount)), 
                if(mr.processing_fee_type=1 ,(mp.payment_amount-mp.processing_fee),mp.payment_amount)) net_amount,    
                mp.payment_status as schedule_status,mp.processing_fee as processing_fees , checkout_status,
                IF(mp.payment_status = 'R','Refunded',IF(mp.payment_status = 'F','Failed',IF(mp.payment_status = 'MR','Manually Refunded',IF(checkout_status='released','Completed(released)',IF(mp.payment_status = 'M','Completed','Completed(pending)'))))) as view_status,
                mp.payment_date as schedule_date,($mp_lastupdt_date) last_updt_dt,mp.membership_payment_id, 'Membership' As Category from membership_registration mr 
                LEFT JOIN membership m ON mr.membership_id = m.membership_id 
                LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id 
                where mp.payment_amount>'0' && mr.company_id = '$company_id' && DATE($mp_lastupdt_date) BETWEEN ($current_date) - INTERVAL 30 DAY AND ($current_date) ORDER BY last_updt_dt desc LIMIT $data_limit, $end_limit";
            }else{
                $union_add = " ORDER BY last_updt_dt desc LIMIT $data_limit, $end_limit";
            }
            $sql1 = sprintf("SELECT er.event_reg_id,e.event_id, er.student_id, ep.cc_name credit_card_name, e.event_title, er.buyer_name , 
                er.payment_type, CONCAT(er.event_registration_column_1,' ',er.event_registration_column_2) as Participant ,         
                IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-',if(er.processing_fee_type=2,(ep.payment_amount+ep.processing_fee),ep.payment_amount)), 
                   if(er.processing_fee_type=2 ,(ep.payment_amount-ep.refunded_amount+ep.processing_fee),ep.payment_amount-ep.refunded_amount)) payment_amount,                     
                   IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-',if(er.processing_fee_type=1,(ep.payment_amount-ep.processing_fee),ep.payment_amount)), 
                   if(er.processing_fee_type=1 ,(ep.payment_amount-(ep.refunded_amount+ep.processing_fee)),ep.payment_amount-ep.refunded_amount)) net_amount,    
                   ep.schedule_status,ep.processing_fee as processing_fees , checkout_status,
                IF(ep.schedule_status = 'R','Refunded',IF(ep.schedule_status = 'F','Failed',IF(ep.schedule_status = 'MR','Manually Refunded',IF(checkout_status='released','Completed(released)',IF(ep.schedule_status = 'M','Completed','Completed(pending)'))))) as view_status,
                ep.schedule_date, ($ep_lastupdt_date) last_updt_dt,ep.event_payment_id, 'Events' As Category from event_registration er 
                LEFT JOIN event e ON er.event_id = e.event_id 
                LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
                where ep.payment_amount>'0' && er.company_id = '%s' && DATE($ep_lastupdt_date) BETWEEN ($current_date) - INTERVAL 30 DAY AND ($current_date) $union_add", mysqli_real_escape_string($this->db, $company_id));

            $result1 = mysqli_query($this->db, $sql1);           
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows1 = mysqli_num_rows($result1);
                if ($num_rows1 > 0) {
                    $curr_date = date("Y-m-d H:i:s");

                    while ($row1 = mysqli_fetch_assoc($result1)) {
                        $temp_amount1 = 0;
                        if ($row1['schedule_status'] == 'S' || $row1['schedule_status'] == 'R' ||$row1['schedule_status'] == 'M' || $row1['schedule_status'] == 'PR' || $row1['schedule_status'] == 'MR' || ($row1['schedule_status'] == 'F' && $row1['payment_type']=='CO')) {
                            if($row1['schedule_status']!='R' && $row1['schedule_status']!='M' && $row1['schedule_status'] != 'MR'){
                                $temp_amount1 = $row1['net_amount'];
                                $approvedtotal = $approvedtotal + $temp_amount1;
                            }
                            $payment_array['Approved'][] = $row1;
                        }
                    }
                }
            }
        } else if($list_flag=='P'){
            if($category=='A'){
                $union_add = "UNION
                    SELECT mr.membership_registration_id,m.membership_id, mr.student_id, mp.cc_name credit_card_name, m.category_title as event_title, mr.buyer_name , 
                    mr.payment_type, CONCAT(mr.membership_registration_column_1,' ',mr.membership_registration_column_2) as Participant ,
                    IF(mp.payment_status = 'R' , concat('-', mp.payment_amount), mp.payment_amount) payment_amount, mp.payment_status as schedule_status,
                    IF(mr.processing_fee_type = 2, mp.processing_fee, 0) processing_fees, ($current_date) as curr_date, mr.membership_status as status, paytime_type as paytime_type,
                    IF(mp.payment_status = 'F','Failed','Past') as view_status, checkout_status,
                    mp.payment_date as schedule_date, ($mp_lastupdt_date) last_updt_dt,mp.membership_payment_id, 'Membership' As Category from membership_registration mr 
                    LEFT JOIN membership m ON mr.membership_id = m.membership_id 
                    LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id 
                    where mr.membership_status IN ('R', 'P') AND mp.payment_amount>'0' && mr.company_id = '$company_id' && (mp.payment_status ='F' || (mp.payment_status ='N' && mp.payment_date<($current_date))) ORDER BY schedule_date desc LIMIT $data_limit, $end_limit";
            }else{
                $union_add = " ORDER BY ep.schedule_date desc LIMIT $data_limit, $end_limit";
            }
            $sql2 = sprintf("SELECT er.event_reg_id,e.event_id, er.student_id, ep.cc_name credit_card_name, e.event_title, er.buyer_name , 
                    er.payment_type, CONCAT(er.event_registration_column_1,' ',er.event_registration_column_2) as Participant ,
                    IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-', ep.payment_amount), ep.payment_amount) payment_amount, ep.schedule_status,
                    IF(er.processing_fee_type = 2, ep.processing_fee, 0) processing_fees, ($current_date) as curr_date, '' as status, '' as paytime_type,
                    IF(ep.schedule_status = 'F','Failed','Past') as view_status, checkout_status,
                    ep.schedule_date, ($ep_lastupdt_date) last_updt_dt,ep.event_payment_id, 'Events' As Category from event_registration er 
                    LEFT JOIN event e ON er.event_id = e.event_id 
                    LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
                    where ep.payment_amount>'0' && er.company_id = '%s' && (ep.schedule_status ='F' || (ep.schedule_status ='N' && ep.schedule_date<($current_date))) $union_add", mysqli_real_escape_string($this->db, $company_id));
        
            $result2 = mysqli_query($this->db, $sql2);

            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows2 = mysqli_num_rows($result2);
                if ($num_rows2 > 0) {
//                    $curr_date = date("Y-m-d H:i:s");

                    while ($row2 = mysqli_fetch_assoc($result2)) {
//                        $curr_date = $row2['curr_date'];
                        $temp_amount2 = 0; //                       
                        if ($row2['schedule_status'] == 'F' && ($row2['payment_type']=='RE' || $row2['payment_type']=='R')) {
                            $temp_amount2 = $row2['payment_amount'] + $row2['processing_fees'];

                            $pasttotal = $pasttotal + $temp_amount2;
                            $payment_array['Past'][] = $row2;
                        } elseif ($row2['schedule_status'] == 'N') {
                            if($row2['Category']=='Membership' && $row2['status']=='P' && $row2['paytime_type']=='R'){
                                
                            }else{
                                $temp_amount2 = $row2['payment_amount'] + $row2['processing_fees'];
                                if ($row2['payment_amount'] < 0) {
                                    $temp_amount2 = $row2['payment_amount'] - $row2['processing_fees'];
                                }
                                if(strtotime($row2['schedule_date']) < strtotime($row2['curr_date'])){
    //                            if ($row2['schedule_date'] < $curr_date) {
                                    $pasttotal = $pasttotal + $temp_amount2;
                                    $payment_array['Past'][] = $row2;
                                }
                            }
                        }
                    }
                }
            }
        } else if($list_flag=='U'){
            if($category=='A'){
                $union_add = "UNION
                SELECT mr.membership_registration_id,m.membership_id, mr.student_id, mp.cc_name credit_card_name, m.category_title as event_title, mr.buyer_name , 
                mr.payment_type, CONCAT(mr.membership_registration_column_1,' ',mr.membership_registration_column_2) as Participant ,
                mp.payment_amount, mp.payment_status as schedule_status,
                IF(mr.processing_fee_type = 2 ,mp.processing_fee, 0) processing_fees, ($current_date) as curr_date, 'Pending' as view_status, checkout_status,
                mp.payment_date as schedule_date, ($mp_lastupdt_date) last_updt_dt,mp.membership_payment_id, 'Membership' As Category from membership_registration mr 
                LEFT JOIN membership m ON mr.membership_id = m.membership_id 
                LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id 
                where (mr.membership_status='R' || (mr.membership_status='P' AND mr.`resume_date`>=($current_date) AND mr.`resume_date` NOT IN (NULL, '0000-00-00'))) AND mp.payment_amount>'0' and mr.company_id = '$company_id' and (mp.payment_status ='S'  BETWEEN ($current_date) AND ($current_date) + INTERVAL 30 DAY) AND mp.payment_date>=($current_date) ORDER BY schedule_date LIMIT $data_limit, $end_limit";
            }else{
                $union_add = " ORDER BY ep.schedule_date LIMIT $data_limit, $end_limit";
            }
            $sql3 = sprintf("SELECT er.event_reg_id,e.event_id, er.student_id, ep.cc_name credit_card_name, e.event_title, er.buyer_name , 
                er.payment_type, CONCAT(er.event_registration_column_1,' ',er.event_registration_column_2) as Participant ,
                IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-', ep.payment_amount), ep.payment_amount) payment_amount, ep.schedule_status,
                IF(er.processing_fee_type = 2 ,ep.processing_fee, 0) processing_fees, ($current_date) as curr_date, 'Pending' as view_status, checkout_status,
                ep.schedule_date, ($ep_lastupdt_date) last_updt_dt,ep.event_payment_id, 'Events' As Category from event_registration er 
                LEFT JOIN event e ON er.event_id = e.event_id 
                LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
                where ep.payment_amount>'0' and er.company_id = '%s' and (ep.schedule_status ='N' and ep.schedule_date BETWEEN ($current_date) AND ($current_date) + INTERVAL 30 DAY) AND ep.schedule_date>=($current_date) $union_add", mysqli_real_escape_string($this->db,$company_id));
            $result3 = mysqli_query($this->db, $sql3);

            if (!$result3) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows3 = mysqli_num_rows($result3);
                if ($num_rows3 > 0) {
//                    $curr_date = date("Y-m-d H:i:s");
                    while ($row3 = mysqli_fetch_assoc($result3)) {
                        $temp_amount3 = 0;
                        if ($row3['schedule_status'] == 'N') {
                            $temp_amount = $row3['payment_amount'] + $row3['processing_fees'];
                             $upcomingtotal = $upcomingtotal + $temp_amount;
                            $payment_array['Upcoming'][] = $row3;
                        }
                    }
                }
            }
        }

        $total['Approved_total'] = $approvedtotal;
        $total['Past_total'] = $pasttotal;
        $total['Upcoming_total'] = $upcomingtotal;
        $msg = array('status' => "Success", "msg" => $payment_array, "total" => $total);
        $this->response($this->json($msg), 200);
        date_default_timezone_set($curr_time_zone);
    }                                                                                                                   
    
   protected function getPaymentDetailsByFilterType($company_id,$payment_type,$payment_days_type,$payment_startdate_limit,$payment_enddate_limit,$category,$search,$draw_table,$length_table,$start,$sorting){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $last_updtdate_ep = " DATE(CONVERT_TZ(ep.last_updt_dt,$tzadd_add,'$new_timezone'))" ;
        $last_updtdate_mp = " DATE(CONVERT_TZ(mp.last_updt_dt,$tzadd_add,'$new_timezone'))" ;
        $last_updtdate_tp = " DATE(CONVERT_TZ(tp.last_updt_dt,$tzadd_add,'$new_timezone'))" ;
        $last_updtdate_rp = " DATE(CONVERT_TZ(rp.last_updt_dt,$tzadd_add,'$new_timezone'))" ;
        $last_updtdate_msp = " DATE(CONVERT_TZ(msp.last_updt_dt,$tzadd_add,'$new_timezone'))" ;
        $date_format = '%b %d, %Y';
        $search_value=$search['value'];
        if ($payment_type == 'A') {
        if(!empty($search_value)){
           $s_text=" where (buyer_name like '%$search_value%' or buyer_email like '%$search_value%' or event_title like '%$search_value%' or `buyer_phone` like  '%$search_value%' or 
                     Participant like  '%$search_value%' or DATE_FORMAT((schedule_date), '%b %d, %Y')  like  '%$search_value%'or  payment_amount  like '%$search_value' or view_status like '%$search_value%'
                     or  net_amount like  '%$search_value%'  or Category like  '%$search_value%' or  DATE_FORMAT(last_updt_dt, '%b %d, %Y')  like  '%$search_value%' or card_credit_string like  '%$search_value%'
                     or  credit_card_name  like  '%$search_value%' )";
        }else{
             $s_text="";
        }
        } else {
            if (!empty($search_value)) {
                $s_text = " where (buyer_name like '%$search_value%' or buyer_email like '%$search_value%' or event_title like '%$search_value%' or `buyer_phone` like  '%$search_value%' or 
                     Participant like  '%$search_value%' or DATE_FORMAT((schedule_date), '%b %d, %Y')  like  '%$search_value%'or  payment_amount  like '%$search_value' or view_status like '%$search_value%'
                       or Category like  '%$search_value%' or  DATE_FORMAT(last_updt_dt, '%b %d, %Y')  like  '%$search_value%' 
                     or  credit_card_name  like  '%$search_value%' )";
            } else {
                $s_text = "";
            }
        }
          $sort = $sorting[0]['column'];
        $sort_type=$sorting[0]['dir'];
//        
        
        $day_text = $type_text = $mday_text = $mtype_text = '';
        $tday_text = $ttype_text = $rday_text = $rtype_text = $misday_text = $mistype_text = '';
        $approvedtotal = $pasttotal = $failedtotal = $upcomingtotal = $approved_manual_total = $cash_total = $check_total =0;;
        $payment_array =$out= [];
//        $payment_array['Approved']=$payment_array['Past']=$payment_array['Upcoming']=[];
        $membership=$membership2=$membership3 = $events =$event3=$events2= $trial=$trial2=$trial3=$retail=$retail2=$retail3='';
        if($payment_days_type == 1){
            $day_text = "and DATE($last_updtdate_ep) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
            $mday_text = "and DATE($last_updtdate_mp) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
            $tday_text = "and DATE($last_updtdate_tp) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
            $rday_text = "and DATE($last_updtdate_rp) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
            $misday_text = "and DATE($last_updtdate_msp) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
        }elseif($payment_days_type == 2){
            $day_text = "and DATE($last_updtdate_ep) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
            $mday_text = "and DATE($last_updtdate_mp) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
            $tday_text = "and DATE($last_updtdate_tp) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
            $rday_text = "and DATE($last_updtdate_rp) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
            $misday_text = "and DATE($last_updtdate_msp) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
        }elseif($payment_days_type == 3){
            $day_text = "and DATE($last_updtdate_ep) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
            $mday_text = "and DATE($last_updtdate_mp) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
            $tday_text = "and DATE($last_updtdate_tp) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
            $rday_text = "and DATE($last_updtdate_rp) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
            $misday_text = "and DATE($last_updtdate_msp) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
        }elseif($payment_days_type == 4){
            $day_text = "and DATE($last_updtdate_ep) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
            $mday_text = "and DATE($last_updtdate_mp) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
            $tday_text = "and DATE($last_updtdate_tp) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
            $rday_text = "and DATE($last_updtdate_rp) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
            $misday_text = "and DATE($last_updtdate_msp) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
        }elseif($payment_days_type == 5){
            $day_text = "";
            $mday_text = "";
            $tday_text = "";
            $rday_text = "";
            $misday_text = "";
        }

        /*
         * S - successfully completed via payment method - credit card 
         * R - success refunded payment via payment method - credit card 
         * M - successfully manual payment via payment method - credit or cash or check
         * MF - successfully manual full refund - cash or check
         * MP - partial manual  payment method - cash or check
         * PR - partial refund completed via payment method - credit card 
         * CR or FR - complete refund via payment method - credit card
         */
        if($payment_type == 'A'){
            $type_text ="and (ep.schedule_status ='S' || ep.schedule_status ='R' || ep.schedule_status ='M' || ep.schedule_status ='MF' || ep.schedule_status ='PR' || ep.schedule_status ='MP' || ep.schedule_status ='MR' || ep.schedule_status ='FR' || ep.schedule_status ='CR')"; 
            $mtype_text ="and (mp.payment_status ='S' || mp.payment_status = 'R' || mp.payment_status ='M' || mp.payment_status ='MF' || mp.payment_status ='FR' || mp.payment_status ='MR')";
            $ttype_text = "and (tp.payment_status ='S' || tp.payment_status = 'R' || tp.payment_status ='FR' || tp.payment_status ='M' || tp.payment_status ='MF' || tp.payment_status ='MR')";
            $rtype_text = "and (rp.payment_status ='S' || rp.payment_status = 'R' || rp.payment_status ='FR' || rp.payment_status ='PR' || rp.payment_status ='M' || rp.payment_status ='MF' || rp.payment_status ='MR' || rp.payment_status ='MP')";
            $mistype_text = "and (msp.payment_status ='S' || msp.payment_status = 'R' || msp.payment_status ='FR' || msp.payment_status ='M' || msp.payment_status ='MR' || msp.payment_status ='MF')";
        }elseif($payment_type == 'P'){
            $type_text =" and (ep.schedule_status ='F' or (DATE(ep.schedule_date) < ($currdate_add) and ep.schedule_status='N'))"; 
            $mtype_text =" and mr.membership_status IN ('R', 'P', 'S','C') and (mp.payment_status ='F' || (mp.payment_status = 'N' and DATE(mp.payment_date) < ($currdate_add)))";
            $ttype_text = " and (tp.payment_status ='F' || (tp.payment_status = 'N' and DATE(tp.payment_date) < ($currdate_add)))";
            $rtype_text = " and (rp.payment_status ='F' || (rp.payment_status = 'N' and DATE(rp.payment_date) < ($currdate_add)))";
            $mistype_text = " and (msp.payment_status ='F' || (msp.payment_status = 'N' and DATE(msp.payment_date) < ($currdate_add)))";
        }elseif($payment_type == 'U'){
            if($payment_days_type == 1){
                $day_text = "and  DATE(ep.schedule_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 30 DAY";
                $mday_text = "and  DATE(mp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 30 DAY";
                $tday_text = "and  DATE(tp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 30 DAY";
                $rday_text = "and  DATE(rp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 30 DAY";
                $misday_text = "and  DATE(msp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 30 DAY";
            }elseif($payment_days_type == 2){
                $day_text = "and DATE(ep.schedule_date) BETWEEN ($currdate_add)  AND ($currdate_add)+ INTERVAL 60 DAY ";
                $mday_text = "and DATE(mp.payment_date) BETWEEN ($currdate_add)  AND ($currdate_add)+ INTERVAL 60 DAY ";
                $tday_text = "and  DATE(tp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 60 DAY";
                $rday_text = "and  DATE(rp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 60 DAY";
                $misday_text = "and  DATE(msp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 60 DAY";
            }elseif($payment_days_type == 3){
                $day_text = "and DATE(ep.schedule_date) BETWEEN ($currdate_add)  AND ($currdate_add)+ INTERVAL 90 DAY ";
                $mday_text = "and DATE(mp.payment_date) BETWEEN ($currdate_add)  AND ($currdate_add)+ INTERVAL 90 DAY ";
                $tday_text = "and  DATE(tp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 90 DAY";
                $rday_text = "and  DATE(rp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 90 DAY";
                $misday_text = "and  DATE(msp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 90 DAY";
            }elseif($payment_days_type == 4){
                $day_text = "and DATE(ep.schedule_date) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
                $mday_text = "and DATE(mp.payment_date) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
                $tday_text = "and DATE(tp.payment_date) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
                $rday_text = "and DATE(rp.payment_date) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
                $misday_text = "and DATE(msp.payment_date) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
            }elseif($payment_days_type == 5){
                $day_text = "";
                $mday_text = "";
                $tday_text = "";
                $rday_text = "";
                $misday_text = "";
            }
            $type_text =" and (ep.schedule_status ='N' and (ep.schedule_date >= ($currdate_add))) "; 
            $mtype_text =" and (mr.membership_status='R' || (mr.membership_status='P' AND mr.`resume_date`>=($currdate_add) AND mr.`resume_date` NOT IN (NULL, '0000-00-00'))) and (mp.payment_status ='N' and (mp.payment_date >= ($currdate_add)))";
            $ttype_text =" and (tp.payment_status ='N' and (tp.payment_date >= ($currdate_add))) ";
            $rtype_text =" and (rp.payment_status ='N' and (rp.payment_date >= ($currdate_add))) "; 
            $mistype_text =" and (msp.payment_status ='N' and (msp.payment_date >= ($currdate_add))) ";
        }
        
        if($payment_type=='A'){
           $column=array(1=>"last_updt_dt",2=>"schedule_date",3=>"buyer_name",4=>"Participant",5=>"buyer_phone",6=>"buyer_email",7=>"Category",8=>"event_title",9=>"cast(payment_amount AS unsigned)",10=>"cast(net_amount AS unsigned)",11=>"cast(tax_amount AS unsigned)",12=>"view_status",13=>"card_credit_string");
        }else{
            $column=array(1=>"schedule_date",2=>"buyer_name",3=>"Participant",4=>"buyer_phone",5=>"buyer_email",6=>"Category",7=>"event_title",8=>"cast(payment_amount AS unsigned)",9=>"view_status",10=>"credit_card_name");
        }
        
        if($payment_type == 'A'){
            
//            $events = "SELECT er.event_reg_id,e.event_id, er.student_id, ep.cc_name credit_card_name, e.event_title, er.buyer_name, er.`buyer_email`, er.`buyer_phone`,
//                    IF((SELECT count(*) FROM `student` WHERE `push_device_id` !='' AND `push_device_id` IS NOT NULL AND (`student_id` IN (SELECT `student_id` FROM `event_registration` WHERE `student_id`>0 AND `event_reg_id`=er.`event_reg_id` ) 
//                    OR `student_email` IN (SELECT `buyer_email` FROM `event_registration` WHERE `event_reg_id`=er.`event_reg_id`)))>0,'Y','N') show_icon,
//                    er.payment_type, CONCAT(er.event_registration_column_1,' ',er.event_registration_column_2) as Participant ,
//                    IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-',if(er.processing_fee_type=2,(ep.payment_amount+ep.processing_fee),ep.payment_amount)), 
//                    if(er.processing_fee_type=2 ,IF(ep.schedule_status='CR'||ep.schedule_status='FR',ep.payment_amount+ep.processing_fee,ep.payment_amount-ep.refunded_amount+ep.processing_fee),IF(ep.schedule_status='CR'||ep.schedule_status='FR', ep.payment_amount, ep.payment_amount-ep.refunded_amount))) payment_amount,                
//                    IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-',if(er.processing_fee_type=1,(ep.payment_amount-ep.processing_fee),ep.payment_amount)),
//                    if(er.processing_fee_type=1 ,IF(ep.schedule_status='CR'||ep.schedule_status='FR', ep.payment_amount-ep.processing_fee,ep.payment_amount-(ep.refunded_amount+ep.processing_fee)), IF(ep.schedule_status='CR'||ep.schedule_status='FR', ep.payment_amount, ep.payment_amount-ep.refunded_amount))) net_amount,                
//                    ep.schedule_status,ep.processing_fee as processing_fees ,  checkout_status,
//                    IF(ep.schedule_status = 'FR' || ep.schedule_status = 'CR', 'Completed(refunded)', IF(ep.schedule_status = 'R','Refunded',IF(ep.schedule_status = 'F','Failed',IF(ep.schedule_status = 'MR','Manually Refunded',IF(checkout_status='released','Completed(released)',IF(ep.schedule_status = 'M','Completed','Completed(pending)')))))) as view_status,
//                    ep.schedule_date, IF(ep.schedule_status='FR'||ep.schedule_status='CR', IFNULL(ep.`schedule_date`, DATE(ep.created_dt)), ($last_updtdate_ep)) last_updt_dt,ep.event_payment_id, 'Events' As Category from event_registration er 
//                    LEFT JOIN event e ON er.event_id = e.event_id 
//                    LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
//                    where ep.payment_amount>'0' && er.company_id = '$company_id' $day_text $type_text";            
//            
//            $membership = " SELECT mr.membership_registration_id as event_reg_id ,m.membership_id as event_id, mr.student_id, mp.cc_name credit_card_name, m.category_title as event_title, mr.buyer_name, mr.`buyer_email`, mr.`buyer_phone`,
//                    IF((SELECT count(*) FROM `student` WHERE `push_device_id` !='' AND `push_device_id` IS NOT NULL AND (`student_id` IN (SELECT `student_id` FROM `membership_registration` WHERE `student_id`>0 AND `membership_registration_id`=mr.`membership_registration_id` ) 
//                    OR `student_email` IN (SELECT `buyer_email` FROM `membership_registration` WHERE `membership_registration_id`=mr.`membership_registration_id`)))>0,'Y','N') show_icon, 
//                    mr.payment_type, CONCAT(mr.membership_registration_column_1,' ',mr.membership_registration_column_2) as Participant ,
//                    IF(mp.payment_status = 'R' , concat('-',if(mr.processing_fee_type=2,(mp.payment_amount+mp.processing_fee),mp.payment_amount)), 
//                    if(mr.processing_fee_type=2 ,mp.payment_amount+mp.processing_fee,mp.payment_amount-mp.refunded_amount)) payment_amount,
//                    IF(mp.payment_status = 'R' , concat('-',if(mr.processing_fee_type=1,(mp.payment_amount-mp.processing_fee),mp.payment_amount)),
//                    if(mr.processing_fee_type=1 ,mp.payment_amount-mp.processing_fee,mp.payment_amount)) net_amount,
//                    mp.payment_status as schedule_status ,mp.processing_fee as processing_fees ,  checkout_status,
//                    IF(mp.payment_status = 'FR', 'Completed(refunded)', IF(mp.payment_status = 'R','Refunded',IF(mp.payment_status = 'F','Failed',IF(mp.payment_status = 'MR','Manually Refunded',IF(checkout_status='released','Completed(released)',IF(mp.payment_status = 'M','Completed','Completed(pending)')))))) as view_status,
//                    mp.payment_date as schedule_date, IF(mp.payment_date='FR', IFNULL(mp.payment_date, DATE(mp.created_dt)), ($last_updtdate_mp)) last_updt_dt,mp.membership_payment_id as event_payment_id, 'Membership' As Category from membership_registration mr 
//                    LEFT JOIN membership m ON mr.membership_id = m.membership_id 
//                    LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id
//                    where mp.payment_amount>'0' && mr.company_id = '$company_id' $mday_text $mtype_text order by last_updt_dt desc LIMIT $data_limit, $end_limit";
            $processing_fee_temp="if(ep.`schedule_status`='PR'||ep.`schedule_status`='CR',(SELECT SUM(`processing_fee`) FROM `event_payment` WHERE `event_reg_id`=ep.`event_reg_id` AND `checkout_id`=ep.`checkout_id` AND `schedule_status` IN ('R','PR')),ep.`processing_fee`)";
            $events = "SELECT er.event_reg_id,e.event_id, er.student_id,ep.`payment_from`,if(ep.`payment_from`='S',ep.stripe_card_name,ep.cc_name) as credit_card_name, e.event_title , er.buyer_name , er.`buyer_email`, er.`buyer_phone`,
                    if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon,
                    if(er.`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                    er.payment_type, CONCAT(er.event_registration_column_2,', ',er.event_registration_column_1) as Participant ,
                    IF(ep.`credit_method`='CC',IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-',if(er.processing_fee_type=2,(ep.payment_amount+$processing_fee_temp),ep.payment_amount)), 
                    if(er.processing_fee_type=2 ,IF(ep.schedule_status='CR'||ep.schedule_status='FR',ep.payment_amount+$processing_fee_temp,ep.payment_amount+$processing_fee_temp),IF(ep.schedule_status='CR'||ep.schedule_status='FR' , ep.payment_amount, ep.payment_amount))),'') payment_amount,                
                    IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-',if(er.processing_fee_type=1,(ep.payment_amount-$processing_fee_temp),ep.payment_amount)),
                    if(er.processing_fee_type=1 ,IF(ep.schedule_status='CR'||ep.schedule_status='FR', ep.payment_amount-$processing_fee_temp,ep.payment_amount-($processing_fee_temp)), IF(ep.schedule_status='CR'||ep.schedule_status='FR', ep.payment_amount, ep.payment_amount))) net_amount,'' as tax_value, 
                    ep.schedule_status,$processing_fee_temp as processing_fees ,  checkout_status,
                    IF(ep.schedule_status = 'FR' || ep.schedule_status = 'CR', 'Completed(refunded)', IF(ep.schedule_status = 'R','Refunded',IF(ep.schedule_status = 'F','Failed',IF(ep.schedule_status = 'MR','Refunded',IF(ep.schedule_status = 'MF','Completed',IF(checkout_status='released','Completed(released)',IF(ep.schedule_status = 'M'||ep.schedule_status = 'MP','Completed','Completed(pending)'))))))) as view_status,
                    ep.schedule_date , IF(ep.schedule_status='FR'||ep.schedule_status='CR', IFNULL(ep.`schedule_date`, DATE(ep.created_dt)), ($last_updtdate_ep)) last_updt_dt,ep.event_payment_id, IF(ep.credit_method ='MC','Manual Credit', IF(ep.credit_method ='CA','Cash Payment',IF(ep.credit_method ='CH','Check Payment',if(ep.`payment_from`='S',ep.stripe_card_name,ep.cc_name)))) card_credit_string, 'Events' As Category from event_registration er 
                    LEFT JOIN event e ON er.event_id = e.event_id 
                    LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
                    LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`= er.`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=er.`company_id`)
                    LEFT JOIN student s1 ON er.`student_id` = s1.student_id and er.company_id = s1.company_id
                    LEFT JOIN student s2 ON er.`buyer_email` = s2.student_email and er.company_id = s2.company_id
                    where ep.payment_amount>'0' and er.company_id = '$company_id' $day_text $type_text";  
            $events2 = "SELECT 'Events' Category, er.event_reg_id, event_payment_id,IF(ep.credit_method ='CA','CA',IF(ep.credit_method ='CH','CH',IF(ep.credit_method='MC','M','W')))flag,
                    IF(ep.schedule_status='M',ep.payment_amount,IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-',if(er.processing_fee_type=1,(ep.payment_amount-$processing_fee_temp),ep.payment_amount)),
                    if(er.processing_fee_type=1 ,IF(ep.schedule_status='CR'||ep.schedule_status='FR', ep.payment_amount-$processing_fee_temp,ep.payment_amount-($processing_fee_temp)), IF(ep.schedule_status='CR'||ep.schedule_status='FR', ep.payment_amount, ep.payment_amount)))) net_amount 
                    FROM event_registration er 
                    LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
                    WHERE ep.payment_amount>'0' and er.company_id = '$company_id' AND ep.schedule_status!='C' $day_text $type_text";
            
            $membership = " SELECT mr.membership_registration_id as event_reg_id ,m.membership_id as event_id, mr.student_id,mp.`payment_from`,if(mp.`payment_from`='S',mp.stripe_card_name,mp.cc_name) as credit_card_name, m.category_title as event_title, mr.buyer_name, mr.`buyer_email`, mr.`buyer_phone`,
                    if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, 
                    if(mr.`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                    mr.payment_type, CONCAT(mr.membership_registration_column_2,', ',mr.membership_registration_column_1) as Participant ,
                    IF(mp.`credit_method`='CC',IF(mp.payment_status = 'R' || mp.payment_status = 'MR', concat('-',if(mr.processing_fee_type=2,(mp.payment_amount+mp.processing_fee),mp.payment_amount)), 
                    if(mr.processing_fee_type=2 ,mp.payment_amount+mp.processing_fee,mp.payment_amount-mp.refunded_amount)),'') payment_amount,
                    IF(mp.payment_status = 'R' || mp.payment_status = 'MR', concat('-',if(mr.processing_fee_type=1,(mp.payment_amount-mp.processing_fee),mp.payment_amount)),
                    if(mr.processing_fee_type=1 ,mp.payment_amount-mp.processing_fee,mp.payment_amount)) net_amount, '' as tax_value,
                    mp.payment_status as schedule_status ,mp.processing_fee as processing_fees ,  checkout_status,
                    IF(mp.payment_status = 'FR', 'Completed(refunded)', IF(mp.payment_status = 'R','Refunded',IF(mp.payment_status = 'F','Failed',IF(mp.payment_status = 'MR','Refunded',IF(mp.payment_status = 'MF','Completed',IF(checkout_status='released','Completed(released)',IF(mp.payment_status = 'M' || mp.payment_status = 'MP','Completed','Completed(pending)'))))))) as view_status,
                    mp.payment_date as schedule_date, IF(mp.payment_status='FR', IFNULL(mp.payment_date, DATE(mp.created_dt)), ($last_updtdate_mp)) last_updt_dt,mp.membership_payment_id as event_payment_id, IF(mp.credit_method ='MC','Manual Credit', IF(mp.credit_method ='CA','Cash Payment',IF(mp.credit_method ='CH','Check Payment',if(mp.`payment_from`='S',mp.stripe_card_name,mp.cc_name)))) card_credit_string, 'Membership' As Category from membership_registration mr 
                    LEFT JOIN membership m ON mr.membership_id = m.membership_id 
                    LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id
                    LEFT JOIN student s1 ON mr.`student_id` = s1.student_id and mr.company_id = s1.company_id
                     LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=mr.`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                    LEFT JOIN student s2 ON mr.`buyer_email` = s2.student_email and mr.company_id = s2.company_id
                    where mp.payment_amount>'0' and mr.company_id = '$company_id' $mday_text $mtype_text";
               $membership2 = "SELECT 'Membership' Category, mr.membership_registration_id, membership_payment_id,IF(mp.credit_method ='CA','CA',IF(mp.credit_method ='CH','CH',IF(mp.credit_method='MC','M','W')))flag,
                    IF(mp.payment_status='M',mp.payment_amount,IF(mp.payment_status = 'R' || mp.payment_status = 'MR' , concat('-',if(mr.processing_fee_type=1,(mp.payment_amount-mp.processing_fee),mp.payment_amount)),
                    if(mr.processing_fee_type=1 ,mp.payment_amount-mp.processing_fee,mp.payment_amount))) net_amount
                    FROM membership_registration mr 
                    LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id
                    where mp.payment_amount>'0' and mr.company_id = '$company_id' AND mp.payment_status!='C' $mday_text $mtype_text";

//            jeeva----sep20--2018 ----starting 
            $membership3="SELECT  count( * ) total_record ,'membership' as category from membership_registration mr 
                         LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id
                         where mp.payment_amount>'0' and mr.company_id = '$company_id' $mday_text $mtype_text ";
            
            $event3="SELECT  count( * ) total_record ,'event' as category from event_registration er 
                    LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
                    where ep.payment_amount>'0' and er.company_id = '$company_id' $day_text $type_text";
            
              $trial = " SELECT tr.trial_reg_id as event_reg_id ,t.trial_id as event_id, tr.student_id,tp.`payment_from`,if(tp.`payment_from`='S',tp.`stripe_card_name`,tp.`cc_name` ) as credit_card_name, t.trial_title as event_title, tr.buyer_name, tr.`buyer_email`, tr.`buyer_phone`,
                    if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, 
                    if(tr.`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                    tr.payment_type, CONCAT(tr.trial_registration_column_2,', ',tr.trial_registration_column_1) as Participant ,
                    IF(tp.`credit_method`='CC',IF(tp.payment_status = 'R' || tp.payment_status = 'MR' , concat('-',if(tr.processing_fee_type=2,(tp.payment_amount+tp.processing_fee),tp.payment_amount)), 
                    if(tr.processing_fee_type=2 ,tp.payment_amount+tp.processing_fee,tp.payment_amount-tp.refunded_amount)),'') payment_amount,
                    IF(tp.payment_status = 'R' || tp.payment_status = 'MR', concat('-',if(tr.processing_fee_type=1,(tp.payment_amount-tp.processing_fee),tp.payment_amount)),
                    if(tr.processing_fee_type=1 ,tp.payment_amount-tp.processing_fee,tp.payment_amount)) net_amount,'' as tax_value,
                    tp.payment_status as schedule_status ,tp.processing_fee as processing_fees ,  checkout_status,
                    IF(tp.payment_status = 'FR', 'Completed(refunded)', IF(tp.payment_status = 'R','Refunded',IF(tp.payment_status = 'F','Failed',IF(tp.payment_status = 'MR','Refunded',IF(tp.payment_status = 'MF','Completed',IF(checkout_status='released','Completed(released)',IF(tp.payment_status = 'M' || tp.payment_status = 'MP','Completed','Completed(pending)'))))))) as view_status,
                    tp.payment_date as schedule_date, IF(tp.payment_status='FR', IFNULL(tp.payment_date, DATE(tp.created_dt)), ($last_updtdate_tp)) last_updt_dt,tp.trial_payment_id as event_payment_id, IF(tp.credit_method ='MC','Manual Credit', IF(tp.credit_method ='CA','Cash Payment',IF(tp.credit_method ='CH','Check Payment',if(tp.`payment_from`='S',tp.`stripe_card_name`,tp.`cc_name` )))) card_credit_string, 'Trial' As Category from trial_registration tr 
                    LEFT JOIN trial t ON tr.trial_id = t.trial_id 
                    LEFT JOIN trial_payment tp on tp.trial_reg_id = tr.trial_reg_id
                    LEFT JOIN student s1 ON tr.`student_id` = s1.student_id and tr.company_id = s1.company_id
                    LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=tr.`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                    LEFT JOIN student s2 ON tr.`buyer_email` = s2.student_email and tr.company_id = s2.company_id
                    where tp.payment_amount>'0' and tr.company_id = '$company_id' $tday_text $ttype_text";
         $trial2 = "SELECT 'Trial' Category, tr.trial_reg_id, trial_payment_id,IF(tp.credit_method ='CA','CA',IF(tp.credit_method ='CH','CH',IF(tp.credit_method='MC','M','W')))flag,
                    IF(tp.payment_status='M',tp.payment_amount,IF(tp.payment_status = 'R' || tp.payment_status = 'MR' , concat('-',if(tr.processing_fee_type=1,(tp.payment_amount-tp.processing_fee),tp.payment_amount)),
                    if(tr.processing_fee_type=1 ,tp.payment_amount-tp.processing_fee,tp.payment_amount))) net_amount
                    FROM trial_registration tr 
                    LEFT JOIN trial_payment tp on tp.trial_reg_id = tr.trial_reg_id
                    where tp.payment_amount>'0' and tr.company_id = '$company_id' AND tp.payment_status!='C' $tday_text $ttype_text";
         $trial3 = "SELECT  count( * ) total_record ,'trial' as category from trial_registration tr 
                     LEFT JOIN trial_payment tp on tp.trial_reg_id = tr.trial_reg_id
                      where tp.payment_amount>'0' and tr.company_id = '$company_id' $tday_text $ttype_text ";
            //***PAYMENT RECORDS FOR RETAIL***
         $processing_fee_temp_retail="if(rp.`payment_status`='PR'||rp.`payment_status`='FR',(SELECT SUM(`processing_fee`) FROM `retail_payment` WHERE `retail_order_id`=rp.`retail_order_id` AND `checkout_id`=rp.`checkout_id` AND `payment_status` IN ('R','PR')),rp.`processing_fee`)";
         $retail="SELECT rto.`retail_order_id`as event_reg_id,'' as event_id,rto.`student_id`,rp.`payment_from`,if(rp.`payment_from`='S',rp.`stripe_card_name`,rp.cc_name) as credit_card_name,GROUP_CONCAT(rod.retail_product_name) as event_title,rto.`buyer_name`,rto.`buyer_email`,rto.`buyer_phone`,
                 if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon,
                 if(rto.`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                 rto.payment_type,'' as Participant, IF(rp.`credit_method`='CC',IF(rp.payment_status = 'R' || rp.payment_status = 'MR' , concat('-',rp.payment_amount), rp.payment_amount),'') payment_amount,
                 IF(rp.payment_status = 'R' || rp.payment_status = 'MR', concat('-',rp.payment_amount-rp.processing_fee), IF(rp.payment_status = 'PR' || rp.payment_status = 'FR', rp.payment_amount-$processing_fee_temp_retail, rp.payment_amount-rp.processing_fee)) net_amount,
                 concat(c.`wp_currency_symbol`,IF(rp.`retail_tax_payment`=0, '', IF(rp.payment_status = 'R', '-', '')),rp.`retail_tax_payment`) as tax_value, $processing_fee_temp_retail as processing_fees , 
                 rp.payment_status as schedule_status , checkout_status,
                 IF(rp.payment_status = 'FR', 'Completed(refunded)', IF(rp.payment_status = 'R','Refunded',IF(rp.payment_status = 'F','Failed',IF(rp.payment_status = 'MR','Refunded',IF(rp.payment_status = 'MF','Completed',IF(checkout_status='released','Completed(released)',IF(rp.payment_status = 'M' || rp.payment_status = 'MP','Completed','Completed(pending)'))))))) as view_status,
                 rp.payment_date as schedule_date, IF(rp.payment_status='FR', IFNULL(rp.payment_date, DATE(rp.created_dt)), ($last_updtdate_rp)) last_updt_dt,rp.retail_payment_id as event_payment_id, IF(rp.credit_method ='MC','Manual Credit', IF(rp.credit_method ='CA','Cash Payment',IF(rp.credit_method ='CH','Check Payment',if(rp.`payment_from`='S',rp.`stripe_card_name`,rp.cc_name)))) card_credit_string, 'Retail' As Category FROM retail_orders rto
                 LEFT JOIN retail_order_details rod ON rto.retail_order_id = rod.retail_order_id
                 LEFT JOIN retail_payment rp on rp.retail_order_id = rto.retail_order_id
                 LEFT JOIN student s1 ON rto.`student_id` = s1.student_id and rto.company_id = s1.company_id 
                 LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=rto.`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                 LEFT JOIN `company` c on c.`company_id`=rto.`company_id`
                 LEFT JOIN student s2 ON rto.`buyer_email` = s2.student_email and rto.company_id = s2.company_id 
                 WHERE rp.payment_amount>'0' AND rto.company_id = '$company_id' $rday_text $rtype_text group by event_reg_id, retail_payment_id";
         $retail2="SELECT 'Retail' Category, rto.retail_order_id, retail_payment_id,IF(rp.credit_method ='CA','CA',IF(rp.credit_method ='CH','CH',IF(rp.credit_method='MC','M','W')))flag,
                    IF(rp.payment_status='M',rp.payment_amount,IF(rp.payment_status = 'R' || rp.payment_status = 'MR' , concat('-',rp.payment_amount-rp.processing_fee),
                    rp.payment_amount-rp.processing_fee)) net_amount
                    FROM retail_orders rto 
                    LEFT JOIN retail_payment rp on rp.retail_order_id = rto.retail_order_id
                    where rp.payment_amount>'0' && rto.company_id = '$company_id' AND rp.payment_status!='C' $rday_text $rtype_text";
         $retail3="SELECT  count( * ) total_record ,'Retail' as category from retail_orders rto 
                    LEFT JOIN retail_payment rp on rp.retail_order_id = rto.retail_order_id
                    WHERE rp.payment_amount>'0' && rto.company_id = '$company_id' $rday_text $rtype_text";
         
    // Miscellaneous queries
         
            $misc = "SELECT ms.misc_order_id as event_reg_id ,'' as event_id, ms.student_id,msp.`payment_from`,if(msp.`payment_from`='S',msp.`stripe_card_name`, msp.cc_name) as credit_card_name, '' as event_title, ms.buyer_name, ms.`buyer_email`, ms.`buyer_phone`,
                    if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, 
                    if (ms.`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                    '' payment_type, '' as Participant , IF(msp.`credit_method`='CC',IF(msp.payment_status = 'R' || msp.payment_status = 'MR' , concat('-',if(ms.processing_fee_type=2,(msp.payment_amount+msp.processing_fee),msp.payment_amount)), 
                    if( ms.processing_fee_type=2 ,msp.payment_amount+msp.processing_fee,msp.payment_amount-msp.refunded_amount)),'') payment_amount,
                    IF(msp.payment_status = 'R' || msp.payment_status = 'MR' , concat('-',if(ms.processing_fee_type=1,(msp.payment_amount-msp.processing_fee),msp.payment_amount)),
                    if(ms.processing_fee_type=1 ,msp.payment_amount-msp.processing_fee,msp.payment_amount)) net_amount,'' as tax_value,
                    msp.payment_status as schedule_status ,msp.processing_fee as processing_fees ,  checkout_status,
                    IF(msp.payment_status = 'FR', 'Completed(refunded)', IF(msp.payment_status = 'R','Refunded',IF(msp.payment_status = 'F','Failed',IF(msp.payment_status = 'MR','Refunded',IF(msp.payment_status = 'MF','Completed',IF(checkout_status='released','Completed(released)',IF(msp.payment_status = 'M','Completed','Completed(pending)'))))))) as view_status,
                    msp.payment_date as schedule_date, IF(msp.payment_status='FR', IFNULL(msp.payment_date, DATE(msp.created_dt)), ($last_updtdate_msp)) last_updt_dt,msp.misc_payment_id as event_payment_id, IF(msp.credit_method ='MC','Manual Credit', IF(msp.credit_method ='CA','Cash Payment',IF(msp.credit_method ='CH','Check Payment',if(msp.`payment_from`='S',msp.`stripe_card_name`, msp.cc_name)))) card_credit_string, 'Miscellaneous' As Category from misc_order ms 
                    LEFT JOIN misc_payment msp on msp.misc_order_id = ms.misc_order_id
                    LEFT JOIN student s1 ON ms.`student_id` = s1.student_id and ms.company_id = s1.company_id
                    LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=ms.`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                    LEFT JOIN student s2 ON ms.`buyer_email` = s2.student_email and ms.company_id = s2.company_id
                    where msp.payment_amount>'0' and ms.company_id = '$company_id' $misday_text $mistype_text  order by $column[$sort] $sort_type ";
         
            $misc2 = "SELECT 'Miscellaneous' Category, ms.misc_order_id, misc_payment_id,IF(msp.credit_method ='CA','CA',IF(msp.credit_method ='CH','CH',IF(msp.credit_method='MC','M','W')))flag,
                    IF(msp.payment_status='M',msp.payment_amount,IF(msp.payment_status = 'R' || msp.payment_status = 'MR' , concat('-',if( ms.processing_fee_type=1,(msp.payment_amount-msp.processing_fee),msp.payment_amount)),
                    if (ms.processing_fee_type=1 ,msp.payment_amount-msp.processing_fee,msp.payment_amount))) net_amount
                    FROM misc_order ms 
                    LEFT JOIN misc_payment msp on msp.misc_order_id = ms.misc_order_id
                    where msp.payment_amount>'0' && ms.company_id = '$company_id' AND msp.payment_status!='C' $misday_text $mistype_text";
         
           $misc3 = "SELECT  count( * ) total_record ,'Miscellaneous' as category from misc_order ms 
                     LEFT JOIN misc_payment msp on msp.misc_order_id = ms.misc_order_id
                      where msp.payment_amount>'0' && ms.company_id = '$company_id' $misday_text $mistype_text ";
         
            if($category=='E'){
                $sql=sprintf("select * from ($events order by $column[$sort] $sort_type )te %s LIMIT $start, $length_table", $s_text);
                $sql_count=sprintf("$event3");
                $sql2=sprintf("SELECT SUM(net_amount) total_amount,flag FROM ($events2)t1 GROUP BY flag");
                $sql_sea_count=sprintf("select * from ($events)te %s", $s_text);
            }elseif ($category=='M') {
                $sql=sprintf("select * from ($membership order by $column[$sort] $sort_type ) te %s LIMIT $start, $length_table" ,$s_text);
                $sql_count=sprintf("$membership3");
                $sql2=sprintf("SELECT SUM(net_amount) total_amount,flag FROM ($membership2)t1 GROUP BY flag");
                $sql_sea_count=sprintf("select * from ($membership ) te %s" ,$s_text);
            }elseif ($category=='T'){
                $sql=sprintf("select * from ($trial order by $column[$sort] $sort_type ) te %s LIMIT $start, $length_table" ,$s_text);
                $sql_count=sprintf("$trial3");
                $sql2=sprintf("SELECT SUM(net_amount) total_amount,flag FROM ($trial2)t1 GROUP BY flag");
                $sql_sea_count=sprintf("select * from ($trial) te %s" ,$s_text);
            }elseif ($category=='R'){
                $sql=sprintf("select * from ($retail order by $column[$sort] $sort_type)te %s LIMIT $start, $length_table", $s_text);
                $sql_count=sprintf("$retail3");
                $sql2=sprintf("SELECT SUM(net_amount) total_amount,flag FROM ($retail2)t1 GROUP BY flag");
                $sql_sea_count=sprintf("select * from ($retail)te %s", $s_text);
            }elseif ($category=='C'){
                $sql=sprintf("select * from ($misc)te %s LIMIT $start, $length_table", $s_text);
                $sql_count=sprintf("$misc3");
                $sql2=sprintf("SELECT SUM(net_amount) total_amount,flag FROM ($misc2)t1 GROUP BY flag");
                $sql_sea_count=sprintf("select * from ($misc)te %s", $s_text);
            }elseif ($category=='A') {
                $sql=sprintf("select * from ($events UNION $membership UNION $trial UNION $retail UNION $misc) te %s LIMIT $start, $length_table", $s_text);
                $sql_count=sprintf("$event3 UNION $membership3 UNION $trial3 UNION $retail3 UNION $misc3");
                $sql2=sprintf("SELECT SUM(net_amount) total_amount,flag FROM ($events2 UNION $membership2 UNION $trial2 UNION $retail2 UNION $misc2)t1 GROUP BY flag");
                $sql_sea_count=sprintf("select * from ($events UNION $membership UNION $trial UNION $retail UNION $misc) te %s", $s_text);
            }                       

            
            $result = mysqli_query($this->db,  $sql);
            $result_count= mysqli_query($this->db,  $sql_count);
            $result_sea= mysqli_query($this->db,  $sql_sea_count);
            
        }else{
            if($payment_type == 'U'){
                $order_str = "order by $column[$sort] $sort_type";
            }else{
                $order_str = "order by $column[$sort] $sort_type";
            }
           
            $events = "SELECT er.event_reg_id,e.event_id, er.student_id,ep.`payment_from`,if(ep.`payment_from`='S',ep.stripe_card_name,ep.cc_name) as credit_card_name, e.event_title, er.buyer_name, er.`buyer_email`, er.`buyer_phone`,
                    if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, 
                    if(er.`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                    er.payment_type, CONCAT(er.event_registration_column_2,', ',er.event_registration_column_1) as Participant ,
                    IF(er.processing_fee_type = 2, (ep.payment_amount+ep.processing_fee), ep.payment_amount) payment_amount, ep.schedule_status,
                    IF(er.processing_fee_type = 2, ep.processing_fee, 0) processing_fees, '' as status, '' as paytime_type, checkout_status,
                    IF(ep.schedule_status = 'FR' || ep.schedule_status = 'CR', 'Completed(refunded)', IF(ep.schedule_status = 'R','Refunded',IF(ep.schedule_status = 'F','Failed',IF(ep.schedule_status = 'MR','Manually Refunded',IF(checkout_status='released','Completed(released)',IF(ep.schedule_status = 'M','Completed','Completed(pending)')))))) as view_status,
                    ep.schedule_date, ($last_updtdate_ep) last_updt_dt,ep.event_payment_id, 'Events' As Category from event_registration er 
                    LEFT JOIN event e ON er.event_id = e.event_id 
                    LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
                    LEFT JOIN student s1 ON er.`student_id` = s1.student_id and er.company_id = s1.company_id
                    LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`= er.`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                    LEFT JOIN student s2 ON er.`buyer_email` = s2.student_email and er.company_id = s2.company_id
                    where ep.payment_amount>'0' and er.company_id = '$company_id' $day_text $type_text";
            $events2 = "SELECT 'Events' Category, er.event_reg_id, event_payment_id,'W' flag, IF(er.processing_fee_type = 2, (ep.payment_amount+ep.processing_fee), ep.payment_amount) payment_amount
                    FROM event_registration er 
                    LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
                    where ep.payment_amount>'0' and er.company_id = '$company_id' $day_text $type_text";
           
            $membership = "SELECT mr.membership_registration_id as event_reg_id,m.membership_id as event_id, mr.student_id,mp.`payment_from`, if(mp.`payment_from`='S',mp.stripe_card_name,mp.cc_name) credit_card_name, m.category_title as event_title, mr.buyer_name, mr.`buyer_email`, mr.`buyer_phone`,
                    if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, 
                    if(mr.`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                    mr.payment_type, CONCAT(mr.membership_registration_column_2,', ',mr.membership_registration_column_1) as Participant ,
                    IF(mr.processing_fee_type = 2, (mp.processing_fee+mp.payment_amount), mp.payment_amount) payment_amount, mp.payment_status as schedule_status,IF(mr.processing_fee_type = 2 ,mp.processing_fee, 0) processing_fees, mr.membership_status as status, paytime_type as paytime_type, checkout_status,
                    IF(mp.payment_status = 'FR', 'Completed(refunded)', IF(mp.payment_status = 'R','Refunded',IF(mp.payment_status = 'F','Failed',IF(mp.payment_status = 'MR','Manually Refunded',IF(checkout_status='released','Completed(released)',IF(mp.payment_status = 'M','Completed','Completed(pending)')))))) as view_status,
                    mp.payment_date as schedule_date, ($last_updtdate_mp) last_updt_dt,mp.membership_payment_id as event_payment_id, 'Membership' As Category from membership_registration mr 
                    LEFT JOIN membership m ON mr.membership_id = m.membership_id 
                    LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id
                    LEFT JOIN student s1 ON mr.`student_id` = s1.student_id and mr.company_id = s1.company_id
                    LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`= mr.`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                    LEFT JOIN student s2 ON mr.`buyer_email` = s2.student_email and mr.company_id = s2.company_id
                    where mp.payment_amount>'0' and mr.company_id =  '$company_id' $mday_text $mtype_text";
            $membership2 = "SELECT 'Membership' Category, mr.membership_registration_id, membership_payment_id,'W' flag, IF(mr.processing_fee_type = 2, (mp.processing_fee+mp.payment_amount), mp.payment_amount) payment_amount
                    from membership_registration mr 
                    LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id
                    where mp.payment_amount>'0' and mr.company_id = '$company_id' $mday_text $mtype_text";
            
//               jeeva----sep20--2018 ----starting 
            
            $membership3="SELECT count(*) as total_record, 'membership' As category from membership_registration mr 
                    LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id
                    where mp.payment_amount>'0' and mr.company_id =  '$company_id' $mday_text $mtype_text";
            
            $event3="SELECT  count(*) as total_record ,'event' as category from event_registration er 
                    LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
                    where ep.payment_amount>'0' and er.company_id = '$company_id' $day_text $type_text";
            
             $trial = "SELECT tr.trial_reg_id as event_reg_id,t.trial_id as event_id, tr.student_id,tp.`payment_from`,if(tp.`payment_from`='S',tp.`stripe_card_name`,tp.`cc_name` ) as credit_card_name, t.trial_title as event_title, tr.buyer_name, tr.`buyer_email`, tr.`buyer_phone`,
                    if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, 
                     if(tr.`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                    tr.payment_type, CONCAT(tr.trial_registration_column_2,', ',tr.trial_registration_column_1) as Participant ,
                    IF(tr.processing_fee_type = 2, (tp.processing_fee+tp.payment_amount), tp.payment_amount) payment_amount, tp.payment_status as schedule_status,IF(tr.processing_fee_type = 2 ,tp.processing_fee, 0) processing_fees, tr.trial_status as status, 'I' as paytime_type, checkout_status,
                    IF(tp.payment_status = 'FR', 'Completed(refunded)', IF(tp.payment_status = 'R','Refunded',IF(tp.payment_status = 'F','Failed',IF(tp.payment_status = 'MR','Manually Refunded',IF(checkout_status='released','Completed(released)',IF(tp.payment_status = 'M','Completed','Completed(pending)')))))) as view_status,
                    tp.payment_date as schedule_date, ($last_updtdate_tp) last_updt_dt,tp.trial_payment_id as event_payment_id, 'Trial' As Category from trial_registration tr 
                    LEFT JOIN trial t ON tr.trial_id = t.trial_id 
                    LEFT JOIN trial_payment tp on tp.trial_reg_id = tr.trial_reg_id
                    LEFT JOIN student s1 ON tr.`student_id` = s1.student_id and tr.company_id = s1.company_id
                    LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`= tr.`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                    LEFT JOIN student s2 ON tr.`buyer_email` = s2.student_email and tr.company_id = s2.company_id
                    where tp.payment_amount>'0' and tr.company_id =  '$company_id' $tday_text $ttype_text";
            $trial2 = "SELECT 'Trial' Category, tr.trial_reg_id, trial_payment_id,'W' flag, IF(tr.processing_fee_type = 2, (tp.processing_fee+tp.payment_amount), tp.payment_amount) payment_amount
                    from trial_registration tr 
                    LEFT JOIN trial_payment tp on tp.trial_reg_id = tr.trial_reg_id
                    where tp.payment_amount>'0' && tr.company_id = '$company_id' $tday_text $ttype_text"; 
            $trial3 = "SELECT count(*) as total_record, 'Trial' As category from trial_registration tr 
                    LEFT JOIN trial_payment tp on tp.trial_reg_id = tr.trial_reg_id
                    where tp.payment_amount>'0' and tr.company_id =  '$company_id' $tday_text $ttype_text";
            $retail="SELECT rto.retail_order_id as event_reg_id,'' as event_id, rto.student_id,rp.`payment_from`,if(rp.`payment_from`='S',rp.`stripe_card_name`,rp.cc_name) credit_card_name, GROUP_CONCAT(rod.retail_product_name) as event_title, rto.buyer_name, rto.`buyer_email`, rto.`buyer_phone`,
                    if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, 
                     if(rto.`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                    rto.payment_type, '' as Participant ,
                    rp.payment_amount payment_amount, rp.payment_status as schedule_status,IF(rto.processing_fee_type = 2 ,rp.processing_fee, 0) processing_fees, rp.payment_status as status, 'I' as paytime_type, checkout_status,
                    IF(rp.payment_status = 'FR', 'Completed(refunded)', IF(rp.payment_status = 'R','Refunded',IF(rp.payment_status = 'F','Failed',IF(rp.payment_status = 'MR','Manually Refunded',IF(checkout_status='released','Completed(released)',IF(rp.payment_status = 'M','Completed','Completed(pending)')))))) as view_status,
                    rp.payment_date as schedule_date, ($last_updtdate_rp) last_updt_dt,rp.retail_payment_id as event_payment_id, 'Retail' As Category from retail_orders rto 
                    LEFT JOIN retail_order_details rod ON rto.retail_order_id = rod.retail_order_id 
                    LEFT JOIN retail_payment rp on rp.retail_order_id = rto.retail_order_id
                    LEFT JOIN student s1 ON rto.`student_id` = s1.student_id and rto.company_id = s1.company_id
                    LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`= rto.`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=rto.`company_id`)
                    LEFT JOIN student s2 ON rto.`buyer_email` = s2.student_email and rto.company_id = s2.company_id
                    where rp.payment_amount>'0' and rto.company_id =  '$company_id' AND rod.`retail_payment_status`!='C' $rday_text $rtype_text group by event_reg_id";
            $retail2="SELECT 'Retail' Category, rto.retail_order_id, rp.`retail_payment_id`,'W' flag, rp.payment_amount payment_amount
                    from retail_orders rto 
                    LEFT JOIN retail_payment rp on rp.retail_order_id = rto.retail_order_id
                    where rp.payment_amount>'0' and rto.company_id = '$company_id' $rday_text $rtype_text";
            $retail3="SELECT count(*) as total_record, 'Retail' As category from retail_orders rto 
                    LEFT JOIN retail_payment rp on rp.retail_order_id = rto.retail_order_id
                    where rp.payment_amount>'0' and rto.company_id =  '$company_id' $rday_text $rtype_text";
            
            $misc = "SELECT ms.misc_order_id as event_reg_id, '' as event_id, ms.student_id,msp.`payment_from`,if(msp.`payment_from`='S',msp.`stripe_card_name`, msp.cc_name) as credit_card_name, '' as event_title, ms.buyer_name, ms.`buyer_email`, ms.`buyer_phone`,
                    if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, 
                     if(ms.`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                    '' as payment_type, '' as Participant ,  IF(ms.processing_fee_type = 2, (msp.processing_fee+msp.payment_amount), msp.payment_amount) payment_amount, msp.payment_status as schedule_status,IF(ms.processing_fee_type = 2 ,msp.processing_fee, 0) processing_fees, '' as status, 'I' as paytime_type, checkout_status,
                    IF(msp.payment_status = 'FR', 'Completed(refunded)', IF(msp.payment_status = 'R','Refunded',IF(msp.payment_status = 'F','Failed',IF(msp.payment_status = 'MR','Manually Refunded',IF(checkout_status='released','Completed(released)',IF(msp.payment_status = 'M','Completed','Completed(pending)')))))) as view_status,
                    msp.payment_date as schedule_date, ($last_updtdate_msp) last_updt_dt,msp.misc_payment_id as event_payment_id, 'Miscellaneous' As Category from misc_order ms 
                    LEFT JOIN misc_payment msp on msp.misc_order_id = ms.misc_order_id
                    LEFT JOIN student s1 ON ms.`student_id` = s1.student_id and ms.company_id = s1.company_id
                    LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`= ms.`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                    LEFT JOIN student s2 ON ms.`buyer_email` = s2.student_email and ms.company_id = s2.company_id
                    where msp.payment_amount>'0' and ms.company_id =  '$company_id' $misday_text $mistype_text $order_str";
            $misc2 = "SELECT 'Miscellaneous' Category, ms.misc_order_id, misc_payment_id,'W' flag, IF(ms.processing_fee_type = 2, (msp.processing_fee+msp.payment_amount), msp.payment_amount) payment_amount
                    from misc_order ms 
                    LEFT JOIN misc_payment msp on msp.misc_order_id = ms.misc_order_id
                    where msp.payment_amount>'0' and ms.company_id = '$company_id' $misday_text $mistype_text"; 
            $misc3 = "SELECT count(*) as total_record, 'Miscellaneous' As category from misc_order ms 
                    LEFT JOIN misc_payment msp on msp.misc_order_id = ms.misc_order_id
                    where msp.payment_amount>'0' and ms.company_id =  '$company_id' $misday_text $mistype_text";
            
            if($category=='E'){
                $sql=sprintf("select * from($events $order_str) te %s LIMIT $start, $length_table",$s_text);
                $sql2=sprintf("SELECT SUM(payment_amount) total_amount,flag FROM ($events2)t1 GROUP BY flag");
                $sql_count=sprintf("$event3");
                $sql_sea_count=sprintf("select * from($events) te %s ",$s_text);
            }elseif ($category=='M') {
                $sql=sprintf("select * from($membership $order_str) te %s LIMIT $start, $length_table",$s_text);
                $sql2=sprintf("SELECT SUM(payment_amount) total_amount,flag FROM ($membership2)t1 GROUP BY flag");
                $sql_count=sprintf("$membership3");
                $sql_sea_count=sprintf("select * from($membership) te %s ",$s_text);
            }elseif ($category=='T'){
                $sql=sprintf("select * from($trial $order_str) te %s LIMIT $start, $length_table",$s_text);
                $sql2=sprintf("SELECT SUM(payment_amount) total_amount,flag FROM ($trial2)t1 GROUP BY flag");
                $sql_count=sprintf("$trial3");
                $sql_sea_count=sprintf("select * from($trial) te %s ",$s_text);
            }elseif($category=='R'){
                $sql=sprintf("select * from($retail $order_str) te %s LIMIT $start, $length_table",$s_text);
                $sql2=sprintf("SELECT SUM(payment_amount) total_amount,flag FROM ($retail2)t1 GROUP BY flag");
                $sql_count=sprintf("$retail3");
                $sql_sea_count=sprintf("select * from($retail) te %s ",$s_text);
            }elseif($category=='C'){
                $sql=sprintf("select * from($misc) te %s LIMIT $start, $length_table",$s_text);
                $sql2=sprintf("SELECT SUM(payment_amount) total_amount,flag FROM ($misc2)t1 GROUP BY flag");
                $sql_count=sprintf("$misc3");
                $sql_sea_count=sprintf("select * from($misc) te %s ",$s_text);
            }elseif($category=='A') {
                $sql=sprintf("select * from($events UNION $membership UNION $trial UNION $retail UNION $misc) te %s LIMIT $start, $length_table",$s_text);
                $sql2=sprintf("SELECT SUM(payment_amount) total_amount,flag FROM ($events2 UNION $membership2 UNION $trial2 UNION $retail2 UNION $misc2)t1 GROUP BY flag");
                $sql_count=sprintf("$event3 UNION $membership3 UNION $trial3 UNION $retail3 UNION $misc3");
                $sql_sea_count=sprintf("select * from($events UNION $membership UNION $trial UNION $retail UNION $misc) te %s ",$s_text);
            }
            $result = mysqli_query($this->db,  $sql);
            $result_count= mysqli_query($this->db,  $sql_count);
            $result_sea= mysqli_query($this->db,  $sql_sea_count);
        }
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            if(!$result_count){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_count");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }else{
                $num_rows_count = mysqli_num_rows($result_count);
                $recordsTotal=0;
                if ($num_rows_count > 0) {
                    while ($row_count = mysqli_fetch_assoc($result_count)){
                        $recordsTotal += $row_count['total_record'];
                    }
                    $out['recordsTotal'] = $recordsTotal;
                     $out['recordsFiltered']= $recordsTotal;
                    
                }
            }
            if(!$result_sea){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_sea_count");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }else{
                $num_rows_count1 = mysqli_num_rows($result_sea);
                if($num_rows_count1>0){
                      $out['recordsFiltered'] = $num_rows_count1;
                }else{
                     $out['recordsFiltered']=0;
                }
            }
            $num_rows = mysqli_num_rows($result);
//            if(!empty($search_value)){
//                $out['recordsFiltered'] = $num_rows; 
//            }
           if($num_rows>0){
                $result2 = mysqli_query($this->db, $sql2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }else{
                    $num_rows2 = mysqli_num_rows($result2);
                    if ($num_rows2 > 0) {
                        while ($row2 = mysqli_fetch_assoc($result2)) {
                            if ($payment_type == 'A') {
                                if ($row2['flag'] == 'W') {
                                    $approvedtotal = $row2['total_amount'];
                                } elseif ($row2['flag'] == 'M') {
                                    $approved_manual_total = $row2['total_amount'];
                                } elseif ($row2['flag'] == 'CH') {
                                    $check_total = $row2['total_amount'];
                                }elseif ($row2['flag'] == 'CA') {
                                    $cash_total = $row2['total_amount'];
                                }
                            } elseif ($payment_type == 'P') {
                                $pasttotal = $row2['total_amount'];
                            } elseif ($payment_type == 'U') {
                                $upcomingtotal = $row2['total_amount'];
                            }
                        }
                    }
                }
//               $payment_array = [];
               if ($payment_type == 'A') {
                    $temp_amount = 0;
                    while ($row = mysqli_fetch_assoc($result)) {
//                        if($row['schedule_status']!='R' && $row['schedule_status']!='MR'){
                            $temp_amount = $row['net_amount'];
//                            $approvedtotal = $approvedtotal + $temp_amount;
//                        }
                        $payment_array[] = $row;
                    }
                    $out['paytotal']=$approvedtotal;
                    $out['paymanual']=$approved_manual_total;
                    $out['paycash']=$cash_total;
                    $out['paycheck']=$check_total;
                    
                } else if ($payment_type == 'P') {
                    $temp_amount = 0;
                    while ($row = mysqli_fetch_assoc($result)){
                        if($row['schedule_status']=='F'){
                            $row['view_status'] = 'Failed';
                        }else{
                            $row['view_status'] = 'Past';
                        }
                        if($row['Category']=='Membership' && ($row['status']=='T')){
                                
                        }else{
                            $temp_amount = $row['payment_amount']+$row['processing_fees'];
//                            $pasttotal = $pasttotal + $temp_amount;
                            $payment_array[] = $row;
                        }
                    }
                      $out['paytotal']=$pasttotal;
                } else if ($payment_type == 'U') {
                    $temp_amount = 0;
                    while ($row = mysqli_fetch_assoc($result)) {
                        if($row['schedule_status']=='N'){
                            $row['view_status'] = 'Pending';
                        }else{
                            $row['view_status'] = '';
                        }
                        $temp_amount = $row['payment_amount']+$row['processing_fees'];
//                        $upcomingtotal = $upcomingtotal + $temp_amount;
                        $payment_array[] = $row;
                    }
                     $out['paytotal']=$upcomingtotal;
                }
//                $filtered_total['Manual_total']=$approved_manual_total;
//                $filtered_total['Approved_total']= $approvedtotal;
//                $filtered_total['Past_total']= $pasttotal;
//                $filtered_total['Upcoming_total']= $upcomingtotal;
                $out['draw']=$draw_table;
                $out['data']=$payment_array;
//                $out['grosstotal']=$filtered_total;
//               $msg = array('status' => "Success", "msg" => $out,"total" => $filtered_total);
               $this->response($this->json($out), 200);
               
           }else{
                $out['draw']=$draw_table;
                $out['data']=$payment_array;
                $out['recordsTotal'] = 0;
                $out['recordsFiltered'] = 0;
                
//               $error = array('status' => "Failed", "msg" => "No records found.");
               $this->response($this->json($out), 200);
           }
        }
        date_default_timezone_set($curr_time_zone);
    }          
    
    protected function createCsvPaymentDetails($company_id,$payment_type,$category,$payment_days_type,$payment_startdate_limit,$payment_enddate_limit) {
        
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $last_updtdate_ep = " DATE(CONVERT_TZ(ep.last_updt_dt,$tzadd_add,'$new_timezone'))" ;
        $last_updtdate_mp = " DATE(CONVERT_TZ(mp.last_updt_dt,$tzadd_add,'$new_timezone'))" ;
        $last_updtdate_tp = " DATE(CONVERT_TZ(tp.last_updt_dt,$tzadd_add,'$new_timezone'))" ;
        $last_updtdate_rp = " DATE(CONVERT_TZ(rp.last_updt_dt,$tzadd_add,'$new_timezone'))" ;
        $last_updtdate_msp = " DATE(CONVERT_TZ(msp.last_updt_dt,$tzadd_add,'$new_timezone'))" ;
       $day_text='';
       $mday_text='';
       $mtype_text ='';
       $type_text='';
       $tday_text = $ttype_text = '';
       $rday_text = $rtype_text = $misday_text = $mistype_text = '';
       $approvedtotal = 0;
       $pasttotal = 0;
       $failedtotal = 0;
       $upcomingtotal = 0;
       $payment_array = [];
       $payment_array['Approved']=$payment_array['Past']=$payment_array['Upcoming']=[];
       $union_all = '';
       $membership = '';
       $events = '';
       $retail='';
       if($payment_days_type == 1){
           $day_text = "and DATE($last_updtdate_ep) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
           $mday_text = "and DATE($last_updtdate_mp) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
           $tday_text = "and DATE($last_updtdate_tp) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
           $rday_text = "and DATE($last_updtdate_rp) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
           $misday_text = "and DATE($last_updtdate_msp) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
       }elseif($payment_days_type == 2){
           $day_text = "and DATE($last_updtdate_ep) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
           $mday_text = "and DATE($last_updtdate_mp) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
           $tday_text = "and DATE($last_updtdate_tp) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
           $rday_text = "and DATE($last_updtdate_rp) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
           $misday_text = "and DATE($last_updtdate_msp) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
       }elseif($payment_days_type == 3){
           $day_text = "and DATE($last_updtdate_ep) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
           $mday_text = "and DATE($last_updtdate_mp) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
           $tday_text = "and DATE($last_updtdate_tp) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
           $rday_text = "and DATE($last_updtdate_rp) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
           $misday_text = "and DATE($last_updtdate_msp) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
       }elseif($payment_days_type == 4){
           $day_text = "and DATE($last_updtdate_ep) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
           $mday_text = "and DATE($last_updtdate_mp) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
           $tday_text = "and DATE($last_updtdate_tp) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
           $rday_text = "and DATE($last_updtdate_rp) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
           $misday_text = "and DATE($last_updtdate_msp) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
       }elseif($payment_days_type == 5){
           $day_text = "";
           $mday_text = "";
           $tday_text = "";
           $rday_text = "";
           $misday_text = "";
       }

       if($payment_type == 'A'){
           $type_text ="and (ep.schedule_status ='S' || ep.schedule_status ='R' || ep.schedule_status ='M' || ep.schedule_status ='MF' || ep.schedule_status ='PR' || ep.schedule_status ='MP' || ep.schedule_status ='MR' || ep.schedule_status ='FR' || ep.schedule_status ='CR')"; 
            $mtype_text ="and (mp.payment_status ='S' || mp.payment_status = 'R' || mp.payment_status ='M' || mp.payment_status ='MF' || mp.payment_status ='FR' || mp.payment_status ='MR')";
            $ttype_text = "and (tp.payment_status ='S' || tp.payment_status = 'R' || tp.payment_status ='FR' || tp.payment_status ='M' || tp.payment_status ='MF' || tp.payment_status ='MR')";
            $rtype_text = "and (rp.payment_status ='S' || rp.payment_status = 'R' || rp.payment_status ='FR' || rp.payment_status ='PR' || rp.payment_status ='M' || rp.payment_status ='MF' || rp.payment_status ='MR' || rp.payment_status ='MP')";
            $mistype_text = "and (msp.payment_status ='S' || msp.payment_status = 'R' || msp.payment_status ='FR' || msp.payment_status ='M' || msp.payment_status ='MR' || msp.payment_status ='MF')";
       }else if($payment_type == 'P'){
           $type_text =" and ((ep.schedule_status ='F' && payment_type='RE') or (DATE(ep.schedule_date) < ($currdate_add) and ep.schedule_status='N'))"; 
           $mtype_text =" and mr.membership_status IN ('R', 'P','S','C') and (mp.payment_status ='F' || (mp.payment_status = 'N' and DATE(mp.payment_date) < ($currdate_add)))";
           $ttype_text = " and (tp.payment_status ='F' || (tp.payment_status = 'N' and DATE(tp.payment_date) < ($currdate_add)))";
           $rtype_text = " and (rp.payment_status ='F' || (rp.payment_status = 'N' and DATE(rp.payment_date) < ($currdate_add)))";
           $mistype_text = "and (msp.payment_status ='F' || (msp.payment_status = 'N' and DATE(msp.payment_date) < ($currdate_add)))";
       }
//       else if($payment_type == 'F'){
//           $type_text ="and ep.schedule_status ='F'"; 
//           $mtype_text ="and mp.payment_status ='F'";
//       }
       else if($payment_type == 'U'){
            if($payment_days_type == 1){
               $day_text = "and  DATE(ep.schedule_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 30 DAY";
               $mday_text = "and  DATE(mp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 30 DAY";
               $tday_text = "and  DATE(tp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 30 DAY";
               $rday_text = "and  DATE(rp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 30 DAY";
               $misday_text = "and  DATE(msp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 30 DAY";
           }elseif($payment_days_type == 2){
               $day_text = "and DATE(ep.schedule_date) BETWEEN ($currdate_add)  AND ($currdate_add)+ INTERVAL 60 DAY ";
               $mday_text = "and DATE(mp.payment_date) BETWEEN ($currdate_add)  AND ($currdate_add)+ INTERVAL 60 DAY ";
               $tday_text = "and  DATE(tp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 60 DAY";
               $rday_text = "and  DATE(rp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 60 DAY";
               $misday_text = "and  DATE(msp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 60 DAY";
           }elseif($payment_days_type == 3){
               $day_text = "and DATE(ep.schedule_date) BETWEEN ($currdate_add)  AND ($currdate_add)+ INTERVAL 90 DAY ";
               $mday_text = "and DATE(mp.payment_date) BETWEEN ($currdate_add)  AND ($currdate_add)+ INTERVAL 90 DAY ";
               $tday_text = "and  DATE(tp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 90 DAY";
               $rday_text = "and  DATE(rp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 90 DAY";
               $misday_text = "and  DATE(msp.payment_date) BETWEEN ($currdate_add) AND ($currdate_add) + INTERVAL 90 DAY";
           }elseif($payment_days_type == 4){
               $day_text = "and DATE(ep.schedule_date) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
               $mday_text = "and DATE(mp.payment_date) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
               $tday_text = "and DATE(tp.payment_date) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
               $rday_text = "and DATE(rp.payment_date) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
               $misday_text = "and DATE(msp.payment_date) BETWEEN '".$payment_startdate_limit."' AND '".$payment_enddate_limit."'";
           }elseif($payment_days_type == 5){
               $day_text = "";
               $mday_text = "";
               $tday_text = "";
               $rday_text = "";
               $misday_text = "";
           }
           $type_text =" and (ep.schedule_status ='N' and (ep.schedule_date >= ($currdate_add))) "; 
           $mtype_text =" and (mr.membership_status='R' || (mr.membership_status='P' AND mr.`resume_date`>=($currdate_add) AND mr.`resume_date` NOT IN (NULL, '0000-00-00'))) and (mp.payment_status ='N' and (mp.payment_date >= ($currdate_add)))";
           $ttype_text =" and (tp.payment_status ='N' and (tp.payment_date >= ($currdate_add))) ";
           $rtype_text =" and (rp.payment_status ='N' and (rp.payment_date >= ($currdate_add))) ";
           $mistype_text =" and (msp.payment_status ='N' and (msp.payment_date >= ($currdate_add))) ";
       }
       
       if($payment_type == 'A'){
           
            $processing_fee_temp="if(ep.`schedule_status`='PR'||ep.`schedule_status`='CR',(SELECT SUM(`processing_fee`) FROM `event_payment` WHERE `event_reg_id`=ep.`event_reg_id` AND `checkout_id`=ep.`checkout_id` AND `schedule_status` IN ('R','PR')),ep.`processing_fee`)";
            $events = "SELECT er.event_reg_id,e.event_id, er.student_id, IF(ep.`schedule_status`IN('M','MR'),'',ep.cc_name) credit_card_name,
                 e.event_title, '' category_title,'' membership_title, '' trial_title,''retail_title, concat(er.`buyer_last_name`,', ',er.`buyer_first_name`) buyer_name, er.`buyer_email`, er.`buyer_phone`,
                er.payment_type, CONCAT(er.event_registration_column_2,', ',er.event_registration_column_1) as Participant ,
                    IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-',if(er.processing_fee_type=2,(ep.payment_amount+$processing_fee_temp),ep.payment_amount)), 
                    if(er.processing_fee_type=2 ,IF(ep.schedule_status='CR'||ep.schedule_status='FR',ep.payment_amount+$processing_fee_temp,ep.payment_amount+$processing_fee_temp),IF(ep.schedule_status='CR'||ep.schedule_status='FR' , ep.payment_amount, ep.payment_amount))) payment_amount,                
                    IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-',if(er.processing_fee_type=1,(ep.payment_amount-$processing_fee_temp),ep.payment_amount)),
                    if(er.processing_fee_type=1 ,IF(ep.schedule_status='CR'||ep.schedule_status='FR', ep.payment_amount-$processing_fee_temp,ep.payment_amount-($processing_fee_temp)), IF(ep.schedule_status='CR'||ep.schedule_status='FR', ep.payment_amount, ep.payment_amount))) net_amount,'' as tax_value, 
                    ep.schedule_status,$processing_fee_temp as processing_fees ,  checkout_status,
                    IF(ep.schedule_status = 'FR' || ep.schedule_status = 'CR', 'Completed(refunded)', IF(ep.schedule_status = 'R','Refunded',IF(ep.schedule_status = 'F','Failed',IF(ep.schedule_status = 'MR','Refunded',IF(ep.schedule_status = 'MF','Completed',IF(checkout_status='released','Completed(released)',IF(ep.schedule_status = 'M','Completed','Completed(pending)'))))))) as view_status,
                    ep.schedule_date, IF(ep.schedule_status='FR'||ep.schedule_status='CR', IFNULL(ep.`schedule_date`, DATE(ep.created_dt)), ($last_updtdate_ep)) last_updt_dt,ep.event_payment_id, 'Events' As Category from event_registration er 
                LEFT JOIN event e ON er.event_id = e.event_id 
                LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
                    LEFT JOIN student s1 ON er.`student_id` = s1.student_id and er.company_id = s1.company_id
                    LEFT JOIN student s2 ON er.`buyer_email` = s2.student_email and er.company_id = s2.company_id
                where ep.payment_amount>'0' && er.company_id = '$company_id' $day_text $type_text";
            
            
            $membership = " SELECT mr.membership_registration_id as event_reg_id ,m.membership_id as event_id, mr.student_id, IF(mp.`payment_status`IN('M','MR'),'',mp.cc_name) credit_card_name,'' event_title, m.category_title ,mo.membership_title, '' trial_title,''retail_title , concat(mr.`buyer_last_name`,', ',mr.`buyer_first_name`) buyer_name, mr.`buyer_email`, mr.`buyer_phone`,
                mr.payment_type, CONCAT(mr.membership_registration_column_2,', ',mr.membership_registration_column_1) as Participant ,
                IF(mp.payment_status = 'R' || mp.payment_status = 'MR', concat('-',if(mr.processing_fee_type=2,(mp.payment_amount+mp.processing_fee),mp.payment_amount)), 
                    if(mr.processing_fee_type=2 ,mp.payment_amount+mp.processing_fee,mp.payment_amount-mp.refunded_amount)) payment_amount,
                IF(mp.payment_status = 'R' || mp.payment_status = 'MR' , concat('-',if(mr.processing_fee_type=1,(mp.payment_amount-mp.processing_fee),mp.payment_amount)),
                    if(mr.processing_fee_type=1 ,mp.payment_amount-mp.processing_fee,mp.payment_amount)) net_amount,'' as tax_value,
                mp.payment_status as schedule_status ,mp.processing_fee as processing_fees ,  checkout_status,
                   IF(mp.payment_status = 'FR', 'Completed(refunded)', IF(mp.payment_status = 'R','Refunded',IF(mp.payment_status = 'F','Failed',IF(mp.payment_status = 'MR','Refunded',IF(mp.payment_status = 'MF','Completed',IF(checkout_status='released','Completed(released)',IF(mp.payment_status = 'M','Completed','Completed(pending)'))))))) as view_status,
                    mp.payment_date as schedule_date, IF(mp.payment_date='FR', IFNULL(mp.payment_date, DATE(mp.created_dt)), ($last_updtdate_mp)) last_updt_dt,mp.membership_payment_id as event_payment_id, 'Membership' As Category from membership_registration mr 
                LEFT JOIN membership m ON mr.membership_id = m.membership_id 
                LEFT JOIN membership_options mo on mo.membership_id=m.membership_id AND mr.`membership_option_id`=mo.`membership_option_id`
                LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id
                    LEFT JOIN student s1 ON mr.`student_id` = s1.student_id and mr.company_id = s1.company_id
                    LEFT JOIN student s2 ON mr.`buyer_email` = s2.student_email and mr.company_id = s2.company_id
                where mp.payment_amount>'0' && mr.company_id = '$company_id' $mday_text $mtype_text";
            
              $trial = " SELECT tr.trial_reg_id as event_reg_id ,t.trial_id as event_id, tr.student_id, IF(tp.`payment_status`IN('M','MR'),'',tp.cc_name) credit_card_name,'' event_title, '' category_title ,'' membership_title,t.trial_title,''retail_title , concat(tr.`buyer_last_name`,', ',tr.`buyer_first_name`) buyer_name, tr.`buyer_email`, tr.`buyer_phone`,
                tr.payment_type, CONCAT(tr.trial_registration_column_2,', ',tr.trial_registration_column_1) as Participant ,
                IF(tp.payment_status = 'R' || tp.payment_status = 'MR' , concat('-',if(tr.processing_fee_type=2,(tp.payment_amount+tp.processing_fee),tp.payment_amount)), 
                    if(tr.processing_fee_type=2 ,tp.payment_amount+tp.processing_fee,tp.payment_amount-tp.refunded_amount)) payment_amount,
                IF(tp.payment_status = 'R' || tp.payment_status = 'MR' , concat('-',if(tr.processing_fee_type=1,(tp.payment_amount-tp.processing_fee),tp.payment_amount)),
                    if(tr.processing_fee_type=1 ,tp.payment_amount-tp.processing_fee,tp.payment_amount)) net_amount,'' as tax_value,
                tp.payment_status as schedule_status ,tp.processing_fee as processing_fees ,  checkout_status,
                    IF(tp.payment_status = 'FR', 'Completed(refunded)', IF(tp.payment_status = 'R','Refunded',IF(tp.payment_status = 'F','Failed',IF(tp.payment_status = 'MR','Refunded',IF(tp.payment_status = 'MF','Completed',IF(checkout_status='released','Completed(released)',IF(tp.payment_status = 'M','Completed','Completed(pending)'))))))) as view_status,
                    tp.payment_date as schedule_date, IF(tp.payment_date='FR', IFNULL(tp.payment_date, DATE(tp.created_dt)), ($last_updtdate_tp)) last_updt_dt,tp.trial_payment_id as event_payment_id, 'Trial' As Category from trial_registration tr 
                LEFT JOIN trial t ON tr.trial_id = t.trial_id 
                LEFT JOIN trial_payment tp on tp.trial_reg_id = tr.trial_reg_id
                    LEFT JOIN student s1 ON tr.`student_id` = s1.student_id and tr.company_id = s1.company_id
                    LEFT JOIN student s2 ON tr.`buyer_email` = s2.student_email and tr.company_id = s2.company_id
                where tp.payment_amount>'0' && tr.company_id = '$company_id' $tday_text $ttype_text ";
              
              $retail="SELECT rto.retail_order_id as event_reg_id ,'' as event_id, rto.student_id, IF(rp.`payment_status`IN('M','MR'),'',rp.cc_name) credit_card_name,'' event_title, '' category_title ,'' membership_title,''trial_title ,GROUP_CONCAT(rod.retail_product_name)as retail_title, concat(rto.`buyer_last_name`,', ',rto.`buyer_first_name`) buyer_name, rto.`buyer_email`, rto.`buyer_phone`,
                rto.payment_type, '' Participant ,
                IF(rp.payment_status = 'R' || rp.payment_status = 'MR' , concat('-',rp.payment_amount), 
                    rp.payment_amount) payment_amount,
                IF(rp.payment_status = 'R' || rp.payment_status = 'MR', concat('-',rp.payment_amount-rp.processing_fee),
                    rp.payment_amount-rp.processing_fee) net_amount,concat(c.`wp_currency_symbol`,IF(rp.`retail_tax_payment`=0, '', IF(rp.payment_status = 'R', '-', '')),rp.`retail_tax_payment`) as tax_value,
                rp.payment_status as schedule_status ,rp.processing_fee as processing_fees ,  checkout_status,
                    IF(rp.payment_status = 'FR', 'Completed(refunded)', IF(rp.payment_status = 'R','Refunded',IF(rp.payment_status = 'F','Failed',IF(rp.payment_status = 'MR','Refunded',IF(rp.payment_status = 'MF','Completed',IF(checkout_status='released','Completed(released)',IF(rp.payment_status = 'M','Completed','Completed(pending)'))))))) as view_status,
                    rp.payment_date as schedule_date, IF(rp.payment_date='FR', IFNULL(rp.payment_date, DATE(rp.created_dt)), ($last_updtdate_rp)) last_updt_dt,rp.retail_payment_id as event_payment_id, 'Retail' As Category from retail_orders rto 
                LEFT JOIN retail_order_details rod ON rto.retail_order_id = rod.retail_order_id  
                LEFT JOIN retail_payment rp on rp.retail_order_id = rto.retail_order_id
                    LEFT JOIN student s1 ON rto.`student_id` = s1.student_id and rto.company_id = s1.company_id
                    LEFT JOIN company c ON rto.`company_id` = c.company_id
                    LEFT JOIN student s2 ON rto.`buyer_email` = s2.student_email and rto.company_id = s2.company_id
                where rp.payment_amount>'0' AND rto.company_id = '$company_id' AND rod.`retail_payment_status`!='C' $rday_text $rtype_text group by event_reg_id";
              
              // Miscellaneous queries
            $misc = "SELECT ms.misc_order_id as event_reg_id ,'' as event_id, ms.student_id, IF(msp.`payment_status`IN('M','MR'),'',msp.cc_name) credit_card_name,'' event_title, '' category_title ,'' membership_title,'' trial_title,''retail_title , concat(ms.`buyer_last_name`,', ',ms.`buyer_first_name`) buyer_name, ms.`buyer_email`, ms.`buyer_phone`,
                '' payment_type, '' as Participant ,
                IF(msp.payment_status = 'R' || msp.payment_status = 'MR' , concat('-',if(ms.processing_fee_type=2,(msp.payment_amount+msp.processing_fee),msp.payment_amount)), 
                    if(ms.processing_fee_type=2 ,msp.payment_amount+msp.processing_fee,msp.payment_amount-msp.refunded_amount)) payment_amount,
                IF(msp.payment_status = 'R' || msp.payment_status = 'MR' , concat('-',if(ms.processing_fee_type=1,(msp.payment_amount-msp.processing_fee),msp.payment_amount)),
                    if(ms.processing_fee_type=1 ,msp.payment_amount-msp.processing_fee,msp.payment_amount)) net_amount,'' as tax_value,
                msp.payment_status as schedule_status ,msp.processing_fee as processing_fees ,  checkout_status,
                    IF(msp.payment_status = 'FR', 'Completed(refunded)', IF(msp.payment_status = 'R','Refunded',IF(msp.payment_status = 'F','Failed',IF(msp.payment_status = 'MR','Refunded',IF(msp.payment_status = 'MF','Completed',IF(checkout_status='released','Completed(released)',IF(msp.payment_status = 'M','Completed','Completed(pending)'))))))) as view_status,
                    msp.payment_date as schedule_date, IF(msp.payment_date='FR', IFNULL(msp.payment_date, DATE(msp.created_dt)), ($last_updtdate_msp)) last_updt_dt,msp.misc_payment_id as event_payment_id, 'Miscellaneous' As Category from misc_order ms 
                LEFT JOIN misc_payment msp on msp.misc_order_id = ms.misc_order_id
                    LEFT JOIN student s1 ON ms.`student_id` = s1.student_id and ms.company_id = s1.company_id
                    LEFT JOIN student s2 ON ms.`buyer_email` = s2.student_email and ms.company_id = s2.company_id
                where msp.payment_amount>'0' && ms.company_id =   '$company_id' $misday_text $mistype_text ";
         
            if($category=='E'){
                $sql=sprintf("$events order by last_updt_dt desc");
            }elseif ($category=='M') {
                 $sql=sprintf("$membership order by last_updt_dt desc");
            }elseif($category=='T'){
                $sql=sprintf("$trial order by last_updt_dt desc");
            }elseif($category=='R'){
                $sql=sprintf("$retail order by last_updt_dt desc");
            }elseif($category=='C'){
                $sql=sprintf("$misc order by last_updt_dt desc");
            }elseif ($category=='A') {
                $sql=sprintf("$events UNION $membership UNION $trial UNION $retail UNION $misc order by last_updt_dt desc");
            }                       

            $result = mysqli_query($this->db,  $sql);
       }else{
           if($payment_type == 'U'){
               $order_str = "order by schedule_date";
           }else{
               $order_str = "order by schedule_date desc";
           }
           
        $events = "SELECT er.event_reg_id,e.event_id, er.student_id, IF(ep.`schedule_status`IN('M','MR'),'',ep.cc_name) credit_card_name, e.event_title, '' category_title,'' membership_title, '' trial_title,''retail_title, concat(er.`buyer_last_name`,', ',er.`buyer_first_name`) buyer_name , er.`buyer_email`, er.`buyer_phone`, 
                er.payment_type, CONCAT(er.event_registration_column_2,', ',er.event_registration_column_1) as Participant ,
                IF(ep.schedule_status = 'R' || ep.schedule_status = 'MR', concat('-', ep.payment_amount), ep.payment_amount) payment_amount, ep.schedule_status,
                IF(er.processing_fee_type = 2 ,ep.processing_fee, 0) processing_fees, '' as status, '' as paytime_type, checkout_status,
                ep.schedule_date, ($last_updtdate_ep) last_updt_dt,ep.event_payment_id, 'Events' As Category from event_registration er 
                LEFT JOIN event e ON er.event_id = e.event_id 
                LEFT JOIN event_payment ep on ep.event_reg_id = er.event_reg_id 
                where ep.payment_amount>'0' and er.company_id = '$company_id' $day_text $type_text";
           
           
        $membership = "SELECT mr.membership_registration_id as event_reg_id,m.membership_id as event_id, mr.student_id, IF(mp.`payment_status`IN('M','MR'),'',mp.cc_name) credit_card_name,'' event_title, m.category_title ,mo.membership_title, '' trial_title ,''retail_title, concat(mr.`buyer_last_name`,', ',mr.`buyer_first_name`) buyer_name , mr.`buyer_email`, mr.`buyer_phone`, 
                mr.payment_type, CONCAT(mr.membership_registration_column_2,', ',mr.membership_registration_column_1) as Participant ,
                mp.payment_amount,mp.payment_status as schedule_status,IF(mr.processing_fee_type = 2 ,mp.processing_fee, 0) processing_fees, mr.membership_status as status, paytime_type as paytime_type, checkout_status,
                mp.payment_date as schedule_date, ($last_updtdate_mp) last_updt_dt,mp.membership_payment_id as event_payment_id, 'Membership' As Category from membership_registration mr 
                LEFT JOIN membership m ON mr.membership_id = m.membership_id 
                LEFT JOIN membership_options mo on mo.membership_id=m.membership_id AND mr.`membership_option_id`=mo.`membership_option_id`
                LEFT JOIN membership_payment mp on mp.membership_registration_id = mr.membership_registration_id
                where mp.payment_amount>'0' && mr.company_id =  '$company_id' $mday_text $mtype_text ";
        
        $trial = "SELECT tr.trial_reg_id as event_reg_id,t.trial_id as event_id, tr.student_id, IF(tp.`payment_status`IN('M','MR'),'',tp.cc_name) credit_card_name,'' event_title, '' category_title ,'' membership_title , t.trial_title,''retail_title,concat(tr.`buyer_last_name`,', ',tr.`buyer_first_name`) buyer_name , tr.`buyer_email`, tr.`buyer_phone`, 
                tr.payment_type, CONCAT(tr.trial_registration_column_2,', ',tr.trial_registration_column_1) as Participant ,
                tp.payment_amount,tp.payment_status as schedule_status,IF(tr.processing_fee_type = 2 ,tp.processing_fee, 0) processing_fees, tr.trial_status as status, '' as paytime_type, checkout_status,
                tp.payment_date as schedule_date, ($last_updtdate_tp) last_updt_dt,tp.trial_payment_id as event_payment_id, 'Trial' As Category from trial_registration tr 
                LEFT JOIN trial t ON tr.trial_id = t.trial_id 
                LEFT JOIN trial_payment tp on tp.trial_reg_id = tr.trial_reg_id
                where tp.payment_amount>'0' && tr.company_id =  '$company_id' $tday_text $ttype_text ";
        
        $retail="SELECT rto.retail_order_id as event_reg_id,'' as event_id, rto.student_id, IF(rp.`payment_status`IN('M','MR'),'',rp.cc_name) credit_card_name,'' event_title, '' category_title ,'' membership_title , '' trial_title,GROUP_CONCAT(rod.retail_product_name) retail_title,concat(rto.`buyer_last_name`,', ',rto.`buyer_first_name`) buyer_name , rto.`buyer_email`, rto.`buyer_phone`, 
                rto.payment_type, '' as Participant ,
                rp.payment_amount,rp.payment_status as schedule_status,IF(rto.processing_fee_type = 2 ,rp.processing_fee, 0) processing_fees, rp.payment_status as status, '' as paytime_type, checkout_status,
                rp.payment_date as schedule_date, ($last_updtdate_rp) last_updt_dt,rp.retail_payment_id as event_payment_id, 'Retail' As Category from retail_orders rto 
                LEFT JOIN retail_order_details rod ON rto.retail_order_id = rod.retail_order_id
                LEFT JOIN retail_payment rp on rp.retail_order_id = rto.retail_order_id
                where rp.payment_amount>'0' AND rto.company_id =  '$company_id' AND rod.`retail_payment_status`!='C' $rday_text $rtype_text group by event_reg_id ";
        
        $misc = "SELECT ms.misc_order_id as event_reg_id,'' as event_id, ms.student_id, IF(msp.`payment_status`IN('M','MR'),'',msp.cc_name) credit_card_name,'' event_title, '' category_title ,'' membership_title , '' trial_title,'' retail_title,concat(ms.`buyer_last_name`,', ',ms.`buyer_first_name`) buyer_name , ms.`buyer_email`, ms.`buyer_phone`, 
                '' payment_type, '' as Participant ,
                msp.payment_amount,msp.payment_status as schedule_status,IF(ms.processing_fee_type = 2 ,msp.processing_fee, 0) processing_fees, '' as status, '' as paytime_type, checkout_status,
                msp.payment_date as schedule_date, ($last_updtdate_msp) last_updt_dt,msp.misc_payment_id as event_payment_id, 'Miscellaneous' As Category from misc_order ms
                LEFT JOIN misc_payment msp on msp.misc_order_id = ms.misc_order_id
                where msp.payment_amount>'0' && ms.company_id =  '$company_id' $misday_text $mistype_text";
           
           if($category=='E'){
                $sql=sprintf("$events $order_str");
            }elseif ($category=='M') {
                 $sql=sprintf("$membership $order_str");
            }elseif ($category=='T'){
                $sql=sprintf("$trial $order_str");
            }elseif ($category=='R'){
                $sql=sprintf("$retail $order_str");
            }elseif ($category=='C'){
                $sql=sprintf("$misc");
            }elseif ($category=='A') {
                $sql=sprintf("$events UNION $membership UNION $trial UNION $retail UNION $misc");
            }
            $result = mysqli_query($this->db,  $sql);
       }         
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
           $num_rows = mysqli_num_rows($result);
           if($num_rows>0){
//               $payment_array = [];
               if ($payment_type == 'A') {
                    while ($row = mysqli_fetch_assoc($result)) {
                        if($row['schedule_status']!='R' && $row['schedule_status']!='M' && $row['schedule_status']!='MR'){
                            $temp_amount = 0;
                            $temp_amount = $row['net_amount'];
                            $approvedtotal = $approvedtotal + $temp_amount;
                        }
                        $payment_array['Approved'][] = $row;
                    }
                } else if ($payment_type == 'P') {
                    while ($row = mysqli_fetch_assoc($result)){
                        if($row['schedule_status']=='F'){
                            $row['view_status'] = 'Failed';
                        }else{
                            $row['view_status'] = 'Past';
                        }
                        if($row['Category']=='Membership' && $row['status']=='P' && $row['paytime_type']=='R'){
                                
                        }else{
                            $temp_amount = 0;
                            $temp_amount = $row['payment_amount']+$row['processing_fees'];
                            $pasttotal = $pasttotal + $temp_amount;
                            $payment_array['Past'][] = $row;
                        }
                    }
                    }
//                else if ($payment_type == 'F') {
//                    while ($row = mysqli_fetch_assoc($result)) {
//                        $temp_amount = 0;
//                        $temp_amount = $row['payment_amount']+$row['processing_fees'];
//                        $failedtotal = $failedtotal + $temp_amount;
//                        $payment_array['Failed'][] = $row;
//                    }
//                } 
                else if ($payment_type == 'U') {
                    while ($row = mysqli_fetch_assoc($result)) {
                        if($row['schedule_status']=='N'){
                            $row['view_status'] = 'Pending';
                        }else{
                            $row['view_status'] = '';
                        }
                        $temp_amount = 0;
                        $temp_amount = $row['payment_amount']+$row['processing_fees'];
                        $upcomingtotal = $upcomingtotal + $temp_amount;
                        $payment_array['Upcoming'][] = $row;
                    }
                }
           }    
        }
      
        $header = $body = '';
        if ($payment_type == 'A') {
            $payment_array2 = $payment_array['Approved'];
            $header .= "Payment Date,Billdue,Buyer Name,Buyer Email,Buyer Phone,Participant Name,Category,Event Name,Membership Program,Membership Option,Trial Program,Retail,Amount,Net,Tax Value,Status,Payment Method\r\n";
            for ($i = 0; $i < count($payment_array2); $i++) {
//                echo $payment_array2[$i]['last_updt_dt'];
                $body .= '"' . str_replace('"', "''", $payment_array2[$i]['last_updt_dt']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['schedule_date']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['Participant']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['Category']) . '"' . "," .
                        '"' . str_replace('"', "''", $payment_array2[$i]['event_title']) . '"' . "," .'"' . str_replace('"', "''", $payment_array2[$i]['category_title']) . '"' . "," .'"' . str_replace('"', "''", $payment_array2[$i]['membership_title']) . '"' . "," .'"' . str_replace('"', "''", $payment_array2[$i]['trial_title']) . '"' . "," .'"' . str_replace('"', "''", $payment_array2[$i]['retail_title']) . '"'. "," .
                        '"' . str_replace('"', "''", $payment_array2[$i]['payment_amount']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['net_amount']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['tax_value']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['view_status']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['credit_card_name']) . '"' ."\r\n";
            }
        } elseif($payment_type=='P'){
            $payment_array2 = $payment_array['Past'];
            $header .= "Billdue,Buyer Name,Buyer Email,Buyer Phone,Participant Name,Category,Event Name,Membership Program,Membership Option,Trial Program,Retail,Amount,Pass Fees On,Status,Payment Method\r\n";
            for ($i = 0; $i < count($payment_array2); $i++) {
                $body .= '"' . str_replace('"', "''", $payment_array2[$i]['schedule_date']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['Participant']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['Category']) . '"' . "," .
                        '"' . str_replace('"', "''", $payment_array2[$i]['event_title']) . '"' . ","  .'"' . str_replace('"', "''", $payment_array2[$i]['category_title']) . '"' . "," .'"' . str_replace('"', "''", $payment_array2[$i]['membership_title']) . '"' . "," .'"' . str_replace('"', "''", $payment_array2[$i]['trial_title']) . '"' . "," .'"' . str_replace('"', "''", $payment_array2[$i]['retail_title']) . '"'. "," . '"' . str_replace('"', "''", $payment_array2[$i]['payment_amount']) . '"' . "," .
                        '"' . str_replace('"', "''", $payment_array2[$i]['processing_fees']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['view_status']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['credit_card_name']) . '"' ."\r\n";
            }
        }else{
            $payment_array2 = $payment_array['Upcoming'];
            $header .= "Billdue,Buyer Name,Buyer Email,Buyer Phone,Participant Name,Category,Event Name,Membership Program,Membership Option,Trial Program,Retail,Amount,Status,Payment Method\r\n";
            for ($i = 0; $i < count($payment_array2); $i++) {
                 $body .= '"' . str_replace('"', "''", $payment_array2[$i]['schedule_date']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['buyer_name']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['buyer_email']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['buyer_phone']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['Participant']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['Category']) . '"' . "," .
                         '"' . str_replace('"', "''", $payment_array2[$i]['event_title']) . '"' . ","  .'"' . str_replace('"', "''", $payment_array2[$i]['category_title']) . '"' . "," .'"' . str_replace('"', "''", $payment_array2[$i]['membership_title']) . '"' . "," .'"' . str_replace('"', "''", $payment_array2[$i]['trial_title']) . '"' . "," .'"' . str_replace('"', "''", $payment_array2[$i]['retail_title']) . '"'. "," . '"' . str_replace('"', "''", $payment_array2[$i]['payment_amount']) . '"' . "," .
                        '"' . str_replace('"', "''", $payment_array2[$i]['processing_fees']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['view_status']) . '"' . "," . '"' . str_replace('"', "''", $payment_array2[$i]['credit_card_name']) . '"' ."\r\n";
            }
        }
        
        ob_clean();
        ob_start();
        $file = "../../../uploads/paymentDetails.csv";
        $ifp = fopen($file, "w");
        fwrite($ifp, $header);
        fwrite($ifp, $body);
        fclose($ifp);
        header('Content-Type: application/x-download; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        readfile($file);
        unlink($file);
        date_default_timezone_set($curr_time_zone);
        exit();
    }
    
    protected function createCsvStudentDetails($company_id,$selected_student_list,$select_all_flag,$search) {
       
        $output_student_id = [];
         if ($select_all_flag == 'Y') {
         if(!empty($search)){
             $s_text=" and (student_name like  '%$search%'  or student_email like  '%$search%'  or first_login_dt like  '%$search%'  or  last_login_dt like  '%$search%')";
         }else{
             $s_text='';
         }
             
            $sql_all = sprintf("SELECT `student_id` as id
                FROM `student` WHERE company_id = '%s' AND `deleted_flag`!='Y' %s", $company_id,$s_text);
            $result_all = mysqli_query($this->db, $sql_all);
            if (!$result_all) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_all");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($result_all);
                if ($num_rows > 0) {
                    while ($rows1 = mysqli_fetch_assoc($result_all)) {
                        $output_student_id[] = $rows1['id'];
                    }
                }
            }
        }
        
        for($i=0; $i<count($selected_student_list); $i++){
            $output_student_id[] = $selected_student_list[$i]['id'];
        }
        $student_id_list = implode(",", $output_student_id);        
        
        $sql = sprintf("SELECT `student_id`,`student_name`,`student_email`, `first_login_dt`, `last_login_dt` FROM `student` WHERE company_id = '%s' AND `student_id` IN (%s) AND `deleted_flag`!='Y' ORDER BY `last_login_dt`", $company_id, mysqli_real_escape_string($this->db, $student_id_list));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output['student_details'][] = $row;
                }
//                $out = array('status' => "Success", 'msg' => $output);
//                // If success everythig is good send header as "OK" and user details
//                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Student details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
        
        $header = $body = '';
            $student = $output['student_details'];
            $header .= "Name,Email,First Login,Last Login\r\n";
            for ($i = 0; $i < count($student); $i++) {
                $body .= '"' . str_replace('"', "''", $student[$i]['student_name']) . '"' . "," . '"' . str_replace('"', "''", $student[$i]['student_email']) . '"' . "," . '"' . str_replace('"', "''", $student[$i]['first_login_dt']) . '"' . "," . '"' . str_replace('"', "''", $student[$i]['last_login_dt']) . '"' ."\r\n";
            }
        
        ob_clean();
        ob_start();
        $file = "../../../uploads/studentDetails.csv";
        $ifp = fopen($file, "w");
        fwrite($ifp, $header);
        fwrite($ifp, $body);
        fclose($ifp);
        header('Content-Type: application/x-download; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        readfile($file);
        unlink($file);
        exit();
        
    }
    
    protected function getDashboardDetails($company_id,$netsales_flag,$member,$netsales,$activemembers_flag,$rentation_flag,$sales_period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = "DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $ep_last_upd_date=" DATE(CONVERT_TZ(ep.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $mr_last_upd_date=" DATE(CONVERT_TZ(mr.last_updt_dt,$tzadd_add,'$new_timezone'))";
        
        $membership = $membership_new = $membership_id_arr = $event_id_arr = $event_title_arr = $event = $retail = $final_ms_sales_array = $final_ts_sales_array = $final_rs_sales_array = $final_rcs_sales_array = $final_active_members_array = $final_new_members_array = $final_pc_members_array=$final_rc_members_array = $final_e_sales_array = $final_can_members_array=[];
        $m_total_sales_arr = $t_total_sales_arr = $r_total_sales_arr = $misc_total_sales_arr = $rc_total_sales_arr = $m_total_active_members_arr = $m_total_new_members = $m_total_pause_members = $m_total_cancel_members =$m_total_completed_members= $e_total_sales_arr = $net_gain_arr = [0, 0, 0, 0, 0, 0];
        $m_total_pause_resume_members=['-','-','-','-','-','-'];
        $failed_total = $membership_total = $event_total = $current_hold =$upcoming_total= 0;
        $net_sales = $active_members = $retention = [];$category_arr = $child_id_arr = $retail_child_array = [];
        $curr_month = $last_month = '';
        $date_str = "%Y-%m-01";
        if($member =='A'){
             $p_months=($activemembers_flag*1);
             $curr_date = date('Y-m-d', strtotime("-$p_months month"));
            $month_array = [];
            
            for ($i = 0; $i < 6; $i++) {
                $format_array[] = date('Y-m', strtotime("first day of -$i month", strtotime($curr_date)));
                $month_array[] = date('n', strtotime("first day of -$i month", strtotime($curr_date)));
                $month_name[] = date('M Y', strtotime("first day of -$i month", strtotime($curr_date)));
            }
             $format_list = implode("','", $format_array);
            
             $month_name_array = ["first_month","second_month","third_month","fourth_month","fifth_month","sixth_month"];
        }
        if ($netsales == 'N') {
            $n_months = ($netsales_flag*1);
            $oneplus=$n_months+1;
           
            $month_array1 = [];
            if($sales_period=='A'){
                
                 $curr_date1 = date('Y-m-d', strtotime("-$n_months year"));
                 $curr_datepulsoneyear = date('Y-m-01', strtotime(" $oneplus year"));
                for ($i = 0; $i < 6; $i++) {
                    $format_array1[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date1)));
                    $month_array1[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date1)));
                    $month_name1[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date1)));
                }
                
            }elseif($sales_period=='M'){
                $curr_datepulsoneyear = date('Y-m-01', strtotime(" $oneplus month"));
                 $curr_date1 = date('Y-m-d', strtotime("-$n_months month"));
                for ($i = 0; $i < 6; $i++) {
                    $format_array1[] = date('Y-m', strtotime("first day of -$i month", strtotime($curr_date1)));
                    $month_array1[] = date('n', strtotime("first day of -$i month", strtotime($curr_date1)));
                    $month_name1[] = date('M Y', strtotime("first day of -$i month", strtotime($curr_date1)));
                }
            }
           
            $format_list1 = implode("','", $format_array1);
           
            $month_name_array = ["first_month","second_month","third_month","fourth_month","fifth_month","sixth_month"];
        }

//        $curr_date = date('Y-m-d',strtotime("-$p_months month"));
//        $month_array = [];
//        for($i=0;$i<4;$i++){
//            $format_array[] = date('Y-m', strtotime("first day of -$i month", strtotime($curr_date)));
//            $month_array[] = date('n', strtotime("first day of -$i month", strtotime($curr_date)));
//            $month_name[] = date('M-y', strtotime("first day of -$i month", strtotime($curr_date)));
//        }
//        $month_name_array = ["first_month","second_month","third_month","fourth_month"];
                
       
        
        //net_sales for trial and membership
        if($sales_period=='A'){
            $sql = sprintf("SELECT 'membership' as type,concat('m_',`category_id`) unique_id, period, SUBSTR(period, 1, 4) month, category_id, 0 as child_id, m.category_title, SUM(ifnull(`sales_amount`,0)) net_amount
                FROM `membership_dimensions`md LEFT JOIN `membership` m ON md.`category_id`=m.`membership_id` WHERE md.company_id='%s' AND `option_id`=0 AND m.`deleted_flg`!='D' AND  SUBSTR(period, 1, 4) IN ('%s') GROUP BY category_id,  SUBSTR(period, 1, 4)
                 UNION ALL
                 SELECT 'trial' as type,concat('t_',td.`trial_id`) unique_id, period, SUBSTR(period, 1, 4) month, td.trial_id as category_id, 0 as child_id, t.trial_title as category_title , SUM(ifnull(`sales_amount`,0)) net_amount
                 FROM `trial_dimensions` td LEFT JOIN `trial` t ON td.`trial_id`=t.`trial_id` WHERE td.company_id='%s' AND SUBSTR(period, 1, 4) IN ('%s') GROUP BY td.trial_id,  SUBSTR(td.period, 1, 4)
                 UNION ALL
                 SELECT 'retail' as type,concat('r_',`product_id`) unique_id, period, SUBSTR(period, 1, 4) month, `product_id` as category_id, `child_id`, rp.`retail_product_title`, SUM(ifnull(`net_sales`,0)) net_amount
                 FROM `retail_dimensions` rd LEFT JOIN `retail_products` rp ON rd.`product_id`=rp.`retail_product_id` WHERE rd.company_id='%s' AND rd.`deleted_flg`!='D' AND rd.`child_id`=0 AND rp.`retail_product_status`!='D' AND SUBSTR(period, 1, 4) IN ('%s') GROUP BY `product_id`, SUBSTR(period, 1, 4)
                 UNION ALL
                 SELECT 'child' as type,concat('rc_',`child_id`) unique_id, period, SUBSTR(period, 1, 4) month, `product_id` as category_id, `child_id`, rp.`retail_product_title`, SUM(ifnull(`net_sales`,0)) net_amount
                 FROM `retail_dimensions` rd LEFT JOIN `retail_products` rp ON rd.`child_id`=rp.`retail_product_id` WHERE rd.company_id='%s' AND rd.`deleted_flg`!='D' AND rd.`child_id`!=0 AND rp.`retail_product_status`!='D' AND SUBSTR(period, 1, 4) IN ('%s') GROUP BY `product_id`, `child_id`, SUBSTR(period, 1, 4)", mysqli_real_escape_string($this->db, $company_id), $format_list1, mysqli_real_escape_string($this->db, $company_id), $format_list1, mysqli_real_escape_string($this->db, $company_id), $format_list1, mysqli_real_escape_string($this->db, $company_id), $format_list1);
            
            $misc_query = sprintf("SELECT 'misc' as type,'' as unique_id, period, SUBSTR(period, 1, 4) month, '' as category_id, 0 as child_id, '' as category_title , SUM(ifnull(md.`sales_amount`,0)) net_amount
                 FROM `misc_dimensions` md WHERE md.company_id='%s' AND SUBSTR(md.period, 1, 4) IN ('%s') GROUP BY SUBSTR(md.period, 1, 4)", mysqli_real_escape_string($this->db, $company_id), $format_list1);
        }elseif($sales_period=='M'){
            $sql = sprintf("SELECT 'membership' as type,concat('m_',`category_id`) unique_id, period, SUBSTR(period, 6, 7) month, category_id, 0 as child_id, m.category_title, SUM(ifnull(`sales_amount`,0)) net_amount
                 FROM `membership_dimensions`md LEFT JOIN `membership` m ON md.`category_id`=m.`membership_id` WHERE md.company_id='%s' AND `option_id`=0 AND m.`deleted_flg`!='D' AND period IN ('%s') GROUP BY category_id, `period`
                  UNION ALL
                  SELECT 'trial' as type,concat('t_',td.`trial_id`) unique_id, period, SUBSTR(period, 6, 7) month, td.trial_id as category_id, 0 as child_id, t.trial_title as category_title , SUM(ifnull(`sales_amount`,0)) net_amount
                  FROM `trial_dimensions` td LEFT JOIN `trial` t ON td.`trial_id`=t.`trial_id` WHERE td.company_id='%s' AND period IN ('%s') GROUP BY td.trial_id, td.period
                  UNION ALL
                  SELECT 'retail' as type,concat('r_',`product_id`) unique_id, period, SUBSTR(period, 6, 7) month, `product_id` as category_id, `child_id`, rp.`retail_product_title`, SUM(ifnull(`net_sales`,0)) net_amount
                 FROM `retail_dimensions` rd LEFT JOIN `retail_products` rp ON rd.`product_id`=rp.`retail_product_id` WHERE rd.company_id='%s' AND rd.`deleted_flg`!='D' AND `child_id`=0 AND rp.`retail_product_status`!='D' AND period IN ('%s') GROUP BY `product_id`, `period`
                 UNION ALL
                  SELECT 'child' as type,concat('rc_',`child_id`) unique_id, period, SUBSTR(period, 6, 7) month, `product_id` as category_id, `child_id`, rp.`retail_product_title`, SUM(ifnull(`net_sales`,0)) net_amount
                 FROM `retail_dimensions` rd LEFT JOIN `retail_products` rp ON rd.`child_id`=rp.`retail_product_id` WHERE rd.company_id='%s' AND rd.`deleted_flg`!='D' AND `child_id`!=0 AND rp.`retail_product_status`!='D' AND period IN ('%s') GROUP BY `product_id`, `child_id`, `period`", mysqli_real_escape_string($this->db, $company_id), $format_list1, mysqli_real_escape_string($this->db, $company_id), $format_list1, mysqli_real_escape_string($this->db, $company_id), $format_list1, mysqli_real_escape_string($this->db, $company_id), $format_list1);
            
            $misc_query = sprintf("SELECT 'misc' as type, '' as unique_id, period, SUBSTR(period, 6, 7) month, '' as category_id, 0 as child_id, '' as category_title , SUM(ifnull(md.`sales_amount`,0)) net_amount
                  FROM `misc_dimensions` md WHERE md.company_id='%s' AND period IN ('%s') GROUP BY md.period", mysqli_real_escape_string($this->db, $company_id), $format_list1);
        }
//        log_info("dash   ".$sql);
        $res = mysqli_query($this->db, $sql);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows > 0) {
                $unique_id_arr=[];
                while ($row = mysqli_fetch_assoc($res)) {
                    $temp = $temp2 = [];
                    $temp['month'] = $temp2['month'] = (int) $row['month'];
                    $temp['type'] = $temp2['type'] = $type = $row['type'];
                    $temp['unique_id'] = $row['unique_id'];
                    $temp['category_id'] = $temp2['category_id'] = $category_id = $row['category_id'];
                    $temp['category_title'] = $temp2['category_title'] = $row['category_title'];
                    $temp['net_amount'] = $net_amount = $row['net_amount'];
                    $temp['child_id'] = $row['child_id'];
                    $temp['db'] = 1;
                    $membership_net[] = $temp;
                    if (!empty($category_id) && !in_array($row['unique_id'], $unique_id_arr) ) {
                        $unique_id_arr[] = $row['unique_id'];
                        $child_id_arr[] = $row['child_id'];
                        $membership_id_arr[] = $category_id;
                        $membership_title_arr[] = $row['category_title'];
                        $category_arr[] = $row['type'];
                        if($row['type'] == 'child') {
                            if (!isset($retail_child_array[$category_id])) {
                                $retail_child_array[$category_id] = [];
                            }
                            if (!in_array($row['child_id'], $retail_child_array[$category_id])) {
                                $retail_child_array[$category_id][] = $row['child_id'];
                            }
                        }
                    }
                }
                
                for ($j = 0; $j < 6; $j++) {
                    $m_total_sales = $t_total_sales = $r_total_sales = $rc_total_sales = 0;
                    for ($x = 0; $x < count($unique_id_arr); $x++) {
                        $temp = [];
                        $m_present = 0;
                        $c_id = $membership_id_arr[$x];
                        $u_id = $unique_id_arr[$x];
                        $c_title = $membership_title_arr[$x];
                        $child_id = $child_id_arr[$x];
                        for ($k = 0; $k < count($membership_net); $k++) {
                            if ($month_array1[$j] == $membership_net[$k]['month'] &&  $u_id == $membership_net[$k]['unique_id']) {
                                $membership_new_net[] = $membership_net[$k];
                                if($membership_net[$k]['type'] === 'membership'){
                                    $m_present = 1;
                                    $m_total_sales += $membership_net[$k]['net_amount'];
                                }else if($membership_net[$k]['type'] === 'trial'){
                                    $m_present = 1;
                                   $t_total_sales  += $membership_net[$k]['net_amount'];
                                }else if($membership_net[$k]['type'] === 'retail'){
                                    $m_present = 1;
                                   $r_total_sales  += $membership_net[$k]['net_amount'];
                                }else if($membership_net[$k]['type'] === 'child'){
                                    $m_present = 1;
                                    $rc_total_sales += $membership_net[$k]['net_amount'];
                                }
                            }
                        }
                        if ($m_present == 0) {                            
                            $temp['month'] = $month_array1[$j];
                            $temp['type'] = $category_arr[$x]; 
                            $temp['unique_id'] = $u_id;
                            $temp['category_id'] = $c_id;
                            $temp['child_id'] = $child_id;
                            $temp['category_title'] = $c_title;
                            $temp['net_amount'] = number_format(0, 2);
                            $temp['db'] = 0;
//                            $membership[] = $temp;
                            $membership_new_net[] = $temp;
                        }
                    }
                    $m_total_sales_arr[$j] = $m_total_sales;
                    $t_total_sales_arr[$j] = $t_total_sales;
                    $r_total_sales_arr[$j] = $r_total_sales;
                    $rc_total_sales_arr[$j] = $rc_total_sales; 
                }

                for ($j = count($month_array1) - 1; $j >= 0; $j--) {
                    for ($i = 0; $i < count($membership_new_net); $i++) {
                        if ($membership_new_net[$i]['month'] == $month_array1[$j]) {
                            $membership_new_net[$i]['net_amount'] = number_format($membership_new_net[$i]['net_amount'], 2);
                        }
                    }
                }
                
                $r = $b = $c = 0;
                for ($x = 0; $x < count($unique_id_arr); $x++) {
                    $trial_present = $retail_present = $retail_child_present = 0;
                    $c_id = $membership_id_arr[$x];
                    $u_id = $unique_id_arr[$x];
                    for ($j = 0; $j < 6; $j++) {
                        for ($k = 0; $k < count($membership_new_net); $k++) {
                            if ($membership_new_net[$k]['month'] == $month_array1[$j] && $membership_new_net[$k]['unique_id'] == $u_id) {
                                $title = $membership_new_net[$k]['category_title'];
                                $id = $membership_new_net[$k]['category_id'];
                                if($membership_new_net[$k]['type'] === 'membership'){
                                    $final_ms_sales_array[$x]['name'] = $title;
                                    $final_ms_sales_array[$x]['id'] = $id;
                                    $final_ms_sales_array[$x][$month_name_array[$j]] = $membership_new_net[$k]['net_amount'];
                                }else if($membership_new_net[$k]['type'] === 'trial'){
                                    $trial_present = 1;
                                    $final_ts_sales_array[$r]['name'] = $title;
                                    $final_ts_sales_array[$r]['id'] = $id;
                                    $final_ts_sales_array[$r][$month_name_array[$j]] = $membership_new_net[$k]['net_amount'];
                                }else if($membership_new_net[$k]['type'] === 'retail'){
                                    $retail_present = 1;
                                    $final_rs_sales_array[$b]['name'] = $title;
                                    $final_rs_sales_array[$b]['id'] = $id;
                                    $final_rs_sales_array[$b][$month_name_array[$j]] = $membership_new_net[$k]['net_amount'];
                                }else if($membership_new_net[$k]['type'] === 'child'){
                                    $retail_child_present = 1;
                                    $final_rcs_sales_array[$c]['name'] = $title;
                                    $final_rcs_sales_array[$c]['id'] = $membership_new_net[$k]['child_id'];
                                    $final_rcs_sales_array[$c][$month_name_array[$j]] = $membership_new_net[$k]['net_amount'];
                                }
                            }
                        }
                    }
                    if($trial_present == 1){
                        $r++;
                    }else if($retail_present == 1){
                        $b++;
                    }else if($retail_child_present == 1){
                        $c++;
                    }
                }

                for ($ms = 0; $ms < count($final_ms_sales_array); $ms++) {
                    $check_zero = 0;                    
                    for ($j = 2; $j <= 7; $j++) {                        
                        if ((double) $final_ms_sales_array[$ms][$month_name_array[$j - 2]] > 0) {
                            $check_zero = 1;
                        }
                    }
                    if ($check_zero == 0) {
                        array_splice($final_ms_sales_array, $ms, 1);
                        $ms--;
                    }
                }


                for ($ts = 0; $ts < count($final_ts_sales_array); $ts++) {
                    $check_zero = 0;
                    for ($j = 2; $j <= 7; $j++) {
                        if ((double) $final_ts_sales_array[$ts][$month_name_array[$j - 2]] > 0) {
                            $check_zero = 1;
                        }
                    }
                    if ($check_zero == 0) {
                        array_splice($final_ts_sales_array, $ts, 1);
                        $ts--;
                    }
                }
                
                for ($rs = 0; $rs < count($final_rs_sales_array); $rs++) {
                    $check_zero = 0;
                    for ($j = 2; $j <= 7; $j++) {
                        if ((double) $final_rs_sales_array[$rs][$month_name_array[$j - 2]] > 0) {
                            $check_zero = 1;
                        }
                    }
                    if ($check_zero == 0) {
                        array_splice($final_rs_sales_array, $rs, 1);
                        $rs--;
                    }
                }
                
                for ($rs = 0; $rs < count($final_rcs_sales_array); $rs++) {
                    $check_zero = 0;
                    for ($j = 2; $j <= 7; $j++) {
                        if ((double) $final_rcs_sales_array[$rs][$month_name_array[$j - 2]] > 0) {
                            $check_zero = 1;
                        }
                    }
                    if ($check_zero == 0) {
                        array_splice($final_rcs_sales_array, $rs, 1);
                        $rs--;
                    }
                }
            }
        }   
        
        //miscellenious charges
        $misc_res = mysqli_query($this->db, $misc_query);
        if (!$misc_res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$misc_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $misc_net = [];
            if (mysqli_num_rows($misc_res) > 0) {
                while ($misc_row = mysqli_fetch_assoc($misc_res)) {
                    $misc_net[] = $misc_row;
                }
            }

            for ($misc = 0; $misc < 6; $misc++) {
                for($mi = 0; $mi < count($misc_net); $mi++){
                    if ($month_array1[$misc] == $misc_net[$mi]['month']) {
                        $misc_total_sales_arr[$misc] += $misc_net[$mi]['net_amount'];
                    }
                }
            }
        }


        //members
        $membership_id_arr1=$membership_title_arr1=[];

        $sql1 = sprintf("SELECT period, SUBSTR(period, 6, 7) month, category_id, category_title, SUM(active_members) active, SUM(reg_new) as new, SUM(reg_hold) as pause, SUM(reg_cancelled) as cancel ,SUM(reg_completed) as completed,IF(SUM(IFNULL(reg_resumed_from_hold,0))>0,reg_resumed_from_hold,'-') as resumed_from_hold
                FROM `membership_dimensions` WHERE company_id='%s' AND `option_id`=0 AND period IN ('%s') GROUP BY category_id, `period`", mysqli_real_escape_string($this->db, $company_id), $format_list);
        $res1 = mysqli_query($this->db, $sql1);
        if(!$res1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($res1);
            if($num_of_rows>0){
                while($row= mysqli_fetch_assoc($res1)){
                    $temp = $temp2 = [];
                    $temp['month'] = $temp2['month'] = (int)$row['month'];
                    $temp['category_id'] = $temp2['category_id'] = $category_id1 = $row['category_id'];
                    $temp['category_title'] = $temp2['category_title'] = $row['category_title'];
//                    $temp['net_amount'] = $net_amount = $row['net_amount'];
                    $temp['active'] = $temp2['active'] = $row['active'];
                    $temp['new'] = $temp2['new'] = $row['new'];
                    $temp['pause'] = $temp2['pause'] = $row['pause'];
                    $temp['resumed_from_hold'] = $temp2['resumed_from_hold'] = $row['resumed_from_hold'];
                    $temp['cancel'] = $temp2['cancel'] = $row['cancel'];
                    $temp['completed']=$temp2['completed']=$row['completed'];
//                    $temp['cancel/hold'] = $row['pause']+$row['cancel'];
                    $temp['db'] = 1;
                    $membership[] = $temp;
                    if(!empty($category_id1) && !in_array($category_id1, $membership_id_arr1)){
                        $membership_id_arr1[] = $category_id1;
                        $membership_title_arr1[] = $row['category_title'];
                    }
                }
               
                for($j=0;$j<6;$j++){

                    $m_total_sales = $active = $new = $pause = $cancel = $net_gain = $completed = $resumed_from_hold =0;
                    for($x=0;$x<count($membership_id_arr1);$x++){
                        $temp=[];
                        $m_present = 0;
                        $c_id = $membership_id_arr1[$x];
                        $c_title = $membership_title_arr1[$x];
                        for($k=0;$k<count($membership);$k++){
                            if($month_array[$j]==$membership[$k]['month'] && (int)$c_id==$membership[$k]['category_id']){
                                $m_present = 1;
                                $membership_new[] = $membership[$k];
//                                $m_total_sales += $membership[$k]['net_amount'];
                                $active += $membership[$k]['active'];
                                $new += $membership[$k]['new'];
                                $pause += $membership[$k]['pause'];
                                if($membership[$k]['resumed_from_hold']!='-'){
                                $resumed_from_hold += $membership[$k]['resumed_from_hold'];
                                }
                                $cancel += $membership[$k]['cancel'];
                                $completed +=$membership[$k]['completed'];
                            }
                        }
                        if($m_present==0){
                            $temp['month'] = $month_array[$j];
                            $temp['category_id'] = $c_id;
                            $temp['category_title'] = $c_title;
//                            $temp['net_amount'] = number_format(0, 2);
                            $temp['active'] = 0;
                            $temp['new'] = 0;
                            $temp['pause'] = 0;
                            $temp['resumed_from_hold'] = '-';
                            $temp['cancel'] = 0;
                            $temp['completed'] = 0;
//                            $temp['cancel/hold'] = 0;
                            $temp['db'] = 0;
//                            $membership[] = $temp;
                            $membership_new[] = $temp;
                        }
                    }
//                    $m_total_sales_arr[$j] = $m_total_sales;
                    $m_total_active_members_arr[$j] = $active;
                    $m_total_new_members[$j] = $new;
                    $m_total_pause_members[$j] = $pause;
                    if($resumed_from_hold>0){
                        $m_total_pause_resume_members[$j] = $resumed_from_hold;
                    }
                    $m_total_cancel_members[$j] = $cancel;
                    $m_total_completed_members[$j] = $completed;                    
                    $net_gain_arr[$j] = ($new+$resumed_from_hold)-($pause+$cancel+$completed);
                }
                
                for ($j = count($month_array)-1; $j >= 0; $j--) {
                    $active = 0;
                    for ($i = 0; $i < count($membership_new); $i++) {
                        if($membership_new[$i]['month']==$month_array[$j]){
//                            $membership_new[$i]['net_amount'] = number_format($membership_new[$i]['net_amount'], 2);
                            if (($membership_new[$i]['active'] == 0 && $membership_new[$i]['db'] == 0 && $j!=count($month_array)-1) ||($membership_new[$i]['active'] < 0 && $j!=count($month_array)-1) ) {
                                for ($k = $i; $k < count($membership_new); $k++) {
                                    if (($membership_new[$i]['category_id'] == $membership_new[$k]['category_id']) && $membership_new[$k]['month'] ==$month_array[$j+1]) {
                                        $membership_new[$i]['active'] = $membership_new[$k]['active'];
                                        $active += $membership_new[$k]['active'];
                                    }
                                }
                            } else {
                                $active += $membership_new[$i]['active'];
                            }
                        }
                    }
                    $m_total_active_members_arr[$j] = $active;
                }                
                for($x=0;$x<count($membership_id_arr1);$x++){
                    $c_id = $membership_id_arr1[$x];
                    for($j=0;$j<6;$j++){
                        for($k=0;$k<count($membership_new);$k++){
                            if($membership_new[$k]['month']==$month_array[$j] && $membership_new[$k]['category_id']==$c_id){
                                $title = $membership_new[$k]['category_title'];
                                $id = $membership_new[$k]['category_id'];
                                $final_active_members_array[$x]['name'] = $final_new_members_array[$x]['name']  = $final_pc_members_array[$x]['name']=$final_rc_members_array[$x]['name']=$final_can_members_array[$x]['name'] = $title;
                                $final_active_members_array[$x]['id'] = $final_new_members_array[$x]['id']  = $final_pc_members_array[$x]['id'] =$final_rc_members_array[$x]['id']=$final_can_members_array[$x]['id']= $id;
                                
//                                $final_ms_sales_array[$x][$month_name_array[$j]] = $membership_new[$k]['net_amount'];
                                
                                $final_active_members_array[$x][$month_name_array[$j]] = $membership_new[$k]['active'];
                                
                                $final_new_members_array[$x][$month_name_array[$j]] = $membership_new[$k]['new'];
                                
                                $final_pc_members_array[$x][$month_name_array[$j]] = $membership_new[$k]['pause'];
                                
                                $final_rc_members_array[$x][$month_name_array[$j]] = $membership_new[$k]['resumed_from_hold'];
                                
                                $final_can_members_array[$x][$month_name_array[$j]] =$membership_new[$k]['cancel']+$membership_new[$k]['completed'];
                            }                            
                        }
                    }
                }
                
                for ($ms1 = 0; $ms1 < count($final_active_members_array); $ms1++) {
                    $check_zero = 0;                    
                    for ($j1 = 2; $j1 <= 7; $j1++) {                        
                        if ((double) $final_active_members_array[$ms1][$month_name_array[$j1 - 2]] > 0) {
                            $check_zero = 1;
                        }
                    }
                    if ($check_zero == 0) {
                        array_splice($final_active_members_array, $ms1, 1);
                        $ms1--;
                    }
                }
                
                
                for ($ms2 = 0; $ms2 < count($final_new_members_array); $ms2++) {
                    $check_zero = 0;                    
                    for ($j2 = 2; $j2 <= 7; $j2++) {                        
                        if ((double) $final_new_members_array[$ms2][$month_name_array[$j2 - 2]] > 0) {
                            $check_zero = 1;
                        }
                    }
                    if ($check_zero == 0) {
                        array_splice($final_new_members_array, $ms2, 1);
                        $ms2--;
                    }
                }
                
                for ($ms3 = 0; $ms3 < count($final_pc_members_array); $ms3++) {
                    $check_zero = 0;                    
                    for ($j3 = 2; $j3 <= 7; $j3++) {                        
                        if ((double) $final_pc_members_array[$ms3][$month_name_array[$j3 - 2]] > 0) {
                            $check_zero = 1;
                        }
                    }
                    if ($check_zero == 0) {
                        array_splice($final_pc_members_array, $ms3, 1);
                        $ms3--;
                    }
                }
                
                
                for ($ms4 = 0; $ms4 < count($final_can_members_array); $ms4++) {
                    $check_zero = 0;                    
                    for ($j4 = 2; $j4 <= 7; $j4++) {                        
                        if ((double) $final_can_members_array[$ms4][$month_name_array[$j4 - 2]] > 0) {
                            $check_zero = 1;
                        }
                    }
                    if ($check_zero == 0) {
                        array_splice($final_can_members_array, $ms4, 1);
                        $ms4--;
                    }
                }
                
                for ($ms5 = 0; $ms5 < count($final_rc_members_array); $ms5++) {
                    $check_zero = 0;                    
                    for ($j5 = 2; $j5 <= 7; $j5++) {                        
                        if ((double) $final_rc_members_array[$ms5][$month_name_array[$j5 - 2]] > 0) {
                            $check_zero = 1;
                        }
                    }
                    if ($check_zero == 0) {
                        array_splice($final_rc_members_array, $ms5, 1);
                        $ms5--;
                    }
                }
            }
        } 
        
        $months = array (1=>'Jan',2=>'Feb',3=>'Mar',4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec');
        $sql2 = sprintf("SELECT *, month(($currdate_add)) curr_month, month(DATE_FORMAT(($currdate_add), '%s') - INTERVAL 3 MONTH) last_month FROM (
                SELECT month($mr_last_upd_date) as month, 'Membership' as Category, 'hold'  as status, sum(1) net_amount FROM  membership_registration mr 
                WHERE  mr.company_id = '%s' AND mr.membership_status='P' GROUP BY  month($mr_last_upd_date)
                UNION
                SELECT  '' as month, 'Failed' as Category, 'Failed'  as status, sum(net_amount) net_amount FROM (
                    SELECT sum(if(mr.processing_fee_type=2 ,(mp.payment_amount+mp.processing_fee),mp.payment_amount)) net_amount, 'M' category, mp.membership_registration_id reg_id, mp.membership_payment_id payment_id, mp.last_updt_dt
                    FROM membership_registration mr  LEFT JOIN membership_payment mp ON mp.membership_registration_id = mr.membership_registration_id
                    WHERE mr.company_id = '%s'  AND membership_status IN ('R', 'P', 'S', 'C') AND (mp.payment_status = 'F' OR (mp.payment_status='N' AND mp.payment_date<($currdate_add)))
                    UNION
                    SELECT sum( if(er.processing_fee_type=2 ,ep.payment_amount+ep.processing_fee,ep.payment_amount)) net_amount, 'E' category, ep.event_reg_id reg_id, ep.event_payment_id payment_id, ep.last_updt_dt
                    FROM  event_payment ep LEFT JOIN  event_registration er ON ep.event_reg_id = er.event_reg_id LEFT JOIN event e ON er.event_id = e.event_id 
                    WHERE  er.company_id = '%s'  AND (ep.schedule_status = 'F' OR (ep.schedule_status='N' AND ep.schedule_date<($currdate_add)))
                )t2
                UNION
                SELECT  '' as month, 'upcoming' as Category, 'upcoming'  as status, sum(net_amount) net_amount FROM (
                    SELECT sum(if(mr.processing_fee_type=1 ,(mp.payment_amount),(mp.payment_amount+mp.processing_fee))) net_amount, 'M' category, mp.membership_registration_id reg_id, mp.membership_payment_id payment_id, mp.last_updt_dt
                    FROM membership_registration mr  LEFT JOIN membership_payment mp ON mp.membership_registration_id = mr.membership_registration_id
                    WHERE mr.company_id = '%s'  AND (mr.membership_status='R' OR (mr.membership_status='P' AND mr.`resume_date`>=($currdate_add) AND mr.`resume_date` NOT IN (NULL, '0000-00-00'))) AND (mp.payment_status='N' AND mp.payment_date>($currdate_add) and mp.payment_date<(($currdate_add) + interval 1 month))
                       
                    UNION
                    SELECT sum( if(er.processing_fee_type=1 ,ep.payment_amount,ep.payment_amount+ep.processing_fee)) net_amount, 'E' category, ep.event_reg_id reg_id, ep.event_payment_id payment_id, ep.last_updt_dt
                    FROM  event_payment ep LEFT JOIN  event_registration er ON ep.event_reg_id = er.event_reg_id LEFT JOIN event e ON er.event_id = e.event_id 
                    WHERE  er.company_id = '%s'  AND (ep.schedule_status='N' AND ep.schedule_date>($currdate_add) and ep.schedule_date<(($currdate_add) + interval 1 month) and  ep.payment_amount>'0' )
                )t3
            )t1 order by status,category,month asc", $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
        $res2 = mysqli_query($this->db, $sql2);
        if(!$res2){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{            
            $num_rows = mysqli_num_rows($res2);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($res2)){
                    $curr_month = $row['curr_month'];
                    $last_month = $row['last_month'];
                    $net_amount = $row['net_amount'];
                    $month = $row['month'];
                    if($row['status']=='Failed'){
                        $failed_total = $net_amount;
                    }
                    if($row['Category']=='Event'){
                        $event_total += $net_amount;
//                        $row['month_name'] = $months[$month];
                        $event[] = $row;
                    }
                    if($row['Category']=='Membership' && $row['status']=='hold'){
                        $current_hold += $net_amount;
                    }
                    if($row['status']=='upcoming'){
                        $upcoming_total = $net_amount;
                    }
                }
            }
        }
        
        $ret = $ret_new = $final_ret = $category = $category_title = [];
        $final_retention_array_value = [];
        $final_retention_arr = [0, 0, 0, 0, 0, 0];
                
//        $sql3 = sprintf("SELECT period, category_id, category_title, reg_cancelled FROM `membership_dimensions` 
//                WHERE `company_id`='%s' AND `option_id`=0 AND `reg_cancelled`>0 AND ($currdate_add)>=DATE_FORMAT(($currdate_add), '%s') - INTERVAL 12 MONTH 
//                GROUP BY `category_id`, `period`", mysqli_real_escape_string($this->db, $company_id), $date_str);
//        $res3 = mysqli_query($this->db, $sql3);
//        if(!$res3){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//        }else{            
//            $num_rows = mysqli_num_rows($res3);
//            if($num_rows>0){
//                for($i=0;$i<13;$i++){
//                    $format_array2[] = date('Y-m', strtotime("first day of -$i month", strtotime($curr_date)));
//                    $month_array2[] = date('n', strtotime("first day of -$i month", strtotime($curr_date)));
//                    $month_name2[] = date('M-y', strtotime("first day of -$i month", strtotime($curr_date)));
//                }
//                while($row = mysqli_fetch_assoc($res3)){
//                    $cat_id = $row['category_id'];
//                    $cat_title = $row['category_title'];
//                    $ret[] = $row;
//                    if(!empty($cat_id) && !in_array($cat_id, $category)){
//                        $category[] = $cat_id;
//                    }
//                    if(!empty($cat_title) && !in_array($cat_title, $category_title)){
//                        $category_title[] = $cat_title;
//                    }
//                }
//                
//                
//                for($j=0;$j<13;$j++){
//                    $temp_ret = 0;
//                    for($l=0;$l<count($category);$l++){
//                        $temp=[];
//                        $c_id = $category[$l];
//                        $c_title = $category_title[$l];
//                        $present = 0;
//                        for($k=0;$k<count($ret);$k++){
//                            if($format_array2[$j]==$ret[$k]['period'] && $c_id==$ret[$k]['category_id']){
//                                $present = 1;
//                                $temp_ret += $ret[$k]['reg_cancelled'];
//                                $ret_new[] = $ret[$k];
//                            }
//                        }
//                        if($present==0){
//                            $temp['category_id'] = $c_id;
//                            $temp['category_title'] = $c_title;
//                            $temp['period'] = $format_array2[$j];
//                            $temp['reg_cancelled'] = 0;
//                            $ret_new[] = $temp;
//                        }
//                    }
//                    $final_ret[$j] = $temp_ret;
//                    if($j<4){
//                        $final_retention_arr[0] += $temp_ret;
//                    }elseif($j>=4&&$j<7){
//                        $final_retention_arr[1] += $temp_ret;
//                    }elseif($j>=7&&$j<9){
//                        $final_retention_arr[2] += $temp_ret;
//                    }else{
//                        $final_retention_arr[3] += $temp_ret;
//                    }
//                }
//                
//                for($j=0;$j<count($category);$j++){
//                    $cancelled=0;
//                    for($k=0;$k<count($ret_new);$k++){
//                        for($i=0;$i<13;$i++){                            
//                            if($ret_new[$k]['category_id']==$category[$j] && $ret_new[$k]['period']==$format_array2[$i]){
//                                $temp=[];
//                                $temp['category_id'] = $ret_new[$k]['category_id'];
//                                $temp['category_title'] = $ret_new[$k]['category_title'];
//                                if($i<3){
//                                    $cancelled += $ret_new[$k]['reg_cancelled'];
//                                }elseif($i==3){
//                                    $cancelled += $ret_new[$k]['reg_cancelled'];
//                                    $temp['reg_cancelled'] = $cancelled;
//                                    $final_retention_array_value[$j][0] = $temp;
//                                    $cancelled=0;
//                                }elseif($i<6){
//                                    $cancelled += $ret_new[$k]['reg_cancelled'];
//                                }elseif($i==6){
//                                    $cancelled += $ret_new[$k]['reg_cancelled'];
//                                    $temp['reg_cancelled'] = $cancelled;
//                                    $final_retention_array_value[$j][1] = $temp;
//                                    $cancelled=0;
//                                }elseif($i<9){
//                                    $cancelled += $ret_new[$k]['reg_cancelled'];
//                                }elseif($i==9){
//                                    $cancelled += $ret_new[$k]['reg_cancelled'];
//                                    $temp['reg_cancelled'] = $cancelled;
//                                    $final_retention_array_value[$j][2] = $temp;
//                                    $cancelled=0;
//                                }elseif($i<12){
//                                    $cancelled += $ret_new[$k]['reg_cancelled'];
//                                }elseif($i==12){
//                                    $cancelled += $ret_new[$k]['reg_cancelled'];
//                                    $temp['reg_cancelled'] = $cancelled;
//                                    $final_retention_array_value[$j][3] = $temp;
//                                    $cancelled=0;
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
        
       
        $category_title = [];
        $sql3 = sprintf("SELECT ceil(DATEDIFF(`cancelled_date`,`created_dt`)/30) as mnths,`membership_category_title`
          ,`membership_id`,`membership_option_id` from membership_registration where `company_id`='%s' and `cancelled_date` is not null and 
          cancelled_date !='0000-00-00' and `membership_status` IN ('C','S')", mysqli_real_escape_string($this->db, $company_id));
        $result3 = mysqli_query($this->db, $sql3);
        if (!$result3) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $nums_rows3 = mysqli_num_rows($result3);
            if ($nums_rows3 > 0) {
                while ($rows3 = mysqli_fetch_assoc($result3)) {
//                    $ret['mnths']=$rows3['mnths'];
                    $membership_category_title = $rows3['membership_category_title'];
                    $membership_id2 = $rows3['membership_id'];
//                    $ret['membership_option_id']=$rows3['membership_option_id'];
                    $ret[] = $rows3;
                    if (!empty($membership_id2) && !in_array($membership_id2, $category)) {
                        $category[] = $membership_id2;
//                    }
//                    if (!empty($membership_category_title) && !in_array($membership_category_title, $category_title)) {
                        $category_title[] = $membership_category_title;
                    }
                }

                $ret_month_num=[0,4,7,10,13,16,19,22,25,28,31,34,36];
//                for($m=0; $m<4; $m++){
                    for ($i = 0; $i < count($category); $i++) {
                        $m_id = $category[$i];
//                        $final_retention_array_value[$i]['category_id'] = $category[$i];
                        $final_retention_array_value[$i]['name'] = $category_title[$i];
                        for ($j = 0; $j < count($ret); $j++) {
                            if ($m_id == $ret[$j]['membership_id']) {
//                                if ($rentation_flag == 0) {
                                for($z=$rentation_flag;$z<$rentation_flag+1;$z++){
                                     if ($ret[$j]['mnths'] >= $ret_month_num[$z]  && $ret[$j]['mnths'] < $ret_month_num[$z+1]) {
                                        if (isset($final_retention_array_value[$i]['first_month'])) {
                                            $final_retention_array_value[$i] ['first_month'] += 1;
                                        } else {
                                            $final_retention_array_value[$i]['first_month'] = 1;
                                        }
//                                        $ret_array_by_month['total'][0] = $final_retention_array_value[$i][0]['reg_cancelled'];
                                    }
                                    
                                    if ($ret[$j]['mnths'] >= $ret_month_num[$z+1] && $ret[$j]['mnths'] < $ret_month_num[$z+2]) {
                                        if (isset($final_retention_array_value[$i]['second_month'])) {
                                            $final_retention_array_value[$i]['second_month'] += 1;
                                        } else {
                                            $final_retention_array_value[$i]['second_month'] = 1;
                                        }
//                                        $ret_array_by_month['total'][1] = $final_retention_array_value[$i][1]['reg_cancelled'];
                                    }
                                    
                                    if ($ret[$j]['mnths'] >= $ret_month_num[$z+2] && $ret[$j]['mnths'] < $ret_month_num[$z+3]) {
                                        if (isset($final_retention_array_value[$i]['third_month'])) {
                                            $final_retention_array_value[$i]['third_month'] += 1;
                                        } else {
                                            $final_retention_array_value[$i]['third_month'] = 1;
                                        }
//                                        $ret_array_by_month['total'][2] = $final_retention_array_value[$i][2]['reg_cancelled'];
                                    }
                                    
                                    if ($ret[$j]['mnths'] >= $ret_month_num[$z+3] && $ret[$j]['mnths'] < $ret_month_num[$z+4]) {
                                        if (isset($final_retention_array_value[$i]['fourth_month'])) {
                                            $final_retention_array_value[$i]['fourth_month'] += 1;
                                        } else {
                                            $final_retention_array_value[$i]['fourth_month'] = 1;
                                        }
//                                        $ret_array_by_month['total'][3] = $final_retention_array_value[$i][3]['reg_cancelled'];
                                    }
                                     if ($ret[$j]['mnths'] >= $ret_month_num[$z+4] && $ret[$j]['mnths'] < $ret_month_num[$z+5]) {
                                        if (isset($final_retention_array_value[$i]['fifth_month'])) {
                                            $final_retention_array_value[$i]['fifth_month'] += 1;
                                        } else {
                                            $final_retention_array_value[$i]['fifth_month'] = 1;
                                        }
//                                        $ret_array_by_month['total'][3] = $final_retention_array_value[$i][3]['reg_cancelled'];
                                    }
                                     if ($ret[$j]['mnths'] >= $ret_month_num[$z+5] && $ret[$j]['mnths'] < $ret_month_num[$z+6]) {
                                        if (isset($final_retention_array_value[$i]['sixth_month'])) {
                                            $final_retention_array_value[$i]['sixth_month'] += 1;
                                        } else {
                                            $final_retention_array_value[$i]['sixth_month'] = 1;
                                        }
//                                        $ret_array_by_month['total'][3] = $final_retention_array_value[$i][3]['reg_cancelled'];
                                    }
                                }
                                   
//                                }
//                                if ($rentation_flag == 1) {
//                                    if ($ret[$j]['mnths'] > 12 && $ret[$j]['mnths'] <= 15) {
//                                        if (isset($final_retention_array_value[$i]['first_month'])) {
//                                            $final_retention_array_value[$i]['first_month'] += 1;
//                                        } else {
//                                            $final_retention_array_value[$i]['first_month'] = 1;
//                                        }
////                                        $ret_array_by_month['total'][0] = $final_retention_array_value[$i][0]['reg_cancelled'];
//                                    }
//                                    if ($ret[$j]['mnths'] > 15 && $ret[$j]['mnths'] <= 18) {
//                                        if (isset($final_retention_array_value[$i]['second_month'])) {
//                                            $final_retention_array_value[$i]['second_month'] += 1;
//                                        } else {
//                                            $final_retention_array_value[$i]['second_month'] = 1;
//                                        }
////                                        $ret_array_by_month['total'][1] = $final_retention_array_value[$i][1]['reg_cancelled'];
//                                    }
//                                    if ($ret[$j]['mnths'] > 18 && $ret[$j]['mnths'] <= 21) {
//                                        if (isset($final_retention_array_value[$i]['third_month'])) {
//                                            $final_retention_array_value[$i]['third_month'] += 1;
//                                        } else {
//                                            $final_retention_array_value[$i]['third_month'] = 1;
//                                        }
////                                        $ret_array_by_month['total'][2] = $final_retention_array_value[$i][2]['reg_cancelled'];
//                                    }
//                                    if ($ret[$j]['mnths'] > 21 && $ret[$j]['mnths'] <= 24) {
//                                        if (isset($final_retention_array_value[$i]['fourth_month'])) {
//                                            $final_retention_array_value[$i]['fourth_month'] += 1;
//                                        } else {
//                                            $final_retention_array_value[$i]['fourth_month'] = 1;
//                                        }
////                                        $ret_array_by_month['total'][3] = $final_retention_array_value[$i][3]['reg_cancelled'];
//                                    }
//                                }
//                                if ($rentation_flag == 2) {
//                                    if ($ret[$j]['mnths'] > 24 && $ret[$j]['mnths'] <= 27) {
//                                        if (isset($final_retention_array_value[$i]['first_month'])) {
//                                            $final_retention_array_value[$i]['first_month']+= 1;
//                                        } else {
//                                            $final_retention_array_value[$i]['first_month'] = 1;
//                                        }
////                                        $ret_array_by_month['total'][0] = $final_retention_array_value[$i][0]['reg_cancelled'];
//                                    }
//                                    if ($ret[$j]['mnths'] > 27 && $ret[$j]['mnths'] <= 30) {
//                                        if (isset($final_retention_array_value[$i][1]['reg_cancelled'])) {
//                                            $final_retention_array_value[$i]['second_month'] += 1;
//                                        } else {
//                                            $final_retention_array_value[$i]['second_month'] = 1;
//                                        }
////                                        $ret_array_by_month['total'][1] = $final_retention_array_value[$i][1]['reg_cancelled'];
//                                    }
//                                    if ($ret[$j]['mnths'] > 30 && $ret[$j]['mnths'] <= 33) {
//                                        if (isset($final_retention_array_value[$i]['third_month'])) {
//                                            $final_retention_array_value[$i]['third_month'] += 1;
//                                        } else {
//                                            $final_retention_array_value[$i]['third_month'] = 1;
//                                        }
////                                        $ret_array_by_month['total'][2] = $final_retention_array_value[$i][2]['reg_cancelled'];
//                                    }
//                                    if ($ret[$j]['mnths'] > 33 && $ret[$j]['mnths'] <= 36) {
//                                        if (isset($final_retention_array_value[$i]['fourth_month'])) {
//                                            $final_retention_array_value[$i]['fourth_month'] += 1;
//                                        } else {
//                                            $final_retention_array_value[$i]['fourth_month'] = 1;
//                                        }
////                                        $ret_array_by_month['total'][3] = $final_retention_array_value[$i][3]['reg_cancelled'];
//                                    }
//                                     if ($ret[$j]['mnths'] > 36) {
//                                        if (isset($final_retention_array_value[$i]['fourth_month'])) {
//                                            $final_retention_array_value[$i]['fourth_month'] += 1;
//                                        } else {
//                                            $final_retention_array_value[$i]['fourth_month'] = 1;
//                                        }
////                                        $ret_array_by_month['total'][3] = $final_retention_array_value[$i][3]['reg_cancelled'];
//                                    }
//                                }
                            }
                        }
                    }
                    
            }
        }
       
        for($x=0;$x<count($final_retention_array_value);$x++){
            $a=$final_retention_array_value[$x];
           if (!array_key_exists("first_month",$a)) {
               $final_retention_array_value[$x]['first_month']=0;
            } 
            if (!array_key_exists("second_month",$a)) {
               $final_retention_array_value[$x]['second_month']=0;
            } 
            if (!array_key_exists("third_month",$a)) {
               $final_retention_array_value[$x]['third_month']=0;
            } 
            if (!array_key_exists("fourth_month",$a)) {
               $final_retention_array_value[$x]['fourth_month']=0;
            } 
            if (!array_key_exists("fifth_month",$a)) {
               $final_retention_array_value[$x]['fifth_month']=0;
            } 
            if (!array_key_exists("sixth_month",$a)) {
               $final_retention_array_value[$x]['sixth_month']=0;
            } 
        }
        
        for ($rt = 0; $rt < count($final_retention_array_value); $rt++) {
                    $check_zero = 0;                    
                    for ($j = 1; $j <= 6; $j++) {                        
                        if ((double) $final_retention_array_value[$rt][$month_name_array[$j - 1]] > 0) {
                            $check_zero = 1;
                        }
                    }
                    if ($check_zero == 0) {
                        array_splice($final_retention_array_value, $rt, 1);
                        $rt--;
                    }
                }        
      
        $final_retention_array = [];
        $final_retention_array_total =$final_retention_array_total1=$final_retention_array_total2=$final_retention_array_total3=$final_retention_array_total4=$final_retention_array_total5 =0;
        for ($m = 0; $m < count($final_retention_array_value); $m++) {
            $final_retention_array_total += $final_retention_array_value[$m]['first_month'];
            $final_retention_array_total1 +=$final_retention_array_value[$m]['second_month'];
            $final_retention_array_total2 += $final_retention_array_value[$m]['third_month'];
            $final_retention_array_total3 +=$final_retention_array_value[$m]['fourth_month'];
            $final_retention_array_total4 +=$final_retention_array_value[$m]['fifth_month'];
            $final_retention_array_total5 +=$final_retention_array_value[$m]['sixth_month'];
        }
        $final_retention_array[] = $final_retention_array_total;
        $final_retention_array[] = $final_retention_array_total1;
        $final_retention_array[] = $final_retention_array_total2;
        $final_retention_array[] = $final_retention_array_total3;
         $final_retention_array[] = $final_retention_array_total4;
          $final_retention_array[] = $final_retention_array_total5;

       
//        $month_rent=[];
//        if($rentation_flag==1){
//            $m_flag=4;
//        }if($rentation_flag==2){
//            $m_flag=8;
//        }if($rentation_flag==0){
//            $m_flag=0;
//        }
        $mnth_rent=["0-3","4-6","7-9","10-12","13-15","16-18","19-21","22-24","25-27","28-30","31-33","33-36","36+"];
        for($x=$rentation_flag;$x<$rentation_flag+6;$x++){
            $month_rent[]=$mnth_rent[$x];
        }
        $processing_fee_temp="if(ep.`schedule_status`='PR'||ep.`schedule_status`='CR',(SELECT SUM(`processing_fee`) FROM `event_payment` WHERE `event_reg_id`=ep.`event_reg_id` AND `checkout_id`=ep.`checkout_id` AND `schedule_status` IN ('R','PR')),ep.`processing_fee`)";
        if($sales_period=="A"){
             $year_str = "%Y";
           $sql4 = sprintf("select e.company_id, year(concat(yr,'-01-01')) as month, yr, e.event_id, e.event_type, ifnull(amount,0) net_amount, e.event_title from
                event e left outer join  
                (
                    SELECT DATE_FORMAT( @d:=(@d - interval 1 year),'%s')  yr
                    FROM (SELECT @d:=date('%s')) r
                    CROSS JOIN information_schema.tables t 
                    WHERE @d > DATE_FORMAT('%s' - INTERVAL 5 year , '%s')
                ) m1 on 1=1
                left outer join
                (
                    SELECT  DATE_FORMAT($ep_last_upd_date,'%s') as year, er.company_id, er.event_id, 
                    sum( 
                        IF(ep.schedule_status = 'R',
                            if(er.processing_fee_type=1, ep.payment_amount-ep.`processing_fee`,ep.payment_amount)*-1,
                            if(er.processing_fee_type=1, ep.payment_amount-($processing_fee_temp),ep.payment_amount)                    
                        )
                    ) amount 
                    FROM `event_registration` er LEFT JOIN `event_payment` ep  ON er.`event_reg_id` = ep.`event_reg_id` 
                    WHERE if(('%s'<=CURRENT_DATE),ep.schedule_status IN ('S','R','FR','PR','CR','M','MR','MF','MP'),ep.schedule_status IN ('N')) AND ep.`credit_method`!='MC'
                    GROUP BY er.company_id, er.`event_id`,  DATE_FORMAT($ep_last_upd_date,'%s')
                ) s
                on e.company_id=s.company_id and e.event_id = s.event_id and yr=s.year
                where e.`event_type` IN ('S', 'M') AND e.`company_id`='%s' and yr between DATE_FORMAT('%s' - INTERVAL 5 year, '%s') and DATE_FORMAT('%s','%s')",
                mysqli_real_escape_string($this->db, $year_str), mysqli_real_escape_string($this->db, $curr_datepulsoneyear), mysqli_real_escape_string($this->db, $curr_date1), mysqli_real_escape_string($this->db, $date_str), 
                mysqli_real_escape_string($this->db, $year_str), mysqli_real_escape_string($this->db, $curr_date1), mysqli_real_escape_string($this->db, $year_str), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_date1), 
                mysqli_real_escape_string($this->db, $year_str), mysqli_real_escape_string($this->db, $curr_date1), mysqli_real_escape_string($this->db, $year_str));
        }elseif($sales_period=="M"){
            $month_str = "%Y-%m";
            $sql4 = sprintf("select e.company_id, month(concat(mnth,'-01')) as month, mnth, e.event_id, e.event_type, ifnull(amount,0) net_amount, e.event_title from
                event e left outer join  
                (
                    SELECT DATE_FORMAT( @d:=(@d - interval 1 month),'%s')  mnth
                    FROM (SELECT @d:=date('%s')) r
                    CROSS JOIN information_schema.tables t 
                    WHERE @d > DATE_FORMAT('%s' - INTERVAL 5 month , '%s')
                ) m1 on 1=1
                left outer join
                (
                    SELECT  DATE_FORMAT($ep_last_upd_date,'%s') as month, er.company_id, er.event_id, 
                    sum( 
                        IF(ep.schedule_status = 'R',
                            if(er.processing_fee_type=1, ep.payment_amount-ep.`processing_fee`,ep.payment_amount)*-1,
                            if(er.processing_fee_type=1, ep.payment_amount-($processing_fee_temp),ep.payment_amount)
                        )
                    ) amount 
                    FROM `event_registration` er LEFT JOIN `event_payment` ep  ON er.`event_reg_id` = ep.`event_reg_id` 
                    WHERE if(('%s'<=CURRENT_DATE),ep.schedule_status IN ('S','R','FR','PR','CR','M','MR','MF','MP'),ep.schedule_status IN ('N')) AND ep.`credit_method`!='MC'
                    GROUP BY er.company_id, er.`event_id`,  DATE_FORMAT($ep_last_upd_date,'%s')
                ) s
                on e.company_id=s.company_id and e.event_id = s.event_id and mnth=s.month
                where e.`event_type` IN ('S', 'M') AND e.company_id='%s' and mnth between DATE_FORMAT('%s' - INTERVAL 5 month, '%s') and DATE_FORMAT('%s','%s')", mysqli_real_escape_string($this->db, $month_str), mysqli_real_escape_string($this->db, $curr_datepulsoneyear),
                    mysqli_real_escape_string($this->db, $curr_date1), mysqli_real_escape_string($this->db, $date_str), mysqli_real_escape_string($this->db, $month_str), mysqli_real_escape_string($this->db, $curr_date1),
                    mysqli_real_escape_string($this->db, $month_str), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_date1), mysqli_real_escape_string($this->db, $month_str), 
                    mysqli_real_escape_string($this->db, $curr_date1), mysqli_real_escape_string($this->db, $month_str));
        }

//         log_info("yfagui   ".$sql4);
        $res4 = mysqli_query($this->db, $sql4);
        if(!$res4){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql4");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{            
            $num_rows4 = mysqli_num_rows($res4);
            if($num_rows4>0){
                while($row4 = mysqli_fetch_assoc($res4)){
                    $event_id = $row4['event_id'];
                    $month = $row4['month'];
                     $net_amount = $row4['net_amount'];
                    $event_total += $net_amount;
//                    $row4['month_name'] = $months[$month];
                    $event[] = $row4;
                    if(!empty($event_id) && !in_array($event_id, $event_id_arr)){
                        $event_id_arr[] = $event_id;
                        $event_title_arr[] = $row4['event_title'];
                    }
                }
            }
        }
        
        
        
//        $retention_final = [];
//        for($i=0;$i<count($final_retention_array_value);$i++){            
//            for($j=0;$j<count($final_retention_array_value[$i]);$j++){
//                $retention_final[$i]['name'] = $final_retention_array_value[$i][$j]['category_title'];
//                $retention_final[$i][$month_name_array[$j]] = $final_retention_array_value[$i][$j]['reg_cancelled'];
//            }
//        }
        
        for($j=0;$j<6;$j++){
            $e_total_sales=0;
            $temp = [];
            $e_present =  0;
            for($x=0;$x<count($event_id_arr);$x++){
                $e_id = $event_id_arr[$x];
                $e_title = $event_title_arr[$x];
            for($k=0;$k<count($event);$k++){
                    if($month_array1[$j]==$event[$k]['month'] && (int)$e_id==$event[$k]['event_id']){
                    $e_present = 1;
                        $event[$k]['title'] = $e_title;
                    $event_new[] = $event[$k];
                    $e_total_sales += $event[$k]['net_amount'];
                }
            }
            }
//            for($k=0;$k<count($event);$k++){
//                if($month_array[$j]==$event[$k]['month']){
//                    $e_present = 1;
//                    $event_new[] = $event[$k];
//                    $e_total_sales += $event[$k]['net_amount'];
//                }
//            }
//            if($e_present==0){                        
//                $temp['month'] = $month_array[$j];
//                $temp['month_name'] = $months[$month_array[$j]];
//                $temp['Category'] =  "Event";
//                $temp['status'] = "Success";
//                $temp['net_amount'] = number_format(0, 2);
//                $event[] = $temp;
//                $event_new[] = $event[$k];
//            }
            $e_total_sales_arr[$j] = $e_total_sales;
        }

        for($x=0;$x<count($event_id_arr);$x++){
            $e_id = $event_id_arr[$x];
            for($j=0;$j<6;$j++){
                for($k=0;$k<count($event_new);$k++){
                    if($event_new[$k]['month']==$month_array1[$j] && $event_new[$k]['event_id']==$e_id){
                        $title = $event_new[$k]['title'];
                        $id = $event_new[$k]['event_id'];
                        $final_e_sales_array[$x]['name'] = $title;
                        $final_e_sales_array[$x]['id'] = $id;
                        $final_e_sales_array[$x][$month_name_array[$j]] =$event_new[$k]['net_amount'];
                    }                            
                }
            }
        }
        
        
        for($i=0;$i<count($final_e_sales_array);$i++){
            $check_zero=0;
            for($j=2;$j<=7;$j++){
                if((double)$final_e_sales_array[$i][$month_name_array[$j-2]]>0 ){
                     $check_zero=1;
                }
            }
            if($check_zero==0){
                array_splice($final_e_sales_array,$i,1);
                  $i--;
            }
        }   
      
        $output = $sales_total_arr = [];
        for($j=0;$j<6;$j++){
            $sales_total_arr[$j] = $m_total_sales_arr[$j]+$e_total_sales_arr[$j]+$t_total_sales_arr[$j]+$r_total_sales_arr[$j]+$misc_total_sales_arr[$j];
        }

        $output['months'] = $month_name;
        $output['month_netsales']=$month_name1;
        $output['month_ret']=$month_rent;
        $output['sales'] = $output['members'] = $output['retention'] = $output['failed'] = $output['hold'] = [];
        $sales1 = $sales2 = $sales3 = $sales4 =$sales5= $t_sales = $r_sales= $misc_sales =$mem1 = $mem2 = $mem3 = $mem4 = $ret1 = $ret2 = [];
        $sales1['name'] = 'Total';
        $sales2['name'] = 'Memberships';
        $sales3['name'] = 'Events';
        $sales4['name'] = 'Current Failed/Past due Payments:';
        $t_sales['name'] = 'Trial Program';
        $r_sales['name'] = 'Retail';
        $misc_sales['name'] = 'Miscellaneous';
        for($i=0;$i<6;$i++){
            $sales1[$month_name_array[$i]] = number_format($sales_total_arr[$i], 2);
            $sales2[$month_name_array[$i]] = number_format($m_total_sales_arr[$i], 2);
            $sales3[$month_name_array[$i]] = number_format($e_total_sales_arr[$i], 2);
            $t_sales[$month_name_array[$i]] = number_format($t_total_sales_arr[$i], 2);
            $r_sales[$month_name_array[$i]] = number_format($r_total_sales_arr[$i], 2);
            $misc_sales[$month_name_array[$i]] = number_format($misc_total_sales_arr[$i], 2);
        }
        
        for ($i = 0; $i < count($final_rs_sales_array); $i++) {
            $final_rs_sales_array[$i]['children'] = [];
            foreach ($retail_child_array as $key => $value) {
                if ($final_rs_sales_array[$i]['id'] == $key) {
                    for ($j = 0; $j < count($final_rcs_sales_array); $j++) {
                        for($k = 0; $k < count($value); $k++){
                            if($value[$k] == $final_rcs_sales_array[$j]['id']){
                                $final_rs_sales_array[$i]['children'][] = $final_rcs_sales_array[$j];
                            }
                        }
                    }
                }
            }
        }
        
        $sales2['children'] = $final_ms_sales_array;
        $t_sales['children'] = $final_ts_sales_array;
        $r_sales['children'] = $final_rs_sales_array;
        $misc_sales['children'] = [];
        $first_month=array();
        foreach ($final_e_sales_array as $key => $row) {
            $first_month[$key] = $row['first_month'];
        }
        $final_event_sales_array= array_multisort($first_month, SORT_DESC,$final_e_sales_array);
        $sales3['children'] =$final_e_sales_array;
        
        if(empty($failed_total)){
            $sales4['first_month'] = number_format(0, 2);
        }else{
            $sales4['first_month'] = number_format($failed_total, 2);
        }
        
        if (empty($upcoming_total)) {
            $sales5['first_month'] = number_format(0, 2);
        } else {
            $sales5['first_month'] = number_format($upcoming_total, 2);
        }
        $sales1['children'][] = $sales2;
        $sales1['children'][] = $t_sales;
        $sales1['children'][] = $sales3;
        $sales1['children'][] = $r_sales;
        $sales1['children'][] = $misc_sales;
        
        $output['sales'][] = $sales1;
        $output['failed'] = $sales4;
        $output['upcoming'] = $sales5;

        $mem1['name'] = 'Total';
        $mem2['name'] = 'New';
        $mem3['name'] = 'Hold';
        $mem4['name'] = 'Resumed from Hold';
        $mem5['name']=  'Cancelled / Completed';
        $mem6['name'] = 'Net Gain';
        $mem7['name'] = 'Members on Hold:';
        for($i=0;$i<6;$i++){
            $mem1[$month_name_array[$i]] = $m_total_active_members_arr[$i];
            $mem2[$month_name_array[$i]] = $m_total_new_members[$i];
            $mem3[$month_name_array[$i]] = $m_total_pause_members[$i];
            $mem4[$month_name_array[$i]] = $m_total_pause_resume_members[$i];
            $mem5[$month_name_array[$i]] =$m_total_cancel_members[$i]+$m_total_completed_members[$i];
            $mem6[$month_name_array[$i]] = $net_gain_arr[$i];
            
        }
        $mem1['children'] = $final_active_members_array;
        $mem2['children'] = $final_new_members_array;
        $mem3['children'] = $final_pc_members_array;
        $mem4['children'] = $final_rc_members_array;
        $mem5['children']=$final_can_members_array;
        $mem7['first_month'] = $current_hold;
        $output['members'][] = $mem1;
        $output['members'][] = $mem2;
        $output['members'][] = $mem3;
        $output['members'][] = $mem4;
        $output['members'][] = $mem5;
        $output['members'][] = $mem6;
        $output['hold'] = $mem7;
        
        $ret1['name'] = 'Members Cancellations at';
        for($i=0;$i<6;$i++){
            if(empty($final_retention_array[$i])){
                $ret1[$month_name_array[$i]] = 0;
            }else{
                $ret1[$month_name_array[$i]] =  $final_retention_array[$i];
            }
        }     
        $ret1['children'] = $final_retention_array_value;
        $output['retention'][] = $ret1;
        
        $msg = array('status' => "Success", "msg" => $output);
        $this->response($this->json($msg), 200);
        date_default_timezone_set($curr_time_zone);
    }

        public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }    

//    //Get Student details
//    public function getStudentPaymentInfoDetails($company_id, $call_back) {
//
//        $sql = sprintf("SELECT mr.student_id, concat(`membership_registration_column_1`,' ', `membership_registration_column_2`) student_name, `buyer_name`, `buyer_email`, `buyer_phone`,     
//                        `buyer_postal_code`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`,       
//                        `membership_registration_column_1`, `membership_registration_column_2`, `membership_registration_column_3`, `membership_registration_column_4`, `membership_registration_column_5`,           
//                        `membership_registration_column_6`, `membership_registration_column_7`, `membership_registration_column_8`, `membership_registration_column_9`, `membership_registration_column_10`,         
//                        `participant_street`, `participant_city`, `participant_state`, `participant_zip`, `participant_country` ,created_dt , last_updt_dt  
//                        FROM `membership_registration` mr LEFT JOIN student s ON s.`company_id`= mr.`company_id` AND mr.`student_id`= s.`student_id` AND s.`deleted_flag`!='Y' WHERE mr.company_id = '%s' GROUP BY `buyer_email`, `credit_card_id`
//                        UNION
//                        SELECT er.student_id, concat(`event_registration_column_1`,' ', `event_registration_column_2`) student_name, er.`buyer_name`, er.`buyer_email`, er.`buyer_phone`,           
//                        er.`buyer_postal_code`, er.`credit_card_id`, er.`credit_card_status`, er.`credit_card_name`, er.`credit_card_expiration_month`, er.`credit_card_expiration_year`,     
//                        `event_registration_column_1`, `event_registration_column_2`, `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`,     
//                        `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`,  
//                        '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country ,created_dt, last_updt_dt
//                        FROM `event_registration` er LEFT JOIN student s ON s.`company_id`= er.`company_id` AND er.`student_id`= s.`student_id` AND s.`deleted_flag` !='Y' WHERE er.company_id = '%s'
//                        GROUP BY `buyer_email`, `credit_card_id` ORDER BY `buyer_email` ASC ,`created_dt` DESC, `last_updt_dt` DESC", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
//        $result = mysqli_query($this->db, $sql);
//        if(!$result){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//        }else{
//            $num_of_rows = mysqli_num_rows($result);
//            if ($num_of_rows > 0) {
//                $output = '';
//                $card = $stud_list = [];
//                while ($row = mysqli_fetch_assoc($result)) {
//                    $email = strtolower($row['buyer_email']);
//                    if (!in_array($email, $stud_list, true)) {
//                        $stud_list[] = $email;
//                        if (!empty($row['credit_card_id'])) {
//                            $card['credit_card_id'] = $row['credit_card_id'];
//                            $card['credit_card_status'] = $row['credit_card_status'];
//                            $card['credit_card_name'] = $row['credit_card_name'];
//                            $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
//                            $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
//                            $card['postal_code'] = $row['buyer_postal_code'];
//                            $row['card_details'][] = $card;
//                        }
//                        unset($row['credit_card_id']);
//                        unset($row['credit_card_status']);
//                        unset($row['credit_card_name']);
//                        unset($row['credit_card_expiration_month']);
//                        unset($row['credit_card_expiration_year']);
//                        $output['student_details'][] = $row;
//                    } else {
//                        if (!empty($row['credit_card_id'])) {
//                            $card['credit_card_id'] = $row['credit_card_id'];
//                            $card['credit_card_status'] = $row['credit_card_status'];
//                            $card['credit_card_name'] = $row['credit_card_name'];
//                            $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
//                            $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
//                            $card['postal_code'] = $row['buyer_postal_code'];
//                            $output['student_details'][count($output['student_details']) - 1]['card_details'][] = $card;
//                        }
//                    }
//                }
//                $out = array('status' => "Success", 'msg' => $output);
//                if ($call_back == 0) {
//                    // If success everythig is good send header as "OK" and user details
//                    $this->response($this->json($out), 200);
//                } else {
//                    return $out;
//                }
//            } else {
//                $error = array('status' => "Failed", "msg" => "Student details doesn't exist.");
//                if ($call_back == 0) {
//                    $this->response($this->json($error), 200); // If no records "No Content" status
//                } else {
//                    return $error;
//                }
//            }
//        }
//    }    

    //Get Student details
    protected function getStudentPaymentInfoDetails($company_id, $call_back) {
        $group_by_text = '';
        $query = sprintf("SELECT c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if (mysqli_num_rows($result1) > 0) {
                $row = mysqli_fetch_assoc($result1);
                $wepay_status = $row['wepay_status'];
                $stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
            }
        }
        if ($stripe_status == 'Y' && $stripe_account_state != 'N'){
            $group_by_text = ', payment_method_id';
        }elseif($wepay_status == 'Y'){
            $group_by_text = ', credit_card_id';
        }else{
            $group_by_text = '';
        }

        $sql = sprintf("SELECT mr.student_id,mr.`participant_id`, concat(`membership_registration_column_1`,' ', `membership_registration_column_2`) student_name, mr.`buyer_first_name`,mr.`buyer_last_name`, `buyer_email`, `buyer_phone`,     
                        `buyer_postal_code`, mr.`payment_method_id`, mr.`credit_card_id`, `credit_card_status`, `credit_card_name`, `stripe_card_name`,`credit_card_expiration_month`, `credit_card_expiration_year`,       
                        `membership_registration_column_1` reg_col_1, `membership_registration_column_2`  reg_col_2, `membership_registration_column_3` reg_col_3, `membership_registration_column_4` reg_col_4, `membership_registration_column_5` reg_col_5,           
                        `membership_registration_column_6`  reg_col_6, `membership_registration_column_7` reg_col_7, `membership_registration_column_8` reg_col_8, `membership_registration_column_9` reg_col_9, `membership_registration_column_10` reg_col_10,         
                        `participant_street`, `participant_city`, `participant_state`, `participant_zip`, `participant_country`, concat(`membership_registration_column_1`,' ', `membership_registration_column_2`) sname, created_dt , last_updt_dt, mr.registration_from  
                        FROM `membership_registration` mr LEFT JOIN student s ON s.`company_id`= mr.`company_id` AND mr.`student_id`= s.`student_id` AND s.`deleted_flag`!='Y' WHERE mr.company_id = '%s' AND (TRIM(mr.`payment_method_id`)!='' || TRIM(mr.`credit_card_id`)!='') GROUP BY `buyer_email`, sname %s
                        UNION
                        SELECT er.student_id,er.`participant_id`, concat(`event_registration_column_1`,' ', `event_registration_column_2`) student_name, er.`buyer_first_name`,er.`buyer_last_name`, er.`buyer_email`, er.`buyer_phone`,           
                        er.`buyer_postal_code`, er.`payment_method_id`, er.`credit_card_id`, er.`credit_card_status`, er.`credit_card_name`,`stripe_card_name`, er.`credit_card_expiration_month`, er.`credit_card_expiration_year`,     
                        `event_registration_column_1` reg_col_1, `event_registration_column_2` reg_col_2, `event_registration_column_3` reg_col_3, `event_registration_column_4` reg_col_4, `event_registration_column_5` reg_col_5,     
                        `event_registration_column_6` reg_col_6, `event_registration_column_7` reg_col_7, `event_registration_column_8` reg_col_8, `event_registration_column_9` reg_col_9, `event_registration_column_10` reg_col_10,  
                        '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country, concat(`event_registration_column_1`,' ', `event_registration_column_2`) sname, created_dt, last_updt_dt, er.registration_from 
                        FROM `event_registration` er LEFT JOIN student s ON s.`company_id`= er.`company_id` AND er.`student_id`= s.`student_id` AND s.`deleted_flag` !='Y' WHERE er.company_id = '%s' AND (TRIM(er.`payment_method_id`)!='' || TRIM(er.`credit_card_id`)!='') GROUP BY `buyer_email`, sname %s 
                        UNION
                        SELECT tr.student_id, tr.`participant_id`,concat(`trial_registration_column_1`,' ', `trial_registration_column_2`) student_name, tr.`buyer_first_name`,tr.`buyer_last_name`, tr.`buyer_email`, tr.`buyer_phone`,           
                        tr.`buyer_postal_code`, tr.`payment_method_id`, tr.`credit_card_id`, tr.`credit_card_status`, tr.`credit_card_name`,`stripe_card_name`, tr.`credit_card_expiration_month`, tr.`credit_card_expiration_year`,     
                        `trial_registration_column_1` reg_col_1, `trial_registration_column_2` reg_col_2, `trial_registration_column_3` reg_col_3, `trial_registration_column_4` reg_col_4, `trial_registration_column_5` reg_col_5,     
                        `trial_registration_column_6` reg_col_6, `trial_registration_column_7` reg_col_7, `trial_registration_column_8` reg_col_8, `trial_registration_column_9` reg_col_9, `trial_registration_column_10` reg_col_10,  
                        '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country ,concat(`trial_registration_column_1`,' ', `trial_registration_column_2`) sname,created_dt, last_updt_dt, tr.registration_from 
                        FROM `trial_registration` tr LEFT JOIN student s ON s.`company_id`= tr.`company_id` AND tr.`student_id`= s.`student_id` AND s.`deleted_flag` !='Y' WHERE tr.company_id = '%s' AND (TRIM(tr.`payment_method_id`)!='' || TRIM(tr.`credit_card_id`)!='') GROUP BY `buyer_email`, sname %s
                        UNION
                        SELECT ms.student_id, '' as participant_id, '' as student_name, ms.`buyer_first_name`,ms.`buyer_last_name`, ms.`buyer_email`, ms.`buyer_phone`,           
                        '' as buyer_postal_code, ms.`payment_method_id`, ms.`credit_card_id`, ms.`credit_card_status`, ms.`credit_card_name`,`stripe_card_name`, ms.`credit_card_expiration_month`, ms.`credit_card_expiration_year`,     
                        '' as reg_col_1, '' as reg_col_2, '' as reg_col_3, '' as reg_col_4, '' as reg_col_5, '' as reg_col_6, '' as reg_col_7, '' as reg_col_8, '' as reg_col_9, '' as reg_col_10,  
                        '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country ,'' as sname,created_dt, last_updt_dt, ms.registration_from 
                        FROM `misc_order` ms LEFT JOIN student s ON s.`company_id`= ms.`company_id` AND ms.`student_id`= s.`student_id` AND s.`deleted_flag` !='Y' WHERE ms.company_id = '%s' AND (TRIM(ms.`payment_method_id`)!='' || TRIM(ms.`credit_card_id`)!='') GROUP BY `buyer_email` %s
                        ORDER BY `buyer_email` ASC ,`created_dt` DESC, `last_updt_dt` DESC", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $group_by_text),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $group_by_text), mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $group_by_text), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $group_by_text));                
        $result = mysqli_query($this->db, $sql);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $output = $output['student_details'] = [];
                $stud_list = $obj_list = $out = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $out[] = $row;
                }
                for($i=0; $i<count($out); $i++){
                    $card = [];
                    $row = $out[$i];
                    $email = strtolower(trim($row['buyer_email']));
                    $card_id = $row['payment_method_id'];
                    $name = strtolower(trim($row['sname']));
                    $obj = array("email"=>"$email", "name"=>"$name");
                    if(!in_array($obj, $obj_list)){
                        $obj_list[] = $obj;
                        if ($stripe_status == 'Y' && $stripe_account_state != 'N'){
                            if (!empty($row['payment_method_id'])) {
                                $card['payment_method_id'] = $row['payment_method_id'];
                                $card['credit_card_name'] = $row['stripe_card_name'];
                                $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                $card['postal_code'] = $row['buyer_postal_code'];
                                $row['card_details'][] = $card;
                            }               
                            unset($row['credit_card_id']); 
                            unset($row['credit_card_status']);
                            unset($row['credit_card_name']);
                            unset($row['payment_method_id']);
                            unset($row['stripe_card_name']);
                            unset($row['credit_card_expiration_month']);
                            unset($row['credit_card_expiration_year']);
                            if(isset($output['student_details'])){
                                $row['stud_index'] = count($output['student_details']);
                            }else{
                                $row['stud_index'] = 0;
                            }
                        }else if ($wepay_status == 'Y'){
                            if (!empty($row['credit_card_id'])) {
                                $card['credit_card_id'] = $row['credit_card_id'];
                                $card['credit_card_status'] = $row['credit_card_status'];
                                $card['credit_card_name'] = $row['credit_card_name'];
                                $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                $card['postal_code'] = $row['buyer_postal_code'];
                                $row['card_details'][] = $card;
                            }
                            unset($row['payment_method_id']);
                            unset($row['stripe_card_name']);
                            unset($row['credit_card_id']);
                            unset($row['credit_card_status']);
                            unset($row['credit_card_name']);
                            unset($row['credit_card_expiration_month']);
                            unset($row['credit_card_expiration_year']);
                            if(isset($output['student_details'])){
                                $row['stud_index'] = count($output['student_details']);
                            }else{
                                $row['stud_index'] = 0;
                            }                             
                        }
                        $output['student_details'][] = $row;
                    }else{
                        $obj_index = array_search($obj, $obj_list);
                        if ($stripe_status == 'Y' && $stripe_account_state != 'N'){
                            if (!empty($row['payment_method_id'])) {
                                $card['payment_method_id'] = $row['payment_method_id'];
                                $card['credit_card_name'] = $row['stripe_card_name'];
                                $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                $card['postal_code'] = $row['buyer_postal_code'];
    //                            if(isset($output['student_details'])){
                            }
                        }else if ($wepay_status == 'Y'){
                            if (!empty($row['credit_card_id'])) {
                                $card['credit_card_id'] = $row['credit_card_id'];
                                $card['credit_card_status'] = $row['credit_card_status'];
                                $card['credit_card_name'] = $row['credit_card_name'];
                                $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                $card['postal_code'] = $row['buyer_postal_code'];
    //                            if(isset($output['student_details'])){    
                            }
                        }
                        $output['student_details'][$obj_index]['card_details'][] = $card; 
                    }
                }
                $out2 = array('status' => "Success", 'msg' => $output);
                if ($call_back == 0) {
                    // If success everythig is good send header as "OK" and user details
                    $this->response($this->json($out2), 200);
                } else {
                    return $out2;
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Student details doesn't exist.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200); // If no records "No Content" status
                } else {
                    return $error;
                }
            }
        }
    }

    
    protected function sendGroupPushMessageForStudent($company_id, $subject, $message, $student_id, $also_send_mail, $attached_file, $file_name,$select_all_flag,$search){
        
        $output1 = $android = $ios = [];
        if ($select_all_flag == 'Y') {
            
       if(!empty($search)){
             $s_text=" and (student_name like  '%$search%'  or student_email like  '%$search%'  or first_login_dt like  '%$search%'  or  last_login_dt like  '%$search%')";
         }else{
             $s_text='';
         }
            $sql_all = sprintf("SELECT `student_id` as id
                FROM `student` WHERE company_id = '%s' AND `deleted_flag`!='Y' %s", mysqli_real_escape_string($this->db,$company_id),$s_text);
            $result_all = mysqli_query($this->db, $sql_all);
            if (!$result_all) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_all");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($result_all);
                if ($num_rows > 0) {
                    while ($rows1 = mysqli_fetch_assoc($result_all)) {
                        $output1[] = $rows1['id'];
                    }
                }
            }
        }
        for($i=0; $i<count($student_id); $i++){
            $output1[] = $student_id[$i]['id'];
        }
        $student_id_list = implode(",", $output1);

        $query = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
            (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
               IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props  FROM `student` s
            LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id)
            WHERE `company_id`='%s' AND `deleted_flag`!='Y' AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
            AND push_device_id IS NOT NULL AND trim(push_device_id)!='' AND `student_id`in (%s) order by app_id) t1            
            LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
            where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' and IFNULL(m.student_id,mp.student_id) IN (%s) group by student_id) t2 USING(company_id, student_id)", 
            mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$student_id_list),mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$student_id_list));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $stud_id = $output = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                    if(!is_null($row['student_id']) && !empty($row['student_id'])){
                        $stud_id[] = $row['student_id'];
                    }
                }
                $msg_id = $this->insertPushMessage($company_id,$stud_id,$message,'AC');
                $update_message = sprintf("UPDATE `message` SET `push_delivered_date`=NOW() WHERE `message_id`='%s' and `company_id`='%s'", $msg_id, $company_id);
                $result_update_message = mysqli_query($this->db, $update_message);
                if (!$result_update_message) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                $query2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                    (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                       IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props  FROM `student` s
                    LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id)
                    WHERE `company_id`='%s' AND `deleted_flag`!='Y' AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                    AND push_device_id IS NOT NULL AND trim(push_device_id)!='' AND `student_id`in (%s) order by app_id) t1            
                    LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                    where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' and IFNULL(m.student_id,mp.student_id) IN (%s) group by student_id) t2 USING(company_id, student_id)", 
                    mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$student_id_list),mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$student_id_list));
                $result2 = mysqli_query($this->db, $query2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $num_of_rows2 = mysqli_num_rows($result2);
                    if ($num_of_rows2 > 0) {
                        $output=[];
                        while ($row2 = mysqli_fetch_assoc($result2)) {
                            $output[] = $row2;
                        }
                    }
                }
                $r_rand = mt_rand(1,9999);
                $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();  //for creating pem file from db
                if (!file_exists($rfolder_name)) {
                    $oldmask = umask(0);
                    mkdir($rfolder_name, 0777, true);
                    umask($oldmask);
                }else{ // to make directory once.
                    $r = 0;
                    while ($r < 100) {
                        $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();
                        if (!file_exists($rfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($rfolder_name, 0777, true);
                            umask($oldmask);
                            break;
                        }
                     $r++;
                    }
                }
                $ios_socket_url ='';
                $ios_app_id_array = [];
                $host = $_SERVER['HTTP_HOST'];
                if (strpos($host, 'mystudio.academy') !== false) {
//                if (strpos($host, 'www.mystudio.academy') !== false || strpos($host, 'stage.mystudio.academy') !== false) {
                    //production
                    $ios_socket_url = "ssl://gateway.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];
                        
                        if ($devicetype == 'I') {
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['prod_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            /*
                            if ($obj['type'] == 'A') {
                                $android['key']['others'] = $obj['android_props'];
                                $android['device_token']['others'][] = $devicetoken;
                            } else if ($obj['type'] == 'M') {
                                $android['key']['mystudio'] = $obj['android_props'];
                                $android['device_token']['mystudio'][] = $devicetoken;
                            }
                             */
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }else{
                    //development
                    $ios_socket_url = "ssl://gateway.sandbox.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];                        
                        
                        if ($devicetype == 'I') {
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['dev_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif ($devicetype == 'A') {
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            /*
                            if ($obj['type'] == 'A') {
                                $android['key']['others'] = $obj['android_props'];
                                $android['device_token']['others'][] = $devicetoken;
                            } else if ($obj['type'] == 'M') {
                                $android['key']['mystudio'] = $obj['android_props'];
                                $android['device_token']['mystudio'][] = $devicetoken;
                            }
                             */
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;                            
                        } else {
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }
                $send_count = $this->sendpush($message, $ios, $android, $ios_socket_url);
                $update_message = sprintf("UPDATE `message` SET `no_of_users`='%s' WHERE `message_id`='%s' and `company_id`='%s'", $send_count, $msg_id, $company_id);
                $result_update_message = mysqli_query($this->db, $update_message);
                if (!$result_update_message) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                $check = $this->deleteDirectory("$rfolder_name");// delete directory with content
                $out = array('status' => "Success", 'msg' => "Push Message sent successfully. ");
                if($also_send_mail!='Y'){
                    $this->response($this->json($out), 200);
                }else{
                    $this->sendGroupPushMailForStudent($company_id, $subject, $message, $student_id, $out['msg'], $attached_file, $file_name,$select_all_flag,$search);
                }
//                return $out;
            }else{
                $out = array('status' => "Failed", 'msg' => "Push Message sent successfully ");
                if($also_send_mail!='Y'){
                    $this->response($this->json($out), 200);
                }else{
                    $this->sendGroupPushMailForStudent($company_id, $subject, $message, $student_id, $out['msg'], $attached_file, $file_name,$select_all_flag,$search);
                }
//                return $out;
            }
        }
    }
    
    public function sendIndividualPushMessageForPaymentUser($company_id, $subject, $message, $selected_payment_list, $also_send_mail, $attached_file, $file_name){
        
        $output1 = $output2 = $output3= $output4 = $output5 = $android = $ios = [];
        for($i=0; $i<count($selected_payment_list); $i++){
            if($selected_payment_list[$i]['Category']=='Membership'){
                $output1[] = $selected_payment_list[$i]['event_reg_id'];
            }elseif($selected_payment_list[$i]['Category']=='Events'){
                $output2[] = $selected_payment_list[$i]['event_reg_id'];
            }elseif($selected_payment_list[$i]['Category']=='Trial'){
                 $output3[] = $selected_payment_list[$i]['event_reg_id'];
            }elseif($selected_payment_list[$i]['Category']=='Retail'){
                 $output4[] = $selected_payment_list[$i]['event_reg_id'];
            }elseif($selected_payment_list[$i]['Category']=='Miscellaneous'){
                 $output5[] = $selected_payment_list[$i]['event_reg_id'];
            }
        }
        $mem_reg_id_list = implode(",", $output1);
        $event_reg_id_list = implode(",", $output2);
        $trial_reg_id_list = implode(",", $output3);
        $retail_reg_id_list = implode(",", $output4);
        $misc_reg_id_list = implode(",", $output5);
            
        if(!empty($output1)){
            $query = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                   (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                   IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   
                   LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id) WHERE `company_id`='%s' 
                   AND (`student_id` IN 
                   (SELECT `student_id` FROM `membership_registration` WHERE `company_id`='%s' AND `membership_registration_id` in (%s))
                   OR `student_email` IN 
                   (SELECT `buyer_email` FROM `membership_registration` WHERE `company_id`='%s' AND `membership_registration_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                   AND push_device_id IS NOT NULL AND trim(push_device_id)!='') t1            
                   LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                   where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), 
                   mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$mem_reg_id_list),
                   mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$mem_reg_id_list), mysqli_real_escape_string($this->db,$company_id));
        }elseif(!empty($output2)){
            $query = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                   (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                   IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   
                   LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id) WHERE `company_id`='%s' 
                   AND (`student_id` IN 
                   (SELECT `student_id` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s))
                   OR `student_email` IN 
                   (SELECT `buyer_email` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                   AND push_device_id IS NOT NULL AND trim(push_device_id)!='') t1            
                   LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                   where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), 
                   mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_reg_id_list),
                   mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_reg_id_list), mysqli_real_escape_string($this->db,$company_id));
        }elseif(!empty($output3)){
              $query = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                   (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                   IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   
                   LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id) WHERE `company_id`='%s' 
                   AND (`student_id` IN 
                   (SELECT `student_id` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s))
                   OR `student_email` IN 
                   (SELECT `buyer_email` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                   AND push_device_id IS NOT NULL AND trim(push_device_id)!='') t1            
                   LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                   where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), 
                   mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_reg_id_list),
                   mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_reg_id_list), mysqli_real_escape_string($this->db,$company_id));
        }elseif(!empty($output4)){
              $query = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                   (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                   IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   
                   LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id) WHERE `company_id`='%s' 
                   AND (`student_id` IN 
                   (SELECT `student_id` FROM `retail_orders` WHERE `company_id`='%s' AND `retail_order_id` in (%s))
                   OR `student_email` IN 
                   (SELECT `buyer_email` FROM `retail_orders` WHERE `company_id`='%s' AND `retail_order_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                   AND push_device_id IS NOT NULL AND trim(push_device_id)!='') t1            
                   LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                   where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), 
                   mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_reg_id_list),
                   mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_reg_id_list), mysqli_real_escape_string($this->db,$company_id));
        }else{
              $query = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                   (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                   IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   
                   LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id) WHERE `company_id`='%s' 
                   AND (`student_id` IN 
                   (SELECT `student_id` FROM `misc_order` WHERE `company_id`='%s' AND `misc_order_id` in (%s))
                   OR `student_email` IN 
                   (SELECT `buyer_email` FROM `misc_order` WHERE `company_id`='%s' AND `misc_order_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                   AND push_device_id IS NOT NULL AND trim(push_device_id)!='') t1            
                   LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                   where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), 
                   mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$misc_reg_id_list),
                   mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$misc_reg_id_list), mysqli_real_escape_string($this->db,$company_id));
        }
        $result = mysqli_query($this->db, $query);
        
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $student_id = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                    if(!is_null($row['student_id']) && !empty($row['student_id'])){
                        $student_id[] = $row['student_id'];
                    }
                }
                $msg_id = $this->insertPushMessage($company_id,$student_id,$message,'PP');
                if(!empty($output1)){
                    $query2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                           (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                           IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   
                           LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id) WHERE `company_id`='%s' 
                           AND (`student_id` IN 
                           (SELECT `student_id` FROM `membership_registration` WHERE `company_id`='%s' AND `membership_registration_id` in (%s))
                           OR `student_email` IN 
                           (SELECT `buyer_email` FROM `membership_registration` WHERE `company_id`='%s' AND `membership_registration_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                           AND push_device_id IS NOT NULL AND trim(push_device_id)!='') t1            
                           LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                           where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), 
                           mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$mem_reg_id_list),
                           mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$mem_reg_id_list), mysqli_real_escape_string($this->db,$company_id));
                }elseif(!empty($output2)){
                    $query2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                           (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                           IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   
                           LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id) WHERE `company_id`='%s' 
                           AND (`student_id` IN 
                           (SELECT `student_id` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s))
                           OR `student_email` IN 
                           (SELECT `buyer_email` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                           AND push_device_id IS NOT NULL AND trim(push_device_id)!='') t1            
                           LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                           where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), 
                           mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_reg_id_list),
                           mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_reg_id_list), mysqli_real_escape_string($this->db,$company_id));
                }elseif(!empty($output3)){
                      $query2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                           (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                           IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   
                           LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id) WHERE `company_id`='%s' 
                           AND (`student_id` IN 
                           (SELECT `student_id` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s))
                           OR `student_email` IN 
                           (SELECT `buyer_email` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                           AND push_device_id IS NOT NULL AND trim(push_device_id)!='') t1            
                           LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                           where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), 
                           mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_reg_id_list),
                           mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_reg_id_list), mysqli_real_escape_string($this->db,$company_id));
                }elseif(!empty($output4)){
                      $query2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                           (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                           IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   
                           LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id) WHERE `company_id`='%s' 
                           AND (`student_id` IN 
                           (SELECT `student_id` FROM `retail_orders` WHERE `company_id`='%s' AND `retail_order_id` in (%s))
                           OR `student_email` IN 
                           (SELECT `buyer_email` FROM `retail_orders` WHERE `company_id`='%s' AND `retail_order_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                           AND push_device_id IS NOT NULL AND trim(push_device_id)!='') t1            
                           LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                           where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), 
                           mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_reg_id_list),
                           mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_reg_id_list), mysqli_real_escape_string($this->db,$company_id));
                }else{
                      $query2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                           (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                           IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   
                           LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id) WHERE `company_id`='%s' 
                           AND (`student_id` IN 
                           (SELECT `student_id` FROM `misc_order` WHERE `company_id`='%s' AND `misc_order_id` in (%s))
                           OR `student_email` IN 
                           (SELECT `buyer_email` FROM `misc_order` WHERE `company_id`='%s' AND `misc_order_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                           AND push_device_id IS NOT NULL AND trim(push_device_id)!='') t1            
                           LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                           where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), 
                           mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$misc_reg_id_list),
                           mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$misc_reg_id_list), mysqli_real_escape_string($this->db,$company_id));
                }
                $result2 = mysqli_query($this->db, $query2);

                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $num_of_rows2 = mysqli_num_rows($result2);
                    if ($num_of_rows2 > 0) {
                        $output = [];
                        while ($row2 = mysqli_fetch_assoc($result2)) {
                            $output[] = $row2;
                        }
                    }
                }
                $r_rand = mt_rand(1,9999);
                $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();  //for creating pem file from db
                if (!file_exists($rfolder_name)) {
                    $oldmask = umask(0);
                    mkdir($rfolder_name, 0777, true);
                    umask($oldmask);
                }else{ // to make directory once.
                     $r = 0;
                    while ($r < 100) {
                        $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();
                        if (!file_exists($rfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($rfolder_name, 0777, true);
                            umask($oldmask);
                            break;
                        }
                     $r++;
                    }
                }
                $ios_socket_url ='';
                $ios_app_id_array = [];
                $host = $_SERVER['HTTP_HOST'];
                if (strpos($host, 'mystudio.academy') !== false) {
//                if (strpos($host, 'www.mystudio.academy') !== false || strpos($host, 'stage.mystudio.academy') !== false) {
                    //production
                    $ios_socket_url = "ssl://gateway.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];
                        
                        if ($devicetype == 'I') {                            
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['prod_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }else{
                    //development
                    $ios_socket_url = "ssl://gateway.sandbox.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];                        
                        
                        if ($devicetype == 'I') {
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['dev_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }
                $sent_count = $this->sendpush($message, $ios, $android, $ios_socket_url);
                $check = $this->deleteDirectory("$rfolder_name");// delete directory with content
                $out = array('status' => "Success", 'msg' => "Push Message sent successfully. ");
                if($also_send_mail!='Y'){
                    $this->response($this->json($out), 200);
                }else{
                    $this->sendIndividualPushMailForPaymentUser($company_id, $subject, $message, $selected_payment_list, $out['msg'], $attached_file, $file_name);
                }
//                return $out;
            }else{
                $out = array('status' => "Failed", 'msg' => "Push Message sent successfully ");
                if($also_send_mail!='Y'){
                    $this->response($this->json($out), 200);
                }else{
                    $this->sendIndividualPushMailForPaymentUser($company_id, $subject, $message, $selected_payment_list, $out['msg'], $attached_file, $file_name);
                }
//                return $out;
            }
        }
    }
    
    public function insertPushMessage($company_id, $student_id, $message_text, $push_from){
        $message_id = '';
//        if(count($student_id)==1){
//            $message_to = 'I';
//            $stud_id = $student_id[0];
//        }else
        if(count($student_id)==0){
            $message_to = 'A';
            $stud_id = 'NULL';
        }else{
            $message_to = 'G';
            $stud_id = 'NULL';
        }
        
        $sql1 = sprintf("INSERT INTO `message` (`message_text`, `company_id`, `message_to`, `student_id`, `push_from`) VALUES ('%s', '%s', '%s', %s, '%s')", mysqli_real_escape_string($this->db,$message_text), $company_id, $message_to, $stud_id, $push_from);
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $message_id = mysqli_insert_id($this->db);
            if($message_id>0 && $message_to!='I'){
                $this->sendGroupMessage($company_id, $message_id, $student_id);
            }
        }
        return $message_id;
    }
    
    public function sendIndividualPushMailForPaymentUser($company_id, $subject, $message, $selected_payment_list, $push_msg, $attached_file, $file_name){
        if(empty($subject)){
            $subject = "Message";
        }
        
        $output1 = $output2 = $output3 =$output4 = $output5 = [];
        for($i=0; $i<count($selected_payment_list); $i++){
            if($selected_payment_list[$i]['Category']=='Membership'){
                $output1[] = $selected_payment_list[$i]['event_reg_id'];
            }elseif($selected_payment_list[$i]['Category']=='Events'){
                $output2[] = $selected_payment_list[$i]['event_reg_id'];
            }elseif($selected_payment_list[$i]['Category']=='Trial'){
                 $output3[] = $selected_payment_list[$i]['event_reg_id'];
            }elseif($selected_payment_list[$i]['Category']=='Retail'){
                 $output4[] = $selected_payment_list[$i]['event_reg_id'];
            }elseif($selected_payment_list[$i]['Category']=='Miscellaneous'){
                 $output5[] = $selected_payment_list[$i]['event_reg_id'];
            }
        }
        $mem_reg_id_list = implode(",", $output1);
        $event_reg_id_list = implode(",", $output2);
        $trial_reg_id_list = implode(",", $output3);
        $retail_reg_id_list = implode(",", $output4);
        $misc_reg_id_list = implode(",", $output5);
        
        if(!empty($output1) && !empty($output2) && !empty($output3)&& !empty($output4) && !empty($output5)){
            $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, s.`student_email`,s.`student_cc_email`  
                FROM  `student` s LEFT JOIN  `company` c ON c.`company_id` = s.`company_id` 
                WHERE `student_id` in (SELECT `student_id` FROM `membership_registration` WHERE `company_id`='%s' AND `membership_registration_id` in (%s)
                UNION
                SELECT `student_id` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s)
                UNION
                SELECT `student_id` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s))
                UNION
                SELECT `student_id` FROM `retail_orders` WHERE `company_id`='%s' AND `retail_order_id` in (%s)
                UNION
                SELECT `student_id` FROM `misc_order` WHERE `company_id`='%s' AND `misc_order_id` in (%s))  GROUP BY student_email
                UNION
                SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, `buyer_email` as student_email,'' student_cc_email FROM `student` s 
                LEFT JOIN `company` c ON c.`company_id` = s.`company_id` LEFT JOIN `membership_registration` mr ON mr.`company_id` = s.`company_id` 
                WHERE c.`company_id`='%s' AND `membership_registration_id` in (%s) GROUP BY buyer_email
                UNION 
                SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, `buyer_email` as student_email,'' student_cc_email FROM `student` s 
                LEFT JOIN `company` c ON c.`company_id` = s.`company_id` LEFT JOIN `event_registration` er ON er.`company_id` = s.`company_id` 
                WHERE c.`company_id`='%s' AND `event_reg_id` in (%s) GROUP BY buyer_email
                UNION
                SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, `buyer_email` as student_email,'' student_cc_email FROM `student` s 
                LEFT JOIN `company` c ON c.`company_id` = s.`company_id` LEFT JOIN `trial_registration` tr ON tr.`company_id` = s.`company_id` 
                WHERE c.`company_id`='%s' AND `trial_reg_id` in (%s) GROUP BY buyer_email
                UNION
                SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, `buyer_email` as student_email,'' student_cc_email FROM `student` s 
                LEFT JOIN `company` c ON c.`company_id` = s.`company_id` LEFT JOIN `retail_orders` rto ON rto.`company_id` = s.`company_id` 
                WHERE c.`company_id`='%s' AND `retail_order_id` in (%s) GROUP BY buyer_email
                UNION
                SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, `buyer_email` as student_email,'' student_cc_email FROM `student` s 
                LEFT JOIN `company` c ON c.`company_id` = s.`company_id` LEFT JOIN `misc_order` mo ON mo.`company_id` = s.`company_id` 
                WHERE c.`company_id`='%s' AND `misc_order_id` in (%s) GROUP BY buyer_email", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$mem_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_reg_id_list), 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$misc_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$mem_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$misc_reg_id_list));
        }else{
            if(!empty($output1)){
               
                $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`,t1.*FROM( select s.`student_email`,s.`student_cc_email` FROM  `student` s LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`) WHERE `student_id` in 
                 (SELECT `student_id` FROM `membership_registration` WHERE `company_id`='%s' AND `membership_registration_id` in (%s))  and s.`deleted_flag`!='Y' and  s.`student_email` != IFNULL(b.`bounced_mail`, '') GROUP BY student_email
                 UNION 
                 SELECT  `buyer_email` as student_email,'' student_cc_email FROM `student` s 
                 LEFT JOIN `membership_registration` mr ON mr.`company_id` = s.`company_id` and s.student_id = mr.student_id
                 LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`= buyer_email AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
                 WHERE mr.`company_id`='%s'and s.`deleted_flag`!='Y' AND `membership_registration_id` in (%s) and  `buyer_email` != IFNULL(b.`bounced_mail`, '')  GROUP BY buyer_email)t1, company c WHERE c.`company_id`='%s'  group by student_email", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$mem_reg_id_list), 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$mem_reg_id_list),mysqli_real_escape_string($this->db,$company_id));
            }elseif(!empty($output2)){
                
                 $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, t1.* from
                (select s.`student_email`,s.`student_cc_email` 
                FROM  `student` s  LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
                WHERE `student_id` in (SELECT `student_id` FROM `event_registration` WHERE `company_id`='%s' AND `event_reg_id` in (%s)) and s.`deleted_flag`!='Y' and  s.`student_email` != IFNULL(b.`bounced_mail`, '')  GROUP BY student_email
                UNION 
                SELECT `buyer_email` as student_email,'' student_cc_email FROM `student` s 
                LEFT JOIN `event_registration` er ON er.`company_id` = s.`company_id`  and er.`student_id` = s.`student_id`
                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=er.`company_id`)
                WHERE er.`company_id`='%s' and s.`deleted_flag`!='Y'  AND `event_reg_id` in (%s) and  buyer_email != IFNULL(b.`bounced_mail`, '')  GROUP BY buyer_email ) t1,company c WHERE c.`company_id`='%s'  group by student_email", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$event_reg_id_list), mysqli_real_escape_string($this->db,$company_id));

            }elseif(!empty($output3)){
                 $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, t1.* from
                (select s.`student_email`,s.`student_cc_email` 
                FROM  `student` s  LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
                WHERE `student_id` in (SELECT `student_id` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s))  and s.`deleted_flag`!='Y' and  s.`student_email` != IFNULL(b.`bounced_mail`, '') GROUP BY student_email
                UNION 
                SELECT `buyer_email` as student_email,'' student_cc_email FROM  `trial_registration` tr 
                 LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=tr.`company_id`)  left join `student` s  on s.student_id = tr.student_id
                WHERE tr.`company_id`='%s' and s.`deleted_flag`!='Y'  AND `trial_reg_id` in (%s) and  `buyer_email` != IFNULL(b.`bounced_mail`, '') GROUP BY buyer_email) t1,company c WHERE c.`company_id`='%s'  group by student_email ", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_reg_id_list),mysqli_real_escape_string($this->db,$company_id));

            }elseif(!empty($output4)){
                 $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, t1.* from
                (select s.`student_email`,s.`student_cc_email` 
                FROM  `student` s  LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
                WHERE `student_id` in (SELECT `student_id` FROM `retail_orders` WHERE `company_id`='%s' AND `retail_order_id` in (%s))  and s.`deleted_flag`!='Y' and  s.`student_email` != IFNULL(b.`bounced_mail`, '') GROUP BY student_email
                UNION 
                SELECT `buyer_email` as student_email,'' student_cc_email FROM  `retail_orders` rto 
                 LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=rto.`company_id`)  left join `student` s  on s.student_id = rto.student_id
                WHERE rto.`company_id`='%s' and s.`deleted_flag`!='Y'  AND `retail_order_id` in (%s) and  `buyer_email` != IFNULL(b.`bounced_mail`, '') GROUP BY buyer_email) t1,company c WHERE c.`company_id`='%s'  group by student_email ", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_reg_id_list),mysqli_real_escape_string($this->db,$company_id));

            }else{
                 $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, t1.* from
                (select s.`student_email`,s.`student_cc_email` 
                FROM  `student` s  LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
                WHERE `student_id` in (SELECT `student_id` FROM `misc_order` WHERE `company_id`='%s' AND `misc_order_id` in (%s))  and s.`deleted_flag`!='Y' and  s.`student_email` != IFNULL(b.`bounced_mail`, '') GROUP BY student_email
                UNION 
                SELECT `buyer_email` as student_email,'' student_cc_email FROM  `misc_order` mo 
                 LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=mo.`company_id`)  left join `student` s  on s.student_id = mo.student_id
                WHERE mo.`company_id`='%s' and s.`deleted_flag`!='Y'  AND `misc_order_id` in (%s) and  `buyer_email` != IFNULL(b.`bounced_mail`, '') GROUP BY buyer_email) t1,company c WHERE c.`company_id`='%s'  group by student_email ", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$misc_reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$misc_reg_id_list),mysqli_real_escape_string($this->db,$company_id));

            }
        }
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $cmp_name = $reply_to = '';
                $initial_check = 0;
                while ($row = mysqli_fetch_assoc($result)) {
                    if($initial_check==0){
                        $cmp_name = $row['company_name'];
                        $reply_to = $row['email_id'];
                        $initial_check++;
                    }
                    $output[] = $row;
                }
                    $footer_detail = $this->getunsubscribefooter($company_id);
                for ($idx = 0; $idx < count($output); $idx++) {
                    $obj = (Array) $output[$idx];
                    $user_emailid = $obj["student_email"];
                    $cc_email_list = $obj["student_cc_email"];
                    //echo "******count******".$idx;
                    $sendEmail_status = $this->sendEmail($user_emailid, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file,$cc_email_list,$company_id,'',$footer_detail);
                    if($sendEmail_status['status']=="true"){
                        $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200);
                    }else{
                        $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200); // If no records "No Content" status
                    }
                }
                if(empty($push_msg)){
                    $push_msg="Email Sent Successfully.";
                }
                $msg = array('status' => "Success", "msg" => $push_msg);
                $this->response($this->json($msg), 200);
            } else {
                $error = array('status' => "Success", "msg" => " Email failed due to invalid address");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
    protected function sendGroupPushMailForStudent($company_id, $subject, $message, $student_id, $push_msg, $attached_file, $file_name,$select_all_flag,$search){
        if(empty($subject)){
            $subject = "Message";
        }
        $output1 = [];
        if ($select_all_flag == 'Y') {
            
       if(!empty($search)){
             $s_text=" and (student_name like  '%$search%'  or student_email like  '%$search%'  or first_login_dt like  '%$search%'  or  last_login_dt like  '%$search%')";
         }else{
             $s_text='';
         }
            $sql_all = sprintf("SELECT `student_id` as id
                FROM `student` WHERE company_id = '%s' AND `deleted_flag`!='Y' %s", $company_id,$s_text);
            $result_all = mysqli_query($this->db, $sql_all);
            if (!$result_all) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_all");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($result_all);
                if ($num_rows > 0) {
                    while ($rows1 = mysqli_fetch_assoc($result_all)) {
                        $output1[] = $rows1['id'];
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => "Students doesn't exist to send message for this company.");
                    $this->response($this->json($error), 200);
                }
            }
        }
        for($i=0; $i<count($student_id); $i++){
            $output1[] = $student_id[$i]['id'];
        }
        $student_id_list = implode(",", $output1);
         $countForBouncedEmail=$this->checkBouncedEmailCount($student_id_list);
//        $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, s.`student_email`,s.`student_cc_email` 
//                FROM  `student` s LEFT JOIN  `company` c ON c.`company_id` = s.`company_id` 
//                WHERE `student_id` in (%s)", 
//                mysqli_real_escape_string($this->db,$student_id_list));
        $sql=sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, s.`student_email`,s.`student_cc_email` , b.`bounced_mail`
            FROM  `student` s LEFT JOIN  `company` c ON c.`company_id` = s.`company_id` LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
            WHERE `student_id` in (%s) AND s.`deleted_flag`!='Y' AND s.`student_email` != IFNULL(b.`bounced_mail`, '')" ,mysqli_real_escape_string($this->db,$student_id_list));
         $result = mysqli_query($this->db, $sql);

//        $result = mysqli_query($this->db, $sql);


        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
              $send_count=0;
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {

                $cmp_name = $reply_to = '';
                $initial_check = 0;
                while ($row = mysqli_fetch_assoc($result)) {
                    if($initial_check==0){
                        $cmp_name = $row['company_name'];
                        $reply_to = $row['email_id'];                        
                        $initial_check++;
                    }
                    $output[] = $row;
                }
                    $footer_detail = $this->getunsubscribefooter($company_id);
                for ($idx = 0; $idx < count($output); $idx++) {
                    $obj = (Array) $output[$idx];
                    $user_emailid = $obj["student_email"];
                    $cc_email_list = $obj['student_cc_email'];
                    //echo "******count******".$idx;
                    $sendEmail_status = $this->sendEmail($user_emailid, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file,$cc_email_list,$company_id,'',$footer_detail);
                    if($sendEmail_status['status']=="true"){
                        $send_count++;
                        $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200);
                    }else{
                        $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200); // If no records "No Content" status
                    }
                }
                if($send_count-$countForBouncedEmail['Total_count'] == 0){
                    $push_msg="Email(s) Sent Successfully.";
                }
                elseif($send_count==0){
                    $push_msg="Email(s) failed due to invalid address";
                }else{
//                    $push_msg=$countForBouncedEmail['Total_count']-$send_count. ' of ' .$countForBouncedEmail['Total_count'].' email sending failed. Others sent successfully';
                    $failed_count=$countForBouncedEmail['Total_count']-$send_count;
                    $push_msg=$send_count ." sent. ".$failed_count." failed due to invalid address." ;
                }
//                if(empty($push_msg)){
//                    $push_msg="Email Sent Successfully";
//                }
                $msg = array('status' => "Success", "msg" => $push_msg);
                $this->response($this->json($msg), 200);
            } else {
                 if($send_count==0){
                    $push_msg="Email(s) failed due to invalid address";
                }
                $error = array('status' => "Failed", "msg" => $push_msg);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }    
    
    protected function getAttendanceDashboardDetails($company_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $attendance_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/attendance";
        
        $csv_export_details = $attended = $trial_attended=[];
        
        $sql = sprintf("SELECT `attendance_report_flag`, `attendance_report_frequency` FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $sql_result = mysqli_query($this->db, $sql);
        if(!$sql_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($sql_result)>0){
                $csv_export_details = mysqli_fetch_assoc($sql_result);
            }else{
                $error = array('status' => "Failed", "msg" => "Studio details not available.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
        
        $query1 = sprintf("select sum( not_attend_7_14) not_attend_7_14, sum( not_attend_15_21) not_attend_15_21,
                sum( not_attend_22_30) not_attend_22_30, sum( not_attend_31_60) not_attend_31_60, 
                sum(case when last_14_days <= 2 then 1 else 0 end) last_14_days, sum(case when last_30_days <= 2 then 1 else 0 end) last_30_days
                from attendance_dimension where company_id = '%s' group by company_id", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result1)>0){
                $attended = mysqli_fetch_assoc($result1);
            }
        }
        
        $query2 = sprintf("select sum( not_attend_7_14) not_attend_7_14, sum( not_attend_15_21) not_attend_15_21,
                sum( not_attend_22_30) not_attend_22_30, sum( not_attend_31_60) not_attend_31_60, 
                sum(case when last_14_days <= 2 then 1 else 0 end) last_14_days, sum(case when last_30_days <= 2 then 1 else 0 end) last_30_days
                from trial_attendance_dimension where company_id = '%s' group by company_id", mysqli_real_escape_string($this->db, $company_id));
        $result2 = mysqli_query($this->db, $query2);
        if(!$result2){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result2)>0){
                $trial_attended = mysqli_fetch_assoc($result2);
            }
        }
        $msg = array("status" => "Success", "msg" => array("csv" => $csv_export_details, "attendance" => $attended, "trial_attendance" => $trial_attended, "url" => $attendance_url));
        $this->response($this->json($msg), 200);
    }
    
    protected function updateAttendanceReportFrequencyDetails($company_id, $attendance_report_flag, $attendance_report_frequency){
        $update_query = sprintf("UPDATE `company` SET `attendance_report_flag`='%s', `attendance_report_frequency`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $attendance_report_flag),
                mysqli_real_escape_string($this->db, $attendance_report_frequency), mysqli_real_escape_string($this->db, $company_id));
        $result_update_query = mysqli_query($this->db, $update_query);
        if(!$result_update_query){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
                
        $query1 = sprintf("SELECT `attendance_report_flag`, `attendance_report_frequency` FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result1)>0){
                $attendance_report_details = mysqli_fetch_assoc($result1);
                $msg = array("status" => "Success", "msg" => "Attendance Report Frequency details updated successfully.", "report_frequency_details" => $attendance_report_details);
                $this->response($this->json($msg), 200);
            }else{
                $error = array('status' => "Failed", "msg" => "Studio details not available.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
    protected function downloadAttendanceReportAsCSV($company_id){
        $not_attend_7_14 = $not_attend_15_21 = $not_attend_22_30 = $not_attend_31_60 = $last_14_days = $last_30_days = [];
        $date_format = '%M %e, %Y';
        $query1 = sprintf("select mr.membership_registration_id, mr.buyer_name, concat(mr.membership_registration_column_1, ', ', mr.membership_registration_column_2) participant_name, 
		mr.buyer_phone, mr.buyer_email, IFNULL(mbr.rank_name, '') rank_value,
		IFNULL((SELECT DATE_FORMAT(checkin_datetime,'%s') FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id`and status='N' ORDER BY `checkin_datetime` DESC LIMIT 0,1), '') l_att,
                IFNULL(mbr.`required_attendance_count`, '') att_req, 
                IFNULL((SELECT SUM(1) FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `checkin_datetime`>=mr.`last_advanced_date` and status='N' GROUP BY `company_id`, `membership_registration_id`),0) act_att,
                IF(mbr.`required_attendance_count` IS NULL || mbr.`required_attendance_count`='', concat('total ', IFNULL((SELECT SUM(1) FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `checkin_datetime`>=mr.`last_advanced_date` and status='N' GROUP BY `company_id`, `membership_registration_id`),0)),
                concat(IFNULL((SELECT SUM(1) FROM `attendance` WHERE `company_id`=mr.`company_id` AND `membership_registration_id`=mr.`membership_registration_id` AND `checkin_datetime`>=mr.`last_advanced_date` and status='N' GROUP BY `company_id`, `membership_registration_id`),0), ' out of ', mbr.`required_attendance_count`)) att_count,
		sum( not_attend_7_14) not_attend_7_14, sum( not_attend_15_21) not_attend_15_21,
                sum( not_attend_22_30) not_attend_22_30, sum( not_attend_31_60) not_attend_31_60, 
                sum(case when last_14_days <= 2 then 1 else 0 end) last_14_days, sum(case when last_30_days <= 2 then 1 else 0 end) last_30_days
                from attendance_dimension ad left join membership_registration mr ON ad.company_id=mr.company_id and ad.membership_registration_id=mr.membership_registration_id
                left join membership_ranks mbr on mr.membership_id=mbr.membership_id and mr.rank_id=mbr.membership_rank_id
                where ad.company_id = '%s' group by ad.company_id, ad.membership_registration_id", $date_format, mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result1)>0){
                while($row = mysqli_fetch_assoc($result1)){
                    if($row['not_attend_7_14']>0){
                        $not_attend_7_14[] = $row;
                    }
                    if($row['not_attend_15_21']>0){
                        $not_attend_15_21[] = $row;
                    }
                    if($row['not_attend_22_30']>0){
                        $not_attend_22_30[] = $row;
                    }
                    if($row['not_attend_31_60']>0){
                        $not_attend_31_60[] = $row;
                    }
                    if($row['last_14_days']>0){
                        $last_14_days[] = $row;
                    }
                    if($row['last_30_days']>0){
                        $last_30_days[] = $row;
                    }
                }
                $all_details['Zero Attendance 7 to 14 days'] = $not_attend_7_14;
                $all_details['Zero Attendance 15 to 21 days'] = $not_attend_15_21;
                $all_details['Zero Attendance 22 to 30 days'] = $not_attend_22_30;
                $all_details['Zero Attendance 31+ days'] = $not_attend_31_60;
                $all_details['Attended 2 classes or less last 14 days'] = $last_14_days;
                $all_details['Attended 2 classes or less last 30 days'] = $last_30_days;
                $error = array('status' => "Success", "msg" => $all_details);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }else{
                $error = array('status' => "Failed", "msg" => "No Content");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
    public function sendVerficationDetailsEmail($company_id, $email) {
        $detailsquery = sprintf("SELECT `student_id`,`registration_status`,student_username FROM `student` WHERE  `company_id`='%s' and `student_email`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $detailsresult = mysqli_query($this->db, $detailsquery);
        if (!$detailsresult) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$detailsquery");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($detailsresult);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($detailsresult);
                if ($row['registration_status'] == 'S') {
                    $error = array('status' => "Failed", "msg" => "Account already verified");
                    $this->response($this->json($error), 200);
                } else {
                    $student_id = $row['student_id'];
                    $student_username = $row['student_username'];
                    $selectsql = sprintf("SELECT c.company_name, c.upgrade_status,rv.register_verification_id,rv.register_type, rv.last_updated_date FROM `company` c LEFT JOIN `register_verification` rv 
                    ON c.`company_id`= rv.`register_studio_id` WHERE c.`company_id` = '%s' AND `register_type`='S' AND `registered_type_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
                    $selectresult = mysqli_query($this->db, $selectsql);
                    if (!$selectresult) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $num_rows1 = mysqli_num_rows($selectresult);
                        if ($num_rows1 > 0) {
                            $row1 = mysqli_fetch_assoc($selectresult);
                            $register_verification_id = $row1['register_verification_id'];
                            $user_type = $row1['register_type'];
                            $company_name = $row1['company_name'];
                        } else {
                            $error = array('status' => "Failed", "msg" => "Invalid email entered or user not upgraded to latest version.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    $verification_code = mt_rand(100000, 999999);  // GENERATING RANDOM VERIFICATION CODE
                    $updatequery = sprintf("UPDATE `register_verification` SET `registration_code` = '%s' WHERE `register_verification_id` = '%s' AND `registered_type_id` = '%s'", mysqli_real_escape_string($this->db, $verification_code), mysqli_real_escape_string($this->db, $register_verification_id), mysqli_real_escape_string($this->db, $student_id));
                    $updateresult = mysqli_query($this->db, $updatequery);
                    if (!$updateresult) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatequery");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $verification_id = $register_verification_id;
                        $code = md5($verification_code);
                        $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                        $curr_loc = implode('/', explode('/', $curr_loc, -3));
                        $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/Api/MemberappActivation.html?token=$code&i=$verification_id&t=$user_type";
                        $this->sendActivationLink($email, $company_name, $verification_code, $verification_id, $user_type, $student_username);
                    }
                    $msg = array("status" => "Success", "msg" => "Verification Code and Link generated successfully", "verification_link" => $activation_link, "verification_code" => $verification_code);
                    $this->response($this->json($msg), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid email entered");
                $this->response($this->json($error), 200);
            }
        }
    }

    // NEW REGISTER FUNCTIONS
    protected function verifyUserEmailInDb($user_email,$call_back) {
        $query = sprintf("SELECT  user_email from user where user_email='%s' ", mysqli_real_escape_string($this->db, $user_email));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
           log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if($call_back==0){
               $this->response($this->json($error), 500);
            }else{
                return $error;
            }
        }else{
             $num_of_rows = mysqli_num_rows($result);
             if ($num_of_rows > 0) {
                $error = array('status' => "Failed", "msg" => "User email already exists.");
                if($call_back==0){
                $this->response($this->json($error), 200);
                }else{
                    return 'exist';
                }
            } else {
                $out = array('status' => "Success", 'msg' => "User email available.");
                if($call_back==0){
                $this->response($this->json($out), 200);
                }else{
                    return 'not_exist';
                }
            }
        }
    }
    

    protected function verifyCompanyCodeInDb($company_code,$call_back) {
        $sql = sprintf("SELECT company_code from company where company_code='%s' ", mysqli_real_escape_string($this->db, $company_code));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
            if($call_back==0){
               $this->response($this->json($error), 500);
            }else{
                return $error;
            }
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                if ($call_back == 0) {
                    $error = array('status' => "Failed", "msg" => "Company code already exists.");
                    $this->response($this->json($error), 200);
                } else {
                  return 'N';
                }
               
            } else {
                if ($call_back == 0) {
                   $out = array('status' => "Success", 'msg' => "Company code available.");
                   $this->response($this->json($out), 200);
                } else {
                   return 'Y' ;
                }
                
            }
        }
    }

    public function sendActivationLink($student_email, $company_name, $verification_code, $verification_id, $user_type, $student_username) {
        $message = '';
        $code = md5($verification_code);
        $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $curr_loc = implode('/', explode('/', $curr_loc, -3));
        $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/Api/MemberappActivation.html?token=$code&i=$verification_id&t=$user_type";
        $message .= "Hello $student_username,<br><br>";
        $message .= "<a href='$activation_link'>" . "Click here to activate your account." . "</a><br>";
        $message .= "(or) Use the following verification code to activate your account: ";
        $message .= "<b>$verification_code</b><br>";
        $sendEmail_status = $this->sendEmail($student_email, 'App Verification Code', $message, $company_name, '', '','','','','',''); 
        if ($sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $student_email, "Email Message" => $sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $student_email, "Email Message" => $sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        }
    }


        

    protected function registerWebAppUserInDb($user_email, $user_password, $user_firstname, $user_lastname, $country, $company_name, $web_page, $phone_number, $company_type, $user_phone, $company_code, $user_phone_country) {

        $query = sprintf("SELECT  user_email from user where user_email='%s'", mysqli_real_escape_string($this->db, $user_email));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $error = array('status' => "Failed", "msg" => "User already exists.");
                $this->response($this->json($error), 200);
            } else {
                $currency_code = $currency_symbol = '';
                if (strtoupper($country) == 'CA') {
                    $currency_code = 'CAD';
                    $currency_symbol = 'CAD$';
                } elseif (strtoupper($country) == 'UK') {
                    $currency_code = 'GBP';
                    $currency_symbol = '£';
                } elseif (strtoupper($country) == 'AU') {
                    $currency_code = 'AUD';
                    $currency_symbol = '$';
                } else {
                    $currency_code = 'USD';
                    $currency_symbol = '$';
                }
                $company_check=$this->verifyCompanyCodeInDb($company_code,1);
                if($company_check == 'N'){
                    $error = array('status' => "Failed", "msg" => "company code already exists.");
                    $this->response($this->json($error), 200);
                }
                $insert_company = sprintf("INSERT INTO `company` (`company_code`,`phone_number`,`country`,`company_name`,`company_type`,`web_page`,`wp_currency_code`,`wp_currency_symbol`,`expiry_dt`,registration_complete) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s',DATE_ADD(NOW(), INTERVAL 14 DAY),'Y')", mysqli_real_escape_string($this->db, $company_code), mysqli_real_escape_string($this->db, $phone_number), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_name), mysqli_real_escape_string($this->db, $company_type), mysqli_real_escape_string($this->db, $web_page), $currency_code, $currency_symbol);
                $company_id = '';
                $result_company = mysqli_query($this->db, $insert_company);
                if (!$result_company) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_company");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } else {
                    $company_id = mysqli_insert_id($this->db);
                    $insert_pos_settings = sprintf("INSERT INTO `studio_pos_settings`(`company_id`) VALUES ('$company_id')");
                    $result_pos_settings = mysqli_query($this->db, $insert_pos_settings);
                    if(!$result_pos_settings){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_pos_settings");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $insert_user = sprintf("INSERT INTO `user` (`user_email`,`user_password`,`user_firstname`,`user_lastname`,`user_phone`,`company_id`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $user_email), mysqli_real_escape_string($this->db, md5($user_password)), mysqli_real_escape_string($this->db, $user_firstname), mysqli_real_escape_string($this->db, $user_lastname), mysqli_real_escape_string($this->db, $user_phone), $company_id);
                    $result_user = mysqli_query($this->db, $insert_user);
                    if (!$result_user) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_user");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                        $curr_loc = implode('/', explode('/', $curr_loc, -3));
                        $img_url = $message = "";
                        $img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://')."$curr_loc/WebPortal/image/welcome_email_poster.png";
                        $message .= "<html>";
                        $message .= "<head>";
                        $message .= "<title>Registration Welcome</title>";
                        $message .= "<meta charset='UTF-8'>";
                        $message .= "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
                        $message .= "<style type='text/css'>";
                        $message .= "body{font-family:avenir !important;font-size:12px;font-weight:normal;letter-spacing:normal;line-height:normal;padding:20px;}";
                        $message .= "img{width:600px;}";
                        $message .= "p{font-size:18px;font-weight:500;margin:0;}";
                        $message .= "h1{font-size:18px;font-weight:900;line-height:1.3;letter-spacing:0.5px;margin:0;}";
                        $message .= "a{color:#009a61 !important; text-decoration: none;}";
                        $message .= "h1 > a{font-size: 15px;font-weight: 900;}";
                        $message .= ".content-left{text-align:left;margin-left: 60px;}";
                        $message .= "h1+h1{margin-top: 10px;}";
                        $message .= ".content-left > h1{font-size:18px;font-weight: 800;}";
                        $message .= ".content-left > h1+p{margin-top: 10px;font-size:18px;}";
                        $message .= ".blue{color:#4a90e2;font-weight: 600;}";
                        $message .= "p>a{font-weight: 600;}";
                        $message .= ".container{width:600px;margin:auto;}";
                        $message .= "</style>";
                        $message .= "</head>";
                        $message .= "<body style='font-family:avenir !important;font-size:12px;font-weight:normal;letter-spacing:normal;line-height:normal;padding:20px;color:#111111 !important;'>";
                        $message .= "<div class='container' style='width:600px !important;margin:auto !important;'>";
                        $message .= "<img style='width:600px;' src='$img_url'><br><br><br><br>";
                        $message .= "<p style='font-family:avenir !important;color:#111111 !important;'>Hey $user_firstname!<br><br></p>";
                        $message .= "<p style='font-family:avenir !important;color:#111111 !important;'>Welcome to the MyStudio family! Let’s get you started.<br><br></p>";
                        $message .= "<h1 style='font-family:avenir !important;color:#111111 !important;font-size:18px !important;font-weight:900 !important;'>STEP 1</h1>";
                        $message .= "<p style='font-family:avenir !important;color:#111111 !important;'>Manage your business and customize your member app at <br><a href='https://www.mystudio.app' target='_blank' style='color:#009a61 !important;text-decoration: none !important;'> www.mystudio.app</a>.<br><br></p>";
                        $message .= "<h1 style='font-family:avenir !important;color:#111111 !important;font-size:18px !important;font-weight:900 !important;'>STEP 2</h1>";
                        $message .= "<p style='font-family:avenir !important;color:#111111 !important;'>Members see your app by downloading the MyStudio app.<br> Provide them with these download links:<br><br></p>";
                        $message .= "<div class='content-left' style='text-align:left !important;margin-left: 60px;!important'>";
                        $message .= "<h1 style='font-family:avenir !important;color:#111111 !important;font-size:18px;font-weight:800;'>Apple App Store - <a href='https://itunes.apple.com/us/app/mystudio-app/id1258207230?mt=8' target='_blank' style='color:#009a61 !important;font-size: 15px !important;text-decoration: none !important;'>https://itunes.apple.com/us/app/<br>mystudio-app/id1258207230?mt=8</a></h1>";
                        $message .= "<h1 style='font-family:avenir !important;color:#111111 !important;font-size:18px;font-weight:800;'>Google Play  - <a href='https://play.google.com/store/apps/details?id=com.mystudio.app' target='_blank' style='color:#009a61 !important;font-size: 15px !important;text-decoration: none !important;'>https://play.google.com/store/apps/<br>details?id=com.mystudio.app</a></h1>";
                        $message .= "<p style='font-family:avenir !important;color:#111111 !important;'>Provide your studio code — <span class='blue' style='color:#4a90e2 !important;font-weight: 600 !important;'>$company_code</span> — to your<br></p>";
                        $message .= "<p style='font-family:avenir !important;color:#111111 !important;'> members so they can access your app.<br><br></p>";
                        $message .= "</div>";
                        $message .= "<p style='font-family:avenir !important;color:#111111 !important;'>Visit our <a href='https://intercom.help/mystudio' target='_blank'style='color:#009a61 !important;text-decoration: none !important;'>Help Center</a>  to learn more about how to use MyStudio.<br><br></p>";
                        $message .= "<p style='font-family:avenir !important;color:#111111 !important;'>Also, join our Facebook group <a href='https://www.facebook.com/groups/567299240302290/' target='_blank' style='color:#009a61 !important;text-decoration: none !important;'>here</a> and learn how to grow your<br> businsess from other MyStudio operators today.<br><br></p>";
                        $message .= "<p style='font-family:avenir !important;color:#111111 !important;'>Sincerely,<br>The MyStudio Team<br>www.MyStudio.app </p>";
                        $message .= "<div>"; 
                        $message .= "</body>";
                        $message .= "</html>";
                        $sendEmail_status = $this->sendEmail($user_email, 'Welcome to MyStudio! Now What?', $message, $company_name, '', '', '', '','','','');
                        if ($sendEmail_status['status'] == "true") {
                            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $user_email, "Email Message" => $sendEmail_status['mail_status']);
                            log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200);
                        } else {
                            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $user_email, "Email Message" => $sendEmail_status['mail_status']);
                            log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200); // If no records "No Content" status
                        }
                        // CHECK STRIPE COUNTRY SUPPORT
                        $country_support = 'N';
                        $country_payment_specific = $this->checkCountryPaymentSupport($company_id, $country, 1); // callback - 1/0
                        if($country_payment_specific['status'] == 'Success'){
                            if ($country_payment_specific['payment_support'] === 'S' && $country_payment_specific ['support_status'] == 'Y') {
                                $country_support = 'Y';
                                // CREATE STRIPE CONNECTED ACCOUNT
                                $this->createstripeconnectedaccount($company_id, $company_code, $user_email, 1);
                            } else {
                                $country_support = 'N';
                                if($country_payment_specific['payment_support'] === 'W' && $country_payment_specific ['support_status'] == 'Y'){
                                    // CREATE WEPAY ACCOUNT
                                    $this->wepayUserTokenGeneration($company_id, $user_firstname, $user_lastname, $user_email);
                                }
                            }
                        }
                        $out = array('status' => "Success", 'msg' => "Webapp Activated Successfully", 'company_id'=>"$company_id", 'stripe_country_support'=>"$country_support");
                        // If success everythig is good send header as "OK" and user details
                        $this->responseWithSalesforce($this->json($out), 200);
                        $this->registerCompanyinSalesforceApi($company_id, $company_name, $user_email, $phone_number, $user_firstname, $user_lastname, $user_phone, $company_type, $user_phone_country);
                    }
                }
            }
        }
    }

    public function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }

    public function verifyBouncedEmailForCC($cc_address, $company_id){
        $selecsql=sprintf("SELECT * FROM `bounce_email_verify` b WHERE b.`bounced_mail`='%s' AND (b.`company_id`=0 OR b.`company_id`='%s')", mysqli_real_escape_string($this->db,$cc_address), mysqli_real_escape_string($this->db,$company_id));
        $result= mysqli_query($this->db,$selecsql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
          if(mysqli_num_rows($result)>0){
              return 0;
          }else{
               return 1;
          }
        }
    }
    
    public function unblockbouncedemail($company_id,$bounced_email){
        $count=1;
        $selectsql=sprintf("SELECT `bounced_mail`, `count` FROM `bounce_email_check` WHERE `bounced_mail`='%s'", mysqli_real_escape_string($this->db,$bounced_email));
        $resultselectsql= mysqli_query($this->db, $selectsql);
        if(!$resultselectsql){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.","sql" => "$selectsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($resultselectsql)>0){
                $rows1 = mysqli_fetch_assoc($resultselectsql);
                if ($rows1['count'] >= 5) {
                   $out = array('status' => "Success", 'msg' => "You can't unblock your email. Bounced or Compliant more then two times");
                   $this->response($this->json($out), 200);
                }
                $count = $rows1['count'] + 1;
            }
           
            
            $sql1 = sprintf("INSERT INTO `bounce_email_check`(`bounced_mail`, count) VALUES('%s', '%s') ON DUPLICATE KEY UPDATE `count`='%s'",
                        mysqli_real_escape_string($this->db, $bounced_email), mysqli_real_escape_string($this->db, $count), mysqli_real_escape_string($this->db, $count));
            $result1 = mysqli_query($this->db, $sql1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "sql" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }            
            
            $sql = sprintf("DELETE FROM `bounce_email_verify` WHERE `bounced_mail`='%s'", mysqli_real_escape_string($this->db, $bounced_email));
            $result = mysqli_query($this->db, $sql);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $out = array('status' => "Success", 'msg' => "email unblocked successfully");
                $this->response($this->json($out), 200);
            }
        }
    }
    
   public function checkBouncedEmailCount($reg_id_list){
       
        $sql=sprintf("SELECT count(*) as Total_count , sum(if(s.deleted_flag='Y',1,0)) as deleted_count,count(`bounced_mail`) as Bounced_count FROM `student` s  left JOIN `bounce_email_verify` b ON  s.`student_email` = b.`bounced_mail` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
            where  s.`student_id`  in (%s);",mysqli_real_escape_string($this->db,$reg_id_list));
        $result= mysqli_query($this->db, $sql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
             if(mysqli_num_rows($result)>0){
                 $rows = mysqli_fetch_assoc($result);
                 return $rows;
             }
        }
    }
    
    public function sendBounceEmail_check($cc_address){
        $selecsql=sprintf("SELECT * FROM `bounce_email_verify` WHERE `bounced_mail`='%s'", mysqli_real_escape_string($this->db,$cc_address));
        $result= mysqli_query($this->db,$selecsql);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result)>0){
                $error = array('status' => "Failed", "msg" => "Invalid email or bounced email");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function checkAndUpdateCampaignEmailStatusForUpgradeStatus($company_id) {
        $query = sprintf("SELECT * FROM `campaign` WHERE `company_id`='%s' AND `deleted_flag`='N' AND `status`='E'", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $update_query = sprintf("UPDATE `campaign` SET `status`='D' WHERE `company_id`='%s' AND `deleted_flag`='N' AND `status`='E'", mysqli_real_escape_string($this->db,$company_id));
                $update_result = mysqli_query($this->db, $update_query);
                if(!$update_result){           
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                    log_info($this->json($error_log));
                }
            }
        }
    }
    
    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
    
    //send payment details to Stripe API
    protected function getSubscriptionPlanDetails($company_id, $user_payment_type, $call_back){
        
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        
        $curr_date = date("Y-m-d");
        $to_upgrade = 0;
        $upgrade_to=[];
        $sql = sprintf("select date(`expiry_dt`) expiry, `subscription_status`, `upgrade_status` from `company` c where c.`company_id`='%s'",mysqli_real_escape_string($this->db,$company_id));
        $sql_result = mysqli_query($this->db, $sql);
        if(!$sql_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 0) {
                $this->response($this->json($error), 200);
            } else if ($call_back == 1) {
                return $error;
            }
        }else{
            $sql_num_rows = mysqli_num_rows($sql_result);
            if($sql_num_rows>0){
                $sql_row = mysqli_fetch_assoc($sql_result);
                $expiry = $sql_row['expiry'];
                $sub_status = $sql_row['subscription_status'];
                $upgrade_status = $sql_row['upgrade_status'];

                $to_upgrade = $this->checkCurrentAndNextPlan($upgrade_status, $user_payment_type); //upgrade_status - old plan, user_payment_type - new plan
                
                if($to_upgrade==1){
                    $result_arr=$upgrade_plan=$plan_arr=[];
                    $payment_type='';
                    if ($call_back == 0) {
                        if ($user_payment_type == 'B') {
                            $upgrade_to = ['B'];
                        } else if ($user_payment_type == 'M' || $user_payment_type == 'P') {
                            if ($upgrade_status == 'M') {
                                $upgrade_to = ['P'];
                            } else {
                                $upgrade_to = ['M', 'P'];
                            }
                        } else if ($user_payment_type == 'W' || $user_payment_type == 'WM') {
                            if ($upgrade_status == 'WM') {
                                $upgrade_to = ['W'];
                            } else {
                                $upgrade_to = ['W', 'WM'];
                            }
                        }
                    } else if ($call_back == 1) {
                        $upgrade_to[] = $user_payment_type;
                    }

                    $subscription_fee=$this->getSubscriptionPlanFee($company_id,$upgrade_status);
                    for ($init = 0; $init < count($upgrade_to); $init++) {
                        $payment_type=$upgrade_to[$init];
                        $per_day_payment_sss=$rem_days_sss=$total_days_in_prev_plan=$total_days_in_curr_plan=$plan_fee=$setup_fee=$total_days_in_new_plan=$new_plan_fee=$new_plan_offer_fee=0;
                        $rem_day_payment_sss=$prorate_days_payment=$prorate_days=$credit_days=0;
                        $new_plan_setup_fee=$credited_days=$calculated_payment_amount_temp=$calculated_payment_amount_temp1=$monthly_rem_day_payment=$monthly_rem_days=$monthly_per_day_payment=0;
                        $new_plan_premium_type=$future_payment_date='';
                        
                        if(!empty($subscription_fee)){
                            $plan_fee=$subscription_fee['current_plan_fee'];
                            $setup_fee=$subscription_fee['current_plan_setup_fee'];
                            $total_days_in_prev_plan=$subscription_fee['current_plan_days'];
                            
                            for($i=0;$i<count($subscription_fee['plans']);$i++){                                
                                if($payment_type == $subscription_fee['plans'][$i]['premium_type']){
                                    $new_plan_premium_type = $subscription_fee['plans'][$i]['premium_type'];
                                    $new_plan_fee = $subscription_fee['plans'][$i]['plan_fee'];
                                    $new_plan_offer_fee = $subscription_fee['plans'][$i]['plan_offer_fee'] + 0;
                                    $total_days_in_new_plan = $subscription_fee['plans'][$i]['plan_days'];
                                    $new_plan_setup_fee = $subscription_fee['plans'][$i]['setup_fee'];
                                    break;
                                }
                            }
                        }else{
                           $error = array('status' => "Failed", "msg" => "Unable to get the payment plan details.");
                           $this->response($this->json($error), 200); 
                        }
                                    
                        if (($upgrade_status=='F'|| $upgrade_status=='T') && $sub_status != 'Y') {               // SUBSCRIBE TO NEW PLAN FIRST TIME
 
                            //upgrade to new plan first time
                                          
                            if ($payment_type == 'W') {         // W - White Label
                                if($upgrade_status=='T'){
                                    $calculated_payment_amount = ($new_plan_fee-$new_plan_offer_fee) + $new_plan_setup_fee + 0;
                                }else{
                                    $calculated_payment_amount = $new_plan_fee + $new_plan_setup_fee + 0;
                                }
                                $future_payment_date = date('Y-m-d', strtotime("+1 year"));
                                $payment_desc = "MyStudio White Label Membership";
                                $plan_arr['setup_fee'] = $new_plan_setup_fee;
                                $plan_arr['prorated_fee'] = 0;
                            } elseif ($payment_type == 'P') {        // P - Premium Plan Yearly
                                if($upgrade_status=='T'){
                                    $calculated_payment_amount = ($new_plan_fee-$new_plan_offer_fee) + 0;
                                }else{
                                    $calculated_payment_amount = $new_plan_fee + 0;
                                }
                                $future_payment_date = date('Y-m-d', strtotime("+1 year"));
                                $payment_desc = "MyStudio Premium Yearly Membership";
                                $plan_arr['setup_fee'] = 0;
                                $plan_arr['prorated_fee'] = 0;
                            } elseif($payment_type == 'WM' || $payment_type == 'M' || $payment_type == 'B'){    // Monthly Plans
                                
                                $curr_day = date("j");
                                $no_of_days_in_current_month = date("t");
                                if ($curr_day != '1') {
                                    $monthly_rem_days = $no_of_days_in_current_month - $curr_day + 1;
                                    $monthly_per_day_payment = (float) ($new_plan_fee / 30);
                                    $monthly_rem_day_payment = number_format((float) ($monthly_per_day_payment * $monthly_rem_days), 2, '.', '');
                                    $calculated_payment_amount_temp = $monthly_rem_day_payment + $new_plan_setup_fee;
                                    if ($calculated_payment_amount_temp < 1) {
                                        $calculated_payment_amount = number_format($calculated_payment_amount_temp + $new_plan_fee, 2, '.', '');
                                        $future_payment_date = date('Y-m-d', strtotime('first day of second month'));
                                    } else {
                                        $calculated_payment_amount = number_format($calculated_payment_amount_temp, 2, '.', '');
                                        $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
                                    }
                                    $plan_arr['prorated_fee'] = number_format(($new_plan_fee + $new_plan_setup_fee) - $calculated_payment_amount_temp, 2, '.', '');
                                } else {
                                    $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
                                    $calculated_payment_amount = $new_plan_fee + $new_plan_setup_fee;
                                    $plan_arr['prorated_fee'] = 0;
                                }
                                $plan_arr['setup_fee'] = $new_plan_setup_fee;                                
                                
                                if ($payment_type == 'M') {        // P - Premium Plan Monthly
                                    $payment_desc = "MyStudio Premium Monthly Membership";
                                }elseif($payment_type == 'WM') {   //WM- white label monthly
                                    $payment_desc = "MyStudio White Label Monthly Membership";
                                }else{
                                    $payment_desc = "MyStudio Basic Monthly Membership";
                                }
                            }
                        }else if ($expiry >= $curr_date && $sub_status == 'Y' && $upgrade_status!=='F' && $upgrade_status!=='T') {  //UPDATE PLAN BEFORE EXPIRY DATE
                                //UPGRADE FROM OLD PLAN
                            $rem_days_sss = (strtotime($expiry) - strtotime($curr_date)) / 86400;
                            $per_day_payment_sss = number_format((float) ($plan_fee / $total_days_in_prev_plan), 2, '.', '');
                            $rem_day_payment_sss = number_format((float) ($per_day_payment_sss * $rem_days_sss), 2, '.', '');
                            stripe_log_info("Old_credit : $rem_day_payment_sss");
                            
                            if ($payment_type == 'W') {         // W - White Label
                                
                                if($upgrade_status=='WM'){
                                    $calculated_payment_amount_temp = number_format($new_plan_fee-$rem_day_payment_sss, 2, '.', '');
                                    $calculated_payment_amount= number_format($calculated_payment_amount_temp, 2, '.', '');
                                    $plan_arr['prorated_fee']=number_format($new_plan_fee -$calculated_payment_amount, 2, '.', '');
                                    $plan_arr['setup_fee']=0;
                                }else{
                                    $calculated_payment_amount_temp = number_format($new_plan_fee+$new_plan_setup_fee-$rem_day_payment_sss, 2, '.', '');
                                    $calculated_payment_amount= number_format($calculated_payment_amount_temp, 2, '.', '');
                                    $plan_arr['prorated_fee']=number_format($new_plan_fee +$new_plan_setup_fee-$calculated_payment_amount_temp, 2, '.', '');
                                    $plan_arr['setup_fee']=$new_plan_setup_fee;
                                }
                                $future_payment_date = date('Y-m-d', strtotime($curr_date . "+1 year"));
                                $payment_desc = "MyStudio White Label Yearly Membership";
                            } elseif ($payment_type == 'P') {        // P - Premium Plan Yearly
                                
                                $calculated_payment_amount_temp = number_format($new_plan_fee - $rem_day_payment_sss, 2, '.', '');
                                $calculated_payment_amount= number_format($calculated_payment_amount_temp, 2, '.', '');
                                $plan_arr['prorated_fee']=number_format($new_plan_fee -$calculated_payment_amount_temp, 2, '.', '');
                                $future_payment_date = date('Y-m-d', strtotime($curr_date . "+1 year"));
                                $payment_desc = "MyStudio Premium Yearly Membership";
                                $plan_arr['setup_fee']=0;
//                                stripe_log_info("temp_pay = $temp_payment_amount, r_pro = $r_prorate, calc_pay_temp = $calculated_payment_amount_temp, calc_pay = $calculated_payment_amount, fut=$future_payment_date ");
                                stripe_log_info("calc_pay_temp = $calculated_payment_amount_temp, calc_pay = $calculated_payment_amount, fut=$future_payment_date, pro = ".$plan_arr['prorated_fee']);
                            } elseif ($payment_type == 'M') {        // M - Premium Plan Monthly
                                
                                $curr_day = date("j");
                                $no_of_days_in_current_month = date("t");
                                if ($curr_day != '1') {
                                    $monthly_rem_days = $no_of_days_in_current_month - $curr_day + 1;
                                    $monthly_per_day_payment = number_format((float)($new_plan_fee / 30), 2, '.', '');
                                    $monthly_rem_day_payment = number_format((float) ($monthly_per_day_payment * $monthly_rem_days), 2, '.', '');
                                    $temp_payment_amount = $monthly_rem_day_payment;
                                    $r_prorate = ($new_plan_fee-$monthly_rem_day_payment); 
                                } else {
                                    $temp_payment_amount = $new_plan_fee;
                                    $r_prorate = 0;
                                }                   
                                
                                $calculated_payment_amount_temp = $temp_payment_amount - $rem_day_payment_sss;
                                if($calculated_payment_amount_temp<1){
                                    $calculated_payment_amount= number_format($calculated_payment_amount_temp+$new_plan_fee, 2, '.', '');
                                    $future_payment_date = date('Y-m-d', strtotime('first day of second month'));
                                }else{
                                    $calculated_payment_amount= number_format($calculated_payment_amount_temp, 2, '.', '');
                                    $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
                                }
                                $final_prorate = $r_prorate+$rem_day_payment_sss;
                                $plan_arr['prorated_fee']=number_format($final_prorate, 2, '.', '');                                
                                $payment_desc = "MyStudio Premium Monthly Membership";
                                $plan_arr['setup_fee']=0;    
                            } elseif ($payment_type == 'WM') {        //WM - White Label Plan Monthly
                                
                                $curr_day = date("j");
                                $no_of_days_in_current_month = date("t");
                                $monthly_per_day_payment = number_format((float) ($new_plan_fee / 30), 2, '.', '');
                                if ($curr_day != '1') {
                                    $monthly_rem_days = $no_of_days_in_current_month - $curr_day + 1;
                                    $monthly_rem_day_payment = number_format((float) ($monthly_per_day_payment * $monthly_rem_days), 2, '.', '');
                                    $temp_payment_amount = $monthly_rem_day_payment;
                                    $r_prorate = ($new_plan_fee-$monthly_rem_day_payment); 
                                } else {
                                    $temp_payment_amount = $new_plan_fee;
                                    $r_prorate= 0;
                                }
                                
                                if($rem_day_payment_sss>0){
                                    $calculated_payment_amount_temp1 = $temp_payment_amount+$new_plan_setup_fee - $rem_day_payment_sss; ///(30+199)-250=-21     //else (30+199)-100=129
                                }else{
                                    $calculated_payment_amount_temp1 = $temp_payment_amount+$new_plan_setup_fee;
                                }
                                if($calculated_payment_amount_temp1<1){
                                    $credit_days=number_format((float)(abs($calculated_payment_amount_temp1)/$monthly_per_day_payment), 0, '.', '');//21/2.3 = 8.69days
                                    $prorate_days=$total_days_in_new_plan-$credit_days;
                                    $prorate_days_payment = number_format((float) ($monthly_per_day_payment * $prorate_days), 2, '.', '');
                                    $calculated_payment_amount_temp11=$prorate_days_payment;
                                    if($calculated_payment_amount_temp11>0){
                                        $future_payment_date = date('Y-m-d', strtotime('first day of second month'));
                                    }else{
                                        $calculated_payment_amount_temp11=$new_plan_fee+$calculated_payment_amount_temp11;
                                        $future_payment_date = date('Y-m-d', strtotime('first day of third month'));
                                    }
                                }else{
                                    $calculated_payment_amount_temp11 = number_format($calculated_payment_amount_temp1, 2, '.', '');
                                    $future_payment_date = date('Y-m-d', strtotime('first day of next month'));
                                }
                                $calculated_payment_amount = number_format($calculated_payment_amount_temp11, 2, '.', '');
                                $final_prorate = $r_prorate;
                                $plan_arr['prorated_fee'] = number_format($final_prorate, 2, '.', '');
                                $payment_desc = "MyStudio White Label Monthly Membership";
                                $plan_arr['setup_fee']=$new_plan_setup_fee;
                                stripe_log_info("temp_pay = $temp_payment_amount, r_pro = $r_prorate, calc_pay_temp = $calculated_payment_amount_temp1, calc_pay = $calculated_payment_amount, fut=$future_payment_date, pro_rate = ".$plan_arr['prorated_fee']);
                                stripe_log_info("prorate_days_payment = $prorate_days_payment, calc_pay_temp11 = $calculated_payment_amount_temp11,  prorate_days= $prorate_days, credit days =$credit_days ");
                            }
                        }else{
                            $error = array('status' => "Failed", "msg" => "Something went wrong in your subscription plan. Please contact your administrator.");
                            $this->response($this->json($error), 200);
                        }                                
                        $plan_arr['plan_type']=$payment_type;
                        $plan_arr['plan_fee']=$new_plan_fee;
                        $plan_arr['plan_offer_fee']=$new_plan_offer_fee;
                        $plan_arr['total_due']=$calculated_payment_amount;
                        $plan_arr['next_payment_date']=$future_payment_date;
                        $plan_arr['payment_desc']=$payment_desc;
                        if($payment_type=='W'||$payment_type=='P'||$payment_type=='B'){
                            $upgrade_plan['yearly']=$plan_arr;
                        }else{
                            $upgrade_plan['monthly']=$plan_arr;
                        }
                        $result_arr=$plan_arr;
                    }
                    if(!empty($upgrade_plan)){
                        if($call_back == 0) {
                            $out = array('status' => "Success", "msg" => $upgrade_plan, "current_status" => $upgrade_status);
                            $this->response($this->json($out), 200);
                        } else if ($call_back == 1 && !empty($result_arr)) {
                            return $result_arr;
                        }else{
                            $error = array('status' => "Failed", "msg" => "Something went wrong in your subscription plan Due Calculation. Please contact your administrator.");
                            return $error;
                        }
                    }else{
                        $error = array('status' => "Failed", "msg" => "Something went wrong in your subscription plan Due Calculation. Please contact your administrator.");
                        if($call_back == 0) {                            
                            $this->response($this->json($error), 200);
                        } else if ($call_back == 1) {
                            return $error;
                        }
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => "Cannot upgrade plan. Please contact your administrator.");
                    if($call_back == 0) {                        
                        $this->response($this->json($error), 200);
                    } else if ($call_back == 1) {
                        return $error;
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
                if ($call_back == 0) {                    
                    $this->response($this->json($error), 200); // If no records "No Content" status
                } else if ($call_back == 1) {
                    return $error;
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    protected function updateStudioSubscriptionPlan($company_id,$user_id, $payment_type, $payee_type, $charge_flag, $lead_source_temp, $referer_name_temp){
//        $current_date = date("Y-m-d H:i:s");
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $current_date = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $lead_source=mysqli_real_escape_string($this->db,$lead_source_temp);
        $referer_name=mysqli_real_escape_string($this->db,$referer_name_temp);
        $this->getStripeKeys();
        $curr_date = date("Y-m-d");
        $to_upgrade = 0;
        $future_payment_date=$curr_date;
        $stripe_customer_id = $stripe_card_id = $stripe_charge_id =$ref_string=$stripe_old_card_id ='';        
        $charge_type='free';
        $stripe_payment_status="old_credit";
        
        if($payment_type!='P' && $payment_type!='W' && $payment_type!='M'&& $payment_type!='WM'&& $payment_type!='B'){
            $error_log = array('status' => "Failed", "msg" => "Invalid Payment Parameters.", "payment_type" => "$payment_type");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Invalid Payment Parameters. Payment type error.");
            $this->response($this->json($error), 200);
        }
        
        $sql = sprintf("select `company_name`, `web_page`, date(`expiry_dt`) expiry, `subscription_status`, `upgrade_status`, `list_item_id`, `contact_id`, `stripe_currency_code`, `stripe_currency_symbol`, s.`stripe_customer_id`, s.`stripe_source_id`,s.`sss_id`,
                s.`outstanding_balance`, s.`next_payment_date`, s.`last_successfull_payment_date`, s.`expiry_date`,c.stripe_last_success_payment
                from `company` c LEFT JOIN `stripe_studio_subscription` s ON c.`company_id` = s.`company_id`
                where c.`company_id`='%s' AND c.`stripe_subscription`='Y' AND s.deleted_flag!='Y' AND s.`subscription_end_date` IS NULL ORDER BY s.sss_id DESC LIMIT 0,1",mysqli_real_escape_string($this->db,$company_id));
        $sql_result = mysqli_query($this->db, $sql);
        if(!$sql_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $sql_num_rows = mysqli_num_rows($sql_result);
            if($sql_num_rows>0){
                $sql_row = mysqli_fetch_assoc($sql_result);
                $expiry = $sql_row['expiry'];
                $sub_status = $sql_row['subscription_status'];
                $upgrade_status = $sql_row['upgrade_status'];
                $stripe_currency_code = $sql_row['stripe_currency_code'];
                $stripe_currency_symbol = $sql_row['stripe_currency_symbol'];
                $customer_id = $sql_row['stripe_customer_id'];
                $card_id = $sql_row['stripe_source_id'];
                $stripe_reg_id = $sql_row['sss_id'];
                $outstanding_balance = $sql_row['outstanding_balance'];
                $next_payment_date =$sql_row['next_payment_date'];
                $last_successfull_payment_date=$sql_row['last_successfull_payment_date'];
                $st_expiry_date=$sql_row['expiry_date'];
                $stripe_last_success_payment = $sql_row['stripe_last_success_payment'];
                if($upgrade_status=='F' || $upgrade_status=='T'){
                    $u_status = "'F', 'T'";
                }else{
                    $u_status = "'$upgrade_status'";
                }
                
                $to_upgrade = $this->checkCurrentAndNextPlan($upgrade_status, $payment_type); //upgrade_status - old plan, payment_type - new plan
                if($to_upgrade==1){
                    $u_status = "'$upgrade_status'";
                    if($upgrade_status=='T' || $upgrade_status=='F'){
                        $u_status = "'T', 'F'";
                    }
                    $sql_sss = sprintf("SELECT `sss_id`, `stripe_customer_id`, `premium_type`, `payment_amount`, `expiry_date`, `status`
                            FROM `stripe_studio_subscription` s
                            WHERE s.`company_id`='%s' AND s.`premium_type` IN (%s) AND s.`status`='A' order by s.`sss_id` desc limit 0,1",
                            mysqli_real_escape_string($this->db,$company_id), $u_status);
                    $result_sss = mysqli_query($this->db, $sql_sss);
                    if(!$result_sss){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_sss");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }else{
                        $sql_sss_numrows = mysqli_num_rows($result_sss);
                        if($sql_sss_numrows>0){
                            $row = mysqli_fetch_assoc($result_sss);
                            $stripe_customer_id= $row['stripe_customer_id'];
                            $expiry_sss = $row['expiry_date'];

                            if(($upgrade_status!=='F' && $upgrade_status!=='T')&& $expiry!=$expiry_sss){
                                $error = array('status' => "Failed", "msg" => "Company and Schedule expiry doesn't match. Please contact administrator.");
                                $this->response($this->json($error), 200);
                            }
                            $calculated_payment_amount = $calculated_payment_amount_temp = $failed_outstanding_balance = $succeed_outstanding_balance = 0;
                            $future_payment_date=$studio_payment_status=$studio_expiry_level='';

                            //SUBSCRIBE TO NEW PLAN FOR FIRST TIME
                            if($payee_type == 'update_plan' && $charge_flag=='Y' && ($upgrade_status=='F' || $upgrade_status=='T') && $sub_status != 'Y') {
                                $new_plan_details = $this->getSubscriptionPlanDetails($company_id, $payment_type, 1);
//                                   log_info(json_encode($new_plan_details));
                                if (!empty($new_plan_details)) {
                                    $calculated_payment_amount = $calculated_payment_amount_temp = $new_plan_details['total_due'];
                                    $future_payment_date = $new_plan_details['next_payment_date'];
                                    $payment_desc = $new_plan_details['payment_desc'];
                                    $new_plan_fee = $new_plan_details['plan_fee'];
                                    $new_plan_setup_fee = $new_plan_details['setup_fee'];
                                    $failed_outstanding_balance = $calculated_payment_amount;
                                } else {
                                    $error = array('status' => "Failed", "msg" => $new_plan_details['msg']);
                                    $this->response($this->json($error), 200);
                                }
                            }else if ($expiry_sss >= $curr_date && $sub_status == 'Y' && $payee_type=='update_plan' && $charge_flag=='Y' && ($upgrade_status!=='F'||$upgrade_status!=='T')) {  //UPDATE PLAN BEFORE EXPIRY DATE
                                $upgrade_plan_details= $this->getSubscriptionPlanDetails($company_id, $payment_type, 1);
                                if(!empty($upgrade_plan_details)){
                                    $calculated_payment_amount_temp=$upgrade_plan_details['total_due'];
                                    $future_payment_date=$upgrade_plan_details['next_payment_date'];
                                    $payment_desc=$upgrade_plan_details['payment_desc'];
                                    $new_plan_fee=$upgrade_plan_details['plan_fee'];
                                    $new_plan_setup_fee=$upgrade_plan_details['setup_fee'];                                                                               

                                    if ($outstanding_balance < 0) {
                                        if (-1 * $outstanding_balance >= $calculated_payment_amount_temp) {      //outstanding = -100, plan_fee = 29
                                            $calculated_payment_amount = 0;
                                            $charge_type = 'free';
                                            $succeed_outstanding_balance = $outstanding_balance + $calculated_payment_amount_temp;
                                        } else {                                                                //outstanding = -10, plan_fee = 29
                                            $calculated_payment_amount = number_format($calculated_payment_amount_temp + $outstanding_balance, 2, '.', '');
                                            $succeed_outstanding_balance = 0;
                                            $failed_outstanding_balance = $calculated_payment_amount_temp + $outstanding_balance;
                                        }
                                    } else {
                                        $calculated_payment_amount = number_format($outstanding_balance + $calculated_payment_amount_temp, 2, '.', '');
                                        $succeed_outstanding_balance = 0;
                                        $failed_outstanding_balance = $calculated_payment_amount_temp + $outstanding_balance;
                                    }
                                }else{
                                    $error = array('status' => "Failed", "msg" => $upgrade_plan_details['msg']);
                                    $this->response($this->json($error), 200);
                                }
                            }else{
                                $error = array('status' => "Failed", "msg" => "Something went wrong in your subscription plan. Please contact your administrator.");
                                $this->response($this->json($error), 200);
                            }
                        }else{
                            $error_log = array('status' => "Failed", "msg" => "Schedule details not available.", "query" => "$sql_sss");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Previous payment details not available. Please contact your administrator.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => "Cannot upgrade plan. Please contact your administrator.");
                    $this->response($this->json($error), 200);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Plan can not be changed due to no card details available for Payments Processing via Stripe .");
                $this->response($this->json($error), 200);// If no records "No Content" status
            }
        }
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        
        try {
//            log_info("payee_type : ".$payee_type.  "   pay_amount : ".$calculated_payment_amount);
            if($payee_type == 'update_plan' && $charge_flag=='Y'&& $calculated_payment_amount>=1 && ($upgrade_status=='F'|| $upgrade_status=='T')){
            // Use Stripe's library to make requests...            
                $stripe_charge_create = \Stripe\Charge::create([
                    "amount" => ($calculated_payment_amount*100),
                    "currency" => "$stripe_currency_code",
                    "customer" => "$stripe_customer_id", 
                    "description" => "$payment_desc",
                    "statement_descriptor" => substr($payment_desc,0,21)
                ]);
                $body3 = json_encode($stripe_charge_create);
                stripe_log_info("Customer_old_update : $body3");
                $body3_arr = json_decode($body3, true);
                $stripe_charge_id = $body3_arr['id'];
                $brand=$body3_arr['source']['brand'];
                $last4 = $body3_arr['source']['last4'];
                $stripe_old_card_id=$body3_arr['source']['id'];
                $charge_type=$body3_arr['source']['object'];
                $stripe_payment_status=$body3_arr['status'];
                $temp=' xxxxxx';
                $stripe_card_name=$brand.$temp.$last4;

            }else if($payee_type == 'update_plan' && $calculated_payment_amount>=1 && $charge_flag=='Y'){
                // Use Stripe's library to make requests...            
                $stripe_charge_create = \Stripe\Charge::create([
                    "amount" => ($calculated_payment_amount*100),
                    "currency" => "$stripe_currency_code",
                    "customer" => "$customer_id", 
                    "description" => "$payment_desc",
                    "statement_descriptor" => substr($payment_desc,0,21)
                ]);            
                $body3 = json_encode($stripe_charge_create);
                stripe_log_info("Customer_old_update : $body3");
                $body3_arr = json_decode($body3, true);
                $stripe_charge_id = $body3_arr['id'];
                $brand=$body3_arr['source']['brand'];
                $last4 = $body3_arr['source']['last4'];
                $stripe_old_card_id=$body3_arr['source']['id'];
                $charge_type=$body3_arr['source']['object'];
                $stripe_payment_status=$body3_arr['status'];
                $temp=' xxxxxx';
                $stripe_card_name=$brand.$temp.$last4;
            }
        } catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
          // Too many requests made to the API too quickly     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
          // Invalid parameters were supplied to Stripe's API     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
          // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
          // Network communication with Stripe failed     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send yourself an email     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        }
        if(isset($err) && !empty($err)){
            $stripe_payment_status='';
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);// If no records "No Content" status
        }
    
        
        

        if($payee_type=='update_plan' && $charge_flag=='Y'){
            if($stripe_payment_status=="succeeded"||$stripe_payment_status=="captured"||$stripe_payment_status=="old_credit"){
                $insert_table='`stripe_studio_subscription_payments`';
                $last_successfull_pay_date = $curr_date;
                $sub_status = 'Y';
                $outstanding_balance_c=$succeed_outstanding_balance;
                $stripe_last_success_payment=$curr_date;
                $status=$studio_payment_status=$studio_expiry_level='A';
                $pay_type=$payment_type;
            }else{
                $insert_table='`stripe_studio_subscription_payments_history`';
                $last_successfull_pay_date = $last_successfull_payment_date;
                $sub_status = 'N';
                $outstanding_balance_c=$failed_outstanding_balance;
                $status='A';
                $pay_type=$upgrade_status;
                $studio_payment_status='N';
            }

            if($upgrade_status=='T'||$upgrade_status=='F'){
                $ref_string= ",`lead_source`='$lead_source' , `referer_name`='$referer_name'";
            }
            
            $sql2 = sprintf("update `company` set `expiry_dt`='%s', `studio_expiry_level`='%s',`studio_payment_status`='%s', `stripe_last_success_payment`='$stripe_last_success_payment', `subscription_status`='%s', `upgrade_status`='%s'$ref_string where `company_id`='%s'",
                    mysqli_real_escape_string($this->db,$future_payment_date),$studio_expiry_level,$studio_payment_status,$sub_status,mysqli_real_escape_string($this->db,$pay_type),mysqli_real_escape_string($this->db,$company_id));
            $sql_result2 = mysqli_query($this->db, $sql2);
            if(!$sql_result2){
                $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($err_log));
                $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($res), 200);
            }
        }
        
        //    UPGRADE TO NEW PLAN FIRST TIME
        if($payee_type=='update_plan' && $charge_flag=='Y' && ($upgrade_status=='T'||$upgrade_status=='F')){
            $update_plan=sprintf("UPDATE `stripe_studio_subscription` SET `status`='%s', `expiry_date`='%s', `outstanding_balance`='%s', `premium_type`='%s', `payment_amount`='%s', `setup_fee`='%s', `first_payment`='%s', `first_payment_date`='%s',`next_payment_date`='%s', `last_successfull_payment_date`='%s' WHERE `sss_id`='%s' AND `company_id`='%s' ",
                    $status,mysqli_real_escape_string($this->db, $future_payment_date), mysqli_real_escape_string($this->db, $outstanding_balance_c), mysqli_real_escape_string($this->db, $pay_type), mysqli_real_escape_string($this->db, $new_plan_fee),
                    mysqli_real_escape_string($this->db, $new_plan_setup_fee), mysqli_real_escape_string($this->db, $calculated_payment_amount), mysqli_real_escape_string($this->db, $curr_date),mysqli_real_escape_string($this->db, $future_payment_date), mysqli_real_escape_string($this->db,$last_successfull_pay_date),
                    mysqli_real_escape_string($this->db, $stripe_reg_id),mysqli_real_escape_string($this->db, $company_id)); 
            $result_update_plan = mysqli_query($this->db, $update_plan);
            if(!$result_update_plan){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_plan");
                log_info($this->json($error_log));
                $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($res), 200);
            }else{
                $insert_query2 = sprintf("INSERT INTO $insert_table(`company_id`, `sss_id`, `source_id`, `charge_id`, `charge_amount`, `payment_amount`, `charge_status`,`charge_type`) 
                VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $stripe_reg_id),
                        mysqli_real_escape_string($this->db, $stripe_old_card_id), mysqli_real_escape_string($this->db, $stripe_charge_id),
                        mysqli_real_escape_string($this->db, $calculated_payment_amount), mysqli_real_escape_string($this->db, $calculated_payment_amount_temp), 
                        mysqli_real_escape_string($this->db, $stripe_payment_status), mysqli_real_escape_string($this->db, $charge_type));
                $result_query2 = mysqli_query($this->db, $insert_query2);
                if (!$result_query2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query2");
                    log_info($this->json($error_log));
                    $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($res), 200);
                }else{
                    $res = array("status" => "Success", "msg" => "Subscription Sucessfully Updated.","new_upgrade_status"=>$pay_type);
                    log_info($this->json($res));
                    $this->response($this->json($res), 200);
                    
                }
            }
        }else if($payee_type=='update_plan' && $charge_flag=='Y' && ($upgrade_status!=='T' && $upgrade_status!=='F')){            //UPGRADE NEW PLAN HAVING OLD SUSBSCRIPTION
            $update_payment = sprintf("INSERT INTO `stripe_studio_subscription`(`company_id`,`user_id`, `status`, `expiry_date`, `premium_type`, `payment_amount`,`stripe_card_status`,`card_exp_year`,`card_exp_month`, 
                    `stripe_customer_id`, `stripe_source_id`, `firstname`, `lastname`, `email`, `phone`, `streetaddress`, `city`, `state`, `country`, `zip`,`stripe_card_name`)
                    SELECT `company_id`,`user_id`, `status`, `expiry_date`, `premium_type`, `payment_amount`,`stripe_card_status`,`card_exp_year`,`card_exp_month`, 
                    `stripe_customer_id`, `stripe_source_id`, `firstname`, `lastname`, `email`, `phone`, `streetaddress`, `city`, `state`, `country`, `zip`,`stripe_card_name` FROM `stripe_studio_subscription`
                    WHERE `sss_id`='%s' AND `stripe_customer_id`='%s' AND `company_id`='%s' AND IF(`premium_type` IN ('T','F'),'',`subscription_end_date` IS NULL ) AND `deleted_flag`!='Y'", 
                    mysqli_real_escape_string($this->db, $stripe_reg_id), mysqli_real_escape_string($this->db, $customer_id), mysqli_real_escape_string($this->db, $company_id));
            $result_update_payment = mysqli_query($this->db, $update_payment);
            if(!$result_update_payment){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_payment");
                log_info($this->json($error_log));
                $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($res), 200);
            }else{            
                $new_sss_id = mysqli_insert_id($this->db);
                $update_query = sprintf("UPDATE `stripe_studio_subscription` SET `subscription_end_date`='%s' WHERE `sss_id`='%s' AND `stripe_customer_id`='%s' AND `company_id`='%s' AND `deleted_flag`!='Y' ",
                        $curr_date,mysqli_real_escape_string($this->db, $stripe_reg_id), mysqli_real_escape_string($this->db, $customer_id), mysqli_real_escape_string($this->db, $company_id));
                $result_update_query = mysqli_query($this->db, $update_query);
                if(!$result_update_query){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                    log_info($this->json($error_log));
                    $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($res), 200);
                }else{
                    if(mysqli_affected_rows($this->db)>0){
                                                        
                           $update_plan=sprintf("UPDATE `stripe_studio_subscription` SET `user_id`='%s',`expiry_date`='%s', `premium_type`='%s',`payment_amount`='%s',`setup_fee`='%s',`first_payment`='%s',
                                    `first_payment_date`='%s',`next_payment_date`='%s',`outstanding_balance`='%s',`last_successfull_payment_date`='%s' WHERE `sss_id`='%s' AND `company_id`='%s' ",
                                    mysqli_real_escape_string($this->db, $user_id),mysqli_real_escape_string($this->db, $future_payment_date), mysqli_real_escape_string($this->db, $pay_type), mysqli_real_escape_string($this->db, $new_plan_fee),
                                    mysqli_real_escape_string($this->db, $new_plan_setup_fee), mysqli_real_escape_string($this->db, $calculated_payment_amount),mysqli_real_escape_string($this->db, $curr_date), 
                                    mysqli_real_escape_string($this->db, $future_payment_date), mysqli_real_escape_string($this->db, $outstanding_balance_c), mysqli_real_escape_string($this->db,$last_successfull_pay_date),
                                    mysqli_real_escape_string($this->db, $new_sss_id),mysqli_real_escape_string($this->db, $company_id)); 
                            $result_update_plan = mysqli_query($this->db, $update_plan);
                            if(!$result_update_plan){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_plan");
                                log_info($this->json($error_log));
                                $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($res), 200);
                            }else{
                                if(!empty($new_sss_id && $new_sss_id!==0)){
                                    $insert_query2 = sprintf("INSERT INTO $insert_table(`company_id`, `sss_id`, `source_id`, `charge_id`, `charge_amount`, `payment_amount`, `charge_status`,`charge_type`) 
                                    VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $new_sss_id),
                                            mysqli_real_escape_string($this->db, $stripe_old_card_id), mysqli_real_escape_string($this->db, $stripe_charge_id),
                                            mysqli_real_escape_string($this->db, $calculated_payment_amount), mysqli_real_escape_string($this->db, $calculated_payment_amount_temp), 
                                            mysqli_real_escape_string($this->db, $stripe_payment_status), mysqli_real_escape_string($this->db, $charge_type));
                                    $result_query2 = mysqli_query($this->db, $insert_query2);
                                    if (!$result_query2) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query2");
                                        log_info($this->json($error_log));
                                        $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($res), 200);
                                    }else{
                                        $res = array("status" => "Success", "msg" => "Subscription Sucessfully Updated.","new_upgrade_status"=>$pay_type);
                                        log_info($this->json($res));
                                        $this->response($this->json($res), 200);                                        
                                    }
                                }else{
                                    $res = array("status" => "Failed", "msg" => " Registration Details Not Updated .");
                                    log_info($this->json($res));
                                    $this->response($this->json($res), 200);
                                }
                            }
                        }else{
                            $res = array("status" => "Failed", "msg" => " Registration Details Not Updated .");
                            log_info($this->json($res));
                            $this->response($this->json($res), 200);
                            
                        }
                    }

                }
            }

        date_default_timezone_set($curr_time_zone);
    }
    
    protected function updateStudioSubscriptionCardDetails($company_id,$payment_type, $payee_type,$charge_flag, $firstname, $lastname, $address, $city, $state, $country, $zip, $stripe_token_id){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $current_date = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        
        $this->getStripeKeys();
        $customer_id = $stripe_customer_id = $stripe_card_id = $stripe_charge_id =$ref_string=$stripe_old_card_id=$stripe_card_status=$stripe_card_name =$card_valid_status=$card_exp_month=$card_exp_year='';        
        $token_id= mysqli_real_escape_string($this->db,$stripe_token_id);
        if($payee_type=='registration'){
            $this->checkPaysimpleSchedulesForStudio($company_id);
        }               
        
        $sql = sprintf("select `company_name`, date(`expiry_dt`) expiry, `subscription_status`, `upgrade_status`, `studio_expiry_level`,u.`user_email`,u.`user_id`
                from `company` c 
                LEFT JOIN `user` u ON u.`company_id` = c.`company_id` where c.`company_id`='%s' AND u.`user_type`='A' ORDER BY u.`user_id` DESC LIMIT 0,1",mysqli_real_escape_string($this->db,$company_id));
        $sql_result = mysqli_query($this->db, $sql);
        if(!$sql_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $sql_num_rows = mysqli_num_rows($sql_result);
            if($sql_num_rows>0){
                $sql_row = mysqli_fetch_assoc($sql_result);
                $expiry = $sql_row['expiry'];
                $sub_status = $sql_row['subscription_status'];
                $upgrade_status = $sql_row['upgrade_status'];
                $company_name = $sql_row['company_name'];
                $studio_expiry_level=$sql_row['studio_expiry_level'];
                $email=$sql_row['user_email'];
                $user_id=$sql_row['user_id'];
            }else{
                $error = array('status' => "Failed", "msg" => "Company or Studio Admin details not found");
                $this->response($this->json($error), 200);
            }
        }
        
            $sql2 = sprintf("select s.`stripe_customer_id`, s.`stripe_source_id`, s.`sss_id`, s.`outstanding_balance`, s.`next_payment_date`, s.`last_successfull_payment_date`, s.`expiry_date`
                    from `company` c LEFT JOIN `stripe_studio_subscription` s ON c.`company_id` = s.`company_id`
                    where c.`company_id`='%s' AND s.deleted_flag!='Y' AND s.`subscription_end_date` IS NULL ORDER BY s.sss_id DESC LIMIT 0,1",mysqli_real_escape_string($this->db,$company_id));
            $sql_result2 = mysqli_query($this->db, $sql2);
            if(!$sql_result2){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                $sql_num_rows2 = mysqli_num_rows($sql_result2);
                if($sql_num_rows2>0){
                    $sql_row = mysqli_fetch_assoc($sql_result2);
                    $payee_type='update_card';
                    $customer_id = $sql_row['stripe_customer_id'];
                }else{
                    $payee_type='registration';
                }
            }
        
        if($payee_type=='registration' && (empty($company_name) || empty($upgrade_status) || empty($sub_status))){
            $error = array('status' => "Failed", "msg" => "Studio Details not found.");
            $this->response($this->json($error), 200);
        }else if($payee_type=='update_card' && (empty($company_name) || empty($upgrade_status) || empty($customer_id) || empty($sub_status))){
            $error = array('status' => "Failed", "msg" => "Studio Details not found.");
            $this->response($this->json($error), 200);
        }else{
            \Stripe\Stripe::setApiKey($this->stripe_sk);

            try {
                if ($payee_type == 'registration') {
                // Use Stripe's library to make requests...
                    $stripe_customer_create = \Stripe\Customer::create([
                                "email" => "$email",
                                "description" => "Customer Subscription for $company_name",
                                "source" => "$token_id" // obtained with Stripe.js
                    ]);
                    $body = json_encode($stripe_customer_create);
                    stripe_log_info("Customer_new : $body");
                    $body_arr = json_decode($body, true);

                    $stripe_customer_id = $body_arr['id'];
                    $stripe_card_id = $body_arr['default_source'];
                    $stripe_card_status='Active';
                    $card_exp_month=$body_arr['sources']['data'][0]['exp_month'];
                    $card_exp_year=$body_arr['sources']['data'][0]['exp_year'];
                    $brand = $body_arr['sources']['data'][0]['brand'];
                    $last4 = $body_arr['sources']['data'][0]['last4'];
                    $temp=' xxxxxx';
                    $stripe_card_name=$brand.$temp.$last4;
                } else if ($payee_type == 'update_card') {
                    $card_update = \Stripe\Customer::retrieve($customer_id);

                    $card_update->description = "Customer Card Update to Subscription for $company_name";
                    $card_update->source = $token_id; // obtained with Stripe.js
                    $card_update->save();               

                    $body7 = json_encode($card_update);
                    stripe_log_info("card_update_select_result : $body7");
                    $body7_arr = json_decode($body7, true);
                    $stripe_customer_id_updated = $body7_arr['id'];
                    $stripe_card_id_updated =$body7_arr['default_source'];
                    $stripe_card_status='Active';
                    $card_exp_month=$body7_arr['sources']['data'][0]['exp_month'];
                    $card_exp_year=$body7_arr['sources']['data'][0]['exp_year'];
                    $brand = $body7_arr['sources']['data'][0]['brand'];
                    $last4 = $body7_arr['sources']['data'][0]['last4'];
                    $temp=' xxxxxx';
                    $stripe_card_name=$brand.$temp.$last4;
                }
            } catch(\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
              // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
              // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
              // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
              // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
              // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
              // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if(isset($err) && !empty($err)){
                log_info($this->json($err));
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);// If no records "No Content" status
            }
            
            if($payee_type=='registration'){            //CREATE ONLY CUSTOMER
                $payment_amount=$setup_fee=0;
                $payment_type=$upgrade_status;
                $sql2 = sprintf("update `company` set `stripe_subscription`='Y',`studio_payment_status`='N' where `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
                $sql_result2 = mysqli_query($this->db, $sql2);
                if(!$sql_result2){
                    $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($err_log));
                    $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($res), 200);
                }else{
                    $insert_query = sprintf("INSERT INTO `stripe_studio_subscription`(`company_id`, `user_id`, `status`, `expiry_date`, `premium_type`, `payment_amount`, `setup_fee`,`stripe_card_status`, 
                            `stripe_customer_id`, `stripe_source_id`, `firstname`, `lastname`, `email`, `streetaddress`, `city`, `state`, `country`, `zip`,`card_exp_month`,`card_exp_year`,`stripe_card_name`) 
                            VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                            mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $user_id), mysqli_real_escape_string($this->db, 'A'), mysqli_real_escape_string($this->db, $expiry), 
                            mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $setup_fee), mysqli_real_escape_string($this->db, $stripe_card_status), 
                            mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $stripe_card_id), mysqli_real_escape_string($this->db, $firstname), mysqli_real_escape_string($this->db, $lastname), 
                            mysqli_real_escape_string($this->db, $email), mysqli_real_escape_string($this->db, $address), mysqli_real_escape_string($this->db, $city), mysqli_real_escape_string($this->db, $state),
                            mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $zip),mysqli_real_escape_string($this->db,$card_exp_month),mysqli_real_escape_string($this->db,$card_exp_year),
                            mysqli_real_escape_string($this->db, $stripe_card_name));
                    $result_query = mysqli_query($this->db, $insert_query);
                    if(!$result_query){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query");
                        log_info($this->json($error_log));
                        $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($res), 200);
                    }else{
                        if($charge_flag=='Y'){
                            $rerun_payment=$this->reRunSubscriptionPayment($company_id, 1);
                            $rerun_status=" Re-run payment successfull";
                            if($rerun_payment['status']!=='Success'){
                                $error=array("status"=>$rerun_payment['status'],"msg"=>$rerun_payment['msg']);
                                $rerun_status = 'Rerun Failed '.$error['msg'];
                            $acc_details=$this->getStripeCreditCardAccountDetails($company_id,1);                            
                            $res = array("status" => "Failed", "msg" => "$rerun_status Card Details Updated Successfully.","account_details"=>$acc_details,"studio_expiry_level"=>$studio_expiry_level,"from_rerun"=>"Y"); 
                            $this->response($this->json($res), 200);
                            }
                            $acc_details=$this->getStripeCreditCardAccountDetails($company_id,1);                            
                            $res = array("status" => "Success", "msg" => "Card Details Updated Successfully. $rerun_status","account_details"=>$acc_details,"studio_expiry_level"=>$rerun_payment['studio_expiry_level'],"from_rerun"=>"Y");
                        }else{
                            $acc_details=$this->getStripeCreditCardAccountDetails($company_id,1);
                            $succ_msg = array("status" => "Success", "msg" => "Card Details Updated Successfully.","account_details"=>$acc_details,"studio_expiry_level"=>$studio_expiry_level,"from_rerun"=>"N");
                            $this->response($this->json($succ_msg), 200);
                        }
                    }
                }
            }else if($payee_type=='update_card' ){           //UPDATE CARD AND RERUN(BASED ON CONDITION YES OR NO)
                $update_query = sprintf("UPDATE `stripe_studio_subscription` SET `user_id`='%s',`stripe_source_id`='%s', `firstname`='%s', `lastname`='%s', `email`='%s', `streetaddress`='%s', 
                            `city`='%s', `state`='%s', `country`='%s', `zip`='%s',`card_exp_month`='%s',`card_exp_year`='%s', `stripe_card_name`='%s',`stripe_card_status`='%s' 
                            WHERE `stripe_customer_id`='%s' AND `company_id`='%s' AND `deleted_flag`!='Y' ",mysqli_real_escape_string($this->db, $user_id), mysqli_real_escape_string($this->db, $stripe_card_id_updated),
                        mysqli_real_escape_string($this->db, $firstname), mysqli_real_escape_string($this->db, $lastname), mysqli_real_escape_string($this->db, $email), 
                        mysqli_real_escape_string($this->db, $address), mysqli_real_escape_string($this->db, $city),                                
                        mysqli_real_escape_string($this->db, $state), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $zip),
                        mysqli_real_escape_string($this->db, $card_exp_month), mysqli_real_escape_string($this->db, $card_exp_year), mysqli_real_escape_string($this->db, $stripe_card_name),
                        mysqli_real_escape_string($this->db, $stripe_card_status), mysqli_real_escape_string($this->db, $stripe_customer_id_updated), mysqli_real_escape_string($this->db, $company_id));
                $result_update_query = mysqli_query($this->db, $update_query);
                if(!$result_update_query){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                    log_info($this->json($error_log));
                    $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($res), 200);
                }else{
                    if(mysqli_affected_rows($this->db)>0){
                        $rerun_status=$acc_details=$content='';
                        if($charge_flag=='Y'){
                            $rerun_payment=$this->reRunSubscriptionPayment($company_id, 1);
                            $rerun_status=" Re-run payment successfull";
                            if($rerun_payment['status']!=='Success'){
                                $error=array("status"=>$rerun_payment['status'],"msg"=>$rerun_payment['msg']);
                                $rerun_status = 'Rerun Failed '.$error['msg'];
                                $acc_details=$this->getStripeCreditCardAccountDetails($company_id,1);                            
                                $res = array("status" => "Failed", "msg" => "$rerun_status Card Details Updated Successfully.","account_details"=>$acc_details,"studio_expiry_level"=>$studio_expiry_level,"from_rerun"=>"Y"); 
                                $this->response($this->json($res), 200);
                            }
                            $acc_details=$this->getStripeCreditCardAccountDetails($company_id,1);                            
                            $res = array("status" => "Success", "msg" => "Card Details Updated Successfully. $rerun_status","account_details"=>$acc_details,"studio_expiry_level"=>$rerun_payment['studio_expiry_level'],"from_rerun"=>"Y");
                        }else{
                            $acc_details=$this->getStripeCreditCardAccountDetails($company_id,1);                            
                            $res = array("status" => "Success", "msg" => "Card Details Updated Successfully.","account_details"=>$acc_details,"studio_expiry_level"=>$studio_expiry_level,"from_rerun"=>"N");
                        }
                        log_info($this->json($res));
                        $this->response($this->json($res), 200);                            
                    }else{
                        $res = array("status" => "Failed", "msg" => "Failed to update payment new method.");
                        log_info($this->json($res)); 
                        $this->response($this->json($res), 200);
                    }
                }
            }else{
                $res = array("status" => "Failed", "msg" => "Wrong payment method, Update eror.");
                $this->response($this->json($res), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    protected function reRunSubscriptionPayment($company_id, $call_back){
        $this->checkPaysimpleSchedulesForStudio($company_id);
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        
        $this->getStripeKeys();
        $curr_date = date("Y-m-d");
        $stripe_customer_id = $stripe_card_id = $stripe_charge_id = $ref_string = $stripe_old_card_id ='';        
        $charge_type='free';
        $calculated_payment_amount_temp = 0;
        
        $sql = sprintf("select `company_name`, `web_page`, date(`expiry_dt`) expiry, `subscription_status`, `upgrade_status`, `stripe_currency_code`, `stripe_currency_symbol`, s.`stripe_customer_id`, s.`stripe_source_id`,s.`sss_id`,c.`studio_expiry_level`,c.`stripe_subscription`,
                s.`outstanding_balance`, s.`next_payment_date`, s.`last_successfull_payment_date`, s.`expiry_date`, s.`premium_type`, s.`payment_amount`, `stripe_last_success_payment`,`studio_payment_status`
                from `company` c LEFT JOIN `stripe_studio_subscription` s ON c.`company_id` = s.`company_id`
                where c.`company_id`='%s' AND s.deleted_flag!='Y' AND `subscription_status`='N' AND `upgrade_status` NOT IN ('F','T') AND `upgrade_status`=`premium_type` AND s.`subscription_end_date` IS NULL ORDER BY s.sss_id DESC LIMIT 0,1",mysqli_real_escape_string($this->db,$company_id));
        $sql_result = mysqli_query($this->db, $sql);
        if(!$sql_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 1) {
                return $error;
            } else {
                $this->response($this->json($error), 200);
            }
        }else{
            $sql_num_rows = mysqli_num_rows($sql_result);
            if($sql_num_rows>0){
                $sql_row = mysqli_fetch_assoc($sql_result);
                $expiry = $sql_row['expiry'];
                $sub_status = $sql_row['subscription_status'];
                $upgrade_status = $sql_row['upgrade_status'];
                $stripe_currency_code = $sql_row['stripe_currency_code'];
                $stripe_customer_id = $customer_id = $sql_row['stripe_customer_id'];
                $card_id = $sql_row['stripe_source_id'];
                $stripe_reg_id = $sql_row['sss_id'];
                $calculated_payment_amount_temp = $outstanding_balance = $sql_row['outstanding_balance'];
                $next_payment_date =$sql_row['next_payment_date'];
                $last_successfull_payment_date=$sql_row['last_successfull_payment_date'];
                $studio_expiry_level=$sql_row['studio_expiry_level'];
                $premium_amount = $sql_row['payment_amount'];
                $expiry_sss = $sql_row['expiry_date'];
                $premium_type = $sql_row['premium_type'];
                $stripe_last_success_payment = $sql_row['stripe_last_success_payment'];
                $studio_ps = $sql_row['studio_payment_status'];
                $stripe_subscription=$sql_row['stripe_subscription'];
                
                if($expiry!=$expiry_sss || $upgrade_status!=$premium_type || $outstanding_balance<1){
                    $error = array('status' => "Failed", "msg" => "Company and Schedule details doesn't match. Please contact administrator.");
                    $this->sendEmailForAdmins('S',$error['msg']." for Company($company_id)");
                    if ($call_back == 1) {
                        return $error;
                    } else {
                        $this->response($this->json($error), 200);
                    }
                }else{
                    $subscription_fee=$this->getSubscriptionPlanFee($company_id,$upgrade_status);
                }
                
                
                $stripe_payment_status="old_credit";
                $calculated_payment_amount=$per_day_payment_sss=$rem_days_sss=$total_days_in_prev_plan=$total_days_in_curr_plan=$plan_fee=$setup_fee=$total_days_in_new_plan=$new_plan_fee=0;
                $stripe_charge_create='';
                if(!empty($subscription_fee)){
                    $plan_fee=$subscription_fee['current_plan_fee'];
                }else{
                   $error = array('status' => "Failed", "msg" => "Unable to get the payment plan details.");
                   if ($call_back == 1) {
                        return $error;
                    } else {
                        $this->response($this->json($error), 200);
                    }
                }

                if ($sub_status != 'Y') {               //RERUN OLD DUE PAYMENT
                    
                        if ($outstanding_balance > 1) {
                            $calculated_payment_amount = number_format($outstanding_balance, 2, '.', '');
                        }

                        if ($upgrade_status == 'W') {
                            $payment_desc = "MyStudio White Label Yearly Membership";  
                        }else if($upgrade_status == 'P'){
                            $payment_desc = "MyStudio Premium Yearly Membership";                                           
                        }else if($upgrade_status == 'M'){
                            $payment_desc = "MyStudio Premium Monthly Membership";
                        }else if($upgrade_status == 'WM'){
                            $payment_desc = "MyStudio White Label Monthly Membership";
                        }else if($upgrade_status == 'B'){
                            $payment_desc = "MyStudio Basic Monthly Membership";
                        }
                } else{
                    $error = array('status' => "Failed", "msg" => "Something went wrong in your subscription plan. Please contact your administrator.");
                    $this->sendEmailForAdmins('S',$error['msg']." for Company($company_id)");
                    if ($call_back == 1) {
                        return $error;
                    } else {
                        $this->response($this->json($error), 200);
                    }
                }                    
            }else{
                $error = array('status' => "Failed", "msg" => "Studio subscription details doesn't exist.");
//                $this->response($this->json($error), 200);// If no records "No Content" status
                if ($call_back == 1) {
                    return $error;
                } else {
                    $this->response($this->json($error), 200);
                }
            }
        }
            
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        

        try {
            if($calculated_payment_amount>=1){
            // Use Stripe's library to make requests...            
                $stripe_charge_create = \Stripe\Charge::create([
                    "amount" => ($calculated_payment_amount*100),
                    "currency" => "$stripe_currency_code",
                    "customer" => "$customer_id", 
                    "description" => "$payment_desc",
                    "statement_descriptor" => substr($payment_desc,0,21)
                    ]);
                
                $body3 = json_encode($stripe_charge_create);
                stripe_log_info("Customer_old_update : $body3");
                $body3_arr = json_decode($body3, true);

                $stripe_charge_id = $body3_arr['id'];
                $brand=$body3_arr['source']['brand'];
                $last4 = $body3_arr['source']['last4'];
                $stripe_old_card_id=$body3_arr['source']['id'];
                $charge_type=$body3_arr['source']['object'];
                $stripe_payment_status=$body3_arr['status'];
                $temp=' xxxxxx';
                $stripe_card_name=$brand.$temp.$last4;
                $outstanding_balance = 0;
            }
        } catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
          // Too many requests made to the API too quickly     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
          // Invalid parameters were supplied to Stripe's API     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
          // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
          // Network communication with Stripe failed     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send yourself an email     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe     
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
        }
        if(isset($err) && !empty($err)){
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->sendEmailForAdmins('S',$error['msg']." for Company($company_id)");
            if ($call_back == 1) {
                return $error;
            } else {
                $this->response($this->json($error), 200);
            }            
            $stripe_payment_status="card";
        }
        
        if($stripe_payment_status=="succeeded"||$stripe_payment_status=="captured"||$stripe_payment_status=="old_credit"){
            $insert_table='`stripe_studio_subscription_payments`';
            $str = "last_successfull_payment_date = '$curr_date',";
            $studio_expiry_level_new = 'A';
            $sub_sts = 'Y';
            $stripe_last_success_payment = $curr_date;
            $studio_ps='A';
            $stripe_subs='Y';
        } else {
            $stripe_last_success_payment = $last_successfull_payment_date;
            $insert_table = '`stripe_studio_subscription_payments_history`';
            $str = '';
            $studio_expiry_level_new = $studio_expiry_level;
            $sub_sts = 'N';
            $stripe_subs = $stripe_subscription;
            $studio_ps='F';
            
        }
        $sql2 = sprintf("update `company` set `stripe_subscription`='$stripe_subs',`subscription_status`='$sub_sts', `studio_expiry_level`='$studio_expiry_level_new',`studio_payment_status`='$studio_ps', `stripe_last_success_payment`='$stripe_last_success_payment'  where `company_id`='%s'",
                mysqli_real_escape_string($this->db,$company_id));
        $sql_result2 = mysqli_query($this->db, $sql2);
        if(!$sql_result2){
            $err_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($err_log));
            $res = array("status" => "Failed", "msg" => "Internal Server Error.");
            if($call_back==1){
                return $res;
            }else{                                    
                $this->response($this->json($res), 200);
            }
        }        

        $update_query = sprintf("UPDATE `stripe_studio_subscription` SET `payment_amount`='%s',`next_payment_date`='%s', $str `outstanding_balance`='%s' WHERE `sss_id`='%s' AND `stripe_customer_id`='%s' AND `company_id`='%s' AND `deleted_flag`!='Y' ",
             mysqli_real_escape_string($this->db,$calculated_payment_amount),mysqli_real_escape_string($this->db,$expiry_sss),mysqli_real_escape_string($this->db, $outstanding_balance), mysqli_real_escape_string($this->db, $stripe_reg_id),
             mysqli_real_escape_string($this->db, $customer_id), mysqli_real_escape_string($this->db, $company_id));
            $result_update_query = mysqli_query($this->db, $update_query);
        if(!$result_update_query){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
            log_info($this->json($error_log));
            $error = array("status" => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 1) {
                return $error;
            } else {
                $this->response($this->json($error), 200);
            }
        }else{
            if(mysqli_affected_rows($this->db)>0){
                $insert_query2 = sprintf("INSERT INTO $insert_table (`company_id`, `sss_id`, `source_id`, `charge_id`, `charge_amount`, `payment_amount`, `charge_status`,`charge_type`) 
                        VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $stripe_reg_id),
                        mysqli_real_escape_string($this->db, $stripe_old_card_id), mysqli_real_escape_string($this->db, $stripe_charge_id),
                        mysqli_real_escape_string($this->db, $calculated_payment_amount), mysqli_real_escape_string($this->db, $calculated_payment_amount_temp), 
                        mysqli_real_escape_string($this->db, $stripe_payment_status), mysqli_real_escape_string($this->db, $charge_type));
                $result_query2 = mysqli_query($this->db, $insert_query2);
                if (!$result_query2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query2");
                    log_info($this->json($error_log));
                    $res = array("status" => "Failed", "msg" => "Internal Server Error.");
                    if($call_back==1){
                        return $res;
                    }else{                                    
                        $this->response($this->json($res), 200);
                    }
                }else{
                    $res = array("status" => "Success", "msg" => "Subscription Sucessfully Updated.","studio_expiry_level"=>$studio_expiry_level_new);
                    log_info($this->json($res));
                    if($call_back==1){
                        return $res;
                    }else{                                    
                        $this->response($this->json($res), 200);
                    }
                }
            }else{
                $res = array("status" => "UpdateFailed", "msg" => " Registration Details Not Updated .","studio_expiry_level"=>$studio_expiry_level_new);
                log_info($this->json($res));
                if($call_back==1){
                    return $res;
                }else{                                    
                    $this->response($this->json($res), 200);
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    protected function getStripeCreditCardAccountDetails($company_id,$callback){
        
        $query = sprintf("SELECT `stripe_customer_id`,`sss_id`,`stripe_source_id`,`stripe_card_name`,`stripe_card_status`,streetaddress,city,state,country,zip,card_exp_month,card_exp_year FROM stripe_studio_subscription  WHERE company_id='%s' AND `deleted_flag`!='Y' AND `subscription_end_date` IS  NULL group by `stripe_customer_id` ORDER BY sss_id DESC LIMIT 0,1",mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $query); 
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $acc_details=[];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while($row = mysqli_fetch_assoc($result)){
                    $acc_details['cust_id'] = $row['stripe_customer_id'];
                    $acc_details['source_id'] = $row['stripe_source_id'];
                    $acc_details['stripe_card_name'] = $row['stripe_card_name'];
                    $acc_details['stripe_card_status'] = $row['stripe_card_status'];
                    $acc_details['streetaddress'] = $row['streetaddress'];
                    $acc_details['city'] = $row['city'];
                    $acc_details['state'] = $row['state'];
                    $acc_details['country'] = $row['country'];
                    $acc_details['zip'] = $row['zip'];
                    $acc_details['card_exp_month'] = $row['card_exp_month'];
                    $acc_details['card_exp_year'] = $row['card_exp_year'];
//                    if(!empty(trim($acc_details['cust_id']))&& !empty(trim($acc_details['source_id']))){
//                        $account_details[] = $this->getStripeCreditCardDetails($acc_details['cust_id'],$acc_details['source_id']);
//                    }
                }
                if($callback==1){
                    return $acc_details;
                }else{
                    $success=array('status' => "Success","account_details"=>$acc_details); //send card details response with OK
                    $this->response($this->json($success), 200);
                }
            }else{
                if($callback==1){
                    return $acc_details;
                }else{
                    $error = array('status' => "Failed", "msg" => "No card details added to this Company.");
                    $this->response($this->json($error), 200);// If no records "No Content" status
                }
            }
        }
    }
    
    private function getSubscriptionPlanFee($company_id,$upgrade_status){
        $plan_fee_arr=$plan_fee_arr_final=[];
        $plan_fee_arr_final['current_plan_fee']=$plan_fee_arr_final['current_plan_days']=$plan_fee_arr_final['current_plan_setup_fee']=0;
            $select_fee=sprintf("SELECT * FROM `studio_subscription_fee` WHERE 
                IF((SELECT count(*) count FROM `studio_subscription_fee` WHERE `company_id`='%s')=0,`company_id`=0,
                   (`company_id`='%s' or (`company_id`=0 and `premium_type` not in (select `premium_type` from studio_subscription_fee WHERE `company_id`='%s' AND `fee_end_date` IS NULL))))",
                    mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $company_id));
        $result=mysqli_query($this->db,$select_fee);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_fee");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $nums_rows = mysqli_num_rows($result);
            if ($nums_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $plan_fee_arr['plan_fee'] = $rows['plan_fee']+0;
                    $plan_fee_arr['setup_fee'] = $rows['setup_fee']+0;
                    $plan_fee_arr['premium_type'] = $rows['premium_type'];
                    $plan_fee_arr['plan_days'] = $rows['plan_days'];
                    $plan_fee_arr['plan_offer_fee'] = 0;
                    if(($rows['premium_type']=='W' || $rows['premium_type']=='P')&&$upgrade_status=='T'){
                        $plan_fee_arr['plan_offer_fee'] = $rows['plan_offer_fee'];
                    }
                    if($plan_fee_arr['premium_type']==$upgrade_status){
                        $plan_fee_arr_final['current_plan_fee']=$plan_fee_arr['plan_fee'];
                        $plan_fee_arr_final['current_plan_days']=$plan_fee_arr['plan_days'];
                        $plan_fee_arr_final['current_plan_setup_fee']=$plan_fee_arr['setup_fee'];
                    }
                    $plan_fee_arr_final['plans'][]=$plan_fee_arr;
                }
            }
        }        
        return $plan_fee_arr_final;
    }
    
    public function checkCurrentAndNextPlan($upgrade_status, $payment_type) {
        $to_upgrade = 0;
        if($upgrade_status=='W'){
            if($payment_type=='M'){
                $error = array('status' => "Failed", "msg" => "You can't change White Label Yearly Plan to Premium Plan Monthly via app. Please contact your administrator.");
                $this->response($this->json($error), 200);// Unauthorised
            }elseif($payment_type=='P'){
                $error = array('status' => "Failed", "msg" => "You can't change White Label Yearly Plan to Premium Plan Yearly via app. Please contact your administrator.");
                $this->response($this->json($error), 200);// Unauthorised
            }elseif($payment_type=='WM'){
                $error = array('status' => "Failed", "msg" => "You can't change White Label Yearly Plan to White Label Monthly via app. Please contact your administrator.");
                $this->response($this->json($error), 200);// Unauthorised
            }elseif($payment_type=='W'){
                $error = array('status' => "Failed", "msg" => "You are already in White Label Plan. Please try payment after the company validity is expired.");
                $this->response($this->json($error), 200);// Unauthorised
            }elseif($payment_type=='B'){
                $error = array('status' => "Failed", "msg" => "You can't change White Label Yearly Plan to Basic Plan Monthly via app. Please contact your administrator.");
                $this->response($this->json($error), 200);// Unauthorised
            }
        }elseif($upgrade_status=='WM'){
            if($payment_type=='B'){
                $error = array('status' => "Failed", "msg" => "You can't change White label Plan Monthly to Basic Plan Monthly via app. Please contact your administrator.");
                $this->response($this->json($error), 200);// Unauthorised
            }elseif($payment_type=='M'){
                $error = array('status' => "Failed", "msg" => "You can't change White label Plan Monthly to Premium Plan Monthly via app. Please contact your administrator.");
                $this->response($this->json($error), 200);// Unauthorised
            }elseif($payment_type=='P'){
                $error = array('status' => "Failed", "msg" => "You can't change White label Plan Monthly to Premium Plan Yearly via app. Please contact your administrator.");
                $this->response($this->json($error), 200);// Unauthorised
            }elseif($payment_type=='WM'){
                $error = array('status' => "Failed", "msg" => "You are already in White label Plan Monthly. Please try payment after the company validity is expired.");
                $this->response($this->json($error), 200);// Unauthorised
            }elseif($payment_type=='W'){
                $to_upgrade=1;
            }
        }elseif($upgrade_status=='P'){
            if($payment_type=='M'){
                $error = array('status' => "Failed", "msg" => "You can't change Premium Plan Yearly to Premium Plan Monthly via app. Please contact your administrator.");
                $this->response($this->json($error), 200);// Unauthorised
            }elseif($payment_type=='P'){
                $error = array('status' => "Failed", "msg" => "You are already in Premium Plan Yearly. Please try payment after the company validity is expired.");
                $this->response($this->json($error), 200);// Unauthorised
            }elseif($payment_type=='B'){
                $error = array('status' => "Failed", "msg" => "You can't change Premium Plan Yearly Plan to Basic Plan Monthly via app. Please contact your administrator.");
                $this->response($this->json($error), 200);// Unauthorised
            }elseif($payment_type=='W' || $payment_type=='WM'){
                $to_upgrade=1;
            }
        }elseif($upgrade_status=='M'){
            if($payment_type=='B'){
                $error = array('status' => "Failed", "msg" => "You can't change Premium Plan Monthly to Basic Plan Monthly via app. Please contact your administrator..");
                $this->response($this->json($error), 200);// Unauthorised
            }else if($payment_type=='M'){
                $error = array('status' => "Failed", "msg" => "You are already in Premium Plan Monthly. Please try payment after the company validity is expired.");
                $this->response($this->json($error), 200);// Unauthorised
            }else{
                $to_upgrade=1;
            }
        }elseif($upgrade_status=='B'){
            if($payment_type=='B'){
                $error = array('status' => "Failed", "msg" => "You are already in Basic Plan Monthly. Please try payment after the company validity is expired.");
                $this->response($this->json($error), 200);// Unauthorised
            }else{
                $to_upgrade=1;
            }                    
        }else if($upgrade_status=='T'||$upgrade_status=='F'){
            $to_upgrade=1;
        }
        return $to_upgrade;
    }
    
    private function sendEmailForAdmins($subject,$msg){
        if($this->env=='P'){
            $env = "Production";
        }else{
            $env = "Development";
        }
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com,ravi@technogemsinc.com,deepak@technogemsinc.com,jeeva@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if($subject == "W"){
                $mail->Subject = "$env - Whitelabel Files upload status.";
            }else{
                $mail->Subject = "$env - Stripe Subscription Handling Error.";
            }
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    public function checkPaysimpleSchedulesForStudio($company_id){
        $query = sprintf("SELECT * FROM `user_account` WHERE `company_id`='%s' AND `status`='Active'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row_ua = mysqli_fetch_assoc($result)){
                    $ua_id = $row_ua['user_account_id'];
                    $expiry_ua = $row_ua['expiry_dt'];
                    $sch_id = $row_ua['schedule_id'];
                    
                    $ps_response = $this->ps->accessPaySimpleApi("recurringpayment/$sch_id/suspend",'PUT','{}');     
                    if($ps_response['curl_status']==204 || $ps_response['status'] == "Success"){
                        log_info("checkPaysimpleSchedulesForStudio: Schedule($sch_id) Suspended");
                        $update_query=sprintf("UPDATE `user_account` SET `status`='Suspended`,`last_payment_status`='Success' WHERE `schedule_id`='%s'", mysqli_real_escape_string($this->db, $sch_id));
                        $res_update=mysqli_query($this->db,$update_query);
                        if(!$res_update){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                            log_info($this->json($error_log));                            
                        }
                    }
                }
                return true;
            }else{
                return true;
            }
        }
    }
    //Zapier Implementation
    protected function getCustomAPIkeydetailsInDB($company_id,$callback) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $tz_str = str_replace("_"," ",explode("/", $new_timezone))[1]." time (GMT ".DATE("P").")";
        $tz = date('T');
        
        $out = [];
        $sql1 = sprintf("SELECT * FROM `custom_api_key` WHERE `company_id`='%s' AND `api_type`!='Z' AND `deleted_flag`!='Y' order by `created_dt` desc",mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql1);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result)>0){
                while ($row = mysqli_fetch_assoc($result)) {
                    $out[]= $row;
                }if($callback==1){
                    return $out;
                }else{
                    $out = array('status' => "Success","list" => $out,"tz"=>$tz);
                    $this->response($this->json($out), 200);
                }
            }else{
                $output = array('status' => "Success","list" => $out,"tz"=>$tz);
                $this->response($this->json($output), 200);
            }
        } 
    }
    protected function getzapierApiKeyInDB($company_id) {
        
        $sql = sprintf("SELECT api_key FROM `custom_api_key` WHERE `company_id`='%s' AND `api_type`='%s'",
                    mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, 'Z'));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result)>0){
                $row = mysqli_fetch_assoc($result);
                $api_key = $row['api_key'];
                return $api_key;
            }else{
                if (function_exists('com_create_guid')) {
                    $api_key = com_create_guid();
                } else {
                    mt_srand((double) microtime() * 10000); //optional for php 4.2.0 and up.
                    $charid = strtoupper(md5(uniqid(rand(), true)));
                    $hyphen = chr(45); // "-"
                    $uuid =  substr($charid, 0, 8) . $hyphen
                            . substr($charid, 8, 4) . $hyphen
                            . substr($charid, 12, 4) . $hyphen
                            . substr($charid, 16, 4) . $hyphen
                            . substr($charid, 20, 12); 
                    $api_key = $uuid;
                }

                $sql1 = sprintf("INSERT INTO `custom_api_key` (`company_id`,`api_key`,`api_type`) VALUES ('%s', '%s', '%s')",
                    mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $api_key),mysqli_real_escape_string($this->db, 'Z'));
                $result1 = mysqli_query($this->db, $sql1);
                if (!$result1) {
                    $err_no = mysqli_errno($this->db);
                    if ($err_no == 1062) {
                        sleep(0.5);
                        $this->getzapierApiKeyInDB($company_id);
                    }
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
//                    $out = array('status' => "Success","keyvalue" => $api_key);
//                    $this->response($this->json($out), 200);
                    return $api_key;
                }
            }
            
        }
    }
    
    protected function createCustomAPIkeyInDB($company_id,$custom_api_name,$api_type) {
        $api_key = '';
        if (function_exists('com_create_guid')) {
            $api_key = com_create_guid();
        } else {
            mt_srand((double) microtime() * 10000); //optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45); // "-"
            $uuid =  substr($charid, 0, 8) . $hyphen
                    . substr($charid, 8, 4) . $hyphen
                    . substr($charid, 12, 4) . $hyphen
                    . substr($charid, 16, 4) . $hyphen
                    . substr($charid, 20, 12); 
            $api_key = $uuid;
        }
        $sql1 = sprintf("INSERT INTO `custom_api_key` (`company_id`,`api_key`,`integration_name`,`api_type`) VALUES ('%s', '%s', '%s', '%s')",
                mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $api_key), mysqli_real_escape_string($this->db, $custom_api_name),mysqli_real_escape_string($this->db, $api_type));;
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $err_no = mysqli_errno($this->db);
            if ($err_no == 1062) {
                sleep(0.5);
                $this->createCustomAPIkeyInDB($company_id,$custom_api_name,$api_type);
            }
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $custom_api_details = $this->getCustomAPIkeydetailsInDB($company_id,1);
            $out = array('status' => "Success","list" => $custom_api_details);
            $this->response($this->json($out), 200);
        }
    }
    protected function deleteCustomAPIkeyInDB($company_id,$delete_api_key,$delete_api_name) {
        $sql = sprintf("SELECT * FROM `custom_api_key` WHERE `company_id`='%s' AND `api_key` = '%s'", mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $delete_api_key));
        $result = mysqli_query($this->db, $sql);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if (mysqli_num_rows($result) > 0) {
        $sql1 = sprintf("UPDATE `custom_api_key`  SET `deleted_flag` = 'Y' WHERE `company_id` = '%s' and `api_key` = '%s' and `integration_name` = '%s'",
        mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $delete_api_key),mysqli_real_escape_string($this->db, $delete_api_name));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $custom_api_details = $this->getCustomAPIkeydetailsInDB($company_id,1);
            $out = array('status' => "Success", "msg" => "Custom API Key successfully deleted.","list" => $custom_api_details);
            $this->response($this->json($out), 200);
        }
            }else{
                $out = array('status' => "Success", "msg" => "Custom API Key doesnot exists");
                $this->response($this->json($out), 200);
    }
        }
    }
    protected function updateZapierTrigger($company_id,$target_url,$zap_name,$zap_event_type,$zap_category_type,$zap_api_key){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//       $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        $current_date = date("Y-m-d");
//        check for same URL
        $sql = sprintf("SELECT target_url,event_type,category_type FROM `zap_subscribe` WHERE `company_id`='%s' AND `target_url`='%s' AND `deleted_flag`!='Y'",
                    mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $target_url));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $sql1 = sprintf("UPDATE `zap_subscribe`  SET `event_type` = '%s',`zap_name` = '%s' WHERE `target_url` = '%s' AND `category_type` = '%s' AND `api_key` = '%s' ",
                mysqli_real_escape_string($this->db, $zap_event_type),mysqli_real_escape_string($this->db, $zap_name),mysqli_real_escape_string($this->db, $target_url),mysqli_real_escape_string($this->db,$zap_category_type),mysqli_real_escape_string($this->db,$zap_api_key));
                $result1 = mysqli_query($this->db, $sql1);
                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $zap_details = $this->getzapierdetailsInDB($company_id,1);
                    $out = array('status' => "Success", "msg" => "Trigger successfully updated.","list" => $zap_details);
                    $this->response($this->json($out), 200);
                }
            }else{
                $sql2 = sprintf("INSERT INTO `zap_subscribe` (`company_id`,`target_url`,`zap_name`,`event_type`,`category_type`,`api_key`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')",
                mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $target_url),mysqli_real_escape_string($this->db, $zap_name), mysqli_real_escape_string($this->db, $zap_event_type),mysqli_real_escape_string($this->db,$zap_category_type),mysqli_real_escape_string($this->db,$zap_api_key));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $zap_details = $this->getzapierdetailsInDB($company_id,1);
                    $out = array('status' => "Success", "msg" => "Trigger successfully added.","list" => $zap_details);
                    $this->response($this->json($out), 200);
                }
            }
        }
    }
    protected function testZapTriggerInDB($company_id,$test_target_url) {
        $temp_body = [];
        $auth[] = "Accept: application/json";
        $auth[] = "Content-Type: application/json";
        $auth[] = "X-Hook-Test: true";
        
        $temp_body = ["Id"=>"x","Buyer First Name" =>"xyz","Buyer Last Name" =>"abc","Mobile Phone" =>"123456789","Email" =>"abc@gmail.com","Status" =>"Active","Program Interest" =>"Yoga","Source" =>"Google","Participant First Name" =>"xyz","Participant Last Name" =>"abc","Communication Campaign Status"=>"yes"];
        $body = JSON_ENCODE($temp_body);
        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "$test_target_url");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $auth);
        $result = curl_exec($ch);
        $resultArr = json_decode($result, true);
        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        if(!isset($resultArr['error']) && ($returnCode==200 && $returnCode!=0)){
            $succ_log = array("Curl : " => "Success($returnCode)", "Method: " => "POST", "Url: " => $test_target_url, "Body: " => $body, "Result: " => $result);
            log_info($this->json($succ_log));
        }else{
            $error_log = array("Curl error: " => curl_error($ch)."($returnCode)", "Method: " => "POST", "Url: " => $test_target_url, "Body: " => $body, "Result : " => $result);
            log_info($this->json($error_log));
        }
        curl_close($ch);
        $msg = array("status" => "Success", "msg" => "Sample data successfully sent to Zapier.",);
        $this->response($this->json($msg), 200);
    }
    protected function deleteZapierTriggerInDB($company_id,$target_url) {
        
        $sql = sprintf("SELECT * FROM `zap_subscribe` WHERE `company_id`='%s' AND `target_url` = '%s'", mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $target_url));
        $result = mysqli_query($this->db, $sql);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if (mysqli_num_rows($result) > 0) {
                $sql1 = sprintf("UPDATE `zap_subscribe`  SET `deleted_flag` = 'Y' WHERE `company_id` = '%s' AND `target_url` = '%s'",
                mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $target_url));
                $result1 = mysqli_query($this->db, $sql1);
                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $zap_details = $this->getzapierdetailsInDB($company_id,1);
                    $out = array('status' => "Success", "msg" => "Trigger successfully deleted.","list" => $zap_details);
                    $this->response($this->json($out), 200);
                }
            }else{
                $out = array('status' => "Success", "msg" => "Deleted Trigger doesnot exists");
                $this->response($this->json($out), 200);
            }
        }
    }
    protected function getzapierdetailsInDB($company_id, $callback) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $tz_str = str_replace("_"," ",explode("/", $new_timezone))[1]." time (GMT ".DATE("P").")";
        $tz = date('T');
        $out =[];
        $sql1 = sprintf("SELECT * FROM `zap_subscribe` WHERE `company_id`='%s' AND `deleted_flag`!='Y' order by `created_dt` desc", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql1);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
             $api_key = $this->getzapierApiKeyInDB($company_id);
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $out[] = $row;
                }
                $output = array('status' => "Success", "api_key" => $api_key, "list" => $out,"tz"=>$tz);
                if ($callback == 1) {
                    return $output;
                } else {
                    $this->response($this->json($output), 200);
                }
            }else{
                $output = array('status' => "Success", "api_key" => $api_key, "list" => $out,"tz"=>$tz);
                if ($callback == 1) {
                    return $output;
                } else {
                    $this->response($this->json($output), 200);
                }
            }
        }
    }
    
    protected function createsplashimage($x,$y,$img_height,$img_width,$res_width,$res_height,$name,$filename,$red,$green,$blue){
        list($width, $height) = getimagesize($filename);
            $imagetruecolor = imagecreatetruecolor($res_width, $res_height);
            $white = imagecolorallocate($imagetruecolor, $red, $green, $blue);
            imagefill($imagetruecolor, 0, 0, $white);
            $newimage = imagecreatefrompng($filename);
            imagecopyresampled($imagetruecolor, $newimage, $x, $y, 0, 0, $img_width, $img_height, $width, $height);
            imagepng($imagetruecolor, "$name.png", 9);
    }
    
    protected function createiconandroid($filename,$res,$name){
        list($width, $height) = getimagesize($filename);
        $imagetruecolor = imagecreatetruecolor($res, $res);
        $white = imagecolorallocate($imagetruecolor, 255, 255, 255); //white background
        imagefill($imagetruecolor, 0, 0, $white);
        $newimage = imagecreatefrompng($filename);
        imagecopyresampled($imagetruecolor, $newimage, 0, 0, 0, 0, $res, $res, $width, $height);
        imagepng($imagetruecolor, $name, 9);
    }
    protected  function createandviewimagesandroid($company_id,$splash_flag,$icon_content,$hex_value,$android_zip,$bundle_id){
        $red = hexdec("$hex_value[1]$hex_value[2]"); 
        $green = hexdec("$hex_value[3]$hex_value[4]"); 
        $blue = hexdec("$hex_value[5]$hex_value[6]"); 
        $old = getcwd(); // Save the current directory
        $whitelabel_dir_name = "../../../$this->upload_folder_name/whitelabel";
        chdir($whitelabel_dir_name);
        mkdir("$bundle_id");
        chmod($bundle_id,0777);
        chdir($bundle_id);//go bundle id dir
        mkdir("android");
        chmod("android",0777);
        chdir("android");//go bundle id dir
        $filename = "icon.png";
        $icon_content_encoded = explode("base64,", $icon_content);
        $file_content_decoded = base64_decode($icon_content_encoded[1]);
        $ifp = fopen($filename, "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
        $this->createiconandroid($filename,48,"icon_temp.png");
        mkdir("drawable-hdpi");
        chmod("drawable-hdpi",0777);
        copy('icon_temp.png', 'drawable-hdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,144,"icon_temp.png");
        mkdir("drawable-xxhdpi");
        chmod("drawable-xxhdpi",0777);
        copy('icon_temp.png', 'drawable-xxhdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,48,"icon_temp.png");
        mkdir("drawable-mdpi");
        chmod("drawable-mdpi",0777);
        copy('icon_temp.png', 'drawable-mdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,144,"icon_temp.png");
        mkdir("drawable-xxxhdpi");
        chmod("drawable-xxxhdpi",0777);
        copy('icon_temp.png', 'drawable-xxxhdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,72,"icon_temp.png");
        mkdir("mipmap-hdpi");
        chmod("mipmap-hdpi",0777);
        copy('icon_temp.png', 'mipmap-hdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,36,"icon_temp.png");
        mkdir("mipmap-ldpi");
        chmod("mipmap-ldpi",0777);
        copy('icon_temp.png', 'mipmap-ldpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,48,"icon_temp.png");
        mkdir("mipmap-mdpi");
        chmod("mipmap-mdpi",0777);
        copy('icon_temp.png', 'mipmap-mdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,96,"icon_temp.png");
        mkdir("mipmap-xhdpi");
        chmod("mipmap-xhdpi",0777);
        copy('icon_temp.png', 'mipmap-xhdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,144,"icon_temp.png");
        mkdir("mipmap-xxhdpi");
        chmod("mipmap-xxhdpi",0777);
        copy('icon_temp.png', 'mipmap-xxhdpi/icon.png');
        unlink('icon_temp.png');
        
        $this->createiconandroid($filename,192,"icon_temp.png");
        mkdir("mipmap-xxxhdpi");
        chmod("mipmap-xxxhdpi",0777);
        copy('icon_temp.png', 'mipmap-xxxhdpi/icon.png');
        unlink('icon_temp.png');
        unlink('icon.png');
        chdir($old);
        //splash
        chdir($whitelabel_dir_name."/".$bundle_id."/android");
        $filename = "icon.png";
        $icon_content_encoded = explode("base64,", $icon_content);
        $file_content_decoded = base64_decode($icon_content_encoded[1]);
        $ifp = fopen($filename, "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
        $this->createsplashimage(300,140,200,200,800,480,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-land-hdpi");
        chmod("drawable-land-hdpi",0777);
        copy('screen.png', 'drawable-land-hdpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(110,50,100,100,320,200,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-land-ldpi");
        chmod("drawable-land-ldpi",0777);
        copy('screen.png', 'drawable-land-ldpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(110,50,100,100,720,1280,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-port-xhdpi");
        chmod("drawable-port-xhdpi",0777);
        copy('screen.png', 'drawable-port-xhdpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(140,60,200,200,480,320,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-land-mdpi");
        chmod("drawable-land-mdpi",0777);
        copy('screen.png', 'drawable-land-mdpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(440,160,400,400,1280,720,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-land-xhdpi");
        chmod("drawable-land-xhdpi",0777);
        copy('screen.png', 'drawable-land-xhdpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(140,300,200,200,480,800,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-port-hdpi");
        chmod("drawable-port-hdpi",0777);
        copy('screen.png', 'drawable-port-hdpi/screen.png');
        unlink('screen.png');
        
        $this->createsplashimage(50,110,100,100,200,320,"screen",$filename,$red,$green,$blue);
        mkdir("drawable-port-ldpi");
        chmod("drawable-port-ldpi",0777);
        copy('screen.png', 'drawable-port-ldpi/screen.png');
        unlink('screen.png');
        unlink('icon.png');
    }
    
    protected  function createiconsandsplashios($company_id,$splash_flag,$icon_content,$hex_value,$ios_zip,$bundle_id){
        $red = hexdec("$hex_value[1]$hex_value[2]"); 
        $green = hexdec("$hex_value[3]$hex_value[4]"); 
        $blue = hexdec("$hex_value[5]$hex_value[6]"); 
        $old = getcwd(); // Save the current directory
        $this->createandviewimagesandroid($company_id,$splash_flag,$icon_content,$hex_value,$ios_zip,$bundle_id);
        chdir($old);
        $whitelabel_dir_name = "../../../$this->upload_folder_name/whitelabel/$bundle_id";
        chdir($whitelabel_dir_name);
        mkdir("ios");
        chmod("ios",0777);
        chdir("ios");//go bundle id dir
        mkdir("icons");
        chmod("icons",0777);
        chdir("icons");
        $filename = "icon.png";
        $icon_content_encoded = explode("base64,", $icon_content);
        $file_content_decoded = base64_decode($icon_content_encoded[1]);
        $ifp = fopen($filename, "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
        $resolution_array = [20,40,60,29,58,87,40,80,120,57,114,60,120,180,72,144,76,152,228,167,50,100,1024];
        for($i = 0 ; $i < count($resolution_array) ; $i++){
            list($width,$height) = getimagesize($filename);
            $imagetruecolor = imagecreatetruecolor($resolution_array[$i], $resolution_array[$i]);
            $white = imagecolorallocate($imagetruecolor, 255, 255, 255);//white background
            imagefill($imagetruecolor, 0, 0, $white);
            $newimage = imagecreatefrompng($filename);
            imagecopyresampled($imagetruecolor, $newimage, 0, 0, 0, 0, $resolution_array[$i], $resolution_array[$i], $width, $height);
            if($i == 0){
            imagepng($imagetruecolor, "Icon-App-20x20@1x.png", 9);
            }else if($i == 1){
            imagepng($imagetruecolor, "Icon-App-20x20@2x.png", 9);
            }else if($i == 2){
            imagepng($imagetruecolor, "Icon-App-20x20@3x.png", 9);
            }else if($i == 3){
            imagepng($imagetruecolor, "Icon-App-29x29@1x.png", 9);
            }else if($i == 4){
            imagepng($imagetruecolor, "Icon-App-29x29@2x.png", 9);
            }else if($i == 5){
            imagepng($imagetruecolor, "Icon-App-29x29@3x.png", 9);
            }else if($i == 6){
            imagepng($imagetruecolor, "Icon-App-40x40@1x.png", 9);
            }else if($i == 7){
            imagepng($imagetruecolor, "Icon-App-40x40@2x.png", 9);
            }else if($i == 8){
            imagepng($imagetruecolor, "Icon-App-40x40@3x.png", 9);
            }else if($i == 9){
            imagepng($imagetruecolor, "Icon-App-57x57@1x.png", 9);
            }else if($i == 10){
            imagepng($imagetruecolor, "Icon-App-57x57@2x.png", 9);
            }else if($i == 11){
            imagepng($imagetruecolor, "Icon-App-60x60@1x.png", 9);
            }else if($i == 12){
            imagepng($imagetruecolor, "Icon-App-60x60@2x.png", 9);
            }else if($i == 13){
            imagepng($imagetruecolor, "Icon-App-60x60@3x.png", 9);
            }else if($i == 14){
            imagepng($imagetruecolor, "Icon-App-72x72@1x.png", 9);
            }else if($i == 15){
            imagepng($imagetruecolor, "Icon-App-72x72@2x.png", 9);
            }else if($i == 16){
            imagepng($imagetruecolor, "Icon-App-76x76@1x.png", 9);
            }else if($i == 17){
            imagepng($imagetruecolor, "Icon-App-76x76@2x.png", 9);
            }else if($i == 18){
            imagepng($imagetruecolor, "Icon-App-76x76@3x.png", 9);
            }else if($i == 19){
            imagepng($imagetruecolor, "Icon-App-83.5x83.5@2x.png", 9);
            }else if($i == 20){
            imagepng($imagetruecolor, "Icon-Small-50x50@1x.png", 9);
            }else if($i == 21){
            imagepng($imagetruecolor, "Icon-Small-50x50@2x.png", 9);
            }else if($i == 22){
            imagepng($imagetruecolor, "Icon-App-1024x1024@1x.png", 9);
            }
        }
        unlink("icon.png");
        chdir($old);
        if($splash_flag == "G"){
        chdir($whitelabel_dir_name);//go whitelabel folder
        chdir("ios");
        mkdir("launch_screen");
        chmod("launch_screen",0777);
        chdir("launch_screen");
        $filename = "icon.png";
        $icon_content_encoded = explode("base64,", $icon_content);
        $file_content_decoded = base64_decode($icon_content_encoded[1]);
        $ifp = fopen($filename, "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
//        for 640 * 1136
        $this->createsplashimage(170,418,300,300,640,1136,"Default-568h@2x~iphone",$filename,$red,$green,$blue);
        $this->createsplashimage(225,502,300,300,750,1334,"Default-667h",$filename,$red,$green,$blue);
        $this->createsplashimage(170,330,300,300,640,960,"Default@2x~iphone",$filename,$red,$green,$blue);
//        for iphone x
        $this->createsplashimage(262.5,918,600,600,1125,2436,"iphonex",$filename,$red,$green,$blue);
//        for 1242 x 2208
        $this->createsplashimage(321,804,600,600,1242,2208,"Default-736h",$filename,$red,$green,$blue);
        // for ipad portrait 768 x 1024
        $this->createsplashimage(234,362,300,300,768,1024,"Default-Portrait~ipad",$filename,$red,$green,$blue);
        // for ipad portrait @2x 1536 x 2048
        $this->createsplashimage(468,724,600,600,1536,2048,"Default-Portrait@2x~ipad",$filename,$red,$green,$blue);
        // for ipad landscape 2208 x 1242
        $this->createsplashimage(804,321,600,600,2208,1242,"Default-Landscape-736h",$filename,$red,$green,$blue);
        // for ipad landscape 2048 x 1536
        $this->createsplashimage(724,468,600,600,2048,1536,"Default-Landscape@2x~ipad",$filename,$red,$green,$blue);
        // for ipad landscape 1024 x 768
        $this->createsplashimage(362,234,300,300,1024,768,"Default-Landscape~ipad",$filename,$red,$green,$blue);
        // for DEAFULT iphone 320 x 480
        $this->createsplashimage(60,140,200,200,320,480,"Default~iphone",$filename,$red,$green,$blue);
        // for iphonex landscape 2436 x 1125
        $this->createsplashimage(918,262.5,600,600,2436,1125,"iphonex-lanscape",$filename,$red,$green,$blue);
        unlink("icon.png");
        }else{
            $this->ioszipextract($bundle_id,$ios_zip);
        }
        chdir($old);
        $this->savezipinS3($bundle_id);
        
        chdir("../../../$this->upload_folder_name/whitelabel");
        system('rm -rf ' . escapeshellarg("$bundle_id"), $retval);
        $error = array('status' => "Success", "msg" => "Uploaded successfully");
        $this->response($this->json($error), 200);
    }
    
    protected function ioszipextract($bundle_id,$ioszip_file){
        $old = getcwd(); // Save the current directory
        $dir_name = "../../../$this->upload_folder_name/whitelabel/$bundle_id";
        chdir($dir_name);
        $ios_zip_enocded = explode("base64,", $ioszip_file);
        $file_content_decoded = base64_decode($ios_zip_enocded[1]);
        $ifp = fopen('ios.zip', "wb");
        fwrite($ifp, $file_content_decoded);
        fclose($ifp);
        mkdir("launch_screen");
        chmod("launch_screen",0777);
        $zip = new ZipArchive;
        $res = $zip->open('ios.zip');
        if ($res === TRUE) {
            $zip->extractTo('launch_screen/');
            $zip->close();
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid zip file");
            $this->response($this->json($error), 200);
        }
        unlink('ios.zip');
        $directory = 'launch_screen';
        $files_count = 12;
        if (! is_dir($directory)) {
            $error = array('status' => "Failed", "msg" => "Invalid diretory path");
            $this->response($this->json($error), 200);
        }
        chdir($directory);
        if (is_dir("__MACOSX")) {
            chmod("__MACOSX",0777);
            $files_count = 13;
        }
        chdir($old);
        chdir($dir_name);
        $files = array();
        foreach (scandir($directory) as $file) {
            if ('.' === $file) continue;
            if ('..' === $file) continue;

            $files[] = $file;
        }
//        for ios image count error
        if(count($files) != $files_count){
            $error = array('status' => "Failed", "msg" => "Some of the images in zip file is missing");
            $this->response($this->json($error), 200);
        }
        $ios_splash_file_names = ["Default-568h@2x~iphone.png","Default-667h.png","Default-736h.png","Default-Landscape-736h.png",
            "Default-Landscape@2x~ipad.png","Default-Landscape~ipad.png","Default-Portrait@2x~ipad.png","Default-Portrait~ipad.png",
            "Default@2x~iphone.png","iphonex.png","Default~iphone.png","iphonex-Landscape.png"];
        chdir("launch_screen");
        for( $j=1 ; $j < count($files) ; $j++ ){//skip 1st index (hidden file)
            if( $files[$j] != "__MACOSX"){
            if (in_array(($files[$j]), $ios_splash_file_names)) {
                $width = $height = 0;
                if($files[$j] == "Default-568h@2x~iphone.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 640 || $height != 1136){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default@2x~iphone.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 640 || $height != 960){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-667h.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 750 || $height != 1334){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "iphonex.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 1125 || $height != 2436){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-736h.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 1242 || $height != 2208){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-Portrait~ipad.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 768 || $height != 1024){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-Portrait@2x~ipad.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 1536 || $height != 2048){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-Landscape-736h.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 2208 || $height != 1242){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-Landscape@2x~ipad.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 2048 || $height != 1536){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default-Landscape~ipad.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 1024 || $height != 768){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "Default~iphone.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 320 || $height != 480){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }else if($files[$j] == "iphonex-Landscape.png"){
                    list($width, $height) = getimagesize($files[$j]);
                    if($width != 2436 || $height != 1125){
                        $error = array('status' => "Failed", "msg" => "$files[$j] file is not in correct resolution");
                        $this->response($this->json($error), 200);
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "$files[$j] file is not valid");
                $this->response($this->json($error), 200);
            }
            }
        }
        chdir($old);
    }
    
    protected function savezipinS3($bundle_id){
        // AWS Info
	$bucketName = 'dev.app-files.mystudio.app';
	$IAM_KEY = 'AKIAIGUWYNJGAHDPJGIA';
	$IAM_SECRET = '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';
	// Connect to AWS
	try {
		$s3 = S3Client::factory(
			array(
				'credentials' => array(
					'key' => $IAM_KEY,
					'secret' => $IAM_SECRET
				),
				'version' => 'latest',
				'region'  => 'us-east-1'
			)
		);
	} catch (Exception $e) {
		$error = array('status' => "Failed", "msg" => $e->getMessage());
                $this->response($this->json($error), 200);
	}
	$old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel/$bundle_id";
                chdir($dir_name);
	// For this, I would generate a unqiue random string for the key name. But you can do whatever.
	// single file upload
//	try {
//		$s3->putObject(
//			array(
//				'Bucket'=>$bucketName,
//				'Key' =>  $bundle_id,
//				'SourceFile' => $bundle_id,
//				'StorageClass' => 'REDUCED_REDUNDANCY'
//			)
//		);
//	} catch (S3Exception $e) {
//		$error = array('status' => "Failed", "msg" => $e->getMessage());
//                $this->response($this->json($error), 200);
//	} catch (Exception $e) {
//		$error = array('status' => "Failed", "msg" => $e->getMessage());
//                $this->response($this->json($error), 200);
//	}
//	
//	
               
//               --------- upload entire folder---------
//                $cmd = $s3->getCommand('GetObject', [
//                    'Bucket' => "$bucketName",
//                    'Key' => "$bundle_id"
//                ]);
//                $request = $s3->createPresignedRequest($cmd, '+20 minutes');
//
//                // Get the actual presigned-url
//                $presignedUrl = (string)$request->getUri();
//                echo "url:$presignedUrl<br>";
//                $promise = $manager->promise();
//
//                // Do something when the transfer is complete using the then() method
//                $promise->then(function () {
//                });
//                $promise->otherwise(function ($reason) {
//                    $error = array('status' => "Failed", "msg" => $reason);
//                    $this->response($this->json($error), 200);
//                });
                
                
                //-------move to android folder-------
                // Where the files will be source from
                $source = $dir_name."/android/";
                // Where the files will be transferred to
                $dest = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/android/res-myclub";
                // Create a transfer object
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                // Perform the transfer synchronously
                $manager->transfer();
                
                //-------move to ios icon folder-------
                // Where the files will be source from
                $source = $dir_name."/ios/icons";
                // Where the files will be transferred to
                $dest = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/myclub.appiconset";
                // Create a transfer object
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                // Perform the transfer synchronously
                $manager->transfer();
                
                //-------move to ios splash folder-------
                // Where the files will be source from
                $source = $dir_name."/ios/launch_screen";
                // Where the files will be transferred to
                $dest = "s3://$bucketName/aws_assets/MyStudioWhiteLabel/platforms/ios/mystudio/Images.xcassets/myclub.launchimage";
                // Create a transfer object
                $manager = new \Aws\S3\Transfer($s3, $source, $dest);
                // Perform the transfer synchronously
                $manager->transfer();
                
                chdir($old);
    }
    
    protected function checkbundleidINDB($company_id,$callback) {
//        $this->savezipinS3();
        $sql1 = sprintf("SELECT m.dev_pem,m.prod_pem,m.dev_pem_expiry,m.prod_pem_expiry,wp.profile_type,wp.profile_expiry_date,w.ios_bundle_id,c.upgrade_status,w.ios_username,w.ios_password,w.ios_teamid,w.ios_developerid,w.ios_account_type,w.mystudio_legacy_app FROM `white_label_apps` w LEFT JOIN white_labels_profile wp USING(ios_bundle_id) LEFT JOIN mobile_push_certificate m on w.ios_bundle_id=m.bundle_id  RIGHT JOIN company c 
                ON c.company_id=w.company_id  AND w.`studio_code`=(SELECT company_code from company where company_id='%s')  WHERE c.`company_id`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql1);
//        log_info($sql1);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
                $out['ios_dev_status'] = 'N';
                $out['ios_prod_status'] = 'N';
                $out['provision_dev_status'] = 'N';
                $out['provision_prod_status'] = 'N';
                $out['aps_dev_status'] = 'N';
                $out['aps_prod_status'] = 'N';
            if (mysqli_num_rows($result) > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $outs[] = $rows;
                    if($rows['profile_type'] == "ICD"){
                        $out['ios_dev_status'] = 'Y';
                        $out['ios_dev_expiry_date'] = $rows['profile_expiry_date'];
                    }else if($rows['profile_type'] == "ICP"){
                        $out['ios_prod_status'] = 'Y';
                        $out['ios_prod_expiry_date'] = $rows['profile_expiry_date'];
                    }else if($rows['profile_type'] == "IPD"){
                        $out['provision_dev_status'] = 'Y';
                        $out['provision_dev_expiry_date'] = $rows['profile_expiry_date'];
                    }else if($rows['profile_type'] == "IPP"){
                        $out['provision_prod_status'] = 'Y';
                        $out['provision_prod_expiry_date'] = $rows['profile_expiry_date'];
                    }
                    if(!empty($rows['dev_pem']) && !empty($rows['dev_pem_expiry'])){
                        $out['aps_dev_status'] = 'Y';
                        $out['dev_pem_expiry'] = $rows['dev_pem_expiry'];
                    }
                    if(!empty($rows['prod_pem']) && !empty($rows['prod_pem_expiry'])){
                        $out['aps_prod_status'] = 'Y';
                        $out['prod_pem_expiry'] = $rows['prod_pem_expiry'];
                    }
                }
                if($callback == 0){
                    if(empty($outs[0]['ios_bundle_id']) || is_null($outs[0]['ios_bundle_id'])){
                        $output = array('status' => "Success", "bundle_id_status" => "N","upgrade_status"=>$outs[0]['upgrade_status'],"bundle_id"=>'',"msg"=>$outs[0]);
                    }else{
                        if(!is_null($outs[0]['ios_password']) && !empty($outs[0]['ios_password'])){
                            $outs[0]['ios_password'] = $this->decryptPassword($outs[0]['ios_password']);
                        }else{
                            $outs[0]['ios_password'] = '';
                        }
                        $output = array('status' => "Success", "bundle_id_status" => "Y","bundle_id"=>$outs[0]['ios_bundle_id'],"upgrade_status"=>$outs[0]['upgrade_status'],"msg"=>$outs[0],"msgs"=>"Bundle id updated successfully.","file_flag"=>$out);
                    }
                }else{
                    return $out;
                }
            }else{
                if($callback == 0){
                    $output = array('status' => "Success", "bundle_id_status" => "N");
                }else{
                    return $out;
                }
            }
            $this->response($this->json($output), 200);
        }
    }
    
    protected function updatebundleidINDB($company_id,$bundle_id) {
        
        $sql1 = sprintf("SELECT * FROM `white_label_apps` WHERE `ios_bundle_id`='%s'", mysqli_real_escape_string($this->db, $bundle_id));
        $result = mysqli_query($this->db, $sql1);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) == 1) {
//                $sql2 = sprintf("UPDATE `white_label_apps` SET company_id= '%s',`studio_code`=(SELECT company_code from company where company_id='%s') WHERE `ios_bundle_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $bundle_id));
//                $result2 = mysqli_query($this->db, $sql2);
//                if (!$result2) {
//                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                    log_info($this->json($error_log));
//                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                    $this->response($this->json($error), 200);
//                }else{
//                    $this->checkbundleidINDB($company_id,0);
//                }
                    $error = array('status' => "Failed", "msg" => "Bundle ID already exist. Please contact administrator");
                    $this->response($this->json($error), 200);
            }else{
                $sql2 = sprintf("UPDATE `white_label_apps` SET ios_bundle_id = '%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $bundle_id), mysqli_real_escape_string($this->db, $company_id));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $this->checkbundleidINDB($company_id,0);
                }
            }
        }
    }
    
    
    protected function uploadwhitelabelcertINDB($company_id,$ios_development_cer,$ios_distribution_cer,$aps_development_cer,$aps_Production_cer,$AppName_development_cer,$AppName_distribution_cer,$ios_distribution_cer_name,$aps_development_cer_name,$aps_Production_cer_name,$AppName_development_cer_name,$AppName_distribution_cer_name,$ios_development_cer_name,$ios_account_type) {
        
        $ios_development_cer_enocded = explode("base64,", $ios_development_cer);
        $ios_distribution_cer_enocded = explode("base64,", $ios_distribution_cer);
        $aps_development_cer_enocded = explode("base64,", $aps_development_cer);
        $aps_Production_cer_enocded = explode("base64,", $aps_Production_cer);
        $AppName_development_cer_enocded = explode("base64,", $AppName_development_cer);
        $AppName_distribution_cer_enocded = explode("base64,", $AppName_distribution_cer);
        
        $sql1 = sprintf("SELECT `ios_teamid`,`ios_bundle_id` From  `white_label_apps` WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                $row1 = mysqli_fetch_assoc($result1);
                $teamid = $row1['ios_teamid'];
                $bundle_id = $row1['ios_bundle_id'];
            }
        }

        if($ios_account_type == "I" && (!empty($ios_development_cer) || !empty($ios_distribution_cer) || !empty($aps_development_cer)
                 || !empty($aps_Production_cer) || !empty($AppName_development_cer) || !empty($AppName_distribution_cer))){
//      starts
        $profile_types = ["ICD","ICP","IPD","IPP"];
            
        for ($i = 0; $i < count($profile_types); $i++) {
            $file_content = "";
            $file_content = ($profile_types[$i] == "ICD" ? $ios_development_cer_enocded : (($profile_types[$i] == "ICP" ? $ios_distribution_cer_enocded : (($profile_types[$i] == "IPD" ? $AppName_development_cer_enocded : ($AppName_distribution_cer_enocded) )) )) );
            $file_content_name = "";
            
            if(empty(trim($file_content[0]))){
                continue;
            }
            $file_content_decoded = base64_decode($file_content[1]);
            if ($profile_types[$i] == "ICD") {
                $file_content_name = (explode("fakepath\\", $ios_development_cer_name));
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $textfile = "temp.txt";
                $textfileopen = fopen($textfile, "wb");
                fwrite($textfileopen, $file_content_decoded);
                fclose($textfileopen);
                if( exec("grep 'iPhone Developer' temp.txt")) {
                    unlink("$textfile");
                    $file_content_name[1] = "ios_development.cer";
                    chdir($old);
                }else{
                    unlink($textfile);
                    chdir($old);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
            } else if ($profile_types[$i] == "ICP") {
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $file_content_name = (explode("fakepath\\", $ios_distribution_cer_name));
                $textfile = "temp.txt";
                $textfileopen = fopen($textfile, "wb");
                fwrite($textfileopen, $file_content_decoded);
                fclose($textfileopen);
                if( exec("grep 'iPhone Distribution' temp.txt")) {
                    unlink("$textfile");
                    $file_content_name[1] = "ios_distribution.cer";
                    chdir($old);
                }else{
                    unlink($textfile);
                    chdir($old);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
            } else if ($profile_types[$i] == "IPD") {
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $file_content_name = (explode("fakepath\\", $AppName_development_cer_name));
                $textfile = "temp.mobileprovision";
                $textfileopen = fopen("temp.mobileprovision", "wb");
                fwrite($textfileopen, $file_content_decoded);
                fclose($textfileopen);
                exec("openssl smime -inform der -verify -noverify -in $textfile > tmp.plist");
                $xml=simplexml_load_file("tmp.plist") or die("Error: Cannot create object");
                $jsonxml = json_encode($xml);
                $jsondecoded = json_decode($jsonxml,true);
                if($jsondecoded['dict']['dict']['string'][0] == "development"){
                    unlink("tmp.plist");
                    unlink($textfile);
                    $file_content_name[1] = str_replace(" ","_",$jsondecoded['dict']['string'][0]);
                }else{
                    unlink("tmp.plist");
                    unlink($textfile);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
                chdir($old);
            } else {
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $file_content_name = (explode("fakepath\\", $AppName_distribution_cer_name));
                $textfile = "temp.mobileprovision";
                $textfileopen = fopen("temp.mobileprovision", "wb");
                fwrite($textfileopen, $file_content_decoded);
                fclose($textfileopen);
                exec("openssl smime -inform der -verify -noverify -in $textfile > tmp.plist");
                $xml=simplexml_load_file("tmp.plist") or die("Error: Cannot create object");
                $jsonxml = json_encode($xml);
                $jsondecoded = json_decode($jsonxml,true);
                if($jsondecoded['dict']['dict']['string'][0] == "production"){
                    unlink("tmp.plist");
                    unlink($textfile);
                    $file_content_name[1] = str_replace(" ","_",$jsondecoded['dict']['string'][0]);
                }else{
                    unlink("tmp.plist");
                    unlink($textfile);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
                chdir($old);
            }
            //validation
            $old = getcwd(); // Save the current directory
            $dir_name = "../../../$this->upload_folder_name/whitelabel";
            chdir($dir_name);
            $file = $file_content_name[1];
            $ifp = fopen($file, "wb");
            fwrite($ifp, $file_content_decoded);
            fclose($ifp);
            if($profile_types[$i] == "ICP" || $profile_types[$i] == "ICD"){
                $textfile =(explode(".", $file));
                $textfile = $textfile[0] . ".txt";
                $textfileopen = fopen($textfile, "wb");
                fwrite($textfileopen, $file_content_decoded);
                if( exec("grep '".escapeshellarg("$teamid") . "' ./$textfile")) {
                    fclose($textfileopen);
                    unlink("$textfile");
                }else{
                    fclose($textfileopen);
                    unlink($file_content_name[1]);//delete file
                    unlink("$textfile");
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] and Team ID mismatch");
                    $this->response($this->json($error), 200);
                }
            }
            $exp_date_ssl = $exp_date_ssl_format=$day= $mon = $year= $currentdate= $currentplus15days=$team_id="";
            if($profile_types[$i] == "ICD" || $profile_types[$i] == "ICP"){
                exec("openssl x509 -in $file_content_name[1] -inform DER -out aps.pem -outform PEM");
                exec("openssl pkcs12 -export -inkey app.key -in aps.pem -out aps.p12 -password pass:mystudio");
                exec("openssl pkcs12 -in aps.p12 -out prod.pem -nodes -clcerts -password pass:mystudio");
                $exp_date_ssl = exec('openssl x509 -enddate -noout -in prod.pem');
                log_info($file_content_name[1] . " : $exp_date_ssl");
                $date = substr($exp_date_ssl,9);
                $exp_date_ssl = strtotime($date);
            }else{
                exec("openssl smime -inform der -verify -noverify -in $file_content_name[1] > tmp.plist");
                $xml=simplexml_load_file("tmp.plist") or die("Error: Cannot create object");
                $jsonxml = json_encode($xml);
                $jsondecoded = json_decode($jsonxml,true);
                $team_id = $jsondecoded['dict']['array'][0]['string'];
                $bundle_id_fromxml = $jsondecoded['dict']['dict']['string'][1];
                $bundle_id_fromxml = trim(str_replace($team_id.".","",$bundle_id_fromxml));
                if($bundle_id_fromxml != $bundle_id) {
                    $error = array('status' => "Failed", "msg" => "Bundle ID and certificate mismatch");
                    $this->response($this->json($error), 200);
                }
                log_info($file_content_name[1] . " : " . $jsondecoded['dict']['date'][1]);
                $exp_date_ssl = strtotime($jsondecoded['dict']['date'][1]);
                unlink("tmp.plist");
//                $exp_date_ssl = exec("/usr/libexec/PlistBuddy -c 'Print DeveloperCertificates:0' /dev/stdin <<< $(security cms -D -i $file_content_name[1]) | openssl x509 -inform DER -noout -enddate");
            }
            $exp_date = date("Y-m-d", $exp_date_ssl);
//            log_info("Exp date : ".$exp_date_ssl);
            unlink($file_content_name[1]);//delete file
            if($team_id != $teamid && ($profile_types[$i] == "IPD" || $profile_types[$i] == "IPP")){
                $error = array('status' => "Failed", "msg" => "Team ID & certificates doesn't match.");
                $this->response($this->json($error), 200);
            }
            if($profile_types[$i] == "ICD" || $profile_types[$i] == "ICP"){
                unlink("aps.p12");//delete .p12 file
                unlink("aps.pem");//delete aps.pem
                unlink("prod.pem");//delete prod.pem file
            }
            if($profile_types[$i] == "ICD" || $profile_types[$i] == "ICP"){
                $currentplus15days = date("Y-m-d", strtotime("+15 days"));
            }else{
                $currentplus15days = date("Y-m-d", strtotime("+30 days"));
            }
            $currentplus15days = strtotime($currentplus15days);
            if($exp_date_ssl<$currentplus15days){
                $error = array('status' => "Failed", "msg" => "$file_content_name[1] is expired. Please upload new file!");
                $this->response($this->json($error), 200);
            }
            chdir($old); // Restore the old working directory
            
            $sql1 = sprintf("SELECT * FROM `white_labels_profile` WHERE `ios_bundle_id`='%s' and profile_type='%s'", mysqli_real_escape_string($this->db, $bundle_id), mysqli_real_escape_string($this->db, $profile_types[$i]));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if (mysqli_num_rows($result) == 0) {
                    $sql2 = sprintf("INSERT INTO `white_labels_profile` (`app_id`,`ios_bundle_id`,`app_name`,`profile_type`,`profile_name`,`app_profile`, `profile_expiry_date`) SELECT `app_id`,`ios_bundle_id`,`app_name`,'$profile_types[$i]','$file_content_name[1]','$file_content[1]', '$exp_date' from `white_label_apps` where `ios_bundle_id` = '$bundle_id' ");
                    $result1 = mysqli_query($this->db, $sql2);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                } else {
                    $sql2 = sprintf("UPDATE `white_labels_profile` SET profile_name='%s',app_profile='%s', `profile_expiry_date` = '%s' where `ios_bundle_id` = '%s' and profile_type='%s'", mysqli_real_escape_string($this->db, $file_content_name[1]), mysqli_real_escape_string($this->db, $file_content[1]), mysqli_real_escape_string($this->db, $exp_date), mysqli_real_escape_string($this->db, $bundle_id), mysqli_real_escape_string($this->db, $profile_types[$i]));
                    $result1 = mysqli_query($this->db, $sql2);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        
        //ssl prod  and dev
        $ssl_types = ["dev","prod"];
        $file_contents_array = [];
        $file_contents_array[0]['content'] = "";
        $file_contents_array[0]['expdate'] = "";
        $file_contents_array[1]['content'] = "";
        $file_contents_array[1]['expdate'] = "";
        for($j = 0 ;$j < count($ssl_types) ; $j++){
            $file_contents = [];
            $file_contents = ($ssl_types[$j] == "dev" ? $aps_development_cer_enocded : $aps_Production_cer_enocded);
            $file_content_name = "";
            if(empty(trim($file_contents[0]))){
                continue;
            }
            $file_contents_decoded = base64_decode($file_contents[1]);
            if ($ssl_types[$j] == "dev") {
                $file_content_name = (explode("fakepath\\", $aps_development_cer_name));
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $textfile = "temp.txt";
                $textfileopen = fopen($textfile, "wb");
                fwrite($textfileopen, $file_contents_decoded);
                fclose($textfileopen);
                if( exec("grep 'Apple Development IOS Push Services' temp.txt")) {
                    unlink("$textfile");
                    $file_content_name[1] = "aps_development.cer";
                    chdir($old);
                }else{
                    unlink($textfile);
                    chdir($old);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
            }else{
                $file_content_name = (explode("fakepath\\", $aps_Production_cer_name));
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/whitelabel";
                chdir($dir_name);
                $textfile = "temp.txt";
                $textfileopen = fopen($textfile, "wb");
                fwrite($textfileopen, $file_contents_decoded);
                fclose($textfileopen);
                if( exec("grep 'Apple Push Services' temp.txt")) {
                    unlink("$textfile");
                    $file_content_name[1] = "aps_production.cer";
                    chdir($old);
                }else{
                    unlink($textfile);
                    chdir($old);
                    $error = array('status' => "Failed", "msg" => "$file_content_name[1] is not valid");
                    $this->response($this->json($error), 200);
                }
            }
            //validation
            $old = getcwd(); // Save the current directory
            $dir_name = "../../../$this->upload_folder_name/whitelabel";
            chdir($dir_name);
            $file = $file_content_name[1];
            $ifp = fopen($file, "wb");
            fwrite($ifp, $file_contents_decoded);
            fclose($ifp);
            $exp_date_ssl = $exp_date_ssl_format=$day= $mon = $year= $currentdate= $currentplus15days="";
            exec("openssl x509 -in $file_content_name[1] -inform DER -out aps.pem -outform PEM");
            exec("openssl pkcs12 -export -inkey app.key -in aps.pem -out aps.p12 -password pass:mystudio");
            exec("openssl pkcs12 -in aps.p12 -out prod.pem -nodes -clcerts -password pass:mystudio");
            $exp_date_ssl = exec('openssl x509 -enddate -noout -in prod.pem');
//                exec("security cms -D -i $file_content_name[1] > tmp.plist");
            $file_contents_array[$j]['content'] = file_get_contents("prod.pem");//pem files array
            unlink($file_content_name[1]);//delete file
            unlink("aps.p12");//delete .p12 file
            unlink("aps.pem");//delete aps.pem
            unlink("prod.pem");//delete prod.pem file
            $date = substr($exp_date_ssl,9);
//            $mon = substr($exp_date_ssl,9,3);
//            $day = substr($exp_date_ssl,13,2);
//            $year = substr($exp_date_ssl,25,4);
            $exp_date_ssl = strtotime($date);
            $exp_date2 = date("Y-m-d", $exp_date_ssl);
             $file_contents_array[$j]['expdate'] = $exp_date2; // EXPIRATION DATE 
            $currentplus15days = date("Y-m-d", strtotime("+30 days"));
            $currentplus15days = strtotime($currentplus15days);
            if($exp_date_ssl<$currentplus15days){
                $error = array('status' => "Failed", "msg" => "$file_content_name[1] is expired. Please upload new file!");
                $this->response($this->json($error), 200);
            }
            chdir($old); // Restore the old working directory
        }
        $sql1 = sprintf("SELECT * FROM `mobile_push_certificate` WHERE `bundle_id`='%s'", mysqli_real_escape_string($this->db, $bundle_id));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if (mysqli_num_rows($result) == 0) {
                    $sql2 = sprintf("INSERT INTO `mobile_push_certificate` (`type`,`bundle_id`,`dev_pem`,`prod_pem`,`dev_pem_expiry`, `prod_pem_expiry`) values ('I','%s','%s','%s','%s','%s') ", mysqli_real_escape_string($this->db, $bundle_id), mysqli_real_escape_string($this->db, $file_contents_array[0]['content']), mysqli_real_escape_string($this->db, $file_contents_array[1]['content']), mysqli_real_escape_string($this->db, $file_contents_array[0]['expdate']), mysqli_real_escape_string($this->db, $file_contents_array[1]['expdate']));
                    $result1 = mysqli_query($this->db, $sql2);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                } else {
                    if(!empty($file_contents_array[0]['content'])){
                    $sql2 = sprintf("UPDATE `mobile_push_certificate` SET dev_pem='%s', dev_pem_expiry = '%s' where `bundle_id` = '%s'", mysqli_real_escape_string($this->db, $file_contents_array[0]['content']), mysqli_real_escape_string($this->db, $file_contents_array[0]['expdate']), mysqli_real_escape_string($this->db, $bundle_id));
                    $result1 = mysqli_query($this->db, $sql2);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                    }
                    if(!empty($file_contents_array[1]['content'])){
                    $sql3 = sprintf("UPDATE `mobile_push_certificate` SET prod_pem='%s', prod_pem_expiry = '%s' where `bundle_id` = '%s'", mysqli_real_escape_string($this->db, $file_contents_array[1]['content']), mysqli_real_escape_string($this->db, $file_contents_array[1]['expdate']), mysqli_real_escape_string($this->db, $bundle_id));
                    $result2 = mysqli_query($this->db, $sql3);
                    if (!$result2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                    }
                }
            }
        }
        
        $all_updated_status = $this->checkbundleidINDB($company_id,1);
        if($all_updated_status['ios_dev_status'] == "Y" && $all_updated_status['ios_prod_status'] == "Y" && $all_updated_status['provision_dev_status'] == "Y" && 
           $all_updated_status['provision_prod_status'] == "Y" && $all_updated_status['aps_dev_status'] == "Y" && $all_updated_status['aps_prod_status'] == "Y"){
            //update flag
            $sql3 = sprintf("UPDATE `white_label_apps` SET all_files_uploaded_flag='R' where `ios_bundle_id` = '%s'",mysqli_real_escape_string($this->db, $bundle_id));
            $result2 = mysqli_query($this->db, $sql3);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
            $this->sendEmailForAdmins('W',"All files for Company id : $company_id , Bundle ID : $bundle_id uploaded successfully");
        }
        
        $error = array('status' => "Success", "msg" => "Certificates updated successfully");
        $this->response($this->json($error), 200);
        
    }

    protected function updateEmailSubscriptionStatus($email, $status,$company_id) {
        
        if($status == 'U'){
          $type =  "unsubscribed";
          $sql = sprintf("INSERT INTO `bounce_email_verify`(`bounced_mail`, `company_id`, `email_subscription_status`)
                             VALUES('%s', '%s', '%s') ON DUPLICATE KEY UPDATE  `company_id`='%s',`email_subscription_status`='%s'",
                            mysqli_real_escape_string($this->db, $email),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $status),
                            mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $status));
                            
        }else{
          $type =  "subscribed";
          $sql = sprintf("DELETE FROM `bounce_email_verify` WHERE `bounced_mail`='%s' AND `company_id`='%s' AND `email_subscription_status`='U'",
                  mysqli_real_escape_string($this->db, $email),mysqli_real_escape_string($this->db, $company_id));
        }
        
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $output = array('status' => "Success", "type" => $type);
            $this->response($this->json($output), 200);
        }
    }
    
    protected function sendsubscriptionemail($unsubscribe_email, $subject, $message, $company_id, $unsuscribed_cc_list) {
        $company_name = $student_cc_email = $reply_to = '';
        
        $sql1 = sprintf("SELECT `company_name`, `email_id` FROM `company` WHERE `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result1);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result1);
                $company_name = $row['company_name'];
                if (!empty($unsuscribed_cc_list)) {
                    $student_cc_email = implode(",", $unsuscribed_cc_list);
                }
                $reply_to = $row['email_id'];
            }
            $sendEmail_status = $this->sendEmail($unsubscribe_email, $subject, $message, $company_name, $reply_to, '', '', $student_cc_email, $company_id, 'subscribe','');
            if($sendEmail_status['status'] == "true") {
                $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $unsubscribe_email, "Email Message" => $sendEmail_status['mail_status']);
                log_info($this->json($error_log));
                $this->response($this->json($error_log),200);
            } else {
                $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $unsubscribe_email, "Email Message" => $sendEmail_status['mail_status']);
                log_info($this->json($error_log));
                $this->response($this->json($error_log),200);
            }
        }
    }
    
    protected function activationlinkCreationForEmail($unsubscribe_email, $subject, $company_id){
        $original_mail = $unsubscribe_email;
        $unsuscribed_cc_list = [];
        $sql1 = sprintf("SELECT `student_cc_email` FROM `student`  WHERE `student_email`='%s' and `company_id`='%s'", mysqli_real_escape_string($this->db, $unsubscribe_email), mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else {
            while ($row = mysqli_fetch_assoc($result1)) {
                $student_cc_email = trim($row['student_cc_email']);
                if (!empty($student_cc_email)) {
                    $cc_list = explode(",", $student_cc_email);
                    for ($i = 0; $i < count($cc_list); $i++) {
                        $subscribtion_check = sprintf("select * from `bounce_email_verify` where `bounced_mail`='%s' AND `email_subscription_status`='U' AND `company_id`='%s'", $cc_list[$i], mysqli_real_escape_string($this->db, $company_id));
                        $sub_result = mysqli_query($this->db, $subscribtion_check);
                        if (!$sub_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$subscribtion_check");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            $bounce_rows = mysqli_num_rows($sub_result);
                            if ($bounce_rows > 0) {
                                $unsuscribed_cc_list[] = $cc_list[$i];
                                $unsubscribe_email .= "," . $cc_list[$i];
                            }
                        }
                    }
                }
            }
        }

        $tomail = base64_encode($unsubscribe_email);
        $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                $curr_loc = implode('/', explode('/', $curr_loc, -4));
                $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/EmailSubscription.php?mail=$tomail?comp_id=$company_id?type=S";
                $message = "
                <html>
                <body><br>
                Click <a href='$activation_link' target='_blank'style='color:blue !important;text-decoration: none !important;'><u>here</u></a> to subscribe!
                    <br>
                    <br>
                    Thank you!
                </body>
                </html>";                
                
        $this->sendsubscriptionemail($original_mail, $subject, $message, $company_id, $unsuscribed_cc_list);
    }
    
    protected function getunsubscribefooter($company_id) {
        $sql = sprintf("SELECT company_name, CONCAT(street_address, IF(street_address = '' , city, CONCAT(', ',city)),IF(street_address = '' OR city = '', state, CONCAT(', ',state)),IF(street_address = '' OR city = '' OR state = '', postal_code, CONCAT(' ',postal_code))) address 
                    from company where company_id = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_name = $row['company_name'];
                $studio_address = trim($row['address']);
                if(!empty($studio_address)){
                    $studio_address = ($studio_address[strlen($studio_address) - 1] === "," ? substr($studio_address, 0, -1) : $studio_address);
                }
                $footer_detail = $studio_name . ($studio_address === "" ? '' : " | $studio_address");
                return $footer_detail;
            }
        }
    }
    
    protected function savepossettingsINDB($company_id, $leads_payment_code, $trial_payment_code, $membership_payment_code, $event_payment_code, $retail_payment_code, $misc_payment_code, $leads_email_status
                    , $misc_processingfee_type, $leadsstatus, $trialstatus, $membershipstatus, $retailstatus, $eventstatus, $miscstatus,$type,$misc_agreement) {
        if($type === "ALL"){
            $sql = sprintf("UPDATE studio_pos_settings SET misc_charge_status = '%s' , retail_status = '%s' ,event_status = '%s' ,membership_status = '%s' ,trial_status = '%s' ,
                leads_status = '%s' ,misc_charge_fee_type = '%s' ,misc_payment_method = '%s' ,retail_payment_method = '%s' ,event_payment_method = '%s' ,
                membership_payment_method = '%s' ,trial_payment_method = '%s' ,leads_payment_method = '%s',leads_email_status = '%s' where company_id='%s'", mysqli_real_escape_string($this->db, $miscstatus), mysqli_real_escape_string($this->db, $retailstatus)
                , mysqli_real_escape_string($this->db, $eventstatus), mysqli_real_escape_string($this->db, $membershipstatus), mysqli_real_escape_string($this->db, $trialstatus)
                , mysqli_real_escape_string($this->db, $leadsstatus), mysqli_real_escape_string($this->db, $misc_processingfee_type), mysqli_real_escape_string($this->db, $misc_payment_code)
                , mysqli_real_escape_string($this->db, $retail_payment_code), mysqli_real_escape_string($this->db, $event_payment_code), mysqli_real_escape_string($this->db, $membership_payment_code)
                , mysqli_real_escape_string($this->db, $trial_payment_code) , mysqli_real_escape_string($this->db, $leads_payment_code), mysqli_real_escape_string($this->db, $leads_email_status)
                , mysqli_real_escape_string($this->db, $company_id));
        }else if($type === "Agreement"){
            $sql = sprintf("UPDATE studio_pos_settings SET misc_sales_agreement = '%s' where company_id='%s'", mysqli_real_escape_string($this->db, $misc_agreement), mysqli_real_escape_string($this->db, $company_id));
        }
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getpossettingdetailsINDB($company_id);
        }
    }
    
    protected function getpossettingdetailsINDB($company_id) {
        $sql = sprintf("SELECT pos.*,c.company_code,c.retail_enabled controlpanel_retail_status from studio_pos_settings pos JOIN company c using (company_id) where company_id='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $company_code = $row['company_code'];
                //http://dev.mystudio.academy/pos/?=j123/368/spos/c/
                $staff_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/pos/?=$company_code/$company_id/spos-e92213e6721525d7spos712627e6db9b98b4/c/";
                $public_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/pos/?=$company_code/$company_id/ppos-8f2f44c8580cc72ppos700ff5d197b5a18f0/c/";
                $error_log = array('status' => "Success", "possettings" => $row,"staff_url"=>"$staff_url","public_url"=>"$public_url");
                $this->response($this->json($error_log),200);
            }
        }
    }
    
       //misc payment history
    public function getmiscpaymenthistory($company_id, $misc_order_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $payment_date = " IF(TIME(mp.payment_date)!='00:00:00', DATE(CONVERT_TZ(mp.payment_date,$tzadd_add,'$new_timezone')), DATE(mp.payment_date)) ";
        $created_date = " IF(TIME(mp.created_dt)!='00:00:00', DATE(CONVERT_TZ(mp.created_dt,$tzadd_add,'$new_timezone')), DATE(mp.created_dt)) ";
        date_default_timezone_set($curr_time_zone);
        $output=[];
        $selectsql = sprintf("SELECT 'payment' as type,mo.buyer_name,mp.credit_method,mp.check_number,mp.`misc_payment_id` payment_id, mp.`misc_order_id` order_id,mp.checkout_status, IF(mo.`processing_fee_type`=2,IF(mp.payment_status in ('S','FR','R'),(IF(mo.payment_method ='CC',(mp.payment_amount+mp.processing_fee),mp.payment_amount)),mp.payment_amount),mp.payment_amount) payment_amount,mo.payment_method,mo.`processing_fee_type`,mp.`payment_status`,
                mp.`payment_amount` payment_amount_without_pf, mp.`processing_fee` ,ifnull(DATE($payment_date),$created_date) as payment_date,mp.`payment_status`,mp.`refunded_amount`,mp.`refunded_date`,if(mp.`payment_from`='S',mp.`stripe_card_name`,mp.`cc_name`) as cc_name,mp.`payment_from`,DATE(mp.`last_updt_dt`) as last_updt_dt 
                FROM `misc_payment` mp LEFT JOIN `misc_order` mo USING(misc_order_id) 
                WHERE mo.company_id='%s' AND mo.misc_order_id= '%s' order by payment_id ",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $misc_order_id));
        $result = mysqli_query($this->db, $selectsql);
        log_info("history  ".$selectsql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $nums_rows = mysqli_num_rows($result);
            if ($nums_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[]= $row;
                }
                $error = array('status' => "Success", "payment_history" => $output);
                $this->response($this->json($error), 200); // If no records "No Content" status  
            } else {
                $error = array('status' => "Failed", "msg" => "Miscellaneous payment history doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status  
            }
        }
    }
    
    protected function applypaymentcreditformiscINDB($company_id, $misc_payment_id,$applycredit_payment_method,$misc_order_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $curr_date = date("Y-m-d");
        $dimesnsion_period = date("Y-m");
        
        $sql = sprintf("UPDATE `misc_payment` SET payment_status='M',credit_method='%s',checkout_id='',cc_id='',cc_name='' where misc_payment_id='%s'", mysqli_real_escape_string($this->db, $applycredit_payment_method),mysqli_real_escape_string($this->db, $misc_payment_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if($applycredit_payment_method == 'CA' || $applycredit_payment_method == 'CH') {
                $update_sales_in_reg = sprintf("UPDATE `misc_order` SET `paid_amount`=`paid_amount`+(select `payment_amount` from `misc_payment` where misc_payment_id='%s') WHERE `misc_order_id`='%s'", mysqli_real_escape_string($this->db, $misc_payment_id), mysqli_real_escape_string($this->db, $misc_order_id));
                $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);

                if (!$result_sales_in_reg) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
                 //Update misc dimesnsion Table
                $update_query6 = sprintf("UPDATE `misc_dimensions`  SET `sales_amount` = `sales_amount`+(SELECT payment_amount from misc_payment where misc_payment_id='%s') WHERE `company_id` ='%s' and period='%s'", mysqli_real_escape_string($this->db, $misc_payment_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $dimesnsion_period));
                $result_update_query6 = mysqli_query($this->db, $update_query6);
                if (!$result_update_query6) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "6- $update_query6");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
            $this->getmiscpaymenthistory($company_id,$misc_order_id);
        }
    }
    
    protected function refundmiscINDB($company_id, $misc_payment_id,$refund_payment_method,$misc_order_id) {
        
        // split call based on wepay or stripe
        $payment_from = '';
        $initial_query = sprintf("SELECT `registration_from` FROM `misc_order` WHERE `company_id`='%s' AND `misc_order_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $misc_order_id));
        $initial_result = mysqli_query($this->db, $initial_query);
        if (!$initial_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$initial_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($initial_result) > 0) {
                $row_payment_from = mysqli_fetch_assoc($initial_result);
                $payment_from = $row_payment_from['registration_from'];
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error), 200);
            }
        }
        if($payment_from == 'S'){ // If stripe then redirect to other function
            $this->stripeRefundMiscInDB($company_id, $misc_payment_id,$refund_payment_method,$misc_order_id);
        }else{ // If wepay continue
            
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone = $user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
            $curr_date = date("Y-m-d");
            $dimesnsion_period = date("Y-m");
            $parent_net_sales = 0;

            if($refund_payment_method === "MC"){
                $error = array('status' => "Failed", "msg" => "Manual credit cannot be refunded");
                $this->response($this->json($error), 200);
            }else if($refund_payment_method === "CH" || $refund_payment_method === "CA" || $refund_payment_method === "CC"){
                    $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
                $paid_amount = $parent_qty = $total_refund_amount = $update_trial_remaining_dues = $update_trial_amount = $trial_refund_sales = $update_refund_sales_flag = $update_zero_cost_payment_record = 0;
                $curr_date = date("Y-m-d");
                $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, wp.`account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency`, `upgrade_status`, c.`wp_currency_symbol`,
                    ms. `paid_amount`, ms.`student_id`, ms.`processing_fee_type`, `buyer_name`, `buyer_email`, `buyer_phone`
                    FROM `wp_account` wp
                    LEFT JOIN `company` c ON c.`company_id`=wp.`company_id` 
                    LEFT JOIN `misc_order` ms ON ms.`company_id`=wp.`company_id`
                    WHERE c.`company_id`='%s' AND ms.`misc_order_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $misc_order_id));
                $result1 = mysqli_query($this->db, $query1);
                 if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "1- $query1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_rows1 = mysqli_num_rows($result1);
                    if ($num_rows1 > 0) {
                        $row = mysqli_fetch_assoc($result1);
                        $access_token = $row['access_token'];
                        $account_id = $row['account_id'];
                        $user_state = $row['wp_user_state'];
                        $acc_state = $row['account_state'];
                        $paid_amount = $row['paid_amount'];
                        $processing_fee_type = $row['processing_fee_type'];
                        $buyer_name = $row['buyer_name'];
                        $buyer_email = $row['buyer_email'];
                        $buyer_phone = $row['buyer_phone'];
                        $gmt_date = gmdate(DATE_RFC822);
                        $sns_msg5 = array("buyer_email" => $buyer_email, "membership_title" => 'misc', "gmt_date" => $gmt_date);
                        if ($user_state == 'deleted') {
                            $log = array("status" => "Failed", "msg" => "You may delete your account yourselves, or the account may be deleted by WePay customer support.");
                            $this->response($this->json($log), 200);
                        }

                        if ($acc_state == 'deleted') {
                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                            $this->response($this->json($log), 200);
                        } elseif ($acc_state == 'disabled') {
                            $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                            $this->response($this->json($log), 200);
                        }

                        if (!empty($access_token) && !empty($account_id)) {
                            if (!function_exists('getallheaders')) {
                                if (!function_exists('apache_request_headers')) {
                                    $user_agent = $_SERVER['HTTP_USER_AGENT'];
                                } else {
                                    $headers = apache_request_headers();
                                    $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                                }
                            } else {
                                $headers = getallheaders();
                                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                            }
                            $temp_password = $this->wp_pswd;
                            $method = 'aes-256-cbc';
                            // Must be exact 32 chars (256 bit)
                            $password = substr(hash('sha256', $temp_password, true), 0, 32);
                            // IV must be exact 16 chars (128 bit)
                            $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                            $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                        }
                    } else {
                        $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                        $this->response($this->json($error), 200);
                    }
                }
                if (isset($misc_payment_id) && !empty($misc_payment_id)) {
                    $get_payment_query = sprintf("SELECT credit_method,`checkout_id`,`checkout_status`,`payment_amount`,`refunded_amount`, `processing_fee`,`last_updt_dt` 
                                FROM `misc_payment`  WHERE `misc_payment_id`='%s' ", $misc_payment_id);
                    $result_payment_query = mysqli_query($this->db, $get_payment_query);
                    if (!$result_payment_query) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $get_payment_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $row_res = mysqli_fetch_assoc($result_payment_query);
                        $payment_id = $row_res['checkout_id'];
                        $payment_status = $row_res['checkout_status'];
                        $db_refunded_amt = $row_res['refunded_amount'];
                        $db_processing_fee = $row_res['processing_fee'];
                        $payment_amount = $row_res['payment_amount'];
                        $old_last_updt_date = $row_res['last_updt_dt'];
                        $stop_wepay = false;
                        if($row_res['credit_method'] == "CH" || $row_res['credit_method'] == "CA"){
                            $stop_wepay = true;
                        }
                    }
                    if ($processing_fee_type == 1) {
                        $parent_net_sales += $payment_amount - $db_processing_fee;
                    } else {
                        $parent_net_sales += $payment_amount;
                    }

                    if (!$stop_wepay) {
                        if ($payment_status == 'new' || $payment_status == 'authorized' || $payment_status == 'reserved') {
                            $sns_msg5['call'] = "Checkout";
                            $sns_msg5['type'] = "Checkout Cancellation";
                            $call_method = "checkout/cancel";
                            //  $json = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","checkout_id"=>$payment_id,"cancel_reason"=>"Participant ");
                            $json = array("checkout_id" => $payment_id, "cancel_reason" => "Participant ");
                        } elseif ($payment_status == 'released' || $payment_status == 'captured') {
                            $sns_msg5['call'] = "Checkout";
                            $sns_msg5['type'] = "Checkout Refund";
                            $call_method = "checkout/refund";
                            $json = array("checkout_id" => $payment_id, "refund_reason" => "Participant ");
                        } else {
                            $error = array("status" => "Failed", "msg" => "Participant payment status is in $payment_status status.");
                            $this->response($this->json($error), 200);
                        }

                        $postData = json_encode($json);
                        $response = $this->wp->accessWepayApi($call_method, 'POST', $postData, $token, $user_agent, $sns_msg5);
                        $this->wp->log_info("wepay_checkout_refund: " . $response['status']);
                        if ($response['status'] !== "Success") {
                            $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
                            $this->response($this->json($log), 200);
                        }
                    }
                }


                $payment_statsus_insert = $refund_payment_method === 'CC' ? 'FR' : ($refund_payment_method === 'CH' ? 'MF' : 'MF');
                $payment_statsus_update = $refund_payment_method === 'CC' ? 'R' : ($refund_payment_method === 'CH' ? 'MR' : 'MR');
                $insert_query = sprintf("INSERT INTO `misc_payment`(`misc_order_id`,`student_id`,`payment_method_id`, `payment_intent_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `stripe_card_name`, `created_dt`, `last_updt_dt`,refunded_amount,refunded_date,credit_method,check_number) 
                                SELECT `misc_order_id`,`student_id`,`payment_method_id`, `payment_intent_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, '$payment_statsus_insert', `stripe_card_name`, `created_dt`,'$old_last_updt_date','0','0000-00-00',credit_method,check_number FROM `misc_payment` WHERE `misc_payment_id`='%s'", mysqli_real_escape_string($this->db, $misc_payment_id));
                $result_insert_query = mysqli_query($this->db, $insert_query);
                if (!$result_insert_query) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $insert_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }

                //Update misc Payment Table
                $update_query3 = sprintf("UPDATE `misc_payment` mp,misc_order mo SET `payment_status`='%s', `refunded_amount`=(IF(mo.processing_fee_type='2',(mp.payment_amount+mp.processing_fee),mp.payment_amount)), `refunded_date`='$curr_date', `paid_amount` = `paid_amount`-$parent_net_sales WHERE mp.misc_order_id=mo.misc_order_id AND mp.`misc_payment_id`='%s'", mysqli_real_escape_string($this->db, $payment_statsus_update), mysqli_real_escape_string($this->db, $misc_payment_id));
                $result_update_query3 = mysqli_query($this->db, $update_query3);
                if (!$result_update_query3) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $update_query3");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }

                //Update misc dimesnsion Table
                $update_query6 = sprintf("UPDATE `misc_dimensions`  SET `sales_amount` = `sales_amount`-(SELECT IF($processing_fee_type = 1 , payment_amount-processing_fee ,payment_amount) from misc_payment where misc_payment_id='%s') WHERE `company_id` ='%s' and period='%s'", mysqli_real_escape_string($this->db, $misc_payment_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $dimesnsion_period));
                $result_update_query6 = mysqli_query($this->db, $update_query6);
                if (!$result_update_query6) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "6- $update_query6");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }

                $this->getmiscpaymenthistory($company_id, $misc_order_id);
            }
        }
    }
    
    protected function reRunMiscPaymentINDB($company_id,$reg_id,$payment_id){
        $query = sprintf("SELECT `registration_from` from `misc_order` WHERE `company_id`='%s' AND `misc_order_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $rows = mysqli_fetch_assoc($result);
            $registration_from = $rows['registration_from'];
            if ($registration_from == 'S'){
                $this->reRunstripeMiscPaymentINDB($company_id, $reg_id, $payment_id);
            }    
        }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;        

        $buyer_name = $buyer_email = $buyer_phone = '';
        
        $query = sprintf("SELECT `access_token`, wp.`account_id`, wp.`account_state`, wp.`currency`, mo.`company_id`, mo.`student_id`, mo.`misc_order_id`, `buyer_name`, `buyer_email`, `buyer_phone`,
                mp.`processing_fee`, mo.`payment_amount` total_due, mo.`paid_amount`, mo.`credit_card_id`, mo.`credit_card_name`, mo.`credit_card_status`,  mp.`credit_method`,
                mp.`payment_amount`, mp.`payment_date`, mp.`payment_status`, mo.`processing_fee_type`, mp.`checkout_id`, mp.`checkout_status`,c.`wp_currency_symbol`,mo.`student_id` 
                FROM `misc_order` mo
                LEFT JOIN `wp_account` wp ON mo.`company_id` = wp.`company_id` 
                LEFT JOIN `misc_payment` mp ON mo.`misc_order_id`=mp.`misc_order_id` AND (mp.`payment_status`='F' || (mp.`payment_status`='N' && mp.`payment_date`< ($currdate_add)))
                LEFT JOIN `company` c ON mo.`company_id` = c.`company_id` 
                WHERE   mo.`misc_order_id` = '%s'  AND `misc_payment_id` = '%s'  AND mp.`payment_amount`>0",
                 mysqli_real_escape_string($this->db, $reg_id),mysqli_real_escape_string($this->db, $payment_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $company_id = $row['company_id'];
                    $access_token = $row['access_token'];
                    $account_id = $row['account_id'];
                    $acc_state = $row['account_state'];
                    $currency = $row['currency'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $cc_id = $row['credit_card_id'];
                    $cc_name = $row['credit_card_name'];
                    if($row['credit_method'] == 'CA' || $row['credit_method'] == 'CH'){
                        $error = array('status' => "Failed", "msg" => "Miscellaneous payment via cash or check can not be rerun.");
                        $this->response($this->json($error), 200);
                    }
                
                    if($acc_state=='deleted'){
                        $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                        $this->response($this->json($log),200);
                    }elseif($acc_state=='disabled'){
                        $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                        $this->response($this->json($log),200);
                    }
                    
                    if(!empty($access_token)){
                        $failure = $success = 0;
                        $payment_amount = $row['payment_amount'];
                        $processing_fee = $row['processing_fee'];
                        $processing_fee_type = $row['processing_fee_type'];
                        $gmt_date=gmdate(DATE_RFC822);
                        $sns_msg3=array("buyer_email"=>$buyer_email,"membership_title"=>'Misc',"gmt_date"=>$gmt_date);
                        $user_agent = "mystudio.academy";
                        $temp_password = $this->wp_pswd;
                        $method = 'aes-256-cbc';
                        // Must be exact 32 chars (256 bit)
                        $password = substr(hash('sha256', $temp_password, true), 0, 32);
                        // IV must be exact 16 chars (128 bit)
                        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                        
//                       
                        $rand = mt_rand(1,99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout?for=misc";
                        $unique_id = $company_id."_".$reg_id."_".$ip."_".$rand."_".time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id."_".$reg_id."_".$date;
                        if($processing_fee_type==2){
                            $w_paid_amount = $payment_amount + $processing_fee;
                        }elseif($processing_fee_type==1){
                            $w_paid_amount = $payment_amount;
                        }
                        $time = time();
                        $json2 = array("account_id"=>"$account_id","short_description"=>"MISC","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                            "fee"=>array("app_fee"=>$processing_fee,"fee_payer"=>"payee_from_app"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                            "email_message"=>array("to_payee"=>"Payment has been added for miscellaneous","to_payer"=>"Payment has been made for miscellaneous"),
                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                            "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"12345")))),
                            "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                "properties"=>array("itemized_receipt"=>array(array("description"=>"miscellaneous", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                        $postData2 = json_encode($json2);                        
                        $sns_msg3['call'] = "Checkout";
                        $sns_msg3['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg3);
                        log_info("wepay_checkout_create: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $success=1;
//                            $this->rbitCallForReRunCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $event_name, $w_paid_amount, $currency);
                        }else{
                            if($response2['msg']['error_code']==1008){
                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg3);
                                log_info("wepay_checkout_create: ".$response2['status']);
                                if($response2['status']=="Success"){
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $success=1;
//                                    $this->rbitCallForReRunCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $event_name, $w_paid_amount, $currency);
                                }else{
                                    if($response2['msg']['error_code']==1008){
                                        $error = array('status' => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error), 200);
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }
                            }else{
                                $error = array('status' => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error), 200);
                            }
                        }
                        
                        $payment_query = sprintf("UPDATE `misc_payment` SET `checkout_id`='%s', `checkout_status`='%s', `payment_status`='S', `cc_id`='$cc_id', `cc_name`='$cc_name' WHERE `misc_payment_id`='%s'",
                                mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state),
                                mysqli_real_escape_string($this->db, $payment_id));
                        $payment_result = mysqli_query($this->db, $payment_query);
                        if (!$payment_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            $this->getmiscpaymenthistory($company_id, $reg_id);
                        }
                    } else {
                        $error = array('status' => "Failed", "msg" => "Access token error. Cannot be accept payments anymore.");
                        $this->response($this->json($error), 200);
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Miscellaneous payment details mismatched.");
                $this->response($this->json($error), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    //    get misc notes
    protected function getmiscnotesINDB($company_id,$order_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $activity_dt_tm = " IF(TIME(`activity_date_time`)!='00:00:00', DATE(CONVERT_TZ(`activity_date_time`,$tzadd_add,'$new_timezone')), DATE(`activity_date_time`))";
        
        
        $output = []; 
        $sql_reg = sprintf("SELECT `misc_note_id`, `activity_text`, $activity_dt_tm activity_date FROM `misc_notes`
                WHERE company_id='%s' AND misc_order_id ='%s' AND `deleted_flag`!='Y' ORDER BY `activity_date_time`, `misc_note_id`", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $order_id));
        $result_reg_details = mysqli_query($this->db, $sql_reg);
        if (!$result_reg_details) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_reg");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            while($rows = mysqli_fetch_assoc($result_reg_details)){
                $output[]=$rows;
            }
            $error = array('status' => "Success", "misc_notes" => $output);
            $this->response($this->json($error), 200);
        }
        date_default_timezone_set($curr_time_zone);
    }
    
//    misc notes
    protected function updatemiscnotesINDB($company_id, $type, $misc_order_id,$note_text,$note_id) {
        
        if($type == "add") {
            $sql = sprintf("INSERT INTO `misc_notes` (`company_id`, `student_id`, `misc_order_id`, `activity_text`) VALUES ('%s', (select student_id from misc_order where misc_order_id = '%s'), '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $misc_order_id), mysqli_real_escape_string($this->db, $misc_order_id), mysqli_real_escape_string($this->db, $note_text));
        }else if($type == "update"){
            $sql = sprintf("UPDATE `misc_notes` SET company_id='%s',student_id=(select student_id from misc_order where misc_order_id = '%s'),misc_order_id='%s',activity_text='%s' where misc_note_id='%s'", mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $misc_order_id),mysqli_real_escape_string($this->db, $misc_order_id),mysqli_real_escape_string($this->db, $note_text),mysqli_real_escape_string($this->db, $note_id));
        }else if($type == "delete"){
            $sql = sprintf("UPDATE `misc_notes` SET deleted_flag='Y' where misc_note_id='%s' AND company_id='%s'",mysqli_real_escape_string($this->db, $note_id),mysqli_real_escape_string($this->db, $company_id));
        }
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getmiscnotesINDB($company_id,$misc_order_id);
        }
    }
    
    protected function encryptPassword($pass){
        if (!function_exists('getallheaders')){
            if(!function_exists ('apache_request_headers')){
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            }else{
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
            }
        }else{
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
        }
        $ip = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR');   
        
        $time = time();
        $temp_password = $this->wp_pswd;
        $method = 'aes-256-cbc';
        // Must be exact 32 chars (256 bit)
        $password = substr(hash('sha256', $temp_password, true), 0, 32);
        // IV must be exact 16 chars (128 bit)
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        $encrypted_password = base64_encode(openssl_encrypt($pass, $method, $password, OPENSSL_RAW_DATA, $iv));
        return $encrypted_password;
    }
    
    protected function decryptPassword($encrypted_pass){
        if (!function_exists('getallheaders')){
            if(!function_exists ('apache_request_headers')){
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            }else{
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
            }
        }else{
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
        }
        $ip = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR');   
        
        $time = time();
        $temp_password = $this->wp_pswd;
        $method = 'aes-256-cbc';
        // Must be exact 32 chars (256 bit)
        $password = substr(hash('sha256', $temp_password, true), 0, 32);
        // IV must be exact 16 chars (128 bit)
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        $decrypted_pass = openssl_decrypt(base64_decode($encrypted_pass), $method, $password, OPENSSL_RAW_DATA, $iv);
        return $decrypted_pass;
    }
    
    protected function saveappinfo($company_id, $ios_developerid, $ios_teamid) {
        $app_id = '';
        $sql1 = sprintf("SELECT `app_id`, `app_name` ,`ios_developerid`, `ios_teamid` From  `white_label_apps` WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                $row1 = mysqli_fetch_assoc($result1);
                $app_id = $row1['app_id'];
                $sql2 = sprintf("UPDATE `white_label_apps` SET ios_developerid= '%s', ios_teamid = '%s', mystudio_legacy_app='T', studio_code=(SELECT company_code from company where company_id='%s') where `app_id` = '%s' ", 
                        mysqli_real_escape_string($this->db, $ios_developerid), mysqli_real_escape_string($this->db, $ios_teamid), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $app_id));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if(mysqli_affected_rows($this->db) > 0) {
                        $this->sendappinfotomystudio($app_id, $company_id);
                    }else{
                        $error = array('status' => "Failed", "msg" => "No changes made in App info");
                        $this->response($this->json($error), 200);
                    }
                }
            } else {
                $sql = sprintf("INSERT INTO `white_label_apps` (`company_id`, `ios_developerid`, `ios_teamid`, `studio_code`) VALUES ('%s', '%s', '%s', (SELECT company_code from company where company_id='%s'))"
                        , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $ios_developerid), mysqli_real_escape_string($this->db, $ios_teamid), mysqli_real_escape_string($this->db, $company_id));
                $result = mysqli_query($this->db, $sql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $app_id = mysqli_insert_id($this->db);
                    if(mysqli_affected_rows($this->db) > 0) {
                        $this->sendappinfotomystudio($app_id, $company_id);
                    }else{
                        $error = array('status' => "Failed", "msg" => "Your studio is not linked to any white label app. Please contact MyStudio.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
    }
    
    protected function sendappinfotomystudio($app_id, $company_id) {
        $app_name = $ios_developerid = $ios_teamid = $user_emailid = $subject = $message = "";
        $query = sprintf("SELECT `app_name`, `ios_developerid`, `ios_teamid` FROM `white_label_apps` WHERE `app_id` = '%s' AND `company_id` = '%s' ", mysqli_real_escape_string($this->db, $app_id),  mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $app_name = $row['app_name'];
                $ios_developerid = $row['ios_developerid'];
                $ios_teamid = $row['ios_teamid'];
//                $user_emailid = 'james@technogemsinc.com';
                $user_emailid = 'support@mystudio.academy';
                $subject = "App transfer details";
                $message .= "<h1 style='font-family: avenir;'>Apple Developer account details<h1/>";
                $message .= '<label style="font-size: 16px;font-family: avenir;"><b>App Name</b>         : ' . $app_name . '</label><br>';
                $message .= '<label style="font-size: 16px;font-family: avenir;"><b>IOS Developer Id</b> : ' . $ios_developerid . '</label><br>';
                $message .= '<label style="font-size: 16px;font-family: avenir;"><b>IOS Team ID</b>      : ' . $ios_teamid . '</label><br>';
                $sendEmail_status = $this->sendEmail($user_emailid, $subject, $message, '', '', '', '', '', $company_id, '', '');
                if ($sendEmail_status['status'] == "true") {
                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                    log_info($this->json($error_log));
                    $error = array('status' => "Success", "msg" => "Developer account details updated successfully.");
                    $this->response($this->json($error), 200);
                } else {
                    $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Developer account details update failed.");
                    $this->response($this->json($error), 200);
                }
            }
        }
    }
    
    protected function updatemanagerdetails($company_id, $ios_username, $ios_password, $ios_account_type) {
        $roletext = '';
        if($ios_account_type === 'I'){
            $roletext = 'Admin user details updated successfully.';
        }else if($ios_account_type === 'E'){
            $roletext = 'App manager details updated successfully.';
        }
        $encrypt_pass = $this->encryptPassword($ios_password);
        $sql1 = sprintf("SELECT `ios_username`, `ios_password` ,`ios_account_type` From  `white_label_apps` WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                $sql = sprintf("UPDATE `white_label_apps` SET ios_username = '%s', ios_password = '%s', ios_account_type = '%s' where `company_id` = '%s' ", mysqli_real_escape_string($this->db, $ios_username), mysqli_real_escape_string($this->db, $encrypt_pass), mysqli_real_escape_string($this->db, $ios_account_type), mysqli_real_escape_string($this->db, $company_id));
                $result = mysqli_query($this->db, $sql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if (mysqli_affected_rows($this->db) > 0) {
                        $error = array('status' => "Success", "msg" => $roletext);
                        $this->response($this->json($error), 200);
                    } else {
                        $error = array('status' => "Failed", "msg" => $roletext);
                        $this->response($this->json($error), 200);
                    }
                }
            } else {
                $sql = sprintf("INSERT INTO `white_label_apps` (`ios_username`,`ios_password`, `ios_account_type`) VALUES ('%s', '%s', '%s')", mysqli_real_escape_string($this->db, $ios_username), mysqli_real_escape_string($this->db, $encrypt_pass), mysqli_real_escape_string($this->db, $ios_account_type));
                $result = mysqli_query($this->db, $sql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if (mysqli_affected_rows($this->db) > 0) {
                        $error = array('status' => "Success", "msg" => $roletext);
                        $this->response($this->json($error), 200);
                    } else {
                        $error = array('status' => "Failed", "msg" => $roletext);
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }  
    }
    public function getUserAgent(){
        if (!function_exists('getallheaders')) {
            if (!function_exists('apache_request_headers')) {
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            } else {
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');

            }
        } else {
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');

        }
        return $user_agent;
    }
    
    public function createstripeconnectedaccount($company_id, $studio_code, $email, $call_back) {
        
        $query = sprintf("SELECT `account_id` FROM `stripe_account` WHERE `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if($call_back==1){
                return $error;
            }else{
                $this->response($this->json($error), 200);
            }
        } else {
            if (mysqli_num_rows($result) > 0) { // Already created account for this particular email
                $error = array('status' => "Failed", "msg" => "Already stripe account created for this particular studio email.");
                $this->response($this->json($error), 200);          
            } else { // Not created account - Create a new account
                $query = sprintf("SELECT `country` FROM `company` WHERE `company_id` = '%s' AND `company_code` = '%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $studio_code));
                $result = mysqli_query($this->db, $query);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    if($call_back==1){
                        return $error;
                    }else{
                        $this->response($this->json($error), 200);
                    }
                } else {
                    if (mysqli_num_rows($result) > 0) {
                        $row = mysqli_fetch_assoc($result);
                        $country = $row['country'];

                        // STRIPE CONNECTED ACCOUNT CREATION PROCESS

                        $this->getStripeKeys();
                        \Stripe\Stripe::setApiKey($this->stripe_sk);
                        \Stripe\Stripe::setApiVersion("2019-05-16");
//                        $cap_str = ($country === 'US') ? '"requested_capabilities" => ["card_payments"]' : '';    //ENABLE ONLY FOR US ACCOUNTS
                        $user_agent = $this->getUserAgent();
                        try {
                            $account_details = \Stripe\Account::create([
                                "type" => "custom",
                                "country" => $country,
                                "email" => $email,
                                "settings" => ["payouts"=>["debit_negative_balances"=>true]],
//                                "settings" => ["payouts"=>[
//                                   "debit_negative_balances"=>true,
//                                   "schedule" => ["interval"=>"weekly","weekly_anchor"=>"monday"]
//                                ]] ,
                                "tos_acceptance" => [
                                 'date' => time(),
                                 'ip' => $_SERVER['REMOTE_ADDR'], // Assumes you're not using a proxy
                                 'user_agent' => $user_agent
                                ]
                            ]);
                        }catch (Stripe\Error\InvalidRequest $e) {
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            stripe_log_info("New Stripe Connected Account creation Error- token: ".json_encode($body));
                            $error = array('status' => "Failed", 'msg' => $err['message']);
                            if($call_back==1){
                                return $error;
                            }else{
                                $this->response($this->json($error), 200);
                            }
//                            $this->response($this->json($error), 200);
                        }catch (Stripe\Error\Permission $e) {
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            stripe_log_info("New Stripe Connected Account creation Error- token: ".json_encode($body));
                            $error = array('status' => "Failed", 'msg' => $err['message']);
//                            $this->response($this->json($error), 200);
                            if($call_back==1){
                                return $error;
                            }else{
                                $this->response($this->json($error), 200);
                            }
                        }
                        $body = json_encode($account_details);
                        $new_account_details = json_decode($body, true);
                        $account_id = $new_account_details['id'];
                        $charges_enabled = ($new_account_details['charges_enabled']) ? 'Y' : 'N';
                        $payouts_enabled = ($new_account_details['payouts_enabled']) ? 'Y' : 'N';
                        $details_submitted = ($new_account_details['details_submitted']) ? 'Y' : 'N';
                        
//                        if(count($new_account_details['requirements']['currently_due']) > 0 || count($new_account_details['requirements']['eventually_due']) > 0){
//                            $requirement_status = 'Y';
//                        }else{
//                            $requirement_status = 'N';
//                        }
                        $requirement_status = ($new_account_details['details_submitted']) ? 'N' : 'Y';

                        $query = sprintf("INSERT INTO `stripe_account`(`company_id`, `account_id`, `user_email`, `charges_enabled`, `payouts_enabled`, `details_submitted`, `requirement_status`, `external_accounts_url`, `account_type`, `currency`, `account_state`)
                                 VALUE ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $account_id), 
                                mysqli_real_escape_string($this->db, $new_account_details['email']), mysqli_real_escape_string($this->db, $charges_enabled), mysqli_real_escape_string($this->db, $payouts_enabled), 
                                mysqli_real_escape_string($this->db, $details_submitted), mysqli_real_escape_string($this->db, $requirement_status), mysqli_real_escape_string($this->db, $new_account_details['external_accounts']['url']), mysqli_real_escape_string($this->db, $new_account_details['type']),
                                mysqli_real_escape_string($this->db, $new_account_details['default_currency']), mysqli_real_escape_string($this->db, 'A'));
                        $result = mysqli_query($this->db, $query);
                        
                        if (!$result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 200);
                            if($call_back==1){
                                return $error;
                            }else{
                                $this->response($this->json($error), 200);
                            }
                        } else {
                            $stripe_account_id = mysqli_insert_id($this->db);
                            $query = sprintf("UPDATE `company` SET `stripe_status` = 'Y' WHERE `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
                            $result = mysqli_query($this->db, $query);
                            if (!$result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                $this->response($this->json($error), 200);
                                if($call_back==1){
                                    return $error;
                                }else{
                                    $this->response($this->json($error), 200);
                                }
                            } else {
                                $error = array('status' => "Success", 'msg' => "Stripe connected account created successfully.", 'stripe_account_id' => $stripe_account_id);
//                                $this->response($this->json($error), 200);
                                if($call_back==1){
                                    return $error;
                                }else{
                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
                    } else {
                        $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
//                        $this->response($this->json($error), 200);
                        if($call_back==1){
                            return $error;
                        }else{
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }
        }
    }

//    Muthulakshmi
    public function createstripeExternalBankAccount($company_id,$user_id,$routing_no,$account_no,$acc_holder_name,$acc_holder_type) {
        
        $country = $currency = $account_id = $delete_external_account_id = '';
        $for_delete_handle = 0;
        $external_deleted__flag = 'N';
        $query = sprintf("SELECT c.`stripe_currency_code`, c.`country`, sa.`account_id` FROM `company` c LEFT JOIN `stripe_account` sa ON c.company_id = sa.company_id  WHERE c.`company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                $country = $row['country'];
                $currency = $row['stripe_currency_code'];
                $account_id = $row['account_id'];
                
                // Select external account if for deleting already existing id
                $sql = sprintf("SELECT `external_account_id` FROM `stripe_external_account` WHERE `company_id` = '%s' AND `account_id` = '%s' AND `deleted_flag` != 'Y' ORDER BY `created_date` desc", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $account_id));
                $external_result = mysqli_query($this->db, $sql);
                if (!$external_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if (mysqli_num_rows($external_result) > 0) {
                        $row1 = mysqli_fetch_assoc($external_result);
                        $delete_external_account_id = $row1['external_account_id'];
                    }
                }
                // End select external account if for deleting already existing id
                
                if(empty($account_id)){
                    $error = array('status' => "Failed", 'msg' => 'Create stripe account first');  // Dont change Msg text - used in front end
                    $this->response($this->json($error), 200);
                }
                
                // STRIPE CONNECTED ACCOUNT CREATION PROCESS
                $this->getStripeKeys();
                \Stripe\Stripe::setApiKey($this->stripe_sk);
                \Stripe\Stripe::setApiVersion("2019-05-16");

                // Generate Bank Account token
                try {
                    $bank_account_details = \Stripe\Token::create([
                        'bank_account' => [
                        'country' => 'AU',
                        'currency' => 'aud',
                        'account_holder_name' => $acc_holder_name,
                        'account_holder_type' => $acc_holder_type,
                        'routing_number' => $routing_no,
                        'account_number' => $account_no
                        ]
                    ]);
                } catch (Stripe\Error\InvalidRequest $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    stripe_log_info("Token for Bank Account - Error: ".json_encode($body));
                    $error = array('status' => "Failed", 'msg' => $err['message']);
                    $this->response($this->json($error), 200);
                }catch (Stripe\Error\Permission $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    stripe_log_info("Token for Bank Account - Error: ".json_encode($body));
                    $error = array('status' => "Failed", 'msg' => $err['message']);
                    $this->response($this->json($error), 200);
                }

                $body = json_encode($bank_account_details);
                $bank_token_details = json_decode($body, true);
                $bank_account_token = $bank_token_details['id'];
                
                // create bank account
                try {
                    $bank_account_create = \Stripe\Account::createExternalAccount($account_id, [
                         "external_account" => $bank_account_token,
                         "default_for_currency" => "true"
                    ]);
                } catch (Stripe\Error\InvalidRequest $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    stripe_log_info("External Account - Bank for connected Account - Error: ".json_encode($body));
                    $error = array('status' => "Failed", 'msg' => $err['message']);
                    $this->response($this->json($error), 200);
                }catch (Stripe\Error\Permission $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    stripe_log_info("External Account - Bank for connected Account - Error: ".json_encode($body));
                    $error = array('status' => "Failed", 'msg' => $err['message']);
                    $this->response($this->json($error), 200);
                }
                
                $body1 = json_encode($bank_account_create);
                $bank_created_details = json_decode($body1, true);
                
                if($bank_created_details['default_for_currency'] == 1){
                    $default_for_currency = 'true';
                }else{
                    $default_for_currency = 'false';
                }
                
                $query1 = sprintf("INSERT INTO `stripe_external_account`(`company_id`, `user_id`, `external_account_id`, `account_type`, `account_id`, `country`, `currency`, `last4_digit`, `account_holder_name`, `account_holder_type`, `bank_name`, `routing_number`, `default_for_currency`)
                VALUE ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                      mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $user_id),mysqli_real_escape_string($this->db, $bank_created_details['id']), mysqli_real_escape_string($this->db, $bank_created_details['object']), mysqli_real_escape_string($this->db, $bank_created_details['account']), mysqli_real_escape_string($this->db, $bank_created_details['country']), mysqli_real_escape_string($this->db, $bank_created_details['currency']), mysqli_real_escape_string($this->db, $bank_created_details['last4'])
                     ,mysqli_real_escape_string($this->db, $bank_created_details['account_holder_name']),mysqli_real_escape_string($this->db, $bank_created_details['account_holder_type']),mysqli_real_escape_string($this->db, $bank_created_details['bank_name']),mysqli_real_escape_string($this->db, $bank_created_details['routing_number']),mysqli_real_escape_string($this->db, $default_for_currency));
                $result1 = mysqli_query($this->db, $query1);
                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.",'qry' => $query1);
                    $this->response($this->json($error), 200);
                } else {
                    // Already added stripe external account should be deleted now
                    if (!empty($delete_external_account_id)) {
                        try {
                            $external_account_updation = \Stripe\Account::deleteExternalAccount($account_id, $delete_external_account_id);
                        } catch (Stripe\Error\Permission $e) {
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            stripe_log_info("External account delete - Error:" . json_encode($body));
                            $for_delete_handle = 1;
//                            $error = array('status' => "Failed", 'msg' => $err['message']);
//                            $this->response($this->json($error), 200);
                        } catch (Stripe\Error\InvalidRequest $e) {
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            stripe_log_info("External account delete - Error:" . json_encode($body));
                            $for_delete_handle = 1;
//                            $error = array('status' => "Failed", 'msg' => $err['message']);
//                            $this->response($this->json($error), 200);
                        }
                        if($for_delete_handle == 0){
                            $body = json_encode($external_account_updation);
                            $delete_external_account_details = json_decode($body, true);
                            if ($delete_external_account_details['deleted'] == 'true') {
                                $external_deleted__flag = 'Y';
                            } else {
                                $external_deleted__flag = 'N';
                            }
                        }
                        $query2 = sprintf("UPDATE `stripe_external_account` SET `deleted_flag`='%s' WHERE `company_id` = '%s' AND `account_id` = '%s' AND `external_account_id` = '%s'", mysqli_real_escape_string($this->db, $external_deleted__flag), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $account_id), mysqli_real_escape_string($this->db, $delete_external_account_id));
                        $result2 = mysqli_query($this->db, $query2);
                        if (!$result2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    $details = $this->getStripePayoutDetailsfromDB($company_id,1);
                    $out = array('status' => "Success", 'msg' => "Stripe Payout details added successfully.",'external_accounts' => $details,'created_details' => $bank_created_details);
                    $this->response($this->json($out), 200);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Company details doesnot exists");
                $this->response($this->json($error), 200);
            }
        }
    }
    public function updatestripeExternalCardAccount($company_id, $user_id, $card_token_value) {

        $account_id = $delete_external_account_id = '';
        $for_delete_handle = 0;
        $external_deleted__flag = 'N';
        
        $query = sprintf("SELECT `account_id` FROM `stripe_account` WHERE `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                $account_id = $row['account_id'];
                
            // Select external account if for deleting already existing id
                $sql = sprintf("SELECT `external_account_id` FROM `stripe_external_account` WHERE `company_id` = '%s' AND `account_id` = '%s' AND `deleted_flag` != 'Y' ORDER BY `created_date` desc", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $account_id));
                $external_result = mysqli_query($this->db, $sql);
                if (!$external_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if (mysqli_num_rows($external_result) > 0) {
                        $row1 = mysqli_fetch_assoc($external_result);
                        $delete_external_account_id = $row1['external_account_id'];
                    }
                }
                // End select external account if for deleting already existing id
                
                $this->getStripeKeys();
                \Stripe\Stripe::setApiKey($this->stripe_sk);
                \Stripe\Stripe::setApiVersion("2019-05-16");
                try {
                    $card_account_create = \Stripe\Account::createExternalAccount($account_id, [
                        "external_account" => $card_token_value,
                        "default_for_currency" => "true"
                    ]);
                } catch (Stripe\Error\Permission $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    stripe_log_info("Token for Debit card - Error: " . json_encode($body));
                    $error = array('status' => "Failed", 'msg' => $err['message']);
                    $this->response($this->json($error), 200);
                } catch (Stripe\Error\InvalidRequest $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    stripe_log_info("Token for Debit card - Error: " . json_encode($body));
                    $error = array('status' => "Failed", 'msg' => $err['message']);
                    $this->response($this->json($error), 200);
                } catch (Stripe\Error\Api $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    stripe_log_info("Token for Debit card - Error: " . json_encode($body));
                    $error = array('status' => "Failed", 'msg' => $err['message']);
                    $this->response($this->json($error), 200);
                }
                $body = json_encode($card_account_create);
                $card_created_details = json_decode($body, true);
                if ($card_created_details['default_for_currency'] == 1) {
                    $default_for_currency = 'true';
                } else {
                    $default_for_currency = 'false';
                }
                
                $query1 = sprintf("INSERT INTO `stripe_external_account`(`company_id`, `user_id`, `external_account_id`, `account_type`, `account_id`, `country`, `currency`, `last4_digit`, `card_brand`,`default_for_currency`)
                VALUE ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $user_id), mysqli_real_escape_string($this->db, $card_created_details['id']), mysqli_real_escape_string($this->db, $card_created_details['object']), mysqli_real_escape_string($this->db, $card_created_details['account']), mysqli_real_escape_string($this->db, $card_created_details['country']), mysqli_real_escape_string($this->db, $card_created_details['currency']), mysqli_real_escape_string($this->db, $card_created_details['last4']), mysqli_real_escape_string($this->db, $card_created_details['brand']), mysqli_real_escape_string($this->db, $default_for_currency));
                $result1 = mysqli_query($this->db, $query1);
                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    // Already added stripe external account should be deleted now
                    if (!empty($delete_external_account_id)) {
                        try {
                            $external_account_updation = \Stripe\Account::deleteExternalAccount($account_id, $delete_external_account_id);
                        } catch (Stripe\Error\Permission $e) {
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            stripe_log_info("External account delete - Error:" . json_encode($body));
                            $for_delete_handle = 1;
//                            $error = array('status' => "Failed", 'msg' => $err['message']);
//                            $this->response($this->json($error), 200);
                        } catch (Stripe\Error\InvalidRequest $e) {
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            stripe_log_info("External account delete - Error:" . json_encode($body));
                            $for_delete_handle = 1;
//                            $error = array('status' => "Failed", 'msg' => $err['message']);
//                            $this->response($this->json($error), 200);
                        }
                        if($for_delete_handle == 0){
                            $body = json_encode($external_account_updation);
                            $delete_external_account_details = json_decode($body, true);
                            if ($delete_external_account_details['deleted'] == 'true') {
                                $external_deleted__flag = 'Y';
                            } else {
                                $external_deleted__flag = 'N';
                            }
                        }
                        $query2 = sprintf("UPDATE `stripe_external_account` SET `deleted_flag`='%s' WHERE `company_id` = '%s' AND `account_id` = '%s' AND `external_account_id` = '%s'", mysqli_real_escape_string($this->db, $external_deleted__flag), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $account_id), mysqli_real_escape_string($this->db, $delete_external_account_id));
                        $result2 = mysqli_query($this->db, $query2);
                        if (!$result2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    // End Already added stripe external account should be deleted now
                    $details = $this->getStripePayoutDetailsfromDB($company_id, 1);
                    $error = array('status' => "Success", 'msg' => "Stripe Payout details added successfully.", 'external_accounts' => $details, 'created_details' => $card_created_details);
                    $this->response($this->json($error), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Create stripe account first"); // Dont change Msg text - used in front end
                $this->response($this->json($error), 200);
            }
        }
    }

    public function getStripePayoutDetailsfromDB($company_id,$callback) {
        $resultupdatelinksql = "";
        $external_accounts = [];
        $acc_query = sprintf("SELECT `account_id` FROM `stripe_account` WHERE `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $acc_result = mysqli_query($this->db, $acc_query);
        if (!$acc_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$acc_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if(mysqli_num_rows($acc_result) > 0){
                $row = mysqli_fetch_assoc($acc_result);
                $stripe_account_id = $row['account_id'];
                
                 // STRIPE CONNECTED ACCOUNT RETRIEVE DETAILS
                    $this->getStripeKeys();
                    \Stripe\Stripe::setApiKey($this->stripe_sk);
                    \Stripe\Stripe::setApiVersion("2019-05-16");
                
                try {                        
                    $connected_account_balance = \Stripe\Balance::retrieve(
                                ['stripe_account' => $stripe_account_id]);
                } catch (Stripe\Error\Permission $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    stripe_log_info("Payout Balance of connected Account - Error:".json_encode($body));
                    $error = array('status' => "Failed", 'msg' => $err['message']);
                    $this->response($this->json($error), 200);
                }catch (Stripe\Error\InvalidRequest $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    stripe_log_info("Payout Balance of connected Account - Error:".json_encode($body));
                    $error = array('status' => "Failed", 'msg' => $err['message']);
                    $this->response($this->json($error), 200);
                }
                
                $total_available_balance = $total_pending_balance = 0;
                for($i=0; $i<count($connected_account_balance->available); $i++){
                    $total_available_balance += $connected_account_balance->available[$i]->amount;
                }
                $total_available_balance = $total_available_balance/100;
                
                for($i=0; $i<count($connected_account_balance->pending); $i++){
                    $total_pending_balance += $connected_account_balance->pending[$i]->amount;
                }
                $total_pending_balance = number_format(($total_pending_balance/100), 2);
                
                $updatelinksql=sprintf("UPDATE `stripe_account` SET `stripe_balance`='%s' WHERE `account_id`='%s'" 
                        , mysqli_real_escape_string($this->db, $total_available_balance), mysqli_real_escape_string($this->db, $stripe_account_id));
                $resultupdatelinksql = mysqli_query($this->db, $updatelinksql);
                
                if (!$resultupdatelinksql) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatelinksql");
                    stripe_log_info($this->json($error_log));
                }
                
                  // Account creator first name and last name update
                try {
                    $account_creator = \Stripe\Account::allPersons($stripe_account_id);
                } catch (Stripe\Error\Permission $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    stripe_log_info("Account creator of connected Account - Error:".json_encode($body));
                    $error = array('status' => "Failed", 'msg' => $err['message']);
                    $this->response($this->json($error), 200);
                }catch (Stripe\Error\InvalidRequest $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    stripe_log_info("Account creator of connected Account - Error:".json_encode($body));
                    $error = array('status' => "Failed", 'msg' => $err['message']);
                    $this->response($this->json($error), 200);
                }
                if(count($account_creator->data) > 0){
                    $updatesql = sprintf("UPDATE `stripe_account` SET `first_name`='%s', `last_name`='%s' WHERE `account_id`='%s'"
                            , mysqli_real_escape_string($this->db, $account_creator->data[0]->first_name)
                            , mysqli_real_escape_string($this->db, $account_creator->data[0]->last_name)
                            , mysqli_real_escape_string($this->db, $account_creator->data[0]->account));
                    $updatesql_result = mysqli_query($this->db, $updatesql);
                    if (!$updatesql_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesql");
                        stripe_log_info($this->json($error_log));
                    }
                }
            }else {
                $error = array('status' => "Failed", "msg" => "Create stripe account first"); // Dont change Msg text - used in front end
                $this->response($this->json($error), 200);
            }
        }
        $query = sprintf("SELECT c.`stripe_currency_code`, c.`stripe_status`, sa.`account_id`, sa.`charges_enabled`, sa.`stripe_balance` as stripe_available_balance, sa.`payout_time_period`, sa.`relationship_status`, sa.`requirement_status`, sa.`first_name`, sa.`last_name`, sa.`company_name` as stripe_business_name FROM `company` c 
                LEFT JOIN `stripe_account` sa ON c.company_id = sa.company_id 
                WHERE c.`company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                $stripe_account_id = $row['account_id'];
//                $stripe_name = $row['first_name']."  ".$row['last_name'];
//                $row['stripe_name'] = $stripe_name;
                $output = $row;
                if(empty($stripe_account_id)){
                    $out = array('status' => "Failed", 'msg' => "Create stripe account first"); // Dont change Msg text - used in front end
                    $this->response($this->json($out), 200);
                }else{
                    $query1 = sprintf("SELECT `bank_name`, `currency`,  `routing_number`, `last4_digit`, `card_brand`, `external_account_id`, `account_id` FROM `stripe_external_account` WHERE `company_id` = '%s' AND `deleted_flag` != 'Y' ORDER by `stripe_external_account_id` desc limit 0,1", mysqli_real_escape_string($this->db, $company_id));
                    $result1 = mysqli_query($this->db, $query1);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        if(mysqli_num_rows($result1) > 0){
                            while ($rows = mysqli_fetch_assoc($result1)) {
                                $external_accounts[] = $rows;
                            }
                        }
                    }
                    
                     // STRIPE CONNECTED ACCOUNT RETRIEVE DETAILS
                    $this->getStripeKeys();
                    \Stripe\Stripe::setApiKey($this->stripe_sk);
                    \Stripe\Stripe::setApiVersion("2019-05-16");
                    try {
                        $retrieve_account = \Stripe\Account::retrieve($stripe_account_id);
                    } catch (Stripe\Error\Permission $e) {
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        stripe_log_info("Account details of connected Account - Error:".json_encode($body));
                        $error = array('status' => "Failed", 'msg' => $err['message']);
                        $this->response($this->json($error), 200);
                    }catch (Stripe\Error\InvalidRequest $e) {
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        stripe_log_info("Account details of connected Account - Error:".json_encode($body));
                        $error = array('status' => "Failed", 'msg' => $err['message']);
                        $this->response($this->json($error), 200);
                    }
                    $body = json_encode($retrieve_account);   
                    $retrieve_account_details = json_decode($body, true); 
                    
                    try {
                        $payout_list = \Stripe\Payout::all([
                            "limit" => 10],
                            ['stripe_account' => $stripe_account_id]);
//                        
                        $payou_details = [];
                        if(count($payout_list->data) > 0){
                            for($i=0; $i<count($payout_list->data); $i++){
                                $payou_details[$i]['amount'] = number_format((($payout_list->data[$i]->amount)/100), 2);
                                $payou_details[$i]['currency'] = $payout_list->data[$i]->currency;
                                $payou_details[$i]['status'] = $payout_list->data[$i]->status;
                                $payou_details[$i]['created'] = date("M d, Y", $payout_list->data[$i]->created);
                                $payou_details[$i]['destination'] = $payout_list->data[$i]->destination;
                                $payou_details[$i]['type'] = $payout_list->data[$i]->type; 
                                $payou_details[$i]['description'] = $payout_list->data[$i]->description;                            
                            }
                        }
                        if(count($payou_details) > 0){
                            for($i=0; $i<count($payou_details); $i++){
                                if($payou_details[$i]['type'] == "bank_account"){
                                    $select_query = sprintf("SELECT `bank_name`,`last4_digit` from `stripe_external_account` WHERE `company_id` = '%s' AND `external_account_id`= '%s'"
                                      , mysqli_real_escape_string($this->db, $company_id)
                                      , mysqli_real_escape_string($this->db, $payou_details[$i]['destination']));
                                }else{
                                    $select_query = sprintf("SELECT `card_brand`,`last4_digit` from `stripe_external_account` WHERE `company_id` = '%s' AND `external_account_id`= '%s'"
                                      , mysqli_real_escape_string($this->db, $company_id)
                                      , mysqli_real_escape_string($this->db, $payou_details[$i]['destination']));
                                }
                                $query_result = mysqli_query($this->db, $select_query);
                                if (!$query_result) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_query");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                } else {
                                    if(mysqli_num_rows($query_result) > 0){
                                        $row = mysqli_fetch_assoc($query_result);
                                        if($payou_details[$i]['type'] == "bank_account"){                                            
                                            $ext_account_name = $row['bank_name'];
                                        }else{
                                            $ext_account_name = $row['card_brand'];
                                        }
                                        $ext_account_number = $row['last4_digit'];
                                        $payou_details[$i]['account_name'] = $ext_account_name."   ****".$ext_account_number;
                                    }else{
//                                        $error = array('status' => "Failed", "msg" => "External account does not exist"); 
//                                        $this->response($this->json($error), 200);                                        
                                    }
                                }                                
                            }
                        }
                        
                    } catch (Stripe\Error\Permission $e) {
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        stripe_log_info("Payout list of connected Account - Error:".json_encode($body));
                        $error = array('status' => "Failed", 'msg' => $err['message']);
                        $this->response($this->json($error), 200);
                    }catch (Stripe\Error\InvalidRequest $e) {
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        stripe_log_info("Payout list of connected Account - Error:".json_encode($body));
                        $error = array('status' => "Failed", 'msg' => $err['message']);
                        $this->response($this->json($error), 200);
                    }
                    $body = json_encode($payout_list);   
                    $payout_list_details = json_decode($body, true);                     
                    
                    if ($callback == 0) {
                        $out = array('status' => "Success", 'msg' => $output , 'external_accounts' => $external_accounts,'connected_account_detail' => $retrieve_account_details,'payout_list' => $payou_details,'stripe_pending_balance' => $total_pending_balance);
                        $this->response($this->json($out), 200);
                    } else {
                        return $external_accounts;
                    }
                }
            }else {
                $error = array('status' => "Failed", "msg" => "Currency code doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
//            ****** ****** ***** *** ******* ****
        // wepay process
//        $user_id = $access_token =  $user_name = $account_id = $account_state = $action_reasons = $disabled_reasons = $country = $company_name = $contact_id = $list_item_id = '';
//        $account_address = $account_phone = $account_mail = [];
//        $website = $url = $social = $currency_code = $currency_symbol = '';
////        $first_name = $last_name = $email = $studio_code = $phone =  $type = '';
//        
//        $gmt_date = gmdate(DATE_RFC822);
//        $sns_msg5 = array("buyer_email" => $company_id, "membership_title" => $user_email, "gmt_date" => $gmt_date);
//        if (!function_exists('getallheaders')){
//            if(!function_exists ('apache_request_headers')){
//                $user_agent = $_SERVER['HTTP_USER_AGENT'];
//            }else{
//                $headers = apache_request_headers();
//                $user_agent = $headers['User-Agent'];
//            }
//        }else{
//            $headers = getallheaders();
//            $user_agent = $headers['User-Agent'];
//        }
//        $ip = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR');   
//        
//        $time = time();
//        $password = $this->wp_pswd;
//        $method = 'aes-256-cbc';
//        // Must be exact 32 chars (256 bit)
//        $password = substr(hash('sha256', $password, true), 0, 32);
//        // IV must be exact 16 chars (128 bit)
//        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
//        
//        $query_check_wepay = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
//        $result_check_wepay = mysqli_query($this->db, $query_check_wepay);
//        if(!$result_check_wepay){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_check_wepay");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//        }else{
//            if(mysqli_num_rows($result_check_wepay)>0){
//                $error = array('status' => "Failed", "msg" => "Wepay user account details already available to this studio. Please contact administrator to reset Wepay account.");
//                $this->response($this->json($error), 200);
//            }
//        }
//        if($type=='L'){
//            $query_check_email = sprintf("SELECT * FROM `wp_account` WHERE `user_email`='%s' ORDER BY `wp_account_id` DESC limit 0,1", mysqli_real_escape_string($this->db, $user_email));
//            $result_check_mail = mysqli_query($this->db, $query_check_email);
//            if(!$result_check_mail){
//                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_check_email");
//                log_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                $this->response($this->json($error), 200);
//            }else{
//                $numrows = mysqli_num_rows($result_check_mail);
//                if($numrows>0){
//                    $row = mysqli_fetch_assoc($result_check_mail);
////                    if($row['first_name']==$user_first_name && $row['last_name']==$user_last_name){
//                        if($row['company_id']==$company_id){
//                            $error = array('status' => "Failed", "msg" => "Same Wepay user details available to this studio.");
//                            $this->response($this->json($error), 200);
//                        }else{
//                            $user_id = $row['wp_user_id'];
//                            $token = $row['access_token'];
//                            $access_token = openssl_decrypt(base64_decode($token), $method, $password, OPENSSL_RAW_DATA, $iv);
//                        }
////                    }else{
////                        $error = array('status' => "Failed", "msg" => "Wepay user details mismatched.");
////                        $this->response($this->json($error), 200);
////                    }
//                }else{
//                    $error = array('status' => "Failed", "msg" => "No user account created via Mystudio app.");
//                    $this->response($this->json($error), 200);
//                }
//            }
//        }else{
//            $json = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","email"=>"$user_email","scope"=>"manage_accounts,collect_payments,view_user,send_money,preapprove_payments",
//                "first_name"=>"$user_first_name","last_name"=>"$user_last_name","original_ip"=>"$ip","original_device"=>"$user_agent","tos_acceptance_time"=>$time);
//            $postData = json_encode($json);            
//            $sns_msg5['call'] = "User";
//            $sns_msg5['type'] = "User registration";
//            $response = $this->wp->accessWepayApi("user/register",'POST',$postData,'',$user_agent,$sns_msg5);
//            $this->wp->log_info("wepay_user_create: ".$response['status']);
//            if($response['status'] == "Success"){
//                $res = $response['msg'];
//                $user_id = $res['user_id'];
//                $access_token = $res['access_token'];
//                if(isset($res['expires_in']) && !empty($res['expires_in'])){
//                    $json5 = array("email_subject"=>"MyStudio - Verify your WePay account");
//                    $postData5 = json_encode($json5);
//                    $sns_msg5['call'] = "User";
//                    $sns_msg5['type'] = "Sending User confirmation email";
//                    $response5 = $this->wp->accessWepayApi("user/send_confirmation",'POST',$postData5,$access_token,$user_agent,$sns_msg5);
//                    if($response5['status'] == "Success"){
//
//                    }else{
//                        $log = array("status" => "Failed", "msg" => $response5['msg']['error_description']);
//                        $this->response($this->json($log),200);
//                    }
//                }
//            }else{
//                $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                $this->response($this->json($log),200);
//            }
//        }     
//        
//        
////        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
//        
//        $query_studio_details = sprintf("SELECT * FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
//        $result_studio_details = mysqli_query($this->db, $query_studio_details);
//        if(!$result_studio_details){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_studio_details");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//        }else{
//            $time = time();
//            $num_rows = mysqli_num_rows($result_studio_details);
//            if($num_rows>0){
//                $row = mysqli_fetch_assoc($result_studio_details);
//                $company_name = $row['company_name'];
//                $contact_id = $row['contact_id'];
//                $list_item_id = $row['list_item_id'];
//                $currency_code = $row['wp_currency_code'];
//                $currency_symbol = $row['wp_currency_symbol'];
//                if(!empty(trim($row['country']))){
//                    if (strtolower($row['country']) == 'ca' || strtolower($row['country']) == 'canada'){
//                        $country="CA";
//                    }elseif (strtolower($row['country']) == 'uk' || strtolower($row['country']) == 'gb' || strtolower($row['country']) == 'united kingdom'){
//                        $country="GB";
//                    }else{
//                        $country="US";
//                    }
//                }else{
//                    $country = "US";
//                }
//                if(!empty(trim($row['postal_code']))){
//                    $temp=[];
//                    $account_address['receive_time'] = $time;
//                    $account_address['type'] = "address";
//                    $account_address['source'] = "user";
//                    if(!empty(trim($row['street_address']))){
//                        $temp['address1']=$row['street_address'];
//                    }else{
//                        $temp['address1']="N/A";
//                    }
//                    if(!empty(trim($row['city']))){
//                        $temp['city']=$row['city'];
//                    }else{
//                        $temp['city']="N/A";
//                    }
//                    if(!empty(trim($row['postal_code']))){
//                        $temp['zip']=$row['postal_code'];
//                    }else{
//                        $temp['zip']="N/A";
//                    }
//                    $temp['country'] = $country;
//                    if(!empty(trim($row['web_page']))){
//                        $website = $row['web_page'];
//                    }
//                    if(!empty(trim($row['facebook_url']))){
//                        $social = "facebook";
//                        $url = $row['facebook_url'];
//                    }elseif(!empty(trim($row['twitter_url']))){
//                        $social = "twitter";
//                        $url = $row['twitter_url'];
//                    }
//                    $account_address['properties']['address'] = $temp;
//                }
//                if(!empty(trim($row['phone_number']))){
//                    $account_phone['receive_time'] = $time;
//                    $account_phone['type'] = "phone";
//                    $account_phone['source'] = "user";
//                    $account_phone['properties']['phone'] = $row['phone_number'];
//                    $account_phone['properties']['type'] = "work";
//                }else{
//                    $account_phone['receive_time'] = $time;
//                    $account_phone['type'] = "phone";
//                    $account_phone['source'] = "user";
//                    $account_phone['properties']['phone'] = "N/A";
//                    $account_phone['properties']['type'] = "work";
//                }
//                if(!empty(trim($row['email_id']))){
//                    $account_mail['receive_time'] = $time;
//                    $account_mail['type'] = "email";
//                    $account_mail['source'] = "user";
//                    $account_mail['properties']['email'] = $row['email_id'];
//                }else{
//                    $account_mail['receive_time'] = $time;
//                    $account_mail['type'] = "email";
//                    $account_mail['source'] = "user";
//                    $account_mail['properties']['email'] = "N/A";
//                }
//            }else{
//                $error = array('status' => "Failed", "msg" => "Studio details doesn't exist.");
//                $this->response($this->json($error), 200);
//            }
//        }
//
////// My secret message 1234
////$decrypted = openssl_decrypt(base64_decode($encrypted), $method, $password, OPENSSL_RAW_DATA, $iv);
//
//        
//                    
////        $json = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","code"=>"$code","redirect_uri"=>"$redirect_uri");
////        $postData = json_encode($json);
////        $response = $this->wp->accessWepayApi("oauth2/token",'POST',$postData,'',$user_agent);
////        $this->wp->log_info("wepay_oauth2_token: ".$response['status']);
////        if($response['status'] == "Success"){
////            $user_id = $response['msg']['user_id'];
////            $access_token = $response['msg']['access_token'];
////            if(!empty($user_id)){
//                $sns_msg5['call'] = "User";
//                $sns_msg5['type'] = "User details";
//                $response2 = $this->wp->accessWepayApi("user",'GET','',$access_token,$user_agent,$sns_msg5);
//                $this->wp->log_info("wepay_getuser: ".$response2['status']);
//                if($response2['status'] == "Success"){
//                    $res = $response2['msg'];
//                    $first_name = $res['first_name'];
//                    $last_name = $res['last_name'];
//                    $user_name = $res['user_name'];
//                    $email = $res['email'];
//                    $user_state = $res['state'];
//                    $user_callback_url = $this->server_url."/Wepay2/updateUser";
//                    $json1 = array("callback_uri"=>"$user_callback_url");
//                    $postData1 = json_encode($json1);
//                    $sns_msg5['call'] = "User";
//                    $sns_msg5['type'] = "Updating User details";
//                    $resp = $this->wp->accessWepayApi("user/modify",'POST',$postData1,$access_token,$user_agent,$sns_msg5);
//                    $this->wp->log_info("wepay_user_modify: ".$resp['status']);
//                }else{
//                    $log = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                    $this->response($this->json($log),200);
//                }
////            }
//            
//            $acc_callback_url = $this->server_url."/Wepay2/updateAccount";
//            if(strtolower($country)=="canada" || strtolower($country)=="ca"){
//                $json3 = array("name"=>"$company_name","description"=>"$company_name","callback_uri"=>"$acc_callback_url","country"=>"CA","country_options"=>array("debit_opt_in"=> true),"currencies"=>array("$currency_code"));
//            }elseif(strtolower($row['country']) == 'uk' || strtolower($row['country']) == 'gb' || strtolower($row['country']) == 'united kingdom'){
//                $json3 = array("name"=>"$company_name","description"=>"$company_name","callback_uri"=>"$acc_callback_url","country"=>"GB","currencies"=>array("$currency_code"));
//            }else{
//                $json3 = array("name"=>"$company_name","description"=>"$company_name","callback_uri"=>"$acc_callback_url");
//            }
//            $postData3 = json_encode($json3);
//            $sns_msg5['call'] = "Account";
//            $sns_msg5['type'] = "Account Creation";
//            $response3 = $this->wp->accessWepayApi("account/create",'POST',$postData3,$access_token,$user_agent,$sns_msg5);
//            $this->wp->log_info("wepay_account_create: ".$response3['status']);
//            if($response3['status'] == "Success"){
//                $res = $response3['msg'];
//                $account_id = $res['account_id'];
//                $account_state = $res['state'];
//                $action_reasons = implode(",", $res['action_reasons']);
//                $disabled_reasons = implode(",", $res['disabled_reasons']);
//                $currency = implode(",", $res['currencies']);
//                $this->rbitCallForAccount($access_token, $user_agent, $account_id, $company_name, $time, $account_address, $account_mail, $account_phone, $website, $social, $url);
//            }else{
//                $log = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
//                $this->response($this->json($log),200);
//            }
//            
//            $encrypted_access_token = base64_encode(openssl_encrypt($access_token, $method, $password, OPENSSL_RAW_DATA, $iv));
//            $query = sprintf("INSERT INTO `wp_account`(`company_id`, `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `first_name`, `last_name`, `user_name`, `user_email`, `action_reasons`, `disabled_reasons`, `currency`)
//                    VALUE ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $user_id), mysqli_real_escape_string($this->db, $user_state), mysqli_real_escape_string($this->db, $encrypted_access_token),
//                    mysqli_real_escape_string($this->db, $account_id), mysqli_real_escape_string($this->db, $account_state), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name),
//                    mysqli_real_escape_string($this->db, $user_name), mysqli_real_escape_string($this->db, $email), mysqli_real_escape_string($this->db, $action_reasons), mysqli_real_escape_string($this->db, $disabled_reasons), mysqli_real_escape_string($this->db, $currency));
//            $result = mysqli_query($this->db, $query);
//            
//            if(!$result){
//                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
//                log_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                $this->response($this->json($error), 200);
//            }
//            
//            if(!empty($account_state) && $account_state!="disabled" && $account_state!="deleted"){
//                if($account_state=="pending"){
//                    $status = 'P';
//                }else{
//                    $status = 'Y';
//                }
//                $update_company = sprintf("UPDATE `company` SET `wepay_status`='%s' WHERE `company_id`='%s'", $status, mysqli_real_escape_string($this->db, $company_id));
//                $result_update_company = mysqli_query($this->db, $update_company);
//                if(!$result_update_company){
//                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
//                    log_info($this->json($error_log));
//                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                    $this->response($this->json($error), 200);
//                }
//                if(!empty($list_item_id)&&!empty($contact_id)){
//                    $wp_status = "1";
//                    $json = array("contactIds"=>array("$contact_id"),"fieldValues"=>array("24"=>array(array("raw"=>"$wp_status"))));
//                    $postData = json_encode($json);
//                    $sf_response = $this->sf->accessSalesforceApi("lists/$this->list_id/listitems/$list_item_id",'PUT',$postData);
//                    log_info("updateCompanyDetailsinSalesforceApi_ListItemUpdation: ".$sf_response['status']);
//                }
//            }
//            $log = array("status" => "Success", "msg" => "Wepay user account created successfully. Please check your email for the confirmation to link your bank account.");
//            $this->response($this->json($log),200); 
//        }else{
//            $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//            $this->response($this->json($log),200);
//        }
    
    protected function getStripeEmailFromDB($company_id) { //Get studio email for stripe account creation
        $execute_query = 0; // if condition met execute query
        $sql = sprintf("SELECT `email_id` From  `company` WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) { // select from company table
                $row = mysqli_fetch_assoc($result);
                $stripememail = $row['email_id'];
                if(!empty($stripememail)){
                    $out = array("status" => "Success", "msg" => $stripememail, "email_from" => 'S');
                    $this->response($this->json($out),200);
                }else{
                    $execute_query = 1;
                }
            }else {
                $execute_query = 1;
            }
            if($execute_query == 1){
               $sql1 = sprintf("SELECT `user_email` as email_id From  `user` WHERE `company_id` = '%s' AND `user_type` = 'A' ", mysqli_real_escape_string($this->db, $company_id));
                $result1 = mysqli_query($this->db, $sql1);
                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if (mysqli_num_rows($result1) > 0) { // If company email empty then select from user table
                       $row = mysqli_fetch_assoc($result1);
                       $stripememail = $row['email_id'];
                       if(!empty($stripememail)){
                           $out = array("status" => "Success", "msg" => $stripememail, "email_from" => 'U');
                           $this->response($this->json($out),200); 
                       }else{
                           $error = array('status' => "Failed", "msg" => "Email doesn't exist.");
                           $this->response($this->json($error), 200);
                       }
                    }else{
                        $error = array('status' => "Failed", "msg" => "Email doesn't exist.");
                        $this->response($this->json($error), 200);
                    } 
                } 
            }  
        }
    }
    
    protected function updateaccounttypeINDB($company_id,$wl_type,$bundle_id) {
        $sql2 = sprintf("UPDATE `white_label_apps` SET ios_account_type= '%s' where `company_id` = '%s' ", 
                mysqli_real_escape_string($this->db, $wl_type), mysqli_real_escape_string($this->db, $company_id));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $error = array('status' => "Success", "msg" => "Updated Successfully");
            $this->response($this->json($error), 200);
        }
    }
    
    protected function setupIntentCreation($company_id, $actual_student_id, $payment_method_id, $buyer_email, $buyer_first_name, $buyer_last_name, $stripe_customer_id){
        $out = [];
        $studio_name = $stripe_customer_id = "";
        
        $query2 = sprintf("SELECT c.`company_name`,s.`account_id` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result2 = mysqli_query($this->db, $query2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $row = mysqli_fetch_assoc($result2);
            $stripe_account_id = $row['account_id'];
            $studio_name = $row['company_name'];
        }
        
        if (is_null($actual_student_id) || $actual_student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Stripe', $studio_name);
            $student_id = $stud_array['student_id'];
            $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
        } else {
            $student_id = $actual_student_id;
            if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
            }
        }
        
        if (is_null($stripe_customer_id) || empty($stripe_customer_id)) {  //Creates a new customer if there is no previous customer - arun
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");
            
            try {
                $stripe_customer_create = \Stripe\Customer::create([
                            "email" => $buyer_email,
                            "description" => 'Customer created with '. $buyer_email .' for ' . $studio_name . ' studio(' . $company_id . ')',
                ]);
                $body = json_encode($stripe_customer_create);
                stripe_log_info("Customer_new : $body");
                $body_arr = json_decode($body, true);
                $stripe_customer_id = $body_arr['id'];
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            
            if(isset($err) && !empty($err)){
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);// If no records "No Content" status
            }
            //update stripe customer id to db - arun 
            $query2 = sprintf("UPDATE `student` SET `stripe_customer_id` = '%s' WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $student_id));
            $result2 = mysqli_query($this->db, $query2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }           
        }
        
        //attaching customer to payment method - arun 
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try{
            $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
            $payment_method->attach(['customer' => $stripe_customer_id]);
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught       
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        // creatiing setup indent to process payment - arun 
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try{
            $create_setup_indent = \Stripe\SetupIntent::create([
                                'payment_method_types' => ['card'],
                                'customer' => $stripe_customer_id,
                                'payment_method' => $payment_method_id,
                                'payment_method_options' => ['card'=>['request_three_d_secure'=>'any']],
                                'confirm' => true,
                                'on_behalf_of' => $stripe_account_id, // only accepted for card payments
            ]);
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught       
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        if ($create_setup_indent->status == 'requires_action' && $create_setup_indent->next_action->type == 'use_stripe_sdk') {
            $out['status'] = 'Success';
            $out['msg'] = 'Setup Intent created successfully.';
            $out['requires_action'] = true;
            $out['payment_intent_client_secret'] = $create_setup_indent->client_secret;
        } else if ($create_setup_indent->status == 'succeeded') {
            # The payment didn’t need any additional actions and completed!
            # # Handle post-payment fulfillment
            $out['status'] = 'Success';
            $out['msg'] = 'Setup Intent created successfully.';
        } else {
            # Invalid status
            $out['status'] = 'Failed';
            $out['msg'] = 'Invalid SetupIntent status.';
            $out['error'] = 'Invalid SetupIntent status';
        }
       $this->response($this->json($out), 200);
    }
    
    public function getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $payment_processor, $studio_name) {
        $select_student = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$company_id));
        $result_select_student = mysqli_query($this->db, $select_student);
        if (!$result_select_student) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student);
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result_select_student) > 0) {
                $r = mysqli_fetch_assoc($result_select_student);
                $student_id = $r['student_id'];
                if(is_null($r['stripe_customer_id']) || empty(trim($r['stripe_customer_id']))){
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = $r['stripe_customer_id'];
                    }
                }else{
                    $stripe_customer_id = $r['stripe_customer_id'];
                }
            } else {
                $insert_student = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_lastname`, `student_email`) VALUES('%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), 
                        mysqli_real_escape_string($this->db,$buyer_first_name), mysqli_real_escape_string($this->db,$buyer_last_name), mysqli_real_escape_string($this->db,$buyer_email));
                $result_insert_student = mysqli_query($this->db, $insert_student);
                if (!$result_insert_student) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student);
                    $this->response($this->json($error), 200);
                } else {
                    $student_id = mysqli_insert_id($this->db);
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = '';
                    }
                }
            }
            return array("student_id" => $student_id, "stripe_customer_id" => $stripe_customer_id);
        }
    }
    
    protected function checkCountryPaymentSupport($company_id, $country, $callback) { // GET COUNTRY PAYMENT SUPPORT
        $row = [];
        $sql = sprintf("SELECT * FROM country_payment_specific WHERE `payment_support` = 'S' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        UNION
                        SELECT * FROM country_payment_specific WHERE `payment_support` = 'W' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        ORDER BY `payment_support` ASC, `company_id` DESC", mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id)
                , mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                if ($callback == 0) {
                    $out = array("status" => "Success", "msg" => $row);
                } else {
                    $row['status'] = 'Success';
                    return $row;
                }
                $this->response($this->json($out), 200);
            } else {
                $out = array("status" => "Failed", "msg" => "Studio country is not yet available for payment support.");
                return $out;
            }
        }
    }
    
    // Muthulakshmi  Stripe Refund for Misc
    public function getCompanyStripeAccount($company_id, $call_back){
        $query = sprintf("SELECT * FROM `stripe_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result) >0){
                $row = mysqli_fetch_assoc($result);
                if($call_back == 1){
                    return $row;
                }else{
                    $msg = array("status" => "Success", "msg" => "Stripe Account details", "stripe_details" => "$row");
                    $this->response($this->json($msg), 200);
                }
            }else{
                if($call_back == 1){
                    return '';
                }else{
                    $msg = array("status" => "Failed", "msg" => "Stripe Account details", "stripe_details" => "");
                    $this->response($this->json($msg), 200);
                }
            }
        }
    }
    public function stripeRefundMiscInDB($company_id, $misc_payment_id,$refund_payment_method,$misc_order_id) {
        $charge_id = $payment_intent_id = $payment_status = $db_processing_fee = $old_last_updt_date = $processing_fee_type = '';
        $payment_amount = 0;
        $charge_array = $charge_retrieve_arr = $misc_refund_arr = [];
        
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $curr_date = date("Y-m-d");
        $dimesnsion_period = date("Y-m");
        $parent_net_sales = 0;
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
        
        if($refund_payment_method === "MC"){
            $error = array('status' => "Failed", "msg" => "Manual credit cannot be refunded");
            $this->response($this->json($error), 200);
        }else if($refund_payment_method === "CH" || $refund_payment_method === "CA" || $refund_payment_method === "CC"){
            
            if ((!empty($stripe_account) && $stripe_account['charges_enabled'] == 'Y')){
                 // Misc updatation
                if (isset($misc_payment_id) && !empty($misc_payment_id)) {
                    $get_payment_query = sprintf("SELECT mp.credit_method, mp.`payment_intent_id`, mp.`checkout_status`, mp.`payment_amount`, mp.`refunded_amount`, mp.`processing_fee`, mp.`last_updt_dt`, mo.`processing_fee_type`, sa.`stripe_refund_flag` 
                                         FROM `misc_order` mo 
                                         LEFT JOIN `misc_payment` mp ON mp.`misc_order_id` = mo.`misc_order_id` AND mp.`misc_payment_id`= '%s'
                                         LEFT JOIN `stripe_account` sa ON sa.`company_id` = mo.`company_id`
                                         WHERE mo.`company_id`='%s' AND mo.`misc_order_id`='%s' ", mysqli_real_escape_string($this->db, $misc_payment_id), mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $misc_order_id));
                    $result_payment_query = mysqli_query($this->db, $get_payment_query);
                    if (!$result_payment_query) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $get_payment_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $row_res = mysqli_fetch_assoc($result_payment_query);
                        $payment_intent_id = $row_res['payment_intent_id'];
                        $payment_status = $row_res['checkout_status'];
                        $db_processing_fee = $row_res['processing_fee'];
                        $payment_amount = $row_res['payment_amount'];
                        $old_last_updt_date = $row_res['last_updt_dt'];
                        $processing_fee_type = $row_res['processing_fee_type'];
                        $stripe_refund_flag = $row_res['stripe_refund_flag'];
                        
                        if($payment_status == "requires_action"){
                            $error = array("status" => "Failed", "msg" => "Can't able to refund amount due to action required card.");
                            $this->response($this->json($error), 200);
                        }
                        
                        if($stripe_refund_flag == 'N'){
                            $error = array("status" => "Failed", "msg" => "Connected account does not have sufficient fund to cover the refund");
                            $this->response($this->json($error), 200);
                        }


                        if($row_res['credit_method'] == "CC" && !empty($payment_intent_id)){ // for retrieve charge id
                            $charge_retrieve_arr = $this->stripeChargeIdRetrieve($payment_intent_id);
                            $charge_array_initial = json_decode(json_encode($charge_retrieve_arr),true);
                            $charge_array = $charge_array_initial['charges']['data'];
                            $charge_id = $charge_array[0]['id'];
                        }
                        if($row_res['credit_method'] == "CC" && !empty($charge_id)){ // for refund
                            try {
                                $create_refund = \Stripe\Refund::create([
                                    "charge" => $charge_id,
                                    "reverse_transfer" => true]);
                            } catch (\Stripe\Error\Card $e) {
                                // Since it's a decline, \Stripe\Error\Card will be caught       
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\RateLimit $e) {
                                // Too many requests made to the API too quickly      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\InvalidRequest $e) {
                                // Invalid parameters were supplied to Stripe's API      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Authentication $e) {
                                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\ApiConnection $e) {
                                // Network communication with Stripe failed      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Base $e) {
                                // Display a very generic error to the user, and maybe send yourself an email      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (Exception $e) {
                                // Something else happened, completely unrelated to Stripe      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            }
                            if (isset($err) && !empty($err)) {
                                $error = array('status' => "Failed", "msg" => $err['message']);
                                $this->response($this->json($error), 200);
                            }
                            $misc_refund_arr = json_decode(json_encode($create_refund),true);
                            if ($misc_refund_arr['status'] == 'succeeded'){
                                
                            }else{
                                $error = array("status" => "Failed", "msg" => "Can't able to refund amount.");
                                $this->response($this->json($error), 200);
                            }
                        }
                        if ($processing_fee_type == 1) {
                            $parent_net_sales += $payment_amount - $db_processing_fee;
                        } else {
                            $parent_net_sales += $payment_amount;
                        }
                        // Insert record into our table
                        $payment_statsus_insert = $refund_payment_method === 'CC' ? 'FR' : ($refund_payment_method === 'CH' ? 'MF' : 'MF');
                        $payment_statsus_update = $refund_payment_method === 'CC' ? 'R' : ($refund_payment_method === 'CH' ? 'MR' : 'MR');
                        $insert_query = sprintf("INSERT INTO `misc_payment`(`misc_order_id`,`student_id`, `payment_intent_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`,`payment_method_id`, `stripe_card_name`, `created_dt`, `last_updt_dt`,refunded_amount,refunded_date,credit_method,check_number,`payment_from`) 
                                        SELECT `misc_order_id`,`student_id`, `payment_intent_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, '$payment_statsus_insert',`payment_method_id`, `stripe_card_name`, `created_dt`,'$old_last_updt_date','0','0000-00-00',credit_method,check_number,`payment_from` FROM `misc_payment` WHERE `misc_payment_id`='%s'", mysqli_real_escape_string($this->db, $misc_payment_id));
                        $result_insert_query = mysqli_query($this->db, $insert_query);
                        if (!$result_insert_query) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $insert_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }

                        //Update misc Payment Table
                        $update_query3 = sprintf("UPDATE `misc_payment` mp,misc_order mo SET `payment_status`='%s', `refunded_amount`=(IF(mo.processing_fee_type='2',(mp.payment_amount+mp.processing_fee),mp.payment_amount)), `refunded_date`='$curr_date', `paid_amount` = `paid_amount`-$parent_net_sales WHERE mp.misc_order_id=mo.misc_order_id AND mp.`misc_payment_id`='%s'", mysqli_real_escape_string($this->db, $payment_statsus_update), mysqli_real_escape_string($this->db, $misc_payment_id));
                        $result_update_query3 = mysqli_query($this->db, $update_query3);
                        if (!$result_update_query3) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $update_query3");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }

                        //Update misc dimesnsion Table
                        $update_query6 = sprintf("UPDATE `misc_dimensions`  SET `sales_amount` = `sales_amount`-(SELECT IF($processing_fee_type = 1 , payment_amount-processing_fee ,payment_amount) payment_amount from misc_payment where misc_payment_id='%s') WHERE `company_id` ='%s' and period='%s'", mysqli_real_escape_string($this->db, $misc_payment_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $dimesnsion_period));
                        $result_update_query6 = mysqli_query($this->db, $update_query6);
                        if (!$result_update_query6) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "6- $update_query6");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                        $this->getmiscpaymenthistory($company_id, $misc_order_id);
                    }
                }
            }else{
                $error = array("status" => "Failed", "msg" => "We cannot able to access your stripe account due to invalid stripe status.");
                $this->response($this->json($error), 200);
            }
        }else{
            $error = array("status" => "Failed", "msg" => "Invalid payment method");
            $this->response($this->json($error), 200);
        }
    }
    
    public function stripeChargeIdRetrieve($payment_intent_id) {
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        if (!empty($payment_intent_id)) {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $charge_id_retrieve = \Stripe\PaymentIntent::retrieve($payment_intent_id);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
            return $charge_id_retrieve;
        }
    }
    
    public function wepayUserTokenGeneration($company_id, $user_first_name, $user_last_name, $user_email) {
        $user_id = $access_token = $first_name = $last_name = $user_name = $email = $account_id = $account_state = $action_reasons = $disabled_reasons = $country = $company_name = $contact_id = $list_item_id = '';
        $account_address = $account_phone = $account_mail = [];
        $website = $url = $social = $currency_code = $currency_symbol = '';
        $gmt_date = gmdate(DATE_RFC822);
        $sns_msg5 = array("buyer_email" => $company_id, "membership_title" => $user_email, "gmt_date" => $gmt_date);
        if (!function_exists('getallheaders')){
            if(!function_exists ('apache_request_headers')){
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            }else{
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
            }
        }else{
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
        }
        $ip = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR');   
        
        $time = time();
        $temp_password = $this->wp_pswd;
        $method = 'aes-256-cbc';
        // Must be exact 32 chars (256 bit)
        $password = substr(hash('sha256', $temp_password, true), 0, 32);
        // IV must be exact 16 chars (128 bit)
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        
        
        $query_check_wepay = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result_check_wepay = mysqli_query($this->db, $query_check_wepay);
        if(!$result_check_wepay){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_check_wepay");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result_check_wepay)>0){
                $error = array('status' => "Failed", "msg" => "Wepay user account details already available to this studio. Please contact administrator to reset Wepay account.");
                $this->response($this->json($error), 200);
            }
        }
        $type = 'R';
        $query_check_email = sprintf("SELECT * FROM `wp_account` WHERE `user_email`='%s' ORDER BY `wp_account_id` DESC limit 0,1", mysqli_real_escape_string($this->db, $user_email));
        $result_check_mail = mysqli_query($this->db, $query_check_email);
        if (!$result_check_mail) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_check_email");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $numrows = mysqli_num_rows($result_check_mail);
            if ($numrows > 0) {
                if ($row['company_id'] == $company_id) {
                    $error = array('status' => "Failed", "msg" => "Same Wepay user details available to this studio.");
                    $this->response($this->json($error), 200);
                } else {
                    $user_id = $row['wp_user_id'];
                    $token = $row['access_token'];
                    $access_token = openssl_decrypt(base64_decode($token), $method, $password, OPENSSL_RAW_DATA, $iv);
                    $type = 'L';
                }
            }
        }


        if($type=='L'){
            $query_check_email = sprintf("SELECT * FROM `wp_account` WHERE `user_email`='%s' ORDER BY `wp_account_id` DESC limit 0,1", mysqli_real_escape_string($this->db, $user_email));
            $result_check_mail = mysqli_query($this->db, $query_check_email);
            if(!$result_check_mail){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_check_email");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                $numrows = mysqli_num_rows($result_check_mail);
                if($numrows>0){
                    $row = mysqli_fetch_assoc($result_check_mail);
//                    if($row['first_name']==$user_first_name && $row['last_name']==$user_last_name){
                        if($row['company_id']==$company_id){
                            $error = array('status' => "Failed", "msg" => "Same Wepay user details available to this studio.");
                            $this->response($this->json($error), 200);
                        }else{
                            $user_id = $row['wp_user_id'];
                            $token = $row['access_token'];
                            $access_token = openssl_decrypt(base64_decode($token), $method, $password, OPENSSL_RAW_DATA, $iv);
                        }
//                    }else{
//                        $error = array('status' => "Failed", "msg" => "Wepay user details mismatched.");
//                        $this->response($this->json($error), 200);
//                    }
                }else{
                    $error = array('status' => "Failed", "msg" => "No user account created via Mystudio app.");
                    $this->response($this->json($error), 200);
                }
            }
        }else{
            $json = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","email"=>"$user_email","scope"=>"manage_accounts,collect_payments,view_user,send_money,preapprove_payments",
                "first_name"=>"$user_first_name","last_name"=>"$user_last_name","original_ip"=>"$ip","original_device"=>"$user_agent","tos_acceptance_time"=>$time);
            $postData = json_encode($json);            
            $sns_msg5['call'] = "User";
            $sns_msg5['type'] = "User registration";
            $response = $this->wp->accessWepayApi("user/register",'POST',$postData,'',$user_agent,$sns_msg5);
            $this->wp->log_info("wepay_user_create: ".$response['status']);
            if($response['status'] == "Success"){
                $res = $response['msg'];
                $user_id = $res['user_id'];
                $access_token = $res['access_token'];
                if(isset($res['expires_in']) && !empty($res['expires_in'])){
                    $json5 = array("email_subject"=>"MyStudio - Verify your WePay account");
                    $postData5 = json_encode($json5);
                    $sns_msg5['call'] = "User";
                    $sns_msg5['type'] = "Sending User confirmation email";
                    $response5 = $this->wp->accessWepayApi("user/send_confirmation",'POST',$postData5,$access_token,$user_agent,$sns_msg5);
                    if($response5['status'] == "Success"){

                    }else{
                        $log = array("status" => "Failed", "msg" => $response5['msg']['error_description']);
                        $this->response($this->json($log),200);
                    }
                }
            }else{
                $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
                $this->response($this->json($log),200);
            }
        }     
        
        
//        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
        
        $query_studio_details = sprintf("SELECT * FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_studio_details = mysqli_query($this->db, $query_studio_details);
        if(!$result_studio_details){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query_studio_details");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $time = time();
            $num_rows = mysqli_num_rows($result_studio_details);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result_studio_details);
                $company_name = $row['company_name'];
                $contact_id = $row['contact_id'];
                $list_item_id = $row['list_item_id'];
                $currency_code = $row['wp_currency_code'];
                $currency_symbol = $row['wp_currency_symbol'];
                if(!empty(trim($row['country']))){
                    if (strtolower($row['country']) == 'ca' || strtolower($row['country']) == 'canada'){
                        $country="CA";
                    }elseif (strtolower($row['country']) == 'uk' || strtolower($row['country']) == 'gb' || strtolower($row['country']) == 'united kingdom'){
                        $country="GB";
                    }else{
                        $country="US";
                    }
                }else{
                    $country = "US";
                }
                if(!empty(trim($row['postal_code']))){
                    $temp=[];
                    $account_address['receive_time'] = $time;
                    $account_address['type'] = "address";
                    $account_address['source'] = "user";
                    if(!empty(trim($row['street_address']))){
                        $temp['address1']=$row['street_address'];
                    }else{
                        $temp['address1']="N/A";
                    }
                    if(!empty(trim($row['city']))){
                        $temp['city']=$row['city'];
                    }else{
                        $temp['city']="N/A";
                    }
                    if(!empty(trim($row['postal_code']))){
                        $temp['zip']=$row['postal_code'];
                    }else{
                        $temp['zip']="N/A";
                    }
                    $temp['country'] = $country;
                    if(!empty(trim($row['web_page']))){
                        $website = $row['web_page'];
                    }
                    if(!empty(trim($row['facebook_url']))){
                        $social = "facebook";
                        $url = $row['facebook_url'];
                    }elseif(!empty(trim($row['twitter_url']))){
                        $social = "twitter";
                        $url = $row['twitter_url'];
                    }
                    $account_address['properties']['address'] = $temp;
                }
                if(!empty(trim($row['phone_number']))){
                    $account_phone['receive_time'] = $time;
                    $account_phone['type'] = "phone";
                    $account_phone['source'] = "user";
                    $account_phone['properties']['phone'] = $row['phone_number'];
                    $account_phone['properties']['type'] = "work";
                }else{
                    $account_phone['receive_time'] = $time;
                    $account_phone['type'] = "phone";
                    $account_phone['source'] = "user";
                    $account_phone['properties']['phone'] = "N/A";
                    $account_phone['properties']['type'] = "work";
                }
                if(!empty(trim($row['email_id']))){
                    $account_mail['receive_time'] = $time;
                    $account_mail['type'] = "email";
                    $account_mail['source'] = "user";
                    $account_mail['properties']['email'] = $row['email_id'];
                }else{
                    $account_mail['receive_time'] = $time;
                    $account_mail['type'] = "email";
                    $account_mail['source'] = "user";
                    $account_mail['properties']['email'] = "N/A";
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Studio details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }

//// My secret message 1234
//$decrypted = openssl_decrypt(base64_decode($encrypted), $method, $password, OPENSSL_RAW_DATA, $iv);

        
                    
//        $json = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","code"=>"$code","redirect_uri"=>"$redirect_uri");
//        $postData = json_encode($json);
//        $response = $this->wp->accessWepayApi("oauth2/token",'POST',$postData,'',$user_agent);
//        $this->wp->log_info("wepay_oauth2_token: ".$response['status']);
//        if($response['status'] == "Success"){
//            $user_id = $response['msg']['user_id'];
//            $access_token = $response['msg']['access_token'];
//            if(!empty($user_id)){
                $sns_msg5['call'] = "User";
                $sns_msg5['type'] = "User details";
                $response2 = $this->wp->accessWepayApi("user",'GET','',$access_token,$user_agent,$sns_msg5);
                $this->wp->log_info("wepay_getuser: ".$response2['status']);
                if($response2['status'] == "Success"){
                    $res = $response2['msg'];
                    $first_name = $res['first_name'];
                    $last_name = $res['last_name'];
                    $user_name = $res['user_name'];
                    $email = $res['email'];
                    $user_state = $res['state'];
                    $user_callback_url = $this->server_url."/Wepay2/updateUser";
                    $json1 = array("callback_uri"=>"$user_callback_url");
                    $postData1 = json_encode($json1);
                    $sns_msg5['call'] = "User";
                    $sns_msg5['type'] = "Updating User details";
                    $resp = $this->wp->accessWepayApi("user/modify",'POST',$postData1,$access_token,$user_agent,$sns_msg5);
                    $this->wp->log_info("wepay_user_modify: ".$resp['status']);
                }else{
                    $log = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                    $this->response($this->json($log),200);
                }
//            }
            
            $acc_callback_url = $this->server_url."/Wepay2/updateAccount";
            if(strtolower($country)=="canada" || strtolower($country)=="ca"){
                $json3 = array("name"=>"$company_name","description"=>"$company_name","callback_uri"=>"$acc_callback_url","country"=>"CA","country_options"=>array("debit_opt_in"=> true),"currencies"=>array("$currency_code"));
            }elseif(strtolower($row['country']) == 'uk' || strtolower($row['country']) == 'gb' || strtolower($row['country']) == 'united kingdom'){
                $json3 = array("name"=>"$company_name","description"=>"$company_name","callback_uri"=>"$acc_callback_url","country"=>"GB","currencies"=>array("$currency_code"));
            }else{
                $json3 = array("name"=>"$company_name","description"=>"$company_name","callback_uri"=>"$acc_callback_url");
            }
            $postData3 = json_encode($json3);
            $sns_msg5['call'] = "Account";
            $sns_msg5['type'] = "Account Creation";
            $response3 = $this->wp->accessWepayApi("account/create",'POST',$postData3,$access_token,$user_agent,$sns_msg5);
            $this->wp->log_info("wepay_account_create: ".$response3['status']);
            if($response3['status'] == "Success"){
                $res = $response3['msg'];
                $account_id = $res['account_id'];
                $account_state = $res['state'];
                $action_reasons = implode(",", $res['action_reasons']);
                $disabled_reasons = implode(",", $res['disabled_reasons']);
                $currency = implode(",", $res['currencies']);
                $this->rbitCallForAccount($access_token, $user_agent, $account_id, $company_name, $time, $account_address, $account_mail, $account_phone, $website, $social, $url);
            }else{
                $log = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                $this->response($this->json($log),200);
            }
            
            $encrypted_access_token = base64_encode(openssl_encrypt($access_token, $method, $password, OPENSSL_RAW_DATA, $iv));
            $query = sprintf("INSERT INTO `wp_account`(`company_id`, `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `first_name`, `last_name`, `user_name`, `user_email`, `action_reasons`, `disabled_reasons`, `currency`)
                    VALUE ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $user_id), mysqli_real_escape_string($this->db, $user_state), mysqli_real_escape_string($this->db, $encrypted_access_token),
                    mysqli_real_escape_string($this->db, $account_id), mysqli_real_escape_string($this->db, $account_state), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name),
                    mysqli_real_escape_string($this->db, $user_name), mysqli_real_escape_string($this->db, $email), mysqli_real_escape_string($this->db, $action_reasons), mysqli_real_escape_string($this->db, $disabled_reasons), mysqli_real_escape_string($this->db, $currency));
            $result = mysqli_query($this->db, $query);
            
            if(!$result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
            
            if(!empty($account_state) && $account_state!="disabled" && $account_state!="deleted"){
                if($account_state=="pending"){
                    $status = 'P';
                }else{
                    $status = 'Y';
                }
                $update_company = sprintf("UPDATE `company` SET `wepay_status`='%s' WHERE `company_id`='%s'", $status, mysqli_real_escape_string($this->db, $company_id));
                $result_update_company = mysqli_query($this->db, $update_company);
                if(!$result_update_company){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                if(!empty($list_item_id)&&!empty($contact_id)){
                    $wp_status = "1";
                    $json = array("contactIds"=>array("$contact_id"),"fieldValues"=>array("24"=>array(array("raw"=>"$wp_status"))));
                    $postData = json_encode($json);
                    $sf_response = $this->sf->accessSalesforceApi("lists/$this->list_id/listitems/$list_item_id",'PUT',$postData);
                    log_info("updateCompanyDetailsinSalesforceApi_ListItemUpdation: ".$sf_response['status']);
                }
            }
            $log = array("status" => "Success", "msg" => "Wepay user account created successfully. Please check your email for the confirmation to link your bank account.");
            return $log;
//        }else{
//            $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//            $this->response($this->json($log),200);
//        }
    }
    
    public function getCountryList() {
        $supported_country_list = [];
        $query = sprintf("SELECT `country`, `payment_support`, `support_status` FROM country_payment_specific WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, 0));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_object($result)) {
                    $supported_country_list [] = $row;
                }
            }
            $out = array("status" => "Success", "supported_country_list" => $supported_country_list);
            $this->response($this->json($out),200);
        }
    }
    
    public function rbitCallForAccount($token, $user_agent, $account_id, $company_name, $time, $account_address, $account_mail, $account_phone, $website, $social, $url) {
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg6=array("buyer_email"=>$account_id,"membership_title"=>$company_name,"gmt_date"=>$gmt_date,"call"=>"Account","type"=>"rbit");
        if (empty($account_address) && empty($account_mail) && empty($account_phone)) {
            $error_log = array('status' => "Failed", "msg" => "Studio details are empty.");
            log_info($this->json($error_log));
        } else {
            for ($i = 1; $i < 5; $i++) {
                $json = [];
                $postData = '';
                switch ($i) {
                    case 1:
                        $json['associated_object_type'] = "account";
                        $json['associated_object_id'] = $account_id;
                        $json['receive_time'] = $time;
                        $json['type'] = "person";
                        $json['source'] = "user";
                        $json['properties']['name'] = "$company_name";
                        $temp = [];
                        if (!empty($account_address)) {
                            array_push($temp, $account_address);
                        }
                        if (!empty($account_mail)) {
                            array_push($temp, $account_mail);
                        }
                        if (!empty($account_phone)) {
                            array_push($temp, $account_phone);
                        }
                        $json['related_rbits'] = $temp;
                        break;
                    case 2:
                        $json = array("associated_object_type" => "account", "associated_object_id" => $account_id, "receive_time" => $time, "type" => "business_name", "source" => "user", "properties" => array("business_name" => "$company_name"));
                        break;
                    case 3:
                        $json = array("associated_object_type" => "account", "associated_object_id" => $account_id, "receive_time" => $time, "type" => "business_description", "source" => "user", "properties" => array("business_description" => "Martial arts studio"));
                        break;
                    case 4:
                        if (!empty(trim($website))) {
                            $json = array("associated_object_type" => "account", "associated_object_id" => $account_id, "receive_time" => $time, "type" => "external_account", "source" => "user",
                                "properties" => array("is_partner_account" => "no", "account_type" => "$company_name", "uri" => "$website"));
                        } else {
                            continue 2;
                        }
                        break;
//                    case 5:
//                        if (!empty(trim($url))) {
//                            $json = array("associated_object_type" => "account", "associated_object_id" => $account_id, "receive_time" => $time, "type" => "external_account", "source" => "user",
//                                "properties" => array("is_partner_account" => "no", "account_type" => "$social", "uri" => "$url"));
//                        } else {
//                            continue 2;
//                        }
//                        break;
                    default :
                        break;
                }

                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi("rbit/create", 'POST', $postData, $token, $user_agent,$sns_msg6);
                $this->wp->log_info("wepay_rbit_create: " . $response['status']);
                if ($response['status'] == "Success") {
                    continue;
                } else {
                    $error = array("status" => "Failed", "msg" => $response['msg']['error_description']);
                    $this->response($this->json($error), 200);
                }
            }
        }
    }
    
    protected function reRunstripeMiscPaymentINDB($company_id, $reg_id, $payment_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        $has_3D_secure = 'N';
        date_default_timezone_set($new_timezone);
        $button_url = $buyer_name = $buyer_email = $buyer_phone = $payment_method_id = $stripe_card_name = $studio_name = $stripe_currency = $stripe_account_id = $stripe_customer_id = '';
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";

        $query = sprintf("SELECT mo.`company_id`, mo.`student_id`, mo.`misc_order_id`, `buyer_name`, `buyer_email`, `buyer_phone`,
                mp.`processing_fee`, mo.`payment_amount` total_due, mo.`paid_amount`, mo.`credit_card_id`, mo.`credit_card_name`, mo.`credit_card_status`,  mp.`credit_method`,
                mp.`payment_amount`, mp.`payment_date`, mp.`payment_status`, mo.`processing_fee_type`, mp.`checkout_id`, mp.`checkout_status`,c.`wp_currency_symbol`,mo.`student_id`,c.`company_name`,s.`account_id`,mo.`stripe_customer_id`,mp.`stripe_card_name`,mp.`payment_method_id`,mo.`student_id`,s.`currency`  
                FROM `misc_order` mo
                LEFT JOIN `misc_payment` mp ON mo.`misc_order_id`=mp.`misc_order_id` AND (mp.`payment_status`='F' || (mp.`payment_status`='N' && mp.`payment_date`< ($currdate_add)))
                LEFT JOIN `company` c ON mo.`company_id` = c.`company_id` 
                LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` 
                WHERE   mo.`misc_order_id` = '%s'  AND `misc_payment_id` = '%s'  AND mp.`payment_amount`>0", mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $payment_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } 
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $company_id = $row['company_id'];
                    $student_id = $row['student_id'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $payment_method_id = $row['payment_method_id'];
                    $stripe_card_name = $row['stripe_card_name'];
                    $studio_name = $row['company_name'];
                    $stripe_currency = $row['currency'];
                    $stripe_account_id = $row['account_id'];
                    $stripe_customer_id = $row['stripe_customer_id'];

                    if ($row['credit_method'] == 'CA' || $row['credit_method'] == 'CH') {
                        $error = array('status' => "Failed", "msg" => "Miscellaneous payment via cash or check can not be rerun.");
                        $this->response($this->json($error), 200);
                    }

                    if (!empty($payment_method_id)) {
                        $payment_amount = $row['payment_amount'];
                        $processing_fee = $row['processing_fee'];
                        $processing_fee_type = $row['processing_fee_type'];

                        if ($processing_fee_type == 2) {
                            $w_paid_amount = $payment_amount + $processing_fee;
                        } elseif ($processing_fee_type == 1) {
                            $w_paid_amount = $payment_amount;
                        }

                        $this->getStripeKeys();
                        \Stripe\Stripe::setApiKey($this->stripe_sk);
                        \Stripe\Stripe::setApiVersion("2019-05-16");
                        if (!empty($stripe_customer_id) && !empty($payment_method_id)) {
                            if ($processing_fee_type == 2) {
                                $w_paid_amount = $payment_amount + $processing_fee;
                            } elseif ($processing_fee_type == 1) {
                                $w_paid_amount = $payment_amount;
                            }
                            $w_paid_amount_round = round($w_paid_amount, 2);
                            $dest_amount = $w_paid_amount - $processing_fee;
                            $dest_amount_round = round($dest_amount, 2);
                            $dest_amount_in_dollars = $dest_amount_round * 100;
                            $amount_in_dollars = $w_paid_amount_round * 100;
                            $checkout_type = 'misc';
                            $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $checkout_type, '');
                            if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                                $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                $checkout_state = 'released';
                            } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {
                                $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                $checkout_state = 'requires_action';
                                $has_3D_secure = 'Y';
                                $next_action_type = $payment_intent_result['next_action']['type'];
                                if ($next_action_type != "redirect_to_url") {
                                    # Payment not supported
                                    $out['status'] = 'Failed';
                                    $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                                    $this->response($this->json($out), 200);
                                }
                                $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                            } else {
                                # Invalid status
                                $out['status'] = 'Failed';
                                $out['msg'] = 'Invalid PaymentIntent status.';
                                $out['error'] = 'Invalid PaymentIntent status';
                                $this->response($this->json($out), 200);
                            }
                        }
                        if (!empty($payment_method_id)) {
                            try {
                                $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                                $brand = $retrive_payment_method->card->brand;
                                $last4 = $retrive_payment_method->card->last4;
                                $temp = ' xxxxxx';
                                $stripe_card_name = $brand . $temp . $last4;
                            } catch (\Stripe\Error\Card $e) {
                                // Since it's a decline, \Stripe\Error\Card will be caught     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\RateLimit $e) {
                                // Too many requests made to the API too quickly     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\InvalidRequest $e) {
                                // Invalid parameters were supplied to Stripe's API     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Authentication $e) {
                                // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\ApiConnection $e) {
                                // Network communication with Stripe failed     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Base $e) {
                                // Display a very generic error to the user, and maybe send yourself an email     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (Exception $e) {
                                // Something else happened, completely unrelated to Stripe     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            }
                            if (isset($err) && !empty($err)) {
                                $error = array('status' => "Failed", "msg" => $err['message']);
                                $this->response($this->json($error), 200);
                            }
                        }
                        $payment_query = sprintf("UPDATE `misc_payment` SET `payment_intent_id`='%s', `checkout_status`='%s', `payment_status`='S', `payment_method_id`='$payment_method_id', `stripe_card_name`='$stripe_card_name' WHERE `misc_payment_id`='%s'", mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $payment_id));
                        $payment_result = mysqli_query($this->db, $payment_query);
                        if (!$payment_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            if ($has_3D_secure == 'Y') {
                                $email_msg = 'Hi ' . $buyer_name . ',<br>';
                                $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">This is an authentication email for Membership ' . $membership_title . ' registration rerun payment. Kindly click the launch button for redirecting to Stripe.</p>';
                                $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">Complete a required action for this payment. Once the redirect is completed for this PaymentIntent, the payment will proceed.</p>';
                                $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name, $email_msg);
                            }
                            $this->getmiscpaymenthistory($company_id, $reg_id);
                        }
                    } else {
                        $error = array('status' => "Failed", "msg" => "Payment Method error. Cannot be accept payments anymore.");
                        $this->response($this->json($error), 200);
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Miscellaneous payment details mismatched.");
                $this->response($this->json($error), 200);
            }
        date_default_timezone_set($curr_time_zone);
    }

    public function payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $p_type, $cc_type) {
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        if ($cc_type == 'N') {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }
        try {
            $payment_intent_create = \Stripe\PaymentIntent::create([
                        'payment_method' => $payment_method_id,
                        'customer' => $stripe_customer_id,
                        'amount' => $amount_in_dollars,
                        'currency' => $stripe_currency,
                        'confirmation_method' => 'automatic',
                        'capture_method' => 'automatic',
                        'confirm' => true,
                        'return_url' => $this->server_url."/stripeUpdation.php?type=authenticate&c_id=$company_id&p_type=$p_type&s_id=$student_id",
                        'metadata' => ['c_id' => $company_id, 'p_type' => $p_type, 's_id' => $student_id, 's_name' => $studio_name],
                        'setup_future_usage' => 'off_session',
                        'description' => 'Payment Intent for ' . $buyer_email,
                        'payment_method_types' => ['card'],
                        'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                        'receipt_email' => $buyer_email,
                        'transfer_data' => ['destination' => $stripe_account_id, 'amount' => $dest_amount_in_dollars],
                        'on_behalf_of' => $stripe_account_id,
            ]);
            $body3 = json_encode($payment_intent_create);
            stripe_log_info("Stripe Payment Creation : $body3");
            $return_array['temp_payment_status'] = $payment_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded') {
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['cc_month'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_month;
                $return_array['cc_year'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_year;
                $brand = $payment_intent_create->charges->data[0]->payment_method_details->card->brand;
                $last4 = $payment_intent_create->charges->data[0]->payment_method_details->card->last4;
                $temp = ' xxxxxx';
                $return_array['stripe_card_name'] = $brand . $temp . $last4;
            } else if ($return_array['temp_payment_status'] == 'requires_action') {
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['payment_intent_client_secret'] = $payment_intent_create->client_secret;
                $return_array['next_action'] = $payment_intent_create->next_action;
//                $return_array['next_action']['type'] = $payment_intent_create->next_action->type;
//                $return_array['next_action']['redirect_to_url']['url'] = $payment_intent_create->next_action->redirect_to_url->url;
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }
    
    private function getRealIpAddr(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet        
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    public function sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg){
        
        // EMAIL CONTENT
        $button_text = 'Launch';
        $subject = 'Stripe authenticate';
        $message = $email_msg.'<br>'.'<center><a href="'.$button_url.'"><button type="button" style="width: 190px;border-radius: 5px;background: #009a61;border:1px solid 009a61;color: #ffffff;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 45px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;text-transform: none;overflow: visible;box-sizing: border-box;cursor: pointer;">'.$button_text.'</button></a></center>';
        $sendEmail_status = $this->sendEmail($buyer_email, $subject, $message, $studio_name, '', '', '', '', $company_id, '','');
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
    }

}

?>