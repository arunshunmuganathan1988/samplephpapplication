<?php


       header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');

require_once '../../../Globals/cont_log.php';
require_once 'Salesforce.php';
require_once 'WePay.php';
require_once('../../PHPMailer_5.2.1/class.phpmailer.php');
require_once('../../PHPMailer_5.2.1/class.smtp.php');
require_once 'tfpdf.php';
//require_once '../../../aws/aws-autoloader.php';
require_once __DIR__.'/../../../aws/aws.phar';
require_once '../../Stripe/init.php';

use Aws\Sns\SnsClient;


class TrialModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    public $local_upload_folder_name = '';
    private $sf;
    private $list_id = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_redirect_uri = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $server_url;
    private $stripe_pk = '', $stripe_sk = '';

    public function __construct() {
        require_once '../../../Globals/config.php';
        require_once  "../../../Globals/stripe_props.php";
        $this->inputs();
        $this->sf = new Salesforce();
        $this->wp = new wepay();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->local_upload_folder_name = $GLOBALS['local_uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->list_id = $this->sf->list_id;
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = "../../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'dev.mystudio.academy') !== false) {
            $this->server_url = "http://dev.mystudio.academy";
        } elseif (strpos($host, 'dev2.mystudio.academy') !== false) {
            $this->server_url = "http://dev2.mystudio.academy";
        } elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'];
        } else {
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithSalesforce($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
        header('Access-Control-Allow-Origin:'.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    protected function clean($string) {
        $str = preg_replace('/\s+/', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    }
    
    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
    
    public function getTrialProgramDetails($company_id, $trial_id, $call_back) {//call_back, 0 -> returned & exit, 1 -> return response
        //check array_slice in following before alter or add any columns in this query
        $sql = sprintf("SELECT `trial_id`, t.`company_id`, `trial_status`, `trial_title`, `trial_subtitle`, `trial_desc`, CONCAT(`trial_prog_url`,'//',UNIX_TIMESTAMP()) trial_prog_url, `trial_welcome_url`, `trial_banner_img_url`, `trial_banner_img_content`, `trial_waiver_policies`, `net_sales`, `processing_fee_type`, 
            `price_amount`, `program_length_type`, `program_length`, `trial_sort_order`,  `trial_registration_column_1`, `trial_registration_column_1_flag`, `trial_registration_column_2`, `trial_registration_column_2_flag`, `trial_registration_column_3`, `trial_registration_column_3_flag`, `trial_registration_column_4`, 
            `trial_registration_column_4_flag`, `trial_registration_column_5`, `trial_registration_column_5_flag`, `trial_registration_column_6`, `trial_registration_column_6_flag`, `trial_registration_column_7`, `trial_registration_column_7_flag`, `trial_registration_column_8`, `trial_registration_column_8_flag`, 
            `trial_registration_column_9`, `trial_registration_column_9_flag`, `trial_registration_column_10`, `trial_registration_column_10_flag`,`trial_lead_source_1`, `trial_lead_source_2`,  `trial_lead_source_3`, 
             `trial_lead_source_4`, `trial_lead_source_5`, `trial_lead_source_6`,  `trial_lead_source_7`,  `trial_lead_source_8`, 
            `trial_lead_source_9`,  `trial_lead_source_10`,IF(t.`trial_status`='P','published',IF(t.`trial_status`='S','draft','unpublished')) list_type
        FROM `trial` t left join company c on t.`company_id`=c.`company_id` where `trial_id`='%s' and t.`company_id`='%s'", mysqli_real_escape_string($this->db,$trial_id), mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
//        log_info($sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if($call_back==1){
                return $error;
            }else{
                $this->response($this->json($error), 200);
            }
        } else {
          $trial_program_list = [];
          $temp_cat = [];            
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                    $reg_columns =  [];
                    $trial_id = $row['trial_id'];
                   $row_category = array_slice($row, 0, 17);
                    $reg_field_name_array = [];
                    $temp_reg_field = "trial_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "trial_registration_column_".$z."_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row_category['reg_columns'] = $reg_field_name_array;
                   
                    $temp_lead_field = "trial_lead_source_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_lead = $temp_lead_field."$z";      //for filed names
                       if($row[$reg_lead]!=''){
                            $lead_name_array[] = array("lead_col_name"=>$row[$reg_lead],"lead_col_index"=>$y);
                        }
                    } 
                  $row_category['lead_columns'] = $lead_name_array;
                   $row_category['list_type'] = $row['list_type'];
                   
             }
                 $row_category['discount'] = $this->getTrialDiscountDetails($company_id, $trial_id);//work
             $trial_program_list = $row_category;
                    
                $out = array('status' => "Success", 'msg' => $trial_program_list);
                // If success everythig is good send header as "OK" and user details
                if($call_back==1){
                    return $out;
                }else{
                    $this->response($this->json($out), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Trial details doesn't exist.");
                if($call_back==1){
                    return $error;
                }else{
                    $this->response($this->json($error), 200);
                }
            }
        }
    }

    public function addTrialProgramDetails($company_id, $trial_status, $trial_title, $trial_subtitle,  $trial_banner_img_content, $trial_welcome_url, $trial_description,$trial_banner_img_type){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_new = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $trial_id = $return_trial_id = '';
        $trial_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->local_upload_folder_name/Default/default.png";
        
       
        $sql1 = sprintf("INSERT INTO `trial` (`company_id`, `trial_status`, `trial_title`, `trial_subtitle`, `trial_banner_img_url`, `trial_welcome_url`, `trial_desc`, `trial_sort_order`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', NextVal('trial_sort_order_seq'))", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_status),
                mysqli_real_escape_string($this->db, $trial_title), mysqli_real_escape_string($this->db, $trial_subtitle),
                mysqli_real_escape_string($this->db, $trial_banner_img_url), mysqli_real_escape_string($this->db, $trial_welcome_url),
                mysqli_real_escape_string($this->db, $trial_description));
        $result1 = mysqli_query($this->db, $sql1);

        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $trial_id = mysqli_insert_id($this->db);
            
            $sql2 = sprintf("SELECT `company_code` FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
            $result2 = mysqli_query($this->db, $sql2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $output_value = mysqli_fetch_assoc($result2);
                $company_code = $output_value['company_code'];
            }
            $company_code_new = $this->clean($company_code);
            $trial_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/t/?=$company_code_new/$company_id/$trial_id"; 

            $sql3 = sprintf("UPDATE `trial` SET `trial_prog_url` = '$trial_url' WHERE `trial_id` = '%s' ", mysqli_real_escape_string($this->db, $trial_id));
            $result3 = mysqli_query($this->db, $sql3);
            if (!$result3) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
                
        
            if ($trial_banner_img_content != '') {
                $trial_banner_img_url = "";
                $cfolder_name = "Company_$company_id";
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name";
                chdir($dir_name);
                if (!file_exists($cfolder_name)) {
                    $oldmask = umask(0);
                    mkdir($cfolder_name, 0777, true);
                    umask($oldmask);
                }
                chdir($cfolder_name);
                $folder_name = "Trial";
                if (!file_exists($folder_name)) {
                    $oldmask = umask(0);
                    mkdir($folder_name, 0777, true);
                    umask($oldmask);
                }
                chdir($folder_name);
                $image_content = base64_decode($trial_banner_img_content);
                $file_name = $company_id . "-" . $trial_id;
                $file = "$file_name.$trial_banner_img_type";
                $img = $file; // logo - your file name
                $ifp = fopen($img, "wb");
                fwrite($ifp, $image_content);
                fclose($ifp);
                chdir($old); // Restore the old working directory

                $trial_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->local_upload_folder_name/$cfolder_name/$folder_name/$file";

                $update_category = sprintf("UPDATE `trial` SET `trial_banner_img_url`='%s' WHERE `trial_id`='%s'", mysqli_real_escape_string($this->db,$trial_banner_img_url), $trial_id);
                $result = mysqli_query($this->db, $update_category);
                
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_category");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
            $this->getTrialProgramDetails($company_id, $trial_id, 0);   //call_back, 0 -> returned & exit, 1 -> return response
        }
        date_default_timezone_set($curr_time_zone);
    }
    
     public function updateTrialPaymentDetailsInDB($company_id, $trial_id, $trial_price, $trial_processingfeetype, $trial_programlengthperiod, $trial_programlengthquantity ) {

        $return_trial_id="";
        $update_company = sprintf("UPDATE `trial` SET `price_amount`='%s',`processing_fee_type`='%s',`program_length_type`='%s',`program_length`='%s' WHERE `company_id`='%s' AND `trial_id`='%s'", 
                mysqli_real_escape_string($this->db,$trial_price),  mysqli_real_escape_string($this->db,$trial_processingfeetype),
                mysqli_real_escape_string($this->db,$trial_programlengthperiod), mysqli_real_escape_string($this->db,$trial_programlengthquantity),  mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id));
        $result = mysqli_query($this->db, $update_company);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {

            $this->getTrialProgramDetails($company_id,$trial_id ,0); 
        }
    }
    
     public function updateTrialRegistrationDetailsInDB($company_id, $trial_id, $trial_column_name, $trial_column_value, $trial_mandatory_column_name, $trial_mandatory_column_value, $list_type){

        $return_trial_id="";
        $update_trial = sprintf("UPDATE `trial` SET $trial_column_name='%s',$trial_mandatory_column_name='%s' WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db,$trial_column_value), mysqli_real_escape_string($this->db,$trial_mandatory_column_value), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id));
        $result = mysqli_query($this->db, $update_trial);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
           $this->getTrialProgramDetails($company_id,$trial_id ,0); 
        }
    }
    
    public function deleteTrialRegistrationDetailsInDB($company_id, $trial_id, $trial_column_name, $list_type) {
        
        $column_array = array("trial_registration_column_1","trial_registration_column_2","trial_registration_column_3",
            "trial_registration_column_4","trial_registration_column_5","trial_registration_column_6",
            "trial_registration_column_7","trial_registration_column_8",
            "trial_registration_column_9","trial_registration_column_10");
        
        $index = array_search($trial_column_name, $column_array);
        
        $column_string = '';
        for($i=$index; $i<10; $i++){
            if($i!=9){
                $j=$i+1;
                $column_string .= "$column_array[$i] = $column_array[$j] , ";
            }else{
                $column_string .= "$column_array[$i] = ''";                
            }
        }
 
        $update_trial = sprintf("UPDATE `trial` SET $column_string WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
        $result = mysqli_query($this->db, $update_trial);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {

            $update_trial_registration = sprintf("UPDATE `trial_registration` SET $column_string WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
            $result = mysqli_query($this->db, $update_trial_registration);

            //Print Company details after login success
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial_registration");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
               $this->getTrialProgramDetails($company_id,$trial_id ,0); 
            }
        }
    }
    
     public function trialRegistrationSortingDetailsInDB($company_id, $trial_id, $old_location_id, $new_location_id, $list_type) {
        
        $return_trial_id=$trial_old_location_value=$trial_old_location_mandatory_value=$trial_reg_old_location_value=$column_string=$column_mandatory_string=$index="";
        
         $column_array = array("trial_registration_column_1","trial_registration_column_2","trial_registration_column_3",
           "trial_registration_column_4","trial_registration_column_5","trial_registration_column_6",
           "trial_registration_column_7","trial_registration_column_8",
           "trial_registration_column_9","trial_registration_column_10");
       
       $column_mandatory_array = array("trial_registration_column_1_flag","trial_registration_column_2_flag","trial_registration_column_3_flag",
           "trial_registration_column_4_flag","trial_registration_column_5_flag","trial_registration_column_6_flag",
           "trial_registration_column_7_flag","trial_registration_column_8_flag",
           "trial_registration_column_9_flag","trial_registration_column_10_flag");
        
        $old_location_id -= 1;
        $new_location_id -= 1;
        
        $get_trial_column = sprintf("select $column_array[$old_location_id],$column_mandatory_array[$old_location_id] from `trial` where `trial_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $company_id));
        $get_trial_result = mysqli_query($this->db, $get_trial_column);
        if (!$get_trial_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_trial_column");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($get_trial_result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($get_trial_result);
                $trial_old_location_value = $row[$column_array[$old_location_id]];
                $trial_old_location_mandatory_value = $row[$column_mandatory_array[$old_location_id]];
            }else{
                $error = array('status' => "Failed", "msg" => "trial details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }
        
        $get_trial_reg_column = sprintf("select $column_array[$old_location_id] from `trial_registration` where `trial_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $company_id));
        $get_trial_reg_result = mysqli_query($this->db, $get_trial_reg_column);
        if (!$get_trial_reg_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_trial_reg_column");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($get_trial_reg_result);
            if ($num_rows1 > 0) {
                $row = mysqli_fetch_assoc($get_trial_reg_result);
                $trial_reg_old_location_value = $row[$column_array[$old_location_id]];
            } 
    }
        
        if ($old_location_id < $new_location_id) {
            for ($i = $old_location_id; $i < $new_location_id; $i++) {               
                $j = $i + 1;
                $column_string .= "$column_array[$i] = $column_array[$j] , ";
                $column_mandatory_string .= "$column_mandatory_array[$i] = $column_mandatory_array[$j] , ";
            }
        } else {          
            for ($i = $old_location_id; $i > $new_location_id; $i--) {  
                $j = $i - 1;
                $column_string .= "$column_array[$i] = $column_array[$j] , "; 
                $column_mandatory_string .= "$column_mandatory_array[$i] = $column_mandatory_array[$j] , ";
            }
        }

        $update_trial = sprintf("UPDATE `trial` SET $column_string $column_mandatory_string $column_array[$new_location_id]='%s', $column_mandatory_array[$new_location_id]='%s' WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db, $trial_old_location_value), mysqli_real_escape_string($this->db, $trial_old_location_mandatory_value), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
        $result = mysqli_query($this->db, $update_trial);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {

            $update_trial_registration = sprintf("UPDATE `trial_registration` SET $column_string $column_array[$new_location_id]='%s' WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_old_location_value), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
            $result = mysqli_query($this->db, $update_trial_registration);

            //Print Company details after login success
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial_registration");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
               
              $this->getTrialProgramDetails($company_id,$trial_id ,0); 
            }
        }
    }
    
     public function trialLeadsourceSortingDetailsInDB($company_id, $trial_id, $old_location_id, $new_location_id, $list_type) {
        
        $return_trial_id=$trial_old_location_value=$trial_old_location_mandatory_value=$trial_reg_old_location_value=$column_string=$column_mandatory_string=$index="";
        
         $column_array = array("trial_lead_source_1","trial_lead_source_2","trial_lead_source_3",
            "trial_lead_source_4","trial_lead_source_5","trial_lead_source_6",
            "trial_lead_source_7","trial_lead_source_8",
            "trial_lead_source_9","trial_lead_source_10");
       
       
        
        $old_location_id -= 1;
        $new_location_id -= 1;
        
        $get_trial_column = sprintf("select $column_array[$old_location_id] from `trial` where `trial_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $company_id));
        $get_trial_result = mysqli_query($this->db, $get_trial_column);
        if (!$get_trial_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_trial_column");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($get_trial_result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($get_trial_result);
                $trial_old_location_value = $row[$column_array[$old_location_id]];
            }else{
                $error = array('status' => "Failed", "msg" => "trial details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }
     if ($old_location_id < $new_location_id) {
            for ($i = $old_location_id; $i < $new_location_id; $i++) {               
                $j = $i + 1;
                $column_string .= "$column_array[$i] = $column_array[$j] , ";
            }
        } else {          
            for ($i = $old_location_id; $i > $new_location_id; $i--) {  
                $j = $i - 1;
                $column_string .= "$column_array[$i] = $column_array[$j] , "; 
           }
        }

        $update_trial = sprintf("UPDATE `trial` SET $column_string  $column_array[$new_location_id]='%s'  WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db, $trial_old_location_value),  mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
        $result = mysqli_query($this->db, $update_trial);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getTrialProgramDetails($company_id,$trial_id ,0); 
        }
    }
    
    public function getTrialDiscountDetails($company_id, $trial_id){
        $sql = sprintf("SELECT `trial_discount_id` , `discount_type` , `discount_code` , `discount_amount` FROM `trial_discount`
                WHERE `company_id` = '%s' AND `trial_id`='%s' AND `is_expired`!='Y'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id));
        $result = mysqli_query($this->db, $sql);
        $output = $response = [];
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $output[] = $row;
                }
                $response['status'] = "Success";
            }else{
                $response['status'] = "Failed";
            }
        }
        $response['discount'] = $output;
        return $response;
    }
    
 public function updateTrialLeadSourceDetailsInDB($company_id, $trial_id, $trial_leadsource_name, $trial_leadsource_value,  $list_type){

//        $return_trial_id="";
        $update_trial = sprintf("UPDATE `trial` SET $trial_leadsource_name='%s' WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db,$trial_leadsource_value), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id));
        $result = mysqli_query($this->db, $update_trial);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
           $this->getTrialProgramDetails($company_id,$trial_id ,0); 
        }
    }
    
    public function deleteTrialLeadSourceDetailsInDB($company_id, $trial_id, $trial_leadsource_name, $list_type) {
        
        $column_array = array("trial_lead_source_1","trial_lead_source_2","trial_lead_source_3",
            "trial_lead_source_4","trial_lead_source_5","trial_lead_source_6",
            "trial_lead_source_7","trial_lead_source_8",
            "trial_lead_source_9","trial_lead_source_10");
        
        $index = array_search($trial_leadsource_name, $column_array);
        
        $column_string = '';
        for($i=$index; $i<10; $i++){
            if($i!=9){
                $j=$i+1;
                $column_string .= "$column_array[$i] = $column_array[$j] , ";
            }else{
                $column_string .= "$column_array[$i] = ''";                
            }
        }
 
        $update_trial = sprintf("UPDATE `trial` SET $column_string WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
        $result = mysqli_query($this->db, $update_trial);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
             $this->getTrialProgramDetails($company_id,$trial_id ,0); 

        }
    }
    
     public function updateTrialWaiverDetails($company_id, $trial_id, $waiver_policies) {
        
        $query1 = sprintf("SELECT * FROM `trial` WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
        $result1 = mysqli_query($this->db, $query1);

        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result1);
            if ($num_of_rows == 0) {
                $error = array('status' => "Failed", "msg" => "Trial details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }
        
        $update_waiver = sprintf("UPDATE `trial` SET `trial_waiver_policies`='%s' WHERE `company_id`='%s' AND `trial_id`='%s'", 
                mysqli_real_escape_string($this->db, $waiver_policies), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
        $result = mysqli_query($this->db, $update_waiver);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_waiver");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getTrialProgramDetails($company_id,$trial_id ,0); 
        }
    }
    
     public function inserttrialdiscount($company_id, $trial_id, $discount_type, $discount_code, $discount_amount, $list_type){
       $return_trial_id="";
        $query = sprintf("SELECT * FROM `trial_discount` WHERE `company_id`='%s' AND `trial_id`='%s' AND `discount_code`='%s' AND `is_expired`='N'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id), mysqli_real_escape_string($this->db,$discount_code));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($result);
            if($num_of_rows>0){
                $error = array('status' => "Failed", "msg" => "Discount Code already available for this trial program.");
                $this->response($this->json($error), 200);
            }
        }
        $sql1 = sprintf("INSERT INTO `trial_discount` (`company_id`, `trial_id`, `discount_type`, `discount_code`, `discount_amount`) VALUES ('%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id), mysqli_real_escape_string($this->db,$discount_type), mysqli_real_escape_string($this->db,$discount_code), mysqli_real_escape_string($this->db,$discount_amount));      
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getTrialProgramDetails($company_id,$trial_id ,0);             
        }
    }
    
      public function updatetrialDiscountInDB($company_id,$trial_id,$trial_discount_id,$discount_type,$discount_code,$discount_amount, $list_type) {
//    $return_trial_id="";
        $query = sprintf("SELECT * FROM `trial_discount` WHERE `company_id`='%s' AND `trial_id`='%s' AND `discount_code`='%s' AND `trial_discount_id`!='%s' AND `is_expired`='N'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id), mysqli_real_escape_string($this->db,$discount_code), mysqli_real_escape_string($this->db,$trial_discount_id));
        $query_result = mysqli_query($this->db, $query);
        if(!$query_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($query_result);
            if($num_of_rows>0){
                $error = array('status' => "Failed", "msg" => "Discount Code already available for this trial program.");
                $this->response($this->json($error), 200);
            }
        }
        $update_company = sprintf("UPDATE `trial_discount` SET `discount_type`='%s',`discount_code`='%s',`discount_amount`='%s' WHERE `company_id`='%s' AND `trial_id`='%s' AND `trial_discount_id`='%s'", mysqli_real_escape_string($this->db,$discount_type), mysqli_real_escape_string($this->db,$discount_code), mysqli_real_escape_string($this->db,$discount_amount), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id), mysqli_real_escape_string($this->db,$trial_discount_id));
        $result = mysqli_query($this->db, $update_company);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getTrialProgramDetails($company_id,$trial_id ,0);  
        }
    }
    
      public function deletetrialDiscountInDB($company_id, $trial_id, $trial_discount_id, $list_type){

       $update_company = sprintf("UPDATE `trial_discount` SET `is_expired`='Y' WHERE `company_id`='%s' AND `trial_id`='%s' AND `trial_discount_id`='%s'", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id), mysqli_real_escape_string($this->db,$trial_discount_id));
        $result = mysqli_query($this->db, $update_company);

        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
         $this->getTrialProgramDetails($company_id,$trial_id ,0); 
        }
    }  
    
      public function  updatetrialDetails($trial_id, $company_id, $trial_status, $trial_title, $trial_subtitle,  $trial_banner_img_content, $trial_welcome_url, $trial_description,$old_trial_banner_img_url,$trial_banner_img_url,$trial_image_update){
        $sort_order_value = '';
//        $new_list_type = $list_type;
        if($trial_status=='P'){
            $new_list_type = "live";
        }elseif($trial_status=='S'){
            $new_list_type = "draft";
        }
//        exit();
        if($trial_status=='P'){
            $sort_order_value = ", `trial_sort_order` = NextVal('trial_sort_order_seq')";
        }
        if ($trial_image_update === 'Y') {
            $update_trial = sprintf("UPDATE `trial` SET `trial_title`='%s',  `trial_subtitle`='%s', `trial_desc`='%s', `trial_welcome_url`='%s', `trial_banner_img_url`='%s', `trial_status`='%s' $sort_order_value WHERE `trial_id`='%s' and `company_id`='%s'",mysqli_real_escape_string($this->db,$trial_title), mysqli_real_escape_string($this->db,$trial_subtitle), mysqli_real_escape_string($this->db,$trial_description), mysqli_real_escape_string($this->db,$trial_welcome_url), mysqli_real_escape_string($this->db,$trial_banner_img_url), mysqli_real_escape_string($this->db,$trial_status), mysqli_real_escape_string($this->db,$trial_id), mysqli_real_escape_string($this->db,$company_id));
        } else {
            $update_trial = sprintf("UPDATE `trial` SET `trial_title`='%s',  `trial_subtitle`='%s', `trial_desc`='%s', `trial_welcome_url`='%s', `trial_status`='%s' $sort_order_value WHERE `trial_id`='%s' and `company_id`='%s'",  mysqli_real_escape_string($this->db,$trial_title), mysqli_real_escape_string($this->db,$trial_subtitle), mysqli_real_escape_string($this->db,$trial_description), mysqli_real_escape_string($this->db,$trial_welcome_url),  mysqli_real_escape_string($this->db,$trial_status), mysqli_real_escape_string($this->db,$trial_id), mysqli_real_escape_string($this->db,$company_id));
        }
        $result = mysqli_query($this->db, $update_trial);
        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if($trial_image_update==='Y' && !empty($old_trial_banner_img_url) && strpos($old_trial_banner_img_url, 'default.png') === false){
                $cfolder_name = "Company_$company_id";
                $folder_name = "Trial";
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/$cfolder_name/$folder_name";
                chdir($dir_name);
                $temp_old_img_url = stripslashes($old_trial_banner_img_url);
                $split = explode("$cfolder_name/$folder_name/",$temp_old_img_url);
                $filename = $split[1];                
                if(file_exists($filename)){
                    unlink($filename);
                }
                chdir($old); // Restore the old working directory
            }
           
          $this->getTrialProgramDetails($company_id,$trial_id ,0); 
        }
    }
    
    //Get trial details by Company ID
    public function getTrialDetailByCompany($company_id, $list_type, $call_back) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        
        $trial_count['P'] = $trial_count['S'] = 0;
        
        $sql_count = sprintf("SELECT COUNT(*) trial_count, trial_status FROM  `trial` WHERE `company_id`='%s' AND `trial_status`!='D' GROUP BY trial_status", mysqli_real_escape_string($this->db,$company_id));
        $result_count = mysqli_query($this->db, $sql_count);
        if(!$result_count){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_count");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result_count);
            if($num_rows>0){
                while($row_count = mysqli_fetch_assoc($result_count)){
                    if($row_count['trial_status']=='P'){
                        $trial_count['P'] = $row_count['trial_count'];
                    }elseif($row_count['trial_status']=='S'){
                        $trial_count['S'] += $row_count['trial_count'];
                    }elseif($row_count['trial_status']=='U'){
                        $trial_count['S'] += $row_count['trial_count'];
                    }
                }
            }
        }
        
        if($list_type=='P'){
            $status = "'P'";
        }else{
            $status = "'S','U'";
        }
        $trial_list_url_init = 0;
        $trial_list_url = $company_code = '';
        $sql = sprintf("SELECT `trial_id`, t.`company_id`, `trial_status`, `trial_title`, `trial_subtitle`, `trial_desc`, CONCAT(`trial_prog_url`,'//',UNIX_TIMESTAMP()) trial_prog_url, `trial_welcome_url`, `trial_banner_img_url`, `trial_banner_img_content`, `trial_waiver_policies`,`registrations_count` registration_count, `net_sales`, `processing_fee_type`, 
            `price_amount`, `program_length_type`, `program_length`, `trial_sort_order`,  `trial_registration_column_1`, `trial_registration_column_1_flag`, `trial_registration_column_2`, `trial_registration_column_2_flag`, `trial_registration_column_3`, `trial_registration_column_3_flag`, `trial_registration_column_4`, 
            `trial_registration_column_4_flag`, `trial_registration_column_5`, `trial_registration_column_5_flag`, `trial_registration_column_6`, `trial_registration_column_6_flag`, `trial_registration_column_7`, `trial_registration_column_7_flag`, `trial_registration_column_8`, `trial_registration_column_8_flag`, 
            `trial_registration_column_9`, `trial_registration_column_9_flag`, `trial_registration_column_10`, `trial_registration_column_10_flag`,`trial_lead_source_1`,  `trial_lead_source_2`,  `trial_lead_source_3`, 
             `trial_lead_source_4`, `trial_lead_source_5`,  `trial_lead_source_6`,  `trial_lead_source_7`, `trial_lead_source_8`, 
            `trial_lead_source_9`,  `trial_lead_source_10`, c.`company_code`,IF(t.`trial_status`='P','published',IF(t.`trial_status`='S','draft','unpublished')) list_type
        FROM `trial` t left join company c on t.`company_id`=c.`company_id` where c.`company_id`='%s' and `trial_status` IN ($status) ORDER BY `trial_sort_order` desc",  mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 1) {
                return $error;
            } else {
                $this->response($this->json($error), 200);
            }
        } else { 
            $output =  $response['live'] = $response['draft'] = $response['unpublished'] = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    if($trial_list_url_init==0){
                        $company_code = $row['company_code'];
                        $trial_list_url_init++;
                        $trial_list_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/t/?=$company_code/$company_id".'///'.time();
                    }
                    $disc = $this->getTrialDiscountDetails($company_id, $row['trial_id']);
                    $row['discount'] = $disc['discount'];
                    $reg_field_name_array = [];
                    $temp_reg_field = "trial_registration_column_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_field = $temp_reg_field."$z";      //for filed names
                        $reg_field_mandatory = "trial_registration_column_".$z."_flag";
                        if($row[$reg_field]!=''){
                            $reg_field_name_array[] = array("reg_col_name"=>$row[$reg_field],"reg_col_mandatory"=>$row[$reg_field_mandatory],"reg_col_index"=>$y);
                        }
                    }
                    $row['reg_columns'] = $reg_field_name_array;
                    $lead_name_array = [];
                    $temp_lead_field = "trial_lead_source_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_lead = $temp_lead_field."$z";      //for filed names
                        if($row[$reg_lead]!=''){
                            $lead_name_array[] = array("lead_col_name"=>$row[$reg_lead],"lead_col_index"=>$y);
                        }
                    } 
                    $row['lead_columns'] = $lead_name_array;
                    
                    $output[] = $row;
                }
                
                for ($i = 0; $i < count($output); $i++) {//split-up by live, past & draft
                    if ($output[$i]['list_type'] == 'published') {
                        $response['live'][] = $output[$i];
                    } elseif ($output[$i]['list_type'] == 'draft' || $output[$i]['list_type'] == 'unpublished') {
                        $response['draft'][] = $output[$i];
                    } 
//                    elseif ($output[$i]['list_type'] == 'unpublished') {
//                        $response['unpublished'][] = $output[$i];
//                    }
                }
                usort($response['live'], function($a, $b) {
                    return $a['trial_sort_order'] < $b['trial_sort_order'];
                });
                $out = array('status' => "Success", 'msg' => $response, 'trial_list_url' => $trial_list_url, "count" => $trial_count);
                // If success everythig is good send header as "OK" and user details
                if ($call_back == 1) {
                    return $out;
                } else {
                    $this->response($this->json($out), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Trial details doesn't exist.", "count" => $trial_count);
                if ($call_back == 1) {
                    return $error;
                } else {
                    $this->response($this->json($error), 200);
                } // If no records "No Content" status
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
      public function deleteTrial($company_id, $trial_id, $list_type){
        $check_trial_registrations = sprintf("SELECT * FROM `trial_registration` WHERE `trial_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $trial_id),mysqli_real_escape_string($this->db, $company_id));
        $result_trial_reg = mysqli_query($this->db, $check_trial_registrations);
        if(!$result_trial_reg){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_trial_registrations");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($result_trial_reg);
            if($num_of_rows>0){
                $error = array('status' => "Failed", "msg" => "This trial program cannot be canceled since there are registered users.");
                $this->response($this->json($error), 200);
            }
        }
        
        $get_trial = sprintf("select `trial_id` from `trial` where `trial_id`='%s' AND `company_id`='%s'",  mysqli_real_escape_string($this->db, $trial_id),  mysqli_real_escape_string($this->db, $company_id));
        $get_result = mysqli_query($this->db, $get_trial);
        if(!$get_result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_trial");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($get_result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($get_result);
                $trial_id = $row['trial_id'];
                
                $update_trial = sprintf("Update `trial` SET `trial_status`='D' WHERE `trial_id`='%s' and `company_id`='%s'", mysqli_real_escape_string($this->db, $trial_id),  mysqli_real_escape_string($this->db, $company_id));
                $result = mysqli_query($this->db, $update_trial);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                     $out = array('status' => "Success", 'msg' => "Trial program Deleted Successfully.");
                        // If success everythig is good send header as "OK" and user details
                        $this->response($this->json($out), 200);
                }
            }else{
                $out = array('status' => "Success", 'msg' => "No Trial program found to delete.");
                    // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            }
        }
    }
    
     public function gettrialParticipantDetails($company_id, $trial_id, $trial_status_tab, $search, $draw_table, $length_table, $start, $sorting) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

//        $end_limit=500;
        $sort = $sorting[0]['column'];
        $sort_type = $sorting[0]['dir'];
        $search_value = $search['value'];
        $out = $output = [];
        $out['active'] = $out['enrolled'] = $out['didnotstart'] = $out['cancelled'] = $out['recordsTotal']= $out['recordsFiltered'] = 0;
        $date_format = "'%b %d, %Y'";
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $last_updt_date = " DATE(CONVERT_TZ(tr.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
//         $end_date = " DATE(CONVERT_TZ(tr.end_date,$tzadd_add,'$new_timezone'))";
//          $start_date = " DATE(CONVERT_TZ(tr.start_date,$tzadd_add,'$new_timezone'))";


        if (!empty($search_value)) {
            $s_text = " and ( buyer_name like  '%$search_value%'  or buyer_email like  '%$search_value%' or `buyer_phone` like  '%$search_value%' or  tr.`trial_status` like '%$search_value%'
                    or CONCAT(tr.`trial_registration_column_2`, ', ', tr.`trial_registration_column_1`) like  '%$search_value%' or   DATE_FORMAT(tr.`start_date`, '%b %d, %Y')  like  '%$search_value%' 
                     or  DATE_FORMAT(tr.`end_date`, '%b %d, %Y')  like  '%$search_value%' or  DATE_FORMAT(tr.`registration_date`, '%b %d, %Y')  like  '%$search_value%' or  t.`trial_title` like  '%$search_value%'  or  tr.`trial_registration_column_4` like  '%$search_value%')";
        } else {
            $s_text = '';
        }

      $column = array(2 => "buyer_name", 3 => "participant_name", 4 => "age", 5 => "participant_phone", 6 => "participant_email", 7 => "trial_status", 8 => "date(registration_date)", 9 => "trial_program", 10 => "date(start_date)", 11 => "date(end_date)", 12 => "leadsource");

        if ($trial_status_tab == 'A') { //ORDERS TAB
            $trial_tab = "tr.`trial_status` in ('A')";
        } else if ($trial_status_tab == 'E') {
            $trial_tab = "tr.`trial_status` in ('E')";
        } else if ($trial_status_tab == 'D') {
            $trial_tab = "tr.`trial_status` in ('D')";
        } else if ($trial_status_tab == 'C') {
            $trial_tab = "tr.`trial_status` in ('C')";
        }

        if (!empty($search_value)) {
            $query1 = sprintf("SELECT tr.`trial_reg_id` as id, tr.`company_id`, tr.`trial_id`, tr.`student_id`, `buyer_name`, CONCAT(tr.`trial_registration_column_1`, ' ', tr.`trial_registration_column_2`) participant_name,
                IF(trial_reg_type_user='P', 'Control Panel', IF(trial_reg_type_user='M', 'Mobile App', IF(trial_reg_type_user='U', 'Website URL', ''))) trial_reg_type_user,
                `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(tr.`registration_date`,%s) registration_date, `payment_type`, tr.`payment_amount`,
                tr.`paid_amount` paid_amount, DATE_FORMAT(($last_updt_date),%s) last_updt_dt, if(`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, 
                IF(tr.`trial_status`='A','Active',IF(tr.`trial_status`='E','Enrolled',IF(tr.`trial_status`='C','Cancelled','Didnotstart'))) trial_status, tr.`trial_registration_column_4` leadsource, DATE_FORMAT(tr.`start_date`,%s) start_date, DATE_FORMAT(tr.`end_date`,%s) end_date,t.`trial_title` trial_program
                FROM `trial_registration` tr left join `trial` t on tr.`company_id`=t.`company_id` AND tr.`trial_id`=t.`trial_id` 
                LEFT JOIN student s1 on tr.`student_id` = s1.student_id and tr.company_id = s1.company_id
                LEFT JOIN student s2 on tr.`buyer_email` = s2.student_email and tr.company_id = s2.company_id
                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email`  AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                WHERE tr.`company_id`='%s' AND tr.`trial_id` ='%s'  %s AND $trial_tab ", $date_format, $date_format, $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), $s_text);
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $numsql_rows1 = mysqli_num_rows($result1);
                if ($numsql_rows1 > 0) {
                    $out['recordsFiltered'] = $numsql_rows1;
                }
            }
        }
        $query = sprintf("SELECT tr.`trial_reg_id` as id, tr.`company_id`, tr.`trial_id`, tr.`student_id`, `buyer_name`, CONCAT(tr.`trial_registration_column_2`, ', ', tr.`trial_registration_column_1`) participant_name,
                IF(trial_reg_type_user='P', 'Control Panel', IF(trial_reg_type_user='M', 'Mobile App', IF(trial_reg_type_user='U', 'Website URL', ''))) trial_reg_type_user,
                `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(tr.`registration_date`,%s) registration_date, `payment_type`, tr.`payment_amount`,
                tr.`paid_amount` paid_amount, DATE_FORMAT(($last_updt_date),%s) last_updt_dt, if(`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, 
                IF(tr.`trial_status`='A','Active',IF(tr.`trial_status`='E','Enrolled',IF(tr.`trial_status`='C','Cancelled','Didnotstart'))) trial_status, tr.`trial_registration_column_4` leadsource, IF(tr.`trial_registration_column_3` = '0000-00-00'
                    OR tr.`trial_registration_column_3` IS NULL,
                '',TRUNCATE(DATEDIFF($currdate_add,tr.`trial_registration_column_3`) / 365.25,0)) age, DATE_FORMAT(tr.`start_date`,%s) start_date, DATE_FORMAT(tr.`end_date`,%s) end_date,t.`trial_title` trial_program
                FROM `trial_registration` tr left join `trial` t on tr.`company_id`=t.`company_id` AND tr.`trial_id`=t.`trial_id` 
                LEFT JOIN student s1 on tr.`student_id` = s1.student_id and tr.company_id = s1.company_id
                LEFT JOIN student s2 on tr.`buyer_email` = s2.student_email and tr.company_id = s2.company_id
                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email`  AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                WHERE tr.`company_id`='%s' AND tr.`trial_id` ='%s'  %s AND $trial_tab ORDER BY $column[$sort] $sort_type LIMIT $start, $length_table ", $date_format, $date_format, $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), $s_text);
        $result = mysqli_query($this->db, $query);
//        log_info("ravi trial   ".$query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            $sql = sprintf("SELECT count(*) status_count, trial_status from `trial_registration` where trial_id='%s' group by trial_status", mysqli_real_escape_string($this->db, $trial_id));
            $result_sql = mysqli_query($this->db, $sql);
            if (!$result_sql) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $numsql_rows = mysqli_num_rows($result_sql);
                if ($numsql_rows > 0) {
                    while ($rows1 = mysqli_fetch_assoc($result_sql)) {
                        $trial_status = $rows1['trial_status'];
                        $status_count = $rows1['status_count'];
                        if ($trial_status == 'A') {
                            $out['active'] = $status_count;
                        } elseif ($trial_status == 'E') {
                            $out['enrolled'] = $status_count;
                        } elseif ($trial_status == 'D') {
                            $out['didnotstart'] = $status_count;
                        } elseif ($trial_status == 'C') {
                            $out['cancelled'] = $status_count;
                        }
                    }
                    if ($trial_status_tab == 'A') {
                           $out['recordsTotal'] =   $out['active'];
                           if (empty($search_value)) {
                            $out['recordsFiltered'] = $out['active'];
                        }
                    } else if ($trial_status_tab == 'E') {
                           $out['recordsTotal'] = $out['enrolled'];
                             if (empty($search_value)) {
                            $out['recordsFiltered'] = $out['enrolled'];
                        }
                    } else if ($trial_status_tab == 'D') {
                           $out['recordsTotal'] = $out['didnotstart'];
                             if (empty($search_value)) {
                            $out['recordsFiltered'] = $out['didnotstart'];
                        }
                    } else if ($trial_status_tab == 'C') {
                           $out['recordsTotal'] = $out['cancelled'];
                           if (empty($search_value)) {
                            $out['recordsFiltered'] = $out['cancelled'];
                        }
                    }
                }
            }
            if ($num_rows > 0) {
//                $output['recent'] = $output['cancelled'] = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    if ($trial_status_tab == 'A') {
                        $output[] = $row;
                    } else if ($trial_status_tab == 'E') {
                        $output[] = $row;
                    } else if ($trial_status_tab == 'D') {
                        $output[] = $row;
                    } else if ($trial_status_tab == 'C') {
                        $output[] = $row;
                    }
                }
                $out['draw'] = $draw_table;
                $out['data'] = $output;
//                if (!empty($search_value)) {
//                    $out['recordsFiltered'] = $num_rows;
//                }
//                $res = array("status" => "Success", "msg" => $output, "count" => $count);
                $this->response($this->json($out), 200);
            } else {
                $out['draw'] = $draw_table;
                $out['data'] = $output;
//                $error = array('status' => "Failed", "msg" => "No participants registered yet.", "count" => $count);
                $this->response($this->json($out), 200); // If no records "No Content" status
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function trialProgramCheckout($company_id, $trial_id, $actual_student_id, $actual_participant_id, $trial_name, $desc,  $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount,  $processing_fee_type, $discount,  $payment_type, $upgrade_status,  $reg_type_user, $reg_version_user,$program_length,$program_length_type,$start_date,$participant_street, $participant_city, $participant_state, $participant_zip, $participant_country){
        $this->getStudioSubscriptionStatus($company_id);
        $this->addTrialDimensions($company_id, $trial_id,'');
        $buyer_name = $buyer_first_name.' '.$buyer_last_name;  
        if (is_null($actual_student_id) || $actual_student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, '', '');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $actual_student_id;
        }
        if (is_null($actual_participant_id) || $actual_participant_id == 0) {
            $participant_id = $this->getParticipantId($company_id, $student_id, $reg_col1, $reg_col2, $reg_col3);
        } else {
            $participant_id = $actual_participant_id;
        }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        
        $pr_fee = $temp_pr_fee = 0;
        $current_date = date("Y-m-d");
        if($program_length_type == 'D'){
            $end_date = date("Y-m-d", strtotime("+$program_length days",strtotime($start_date)));
        }else{
            $end_date = date("Y-m-d", strtotime("+$program_length week",strtotime($start_date)));
        }
        
        
//        $check_deposit_failure = 0;
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg1=array("buyer_email"=>$buyer_email,"membership_title"=>$trial_name,"gmt_date"=>$gmt_date);
        $p_f = $this->getProcessingFee($company_id, $upgrade_status);
        $process_fee_per = $p_f['PP'];
        $process_fee_val = $p_f['PT'];
        
        
       if($payment_amount > 0){
           $payment_type = 'O';
           $processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
       }else{
           $payment_type = 'F';
           $processing_fee = 0;
       }
        
       
        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency` FROM `wp_account` WHERE `company_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                if($user_state=='deleted'){
                    $msg = "Wepay user registered was deleted.";
                    $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
                    $this->response($this->json($log),200);
                }
                
                if($acc_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log),200);
                }elseif($acc_state=='disabled'){
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log),200);
                }
                
                if(!empty($access_token) && !empty($account_id)){
                    if (!function_exists('getallheaders')){
                        if(!function_exists ('apache_request_headers')){
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        }else{
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    }else{
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        
                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                    
                    $w_paid_amount = $paid_amount = $paid_fee = $gross = 0;
                    $checkout_id = $checkout_state = '';                    
                    $cc_name = $cc_month = $cc_year = '';
                    $cc_status = $cc_state;
                    
                    if($payment_amount>0){
                        $checkout_id_flag = 1;
                        $rand = mt_rand(1,99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout?for=trial";
                        $unique_id = $company_id."_".$trial_id."_".$ip."_".$rand."_".time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id."_".$trial_id."_".$date;
//                        if($processing_fee_type==2){
//                            $fee_payer = "payer_from_app";
//                        }elseif($processing_fee_type==1){
                            $fee_payer = "payee_from_app";
//                        }
                        $paid_amount = $payment_amount;
                        $paid_fee = $processing_fee;
                        
                        if($processing_fee_type==2){
                            $w_paid_amount = $payment_amount + $processing_fee;
                        }elseif($processing_fee_type==1){
                            $w_paid_amount = $payment_amount;
                        }
                        
                        
                        $time = time();
                        $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                            "fee"=>array("app_fee"=>$paid_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                            "email_message"=>array("to_payee"=>"Payment has been added for $trial_name","to_payer"=>"Payment has been made for $trial_name"),
                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                            "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code","country"=>"$country")))),
                            "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                "properties"=>array("itemized_receipt"=>array(array("description"=>"$trial_name", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                        $postData2 = json_encode($json2);
                        $sns_msg1['call'] = "Checkout";
                        $sns_msg1['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                        $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $gross = $res2['gross'];
//                            $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                        }else{
                            if($response2['msg']['error_code']==1008){
                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                                $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                                if($response2['status']=="Success"){
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $gross = $res2['gross'];
//                                    $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                                }else{
                                    if($response2['msg']['error_code']==1008){
                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error),200);
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error),200);
                            }
                        }
                    }else{
                        $checkout_id_flag = 0;
                        if(!empty(trim($cc_id))){
                            $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                            $postData3 = json_encode($json3);
                            $sns_msg1['call'] = "Credit Card";
                            $sns_msg1['type'] = "Credit card details";
                            $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                            $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                            if($response3['status']=="Success"){
                                $res3 = $response3['msg'];
                                if($res3['state']=='new'){
                                    $json2 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id,"account_id"=>$account_id);
                                    $postData2 = json_encode($json2);                                    
                                    $sns_msg1['call'] = "Credit Card";
                                    $sns_msg1['type'] = "Credit Card Authorization";
                                    $response2 = $this->wp->accessWepayApi("credit_card/authorize",'POST',$postData2,$token,$user_agent,$sns_msg1);
                                    $this->wp->log_info("wepay_credit_card_authorize: ".$response2['status']);
                                    if($response2['status']=="Success"){
                                        $res2 = $response2['msg'];
                                        $cc_name = $res2['credit_card_name'];
                                        $cc_new_state = $res2['state'];
                                        if($cc_state!=$cc_new_state){
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res2['expiration_month'];
                                        $cc_year = $res2['expiration_year'];
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }elseif($res3['state']=='authorized'){
                                    $cc_name = $res3['credit_card_name'];
                                    $cc_new_state = $res3['state'];
                                    $cc_month = $res3['expiration_month'];
                                    $cc_year = $res3['expiration_year'];
                                }else{
                                    if($res3['state']=='expired'){
                                        $msg = "Given credit card is expired.";
                                    }elseif($res3['state']=='deleted'){
                                        $msg = "Given credit card was deleted.";
                                    }else{
                                        $msg = "Given credit card details is invalid.";
                                    }
                                    $error = array("status" => "Failed", "msg" => $msg);
                                    $this->response($this->json($error),200);
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                $this->response($this->json($error),200);
                            }
                        }

                    }
                    
                    if(!empty(trim($cc_id))){
                        $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                        $postData3 = json_encode($json3);
                        $sns_msg1['call'] = "Credit Card";
                        $sns_msg1['type'] = "Credit Card details";
                        $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                        $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                        if($response3['status']=="Success"){
                            $res3 = $response3['msg'];
                            $cc_name = $res3['credit_card_name'];
                            $cc_new_state = $res3['state'];
                            if($cc_state!=$cc_new_state){
                                $cc_status = $cc_new_state;
                            }
                            $cc_month = $res3['expiration_month'];
                            $cc_year = $res3['expiration_year'];
                        }else{
                            for($k=0;$k<2;$k++){
                                if($response3['msg']['error_code'] == 503){
                                    sleep(2);
                                    $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                                    $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                                    if($response3['status']=="Success"){
                                        $res3 = $response3['msg'];
                                        $cc_name = $res3['credit_card_name'];
                                        $cc_new_state = $res3['state'];
                                        if($cc_state!=$cc_new_state){
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res3['expiration_month'];
                                        $cc_year = $res3['expiration_year'];
                                        break;
                                    }
                                    if($k==1){
                                        if($checkout_id_flag == 1){
                                            $type = $response3['msg']['error'];
                                            $description =  $response3['msg']['error_description']." Buyer Name:".$buyer_name." Buyer Email:".$buyer_email." Checkout Id:".$checkout_id." Participant Name:".$reg_col1." ".$reg_col2;      
                                            $insert_alertquery = sprintf("INSERT INTO `wepay_critical_alert`(`company_id`, `type`, `description`) VALUES ('%s','%s','%s')",mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $type),mysqli_real_escape_string($this->db, $description));
                                            $result_alert = mysqli_query($this->db, $insert_alertquery);
                                            if(!$result_alert){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_alertquery");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            } 
                                            $type1 = $response3['msg']['error']." Regarding Credit Card";
                                            $description1 = "Fetching Credit Card details failed"."\n"." Buyer Name:".$buyer_name."\n"." Buyer Email:".$buyer_email."\n"." Checkout Id:".$checkout_id."\n"." Participant Name:".$reg_col1." ".$reg_col2;     
                                            $this->sendSnsforfailedcheckout($type1,$description1);
                                        }
                                        $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }else{
                                    $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                    $this->response($this->json($error),200);
                                }
                            }
                        }
                    }
                    
                     if($processing_fee_type==1){
                                $paid_amount -= $processing_fee;
                            }
                           
                    $payment_method = 'CC';
                    $query = sprintf("INSERT INTO `trial_registration`(`company_id`, `trial_id`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                        `payment_type`, `payment_amount`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`,  `start_date`, `end_date`, `trial_status`, `discount`,
                        `trial_registration_column_1`, `trial_registration_column_2`, `trial_registration_column_3`, `trial_registration_column_4`, `trial_registration_column_5`, `trial_registration_column_6`, `trial_registration_column_7`, `trial_registration_column_8`, `trial_registration_column_9`, `trial_registration_column_10`,
                        `trial_reg_type_user`,`trial_reg_version_user`,`program_length`,`program_length_type`, `trial_participant_street`, `trial_participant_city`, `trial_participant_state`, `trial_participant_zip`, `trial_participant_country`, `payment_method`)
                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), 
                            mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db,$participant_id),mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code),
                            mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_type), 
                            mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name),
                            mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year),  mysqli_real_escape_string($this->db, $start_date),mysqli_real_escape_string($this->db, $end_date), mysqli_real_escape_string($this->db, 'A'), mysqli_real_escape_string($this->db, $discount),
                            mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4),
                            mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), 
                            mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $program_length),mysqli_real_escape_string($this->db, $program_length_type),mysqli_real_escape_string($this->db, $participant_street), mysqli_real_escape_string($this->db, $participant_city),
                            mysqli_real_escape_string($this->db, $participant_state), mysqli_real_escape_string($this->db, $participant_zip), mysqli_real_escape_string($this->db, $participant_country), mysqli_real_escape_string($this->db, $payment_method));
                    $result = mysqli_query($this->db, $query);
                    if(!$result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $trial_reg_id = mysqli_insert_id($this->db);

                        if ($payment_amount > 0) {
                            $pstatus = 'S';
                            $payment_query = sprintf("INSERT INTO `trial_payment`(`trial_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `payment_date`, `payment_status`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $pstatus), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name));
                            $payment_result = mysqli_query($this->db, $payment_query);
                            if (!$payment_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                                log_info($this->json($error_log));
                            }
                        }

                        $update_trial_query = sprintf("UPDATE `trial` SET `registrations_count` = `registrations_count`+1,`active_count`=`active_count`+1  WHERE `trial_id`='%s'", mysqli_real_escape_string($this->db, $trial_id));
                        $update_trial_result = mysqli_query($this->db, $update_trial_query);
                            if(!$update_trial_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                     
                        
                        $selectcurrency=sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'",  mysqli_escape_string($this->db, $company_id));
                        $resultselectcurrency=  mysqli_query($this->db, $selectcurrency);
                        if(!$resultselectcurrency){
                             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                            log_info($this->json($error_log));
                        }else{
                            $row = mysqli_fetch_assoc($resultselectcurrency);
                            $wp_currency_symbol=$row['wp_currency_symbol'];
                        }
                        $activity_text = 'Registration date.';
                        $activity_type="registration";
                    
                    if(!empty($discount)){
                        $activity_text .= " Trial fee discount value used $wp_currency_symbol".$discount;
                    }
                    
                    $curr_date = gmdate("Y-m-d H:i:s");
                    $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), $activity_type, 
                            mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                    $result_history = mysqli_query($this->db, $insert_history);
                    if (!$result_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                        
                    $this->updateTrialDimensionsMembers($company_id, $trial_id, 'A','',$trial_reg_id);
                        $trial_details = $this->getTrialProgramDetails($company_id, $trial_id, 1);
                        $msg = array("status" => "Success", "msg" => "Trial program registration was successful. Email confirmation sent.", "trial_details"=>$trial_details);
                        $this->responseWithWepay($this->json($msg),200);
                        $this->sendOrderReceiptForTrialPayment($company_id,$trial_reg_id,0);
                    }
                }
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error),200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
            } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
         }
    }
    
    public function getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $payment_processor, $studio_name) {
        $select_student = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$company_id));
        $result_select_student = mysqli_query($this->db, $select_student);
        if (!$result_select_student) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student);
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result_select_student) > 0) {
                $r = mysqli_fetch_assoc($result_select_student);
                $student_id = $r['student_id'];
                if(is_null($r['stripe_customer_id']) || empty(trim($r['stripe_customer_id']))){
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = $r['stripe_customer_id'];
                    }
                }else{
                    $stripe_customer_id = $r['stripe_customer_id'];
                }
            } else {
                $insert_student = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_lastname`, `student_email`) VALUES('%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), 
                        mysqli_real_escape_string($this->db,$buyer_first_name), mysqli_real_escape_string($this->db,$buyer_last_name), mysqli_real_escape_string($this->db,$buyer_email));
                $result_insert_student = mysqli_query($this->db, $insert_student);
                if (!$result_insert_student) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student);
                    $this->response($this->json($error), 200);
                } else {
                    $student_id = mysqli_insert_id($this->db);
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = '';
                    }
                }
            }
            return array("student_id" => $student_id, "stripe_customer_id" => $stripe_customer_id);
        }
    }
    
     public function getParticipantId($company_id, $student_id, $first_name, $last_name, $dob) {
       
        $select_participant2 = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name), mysqli_real_escape_string($this->db, $dob));
        $result_select_participant2 = mysqli_query($this->db, $select_participant2);
        if (!$result_select_participant2) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_participant2);
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result_select_participant2) > 0) {
                $r = mysqli_fetch_assoc($result_select_participant2);
                $participant_id = $r['participant_id'];
                $deleted_flag = $r['deleted_flag'];
                if($deleted_flag=='D'){
                    $update_participant = sprintf("UPDATE `participant` SET `deleted_flag`='N' WHERE `participant_id`='%s'", mysqli_real_escape_string($this->db, $participant_id));
                    $result_update_participant = mysqli_query($this->db, $update_participant);
                    if (!$result_update_participant) {
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_participant);
                        $this->response($this->json($error), 200);
                    }
                }
            } else {
                $insert_participant2 = sprintf("INSERT INTO `participant`(`company_id`, `student_id`, `participant_first_name`, `participant_last_name`, `date_of_birth`) VALUES('%s', '%s', '%s', '%s', '%s')",
                        $company_id, $student_id, mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name), $dob);
                $result_insert_participant2 = mysqli_query($this->db, $insert_participant2);
                if (!$result_insert_participant2) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_participant2);
                    $this->response($this->json($error), 200);
                } else {
                    $participant_id = mysqli_insert_id($this->db);
                }
            }
            return $participant_id;
        }
    }

        private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = (select if('%s'='S','S','W'))", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = (select if('%s'='S','S','W'))", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }
    
     private function calculateProcessingFee($amount, $fee_type, $process_fee_per, $process_fee_val) {
        $p1_fee = ($amount * ($process_fee_per / 100)) + $process_fee_val;
        $p2_fee = ($amount + $p1_fee) * ($process_fee_per / 100) + $process_fee_val;
        if ($fee_type === 2 || $fee_type === '2') {
            $p_fee = $p2_fee;
        } else {
            $p_fee = $p1_fee;
        }
        return $p_fee;
    }
    
     private function getRealIpAddr(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet        
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
     public function sendOrderReceiptForTrialPayment($company_id,$trial_reg_id, $return_value){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $created_date =" DATE(CONVERT_TZ(tp.created_dt,$tzadd_add,'$new_timezone'))";
       
        $waiver_policies = '';
        $query = sprintf("SELECT tr.`trial_status`, tr.`buyer_name`, tr.`buyer_email`, tr.`buyer_phone`, tr.`buyer_postal_code`,tr.`start_date`,tr.`end_date`,
                tr.`registration_date`, tr.`registration_amount`, tr.`processing_fee_type`, tr.`payment_type`, c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`,
                tr.`program_length`, tr.`program_length_type`, concat(tr.`trial_registration_column_2`,',',tr.`trial_registration_column_1`) participant,  tr.`trial_registration_column_4`
                lead_Source,t.`trial_title`,t.`trial_waiver_policies`,tp.`payment_date`,tp.`payment_amount`,tp.`processing_fee`,tp.`payment_status`,tp.`refunded_amount`,tp.`refunded_date`,if(tp.`payment_from`='S',tp.`stripe_card_name`,tp.`cc_name`) as cc_name,DATE($created_date) created_dt,tp.`last_updt_dt` FROM `trial_registration` tr 
                left join `company` c on(c.`company_id`=tr.`company_id`) left join `student` s on(tr.`student_id`=s.`student_id`) left join `trial` t
                on(t.`trial_id`=tr.`trial_id`) left join `trial_payment` tp on(tr.`trial_reg_id`=tp.`trial_reg_id`) 
                WHERE tr.`trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
            exit();
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
                $trial_name = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $student_cc_email_list = $wp_currency_code = $wp_currency_symbol =$processing_fee_type= '';
                $current_plan_details = $trial_details =  $payment_details = [];
                 while($row = mysqli_fetch_assoc($result)){
                    $paid_amt = 0;
                    $trial_name = $row['trial_title'];
                    $participant_name = $row['participant'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['trial_waiver_policies'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $payment_status = $row['payment_status'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $student_cc_email_list = $row['student_cc_email'];
                    $start_date  = date("M dS, Y", strtotime($row['start_date']));
                    $end_date  = date("M dS, Y", strtotime($row['end_date']));
                    $temp=[];
                    
                    if($row['processing_fee_type']==2){
                      $payment_amount  = $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                    }else{
                       $payment_amount = $temp['amount'] = $row['payment_amount'];
                    }
                    $temp['date'] = $row['payment_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
//                    $temp['edit_status'] = 'N';
                    $temp['payment_status'] = $row['payment_status'];
                    $temp['processing_fee'] = $row['processing_fee']; 
                    $temp['cc_name'] = $row['cc_name'];
                   
                   
                        $payment_details[] = $temp;
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                log_info($this->json($error_log));
                if($return_value==1){
                    $error = array('status' => "Failed", "msg" => "No records found.");
                    $this->response($this->json($error), 200);
                }
                exit();
            }
        }
        
        $subject = $trial_name." Order Confirmation";
        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
        $message .= "<b>Order Details</b><br><br>";
        $message .= "$trial_name"."<br>";
        $message .= "Starts: "."$start_date"."<br>";
        $message .= "Ends: "."$end_date"."<br>";
        $message .= "Participant: "."$participant_name"."<br><br>";
          
        if($payment_status == 'S' && $payment_details[0]['amount'] > 0){
                $message .= "<b>Billing Details</b><br><br>";
                $message .= "Total Due: "."$wp_currency_symbol$payment_amount"."<br>";
                  if(count($payment_details)>0){
                    $pf_str = "";
                    $pr_fee = $payment_details[0]['processing_fee'];
                    if($processing_fee_type==2){
                        $pf_str = " (includes $wp_currency_symbol".$pr_fee." administrative fees)";
                    }
                        $amount = $payment_details[0]['amount'];
                        $refunded_amount = $payment_details[0]['refunded_amount'];
                        $final_amt = $amount-$refunded_amount;
                        $payment_status = $payment_details[0]['payment_status'];
//                        $date = date("M dS, Y", strtotime($payment_details[0]['date']));
                        $paid_str = "Amount paid: ";
                        if($payment_status == 'S'){
                            $pay_type= "(".$payment_details[0]['cc_name'].")";
                        }else{
                            $pay_type="(manual credit applied)";
                        }
                        $message .= " $paid_str"."$wp_currency_symbol$payment_amount"."$pf_str$pay_type"." <br>";
                 }
            $message .= "<br><br>";
        }
        
        $waiver_present = 0;
        $file = '';
        if(!empty(trim($waiver_policies))){
            $waiver_present = 1;
            $file_name_initialize = 10;
            for($z=0; $z<$file_name_initialize; $z++){
                $dt = time();
                $file = "../../../uploads/".$dt."_waiver_policies.html";        
                if(file_exists($file)){
                    sleep(1);
                    $file_name_initialize++;
                }else{
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }

        $sendEmail_status = $this->sendEmailForTrial($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list,$student_cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            log_info($this->json($error_log));
            if($return_value==1){
                $log = array('status' => "Success", "msg" => "Trial order receipt sent successfully.");
                $this->response($this->json($log), 200);
            }
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Trial order receipt sent failed.");
                $this->response($this->json($error), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function copyTrial($company_id, $trial_id, $trial_template_flag, $list_type){
        $company_code = "";
        if($trial_template_flag == "Y"){
            $comp_id = "6";
        } else {
            $comp_id = $company_id;
        }
        $sql_company_code = sprintf("SELECT  `company_code` FROM  `company` WHERE  `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result_company_code = mysqli_query($this->db, $sql_company_code);
        if (!$result_company_code) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_company_code");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $output_value = mysqli_fetch_assoc($result_company_code);
            $company_code = $output_value['company_code'];
        }
        
        $sql = sprintf("SELECT `trial_status`, `trial_title`, `trial_subtitle`, `trial_desc`, `trial_prog_url`, `trial_welcome_url`, `trial_banner_img_url`, `trial_banner_img_content`, `trial_waiver_policies`, `registrations_count`, `net_sales`, `processing_fee_type`, `price_amount`, 
            `program_length_type`, `program_length`, `trial_sort_order`, `trial_registration_column_1`, `trial_registration_column_1_flag`, `trial_registration_column_2`, `trial_registration_column_2_flag`, `trial_registration_column_3`, `trial_registration_column_3_flag`, 
            `trial_registration_column_4`, `trial_registration_column_4_flag`, `trial_registration_column_5`, `trial_registration_column_5_flag`, `trial_registration_column_6`, `trial_registration_column_6_flag`, `trial_registration_column_7`, `trial_registration_column_7_flag`, `trial_registration_column_8`,
            `trial_registration_column_8_flag`, `trial_registration_column_9`, `trial_registration_column_9_flag`, `trial_registration_column_10`, `trial_registration_column_10_flag`, `trial_lead_source_1`, `trial_lead_source_2`, `trial_lead_source_3`, `trial_lead_source_4`, `trial_lead_source_5`,
            `trial_lead_source_6`, `trial_lead_source_7`, `trial_lead_source_8`, `trial_lead_source_9`, `trial_lead_source_10`, `created_dt`, `last_updt_dt` FROM `trial` WHERE `company_id`='%s' AND `trial_id`='%s'
             AND `trial_status`!='D'", mysqli_real_escape_string($this->db,$comp_id), mysqli_real_escape_string($this->db,$trial_id));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                   $output[] = $row;
                }
                
                        $sql1 = sprintf("INSERT INTO `trial`(`company_id`, `trial_status`, `trial_title`, `trial_subtitle`, `trial_desc`, 
                                `trial_prog_url`, `trial_welcome_url`, `trial_banner_img_content`, `trial_waiver_policies`, `processing_fee_type`, `price_amount`, `program_length_type`, `program_length`, `trial_registration_column_1`, `trial_registration_column_1_flag`, `trial_registration_column_2`, 
                                `trial_registration_column_2_flag`, `trial_registration_column_3`, `trial_registration_column_3_flag`, `trial_registration_column_4`, `trial_registration_column_4_flag`, `trial_registration_column_5`, `trial_registration_column_5_flag`, `trial_registration_column_6`, `trial_registration_column_6_flag`, `trial_registration_column_7`, `trial_registration_column_7_flag`, `trial_registration_column_8`, `trial_registration_column_8_flag`, 
                                `trial_registration_column_9`, `trial_registration_column_9_flag`, `trial_registration_column_10`, `trial_registration_column_10_flag`, `trial_lead_source_1`, `trial_lead_source_2`, `trial_lead_source_3`, `trial_lead_source_4`, `trial_lead_source_5`, `trial_lead_source_6`, `trial_lead_source_7`, `trial_lead_source_8`, `trial_lead_source_9`, `trial_lead_source_10`,`trial_sort_order`) 
                                VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s',
                                NextVal('trial_sort_order_seq'))", mysqli_real_escape_string($this->db, $company_id), 
                            mysqli_real_escape_string($this->db, 'S'), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_title'] . " copy"), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_subtitle'] . " copy"), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_desc']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_prog_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_welcome_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_banner_img_content']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_waiver_policies']), 
                            mysqli_real_escape_string($this->db, $output[0]['processing_fee_type']), 
                            mysqli_real_escape_string($this->db, $output[0]['price_amount']), 
                            mysqli_real_escape_string($this->db, $output[0]['program_length_type']), 
                            mysqli_real_escape_string($this->db, $output[0]['program_length']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_1']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_1_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_2']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_2_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_3']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_3_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_4']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_4_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_5']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_5_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_6']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_6_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_7']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_7_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_8']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_8_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_9']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_9_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_10']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_registration_column_10_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_1']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_2']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_3']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_4']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_5']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_6']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_7']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_8']), 
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_9']),
                            mysqli_real_escape_string($this->db, $output[0]['trial_lead_source_10']));

                 $result1 = mysqli_query($this->db, $sql1);//discount

                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $last_inserted_trial_id = mysqli_insert_id($this->db);
                     $company_code_new = $this->clean($company_code);
                     $trial_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/t/?=$company_code_new/$company_id/$last_inserted_trial_id";
                    $trial_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->local_upload_folder_name/Default/default.png";
                    if($output[0]['trial_banner_img_url'] !== $trial_banner_img_url){
                        $file_name = $company_id . "-" . $last_inserted_trial_id;
                        $cfolder_name = "Company_$company_id";
                          $old = getcwd(); // Save the current directory
                        $dir_name = "../../../$this->upload_folder_name";
                        chdir($dir_name);
                        if (!file_exists($cfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($cfolder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($cfolder_name);
                        $folder_name = "Trial";
                        if (!file_exists($folder_name)) {
                            $oldmask = umask(0);
                            mkdir($folder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($folder_name);
                        $file = $file_name.'.png' ;
                        $check = copy($output[0]['trial_banner_img_url'],$file);
                       chdir($old);
                       if($check){
                        $trial_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->local_upload_folder_name/$cfolder_name/$folder_name/$file";
                       }else{
                           $error_log = array('status' => "Failed", "msg" => "Image copy failed", "image_url" => $output[0]['trial_banner_img_url']);
                            log_info($this->json($error_log));
                       }
                    }

                        $sql_update_url = sprintf("UPDATE `trial` SET `trial_prog_url` = '$trial_url',`trial_banner_img_url` = '$trial_banner_img_url' WHERE `trial_id` = '%s' ", mysqli_real_escape_string($this->db, $last_inserted_trial_id));
                        $result_update_url = mysqli_query($this->db, $sql_update_url);
                        if (!$result_update_url) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update_url");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else {
                        $discount = $this->getTrialDiscountDetails($company_id, $trial_id);
                        $disc = $discount['discount'];
                        if ($discount['status'] == 'Success') {
                            for ($i = 0; $i < count($disc); $i++) {
                                $sql_disc = sprintf("INSERT INTO `trial_discount`(`company_id`, `trial_id`, `discount_type`, `discount_code`, `discount_amount`) VALUES ('%s', '%s', '%s', '%s', '%s')", 
                                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $last_inserted_trial_id), mysqli_real_escape_string($this->db, $disc[$i]['discount_type']), 
                                        mysqli_real_escape_string($this->db, $disc[$i]['discount_code']), mysqli_real_escape_string($this->db, $disc[$i]['discount_amount']));
                                $result_disc = mysqli_query($this->db, $sql_disc);
                                if (!$result_disc) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_disc");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
                    }
                }
                $this->getTrialDetailByCompany($company_id, $list_type, 0);
            } else {
                $error = array('status' => "Failed", "msg" => "Trial program details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
     public function createtrialCSV($company_id, $trial_id){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);        
        $title = $subtitle = $start = $end = "";$output = [];
        $cus_field_1 = $cus_field_2 = $cus_field_3 = $cus_field_4 = $cus_field_5 = $cus_field_6 = $cus_field_7 = $cus_field_8 = $cus_field_9 = $cus_field_10 = "";
        $select_query = sprintf("SELECT t.`trial_status`, t.`trial_title`, t.`trial_subtitle`, t.`trial_desc`, t.`trial_prog_url`, t.`trial_welcome_url`, t.`trial_banner_img_url`, t.`trial_banner_img_content`, t.`trial_waiver_policies`, t.`registrations_count`, t.`net_sales`, t.`processing_fee_type`, t.`price_amount`, 
            t.`program_length_type`, t.`program_length`, t.`trial_sort_order`, concat(t.`trial_registration_column_1`,' ', t.`trial_registration_column_2`) participant_name, t.`trial_registration_column_3`,  
            t.`trial_registration_column_4`, t.`trial_registration_column_5`,  t.`trial_registration_column_6`,  t.`trial_registration_column_7`,  t.`trial_registration_column_8`,
            t.`trial_registration_column_9`, t.`trial_registration_column_10`, t.`trial_lead_source_1`, t.`trial_lead_source_2`, t.`trial_lead_source_3`, t.`trial_lead_source_4`, t.`trial_lead_source_5`,
            t.`trial_lead_source_6`, t.`trial_lead_source_7`, t.`trial_lead_source_8`, t.`trial_lead_source_9`, t.`trial_lead_source_10`, tr.`trial_status` reg_status,concat(tr.`buyer_last_name`,', ',tr.`buyer_first_name`) buyer_name, tr.`buyer_email`, tr.`buyer_phone`, tr.`buyer_postal_code`, tr.`registration_date`, tr.`registration_amount`,tr.`payment_amount`,
           tr.`payment_amount`, tr.`paid_amount`, tr.`refunded_amount`,concat(tr.`trial_registration_column_2`,', ',tr.`trial_registration_column_1`) participant, tr.`trial_registration_column_3` cus_field_3,
            tr.`trial_registration_column_4` cus_field_4, tr.`trial_registration_column_5` cus_field_5, tr.`trial_registration_column_6` cus_field_6, tr.`trial_registration_column_7` cus_field_7, tr.`trial_registration_column_8` cus_field_8, tr.`trial_registration_column_9` cus_field_9,
            tr.`trial_registration_column_10` cus_field_10,tr.`start_date`,tr.`end_date` FROM `trial` t,`trial_registration` tr WHERE t.`company_id`='%s' AND t.`trial_id`='%s'
             AND t.`trial_status`!='D' AND t.`trial_id`=tr.`trial_id`", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_id));
        $result_select = mysqli_query($this->db, $select_query);
        if(!$result_select){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result_select);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_select)) {
                    $trial_details = $row;
                    $title = $row['trial_title'];
                    $subtitle = $row['trial_subtitle'];
                    if($row['program_length_type'] == 'D'){
                       $length = "$row[program_length]" . " " . "day";
                    }else{
                        $length = "$row[program_length]" . " " . "week";
                    }
                    
//                $start_date = $row['start_date'];
//                $end_date = $row['end_date'];
                    $output[] = $row;
                }
            } else {
                $error = array("status" => "Failed", "msg" => "Trial program details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }

        $header = $body = '';
        $header .= "Participant Name,Trial Program Title,Program Length,";
        
        $temp_reg_field = "trial_registration_column_";
        for ($z = 3; $z <= 10; $z++) { //collect registration fields as array
            $reg_field = $temp_reg_field . "$z";
            if ($trial_details[$reg_field] != '') {
                  $header = $header .'"'. str_replace('"', "''", $trial_details[$reg_field]) .'"'. ",";
            }
        }
        

        $header = $header . "Buyer Name,Buyer Email,Buyer Phone Number,Buyer Postal code,Registration Date,Start Date,End Date,Price Amount,"
                . "Total Paid by User to date\r\n";

        for ($row_index = 0; $row_index < $num_rows; $row_index++) {
            if (!empty(trim($output[$row_index]['trial_title']))) {
                $trial_title = str_replace('"', "''", $output[$row_index]['trial_title']);
            } else {
                $trial_title = "";
            }
           
           
            $body = $body . '"' . $output[$row_index]['participant'].'"' . ',' . $trial_title .  ","  . $length . ",";
            $temp_reg_field_value = "cus_field_";
            for ($z = 3; $z <= 10; $z++) { //collect registration fields as array
                $reg_field = $temp_reg_field . "$z";
                $reg_field_value = $temp_reg_field_value . "$z"; //for filed names
                if ($trial_details[$reg_field] != '') {
                    $body = $body . '"' . str_replace('"', "''", $output[$row_index][$reg_field_value]) . '"' . ",";
                }
            }

            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_name']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_email']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_phone']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_postal_code']) . '"' . ",";

            if (!empty(trim($output[$row_index]['registration_date']))) {
                if ("0000-00-00" == $output[$row_index]['registration_date']) {
                    $registration_date = "";
                } else {
                    $reg_date = $output[$row_index]['registration_date'];
                    $registration_date = date('Y-m-d', strtotime($reg_date));
                }
            } else {
                $registration_date = "";
            }
            if (!empty(trim($output[$row_index]['start_date']))) {
                if ("0000-00-00" == $output[$row_index]['start_date']) {
                    $start_date = "";
                } else {
                    $reg_date = $output[$row_index]['start_date'];
                    $start_date = date('Y-m-d', strtotime($reg_date));
                }
            } else {
                $start_date = "";
            }
            if (!empty(trim($output[$row_index]['end_date']))) {
                if ("0000-00-00" == $output[$row_index]['end_date']) {
                    $end_date = "";
                } else {
                    $reg_date = $output[$row_index]['end_date'];
                    $end_date = date('Y-m-d', strtotime($reg_date));
                }
            } else {
                $end_date = "";
            }

            $body = $body . $registration_date . ",";
            $body = $body . $start_date . ",";
            $body = $body . $end_date . ",";
            $body = $body . $output[$row_index]['price_amount'] . ",";

            $total_paid_by_userToDate = $output[$row_index]['payment_amount'];
            $body = $body . $total_paid_by_userToDate . "\r\n";

        }
        ob_clean();
        ob_start();
        $file = "../../../uploads/dummy1.csv";
        $ifp = fopen($file, "w");
        fwrite($ifp, $header);
        fwrite($ifp, $body);
        fclose($ifp);
        header('Content-Type: application/x-download; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        readfile($file);
        unlink($file);
        date_default_timezone_set($curr_time_zone);
        exit();        
    } 
   
     private function sendSnsforfailedcheckout($sub,$msg){

            $topicarn = 'arn:aws:sns:us-east-1:241334827363:mystudio';
         
                    $subject = $sub;
    //                $subject = $sns_sub . " -transaction failed";
                    $message = $msg;

                    $credentials = new Aws\Credentials\Credentials('AKIAIGUWYNJGAHDPJGIA', 'w4oKbP0UhJZr1W0hGuULhkdEoxEHY2YxEq/6uycT');               
                    $sns = SnsClient::factory([
                        'credentials' => $credentials,
                        'region' => 'us-east-1',
                        'version' => 'latest'
                    ]);
    //                log_info("object cretaed");
                    $result1 = $sns->publish(array(
                        'TopicArn' => $topicarn,
                        // Message is required
                        'Message' => $message,
                        'Subject' => $subject,
                        //'MessageStructure' => 'string',
                        'MessageAttributes' => array(
                            // Associative array of custom 'String' key names
                            'String' => array(
                                // DataType is required
        //            'DataType' => '',
                                'DataType' => 'String',
                                'StringValue' => '200',
                            ),
                        // ... repeated
                        ),
                    ));
               
    }
    
    public function sendEmailForTrial($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list,$student_cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if(!empty(trim($student_cc_email_list))){
                $student_cc_addresses = explode(',', $student_cc_email_list);
                for($init_1=0;$init_1<count($student_cc_addresses);$init_1++){
                    if(filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($student_cc_addresses[$init_1]);
                    }
                }
            }
            
            $mail->Subject = $subject;
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
     public function reRunTrialPayment($company_id,$reg_id,$payment_id){
        $query = sprintf("SELECT `registration_from` FROM `trial_registration`
                    WHERE `company_id`='%s' AND `trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $rows = mysqli_fetch_assoc($result);
            $registration_from = $rows['registration_from'];
            if ($registration_from == 'S'){
                $this->reRunstripeTrialPayment($company_id, $reg_id, $payment_id);
            }    
        }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;        

        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
        
        $query = sprintf("SELECT `access_token`, wp.`account_id`, wp.`account_state`, wp.`currency`, tr.`company_id`, tr.`student_id`, t.`trial_title`, tr.`trial_reg_id`, `trial_payment_id`, tr.`trial_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, 
                tr.`registration_amount`, tp.`processing_fee`, tr.`payment_type`,tr.`payment_amount` total_due, `paid_amount`, `credit_card_id`, `credit_card_name`, `credit_card_status`,  
                tp.`payment_amount`, tp.`payment_date`, tp.`payment_status`, tr.`processing_fee_type`, tp.`checkout_id`, tp.`checkout_status`,c.`wp_currency_symbol`,tr.`student_id` ,tp.`credit_method`
                FROM `trial_registration` tr 
                LEFT JOIN `wp_account` wp ON tr.`company_id` = wp.`company_id` 
                LEFT JOIN `trial_payment` tp ON tr.`trial_reg_id`=tp.`trial_reg_id` AND (tp.`payment_status`='F' || (tp.`payment_status`='N' && tp.`payment_date`< ($currdate_add)))
                LEFT JOIN `trial` t ON t.`trial_id` = tr.`trial_id`
                LEFT JOIN `company` c ON tr.`company_id` = c.`company_id` 
                WHERE   tr.`trial_reg_id` = '%s'  AND `trial_payment_id` = '%s'  AND tp.`payment_amount`>0",
                 mysqli_real_escape_string($this->db, $reg_id),mysqli_real_escape_string($this->db, $payment_id));
//         WHERE ep.`schedule_date`=now() AND `payment_type`='RE' AND ep.`payment_amount`>0 ORDER BY er.`company_id`
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $company_list = [];
                while($row = mysqli_fetch_assoc($result)){
                    $company_id = $row['company_id'];
                    $access_token = $row['access_token'];
                    $account_id = $row['account_id'];
                    $acc_state = $row['account_state'];
                    $currency = $row['currency'];
                    $trial_payment_id = $row['trial_payment_id'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $buyer_postal_code = $row['buyer_postal_code'];
                    $cc_id = $row['credit_card_id'];
                    $cc_name = $row['credit_card_name'];
                    $student_id = $row['student_id'];
                    $date_in_db = $row['payment_date'];
                    if($row['credit_method'] == 'CA' || $row['credit_method'] == 'CH'){
                        $error = array('status' => "Failed", "msg" => "Trial payment via cash or check can not be rerun.");
                        $this->response($this->json($error), 200);
                    }
                
                    if($acc_state=='deleted'){
                        $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                        $this->response($this->json($log),200);
                    }elseif($acc_state=='disabled'){
                        $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                        $this->response($this->json($log),200);
                    }
                    
                    if(!empty($access_token)){
                        $failure = $success = 0;
                        $trial_id = $row['trial_id'];
                        $trial_name = $desc = $row['trial_title'];
                        $payment_amount = $row['payment_amount'];
                        $processing_fee = $row['processing_fee'];
                        $processing_fee_type = $row['processing_fee_type'];
                        $wp_currency_symbol = $row['wp_currency_symbol'];
                        $payment_date = $row['payment_date'];
                        $gmt_date=gmdate(DATE_RFC822);
                        $sns_msg3=array("buyer_email"=>$buyer_email,"membership_title"=>$trial_name,"gmt_date"=>$gmt_date);
                
//                        if (!function_exists('getallheaders')){
//                            if(!function_exists ('apache_request_headers')){
//                                $user_agent = $_SERVER['HTTP_USER_AGENT'];
//                            }else{
//                                $headers = apache_request_headers();
//                                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
//                            }
//                        }else{
//                            $headers = getallheaders();
//                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
//                        }
                        $user_agent = "mystudio.academy";
                        $temp_password = $this->wp_pswd;
                        $method = 'aes-256-cbc';
                        // Must be exact 32 chars (256 bit)
                        $password = substr(hash('sha256', $temp_password, true), 0, 32);
                        // IV must be exact 16 chars (128 bit)
                        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                        
//                        $json = array("account_id"=>"$account_id");
//                        $postData = json_encode($json);
//                        $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent,$sns_msg3);
//                        log_info("wepay_get_account: ".$response['status']);
//                        if($response['status']=='Success'){
//                            $res = $response['msg'];
//                            $account_state = $res['state'];
//                            $currency = $res['currencies'][0];
//                            $action_reasons = implode(",", $res['action_reasons']);
//                            $disabled_reasons = implode(",", $res['disabled_reasons']);
//                            $curr_acc = implode(",", $res['currencies']);
//                            if($account_state=='deleted'){
//                                $error = array('status' => "Failed", "msg" => "Wepay Account has been deleted.");
//                                $this->response($this->json($error), 500);
//                            }elseif($account_state=='disabled'){
//                                $error = array('status' => "Failed", "msg" => "The account has been disabled for following reason(s) - ".$disabled_reasons." by WePay and can no longer accept payments.");
//                                $this->response($this->json($error), 500);
//                            }
//                        }else{
//                            $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
//                            $this->response($this->json($error), 500);
//                        }
                        
                        $rand = mt_rand(1,99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout?for=trial";
                        $unique_id = $company_id."_".$trial_id."_".$ip."_".$rand."_".time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id."_".$trial_id."_".$date;
//                        if($processing_fee_type==2){
//                            $fee_payer = "payer_from_app";
//                        }elseif($processing_fee_type==1){
                            $fee_payer = "payee_from_app";
//                        }
                        if($processing_fee_type==2){
                            $w_paid_amount = $payment_amount + $processing_fee;
                        }elseif($processing_fee_type==1){
                            $w_paid_amount = $payment_amount;
                        }
                        $time = time();
                        $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                            "fee"=>array("app_fee"=>$processing_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                            "email_message"=>array("to_payee"=>"Payment has been added for $trial_name","to_payer"=>"Payment has been made for $trial_name"),
                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                            "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code")))),
                            "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                "properties"=>array("itemized_receipt"=>array(array("description"=>"$trial_name", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                        $postData2 = json_encode($json2);                        
                        $sns_msg3['call'] = "Checkout";
                        $sns_msg3['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg3);
                        log_info("wepay_checkout_create: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $success=1;
//                            $this->rbitCallForReRunCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $event_name, $w_paid_amount, $currency);
                        }else{
                            if($response2['msg']['error_code']==1008){
                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg3);
                                log_info("wepay_checkout_create: ".$response2['status']);
                                if($response2['status']=="Success"){
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $success=1;
//                                    $this->rbitCallForReRunCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $event_name, $w_paid_amount, $currency);
                                }else{
                                    if($response2['msg']['error_code']==1008){
                                        $error = array('status' => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error), 200);
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }
                            }else{
                                $error = array('status' => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error), 200);
                            }
                        }
                        
                        $payment_query = sprintf("UPDATE `trial_payment` SET `checkout_id`='%s', `checkout_status`='%s', `payment_status`='%s', `cc_id`='$cc_id', `cc_name`='$cc_name' WHERE `trial_payment_id`='%s'",
                                mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, 'S'),
                                mysqli_real_escape_string($this->db, $trial_payment_id));
                        $payment_result = mysqli_query($this->db, $payment_query);
                        if (!$payment_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            $curr_date = gmdate("Y-m-d H:i:s");
                            $scheduled_date = date("M d, Y",strtotime($date_in_db));
                            $text = "Rerun payment $wp_currency_symbol"."$w_paid_amount"." past due "."$scheduled_date";

                            $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), $student_id, 'rerun', $text, $curr_date);
                            $result_history = mysqli_query($this->db, $insert_history);
                            if (!$result_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                            $payment_history = $this->getpaymentHistoryDetails($company_id, $reg_id, 1);
                            $msg = array('status' => "Success", "msg" => "Payment Successful.", "payment_history" => $payment_history);
                            $this->response($this->json($msg), 200);
                        }
                    } else {
                        $error = array('status' => "Failed", "msg" => "Access token error. Cannot be accept payments anymore.");
                        $this->response($this->json($error), 200);
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Event payment details mismatched.");
                $this->response($this->json($error), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
     public function updatetrialSortingDetails($trial_sort_order, $company_id, $trial_id, $list_type){
          $query = sprintf("SELECT * FROM `trial` WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
        $res = mysqli_query($this->db, $query);
        if(!$res){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($res);
            if($num_rows==0){
                $error = array('status' => "Failed", "msg" => "Trial program details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }
         $update_message = sprintf("UPDATE `trial` SET `trial_sort_order`='%s' WHERE `trial_id`='%s' AND `company_id`='%s'", $trial_sort_order, $trial_id, $company_id);
        $result = mysqli_query($this->db, $update_message);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => $update_message);
            $this->response($this->json($error), 200);
        }
        $this->getTrialDetailByCompany($company_id, $list_type, 0); 
//        $out = array('status' => "Success", 'msg' => 'Successfully updated','trial_details' => $trial_details);
//                // If success everythig is good send header as "OK" and user details
//        $this->response($this->json($out), 200);
    }
    
    public function updateTrialParticipantStatusInDb($company_id, $student_id, $trial_id, $trial_reg_id,  $trial_status, $next_date){
        $this->addTrialDimensions($company_id, $trial_id, '');
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $curr_date = date("Y-m-d");
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_add = "SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";

        $reg_string = $trial_string =  $date_changing_string = $activity_text='';$old_date =[];
        
        $checkstatus=sprintf("SELECT `trial_status`,`active_date`,`enrolled_date`,`cancelled_date`,`didnotstart_date`,IF((`active_date` is NULL && `enrolled_date` is NULL && `cancelled_date` is NULL && `didnotstart_date` is NULL),`registration_date`,'') reg_date  FROM `trial_registration` WHERE `trial_id`='%s' and  `company_id`='%s' and `trial_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $trial_reg_id));
        $resultcheckstatus= mysqli_query($this->db, $checkstatus);
        if(!$resultcheckstatus){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$checkstatus");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
        }else{
            $result_value= mysqli_fetch_object($resultcheckstatus);
            $old_trial_status = $result_value->trial_status;
//            $old_date['active'] = $result_value->active_date;
//            $old_date['enrolled'] = $result_value->enrolled_date;
//            $old_date['cancelled'] = $result_value->cancelled_date;
//            $old_date['didnotstart'] = $result_value->didnotstart_date;
//            $old_date['reg'] = $result_value->reg_date;
         }
         $temp_date = "'".$next_date."'";
         if ($trial_status == 'A') {
            $reg_string = ", `active_date` = $temp_date, `enrolled_date`= NULL, `cancelled_date`= NULL,`didnotstart_date` = NULL";
            $activity_text = "Status changed to Active";
            if ($old_trial_status == 'E') {
                $trial_string = "`enrolled_count`=`enrolled_count`-1,`active_count`=`active_count`+1";
            } else if ($old_trial_status == 'C') {
                $trial_string = "`cancelled_count`=`cancelled_count`-1,`active_count`=`active_count`+1";
            } else if ($old_trial_status == 'D') {
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1,`active_count`=`active_count`+1";
            }
        } elseif ($trial_status == 'E') {
//            if($next_date <= date("Y-m-d")){
            $reg_string = ", `active_date` = NULL, `enrolled_date`= $temp_date, `cancelled_date`= NULL,`didnotstart_date` = NULL";
            $activity_text = "Status changed to Enrolled";
//            }else{
//               $reg_string =  $activity_text = '';
//            }
            if ($old_trial_status == 'A') {
                $trial_string = "`active_count`=`active_count`-1,`enrolled_count`=`enrolled_count`+1";
            } else if ($old_trial_status == 'C') {
                 $trial_string = "`cancelled_count`=`cancelled_count`-1,`enrolled_count`=`enrolled_count`+1";
            } else if ($old_trial_status == 'D') {
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1,`enrolled_count`=`enrolled_count`+1";
            }
        } elseif ($trial_status == 'C') {
            $reg_string = ", `active_date` = NULL, `enrolled_date`= NULL, `cancelled_date` = $temp_date,`didnotstart_date` = NULL";
            $activity_text = "Status changed to Cancel";
            if ($old_trial_status == 'E') {
                $trial_string = "`enrolled_count`=`enrolled_count`-1,`cancelled_count`=`cancelled_count`+1";
            } else if ($old_trial_status == 'A') {
                $trial_string = "`active_count`=`active_count`-1,`cancelled_count`=`cancelled_count`+1";
            } else if ($old_trial_status == 'D') {
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1,`cancelled_count`=`cancelled_count`+1";
            }
        } else if ($trial_status == 'D') {
            $reg_string = ", `active_date` = NULL, `enrolled_date`= NULL, `cancelled_date`= NULL, `didnotstart_date` = $temp_date";
            $activity_text = "Status changed to Did Not Start";
            if ($old_trial_status == 'E') {
                 $trial_string = "`enrolled_count`=`enrolled_count`-1,`didnotstart_count`=`didnotstart_count`+1";
            } else if ($old_trial_status == 'C') {
                 $trial_string = "`cancelled_count`=`cancelled_count`-1,`didnotstart_count`=`didnotstart_count`+1";
            } else if ($old_trial_status == 'A') {
                 $trial_string = "`active_count`=`active_count`-1,`didnotstart_count`=`didnotstart_count`+1";
            }
        }


        $get_reg_details = sprintf("SELECT tr.*, c.upgrade_status FROM `trial_registration` tr LEFT JOIN company c ON c.company_id=tr.company_id WHERE `trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_id));
            $result_get_reg_details = mysqli_query($this->db, $get_reg_details);
            if (!$result_get_reg_details) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_reg_details");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                $num_rows = mysqli_num_rows($result_get_reg_details);
                if($num_rows>0){
                    $reg_details = mysqli_fetch_assoc($result_get_reg_details);
                    $upgrade_status = $reg_details['upgrade_status'];
//                        $reg_details = $reg_details_arr[0];
                }else{
                    $error = array('status' => "Failed", "msg" => "Registration details mismatched.");
                    $this->response($this->json($error), 200);
                }
            }
            
           $query1 = sprintf("UPDATE `trial_registration` SET `trial_status`='%s' $reg_string WHERE `company_id`='%s' AND `trial_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $trial_status), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id));
        $result1 = mysqli_query($this->db, $query1);
//        log_info("status   ".$query1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
            
        if(!empty(trim($trial_string))){
            $query2 = sprintf("UPDATE `trial` SET $trial_string WHERE `company_id`='%s' AND `trial_id`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
            $result2 = mysqli_query($this->db, $query2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        if (!empty(trim($activity_text))) {
            $currdate = gmdate("Y-m-d H:i:s");
            $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `student_id`,`trial_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $trial_reg_id), 'status', mysqli_real_escape_string($this->db, $activity_text),
                    mysqli_real_escape_string($this->db, $currdate));
            $result_history = mysqli_query($this->db, $insert_history);
            if (!$result_history) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
//        if ($next_date <= date("Y-m-d")) {
            $this->updateTrialDimensionsMembers($company_id, $trial_id, $trial_status, $old_trial_status, $trial_reg_id);
            
//        }
        $reg_details = $this->getTrialRegDetails($company_id, $trial_reg_id, 1);
        $msg = array('status' => "Success", "msg" => "Trial program status updated successfully.", "reg_details" => $reg_details);
        date_default_timezone_set($curr_time_zone);
        $this->response($this->json($msg), 200);
    }
    
     public function getTrialRegDetails($company_id, $trial_reg_id, $call_back) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
//        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $created_date = " DATE(CONVERT_TZ(`created_dt`,$tzadd_add,'$new_timezone'))";
        
        $output = $output['All'] =$output3 =$output['reg_columns']= $cards = [];
        $trial_id =  $buyer_email = '';
        $sql_reg = sprintf("SELECT `trial_reg_id`, `company_id`, `student_id`, `participant_id`, `trial_id`, `trial_status`,  concat(`buyer_first_name`,' ',`buyer_last_name`) buyer_name,
                    `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`,
                    `processing_fee_type`, `processing_fee`, `payment_type`, `payment_amount`, `paid_amount`, `refunded_amount`, `payment_method`, 
                    `credit_card_id`, `credit_card_status`,if(`registration_from`='S',`stripe_card_name`, `credit_card_name`) as credit_card_name,`registration_from`,`credit_card_expiration_month`,(select t.trial_title from trial t where t.`trial_id`=tr.`trial_id`) trial_category,
                    `credit_card_expiration_year`, `start_date` start_date, `end_date`, `enrolled_date`, `cancelled_date`,
                    `program_length`, `program_length_type`, concat(`trial_registration_column_1`,' ',`trial_registration_column_2`) participant_name,
                    `trial_registration_column_3`, `trial_registration_column_4` leadsource, `trial_registration_column_5`,
                    `trial_registration_column_6`, `trial_registration_column_7`, `trial_registration_column_8`,
                    `trial_registration_column_9`, `trial_registration_column_10`, `trial_reg_type_user`, `trial_reg_version_user`,
                    $created_date created_dt FROM `trial_registration` tr  WHERE `company_id`='%s' AND `trial_reg_id`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id));
        $result_reg_details = mysqli_query($this->db, $sql_reg);
        if (!$result_reg_details) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_reg");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_reg_details);
            if ($num_rows > 0) {
                $output = mysqli_fetch_assoc($result_reg_details);
                $trial_id = $output['trial_id'];
                $buyer_email = $output['buyer_email'];
            } else {
                $error = array('status' => "Failed", "msg" => "Trial details doesn't exist.");
                if($call_back==0){
                    $this->response($this->json($error), 200); // If no records "No Content" status  
                }else{
                    return $error;
                }
            }
            
            $all_card_details = $this->getStudentDetailsForTrial($company_id, 1);
            if($all_card_details['status']=='Success'){
                $stud = $all_card_details['msg']['student_details'];
                for($i=0;$i<count($stud);$i++){
                    if($stud[$i]['buyer_email']==strtolower($buyer_email)){
                        if(!empty($stud[$i]['card_details'])){
                            $cards = $stud[$i]['card_details'];
                        }
                    }
                }
            }
            
//            $output2 = $this->getTrialDetailByCompany($company_id, 1);
//            if($output2['status']=='Success'){
//                $out = $output2['msg']['live'];
//                $output['All'] = $out;
//            }else{
//                $error = array('status' => "Failed", "msg" => "trial program details doesn't exist.");
//                if($call_back==0){
//                    $this->response($this->json($error), 200);
//                }else{
//                    return $error;
//                }
//            }
            
            
            $output3 = $this->getparticipantInfoDetails($company_id, $trial_reg_id, 1);
            if(!empty($output3) && $output3['status']=='Success'){
                $output['reg_columns'] = $output3['msg'];                
                $output['reg_columns']['card_details'] = $cards;
            }else{
                $error = array('status' => "Failed", "msg" => "Trial Registration details doesn't exist.");
            }
            
            $out = array('status' => "Success", 'msg' => $output);
            if($call_back==0){
                $this->response($this->json($out), 200);
            }else{
                return $out;
            }
        }
    }
    
     public function getStudentDetailsForTrial($company_id, $call_back) {
        $group_by_text = '';
        $query = sprintf("SELECT c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if (mysqli_num_rows($result1) > 0) {
                $row = mysqli_fetch_assoc($result1);
                $wepay_status = $row['wepay_status'];
                $stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
            }
        }
        if ($stripe_status == 'Y' && $stripe_account_state != 'N'){
            $group_by_text = ', payment_method_id';
        }elseif($wepay_status == 'Y'){
            $group_by_text = ', credit_card_id';
        }else{
            $group_by_text = '';
        }

        $sql = sprintf("SELECT mr.student_id,mr.`participant_id`, concat(`membership_registration_column_1`,' ', `membership_registration_column_2`) student_name, mr.`buyer_first_name`,mr.`buyer_last_name`, `buyer_email`, `buyer_phone`,     
                        `buyer_postal_code`, mr.`payment_method_id`, mr.`credit_card_id`, `credit_card_status`, `credit_card_name`, `stripe_card_name`,`credit_card_expiration_month`, `credit_card_expiration_year`,       
                        `membership_registration_column_1` reg_col_1, `membership_registration_column_2`  reg_col_2, `membership_registration_column_3` reg_col_3, `membership_registration_column_4` reg_col_4, `membership_registration_column_5` reg_col_5,           
                        `membership_registration_column_6`  reg_col_6, `membership_registration_column_7` reg_col_7, `membership_registration_column_8` reg_col_8, `membership_registration_column_9` reg_col_9, `membership_registration_column_10` reg_col_10,         
                        `participant_street`, `participant_city`, `participant_state`, `participant_zip`, `participant_country`, concat(`membership_registration_column_1`,' ', `membership_registration_column_2`) sname, created_dt , last_updt_dt, mr.registration_from  
                        FROM `membership_registration` mr LEFT JOIN student s ON s.`company_id`= mr.`company_id` AND mr.`student_id`= s.`student_id` AND s.`deleted_flag`!='Y' WHERE mr.company_id = '%s' AND (TRIM(mr.`payment_method_id`)!='' || TRIM(mr.`credit_card_id`)!='') GROUP BY `buyer_email`, sname %s
                        UNION
                        SELECT er.student_id,er.`participant_id`, concat(`event_registration_column_1`,' ', `event_registration_column_2`) student_name, er.`buyer_first_name`,er.`buyer_last_name`, er.`buyer_email`, er.`buyer_phone`,           
                        er.`buyer_postal_code`, er.`payment_method_id`, er.`credit_card_id`, er.`credit_card_status`, er.`credit_card_name`,`stripe_card_name`, er.`credit_card_expiration_month`, er.`credit_card_expiration_year`,     
                        `event_registration_column_1` reg_col_1, `event_registration_column_2` reg_col_2, `event_registration_column_3` reg_col_3, `event_registration_column_4` reg_col_4, `event_registration_column_5` reg_col_5,     
                        `event_registration_column_6` reg_col_6, `event_registration_column_7` reg_col_7, `event_registration_column_8` reg_col_8, `event_registration_column_9` reg_col_9, `event_registration_column_10` reg_col_10,  
                        '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country, concat(`event_registration_column_1`,' ', `event_registration_column_2`) sname, created_dt, last_updt_dt, er.registration_from 
                        FROM `event_registration` er LEFT JOIN student s ON s.`company_id`= er.`company_id` AND er.`student_id`= s.`student_id` AND s.`deleted_flag` !='Y' WHERE er.company_id = '%s' AND (TRIM(er.`payment_method_id`)!='' || TRIM(er.`credit_card_id`)!='') GROUP BY `buyer_email`, sname %s 
                        UNION
                        SELECT tr.student_id, tr.`participant_id`,concat(`trial_registration_column_1`,' ', `trial_registration_column_2`) student_name, tr.`buyer_first_name`,tr.`buyer_last_name`, tr.`buyer_email`, tr.`buyer_phone`,           
                        tr.`buyer_postal_code`, tr.`payment_method_id`, tr.`credit_card_id`, tr.`credit_card_status`, tr.`credit_card_name`,`stripe_card_name`, tr.`credit_card_expiration_month`, tr.`credit_card_expiration_year`,     
                        `trial_registration_column_1` reg_col_1, `trial_registration_column_2` reg_col_2, `trial_registration_column_3` reg_col_3, `trial_registration_column_4` reg_col_4, `trial_registration_column_5` reg_col_5,     
                        `trial_registration_column_6` reg_col_6, `trial_registration_column_7` reg_col_7, `trial_registration_column_8` reg_col_8, `trial_registration_column_9` reg_col_9, `trial_registration_column_10` reg_col_10,  
                        '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country ,concat(`trial_registration_column_1`,' ', `trial_registration_column_2`) sname,created_dt, last_updt_dt, tr.registration_from 
                        FROM `trial_registration` tr LEFT JOIN student s ON s.`company_id`= tr.`company_id` AND tr.`student_id`= s.`student_id` AND s.`deleted_flag` !='Y' WHERE tr.company_id = '%s' AND (TRIM(tr.`payment_method_id`)!='' || TRIM(tr.`credit_card_id`)!='') GROUP BY `buyer_email`, sname %s
                        UNION
                        SELECT ms.student_id, '' as participant_id, '' as student_name, ms.`buyer_first_name`,ms.`buyer_last_name`, ms.`buyer_email`, ms.`buyer_phone`,           
                        '' as buyer_postal_code, ms.`payment_method_id`, ms.`credit_card_id`, ms.`credit_card_status`, ms.`credit_card_name`,`stripe_card_name`, ms.`credit_card_expiration_month`, ms.`credit_card_expiration_year`,     
                        '' as reg_col_1, '' as reg_col_2, '' as reg_col_3, '' as reg_col_4, '' as reg_col_5, '' as reg_col_6, '' as reg_col_7, '' as reg_col_8, '' as reg_col_9, '' as reg_col_10,  
                        '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country ,'' as sname,created_dt, last_updt_dt, ms.registration_from 
                        FROM `misc_order` ms LEFT JOIN student s ON s.`company_id`= ms.`company_id` AND ms.`student_id`= s.`student_id` AND s.`deleted_flag` !='Y' WHERE ms.company_id = '%s' AND (TRIM(ms.`payment_method_id`)!='' || TRIM(ms.`credit_card_id`)!='') GROUP BY `buyer_email` %s
                        ORDER BY `buyer_email` ASC ,`created_dt` DESC, `last_updt_dt` DESC", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $group_by_text),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $group_by_text), mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $group_by_text), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $group_by_text));                
        $result = mysqli_query($this->db, $sql);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $output = $output['student_details'] = [];
                $stud_list = $obj_list = $out = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $out[] = $row;
                }
                for($i=0; $i<count($out); $i++){
                    $card = [];
                    $row = $out[$i];
                    $email = strtolower(trim($row['buyer_email']));
                    $card_id = $row['payment_method_id'];
                    $name = strtolower(trim($row['sname']));
                    $obj = array("email"=>"$email", "name"=>"$name");
                    if(!in_array($obj, $obj_list)){
                        $obj_list[] = $obj;
                        if ($stripe_status == 'Y' && $stripe_account_state != 'N'){
                            if (!empty($row['payment_method_id'])) {
                                $card['payment_method_id'] = $row['payment_method_id'];
                                $card['credit_card_name'] = $row['stripe_card_name'];
                                $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                $card['postal_code'] = $row['buyer_postal_code'];
                                $row['card_details'][] = $card;
                            }               
                            unset($row['credit_card_id']); 
                            unset($row['credit_card_status']);
                            unset($row['credit_card_name']);
                            unset($row['payment_method_id']);
                            unset($row['stripe_card_name']);
                            unset($row['credit_card_expiration_month']);
                            unset($row['credit_card_expiration_year']);
                            if(isset($output['student_details'])){
                                $row['stud_index'] = count($output['student_details']);
                            }else{
                                $row['stud_index'] = 0;
                            }
                        }else if ($wepay_status == 'Y'){
                            if (!empty($row['credit_card_id'])) {
                                $card['credit_card_id'] = $row['credit_card_id'];
                                $card['credit_card_status'] = $row['credit_card_status'];
                                $card['credit_card_name'] = $row['credit_card_name'];
                                $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                $card['postal_code'] = $row['buyer_postal_code'];
                                $row['card_details'][] = $card;
                            }
                            unset($row['payment_method_id']);
                            unset($row['stripe_card_name']);
                            unset($row['credit_card_id']);
                            unset($row['credit_card_status']);
                            unset($row['credit_card_name']);
                            unset($row['credit_card_expiration_month']);
                            unset($row['credit_card_expiration_year']);
                            if(isset($output['student_details'])){
                                $row['stud_index'] = count($output['student_details']);
                            }else{
                                $row['stud_index'] = 0;
                            }                             
                        }
                        $output['student_details'][] = $row;
                    }else{
                        $obj_index = array_search($obj, $obj_list);
                        if ($stripe_status == 'Y' && $stripe_account_state != 'N'){
                            if (!empty($row['payment_method_id'])) {
                                $card['payment_method_id'] = $row['payment_method_id'];
                                $card['credit_card_name'] = $row['stripe_card_name'];
                                $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                $card['postal_code'] = $row['buyer_postal_code'];
    //                            if(isset($output['student_details'])){
                            }
                        }else if ($wepay_status == 'Y'){
                            if (!empty($row['credit_card_id'])) {
                                $card['credit_card_id'] = $row['credit_card_id'];
                                $card['credit_card_status'] = $row['credit_card_status'];
                                $card['credit_card_name'] = $row['credit_card_name'];
                                $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                $card['postal_code'] = $row['buyer_postal_code'];
    //                            if(isset($output['student_details'])){    
                            }
                        }
                        $output['student_details'][$obj_index]['card_details'][] = $card; 
                    }
                }
                $out2 = array('status' => "Success", 'msg' => $output);
                if ($call_back == 0) {
                    // If success everythig is good send header as "OK" and user details
                    $this->response($this->json($out2), 200);
                } else {
                    return $out2;
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Student details doesn't exist.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200); // If no records "No Content" status
                } else {
                    return $error;
                }
            }
        }
//        $sql = sprintf("SELECT student_id, concat(`membership_registration_column_1`,' ', `membership_registration_column_2`) student_name, `buyer_name`, `buyer_email`, `buyer_phone`,     
//                        `buyer_postal_code`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`,       
//                        `membership_registration_column_1`, `membership_registration_column_2`, `membership_registration_column_3`, `membership_registration_column_4`, `membership_registration_column_5`,           
//                        `membership_registration_column_6`, `membership_registration_column_7`, `membership_registration_column_8`, `membership_registration_column_9`, `membership_registration_column_10`,         
//                        `participant_street`, `participant_city`, `participant_state`, `participant_zip`, `participant_country` ,created_dt , last_updt_dt  
//                        FROM `membership_registration` WHERE company_id = '%s' GROUP BY `buyer_email`, `credit_card_id`
//                        UNION
//                        SELECT student_id, concat(`event_registration_column_1`,' ', `event_registration_column_2`) student_name, er.`buyer_name`, er.`buyer_email`, er.`buyer_phone`,           
//                        er.`buyer_postal_code`, er.`credit_card_id`, er.`credit_card_status`, er.`credit_card_name`, er.`credit_card_expiration_month`, er.`credit_card_expiration_year`,     
//                        `event_registration_column_1`, `event_registration_column_2`, `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`,     
//                        `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`,  
//                        '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country ,created_dt, last_updt_dt
//                        FROM `event_registration` er
//                        WHERE company_id = '%s' GROUP BY `buyer_email`, `credit_card_id`
//                        UNION
//                        SELECT student_id, concat(`trial_registration_column_1`,' ', `trial_registration_column_2`) student_name, tr.`buyer_name`, tr.`buyer_email`, tr.`buyer_phone`,           
//                        tr.`buyer_postal_code`, tr.`credit_card_id`, tr.`credit_card_status`, tr.`credit_card_name`, tr.`credit_card_expiration_month`, tr.`credit_card_expiration_year`,     
//                        `trial_registration_column_1`, `trial_registration_column_2`, `trial_registration_column_3`, `trial_registration_column_4`, `trial_registration_column_5`,     
//                        `trial_registration_column_6`, `trial_registration_column_7`, `trial_registration_column_8`, `trial_registration_column_9`, `trial_registration_column_10`,  
//                        '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country ,created_dt, last_updt_dt
//                        FROM `trial_registration` tr
//                        WHERE company_id = '%s' GROUP BY `buyer_email`, `credit_card_id`
//                        ORDER BY `buyer_email` ASC ,`created_dt` DESC, `last_updt_dt` DESC", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
//        $result = mysqli_query($this->db, $sql);
////            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
////            log_info($this->json($error_log));
//            
//        if (!$result) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
//            log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
//        } else {
//            $num_of_rows = mysqli_num_rows($result);
//            if ($num_of_rows > 0) {
//                $output = $output['student_details'] = [];
//                $card = $stud_list = [];
//                while ($row = mysqli_fetch_assoc($result)) {
//                    $email = strtolower($row['buyer_email']);
//                    if(!in_array($email, $stud_list, true)){
//                        $stud_list[] = $email;
//                        if(!empty($row['credit_card_id'])){
//                            $card['credit_card_id'] = $row['credit_card_id'];
//                            $card['credit_card_status'] = $row['credit_card_status'];
//                            $card['credit_card_name'] = $row['credit_card_name'];
//                            $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
//                            $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
//                            $card['postal_code'] = $row['buyer_postal_code'];
//                            $row['card_details'][] = $card;
//                        }
//                        unset($row['credit_card_id']);
//                        unset($row['credit_card_status']);
//                        unset($row['credit_card_name']);
//                        unset($row['credit_card_expiration_month']);
//                        unset($row['credit_card_expiration_year']);
//                        $output['student_details'][] = $row;
//                    }else{
//                        if(!empty($row['credit_card_id'])){
//                            $card['credit_card_id'] = $row['credit_card_id'];
//                            $card['credit_card_status'] = $row['credit_card_status'];
//                            $card['credit_card_name'] = $row['credit_card_name'];
//                            $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
//                            $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
//                            $card['postal_code'] = $row['buyer_postal_code'];
//                            $output['student_details'][count($output['student_details'])-1]['card_details'][] = $card;
//                        }
//                    }
//                }
//                $out = array('status' => "Success", 'msg' => $output);
//                if($call_back==0){
//                    // If success everythig is good send header as "OK" and user details
//                    $this->response($this->json($out), 200);
//                } else {
//                    return $out;
//                }
//            } else {
//                $error = array('status' => "Failed", "msg" => "Student details doesn't exist.");
//                if($call_back==0){
//                $this->response($this->json($error), 200); // If no records "No Content" status
//                }else{
//                    return $error;
//                }
//            }
//        }
    }
    
     public function getparticipantInfoDetails($company_id, $trial_reg_id,$call_index) {
         $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
         $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
       $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $output = $lead_name_array = []; 
        
        $sql_reg = sprintf("SELECT t.`trial_registration_column_1` heading_1, t.`trial_registration_column_1_flag` heading_1_mf, t.`trial_registration_column_2` heading_2, t.`trial_registration_column_2_flag` heading_2_mf,
                t.`trial_registration_column_3` heading_3, t.`trial_registration_column_3_flag` heading_3_mf, t.`trial_registration_column_4` heading_4, t.`trial_registration_column_4_flag` heading_4_mf, 
                t.`trial_registration_column_5` heading_5, t.`trial_registration_column_5_flag` heading_5_mf, t.`trial_registration_column_6` heading_6, t.`trial_registration_column_6_flag` heading_6_mf,
                t.`trial_registration_column_7` heading_7, t.`trial_registration_column_7_flag` heading_7_mf, t.`trial_registration_column_8` heading_8, t.`trial_registration_column_8_flag` heading_8_mf, 
                t.`trial_registration_column_9` heading_9, t.`trial_registration_column_9_flag` heading_9_mf, t.`trial_registration_column_10` heading_10, t.`trial_registration_column_10_flag` heading_10_mf,
                t.`trial_lead_source_1`,t.`trial_lead_source_2`,t.`trial_lead_source_3`,t.`trial_lead_source_4`,t.`trial_lead_source_5`,
                t.`trial_lead_source_6`,t.`trial_lead_source_7`,t.`trial_lead_source_8`,t.`trial_lead_source_9`,t.`trial_lead_source_10`,
                tr.`trial_registration_column_1`, tr.`trial_registration_column_2`, tr.`trial_registration_column_3`, IF(tr.`trial_registration_column_3` = '0000-00-00'
                    OR tr.`trial_registration_column_3` IS NULL,
                '',TRUNCATE(DATEDIFF($currdate_add,tr.`trial_registration_column_3`) / 365.25,0)) age,
                tr.`trial_registration_column_4`, tr.`trial_registration_column_5`, tr.`trial_registration_column_6`, tr.`trial_registration_column_7`,
                tr.`trial_registration_column_8`, tr.`trial_registration_column_9`, tr.`trial_registration_column_10`,tr.`trial_participant_street`, tr.`trial_participant_city`, tr.`trial_participant_state`, tr.`trial_participant_zip`, tr.`trial_participant_country`,
                 concat(tr.`buyer_first_name`,' ',tr.`buyer_last_name`) buyer_name, tr.`buyer_first_name`, tr.`buyer_last_name`,tr.`buyer_phone`, tr.`buyer_email`, tr.`buyer_postal_code` FROM `trial` t 
                LEFT JOIN `trial_registration` tr ON t.`trial_id` = tr.`trial_id` AND t.`company_id` = tr.`company_id`   WHERE tr.company_id='%s' AND tr.trial_reg_id= '%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id));
        $result_reg_details = mysqli_query($this->db, $sql_reg);
        if (!$result_reg_details) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_reg");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_reg_details);
            if ($num_rows > 0) {
                $output = mysqli_fetch_assoc($result_reg_details);
                 $temp_lead_field = "trial_lead_source_";
                    for($z=1;$z<=10;$z++){ //collect registration fields as array
                        $y=$z-1;
                        $reg_lead = $temp_lead_field."$z";      //for filed names
                       if($output[$reg_lead]!=''){
                            $lead_name_array[] = array("lead_col_name"=>$output[$reg_lead],"lead_col_index"=>$y);
                        }
                    } 
                  $output['lead_source'] = $lead_name_array;
                $out = array('status' => "Success", 'msg' => $output);
                 date_default_timezone_set($curr_time_zone);
                if($call_index==0){
                    $this->response($this->json($out), 200);
                }elseif($call_index==1){
                    return $out;
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Participant details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status  
            }
        }
    }
    
     public function getTrialHistoryDetails($company_id, $trial_reg_id, $call_back){    //call_back 0 - echo & exit, 1 - return
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $activity_dt_tm = " IF(TIME(`activity_date_time`)!='00:00:00', DATE(CONVERT_TZ(`activity_date_time`,$tzadd_add,'$new_timezone')), DATE(`activity_date_time`))";
        
        
        $output = []; 
        $sql_reg = sprintf("SELECT `trial_history_id`,`activity_type`, `activity_text`, $activity_dt_tm activity_date FROM `trial_history`
                WHERE company_id='%s' AND trial_reg_id ='%s' AND `deleted_flag`!='D' AND `activity_type`!='buyer name edit' ORDER BY `activity_date_time`, `trial_history_id`", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id));
        $result_reg_details = mysqli_query($this->db, $sql_reg);
        if (!$result_reg_details) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_reg");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_reg_details);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_reg_details)) {                   
                    $output[] = $row;
//                    $output['activity_date'] = $row['activity_date_time'];
                }
                $out = array('status' => "Success", 'msg' => $output);
                if($call_back==0){
                    $this->response($this->json($out), 200);
                }else{
                    return $out;
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Trial program history details doesn't exist.");
                if($call_back==0){
                    $this->response($this->json($error), 200); // If no records "No Content" status  
                }else{
                    return $error;
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
     public function sendGroupPushMessageForTrial($company_id, $subject, $message, $trial_reg_id, $also_send_mail, $attached_file, $file_name, $trial_status, $all_select_flag, $search,$trial_id,$trial_startdate_limit,$trial_enddate_limit,$trial_days_type){
        $ios = $android =[]; 
        if($all_select_flag=='Y'){
         $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        
        $tday_text = '';
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
//        $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $last_updt_date = "  DATE(CONVERT_TZ(tr.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $date_format = "'%b %d, %Y'";
//        $next_amount_date = ', %s nad';
//        $first_amount_date = ', %s fad';
        $created_date = "  DATE(CONVERT_TZ(tr.created_dt,$tzadd_add,'$new_timezone'))";
        $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $filter_date_column = "$created_date";
            if ($trial_days_type == 1) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 2) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 3) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 4) {
                $tday_text = "and DATE($filter_date_column) BETWEEN '" . $trial_startdate_limit . "' AND '" . $trial_enddate_limit . "'";
            } elseif ($trial_days_type == 5) {
                $tday_text = "";
            }
        $trial_reg_id = [];
        
         if(!empty($search)){
             $s_text = " and ( buyer_name like  '%$search%'  or buyer_email like  '%$search%' or `buyer_phone` like  '%$search%' or  tr.`trial_status` like '%$search%'
                    or tr.`trial_registration_column_1` like  '%$search%' or tr.`trial_registration_column_2` like  '%$search%' or   DATE_FORMAT(tr.`start_date`, '%b %d, %Y')  like  '%$search%' 
                     or  DATE_FORMAT(tr.`end_date`, '%b %d, %Y')  like  '%$search%' or  DATE_FORMAT(tr.`registration_date`, '%b %d, %Y')  like  '%$search%' or  t.`trial_title` like  '%$search%'  or  tr.`trial_registration_column_4` like  '%$search%' $tday_text)";
         }else{
             $s_text='';
         }
         if ($trial_status == 'A') { //ORDERS TAB
            $trial_tab = "tr.`trial_status` in ('A')";
        } else if ($trial_status == 'E') {
            $trial_tab = "tr.`trial_status` in ('E')";
        } else if ($trial_status == 'D') {
            $trial_tab = "tr.`trial_status` in ('D')";
        } else if ($trial_status == 'C') {
            $trial_tab = "tr.`trial_status` in ('C')";
        }
        
        if(!empty($trial_id)){
            $trial_manage = " AND tr.`trial_id` = $trial_id ";
        }else{
            $trial_manage = '';
        }

        $query = sprintf("SELECT tr.`trial_reg_id` as id, tr.`company_id`, tr.`trial_id`, tr.`student_id`, `buyer_name`, CONCAT(tr.`trial_registration_column_1`, ' ', tr.`trial_registration_column_2`) participant_name,
                IF(trial_reg_type_user='P', 'Control Panel', IF(trial_reg_type_user='M', 'Mobile App', IF(trial_reg_type_user='U', 'Website URL', ''))) trial_reg_type_user,
                `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(tr.`registration_date`,%s) registration_date, `payment_type`, tr.`payment_amount`,
                tr.`paid_amount` paid_amount, DATE_FORMAT(($last_updt_date),%s) last_updt_dt,
                IF(tr.`trial_status`='A','Active',IF(tr.`trial_status`='E','Enrolled',IF(tr.`trial_status`='C','Cancelled','Didnotstart'))) trial_status, tr.`trial_registration_column_4` leadsource, DATE_FORMAT(tr.`start_date`,%s) start_date, DATE_FORMAT(tr.`end_date`,%s) end_date,t.`trial_title` trial_program
                FROM `trial_registration` tr left join `trial` t on tr.`company_id`=t.`company_id` AND tr.`trial_id`=t.`trial_id`  WHERE tr.`company_id`='%s' %s  %s AND $trial_tab ", $date_format, $date_format, $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id), $trial_manage, $s_text);
        $res1 = mysqli_query($this->db, $query);
            log_info("search   "+$query);
            if (!$res1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", 'query' => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $num = mysqli_num_rows($res1);
                if($num>0){
                    while($r = mysqli_fetch_assoc($res1)){
                        $trial_reg_id[] = $r;
                    }
                }
            }
        }
        
        for($i=0; $i<count($trial_reg_id); $i++){
            $output1[] = $trial_reg_id[$i]['id'];
        }
        $reg_id_list = implode(",", $output1);
            
        $query1 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
               IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id)
               WHERE `company_id`='%s' 
                       AND (`student_id` IN 
                       (SELECT `student_id` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s))
                       OR `student_email` IN 
                       (SELECT `buyer_email` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
            AND push_device_id IS NOT NULL AND trim(push_device_id)!='' order by app_id)t1 
                LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", 
               mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list),
               mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list), mysqli_real_escape_string($this->db,$company_id));
        $result1 = mysqli_query($this->db, $query1);
        
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_of_rows = mysqli_num_rows($result1);
            if ($num_of_rows > 0) {
                $student_id = [];
                while ($row = mysqli_fetch_assoc($result1)) {
                    $output[] = $row;
                    if(!is_null($row['student_id']) && !empty($row['student_id'])){
                        $student_id[] = $row['student_id'];
                    }
                }
                $msg_id = $this->insertPushMessage($company_id, $student_id, $message);
                $update_message = sprintf("UPDATE `message` SET `push_delivered_date`=NOW() WHERE `message_id`='%s' and `company_id`='%s'", $msg_id, $company_id);
                $result_update_message = mysqli_query($this->db, $update_message);
                if (!$result_update_message) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                $query2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                        (SELECT s.company_id,s.student_id, lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) as  type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
                       IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props FROM `student` s   LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id)
                       WHERE `company_id`='%s' 
                               AND (`student_id` IN 
                               (SELECT `student_id` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s))
                               OR `student_email` IN 
                               (SELECT `buyer_email` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s))) AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
                    AND push_device_id IS NOT NULL AND trim(push_device_id)!='' order by app_id)t1 
                        LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
                        where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' group by student_id) t2 USING(company_id, student_id)", 
                       mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list),
                       mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list), mysqli_real_escape_string($this->db,$company_id));
                $result2 = mysqli_query($this->db, $query2);

                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }else{
                    $num_of_rows2 = mysqli_num_rows($result2);
                    if ($num_of_rows2 > 0) {
                        $output = [];
                        while ($row2 = mysqli_fetch_assoc($result2)) {
                            $output[] = $row2;
                        }
                    }
                }
                $r_rand = mt_rand(1,9999);
                $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();  //for creating pem file from db
                if (!file_exists($rfolder_name)) {
                    $oldmask = umask(0);
                    mkdir($rfolder_name, 0777, true);
                    umask($oldmask);
                }else{ // to make directory once.
                    $r = 0;
                    while ($r < 100) {
                        $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();
                        if (!file_exists($rfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($rfolder_name, 0777, true);
                            umask($oldmask);
                            break;
                        }
                     $r++;
                    }
                }
                $ios_socket_url ='';
                $ios_app_id_array = [];
                $host = $_SERVER['HTTP_HOST'];
                if (strpos($host, 'mystudio.academy') !== false) {
//                if (strpos($host, 'www.mystudio.academy') !== false || strpos($host, 'stage.mystudio.academy') !== false) {
                    //production
                    $ios_socket_url = "ssl://gateway.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];
                        
                        if ($devicetype == 'I') {                            
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['prod_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }else{
                    //development
                    $ios_socket_url = "ssl://gateway.sandbox.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];                        
                        
                        if ($devicetype == 'I') {
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['dev_pem'];
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }
                $send_count = $this->sendpush($message, $ios, $android, $ios_socket_url);
                $update_message = sprintf("UPDATE `message` SET `no_of_users`='%s' WHERE `message_id`='%s' and `company_id`='%s'", $send_count, $msg_id, $company_id);
                $result_update_message = mysqli_query($this->db, $update_message);
                if (!$result_update_message) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                $check = $this->deleteDirectory("$rfolder_name");// delete directory with content
                $out = array('status' => "Success", 'msg' => "Push Message sent successfully. ");
                if($also_send_mail!='Y'){
                    $this->response($this->json($out), 200);
                }else{
                    $this->sendGroupPushMailForTrial($company_id, $subject, $message, $trial_reg_id, $out['msg'], $attached_file, $file_name,$trial_status, $all_select_flag,$search,$trial_id,$trial_startdate_limit,$trial_enddate_limit,$trial_days_type);
                }
//                return $out;
            }else{
                $this->insertPushMessageForNoSelectedUsers($company_id, $message);
                $out = array('status' => "Failed", 'msg' => "Push Message sent successfully ");
                if($also_send_mail!='Y'){
                    $this->response($this->json($out), 200);
                }else{
                    $this->sendGroupPushMailForTrial($company_id, $subject, $message, $trial_reg_id, $out['msg'], $attached_file, $file_name,$trial_status, $all_select_flag,$search,$trial_id,$trial_startdate_limit,$trial_enddate_limit,$trial_days_type);
                }
//                return $out;
            }
        }
    }
    
    public function insertPushMessageForNoSelectedUsers($company_id, $message_text){
        
        $sql1 = sprintf("INSERT INTO `message` (`message_text`, `company_id`, `message_to`, `push_from`) VALUES ('%s', '%s', 'A', 'TP')", mysqli_real_escape_string($this->db,$message_text), $company_id);
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
    }
    
    public function insertPushMessage($company_id, $student_id, $message_text){
        $message_id = '';
//        if(count($student_id)==1){
//            $message_to = 'I';
//            $stud_id = $student_id[0];
//        }else
        if(count($student_id)==0){
            $message_to = 'A';
            $stud_id = 'NULL';
        }else{
            $message_to = 'G';
            $stud_id = 'NULL';
        }
        
        $sql1 = sprintf("INSERT INTO `message` (`message_text`, `company_id`, `message_to`, `student_id`, `push_from`) VALUES ('%s', '%s', '%s', %s, 'TP')", mysqli_real_escape_string($this->db,$message_text), $company_id, $message_to, $stud_id);
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $message_id = mysqli_insert_id($this->db);
            if($message_id>0 && $message_to!='I'){
                $this->sendGroupMessage($company_id, $message_id, $student_id);
            }
        }
        return $message_id;
    }
    
    //send group message
    protected function sendGroupMessage($company_id, $message_id, $student_id){
        if(empty($student_id)){
            $sql = sprintf("SELECT student_id FROM `student` WHERE `company_id`='%s' AND `deleted_flag`!='Y'
                        AND student_id NOT IN (SELECT student_id FROM message_mapping WHERE message_id='%s')", 
                    mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$message_id));
            $result = mysqli_query($this->db, $sql);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $student_id=[];
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows > 0) {
                    while ($row = mysqli_fetch_assoc($result)) {
                        $student_id[] = $row['student_id'];
                    }
                }
            }
        }
        
        for($i=0; $i<count($student_id);$i++){
            $insert_query = sprintf("INSERT INTO `message_mapping`(`company_id`, `message_id`, `student_id`) VALUES('%s', '%s', '%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $message_id), mysqli_real_escape_string($this->db, $student_id[$i]));
            $insert_result = mysqli_query($this->db, $insert_query);
            if(!$insert_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
     public function sendGroupPushMailForTrial($company_id, $subject, $message, $trial_reg_id, $push_msg, $attached_file, $file_name,$trial_status,$all_select_flag,$search,$trial_id,$trial_startdate_limit,$trial_enddate_limit,$trial_days_type){
        if(empty($subject)){
            $subject = "Message";
        }
        $output1 = [];
        if($all_select_flag=='Y'){
         $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        
        $tday_text = '';
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
//        $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $last_updt_date = "  DATE(CONVERT_TZ(tr.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $date_format = "'%b %d, %Y'";
//        $next_amount_date = ', %s nad';
//        $first_amount_date = ', %s fad';
        $created_date = "  DATE(CONVERT_TZ(tr.created_dt,$tzadd_add,'$new_timezone'))";
        $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $filter_date_column = "$created_date";
            if ($trial_days_type == 1) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 2) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 3) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 4) {
                $tday_text = "and DATE($filter_date_column) BETWEEN '" . $trial_startdate_limit . "' AND '" . $trial_enddate_limit . "'";
            } elseif ($trial_days_type == 5) {
                $tday_text = "";
            }
        $trial_reg_id = [];
        
         if(!empty($search)){
             $s_text = " and ( buyer_name like  '%$search%'  or buyer_email like  '%$search%' or `buyer_phone` like  '%$search%' or  tr.`trial_status` like '%$search%'
                    or tr.`trial_registration_column_1` like  '%$search%' or tr.`trial_registration_column_2` like  '%$search%' or   DATE_FORMAT(tr.`start_date`, '%b %d, %Y')  like  '%$search%' 
                     or  DATE_FORMAT(tr.`end_date`, '%b %d, %Y')  like  '%$search%' or  DATE_FORMAT(tr.`registration_date`, '%b %d, %Y')  like  '%$search%' or  t.`trial_title` like  '%$search%'  or  tr.`trial_registration_column_4` like  '%$search%' $tday_text)";
         }else{
             $s_text='';
         }
         if ($trial_status == 'A') { //ORDERS TAB
            $trial_tab = "tr.`trial_status` in ('A')";
        } else if ($trial_status == 'E') {
            $trial_tab = "tr.`trial_status` in ('E')";
        } else if ($trial_status == 'D') {
            $trial_tab = "tr.`trial_status` in ('D')";
        } else if ($trial_status == 'C') {
            $trial_tab = "tr.`trial_status` in ('C')";
        }
        
        if(!empty($trial_id)){
            $trial_manage = " AND tr.`trial_id` = $trial_id ";
        }else{
            $trial_manage = '';
        }

        $query = sprintf("SELECT tr.`trial_reg_id` as id, tr.`company_id`, tr.`trial_id`, tr.`student_id`, `buyer_name`, CONCAT(tr.`trial_registration_column_1`, ' ', tr.`trial_registration_column_2`) participant_name,
                IF(trial_reg_type_user='P', 'Control Panel', IF(trial_reg_type_user='M', 'Mobile App', IF(trial_reg_type_user='U', 'Website URL', ''))) trial_reg_type_user,
                `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(tr.`registration_date`,%s) registration_date, `payment_type`, tr.`payment_amount`,
                tr.`paid_amount` paid_amount, DATE_FORMAT(($last_updt_date),%s) last_updt_dt, 
                IF(tr.`trial_status`='A','Active',IF(tr.`trial_status`='E','Enrolled',IF(tr.`trial_status`='C','Cancelled','Didnotstart'))) trial_status, tr.`trial_registration_column_4` leadsource, DATE_FORMAT(tr.`start_date`,%s) start_date, DATE_FORMAT(tr.`end_date`,%s) end_date,t.`trial_title` trial_program
                FROM `trial_registration` tr left join `trial` t on tr.`company_id`=t.`company_id` AND tr.`trial_id`=t.`trial_id`  WHERE tr.`company_id`='%s' %s  %s AND $trial_tab ", $date_format, $date_format, $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id), $trial_manage, $s_text);
        $result = mysqli_query($this->db, $query);
            $res1 = mysqli_query($this->db, $query);
            log_info("search   "+$query);
            if (!$res1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", 'query' => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $num = mysqli_num_rows($res1);
                if($num>0){
                    while($r = mysqli_fetch_assoc($res1)){
                        $trial_reg_id[] = $r;
                    }
                }
            }
        }
        for($i=0; $i<count($trial_reg_id); $i++){
            $output1[] = $trial_reg_id[$i]['id'];
        }
        $reg_id_list = implode(",", $output1);
           $countForBouncedEmail=$this->checkBouncedEmailCount($reg_id_list);

//           $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, s.`student_email`,s.`student_cc_email` 
//                FROM  `student` s LEFT JOIN  `company` c ON c.`company_id` = s.`company_id`  LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email`
//                WHERE `student_id` in (SELECT `student_id` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s)) and s.`deleted_flag`!='Y' and  s.`student_email` != IFNULL(b.`bounced_mail`, '')  GROUP BY student_email",
//                   mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$reg_id_list));
        $sql = sprintf("SELECT c.`company_name`, c.`email_id`, c.`upgrade_status`, t1.* from
                (select s.`student_email`,s.`student_cc_email` 
                FROM  `student` s  LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=s.`student_email` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
                WHERE `student_id` in (SELECT `student_id` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id` in (%s))  and s.`deleted_flag`!='Y' and  s.`student_email` != IFNULL(b.`bounced_mail`, '') GROUP BY student_email
                UNION 
                SELECT `buyer_email` as student_email,'' student_cc_email FROM  `trial_registration` tr 
                 LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=tr.`company_id`) left join `student` s  on s.student_id = tr.student_id
                WHERE tr.`company_id`='%s' and s.`deleted_flag`!='Y'  AND `trial_reg_id` in (%s) and  `buyer_email` != IFNULL(b.`bounced_mail`, '') GROUP BY buyer_email) t1,company c WHERE c.`company_id`='%s'  group by student_email ", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list),
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list),mysqli_real_escape_string($this->db,$company_id));
//        log_info($sql);
//        $sql = sprintf("SELECT s.`student_email`,s.`student_cc_email`, c.`company_name`, c.`email_id`, c.`upgrade_status` 
//                    FROM `student` s LEFT JOIN  `company` c ON s.`company_id` = c.`company_id` 
//                    WHERE `student_id` in (SELECT `student_id` FROM `trial_registration` 
//                    WHERE `company_id`='%s' AND `trial_reg_id` in (%s)) and s.`deleted_flag`!='Y' GROUP BY student_email", 
//                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list));

        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $failed_count = $countForBouncedEmail['failed_count'];
             $send_count=0;
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $cmp_name = $reply_to = '';
                $initial_check = 0;
                while ($row = mysqli_fetch_assoc($result)) {
                    if($initial_check==0){
                        $cmp_name = $row['company_name'];
                        $reply_to = $row['email_id'];
                        $initial_check++;
                    }
                    $output[] = $row;
                }
                $footer_detail = $this->getunsubscribefooter($company_id);
                for ($idx = 0; $idx < count($output); $idx++) {
                    $obj = (Array) $output[$idx];
                    $user_emailid = $obj["student_email"];
                    $cc_email_list = $obj['student_cc_email'];
                    //echo "******count******".$idx;
                    $sendEmail_status = $this->sendEmail($user_emailid, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file,$cc_email_list,$company_id,'',$footer_detail);
                    if($sendEmail_status['status']=="true"){
                          $send_count++;
                        $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200);
                    }else{
                        $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                        $failed_count++;
                        log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200); // If no records "No Content" status
                    }
                }
                if($failed_count == 0){
                    $push_msg="Email(s) Sent Successfully.";
                }
                elseif($send_count==0){
                    $push_msg=" Email(s) failed due to invalid address.";
                }else{
                    
//                    $push_msg=$countForBouncedEmail['Total_count']-$send_count.' of '.$countForBouncedEmail['Total_count'].' email sending failed. Others sent successfully';
                    $push_msg= $send_count ." sent. ".$failed_count." failed due to invalid address." ;
                }
//                if(empty($push_msg)){
//                    $push_msg="Email Sent Successfully";
//                }
                $msg = array('status' => "Success", "msg" => $push_msg);
                $this->response($this->json($msg), 200);
            } else {
                 if($send_count==0){
                    $push_msg="Email(s) failed due to invalid address.";
                }
                $error = array('status' => "Failed", "msg" => $push_msg);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
     protected function sendEmail($to, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file,$cc_email_list,$company_id,$type,$footer_detail) {
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }            
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
            $mail->Subject = $subject;
            if (!empty($company_id) && $type != 'subscribe') {
                $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                $curr_loc = implode('/', explode('/', $curr_loc, -4));
//                $tomail = $to;
                if (!empty(trim($cc_email_list))) {
                    $tomail = base64_encode($to . "," . "$cc_email_list");
                } else {
                    $tomail = base64_encode($to);
                }
                $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/EmailSubscription.php?mail=$tomail?comp_id=$company_id?type=U";
                $message = "
            <html>
            <body>
                <p>$message</p><br><br>
                    <footer style='font-size: 13px;color: #746f6f;'>
                    <center>
                    <p>$footer_detail
                    </p>
                    <a href='$activation_link' target='_blank' style='color: #746f6f; !important;text-decoration: none !important;'><u style='color: #746f6f;'>Unsubscribe $to</u></a>
                    </center>
                    </footer>
            </body>
            </html>
            ";
            }
            $mail->Body = $message;
            if(!empty($file_name)){
                $mail->AddStringAttachment(base64_decode($attached_file), $file_name, 'base64');
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $verify_email_for_bounce = $this->verifyBouncedEmailForCC($cc_addresses[$init], $company_id);
                         if($verify_email_for_bounce==1){
                               $mail->AddCC($cc_addresses[$init]);
                         }
//                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
     public function createTrialSelectionCSV($company_id,$trial_reg_id, $trial_status, $all_select_flag,$search,$trial_id,$trial_startdate_limit,$trial_enddate_limit,$trial_days_type){
        if ($all_select_flag == 'Y') {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        
        $tday_text = '';
       if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
//        $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $last_updt_date = "  DATE(CONVERT_TZ(tr.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $date_format = "'%b %d, %Y'";
//        $next_amount_date = ', %s nad';
//        $first_amount_date = ', %s fad';
        $created_date = "  DATE(CONVERT_TZ(tr.created_dt,$tzadd_add,'$new_timezone'))";
        $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $filter_date_column = "$created_date";
            if ($trial_days_type == 1) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 2) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 3) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 4) {
                $tday_text = "and DATE($filter_date_column) BETWEEN '" . $trial_startdate_limit . "' AND '" . $trial_enddate_limit . "'";
            } elseif ($trial_days_type == 5) {
                $tday_text = "";
            }
        $trial_reg_id = [];
        
         if(!empty($search)){
             $s_text = " and ( buyer_name like  '%$search%'  or buyer_email like  '%$search%' or `buyer_phone` like  '%$search%' or  tr.`trial_status` like '%$search%'
                    or tr.`trial_registration_column_1` like  '%$search%' or tr.`trial_registration_column_2` like  '%$search%' or   DATE_FORMAT(tr.`start_date`, '%b %d, %Y')  like  '%$search%' 
                     or  DATE_FORMAT(tr.`end_date`, '%b %d, %Y')  like  '%$search%' or  DATE_FORMAT(tr.`registration_date`, '%b %d, %Y')  like  '%$search%' or  t.`trial_title` like  '%$search%'  or  tr.`trial_registration_column_4` like  '%$search%' $tday_text)";
         }else{
             $s_text='';
         }
         
        if ($trial_status == 'A') { //ORDERS TAB
            $trial_tab = "tr.`trial_status` in ('A')";
        } else if ($trial_status == 'E') {
            $trial_tab = "tr.`trial_status` in ('E')";
        } else if ($trial_status == 'D') {
            $trial_tab = "tr.`trial_status` in ('D')";
        } else if ($trial_status == 'C') {
            $trial_tab = "tr.`trial_status` in ('C')";
        }
        
         if(!empty($trial_id)){
            $trial_manage = " AND tr.`trial_id` = $trial_id ";
        }else{
            $trial_manage = '';
        }

        $query = sprintf("SELECT tr.`trial_reg_id` as id, tr.`company_id`, tr.`trial_id`, tr.`student_id`, `buyer_name`, CONCAT(tr.`trial_registration_column_1`, ' ', tr.`trial_registration_column_2`) participant_name,
                IF(trial_reg_type_user='P', 'Control Panel', IF(trial_reg_type_user='M', 'Mobile App', IF(trial_reg_type_user='U', 'Website URL', ''))) trial_reg_type_user,
                `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(tr.`registration_date`,%s) registration_date, `payment_type`, tr.`payment_amount`,
                tr.`paid_amount` paid_amount, DATE_FORMAT(($last_updt_date),%s) last_updt_dt, 
                IF(tr.`trial_status`='A','Active',IF(tr.`trial_status`='E','Enrolled',IF(tr.`trial_status`='C','Cancelled','Didnotstart'))) trial_status, tr.`trial_registration_column_4` leadsource, DATE_FORMAT(tr.`start_date`,%s) start_date, DATE_FORMAT(tr.`end_date`,%s) end_date,t.`trial_title` trial_program
                FROM `trial_registration` tr left join `trial` t on tr.`company_id`=t.`company_id` AND tr.`trial_id`=t.`trial_id`  WHERE tr.`company_id`='%s' %s  %s AND $trial_tab ", $date_format, $date_format, $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id), $trial_manage, $s_text);
        $res1 = mysqli_query($this->db, $query);
//            log_info("search   ".$query);
            if (!$res1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", 'query' => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $num = mysqli_num_rows($res1);
                if($num>0){
                    while($r = mysqli_fetch_assoc($res1)){
                        $trial_reg_id[] = $r;
                    }
                }
            }
        }
        for($i=0; $i<count($trial_reg_id); $i++){
            $output1[] = $trial_reg_id[$i]['id'];
        }
        $reg_id_list = implode(",", $output1);
        

       $title = $subtitle = $start = $end = "";$output = [];
        $cus_field_1 = $cus_field_2 = $cus_field_3 = $cus_field_4 = $cus_field_5 = $cus_field_6 = $cus_field_7 = $cus_field_8 = $cus_field_9 = $cus_field_10 = "";
        $select_query = sprintf("SELECT t.`trial_status`, t.`trial_title`, t.`trial_subtitle`, t.`trial_desc`, t.`trial_prog_url`, t.`trial_welcome_url`, t.`trial_banner_img_url`, t.`trial_banner_img_content`, t.`trial_waiver_policies`, t.`registrations_count`, t.`net_sales`, t.`processing_fee_type`, t.`price_amount`, 
            t.`program_length_type`, t.`program_length`, t.`trial_sort_order`, concat(t.`trial_registration_column_1`,' ', t.`trial_registration_column_2`) participant_name, t.`trial_registration_column_3`,  
            t.`trial_registration_column_4`, t.`trial_registration_column_5`,  t.`trial_registration_column_6`,  t.`trial_registration_column_7`,  t.`trial_registration_column_8`,
            t.`trial_registration_column_9`, t.`trial_registration_column_10`, t.`trial_lead_source_1`, t.`trial_lead_source_2`, t.`trial_lead_source_3`, t.`trial_lead_source_4`, t.`trial_lead_source_5`,
            t.`trial_lead_source_6`, t.`trial_lead_source_7`, t.`trial_lead_source_8`, t.`trial_lead_source_9`, t.`trial_lead_source_10`, tr.`trial_status` reg_status,concat(tr.`buyer_last_name`,', ',tr.`buyer_first_name`) buyer_name, tr.`buyer_email`, tr.`buyer_phone`, tr.`buyer_postal_code`, tr.`registration_date`, tr.`registration_amount`,tr.`payment_amount`,
           tr.`payment_amount`, tr.`paid_amount`, tr.`refunded_amount`,concat(tr.`trial_registration_column_2`,', ',tr.`trial_registration_column_1`) participant, tr.`trial_registration_column_3` cus_field_3,
            tr.`trial_registration_column_4` cus_field_4, tr.`trial_registration_column_5` cus_field_5, tr.`trial_registration_column_6` cus_field_6, tr.`trial_registration_column_7` cus_field_7, tr.`trial_registration_column_8` cus_field_8, tr.`trial_registration_column_9` cus_field_9,
            tr.`trial_registration_column_10` cus_field_10,tr.`start_date`,tr.`end_date` FROM `trial` t,`trial_registration` tr WHERE t.`company_id`='%s' AND tr.`trial_reg_id` in (%s)
             AND t.`trial_status`!='D' AND t.`trial_id`=tr.`trial_id`", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list));
        $result_select = mysqli_query($this->db, $select_query);
//         log_info("search   ".$select_query);
        if(!$result_select){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result_select);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_select)) {
                    $trial_details = $row;
                    $title = $row['trial_title'];
                    $subtitle = $row['trial_subtitle'];
                    if($row['program_length_type'] == 'D'){
                       $row['length'] = "$row[program_length]" . " " . "day";
                    }else{
                        $row['length'] = "$row[program_length]" . " " . "week";
                    }
                    
//                $start_date = $row['start_date'];
//                $end_date = $row['end_date'];
                    $output[] = $row;
                }
            } else {
                $error = array("status" => "Failed", "msg" => "Trial program details doesn't exist.");
                $this->response($this->json($error), 401);
            }
        }

        $header = $body = '';
        $header .= "Participant Name,Trial Program Title,Program Length,";
        
        $temp_reg_field = "trial_registration_column_";
        for ($z = 3; $z <= 10; $z++) { //collect registration fields as array
            $reg_field = $temp_reg_field . "$z";
            if ($trial_details[$reg_field] != '') {
                  $header = $header .'"'. str_replace('"', "''", $trial_details[$reg_field]) .'"'. ",";
            }
        }
        

        $header = $header . "Buyer Name,Buyer Email,Buyer Phone Number,Buyer Postal code,Registration Date,Start Date,End Date,Price Amount,"
                . "Total Paid by User to date\r\n";

        for ($row_index = 0; $row_index < $num_rows; $row_index++) {
            if (!empty(trim($output[$row_index]['trial_title']))) {
                $trial_title = str_replace('"', "''", $output[$row_index]['trial_title']);
            } else {
                $trial_title = "";
            }
           
           
            $body = $body . '"' . $output[$row_index]['participant'].'"' . ',' . $trial_title .  ","  . $output[$row_index]['length'] . ",";
            $temp_reg_field_value = "cus_field_";
            for ($z = 3; $z <= 10; $z++) { //collect registration fields as array
                $reg_field = $temp_reg_field . "$z";
                $reg_field_value = $temp_reg_field_value . "$z"; //for filed names
                if ($trial_details[$reg_field] != '') {
                    $body = $body . '"' . str_replace('"', "''", $output[$row_index][$reg_field_value]) . '"' . ",";
                }
            }

            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_name']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_email']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_phone']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_postal_code']) . '"' . ",";

            if (!empty(trim($output[$row_index]['registration_date']))) {
                if ("0000-00-00" == $output[$row_index]['registration_date']) {
                    $registration_date = "";
                } else {
                    $reg_date = $output[$row_index]['registration_date'];
                    $registration_date = date('Y-m-d', strtotime($reg_date));
                }
            } else {
                $registration_date = "";
            }
            if (!empty(trim($output[$row_index]['start_date']))) {
                if ("0000-00-00" == $output[$row_index]['start_date']) {
                    $start_date = "";
                } else {
                    $reg_date = $output[$row_index]['start_date'];
                    $start_date = date('Y-m-d', strtotime($reg_date));
                }
            } else {
                $start_date = "";
            }
            if (!empty(trim($output[$row_index]['end_date']))) {
                if ("0000-00-00" == $output[$row_index]['end_date']) {
                    $end_date = "";
                } else {
                    $reg_date = $output[$row_index]['end_date'];
                    $end_date = date('Y-m-d', strtotime($reg_date));
                }
            } else {
                $end_date = "";
            }

            $body = $body . $registration_date . ",";
            $body = $body . $start_date . ",";
            $body = $body . $end_date . ",";
            $body = $body . $output[$row_index]['price_amount'] . ",";

            $total_paid_by_userToDate = $output[$row_index]['payment_amount'];
            $body = $body . $total_paid_by_userToDate . "\r\n";

        }
        
        ob_clean();
        ob_start();
        $file = "../../../uploads/dummy1.csv";
        $ifp = fopen($file, "w");
        fwrite($ifp, $header);
        fwrite($ifp, $body);
        fclose($ifp);
        header('Content-Type: application/x-download; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        readfile($file);
        unlink($file);
        exit(); 
        date_default_timezone_set($curr_time_zone);
    }
    
     public function addorupdateTrialHistoryNote($company_id,$trial_reg_id,$student_id,$status,$history_id,$note_text){
         $curr_date = gmdate('Y-m-d H:i:s');
        if($status=='add'){
            $query = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, 'note'), 
                    mysqli_real_escape_string($this->db, $note_text), mysqli_real_escape_string($this->db, $curr_date));
            $res_msg = "Note added successfully.";
        }else{
            $sql = sprintf("SELECT * FROM `trial_history` WHERE `trial_history_id`='%s' AND `company_id`='%s' AND `trial_reg_id`='%s'",
                    mysqli_real_escape_string($this->db, $history_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id));
            $res = mysqli_query($this->db, $sql);
            if (!$res) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.","query" => "$sql");
                $this->response($this->json($error), 200);
            }else{
                $num_rows = mysqli_num_rows($res);
                if($num_rows==0){
                    $error = array('status' => "Failed", "msg" => "History note details not available.");
                    $this->response($this->json($error), 200);
                }
            }
            if($status=='update'){
                $query = sprintf("UPDATE `trial_history` SET `activity_text`='%s', `activity_date_time`='%s' WHERE `trial_history_id`='%s' AND `company_id`='%s' AND `trial_reg_id`='%s'",
                        mysqli_real_escape_string($this->db, $note_text), mysqli_real_escape_string($this->db, $curr_date),
                        mysqli_real_escape_string($this->db, $history_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id));
                $res_msg = "Note updated successfully.";
            }elseif($status=='delete'){
                $query = sprintf("UPDATE `trial_history` SET `deleted_flag`='D' WHERE `trial_history_id`='%s' AND `company_id`='%s' AND `trial_reg_id`='%s'",
                        mysqli_real_escape_string($this->db, $history_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id));
                $res_msg = "Note deleted successfully.";
            }
        }
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $trial_history = $this->getTrialHistoryDetails($company_id, $trial_reg_id,1);
            $log = array("status" => "Success", "msg" => $res_msg, "trial_history" => $trial_history);
            $this->response($this->json($log),200);
        }
    }
    
    public function getpaymentHistoryDetails($company_id, $reg_id, $call_back) {
        
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $payment_date = " IF(TIME(tp.payment_date)!='00:00:00', DATE(CONVERT_TZ(tp.payment_date,$tzadd_add,'$new_timezone')), DATE(tp.payment_date)) ";
        $created_date = " IF(TIME(tp.created_dt)!='00:00:00', DATE(CONVERT_TZ(tp.created_dt,$tzadd_add,'$new_timezone')), DATE(tp.created_dt)) ";
//        $activity_dt_tm = " IF(TIME(`activity_date_time`)!='00:00:00', DATE(CONVERT_TZ(`activity_date_time`,$tzadd_add,'$new_timezone')), DATE(`activity_date_time`))";
        date_default_timezone_set($curr_time_zone);
        $output=[];
        $selectsql = sprintf("SELECT t.*, 'trial' category FROM (
                SELECT 'payment' as type, tp.credit_method, '' history_id, tp.`trial_payment_id` payment_id, tp.`trial_reg_id` reg_id,tp.checkout_status, IF(`processing_fee_type`=2,IF(tp.payment_status in ('S','FR','R'),tp.payment_amount+tp.processing_fee,tp.payment_amount),tp.payment_amount) payment_amount,tr.payment_type,tr.`processing_fee_type`,tr.`trial_id` reg_trial_id,
                IF(tp.`payment_status`='N', IF(tp.`payment_date`>=CURDATE(),'upcoming','past'), '') current_status, tp.check_number, 
                tp.`payment_amount` payment_amount_without_pf, tp.`processing_fee` processing_fee,ifnull(DATE($payment_date),$created_date) as payment_date,tp.`payment_status`,tp.`refunded_amount`,tp.`refunded_date`,if(tp.`payment_from`='S',tp.`stripe_card_name`,tp.`cc_name`) as cc_name,tp.`payment_from`,DATE(tp.`last_updt_dt`) as last_updt_dt FROM `trial_payment` tp LEFT JOIN `trial_registration` tr ON tp.`trial_reg_id` = tr.`trial_reg_id` 
                WHERE tr.company_id='%s' AND tr.trial_reg_id= '%s' AND tp.`payment_status` NOT IN ('C') order by payment_id) t",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id));
        $result = mysqli_query($this->db, $selectsql);
//        log_info("history  ".$selectsql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $nums_rows = mysqli_num_rows($result);
            if ($nums_rows > 0) {
               
                while ($row = mysqli_fetch_assoc($result)) {
                    if($row['type']=='payment'){
                          if ($row['payment_type'] == 'O' && ($row['payment_status'] == 'R' || $row['payment_status'] == 'MR')) {
                            $row['payment_count'] = "Refunded payment";
                            $output[]= $row;
                        } else if ($row['payment_type'] == 'O') {
                            $row['payment_count'] = 'Payment';
                            $output[]= $row;
                        }
                       
                    }
          
//                    $output[]= $row;
                }
                $output2['upcoming'] =$output2['history'] = [];
                for ($i = 0; $i < count($output); $i++) {
                    if ($output[$i]['payment_type'] == 'O' && ($output[$i]['payment_status']=='N'  || $output[$i]['payment_status']=='F')) {
                        $output2['upcoming'][] = $output[$i];
                    }else{
                         $output2['history'][] = $output[$i];
                    }
                }
                $reg_details = $this->getTrialRegDetails($company_id, $reg_id, 1);
                $out = array('status' => "Success", 'msg' => $output2, "reg_details"=>$reg_details);
                if ($call_back == 0) {
                    $this->response($this->json($out), 200);
                } else {
                    return $out;
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Trial payment history doesn't exist.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200); // If no records "No Content" status  
                } else {
                    return $error;
                }
            }
        }
    }
    
    public function sendTrialPaymentHistoryDetails($company_id, $trial_reg_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        $pf_str = $pay_type = $paid_str = "";
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $created_date =" DATE(CONVERT_TZ(tp.created_dt,$tzadd_add,'$new_timezone'))";
       
        $waiver_policies = '';
        $query = sprintf("SELECT tr.`trial_status`, tr.`buyer_name`, tr.`buyer_email`, tr.`buyer_phone`, tr.`buyer_postal_code`,tr.`start_date`,tr.`end_date`,tp.`check_number`,
                tr.`registration_date`, tr.`registration_amount`, tr.`processing_fee_type`, tr.`payment_type`, c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`,
                tr.`program_length`, tr.`program_length_type`, concat(tr.`trial_registration_column_2`,',',tr.`trial_registration_column_1`) participant,  tr.`trial_registration_column_4`
                lead_Source,tp.`credit_method`,t.`trial_title`,t.`trial_waiver_policies`,tp.`payment_date`,tp.`payment_amount`,tp.`processing_fee`,tp.`payment_status`,tp.`refunded_amount`,tp.`refunded_date`,if(tp.`payment_from`='S',tp.`stripe_card_name`,tp.`cc_name`) as cc_name,DATE($created_date) created_dt,tp.`last_updt_dt` FROM `trial_payment` tp 
                left join `trial_registration` tr on(tr.`trial_reg_id`=tp.`trial_reg_id`) 
                left join `student` s on(tr.`student_id`=s.`student_id`) 
                left join `trial` t on(t.`trial_id`=tr.`trial_id`) 
                left join `company` c on(c.`company_id`=tr.`company_id`) 
                WHERE tr.`trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
            exit();
        } else {
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
                $trial_name = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $student_cc_email_list = $wp_currency_code = $wp_currency_symbol =$processing_fee_type= '';
                $current_plan_details = $trial_details =  $payment_details = [];
                 while($row = mysqli_fetch_assoc($result)){
                    $paid_amt = 0;
                    $trial_name = $row['trial_title'];
                    $participant_name = $row['participant'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['trial_waiver_policies'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $student_cc_email_list = $row['student_cc_email'];
                    $start_date  = date("M dS, Y", strtotime($row['start_date']));
                    $end_date  = date("M dS, Y", strtotime($row['end_date']));
                    $temp=[];
                    
                    if($row['processing_fee_type']==2){
                      $payment_amount  = $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                    }else{
                       $payment_amount = $temp['amount'] = $row['payment_amount'];
                    }
                    $temp['date'] = $row['payment_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['payment_status'] = $row['payment_status'];
                    $temp['processing_fee'] = $row['processing_fee']; 
                    $temp['cc_name'] = $row['cc_name'];
                    $temp['credit_method'] = $row['credit_method'];
                    $temp['check_number'] = $row['check_number'];
                    $payment_details[] = $temp;
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                log_info($this->json($error_log));
                if($return_value==1){
                    $error = array('status' => "Failed", "msg" => "No records found.");
                    $this->response($this->json($error), 200);
                }
                exit();
            }
        }
        
        $subject = $trial_name." Payment History";
        $message .= "<b>Order Details</b><br><br>";
        $message .= "$trial_name"."<br>";
        $message .= "Starts: "."$start_date"."<br>";
        $message .= "Ends: "."$end_date"."<br>";
        $message .= "Participant: "."$participant_name"."<br><br>";
//        $message .= "Total Due: "."$wp_currency_symbol$payment_amount"."<br>";
        if (count($payment_details) > 0) {
            $message .= "<b>Payment History Details</b><br><br>";
            for ($i = 0; $i < count($payment_details); $i++) {
                $amount = $payment_details[$i]['amount'];
                    $refunded_amount = $payment_details[$i]['refunded_amount'];
                if ($payment_details[$i]['payment_status'] == 'S' ||  $payment_details[$i]['payment_status'] == 'FR') {
                    $paid_str = "Amount paid: ";
                    $pay_type = "(" . $payment_details[$i]['cc_name'] . ")";
                }else if($payment_details[$i]['payment_status'] == 'R' || $payment_details[$i]['payment_status'] == 'MR'){
                    $paid_str = "Amount Refunded: ";
                }else if(($payment_details[$i]['payment_status'] == 'N' && $payment_details[$i]['payment_status'] < date("Y-m-d")) || $payment_details[$i]['payment_status'] == 'F'){
                    $paid_str = "Failed payment: ";
                }else if($payment_details[$i]['payment_status'] == 'M' || $payment_details[$i]['payment_status'] == 'MF'){
                     $pay_type = "(manual credit applied)";
                     $paid_str = "Amount paid: ";
                     if($payment_details[$i]['credit_method'] == 'CA'){
                        $pay_type = "(Cash)"; 
                     }else{
                        $pay_type = "(Check #".$payment_details[$i]['check_number'].")";
                     }
                }

                $pr_fee = $payment_details[$i]['processing_fee'];
                if ($processing_fee_type == 2 && $payment_details[$i]['payment_status'] !== 'M' && $payment_details[$i]['credit_method'] == 'CC') {
                    $pf_str = " (includes $wp_currency_symbol" . $pr_fee . " administrative fees)";
                }

               $message .= " $paid_str" . "$wp_currency_symbol$payment_amount" . "$pf_str$pay_type" . " <br>";
            }
        }
        $message .= "<br><br>";
        $waiver_present = 0;
        $file = '';
        if(!empty(trim($waiver_policies))){
            $waiver_present = 1;
            $file_name_initialize = 10;
            for($z=0; $z<$file_name_initialize; $z++){
                $dt = time();
                $file = "../../../uploads/".$dt."_waiver_policies.html";        
                if(file_exists($file)){
                    sleep(1);
                    $file_name_initialize++;
                }else{
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }

        $sendEmail_status = $this->sendEmailForTrial($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list,$student_cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            log_info($this->json($error_log));
            $log = array('status' => "Success", "msg" => "Trial order receipt sent successfully.");
                $this->response($this->json($log), 200);
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Trial order receipt sent failed.");
             $this->response($this->json($error), 200);
        }
        date_default_timezone_set($curr_time_zone);
    }
    
       public function updateParticipantDetails($company_id, $trial_reg_id, $pfirst_name, $plast_name, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $dob, $trial_field_5, $trial_field_6, $trial_field_7, $trial_field_8, $trial_field_9, $trial_field_10,$participant_street, $participant_city, $participant_state, $participant_zip, $participant_country){

           $activity_type = $activity_text = "";
        $curr_date = gmdate('Y-m-d H:i:s');
        $check_buyer = sprintf("SELECT * FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id`='%s'",
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_reg_id));
        $result_buyer = mysqli_query($this->db, $check_buyer);
        if (!$result_buyer) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_buyer");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $nums_rows = mysqli_num_rows($result_buyer);
            if ($nums_rows > 0) {
                $old_reg = mysqli_fetch_object($result_buyer);
                $old_buyer_first_name = $old_reg->buyer_first_name;
                $old_buyer_last_name = $old_reg->buyer_last_name;
                $student_id = $old_reg->student_id;
                if ($old_buyer_first_name !== $buyer_first_name && $old_buyer_last_name !== $buyer_last_name) {
                    $activity_type = "buyer name edit";
                    $activity_text = "buyer name is changed from $old_buyer_first_name $old_buyer_last_name to $buyer_first_name $buyer_last_name.";
                } elseif ($old_buyer_first_name !== $buyer_first_name) {
                    $activity_type = "buyer name edit";
                    $activity_text = "buyer first name is changed from $old_buyer_first_name to $buyer_first_name";
                } elseif ($old_buyer_last_name !== $buyer_last_name) {
                    $activity_type = "buyer name edit";
                    $activity_text = "buyer last name is changed from $old_buyer_last_name to $buyer_last_name";
                }
            } else {
                $error = array('status' => "Failed", "msg" => "No Participant Found.");
                $this->response($this->json($error), 200);
            }
        }
        
        $sql = sprintf("UPDATE `trial_registration` SET `buyer_first_name`='%s',`buyer_last_name`='%s',`buyer_email`='%s',`buyer_phone`='%s',`trial_registration_column_1`='%s', `trial_registration_column_2`='%s',`trial_registration_column_3`='%s',
              `trial_registration_column_5`='%s',`trial_registration_column_6`='%s',`trial_registration_column_7`='%s',`trial_registration_column_8`='%s',`trial_registration_column_9`='%s',`trial_registration_column_10`='%s',`trial_participant_street`='%s', `trial_participant_city`='%s', `trial_participant_state`='%s', `trial_participant_zip`='%s', `trial_participant_country`='%s' WHERE `company_id`='%s' AND `trial_reg_id`='%s'", 
                mysqli_real_escape_string($this->db,$buyer_first_name), mysqli_real_escape_string($this->db,$buyer_last_name),mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$buyer_phone), mysqli_real_escape_string($this->db,$pfirst_name), mysqli_real_escape_string($this->db,$plast_name), mysqli_real_escape_string($this->db,$dob),mysqli_real_escape_string($this->db,$trial_field_5),
                mysqli_real_escape_string($this->db,$trial_field_6),mysqli_real_escape_string($this->db,$trial_field_7),mysqli_real_escape_string($this->db,$trial_field_8),mysqli_real_escape_string($this->db,$trial_field_9),mysqli_real_escape_string($this->db,$trial_field_10),mysqli_real_escape_string($this->db,$participant_street),mysqli_real_escape_string($this->db, $participant_city),
                mysqli_real_escape_string($this->db, $participant_state), mysqli_real_escape_string($this->db, $participant_zip), mysqli_real_escape_string($this->db, $participant_country), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$trial_reg_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if(!empty($activity_type)){
                $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), $student_id, $activity_type, $activity_text, $curr_date);
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
           $participant_details= $this->getparticipantInfoDetails($company_id, $trial_reg_id,1);
           $msg = array('status' => "Success", "msg" => "Participant details updated successfully.", "participant_details" => $participant_details);
           $this->response($this->json($msg), 200);
        }
    }
    
    public function gettrialcustomerDetailsbyfilter($company_id, $trial_status_tab, $search, $draw_table, $length_table, $start, $sorting,$trial_startdate_limit,$trial_enddate_limit,$trial_days_type,$register_type,$notattendeddays,$notattendeddays1){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        
//        $end_limit=500;
        $sort = $sorting[0]['column'];
        $sort_type = $sorting[0]['dir'];
        $search_value = $search['value'];
        $out = $output = [];
        $out['active'] = $out['enrolled'] = $out['didnotstart'] = $out['cancelled'] = $out['recordsTotal']= $out['recordsFiltered'] = 0;
        $date_format = "'%b %d, %Y'";
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $last_updt_date = " DATE(CONVERT_TZ(tr.last_updt_dt,$tzadd_add,'$new_timezone'))";
//         $end_date = " DATE(CONVERT_TZ(tr.end_date,$tzadd_add,'$new_timezone'))";
//          $start_date = " DATE(CONVERT_TZ(tr.start_date,$tzadd_add,'$new_timezone'))";
        $created_date = "  DATE(CONVERT_TZ(tr.created_dt,$tzadd_add,'$new_timezone'))";
        $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $filter_date_column = "$created_date";
        $tday_text =$tday_text_c= '';
        $tday_text2=$tday_text3='';
        if($register_type=='1'){
            if ($trial_days_type == 1) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 2) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 3) {
                $tday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
            } elseif ($trial_days_type == 4) {
                $tday_text = "and DATE($filter_date_column) BETWEEN '" . $trial_startdate_limit . "' AND '" . $trial_enddate_limit . "'";
            } elseif ($trial_days_type == 5) {
                $tday_text = "";
            }
        }elseif($register_type=='2'){
            $tday_text2 = ", IFNULL((SELECT DATEDIFF(CURDATE(),DATE(MAX(checkin_datetime))) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N'),0) l_att2";
            $tday_text3 = "IFNULL((SELECT DATEDIFF(CURDATE(),DATE(MAX(checkin_datetime))) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N'),0) l_att2, ";
           
            if (trim(empty($notattendeddays1))) {
                $tday_text = "HAVING l_att2 > $notattendeddays ";
            } else {
                $tday_text = "HAVING l_att2 BETWEEN $notattendeddays AND $notattendeddays1";
            }
        }elseif($register_type=='3'){
            $tday_text2 = ", IF((SELECT 1 FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N' group by trial_attendance.trial_reg_id)is NULL,100,IFNULL((SELECT count(*) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N'  and DATE(`checkin_datetime`)>=DATE_SUB(CURDATE(),  INTERVAL $notattendeddays1 DAY)),0)) att";
            $tday_text = "HAVING att <= 2";
            $tday_text3 = "IF((SELECT 1 FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N'  group by trial_attendance.trial_reg_id)is NULL,100,IFNULL((SELECT count(*) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N'  and DATE(`checkin_datetime`)>=DATE_SUB(CURDATE(),  INTERVAL $notattendeddays1 DAY)),0)) att,";
        }
        if (!empty($search_value)) {
            $s_text = " and ( buyer_name like  '%$search_value%'  or buyer_email like  '%$search_value%' or `buyer_phone` like  '%$search_value%' or  tr.`trial_status` like '%$search_value%'
                    or CONCAT(tr.`trial_registration_column_2`, ', ', tr.`trial_registration_column_1`) like  '%$search_value%' or   DATE_FORMAT(tr.`start_date`, '%b %d, %Y')  like  '%$search_value%' 
                     or  DATE_FORMAT(tr.`end_date`, '%b %d, %Y')  like  '%$search_value%' or  DATE_FORMAT(tr.`registration_date`, '%b %d, %Y')  like  '%$search_value%' or  t.`trial_title` like  '%$search_value%'  or  tr.`trial_registration_column_4` like  '%$search_value%') $tday_text";
        } else {
            $s_text = "$tday_text";
        }

        $column = array(2 => "buyer_name", 3 => "participant_name", 4 => "age", 5 => "participant_phone", 6 => "participant_email", 7 => "trial_status", 8 => "date(registration_date)", 9 => "trial_program", 10 => "date(start_date)", 11 => "date(end_date)", 12=>"leadsource", 13=>"l_att", 14=>"act_att", 15=>"14_att", 16=>"30_att");
 
        if ($trial_status_tab == 'A') { //ORDERS TAB
            $trial_tab = "tr.`trial_status` in ('A')";
        } else if ($trial_status_tab == 'E') {
            $trial_tab = "tr.`trial_status` in ('E')";
        } else if ($trial_status_tab == 'D') {
            $trial_tab = "tr.`trial_status` in ('D')";
        } else if ($trial_status_tab == 'C') {
            $trial_tab = "tr.`trial_status` in ('C')";
        }
//        if(!empty($trial_id)){
//            $trial_string = " and tr.`trial_id`=$trial_id";
//        }else{
//            $trial_string = "";
//        }
        
        if (!empty($search_value)) {
            $query1 = sprintf("SELECT tr.`trial_reg_id` as id, tr.`company_id`, tr.`trial_id`, tr.`student_id`, `buyer_name`, CONCAT(tr.`trial_registration_column_1`, ' ', tr.`trial_registration_column_2`) participant_name,
                IF(trial_reg_type_user='P', 'Control Panel', IF(trial_reg_type_user='M', 'Mobile App', IF(trial_reg_type_user='U', 'Website URL', ''))) trial_reg_type_user,
                 `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(tr.`registration_date`,%s) registration_date, `payment_type`, tr.`payment_amount`,
                tr.`paid_amount` paid_amount, DATE_FORMAT(($last_updt_date),%s) last_updt_dt,$tday_text3 if(`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                IF((SELECT MAX(checkin_datetime) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id`and status='N') IS NULL, '', DATEDIFF(DATE(NOW()),(SELECT DATE(MAX(checkin_datetime)) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N'))) l_att,
                IFNULL((SELECT SUM(1) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` AND   status='N' GROUP BY `company_id`, `trial_reg_id`),0) act_att,
                IFNULL((SELECT count(*) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N'  and DATE(`checkin_datetime`)>=DATE_SUB(CURDATE(),  INTERVAL 14 DAY)),0) 14_att,
                IFNULL((SELECT count(*) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N'  and DATE(`checkin_datetime`)>=DATE_SUB(CURDATE(),  INTERVAL 30 DAY)),0) 30_att,
                if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, 
                IF(tr.`trial_status`='A','Active',IF(tr.`trial_status`='E','Enrolled',IF(tr.`trial_status`='C','Cancelled','Didnotstart'))) trial_status, tr.`trial_registration_column_4` leadsource, DATE_FORMAT(tr.`start_date`,%s) start_date, DATE_FORMAT(tr.`end_date`,%s) end_date,t.`trial_title` trial_program
                FROM `trial_registration` tr left join `trial` t on tr.`company_id`=t.`company_id` AND tr.`trial_id`=t.`trial_id` 
                LEFT JOIN student s1 on tr.`student_id` = s1.student_id and tr.company_id = s1.company_id
                LEFT JOIN student s2 on tr.`buyer_email` = s2.student_email and tr.company_id = s2.company_id
                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                WHERE tr.`company_id`='%s' and   $trial_tab  %s ", $date_format, $date_format, $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id), $s_text);
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $numsql_rows1 = mysqli_num_rows($result1);
                if ($numsql_rows1 > 0) {

                    $out['recordsFiltered'] = $numsql_rows1;
                }
            }
        }

        $query = sprintf("SELECT tr.`trial_reg_id` as id, tr.`company_id`, tr.`trial_id`, tr.`student_id`, `buyer_name`, CONCAT(tr.`trial_registration_column_2`, ', ', tr.`trial_registration_column_1`) participant_name,
                IF(trial_reg_type_user='P', 'Control Panel', IF(trial_reg_type_user='M', 'Mobile App', IF(trial_reg_type_user='U', 'Website URL', ''))) trial_reg_type_user,
                 `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(tr.`registration_date`,%s) registration_date, `payment_type`, tr.`payment_amount`,
                tr.`paid_amount` paid_amount, DATE_FORMAT(($last_updt_date),%s) last_updt_dt,$tday_text3 if(`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,'Bad email. Please edit.' as error,
                IF((SELECT MAX(checkin_datetime) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id`and status='N') IS NULL, '', DATEDIFF(DATE(NOW()),(SELECT DATE(MAX(checkin_datetime)) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N'))) l_att,
                IFNULL((SELECT SUM(1) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` AND   status='N' GROUP BY `company_id`, `trial_reg_id`),0) act_att,
                IFNULL((SELECT count(*) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N'  and DATE(`checkin_datetime`)>=DATE_SUB(CURDATE(),  INTERVAL 14 DAY)),0) 14_att,
                IFNULL((SELECT count(*) FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` and status='N'  and DATE(`checkin_datetime`)>=DATE_SUB(CURDATE(),  INTERVAL 30 DAY)),0) 30_att,
                if((ifnull(s1.push_device_id,'') = '' and ifnull(s2.push_device_id,'') =''),'N', 'Y') show_icon, IF(tr.`trial_registration_column_3` = '0000-00-00'
                    OR tr.`trial_registration_column_3` IS NULL,
                '',TRUNCATE(DATEDIFF($currdate_add,tr.`trial_registration_column_3`) / 365.25,0)) age,
                IF(tr.`trial_status`='A','Active',IF(tr.`trial_status`='E','Enrolled',IF(tr.`trial_status`='C','Cancelled','Didnotstart'))) trial_status, tr.`trial_registration_column_4` leadsource, DATE_FORMAT(tr.`start_date`,%s) start_date, DATE_FORMAT(tr.`end_date`,%s) end_date,t.`trial_title` trial_program
                FROM `trial_registration` tr left join `trial` t on tr.`company_id`=t.`company_id` AND tr.`trial_id`=t.`trial_id` 
                LEFT JOIN student s1 on tr.`student_id` = s1.student_id and tr.company_id = s1.company_id
                LEFT JOIN student s2 on tr.`buyer_email` = s2.student_email and tr.company_id = s2.company_id
                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email`  AND (b.`company_id`=0 OR b.`company_id`=s1.`company_id`)
                WHERE   tr.`company_id`='%s' and  $trial_tab  %s  ORDER BY $column[$sort] $sort_type LIMIT $start, $length_table ", $date_format, $date_format, $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id), $s_text);
          $result = mysqli_query($this->db, $query);
//        log_info("ravi trial   ".$query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            $sql = sprintf("SELECT count(*) status_count $tday_text2, tr.trial_status from `trial_registration` tr where tr.company_id='%s' group by trial_status  %s ", mysqli_real_escape_string($this->db, $company_id),$tday_text);
            $result_sql = mysqli_query($this->db, $sql);
            if (!$result_sql) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $numsql_rows = mysqli_num_rows($result_sql);
                if ($numsql_rows > 0) {
                    while ($rows1 = mysqli_fetch_assoc($result_sql)) {
                        $trial_status = $rows1['trial_status'];
                        $status_count = $rows1['status_count'];
                        if ($trial_status == 'A') {
                            $out['active'] = $status_count;
                        } elseif ($trial_status == 'E') {
                            $out['enrolled'] = $status_count;
                        } elseif ($trial_status == 'D') {
                            $out['didnotstart'] = $status_count;
                        } elseif ($trial_status == 'C') {
                            $out['cancelled'] = $status_count;
                        }
                    }
                    if ($trial_status_tab == 'A') {
                           $out['recordsTotal'] =   $out['active'];
                           if(empty($search_value)){
                                $out['recordsFiltered'] =   $out['active'];
                           }
                       } else if ($trial_status_tab == 'E') {
                           $out['recordsTotal'] = $out['enrolled'];
                           if(empty($search_value)){
                                 $out['recordsFiltered'] =  $out['enrolled'];
                           }
                       } else if ($trial_status_tab == 'D') {
                           $out['recordsTotal'] = $out['didnotstart'];
                           if (empty($search_value)) {
                            $out['recordsFiltered'] = $out['didnotstart'];
                        }
                    } else if ($trial_status_tab == 'C') {
                           $out['recordsTotal'] = $out['cancelled'];
                           if (empty($search_value)) {
                            $out['recordsFiltered'] = $out['cancelled'];
                        }
                    }
                }
            }
//                 $trial_details  = $this->getTrialDetailByCompany($company_id,1);
//                 if($trial_details['status'] == 'Success'){
//                     $out['live_count'] = count($trial_details['msg']['live']);
//                     $out['draft_count'] = count($trial_details['msg']['draft']);
//                 }else{
//                     $out['live_count'] = 0;
//                     $out['draft_count'] = 0;
//                 }
                 
            if ($num_rows > 0) {
//                $output['recent'] = $output['cancelled'] = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    if ($trial_status_tab == 'A') {
                        $output[] = $row;
                    } else if ($trial_status_tab == 'E') {
                        $output[] = $row;
                    } else if ($trial_status_tab == 'D') {
                        $output[] = $row;
                    } else if ($trial_status_tab == 'C') {
                        $output[] = $row;
                    }
                }
                $out['draw'] = $draw_table;
                $out['data'] = $output;
//                if (!empty($search_value)) {
//                    $out['recordsFiltered'] = $num_rows;
//                }
//                $res = array("status" => "Success", "msg" => $output, "count" => $count);
                $this->response($this->json($out), 200);
            } else {
                $out['draw'] = $draw_table;
                $out['data'] = $output;
//                $error = array('status' => "Failed", "msg" => "No participants registered yet.", "count" => $count);
                $this->response($this->json($out), 200); // If no records "No Content" status
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function processTrialPaymentRefund($company_id, $trial_reg_id, $trial_payment_id, $trial_id){
        // split call based on wepay or stripe
        $payment_from = '';
        $initial_query = sprintf("SELECT `registration_from` FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id));
        $initial_result = mysqli_query($this->db, $initial_query);
        if (!$initial_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$initial_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($initial_result) > 0) {
                $row_payment_from = mysqli_fetch_assoc($initial_result);
                $payment_from = $row_payment_from['registration_from'];
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error), 200);
            }
        }
        if($payment_from == 'S'){ // If stripe then redirect to other function
            $this->stripeRefundTrialInDB($company_id, $trial_reg_id, $trial_payment_id, $trial_id);
        }else{ // If wepay then continue
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone = $user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
    //        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";

            $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = '';
            $country = 'US';
            $paid_amount = $parent_qty = $total_refund_amount = $update_trial_remaining_dues = $update_trial_amount = $trial_refund_sales = $update_refund_sales_flag = $update_zero_cost_payment_record = 0;
            $curr_date = date("Y-m-d");
            $parent_net_sales = 0;
             $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, wp.`account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency`, `upgrade_status`, c.`wp_currency_symbol`,
                    tr. `paid_amount`, tr.`student_id`, tr.`payment_type`, tr.`credit_card_id`, tr.`credit_card_name`, tr.`processing_fee_type`, `buyer_name`, `buyer_email`, `buyer_phone`,
                    `buyer_postal_code`, tr.`trial_id`, tr.`trial_status`,t.`trial_title`,t.`trial_subtitle` FROM `wp_account` wp
                    LEFT JOIN `company` c ON c.`company_id`=wp.`company_id` 
                    LEFT JOIN `trial_registration` tr ON tr.`company_id`=wp.`company_id`
                    LEFT JOIN `trial` t ON t.`company_id`=wp.`company_id` AND t.`trial_id`=tr.`trial_id`
                    WHERE c.`company_id`='%s' AND tr.`trial_reg_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "1- $query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows1 = mysqli_num_rows($result1);
                if ($num_rows1 > 0) {
                    $row = mysqli_fetch_assoc($result1);
                    $access_token = $row['access_token'];
                    $account_id = $row['account_id'];
                    $currency = $row['currency'];
                    $currency_symbol = $row['wp_currency_symbol'];
                    $user_state = $row['wp_user_state'];
                    $acc_state = $row['account_state'];
                    $upgrade_status = $row['upgrade_status'];
                    $student_id = $row['student_id'];
                    $category_status = $row['trial_status'];
                    $trial_title = $row['trial_title'];
                    $trial_subtitle = $row['trial_subtitle'];
                    $trial_id = $row['trial_id'];
                    $paid_amount = $row['paid_amount'];
                    $cc_id = $row['credit_card_id'];
                    $cc_name = $row['credit_card_name'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $buyer_postal_code = $row['buyer_postal_code'];
    //                $country = $row['country'];
                     $gmt_date=gmdate(DATE_RFC822);
                    $sns_msg5=array("buyer_email"=>$buyer_email,"membership_title"=>$trial_title,"gmt_date"=>$gmt_date);
                    if ($user_state == 'deleted') {
                        $log = array("status" => "Failed", "msg" => "You may delete your account yourselves, or the account may be deleted by WePay customer support.");
                        $this->response($this->json($log), 200);
                    }

                    if($acc_state=='deleted'){
                        $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                        $this->response($this->json($log),200);
                    }elseif($acc_state=='disabled'){
                        $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                        $this->response($this->json($log),200);
                    }

                    if (!empty($access_token) && !empty($account_id)) {
                        if (!function_exists('getallheaders')) {
                            if (!function_exists('apache_request_headers')) {
                                $user_agent = $_SERVER['HTTP_USER_AGENT'];
                            } else {
                                $headers = apache_request_headers();
                                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                            }
                        } else {
                            $headers = getallheaders();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                        $temp_password = $this->wp_pswd;
                        $method = 'aes-256-cbc';
                        // Must be exact 32 chars (256 bit)
                        $password = substr(hash('sha256', $temp_password, true), 0, 32);
                        // IV must be exact 16 chars (128 bit)
                        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);

                    }
                } else {
                    $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                    $this->response($this->json($error), 200);
                }
            }



            if ($paid_amount > 0) {
                $rfee = $trfee =$ref_amount= 0;
                $p_f = $this->getProcessingFee($company_id,$upgrade_status);
                $process_fee_per = $p_f['PP'];
                $process_fee_val = $p_f['PT'];

                if (isset($trial_payment_id) && !empty($trial_payment_id)) {
                    $get_payment_query = sprintf("SELECT credit_method,`checkout_id`,`checkout_status`,`payment_amount`,`refunded_amount`, `processing_fee`,`last_updt_dt` 
                                FROM `trial_payment`  WHERE `trial_payment_id`='%s' ",$trial_payment_id);
                    $result_payment_query = mysqli_query($this->db, $get_payment_query);
                    if (!$result_payment_query) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $get_payment_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $row_res = mysqli_fetch_assoc($result_payment_query);
                        $payment_id = $row_res['checkout_id'];
                        $payment_status = $row_res['checkout_status'];
                        $db_refunded_amt = $row_res['refunded_amount'];
                        $db_processing_fee = $row_res['processing_fee'];
                        $payment_amount = $row_res['payment_amount'];
                        $old_last_updt_date = $row_res['last_updt_dt'];
                        $stop_wepay = false;
                        if($row_res['credit_method'] == "CH" || $row_res['credit_method'] == "CA"){
                            $stop_wepay = true;
                        }
                    }
                    if ($processing_fee_type == 1) {
                        $parent_net_sales += $payment_amount - $db_processing_fee;
                        $ref_amount += $payment_amount;
                    } else {
                        $parent_net_sales += $payment_amount;
                        $ref_amount += $payment_amount+ $db_processing_fee;
                    }
                }
                if(!$stop_wepay){
                    if ($payment_status == 'new' || $payment_status == 'authorized' || $payment_status == 'reserved') {
                        $sns_msg5['call'] = "Checkout";
                        $sns_msg5['type'] = "Checkout Cancellation";
                        $call_method = "checkout/cancel";
                        //  $json = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","checkout_id"=>$payment_id,"cancel_reason"=>"Participant ");
                        $json = array("checkout_id" => $payment_id, "cancel_reason" => "Participant ");
                    } elseif ($payment_status == 'released' || $payment_status == 'captured') {
                        $sns_msg5['call'] = "Checkout";
                        $sns_msg5['type'] = "Checkout Refund";
                        $call_method = "checkout/refund";
                        $json = array("checkout_id" => $payment_id, "refund_reason" => "Participant ");
                    } else {
                        $error = array("status" => "Failed", "msg" => "Participant payment status is in $payment_status status.");
                        $this->response($this->json($error), 200);
                    }

                    $postData = json_encode($json);
                    $response = $this->wp->accessWepayApi($call_method, 'POST', $postData, $token, $user_agent,$sns_msg5);
                    $this->wp->log_info("wepay_checkout_refund: " . $response['status']);
                }
                if((isset($response['status']) && $response['status']=="Success") || $stop_wepay){
                    $total_refund_amount += $payment_amount;
                    $payment_amount -= $payment_amount;
                    //  $res = $response['msg'];
                    if ($stop_wepay){
                        $refund_string = 'MF';
                        $status = "MR";
                    }else{
                        $refund_string = 'FR';
                        $status = "R";
                    }

                    //Insert new record with FR status in Trial Payment Table
                    $insert_query = sprintf("INSERT INTO `trial_payment`( `student_id`, `trial_reg_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `created_dt`, `last_updt_dt`,`credit_method`,`check_number`) 
                                SELECT `student_id`, `trial_reg_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, '$refund_string', `cc_id`, `cc_name`, `created_dt`,'$old_last_updt_date',`credit_method`, `check_number` FROM `trial_payment` WHERE `trial_payment_id`='%s'", mysqli_real_escape_string($this->db, $trial_payment_id));
                    $result_insert_query = mysqli_query($this->db, $insert_query);
                    if (!$result_insert_query) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $insert_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }

                    //Update trial Payment Table
                    $update_query3 = sprintf("UPDATE `trial_payment` SET `payment_status`='%s', `refunded_amount`='%s', `refunded_date`='$curr_date' WHERE `trial_payment_id`='%s'", mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $parent_net_sales), mysqli_real_escape_string($this->db, $trial_payment_id));
                    $result_update_query3 = mysqli_query($this->db, $update_query3);
                    if (!$result_update_query3) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $update_query3");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }

                     //Update trial Table
                    $update_query4 = sprintf("UPDATE `trial` SET `net_sales` = `net_sales`-$parent_net_sales WHERE `trial_id` IN (%s)", $trial_id);
                    $result_update_query4 = mysqli_query($this->db, $update_query4);
                    if (!$result_update_query4) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "4- $update_query4");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }

                     //Update Trial Registration Table
                    $update_query6 = sprintf("UPDATE `trial_registration`  SET `refunded_amount` = `refunded_amount`+$parent_net_sales, `paid_amount` = `paid_amount`-$parent_net_sales WHERE `trial_reg_id` IN (%s)", $trial_reg_id);
                    $result_update_query6 = mysqli_query($this->db, $update_query6);
                    if (!$result_update_query6) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "6- $update_query6");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }

                    $this->updateTrialDimensionsNetSales($company_id, $trial_id,'');
                    $payment_history = $this->getpaymentHistoryDetails($company_id, $trial_reg_id, 1);
                    $msg = array("status" => "Success", "msg" => "Refund Successfully Processed.", "payment_history" => $payment_history);
                    $this->response($this->json($msg), 200);
                } else {
                    $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
                    $this->response($this->json($log), 200);
                }
            }else{
                $log = array("status" => "Failed", "msg" => "Payment mismatched.");
                $this->response($this->json($log), 200);
            }
            date_default_timezone_set($curr_time_zone);
        }
    }
      // Muthulakshmi  Stripe Refund for trial
    public function stripeRefundTrialInDB($company_id, $trial_reg_id, $trial_payment_id, $trial_id) {
        $charge_id = $payment_intent_id = $payment_status = $db_processing_fee = $old_last_updt_date = $processing_fee_type = $trial_id = $upgrade_status = $stripe_refund_flag = '';
        $trfee = $ref_amount = $total_refund_amount = $payment_amount = $parent_net_sales = 0;
        $charge_array = [];
        $charge_retrieve_arr = $misc_refund_arr = [];
        
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $curr_date = date("Y-m-d");
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $upgrade_status = $sts['upgrade_status'];
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
        
        if ((!empty($stripe_account) && $stripe_account['charges_enabled'] == 'Y')){
            
            if (isset($trial_payment_id) && !empty($trial_payment_id)) {
                $query1 = sprintf("SELECT tr. `paid_amount`, tr.`student_id`, tr.`payment_type`, tr.`payment_method_id`, tr.`stripe_card_name`,tr.`payment_method`, tr.`processing_fee_type`, tr.`buyer_name`, tr.`buyer_email`, tr.`buyer_phone`,
                    tr.`buyer_postal_code`, tr.`trial_id`, tr.`trial_status`,t.`trial_title`,t.`trial_subtitle`, tp.`payment_intent_id`, sa.`stripe_refund_flag`,
                    tp.`credit_method`,tp.`checkout_status`,tp.`payment_amount`, tp.`processing_fee`,tp.`last_updt_dt`FROM `trial_registration` tr
                    LEFT JOIN `trial_payment` tp ON tp.`trial_reg_id`=tr.`trial_reg_id` AND tp.`trial_payment_id`= '%s'
                    LEFT JOIN `trial` t ON t.`company_id`=tr.`company_id` AND t.`trial_id`=tr.`trial_id`
                    LEFT JOIN `stripe_account` sa ON sa.`company_id`= tr.`company_id`
                    WHERE tr.`company_id`='%s' AND tr.`trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_payment_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id));
                $result1 = mysqli_query($this->db, $query1);
                
               if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "2- $query1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if (mysqli_num_rows($result1) > 0) {
                        $row = mysqli_fetch_assoc($result1);
                        $trial_id = $row['trial_id'];
                        $paid_amount = $row['paid_amount'];
                        $processing_fee_type = $row['processing_fee_type'];
                        $payment_intent_id = $row['payment_intent_id'];
                        $payment_status = $row['checkout_status'];
                        $db_processing_fee = $row['processing_fee'];
                        $payment_amount = $row['payment_amount'];
                        $old_last_updt_date = $row['last_updt_dt'];
                        $stripe_refund_flag = $row['stripe_refund_flag'];
                        
                        if($payment_status == "requires_action"){
                            $error = array("status" => "Failed", "msg" => "Can't able to refund amount due to action required card.");
                            $this->response($this->json($error), 200);
                        }
                        
                        if($stripe_refund_flag == 'N'){
                            $error = array("status" => "Failed", "msg" => "Connected account does not have sufficient fund to cover the refund");
                            $this->response($this->json($error), 200);
                        }

                        if($row['payment_method'] == "CC" && !empty($payment_intent_id)){ // for retrieve charge id
                            $charge_retrieve_arr = $this->stripeChargeIdRetrieve($payment_intent_id);
                            $charge_array_initial = json_decode(json_encode($charge_retrieve_arr),true);
                            $charge_array = $charge_array_initial['charges']['data'];
                            $charge_id = $charge_array[0]['id'];
                        }
                        if($row['payment_method'] == "CC" && !empty($charge_id)){ // for refund
                            try {
                                $create_refund = \Stripe\Refund::create([
                                    "charge" => $charge_id,
                                    "reverse_transfer" => true]);
                            } catch (\Stripe\Error\Card $e) {
                                // Since it's a decline, \Stripe\Error\Card will be caught       
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\RateLimit $e) {
                                // Too many requests made to the API too quickly      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\InvalidRequest $e) {
                                // Invalid parameters were supplied to Stripe's API      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Authentication $e) {
                                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\ApiConnection $e) {
                                // Network communication with Stripe failed      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Base $e) {
                                // Display a very generic error to the user, and maybe send yourself an email      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (Exception $e) {
                                // Something else happened, completely unrelated to Stripe      
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            }
                            if (isset($err) && !empty($err)) {
                                $error = array('status' => "Failed", "msg" => $err['message']);
                                $this->response($this->json($error), 200);
                            }
                            $misc_refund_arr = json_decode(json_encode($create_refund),true);
                            if ($misc_refund_arr['status'] == 'succeeded'){

                            }else{
                                $error = array("status" => "Failed", "msg" => "Can't able to refund amount.");
                                $this->response($this->json($error), 200);
                            }
                        }
                    }else{
                        $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                        $this->response($this->json($error), 200);
                    }
                    if ($paid_amount > 0) {
                        if ($processing_fee_type == 1) {
                            $parent_net_sales += $payment_amount - $db_processing_fee;
                            $ref_amount += $payment_amount;
                        } else {
                            $parent_net_sales += $payment_amount;
                            $ref_amount += $payment_amount+ $db_processing_fee;
                        }
                        $total_refund_amount += $payment_amount;
                        $payment_amount -= $payment_amount;
                        if($row['credit_method'] == "CA" || $row['credit_method'] == "CH"){
                            $refund_string = 'MF';
                            $status = "MR";
                        }else{
                            $refund_string = 'FR';
                            $status = "R";
                        }
                        //Insert new record with FR status in Trial Payment Table
                        $insert_query = sprintf("INSERT INTO `trial_payment`( `student_id`, `trial_reg_id`, `payment_intent_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `stripe_card_name`, `created_dt`, `last_updt_dt`,`credit_method`,`check_number`,`payment_from`) 
                                    SELECT `student_id`, `trial_reg_id`, `payment_intent_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, '$refund_string', `payment_method_id`, `stripe_card_name`, `created_dt`,'$old_last_updt_date',`credit_method`, `check_number` ,`payment_from` FROM `trial_payment` WHERE `trial_payment_id`='%s'", mysqli_real_escape_string($this->db, $trial_payment_id));
                        $result_insert_query = mysqli_query($this->db, $insert_query);
                        if (!$result_insert_query) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $insert_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }

                        //Update trial Payment Table
                        $update_query3 = sprintf("UPDATE `trial_payment` SET `payment_status`='%s', `refunded_amount`='%s', `refunded_date`='$curr_date' WHERE `trial_payment_id`='%s'", mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $parent_net_sales), mysqli_real_escape_string($this->db, $trial_payment_id));
                        $result_update_query3 = mysqli_query($this->db, $update_query3);
                        if (!$result_update_query3) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "3- $update_query3");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }

                         //Update trial Table
                        $update_query4 = sprintf("UPDATE `trial` SET `net_sales` = `net_sales`-$parent_net_sales WHERE `trial_id` IN (%s)", $trial_id);
                        $result_update_query4 = mysqli_query($this->db, $update_query4);
                        if (!$result_update_query4) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "4- $update_query4");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }

                         //Update Trial Registration Table
                        $update_query6 = sprintf("UPDATE `trial_registration`  SET `refunded_amount` = `refunded_amount`+$parent_net_sales, `paid_amount` = `paid_amount`-$parent_net_sales WHERE `trial_reg_id` IN (%s)", $trial_reg_id);
                        $result_update_query6 = mysqli_query($this->db, $update_query6);
                        if (!$result_update_query6) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "6- $update_query6");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }

                        $this->updateTrialDimensionsNetSales($company_id, $trial_id,'');
                        $payment_history = $this->getpaymentHistoryDetails($company_id, $trial_reg_id, 1);
                        $msg = array("status" => "Success", "msg" => "Refund Successfully Processed.", "payment_history" => $payment_history);
                        $this->response($this->json($msg), 200);
                        
                    }else{
                        $log = array("status" => "Failed", "msg" => "Payment mismatched.");
                        $this->response($this->json($log), 200);
                    }
                    date_default_timezone_set($curr_time_zone);
                }
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error), 200);
            }
        }else{
            $error = array("status" => "Failed", "msg" => "We cannot able to access your stripe account due to invalid stripe status.");
            $this->response($this->json($error), 200);
        }
    }
    public function stripeChargeIdRetrieve($payment_intent_id) {
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        if (!empty($payment_intent_id)) {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $charge_id_retrieve = \Stripe\PaymentIntent::retrieve($payment_intent_id);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
            return $charge_id_retrieve;
        }
    }
    public function addTrialDimensions($company_id, $trial_id, $period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if (!empty($period)) {
            $curr_dt = date("Y-m", strtotime($period));
        } else {
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));


        $sql2 = sprintf("SELECT * FROM `trial_dimensions` WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows1 = mysqli_num_rows($result2);
            if ($num_of_rows1 == 0) {
                $insertquery1 = sprintf("INSERT INTO `trial_dimensions`(`company_id`, `period`, `trial_id`) VALUES('%s','%s','%s')", 
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $trial_id));
                $res2 = mysqli_query($this->db, $insertquery1);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    $this->response($this->json($error), 200);
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

    public function updateTrialDimensionsMembers($company_id, $trial_id, $trial_status,$old_trial_status,$trial_reg_id) {
        $flag = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $date_string = $trial_string = "";
//        $a_date = $e_date = $c_date = $d_date = '';
        $date_str = "%Y-%m";$reg_date='';
        $every_month_dt = date("Y-m-d");
        
//        if(!empty($old_date['reg'])){
//            $old_date['active'] = $old_date['reg'];
//        }
//        if(!empty($old_date)){
//        $a_date = "'".$old_date['active']."'";
//        $e_date = "'".$old_date['enrolled']."'";
//        $c_date = "'".$old_date['cancelled']."'";
//        $d_date = "'".$old_date['didnotstart']."'";
//       }
       if ($trial_status == 'A') {
            if ($old_trial_status == 'E') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
                 $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'C') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'D') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } elseif ($trial_status == 'E') {
            if ($old_trial_status == 'A') {
//                $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                 $trial_string = "`active_count`=`active_count`-1";
            } else if ($old_trial_status == 'C') {
//                 $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                  $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'D') {
//                $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                 $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } elseif ($trial_status == 'C') {
             if ($old_trial_status == 'E') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
               $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'A') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                 $trial_string = "`active_count`=`active_count`-1";
            } else if ($old_trial_status == 'D') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } else if ($trial_status == 'D') {
            if ($old_trial_status == 'E') {
//                 $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
                 $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'C') {
//                  $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                   $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'A') {
//                 $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                   $trial_string = "`active_count`=`active_count`-1";
            }
        }
//
//        $selectquery = sprintf("SELECT * FROM `trial_registration` WHERE $date_string and  `company_id`='%s' and `trial_id`='%s' and `trial_reg_id`='%s'",
//                $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $trial_reg_id));
//        $res = mysqli_query($this->db, $selectquery);
//       if (!$res) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
//            log_info($this->json($selectquery));
//            
//        } else {
//            $num_of_rows = mysqli_num_rows($res);
//            if ($num_of_rows == 1 && !empty($old_trial_status)) {
//                $flag = 1;
//            }
//        }
          $selectquery = sprintf("SELECT registration_date FROM `trial_registration` WHERE `company_id`='%s' and `trial_id`='%s' and `trial_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $trial_reg_id));
        $res = mysqli_query($this->db, $selectquery);
       if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
            log_info($this->json($selectquery));
            
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows == 1) {
               $rows = mysqli_fetch_object($res);
               $reg_date = $rows->registration_date;
            }
        }
      
        if (!empty($old_trial_status)) {
            if($trial_status == 'A'){
//                if($flag == 1){
                  $trial_string = "$trial_string, `active_count`=`active_count`+1";
//                }else{
//                  $trial_string = "`active_count`=`active_count`+1";
//                }
            }else if($trial_status == 'E'){
//                if($flag == 1){
                    $trial_string = "$trial_string, `enrolled_count`=`enrolled_count`+1";
//                }else{
//                     $trial_string = "`enrolled_count`=`enrolled_count`+1";
//                }
                
            }else if($trial_status == 'C'){
//                if($flag == 1){
                    $trial_string = "$trial_string, `cancelled_count`=`cancelled_count`+1";
//                }else{
//                     $trial_string = "`cancelled_count`=`cancelled_count`+1";
//                }
                
            }else if($trial_status == 'D'){
//                if($flag == 1){
                    $trial_string = "$trial_string, `didnotstart_count`=`didnotstart_count`+1";
//                }else{
//                     $trial_string = "`didnotstart_count`=`didnotstart_count`+1";
//                }
                
            }
            
        } else {
            $trial_string = "`active_count`=`active_count`+1";
            $reg_date = $every_month_dt;
        }

        if (!empty(trim($trial_string))) {
            $query1 = sprintf("UPDATE `trial_dimensions` SET $trial_string WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') ",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), $reg_date, $date_str);
            $result1 = mysqli_query($this->db, $query1);
//            log_info("dim   ".$query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        
        date_default_timezone_set($curr_time_zone);
    }
    
    public function getTrialDashboardDetails($company_id, $leadsource_flag, $conversion_flag, $sales_period) {
         $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
         $trial = $trial_new = $lead_source_arr = $trial_id_arr = $final_ls_array = $final_active_members_array = $final_trials_array = $final_can_members_array= $final_dnd_members_array=[];
        $t_total_count_arr = $t_total_conversion_members = $t_total_en_members = $net_gain_arr = [0, 0, 0, 0];
        $active_members = $enrolled_members = $cancelled_members =$didnotstart_members= [];$month_array = [];
       $percentage = 0;
        $c_months=($conversion_flag*1);
       $plusone = $c_months + 1;
       //conversion
       if ($sales_period == 'A') {
           $curr_date = date('Y-m-d', strtotime("-$c_months year"));
            $curr_datepulsonemonth = date('Y-m-01', strtotime(" $plusone year"));
            for ($i = 0; $i < 4; $i++) {
                $format_array[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date)));
                $month_array[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date)));
                $month_name[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date)));
            }
        } elseif ($sales_period == 'M') {
            $curr_date = date('Y-m-d', strtotime("-$c_months month"));
            $curr_datepulsonemonth = date('Y-m-01', strtotime(" $plusone month"));
            for ($i = 0; $i < 4; $i++) {
                $format_array[] = date('Y-m', strtotime("first day of -$i month", strtotime($curr_date)));
                $month_array[] = date('n', strtotime("first day of -$i month", strtotime($curr_date)));
                $month_name[] = date('M Y', strtotime("first day of -$i month", strtotime($curr_date)));
            }
        }
        $count_type = array("active","enrolled","cancelled","didnotstart");
        $format_list = implode("','", $format_array);

       $l_months = ($leadsource_flag * 1);
        $oneplus = $l_months + 1;
//      lead source
        $month_array1 = [];
        if ($sales_period == 'A') {
            $curr_date1 = date('Y-m-d', strtotime("-$l_months year"));
            $curr_datepulsoneyear = date('Y-m-01', strtotime(" $oneplus year"));
            for ($i = 0; $i < 4; $i++) {
                $format_array1[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date1)));
                $month_array1[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date1)));
                $month_name1[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date1)));
            }
        } elseif ($sales_period == 'M') {
            $curr_datepulsoneyear = date('Y-m-01', strtotime(" $oneplus month"));
            $curr_date1 = date('Y-m-d', strtotime("-$l_months month"));
            for ($i = 0; $i < 4; $i++) {
                $format_array1[] = date('Y-m', strtotime("first day of -$i month", strtotime($curr_date1)));
                $month_array1[] = date('n', strtotime("first day of -$i month", strtotime($curr_date1)));
                $month_name1[] = date('M Y', strtotime("first day of -$i month", strtotime($curr_date1)));
            }
        }

        $format_list1 = implode("','", $format_array1);
         $month_name_array = ["first_month","second_month","third_month","fourth_month"];
         $date_str = "%Y-%m";
         if($sales_period=='A'){
            $sql = sprintf("SELECT DATE_FORMAT(`registration_date`,'%s') period, SUBSTR(DATE_FORMAT(`registration_date`,'%s'), 1, 4) month, tr.trial_id, (select t.trial_title from trial t where t.`trial_id`=tr.`trial_id`) trial_category, count(*) lead_count, tr.`trial_registration_column_4`
                FROM `trial_registration` tr WHERE tr.company_id='%s' AND SUBSTR(DATE_FORMAT(`registration_date`,'%s'),1,4) IN ('%s') GROUP BY tr.trial_id,  SUBSTR(DATE_FORMAT(`registration_date`,'%s'),1,4), tr.`trial_registration_column_4` order by tr.trial_id,period,lead_count DESC", $date_str,$date_str,mysqli_real_escape_string($this->db, $company_id),$date_str, $format_list1,$date_str);
        }elseif($sales_period=='M'){
           $sql = sprintf("SELECT DATE_FORMAT(`registration_date`,'%s') period, SUBSTR(DATE_FORMAT(`registration_date`,'%s'), 6, 7) month, tr.trial_id, (select t.trial_title from trial t where t.`trial_id`=tr.`trial_id`) trial_category, count(*) lead_count, tr.`trial_registration_column_4`
                FROM `trial_registration` tr WHERE tr.company_id='%s' AND DATE_FORMAT(`registration_date`,'%s') IN ('%s') GROUP BY tr.trial_id,  DATE_FORMAT(`registration_date`,'%s'), tr.`trial_registration_column_4` order by tr.trial_id,period,lead_count DESC", $date_str,$date_str,mysqli_real_escape_string($this->db, $company_id), $date_str,$format_list1,$date_str);
        }
//        log_info("ravi   ".$sql);
        $res = mysqli_query($this->db, $sql);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($res)) {
                    $temp = $temp2 = [];
                    $temp['month'] = $temp2['month'] = (int) $row['month'];
                    $temp['trial_id'] = $temp2['trial_id'] = $trial_id = $row['trial_id'];
                    $temp['trial_category'] = $temp2['trial_category'] = $row['trial_category'];
                    $temp['lead_count'] = $lead_count = $row['lead_count'];
                    $temp['lead_source'] = $lead_source = $row['trial_registration_column_4'];
                    $temp['db'] = 1;
                    $trial_lead[] = $temp;
                    
                     if (!empty($lead_source) && !in_array($lead_source, $lead_source_arr)) {
                         $lead_source_arr[] = $lead_source; 
                     }
                    if (!empty($trial_id) && !in_array($trial_id, $trial_id_arr)) {
                        $trial_id_arr[] = $trial_id;
                        $trial_title_arr[] = $row['trial_category'];
                    }
                    
                }
//                log_info("jhf koh".json_encode($lead_source_arr));
                for ($j = 0; $j < 4; $j++) {
                    $t_total_lead_count = 0;
                    for ($x = 0; $x < count($trial_id_arr); $x++) {
                        $temp = [];
                        $t_present = 0;
                        $c_id = $trial_id_arr[$x];
                        $c_title = $trial_title_arr[$x];
                        for ($k = 0; $k < count($trial_lead); $k++) {
                            if ($month_array1[$j] == $trial_lead[$k]['month'] && (int) $c_id == $trial_lead[$k]['trial_id']) {
                                $t_present = 1;
                                $trial_new_net[] = $trial_lead[$k];
                                $t_total_lead_count += $trial_lead[$k]['lead_count'];
                            }
                        }
                        if ($t_present == 0) {
                            $temp['month'] = $month_array1[$j];
                            $temp['trial_id'] = $c_id;
                            $temp['trial_category'] = $c_title;
                            $temp['lead_count'] = 0;
                            $temp['db'] = 0;
                            $temp['lead_source'] = '';
                            $trial_new_net[] = $temp;
                        }
                    }
                    $t_total_count_arr[$j] = $t_total_lead_count;
                }

//                log_info("new net  ".json_encode($trial_new_net));
                for ($x = 0; $x < count($trial_id_arr); $x++) {
                    $c_id = $trial_id_arr[$x];
//                    $r = 0;
//                    $lead_group = [];
                    $final_lead_group = [];
                    for ($j = 0; $j < 4; $j++) {
                        $r = 0;
                        $lead_count_temp = 0;
                        for ($k = 0; $k < count($trial_new_net); $k++) {
                            if ($trial_new_net[$k]['month'] == $month_array1[$j] && $trial_new_net[$k]['trial_id'] == $c_id) {
//                           log_info("TRIAL PROGRAM   --$x".$trial_new_net[$k]['trial_category']."  MONTH  --$j".$trial_new_net[$k]['month']);

                                $title = $trial_new_net[$k]['trial_category'];
                                $id = $trial_new_net[$k]['trial_id'];
                                $final_ls_array[$x]['name'] = $title;
                                $final_ls_array[$x]['id'] = $id;
                                $lead_count_temp += $trial_new_net[$k]['lead_count'];
                                $final_ls_array[$x][$month_name_array[$j]] = $lead_count_temp;
                                $lead_source = $trial_new_net[$k]['lead_source'];

                                for ($z = 0; ($z < count($lead_source_arr)); $z++) {
                                    if ($lead_source_arr[$z] == $trial_new_net[$k]['lead_source']) {
                                        if ($r == 0 && !empty($final_lead_group)){
                                            if ($final_lead_group[$r]['name'] !== $trial_new_net[$k]['lead_source']) {
                                                $r++;
                                            }
                                        }
                                        $final_lead_group[$r]['name'] = $trial_new_net[$k]['lead_source'];
                                        $final_lead_group[$r][$month_name_array[$j]] = $trial_new_net[$k]['lead_count'];
//                                   log_info("R  $r inner loop" . json_encode($final_lead_group));
                                        $r++;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    $final_ls_array[$x]['children'] = $final_lead_group;
                }
//                echo json_encode($final_lead_group)."yfaug aiah faf";
            }
        }

        //members
        $trial_id_arr1=$trial_title_arr1=[];
        if($sales_period=='A'){
            $sql1 = sprintf("SELECT period, SUBSTR(period, 1, 4) month, td.trial_id, (select t.trial_title from trial t where t.`trial_id`=td.`trial_id`) trial_category, SUM(active_count) active, SUM(enrolled_count) enrolled, SUM(cancelled_count) cancelled, SUM(didnotstart_count) didnotstart
                FROM `trial_dimensions` td WHERE company_id='%s' AND SUBSTR(period, 1, 4) IN ('%s') GROUP BY trial_id, SUBSTR(period, 1, 4)", mysqli_real_escape_string($this->db, $company_id), $format_list);
        }else if($sales_period=='M'){
            $sql1 = sprintf("SELECT period, SUBSTR(period, 6, 7) month, td.trial_id, (select t.trial_title from trial t where t.`trial_id`=td.`trial_id`) trial_category, SUM(active_count) active, SUM(enrolled_count) enrolled, SUM(cancelled_count) cancelled, SUM(didnotstart_count) didnotstart
                FROM `trial_dimensions` td WHERE company_id='%s' AND period IN ('%s') GROUP BY trial_id, period", mysqli_real_escape_string($this->db, $company_id), $format_list);
        }    
//         log_info("ravideep   ".$sql1);
        $res1 = mysqli_query($this->db, $sql1);
        if(!$res1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($res1);
            if($num_of_rows>0){
                while($row= mysqli_fetch_assoc($res1)){
                    $temp = $temp2 = [];$total = 0;
                    $temp['month'] = $temp2['month'] = (int)$row['month'];
                    $temp['trial_id'] = $temp2['trial_id'] = $trial_id1 = $row['trial_id'];
                    $temp['trial_category'] = $temp2['trial_category'] = $row['trial_category'];
                    $total += $temp['active'] = $temp2['active'] = $row['active'];
                    $total += $temp['enrolled'] = $temp2['enrolled'] = $enrolled = $row['enrolled'];
                    $total += $temp['didnotstart'] = $temp2['didnotstart'] = $row['didnotstart'];
                    $total += $temp['cancelled'] = $temp2['cancelled'] = $row['cancelled'];
                    if($total !== 0){
                    $percentage = number_format(($enrolled/$total)*100,0);
                    }else{
                        $percentage = 0;
                    }
                    $temp['c_factor'] = $temp2['c_factor'] = "$percentage% ".$enrolled."/".$total;
                    $temp['db'] = 1;
                    $trial[] = $temp;
                    if(!empty($trial_id1) && !in_array($trial_id1, $trial_id_arr1)){
                        $trial_id_arr1[] = $trial_id1;
                        $trial_title_arr1[] = $row['trial_category'];
                    }
                }
               
                for($j=0;$j<4;$j++){

                   $t_percent= $t_percentage = $t_total = $active = $enrolled = $dnd = $cancel = 0;
                    for($x=0;$x<count($trial_id_arr1);$x++){
                        $temp=[];
                        $t_present = 0;
                        $c_id = $trial_id_arr1[$x];
                        $c_title = $trial_title_arr1[$x];
                        for($k=0;$k<count($trial);$k++){
                            if ($month_array[$j] == $trial[$k]['month'] && (int) $c_id == $trial[$k]['trial_id']) {
                                $t_present = 1;
                                $trial_new[] = $trial[$k];
                                $enrolled += $trial[$k]['enrolled'];
                                $t_total += $trial[$k]['active'] + $trial[$k]['cancelled'] + $trial[$k]['didnotstart'] + $trial[$k]['enrolled'];
                            }
                        }
                        if($t_present==0){
                            $temp['month'] = $month_array[$j];
                            $temp['trial_id'] = $c_id;
                            $temp['trial_category'] = $c_title;
                            $temp['active'] = 0;
                            $temp['enrolled'] = 0;
                            $temp['didnotstart'] = 0;
                            $temp['cancelled'] = 0;
                            $temp['db'] = 0;
                            $temp['c_factor'] = "0% 0/0";
                            $trial_new[] = $temp;
                        }
                    }
                    if($t_total !== 0){
                    $t_percentage = number_format(($enrolled/$t_total)*100,0);
                    }else{
                        $t_percentage = 0;
                    }
                    $t_total_en_members[$j] = $enrolled;
                    $t_total_conversion_members[$j] = "$t_percentage% ".$enrolled."/".$t_total;
                 }
                
                for($x=0;$x<count($trial_id_arr1);$x++){
                    $c_id = $trial_id_arr1[$x];
                    for($j=0;$j<4;$j++){
                        for($k=0;$k<count($trial_new);$k++){
                            if($trial_new[$k]['month']==$month_array[$j] && $trial_new[$k]['trial_id']==$c_id){
                                $title = $trial_new[$k]['trial_category'];
                                $id = $trial_new[$k]['trial_id'];
                                $final_trials_array[$x][$month_name_array[$j]] = $trial_new[$k]['c_factor'];
                                $final_trials_array[$x]['name']= $title;
                                $final_trials_array[$x]['id'] = $id;
                                $active_members[$month_name_array[$j]] = $trial_new[$k]['active'];
                                $enrolled_members[$month_name_array[$j]] = $trial_new[$k]['enrolled'];
                                $cancelled_members[$month_name_array[$j]] = $trial_new[$k]['cancelled'];
                                $didnotstart_members[$month_name_array[$j]] = $trial_new[$k]['didnotstart'];
                            }                       
                        }
                    }
                    $enrolled_members['name'] = 'Enrolled';
                    $final_trials_array[$x]['children'][] = $enrolled_members;
                    $active_members['name'] = 'Active';
                    $final_trials_array[$x]['children'][] = $active_members;
                    $cancelled_members['name'] = 'Cancelled';
                    $final_trials_array[$x]['children'][] = $cancelled_members;
                    $didnotstart_members['name'] = 'Did not start';
                    $final_trials_array[$x]['children'][] = $didnotstart_members;
                }
            }
        }
        
         $output['count_type'] = $count_type;
        $output['month_leadsource']=$month_name1;
        $output['month_conversion']=$month_name;
         $lead1 = $mem1 = $mem2 = $mem3 = $mem4 = [];
        $lead1['name'] = 'Total';

       for ($i = 0; $i < 4; $i++) {
            $lead1[$month_name_array[$i]] = $t_total_count_arr[$i];
        }
        if (count($final_ls_array) > 0) {
            usort($final_ls_array, function($a, $b) {
                return $a['first_month'] < $b['first_month'];
            });
        }
        $lead1['children'] = $final_ls_array;
       $output['leads'][] = $lead1;

       
        $mem2['name'] = 'Total';
        

        for($i=0;$i<4;$i++){
            $mem2[$month_name_array[$i]] = $t_total_conversion_members[$i];
            
       }

        $mem2['children'] = $final_trials_array;

        $output['trials'][] = $mem2;
        $trial_details  = $this->getTrialDetailByCompany($company_id,'P',1);
                 if($trial_details['status'] == 'Success'){
                     $out['P'] = count($trial_details['msg']['live']);
                     $out['S'] = count($trial_details['msg']['draft']);
                 }else{
                     $out['P'] = 0;
                     $out['S'] = 0;
                 }

        
        $msg = array('status' => "Success", "msg" => $output, "count" => $out);
        $this->response($this->json($msg), 200);
        date_default_timezone_set($curr_time_zone);
        
    }
    
     public function updateTrialDimensionsNetSales($company_id, $trial_id, $date) {
        $sql_add_dim = sprintf("SELECT `trial_id` FROM `trial_registration` WHERE `trial_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $company_id));
        $result_add_dim = mysqli_query($this->db, $sql_add_dim);
        if (!$result_add_dim) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_add_dim");
            log_info($this->json($error_log));
        } else {
            $number_rows = mysqli_num_rows($result_add_dim);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_add_dim)) {
                     $trial_id = $row['trial_id'];
                }
                $this->addTrialDimensions($company_id, $trial_id, '');
            }
        }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $mp_last_upd_date=" DATE(CONVERT_TZ(tp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $last_upd_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";
        if(empty($date)){
            $curr_dt = "($currdate_add)";
        }else{
            $curr_dt = "'$date'";
        }

        $date_inc1= "DATE_FORMAT($curr_dt,'%Y-%m')";
        $date_inc2= "DATE_FORMAT($mp_last_upd_date,'%Y-%m')";
        $date_inc3= "DATE_FORMAT($last_upd_date,'%Y-%m')";
        
         $sql = sprintf("SELECT company_id, trial_id,  mp_ud, sum(amount) amount, payment_date FROM (

                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND (tp.payment_status='S' AND tp.checkout_status='released' || tp.payment_status IN ('MF') || tp.payment_status='M' AND tp.`credit_method`!='MC')
                  AND %s = %s 
                  
                  UNION
                  
                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, concat('-',if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount)) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND  (tp.`payment_status`='R' 
                  AND IF(tr.registration_from='S', tp.payment_intent_id NOT IN (SELECT payment_intent_id FROM trial_payment WHERE payment_status='FR' AND %s = %s),
                  tp.checkout_id NOT IN (SELECT checkout_id FROM trial_payment WHERE payment_status='FR' AND %s = %s)) || tp.`payment_status`='MR')
                  AND %s = %s
                  )t1
                  GROUP BY t1.`trial_id`"

                 , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc2, $date_inc1, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc3, $date_inc1, $date_inc3, $date_inc1, $date_inc2, $date_inc1);
        
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
       } else {
            $trial = $trial_id_arr = [];
            $number_rows = mysqli_num_rows($result);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $temp = $temp2 = [];
                    $temp['trial_id'] = $temp2['trial_id'] = $trial_id = $row['trial_id'];
                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
                    $temp2['amount'] = $amount = $row['amount'];
                    $temp2['mp_ud'] = $row['mp_ud'];

                    if (!empty($trial_id) && !in_array($trial_id, $trial_id_arr, true)) {
                        $trial_id_arr[] = $trial_id;
                        $trial[] = $temp;
                    }
                    $option[] = $temp2;
                }

                for ($i = 0; $i < count($option); $i++) {
                    $date_init = 0;
                   
                    $date_str = "%Y-%m";
                    $curr_dt = date("Y-m-d");
                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
//                     
                    $sql1 = sprintf("SELECT * FROM `trial_dimensions` WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')", mysqli_real_escape_string($this->db, $company_id), $option[$i]['trial_id'], $every_month_dt, $date_str);
                    $res1 = mysqli_query($this->db, $sql1);
                    if (!$res1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        log_info($this->json($error_log));
                    } else {
//                           

                        $num_of_rows = mysqli_num_rows($res1);
                        if ($num_of_rows == 1) {

                            if($i==0){
                                    $sales_value = "'%s'";
                                }else{
                                    $sales_value = "`sales_amount`+'%s'";
                                }
                                $sql3 = sprintf("UPDATE `trial_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `trial_id`='%s' ",mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str,mysqli_real_escape_string($this->db, $trial_id));
                                $res3 = mysqli_query($this->db, $sql3);
                                if (!$res3) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                                    log_info($this->json($error_log));
                            }
                        }
                    }
                }
            } else {
                $sql3 = sprintf("UPDATE `trial_dimensions` SET `sales_amount`=0 WHERE `company_id`='%s' AND `period`=%s AND `trial_id`='%s' ",mysqli_real_escape_string($this->db, $company_id), $date_inc1, mysqli_real_escape_string($this->db, $trial_id));
                $res3 = mysqli_query($this->db, $sql3);
                if (!$res3) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                    log_info($this->json($error_log));
                }
                $error = array('status' => "Failed", "msg" => "No records.", "query" => "$sql");
                log_info($this->json($error));
            }
        }
        date_default_timezone_set($curr_time_zone);
        
       
    }

    public function editTrialDatesandlead($company_id, $student_id, $reg_id, $lead_source, $trial_start_date, $trial_end_date){
         $start_date = $end_date = "";
        
        $start_string="`start_date`='$trial_start_date'";
        $end_date_string = ", `end_date`='$trial_end_date'";
        $lead_string = ", `trial_registration_column_4`='$lead_source'";
       
        $query = sprintf("SELECT * FROM `trial_registration` WHERE `company_id`='%s' AND `trial_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $start_date = $row['start_date'];
                $end_date = $row['end_date'];
                $old_lead_source = $row['trial_registration_column_4'];
            }else{
                $error = array('status' => "Failed", "msg" => "Trial details mismatched.");
                $this->response($this->json($error), 200);
            }
        }
        
        $sql = sprintf("UPDATE `trial_registration` SET $start_string $end_date_string $lead_string WHERE `company_id`='%s' AND `trial_reg_id`='%s'", 
                 mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id));
        $res = mysqli_query($this->db, $sql);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $curr_date = gmdate("Y-m-d H:i:s");
            if($start_date!=$trial_start_date){
                $text = "Trial start date changed to ". date("F j, Y", strtotime($trial_start_date));            
                $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `student_id`, `trial_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $reg_id), 'trial start date update', $text, $curr_date);
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
            if($end_date!=$trial_end_date){
                $text = "Trial end date changed to ". date("F j, Y", strtotime($trial_end_date));            
                $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `student_id`, `trial_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $reg_id), 'trial end date update', $text, $curr_date);
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
             if($old_lead_source!=$lead_source){
                $text = "Trial lead source changed to "."$lead_source";            
                $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `student_id`, `trial_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $reg_id), 'trial lead source update', $text, $curr_date);
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
            
            $reg_details = $this->getTrialRegDetails($company_id, $reg_id, 1);
            $msg = array('status' => "Success", "msg" => "Trial registration details updated successfully.", "reg_details"=>$reg_details);
            $this->response($this->json($msg), 200);
        }
    }
    
     protected function sendpush($data, $ios, $android, $socket_url) {
        $send_count = 0;
        //ios
        if (!empty($ios)) {
//            $old_fle_content = '';
            $ctx = stream_context_create();
            for ($r = 0; $r < count($ios['device_token']); $r++) {
//                $token_arr = [];
                $app_id = $ios['app_id'][$r];
                $deviceToken = $ios['device_token'][$r];
                $unread_msg_count = $ios['unread_msg_count'][$r];
//                $new_file_content = file_get_contents($ios['pem'][$r]);
//                if ((!empty($old_fle_content) && $old_fle_content !== $new_file_content) || $r == 0) {
//                    if ($r !== 0) {
//                        fclose($fp);
//                    }
                stream_context_set_option($ctx, 'ssl', 'local_cert', $ios['pem'][$r]);
                stream_context_set_option($ctx, 'ssl', 'passphrase', 'mystudio');
                $fp = stream_socket_client($socket_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                if (!$fp) {
                    $error_log = array('status' => "Failed", "msg" => "Message not delivered(I)[Error - $errstr($err)].", "App id" => $app_id, "Token" => "$deviceToken");
                    log_info($this->json($error_log));
                    continue;
                }
//                }
                // Build the binary notification
                $token_arr = explode(",", $deviceToken);
                $unread_msg_arr = explode(",", $unread_msg_count);
                if (count($token_arr) > 0) {
                    $chunk_token_arr = array_chunk($token_arr, 8);
                    $chunk_bagde_count = array_chunk($unread_msg_arr, 8);
                    $flag = 1;
                    for ($j = 0; $j < count($chunk_token_arr); $j++) {
                        if ($j !== 0) {
                            if ($flag == 1) {
                                fclose($fp);
                            }
                            $deviceToken = implode(",", $chunk_token_arr[$j]);
                            stream_context_set_option($ctx, 'ssl', 'local_cert', $ios['pem'][$r]);
                            stream_context_set_option($ctx, 'ssl', 'passphrase', 'mystudio');
                            $fp = stream_socket_client($socket_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                            if (!$fp) {
                                $error_log = array('status' => "Failed", "msg" => "Message not delivered(I)[Error - $errstr($err)].", "App id" => $app_id, "Token" => "$deviceToken");
                                log_info($this->json($error_log));
                                $flag = 0;
                                continue;
                            } else {
                                $flag = 1;
                            }
                        }
                        for ($c = 0; $c < count($chunk_token_arr[$j]); $c++) {
                            $body['aps'] = array(
                                'alert' => array(
                                    'title' => 'New Message received',
                                    'body' => $data
                                ),
                                'sound' => 'default',
                                'badge' => (int)$chunk_bagde_count[$j][$c]
                            );
                            // Encode the payload as JSON
                            $payload = json_encode($body);
                            $msg = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $chunk_token_arr[$j][$c])) . pack("n", strlen($payload)) . $payload;
                            $result = fwrite($fp, $msg, strlen($msg));
                            // Send it to the server
                            if (!$result) {
                                $error_log = array('status' => "Failed", "msg" => "Message not delivered(I).", "Token" => $chunk_token_arr[$j][$c], "PL" => $payload);
                                log_info($this->json($error_log));
                            } else {
                                $error_log = array('status' => "Success", "msg" => "Message successfully delivered(I).", "Token" => $chunk_token_arr[$j][$c], "PL" => $payload);
                                log_info($this->json($error_log));
                                $send_count++;
                            }
                        }
                    }
                    fclose($fp);
                }
//                $old_fle_content = file_get_contents($ios['pem'][$r]);
            }
        }

        if (!empty($android)) {
            //Android	
            $url = 'https://fcm.googleapis.com/fcm/send';
//            for ($r = 0; $r < count($android['device_token']); $r++) {
            foreach ($android['device_token'] as $point => $values) {
                $key = $android['key'][$point];
                $deviceToken = array_chunk($values, 900);
                for ($j = 0; $j < count($deviceToken); $j++) {
                    $ch = curl_init();
                    $devicetoken = implode(",", $deviceToken[$j]);
                    $fields = array(
                        'registration_ids' => $deviceToken[$j],
                        'data' => array(
                            "message" => $data,
                            "title" => 'New Message',
                            "badge" => 1
                        )
                    );
                    $fields = json_encode($fields);
                    $headers = array(
                        'Authorization: key=' . $key,
                        'Content-Type: application/json'
                    );
                    if ($url) {
                        // Set the url, number of POST vars, POST data
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        // Disabling SSL Certificate support temporarly
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        if ($fields) {
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                        }

                        // Execute post
                        $result = curl_exec($ch);
                        $resultArr = json_decode($result, true);
                        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        if ($result === FALSE || $resultArr['success'] == 0) {
                            //die('Curl failed: ' . curl_error($ch));
                            $error_log = array('status' => "Failed", "msg" => "Message not delivered(A).", "Token" => "$devicetoken", "StatusCode" => $returnCode, "Result" => json_encode($resultArr));
                            log_info($this->json($error_log));
                        } else {
                            $error_log = array('status' => "Success", "msg" => "Message successfully delivered(A).", "Token" => "$devicetoken", "StatusCode" => $returnCode, "Result" => json_encode($resultArr));
                            log_info($this->json($error_log));
                            $send_count += $resultArr['success'];
                        }
                    }
                    curl_close($ch);
                }
            }
        }
        return $send_count;
    }
    
     public function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
  
    public function getTrialAttendanceDetails($company_id,$reg_id,$call_back){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $check_dt_tm = " IF(TIME(`checkin_datetime`)!='00:00:00', CONVERT_TZ(`checkin_datetime`,$tzadd_add,'$new_timezone'), `checkin_datetime`)";
//        $last_ad_dt=" IF(TIME(mr.`last_advanced_date`)!='00:00:00', CONVERT_TZ(mr.`last_advanced_date`,$tzadd_add,'$new_timezone'), mr.`last_advanced_date`)";
        date_default_timezone_set($curr_time_zone);
        $date_format = "'%b %d, %Y'";
        $time_format="'%h:%i:%p'";
        $date_format2="'%e/%c/%Y'";
        $out=$days_count=[];
        
        $sql = sprintf(" SELECT  ifnull(SUM($check_dt_tm > CURDATE() - INTERVAL 7 DAY),0) as last_7_days,ifnull(SUM($check_dt_tm > CURDATE() - INTERVAL 14 DAY),0) as last_14_days, ifnull(SUM($check_dt_tm > CURDATE() - INTERVAL 30 DAY),0) as last_30_days,
                ifnull(SUM($check_dt_tm > CURDATE() - INTERVAL 60 DAY),0) as last_60_days,ifnull(SUM($check_dt_tm > CURDATE() - INTERVAL 90 DAY),0) as last_90_days
                from trial_registration tr LEFT JOIN trial_attendance ta on  ta.`trial_reg_id`=tr.`trial_reg_id`  
                where ta.`trial_reg_id`='%s' and ta.company_id='%s' and ta.`status`='N'",
                mysqli_real_escape_string($this->db, $reg_id),mysqli_real_escape_string($this->db, $company_id));
        $result=  mysqli_query($this->db, $sql);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if($call_back==1){
                return $error;
            }else{
                $this->response($this->json($error), 200);
            }
        }else{
            $num1 = mysqli_num_rows($result);
            if($num1>0){
                $days_count = mysqli_fetch_assoc($result);
            }
        }
        
        $sqlselect=sprintf("SELECT  ta.`trial_attendance_id`, ta.`trial_reg_id`, CONCAT(date_format($check_dt_tm,%s), ' checked in at ', TIME_FORMAT($check_dt_tm, %s)) as text, date_format($check_dt_tm,%s) as checkin_date, $check_dt_tm checkin_datetime, tr.`trial_status`
                FROM `trial_attendance` ta LEFT JOIN `trial_registration` tr ON tr.`trial_reg_id`=ta.`trial_reg_id`
                WHERE ta.`trial_reg_id`='%s' and ta.company_id='%s' and ta.`status`='N' ORDER BY checkin_datetime",
                $date_format,$time_format,$date_format2, mysqli_real_escape_string($this->db, $reg_id),mysqli_real_escape_string($this->db, $company_id));
        $resultselect=  mysqli_query($this->db, $sqlselect);
        if(!$resultselect){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sqlselect");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if($call_back==1){
                return $error;
            }else{
                $this->response($this->json($error), 200);
            }
        }else{
            $num_of_rows=  mysqli_num_rows($resultselect);
            if($num_of_rows > 0){
                $out['attendance_history']=[];
                $out['checkin_dates']=[];
                while($rows=  mysqli_fetch_assoc($resultselect)){
                        $out['attendance_history'][]=$rows;
                        $out['checkin_dates'][]=$rows['checkin_date'];
                    }
            }else{
                $out['attendance_history']=[];
                $out['checkin_dates']=[];
            }
        }
        
        $msg = array('status' => "Success", "msg" => $out, "days_count"=>$days_count);
        if($call_back==1){
            return $msg;
        }else{
            $this->response($this->json($msg), 200);
        }
    }
    
    public  function  deleteOrAddTrialAttendanceDetails($company_id,$reg_id,$flag,$attendance_id,$attendance_date_time,$override_flag){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_new = "CONVERT_TZ('%s','$new_timezone',$tzadd_add)";
        $given_date_time = "CONVERT_TZ('$attendance_date_time','$new_timezone',$tzadd_add)";
        $checkin_date_time = " CONVERT_TZ(`checkin_datetime`,$tzadd_add,'$new_timezone')";
        if($flag=='D'){
            $msg="Trial Attendance History deleted successfully";
            $sql=  sprintf("UPDATE `trial_attendance` SET `status`='%s' WHERE `trial_attendance_id`='%s' and `company_id`='%s'",mysqli_real_escape_string($this->db, $flag),mysqli_real_escape_string($this->db, $attendance_id),mysqli_real_escape_string($this->db, $company_id));
        }else if($flag=='A'){
            $msg="Trial Attendance History added successfully";
            $selectsql=sprintf("SELECT `trial_id`,`student_id`,`participant_id` FROM `trial_registration` WHERE `trial_reg_id`='%s' and `company_id`='%s'",
                    mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $company_id));
            $resultselect=  mysqli_query($this->db, $selectsql);
            if(!$resultselect){
                 $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                $num_of_rows=  mysqli_num_rows($resultselect);
                if($num_of_rows>0){
                    $rows=  mysqli_fetch_assoc($resultselect);
                    $check_flag=sprintf("SELECT tr.trial_reg_id, IFNULL((SELECT 'Y' as flag FROM `trial_attendance` WHERE `company_id`=tr.`company_id` AND `trial_reg_id`=tr.`trial_reg_id` AND `status`='N' AND $checkin_date_time BETWEEN '$attendance_date_time'-INTERVAL 30 MINUTE AND '$attendance_date_time'+INTERVAL 30 MINUTE LIMIT 0,1),'N') checkin_flag
                              FROM `trial_registration` tr  WHERE tr.`trial_reg_id`='%s' and tr.company_id='%s'",mysqli_real_escape_string($this->db, $reg_id),mysqli_real_escape_string($this->db, $company_id));
                            $resultcheck_flag=  mysqli_query($this->db, $check_flag);
            if(!$resultcheck_flag){
                 $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_flag");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                $num_of_rows2=  mysqli_num_rows($resultcheck_flag);
                if($num_of_rows2>0){
                    $rows2=  mysqli_fetch_assoc($resultcheck_flag);
                    if($rows2['checkin_flag']=='Y'){
                        $error = array('status' => "Failed", "msg" => "Attendance Log already exists within this date-time limit.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
//                    $getParticularMembershipDetails = $this->getTrialAttendanceOverrideValidation($company_id, $reg_id, "'$attendance_date_time'", 1);
//                    if($getParticularMembershipDetails["status"]!="Success"){
//                        $this->response($this->json($getParticularMembershipDetails), 200);
//                    }
//                    if($override_flag!='Y'){
//                        if(isset($getParticularMembershipDetails['msg'])){
//                            if($getParticularMembershipDetails['msg']['class_excd_flg']=='Y'){
//                                $error = array('status' => "Failed", "msg" => "You are a rock star and have reached your attendance limit. Please talk to us about adding on more classes.");
//                                $this->response($this->json($error), 200);
//                            }
//                        }
//                    }
//                    if($getParticularMembershipDetails['msg']['checkin_flag']=='Y'){
//                        $error = array('status' => "Failed", "msg" => "Attendance Log already exists within this date-time limit.");
//                        $this->response($this->json($error), 200);
//                    }
                    
                    $sql=sprintf("INSERT INTO `trial_attendance`( `company_id`, `participant_id`, `trial_reg_id`, `trial_id`, `checkin_datetime`, `status`, `checkin_mode` ) 
                            VALUES ('%s','%s','%s','%s',$currdate_new,'%s','%s')",mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db,$rows['participant_id']),mysqli_real_escape_string($this->db,$reg_id),
                            mysqli_real_escape_string($this->db, $rows['trial_id']), mysqli_real_escape_string($this->db,$attendance_date_time),'N','M');
                }else{
                    $error = array('status' => "Failed", "msg" => "Registration details mismatched.");
                    $this->response($this->json($error), 200);
                }
            }
        }
        $result=  mysqli_query($this->db, $sql);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $attendance_details=$this->getTrialAttendanceDetails($company_id,$reg_id,1);
            $message = array('status' => "Success", "msg" => "$msg", "attendance_details" => $attendance_details);
            $this->response($this->json($message), 200);
        }
    }
    
     public function checkBouncedEmailCount($reg_id_list){
        $sql=sprintf("SELECT count(*) as b_count , count(`bounced_mail`) as Bounced_count,sum(if(t.`buyer_email` !=s.`student_email`,1,0)) as s_count, sum(if(s.deleted_flag='Y',1,0)) as deleted_count FROM `trial_registration` t left JOIN `student` s ON  s.`student_id` = t.`student_id`
            and s.`company_id` = t.`company_id`left JOIN `bounce_email_verify` b ON  t.`buyer_email` = b.`bounced_mail` AND (b.`company_id`=0 OR b.`company_id`=s.`company_id`)
            where t.`trial_reg_id` in (%s);",mysqli_real_escape_string($this->db,$reg_id_list));
        log_info($sql);
        $result= mysqli_query($this->db, $sql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
             if(mysqli_num_rows($result)>0){
                 $rows = mysqli_fetch_assoc($result);
                  $rows['failed_count']=$rows['Bounced_count']+$rows['deleted_count'];
                 return $rows;
             }
        }
    }
    
    public function verifyBouncedEmailForCC($cc_address, $company_id){
        $selecsql=sprintf("SELECT * FROM `bounce_email_verify` b WHERE b.`bounced_mail`='%s' AND (b.`company_id`=0 OR b.`company_id`='%s')", mysqli_real_escape_string($this->db,$cc_address), mysqli_real_escape_string($this->db,$company_id));
        $result= mysqli_query($this->db,$selecsql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
          if(mysqli_num_rows($result)>0){
              return 0;
          }else{
               return 1;
          }
        }
    }
    
    public function getStudioSubscriptionStatus($company_id){
        $query = sprintf("SELECT email_id,upgrade_status,subscription_status,wepay_status,stripe_status,stripe_subscription,studio_expiry_level,expiry_dt FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_email = $row['email_id'];
                $upgrade_status = $row['upgrade_status'];
                $subscription_status = $row['subscription_status'];
                $wepay_status = $row['wepay_status'];
                $stripe_status = $row['stripe_status'];
                $stripe_subscription = $row['stripe_subscription'];
                $studio_expiry_level = $row['studio_expiry_level'];
                $expiry_dt = $row['expiry_dt'];
                if (!empty(trim($upgrade_status)) && $subscription_status=='N' && ($studio_expiry_level=='L2' || $upgrade_status=='F')) {
                    $res = array('status' => "Failed", "msg" => "Please contact the business operator to activate the app.", "studio_email"=>$studio_email, "upgrade_status"=>$upgrade_status, "studio_expiry_level" => $studio_expiry_level);
                    $this->response($this->json($res), 200);
                }else{
                    $res = array('status' => "Success", "msg" => "Studio subscription status success", "upgrade_status" => $upgrade_status);
                    return $res;
                }
                
            } else {
                $res = array('status' => "Failed", "msg" => "Studio Details not found.");
                $this->response($this->json($res), 200);
            }

        }
    }
    
    public function updateTrialPaymentMethodDetails($company_id,$trial_reg_id,$new_cc_id,$cc_state,$buyer_first_name,$buyer_last_name,$buyer_email,$postal_code){
         $gmt_date=gmdate(DATE_RFC822);
         $bactivity_type = $bactivity_text = "";
        $sns_msg2=array("buyer_email"=>$buyer_email,"membership_title"=>$trial_reg_id,"gmt_date"=>$gmt_date);
        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency`, 
                  tr.`payment_type` ,tr.`buyer_first_name`,tr.`buyer_last_name`,tr.`student_id`
                  FROM `trial_registration` tr LEFT JOIN `wp_account` wp ON tr.`company_id`=wp.`company_id` 
                  WHERE tr.`trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];                
                $payment_type = $row['payment_type'];
                $student_id = $row['student_id'];
                $old_buyer_first_name = $row['buyer_first_name'];
                $old_buyer_last_name = $row['buyer_last_name'];
                if ($old_buyer_first_name !== $buyer_first_name && $old_buyer_last_name !== $buyer_last_name) {
                    $bactivity_type = "buyer name edit";
                    $bactivity_text = "buyer name is changed from $old_buyer_first_name $old_buyer_last_name to $buyer_first_name $buyer_last_name.";
                } elseif ($old_buyer_first_name !== $buyer_first_name) {
                    $bactivity_type = "buyer name edit";
                    $bactivity_text = "buyer first name is changed from $old_buyer_first_name to $buyer_first_name";
                } elseif ($old_buyer_last_name !== $buyer_last_name) {
                    $bactivity_type = "buyer name edit";
                    $bactivity_text = "buyer last name is changed from $old_buyer_last_name to $buyer_last_name";
                }
//                $schedule_date = $row['s_date'];
                
                if($user_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "You may delete your account yourselves, or the account may be deleted by WePay customer support.");
                    $this->response($this->json($log),200);
                }
                
                if($acc_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log),200);
                }elseif($acc_state=='disabled'){
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log),200);
                }
                
                if(!empty($access_token) && !empty($account_id)){
                    if (!function_exists('getallheaders')){
                        if(!function_exists ('apache_request_headers')){
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        }else{
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    }else{
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        
                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                    
//                    $json = array("account_id"=>"$account_id");
//                    $postData = json_encode($json);
//                    $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent,$sns_msg2);
//                    $this->wp->log_info("wepay_get_account: ".$response['status']);
//                    if($response['status']=='Success'){
//                        $res = $response['msg'];
//                        $account_state = $res['state'];
//                        $currency = $res['currencies'][0];
//                        $curr_acc = implode(",", $res['currencies']);
//                        $disabled_reasons = implode(",", $res['disabled_reasons']);
//                        if($account_state=='deleted'){
//                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
//                            $this->response($this->json($log),200);
//                        }elseif($account_state=='disabled'){
//                            $log = array("status" => "Failed", "msg" => "The account has been disabled for following reason(s) - ".$disabled_reasons." by WePay and can no longer accept payments.");
//                            $this->response($this->json($log),200);
//                        }
//                    }else{
//                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                        $this->response($this->json($log),200);
//                    }
                    
                    $cc_name = $cc_month = $cc_year = '';
                    $cc_status = $cc_state;
                    $json2 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$new_cc_id,"account_id"=>$account_id);
                    $postData2 = json_encode($json2);                    
                    $sns_msg2['call'] = "Credit Card";
                    $sns_msg2['type'] = "Credit Card Authorization";
                    $response2 = $this->wp->accessWepayApi("credit_card/authorize",'POST',$postData2,$token,$user_agent,$sns_msg2);
                    $this->wp->log_info("wepay_credit_card_authorize: ".$response2['status']);
                    if($response2['status']=="Success"){
                        $res2 = $response2['msg'];
                        $cc_name = $res2['credit_card_name'];
                        $cc_new_state = $res2['state'];
                        if($cc_state!=$cc_new_state){
                            $cc_status = $cc_new_state;
                        }
                        $cc_month = $res2['expiration_month'];
                        $cc_year = $res2['expiration_year'];
                    }else{
                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                        $this->response($this->json($error),200);
                    }
                    
//                    if($payment_type=='R'){
//                        if(date("m", strtotime(. date("Y/m/d") .))<=$cc_month){
//                            
//                        }else{
//                            $error = array("status" => "Failed", "msg" => "Credit card is not valid till the last recurring payment.");
//                            $this->response($this->json($error),200);
//                        }
//                    }
                    
                    $update_query = sprintf("UPDATE `trial_registration` SET `buyer_first_name`='%s',`buyer_last_name`='%s',`buyer_email`='%s',`buyer_postal_code`='%s',`credit_card_id`='%s',
                            `credit_card_status`='%s',`credit_card_name`='%s',`credit_card_expiration_month`='%s',`credit_card_expiration_year`='%s' WHERE `trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $buyer_first_name),mysqli_real_escape_string($this->db, $buyer_last_name),
                            mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $postal_code), mysqli_real_escape_string($this->db, $new_cc_id),
                            mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month),
                            mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $trial_reg_id));
                    $result_update = mysqli_query($this->db, $update_query);
                    if(!$result_update){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $curr_date = gmdate("Y-m-d H:i:s");
                        if (!empty($bactivity_type)) {
                            $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), $student_id, $bactivity_type, $bactivity_text, $curr_date);
                            $result_history = mysqli_query($this->db, $insert_history);
                            if (!$result_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                        $update_query2 = sprintf("UPDATE `trial_payment` SET `cc_id`='%s', `cc_name`='%s' WHERE `trial_reg_id`='%s' AND `payment_status` IN ('N','F')",
                                mysqli_real_escape_string($this->db, $new_cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $trial_reg_id));
                        $result_update2 = mysqli_query($this->db, $update_query2);
                        if(!$result_update2){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query2");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                        
                        $activity_text = "Payment method changed ($cc_name)";
                        $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), $student_id, 'Payment method', $activity_text, $curr_date);
                        $result_history = mysqli_query($this->db, $insert_history);
                        if (!$result_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                            $reg_details = $this->getTrialRegDetails($company_id, $trial_reg_id, 1);
                            $msg = array('status' => "Success", "msg" => "New Payment method successfully updated.", "reg_details" => $reg_details);
                            $this->response($this->json($msg), 200);
                     }
                }else{
                    $error = array('status' => "Failed", "msg" => "Wepay details not exist.");
                    $this->response($this->json($error), 200);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Particpant payment details mismatch.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function updateTrialPaymentDetailsAsPaid($company_id,$reg_id,$payment_id,$credit_amount,$credit_method){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $curr_date = date("Y-m-d");
       $partial_credit_flag = $payment_updt_string = "";
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
       
        date_default_timezone_set($curr_time_zone);
        $select_query = sprintf("SELECT tp.*, tr.`processing_fee_type`, tr.`trial_status`, tr.`payment_type`, tr.`credit_card_id`, tr.`student_id`, tr.`credit_card_name`, c.`upgrade_status`, c.`wp_currency_symbol`,tr.`trial_id` 
                FROM `trial_payment` tp LEFT JOIN `trial_registration` tr ON tp.`trial_reg_id`=tr.`trial_reg_id`
                LEFT JOIN `company` c ON tr.`company_id`=c.`company_id`
                WHERE tr.`company_id`='%s' AND tp.`trial_reg_id`='%s' AND tp.`trial_payment_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $payment_id));
        $result_select_query = mysqli_query($this->db, $select_query);
        if (!$result_select_query) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result_select_query);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result_select_query);
                $original_record_status = $status = $row['payment_status'];
                $amount_in_db = $row['payment_amount'];
                $date_in_db = $row['payment_date'];
                $wp_currency_symbol = $row['wp_currency_symbol'];
                $rem_amt = $amount_in_db-$credit_amount;
                $updated_amt = $amount_in_db;
                $trial_id = $row['trial_id'];
                $student_id = $row['student_id'];
//                $updated_amt = $rem_amt;
                $original_record_date = $date_in_db;
                $original_record_fee = 0;
//                echo "".$credit_amount.Amount in Db1  ".$amount_in_db."<br>"."remaining_amount  ".$rem_amt."<br>"."updated_amount  ".$updated_amt."<br>"."amount_in_db2  ".$amount_in_db."<br>";
                if($status!='N' && $status!='F'){
                    if($status=='C'){
                        $msg = array('status' => "Failed", "msg" => "Selected Payment has been cancelled already.");
                        $this->response($this->json($msg), 200);
                    }elseif($status=='P'){
                        $msg = array('status' => "Failed", "msg" => "Selected Payment is currently on-hold.");
                        $this->response($this->json($msg), 200);
                    }elseif($status=='R' || $status=='CR' ){
                        $msg = array('status' => "Failed", "msg" => "Selected Payment has already been paid and refunded.");
                        $this->response($this->json($msg), 200);
                    }elseif($status=='PR'){
                        $msg = array('status' => "Failed", "msg" => "Selected payment has already been paid and partially refunded.");
                        $this->response($this->json($msg), 200);
                    }elseif($status=='S'){
                        $msg = array('status' => "Failed", "msg" => "Selected payment has been paid already.");
                        $this->response($this->json($msg), 200);
                    }else{
                        $msg = array('status' => "Failed", "msg" => "Invalid schedule status.");
                        $this->response($this->json($msg), 200);
                    }
                }
                if($credit_amount != $amount_in_db){ //partial apply payment credit
                    $msg = array('status' => "Failed", "msg" => "Credit amount should be equal to bill amount.");
                    $this->response($this->json($msg), 200);
                }else{
                    if($rem_amt!=0){
                        $msg = array('status' => "Failed", "msg" => "Credit amount should be equal to bill amount.");
                        $this->response($this->json($msg), 200);
                    } else {
                        $original_record_status = 'M';
                        $original_record_date = $curr_date;
                    }
                }//,`payment_date`='$original_record_date'
                
                if ($credit_method == 'CA' || $credit_method == 'CH') {
                    $update_sales_in_reg = sprintf("UPDATE `trial_registration` SET `paid_amount`=`paid_amount`+$credit_amount WHERE `trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $reg_id));
                    $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);
                    if (!$result_sales_in_reg) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }

                    $update_count = sprintf("UPDATE `trial` SET `net_sales`=`net_sales`+$credit_amount WHERE `company_id`='%s' AND `trial_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
                    $result_count = mysqli_query($this->db, $update_count);
                    if (!$result_count) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    if($credit_method  == 'CA') {
                        $credit_type = $credit_string = 'cash';
                    } else {
                        $credit_type = $credit_string = 'check';
                    }
                    $this->updateTrialDimensionsNetSales($company_id,$trial_id,'');
                } else {
                    $credit_type = $credit_string = 'manual credit';
                }
                
                $update_query = sprintf("UPDATE `trial_payment` SET `payment_status`='$original_record_status',`credit_method`='%s' WHERE `trial_reg_id`='%s' AND `trial_payment_id`='%s'", 
                        mysqli_real_escape_string($this->db, $credit_method), mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $payment_id));
                $result_update_query = mysqli_query($this->db, $update_query);
                if (!$result_update_query) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }

                $curr_date = gmdate("Y-m-d H:i:s");
                $scheduled_date = date("M d, Y",strtotime($date_in_db));

                $text = $wp_currency_symbol . "$credit_amount $credit_string applied to payment scheduled on " . $scheduled_date;
                $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), $student_id, $credit_type, $text, $curr_date);
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }

                $payment_history = $this->getpaymentHistoryDetails($company_id, $reg_id, 1);
                $log = array("status" => "Success", "msg" => "Manual Credit applied.", "payment_history" => $payment_history);
                $this->response($this->json($log),200);
                
            }else{
                $error = array('status' => "Failed", "msg" => "No records found.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    protected function getunsubscribefooter($company_id) {
        $sql = sprintf("SELECT company_name, CONCAT(street_address, IF(street_address = '' , city, CONCAT(', ',city)),IF(street_address = '' OR city = '', state, CONCAT(', ',state)),IF(street_address = '' OR city = '' OR state = '', postal_code, CONCAT(' ',postal_code))) address 
                    from company where company_id = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_name = $row['company_name'];
                $studio_address = trim($row['address']);
                if(!empty($studio_address)){
                    $studio_address = ($studio_address[strlen($studio_address) - 1] === "," ? substr($studio_address, 0, -1) : $studio_address);
                }
                $footer_detail = $studio_name . ($studio_address === "" ? '' : " | $studio_address");
                return $footer_detail;
            }
        }
    }
    
    public function stripetrialProgramCheckout($company_id, $trial_id, $actual_student_id, $actual_participant_id, $trial_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $payment_method_id, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $processing_fee_type, $discount, $upgrade_status, $reg_type_user, $reg_version_user, $program_length, $program_length_type, $start_date, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $payment_method,$intent_method_type,$verification_status,$payment_intent_id,$cc_type,$stripe_customer_id) {
        $stripe_account_id = $stripe_currency = $studio_name = $studio_stripe_status = '';
        $cc_month = $cc_year = $stripe_card_name = $button_url = '';
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
        if (($stripe_account != '' && $stripe_account['charges_enabled'] == 'Y' && $payment_amount > 0 ) || $payment_amount == 0) {
            $this->addTrialDimensions($company_id, $trial_id, '');
            $upgrade_status = $sts['upgrade_status'];
            
            $query1 = sprintf("SELECT c.`company_name`,c.`stripe_status`,s.`account_state`,s.`currency`,s.`account_id` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error1), 200);
            } else {
                if (mysqli_num_rows($result1) > 0) {
                    $row = mysqli_fetch_assoc($result1);
                    $stripe_account_id = $row['account_id'];
                    $stripe_currency = $row['currency'];
                    $stripe_account_state = $row['account_state'];
                    $studio_name = $row['company_name'];
                    $studio_stripe_status = $row['stripe_status'];
                }
            }
            
            if (is_null($actual_student_id) || $actual_student_id == 0) {
                $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Stripe', $studio_name);
                $student_id = $stud_array['student_id'];
                $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
            } else {
                $student_id = $actual_student_id;
                if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                    $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                }
            }
            if (is_null($actual_participant_id) || $actual_participant_id == 0) {
                $participant_id = $this->getParticipantId($company_id, $student_id, $reg_col1, $reg_col2, $reg_col3);
            } else {
                $participant_id = $actual_participant_id;
            }
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone = $user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";

            $current_date = date("Y-m-d");
            if ($program_length_type == 'D') {
                $end_date = date("Y-m-d", strtotime("+$program_length days", strtotime($start_date)));
            } else {
                $end_date = date("Y-m-d", strtotime("+$program_length week", strtotime($start_date)));
            }

            $p_f = $this->getProcessingFee($company_id, $upgrade_status, $payment_method);
            $process_fee_per = $p_f['PP'];
            $process_fee_val = $p_f['PT'];

            if ($payment_amount > 0) {
                $payment_type = 'O';
                $processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
            } else {
                $payment_type = 'F';
                $processing_fee = 0;
            }
            
            $w_paid_amount = $paid_amount = $paid_fee = $amount_in_dollars = $dest_amount = $dest_amount_in_dollars = $w_paid_amount_round = $dest_amount_round = $insert_paid_amount = 0;
            $checkout_state = $cc_month = $cc_year = $stripe_card_name = '';
            $has_3D_secure = 'N';
            
            if ($studio_stripe_status == 'Y' && $stripe_account_state != 'N') {
                if ($payment_amount > 0 && $payment_method == "CC") {
                    $paid_amount = $payment_amount;
                    $paid_fee = $processing_fee;

                    if ($processing_fee_type == 2) {
                        $w_paid_amount = $payment_amount + $processing_fee;
                    } elseif ($processing_fee_type == 1) {
                        $w_paid_amount = $payment_amount;
                    }
                    $w_paid_amount_round = round($w_paid_amount, 2);
                    $dest_amount = $w_paid_amount - $processing_fee;
                    $dest_amount_round = round($dest_amount, 2);
                    $dest_amount_in_dollars = $dest_amount_round * 100;
                    $amount_in_dollars = $w_paid_amount_round * 100;
                    $p_type = 'trial';
                    $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $p_type, $cc_type);
                    if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'released';
                    } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'requires_action';
                        $has_3D_secure = 'Y';
                        $next_action_type = $payment_intent_result['next_action']['type'];
                        if($next_action_type != "redirect_to_url"){
                            # Payment not supported
                            $out['status'] = 'Failed';
                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                            $this->response($this->json($out), 200);
                        }
                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                    } else {
                        # Invalid status
                        $out['status'] = 'Failed';
                        $out['msg'] = 'Invalid SetupIntent status.';
                        $out['error'] = 'Invalid SetupIntent status';
                        $this->response($this->json($out), 200);
                    }
                    if (!empty($payment_method_id)) {

                        try {
                            $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                            $brand = $retrive_payment_method->card->brand;
                            $cc_month = $retrive_payment_method->card->exp_month;
                            $cc_year = $retrive_payment_method->card->exp_year;
                            $last4 = $retrive_payment_method->card->last4;
                            $temp = ' xxxxxx';
                            $stripe_card_name = $brand . $temp . $last4;
                        } catch (\Stripe\Error\Card $e) {
                            // Since it's a decline, \Stripe\Error\Card will be caught     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\RateLimit $e) {
                            // Too many requests made to the API too quickly     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\InvalidRequest $e) {
                            // Invalid parameters were supplied to Stripe's API     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Authentication $e) {
                            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\ApiConnection $e) {
                            // Network communication with Stripe failed     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Base $e) {
                            // Display a very generic error to the user, and maybe send yourself an email     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (Exception $e) {
                            // Something else happened, completely unrelated to Stripe     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        }
                        if (isset($err) && !empty($err)) {
                            $error = array('status' => "Failed", "msg" => $err['message']);
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }
//            if ($processing_fee_type == 1) {
//                $paid_amount -= $processing_fee;
//            }
            
            if ($checkout_state == 'released'){
                if ($processing_fee_type == 2) {
                    $insert_paid_amount = $payment_amount;
                } elseif ($processing_fee_type == 1) {
                    $insert_paid_amount = $payment_amount - $processing_fee;
                }
            }else{
                $insert_paid_amount = 0;
            }
            $payment_method = 'CC';
            $registration_from = 'S';
            $payment_from = 'S';
            $query = sprintf("INSERT INTO `trial_registration`(`company_id`, `trial_id`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                    `payment_type`, `payment_amount`,`registration_from`, `payment_method_id`, `stripe_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`,  `start_date`, `end_date`, `trial_status`, `discount`,
                    `trial_registration_column_1`, `trial_registration_column_2`, `trial_registration_column_3`, `trial_registration_column_4`, `trial_registration_column_5`, `trial_registration_column_6`, `trial_registration_column_7`, `trial_registration_column_8`, `trial_registration_column_9`, `trial_registration_column_10`,
                    `trial_reg_type_user`,`trial_reg_version_user`,`program_length`,`program_length_type`, `trial_participant_street`, `trial_participant_city`, `trial_participant_state`, `trial_participant_zip`, `trial_participant_country`, `paid_amount`,`stripe_customer_id`,`payment_method`)
                        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id), mysqli_real_escape_string($this->db, $buyer_first_name),
                    mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $processing_fee),
                    mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $registration_from), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $start_date),
                    mysqli_real_escape_string($this->db, $end_date), mysqli_real_escape_string($this->db, 'A'), mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4), mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), 
                    mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $program_length), mysqli_real_escape_string($this->db, $program_length_type), mysqli_real_escape_string($this->db, $participant_street),
                    mysqli_real_escape_string($this->db, $participant_city), mysqli_real_escape_string($this->db, $participant_state), mysqli_real_escape_string($this->db, $participant_zip), mysqli_real_escape_string($this->db, $participant_country), mysqli_real_escape_string($this->db, $insert_paid_amount), mysqli_real_escape_string($this->db, $stripe_customer_id),mysqli_real_escape_string($this->db, mysqli_real_escape_string($this->db, $payment_method)));
            $result = mysqli_query($this->db, $query);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $trial_reg_id = mysqli_insert_id($this->db);
                if ($payment_amount > 0) {
                    $pstatus = 'S';
                    $payment_query = sprintf("INSERT INTO `trial_payment`(`trial_reg_id`, `student_id`, `processing_fee`, `checkout_status`, `payment_amount`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`,`payment_from`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                            mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $current_date), 
                            mysqli_real_escape_string($this->db, $pstatus), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from));
                    $payment_result = mysqli_query($this->db, $payment_query);
                    if (!$payment_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }

                $update_trial_query = sprintf("UPDATE `trial` SET `registrations_count` = `registrations_count`+1,`active_count`=`active_count`+1, `net_sales`=`net_sales`+$insert_paid_amount  WHERE `trial_id`='%s'", mysqli_real_escape_string($this->db, $trial_id));
                $update_trial_result = mysqli_query($this->db, $update_trial_query);
                if (!$update_trial_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                if (($payment_amount > 0) && ($checkout_state != 'requires_action')){
                    $this->updateTrialDimensionsNetSales($company_id, $trial_id, '');
                }

                $selectcurrency = sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'", mysqli_escape_string($this->db, $company_id));
                $resultselectcurrency = mysqli_query($this->db, $selectcurrency);
                if (!$resultselectcurrency) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                    log_info($this->json($error_log));
                } else {
                    $row = mysqli_fetch_assoc($resultselectcurrency);
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                }
                $activity_text = 'Registration date.';
                $activity_type = "registration";

                if (!empty($discount)) {
                    $activity_text .= " Trial fee discount value used $wp_currency_symbol" . $discount;
                }

                $curr_date = gmdate("Y-m-d H:i:s");
                $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), $activity_type, mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }

                $this->updateTrialDimensionsMembers($company_id, $trial_id, 'A', '', $trial_reg_id);
                $trial_details = $this->getTrialProgramDetails($company_id, $trial_id, 1);
                
                if($has_3D_secure=='N'){
                    $text = "Trial program registration was successful. Email confirmation sent.";
                }else{
                    $text = "Trial program registration was processed and waiting for customer action. Kindly review and authenticate the payment.";
                }
                $msg = array("status" => "Success", "msg" => $text, "trial_details" => $trial_details);
                $this->responseWithWepay($this->json($msg), 200);
                if($has_3D_secure=='N'){
                    $this->sendOrderReceiptForTrialPayment($company_id, $trial_reg_id, 0);
                }else{
                    $email_msg = 'Hi '.$buyer_first_name.' '.$buyer_last_name.',<br>';
                    $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">This is an authentication email for Trial program '.$trial_name.' registration. Kindly click the launch button for redirecting to Stripe.</p>';
                    $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">Complete a required action for this payment. Once the redirect is completed for this PaymentIntent, the payment will proceed.</p>';
                    $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg);
                }
//                $msg = array("status" => "Success", "msg" => "Trial program registration was successful. Email confirmation sent.", "trial_details" => $trial_details);
//                $this->responseWithWepay($this->json($msg), 200);
//                $this->sendOrderReceiptForTrialPayment($company_id, $trial_reg_id, 0);
            }
            date_default_timezone_set($curr_time_zone);
        }else {
            $error = array("status" => "Failed", "msg" => "Payment cannot be done. Please enable charges in stripe account.");
            $this->response($this->json($error), 200);
        }
    }

    public function payment_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$amount_in_dollars,$stripe_currency,$studio_name,$buyer_email,$stripe_account_id,$dest_amount_in_dollars,$p_type,$cc_type){
        if ($cc_type == 'N') {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }

        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try {
            $payment_intent_create = \Stripe\PaymentIntent::create([
                        'payment_method' => $payment_method_id,
                        'customer' => $stripe_customer_id,
                        'amount' => $amount_in_dollars,
                        'currency' => $stripe_currency,
                        'confirmation_method' => 'automatic',
                        'capture_method' => 'automatic',
                        'confirm' => true,
                        'return_url' => $this->server_url."/stripeUpdation.php?type=authenticate&c_id=$company_id&p_type=$p_type&s_id=$student_id",
                        'metadata' =>['c_id' => $company_id,'p_type' => $p_type,'s_id' => $student_id ,'s_name' => $studio_name],
                        'setup_future_usage' => 'off_session',
                        'description' => 'Payment Intent for ' . $buyer_email,
                        'payment_method_types' => ['card'],
                        'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                        'receipt_email' => $buyer_email,
                        'transfer_data' => ['destination' => $stripe_account_id, 'amount' => $dest_amount_in_dollars],
                        'on_behalf_of' => $stripe_account_id,
            ]);
            $body3 = json_encode($payment_intent_create);
            stripe_log_info("Stripe Payment Creation : $body3");
            $return_array['temp_payment_status'] = $payment_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['cc_month'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_month;
                $return_array['cc_year'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_year;
                $brand = $payment_intent_create->charges->data[0]->payment_method_details->card->brand;
                $last4 = $payment_intent_create->charges->data[0]->payment_method_details->card->last4;
                $temp = ' xxxxxx';
                $return_array['stripe_card_name'] = $brand . $temp . $last4;
            }else if ($return_array['temp_payment_status'] == 'requires_action'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['payment_intent_client_secret'] = $payment_intent_create->client_secret;
                $return_array['next_action'] = $payment_intent_create->next_action;
//                $return_array['next_action']['type'] = $payment_intent_create->next_action->type;
//                $return_array['next_action']['redirect_to_url']['url'] = $payment_intent_create->next_action->redirect_to_url->url;
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }
    
    public function getCompanyStripeAccount($company_id, $call_back){
        $query = sprintf("SELECT * FROM `stripe_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result) >0){
                $row = mysqli_fetch_assoc($result);
                if($call_back == 1){
                    return $row;
                }else{
                    $msg = array("status" => "Success", "msg" => "Stripe Account details", "stripe_details" => "$row");
                    $this->response($this->json($msg), 200);
                }
            }else{
                if($call_back == 1){
                    return '';
                }else{
                    $msg = array("status" => "Failed", "msg" => "Stripe Account details", "stripe_details" => "");
                    $this->response($this->json($msg), 200);
                }
            }
        }
    }
    
    public function createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name){
        $stripe_customer_id = '';
        $query = sprintf("SELECT `stripe_customer_id` FROM `student` WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                $stripe_customer_id = $row['stripe_customer_id'];
            }
        }
        if(is_null($stripe_customer_id) || empty(trim($stripe_customer_id))){
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");

            try {
                $stripe_customer_create = \Stripe\Customer::create([
                            "email" => $buyer_email,
                            "description" => "Customer created with $buyer_email for $studio_name($company_id)",
                ]);
                $body = json_encode($stripe_customer_create);
                stripe_log_info("Customer_new : $body");
                $body_arr = json_decode($body, true);
                $stripe_customer_id = $body_arr['id'];
                
                //update stripe customer id to db - arun 
                $query2 = sprintf("UPDATE `student` SET `stripe_customer_id` = '%s' WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $student_id));
                $result2 = mysqli_query($this->db, $query2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }

            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
        
        return $stripe_customer_id;
    }

    public function sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg){
        
        // EMAIL CONTENT
        $button_text = 'Launch';
        $subject = 'Stripe authenticate';
        $message = $email_msg.'<br>'.'<center><a href="'.$button_url.'"><button type="button" style="width: 190px;border-radius: 5px;background: #009a61;border:1px solid 009a61;color: #ffffff;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 45px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;text-transform: none;overflow: visible;box-sizing: border-box;cursor: pointer;">'.$button_text.'</button></a></center>';
        $sendEmail_status = $this->sendEmail($buyer_email, $subject, $message, $studio_name, '', '', '', '', $company_id, '','');
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
    }
    
    public function updateTrialstripePaymentMethodDetails($company_id, $trial_reg_id, $payment_method_id, $buyer_first_name, $buyer_last_name, $buyer_email) {
        $bactivity_type = $bactivity_text = "";
        $query1 = sprintf("SELECT tr.`payment_type` ,tr.`buyer_first_name`,tr.`buyer_last_name`,tr.`student_id`, sa.`charges_enabled`,s.`stripe_customer_id`
                  FROM `trial_registration` tr LEFT JOIN `student` s ON tr.`student_id`=s.`student_id` LEFT JOIN `stripe_account` sa on sa.`company_id` = s.`company_id`
                  WHERE tr.`trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_id));
        $result1 = mysqli_query($this->db, $query1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                $row = mysqli_fetch_assoc($result1);
                $stripe_customer_id = $row['stripe_customer_id'];
                $payment_type = $row['payment_type'];
                $student_id = $row['student_id'];
                $old_buyer_first_name = $row['buyer_first_name'];
                $old_buyer_last_name = $row['buyer_last_name'];
                $stripe_charges_enabled = $row['charges_enabled'];
                if ($old_buyer_first_name !== $buyer_first_name && $old_buyer_last_name !== $buyer_last_name) {
                    $bactivity_type = "buyer name edit";
                    $bactivity_text = "buyer name is changed from $old_buyer_first_name $old_buyer_last_name to $buyer_first_name $buyer_last_name.";
                } elseif ($old_buyer_first_name !== $buyer_first_name) {
                    $bactivity_type = "buyer name edit";
                    $bactivity_text = "buyer first name is changed from $old_buyer_first_name to $buyer_first_name";
                } elseif ($old_buyer_last_name !== $buyer_last_name) {
                    $bactivity_type = "buyer name edit";
                    $bactivity_text = "buyer last name is changed from $old_buyer_last_name to $buyer_last_name";
                }

                if (($stripe_charges_enabled == 'Y') && !empty($stripe_customer_id)) {

                    $cc_name = $cc_month = $cc_year = '';
                    $this->getStripeKeys();
                    \Stripe\Stripe::setApiKey($this->stripe_sk);
                    \Stripe\Stripe::setApiVersion("2019-05-16");
                    try {
                        $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                        $payment_method->attach(['customer' => $stripe_customer_id]);
                    } catch (\Stripe\Error\Card $e) {
                        // Since it's a decline, \Stripe\Error\Card will be caught       
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\RateLimit $e) {
                        // Too many requests made to the API too quickly      
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        // Invalid parameters were supplied to Stripe's API      
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Authentication $e) {
                        // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\ApiConnection $e) {
                        // Network communication with Stripe failed      
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Base $e) {
                        // Display a very generic error to the user, and maybe send yourself an email      
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (Exception $e) {
                        // Something else happened, completely unrelated to Stripe      
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    }
                    if (isset($err) && !empty($err)) {
                        $error = array('status' => "Failed", "msg" => $err['message']);
                        $this->response($this->json($error), 200);
                    }

                    //Retrieving payment method with payment method id for card details
                    try {
                        $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                        $brand = $retrive_payment_method->card->brand;
                        $cc_month = $retrive_payment_method->card->exp_month;
                        $cc_year = $retrive_payment_method->card->exp_year;
                        $last4 = $retrive_payment_method->card->last4;
                        $temp = ' xxxxxx';
                        $stripe_card_name = $brand . $temp . $last4;
                    } catch (\Stripe\Error\Card $e) {
                        // Since it's a decline, \Stripe\Error\Card will be caught     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\RateLimit $e) {
                        // Too many requests made to the API too quickly     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        // Invalid parameters were supplied to Stripe's API     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Authentication $e) {
                        // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\ApiConnection $e) {
                        // Network communication with Stripe failed     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Base $e) {
                        // Display a very generic error to the user, and maybe send yourself an email     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (Exception $e) {
                        // Something else happened, completely unrelated to Stripe     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    }
                    if (isset($err) && !empty($err)) {
                        $error = array('status' => "Failed", "msg" => $err['message']);
                        $this->response($this->json($error), 200);
                    }

                    $update_query = sprintf("UPDATE `trial_registration` SET `buyer_first_name`='%s',`buyer_last_name`='%s',`buyer_email`='%s',`payment_method_id`='%s',
                            `stripe_card_name`='%s',`credit_card_expiration_month`='%s',`credit_card_expiration_year`='%s' WHERE `trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $trial_reg_id));
                    $result_update = mysqli_query($this->db, $update_query);
                    if (!$result_update) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $curr_date = gmdate("Y-m-d H:i:s");
                        if (!empty($bactivity_type)) {
                            $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), $student_id, $bactivity_type, $bactivity_text, $curr_date);
                            $result_history = mysqli_query($this->db, $insert_history);
                            if (!$result_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                        $update_query2 = sprintf("UPDATE `trial_payment` SET `payment_method_id`='%s', `stripe_card_name`='%s' WHERE `trial_reg_id`='%s' AND `payment_status` IN ('N','F')", mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $trial_reg_id));
                        $result_update2 = mysqli_query($this->db, $update_query2);
                        if (!$result_update2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query2");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }

                        $activity_text = "Payment method changed ($stripe_card_name)";
                        $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), $student_id, 'Payment method', $activity_text, $curr_date);
                        $result_history = mysqli_query($this->db, $insert_history);
                        if (!$result_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                        $reg_details = $this->getTrialRegDetails($company_id, $trial_reg_id, 1);
                        $msg = array('status' => "Success", "msg" => "New Payment method successfully updated.", "reg_details" => $reg_details);
                        $this->response($this->json($msg), 200);
                    }
                } else {
                    $error = array('status' => "Failed", "msg" => "Please Enable Your Stripe Payments");
                    $this->response($this->json($error), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Particpant payment details mismatch.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function reRunstripeTrialPayment($company_id, $reg_id, $payment_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $has_3D_secure = 'N';
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";

        $buyer_name = $buyer_email = $buyer_phone = $buyer_postal_code = $button_url = '';

        $query = sprintf("SELECT tr.`company_id`, tr.`student_id`, t.`trial_title`, tr.`trial_reg_id`, `trial_payment_id`, tr.`trial_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, 
                tr.`registration_amount`, tp.`processing_fee`, tr.`payment_type`,tr.`payment_amount` total_due, `paid_amount`, tp.`payment_method_id`, tp.`stripe_card_name`,  
                tp.`payment_amount`, tp.`payment_date`, tp.`payment_status`, tr.`processing_fee_type`, tp.`checkout_status`,c.`wp_currency_symbol`,tr.`student_id` ,tp.`credit_method`,c.`company_name`,s.`currency`,s.`account_id`,tr.`stripe_customer_id`,tp.`payment_intent_id`
                FROM `trial_registration` tr 
                LEFT JOIN `wp_account` wp ON tr.`company_id` = wp.`company_id` 
                LEFT JOIN `trial_payment` tp ON tr.`trial_reg_id`=tp.`trial_reg_id` AND (tp.`payment_status`='F' || (tp.`payment_status`='N' && tp.`payment_date`< ($currdate_add)))
                LEFT JOIN `trial` t ON t.`trial_id` = tr.`trial_id`
                LEFT JOIN `company` c ON tr.`company_id` = c.`company_id` 
                LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` 
                WHERE   tr.`trial_reg_id` = '%s'  AND `trial_payment_id` = '%s'  AND tp.`payment_amount`>0", mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $payment_id));
//         WHERE ep.`schedule_date`=now() AND `payment_type`='RE' AND ep.`payment_amount`>0 ORDER BY er.`company_id`
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $company_list = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $company_id = $row['company_id'];
                    $stripe_currency = $row['currency'];
                    $trial_payment_id = $row['trial_payment_id'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $buyer_phone = $row['buyer_phone'];
                    $buyer_postal_code = $row['buyer_postal_code'];
                    $payment_method_id = $row['payment_method_id'];
                    $stripe_card_name = $row['stripe_card_name'];
                    $student_id = $row['student_id'];
                    $date_in_db = $row['payment_date'];
                    $stripe_account_id = $row['account_id'];
                    $old_payment_intent_id = $row['payment_intent_id'];
                    $stripe_customer_id = $row['stripe_customer_id'];
                    $studio_name = $row['company_name'];
                    if ($row['credit_method'] == 'CA' || $row['credit_method'] == 'CH') {
                        $error = array('status' => "Failed", "msg" => "Trial payment via cash or check can not be rerun.");
                        $this->response($this->json($error), 200);
                    }

                    if (!empty($payment_method_id)) {
                        $failure = $success = 0;
                        $trial_id = $row['trial_id'];
                        $trial_name = $desc = $row['trial_title'];
                        $payment_amount = $row['payment_amount'];
                        $processing_fee = $row['processing_fee'];
                        $processing_fee_type = $row['processing_fee_type'];
                        $wp_currency_symbol = $row['wp_currency_symbol'];
                        $payment_date = $row['payment_date'];

                        $this->getStripeKeys();
                        \Stripe\Stripe::setApiKey($this->stripe_sk);
                        \Stripe\Stripe::setApiVersion("2019-05-16");
                        if (!empty($stripe_customer_id) && !empty($payment_method_id)) {
                            $paid_amount = $payment_amount;
                            if ($processing_fee_type == 2) {
                                $w_paid_amount = $payment_amount + $processing_fee;
                            } elseif ($processing_fee_type == 1) {
                                $w_paid_amount = $payment_amount;
                            }
                            $w_paid_amount_round = round($w_paid_amount, 2);
                            $dest_amount = $w_paid_amount - $processing_fee;
                            $dest_amount_round = round($dest_amount, 2);
                            $dest_amount_in_dollars = $dest_amount_round * 100;
                            $amount_in_dollars = $w_paid_amount_round * 100;
                            $checkout_type = 'trial';
                            $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $checkout_type, '');
                            if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                                $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                $checkout_state = 'released';
                            } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {
                                $payment_intent_id = $payment_intent_result['payment_intent_id'];
                                $checkout_state = 'requires_action';
                                $has_3D_secure = 'Y';
                                $next_action_type = $payment_intent_result['next_action']['type'];
                                if($next_action_type != "redirect_to_url"){
                                    # Payment not supported
                                    $out['status'] = 'Failed';
                                    $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                                    $this->response($this->json($out), 200);
                                }
                                $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                            } else {
                                # Invalid status
                                $out['status'] = 'Failed';
                                $out['msg'] = 'Invalid PaymentIntent status.';
                                $out['error'] = 'Invalid PaymentIntent status';
                                $this->response($this->json($out), 200);
                            }
                            $payment_status = 'S';
                        }

                        if (!empty($payment_method_id)) {
                            try {
                                $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                                $brand = $retrive_payment_method->card->brand;
                                $cc_month = $retrive_payment_method->card->exp_month;
                                $cc_year = $retrive_payment_method->card->exp_year;
                                $last4 = $retrive_payment_method->card->last4;
                                $temp = ' xxxxxx';
                                $stripe_card_name = $brand . $temp . $last4;
                            } catch (\Stripe\Error\Card $e) {
                                // Since it's a decline, \Stripe\Error\Card will be caught     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\RateLimit $e) {
                                // Too many requests made to the API too quickly     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\InvalidRequest $e) {
                                // Invalid parameters were supplied to Stripe's API     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Authentication $e) {
                                // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\ApiConnection $e) {
                                // Network communication with Stripe failed     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (\Stripe\Error\Base $e) {
                                // Display a very generic error to the user, and maybe send yourself an email     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            } catch (Exception $e) {
                                // Something else happened, completely unrelated to Stripe     
                                $body = $e->getJsonBody();
                                $err = $body['error'];
                                $err['status_code'] = $e->getHttpStatus();
                            }
                            if (isset($err) && !empty($err)) {
                                $error = array('status' => "Failed", "msg" => $err['message']);
                                $this->response($this->json($error), 200);
                            }
                        }
                        $payment_query = sprintf("UPDATE `trial_payment` SET `payment_intent_id`='%s', `checkout_status`='%s', `payment_status`='%s', `payment_method_id`='$payment_method_id', `stripe_card_name`='$stripe_card_name' WHERE `trial_payment_id`='%s'", mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $trial_payment_id));
                        $payment_result = mysqli_query($this->db, $payment_query);
                        if (!$payment_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            $curr_date = gmdate("Y-m-d H:i:s");
                            $scheduled_date = date("M d, Y", strtotime($date_in_db));
                            $text = "Rerun payment $wp_currency_symbol" . "$w_paid_amount" . " past due " . "$scheduled_date";

                            $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), $student_id, 'rerun', $text, $curr_date);
                            $result_history = mysqli_query($this->db, $insert_history);
                            if (!$result_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                            $payment_history = $this->getpaymentHistoryDetails($company_id, $reg_id, 1);
                            if($has_3D_secure=='Y'){
                                $email_msg = 'Hi '.$buyer_name.',<br>';
                                $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">This is an authentication email for Trial program '.$trial_name.' registration rerun payment. Kindly click the launch button for redirecting to Stripe.</p>';
                                $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">Complete a required action for this payment. Once the redirect is completed for this PaymentIntent, the payment will proceed.</p>';
                                $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg);
                            }
                            $msg = array('status' => "Success", "msg" => "Payment Successful.", "payment_history" => $payment_history);
                            $this->response($this->json($msg), 200);
                        }
                    } else {
                        $error = array('status' => "Failed", "msg" => "Payment Method error. Cannot be accept payments anymore.");
                        $this->response($this->json($error), 200);
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Event payment details mismatched.");
                $this->response($this->json($error), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

}

?>

