<?php


 header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');

require_once '../../../Globals/cont_log.php';
require_once 'Salesforce.php';
require_once 'WePay.php';
require_once('../../PHPMailer_5.2.1/class.phpmailer.php');
require_once('../../PHPMailer_5.2.1/class.smtp.php');
require_once 'tfpdf.php';
//require_once '../../../aws/aws-autoloader.php';
//
//use Aws\Sns\SnsClient;


class ChatModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    public $local_upload_folder_name = '';
    private $sf;
    private $list_id = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_redirect_uri = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $server_url, $env;

    public function __construct() {
        require_once '../../../Globals/config.php';
        $this->inputs();
        $this->sf = new Salesforce();
        $this->wp = new wepay();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->local_upload_folder_name = $GLOBALS['local_uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->list_id = $this->sf->list_id;
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = "../../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'dev.mystudio.academy') !== false) {
            $this->server_url = "http://dev.mystudio.academy";
            $this->env = 'D';
        } elseif (strpos($host, 'dev2.mystudio.academy') !== false) {
            $this->server_url = "http://dev2.mystudio.academy";
            $this->env = 'D';
        } elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'];
            $this->env = 'P';
        } else {
            $this->server_url = "http://dev.mystudio.academy";
            $this->env = 'D';
        }
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithSalesforce($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
        header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    protected function clean($string) {
        $str = preg_replace('/\s+/', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    }
    
    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    public function getStudentListForChatINDB($company_id) {
        $output = [];
        $getstudent = sprintf("SELECT CONCAT(`student_name`,' ',`student_lastname`) student_name,`student_id` FROM `student` WHERE `company_id`='%s' AND `deleted_flag`!='Y'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $getstudent);
        log_info($getstudent);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getstudent");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while($row = mysqli_fetch_assoc($result)){
                    $output[] = $row;
                }
            }
            $out = array('status' => "Success", 'msg' => $output);
            $this->response($this->json($out), 200);
        }
    }
    
    public function getChatListINDB($company_id,$limit,$search_term,$from_tab) {
        if($from_tab === "U"){
            $inboxunread_filter = "AND read_status='U'";
        }else{
            $inboxunread_filter = "";
        }
        
        if(!empty($search_term)){
            $search_string = "and (student_name LIKE '%$search_term%' OR student_lastname LIKE '%$search_term%')";
        }else{
            $search_string = "";
        }
        $output = [];
            
        $getchat = sprintf("select b.*, concat(c.student_name,' ',c.student_lastname) student_name,
                            IF((select count(*) from message where message_id > b.message_id and message_from = 'P' and message_to = 'I' and student_id=b.student_id)>0,'R','N') replied_flag from
                            (select * from (
                            select message_id, student_id,  message_text, read_status 
                            , TIMEDIFF( NOW(), IFNULL(`message_delivery_date`, `push_delivered_date`)) time_diffrence 
                            , dense_rank() over ( partition by student_id order by read_status  desc , message_id desc )  as msg_rank 
                            ,max(message_id) over() as max_msg_id 
                            from message where company_id = '%s' 
                            AND message_from = 'M' AND IFNULL(`message_delivery_date`, `push_delivered_date`) <= CURRENT_TIMESTAMP $inboxunread_filter
                            ) a where msg_rank =1   )b  join student c on b.student_id = c.student_id %s order by message_id DESC limit %s, 20", mysqli_real_escape_string($this->db, $company_id), $search_string, mysqli_real_escape_string($this->db, $limit));
        
//        log_info($getchat);
        $result = mysqli_query($this->db, $getchat);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getchat");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while($row = mysqli_fetch_assoc($result)){
                    $last_msg_format = "";
                    $last_msg_format = $row['time_diffrence'];
                    $time = explode(":",$last_msg_format);
                    if($time[0] < 1){
                        $row['last_msg_time'] = round($time[1]).'m';
                    }else if((int)$time[0] >= 1 && (int)$time[0] < 24){
                        $row['last_msg_time'] =  round($time[0]).'h';
                    }else if((int)$time[0] >= 24 && (int)$time[0] < 168){
                        $row['last_msg_time'] =  round(($time[0])/24).'d';
                    }else if((int)$time[0] >= 168 && (int)$time[0] < 720){
                        $row['last_msg_time'] =  round((($time[0])/24)/7).'w';
                    }else{
                        $row['last_msg_time'] =  round((($time[0])/24)/30).'mth';
                    }
                    $output[] = $row;
                }
            }
            $out = array('status' => "Success", 'msg' => $output);
            $this->response($this->json($out), 200);
        }
    }
    
    public function getChatConversationINDB($company_id,$student_id,$last_view_message_id,$last_view_message_datetime) {
        $last_view_message_id_string = '';
        if(!empty((int)$last_view_message_id)){
            $last_view_message_id_string = "AND m.message_id < ".mysqli_real_escape_string($this->db, $last_view_message_id);
        }
        $last_view_message_datetime_string = '';
        if(!empty(trim($last_view_message_datetime))){
            $last_view_message_datetime_string = "AND message_delivery_date < '".mysqli_real_escape_string($this->db, $last_view_message_datetime)."'";
        }
        //update read_status
        $read_status = sprintf("UPDATE `message` SET `read_status` = 'R',`read_time`=CURRENT_TIMESTAMP WHERE `student_id` ='%s' AND company_id='%s' AND message_from='M' AND read_status ='U' AND IFNULL(message_delivery_date, push_delivered_date) <= CURRENT_TIMESTAMP", mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
        $result0 = mysqli_query($this->db, $read_status);
        if (!$result0) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$read_status");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
        
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $message_delivery_date = "CONVERT_TZ(`message_delivery_date`,$tzadd_add,'$new_timezone')";
        $output = $groupdatearray = [];
        $time_foramt = "%h:%i%p";
        
        $getchat = sprintf("SELECT  message_delivery_date,
                LOWER(TIME_FORMAT($message_delivery_date, '%s')) msg_time,
                $message_delivery_date comapny_timezone_date,
                IF(m.message_from = 'M',student_name,c.company_name) name
                ,company_id,message_id,message_text,message_from 
                FROM message_views m LEFT JOIN `company` c USING (company_id)
                WHERE `company_id`='%s' and student_id = '%s' $last_view_message_id_string
                AND message_delivery_date <= CURRENT_TIMESTAMP $last_view_message_datetime_string
                ORDER BY message_delivery_date DESC, message_id DESC limit 0, 20",$time_foramt, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $getchat);
//        log_info($getchat);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getchat");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while($row = mysqli_fetch_assoc($result)){
                    $output[] = $row;
                }
            }
            for($i = 0;$i< count($output);$i++){
                $sample_array = [];
                $first_date = explode(" ",$output[$i]['comapny_timezone_date']);
                $sample_array['msg_by_date'][] = $output[$i];
                array_splice($output,$i,1);
                for($j = $i; $j< count($output); $j++){
                    $compare_date = explode(" ",$output[$j]['comapny_timezone_date']);
                    if($compare_date[0] === $first_date[0]){
                       $sample_array['msg_by_date'][] =  $output[$j];
                       array_splice($output,$j,1);
                       $j--;
                    }
                }
                $date=date_create($sample_array['msg_by_date'][0]['comapny_timezone_date']);
                $sample_array['day'] =  date_format($date, "l, F jS");
                $sample_array['day_view'] = $sample_array['msg_by_date'][0]['comapny_timezone_date'];
//                $sample_array['msg_by_date'] = array_reverse($sample_array['msg_by_date']);
                $groupdatearray[] = $sample_array;
                $i--;
            }
            $out = array('status' => "Success", 'msg' => $groupdatearray);
            $this->response($this->json($out), 200);
        }
    }
    
    public function sendchatmessageINDB($company_id,$student_id,$message_text,$message_from,$last_message_id,$last_messagedatetime) {
        
        $query = sprintf("INSERT INTO `message` (`message_text`, `message_from`, `message_to`, `company_id`, `student_id`, `push_delivered_date`, `push_from`) VALUES ('%s', '%s', 'I', '%s', '%s', CURRENT_TIMESTAMP, 'C')", mysqli_real_escape_string($this->db,$message_text), $message_from, $company_id, $student_id);
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $message_id = mysqli_insert_id($this->db);//getsingle message
            
            $trimmed_message = substr($message_text, 0, 230);
            $this->preparepush($company_id, $student_id, $trimmed_message);
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone=$user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
            $host = $_SERVER['HTTP_HOST'];
            if (strpos($host, 'mystudio.academy') !== false){
                $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
            } else {
                $tzadd_add = "'" . $curr_time_zone . "'";
            }
            $message_delivery_date = "CONVERT_TZ(`message_delivery_date`,$tzadd_add,'$new_timezone')";
            $output = $groupdatearray = [];
            $time_foramt = "%h:%i%p";
            $getmessagequery = sprintf("SELECT  message_delivery_date,
                LOWER(TIME_FORMAT($message_delivery_date, '%s')) msg_time,
                $message_delivery_date comapny_timezone_date,
                IF(m.message_from = 'M',student_name,c.company_name) name
                ,company_id,message_id,message_text,message_from 
                FROM message_views m LEFT JOIN `company` c USING (company_id)
                WHERE m.message_id > '%s' and `company_id`='%s' and student_id = '%s'  and message_delivery_date > '%s'
                AND message_delivery_date <= CURRENT_TIMESTAMP ORDER BY message_delivery_date DESC, message_id DESC"
                ,$time_foramt, mysqli_real_escape_string($this->db, $last_message_id),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $student_id),mysqli_real_escape_string($this->db, $last_messagedatetime));
            $result2 = mysqli_query($this->db, $getmessagequery);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getmessagequery");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($result2);
            if ($num_rows > 0) {
                while($row = mysqli_fetch_assoc($result2)){
                    $output[] = $row;
                }
            }
            for($i = 0;$i< count($output);$i++){
                $sample_array = [];
                $first_date = explode(" ",$output[$i]['comapny_timezone_date']);
                $sample_array['msg_by_date'][] = $output[$i];
                array_splice($output,$i,1);
                for($j = $i; $j< count($output); $j++){
                    $compare_date = explode(" ",$output[$j]['comapny_timezone_date']);
                    if($compare_date[0] === $first_date[0]){
                       $sample_array['msg_by_date'][] =  $output[$j];
                       array_splice($output,$j,1);
                       $j--;
                    }
                }
                $date=date_create($sample_array['msg_by_date'][0]['comapny_timezone_date']);
                $sample_array['day'] =  date_format($date, "l, F jS");
                $sample_array['day_view'] = $sample_array['msg_by_date'][0]['comapny_timezone_date'];
//                $sample_array['msg_by_date'] = array_reverse($sample_array['msg_by_date']);
                $groupdatearray[] = $sample_array;
                $i--;
            }
            $out = array('status' => "Success", 'msg' => $groupdatearray);
            $this->response($this->json($out), 200);
            }
        }
    }
    
    public function communicationIntervalCallINDB($company_id,$current_page,$selected_student_id,$last_msg_id,$last_messagedatetime,$unread_count_from_client,$student_last_msg_id,$search_term,$from_tab) {
        
        $getunreadcount = sprintf("SELECT count(distinct student_id ) count_S FROM `message` m 
                    WHERE m.`company_id`='%s' and m.message_from='M' and m.read_status='U'", mysqli_real_escape_string($this->db, $company_id));
//        log_info($getunreadcount);
        $countresult = mysqli_query($this->db, $getunreadcount);
        if (!$countresult) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getunreadcount");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $row = mysqli_fetch_assoc($countresult);
            $unread_count = $row['count_S'];
//            $unread_count = mysqli_num_rows($countresult);
        }
        $from_tab_string = '';
        if($from_tab === "U"){
            $from_tab_string = "AND read_status = 'U' ";
        }
        if($current_page === "conversation"){
            if(!empty($search_term)){
                $search_string = "and (student_name LIKE '%$search_term%' OR student_lastname LIKE '%$search_term%')";
            }else{
                $search_string = "";
            }
            $middleoutput = $groupdatearray  = [];
            $getchat = sprintf("select b.*, concat(c.student_name,' ',c.student_lastname) student_name
                            ,IF((select count(*) from message where message_id > b.message_id and message_from = 'P' and message_to = 'I' and student_id=b.student_id)>0,'R','N') replied_flag from
                            (select * from (
                            select message_id, student_id,  message_text, read_status 
                            , TIMEDIFF( NOW(), IFNULL(`message_delivery_date`, `push_delivered_date`)) time_diffrence 
                            , dense_rank() over ( partition by student_id order by read_status  desc , message_id desc )  as msg_rank 
                            ,max(message_id) over() as max_msg_id 
                            from message where company_id = '%s' 
                            AND message_from = 'M' AND message_id > '%s' $from_tab_string AND IFNULL(`message_delivery_date`, `push_delivered_date`) <= CURRENT_TIMESTAMP
                            ) a where msg_rank =1   )b  join student c on b.student_id = c.student_id %s order by message_id DESC ", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_last_msg_id), $search_string);
//            log_info($getchat);
            $result = mysqli_query($this->db, $getchat);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getchat");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows1 = mysqli_num_rows($result);
                if ($num_rows1 > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                        $last_msg_format = "";
                        $last_msg_format = $row['time_diffrence'];
                        $time = explode(":",$last_msg_format);
                        if($time[0] < 1){
                            $row['last_msg_time'] = round($time[1]).'m';
                        }else if((int)$time[0] >= 1 && (int)$time[0] < 24){
                            $row['last_msg_time'] =  round($time[0]).'h';
                        }else if((int)$time[0] >= 24 && (int)$time[0] < 168){
                            $row['last_msg_time'] =  round(($time[0])/24).'d';
                        }else if((int)$time[0] >= 168 && (int)$time[0] < 720){
                            $row['last_msg_time'] =  round((($time[0])/24)/7).'w';
                        }else{
                            $row['last_msg_time'] =  round((($time[0])/24)/30).'mth';
                        }
                        $middleoutput[] = $row;
                    }
                }

            }

            if(!empty($selected_student_id)){

                $user_timezone = $this->getUserTimezone($company_id);
                $curr_time_zone = date_default_timezone_get();
                $new_timezone = $user_timezone['timezone'];
                date_default_timezone_set($new_timezone);
                $host = $_SERVER['HTTP_HOST'];
                if (strpos($host, 'mystudio.academy') !== false) {
                    $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
                } else {
                    $tzadd_add = "'" . $curr_time_zone . "'";
                }
                $message_delivery_date = "CONVERT_TZ(`message_delivery_date`,$tzadd_add,'$new_timezone')";
                $output = $groupdatearray = [];
                $time_foramt = "%h:%i%p";

                $getchat2 = sprintf("SELECT  message_delivery_date,
                LOWER(TIME_FORMAT($message_delivery_date, '%s')) msg_time,
                $message_delivery_date comapny_timezone_date,
                IF(m.message_from = 'M',student_name,c.company_name) name
                ,company_id,message_id,message_text,message_from 
                FROM message_views m LEFT JOIN `company` c USING (company_id)
                WHERE m.message_id > '%s' and `company_id`='%s' and student_id = '%s' and message_delivery_date > '%s'
                AND message_delivery_date <= CURRENT_TIMESTAMP ORDER BY message_delivery_date DESC, message_id DESC"
                    ,$time_foramt, mysqli_real_escape_string($this->db, $last_msg_id),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $selected_student_id),mysqli_real_escape_string($this->db, $last_messagedatetime));
                $result2 = mysqli_query($this->db, $getchat2);
    //            log_info($getchat2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getchat2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_rows = mysqli_num_rows($result2);
                    if ($num_rows > 0) {
                        while ($row = mysqli_fetch_assoc($result2)) {
                            $output[] = $row;
                        }
                    }
                    for ($i = 0; $i < count($output); $i++) {
                        $sample_array = [];
                        $first_date = explode(" ", $output[$i]['comapny_timezone_date']);
                        $sample_array['msg_by_date'][] = $output[$i];
                        array_splice($output, $i, 1);
                        for ($j = $i; $j < count($output); $j++) {
                            $compare_date = explode(" ", $output[$j]['comapny_timezone_date']);
                            if ($compare_date[0] === $first_date[0]) {
                                $sample_array['msg_by_date'][] = $output[$j];
                                array_splice($output, $j, 1);
                                $j--;
                            }
                        }
                        $date = date_create($sample_array['msg_by_date'][0]['comapny_timezone_date']);
                        $sample_array['day'] = date_format($date, "l, F jS");
                        $sample_array['day_view'] = $sample_array['msg_by_date'][0]['comapny_timezone_date'];
    //                $sample_array['msg_by_date'] = array_reverse($sample_array['msg_by_date']);
                        $groupdatearray[] = $sample_array;
                        $i--;
                    }
                }
                //update read_status
                $read_status = sprintf("UPDATE `message` SET `read_status` = 'R',`read_time`=CURRENT_TIMESTAMP WHERE `student_id` ='%s' AND company_id='%s' AND message_from='M' AND read_status = 'U' AND IFNULL(message_delivery_date, push_delivered_date) <= CURRENT_TIMESTAMP", mysqli_real_escape_string($this->db, $selected_student_id), mysqli_real_escape_string($this->db, $company_id));
                    $result = mysqli_query($this->db, $read_status);
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$read_status");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                    for($k = 0 ;$k < count($middleoutput); $k++ ){
                        if((int)$middleoutput[$k]['student_id'] == (int)$selected_student_id){
                            $unread_count = $unread_count - 1;
                        }
                    }
                }
//        }
                $out = array('status' => "Success",'new_conversation_count' => $unread_count, 'conversation_array' => $middleoutput,"chat_array" => $groupdatearray);
                $this->response($this->json($out), 200);
            }else{
                $out = array('status' => "Success", 'new_conversation_count' => $unread_count);
                $this->response($this->json($out), 200);
            }
    }
    
    public function sendmultiplechatmessageINDB($company_id,$student_id,$message_text,$message_from){
//        if(count($student_id)==1){
//            $message_to = 'I';
//            $stud_id = $student_id[0];
//        }else
        if(count($student_id)==0){
            $message_to = 'A';
            $stud_id = 'NULL';
        }else{
            $message_to = 'G';
            $stud_id = 'NULL';
        }
        
        $query = sprintf("INSERT INTO `message` (`message_text`, `message_from`, `message_to`, `company_id`, `student_id`, `push_delivered_date`, `push_from`) VALUES ('%s', '%s', '$message_to', '%s', %s, CURRENT_TIMESTAMP, 'CCS')", mysqli_real_escape_string($this->db,$message_text), $message_from, $company_id, $stud_id);
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $message_id = mysqli_insert_id($this->db);
            if($message_id>0 && $message_to!='I'){
                $this->sendGroupMessage($company_id, $message_id, $student_id);
            }
            $trimmed_message = substr($message_text, 0, 230);
            $this->preparepush($company_id, $student_id, $trimmed_message);
        }
        $out = array('status' => "Success");
        $this->response($this->json($out), 200);
    }
    
    //send group message
    protected function sendGroupMessage($company_id, $message_id, $student_id){
        if(empty($student_id)){
            $sql = sprintf("SELECT student_id FROM `student` WHERE `company_id`='%s' AND `deleted_flag`!='Y'
                        AND student_id NOT IN (SELECT student_id FROM message_mapping WHERE message_id='%s')", 
                    mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$message_id));
            $result = mysqli_query($this->db, $sql);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $student_id=[];
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows > 0) {
                    while ($row = mysqli_fetch_assoc($result)) {
                        $student_id[] = $row['student_id'];
                    }
                }
            }
        }
        
        for($i=0; $i<count($student_id);$i++){
            $insert_query = sprintf("INSERT INTO `message_mapping`(`company_id`, `message_id`, `student_id`) VALUES('%s', '%s', '%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $message_id), mysqli_real_escape_string($this->db, $student_id[$i]));
            $insert_result = mysqli_query($this->db, $insert_query);
            if(!$insert_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
    }

    protected function preparepush($company_id, $student_id, $message) {
        if(is_array($student_id)){
            $stud_list = implode(",", $student_id);
        }else{
            $stud_list = $student_id;
        }
        $android = $ios = [];
        
        $sql2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
            (SELECT s.company_id,s.student_id,lower(`app_id`) app_id, `push_device_id`, `device_type`, IF(`device_type`='A', IF(`type`!='M','A','M'), `device_type`) type, lower(`bundle_id`) bundle_id,`dev_pem`,`prod_pem`, 
            IFNULL(`android_props`,(SELECT `android_props` FROM `mobile_push_certificate` where `type`='A')) as android_props  FROM `student` s
            LEFT JOIN `mobile_push_certificate` mpc ON lower(s.app_id)=lower(mpc.bundle_id)
            WHERE `company_id`='%s' AND s.`student_id` IN (%s) AND `deleted_flag`!='Y' AND `app_id` is NOT NULL AND trim(`app_id`)!='' 
            AND push_device_id IS NOT NULL AND trim(push_device_id)!='' order by app_id) t1            
            LEFT JOIN (SELECT count(*) unread_msg_count,company_id, IFNULL(m.student_id, mp.student_id) student_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) 
            where message_from='P' and IF(message_to='I',m.read_status='U',mp.read_status='U') and company_id='%s' and IFNULL(m.student_id,mp.student_id) IN (%s) group by student_id) t2 USING(company_id, student_id)", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$stud_list), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$stud_list));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows2 = mysqli_num_rows($result2);
            if ($num_of_rows2 > 0) {
                while ($row = mysqli_fetch_assoc($result2)) {
                    $output[] = $row;
                }
                 $r_rand = mt_rand(1,9999);
                $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();  //for creating pem file from db
                if (!file_exists($rfolder_name)) {
                    $oldmask = umask(0);
                    mkdir($rfolder_name, 0777, true);
                    umask($oldmask);
                }else{ // to make directory once.
                    $r = 0;
                    while ($r < 100) {
                        $rfolder_name = "../../../uploads/Certificate/temp-$r_rand-$company_id".time();
                        if (!file_exists($rfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($rfolder_name, 0777, true);
                            umask($oldmask);
                            break;
                        }
                     $r++;
                    }
                }
                $ios_socket_url ='';
                $ios_app_id_array = [];
                $host = $_SERVER['HTTP_HOST'];
//                if (strpos($host, 'mystudio.academy') !== false) {
                if (strpos($host, 'www.mystudio.academy') !== false || strpos($host, 'stage.mystudio.academy') !== false) {
                    //production  - DISABLE ABOVE LINE TO RECIEVE PUSH FROM TEST FLIGHT
                    $ios_socket_url = "ssl://gateway.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];
                        
                        if ($devicetype == 'I') {
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['prod_pem'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }else{
                    //development
                    $ios_socket_url = "ssl://gateway.sandbox.push.apple.com:2195";
                    for ($idx = 0; $idx < count($output); $idx++) {
                        $obj = (Array) $output[$idx];
                        $app_id = trim($obj['app_id']);
                        $devicetoken = $obj["push_device_id"];
                        $devicetype = $obj["device_type"];                        
                        
                        if ($devicetype == 'I') {
                            if(!in_array($app_id, $ios_app_id_array)){
                                $ios_app_id_array[] = $app_id;
                                $prod_pem = $obj['dev_pem'];
                                $ios['app_id'][] = $app_id;
                                $ios['device_token'][] = $devicetoken;
                                $ios['unread_msg_count'][] = $obj['unread_msg_count'];
                                $pem_file = "$rfolder_name/prod$app_id" . ".pem";
                                if(!file_exists($pem_file)){
                                    $tfp = fopen($pem_file, "wb");
                                    fwrite($tfp, $prod_pem);
                                    fclose($tfp);
                                    chmod($pem_file, 0777);
                                }
                                $ios['pem'][] = $pem_file;
                            }else{
                                $ios['device_token'][array_search($app_id, $ios_app_id_array)]=$ios['device_token'][array_search($app_id, $ios_app_id_array)].",".$devicetoken;
                                $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)] = $ios['unread_msg_count'][array_search($app_id, $ios_app_id_array)].",".$obj['unread_msg_count'];
                            }
                        }elseif($devicetype=='A'){
                            $android['unread_msg_count'][] = $obj['unread_msg_count'];
                            $android['key'][$app_id] = $obj['android_props'];
                            $android['device_token'][$app_id][] = $devicetoken;
                        }else{
                            log_info("Invalid device type($devicetype)");
                            continue;
                        }
                    }
                }
                $sent_count = $this->sendpush($message, $ios, $android, $ios_socket_url);
                $this->deleteDirectory("$rfolder_name");// delete directory with content
                $out = array('status' => "Success", 'msg' => $sent_count);
                return $out;
            } else {
                $out = array('status' => "Failure", 'msg' => 0);
                return $out;
            }
        }
    }
    
    protected function sendpush($data, $ios, $android, $socket_url) {
        $send_count = 0;
        //ios
        if (!empty($ios)) {
            $ctx = stream_context_create();
            for ($r = 0; $r < count($ios['device_token']); $r++) {
//                $token_arr = [];
                $app_id = $ios['app_id'][$r];
                $deviceToken = $ios['device_token'][$r];
                $unread_msg_count = $ios['unread_msg_count'][$r];
//                $new_file_content = file_get_contents($ios['pem'][$r]);
//                if ((!empty($old_fle_content) && $old_fle_content !== $new_file_content) || $r == 0) {
//                    if ($r !== 0) {
//                        fclose($fp);
//                    }
                stream_context_set_option($ctx, 'ssl', 'local_cert', $ios['pem'][$r]);
                stream_context_set_option($ctx, 'ssl', 'passphrase', 'mystudio');
                $fp = stream_socket_client($socket_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                if (!$fp) {
                    $error_log = array('status' => "Failed", "msg" => "Message not delivered(I)[Error - $errstr($err)].", "App id" => $app_id, "Token" => "$deviceToken");
                    log_info($this->json($error_log));
                    continue;
                }
//                }
                // Build the binary notification
                $token_arr = explode(",", $deviceToken);
                $unread_msg_arr = explode(",", $unread_msg_count);
                if (count($token_arr) > 0) {
                    $chunk_token_arr = array_chunk($token_arr, 8);
                    $chunk_bagde_count = array_chunk($unread_msg_arr, 8);
                    $flag = 1;
                    for ($j = 0; $j < count($chunk_token_arr); $j++) {
                        if ($j !== 0) {
                            if ($flag == 1) {
                                fclose($fp);
                            }
                            $deviceToken = implode(",", $chunk_token_arr[$j]);
                            stream_context_set_option($ctx, 'ssl', 'local_cert', $ios['pem'][$r]);
                            stream_context_set_option($ctx, 'ssl', 'passphrase', 'mystudio');
                            $fp = stream_socket_client($socket_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                            if (!$fp) {
                                $error_log = array('status' => "Failed", "msg" => "Message not delivered(I)[Error - $errstr($err)].", "App id" => $app_id, "Token" => "$deviceToken");
                                log_info($this->json($error_log));
                                $flag = 0;
                                continue;
                            } else {
                                $flag = 1;
                            }
                        }
                        for ($c = 0; $c < count($chunk_token_arr[$j]); $c++) {
                            // Create the payload body
                            $body['aps'] = array(
                                'alert' => array(
                                    'title' => 'New Message received',
                                    'body' => $data
                                ),
                                'sound' => 'default',
                                'badge' => (int)$chunk_bagde_count[$j][$c]
                            );
                            // Encode the payload as JSON
                            $payload = json_encode($body);
                            $msg = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $chunk_token_arr[$j][$c])) . pack("n", strlen($payload)) . $payload;
                            $result = fwrite($fp, $msg, strlen($msg));
                            // Send it to the server
                            if (!$result) {
                                $error_log = array('status' => "Failed", "msg" => "Message not delivered(I).", "Token" => $chunk_token_arr[$j][$c], "PL" => $payload);
                                log_info($this->json($error_log));
                            } else {
                                $error_log = array('status' => "Success", "msg" => "Message successfully delivered(I).", "Token" => $chunk_token_arr[$j][$c], "PL" => $payload);
                                log_info($this->json($error_log));
                                $send_count++;
                            }
                        }
                    }
                    fclose($fp);
                }
//                $old_fle_content = file_get_contents($ios['pem'][$r]);
            }
        }

        if (!empty($android)) {
            //Android	
            $url = 'https://fcm.googleapis.com/fcm/send';
//            for ($r = 0; $r < count($android['device_token']); $r++) {
            foreach ($android['device_token'] as $point => $values) {
                $key = $android['key'][$point];
                $deviceToken = array_chunk($values, 900);
                for ($j = 0; $j < count($deviceToken); $j++) {
                    $ch = curl_init();
                    $devicetoken = implode(",", $deviceToken[$j]);
                    $fields = array(
                        'registration_ids' => $deviceToken[$j],
                        'data' => array(
                            "message" => $data,
                            "title" => 'New Message',
                            'badge' => $android['unread_msg_count'][$j]
                        )
                    );
                    $fields = json_encode($fields);
                    $headers = array(
                        'Authorization: key=' . $key,
                        'Content-Type: application/json'
                    );
                    if ($url) {
                        // Set the url, number of POST vars, POST data
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        // Disabling SSL Certificate support temporarly
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        if ($fields) {
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                        }

                        // Execute post
                        $result = curl_exec($ch);
                        $resultArr = json_decode($result, true);
                        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        if ($result === FALSE || $resultArr['success'] == 0) {
                            //die('Curl failed: ' . curl_error($ch));
                            $error_log = array('status' => "Failed", "msg" => "Message not delivered(A).", "Token" => "$devicetoken", "StatusCode" => $returnCode, "Result" => json_encode($resultArr));
                            log_info($this->json($error_log));
                        } else {
                            $error_log = array('status' => "Success", "msg" => "Message successfully delivered(A).", "Token" => "$devicetoken", "StatusCode" => $returnCode, "Result" => json_encode($resultArr));
                            log_info($this->json($error_log));
                            $send_count += $resultArr['success'];
                        }
                    }
                    curl_close($ch);
                }
            }
        }
        return $send_count;
    }

    public function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
    
    //get students name for sent message structure
    public function getSentListINDB($company_id,$search_term) {
        $search_string = "and (s.student_name LIKE '%$search_term%' OR s.student_lastname LIKE '%$search_term%')";
        $output = [];
            
        $getstudentforsent = sprintf("select concat(`student_name`,' ',`student_lastname`) student_name,student_id from student s where company_id ='%s' %s", mysqli_real_escape_string($this->db, $company_id), $search_string);
        
//        log_info($getstudentforsent);
        $result = mysqli_query($this->db, $getstudentforsent);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getstudentforsent");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while($row = mysqli_fetch_assoc($result)){
                    $row['read_status']='R';
                    $output[] = $row;
                }
            }
            $out = array('status' => "Success", 'msg' => $output);
            $this->response($this->json($out), 200);
        }
    }
    
}
?>

