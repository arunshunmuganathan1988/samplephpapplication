<?php

   header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');

require_once '../../../Globals/cont_log.php';
require_once 'Salesforce.php';
require_once 'WePay.php';
require_once('../../PHPMailer_5.2.1/class.phpmailer.php');
require_once('../../PHPMailer_5.2.1/class.smtp.php');

class RetailModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    public $local_upload_folder_name = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_redirect_uri = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $server_url;

    public function __construct() {
        require_once '../../../Globals/config.php';
        $this->inputs();
        $this->wp = new wepay();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->local_upload_folder_name = $GLOBALS['local_uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = "../../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'dev.mystudio.academy') !== false) {
            $this->server_url = "http://dev.mystudio.academy";
        } elseif (strpos($host, 'dev2.mystudio.academy') !== false) {
            $this->server_url = "http://dev2.mystudio.academy";
        } elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'];
        } else {
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
         header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    protected function clean($string) {
        $str = preg_replace('/\s+/', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    }

    public function switchRetailinDb($company_id, $retail_flag) {
        $sql = sprintf("Update company SET `retail_enabled`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $retail_flag), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if ($retail_flag == 'Y') {
                $this->getretail_productsDetails($company_id, 'Y', 0);
            }else{
                $error = array('status' => "Success", "msg" => "Retail Status updated successfully.");
                $this->response($this->json($error), 200);
            }
        }
    }


    public function insertRetailCategory($company_id, $retail_parent_id, $retail_product_type, $retail_product_status, $retail_product_title, $retail_product_subtitle, $retail_product_desc, $tax_rate,$variant_flag,$retail_inventory,$retail_product_price,$retail_product_compare_price,$retail_product_banner_img_url) {
        $retail_product_id = $return_retail_product_id = $parent_id = '';
        $retail_novariant_inventory = "null";
        if(!is_null($retail_inventory) && !empty($retail_inventory)){
            $retail_novariant_inventory = mysqli_real_escape_string($this->db,$retail_inventory);
        }
        if(empty($retail_product_banner_img_url)||$retail_product_banner_img_url==''){
                 $retail_product_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/Default/default.png";
        }
        $compare_price = mysqli_real_escape_string($this->db, $retail_product_compare_price);
        if(is_null($retail_product_compare_price) || $retail_product_compare_price==null || trim($retail_product_compare_price)==''){
            $compare_price = 'null';
        }
        if ($retail_product_type == 'C') {
            $sql1 = sprintf("INSERT INTO `retail_products` (`company_id`, `retail_product_type`, `retail_product_status`, `retail_product_title`, `retail_product_desc`, `retail_product_subtitle`, `retail_banner_img_url`, `retail_sort_order`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', NextVal('retail_product_sort_order_seq'))", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_type), mysqli_real_escape_string($this->db, $retail_product_status), mysqli_real_escape_string($this->db, $retail_product_title), mysqli_real_escape_string($this->db, $retail_product_desc), 
                    mysqli_real_escape_string($this->db, $retail_product_subtitle), mysqli_real_escape_string($this->db, $retail_product_banner_img_url));
        } else {
            $status = $retail_product_status;
            if(!is_null(($retail_parent_id)) && !empty($retail_parent_id)){
                $parent_id = mysqli_real_escape_string($this->db, $retail_parent_id);
                $get_status = sprintf("SELECT `retail_product_status` FROM `retail_products` WHERE `company_id`='%s' AND `retail_product_id`='%s'",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_parent_id));
                $result_get = mysqli_query($this->db, $get_status);
                if(!$result_get){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_status");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    if(mysqli_num_rows($result_get) > 0){
                        $row_status = mysqli_fetch_assoc($result_get);
                        $status = $row_status['retail_product_status'];
                    }
                }            
            }else{
                $parent_id = 'null';
            }
            $sql1 = sprintf("INSERT INTO `retail_products` (`company_id`, `retail_parent_id`, `retail_product_type`, `retail_product_status`, `retail_product_title`, `retail_product_desc`, `retail_sort_order`, `retail_banner_img_url`,`product_tax_rate`,`retail_variant_flag`,`retail_product_inventory`,`retail_product_price`,`retail_product_compare_price`) VALUES ('%s', %s, '%s', '%s', '%s', '%s', NextVal('retail_product_sort_order_seq'), '%s', '%s', '%s', %s,'%s',%s)", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $parent_id), mysqli_real_escape_string($this->db, $retail_product_type), mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $retail_product_title), mysqli_real_escape_string($this->db, $retail_product_desc)
                    , mysqli_real_escape_string($this->db, $retail_product_banner_img_url), mysqli_real_escape_string($this->db, $tax_rate), mysqli_real_escape_string($this->db, $variant_flag), mysqli_real_escape_string($this->db, $retail_novariant_inventory), mysqli_real_escape_string($this->db, $retail_product_price), mysqli_real_escape_string($this->db, $compare_price));
        
            }
        $result1 = mysqli_query($this->db, $sql1);

        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $retail_product_id = mysqli_insert_id($this->db);
            if (!empty(trim($parent_id)) && $parent_id != 'null') {
                $return_retail_product_id = $parent_id;
                $url_str = "$parent_id/$retail_product_id";
            } else {
                $return_retail_product_id = $retail_product_id;
                $url_str = "$retail_product_id/";
            }
            
            $sql2 = sprintf("SELECT  `company_code` FROM  `company` WHERE  `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
            $result2 = mysqli_query($this->db, $sql2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $output_value = mysqli_fetch_assoc($result2);
                $company_code = $output_value['company_code'];
            }
            $company_code_new = $this->clean($company_code);
            $retail_product_title_new = $this->clean($retail_product_title);
            $retail_product_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/r/?=$company_code_new/$company_id/$url_str/";

            $sql3 = sprintf("UPDATE `retail_products` SET `retail_product_url` = '$retail_product_url' WHERE `retail_product_id` = '%s' ", mysqli_real_escape_string($this->db, $retail_product_id));
            $result3 = mysqli_query($this->db, $sql3);
            if (!$result3) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
            if($variant_flag=='Y'){
                $variant_msg = " but still it needs variants added to be published.";
            }else{
                $variant_msg = '';
            }
            $this->getretailindividualdetail($company_id,$retail_product_id,$retail_product_type,'A',$variant_msg);
        }
    }
    
    public function getretailindividualdetail($company_id,$retail_product_id,$type,$action,$variant_msg) {
        $getupdatedrowdetails = sprintf("SELECT `retail_product_id`, `retail_parent_id`, rp.`company_id`, `retail_product_type`, `retail_product_status`, `retail_product_title`, 
            `retail_product_subtitle`, `retail_product_url`, `retail_banner_img_url`, `retail_product_desc`, `product_tax_rate`, `retail_product_inventory`, 
            `retail_product_compare_price`, `retail_purchase_count`, `retail_net_sales`, `retail_sort_order`, `retail_product_price`,
            rp.`retail_variant_flag`, c.`wp_currency_symbol`, IF(rp.`retail_variant_flag`='N', CONCAT(c.wp_currency_symbol,rp.`retail_product_price`),
                (SELECT IF(MIN(variant_price)=MAX(variant_price), CONCAT(c.wp_currency_symbol, MIN(variant_price)), CONCAT(c.wp_currency_symbol, MIN(variant_price), ' - ', c.wp_currency_symbol, MAX(variant_price))) FROM `retail_variants` rv WHERE rv.`company_id`='%s' AND rv.`retail_product_id`=rp.`retail_product_id` AND rv.`deleted_flag`!='Y')) retail_variant_price
            FROM retail_products rp LEFT JOIN `company` c ON rp.`company_id`=c.`company_id` 
            WHERE rp.company_id='%s' AND retail_product_id='%s' AND retail_product_status!='D'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
        $getupdatedrowresult = mysqli_query($this->db, $getupdatedrowdetails);
        if (!$getupdatedrowresult) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getupdatedrowdetails");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if(mysqli_num_rows($getupdatedrowresult)>0){
                $rowdetails = mysqli_fetch_assoc($getupdatedrowresult);
                if($type=='C'){
                    $str = "Category";
                    $variant_msg = "";
                }else{
                    $str = "Product";
                }
                if($action=='A'){
                    $out = array('status' => "Success", 'msgs' => "$str added successfully$variant_msg", 'msg' => $rowdetails);
                }else{
                    $out = array('status' => "Success", 'msgs' => "$str updated successfully$variant_msg", 'msg' => $rowdetails);
                }
                $this->response($this->json($out), 200);
            }else{
                $out = array('status' => "Failed", 'msgs' => "Retail product details doesn't exist.");
                $this->response($this->json($out), 200);
            }
        }
    }

    //
    public function getcategorylistinDB($company_id) {
        $sql = sprintf("SELECT `retail_product_id`, `retail_product_title`  FROM `retail_products` WHERE `company_id`='%s' AND `retail_product_type` = 'C' AND `retail_product_status` = 'P'", 
                mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
        $output=[];
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                $out = array('status' => "Success", 'msg' => $output);
                $this->response($this->json($out), 200);
            } else {
                $out = array('status' => "Success", 'msgs' => "No Category found in database",'msg' => $output);
                $this->response($this->json($out), 200);
            }
        }
    }

    public function updateRetailCategoryDetails($retail_product_id, $company_id, $retail_parent_id, $retail_product_type, $retail_product_title, $retail_product_subtitle, $retail_product_desc, $retail_product_price, $retail_product_compare_price, $retail_product_banner_img_url, $retail_product_status, $retail_product_image_update, $list_type, $old_retail_banner_img_url,$tax_rate,$variant_flag,$retail_inventory) {
        $variant_msg = '';
        $compare_price = mysqli_real_escape_string($this->db, $retail_product_compare_price);
        if(is_null($retail_product_compare_price) || $retail_product_compare_price==null || trim($retail_product_compare_price)==''){
            $compare_price = 'null';
        }
        $retail_novariant_inventory = "null";
        if(!is_null($retail_inventory) && !empty($retail_inventory)){
            $retail_novariant_inventory = mysqli_real_escape_string($this->db,$retail_inventory);
        }
        
        $select_check = sprintf("SELECT * FROM `retail_products` WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `retail_product_status`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
        $result_check = mysqli_query($this->db, $select_check);
        if (!$result_check) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_check");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result_check)==0){
                $error = array('status' => "Failed", "msg" => "Product details not exists.");
                $this->response($this->json($error), 200);
            }
        }
        
        $parent_id = 'null';
        $status = $retail_product_status;
        if(!is_null(($retail_parent_id)) && !empty($retail_parent_id)){
            $parent_id = mysqli_real_escape_string($this->db, $retail_parent_id);
            $get_status = sprintf("SELECT `retail_product_status` FROM `retail_products` WHERE `company_id`='%s' AND `retail_product_id`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_parent_id));
            $result_get = mysqli_query($this->db, $get_status);
            if(!$result_get){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_status");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                if(mysqli_num_rows($result_get) > 0){
                    $row_status = mysqli_fetch_assoc($result_get);
                    $status = $row_status['retail_product_status'];
                }
                $this->updateCategoryandProductNetsales($company_id, $retail_parent_id, $retail_product_id);
            }            
        }else{
            if ($retail_product_type == 'S') {
                $this->updateCategoryandProductNetsales($company_id, '', $retail_product_id);
            }
        }

        if ($retail_product_image_update === 'Y') {
            $update_retail_product = sprintf("UPDATE `retail_products` SET `retail_parent_id`=$parent_id, `retail_product_type`='%s', `retail_product_title`='%s',  `retail_product_subtitle`='%s', `retail_product_desc`='%s', `retail_product_price`='%s', `retail_product_compare_price`=$compare_price, `retail_banner_img_url`='%s', `retail_product_status`='%s',`product_tax_rate`='%s',`retail_variant_flag`='%s',`retail_product_inventory`=%s WHERE `retail_product_id`='%s' and `company_id`='%s'", 
                    mysqli_real_escape_string($this->db, $retail_product_type), mysqli_real_escape_string($this->db, $retail_product_title), mysqli_real_escape_string($this->db, $retail_product_subtitle), mysqli_real_escape_string($this->db, $retail_product_desc), $retail_product_price, mysqli_real_escape_string($this->db, $retail_product_banner_img_url), $status, mysqli_real_escape_string($this->db, $tax_rate), mysqli_real_escape_string($this->db, $variant_flag), mysqli_real_escape_string($this->db, $retail_novariant_inventory), mysqli_real_escape_string($this->db, $retail_product_id), mysqli_real_escape_string($this->db, $company_id));
        } else {
            $update_retail_product = sprintf("UPDATE `retail_products` SET `retail_parent_id`=$parent_id, `retail_product_type`='%s', `retail_product_title`='%s',  `retail_product_subtitle`='%s', `retail_product_desc`='%s', `retail_product_price`='%s', `retail_product_compare_price`=$compare_price, `retail_product_status`='%s',`product_tax_rate`='%s',`retail_variant_flag`='%s',`retail_product_inventory`=%s WHERE `retail_product_id`='%s' and `company_id`='%s'", 
                    mysqli_real_escape_string($this->db, $retail_product_type), mysqli_real_escape_string($this->db, $retail_product_title), mysqli_real_escape_string($this->db, $retail_product_subtitle), mysqli_real_escape_string($this->db, $retail_product_desc), $retail_product_price, $status, mysqli_real_escape_string($this->db, $tax_rate), mysqli_real_escape_string($this->db, $variant_flag), mysqli_real_escape_string($this->db, $retail_novariant_inventory), mysqli_real_escape_string($this->db, $retail_product_id), mysqli_real_escape_string($this->db, $company_id));
        }
        $result = mysqli_query($this->db, $update_retail_product);
        //Print Company details after login success
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_retail_product");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if ($retail_product_image_update === 'Y' && !empty($old_retail_banner_img_url) && strpos($old_retail_banner_img_url, 'default.png') === false) {
                $cfolder_name = "Company_$company_id";
                $folder_name = "Retail";
                $old = getcwd(); // Save the current directory
                $dir_name = "../../../$this->upload_folder_name/$cfolder_name/$folder_name";
                chdir($dir_name);
                $temp_old_img_url = stripslashes($old_retail_banner_img_url);
                $split = explode("$cfolder_name/$folder_name/", $temp_old_img_url);
                $filename = $split[1];
                if (file_exists($filename)) {
                    unlink($filename);
                }
                chdir($old); // Restore the old working directory
            }
            if($variant_flag=='N'){
                $this->deleteallvariantsindb($company_id, $retail_product_id, 1);
            }else{
                $sql = sprintf("SELECT IF(COUNT(*)>0, 'Y', 'N') variant_available FROM `retail_variants` WHERE `company_id` = '%s' AND `retail_product_id` = '%s' AND `deleted_flag` = 'N' ORDER BY `variant_sort_order` DESC"
                        ,mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_product_id));
                $result = mysqli_query($this->db, $sql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } else {
                    $row = mysqli_fetch_assoc($result);
                    $variant_available = $row['variant_available'];
                    if($variant_available=='N'){
                        $variant_msg = " but still it needs variants added to be published.";
                    }
                }
            }
            if ($retail_product_type == 'C') {
                $update_child_status = sprintf("UPDATE `retail_products` SET `retail_product_status`='%s' WHERE `retail_parent_id`='%s' AND `company_id`='%s' AND `retail_product_status`!='D'", mysqli_real_escape_string($this->db, $retail_product_status), mysqli_real_escape_string($this->db, $retail_product_id), mysqli_real_escape_string($this->db, $company_id));
                $result_child_status = mysqli_query($this->db, $update_child_status);
                if (!$result_child_status) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_status");
                    log_info($this->json($error_log));
                }
            }
            $this->getretailindividualdetail($company_id,$retail_product_id,$retail_product_type,'U',$variant_msg);
        }
    }

    public function addRetailDiscountDetails($company_id, $discount_code, $discount_id, $discount_type, $discount_amount, $min_purchase_amount, $min_purchase_flag, $start_date, $end_date, $discount_limit_flag, $discount_limit_value, $type,$applies_entire_or_specific,$selected_products) {
       $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $retail_updt_string = "";
        $query = sprintf("SELECT * FROM `retail_discount` WHERE `company_id`='%s' AND `retail_discount_code`='%s' AND `deleted_flag`='N'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $discount_code));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $result_value = mysqli_fetch_object($result);
                $retail_discount_id = $result_value->retail_discount_id;
                if ($type == 'add') {
                    $error = array('status' => "Failed", "msg" => "Discount Code For Retail already available for this Studio.");
                    $this->response($this->json($error), 200);
                } elseif ($type == 'update' && $discount_id !== $retail_discount_id) {
                    $error = array('status' => "Failed", "msg" => "Discount Code For Retail already available for this Studio.");
                    $this->response($this->json($error), 200);
                }
            }
        }
        if ($min_purchase_flag == 'Q') {
            $value_string = ", '%s'";
            $retail_string = ",`min_purchase_quantity`";
        } elseif ($min_purchase_flag == 'V') {
            $value_string = ", '%s'";
            $retail_string = ",`min_purchase_amount`";
        } else {
            $retail_string = "";
            $value_string = "";
        }
        if ($discount_limit_flag == 'Y') {
            $disc_flag = 'Y';
            $disc_limit = $discount_limit_value;
        } else {
            $disc_flag = 'N';
            $disc_limit = 0;
        }

        if ($type == 'add') {
            $sql1 = sprintf("INSERT INTO `retail_discount` (`retail_discount_usage_type`,`company_id`,`retail_discount_type`, `retail_discount_code`, `retail_discount_amount`, `min_purchase_flag`, `discount_begin_dt`,`discount_end_dt`,`discount_limit_flag`,`discount_limit_value` $retail_string) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s'$value_string)", mysqli_real_escape_string($this->db, $applies_entire_or_specific), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $discount_type), mysqli_real_escape_string($this->db, $discount_code), mysqli_real_escape_string($this->db, $discount_amount), mysqli_real_escape_string($this->db, $min_purchase_flag), mysqli_real_escape_string($this->db, $start_date), mysqli_real_escape_string($this->db, $end_date), mysqli_real_escape_string($this->db, $disc_flag), mysqli_real_escape_string($this->db, $disc_limit), mysqli_real_escape_string($this->db, $min_purchase_amount));
            $msg_string = "Discount Code Added Successfully.";
        } elseif ($type == 'update') {
            if (!empty($retail_string)) {
                $temp = mysqli_real_escape_string($this->db, $min_purchase_amount);
                $retail_updt_string = "$retail_string='$temp'";
            }
            $sql1 = sprintf("UPDATE `retail_discount` SET `retail_discount_usage_type`='%s',`retail_discount_type`='%s', `retail_discount_code`='%s', `retail_discount_amount`='%s', `min_purchase_flag`='%s', `discount_begin_dt`='%s',`discount_end_dt`='%s',`discount_limit_flag`='%s',`discount_limit_value`='%s'$retail_updt_string 
                    WHERE `company_id`='%s' AND `retail_discount_id`='%s'", mysqli_real_escape_string($this->db, $applies_entire_or_specific), mysqli_real_escape_string($this->db, $discount_type), mysqli_real_escape_string($this->db, $discount_code), mysqli_real_escape_string($this->db, $discount_amount), mysqli_real_escape_string($this->db, $min_purchase_flag), mysqli_real_escape_string($this->db, $start_date), mysqli_real_escape_string($this->db, $end_date), mysqli_real_escape_string($this->db, $disc_flag), mysqli_real_escape_string($this->db, $disc_limit), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $discount_id));
            $msg_string = "Discount Code Updated Successfully.";
        }
        $result1 = mysqli_query($this->db, $sql1);

        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if ($type == 'add') {
            $ret_discount_id = mysqli_insert_id($this->db);
            }else{
               $ret_discount_id=$discount_id; 
            }
            if($applies_entire_or_specific=='P'){
                $this->addDiscountProductsInDb($company_id, $ret_discount_id, $selected_products);
            }
            $out = array('status' => "Success", "msg" => $msg_string);
            $this->response($this->json($out), 200);
        }
    }

    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }

    public function getRetailDiscountDetails($company_id, $discount_tab, $draw_table, $length_table, $start, $sorting) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
//        $currdate_add = "SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $currtime_add = "SELECT CONVERT_TZ(now(),$tzadd_add,'$new_timezone')";
        $last_updt_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";
//        $end_limit=200;
//        $sort = $sorting[0]['column'];
//        $sort_type = $sorting[0]['dir'];
        $out = $output =$discount_products=$disc_pr=$disc = [];
        $count['recent_filtered'] = $count['cancelled_filtered'] = 0;
        $date_format = "'%b %d, %Y'";
        $timeformat = "%h:%i %p";       

        if ($discount_tab == 'active') { //ORDERS TAB
            $d_tab = "AND IF(`discount_begin_dt`<=($currtime_add), `discount_end_dt`='0000-00-00 00:00:00' OR `discount_end_dt`>($currtime_add), 0)";
        } elseif ($discount_tab == 'scheduled') {
            $d_tab = "AND `discount_begin_dt`>($currtime_add)";
        } elseif ($discount_tab == 'expired') {
            $d_tab = "AND `discount_end_dt`!='0000-00-00 00:00:00' AND `discount_end_dt`<($currtime_add)";
        } elseif ($discount_tab == 'all') {
            $d_tab = "AND rd.`deleted_flag`!='Y'";
        }

//        $column = array(
//           0=>'created_dt', 2=>'buyer_name', 3=>'participant_name', 4=>'participant_phone',5=>'participant_email',6=>"par_bt_dt_mn $sort_type, par_bt_dt_dy",7=>'cast(participant_age AS unsigned)',8=>'mrk.`rank_name`',9=>'cast(last_advanced AS unsigned)',10=>'mr.created_dt',11=>'membership_reg_type_user',13=>'mr.`next_payment_date`'
//        ); 
//                IF(`discount_begin_dt`='0000-00-00 00:00:00',IF(`discount_end_dt`='0000-00-00 00:00:00','Active',IF(`discount_end_dt`<=($currtime_add),'Expired','Active')),
//                IF(`discount_begin_dt`>($currtime_add), IF(`discount_end_dt`>($currtime_add) OR `discount_end_dt`='0000-00-00 00:00:00','Scheduled','Expired'),'Expired')) AS status,

        $query = sprintf("SELECT rd.`retail_discount_usage_type`,rd.`retail_discount_id` as id, rd.`company_id`,  IFNULL(DATE_FORMAT(`discount_begin_dt`,%s),'') start_date,discount_used_count discount_used, `retail_discount_type`,`min_purchase_flag`,
                IFNULL(DATE_FORMAT(`discount_end_dt`,%s),'') end_date, `retail_discount_code` discount_code, DATE_FORMAT(($last_updt_date),%s) last_updt_dt,`min_purchase_amount`,`min_purchase_quantity`,discount_begin_dt,discount_end_dt,retail_discount_amount,discount_limit_flag,discount_limit_value,TIME_FORMAT(discount_begin_dt,'%s') start_time,TIME_FORMAT(discount_end_dt,'%s') end_time,
                IF(`discount_begin_dt`<($currtime_add), IF(`discount_end_dt`='0000-00-00 00:00:00', 'Active', IF(`discount_end_dt`<=($currtime_add),'Expired','Active')), 'Scheduled') as status,
                rdp.`retail_product_id`,rdp.`retail_discount_id`,rdp.`deleted_flag` disc_prod_del_flg,rp.`retail_product_title`,rp.`retail_banner_img_url`,rp.`retail_variant_flag`,rp.`retail_product_status` prod_sts,
                if(retail_variant_flag='N', rp.`retail_product_price`, (SELECT MAX(variant_price) FROM `retail_variants` rv WHERE rp.`company_id`=rv.`company_id` AND rp.`retail_product_id`=rv.`retail_product_id` AND rv.deleted_flag='N')) retail_product_price
                FROM `retail_discount` rd
                LEFT JOIN `retail_discount_products` rdp ON rd.`retail_discount_id`=rdp.`retail_discount_id` AND rd.`company_id`=rdp.`company_id`
                LEFT JOIN `retail_products` rp ON rp.`retail_product_id`=rdp.`retail_product_id` AND rp.`company_id`=rd.`company_id`
                WHERE rd.`company_id`='%s' AND rd.`deleted_flag`!='Y' $d_tab  order by start_date LIMIT $start, $length_table ", $date_format, $date_format, $date_format, $timeformat, $timeformat, mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            $out['recordsTotal'] = $out['recordsFiltered'] = $num_rows;
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    if (!in_array($row['id'], $disc_pr)) {
                        $output[] = array_slice($row, 0, 21);
                        $disc_pr[] = $row['id'];
                    }
                    if ($row['retail_discount_usage_type'] == 'P' && $row['disc_prod_del_flg']!='Y' && $row['prod_sts']=='P') {
                        $discount_products['retail_discount_id'] = $row['retail_discount_id'];
                        $discount_products['retail_product_id'] = $row['retail_product_id'];
                        $discount_products['retail_product_price'] = $row['retail_product_price'];
                        $discount_products['retail_variant_flag'] = $row['retail_variant_flag'];
                        $discount_products['retail_product_title'] = $row['retail_product_title'];
                        $discount_products['retail_banner_img_url'] = $row['retail_banner_img_url'];
                        $disc[] = $discount_products;
                    }
                }
                for ($k = 0; $k < count($disc); $k++) {
                    for ($m = 0; $m < count($disc_pr); $m++) {
                        if ($disc[$k]['retail_discount_id'] == $disc_pr[$m]) {
                            $output[$m]['discount_products'][] = $disc[$k];
                        }
                    }
                }
                $out['draw'] = $draw_table;
                $out['data'] = $output;
                $this->response($this->json($out), 200);
            } else {
                $out['draw'] = $draw_table;
                $out['data'] = $output;
                $this->response($this->json($out), 200); // If no records "No Content" status
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

    public function updateRetailDiscountDetails($company_id, $discount_tab, $discount_id_list, $select_all_flag) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $output = [];

        if ($select_all_flag == 'Y') {
            $count['recent_filtered'] = $count['cancelled_filtered'] = 0;
            $date_format = "'%b %d, %Y'";
            $host = $_SERVER['HTTP_HOST'];
            if (strpos($host, 'mystudio.academy') !== false) {
                $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
            } else {
                $tzadd_add = "'" . $curr_time_zone . "'";
            }
//            $currdate_add = "SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
            $currtime_add = "SELECT CONVERT_TZ(now(),$tzadd_add,'$new_timezone')";
            $last_updt_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";


            if ($discount_tab == 'active') { //ORDERS TAB
                $d_tab = "AND IF(`discount_begin_dt`<=($currtime_add), `discount_end_dt`='0000-00-00 00:00:00' OR `discount_end_dt`>($currtime_add), 0)";
            } elseif ($discount_tab == 'scheduled') {
                $d_tab = "AND `discount_begin_dt`>($currtime_add)";
            } elseif ($discount_tab == 'expired') {
                $d_tab = "AND `discount_end_dt`!='0000-00-00 00:00:00' AND `discount_end_dt`<($currtime_add)";
            } elseif ($discount_tab == 'all') {
                $d_tab = "AND rd.`deleted_flag`!='Y'";
            }

            $query = sprintf("SELECT rd.`retail_discount_id` as id, rd.`company_id`,  IFNULL(DATE_FORMAT(`discount_begin_dt`,%s),'') start_date,discount_used_count, `retail_discount_type`,`min_purchase_flag`,
                IFNULL(DATE_FORMAT(`discount_end_dt`,%s),'') end_date, `retail_discount_code` discount_code, DATE_FORMAT(($last_updt_date),%s) last_updt_dt,`min_purchase_amount`,`min_purchase_quantity`,
                IF(`discount_begin_dt`<($currtime_add), IF(`discount_end_dt`='0000-00-00 00:00:00', 'Active', IF(`discount_end_dt`<=($currtime_add),'Expired','Active')), 'Scheduled') as status
                FROM `retail_discount` rd WHERE rd.`company_id`='%s' AND rd.`deleted_flag`!='Y' $d_tab", $date_format, $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id));
            $result = mysqli_query($this->db, $query);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $num_rows = mysqli_num_rows($result);
                $out['recordsTotal'] = $out['recordsFiltered'] = $num_rows;
                if ($num_rows > 0) {
                    while ($row = mysqli_fetch_assoc($result)) {
                        $discount_id_list[] = $row['id'];
                    }
                } else {
                    $error = array('status' => "Failed", "msg" => "No Discounts Found.");
                    $this->response($this->json($error), 200);
                }
            }
        }

        for ($i = 0; $i < count($discount_id_list); $i++) {
            $output[] = $discount_id_list[$i]['id'];
        }
        $disc_id_list = implode(",", $output);

        $update_query = sprintf("UPDATE `retail_discount` SET `deleted_flag`='Y' WHERE `retail_discount_id` in (%s) AND `company_id`='%s' ", mysqli_real_escape_string($this->db, $disc_id_list), mysqli_real_escape_string($this->db, $company_id));
        $update_result = mysqli_query($this->db, $update_query);
        if (!$update_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            date_default_timezone_set($curr_time_zone);
            $error = array('status' => "Success", "msg" => "Discount Successfully Deleted.");
            $this->response($this->json($error), 200);
        }
    }
    
    public function addretailvariantdetails($company_id, $retail_id, $variant_name, $variant_inventory_count,$variant_price,$variant_compare_price) {
        $inventory_count = $compare_price = "null";
        if(!is_null($variant_inventory_count) && trim($variant_inventory_count) != ''){
            $inventory_count = mysqli_real_escape_string($this->db,$variant_inventory_count);
        }
        if(!is_null($variant_compare_price) && !empty($variant_compare_price)){
            $compare_price = mysqli_real_escape_string($this->db,$variant_compare_price);
        }
        $insert_variant = sprintf("INSERT INTO `retail_variants` (`company_id`, `retail_product_id`, `retail_variants_name`,`variant_sort_order`, `inventory_count`,`variant_price`,`variant_compare_price`)VALUES ('%s','%s','%s',NextVal('retail_variant_sort_order_seq'), %s,'%s',%s)", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_id), mysqli_real_escape_string($this->db,$variant_name),$inventory_count,  mysqli_real_escape_string($this->db, $variant_price),mysqli_real_escape_string($this->db,$compare_price));
        $result = mysqli_query($this->db, $insert_variant);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_variant");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getretailvariantdetails($company_id, $retail_id); 
        }
    }
    
    public function updateretailvariantdetails($company_id, $retail_id, $variant_name, $variant_inventory_count,$variant_id,$type,$variant_price,$variant_compare_price){
        if($type == "delete"){
            $update_variant = sprintf("UPDATE `retail_variants` SET deleted_flag = 'Y' WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `retail_variant_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_id), mysqli_real_escape_string($this->db, $variant_id));
            $result = mysqli_query($this->db, $update_variant);
        }else{
            $inventory_count = $compare_price = "null";
            if(!is_null($variant_inventory_count) && trim($variant_inventory_count) != ''){
                $inventory_count = mysqli_real_escape_string($this->db,$variant_inventory_count);
            }
            if(!is_null($variant_compare_price) && !empty($variant_compare_price)){
                $compare_price = mysqli_real_escape_string($this->db,$variant_compare_price);
            }
            $update_variant = sprintf("UPDATE `retail_variants` SET retail_variants_name='%s', inventory_count =%s,`variant_price` = '%s',`variant_compare_price` = %s WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `retail_variant_id`='%s'",
                    mysqli_real_escape_string($this->db, $variant_name),$inventory_count,  mysqli_real_escape_string($this->db, $variant_price), $compare_price,mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_id), mysqli_real_escape_string($this->db, $variant_id));
            $result = mysqli_query($this->db, $update_variant);
        }
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_variant");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getretailvariantdetails($company_id, $retail_id); 
        }
    }
    
    public function getretailvariantdetails($company_id, $retail_id) {
        $retail_variant_price='';
        $output = [];
        $sql = sprintf("SELECT * FROM (SELECT 'list' as type, `retail_variant_id` variant_id,`retail_variants_name` variant_name,`variant_price`,`variant_compare_price`,`inventory_count` variant_inventory_count, '' retail_variant_price,`variant_sort_order` as variant_order FROM `retail_variants` WHERE `company_id` = '%s' AND `retail_product_id` = '%s' AND `deleted_flag` = 'N' )T1
                    UNION 
                    SELECT 'count' as type, '' variant_id, '' variant_name, '' variant_price, '' variant_compare_price, '' variant_inventory_count, IF(MIN(variant_price)=MAX(variant_price), CONCAT(c.wp_currency_symbol, MIN(variant_price)), CONCAT(c.wp_currency_symbol, MIN(variant_price), ' - ', c.wp_currency_symbol, MAX(variant_price))) retail_variant_price,0 variant_order FROM `retail_variants` rv LEFT JOIN `company` c ON rv.`company_id`=c.`company_id` WHERE rv.`company_id`='%s' AND rv.`retail_product_id`='%s' AND rv.`deleted_flag`!='Y'  ORDER BY variant_order DESC" ,
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_id), 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$retail_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    if($row['type']=='list'){
                        $output[] = array_slice($row, 1);
                    }else{
                        $retail_variant_price = $row['retail_variant_price'];
                    }
                }
                $out = array('status' => "Success", 'msg' => $output, "retail_variant_price"=>$retail_variant_price);
                $this->response($this->json($out), 200);
            } else {
                $out = array('status' => "Success", 'msgs' => "No Variants found in database",'msg' => $output, "retail_variant_price"=>$retail_variant_price);
                $this->response($this->json($out), 200);
            }
        }
    }
    
    public function deleteallvariantsindb($company_id, $retail_id, $callback){
        $select_sql = sprintf("SELECT * FROM `retail_variants` WHERE `company_id`='%s' AND `retail_product_id`='%s' AND deleted_flag!='Y'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_id));
        $result_select = mysqli_query($this->db, $select_sql);
        if(!$result_select){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result_select)>0){                
                $update_variant = sprintf("UPDATE `retail_variants` SET deleted_flag='Y' WHERE `company_id`='%s' AND `retail_product_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_id));
                $result = mysqli_query($this->db, $update_variant);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_variant");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
        }
    }
    
    public function getretailmanagelistdetails($company_id,$list_type,$parent_id,$template_flag,$template_type) {   //1 - from template flag true, 0 - from template flag false
        if($template_flag!=1){
            $this->checkRetailFirstTime($company_id);
        }
        $query_str = "";
        if(!empty($parent_id)){
            $escaped_input = mysqli_real_escape_string($this->db, $parent_id);
            $query_str = " AND rp.`retail_product_type`='P' AND rp.`retail_parent_id`='$escaped_input'";
        }else{
            if($template_flag==1){
                if($template_type=='C'){
                    $query_str = " AND rp.`retail_product_type` IN ('C')";
                }else{
                    $query_str = " AND rp.`retail_product_type` IN ('S', 'P')";
                }
            }else{
                $query_str = " AND rp.`retail_product_type` IN ('C', 'S')";
            }
        }
        
        if($template_flag==1){
            $comp_id = 6;
        }else{
            $comp_id = $company_id;
        }
        
        $list = $output['live']=$output['draft']=[];
        $count['P']=$count['S']=0;        
        $list = $this->getretailsettingdetails($company_id,1);
        $company_code = $list['msg']['company_code'];
        $retail_list_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/r/?=$company_code/$company_id".'///'.time();
        
        
        $query=sprintf("SELECT rp.`retail_product_id`,rp.`retail_parent_id`, rp.`company_id`, rp.`retail_product_type`, rp.`retail_product_url`, rp.`retail_product_status`, rp.`retail_product_title`,
            IF(rp.retail_product_type='C' , (SELECT IFNULL(SUM(IF(rp2.`retail_variant_flag`='N', rp2.`retail_purchase_count`, (select IFNULL(SUM(`inventory_used_count`),0) from `retail_variants` where company_id=rp2.`company_id` AND `retail_product_id`=rp2.`retail_product_id` AND deleted_flag='N'))),0) from retail_products rp2 where rp2.`retail_parent_id`=rp.`retail_product_id` and rp2.`retail_product_status`='%s'),
            IF(rp.`retail_variant_flag`='N', rp.`retail_purchase_count`,(select IFNULL(SUM(`inventory_used_count`),0) from `retail_variants` where company_id=rp.`company_id` AND `retail_product_id`=rp.`retail_product_id` AND deleted_flag='N'))) retail_purchase_count, 
            rp.`retail_net_sales`, rp.`retail_sort_order`, rp.`retail_banner_img_url`, rp.`retail_product_price`, rp.`retail_product_compare_price`, rp.`retail_product_subtitle`, 
            rp.`retail_variant_flag`, rp.`retail_product_price`, IF(rp.`retail_variant_flag`='N', 0, (SELECT COUNT(*) FROM `retail_variants` WHERE company_id=rp.`company_id` AND `retail_product_id`=rp.`retail_product_id` AND deleted_flag='N')) variant_count,
            c.`wp_currency_symbol`, IF(rp.`retail_variant_flag`='N', CONCAT(c.wp_currency_symbol,rp.`retail_product_price`),
            (SELECT IF(MIN(variant_price)=MAX(variant_price), CONCAT(c.wp_currency_symbol, MIN(variant_price)), CONCAT(c.wp_currency_symbol, MIN(variant_price), ' - ', c.wp_currency_symbol, MAX(variant_price))) FROM `retail_variants` rv WHERE rv.`company_id`='%s' AND rv.`retail_product_id`=rp.`retail_product_id` AND rv.`deleted_flag`!='Y')) retail_variant_price
            FROM `retail_products` rp LEFT JOIN `company` c ON rp.`company_id`=c.`company_id` 
            WHERE rp.`company_id`='%s' AND rp.`retail_product_status`='%s' $query_str GROUP BY rp.`retail_product_id` ORDER BY rp.`retail_sort_order` desc", mysqli_real_escape_string($this->db, $list_type), mysqli_real_escape_string($this->db, $comp_id), mysqli_real_escape_string($this->db, $comp_id),mysqli_real_escape_string($this->db, $list_type) );
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    if($list_type=='P'){
                        $row['retail_product_url'] = $row['retail_product_url'].time();
                        $output['live'][] = $row;
                        $count['P']=$num_of_rows;
                    }else{
                       $output['draft'][] = $row;
                       $count['S']=$num_of_rows;
                    }
                }
                $out = array('status' => "Success", 'msg' => $output,'count'=>$count, 'sales' => $list['msg'],'retail_list_url' => $retail_list_url);
                $this->response($this->json($out), 200);
            } else {
                $out = array('status' => "Success", 'msgs' => "No details found",'count'=>$count, 'sales' => $list['msg'], 'msg' => $output, 'retail_list_url' => $retail_list_url);
                $this->response($this->json($out), 200);
            }
        }
    }
    
    public function updateRetailSettings($company_id, $retail_status, $retail_processing_type, $retail_agreement,$update_type){
        $stripe_status = $wepay_status = 'N';
        $check_wepay = false;
        $company_record = $this->getCompanyRecord($company_id,1); // CALL BACK - 0/1
        $country = $company_record['country'];
        $country_support = $this->checkCountryPaymentSupport($company_id, $country, 1);     // CHECKING WHETHER STRIPE SUPPORTS RESPECTIVE COUNTRY
        if($country_support['status'] == 'Success'){
            if($country_support['payment_support'] == 'S' && $country_support['support_status'] == 'Y'){
                $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
                if ($stripe_account != '' && $stripe_account['account_state'] !== 'N') {        // CHECKING CURRENT STUDIO HAS STRIPE ACCOUNT
                   $stripe_status = $stripe_account['account_state'];
                   $wepay_status = 'N';
                } else {
                    $check_wepay = true;
                }
            }else{
                $check_wepay = true;
            }

            if($check_wepay && $country_support['payment_support'] == 'W' && $country_support['support_status'] == 'Y'){ // CHECKING WHETHER WEPAY SUPPORTS RESPECTIVE COUNTRY
                 $wepay_status = $this->wepayStatus($company_id); 
                 $stripe_status = 'N';
            }
        }
        
        if($update_type == 'sales' && $retail_status=='Y' && $stripe_status == 'N' && $wepay_status != 'A'){
            $resp = $this->getretailsettingdetails($company_id,1);
            if($resp['status']=='Success'){
                $error = array("status" => "Failed", "msg" => "Please enable mobile payments to publish this Online Store.", "msgs" => $resp['msg']);
                $this->response($this->json($error),200);
            }
        }
        if($update_type == 'sales'){
            if($retail_status === "N") {
                $update_settings = sprintf("UPDATE `company` c,`studio_pos_settings` pos SET c.`retail_enabled`='%s',pos.`retail_status`='D' WHERE c.`company_id`=pos.company_id AND c.`company_id`='%s'", mysqli_real_escape_string($this->db, $retail_status), mysqli_real_escape_string($this->db, $company_id));
            } else {
                $update_settings = sprintf("UPDATE `company` SET `retail_enabled`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $retail_status), mysqli_real_escape_string($this->db, $company_id));
            }
        }else{
            $update_settings = sprintf("UPDATE `company` SET `retail_processingfee`='%s',`retail_sales_agreement`='%s' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $retail_processing_type),mysqli_real_escape_string($this->db, $retail_agreement),mysqli_real_escape_string($this->db, $company_id));
        }
        $result = mysqli_query($this->db, $update_settings);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_settings");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getretailsettingdetails($company_id,0);
        }
    }
    
    public function getretailsettingdetails($company_id,$callback) {
        $sql = sprintf("SELECT retail_enabled,retail_processingfee,retail_sales_agreement,company_code from company where company_id = '%s'" ,mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output['retail_processingfee'] = $row['retail_processingfee'];
                    $output['retail_agreement_text'] = $row['retail_sales_agreement'];
                    $output['retail_enabled'] = $row['retail_enabled'];
                    $output['company_code'] = $row['company_code'];
                }
                $out = array('status' => "Success", 'msg' => $output);
                if($callback == 0){
                    $this->response($this->json($out), 200);
                }else{
                    return $out;
                }
            } else {
                $output[] = "";
                $out = array('status' => "Success", 'msgs' => "No Record",'msg' => "$output");
                if($callback == 0){
                    $this->response($this->json($out), 200);
                }else{
                    return $out;
                }
            }
        }
    }
    
//    public function getproductsinDB($company_id,$limit,$search_term,$discount_amount) {
//        if (!empty($search_term)) {
//            $search_term = "and retail_product_title LIKE '%$search_term%'";
//        }
//        $output = [];
//        $query = sprintf("SELECT `retail_product_id`,`retail_product_title`,`retail_banner_img_url`,`retail_product_price`,`retail_product_inventory` from retail_products where company_id='%s' and (retail_product_price >= %s or retail_product_price is null) %s limit %s,50", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $discount_amount), $search_term, mysqli_real_escape_string($this->db, $limit));
//        $result = mysqli_query($this->db, $query);
//        if (!$result) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
//            $this->response($this->json($error_log), 500);
//            log_info($this->json($error_log));
//        } else {
//            $num_of_rows = mysqli_num_rows($result);
//            if ($num_of_rows > 0) {
//                while ($row = mysqli_fetch_assoc($result)) {
//                    $output[] = $row;
//                }
////                $output = (object) $output;
//                $out = array('status' => "Success", 'msg' => $output,'msgs' => "");
//                $this->response($this->json($out), 200);
//            } else {
//                $output = [];
//                $out = array('status' => "Success", 'msgs' => "No details found",'msg' => $output);
//                $this->response($this->json($out), 200);
//            }
//        }
//    }
    
    public function getproductsinDB($company_id,$limit,$search_term,$discount_amount,$callback) {
        if (!empty($search_term)) {
            $s_term = mysqli_real_escape_string($this->db, $search_term);
            $search_term = "and retail_product_title LIKE '%$s_term%'";
        }
        $output = [];
//        $query = sprintf("SELECT rp.`retail_product_id`,`retail_product_title`,`retail_banner_img_url`,`retail_variant_flag`,rp.`retail_product_price`,
//                       IF(rp.`retail_variant_flag`='N',rp.`retail_product_price`, CONCAT(min(rv.`variant_price`),' - ',max(rv.`variant_price`))) retail_product_price,
//                       IF(rp.`retail_variant_flag`='N',rp.`retail_product_price`, max(rv.`variant_price`)) retail_product_price_for_disc,
//                       IF(rp.`retail_variant_flag`='Y', min(rv.`variant_price`), 0) min_variant_price, IF(rp.`retail_variant_flag`='Y', max(rv.`variant_price`), 0) max_variant_price, 
//                       IF(`retail_variant_flag`='N',`retail_product_inventory`,SUM(rv.`inventory_count`)) retail_product_inventory FROM `retail_products` rp 
//                       LEFT JOIN `retail_variants` rv ON rp.`company_id`=rv.`company_id` AND rp.`retail_product_id`=rv.`retail_product_id` AND rv.deleted_flag='N'
//                       WHERE rp.`company_id`='%s' AND rp.`retail_product_status`='P' %s GROUP BY rp.retail_product_id HAVING max_variant_price<='%s' AND
//                       IF(rp.`retail_variant_flag`='N', rp.`retail_product_price`>= %s and rp.`retail_product_price`>= %s and rp.`retail_product_price` != 0, max_variant_price>= %s  and max_variant_price!= 0)
//                       limit %s,50",
        $query = sprintf("SELECT rp.`retail_product_id`,`retail_product_title`,`retail_banner_img_url`,`retail_variant_flag`,
                IF(rp.`retail_variant_flag`='N',rp.`retail_product_price`, IFNULL(CONCAT(min(rv.`variant_price`),' - ',max(rv.`variant_price`)),0)) retail_product_price,
                IF(rp.`retail_variant_flag`='N',rp.`retail_product_price`, IFNULL(max(rv.`variant_price`),0)) retail_product_price_for_disc,
                IF(rp.`retail_variant_flag`='Y', IFNULL(min(rv.`variant_price`), 0), 0) min_variant_price, 
                IF(rp.`retail_variant_flag`='Y', IFNULL(max(rv.`variant_price`), 0), 0) max_variant_price, 
                IF(`retail_variant_flag`='N',`retail_product_inventory`,SUM(rv.`inventory_count`)) retail_product_inventory FROM `retail_products` rp 
                LEFT JOIN `retail_variants` rv ON rp.`company_id`=rv.`company_id` AND rp.`retail_product_id`=rv.`retail_product_id` AND rv.`deleted_flag`='N'
                WHERE rp.`company_id`='%s' AND rp.`retail_product_status`='P' AND rp.`retail_product_type`!='C' %s GROUP BY rp.retail_product_id
                HAVING IF(rp.`retail_variant_flag`='N', retail_product_price>= %s and retail_product_price != 0, max_variant_price>= %s and max_variant_price!= 0) limit %s, 50",
                mysqli_real_escape_string($this->db, $company_id), $search_term, mysqli_real_escape_string($this->db, $discount_amount), mysqli_real_escape_string($this->db, $discount_amount), mysqli_real_escape_string($this->db, $limit));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $output=[];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                if($callback==1){
                    return $output;
                }else{
                    $out = array('status' => "Success", 'msg' => $output);
                    $this->response($this->json($out), 200);
                }
            } else {
                if($callback==1){
                    return $output;
                }else{
                    $out = array('status' => "Success", 'msgs' => "No details found",'msg' => $output);
                    $this->response($this->json($out), 200);
                }
            }
        }
    }
    
    private function addDiscountProductsInDb($company_id, $discount_id, $product_id) {
        $product_list = "";
        for($i=0; $i<count($product_id); $i++){
            if($i==0){
                $product_list = $product_id[$i]['retail_product_id'];
            }else{
                $product_list = $product_list.",".$product_id[$i]['retail_product_id'];
            }
        }
        $select_sql = sprintf("SELECT * FROM `retail_discount_products` WHERE `company_id`='%s' AND `retail_discount_id`='%s' AND `retail_product_id` NOT IN (%s)",
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$discount_id), mysqli_real_escape_string($this->db,$product_list));
        $result_sql = mysqli_query($this->db, $select_sql);
        log_info($select_sql);
        if (!$result_sql) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_sql");
            $this->response($this->json($error_log), 500);
            log_info($this->json($error_log));
        } else {
            $num_of_rows1 = mysqli_num_rows($result_sql);
            if ($num_of_rows1 > 0) {
                $update_sql = sprintf("UPDATE `retail_discount_products` SET `deleted_flag`='Y' WHERE `company_id`='%s' AND `retail_discount_id`='%s' AND `retail_product_id` NOT IN (%s)",
                        mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$discount_id), mysqli_real_escape_string($this->db,$product_list));
                $result_update_sql = mysqli_query($this->db, $update_sql);
                if (!$result_update_sql) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sql");
                    $this->response($this->json($error_log), 500);
                    log_info($this->json($error_log));
                }
                log_info($update_sql);
            }
        }
        // log_info("PL : $product_list");
        for ($i = 0; $i < count($product_id); $i++) {
            $select = sprintf("SELECT * FROM `retail_discount_products` WHERE `retail_product_id`='%s' AND `retail_discount_id`='%s' AND `company_id`='%s' order by last_updated_date desc LIMIT 0,1",
                    mysqli_real_escape_string($this->db,$product_id[$i]['retail_product_id']), mysqli_real_escape_string($this->db,$discount_id), mysqli_real_escape_string($this->db,$company_id));
            $result = mysqli_query($this->db, $select);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select");
                $this->response($this->json($error_log), 500);
                log_info($this->json($error_log));
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows > 0) {
                    $row = mysqli_fetch_assoc($result);
                    if($row['deleted_flag']=='Y'){
                        $update_sql = sprintf("UPDATE `retail_discount_products` SET `deleted_flag`='N' WHERE `company_id`='%s' AND `retail_discount_id`='%s' AND `retail_product_id`='%s'",
                                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$discount_id), mysqli_real_escape_string($this->db,$product_id[$i]['retail_product_id']));
                        $result_update_sql = mysqli_query($this->db, $update_sql);
                        if (!$result_update_sql) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sql");
                            $this->response($this->json($error_log), 500);
                            log_info($this->json($error_log));
                        }
                    }
                }else{
                    $insert=sprintf("INSERT INTO `retail_discount_products` (`company_id`,`retail_discount_id`,`retail_product_id`)VALUES('%s','%s','%s')",
                            mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$discount_id), mysqli_real_escape_string($this->db,$product_id[$i]['retail_product_id']));
                     $result2 = mysqli_query($this->db, $insert);
                     if (!$result2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert");
                        $this->response($this->json($error_log), 500);
                        log_info($this->json($error_log));
                    } 
                    
                }
            }
        }
        return true;
                    
    }
    
    public function copyRetailDetails($company_id, $retail_id, $list_type, $retail_template_flag, $level, $call_back, $from){
        if($retail_template_flag == "Y"){
            $comp_id = "6";
        } else {
            $comp_id = $company_id;
        }
        $copy = '';
        $sql_company_code = sprintf("SELECT  `company_code` FROM  `company` WHERE  `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result_company_code = mysqli_query($this->db, $sql_company_code);
        if (!$result_company_code) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_company_code");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $output_value = mysqli_fetch_assoc($result_company_code);
            $company_code = $output_value['company_code'];
        }
        
        $sql = sprintf("SELECT `retail_product_id`, `retail_parent_id`, `retail_product_type`, `retail_product_status`, `retail_product_title`, 
            `retail_product_subtitle`, `retail_product_desc`,`product_tax_rate`,`retail_variant_flag`, `retail_product_inventory`,
            `retail_product_url`, `retail_product_price`, `retail_product_compare_price`, `retail_banner_img_url` 
             FROM `retail_products` WHERE `company_id`='%s' AND 
            (`retail_product_id`='%s' OR `retail_parent_id`='%s') AND `retail_product_status`!='D' order by retail_product_id", $comp_id, $retail_id, $retail_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                
                if(empty(trim($output[0]['retail_product_compare_price']))||is_null($output[0]['retail_product_compare_price'])){
                    $retail_product_compare_price = 'NULL';
                }else{
                    $retail_product_compare_price = $output[0]['retail_product_compare_price'];
                }
                if(empty(trim($output[0]['retail_product_inventory'])) || is_null($output[0]['retail_product_inventory'])){
                    $retail_product_inventory = 'NULL';
                }else{
                    $retail_product_inventory = $output[0]['retail_product_inventory'];
                }
                $product_type = $output[0]['retail_product_type'];
                if($call_back != 1){
                    if($output[0]['retail_product_type']=='P'){
                        if($from=='L'){
                            $status = $output[0]['retail_product_status'];
                        }else{
                            $output[0]['retail_parent_id'] = '';
                            $product_type = 'S';
                            $status = 'S';
                        }
                    }else{
                        $status = 'S';
                    }
                    $copy = " copy";
                }else{
                    $status = $output[0]['retail_product_status'];
                    $copy = "";
                }
                if (empty(trim($output[0]['retail_parent_id']))) {
                    $parent_id = 'null';
                    $sql1 = sprintf("INSERT INTO `retail_products` (`company_id`, `retail_parent_id`, `retail_product_type`, `retail_product_status`, `retail_product_title`, 
                                    `retail_product_subtitle`, `retail_product_desc`,`product_tax_rate`,`retail_variant_flag`, `retail_product_inventory`,
                                    `retail_product_url`, `retail_product_price`, `retail_product_compare_price`, `retail_banner_img_url`,`retail_sort_order`)
                                    VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', %s, '%s', NextVal('retail_product_sort_order_seq'))",
                            mysqli_real_escape_string($this->db, $company_id), 
                            mysqli_real_escape_string($this->db, $parent_id), 
                            mysqli_real_escape_string($this->db, $product_type), 
                            mysqli_real_escape_string($this->db, $status), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_title'] . $copy), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_subtitle']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_desc']), 
                            mysqli_real_escape_string($this->db, $output[0]['product_tax_rate']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_variant_flag']), 
                            mysqli_real_escape_string($this->db, $retail_product_inventory),                             
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_price']), 
                            mysqli_real_escape_string($this->db, $retail_product_compare_price), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_banner_img_url']));
                } else{
                    $parent_id = $output[0]['retail_parent_id'];
                    $sql1 = sprintf("INSERT INTO `retail_products` (`company_id`, `retail_parent_id`, `retail_product_type`, `retail_product_status`, `retail_product_title`, 
                        `retail_product_subtitle`, `retail_product_desc`, `product_tax_rate`, `retail_variant_flag`, `retail_product_inventory`, 
                        `retail_product_url`, `retail_product_price`, `retail_product_compare_price`, `retail_banner_img_url`, 
                        `retail_sort_order`) VALUES (%s, %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', %s, '%s', NextVal('retail_product_sort_order_seq'))",
                            mysqli_real_escape_string($this->db, $company_id), 
                            mysqli_real_escape_string($this->db, $parent_id), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_type']), 
                            mysqli_real_escape_string($this->db, $status), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_title'] . $copy), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_subtitle']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_desc']), 
                            mysqli_real_escape_string($this->db, $output[0]['product_tax_rate']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_variant_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_inventory']),                            
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_price']),
                            mysqli_real_escape_string($this->db, $retail_product_compare_price), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_banner_img_url'])); 
                }
                $result1 = mysqli_query($this->db, $sql1);

                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $last_inserted_retail_id = mysqli_insert_id($this->db);
                    if($output[0]['retail_product_type'] == 'P' || $output[0]['retail_product_type'] == 'S'|| $output[0]['retail_product_type'] == 'C'){
                        $company_code_new = $this->clean($company_code);
                        if (!empty(trim($parent_id)) && $parent_id != 'null') {
                            $url_str = "$parent_id/$last_inserted_retail_id";
                        }else{
                            $url_str = "$last_inserted_retail_id/";
                        }
                        $retail_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/r/?=$company_code_new/$company_id/$url_str/";
                        $retail_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->upload_folder_name/Default/default.png";
                        if($output[0]['retail_banner_img_url'] !== $retail_banner_img_url){
                            $file_name = $company_id . "-" . $last_inserted_retail_id;
                            $cfolder_name = "Company_$company_id";
                            $old = getcwd(); // Save the current directory
                            $dir_name = "../../../$this->upload_folder_name";
                            chdir($dir_name);
                            if (!file_exists($cfolder_name)) {
                                $oldmask = umask(0);
                                mkdir($cfolder_name, 0777, true);
                                umask($oldmask);
                            }
                            chdir($cfolder_name);
                            $folder_name = "Retail";
                            if (!file_exists($folder_name)) {
                                $oldmask = umask(0);
                                mkdir($folder_name, 0777, true);
                                umask($oldmask);
                            }
                            chdir($folder_name);
                            $file = $file_name.'.png' ;
                            $check = copy($output[0]['retail_banner_img_url'],$file);
                            chdir($old);
                            if($check){
                                $retail_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->upload_folder_name/$cfolder_name/$folder_name/$file";
                            }else{
                                $error_log = array('status' => "Failed", "msg" => "Image copy failed", "image_url" => $output[0]['retail_banner_img_url']);
                                log_info($this->json($error_log)); 
                            }
                        }

                        $sql_update_url = sprintf("UPDATE `retail_products` SET `retail_product_url` = '$retail_url',`retail_banner_img_url` = '$retail_banner_img_url' WHERE `retail_product_id` = '%s' ", mysqli_real_escape_string($this->db, $last_inserted_retail_id));
                        $result_update_url = mysqli_query($this->db, $sql_update_url);
                        if (!$result_update_url) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update_url");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    if ($last_inserted_retail_id > 0) {
                        $output3 = [];
                        $num_of_rows3 = 0;
                        $sql_retail_variants = sprintf("SELECT * FROM `retail_variants` WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `deleted_flag`!='D' ORDER BY `variant_sort_order`", mysqli_real_escape_string($this->db, $comp_id), mysqli_real_escape_string($this->db, $output[0]['retail_product_id']));
                        $result3 = mysqli_query($this->db, $sql_retail_variants);
                        if (!$result3) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_retail_variants");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            $num_of_rows3 = mysqli_num_rows($result3);
                            if ($num_of_rows3 > 0) {
                                while ($row3 = mysqli_fetch_assoc($result3)) {
                                    $output3[] = $row3;
                                }
                            }
                        }
                        if ($num_of_rows3 > 0) {
                            for ($j = 0; $j < $num_of_rows3; $j++) {
                                if(empty(trim($output3[$j]['variant_compare_price']))||is_null($output3[$j]['variant_compare_price'])){
                                    $variant_compare_price = 'NULL';
                                }else{
                                    $variant_compare_price = $output3[$j]['variant_compare_price'];
                                }
                                if(empty(trim($output3[$j]['inventory_count'])) || is_null($output3[$j]['inventory_count'])){
                                    $inventory_count = 'NULL';
                                }else{
                                    $inventory_count = $output3[$j]['inventory_count'];
                                }
                
                                $sql5 = sprintf("INSERT INTO `retail_variants` (`retail_product_id`,`company_id`, `retail_variants_name`,`inventory_count`,`variant_price`,`variant_compare_price`,`variant_sort_order`) VALUES ('%s','%s', '%s', %s, '%s', %s, NextVal('retail_variant_sort_order_seq'))",
                                         mysqli_real_escape_string($this->db, $last_inserted_retail_id),mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $output3[$j]['retail_variants_name']), mysqli_real_escape_string($this->db, $inventory_count)
                                        ,mysqli_real_escape_string($this->db, $output3[$j]['variant_price']), mysqli_real_escape_string($this->db, $variant_compare_price));
                                $result5 = mysqli_query($this->db, $sql5);
                                if (!$result5) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql5");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
                    }
                    if ($num_of_rows > 1) {
                        
                        for ($i = 1; $i < count($output); $i++) {
                            if(empty(trim($output[$i]['retail_product_compare_price']))||is_null($output[$i]['retail_product_compare_price'])){
                                $retail_product_compare_price = 'NULL';
                            }else{
                                $retail_product_compare_price = $output[$i]['retail_product_compare_price'];
                            }
                            if(empty(trim($output[$i]['retail_product_inventory'])) || is_null($output[$i]['retail_product_inventory'])){
                                $retail_product_inventory = 'NULL';
                            }else{
                                $retail_product_inventory = $output[$i]['retail_product_inventory'];
                            }
                            
                            $sql2 = sprintf("INSERT INTO `retail_products` (`company_id`, `retail_parent_id`, `retail_product_type`, `retail_product_status`, `retail_product_title`, 
                                    `retail_product_subtitle`, `retail_product_desc`,`product_tax_rate`,`retail_variant_flag`, `retail_product_inventory`,
                                    `retail_product_url`, `retail_product_price`, `retail_product_compare_price`,  `retail_banner_img_url`,`retail_sort_order`)
                                    VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', %s, '%s', NextVal('retail_product_sort_order_seq'))",
                            mysqli_real_escape_string($this->db, $company_id), 
                            mysqli_real_escape_string($this->db, $last_inserted_retail_id), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_type']), 
                            mysqli_real_escape_string($this->db, $status), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_title'] . $copy), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_subtitle']), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_desc']), 
                            mysqli_real_escape_string($this->db, $output[$i]['product_tax_rate']), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_variant_flag']), 
                            mysqli_real_escape_string($this->db, $retail_product_inventory),
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_url']), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_price']), 
                            mysqli_real_escape_string($this->db, $retail_product_compare_price), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_banner_img_url']));
                            $result2 = mysqli_query($this->db, $sql2);

                            if (!$result2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            } else {
                                $last_inserted_retail_id1 = mysqli_insert_id($this->db);
                                if ($output[$i]['retail_product_type'] == 'P' || $output[$i]['retail_product_type'] == 'S'|| $output[$i]['retail_product_type'] == 'C') {
                                    $company_code_new = $this->clean($company_code);
                                    $url_str = "$last_inserted_retail_id/$last_inserted_retail_id1";
                                    $retail_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/r/?=$company_code_new/$company_id/$url_str/";
                                    $retail_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/Default/default.png";
                                    if ($output[$i]['retail_banner_img_url'] !== $retail_banner_img_url) {
                                        $file_name = $company_id . "-" . $last_inserted_retail_id1;
                                        $cfolder_name = "Company_$company_id";
                                        $old = getcwd(); // Save the current directory
                                        $dir_name = "../../../$this->upload_folder_name";
                                        chdir($dir_name);
                                        if (!file_exists($cfolder_name)) {
                                            $oldmask = umask(0);
                                            mkdir($cfolder_name, 0777, true);
                                            umask($oldmask);
                                        }
                                        chdir($cfolder_name);
                                        $folder_name = "Retail";
                                        if (!file_exists($folder_name)) {
                                            $oldmask = umask(0);
                                            mkdir($folder_name, 0777, true);
                                            umask($oldmask);
                                        }
                                        chdir($folder_name);
                                        $file = $file_name . '.png';
                                        $check = copy($output[$i]['retail_banner_img_url'], $file);
                                        chdir($old);
                                        if ($check) {
                                            $retail_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/$cfolder_name/$folder_name/$file";
                                        } else {
                                            $error_log = array('status' => "Failed", "msg" => "Image copy failed", "image_url" => $output[$i]['retail_banner_img_url']);
                                            log_info($this->json($error_log));
                                        }
                                    }

                                    $sql_update_url = sprintf("UPDATE `retail_products` SET `retail_product_url` = '$retail_url',`retail_banner_img_url` = '$retail_banner_img_url' WHERE `retail_product_id` = '%s' ", mysqli_real_escape_string($this->db, $last_inserted_retail_id1));
                                    $result_update_url = mysqli_query($this->db, $sql_update_url);
                                    if (!$result_update_url) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update_url");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    }
                                    
                                    $output3 = [];
                                    $num_of_rows3 = 0;
                                    $sql_retail_variants = sprintf("SELECT * FROM `retail_variants` WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `deleted_flag`!='D' ORDER BY `variant_sort_order`", mysqli_real_escape_string($this->db, $comp_id), mysqli_real_escape_string($this->db, $output[$i]['retail_product_id']));
                                    $result3 = mysqli_query($this->db, $sql_retail_variants);
                                    if (!$result3) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_retail_variants");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    } else {
                                        $num_of_rows3 = mysqli_num_rows($result3);
                                        if ($num_of_rows3 > 0) {
                                            while ($row3 = mysqli_fetch_assoc($result3)) {
                                                $output3[] = $row3;
                                            }
                                            for ($j = 0; $j < $num_of_rows3; $j++) {
                                                if(empty(trim($output3[$j]['variant_compare_price']))||is_null($output3[$j]['variant_compare_price'])){
                                                    $variant_compare_price = 'NULL';
                                                }else{
                                                    $variant_compare_price = $output3[$j]['variant_compare_price'];
                                                }
                                                if(empty(trim($output3[$j]['inventory_count'])) || is_null($output3[$j]['inventory_count'])){
                                                    $inventory_count = 'NULL';
                                                }else{
                                                    $inventory_count = $output3[$j]['inventory_count'];
                                                }

                                                $sql5 = sprintf("INSERT INTO `retail_variants` (`retail_product_id`,`company_id`, `retail_variants_name`,`inventory_count`,`variant_price`,`variant_compare_price`,`variant_sort_order`) VALUES ('%s','%s', '%s', %s, '%s', %s, NextVal('retail_variant_sort_order_seq'))",
                                                         mysqli_real_escape_string($this->db, $last_inserted_retail_id1),mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $output3[$j]['retail_variants_name']), mysqli_real_escape_string($this->db, $inventory_count)
                                                        ,mysqli_real_escape_string($this->db, $output3[$j]['variant_price']), mysqli_real_escape_string($this->db, $variant_compare_price));
                                                $result5 = mysqli_query($this->db, $sql5);
                                                if (!$result5) {
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql5");
                                                    log_info($this->json($error_log));
                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                    $this->response($this->json($error), 200);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if($call_back==0){
                    if($retail_template_flag=='Y'){
                        $log = array('status' => "Success", "msg" => "Retail details copied successfully.");
                        $this->response($this->json($log), 200); // If no records "No Content" status
                    }else{
                        if($level == "L1"){
                            $this->getretailmanagelistdetails($company_id,$list_type,'',0,'');
                        } else {
                            $this->getretailmanagelistdetails($company_id,$list_type,$parent_id,0,'');
                        }
                    }
                }else{
                    $log = array('status' => "Success", "msg" => "Retail details copied successfully.");
                    return $log;
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Retail details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
              
    }
    
    public function updateRetailSortingDetails($company_id, $retail_product_id, $retail_product_sort_order, $parent_id, $list_type){            
        $update_sql = sprintf("UPDATE `retail_products` SET `retail_sort_order`='%s' WHERE `company_id`='%s' AND `retail_product_id`='%s'", 
                mysqli_real_escape_string($this->db, $retail_product_sort_order), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
        $result = mysqli_query($this->db, $update_sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
//        $out = array('status' => "Success", 'msg' => 'Successfully updated');
//                // If success everythig is good send header as "OK" and user details
//        $this->response($this->json($out), 200);
        $this->getretailmanagelistdetails($company_id, $list_type, $parent_id, 0, '');
    }
    
    public function deleteretailproductinDB($company_id, $retail_product_id){
        $retail_type = $delete_flag = "";
        $find_retail_type = sprintf("SELECT `retail_product_type` from `retail_products` where company_id='%s' and `retail_product_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
        $result = mysqli_query($this->db, $find_retail_type);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$find_retail_type");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $row = mysqli_fetch_assoc($result);
            $retail_type = $row['retail_product_type'];
            if ($retail_type === "C") {
                $find_parent = sprintf("SELECT `retail_parent_id` from `retail_products` where company_id='%s' and `retail_parent_id`='%s' and `retail_product_status`!='D'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
                $find_parent_result = mysqli_query($this->db, $find_parent);
                if (!$find_parent_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$find_parent");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows_cat_parent = mysqli_num_rows($find_parent_result);
                    if ($num_of_rows_cat_parent > 0) {
                        $error = array('status' => "Failed", "msg" => "Category can not be deleted because some products were assigned to this category");
                        $this->response($this->json($error), 200);
                    }else{
                       $this->deleteproductorcategory($company_id,$retail_product_id);
                    }
                }
            }else{//standalone or product having parent
                $find_deletable = sprintf("SELECT IF((SELECT count(rof.FULFILLMENT_STATUS) FROM `retail_order_fulfillment` rof INNER JOIN retail_order_details rod ON rod.retail_order_detail_id = rof.retail_order_detail_id where rod.retail_product_id='%s' and (rof.FULFILLMENT_STATUS = 'F' or rof.FULFILLMENT_STATUS = 'U') and rof.company_id = '%s') = 0, 'YES', 'NO') can_be_deleted", mysqli_real_escape_string($this->db, $retail_product_id), mysqli_real_escape_string($this->db, $company_id));
                $find_deletable_result = mysqli_query($this->db, $find_deletable);
                if (!$find_deletable_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$find_deletable");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $row = mysqli_fetch_assoc($find_deletable_result);
                    $delete_flag = $row['can_be_deleted'];
                    if ($delete_flag === "NO") {
                        $error = array('status' => "Failed", "msg" => "Product cannot be deleted as there are existing sales. Refund all sales to delete, or unpublish the product.");
                        $this->response($this->json($error), 200);
                    }else{
                        $this->deleteproductorcategory($company_id,$retail_product_id);
                    }
                }
            }
        }
    }
    
    //delete product/category
    public function deleteproductorcategory($company_id, $retail_product_id){            
        $del_sql = sprintf("UPDATE `retail_products` SET `retail_product_status`='D' WHERE `company_id`='%s' AND `retail_product_id`='%s'",mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
        $result = mysqli_query($this->db, $del_sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$del_sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
        $out = array('status' => "Success", 'msg' => 'Deleted Successfully');
        $this->response($this->json($out), 200);
    }
    
    public function getRetailFulfillmentdetails($company_id, $retail_fulfillment_tab, $draw_table, $length_table, $start, $sorting, $search) {
        $search_term ="";
        $exclude_chars = [',', ' '];
        $out = $output = [];
        $date_format = "'%b %d, %Y'";
        $search_val = mysqli_real_escape_string($this->db, $search['value']);
        
        if (!empty($search_val)) {
            $name_search = str_replace($exclude_chars, '', $search_val);
            $search_term = "AND (CONCAT(rod.retail_product_name , IFNULL(CONCAT(', ',rod.retail_variant_name), '')) LIKE '%$search_val%' OR buyer_name LIKE '%$search_val%' OR DATE_FORMAT(`retail_order_date`,$date_format) LIKE '%$search_val%'
                    OR replace(replace(IFNULL(buyer_name, ''), ' ', ''), ',', '') LIKE '%$name_search%' 
                    OR CONCAT(replace(replace(IFNULL(ro.buyer_first_name, ''), ' ', ''), ',', ''), replace(replace(IFNULL(ro.buyer_last_name, ''), ' ', ''), ',', '')) LIKE '%$name_search%' )";
        }
        if ($retail_fulfillment_tab == 'all') { //ORDERS TAB
            $tab = "";
        } elseif ($retail_fulfillment_tab == 'unfulfilled') {
            $tab = "AND rof.fulfillment_status = 'U'";
        } elseif ($retail_fulfillment_tab == 'fulfilled') {
            $tab = "AND rof.fulfillment_status = 'F'";
        } elseif ($retail_fulfillment_tab == 'cancelled') {
            $tab = "AND rof.fulfillment_status = 'C'";
        }
        $query= sprintf("SELECT DATE_FORMAT(`retail_order_date`,%s) order_date, buyer_name customer_name, ro.retail_order_id checkout_id, rod.retail_product_id, rod.retail_payment_status order_detail_status,
                rof.fulfillment_status as status, rof.retail_order_fulfillment_id fulfillment_id, CONCAT(rod.retail_product_name, IF(rod.retail_variant_name IS NULL OR TRIM(rod.retail_variant_name)='', '', CONCAT(', ',rod.retail_variant_name))) item
                FROM `retail_orders` ro INNER JOIN retail_order_details rod ON rod.retail_order_id = ro.retail_order_id 
                LEFT JOIN retail_order_fulfillment rof ON rof.retail_order_id = rod.retail_order_id AND rof.`retail_order_detail_id`=rod.`retail_order_detail_id`
                WHERE ro.company_id='%s' $tab %s order by `retail_order_date` DESC, ro.`retail_order_id` DESC  LIMIT $start, $length_table ", $date_format, mysqli_real_escape_string($this->db, $company_id), $search_term);
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[]= $row;
                }
            }
            $out['data'] = $output;
            $out['draw'] = $draw_table;
            $this->response($this->json($out), 200);
        }
    }
    
    public function createRetailFulfillmentCSVINDB($company_id, $retail_fulfillment_tab, $search){
        $date_format = "'%b %d, %Y'";  
        $output = [];
        $search_val = mysqli_real_escape_string($this->db, $search);
        
        if (!empty($search_val)) {
            $search_term = "AND (CONCAT(rod.retail_product_name , IF(TRIM(rod.retail_variant_name)='', '', CONCAT(', ',rod.retail_variant_name))) LIKE '%$search_val%' OR buyer_name LIKE '%$search_val%' OR DATE_FORMAT(`retail_order_date`,$date_format) LIKE '%$search_val%')";
        }else{
            $search_term = '';
        }
        if ($retail_fulfillment_tab == 'all') { //ORDERS TAB
            $tab = "";
        } elseif ($retail_fulfillment_tab == 'unfulfilled') {
            $tab = "AND rof.fulfillment_status = 'U'";
        } elseif ($retail_fulfillment_tab == 'fulfilled') {
            $tab = "AND rof.fulfillment_status = 'F'";
        } elseif ($retail_fulfillment_tab == 'cancelled') {
            $tab = "AND rof.fulfillment_status = 'C'";
        }
        $select_query= sprintf("SELECT DATE_FORMAT(`retail_order_date`,%s) order_date, buyer_name customer_name, ro.retail_order_id checkout_id, rod.retail_product_id, 
                IF(rof.fulfillment_status='U', 'Unfulfilled', IF(rof.fulfillment_status='F', 'Fulfilled', 'Cancelled')) as status, rof.retail_order_fulfillment_id fulfillment_id, CONCAT(rod.retail_product_name, IF(TRIM(rod.retail_variant_name)='', '', CONCAT(', ',rod.retail_variant_name))) item,
                ro.buyer_email,ro.buyer_phone,ro.buyer_street,ro.buyer_city,ro.buyer_state,ro.buyer_zip,ro.buyer_country
                FROM `retail_orders` ro INNER JOIN retail_order_details rod ON rod.retail_order_id = ro.retail_order_id 
                LEFT JOIN retail_order_fulfillment rof ON rof.retail_order_id = rod.retail_order_id AND rof.`retail_order_detail_id`=rod.`retail_order_detail_id`
                WHERE ro.company_id='%s' $tab %s order by order_date", $date_format, mysqli_real_escape_string($this->db, $company_id), $search_term);
        $result_select = mysqli_query($this->db, $select_query);
        log_info($select_query);
        if(!$result_select){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result_select);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_select)) {
                    $output[] = $row;
                }
            } else {
                $error = array("status" => "Failed", "msg" => "Details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }

        $header = $body = '';
        $header .= "Date,Customer,Item,Status,Email,Phone Number,Street,City,State,Postal Code,Country".  "\r\n";

        for ($row_index = 0; $row_index < $num_rows; $row_index++) {

            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['order_date']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['customer_name']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['item']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['status']) . '"'. ",";
            
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_email']) .  '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_phone']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_street']) .  '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_city']) .  '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_state']) .  '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_zip']) .  '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_country']) .  '"';
            
            $body = $body .  "\r\n";

        }
        ob_clean();
        ob_start();
        $file = "../../../uploads/dummy1.csv";
        $ifp = fopen($file, "w");
        fwrite($ifp, $header);
        fwrite($ifp, $body);
        fclose($ifp);
        header('Content-Type: application/x-download; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        readfile($file);
        unlink($file);
        exit();        
    } 
    
    public function changefulfillmentstatusINDB($company_id, $checkout_id, $fulfillment_id, $note, $fulfillment_status,$item_name){  
        $prefix_note = '';
        $updatestatus = sprintf("UPDATE `retail_order_fulfillment` SET `fulfillment_status`='%s' WHERE `company_id`='%s' AND `retail_order_fulfillment_id`='%s'", mysqli_real_escape_string($this->db, $fulfillment_status), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $fulfillment_id));
        $result = mysqli_query($this->db, $updatestatus);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatestatus");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
        
        if($fulfillment_status === "F"){
            $prefix_note = "Order Fulfilled - ".$item_name.'. '.$note;
        }else{
            $prefix_note = "Order Unfulfilled - ".$item_name.'. '.$note;
        }
        $updatesnote = sprintf("INSERT INTO `retail_history` (`company_id`, `retail_order_fulfillment_id`, `retail_order_id`, `activity_type`, `activity_text`) VALUES ('%s','%s', %s, '%s', '%s')", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $fulfillment_id), mysqli_real_escape_string($this->db, $checkout_id), 'status', mysqli_real_escape_string($this->db, $prefix_note));
        $result2 = mysqli_query($this->db, $updatesnote);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesnote");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
        $out = array('status' => "Success", 'msg' => "Update Successfully");
        $this->response($this->json($out), 200);
    }
    
    public function getorderdetailsINDB($company_id, $order_id, $callback){
        $output = [];
        $date_format = "'%b %d, %Y'";
        $select_note = sprintf("SELECT rh.retail_history_id note_id, rof.fulfillment_status, rh.retail_order_fulfillment_id, `activity_text` note, DATE_FORMAT(`activity_date_time`,%s) date,
                    CONCAT(rod.retail_product_name, IF(rod.retail_variant_name IS NULL OR TRIM(rod.retail_variant_name)='', '', CONCAT(', ',rod.retail_variant_name))) item, rod.retail_payment_status order_detail_status,
                    IF(rof.fulfillment_status='F', (SELECT IF(MAX(retail_history_id)=rh.retail_history_id, 'Y', 'N') FROM retail_history WHERE rh.`retail_order_id`=retail_order_id AND retail_order_fulfillment_id IS NOT NULL AND retail_order_fulfillment_id=rof.retail_order_fulfillment_id), 'N') show_undo_fulfillment_link
                    FROM retail_history rh LEFT JOIN retail_order_fulfillment rof ON rh.retail_order_fulfillment_id = rof.retail_order_fulfillment_id 
                    LEFT JOIN `retail_order_details` rod ON rof.retail_order_detail_id=rod.retail_order_detail_id 
                    WHERE rh.`company_id`='%s' AND rh.`retail_order_id`='%s' and rh.deleted_flag ='N' ORDER BY rh.retail_history_id DESC",$date_format, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $order_id));
        $result = mysqli_query($this->db, $select_note);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_note");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
            }
            $out = array('status' => "Success", 'msg' => $output);
            if($callback==0){
                $this->response($this->json($out), 200);
            }else{
                return $out;
            }
        }
    }
    
    public function updatefulfillmentnoteINDB($company_id, $checkout_id, $note, $type, $note_id) {
        if($type === "add"){
            $updatesnote = sprintf("INSERT INTO `retail_history` (`company_id`, `retail_order_id`, `activity_type`, `activity_text`) VALUES ('%s','%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $checkout_id), 'note', mysqli_real_escape_string($this->db, $note));
        }else if($type === "update"){
            $updatesnote = sprintf("UPDATE `retail_history` SET activity_text='%s' WHERE company_id='%s' AND retail_history_id='%s'",mysqli_real_escape_string($this->db, $note), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $note_id));
        }else if($type === "delete"){
            $updatesnote = sprintf("UPDATE `retail_history` SET deleted_flag='Y' WHERE company_id='%s' AND retail_history_id='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $note_id));
        }
        $result = mysqli_query($this->db, $updatesnote);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesnote");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
        $this->getorderdetailsINDB($company_id, $checkout_id, 0);
    }
    
    public function getRetailOrderDetails($company_id, $order_id, $call_back){
        $order = '';
        $order_details = $output = $notes = [];
        $out['order'] = '';
        $out['order_details'] = [];
        $select_query = sprintf("SELECT C.`upgrade_status`, C.`wp_currency_symbol`, wp.`currency`, wp.`access_token`, wp.`wp_user_id`, wp.`wp_user_state`, wp.`account_id`, wp.`account_state`, RO.`retail_order_id`, RO.`company_id`, RO.`student_id`,RO.`registration_from`, RO.`payment_method_id`, RO.`stripe_card_name`,RO.`stripe_customer_id`,sa.`stripe_account_id`,sa.`currency` as stripe_currency,C.`company_name` as studio_name,
                    sa.`stripe_refund_flag`,RO.`buyer_name`, RO.`buyer_email`, RO.`buyer_phone`, RO.`buyer_postal_code`, RO.`buyer_country`, RO.`processing_fee_type`, RO.`processing_fee_val`, RO.`retail_products_tax`, 
                    RO.`credit_card_id`, RO.`credit_card_name`, RO.`retail_discount_id`, RO.`retail_discount_code`, RO.`retail_discount_type`, RO.`retail_discount_value`, RO.`retail_discount_applied_type`, 
                    RO.`retail_discount_min_req_flag`, RO.`retail_discount_min_req_value`, RO.`retail_order_discount_amount`, RO.`retail_order_checkout_amount`, RO.`optional_discount_flag`, RO.`payment_method`,
                    ROD.`retail_order_detail_id`, ROD.`retail_product_id`, ROD.`retail_parent_id`, ROD.`retail_variant_id`, ROD.`retail_product_name`, ROD.`retail_variant_name`, ROD.`retail_cost`, 
                    (ROD.`retail_quantity`-ROD.`retail_cancelled_quantity`) retail_quantity, ROD.`retail_payment_amount`, CONCAT(ROD.retail_product_name, IF(ROD.retail_variant_name IS NULL OR TRIM(ROD.retail_variant_name)='', '', CONCAT(', ',ROD.retail_variant_name))) item, `retail_tax_percentage`, `retail_discount`
                    FROM `retail_orders` RO 
                    LEFT JOIN `retail_order_details` ROD ON RO.`company_id` = ROD.`company_id` AND RO.`retail_order_id`=ROD.`retail_order_id`
                    LEFT JOIN `company` C ON RO.`company_id`=C.`company_id`
                    LEFT JOIN `wp_account` wp ON C.`company_id`=wp.`company_id` 
                    LEFT JOIN `stripe_account` sa ON sa.`company_id`=RO.`company_id` 
                    WHERE RO.`company_id`='%s' AND RO.`retail_order_id`='%s' AND `retail_payment_status` IN ('N', 'P', 'S')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $order_id));
        $result_select = mysqli_query($this->db, $select_query);
        if(!$result_select){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result_select);
            $order_note = $this->getorderdetailsINDB($company_id, $order_id, 1);
            $notes = $order_note['msg'];
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result_select)){
                    $order_id = $row['retail_order_id'];
                    $order = array_slice($row, 0, 40);
                    $order_details[] = array_slice($row, 40);                    
                }
                $output['order'] = $order;
                $output['order_details'] = $order_details;
                $output['notes'] = $notes;
                $error = array('status' => "Success", "msg" => $output);
                if ($call_back == 0) {
                    $this->response($this->json($error), 200);
                } else {
                    return $error;
                }
            }else{
                $out['notes'] = $notes;
                $error = array('status' => "Failed", "msg" => $out);
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function retailEditorderDetailsInDb($company_id, $retail_order, $retail_order_details, $callBack) {
        $total_price = $total_refund = $cancelled_taxpercent = $total_tax_value = $cancelled_tax_value = $total_quantity = $total_order_price_wo_tax = 0;
        $output = $source_retail_array = $source_retail_order = $source_retail_order_details = [];
        $specific_discount_amount = $total_retail_tax = $discount_usage_count = $total_process_price = 0;
        $order_updated = 'N';
        $retail_order_id = $retail_order['retail_order_id'];
        $source_retail_array = $this->getRetailOrderDetails($company_id, $retail_order_id, 1);
        $source_retail_order = $source_retail_array['msg']['order'];
        $source_retail_order_details = $source_retail_array['msg']['order_details'];
        $wp_currency_symbol = $source_retail_order['wp_currency_symbol'];
        $p_f = $this->getProcessingFee($company_id, $source_retail_order['upgrade_status']);
        $process_fee_per = $p_f['PP'];
        $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);

        // compare source and updated array
        for ($i = 0; $i < count($retail_order_details); $i++) {
            if (isset($retail_order_details[$i]['retail_order_detail_id']) && empty($retail_order_details[$i]['retail_order_detail_id'])) {
                $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
            if (isset($retail_order_details[$i]['retail_product_id']) && empty($retail_order_details[$i]['retail_product_id'])) {
                $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
            if (!isset($retail_order_details[$i]['updated_quantity'])) {
                $retail_order_details[$i]['updated_quantity'] = $retail_order_details[$i]['retail_quantity'];
            }
            
            $retail_order_details[$i]['cancelled_count'] = $cancelled_count = $source_retail_order_details[$i]['retail_quantity'] - $retail_order_details[$i]['updated_quantity'];
            $retail_order_details[$i]['cancelled_discount'] = 0;
            $retail_order_details[$i]['cancelled_tax_value'] = 0;
            $retail_order_details[$i]['retail_cancelled_payment_amount'] = 0;
            $retail_order_details[$i]['updated_flag'] = 'N';
            $retail_order_details[$i]['refund_amount'] = 0;
            if ($source_retail_order['retail_order_discount_amount'] > 0) {
                $retail_order_details[$i]['discount_valid'] = 'Y';                
                if ($source_retail_order_details[$i]['retail_quantity'] == $retail_order_details[$i]['updated_quantity']) {
                    $retail_order_details[$i]['retail_payment_amount'] = $source_retail_order_details[$i]['retail_payment_amount'];
                }elseif($retail_order_details[$i]['updated_quantity']==0){
                    $retail_order_details[$i]['updated_flag'] = 'Y';
                    $order_updated = 'Y';
                    $retail_order_details[$i]['discount_valid'] = 'N';
                    $retail_order_details[$i]['retail_payment_amount'] = $source_retail_order_details[$i]['retail_cost'] * $retail_order_details[$i]['updated_quantity'];
                    $retail_order_details[$i]['cancelled_discount'] = $source_retail_order_details[$i]['retail_discount'];
                    $retail_order_details[$i]['tax_value'] = 0;
                    $retail_order_details[$i]['cancelled_tax_value'] = ($source_retail_order_details[$i]['retail_tax_percentage'] / 100) * $source_retail_order_details[$i]['retail_quantity'] * $source_retail_order_details[$i]['retail_cost'];
                    $retail_order_details[$i]['retail_cancelled_payment_amount'] = $source_retail_order_details[$i]['retail_quantity'] * $source_retail_order_details[$i]['retail_cost'];
                    $retail_order_details[$i]['retail_discount'] = 0;
                }else{
                    $retail_order_details[$i]['updated_flag'] = 'Y';
                    $order_updated = 'Y';
                    $retail_order_details[$i]['retail_payment_amount'] = $source_retail_order_details[$i]['retail_cost'] * $retail_order_details[$i]['updated_quantity'];
                    $retail_order_details[$i]['retail_cancelled_payment_amount'] = $source_retail_order_details[$i]['retail_cost'] * $cancelled_count;                    
                    if (($retail_order_details[$i]['updated_quantity'] * $source_retail_order_details[$i]['retail_cost']) < $source_retail_order_details[$i]['retail_discount'] && $source_retail_order['retail_discount_type'] == 'V' && $source_retail_order['retail_discount_applied_type'] == 'P') {
                        $retail_order_details[$i]['discount_valid'] = 'N';
                        $specific_discount_amount += $source_retail_order_details[$i]['retail_discount'];
                        $retail_order_details[$i]['cancelled_discount'] = $source_retail_order_details[$i]['retail_discount'];
                    }
                }
                $total_order_price_wo_tax += $retail_order_details[$i]['retail_payment_amount'];
                $total_quantity += $retail_order_details[$i]['updated_quantity'];
            } else {
                $retail_order_details[$i]['discount_valid'] = 'N';
                if ($source_retail_order_details[$i]['retail_quantity'] == $retail_order_details[$i]['updated_quantity']) {
                    $retail_order_details[$i]['retail_payment_amount'] = $source_retail_order_details[$i]['retail_payment_amount'];
                    $taxpercent = ($source_retail_order_details[$i]['retail_tax_percentage'] / 100) * $source_retail_order_details[$i]['retail_quantity'] * $source_retail_order_details[$i]['retail_cost'];
                    $retail_order_details[$i]['tax_value'] = $taxpercent;
                    $cancelled_taxpercent = 0;
                } elseif($retail_order_details[$i]['updated_quantity']==0) {
                    $retail_order_details[$i]['updated_flag'] = 'Y';
                    $order_updated = 'Y';
                    $taxpercent = 0;
                    $retail_order_details[$i]['tax_value'] = 0;
                    $retail_order_details[$i]['retail_payment_amount'] = $source_retail_order_details[$i]['retail_cost'] * $retail_order_details[$i]['updated_quantity'];
                    $cancelled_taxpercent = ($source_retail_order_details[$i]['retail_tax_percentage'] / 100) * $source_retail_order_details[$i]['retail_quantity'] * $source_retail_order_details[$i]['retail_cost'];
                    $retail_order_details[$i]['cancelled_tax_value'] = $cancelled_taxpercent;
                    $retail_order_details[$i]['retail_cancelled_payment_amount'] = $source_retail_order_details[$i]['retail_quantity'] * $source_retail_order_details[$i]['retail_cost'];
                    $retail_order_details[$i]['refund_amount'] = ($source_retail_order_details[$i]['retail_cost'] * $source_retail_order_details[$i]['retail_quantity']) + $cancelled_taxpercent;
                    $total_process_price += $retail_order_details[$i]['retail_cancelled_payment_amount']+$cancelled_taxpercent;
                } else {
                    $retail_order_details[$i]['updated_flag'] = 'Y';
                    $order_updated = 'Y';
                    $taxpercent = ($source_retail_order_details[$i]['retail_tax_percentage'] / 100) * $retail_order_details[$i]['updated_quantity'] * $source_retail_order_details[$i]['retail_cost'];
                    $retail_order_details[$i]['tax_value'] = $taxpercent;
                    $cancelled_taxpercent = ($source_retail_order_details[$i]['retail_tax_percentage'] / 100) * $cancelled_count * $source_retail_order_details[$i]['retail_cost'];
                    $retail_order_details[$i]['cancelled_tax_value'] = $cancelled_taxpercent;
                    $retail_order_details[$i]['retail_payment_amount'] = $source_retail_order_details[$i]['retail_cost'] * $retail_order_details[$i]['updated_quantity'];
                    $retail_order_details[$i]['retail_cancelled_payment_amount'] = $source_retail_order_details[$i]['retail_cost'] * $cancelled_count;
                    $retail_order_details[$i]['refund_amount'] = ($source_retail_order_details[$i]['retail_cost'] * $cancelled_count) + $cancelled_taxpercent;
                    $total_process_price += $retail_order_details[$i]['retail_payment_amount'] + $taxpercent;
                }
                $total_order_price_wo_tax += $retail_order_details[$i]['retail_payment_amount'];
                $total_price += $retail_order_details[$i]['retail_payment_amount'] + $taxpercent;
                $total_quantity += $retail_order_details[$i]['updated_quantity'];
                $total_tax_value += $taxpercent;
                $cancelled_tax_value += $cancelled_taxpercent;
                $total_refund += $retail_order_details[$i]['refund_amount'];
            }
        }
        
        $retail_order['discount_valid'] = 'Y';
        if($source_retail_order['retail_discount_min_req_flag'] == 'Q' && $total_quantity < $source_retail_order['retail_discount_min_req_value']){
            $retail_order['discount_valid'] = 'N';
        }elseif ($source_retail_order['retail_discount_min_req_flag'] == 'V' && $total_order_price_wo_tax < $source_retail_order['retail_discount_min_req_value']) {
            $retail_order['discount_valid'] = 'N';
        }elseif ($specific_discount_amount > 0) {
            $retail_order['discount_valid'] = 'N';
        }elseif ($source_retail_order['retail_discount_value'] > $total_order_price_wo_tax && $source_retail_order['retail_discount_applied_type'] == 'O'){
            $retail_order['discount_valid'] = 'N';
        }
        $retail_order['order_updated_flag'] = $order_updated;
        $total_discounts = 0;
        
        if ($source_retail_order['retail_order_discount_amount'] > 0) {
            if ($source_retail_order['retail_discount_applied_type'] == 'O' && $retail_order['discount_valid'] == 'N') {
                $discount_usage_count = 1;
            }
            for ($k = 0; $k < count($retail_order_details); $k++) {
                $cancelled_count = $retail_order_details[$k]['cancelled_count'];
                if ($source_retail_order_details[$k]['retail_discount'] > 0) {
                    if ($source_retail_order['retail_discount_applied_type'] == 'O' && $source_retail_order['retail_discount_type'] == 'V') {
                        if ($total_order_price_wo_tax != 0) {
                            $disc_percent_value = ($retail_order_details[$k]['retail_payment_amount'] / $total_order_price_wo_tax ) * $source_retail_order['retail_discount_value'];
                            $retail_order_details[$k]['cancelled_discount'] = $source_retail_order_details[$k]['retail_discount']-$disc_percent_value;
                        } else {
                            $retail_order_details[$k]['cancelled_discount'] = $source_retail_order_details[$k]['retail_discount'];
                            $disc_percent_value = 0.00;
                        }
                        $retail_order_details[$k]['retail_discount'] = $disc_percent_value;
                    }elseif ($source_retail_order['retail_discount_type'] == 'P') {
                        $retail_order_details[$k]['retail_discount'] = ($source_retail_order_details[$k]['retail_cost'] * $retail_order_details[$k]['updated_quantity']) * ($source_retail_order['retail_discount_value'] / 100);
                        $retail_order_details[$k]['cancelled_discount'] = ($source_retail_order_details[$k]['retail_cost'] * $retail_order_details[$k]['cancelled_count']) * ($source_retail_order['retail_discount_value'] / 100);
                    } elseif ($source_retail_order['retail_discount_type'] == 'V') {
                        if($retail_order_details[$k]['discount_valid'] != 'N'){
                            $retail_order_details[$k]['retail_discount'] = $source_retail_order_details[$k]['retail_discount'];
                        }
                    }
                    $total_discounts += $retail_order_details[$k]['retail_discount'];

                    if ($retail_order_details[$k]['discount_valid'] == 'N' || $retail_order['discount_valid'] == 'N') {
                        if ($source_retail_order['retail_discount_applied_type'] == 'P' && $retail_order_details[$k]['discount_valid'] == 'N') {
                            $discount_usage_count++;
                        }
                        $source_tax = ($source_retail_order_details[$k]['retail_tax_percentage'] / 100) * (($retail_order_details[$k]['retail_quantity'] * $source_retail_order_details[$k]['retail_cost']) - $source_retail_order_details[$k]['retail_discount']);
                        $taxpercent = ($source_retail_order_details[$k]['retail_tax_percentage'] / 100) * (($retail_order_details[$k]['updated_quantity'] * $source_retail_order_details[$k]['retail_cost']));
                        $cancelled_taxpercent = ($source_retail_order_details[$k]['retail_tax_percentage'] / 100) * ($retail_order_details[$k]['retail_cancelled_payment_amount'] - $retail_order_details[$k]['cancelled_discount']);
                        $retail_order_details[$k]['discount_valid'] = 'N';
                        $retail_order_details[$k]['tax_value'] = $taxpercent;
                        $retail_order_details[$k]['cancelled_tax_value'] = $cancelled_taxpercent;
                        if ($source_retail_order_details[$k]['retail_quantity'] != $retail_order_details[$k]['updated_quantity']) {
                            if($retail_order_details[$k]['updated_quantity']==0){
                                $total_process_price += $retail_order_details[$k]['retail_cancelled_payment_amount']-$retail_order_details[$k]['cancelled_discount']+$cancelled_taxpercent;
                            }else{
                                $total_process_price += $retail_order_details[$k]['retail_payment_amount']  - $retail_order_details[$k]['retail_discount'] + $taxpercent;
                            }
                        }else{
                            $retail_order_details[$k]['cancelled_discount'] = $retail_order_details[$k]['retail_discount'];
                            $cancelled_taxpercent = $retail_order_details[$k]['cancelled_tax_value'] = $source_tax - $taxpercent;
                        }
                        $retail_order_details[$k]['refund_amount'] = $retail_order_details[$k]['retail_cancelled_payment_amount'] - $retail_order_details[$k]['cancelled_discount'] + $retail_order_details[$k]['cancelled_tax_value'];
                        $total_price += $retail_order_details[$k]['retail_payment_amount'] + $taxpercent;
                    } else {
                        $taxpercent = ($source_retail_order_details[$k]['retail_tax_percentage'] / 100) * ($retail_order_details[$k]['retail_payment_amount'] - $retail_order_details[$k]['retail_discount']);
                        $cancelled_taxpercent = ($source_retail_order_details[$k]['retail_tax_percentage'] / 100) * ($retail_order_details[$k]['retail_cancelled_payment_amount'] - $retail_order_details[$k]['cancelled_discount']);
                        $retail_order_details[$k]['tax_value'] = $taxpercent;
                        $retail_order_details[$k]['cancelled_tax_value'] = $cancelled_taxpercent;
                        $retail_order_details[$k]['refund_amount'] = $retail_order_details[$k]['retail_cancelled_payment_amount'] - $retail_order_details[$k]['cancelled_discount'] + $cancelled_taxpercent;
                        if ($source_retail_order_details[$k]['retail_quantity'] != $retail_order_details[$k]['updated_quantity']) {
                            if($retail_order_details[$k]['updated_quantity']==0){
                                $total_process_price += $retail_order_details[$k]['retail_cancelled_payment_amount'] - $retail_order_details[$k]['cancelled_discount'] + $cancelled_taxpercent;
                            }else{
                                $total_process_price += $retail_order_details[$k]['retail_payment_amount'] - $retail_order_details[$k]['retail_discount'] + $taxpercent;
                            }
                        }
                        $total_price += $retail_order_details[$k]['retail_payment_amount'] - $retail_order_details[$k]['retail_discount'] + $taxpercent;
                    }
                    if ($retail_order_details[$k]['discount_valid'] == 'N' || $retail_order['discount_valid'] == 'N') {
                        $retail_order_details[$k]['retail_discount'] = 0;
                    }
                } else {
                    $taxpercent = ($source_retail_order_details[$k]['retail_tax_percentage'] / 100) * $retail_order_details[$k]['retail_payment_amount'];
                    $cancelled_taxpercent = ($source_retail_order_details[$k]['retail_tax_percentage'] / 100) * $retail_order_details[$k]['retail_cancelled_payment_amount'];
                    $retail_order_details[$k]['tax_value'] = $taxpercent;
                    $retail_order_details[$k]['cancelled_tax_value'] = $cancelled_taxpercent;
                    $retail_order_details[$k]['refund_amount'] = $retail_order_details[$k]['retail_cancelled_payment_amount'] + $cancelled_taxpercent;
                    if ($source_retail_order_details[$k]['retail_quantity'] != $retail_order_details[$k]['updated_quantity']) {
                        if($retail_order_details[$k]['updated_quantity']==0){
                            $total_process_price += $retail_order_details[$k]['retail_cancelled_payment_amount'] + $cancelled_taxpercent;
                        }else{
                            $total_process_price += $retail_order_details[$k]['retail_payment_amount'] + $taxpercent;
                        }
                    }
                    $total_price += $retail_order_details[$k]['retail_payment_amount'] + $taxpercent;
                }
                $total_tax_value += $taxpercent;
                $cancelled_tax_value += $cancelled_taxpercent;
                $total_refund += $retail_order_details[$k]['refund_amount'];
            }
        }

        if ($total_price <= 0 || $source_retail_order['payment_method'] == 'CA' || $source_retail_order['payment_method'] == 'CH') {
            $retail_order['processing_fee_val'] = 0;
        } else {
            $retail_order['processing_fee_val'] = round($this->calculateProcessingFee($total_price, $source_retail_order['processing_fee_type'], $process_fee_per, $process_fee_val), 2);
        }
        $retail_order['retail_order_checkout_amount'] = $total_price;
        $retail_order['retail_order_total_process_price'] = $total_process_price;
        if ($source_retail_order['processing_fee_type'] == 2) {
            $retail_order['retail_order_checkout_amount'] = $total_price + $retail_order['processing_fee_val'];
        }
        $retail_order['retail_order_checkout_amount'] = round($retail_order['retail_order_checkout_amount'],2);
//        log_info("Total_price : ". $total_price);
//        log_info("SRC : ". json_encode($source_retail_order));
//        log_info("Ret Ord : ". json_encode($retail_order));        
//        log_info("Ret Ord Det: ". json_encode($retail_order_details));
        if ($source_retail_order['retail_order_checkout_amount'] == $retail_order['retail_order_checkout_amount']) {
            $retail_order['processing_fee_val'] = $source_retail_order['processing_fee_val'];
            $retail_order['retail_products_tax'] = $source_retail_order['retail_products_tax'];
            $retail_order['retail_order_checkout_amount'] = $source_retail_order['retail_order_checkout_amount'];
            $retail_order['retail_order_discount_amount'] = $source_retail_order['retail_order_discount_amount'];
            $retail_order['retail_total_refund'] = 0;
            $retail_order['retail_total_refund2'] = 0;
            $retail_order['cancelled_products_tax'] = 0;
            $retail_order['cancelled_products_tax2'] = 0;
            $retail_order['discount_usage_count'] = 0;
        } else {
            if ($retail_order['discount_valid'] == 'Y') {
                $retail_order['retail_total_refund2'] = $total_refund;
                $retail_order['retail_products_tax'] = round($total_tax_value, 2);
                $retail_order['cancelled_products_tax'] = round($source_retail_order['retail_products_tax'] - $total_tax_value, 2);
                $retail_order['cancelled_products_tax2'] = $cancelled_tax_value;
                $retail_order['retail_order_discount_amount'] = $total_discounts;
                $retail_order['discount_usage_count'] = $discount_usage_count;
            } else {
                $retail_order['retail_total_refund2'] = $total_refund;
                $retail_order['retail_products_tax'] = round($total_tax_value, 2);
                $retail_order['cancelled_products_tax'] = round($source_retail_order['retail_products_tax'] - $total_tax_value, 2);
                $retail_order['cancelled_products_tax2'] = $cancelled_tax_value;
                $retail_order['retail_order_discount_amount'] = 0;
                $retail_order['discount_usage_count'] = $discount_usage_count;
            }            
        }
        
        $old_retail_order_checkout_amount = $source_retail_order['retail_order_checkout_amount'];
        $new_retail_order_checkout_amount = $retail_order['retail_order_checkout_amount'];
        
        $cancellation_amount = $old_retail_order_checkout_amount-$new_retail_order_checkout_amount;
        $note_str = '';
            
        if($cancellation_amount==0){
            $retail_order['process_type'] = '';
            $retail_order['process_amount'] = 0;
            $retail_order['process_fee'] = 0;
        }elseif($cancellation_amount>0){
            $retail_order['process_type'] = 'R';
            $retail_order['retail_total_refund'] = $cancellation_amount;
            $retail_order['process_amount'] = $cancellation_amount;
            $retail_order['process_fee'] = $source_retail_order['processing_fee_val'] - $retail_order['processing_fee_val'];
            $note_str = " Refund $wp_currency_symbol$cancellation_amount";
        }else{
            $retail_order['process_type'] = 'N';
            $retail_order['process_amount'] = abs($cancellation_amount);
            if ($source_retail_order['payment_method'] == 'CA' || $source_retail_order['payment_method'] == 'CH') {
                $retail_order['process_fee'] = 0;
            } else {
                $retail_order['process_fee'] = round($this->calculateProcessingFee(-1 * $cancellation_amount, $source_retail_order['processing_fee_type'], $process_fee_per, $process_fee_val), 2);
            }
            $note_str = " Charge $wp_currency_symbol".abs($cancellation_amount);
        }

        $output['order'] = $retail_order;
        $output['order_details'] = $retail_order_details;
        $cancelled_string = "";
        $str_init = 0;
        for($quantity = 0; $quantity < count($retail_order_details); $quantity++){
            $cancelled_quantity = 0;
            if((((int)$retail_order_details[$quantity]['retail_quantity']) - ((int)($retail_order_details[$quantity]['updated_quantity']))) > 0){
                if($str_init != 0){
                    $cancelled_string .= ', ';
                }
                if($str_init == 0){
                    $str_init++;
                    $cancelled_string = 'Cancelled ';
                }
                $cancelled_quantity = (string)((int)$retail_order_details[$quantity]['retail_quantity']) - ((int)($retail_order_details[$quantity]['updated_quantity']));
                $cancelled_string .= $cancelled_quantity.' '.$retail_order_details[$quantity]['item'];
            }
            if  (($quantity == count($retail_order_details) - 1)) {
                $cancelled_string .= '.' . $note_str;
            }
        }
        $output['note'] = $cancelled_string;
        if ($callBack == 0) {
            $msg = array('status' => "Success", "msg" => $output);
            $this->response($this->json($msg), 200);
        } else {
            return $output;
        }
    }

    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }
    
    private function calculateProcessingFee($amount, $fee_type, $process_fee_per, $process_fee_val) {
        $p1_fee = ($amount * ($process_fee_per / 100)) + $process_fee_val;
        $p2_fee = ($amount + $p1_fee) * ($process_fee_per / 100) + $process_fee_val;
        if ($fee_type === 2 || $fee_type === '2') {
            $p_fee = $p2_fee;
        } else {
            $p_fee = $p1_fee;
        }
        return $p_fee;
    }
    
    
    public function refundRetailOrdersInDb($company_id, $order, $order_details) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $curr_date = gmdate("Y-m-d");
        
        $out = $this->retailEditorderDetailsInDb($company_id, $order, $order_details, 1);
        $retail_order = $out['order'];
        $retail_order_details = $out['order_details'];
        $retail_order_id = $retail_order['retail_order_id'];
        $cancelled_str = $out['note'];
        
        $source_retail_array = $this->getRetailOrderDetails($company_id, $retail_order_id, 1);
        $source_retail_order = $source_retail_array['msg']['order'];
        $source_retail_order_details = $source_retail_array['msg']['order_details'];
        $access_token = $source_retail_order['access_token'];
        $account_id = $source_retail_order['account_id'];        
        $currency = $source_retail_order['currency'];
        $user_state = $source_retail_order['wp_user_state'];
        $acc_state = $source_retail_order['account_state'];
        $buyer_email = $source_retail_order['buyer_email'];
        $buyer_phone = $source_retail_order['buyer_phone'];
        $buyer_postal_code = $source_retail_order['buyer_postal_code'];
        $country = $source_retail_order['buyer_country'];
        $credit_card_id = $source_retail_order['credit_card_id'];
        $credit_card_name = $source_retail_order['credit_card_name'];
        $retail_product_title = $source_retail_order_details[0]['item'];
        $payment_from = $source_retail_order['registration_from'];
        $buyer_name = $source_retail_order['buyer_name'];
        if($payment_from == 'S'){ // If stripe then redirect to other function
            $this->stripeRefundRetailInDB($company_id, $order, $order_details);
        }else{ // If wepay then continue
        
            $gmt_date=gmdate(DATE_RFC822);
            $sns_msg=array("buyer_email"=>$buyer_email,"membership_title"=>"retail order cancellation process","gmt_date"=>$gmt_date);

            if($user_state=='deleted'){
                $log = array("status" => "Failed", "msg" => "You may delete your account yourselves, or the account may be deleted by WePay customer support.");
                $this->response($this->json($log),200);
            }

            if($acc_state=='deleted'){
                $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                $this->response($this->json($log),200);
            }elseif($acc_state=='disabled'){
                $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                $this->response($this->json($log),200);
            }

            if(!empty($access_token) && !empty($account_id)){
                if (!function_exists('getallheaders')){
                    if(!function_exists ('apache_request_headers')){
                        $user_agent = $_SERVER['HTTP_USER_AGENT'];
                    }else{
                        $headers = apache_request_headers();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                }else{
                    $headers = getallheaders();
                    $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                }
                $temp_password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $temp_password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
            }
            $to_update_due_for_charge = [];

            $new_payment_amount = 0;

            $to_refund = $to_charge = $refund_amount = $refund_fee = $charge_amount = $charge_fee = $retail_total_processing_fee = 0;
            $process_type = $retail_order['process_type'];
            if($process_type=='R'){
                $to_refund = 1;
                $cancellation_amount = $refund_amount = $retail_order['process_amount'];
                $retail_total_processing_fee = $refund_fee = $retail_order['process_fee'];
                for ($i = 0; $i < count($retail_order_details); $i++) {
    //                $to_update_due_for_charge[$i]['handle_amount'] = 0;
    //                if($retail_order_details[$i]['updated_quantity']!=$retail_order_details[$i]['retail_quantity'] && $retail_order_details[$i]['retail_payment_amount']>0){    //check for additional charge payment handling
    //                    $to_update_due_for_charge[$i]['handle_amount'] = $retail_order_details[$i]['refund_amount'];
    //                }elseif($retail_order_details[$i]['updated_quantity']==0){
    //                    $to_update_due_for_charge[$i]['handle_amount'] = $retail_order['process_amount'] * ((($retail_order_details[$i]['retail_cost']*$retail_order_details[$i]['retail_quantity'])-$retail_order_details[$i]['cancelled_discount']+$retail_order_details[$i]['cancelled_tax_value'])/$retail_order['retail_order_total_process_price']);
                        $to_update_due_for_charge[$i]['handle_amount'] = $retail_order_details[$i]['refund_amount'];
    //                }
                    $new_payment_amount += $to_update_due_for_charge[$i]['handle_amount'];
                }
            }elseif($process_type=='N'){
                $to_charge = 1;
                $cancellation_amount = $charge_amount = $retail_order['process_amount'];
                $retail_total_processing_fee = $charge_fee = $retail_order['process_fee'];
                for ($i = 0; $i < count($retail_order_details); $i++) {
                    $to_update_due_for_charge[$i]['handle_amount']=0;
                    if($retail_order_details[$i]['updated_quantity']!=0){
                        $to_update_due_for_charge[$i]['handle_amount'] = $retail_order['process_amount'] * ((($retail_order_details[$i]['retail_cost']*$retail_order_details[$i]['updated_quantity'])-$retail_order_details[$i]['retail_discount']+$retail_order_details[$i]['tax_value'])/($retail_order['retail_order_checkout_amount']-$retail_order['process_amount']));
                    }
                }
                if($source_retail_order['processing_fee_type'] == 2){
                    $charge_amount += $retail_order['process_fee'];
                }
            }

            $payments = [];
            $get_payment = sprintf("SELECT credit_method,`retail_payment_id`, `cc_id`, `cc_name`, `checkout_id`, `checkout_status`, (`payment_amount`-`refunded_amount`) payment_amount, `payment_date`, `actual_paid_date`, `payment_status`, `processing_fee`, `refunded_amount`, `refunded_date`, `last_updt_dt`
                        FROM `retail_payment` WHERE `retail_order_id`='%s' AND `payment_status` IN ('S', 'PR', 'M', 'MP') ORDER BY `retail_payment_id` DESC", mysqli_real_escape_string($this->db, $retail_order_id));
            $result_get_payment = mysqli_query($this->db, $get_payment);
            if(!$result_get_payment){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_payment");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                if(mysqli_num_rows($result_get_payment)>0){
                    while($row_payment = mysqli_fetch_assoc($result_get_payment)){
                        $payments[] = $row_payment;
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => "Payment details mismatched with order details.");
                    $this->response($this->json($error), 200);
                }
            }
            $stop_wepay = false;
            $credit_method = 'CC';
            if($payments[0]['credit_method'] == "CH" || $payments[0]['credit_method'] == "CA"){
                $credit_method = $payments[0]['credit_method'];
                $stop_wepay = true;
            }
            if($to_refund==1){
                $retail_refund_array = [];
                $process_amt = $cancellation_amount;
                for($j=0; $j<count($payments); $j++){
                    $payment_id = $payments[$j]['retail_payment_id'];
                    $checkout_id = $payments[$j]['checkout_id'];
                    $checkout_status = $payments[$j]['checkout_status'];
                    $payment_amount = $payments[$j]['payment_amount'];
                    $payment_fee = $payments[$j]['processing_fee'];
                    $last_updt_dt = $payments[$j]['last_updt_dt'];
                    //$type => P - Partial, F - Full
                    if($payment_amount>$process_amt){
                        $type = 'P';
                        $retail_refund_array[] = array("credit_method"=>$payments[$j]['credit_method'],"type"=>$type, "payment_id"=>$payment_id, "cc_id"=>$payments[$j]['cc_id'], "cc_name"=>$payments[$j]['cc_name'], "checkout_id"=>$checkout_id, "checkout_status"=>$checkout_status, "refund_amount"=>$process_amt, "refund_fee"=>$refund_fee, "last_updt_dt"=>$last_updt_dt);
                        break;
                    }else{
                        $type = 'F';
                        $retail_refund_array[] = array("credit_method"=>$payments[$j]['credit_method'],"type"=>$type, "payment_id"=>$payment_id, "cc_id"=>$payments[$j]['cc_id'], "cc_name"=>$payments[$j]['cc_name'], "checkout_id"=>$checkout_id, "checkout_status"=>$checkout_status, "refund_amount"=>$payment_amount, "refund_fee"=>$payment_fee, "last_updt_dt"=>$last_updt_dt);
                        $process_amt -= $payment_amount;
                        $refund_fee -= $payment_fee;
                    }
                }
                for($k=0;$k<count($retail_refund_array);$k++){
                    $payment_id = $retail_refund_array[$k]['payment_id'];
                    $checkout_status = $retail_refund_array[$k]['checkout_status'];
                    $checkout_id = $retail_refund_array[$k]['checkout_id'];
                    $refund_amount = $retail_refund_array[$k]['refund_amount'];
                    $refund_fee = $retail_refund_array[$k]['refund_fee'];
                    $type = $retail_refund_array[$k]['type'];
                    $last_updt_date = $retail_refund_array[$k]['last_updt_dt'];
                    if(!$stop_wepay){
                    if($checkout_status=='new' || $checkout_status=='authorized' || $checkout_status=='reserved'){
                        $error = array('status' => "Failed", "msg" => "Some payment(s) are not yet captured.");
                        $this->response($this->json($error), 200);
                    }elseif($checkout_status=='released' || $checkout_status=='captured'){
                        $sns_msg['call'] = "Checkout";
                        $sns_msg['type'] = "Checkout Refund";
                        $call_method = "checkout/refund";
                        $json = array("checkout_id"=>$checkout_id,"amount"=>$refund_amount,"app_fee"=>$refund_fee,"refund_reason"=>"Participant ");
                    }else{
                        $error = array("status" => "Failed", "msg" => "Participant payment status is in $checkout_status status.");
                        $this->response($this->json($error),200);
                    }
                    $postData = json_encode($json);
                    $response = $this->wp->accessWepayApi($call_method,'POST',$postData,$token,$user_agent,$sns_msg);
                    $this->wp->log_info("wepay_checkout_refund: ".$response['status']);
                    }
                    if((isset($response['status']) && $response['status']=="Success") || $stop_wepay){
                        if($type == 'F') {
                            if ($stop_wepay) {
                                $refund_string = 'MF';
                                $refund_string1 = 'MR';
                                $refund_fee_string = '0';
                            } else {
                                $refund_string = 'FR';
                                $refund_string1 = 'R';
                                $refund_fee_string = '`processing_fee`';
                            }
                            $insert_new_refunded_query = sprintf("INSERT INTO `retail_payment`( `retail_order_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `refunded_date`, `cc_id`, `cc_name`, `retail_tax_payment`, `created_dt`, `last_updt_dt`,`credit_method`) 
                                    SELECT `retail_order_id`, `checkout_id`, `checkout_status`, `payment_amount`, $refund_fee_string, `payment_date`, '$refund_string', CURDATE(), `cc_id`, `cc_name`, `retail_tax_payment`, `created_dt`, '$last_updt_date','$credit_method' FROM `retail_payment` WHERE `retail_payment_id`='%s'", $payment_id);

                            $update_query2 = sprintf("UPDATE `retail_payment` SET `payment_amount` = '%s', `payment_status`='$refund_string1', `refunded_amount`=0, `processing_fee`='%s', `retail_tax_payment`='%s' WHERE `retail_payment_id`='%s'", 
                                mysqli_real_escape_string($this->db, $refund_amount), mysqli_real_escape_string($this->db, $refund_fee), mysqli_real_escape_string($this->db, $retail_order['cancelled_products_tax']), $payment_id);
                        }else{
                            if ($stop_wepay) {
                                $refund_string = 'MP';
                                $refund_string1 = 'MR';
                            } else {
                                $refund_string = 'PR';
                                $refund_string1 = 'R';
                            }
                            $insert_new_refunded_query = sprintf("INSERT INTO `retail_payment`( `retail_order_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `retail_tax_payment`, `created_dt`, `last_updt_dt`,`credit_method`) 
                                    SELECT `retail_order_id`, `checkout_id`, `checkout_status`, $refund_amount, $refund_fee, CURDATE(), '$refund_string1', `cc_id`, `cc_name`, '%s', `created_dt`, NOW(),'$credit_method' FROM `retail_payment` WHERE `retail_payment_id`='%s'", mysqli_real_escape_string($this->db, $retail_order['cancelled_products_tax']), $payment_id);                    

                            $update_query2 = sprintf("UPDATE `retail_payment` SET `payment_status`='$refund_string', `refunded_amount`=`refunded_amount`+'%s', `processing_fee`=`processing_fee`-'%s', `refunded_date`=CURDATE() WHERE `retail_payment_id`='%s'", 
                                mysqli_real_escape_string($this->db, $refund_amount), mysqli_real_escape_string($this->db, $refund_fee), $payment_id);
                        }
                        $result_insert_new_refunded_query = mysqli_query($this->db, $insert_new_refunded_query);
                        if (!$result_insert_new_refunded_query) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_new_refunded_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            $result_update_query2 = mysqli_query($this->db, $update_query2);
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query2");
                            if(!$result_update_query2){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                    }else{
                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
                        $this->response($this->json($log),200);
                    }
                }
            }elseif($to_charge==1){
                if(!$stop_wepay){
                $rand = mt_rand(1,99999);
                $ip = $this->getRealIpAddr();
                $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout?for=retail";
                $unique_id = $company_id."_".$retail_order_id."_".$ip."_".$rand."_".time();
                $date = date("Y-m-d_H:i:s");
                $ref_id = $company_id."_".$retail_order_id."_".$date;
                $fee_payer = "payee_from_app";
                $paid_fee = $charge_fee;
                $w_paid_amount = $charge_amount;

                $time = time();
                $json2 = array("account_id"=>"$account_id","short_description"=>"$retail_product_title","type"=>"goods","amount"=>"$w_paid_amount","currency"=>"$currency",
                    "fee"=>array("app_fee"=>$paid_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                    "email_message"=>array("to_payee"=>"Payment has been added for $retail_product_title","to_payer"=>"Payment has been made for $retail_product_title"),
                    "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$credit_card_id)),"callback_uri"=>"$checkout_callback_url",
                    "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                        array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                        array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code","country"=>"$country")))),
                    "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                        "properties"=>array("itemized_receipt"=>array(array("description"=>"$retail_product_title", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                $postData2 = json_encode($json2);
                $sns_msg['call'] = "Checkout";
                $sns_msg['type'] = "Checkout Creation";
                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg);
                $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                if($response2['status']=="Success"){
                    $res2 = $response2['msg'];
                    $checkout_id = $res2['checkout_id'];
                    $checkout_state = $res2['state'];
                    $gross = $res2['gross'];
                }else{
                    if($response2['msg']['error_code']==1008){
                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg);
                        $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $gross = $res2['gross'];
                        }else{
                            if($response2['msg']['error_code']==1008){
                                $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                $this->response($this->json($error),200);
                            }else{
                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error),200);
                            }
                        }
                    }else{
                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                        $this->response($this->json($error),200);
                    }
                }
                $tax_str = 0;
                if($retail_order['cancelled_products_tax']!=0){
                    $tax_str = abs($retail_order['cancelled_products_tax']);
                }
                $payment_query = sprintf("INSERT INTO `retail_payment`(`retail_order_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `retail_tax_payment`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $charge_fee),
                        mysqli_real_escape_string($this->db, $charge_amount), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $credit_card_id), mysqli_real_escape_string($this->db, $credit_card_name), $tax_str);
                }else{
                    $tax_str = 0;
                    if($retail_order['cancelled_products_tax']!=0){
                    $tax_str = abs($retail_order['cancelled_products_tax']);
                    }
                    $payment_query = sprintf("INSERT INTO `retail_payment`(`retail_order_id`,`payment_amount`, `payment_date`, `payment_status`,`retail_tax_payment`,credit_method) VALUES ('%s','%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $charge_amount), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, 'M'),$tax_str,$credit_method);
                }
                $payment_result = mysqli_query($this->db, $payment_query);
                if(!$payment_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
    //        log_info("cancell : ".json_encode($retail_order_details));
            $cancelled_qty = $total_cancelled_qty = 0;
            for($i=0; $i<count($retail_order_details); $i++){
                $split_amount_str = "";
                if($to_charge==1 && $to_update_due_for_charge[$i]['handle_amount']>0 && $credit_method == 'CC'){
                    $split_amount_str = ",`rem_due`=`rem_due`+'".$to_update_due_for_charge[$i]['handle_amount']."'";
                }
                $cancelled_qty = $retail_order_details[$i]['cancelled_count'];
                $total_cancelled_qty += $cancelled_qty;
                if($retail_order_details[$i]['updated_quantity']==$retail_order_details[$i]['retail_quantity']){
                    $update_ord_det = sprintf("UPDATE `retail_order_details` SET `retail_discount`='%s' $split_amount_str WHERE `retail_order_detail_id`='%s'",
                        mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_discount']), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_order_detail_id']));
                }elseif($retail_order_details[$i]['updated_quantity']==0){
                     $update_ord_det = sprintf("UPDATE `retail_order_details` SET `retail_payment_amount`='%s', `retail_payment_status`='C', `retail_discount`='%s',`retail_cancelled_quantity`=`retail_quantity` WHERE `retail_order_detail_id`='%s'",
                            mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_payment_amount']), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_discount']), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_order_detail_id']));
                }elseif($retail_order_details[$i]['updated_quantity']<$retail_order_details[$i]['retail_quantity']){
                    $update_ord_det = sprintf("UPDATE `retail_order_details` SET `retail_cancelled_quantity`=`retail_cancelled_quantity`+%s,`retail_payment_amount`=(`retail_cost`*'%s'),`retail_discount`='%s' $split_amount_str WHERE `retail_order_detail_id`='%s'",
                            mysqli_real_escape_string($this->db, ($retail_order_details[$i]['retail_quantity']-$retail_order_details[$i]['updated_quantity'])), 
                            mysqli_real_escape_string($this->db, $retail_order_details[$i]['updated_quantity']), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_discount']), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_order_detail_id']));
                }else{
                    $error = array('status' => "Failed", "msg" => "Invalid quantity update.");
                    $this->response($this->json($error), 200);
                }
                $result_update_ord_det = mysqli_query($this->db, $update_ord_det);
                if(!$result_update_ord_det){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_ord_det");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    if($cancelled_qty>0){
                        $select_fulfillment = sprintf("SELECT `retail_order_fulfillment_id`, `fulfillment_status` FROM `retail_order_fulfillment` WHERE `company_id`='%s' AND `retail_order_detail_id`='%s' AND `fulfillment_status`!='C' ORDER BY `retail_order_fulfillment_id` DESC",
                                mysqli_real_escape_string($this->db, $company_id), $retail_order_details[$i]['retail_order_detail_id']);
                        $result_fulfillment = mysqli_query($this->db, $select_fulfillment);
                        if(!$result_fulfillment){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_fulfillment");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else{
                            if(mysqli_num_rows($result_fulfillment)){
                                $fulfilled_array = $unfulfilled_array = [];
                                while($row_fulfill = mysqli_fetch_assoc($result_fulfillment)){
                                    if($row_fulfill['fulfillment_status']=='F'){
                                        $fulfilled_array[] = $row_fulfill['retail_order_fulfillment_id'];
                                    }elseif($row_fulfill['fulfillment_status']=='U'){
                                        $unfulfilled_array[] = $row_fulfill['retail_order_fulfillment_id'];
                                    }
                                }
                                $all_array = array_slice(array_merge($unfulfilled_array,$fulfilled_array),0,$cancelled_qty);
                                $id_list = implode(",", $all_array);

                                $update_fulfillment = sprintf("UPDATE `retail_order_fulfillment` SET `fulfillment_status`='C' WHERE `retail_order_fulfillment_id` IN (%s)", $id_list);
                                $result_update_fulfillment = mysqli_query($this->db, $update_fulfillment);
                                if(!$result_update_fulfillment){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_fulfillment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
    //                    if ($retail_order_details[$i]['discount_valid'] == 'N' || $retail_order['discount_valid'] == 'N') {
    //                        $total_price = $retail_order_details[$i]['tax_value'] + $retail_order_details[$i]['retail_payment_amount'];
    //                    } else {
    //                        $total_price = $retail_order_details[$i]['retail_payment_amount'] - $retail_order_details[$i]['retail_discount'] + $retail_order_details[$i]['tax_value'];
    //                    }
                        $net_sales_string = $str = "";
                        $product_processing_fee = $final_net_update = 0;
                        //                    log_info("Order : ".json_encode($retail_order));
                        //                    log_info("Order details : ".json_encode($retail_order_details));
                        //                    log_info("to_update_due_for_charge".json_encode($to_update_due_for_charge));
                        if ($to_refund == 1 && $to_update_due_for_charge[$i]['handle_amount'] != 0) {
                            $final_net_update = $to_update_due_for_charge[$i]['handle_amount'];
                            if ($source_retail_order['processing_fee_type'] == 1) {
                                $product_processing_fee = $retail_total_processing_fee * ($to_update_due_for_charge[$i]['handle_amount'] / $new_payment_amount);
                                $final_net_update -= $product_processing_fee;
                            }
                            $net_sales_string = "`retail_net_sales`=`retail_net_sales`-$final_net_update ,";
                            if (empty($retail_order_details[$i]['retail_parent_id']) || is_null($retail_order_details[$i]['retail_parent_id'])) {
                                $str = "IN (%s)";
                                $this->addretailDimensions($company_id, $retail_order_details[$i]['retail_product_id'], '', '');
                                $this->updateRetailDimensionsNetSales($company_id, $retail_order_details[$i]['retail_product_id'], '', '', ($final_net_update) * -1);
                            } else {
                                $str = "IN (%s," . $retail_order_details[$i]['retail_parent_id'] . ")";
                                $this->addretailDimensions($company_id, $retail_order_details[$i]['retail_parent_id'], $retail_order_details[$i]['retail_product_id'], '');
                                $this->updateRetailDimensionsNetSales($company_id, $retail_order_details[$i]['retail_parent_id'], $retail_order_details[$i]['retail_product_id'], '', ($final_net_update) * -1);
                            }
                        } elseif ($to_charge == 1 && $to_update_due_for_charge[$i]['handle_amount'] != 0 && $credit_method != 'CC') {
                            $final_net_update = $to_update_due_for_charge[$i]['handle_amount'];
                            if ($source_retail_order['processing_fee_type'] == 1) {
                                $product_processing_fee = $retail_total_processing_fee * ($to_update_due_for_charge[$i]['handle_amount'] / $new_payment_amount);
                                $final_net_update -= $product_processing_fee;
                            }
                            $net_sales_string = "`retail_net_sales`=`retail_net_sales`+$final_net_update ,";
                            if (empty($retail_order_details[$i]['retail_parent_id']) || is_null($retail_order_details[$i]['retail_parent_id'])) {
                                $str = "IN (%s)";
                                $this->addretailDimensions($company_id, $retail_order_details[$i]['retail_product_id'], '', '');
                                $this->updateRetailDimensionsNetSales($company_id, $retail_order_details[$i]['retail_product_id'], '', '', $final_net_update);
                            } else {
                                $str = "IN (%s," . $retail_order_details[$i]['retail_parent_id'] . ")";
                                $this->addretailDimensions($company_id, $retail_order_details[$i]['retail_parent_id'], $retail_order_details[$i]['retail_product_id'], '');
                                $this->updateRetailDimensionsNetSales($company_id, $retail_order_details[$i]['retail_parent_id'], $retail_order_details[$i]['retail_product_id'], '', $final_net_update);
                            }
                        } elseif (empty($to_update_due_for_charge)) {
                            if (empty($retail_order_details[$i]['retail_parent_id']) || is_null($retail_order_details[$i]['retail_parent_id'])) {
                                $str = "IN (%s)";
                            } else {
                                $str = "IN (%s," . $retail_order_details[$i]['retail_parent_id'] . ")";
                            }
                        }
                        //                    log_info("net_sales_string : $net_sales_string");
                        if(empty($retail_order_details[$i]['retail_variant_id']) || is_null($retail_order_details[$i]['retail_variant_id'])) {
                            $update_retail_products = sprintf("UPDATE `retail_products` SET $net_sales_string`retail_product_inventory`=IF(`retail_product_inventory` IS NULL,NULL,`retail_product_inventory`+$cancelled_qty),`retail_purchase_count`=`retail_purchase_count`-$cancelled_qty WHERE `company_id`='%s' AND `retail_product_id` $str ", mysqli_real_escape_string($this->db, $company_id), $retail_order_details[$i]['retail_product_id']);
                        } else {
                            if($final_net_update>0){
                                $update_net = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`-$final_net_update WHERE `company_id`='%s' AND `retail_product_id` $str", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_product_id']));
                                $result_net = mysqli_query($this->db, $update_net);
                                if (!$result_net) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_net");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                            $update_retail_products = sprintf("UPDATE `retail_variants` SET `inventory_count`=IF(`inventory_count` IS NULL,NULL,`inventory_count`+$cancelled_qty), inventory_used_count=inventory_used_count-$cancelled_qty WHERE `company_id`='%s' AND `retail_variant_id`='%s' ", mysqli_real_escape_string($this->db, $company_id), $retail_order_details[$i]['retail_variant_id']);
                        }
                        $result_update_products = mysqli_query($this->db, $update_retail_products);
                        if (!$result_update_products) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_retail_products");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }

            $insert_notes = sprintf("INSERT INTO `retail_history` (`company_id`, `retail_order_fulfillment_id`, `retail_order_id`, `activity_type`, `activity_text`) VALUES ('%s','%s', %s, '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, '-1'), mysqli_real_escape_string($this->db, $retail_order_id), 'cancel', mysqli_real_escape_string($this->db, $cancelled_str));
            $result_insert_notes = mysqli_query($this->db, $insert_notes);
            if (!$result_insert_notes) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_notes");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }

            $discount_update_string = $update_retail_order = "";
            if($total_cancelled_qty > 0) {
                if($retail_order['discount_valid'] == 'Y'){
                    $update_retail_order = sprintf("UPDATE `retail_orders` SET `retail_order_checkout_amount`='%s',`retail_order_discount_amount`='%s',`retail_order_refunded_amount`=`retail_order_refunded_amount`+%s,`retail_products_tax`='%s',`processing_fee_val`='%s' WHERE `company_id`='%s' AND `retail_order_id`='%s'",
                            mysqli_real_escape_string($this->db, $retail_order['retail_order_checkout_amount']), mysqli_real_escape_string($this->db, $retail_order['retail_order_discount_amount']), mysqli_real_escape_string($this->db, $retail_order['retail_total_refund']), 
                            mysqli_real_escape_string($this->db, $retail_order['retail_products_tax']), mysqli_real_escape_string($this->db,$retail_order['processing_fee_val']),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $retail_order['retail_order_id']));
                }elseif ($retail_order['discount_valid'] == 'N') {
                    $update_retail_order = sprintf("UPDATE `retail_orders` SET `retail_order_checkout_amount`='%s',`retail_order_discount_amount`='%s',`retail_order_refunded_amount`=`retail_order_refunded_amount`+%s,`retail_products_tax`='%s',`retail_discount_id`=0,`retail_discount_code`='',
                                `retail_discount_type`='',`retail_discount_value`=0,`retail_discount_min_req_flag`='',`retail_discount_min_req_value`='',`retail_discount_applied_type`='', `processing_fee_val`='%s' WHERE `company_id`='%s' AND `retail_order_id`='%s'",
                            mysqli_real_escape_string($this->db, $retail_order['retail_order_checkout_amount']), mysqli_real_escape_string($this->db, $retail_order['retail_order_discount_amount']), mysqli_real_escape_string($this->db, $retail_order['retail_total_refund']), 
                            mysqli_real_escape_string($this->db, $retail_order['retail_products_tax']), mysqli_real_escape_string($this->db,$retail_order['processing_fee_val']),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $retail_order['retail_order_id']));
                }

                if($retail_order['discount_usage_count']>0 && $retail_order['optional_discount_flag'] == 'N'){
                    $discount_update_string = sprintf("UPDATE `retail_discount` SET `discount_used_count`=`discount_used_count`-%s WHERE `company_id`='%s' AND `retail_discount_id`='%s' ",mysqli_real_escape_string($this->db, $retail_order['discount_usage_count']),
                            mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $retail_order['retail_discount_id'])); 
                }
            }

            if(!empty($update_retail_order)){
                $result_update_order = mysqli_query($this->db, $update_retail_order);
                if (!$result_update_order) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_retail_order");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if (!empty($discount_update_string)) {
                        $result_update_discount = mysqli_query($this->db, $discount_update_string);
                        if (!$result_update_discount) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$discount_update_string");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }
            $log = array("status" => "Success", "msg" => "Edit Order Processed Successfully.");
            $this->response($this->json($log),200);
        }
    }
    
    private function getRealIpAddr(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet        
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    public function updateVariantSortOrder($company_id, $sort_id, $variant_id,$retail_id){
        
        $sql_retail_variant = sprintf("SELECT * FROM `retail_variants` WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `retail_variant_id`='%s' AND `deleted_flag`!='D' ORDER BY `variant_sort_order`", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_id), mysqli_real_escape_string($this->db, $variant_id));
        $result_variant = mysqli_query($this->db, $sql_retail_variant);

        if (!$result_variant) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_retail_variant");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows2 = mysqli_num_rows($result_variant);
            if ($num_of_rows2 == 0) {
                $error = array('status' => "Failed", "msg" => "Membership rank details not available.");
                $this->response($this->json($error), 200);
            }
        }
        
        $update_sort_order = sprintf("UPDATE `retail_variants` SET `variant_sort_order`='%s' WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `retail_variant_id`='%s'",
                mysqli_real_escape_string($this->db, $sort_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_id), mysqli_real_escape_string($this->db, $variant_id));
        $result = mysqli_query($this->db, $update_sort_order);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sort_order");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $this->getretailvariantdetails($company_id, $retail_id);
        }
        
    }
    
      //retail dashboard
    public function addretailDimensions($company_id, $product_id, $child_id, $period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($child_id)) {
            $sql1 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND  `child_id`='%s' AND child_id !=0  AND `period`='%s' AND `deleted_flg`!='D'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $insertquery = sprintf("INSERT INTO `retail_dimensions`(`company_id`, `period`, `product_id`, `child_id`) VALUES('%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id));
                    $res = mysqli_query($this->db, $insertquery);
                    if (!$res) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        if (!empty($product_id)) {
            $sql2 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s' AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result2 = mysqli_query($this->db, $sql2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows1 = mysqli_num_rows($result2);
                if ($num_of_rows1 == 0) {
                    $insertquery1 = sprintf("INSERT INTO `retail_dimensions`(`company_id`, `period`, `product_id`,  `child_id`) VALUES('%s', '%s','%s',0)",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $product_id));
                    $res2 = mysqli_query($this->db, $insertquery1);
                    if (!$res2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function updateRetailDimensionsNetSales($company_id, $product_id, $child_id, $date, $amount) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        if (!empty($date)) {
            $every_month_dt = date("Y-m", strtotime($date));
        } else {
            $every_month_dt = date("Y-m");
        }

//        $date_init = 0;
//        $date_str = "%Y-%m";
//        $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));

        if (!empty($child_id)) {
            $sql1 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`='%s' AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), $every_month_dt);
            $res1 = mysqli_query($this->db, $sql1);
            if (!$res1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($res1);
                if ($num_of_rows == 1) {
                    $sql2 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`='%s' AND `period`='%s'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), $every_month_dt);
                    $res2 = mysqli_query($this->db, $sql2);
                    if (!$res2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $sql3 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
                        $res3 = mysqli_query($this->db, $sql3);
                        if (!$res3) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }
        } else {
            $sql2 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
            $res2 = mysqli_query($this->db, $sql2);
            if (!$res2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($res2);
                if ($num_of_rows == 1) {
                    $sql3 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `period`='%s' AND `child_id`='0'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
                    $res3 = mysqli_query($this->db, $sql3);
                    if (!$res3) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function updateCategoryandProductNetsales($company_id ,$retail_parent_id, $retail_product_id){
        $old_update = $new_update = "";
        
        $check_old_product = sprintf("select `retail_parent_id`,`retail_net_sales` from `retail_products` where `company_id`='%s' AND `retail_product_id`='%s' AND `retail_product_status`!='D'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
        $check_result = mysqli_query($this->db, $check_old_product);
        if (!$check_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_old_product");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($check_result);
            if ($num_of_rows > 0) {
                $check_row = mysqli_fetch_assoc($check_result);
                $old_retail_parent_id = $check_row['retail_parent_id'];
                $retail_net_sales = $check_row['retail_net_sales'];
                if (!empty($retail_parent_id)) {
                    if($retail_parent_id !== $old_retail_parent_id && !empty($old_retail_parent_id) && !is_null($old_retail_parent_id)){
                        $old_update = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`-$retail_net_sales WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `retail_product_status`!='D'",
                                 mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $old_retail_parent_id));
                        $new_update = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`+$retail_net_sales WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `retail_product_status`!='D'",
                                 mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_parent_id));
                    }elseif (empty($old_retail_parent_id) || is_null($old_retail_parent_id)) {
                        $new_update = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`+$retail_net_sales WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `retail_product_status`!='D'",
                                 mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_parent_id));
                    }
                } else {
                    if(!empty($old_retail_parent_id) && !is_null($old_retail_parent_id)){
                        $old_update = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`-$retail_net_sales WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `retail_product_status`!='D'",
                                 mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $old_retail_parent_id));
                    }
                }
            }
        }
        
        if (!empty($old_update)) {
            $old_res = mysqli_query($this->db, $old_update);
            if (!$old_res) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$old_update");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }

        if (!empty($new_update)) {
            $new_res = mysqli_query($this->db, $new_update);
            if (!$new_res) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$new_update");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function getRetailTabs($company_id){
        $sql = sprintf("SELECT * FROM `retail_order_fulfillment` WHERE `company_id`='%s'" , mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);        
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $msg = array('status' => "Success", "msg" => "F");  //Fulfillment tab
                $this->response($this->json($msg), 200);
            }else{
                $msg = array('status' => "Success", "msg" => "R");  //Retail options/Products tab
                $this->response($this->json($msg), 200);
            }
        }
    }
    
    private function checkRetailFirstTime($company_id) {
        $query = sprintf("SELECT * FROM `retail_products` where company_id='%s' ORDER BY retail_product_id", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if(mysqli_num_rows($result)==0){
                $query2 = sprintf("SELECT * FROM `retail_products` where company_id='6' AND retail_product_status='P' ORDER BY retail_product_id", mysqli_real_escape_string($this->db, $company_id));
                $result2 = mysqli_query($this->db, $query2);
                if (!$result2) {
                    $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log2));
                    $error2 = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error2), 200);
                } else {
                    $num_row = mysqli_num_rows($result2);
                    if ($num_row > 0) {
                        while ($row = mysqli_fetch_assoc($result2)) {
                            $product_id = $row['retail_product_id'];
                            $type_check = $row['retail_product_type'];
                            if ($type_check != 'P') {
                                $this->copyRetailDetails($company_id, $product_id, '', 'Y', '', 1, 'C');
                            }
                        }
                        // UPDATE POS EVENT STATUS TO ENABLED
                        $query3 = sprintf("UPDATE studio_pos_settings SET `retail_status` = 'E' WHERE `company_id` = '%s' ", mysqli_real_escape_string($this->db, $company_id));
                        $result3 = mysqli_query($this->db, $query3);
                        if (!$result3) {
                            $error_log3 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                            log_info($this->json($error_log3));
                            $error3 = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error3), 200);
                        }
                    }else{
                        $error = array('status' => "Failed", "msg" => "Retail details does not exist.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
    }
    
    public function wepayStatus($company_id) {
        $query = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s' ORDER BY wp_account_id DESC LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                if($row['account_state']=='disabled' || $row['account_state']=='deleted') {
                    return 'D';
                } else {
                    return 'A';
                }
            } else {
                return 'N';
            }
        }
    }
    
    public function copyRetailDetailsForStudio($from_company, $company_id) {
        $query2 = sprintf("SELECT * FROM `retail_products` where company_id='%s' AND retail_product_status='P' ORDER BY retail_product_id", 
                mysqli_real_escape_string($this->db, $from_company), mysqli_real_escape_string($this->db, $company_id));
        $result2 = mysqli_query($this->db, $query2);
        if (!$result2) {
            $error_log2 = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
            log_info($this->json($error_log2));
            $error2 = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error2), 200);
        } else {
            $num_row = mysqli_num_rows($result2);
            if ($num_row > 0) {
                while ($row = mysqli_fetch_assoc($result2)) {
                    $product_id = $row['retail_product_id'];
                    $type_check = $row['retail_product_type'];
                    if ($type_check != 'P') {
                        $this->copyRetailDetailsForSpecificCompany($from_company, $company_id, $product_id, '', 'Y', '', 1);
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Retail details does not exist.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function copyRetailDetailsForSpecificCompany($from_company, $company_id, $retail_id, $list_type, $retail_template_flag, $level, $call_back){
        $sql_company_code = sprintf("SELECT  `company_code` FROM  `company` WHERE  `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result_company_code = mysqli_query($this->db, $sql_company_code);
        if (!$result_company_code) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_company_code");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $output_value = mysqli_fetch_assoc($result_company_code);
            $company_code = $output_value['company_code'];
        }
        
        $sql = sprintf("SELECT `retail_product_id`, `retail_parent_id`, `retail_product_type`, `retail_product_status`, `retail_product_title`, 
            `retail_product_subtitle`, `retail_product_desc`,`product_tax_rate`,`retail_variant_flag`, `retail_product_inventory`,
            `retail_product_url`, `retail_product_price`, `retail_product_compare_price`, `retail_banner_img_url` 
             FROM `retail_products` WHERE `company_id`='%s' AND 
            (`retail_product_id`='%s' OR `retail_parent_id`='%s') AND `retail_product_status`='P' order by retail_product_id", $from_company, $retail_id, $retail_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                
                if(empty(trim($output[0]['retail_product_compare_price']))||is_null($output[0]['retail_product_compare_price'])){
                    $retail_product_compare_price = 'NULL';
                }else{
                    $retail_product_compare_price = $output[0]['retail_product_compare_price'];
                }
                if(empty(trim($output[0]['retail_product_inventory'])) || is_null($output[0]['retail_product_inventory'])){
                    $retail_product_inventory = 'NULL';
                }else{
                    $retail_product_inventory = $output[0]['retail_product_inventory'];
                }
                if (empty(trim($output[0]['retail_parent_id']))) {
                    $parent_id = 'null';
                    $sql1 = sprintf("INSERT INTO `retail_products` (`company_id`, `retail_parent_id`, `retail_product_type`, `retail_product_status`, `retail_product_title`, 
                                    `retail_product_subtitle`, `retail_product_desc`,`product_tax_rate`,`retail_variant_flag`, `retail_product_inventory`,
                                    `retail_product_url`, `retail_product_price`, `retail_product_compare_price`, `retail_banner_img_url`,`retail_sort_order`)
                                    VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', %s, '%s', NextVal('retail_product_sort_order_seq'))",
                            mysqli_real_escape_string($this->db, $company_id), 
                            mysqli_real_escape_string($this->db, $parent_id), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_type']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_status']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_title']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_subtitle']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_desc']), 
                            mysqli_real_escape_string($this->db, $output[0]['product_tax_rate']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_variant_flag']), 
                            mysqli_real_escape_string($this->db, $retail_product_inventory),                             
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_price']), 
                            mysqli_real_escape_string($this->db, $retail_product_compare_price), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_banner_img_url']));
                } else{
                    $parent_id = $output[0]['retail_parent_id'];
                    $sql1 = sprintf("INSERT INTO `retail_products` (`company_id`, `retail_parent_id`, `retail_product_type`, `retail_product_status`, `retail_product_title`, 
                        `retail_product_subtitle`, `retail_product_desc`, `product_tax_rate`, `retail_variant_flag`, `retail_product_inventory`, 
                        `retail_product_url`, `retail_product_price`, `retail_product_compare_price`, `retail_banner_img_url`, 
                        `retail_sort_order`) VALUES (%s, %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', %s, '%s', NextVal('retail_product_sort_order_seq'))",
                            mysqli_real_escape_string($this->db, $company_id), 
                            mysqli_real_escape_string($this->db, $parent_id), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_type']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_status']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_title']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_subtitle']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_desc']), 
                            mysqli_real_escape_string($this->db, $output[0]['product_tax_rate']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_variant_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_inventory']),                            
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_product_price']),
                            mysqli_real_escape_string($this->db, $retail_product_compare_price), 
                            mysqli_real_escape_string($this->db, $output[0]['retail_banner_img_url'])); 
                }
                $result1 = mysqli_query($this->db, $sql1);

                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $last_inserted_retail_id = mysqli_insert_id($this->db);
                    if($output[0]['retail_product_type'] == 'P' || $output[0]['retail_product_type'] == 'S'|| $output[0]['retail_product_type'] == 'C'){
                        $company_code_new = $this->clean($company_code);
                        if (!empty(trim($parent_id)) && $parent_id != 'null') {
                            $url_str = "$parent_id/$last_inserted_retail_id";
                        }else{
                            $url_str = "$last_inserted_retail_id/";
                        }
                        $retail_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/r/?=$company_code_new/$company_id/$url_str/";
                        $retail_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->upload_folder_name/Default/default.png";
                        if($output[0]['retail_banner_img_url'] !== $retail_banner_img_url){
                            $file_name = $company_id . "-" . $last_inserted_retail_id;
                            $cfolder_name = "Company_$company_id";
                            $old = getcwd(); // Save the current directory
                            $dir_name = "../../../$this->upload_folder_name";
                            chdir($dir_name);
                            if (!file_exists($cfolder_name)) {
                                $oldmask = umask(0);
                                mkdir($cfolder_name, 0777, true);
                                umask($oldmask);
                            }
                            chdir($cfolder_name);
                            $folder_name = "Retail";
                            if (!file_exists($folder_name)) {
                                $oldmask = umask(0);
                                mkdir($folder_name, 0777, true);
                                umask($oldmask);
                            }
                            chdir($folder_name);
                            $file = $file_name.'.png' ;
                            $check = copy($output[0]['retail_banner_img_url'],$file);
                            chdir($old);
                            if($check){
                                $retail_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME']."/$this->upload_folder_name/$cfolder_name/$folder_name/$file";
                            }else{
                                $error_log = array('status' => "Failed", "msg" => "Image copy failed", "image_url" => $output[0]['retail_banner_img_url']);
                                log_info($this->json($error_log)); 
                            }
                        }

                        $sql_update_url = sprintf("UPDATE `retail_products` SET `retail_product_url` = '$retail_url',`retail_banner_img_url` = '$retail_banner_img_url' WHERE `retail_product_id` = '%s' ", mysqli_real_escape_string($this->db, $last_inserted_retail_id));
                        $result_update_url = mysqli_query($this->db, $sql_update_url);
                        if (!$result_update_url) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update_url");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    if ($last_inserted_retail_id > 0) {
                        $output3 = [];
                        $num_of_rows3 = 0;
                        $sql_retail_variants = sprintf("SELECT * FROM `retail_variants` WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `deleted_flag`!='D' ORDER BY `variant_sort_order`", mysqli_real_escape_string($this->db, $from_company), mysqli_real_escape_string($this->db, $output[0]['retail_product_id']));
                        $result3 = mysqli_query($this->db, $sql_retail_variants);
                        if (!$result3) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_retail_variants");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            $num_of_rows3 = mysqli_num_rows($result3);
                            if ($num_of_rows3 > 0) {
                                while ($row3 = mysqli_fetch_assoc($result3)) {
                                    $output3[] = $row3;
                                }
                            }
                        }
                        if ($num_of_rows3 > 0) {
                            for ($j = 0; $j < $num_of_rows3; $j++) {
                                if(empty(trim($output3[$j]['variant_compare_price']))||is_null($output3[$j]['variant_compare_price'])){
                                    $variant_compare_price = 'NULL';
                                }else{
                                    $variant_compare_price = $output3[$j]['variant_compare_price'];
                                }
                                if(empty(trim($output3[$j]['inventory_count'])) || is_null($output3[$j]['inventory_count'])){
                                    $inventory_count = 'NULL';
                                }else{
                                    $inventory_count = $output3[$j]['inventory_count'];
                                }
                
                                $sql5 = sprintf("INSERT INTO `retail_variants` (`retail_product_id`,`company_id`, `retail_variants_name`,`inventory_count`,`variant_price`,`variant_compare_price`,`variant_sort_order`) VALUES ('%s','%s', '%s', %s, '%s', %s, NextVal('retail_variant_sort_order_seq'))",
                                         mysqli_real_escape_string($this->db, $last_inserted_retail_id),mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $output3[$j]['retail_variants_name']), mysqli_real_escape_string($this->db, $inventory_count)
                                        ,mysqli_real_escape_string($this->db, $output3[$j]['variant_price']), mysqli_real_escape_string($this->db, $variant_compare_price));
                                $result5 = mysqli_query($this->db, $sql5);
                                if (!$result5) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql5");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
                    }
                    if ($num_of_rows > 1) {
                        
                        for ($i = 1; $i < count($output); $i++) {
                            if(empty(trim($output[$i]['retail_product_compare_price']))||is_null($output[$i]['retail_product_compare_price'])){
                                $retail_product_compare_price = 'NULL';
                            }else{
                                $retail_product_compare_price = $output[$i]['retail_product_compare_price'];
                            }
                            if(empty(trim($output[$i]['retail_product_inventory'])) || is_null($output[$i]['retail_product_inventory'])){
                                $retail_product_inventory = 'NULL';
                            }else{
                                $retail_product_inventory = $output[$i]['retail_product_inventory'];
                            }
                            $sql2 = sprintf("INSERT INTO `retail_products` (`company_id`, `retail_parent_id`, `retail_product_type`, `retail_product_status`, `retail_product_title`, 
                                    `retail_product_subtitle`, `retail_product_desc`,`product_tax_rate`,`retail_variant_flag`, `retail_product_inventory`,
                                    `retail_product_url`, `retail_product_price`, `retail_product_compare_price`,  `retail_banner_img_url`,`retail_sort_order`)
                                    VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', %s, '%s', NextVal('retail_product_sort_order_seq'))",
                            mysqli_real_escape_string($this->db, $company_id), 
                            mysqli_real_escape_string($this->db, $last_inserted_retail_id), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_type']), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_status']), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_title']), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_subtitle']), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_desc']), 
                            mysqli_real_escape_string($this->db, $output[$i]['product_tax_rate']), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_variant_flag']), 
                            mysqli_real_escape_string($this->db, $retail_product_inventory),
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_url']), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_product_price']), 
                            mysqli_real_escape_string($this->db, $retail_product_compare_price), 
                            mysqli_real_escape_string($this->db, $output[$i]['retail_banner_img_url']));
                            $result2 = mysqli_query($this->db, $sql2);

                            if (!$result2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            } else {
                                $last_inserted_retail_id1 = mysqli_insert_id($this->db);
                                if ($output[$i]['retail_product_type'] == 'P' || $output[$i]['retail_product_type'] == 'S'|| $output[$i]['retail_product_type'] == 'C') {
                                    $company_code_new = $this->clean($company_code);
                                    $url_str = "$last_inserted_retail_id/$last_inserted_retail_id1";
                                    $retail_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/r/?=$company_code_new/$company_id/$url_str/";
                                    $retail_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/Default/default.png";
                                    if ($output[$i]['retail_banner_img_url'] !== $retail_banner_img_url) {
                                        $file_name = $company_id . "-" . $last_inserted_retail_id1;
                                        $cfolder_name = "Company_$company_id";
                                        $old = getcwd(); // Save the current directory
                                        $dir_name = "../../../$this->upload_folder_name";
                                        chdir($dir_name);
                                        if (!file_exists($cfolder_name)) {
                                            $oldmask = umask(0);
                                            mkdir($cfolder_name, 0777, true);
                                            umask($oldmask);
                                        }
                                        chdir($cfolder_name);
                                        $folder_name = "Retail";
                                        if (!file_exists($folder_name)) {
                                            $oldmask = umask(0);
                                            mkdir($folder_name, 0777, true);
                                            umask($oldmask);
                                        }
                                        chdir($folder_name);
                                        $file = $file_name . '.png';
                                        $check = copy($output[$i]['retail_banner_img_url'], $file);
                                        chdir($old);
                                        if ($check) {
                                            $retail_banner_img_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/$cfolder_name/$folder_name/$file";
                                        } else {
                                            $error_log = array('status' => "Failed", "msg" => "Image copy failed", "image_url" => $output[$i]['retail_banner_img_url']);
                                            log_info($this->json($error_log));
                                        }
                                    }

                                    $sql_update_url = sprintf("UPDATE `retail_products` SET `retail_product_url` = '$retail_url',`retail_banner_img_url` = '$retail_banner_img_url' WHERE `retail_product_id` = '%s' ", mysqli_real_escape_string($this->db, $last_inserted_retail_id1));
                                    $result_update_url = mysqli_query($this->db, $sql_update_url);
                                    if (!$result_update_url) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update_url");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    }
                                    
                                    $output3 = [];
                                    $num_of_rows3 = 0;
                                    $sql_retail_variants = sprintf("SELECT * FROM `retail_variants` WHERE `company_id`='%s' AND `retail_product_id`='%s' AND `deleted_flag`!='D' ORDER BY `variant_sort_order`", mysqli_real_escape_string($this->db, $from_company), mysqli_real_escape_string($this->db, $output[$i]['retail_product_id']));
                                    $result3 = mysqli_query($this->db, $sql_retail_variants);
                                    if (!$result3) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_retail_variants");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    } else {
                                        $num_of_rows3 = mysqli_num_rows($result3);
                                        if ($num_of_rows3 > 0) {
                                            while ($row3 = mysqli_fetch_assoc($result3)) {
                                                $output3[] = $row3;
                                            }
                                            for ($j = 0; $j < $num_of_rows3; $j++) {
                                                if(empty(trim($output3[$j]['variant_compare_price']))||is_null($output3[$j]['variant_compare_price'])){
                                                    $variant_compare_price = 'NULL';
                                                }else{
                                                    $variant_compare_price = $output3[$j]['variant_compare_price'];
                                                }
                                                if(empty(trim($output3[$j]['inventory_count'])) || is_null($output3[$j]['inventory_count'])){
                                                    $inventory_count = 'NULL';
                                                }else{
                                                    $inventory_count = $output3[$j]['inventory_count'];
                                                }

                                                $sql5 = sprintf("INSERT INTO `retail_variants` (`retail_product_id`,`company_id`, `retail_variants_name`,`inventory_count`,`variant_price`,`variant_compare_price`,`variant_sort_order`) VALUES ('%s','%s', '%s', %s, '%s', %s, NextVal('retail_variant_sort_order_seq'))",
                                                         mysqli_real_escape_string($this->db, $last_inserted_retail_id1),mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $output3[$j]['retail_variants_name']), mysqli_real_escape_string($this->db, $inventory_count)
                                                        ,mysqli_real_escape_string($this->db, $output3[$j]['variant_price']), mysqli_real_escape_string($this->db, $variant_compare_price));
                                                $result5 = mysqli_query($this->db, $sql5);
                                                if (!$result5) {
                                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql5");
                                                    log_info($this->json($error_log));
                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                    $this->response($this->json($error), 200);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if($call_back==0){
                    if($retail_template_flag=='Y'){
                        $log = array('status' => "Success", "msg" => "Retail details copied successfully.");
                        $this->response($this->json($log), 200); // If no records "No Content" status
                    }else{
                        if($level == "L1"){
                            $this->getretailmanagelistdetails($company_id,$list_type,'',0,'');
                        } else {
                            $this->getretailmanagelistdetails($company_id,$list_type,$parent_id,0,'');
                        }
                    }
                }else{
                    $log = array('status' => "Success", "msg" => "Retail details copied successfully.");
                    return $log;
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Retail details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
              
    }
    
    public function getCompanyStripeAccount($company_id, $call_back){
        $query = sprintf("SELECT * FROM `stripe_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result) >0){
                $row = mysqli_fetch_assoc($result);
                if($call_back == 1){
                    return $row;
                }else{
                    $msg = array("status" => "Success", "msg" => "Stripe Account details", "stripe_details" => "$row");
                    $this->response($this->json($msg), 200);
                }
            }else{
                if($call_back == 1){
                    return '';
                }else{
                    $msg = array("status" => "Failed", "msg" => "Stripe Account details", "stripe_details" => "");
                    $this->response($this->json($msg), 200);
                }
            }
        }
    }
    
    protected function checkCountryPaymentSupport($company_id, $country, $callback) { // GET COUNTRY PAYMENT SUPPORT
        $row = [];
        $sql = sprintf("SELECT * FROM country_payment_specific WHERE `payment_support` = 'S' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        UNION
                        SELECT * FROM country_payment_specific WHERE `payment_support` = 'W' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        ORDER BY `payment_support` ASC, `company_id` DESC", mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id)
                , mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                if ($callback == 0) {
                    $out = array("status" => "Success", "msg" => $row);
                } else {
                    $row['status'] = 'Success';
                    return $row;
                }
                $this->response($this->json($out), 200);
            } else {
                $out = array("status" => "Failed", "msg" => "Studio country is not yet available for payment support.");
                return $out;
            }
        }
    }
    
    protected function getCompanyRecord($company_id, $callback) { // GET COMPANY RECORD 
        $row = [];
        $sql = sprintf("SELECT * FROM company where `company_id` = '%s'",mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                if ($callback == 0) {
                    $out = array("status" => "Success", "msg" => $row);
                } else {
                    return $row;
                }
                $this->response($this->json($out), 200);
            } else {
                $row['support_status'] = 'N';
                if ($callback == 0) {
                    $out = array("status" => "Failed", "msg" => $row);
                } else {
                    return $row;
                }
                $this->response($this->json($out), 200);
            }
        }
    }
      // Muthulakshmi  Stripe Refund for retail
    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }
    public function stripeRefundRetailInDB($company_id, $order, $order_details) {
        $user_timezone = $this->getUserTimezone($company_id);
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $curr_date = gmdate("Y-m-d");
        
        $out = $this->retailEditorderDetailsInDb($company_id, $order, $order_details, 1);
        $retail_order = $out['order'];
        $retail_order_details = $out['order_details'];
        $retail_order_id = $retail_order['retail_order_id'];
        $cancelled_str = $out['note'];
        
        $source_retail_array = $this->getRetailOrderDetails($company_id, $retail_order_id, 1);
        $source_retail_order = $source_retail_array['msg']['order'];
        $source_retail_order_details = $source_retail_array['msg']['order_details'];
        $buyer_email = $source_retail_order['buyer_email'];
        $payment_method_id = $source_retail_order['payment_method_id'];
        $stripe_card_name = $source_retail_order['stripe_card_name'];
        $student_id = $source_retail_order['student_id'];
        $stripe_customer_id  = $source_retail_order['stripe_customer_id'];
        $stripe_currency = $source_retail_order['stripe_currency'];
        $studio_name = $source_retail_order['studio_name'];
        $stripe_account_id = $source_retail_order['stripe_account_id'];
        $stripe_refund_flag = $source_retail_order['stripe_refund_flag'];
        if($stripe_refund_flag == 'N'){
            $error = array('status' => "Failed", "msg" => "Connected account does not have sufficient fund to cover the refund.");
            $this->response($this->json($error), 200);
        }
        $cc_type = 'E';
        $has_3D_secure = 'N';
        $checkout_state = '';
        
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
        
        if ((!empty($stripe_account) && $stripe_account['charges_enabled'] == 'Y')){   

            $to_update_due_for_charge = [];
            $new_payment_amount = 0;
            $to_refund = $to_charge = $refund_amount = $refund_fee = $charge_amount = $charge_fee = $retail_total_processing_fee = 0;
            $process_type = $retail_order['process_type'];
            if($process_type=='R'){
                $to_refund = 1;
                $cancellation_amount = $refund_amount = $retail_order['process_amount'];
                $retail_total_processing_fee = $refund_fee = $retail_order['process_fee'];
                for ($i = 0; $i < count($retail_order_details); $i++) {
                    $to_update_due_for_charge[$i]['handle_amount'] = $retail_order_details[$i]['refund_amount'];
                    $new_payment_amount += $to_update_due_for_charge[$i]['handle_amount'];
                }
            }elseif($process_type=='N'){
                $to_charge = 1;
                $cancellation_amount = $charge_amount = $retail_order['process_amount'];
                $retail_total_processing_fee = $charge_fee = $retail_order['process_fee'];
                for ($i = 0; $i < count($retail_order_details); $i++) {
                    $to_update_due_for_charge[$i]['handle_amount']=0;
                    if($retail_order_details[$i]['updated_quantity']!=0){
                        $to_update_due_for_charge[$i]['handle_amount'] = $retail_order['process_amount'] * ((($retail_order_details[$i]['retail_cost']*$retail_order_details[$i]['updated_quantity'])-$retail_order_details[$i]['retail_discount']+$retail_order_details[$i]['tax_value'])/($retail_order['retail_order_checkout_amount']-$retail_order['process_amount']));
                    }
                }
                if($source_retail_order['processing_fee_type'] == 2){
                    $charge_amount += $retail_order['process_fee'];
                }
            }

            $payments = [];
            $get_payment = sprintf("SELECT credit_method,`retail_payment_id`, `payment_method_id`, `stripe_card_name`, `payment_intent_id`, `checkout_status`, (`payment_amount`-`refunded_amount`) payment_amount, `payment_date`, `actual_paid_date`, `payment_status`, `processing_fee`, `refunded_amount`, `refunded_date`, `last_updt_dt`
                        FROM `retail_payment` WHERE `retail_order_id`='%s' AND `payment_status` IN ('S', 'PR', 'M', 'MP') ORDER BY `retail_payment_id` DESC", mysqli_real_escape_string($this->db, $retail_order_id));
            $result_get_payment = mysqli_query($this->db, $get_payment);
            if(!$result_get_payment){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_payment");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                if(mysqli_num_rows($result_get_payment)>0){
                    while($row_payment = mysqli_fetch_assoc($result_get_payment)){
                        $payments[] = $row_payment;
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => "Payment details mismatched with order details.");
                    $this->response($this->json($error), 200);
                }
            }
            $credit_method = 'CC';
            $payment_intent_id = $button_url = '';
            if($payments[0]['credit_method'] == "CH" || $payments[0]['credit_method'] == "CA"){
                $credit_method = $payments[0]['credit_method'];
            }
            if($to_refund==1){
                $retail_refund_array = [];
                $process_amt = $cancellation_amount;
                for($j=0; $j<count($payments); $j++){
                    $payment_id = $payments[$j]['retail_payment_id'];
                    $payment_intent_id = $payments[$j]['payment_intent_id'];
                    $checkout_status = $payments[$j]['checkout_status'];
                    $payment_amount = $payments[$j]['payment_amount'];
                    $payment_fee = $payments[$j]['processing_fee'];
                    $last_updt_dt = $payments[$j]['last_updt_dt'];
                    //$type => P - Partial, F - Full
                    if($payment_amount>$process_amt){
                        $type = 'P';
                        $retail_refund_array[] = array("credit_method"=>$payments[$j]['credit_method'],"type"=>$type, "payment_id"=>$payment_id, "payment_method_id"=>$payments[$j]['payment_method_id'], "stripe_card_name"=>$payments[$j]['stripe_card_name'], "payment_intent_id"=>$payment_intent_id, "checkout_status"=>$checkout_status, "refund_amount"=>$process_amt, "refund_fee"=>$refund_fee, "last_updt_dt"=>$last_updt_dt);
                        break;
                    }else{
                        $type = 'F';
                        $retail_refund_array[] = array("credit_method"=>$payments[$j]['credit_method'],"type"=>$type, "payment_id"=>$payment_id, "payment_method_id"=>$payments[$j]['payment_method_id'], "stripe_card_name"=>$payments[$j]['stripe_card_name'], "payment_intent_id"=>$payment_intent_id, "checkout_status"=>$checkout_status, "refund_amount"=>$payment_amount, "refund_fee"=>$payment_fee, "last_updt_dt"=>$last_updt_dt);
                        $process_amt -= $payment_amount;
                        $refund_fee -= $payment_fee;
                    }
                }
                for($k=0;$k<count($retail_refund_array);$k++){
                    $payment_id = $retail_refund_array[$k]['payment_id'];
                    $checkout_status = $retail_refund_array[$k]['checkout_status'];
                    $payment_intent_id = $retail_refund_array[$k]['payment_intent_id'];
                    $refund_amount = $retail_refund_array[$k]['refund_amount'];
                    $refund_fee = $retail_refund_array[$k]['refund_fee'];
                    $type = $retail_refund_array[$k]['type'];
                    $last_updt_date = $retail_refund_array[$k]['last_updt_dt'];
                    
                    // stripe refund start
                    if($credit_method == "CC" && !empty($payment_intent_id)){ // for retrieve charge id
                        $charge_retrieve_arr = $this->stripeChargeIdRetrieve($payment_intent_id);
                        $charge_array_initial = json_decode(json_encode($charge_retrieve_arr),true);
                        $charge_array = $charge_array_initial['charges']['data'];
                        $charge_id = $charge_array[0]['id'];
                    }
                    $round_refund_amount = $refund_amount * 100;
                    if($credit_method == "CC" && !empty($charge_id)){ // for refund
                        try {
                            $create_refund = \Stripe\Refund::create([
                                "charge" => $charge_id,
                                "amount" => $round_refund_amount,
                                "reverse_transfer" => true]);
                        } catch (\Stripe\Error\Card $e) {
                            // Since it's a decline, \Stripe\Error\Card will be caught       
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\RateLimit $e) {
                            // Too many requests made to the API too quickly      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\InvalidRequest $e) {
                            // Invalid parameters were supplied to Stripe's API      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Authentication $e) {
                            // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\ApiConnection $e) {
                            // Network communication with Stripe failed      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Base $e) {
                            // Display a very generic error to the user, and maybe send yourself an email      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (Exception $e) {
                            // Something else happened, completely unrelated to Stripe      
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        }
                        if (isset($err) && !empty($err)) {
                            $error = array('status' => "Failed", "msg" => $err['message']);
                            $this->response($this->json($error), 200);
                        }
                        $misc_refund_arr = json_decode(json_encode($create_refund),true);
                        if ($misc_refund_arr['status'] == 'succeeded'){

                        }else{
                            $error = array("status" => "Failed", "msg" => "Can't able to refund amount.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    if($checkout_status !="requires_action"){
                        if($type == 'F') {
                            if ($credit_method == 'CA' || $credit_method == 'CH') {
                                $refund_string = 'MF';
                                $refund_string1 = 'MR';
                                $refund_fee_string = '0';
                            } else {
                                $refund_string = 'FR';
                                $refund_string1 = 'R';
                                $refund_fee_string = '`processing_fee`';
                            }
                            $insert_new_refunded_query = sprintf("INSERT INTO `retail_payment`( `retail_order_id`, `payment_intent_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `refunded_date`, `payment_method_id`, `stripe_card_name`, `retail_tax_payment`, `created_dt`, `last_updt_dt`,`credit_method`,`payment_from`) 
                                    SELECT `retail_order_id`, `payment_intent_id`, `checkout_status`, `payment_amount`, $refund_fee_string, `payment_date`, '$refund_string', CURDATE(), `payment_method_id`, `stripe_card_name`, `retail_tax_payment`, `created_dt`, '$last_updt_date','$credit_method',`payment_from` FROM `retail_payment` WHERE `retail_payment_id`='%s'", $payment_id);

                            $update_query2 = sprintf("UPDATE `retail_payment` SET `payment_amount` = '%s', `payment_status`='$refund_string1', `refunded_amount`=0, `processing_fee`='%s', `retail_tax_payment`='%s' WHERE `retail_payment_id`='%s'", 
                                mysqli_real_escape_string($this->db, $refund_amount), mysqli_real_escape_string($this->db, $refund_fee), mysqli_real_escape_string($this->db, $retail_order['cancelled_products_tax']), $payment_id);
                        }else{
                            if ($credit_method == 'CA' || $credit_method == 'CH') {
                                $refund_string = 'MP';
                                $refund_string1 = 'MR';
                            } else {
                                $refund_string = 'PR';
                                $refund_string1 = 'R';
                            }
                            $insert_new_refunded_query = sprintf("INSERT INTO `retail_payment`( `retail_order_id`, `payment_intent_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `stripe_card_name`, `retail_tax_payment`, `created_dt`, `last_updt_dt`,`credit_method`,`payment_from`) 
                                    SELECT `retail_order_id`, `payment_intent_id`, `checkout_status`, $refund_amount, $refund_fee, CURDATE(), '$refund_string1', `payment_method_id`, `stripe_card_name`, '%s', `created_dt`, NOW(),'$credit_method',`payment_from` FROM `retail_payment` WHERE `retail_payment_id`='%s'", mysqli_real_escape_string($this->db, $retail_order['cancelled_products_tax']), $payment_id);                    

                            $update_query2 = sprintf("UPDATE `retail_payment` SET `payment_status`='$refund_string', `refunded_amount`=`refunded_amount`+'%s', `processing_fee`=`processing_fee`-'%s', `refunded_date`=CURDATE() WHERE `retail_payment_id`='%s'", 
                                mysqli_real_escape_string($this->db, $refund_amount), mysqli_real_escape_string($this->db, $refund_fee), $payment_id);
                        }
                        $result_insert_new_refunded_query = mysqli_query($this->db, $insert_new_refunded_query);
                        if (!$result_insert_new_refunded_query) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_new_refunded_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            $result_update_query2 = mysqli_query($this->db, $update_query2);
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query2");
                            if(!$result_update_query2){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                    }else{
                        $log = array("status" => "Failed", "msg" => 'Not a valid checkout status');
                        $this->response($this->json($log),200);
                    }
                }
            }elseif($to_charge==1){
                if($credit_method == "CC"){
                    $paid_fee = $charge_fee;
                    $w_paid_amount = $charge_amount;
                    $w_paid_amount_round = round($w_paid_amount, 2);
                    $dest_amount = $w_paid_amount - $paid_fee;
                    $dest_amount_round = round($dest_amount, 2);
                    $dest_amount_in_dollars = $dest_amount_round * 100;
                    $amount_in_dollars = $w_paid_amount_round * 100; 
                    $p_type = 'retail';
                    $payment_intent_result = $this->payment_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$amount_in_dollars,$stripe_currency,$studio_name,$buyer_email,$stripe_account_id,$dest_amount_in_dollars,$p_type,$cc_type);
                    if ($payment_intent_result['temp_payment_status'] == 'succeeded'){
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'released';
                    }elseif($payment_intent_result['temp_payment_status'] == 'requires_action') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'requires_action';
                        $has_3D_secure = 'Y';
                        $next_action_type = $payment_intent_result['next_action']['type'];
                        if($next_action_type != "redirect_to_url"){
                            # Payment not supported
                            $out['status'] = 'Failed';
                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                            $this->response($this->json($out), 200);
                        }
                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                    } else {
                        # Invalid status
                        $out['status'] = 'Failed';
                        $out['msg'] = 'Invalid SetupIntent status.';
                        $out['error'] = 'Invalid SetupIntent status';
                        $this->response($this->json($out), 200);
                    }
                    
                    $tax_str = 0;
                    if($retail_order['cancelled_products_tax']!=0){
                        $tax_str = abs($retail_order['cancelled_products_tax']);
                    }
                    $payment_query = sprintf("INSERT INTO `retail_payment`(`retail_order_id`, `payment_intent_id`, `checkout_status`, `processing_fee`, `payment_amount`, `payment_date`, `payment_status`, `payment_method_id`, `stripe_card_name`, `retail_tax_payment`,`payment_from`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','S')",
                            mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $charge_fee),
                        mysqli_real_escape_string($this->db, $charge_amount), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), $tax_str);
                }else{
                    $tax_str = 0;
                    if($retail_order['cancelled_products_tax']!=0){
                    $tax_str = abs($retail_order['cancelled_products_tax']);
                    }
                    $payment_query = sprintf("INSERT INTO `retail_payment`(`retail_order_id`,`payment_amount`, `payment_date`, `payment_status`,`retail_tax_payment`,credit_method) VALUES ('%s','%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $charge_amount), mysqli_real_escape_string($this->db, $curr_date), mysqli_real_escape_string($this->db, 'M'),$tax_str,$credit_method);
                }
                $payment_result = mysqli_query($this->db, $payment_query);
                if(!$payment_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
            $cancelled_qty = $total_cancelled_qty = 0;
            for($i=0; $i<count($retail_order_details); $i++){
                $split_amount_str = "";
                if($to_charge==1 && $to_update_due_for_charge[$i]['handle_amount']>0 && $credit_method == 'CC'){
                    $split_amount_str = ",`rem_due`=`rem_due`+'".$to_update_due_for_charge[$i]['handle_amount']."'";
                }
                $cancelled_qty = $retail_order_details[$i]['cancelled_count'];
                $total_cancelled_qty += $cancelled_qty;
                if($retail_order_details[$i]['updated_quantity']==$retail_order_details[$i]['retail_quantity']){
                    $update_ord_det = sprintf("UPDATE `retail_order_details` SET `retail_discount`='%s' $split_amount_str WHERE `retail_order_detail_id`='%s'",
                        mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_discount']), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_order_detail_id']));
                }elseif($retail_order_details[$i]['updated_quantity']==0){
                     $update_ord_det = sprintf("UPDATE `retail_order_details` SET `retail_payment_amount`='%s', `retail_payment_status`='C', `retail_discount`='%s',`retail_cancelled_quantity`=`retail_quantity` WHERE `retail_order_detail_id`='%s'",
                            mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_payment_amount']), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_discount']), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_order_detail_id']));
                }elseif($retail_order_details[$i]['updated_quantity']<$retail_order_details[$i]['retail_quantity']){
                    $update_ord_det = sprintf("UPDATE `retail_order_details` SET `retail_cancelled_quantity`=`retail_cancelled_quantity`+%s,`retail_payment_amount`=(`retail_cost`*'%s'),`retail_discount`='%s' $split_amount_str WHERE `retail_order_detail_id`='%s'",
                            mysqli_real_escape_string($this->db, ($retail_order_details[$i]['retail_quantity']-$retail_order_details[$i]['updated_quantity'])), 
                            mysqli_real_escape_string($this->db, $retail_order_details[$i]['updated_quantity']), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_discount']), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_order_detail_id']));
                }else{
                    $error = array('status' => "Failed", "msg" => "Invalid quantity update.");
                    $this->response($this->json($error), 200);
                }
                $result_update_ord_det = mysqli_query($this->db, $update_ord_det);
                if(!$result_update_ord_det){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_ord_det");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    if($cancelled_qty>0){
                        $select_fulfillment = sprintf("SELECT `retail_order_fulfillment_id`, `fulfillment_status` FROM `retail_order_fulfillment` WHERE `company_id`='%s' AND `retail_order_detail_id`='%s' AND `fulfillment_status`!='C' ORDER BY `retail_order_fulfillment_id` DESC",
                                mysqli_real_escape_string($this->db, $company_id), $retail_order_details[$i]['retail_order_detail_id']);
                        $result_fulfillment = mysqli_query($this->db, $select_fulfillment);
                        if(!$result_fulfillment){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_fulfillment");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else{
                            if(mysqli_num_rows($result_fulfillment)){
                                $fulfilled_array = $unfulfilled_array = [];
                                while($row_fulfill = mysqli_fetch_assoc($result_fulfillment)){
                                    if($row_fulfill['fulfillment_status']=='F'){
                                        $fulfilled_array[] = $row_fulfill['retail_order_fulfillment_id'];
                                    }elseif($row_fulfill['fulfillment_status']=='U'){
                                        $unfulfilled_array[] = $row_fulfill['retail_order_fulfillment_id'];
                                    }
                                }
                                $all_array = array_slice(array_merge($unfulfilled_array,$fulfilled_array),0,$cancelled_qty);
                                $id_list = implode(",", $all_array);

                                $update_fulfillment = sprintf("UPDATE `retail_order_fulfillment` SET `fulfillment_status`='C' WHERE `retail_order_fulfillment_id` IN (%s)", $id_list);
                                $result_update_fulfillment = mysqli_query($this->db, $update_fulfillment);
                                if(!$result_update_fulfillment){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_fulfillment");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
                        $net_sales_string = $str = "";
                        $product_processing_fee = $final_net_update = 0;
                        if ($to_refund == 1 && $to_update_due_for_charge[$i]['handle_amount'] != 0) {
                            $final_net_update = $to_update_due_for_charge[$i]['handle_amount'];
                            if ($source_retail_order['processing_fee_type'] == 1) {
                                $product_processing_fee = $retail_total_processing_fee * ($to_update_due_for_charge[$i]['handle_amount'] / $new_payment_amount);
                                $final_net_update -= $product_processing_fee;
                            }
                            $net_sales_string = "`retail_net_sales`=`retail_net_sales`-$final_net_update ,";
                            if (empty($retail_order_details[$i]['retail_parent_id']) || is_null($retail_order_details[$i]['retail_parent_id'])) {
                                $str = "IN (%s)";
                                $this->addretailDimensions($company_id, $retail_order_details[$i]['retail_product_id'], '', '');
                                $this->updateRetailDimensionsNetSales($company_id, $retail_order_details[$i]['retail_product_id'], '', '', ($final_net_update) * -1);
                            } else {
                                $str = "IN (%s," . $retail_order_details[$i]['retail_parent_id'] . ")";
                                $this->addretailDimensions($company_id, $retail_order_details[$i]['retail_parent_id'], $retail_order_details[$i]['retail_product_id'], '');
                                $this->updateRetailDimensionsNetSales($company_id, $retail_order_details[$i]['retail_parent_id'], $retail_order_details[$i]['retail_product_id'], '', ($final_net_update) * -1);
                            }
                        } elseif ($to_charge == 1 && $to_update_due_for_charge[$i]['handle_amount'] != 0 && $credit_method != 'CC') {
                            $final_net_update = $to_update_due_for_charge[$i]['handle_amount'];
                            if ($source_retail_order['processing_fee_type'] == 1) {
                                $product_processing_fee = $retail_total_processing_fee * ($to_update_due_for_charge[$i]['handle_amount'] / $new_payment_amount);
                                $final_net_update -= $product_processing_fee;
                            }
                            $net_sales_string = "`retail_net_sales`=`retail_net_sales`+$final_net_update ,";
                            if (empty($retail_order_details[$i]['retail_parent_id']) || is_null($retail_order_details[$i]['retail_parent_id'])) {
                                $str = "IN (%s)";
                                $this->addretailDimensions($company_id, $retail_order_details[$i]['retail_product_id'], '', '');
                                $this->updateRetailDimensionsNetSales($company_id, $retail_order_details[$i]['retail_product_id'], '', '', $final_net_update);
                            } else {
                                $str = "IN (%s," . $retail_order_details[$i]['retail_parent_id'] . ")";
                                $this->addretailDimensions($company_id, $retail_order_details[$i]['retail_parent_id'], $retail_order_details[$i]['retail_product_id'], '');
                                $this->updateRetailDimensionsNetSales($company_id, $retail_order_details[$i]['retail_parent_id'], $retail_order_details[$i]['retail_product_id'], '', $final_net_update);
                            }
                        } elseif (empty($to_update_due_for_charge)) {
                            if (empty($retail_order_details[$i]['retail_parent_id']) || is_null($retail_order_details[$i]['retail_parent_id'])) {
                                $str = "IN (%s)";
                            } else {
                                $str = "IN (%s," . $retail_order_details[$i]['retail_parent_id'] . ")";
                            }
                        }
                        if(empty($retail_order_details[$i]['retail_variant_id']) || is_null($retail_order_details[$i]['retail_variant_id'])) {
                            $update_retail_products = sprintf("UPDATE `retail_products` SET $net_sales_string`retail_product_inventory`=IF(`retail_product_inventory` IS NULL,NULL,`retail_product_inventory`+$cancelled_qty),`retail_purchase_count`=`retail_purchase_count`-$cancelled_qty WHERE `company_id`='%s' AND `retail_product_id` $str ", mysqli_real_escape_string($this->db, $company_id), $retail_order_details[$i]['retail_product_id']);
                        } else {
                            if($final_net_update>0){
                                $update_net = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`-$final_net_update WHERE `company_id`='%s' AND `retail_product_id` $str", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_order_details[$i]['retail_product_id']));
                                $result_net = mysqli_query($this->db, $update_net);
                                if (!$result_net) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_net");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                            $update_retail_products = sprintf("UPDATE `retail_variants` SET `inventory_count`=IF(`inventory_count` IS NULL,NULL,`inventory_count`+$cancelled_qty), inventory_used_count=inventory_used_count-$cancelled_qty WHERE `company_id`='%s' AND `retail_variant_id`='%s' ", mysqli_real_escape_string($this->db, $company_id), $retail_order_details[$i]['retail_variant_id']);
                        }
                        $result_update_products = mysqli_query($this->db, $update_retail_products);
                        if (!$result_update_products) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_retail_products");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }

            $insert_notes = sprintf("INSERT INTO `retail_history` (`company_id`, `retail_order_fulfillment_id`, `retail_order_id`, `activity_type`, `activity_text`) VALUES ('%s','%s', %s, '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, '-1'), mysqli_real_escape_string($this->db, $retail_order_id), 'cancel', mysqli_real_escape_string($this->db, $cancelled_str));
            $result_insert_notes = mysqli_query($this->db, $insert_notes);
            if (!$result_insert_notes) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_notes");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }

            $discount_update_string = $update_retail_order = "";
            if($total_cancelled_qty > 0) {
                if($retail_order['discount_valid'] == 'Y'){
                    $update_retail_order = sprintf("UPDATE `retail_orders` SET `retail_order_checkout_amount`='%s',`retail_order_discount_amount`='%s',`retail_order_refunded_amount`=`retail_order_refunded_amount`+%s,`retail_products_tax`='%s',`processing_fee_val`='%s' WHERE `company_id`='%s' AND `retail_order_id`='%s'",
                            mysqli_real_escape_string($this->db, $retail_order['retail_order_checkout_amount']), mysqli_real_escape_string($this->db, $retail_order['retail_order_discount_amount']), mysqli_real_escape_string($this->db, $retail_order['retail_total_refund']), 
                            mysqli_real_escape_string($this->db, $retail_order['retail_products_tax']), mysqli_real_escape_string($this->db,$retail_order['processing_fee_val']),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $retail_order['retail_order_id']));
                }elseif ($retail_order['discount_valid'] == 'N') {
                    $update_retail_order = sprintf("UPDATE `retail_orders` SET `retail_order_checkout_amount`='%s',`retail_order_discount_amount`='%s',`retail_order_refunded_amount`=`retail_order_refunded_amount`+%s,`retail_products_tax`='%s',`retail_discount_id`=0,`retail_discount_code`='',
                                `retail_discount_type`='',`retail_discount_value`=0,`retail_discount_min_req_flag`='',`retail_discount_min_req_value`='',`retail_discount_applied_type`='', `processing_fee_val`='%s' WHERE `company_id`='%s' AND `retail_order_id`='%s'",
                            mysqli_real_escape_string($this->db, $retail_order['retail_order_checkout_amount']), mysqli_real_escape_string($this->db, $retail_order['retail_order_discount_amount']), mysqli_real_escape_string($this->db, $retail_order['retail_total_refund']), 
                            mysqli_real_escape_string($this->db, $retail_order['retail_products_tax']), mysqli_real_escape_string($this->db,$retail_order['processing_fee_val']),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $retail_order['retail_order_id']));
                }

                if($retail_order['discount_usage_count']>0 && $retail_order['optional_discount_flag'] == 'N'){
                    $discount_update_string = sprintf("UPDATE `retail_discount` SET `discount_used_count`=`discount_used_count`-%s WHERE `company_id`='%s' AND `retail_discount_id`='%s' ",mysqli_real_escape_string($this->db, $retail_order['discount_usage_count']),
                            mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $retail_order['retail_discount_id'])); 
                }
            }

            if(!empty($update_retail_order)){
                $result_update_order = mysqli_query($this->db, $update_retail_order);
                if (!$result_update_order) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_retail_order");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if (!empty($discount_update_string)) {
                        $result_update_discount = mysqli_query($this->db, $discount_update_string);
                        if (!$result_update_discount) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$discount_update_string");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }
            if($to_charge==1 && $credit_method == "CC"){
                if ($has_3D_secure == 'Y') {
                    $email_msg = 'Hi '.$buyer_name.',<br>';
                    $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">This is an authentication email for Retail purchase rerun payment. Kindly click the launch button for redirecting to Stripe.</p>';
                    $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">Complete a required action for this payment. Once the redirect is completed for this PaymentIntent, the payment will proceed.</p>';
                    $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg);
                }elseif($checkout_state == 'released'){
                    $this->updateStripeCCDetailsAfterRelease($payment_intent_id);
                }
            }
            $log = array("status" => "Success", "msg" => "Edit Order Processed Successfully.");
            $this->response($this->json($log),200);
        }else{
            $error = array("status" => "Failed", "msg" => "We cannot able to access your stripe account due to invalid stripe status.");
            $this->response($this->json($error), 200);
        }
    }
    public function payment_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$amount_in_dollars,$stripe_currency,$studio_name,$buyer_email,$stripe_account_id,$dest_amount_in_dollars,$p_type,$cc_type){
        if ($cc_type == 'N') {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }

        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try {
            $payment_intent_create = \Stripe\PaymentIntent::create([
                        'payment_method' => $payment_method_id,
                        'customer' => $stripe_customer_id,
                        'amount' => $amount_in_dollars,
                        'currency' => $stripe_currency,
                        'confirmation_method' => 'automatic',
                        'capture_method' => 'automatic',
                        'confirm' => true,
                        'return_url' => $this->server_url."/stripeUpdation.php?type=authenticate&c_id=$company_id&p_type=$p_type&s_id=$student_id",
                        'metadata' =>['c_id' => $company_id,'p_type' => $p_type,'s_id' => $student_id ,'s_name' => $studio_name],
                        'setup_future_usage' => 'off_session',
                        'description' => 'Payment Intent for ' . $buyer_email,
                        'payment_method_types' => ['card'],
                        'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                        'receipt_email' => $buyer_email,
                        'transfer_data' => ['destination' => $stripe_account_id, 'amount' => $dest_amount_in_dollars],
                        'on_behalf_of' => $stripe_account_id,
                        'statement_descriptor' => substr($studio_name,0,21),
            ]);
            $body3 = json_encode($payment_intent_create);
            stripe_log_info("Stripe Payment Creation : $body3");
            $return_array['temp_payment_status'] = $payment_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['cc_month'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_month;
                $return_array['cc_year'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_year;
                $brand = $payment_intent_create->charges->data[0]->payment_method_details->card->brand;
                $last4 = $payment_intent_create->charges->data[0]->payment_method_details->card->last4;
                $temp = ' xxxxxx';
                $return_array['stripe_card_name'] = $brand . $temp . $last4;
            }else if ($return_array['temp_payment_status'] == 'requires_action'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['payment_intent_client_secret'] = $payment_intent_create->client_secret;
                $return_array['next_action'] = $payment_intent_create->next_action;
//                $return_array['next_action']['type'] = $payment_intent_create->next_action->type;
//                $return_array['next_action']['redirect_to_url']['url'] = $payment_intent_create->next_action->redirect_to_url->url;
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }
    public function stripeChargeIdRetrieve($payment_intent_id) {
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        if (!empty($payment_intent_id)) {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $charge_id_retrieve = \Stripe\PaymentIntent::retrieve($payment_intent_id);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
            return $charge_id_retrieve;
        }
    }
    public function updateStripeCCDetailsAfterRelease($payment_intent_id){
        $checkout_state = $retail_order_id = $payment_id = $processing_fee_type = '';
        $retail_total_processing_fee = $fee = $payment_amount = 0;
        $retail_arr=[];
        $query = sprintf("SELECT rto.`company_id`,rod.`retail_order_detail_id`,rod.`retail_payment_amount`, rod.`retail_product_id`, rto.`retail_order_id`,rod.`retail_parent_id`,rod.`rem_due`, rp.`retail_payment_id`, rp.`checkout_id`, rp.`checkout_status`, rp.`payment_amount`, rp.`processing_fee`, rto.`processing_fee_type`,rto.`retail_discount_id`,rod.`retail_tax_percentage`,rod.`retail_discount`
                    FROM `retail_orders` rto 
                    LEFT JOIN `retail_payment` rp ON rto.`retail_order_id`=rp.`retail_order_id`
                    LEFT JOIN `retail_order_details` rod ON rto.`retail_order_id`=rod.`retail_order_id`
                    WHERE rp.`payment_intent_id` = '%s' AND rp.`payment_status` IN ('S', 'PR') ORDER BY rp.`retail_payment_id`", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            stripe_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $k=0;
                while($row = mysqli_fetch_assoc($result)){
                    $retail_order_id = $row['retail_order_id'];
                    $payment_id = $row['retail_payment_id'];
                    $retail_total_processing_fee = $fee = $row['processing_fee'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $payment_amount = $row['payment_amount'];
                    $company_id = $row['company_id'];
                    $retail_arr[$k]['retail_order_detail_id'] = $row['retail_order_detail_id'];
                    $retail_arr[$k]['retail_product_id'] = $row['retail_product_id'];
                    $retail_arr[$k]['retail_parent_id'] = $row['retail_parent_id'];
                    $retail_arr[$k]['retail_price'] = $row['retail_payment_amount'];
                    $retail_arr[$k]['retail_tax_percentage'] = $row['retail_tax_percentage'];
                    $retail_arr[$k]['retail_discount'] = $row['retail_discount'];
                    $retail_arr[$k]['rem_due'] = $row['rem_due'];
                    $k++;
                }
                $payment_status = "";
                $checkout_state = "released";
                $update_amount = 0;
                $paid_amount = $payment_amount;
                if($processing_fee_type==1){
                    $total_amount = $update_amount = $payment_amount;
                }elseif($processing_fee_type==2){
                    $total_amount = $update_amount = $payment_amount - $fee;
                }
                $update_sales_in_reg = sprintf("UPDATE `retail_orders` SET `retail_order_paid_amount`=`retail_order_paid_amount`+$paid_amount WHERE `retail_order_id`='%s'", mysqli_real_escape_string($this->db, $retail_order_id));
                $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);

                if(!$result_sales_in_reg){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                    stripe_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
                for($i=0;$i<count($retail_arr);$i++){
                    $retail_order_detail_id = $retail_arr[$i]['retail_order_detail_id'];
                    $retail_parent_id=$retail_arr[$i]['retail_parent_id'];
                    $retail_product_id=$retail_arr[$i]['retail_product_id'];
                    $rem_due = $retail_arr[$i]['rem_due'];
                    $str = '';

                    if($rem_due>0 && $update_amount>0){
                        $due = $sales_due = 0;
                        if($rem_due>$update_amount){
                            $sales_due = $update_amount;
                            $due = $rem_due - $update_amount;
                            $update_amount = 0;
                            if($due<.5){
                                $sts = 'S';
                            }else{
                                $sts = 'P';
                            }
                        }else{
                            $sales_due = $rem_due;
                            $due = 0;
                            $sts = 'S';
                            $update_amount -= $rem_due;
                        }
                        $product_processing_fee = $retail_total_processing_fee * ($retail_arr[$i]['rem_due']/$total_amount);
                        $final_net_update = $sales_due;
                        if($processing_fee_type==1){
                            $final_net_update = $sales_due-$product_processing_fee;
                        }
                        if (empty($retail_parent_id) || is_null($retail_parent_id)) {
                            $str = "IN (%s)";
                            $this->addretailDimensions($company_id, $retail_arr[$i]['retail_product_id'], '', '');
                            $this->updateRetailDimensionsNetSales($company_id, $retail_arr[$i]['retail_product_id'], '', '', $final_net_update);
                        } else {
                            $str = "IN (%s,$retail_parent_id)";
                            $this->addretailDimensions($company_id, $retail_arr[$i]['retail_parent_id'], $retail_arr[$i]['retail_product_id'], '');
                            $this->updateRetailDimensionsNetSales($company_id, $retail_arr[$i]['retail_parent_id'], $retail_arr[$i]['retail_product_id'], '', $final_net_update);
                        }

                        $update_order_details = sprintf("UPDATE `retail_order_details` SET `rem_due`='%s', `retail_payment_status`='$sts' WHERE `company_id`='%s' AND `retail_order_id`='%s' AND `retail_order_detail_id`='%s'", mysqli_real_escape_string($this->db, $due),
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $retail_order_detail_id));
                        $result_order_details = mysqli_query($this->db, $update_order_details);
                        if(!$result_order_details){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_order_details");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }

                        $update_count = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`+$final_net_update WHERE `company_id`='%s' AND `retail_product_id` $str",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
                        $result_count = mysqli_query($this->db, $update_count);
                        if(!$result_count){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }
                }
                
                $update_query = sprintf("UPDATE `retail_payment` SET `checkout_status`='%s', `actual_paid_date`=CURDATE() $payment_status WHERE `retail_order_id`='%s' and `retail_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                $update_result = mysqli_query($this->db, $update_query);
                if(!$update_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                    stripe_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }
    }
    public function sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name, $email_msg){
        
        // EMAIL CONTENT
        $button_text = 'Launch';
        $subject = 'Stripe authenticate';
        $message = $email_msg.'<br>'.'<center><a href="'.$button_url.'"><button type="button" style="width: 190px;border-radius: 5px;background: #009a61;border:1px solid 009a61;color: #ffffff;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 45px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;text-transform: none;overflow: visible;box-sizing: border-box;cursor: pointer;">'.$button_text.'</button></a></center>';
        $sendEmail_status = $this->sendEmail($buyer_email, $subject, $message, $studio_name, '', '', '', '', $company_id, '');
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
    }
    private function sendEmail($to, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file, $cc_email_list, $company_id, $footer_detail) {
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }            
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
            $mail->Subject = $subject;
             if (!empty($company_id)) {
                $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                $curr_loc = implode('/', explode('/', $curr_loc, -4));
//                $tomail = $to;
//                if (!empty(trim($cc_email_list))) {
//                    $tomail = base64_encode($to . "," . "$cc_email_list");
//                } else {
                    $tomail = base64_encode($to);
//                }
                $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/EmailSubscription.php?mail=$tomail?comp_id=$company_id?type=U";
                $message = "
            <html>
            <body>
                <p>$message</p><br><br>
                    <footer style='font-size: 13px;color: #746f6f;'>
                    <center>
                    <p>$footer_detail
                    </p>
                    <a href='$activation_link' target='_blank' style='color: #746f6f; !important;text-decoration: none !important;'><u style='color: #746f6f;'>Unsubscribe $to</u></a>
                    </center>
                    </footer>
            </body>
            </html>
            ";
            }
            $mail->Body = $message;
            if(!empty($file_name)){
                $mail->AddStringAttachment(base64_decode($attached_file), $file_name, 'base64');
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $verify_email_for_bounce = $this->verifyBouncedEmailForCC($cc_addresses[$init], $company_id);
                         if($verify_email_for_bounce==1){
                               $mail->AddCC($cc_addresses[$init]);
                         }
//                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }

}
