<?php


 header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');

require_once '../../../Globals/cont_log.php';
require_once 'Salesforce.php';
require_once 'WePay.php';
require_once('../../PHPMailer_5.2.1/class.phpmailer.php');
require_once('../../PHPMailer_5.2.1/class.smtp.php');
require_once 'tfpdf.php';
//require_once '../../../aws/aws-autoloader.php';
//
//use Aws\Sns\SnsClient;


class LeadsModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    public $local_upload_folder_name = '';
    private $sf;
    private $list_id = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_redirect_uri = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $server_url, $env;

    public function __construct() {
        require_once '../../../Globals/config.php';
        $this->inputs();
        $this->sf = new Salesforce();
        $this->wp = new wepay();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->local_upload_folder_name = $GLOBALS['local_uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->list_id = $this->sf->list_id;
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = "../../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'dev.mystudio.academy') !== false) {
            $this->server_url = "http://dev.mystudio.academy";
            $this->env = 'D';
        } elseif (strpos($host, 'dev2.mystudio.academy') !== false) {
            $this->server_url = "http://dev2.mystudio.academy";
            $this->env = 'D';
        } elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'];
            $this->env = 'P';
        } else {
            $this->server_url = "http://dev.mystudio.academy";
            $this->env = 'D';
        }
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithSalesforce($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
        header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    protected function clean($string) {
        $str = preg_replace('/\s+/', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    }
    
    public function updateprograminterestdetails($company_id, $program_name, $program_id, $type, $call_back) {// call_back  = 0 response 1 =return// 
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_new = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";

        if ($type == 'update') {
            $pi_ls_name = 'Not Specified';
            $default_check = sprintf("select * from `leads_program` where `company_id`='%s' and `program_id`='%s' and `deleted_flg`!='D'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id));
            $default_result_check = mysqli_query($this->db, $default_check);
            if (!$default_result_check) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$default_check");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200);
                } else {
                    return $error;
                }
            } else {
                $default_rows = mysqli_num_rows($default_result_check);
                if ($default_rows > 0) {
                    $default = mysqli_fetch_assoc($default_result_check);
                    if ($default['program_name'] == $pi_ls_name) {
                        $error = array('status' => "Failed", "msg" => "$pi_ls_name can not be changed to $program_name");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }

        $check = sprintf("select * from `leads_program` where `company_id`='%s' and `program_name`='%s' and `deleted_flg`!='D'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_name));
        $result_check = mysqli_query($this->db, $check);
        if (!$result_check) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 0) {
                $this->response($this->json($error), 200);
            } else {
                return $error;
            }
        } else {
            $check_rows = mysqli_num_rows($result_check);
            if ($check_rows > 0) {
                $error = array('status' => "Failed", "msg" => "program name already exits for this studio");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200);
                } else {
                    return $error;
                }
            }
        }
        if ($type == 'add') {
            $sql1 = sprintf("INSERT INTO `leads_program` (`company_id`, `program_name`, `program_sort_order`) VALUES ('%s', '%s',  NextVal('program_sort_order_seq'))",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_name));
        } else if ($type == 'update') {
            $sql1 = sprintf("update `leads_program` set `program_name` = '%s' where `company_id`='%s' and `program_id`='%s'", 
                    mysqli_real_escape_string($this->db, $program_name), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id));
        }
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 0) {
                $this->response($this->json($error), 200);
            } else {
                return $error;
            }
        }
        if ($call_back == 0) {//call_back, 0 -> returned & exit, 1 -> return response
            $this->getleadsProgramDetails($company_id, 0);
        } else {
            return 'success';
        }
        date_default_timezone_set($curr_time_zone);
    }

    public function updateleadsourcedetails($company_id, $source_name, $source_id, $type, $call_back) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_new = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        
        if ($type == 'update') {
            $pi_ls_name = 'Not Specified';
            $default_check = sprintf("select * from `leads_source` where `company_id`='%s' and `source_id`='%s' and `deleted_flg`!='D'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $source_id));
            $default_result_check = mysqli_query($this->db, $default_check);
            if (!$default_result_check) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$default_check");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200);
                } else {
                    return $error;
                }
            } else {
                $default_rows = mysqli_num_rows($default_result_check);
                if ($default_rows > 0) {
                    $default = mysqli_fetch_assoc($default_result_check);
                    if ($default['source_name'] == $pi_ls_name) {
                        $error = array('status' => "Failed", "msg" => "$pi_ls_name can not be changed to $source_name");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }

         $check = sprintf("select * from `leads_source` where `company_id`='%s' and `source_name`='%s' and `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $source_name));
            $result_check = mysqli_query($this->db, $check);
            if (!$result_check) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200);
                } else {
                    return $error;
                }
            } else {
                $check_rows = mysqli_num_rows($result_check);
                if ($check_rows > 0) {
                    $error = array('status' => "Failed", "msg" => "source name already exits for this studio");
                    if ($call_back == 0) {
                        $this->response($this->json($error), 200);
                    } else {
                        return $error;
                    }
                }
            }
        if ($type == 'add') {
            $sql1 = sprintf("INSERT INTO `leads_source` (`company_id`, `source_name`, `source_sort_order`) VALUES ('%s', '%s',  NextVal('source_sort_order_seq'))", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $source_name));
        } else if ($type == 'update') {
            $sql1 = sprintf("update `leads_source` set `source_name` = '%s' where `company_id`='%s' and `source_id`='%s'", 
                    mysqli_real_escape_string($this->db, $source_name), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $source_id));
        }
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 0) {
                $this->response($this->json($error), 200);
            } else {
                return $error;
            }
        }
        if ($call_back == 0) {
            $this->getleadsProgramDetails($company_id, 0);   //call_back, 0 -> returned & exit, 1 -> return response
        } else {
            return 'success';
        }

        date_default_timezone_set($curr_time_zone);
    }

    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
            } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
         }
    }
    
    public function getleadsProgramDetails($company_id, $call_back) {//call_back, 0 -> returned & exit, 1 -> return response
        //check array_slice in following before alter or add any columns in this query
        $sql = sprintf("SELECT 'program' as type , `program_id`, `company_id`, `program_name`, `program_sort_order` sort_order FROM `leads_program` 
                 WHERE `company_id`='%s' and `deleted_flg`!='D' 
                 UNION 
                 SELECT 'source' as type , `source_id`, `company_id`, `source_name`, `source_sort_order` sort_order FROM `leads_source` 
                 WHERE `company_id`='%s' and `deleted_flg`!='D' order by sort_order",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 1) {
                return $error;
            } else {
                $this->response($this->json($error), 200);
            }
        } else {
            $leads_program_list = $leads_source_list = $output = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $r = $b = 0;
                while ($row = mysqli_fetch_assoc($result)) {
                    if ($row['type'] == 'program') {
                        $leads_program_list[$r]['id'] = $row['program_id'];
                        $leads_program_list[$r]['name'] = $row['program_name'];
                        $leads_program_list[$r]['category_sort_order'] = $row['sort_order'];
                        $r++;
                    } else if ($row['type'] == 'source') {
                        $leads_source_list[$b]['id'] = $row['program_id'];
                        $leads_source_list[$b]['name'] = $row['program_name'];
                        $leads_source_list[$b]['category_sort_order'] = $row['sort_order'];
                        $b++;
                    }
                }
                $output['program_interest'] = $leads_program_list;
                $output['lead_source'] = $leads_source_list;
                $out = array('status' => "Success", 'msg' => $output);
                // If success everythig is good send header as "OK" and user details
                if ($call_back == 1) {
                    return $out;
                } else {
                    $this->response($this->json($out), 200);
                }
            } else {
                $type = 'add';
                $source_name = ['Not Specified', 'Google', 'Facebook'];
                $program_name = 'Not Specified';
                for ($i = 0; $i < 3; $i++) {
                    $check = $this->updateleadsourcedetails($company_id, $source_name[$i], '', $type, 1);
                }
                $check = $this->updateprograminterestdetails($company_id, $program_name, '', $type, 1);
                $this->getleadsProgramDetails($company_id, 0);
            }
        }
    }

    public function updatesortlistDetails($sort_order, $company_id, $id, $type){
        if($type == 'pi'){
             $query = sprintf("SELECT * FROM `leads_program` WHERE `company_id`='%s' AND `program_id`='%s'",
                     mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $id));
               $update_message = sprintf("UPDATE `leads_program` SET `program_sort_order`='%s' WHERE `program_id`='%s' AND `company_id`='%s'",
                 mysqli_real_escape_string($this->db,$sort_order), mysqli_real_escape_string($this->db,$id), mysqli_real_escape_string($this->db,$company_id));
        }else if($type == 'ls'){
            $query = sprintf("SELECT * FROM `leads_source` WHERE `company_id`='%s' AND `source_id`='%s'",
                     mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $id));
             $update_message = sprintf("UPDATE `leads_source` SET `source_sort_order`='%s' WHERE `source_id`='%s' AND `company_id`='%s'",
                mysqli_real_escape_string($this->db,$sort_order), mysqli_real_escape_string($this->db,$id), mysqli_real_escape_string($this->db,$company_id));
        }
         
        $res = mysqli_query($this->db, $query);
        if(!$res){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($res);
            if($num_rows==0){
                $error = array('status' => "Failed", "msg" => "Details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }
       
        $result = mysqli_query($this->db, $update_message);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
        $leads_details = $this->getleadsProgramDetails($company_id, 1); 
        $out = array('status' => "Success", 'msg' => $leads_details['msg']);
                // If success everythig is good send header as "OK" and user details
        $this->response($this->json($out), 200);
    }
    
    public function checkparticipantexistandDelete($company_id, $id, $type){
        $s_lead_details = [];
        if($type == 'pi'){
             $query = sprintf("SELECT * FROM `leads_registration` WHERE `company_id`='%s' AND `program_id`='%s'",
                     mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $id));
               $update_message = sprintf("UPDATE `leads_program` SET `deleted_flg`='D' WHERE `program_id`='%s' AND `company_id`='%s'",
                   mysqli_real_escape_string($this->db,$id), mysqli_real_escape_string($this->db,$company_id));
               $category = 'Program Interest Successfully Deleted';
               $category1 = 'Program Interest Can Not Be Deleted';
        }else if($type == 'ls'){
            $query = sprintf("SELECT * FROM `leads_registration` WHERE `company_id`='%s' AND `source_id`='%s'",
                     mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $id));
             $update_message = sprintf("UPDATE `leads_source` SET `deleted_flg`='D' WHERE `source_id`='%s' AND `company_id`='%s'",
                   mysqli_real_escape_string($this->db,$id), mysqli_real_escape_string($this->db,$company_id));
             $category = 'Lead Source Successfully Deleted';
             $category1 = 'Lead Source Can Not Be Deleted';
        }
         
        $res = mysqli_query($this->db, $query);
        if(!$res){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                if ($num_rows >= 100) {
                    $error = array('status' => "Success", 'msg' => array('participant_exist' => 'C'), "msgs" => "$category1");
                    $this->response($this->json($error), 200);
                } else {
                    $leads_details = $this->getleadsProgramDetails($company_id, 1);
                    $leads_details['msg']['participant_exist'] = 'Y';
                    $leads_details['msg']['type'] = $type;
                    if ($type == 'pi') {
                        for ($i = 0; $i <= count($leads_details['msg']['program_interest']) - 1; $i++) {
                            if ($id == $leads_details['msg']['program_interest'][$i]['id']) {
                                $old_lead = array_splice($leads_details['msg']['program_interest'], $i, 1);
                                break;
                            }
                        }
                    } elseif ($type == 'ls') {
                        for ($i = 0; $i <= count($leads_details['msg']['lead_source']) - 1; $i++) {
                            if ($id == $leads_details['msg']['lead_source'][$i]['id']) {
                                $old_lead = array_splice($leads_details['msg']['lead_source'], $i, 1);
                                break;
                            }
                        }
                    }
                    $error = array('status' => "Success", "msg" => $leads_details['msg'], "to_be_deleted" => $old_lead);
                    $this->response($this->json($error), 200);
                }
            }
        }
       
        $result = mysqli_query($this->db, $update_message);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => $update_message);
            $this->response($this->json($error), 200);
        }
        $leads_details = $this->getleadsProgramDetails($company_id, 1); 
        $leads_details['msg']['participant_exist'] = 'N';
         $out = array('status' => "Success", 'msg' => $leads_details['msg'], 'msgs' => $category);
                // If success everythig is good send header as "OK" and user details
        $this->response($this->json($out), 200);
    }
    
    public function addIndividualLeadsMember($company_id, $program_id, $source_id, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $p_firstname, $p_lastname, $leads_status, $include_campaign_email, $reg_type_user, $reg_version_user, $reg_entry_type){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        
        $current_date = date("Y-m-d");
        if ($program_id == 1) {
            $n_query1 = sprintf("select `program_id` from `leads_program` where `company_id`='%s' and `program_name`='Not Specified' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db,$company_id));
            $n_result1 = mysqli_query($this->db, $n_query1);
            if (!$n_result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows1 = mysqli_num_rows($n_result1);
                if ($num_rows1 == 1) {
                    $result_obj = mysqli_fetch_object($n_result1);
                    $program_id = $result_obj->program_id;
                }
            }
        }
        if ($source_id == 1) {
            $n_query2 = sprintf("select `source_id` from `leads_source` where `company_id`='%s' and `source_name`='Not Specified' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db,$company_id));
            $n_result2 = mysqli_query($this->db, $n_query2);
            if (!$n_result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows2 = mysqli_num_rows($n_result2);
                if ($num_rows2 == 1) {
                    $result_obj1 = mysqli_fetch_object($n_result2);
                    $source_id = $result_obj1->source_id;
                }
            }
        }
         $this->addLeadsDimensions($company_id, $program_id, $source_id, '');
          $checkold=sprintf("SELECT Max(zap_reg_id)+1 zap_count FROM `leads_registration`  WHERE  `company_id`='%s' ",
                mysqli_real_escape_string($this->db, $company_id));
        $resultold = mysqli_query($this->db, $checkold);
        if (!$resultold) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$checkold");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
             if(mysqli_num_rows($resultold)>0){
                $old_value= mysqli_fetch_object($resultold);
                $zap_reg_id = $old_value->zap_count;
            }
        }

        $query = sprintf("INSERT INTO `leads_registration`(`company_id`, `program_id`, `source_id`,`zap_reg_id`, `leads_status`, `buyer_first_name`, `buyer_last_name`, `buyer_phone`, `buyer_email`, `participant_firstname`, `participant_lastname`, 
                `registration_date`, `include_campaign_email`, `leads_reg_type_user`, `leads_reg_version_user`, `leads_entry_type`) 
                VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id), 
                mysqli_real_escape_string($this->db, $source_id), mysqli_real_escape_string($this->db, $zap_reg_id),mysqli_real_escape_string($this->db, $leads_status), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_email), 
                mysqli_real_escape_string($this->db, $p_firstname), mysqli_real_escape_string($this->db, $p_lastname),  mysqli_real_escape_string($this->db, $current_date),
                mysqli_real_escape_string($this->db, $include_campaign_email), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $reg_entry_type));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $err_no = mysqli_errno($this->db);
            if($err_no == 1062){
                sleep(0.5);
                $this->addIndividualLeadsMember($company_id, $program_id, $source_id, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $p_firstname, $p_lastname, $leads_status, $include_campaign_email, $reg_type_user, $reg_version_user, $reg_entry_type);
            }
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $leads_reg_id = mysqli_insert_id($this->db);

            $activity_text = 'Registration date.';
            $activity_type="Individual Registration";
            if(!empty($buyer_email) && $include_campaign_email=='Y'){
                $this->campaignSendmail($leads_reg_id,$company_id,$buyer_email);
            }


            $curr_date = gmdate("Y-m-d H:i:s");
            $insert_history = sprintf("INSERT INTO `leads_history`(`company_id`, `leads_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id),  $activity_type, 
                    mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
            $result_history = mysqli_query($this->db, $insert_history);
            if (!$result_history) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }

            $this->updateLeadsDimensionsMembers($company_id, $program_id, $source_id, $leads_status, '', $leads_reg_id, '', '');
            
            //  For zapier Implementation
            if($include_campaign_email === 'Y'){
                $zap_include_campaign_email = 'Yes';
            }else{
                $zap_include_campaign_email = 'No';
            }
            $target_url_arr = [];
            $zapsql = sprintf("SELECT target_url from zap_subscribe where company_id='%s' AND (event_type='A' OR event_type='B') AND category_type='L' AND deleted_flag !='Y'", mysqli_real_escape_string($this->db, $company_id));
            $zapresult = mysqli_query($this->db, $zapsql);
            if (!$zapresult) {
                $error = array('status' => "Failed","msg" => "Internal Server Error.", "query" => "$zapsql");
                log_info($this->json($error));
            } else {
                $zap_num_of_rows = mysqli_num_rows($zapresult);
                if ($zap_num_of_rows > 0) {
                    while ($zaprow = mysqli_fetch_assoc($zapresult)) {
                        $target_url_arr[] = $zaprow['target_url'];
                    }
                    $tmpkey_arr = $arr_check = [];
                    for($i=0;$i<count($target_url_arr);$i++){
                        $tmp = explode( '/', $target_url_arr[$i]);
                        $last_two = array_splice($tmp,count($tmp)-3);
                        $tmpkey = implode("/",$tmp);
                        $tmpkey .= '/'.$last_two[0].'/';
                        if(!in_array($tmpkey,$tmpkey_arr)){
                            $tmpkey_arr[] = $tmpkey;
                        }
                        $arr_check[$tmpkey][] = $last_two[1];
                    }
                    for ($i = 0; $i < count($tmpkey_arr); $i++) {
                        foreach ($arr_check as $key => $value) {
                            if ($tmpkey_arr[$i] == $key) {
                                for ($j = 0; $j < count($value); $j++) {
                                    $tmpkey_arr[$i] = $tmpkey_arr[$i].$value[$j];
                                    if($j!== count($value) -1){
                                        $tmpkey_arr[$i] = $tmpkey_arr[$i].',';
                                    }else if($j == count($value)-1){
                                        $tmpkey_arr[$i] = $tmpkey_arr[$i].'/';
                                    }
                                }
                            }
                        }
                    }
                    $auth[] = "Accept: application/json";
                    $auth[] = "Content-Type: application/json";
                    $new_query = sprintf("SELECT 'pi' as type, `program_name` name from `leads_program` where `program_id`='%s' and `deleted_flg`!='D'
                                UNION
                    SELECT 'ls' as type, `source_name` name from `leads_source` where `source_id`='%s' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db, $source_id));
                    $new_res = mysqli_query($this->db, $new_query);
                    if (!$new_res) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$new_query");
                        log_info($this->json($error_log));
                    } else {
                        if(mysqli_num_rows($new_res)>0){
                            while($row = mysqli_fetch_assoc($new_res)){
                                $type = $row['type'];
                                if ($type == 'pi') {
                                    $program_name = $row['name'];
                                } 
                                if ($type == 'ls') {
                                    $source_name = $row['name'];
                                }
                            }
                        }
                    }
                    for($i=0;$i<count($tmpkey_arr);$i++){
                        $temp_body=["Id"=>"$zap_reg_id","Buyer First Name" =>"$buyer_first_name","Buyer Last Name" =>"$buyer_last_name","Mobile Phone" =>"$buyer_phone","Email" =>"$buyer_email","Status" =>"Active","Program Interest" =>"$program_name","Source" =>"$source_name","Participant First Name" =>"$p_firstname","Participant Last Name" =>"$p_lastname","Communication Campaign Status"=>$zap_include_campaign_email];
                        $body = JSON_ENCODE($temp_body);

                        $ch = curl_init(); 
                        curl_setopt($ch, CURLOPT_URL, "$tmpkey_arr[$i]");
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $auth);

                        $result = curl_exec($ch);
                        $resultArr = json_decode($result, true);
                        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        if(!isset($resultArr['error']) && ($returnCode==200 && $returnCode!=0)){
                            $succ_log = array("Curl : " => "Success($returnCode)", "Method: " => "POST", "Url: " => $tmpkey_arr[$i], "Body: " => $body, "Result: " => $result);
                            log_info($this->json($succ_log));
                        }else{
                            $error_log = array("Curl error: " => curl_error($ch)."($returnCode)", "Method: " => "POST", "Url: " => $tmpkey_arr[$i], "Body: " => $body, "Result : " => $result);
                            log_info($this->json($error_log));
                        }
                        curl_close($ch);
                    } 
                }
            }
             // End Zapier Implementation
            $msg = array("status" => "Success", "msg" => "Leads has been added successfully.");
            $this->response($this->json($msg), 200);
        }
        date_default_timezone_set($curr_time_zone);
     
    }
    
    public function getleadscustomerDetailsbyfilter($company_id, $leads_status_tab, $search, $draw_table, $length_table, $start, $sorting,$leads_startdate_limit,$leads_enddate_limit,$leads_days_type){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        
//        $end_limit=500;
        $sort = $sorting[0]['column'];
        $sort_type = $sorting[0]['dir'];
        $search_value = $search['value'];
        $out = $output = [];
        $out['active'] = $out['enrolled'] = $out['notinterested'] = $out['recordsTotal']= $out['recordsFiltered'] = 0;
        $date_format = "'%b %d, %Y'";
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $last_updt_date = " DATE(CONVERT_TZ(lr.last_updt_dt,$tzadd_add,'$new_timezone'))";
//         $end_date = " DATE(CONVERT_TZ(tr.end_date,$tzadd_add,'$new_timezone'))";
//          $start_date = " DATE(CONVERT_TZ(tr.start_date,$tzadd_add,'$new_timezone'))";
        $created_date = "  DATE(CONVERT_TZ(lr.created_dt,$tzadd_add,'$new_timezone'))";
        $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $filter_date_column = "$created_date";
        $lday_text =$lday_text_c= '';
        $lday_text2=$lday_text3='';
         if ($leads_days_type == 1) {
            $lday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
        } elseif ($leads_days_type == 2) {
            $lday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
        } elseif ($leads_days_type == 3) {
            $lday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
        } elseif ($leads_days_type == 4) {
            $lday_text = "and DATE($filter_date_column) BETWEEN '" . $leads_startdate_limit . "' AND '" . $leads_enddate_limit . "'";
        } elseif ($leads_days_type == 5) {
            $lday_text = "";
        }

        if (!empty($search_value)) {
            $s_text = " and ( concat(`buyer_last_name`,', ',`buyer_first_name`) like  '%$search_value%'  or buyer_email like  '%$search_value%' or `buyer_phone` like  '%$search_value%' or  lr.`leads_status` like '%$search_value%'
                    or `participant_firstname` like  '%$search_value%' or `participant_lastname` like  '%$search_value%' or   DATE_FORMAT(lr.`registration_date`, '%b %d, %Y')  like  '%$search_value%' 
                    or  lp.`program_name` like  '%$search_value%'  or  ls.`source_name` like  '%$search_value%') $lday_text";
        } else {
            $s_text = "$lday_text";
        }

        $column = array(2 => "buyer_name", 3 => "participant_phone", 4 => "participant_email", 5 => "leads_status", 6 => "program_interest", 7 => "lead_source", 8 => "participant_first_name", 9 => "participant_last_name", 10 =>"DATE(lr.`registration_date`)");
       
        if ($leads_status_tab == 'A') { //ORDERS TAB
            $leads_tab = "lr.`leads_status` in ('A')";
        } else if ($leads_status_tab == 'E') {
            $leads_tab = "lr.`leads_status` in ('E')";
        } else if ($leads_status_tab == 'NI') {
            $leads_tab = "lr.`leads_status` in ('NI')";
        } 

         $query = sprintf("SELECT lr.`leads_reg_id` as id, lr.`company_id`, lr.`program_id`, lr.`source_id`, concat(`buyer_last_name`,', ',`buyer_first_name`) as buyer_name, `participant_firstname` participant_first_name, `participant_lastname` participant_last_name,
                IF(leads_reg_type_user='P', 'Control Panel', IF(leads_reg_type_user='M', 'Mobile App', IF(leads_reg_type_user='U', 'Website URL', ''))) leads_reg_type_user, 'N' as show_icon,
                 `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(lr.`registration_date`,%s) opt_in_date, DATE_FORMAT(($last_updt_date),%s) last_updt_dt, if(`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,b.`error`,
                IF(lr.`leads_status`='A','Active',IF(lr.`leads_status`='E','Enrolled','NotInterested')) leads_status, (select `source_name` from `leads_source` where `source_id`=lr.`source_id` and `company_id`=lr.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=lr.`program_id` and `company_id`=lr.`company_id`) program_interest
                FROM `leads_registration` lr 
                LEFT JOIN `leads_program` lp on lr.`company_id`=lp.`company_id` AND lr.`program_id`=lp.`program_id` 
                LEFT JOIN `leads_source` ls on lr.`company_id`=ls.`company_id` AND lr.`source_id`=ls.`source_id`
                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=lr.`company_id`) WHERE lr.`company_id`='%s' %s AND  $leads_tab AND lr.`deleted_flag`!='Y'
                 ORDER BY $column[$sort] $sort_type LIMIT $start, $length_table ", $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id), $s_text);
          $result = mysqli_query($this->db, $query);
//        log_info("ravi leads   ".$query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            $sql = sprintf("SELECT count(*) status_count, lr.leads_status, 'wo' as search from `leads_registration` lr where lr.company_id='%s' %s AND lr.`deleted_flag`!='Y' group by leads_status
                    UNION 
                    SELECT count(*) status_count, lr.leads_status, 'w' as search FROM `leads_registration` lr 
                LEFT JOIN `leads_program` lp on lr.`company_id`=lp.`company_id` AND lr.`program_id`=lp.`program_id` 
                LEFT JOIN `leads_source` ls on lr.`company_id`=ls.`company_id` AND lr.`source_id`=ls.`source_id`
                 WHERE lr.`company_id`='%s' %s %s AND lr.`deleted_flag`!='Y' group by leads_status", mysqli_real_escape_string($this->db, $company_id),$lday_text,mysqli_real_escape_string($this->db, $company_id),$lday_text,$s_text);
            $result_sql = mysqli_query($this->db, $sql);
            if (!$result_sql) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $numsql_rows = mysqli_num_rows($result_sql);
                if ($numsql_rows > 0) {
                    while ($rows1 = mysqli_fetch_assoc($result_sql)) {
                        $leads_status = $rows1['leads_status'];
                        $status_count = $rows1['status_count'];
                        if ($rows1['search'] == 'wo') {
                            if ($leads_status == 'A') {
                                $out['active'] = $status_count;
                            } elseif ($leads_status == 'E') {
                                $out['enrolled'] = $status_count;
                            } elseif ($leads_status == 'NI') {
                                $out['notinterested'] = $status_count;
                            }
                            if ($leads_status_tab == 'A') {
                                $out['recordsTotal'] = $out['active'];
                            } else if ($leads_status_tab == 'E') {
                                $out['recordsTotal'] = $out['enrolled'];
                            } else if ($leads_status_tab == 'NI') {
                                $out['recordsTotal'] = $out['notinterested'];
                            }
                        } elseif ($rows1['search'] == 'w') {
                            if ($leads_status_tab == 'A') {
                                $out['recordsFiltered'] =$out['active'];
                            } else if ($leads_status_tab == 'E') {
                                $out['recordsFiltered'] = $out['enrolled'];
                            } else if ($leads_status_tab == 'NI') {
                                $out['recordsFiltered'] = $out['notinterested'];
                            }
                        }
                    }
                }
            }
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    if ($leads_status_tab == 'A') {
                        $output[] = $row;
                    } else if ($leads_status_tab == 'E') {
                        $output[] = $row;
                    } else if ($leads_status_tab == 'NI') {
                        $output[] = $row;
                    } 
                }
                $out['draw'] = $draw_table;
                $out['data'] = $output;
                if (!empty($search_value)) {
                    $out['recordsFiltered'] = $num_rows;
                }
//                $res = array("status" => "Success", "msg" => $output, "count" => $count);
                $this->response($this->json($out), 200);
            } else {
                $out['draw'] = $draw_table;
                $out['data'] = $output;
//                $error = array('status' => "Failed", "msg" => "No participants registered yet.", "count" => $count);
                $this->response($this->json($out), 200); // If no records "No Content" status
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
     public function moveanddeletefieldindb($company_id, $id, $type, $old_lead){
         $old_id = $old_lead[0]['id'];
         $old_reg_id = $program_id = $source_id = $new_program_id = $new_source_id = $activity_type = $activity_text = '';
         $output =[];
        if($type == 'pi'){
             $query = sprintf("SELECT lr.*,(select `source_name` from `leads_source` where `source_id`=lr.`source_id` and `company_id`=lr.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=lr.`program_id` and `company_id`=lr.`company_id`) program_interest FROM `leads_registration` lr WHERE lr.`company_id`='%s' AND lr.`program_id`='%s' AND lr.`deleted_flag`!='Y'",
                     mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $old_id));
               $update_message = sprintf("UPDATE `leads_program` SET `deleted_flg`='D' WHERE `program_id`='%s' AND `company_id`='%s'",
                   mysqli_real_escape_string($this->db,$old_id), mysqli_real_escape_string($this->db,$company_id));
               $category = 'Program Interest Successfully moved and deleted';
             $lead_string = " `program_id`='%s'";
             $new_program_id = $id;
             $new_query = sprintf("SELECT `program_name` from `leads_program` where `program_id`='%s' and `deleted_flg`!='D'",
                          mysqli_real_escape_string($this->db, $id));
        }else if($type == 'ls'){
            $query = sprintf("SELECT lr.*,(select `source_name` from `leads_source` where `source_id`=lr.`source_id` and `company_id`=lr.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=lr.`program_id` and `company_id`=lr.`company_id`) program_interest FROM `leads_registration` lr WHERE lr.`company_id`='%s' AND lr.`source_id`='%s' AND lr.`deleted_flag`!='Y'",
                     mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $old_id));
             $update_message = sprintf("UPDATE `leads_source` SET `deleted_flg`='D' WHERE `source_id`='%s' AND `company_id`='%s'",
                   mysqli_real_escape_string($this->db,$old_id), mysqli_real_escape_string($this->db,$company_id));
             $category = 'Lead Source Successfully moved and deleted';
               $lead_string = " `source_id`='%s'";  
               $new_source_id = $id;
             $new_query = sprintf("SELECT `source_name` from `leads_source` where `source_id`='%s' and `deleted_flg`!='D'",
                          mysqli_real_escape_string($this->db, $id));
        }
         
        $res = mysqli_query($this->db, $query);
        if(!$res){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $temp = [];
                while ($row = mysqli_fetch_assoc($res)) {
                    $temp[] = $row['leads_reg_id'];
                    $output [] = $row;
                     $program_id = $row['program_id'];
                     $source_id = $row['source_id'];
                }
                $old_reg_id = implode(",", $temp);
                 $move_query = sprintf("UPDATE `leads_registration` SET $lead_string WHERE `leads_reg_id` in (%s) and `company_id`='%s'",
                        mysqli_real_escape_string($this->db,$id),   mysqli_real_escape_string($this->db,$old_reg_id), mysqli_real_escape_string($this->db,$company_id)); 
                
                $result_move = mysqli_query($this->db, $move_query);
                if (!$result_move) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$move_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $new_res = mysqli_query($this->db, $new_query);
                    if (!$new_res) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$new_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }else{
                        $new_row = mysqli_fetch_object($new_res);
                        if ($type == 'pi') { 
                            $program_name = $new_row->program_name;
                        }else if ($type == 'ls') {
                             $source_name = $new_row->source_name;
                        }
                    }
                    for ($i = 0; $i < count($temp); $i++) {
                        if ($type == 'pi') {                           //Reverse logic ..
                            $new_source_id = $output[$i]['source_id'];
                            $source_id = $output[$i]['source_id'];
                            $program_interest = $output[$i]['program_interest'];
                            if (empty($activity_type)) {
                                $activity_type = 'Move';
                                $activity_text = "Program Interest moved from $program_interest to $program_name";
                            }
                        } else if ($type == 'ls') {
                            $new_program_id = $output[$i]['program_id'];
                            $program_id = $output[$i]['program_id'];
                            $lead_source = $output[$i]['lead_source'];
                            if (empty($activity_type)) {
                                $activity_type = 'Move';
                                $activity_text = "Lead Source moved from $lead_source to $source_name";
                            }
                        }
                        $leads_reg_id = $temp[$i];
                        $leads_status = $output[$i]['leads_status'];
                        $leads_reg_date = $output[$i]['registration_date'];
                        $curr_date = gmdate("Y-m-d H:i:s");
                        $insert_history = sprintf("INSERT INTO `leads_history`(`company_id`, `leads_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')", 
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id), $activity_type, mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                        $result_history = mysqli_query($this->db, $insert_history);
                        if (!$result_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                        $this->addLeadsDimensions($company_id, $new_program_id, $new_source_id, $leads_reg_date);
                        $this->updateLeadsDimensionsMembers($company_id, $new_program_id, $new_source_id, $leads_status, '', $leads_reg_id, $program_id, $source_id);
//                        $old_check = array('old_status' => $leads_status);
//                        $this->updateLeadsDimensionsMembers($company_id, $program_id, $source_id, $old_check, '', $leads_reg_id);
                    }
                    $this->setDeletedflagForLeadsdimensions($company_id, $type, $old_id);
                }
            }
        }
       
        $result = mysqli_query($this->db, $update_message);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
        $leads_details = $this->getleadsProgramDetails($company_id, 1); 
        $out = array('status' => "Success", 'msg' => $leads_details['msg'], 'msgs' => $category);
                // If success everythig is good send header as "OK" and user details
        $this->response($this->json($out), 200);
    }
    public function createLeadsCSV($company_id){
        $opt_string = $header = '';
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        ob_clean();
        ob_start();
        $header.= "Buyer First Name,Buyer Last Name,Mobile Phone,Email,Participant First Name,Participant Last Name,Program Interest,Source";
        
        $file = "../../../uploads/lead.csv";
        $ifp = fopen($file, "w");
        fwrite($ifp, $header);
        fclose($ifp);
        header('Content-Type: application/x-download; charset=utf-8');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        readfile($file);
        unlink($file);
        date_default_timezone_set($curr_time_zone);
        exit();
    }
    
    /**
     Detects the end-of-line character of a string.
     @param string $str      The string to check.
     @param string $key      [io] Name of the detected eol key.
     @return string The detected EOL, or default one.
    */
   public static function detectEOL($str) {
       static $eols = array(
           "\n\r", // 0x0A - 0x0D - acorn BBC
           "\r\n", // 0x0D - 0x0A - Windows, DOS OS/2
           "\n", // 0x0A -      - Unix, OSX
           "\r" // 0x0D -      - Apple ][, TRS80
       );

       $curCount = 0;
       $curEol = '';
       foreach ($eols as $eol) {
           if (($count = substr_count($str, $eol)) > $curCount) {
               $curCount = $count;
               $curEol = $eol;
           }
       }
       return $curEol;
   }
   
    public function createLeadImportCSV($company_id, $arraycsv, $call_back) {
        $pgminterest = $pgminterest_id = $leadsource = $leadsource_id = $arraycsvfile = $csv_array = [];
        $mimes = array('text/csv','application/vnd.ms-excel','text/comma-separated-values', 'application/csv' , 'application/excel' , 'application/vnd.msexcel');
        $temp1 = explode(";", $arraycsv);
        $check = explode(":", $temp1[0]);
        $error_occured = false;
        if (in_array($check[1], $mimes)) { //decode csv file
            $pos = strpos($arraycsv, ';');
            if ($pos !== false) {
//                $arraycsv = base64_decode(preg_replace('#^data:text/\w+;base64,#i', '', $arraycsv));
                $encodeddata = explode("base64,", $arraycsv);
                $arraycsv = base64_decode($encodeddata[1]);
            }
            $eol_string = $this->detectEOL($arraycsv);
            $arraycsvfile = str_getcsv($arraycsv, $eol_string);
            $leadscheckheader = "Buyer First Name,Buyer Last Name,Mobile Phone,Email,Participant First Name,Participant Last Name,Program Interest,Source";
            if ($arraycsvfile[0] == $leadscheckheader) {
                
            } else {
                $out = array('status' => "Failed", 'msg' => "CSV template is not in the correct format. Please use the provided template.");
                log_info($this->JSON($out));
                $this->response($this->json($out), 200);
            }
            array_splice($arraycsvfile, 0, 1); //HeaderSplice
            if (!empty($arraycsvfile)) {
                $leadpgmarray = $this->getleadsProgramDetails($company_id, 1);
                $leadarray = $leadpgmarray['msg']['lead_source'];
                $programarray = $leadpgmarray['msg']['program_interest'];
                for ($k = 0; $k < count($leadarray); $k++) {
                    $leadsource[] = strtolower($leadarray[$k]['name']);
                    $leadsource_id[] = $leadarray[$k]['id'];
                }
                for ($k = 0; $k < count($programarray); $k++) {
                    $pgminterest[] = strtolower($programarray[$k]['name']);
                    $pgminterest_id[] = $programarray[$k]['id'];
                }
               for ($i = 0; $i < count($arraycsvfile); $i++) {
                    $value = explode(",", $arraycsvfile[$i]);
                    $k = '';
                    for ($l = 0; $l < count($value) - 1; $l++) {
                        $k .= ',';
                    }
                    if (trim($arraycsvfile[$i]) != $k) {
                        $missing_fields = "";
                        $record_no = $i + 1;
                        if ((empty(trim($value[0])) && empty(trim($value[1]))) || (empty(trim($value[2])) && empty(trim($value[3])))) {
                            $error_occured = true;
                            //checking which field is missing
                            if (empty(trim($value[0])) && empty(trim($value[1]))) {
                            $missing_fields .= 'Buyer First Name and Buyer Last Name.';
                            }
                            if (empty(trim($value[2])) && empty(trim($value[3]))) {
                            $missing_fields .= 'Buyer Email and Mobile Phone.';
                            }
                            $error_records[] = (object) array('record_number' => "$record_no", "missing_fields" => "$missing_fields");
                        } else {
                            for ($j = 0; $j < count($arraycsvfile); $j++) {
                                $individualvalue = explode(",", $arraycsvfile[$i]);
//                    For program Interest check
                                if (!empty($individualvalue[6])) {
                                    if (in_array(strtolower($individualvalue[6]), $pgminterest)) {
                                        $individualvalue[8] = $pgminterest_id[array_search(strtolower($individualvalue[6]), $pgminterest)];
                                    } else {
                                        $individualvalue[6] = $pgminterest[0];
                                        $individualvalue[8] = $pgminterest_id[0];
                                    }
                                } else {
                                    $individualvalue[6] = $pgminterest[0];
                                    $individualvalue[8] = $pgminterest_id[0];
                                }
//                    For Leadsource check
                                if (!empty($individualvalue[7])) {
                                    if (in_array(strtolower($individualvalue[7]), $leadsource)) {
                                        $individualvalue[9] = $leadsource_id[array_search(strtolower($individualvalue[7]), $leadsource)];
                                    } else {
                                        $individualvalue[7] = $leadsource[0];
                                        $individualvalue[9] = $leadsource_id[0];
                                    }
                                } else {
                                    $individualvalue[7] = $leadsource[0];
                                    $individualvalue[9] = $leadsource_id[0];
                                }
                            }
                            $csv_array[] = $individualvalue;
                        }
                    }
                }

                if ($error_occured) {
                    $out = array('status' => "Failed", 'msg' => "CSV template is not in the correct format. Please use the provided template.", 'msgs' => "Required field is missing at records :", 'records' => $error_records);
                    log_info($this->JSON($out));
                    $this->response($this->json($out), 200);
                }
                //success csv can be imported
                if ($call_back == 0) {
                    $out = array('status' => "Success", 'msg' => "csv can be imported");
                    $this->response($this->json($out), 200);
                } else {
                    $out = array('status' => "Success", 'msg' => "csv can be imported", "csv_array" => $csv_array);
                    return $out;
                }
            } else {
                $out = array('status' => "Failed", 'msg' => "No Record found in csv");
                log_info($this->JSON($out));
                $this->response($this->json($out), 200);
            }
        } else {
            $out = array('status' => "Failed", 'msg' => "CSV template is not in the correct format. Please use the provided template.");
            log_info($this->JSON($out));
            $this->response($this->json($out), 200);
        }
    }

    public function InsertImportedCSV($company_id, $arraycsv, $include_campaign_email, $recursive_check) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $current_date = date("Y-m-d");
        if ($recursive_check == 0) {
            $insert_value = $this->createLeadImportCSV($company_id, $arraycsv, 1);
            $insertvalue = $insert_value['csv_array'];
        } else {
            $insertvalue = $arraycsv;
        }
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = $version;
        if ($include_campaign_email === 'Y') {
            $zap_include_campaign_email = 'Yes';
        }else{
             $zap_include_campaign_email = 'No';
        }
           $checkold=sprintf("SELECT Max(zap_reg_id)+1 zap_count FROM `leads_registration`  WHERE  `company_id`='%s' ",
                mysqli_real_escape_string($this->db, $company_id));
        $resultold = mysqli_query($this->db, $checkold);
        if (!$resultold) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$checkold");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
             if(mysqli_num_rows($resultold)>0){
                $old_value= mysqli_fetch_object($resultold);
                $zap_reg_id = $old_value->zap_count;
            }
        }
        for ($j = 0; $j < count($insertvalue); $j++) {
            $insertvalues = explode(",", implode(',', $insertvalue[$j]));
             $this->addLeadsDimensions($company_id, $insertvalues[8], $insertvalues[9], '');
//            $buyer_name = "$insertvalues[0]"." $insertvalues[1]";
            $query = sprintf("INSERT INTO `leads_registration`(`company_id`, `program_id`, `source_id`, `zap_reg_id`,`leads_status`, `buyer_first_name`, `buyer_last_name`, `buyer_phone`, `buyer_email`, `participant_firstname`, `participant_lastname`, 
                `registration_date`, `include_campaign_email`, `leads_reg_type_user`, `leads_reg_version_user`, `leads_entry_type`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $insertvalues[8]), mysqli_real_escape_string($this->db, $insertvalues[9]), mysqli_real_escape_string($this->db, $zap_reg_id),mysqli_real_escape_string($this->db, 'A'), 
                    mysqli_real_escape_string($this->db, $insertvalues[0]), mysqli_real_escape_string($this->db, $insertvalues[1]), mysqli_real_escape_string($this->db, $insertvalues[2]), mysqli_real_escape_string($this->db, $insertvalues[3]), 
                    mysqli_real_escape_string($this->db, $insertvalues[4]), mysqli_real_escape_string($this->db, $insertvalues[5]), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $include_campaign_email), 
                    mysqli_real_escape_string($this->db, 'P'), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, 'C'));
            $result = mysqli_query($this->db, $query);
            if (!$result) {
                $err_no = mysqli_errno($this->db);
                if ($err_no == 1062) {
                    sleep(0.5);
//                    $return_insert_arr =
                            array_splice($insertvalue,$j);
                    $this->InsertImportedCSV($company_id, $insertvalue, $include_campaign_email,1);
                }
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                $leads_reg_id = mysqli_insert_id($this->db);
                $activity_text = 'Registration date.';
                        $activity_type="CSV Registration";
                    if(!empty($insertvalues[3]) && $include_campaign_email=='Y'){
                          $this->campaignSendmail($leads_reg_id,$company_id,$insertvalues[3]);
                    }
                    $curr_date = gmdate("Y-m-d H:i:s");
                    $insert_history = sprintf("INSERT INTO `leads_history`(`company_id`, `leads_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id),  $activity_type, 
                            mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                    $result_history = mysqli_query($this->db, $insert_history);
                    if (!$result_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                $this->updateLeadsDimensionsMembers($company_id, $insertvalues[8], $insertvalues[9], 'A', '', $leads_reg_id, '', '');
                $temp_body[] = ["Id"=>"$zap_reg_id","Buyer First Name" =>$insertvalues[0],"Buyer Last Name" =>$insertvalues[1],"Mobile Phone" =>$insertvalues[2],"Email" =>$insertvalues[3],"Status" =>"Active","Program Interest" =>$insertvalues[6],"Source" =>$insertvalues[7],"Participant First Name" =>$insertvalues[4],"Participant Last Name" =>$insertvalues[5],"Communication Campaign Status" =>$zap_include_campaign_email];
                $zap_reg_id++;
            }
        }
        
        //  For zapier Implementation
            $target_url_arr = [];
            $zapsql = sprintf("SELECT target_url from zap_subscribe where company_id='%s' AND (event_type='A' OR event_type='B') AND category_type='L' AND deleted_flag !='Y'", mysqli_real_escape_string($this->db, $company_id));
            $zapresult = mysqli_query($this->db, $zapsql);
            if (!$zapresult) {
                $error = array('status' => "Failed","msg" => "Internal Server Error.", "query" => "$zapsql");
                log_info($this->json($error));
            } else {
                $zap_num_of_rows = mysqli_num_rows($zapresult);
                if ($zap_num_of_rows > 0) {
                    while ($zaprow = mysqli_fetch_assoc($zapresult)) {
                        $target_url_arr[] = $zaprow['target_url'];
                    }
                    $tmpkey_arr = $arr_check = [];
                    for($i=0;$i<count($target_url_arr);$i++){
                        $tmp = explode( '/', $target_url_arr[$i]);
                        $last_two = array_splice($tmp,count($tmp)-3);
                        $tmpkey = implode("/",$tmp);
                        $tmpkey .= '/'.$last_two[0].'/';
                        if(!in_array($tmpkey,$tmpkey_arr)){
                            $tmpkey_arr[] = $tmpkey;
                        }
                        $arr_check[$tmpkey][] = $last_two[1];
                    }
                    for ($i = 0; $i < count($tmpkey_arr); $i++) {
                        foreach ($arr_check as $key => $value) {
                            if ($tmpkey_arr[$i] == $key) {
                                for ($j = 0; $j < count($value); $j++) {
                                    $tmpkey_arr[$i] = $tmpkey_arr[$i].$value[$j];
                                    if($j!== count($value) -1){
                                        $tmpkey_arr[$i] = $tmpkey_arr[$i].',';
                                    }else if($j == count($value)-1){
                                        $tmpkey_arr[$i] = $tmpkey_arr[$i].'/';
                                    }
                                }
                            }
                        }
                    }
                    $auth[] = "Accept: application/json";
                    $auth[] = "Content-Type: application/json";
                    for($j=0;$j<count($tmpkey_arr);$j++){
                        $split_body = array_chunk($temp_body, 90);
                        for($i=0;$i<count($split_body);$i++){
                            $body = JSON_ENCODE($split_body[$i]);
                            $ch = curl_init(); 
                            curl_setopt($ch, CURLOPT_URL, "$tmpkey_arr[$j]");
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $auth);

                            $result = curl_exec($ch);
                            $resultArr = json_decode($result, true);
                            $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);

                            if(!isset($resultArr['error']) && ($returnCode==200 && $returnCode!=0)){
                                $succ_log = array("Curl : " => "Success($returnCode)", "Method: " => "POST", "Url: " => $tmpkey_arr[$j], "Body: " => $body, "Result: " => $result);
                                log_info($this->json($succ_log));
                            }else{
                                $error_log = array("Curl error: " => curl_error($ch)."($returnCode)", "Method: " => "POST", "Url: " => $tmpkey_arr[$j], "Body: " => $body, "Result : " => $result);
                                log_info($this->json($error_log));
                            }
                            curl_close($ch);
                        }  
                    }
                }
            }
//            End of zapier
        
        $error = array('status' => "Success", "msg" => "Leads successfully imported.");
        $this->response($this->json($error), 200);
    }

    public function getLeadsRegDetails($company_id, $leads_reg_id, $call_back) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
//        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $created_date = " DATE(CONVERT_TZ(`created_dt`,$tzadd_add,'$new_timezone'))";
        
        $output = $output['All'] =$output3 =$output['reg_columns']= $cards = [];
        $sql_reg = sprintf("SELECT `leads_reg_id`, `company_id`, `leads_status`,`buyer_first_name`, `buyer_last_name`,
                    `buyer_email`, `buyer_phone`, `registration_date`, (select `source_name` from `leads_source` where `source_id`=lr.`source_id` and `company_id`=lr.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=lr.`program_id` and `company_id`=lr.`company_id`) program_interest,
                    `participant_firstname` participant_first_name, `participant_lastname` participant_last_name,
                     `leads_reg_type_user`, `leads_reg_version_user`, $created_date created_dt ,`program_id`,`source_id`
                   FROM `leads_registration` lr  WHERE `company_id`='%s' AND `leads_reg_id`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id));
        $result_reg_details = mysqli_query($this->db, $sql_reg);
        if (!$result_reg_details) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_reg");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_reg_details);
            if ($num_rows > 0) {
                $output = mysqli_fetch_assoc($result_reg_details);
            } else {
                $error = array('status' => "Failed", "msg" => "Leads details doesn't exist.");
                if($call_back==0){
                    $this->response($this->json($error), 200); // If no records "No Content" status  
                }else{
                    return $error;
                }
            }
            
            $output2 = $this->getleadsProgramDetails($company_id, 1);
            if($output2['status']=='Success'){
                $out = $output2['msg'];
                $output['All'] = $out;
            }else{
                $error = array('status' => "Failed", "msg" => "Leads program details doesn't exist.");
                if($call_back==0){
                    $this->response($this->json($error), 200);
                }else{
                    return $error;
                }
            }
            $out = array('status' => "Success", 'msg' => $output);
            if($call_back==0){
                $this->response($this->json($out), 200);
            }else{
                return $out;
            }
        }
    }
    
     public function getLeadsHistoryDetails($company_id, $leads_reg_id, $call_back){    //call_back 0 - echo & exit, 1 - return
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $activity_dt_tm = " IF(TIME(`activity_date_time`)!='00:00:00', DATE(CONVERT_TZ(`activity_date_time`,$tzadd_add,'$new_timezone')), DATE(`activity_date_time`))";
        
        
        $output = []; 
        $sql_reg = sprintf("SELECT `leads_history_id`,`activity_type`, `activity_text`, $activity_dt_tm activity_date FROM `leads_history`
                WHERE company_id='%s' AND leads_reg_id ='%s' AND `deleted_flag`!='D' ORDER BY `activity_date_time`, `leads_history_id`", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id));
        $result_reg_details = mysqli_query($this->db, $sql_reg);
        if (!$result_reg_details) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_reg");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result_reg_details);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_reg_details)) {                   
                    $output[] = $row;
//                    $output['activity_date'] = $row['activity_date_time'];
                }
                $out = array('status' => "Success", 'msg' => $output);
                if($call_back==0){
                    $this->response($this->json($out), 200);
                }else{
                    return $out;
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Leads history details doesn't exist.");
                if($call_back==0){
                    $this->response($this->json($error), 200); // If no records "No Content" status  
                }else{
                    return $error;
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
     public function addorupdateLeadsHistoryNote($company_id,$leads_reg_id,$status,$history_id,$note_text){
         $curr_date = gmdate('Y-m-d H:i:s');
        if($status=='add'){
            $query = sprintf("INSERT INTO `leads_history`(`company_id`, `leads_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id), mysqli_real_escape_string($this->db, 'note'), 
                    mysqli_real_escape_string($this->db, $note_text), mysqli_real_escape_string($this->db, $curr_date));
            $res_msg = "Note added successfully.";
        }else{
            $sql = sprintf("SELECT * FROM `leads_history` WHERE `leads_history_id`='%s' AND `company_id`='%s' AND `leads_reg_id`='%s'",
                    mysqli_real_escape_string($this->db, $history_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id));
            $res = mysqli_query($this->db, $sql);
            if (!$res) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.","query" => "$sql");
                $this->response($this->json($error), 200);
            }else{
                $num_rows = mysqli_num_rows($res);
                if($num_rows==0){
                    $error = array('status' => "Failed", "msg" => "History note details not available.");
                    $this->response($this->json($error), 200);
                }
            }
            if($status=='update'){
                $query = sprintf("UPDATE `leads_history` SET `activity_text`='%s', `activity_date_time`='%s' WHERE `leads_history_id`='%s' AND `company_id`='%s' AND `leads_reg_id`='%s'",
                        mysqli_real_escape_string($this->db, $note_text), mysqli_real_escape_string($this->db, $curr_date),
                        mysqli_real_escape_string($this->db, $history_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id));
                $res_msg = "Note updated successfully.";
            }elseif($status=='delete'){
                $query = sprintf("UPDATE `leads_history` SET `deleted_flag`='D' WHERE `leads_history_id`='%s' AND `company_id`='%s' AND `leads_reg_id`='%s'",
                        mysqli_real_escape_string($this->db, $history_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id));
                $res_msg = "Note deleted successfully.";
            }
        }
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $leads_history = $this->getLeadsHistoryDetails($company_id, $leads_reg_id,1);
            $log = array("status" => "Success", "msg" => $res_msg, "leads_history" => $leads_history);
            $this->response($this->json($log),200);
        }
    }
    
     public function sendGroupPushMailForLead($company_id, $subject, $message, $attached_file, $file_name, $from, $leads_status_tab, $leads_reg_id, $all_select_flag, $search_value, $leads_startdate_limit, $leads_enddate_limit, $leads_days_type) {
//
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

//        $end_limit=500;
        $output1 = $output2 = [];
        $reg_id_list = '';
        $date_format = "'%b %d, %Y'";
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $last_updt_date = " DATE(CONVERT_TZ(lr.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $created_date = "  DATE(CONVERT_TZ(lr.created_dt,$tzadd_add,'$new_timezone'))";
        $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $filter_date_column = "$created_date";
        $lday_text = $lday_text_c = '';
        if ($leads_days_type == 1) {
            $lday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
        } elseif ($leads_days_type == 2) {
            $lday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
        } elseif ($leads_days_type == 3) {
            $lday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
        } elseif ($leads_days_type == 4) {
            $lday_text = "and DATE($filter_date_column) BETWEEN '" . $leads_startdate_limit . "' AND '" . $leads_enddate_limit . "'";
        } elseif ($leads_days_type == 5) {
            $lday_text = "";
        }

        if (!empty($search_value)) {
            $s_text = " and ( concat(`buyer_first_name`,' ',`buyer_last_name`) like  '%$search_value%'   or buyer_email like  '%$search_value%' or `buyer_phone` like  '%$search_value%' or  lr.`leads_status` like '%$search_value%'
                    or `participant_firstname` like  '%$search_value%' or `participant_lastname` like  '%$search_value%' or   DATE_FORMAT(lr.`registration_date`, '%b %d, %Y')  like  '%$search_value%' 
                    or  lp.`program_name` like  '%$search_value%'  or  ls.`source_name` like  '%$search_value%') $lday_text";
        } else {
            $s_text = "$lday_text";
        }

        if ($leads_status_tab == 'A') { //ORDERS TAB
            $leads_tab = "lr.`leads_status` in ('A')";
        } else if ($leads_status_tab == 'E') {
            $leads_tab = "lr.`leads_status` in ('E')";
        } else if ($leads_status_tab == 'NI') {
            $leads_tab = "lr.`leads_status` in ('NI')";
        }
        if ($all_select_flag == 'Y') {
            
            $query = sprintf("SELECT lr.`leads_reg_id` as id, lr.`company_id`, lr.`program_id`, lr.`source_id`, `buyer_first_name`, `buyer_last_name`, `participant_firstname` participant_first_name, `participant_lastname` participant_last_name,
                IF(leads_reg_type_user='P', 'Control Panel', IF(leads_reg_type_user='M', 'Mobile App', IF(leads_reg_type_user='U', 'Website URL', ''))) leads_reg_type_user, 'N' as show_icon,
                 `buyer_email` as participant_email, `buyer_phone` participant_phone, DATE_FORMAT(lr.`registration_date`,%s) opt_in_date, DATE_FORMAT(($last_updt_date),%s) last_updt_dt, if(`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,b.`error`,
                IF(lr.`leads_status`='A','Active',IF(lr.`leads_status`='E','Enrolled','NotInterested')) leads_status, (select `source_name` from `leads_source` where `source_id`=lr.`source_id` and `company_id`=lr.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=lr.`program_id` and `company_id`=lr.`company_id`) program_interest, c.`company_name`, c.`email_id` company_email_id
                FROM `leads_registration` lr 
                LEFT JOIN `company` c ON lr.`company_id` = c.`company_id`
                LEFT JOIN `leads_program` lp on lr.`company_id`=lp.`company_id` AND lr.`program_id`=lp.`program_id` 
                LEFT JOIN `leads_source` ls on lr.`company_id`=ls.`company_id` AND lr.`source_id`=ls.`source_id`
                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=lr.`company_id`)  WHERE lr.`company_id`='%s' %s AND  $leads_tab AND `buyer_email` is NOT NULL and trim(buyer_email)!='' AND lr.`deleted_flag`!='Y' group by buyer_email" ,
                    $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id), $s_text);
       // LOG_INFO($query);
           
        } else {
            for ($i = 0; $i < count($leads_reg_id); $i++) {
                $output2[] = $leads_reg_id[$i]['id'];
            }
            $reg_id_list = implode(",", $output2);
            $query = sprintf("SELECT lr.`leads_reg_id` as id, lr.`company_id`, lr.`program_id`, lr.`source_id`, `buyer_first_name`, `buyer_last_name`, `participant_firstname` participant_first_name, `participant_lastname` participant_last_name,
                IF(leads_reg_type_user='P', 'Control Panel', IF(leads_reg_type_user='M', 'Mobile App', IF(leads_reg_type_user='U', 'Website URL', ''))) leads_reg_type_user, 'N' as show_icon,
                 `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(lr.`registration_date`,%s) opt_in_date, DATE_FORMAT(($last_updt_date),%s) last_updt_dt, if(`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,b.`error`,
                IF(lr.`leads_status`='A','Active',IF(lr.`leads_status`='E','Enrolled','NotInterested')) leads_status, (select `source_name` from `leads_source` where `source_id`=lr.`source_id` and `company_id`=lr.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=lr.`program_id` and `company_id`=lr.`company_id`) program_interest, c.`company_name`, c.`email_id` company_email_id
                FROM `leads_registration` lr 
                LEFT JOIN `company` c ON lr.`company_id` = c.`company_id`
                LEFT JOIN `leads_program` lp on lr.`company_id`=lp.`company_id` AND lr.`program_id`=lp.`program_id` 
                LEFT JOIN `leads_source` ls on lr.`company_id`=ls.`company_id` AND lr.`source_id`=ls.`source_id`
                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=lr.`company_id`) WHERE lr.`company_id`='%s' %s AND  $leads_tab AND `buyer_email` is NOT NULL  AND lr.`leads_reg_id` in (%s) and trim(buyer_email)!='' group by buyer_email",
                    $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id), $s_text, mysqli_real_escape_string($this->db, $reg_id_list));
        }
        $res1 = mysqli_query($this->db, $query);
        $Bounced_count = $one_time_check = 0;
        $reply_to = '';
        if (!$res1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", 'query' => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($res1);
            if ($num_rows > 0) {
                while ($rows1 = mysqli_fetch_assoc($res1)) {
                    $check = 'Y';
                    if($one_time_check==0){
                        $one_time_check++;
                        $reply_to = $rows1['company_email_id'];
                    }
                    if($rows1['bounce_flag'] == $check){
                        $Bounced_count++;                        
                    }else{
                        $output1[] = $rows1;
                    }
                }
//                if (empty($reg_id_list)) {
//                    for ($i = 0; $i < count($output1); $i++) {
//                        $output2[] = $output1[$i]['id'];
//                    }
//                    $reg_id_list = implode(",", $output1);
//                }
            }
        }
        $send_count = 0;
        $footer_detail = $this->getunsubscribefooter($company_id);
        for ($idx = 0; $idx < count($output1); $idx++) {
            $obj = (Array) $output1[$idx];
           
            $user_emailid = $obj["participant_email"];
            $cmp_name = $obj["company_name"];
            //echo "******count******".$idx;
            $sendEmail_status = $this->sendEmail($user_emailid, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file, '',$company_id,'',$footer_detail);
            if ($sendEmail_status['status'] == "true") {
                $send_count++;
                $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200);
            } else {
                $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $user_emailid, "Email Message" => $sendEmail_status['mail_status']);
                log_info($this->json($error_log));
//                        $this->response($this->json($error_log), 200); // If no records "No Content" status
            }
        }

        $countForTotal = count($output1);
        if ($countForTotal == $send_count && $Bounced_count==0) {
            $push_msg = "Email Sent Successfully";
        } elseif ($send_count == 0) {
            $push_msg = " All " . $countForTotal. " email sending failed.";
        } else {
            
            $push_msg =$send_count." sent. ".$Bounced_count." failed due to invalid address.";
            //$push_msg = $countForTotal - $send_count . ' of ' . $countForTotal. ' email sending failed. Others sent successfully';
        }
//                if(empty($push_msg)){
//                    $push_msg="Email Sent Successfully";
//                }
        $msg = array('status' => "Success", "msg" => $push_msg);
        $this->response($this->json($msg), 200);
        if ($send_count == 0) {
            $push_msg = " All " . $countForTotal. " email sending failed.";
            $error = array('status' => "Failed", "msg" => $push_msg);
            $this->response($this->json($error), 200); // If no records "No Content" status
        }
    }

    private function sendEmail($to, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file,$cc_email_list,$company_id,$type,$footer_detail) {
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }            
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
            $mail->Subject = $subject;
            if (!empty($company_id) && $type != 'subscribe') {
                $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                $curr_loc = implode('/', explode('/', $curr_loc, -4));
//                $tomail = $to;
//                if (!empty(trim($cc_email_list))) {
//                    $tomail = base64_encode($to . "," . "$cc_email_list");
//                } else {
                    $tomail = base64_encode($to);
//                }
                $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/EmailSubscription.php?mail=$tomail?comp_id=$company_id?type=U";
                $message = "
            <html>
            <body>
                <p>$message</p><br><br>
                    <footer style='font-size: 13px;color: #746f6f;'>
                    <center>
                    <p>$footer_detail
                    </p>
                    <a href='$activation_link' target='_blank' style='color: #746f6f; !important;text-decoration: none !important;'><u style='color: #746f6f;'>Unsubscribe $to</u></a>
                    </center>
                    </footer>
            </body>
            </html>
            ";
            }
            $mail->Body = $message;
            if(!empty($file_name)){
                $mail->AddStringAttachment(base64_decode($attached_file), $file_name, 'base64');
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $verify_email_for_bounce = $this->verifyBouncedEmailForCC($cc_addresses[$init], $company_id);
                         if($verify_email_for_bounce==1){
                               $mail->AddCC($cc_addresses[$init]);
                         }
//                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    public function createLeadSelectionCSV($company_id,$leads_reg_id, $leads_status_tab, $all_select_flag,$search_value,$leads_startdate_limit,$leads_enddate_limit,$leads_days_type){
        if ($all_select_flag == 'Y') {
            $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        
//        $end_limit=500;
         $date_format = "'%b %d, %Y'";
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $last_updt_date = " DATE(CONVERT_TZ(lr.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $created_date = "  DATE(CONVERT_TZ(lr.created_dt,$tzadd_add,'$new_timezone'))";
        $currdate_add = "  DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $filter_date_column = "$created_date";
        $lday_text =$lday_text_c= '';
        if ($leads_days_type == 1) {
            $lday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 30 DAY AND ($currdate_add)";
        } elseif ($leads_days_type == 2) {
            $lday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 60 DAY AND ($currdate_add)";
        } elseif ($leads_days_type == 3) {
            $lday_text = "and DATE($filter_date_column) BETWEEN ($currdate_add) - INTERVAL 90 DAY AND ($currdate_add)";
        } elseif ($leads_days_type == 4) {
            $lday_text = "and DATE($filter_date_column) BETWEEN '" . $leads_startdate_limit . "' AND '" . $leads_enddate_limit . "'";
        } elseif ($leads_days_type == 5) {
            $lday_text = "";
        }

        if (!empty($search_value)) {
            $s_text = " and ( concat(`buyer_first_name`,' ',`buyer_last_name`) like  '%$search_value%'   or buyer_email like  '%$search_value%' or `buyer_phone` like  '%$search_value%' or  lr.`leads_status` like '%$search_value%'
                    or `participant_firstname` like  '%$search_value%' or `participant_lastname` like  '%$search_value%' or   DATE_FORMAT(lr.`registration_date`, '%b %d, %Y')  like  '%$search_value%' 
                    or  lp.`program_name` like  '%$search_value%'  or  ls.`source_name` like  '%$search_value%') $lday_text";
        } else {
            $s_text = "$lday_text";
        }

         if ($leads_status_tab == 'A') { //ORDERS TAB
            $leads_tab = "lr.`leads_status` in ('A')";
        } else if ($leads_status_tab == 'E') {
            $leads_tab = "lr.`leads_status` in ('E')";
        } else if ($leads_status_tab == 'NI') {
            $leads_tab = "lr.`leads_status` in ('NI')";
        } 

          $query = sprintf("SELECT lr.`leads_reg_id` as id, lr.`company_id`, lr.`program_id`, lr.`source_id`, `buyer_first_name`, `buyer_last_name`, `participant_firstname` participant_first_name, `participant_lastname` participant_last_name,
                IF(leads_reg_type_user='P', 'Control Panel', IF(leads_reg_type_user='M', 'Mobile App', IF(leads_reg_type_user='U', 'Website URL', ''))) leads_reg_type_user, 'N' as show_icon,
                 `buyer_email` participant_email, `buyer_phone` participant_phone, DATE_FORMAT(lr.`registration_date`,%s) opt_in_date, DATE_FORMAT(($last_updt_date),%s) last_updt_dt, if(`buyer_email` = IFNULL(b.`bounced_mail`, '') AND b.`email_subscription_status`='B','Y','N') bounce_flag,b.`email_subscription_status`,b.`type`,b.`error`,
                IF(lr.`leads_status`='A','Active',IF(lr.`leads_status`='E','Enrolled','NotInterested')) leads_status, (select `source_name` from `leads_source` where `source_id`=lr.`source_id` and `company_id`=lr.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=lr.`program_id` and `company_id`=lr.`company_id`) program_interest, (select `company_name` from `company` where `company_id`=lr.`company_id`) company_name 
                FROM `leads_registration` lr 
                LEFT JOIN `leads_program` lp on lr.`company_id`=lp.`company_id` AND lr.`program_id`=lp.`program_id` 
                LEFT JOIN `leads_source` ls on lr.`company_id`=ls.`company_id` AND lr.`source_id`=ls.`source_id`
                LEFT JOIN `bounce_email_verify` b on b.`bounced_mail`=`buyer_email` AND (b.`company_id`=0 OR b.`company_id`=lr.`company_id`)  WHERE lr.`company_id`='%s' %s AND  $leads_tab AND lr.`deleted_flag`!='Y'",
                $date_format, $date_format, mysqli_real_escape_string($this->db, $company_id), $s_text);
        $res1 = mysqli_query($this->db, $query);
        
            log_info("search   ".$query);
            if (!$res1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", 'query' => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $num_rows = mysqli_num_rows($res1);
                if ($num_rows > 0) {
                    while ($rows1 = mysqli_fetch_assoc($res1)) {
                        $output1[] = $rows1['id'];
                    }
                }
            }
        
        }
        for($i=0; $i<count($leads_reg_id); $i++){
            $output1[] = $leads_reg_id[$i]['id'];
        }
        $reg_id_list = implode(",", $output1);
       $output = [];
        $select_query = sprintf("SELECT concat(`buyer_last_name`,', ', `buyer_first_name`) buyer_name, lr.`buyer_phone`, lr.`buyer_email`,IF(lr.`leads_status`='A','Active',IF(lr.`leads_status`='E','Enrolled','NotInterested')) leads_status,(select `source_name` from `leads_source` where `source_id`=lr.`source_id` and `company_id`=lr.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=lr.`program_id` and `company_id`=lr.`company_id`) program_interest ,lr.`participant_firstname`,lr.`participant_lastname`,lr.`registration_date` FROM `leads_registration` lr 
                LEFT JOIN `leads_program` lp on lr.`company_id`=lp.`company_id` AND lr.`program_id`=lp.`program_id` 
                LEFT JOIN `leads_source` ls on lr.`company_id`=ls.`company_id` AND lr.`source_id`=ls.`source_id` WHERE lr.`company_id`='%s' AND lr.`leads_reg_id` in (%s)", mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$reg_id_list));
        $result_select = mysqli_query($this->db, $select_query);
//         log_info("search   ".$select_query);
        if(!$result_select){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result_select);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_select)) {
                     $output[] = $row;
                }
            } else {
                $error = array("status" => "Failed", "msg" => "lead program details doesn't exist.");
                $this->response($this->json($error), 401);
            }
        }
        ob_clean();
        ob_start();
        $header = $body = '';
        $header .= "Buyer,Mobile Phone,Email,Status,Program Interest,Source,Participant First Name,Participant Last Name,Opt in Date"."\r\n";
       for ($row_index = 0; $row_index < $num_rows; $row_index++) {
           
            $body = $body . '"' . $output[$row_index]['buyer_name'].'"' . "," ;
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_phone']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['buyer_email']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['leads_status']) . '"' . ",";
            $body = $body . '"' . str_replace('"', "''", $output[$row_index]['program_interest']) . '"' . ",";
             $body = $body . '"' . str_replace('"', "''", $output[$row_index]['lead_source']) . '"' . ",";
             $body = $body . '"' . str_replace('"', "''", $output[$row_index]['participant_firstname']) . '"' . ",";
             $body = $body . '"' . str_replace('"', "''", $output[$row_index]['participant_lastname']) . '"' . ",";
             

            if (!empty(trim($output[$row_index]['registration_date']))) {
                if ("0000-00-00" == $output[$row_index]['registration_date']) {
                    $registration_date = "";
                } else {
                    $reg_date = $output[$row_index]['registration_date'];
                    $registration_date = date('Y-m-d', strtotime($reg_date));
                }
            } else {
                $registration_date = "";
            }
           $body = $body . $registration_date . "\r\n";
            
        }

        $file = "../../../uploads/lead.csv";
        $ifp = fopen($file, "w");
        fwrite($ifp, $header);
        fwrite($ifp, $body);
        fclose($ifp);
        header('Content-Type: application/x-download; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        readfile($file);
        unlink($file);
        exit(); 
        date_default_timezone_set($curr_time_zone);
    }

    public function verifyBouncedEmailForCC($cc_address, $company_id){
        $selecsql=sprintf("SELECT * FROM `bounce_email_verify` b WHERE b.`bounced_mail`='%s' AND (b.`company_id`=0 OR b.`company_id`='%s')", mysqli_real_escape_string($this->db,$cc_address), mysqli_real_escape_string($this->db,$company_id));
        $result= mysqli_query($this->db,$selecsql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
          if(mysqli_num_rows($result)>0){
              return 0;
          }else{
               return 1;
          }
        }
    }
    
    public function updateLeadsDetails($company_id, $leads_reg_id, $buyer_fname,$buyer_lname, $buyer_phone, $buyer_email, $pfirst_name, $plast_name, $program_interest_id, $lead_source_id){
        $checkold=sprintf("SELECT `zap_reg_id`,`registration_date`,`leads_status`,IF(lr.`leads_status`='A','Active',IF(lr.`leads_status`='E','Enrolled','NotInterested')) zap_leads_status,IF(lr.`include_campaign_email`='Y','Yes','No') zap_include_campaign,(select `source_name` from `leads_source` where `source_id`=lr.`source_id` and `company_id`=lr.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=lr.`program_id` and `company_id`=lr.`company_id`) program_interest,`program_id`,`source_id` FROM `leads_registration` lr WHERE  `company_id`='%s' and `leads_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $leads_reg_id));
        $resultold = mysqli_query($this->db, $checkold);
        if (!$resultold) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$checkold");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($resultold)>0){
                $old_value= mysqli_fetch_object($resultold);
                $program_id = $old_value->program_id;
                $source_id = $old_value->source_id;
                $program_interest = $old_value->program_interest;
                $lead_source = $old_value->lead_source;
                $leads_status = $old_value->leads_status;
                $zap_reg_id = $old_value->zap_reg_id;
                $zap_leads_status = $old_value->zap_leads_status;
                $zap_communication_campaign = $old_value->zap_include_campaign;
                $leads_reg_date = $old_value->registration_date;
            }else{
                $error = array('status' => "Failed", "msg" => "Lead registration data not available.");
                $this->response($this->json($error), 200);
            }
        }

        $sql = sprintf("UPDATE `leads_registration` SET `buyer_first_name`='%s',`buyer_last_name`='%s',`buyer_email`='%s',`buyer_phone`='%s',`participant_firstname`='%s',`participant_lastname`='%s',`program_id`='%s',`source_id`='%s'  WHERE `company_id`='%s' AND `leads_reg_id`='%s'", 
                mysqli_real_escape_string($this->db,$buyer_fname), mysqli_real_escape_string($this->db,$buyer_lname), mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$buyer_phone), mysqli_real_escape_string($this->db,$pfirst_name),mysqli_real_escape_string($this->db,$plast_name),
                mysqli_real_escape_string($this->db,$program_interest_id),mysqli_real_escape_string($this->db,$lead_source_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$leads_reg_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $new_query = sprintf("SELECT 'pi' as type, `program_name` name from `leads_program` where `program_id`='%s' and `deleted_flg`!='D'
                                UNION
                    SELECT 'ls' as type, `source_name` name from `leads_source` where `source_id`='%s' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $program_interest_id), mysqli_real_escape_string($this->db, $lead_source_id));
            $new_res = mysqli_query($this->db, $new_query);
            if (!$new_res) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$new_query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if(mysqli_num_rows($new_res)>0){
                    while($row = mysqli_fetch_assoc($new_res)){
                        $type = $row['type'];
                        if ($type == 'pi') {
                            $program_name = $row['name'];
                        } 
                        if ($type == 'ls') {
                            $source_name = $row['name'];
                        }
                    }
                }
            }
            if ($program_id != $program_interest_id) {
                $activity_type = 'Edit';
                $activity_text = "Program Interest changed from $program_interest to $program_name";

                $curr_date = gmdate("Y-m-d H:i:s");
                $insert_history = sprintf("INSERT INTO `leads_history`(`company_id`, `leads_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id), $activity_type, mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
             if ($source_id != $lead_source_id) {
                $activity_type = 'Edit';
                $activity_text = "Lead Source changed from $lead_source to $source_name";

                $curr_date = gmdate("Y-m-d H:i:s");
                $insert_history = sprintf("INSERT INTO `leads_history`(`company_id`, `leads_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id), $activity_type, mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            }
            if($program_id != $program_interest_id || $source_id != $lead_source_id){
                $this->addLeadsDimensions($company_id, $program_interest_id, $lead_source_id, $leads_reg_date);
                $this->updateLeadsDimensionsMembers($company_id, $program_interest_id, $lead_source_id, $leads_status, '', $leads_reg_id, $program_id, $source_id);
//                $old_check = array('old_status' => $leads_status);
//                $this->updateLeadsDimensionsMembers($company_id, $program_id, $source_id, $old_check, '', $leads_reg_id);
            }
            $leads_details = $this->getLeadsRegDetails($company_id, $leads_reg_id, 1);
            //  For zapier Implementation
            $target_url_arr = [];
            $zapsql = sprintf("SELECT target_url from zap_subscribe where company_id='%s' AND (event_type='U' OR event_type='B') AND category_type='L' AND deleted_flag !='Y'", mysqli_real_escape_string($this->db, $company_id));
            $zapresult = mysqli_query($this->db, $zapsql);
            if (!$zapresult) {
                $error = array('status' => "Failed","msg" => "Internal Server Error.", "query" => "$zapsql");
                log_info($this->json($error));
            } else {
                $zap_num_of_rows = mysqli_num_rows($zapresult);
                if ($zap_num_of_rows > 0) {
                    while ($zaprow = mysqli_fetch_assoc($zapresult)) {
                        $target_url_arr[] = $zaprow['target_url'];
                    }
                    $tmpkey_arr = $arr_check = [];
                    for($i=0;$i<count($target_url_arr);$i++){
                        $tmp = explode( '/', $target_url_arr[$i]);
                        $last_two = array_splice($tmp,count($tmp)-3);
                        $tmpkey = implode("/",$tmp);
                        $tmpkey .= '/'.$last_two[0].'/';
                        if(!in_array($tmpkey,$tmpkey_arr)){
                            $tmpkey_arr[] = $tmpkey;
                        }
                        $arr_check[$tmpkey][] = $last_two[1];
                    }
                    for ($i = 0; $i < count($tmpkey_arr); $i++) {
                        foreach ($arr_check as $key => $value) {
                            if ($tmpkey_arr[$i] == $key) {
                                for ($j = 0; $j < count($value); $j++) {
                                    $tmpkey_arr[$i] = $tmpkey_arr[$i].$value[$j];
                                    if($j!== count($value) -1){
                                        $tmpkey_arr[$i] = $tmpkey_arr[$i].',';
                                    }else if($j == count($value)-1){
                                        $tmpkey_arr[$i] = $tmpkey_arr[$i].'/';
                                    }
                                }
                            }
                        }
                    }
                    for($i=0;$i<count($tmpkey_arr);$i++){
                        $auth[] = "Accept: application/json";
                        $auth[] = "Content-Type: application/json";
                        $temp_body=["Id"=>"$zap_reg_id","Buyer First Name" =>"$buyer_fname","Buyer Last Name" =>"$buyer_lname","Mobile Phone" =>"$buyer_phone","Email" =>"$buyer_email","Status" =>$zap_leads_status,"Program Interest" =>"$program_name","Source" =>"$source_name","Participant First Name" =>"$pfirst_name","Participant Last Name" =>"$plast_name","Communication Campaign Status"=>$zap_communication_campaign];
                        $body = JSON_ENCODE($temp_body);

                        $ch = curl_init(); 
                        curl_setopt($ch, CURLOPT_URL, "$tmpkey_arr[$i]");
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $auth);

                        $result = curl_exec($ch);
                        $resultArr = json_decode($result, true);
                        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        if(!isset($resultArr['error']) && ($returnCode==200)){
                            $succ_log = array("Curl : " => "Success($returnCode)", "Method: " => "POST", "Url: " => $tmpkey_arr[$i], "Body: " => $body, "Result: " => $result);
                            log_info($this->json($succ_log));
                        }else{
                            $error_log = array("Curl error: " => curl_error($ch)."($returnCode)", "Method: " => "POST", "Url: " => $tmpkey_arr[$i], "Body: " => $body, "Result : " => $result);
                            log_info($this->json($error_log));
                        }
                        curl_close($ch);
                    }   
                }
            }
             // End Zapier Implementation
           $msg = array('status' => "Success", "msg" => "Leads details updated successfully.", 'leads_details' => $leads_details);
           $this->response($this->json($msg), 200);
        }
    }
    
    public function updateleadsParticipantStatusInDb($company_id, $leads_reg_id,  $leads_status, $next_date){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $curr_date = date("Y-m-d");
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_add = "SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";

        $reg_string = $leads_string =  $date_changing_string = $activity_text='';
        
        $checkstatus=sprintf("SELECT `registration_date`,`leads_status`,`program_id`,`source_id` FROM `leads_registration` WHERE  `company_id`='%s' and `leads_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $leads_reg_id));
        $resultcheckstatus= mysqli_query($this->db, $checkstatus);
        if(!$resultcheckstatus){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$checkstatus");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
        }else{
            $result_value= mysqli_fetch_object($resultcheckstatus);
            $old_leads_status = $result_value->leads_status;
            $program_id = $result_value->program_id;
            $source_id = $result_value->source_id;
            $leads_reg_date = $result_value->registration_date;
        }
         $this->addLeadsDimensions($company_id, $program_id, $source_id, $leads_reg_date);
          if ($leads_status == 'A') {
              $activity_text = "Status changed to Active";
            if ($old_leads_status == 'E') {
                $leads_string = "`enrolled_count`=`enrolled_count`-1,`active_count`=`active_count`+1";
            }  else if ($old_leads_status == 'NI') {
                $leads_string = "`notinterested_count`=`notinterested_count`-1,`active_count`=`active_count`+1";
            }
        } elseif ($leads_status == 'E') {
             $activity_text = "Status changed to Enrolled";
            if ($old_leads_status == 'A') {
                $leads_string = "`active_count`=`active_count`-1,`enrolled_count`=`enrolled_count`+1";
            }else if ($old_leads_status == 'NI') {
                $leads_string = "`notinterested_count`=`notinterested_count`-1,`enrolled_count`=`enrolled_count`+1";
            }
        } elseif ($leads_status == 'NI') {
            $activity_text = "Status changed to Not Interested";
            if ($old_leads_status == 'E') {
                $leads_string = "`enrolled_count`=`enrolled_count`-1,`cancelled_count`=`cancelled_count`+1";
            } else if ($old_leads_status == 'A') {
                $leads_string = "`active_count`=`active_count`-1,`cancelled_count`=`cancelled_count`+1";
            } 
        } 

        $get_reg_details = sprintf("SELECT lr.*, c.upgrade_status FROM `leads_registration` lr LEFT JOIN company c ON c.company_id=lr.company_id WHERE `leads_reg_id`='%s'", mysqli_real_escape_string($this->db, $leads_reg_id));
            $result_get_reg_details = mysqli_query($this->db, $get_reg_details);
            if (!$result_get_reg_details) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_reg_details");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }else{
                $num_rows = mysqli_num_rows($result_get_reg_details);
                if($num_rows>0){
                    $reg_details = mysqli_fetch_assoc($result_get_reg_details);
                    $upgrade_status = $reg_details['upgrade_status'];
//                        $reg_details = $reg_details_arr[0];
                }else{
                    $error = array('status' => "Failed", "msg" => "Registration details mismatched.");
                    $this->response($this->json($error), 200);
                }
            }
            
           $query1 = sprintf("UPDATE `leads_registration` SET `leads_status`='%s' $reg_string WHERE `company_id`='%s' AND `leads_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $leads_status), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id));
        $result1 = mysqli_query($this->db, $query1);
//        log_info("status   ".$query1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }
            
        if (!empty(trim($activity_text))) {
            $currdate = gmdate("Y-m-d H:i:s");
            $insert_history = sprintf("INSERT INTO `leads_history`(`company_id`, `leads_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id), 'status', mysqli_real_escape_string($this->db, $activity_text),
                    mysqli_real_escape_string($this->db, $currdate));
            $result_history = mysqli_query($this->db, $insert_history);
            if (!$result_history) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        $this->updateLeadsDimensionsMembers($company_id, $program_id, $source_id, $leads_status, $old_leads_status, $leads_reg_id, '', '');
        $reg_details = $this->getLeadsRegDetails($company_id, $leads_reg_id, 1);
        $msg = array('status' => "Success", "msg" => "Leads status updated successfully.", "reg_details" => $reg_details);
        date_default_timezone_set($curr_time_zone);
        $this->response($this->json($msg), 200);
    }
    
     //dashboard
    public function addLeadsDimensions($company_id, $program_id, $source_id, $period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($source_id)) {
            $sql1 = sprintf("SELECT * FROM `leads_dimensions` WHERE `company_id`='%s' AND `program_id`='%s' AND  `source_id`='%s' AND source_id !=0  AND `period`='%s' AND `deleted_flg`!='D'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db, $source_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $insertquery = sprintf("INSERT INTO `leads_dimensions`(`company_id`, `period`, `program_id`, `source_id`) VALUES('%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db, $source_id));
                    $res = mysqli_query($this->db, $insertquery);
                    if (!$res) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        if (!empty($program_id)) {
            $sql2 = sprintf("SELECT * FROM `leads_dimensions` WHERE `company_id`='%s' AND `program_id`='%s' AND `source_id`=0 AND `period`='%s' AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result2 = mysqli_query($this->db, $sql2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows1 = mysqli_num_rows($result2);
                if ($num_of_rows1 == 0) {
                    $insertquery1 = sprintf("INSERT INTO `leads_dimensions`(`company_id`, `period`, `program_id`,  `source_id`) VALUES('%s', '%s','%s','0')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $program_id));
                    $res2 = mysqli_query($this->db, $insertquery1);
                    if (!$res2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function updateLeadsDimensionsMembers($company_id, $program_id, $source_id, $leads_status, $old_leads_status, $leads_reg_id, $old_program_id, $old_source_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $leads_string = $leads_sub_string = "";
        $date_str = "%Y-%m";
        $reg_date = '';
//        $every_month_dt = date("Y-m-d");
//        if (is_array($leads_status)) {
//            
//        } else {
            if ($leads_status == 'A') {
                if ($old_leads_status == 'E') {
                    $leads_string = "`enrolled_count`=`enrolled_count`-1";
                } else if ($old_leads_status == 'NI') {
                    $leads_string = "`notinterested_count`=`notinterested_count`-1";
                }
            } elseif ($leads_status == 'E') {
                if ($old_leads_status == 'A') {
                    $leads_string = "`active_count`=`active_count`-1";
                } else if ($old_leads_status == 'NI') {
                    $leads_string = "`notinterested_count`=`notinterested_count`-1";
                }
            } elseif ($leads_status == 'NI') {
                if ($old_leads_status == 'E') {
                    $leads_string = "`enrolled_count`=`enrolled_count`-1";
                } else if ($old_leads_status == 'A') {
                    $leads_string = "`active_count`=`active_count`-1";
                }
            }
//        }

        $selectquery = sprintf("SELECT registration_date FROM `leads_registration` WHERE `company_id`='%s' and `leads_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id));
        $res = mysqli_query($this->db, $selectquery);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
            log_info($this->json($selectquery));
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows == 1) {
                $rows = mysqli_fetch_object($res);
                $reg_date = $rows->registration_date;
            }
        }

        if (!empty($old_leads_status)) {
            if ($leads_status == 'A') {
                $leads_string = "$leads_string, `active_count`=`active_count`+1";
            } else if ($leads_status == 'E') {
                $leads_string = "$leads_string, `enrolled_count`=`enrolled_count`+1";
            } else if ($leads_status == 'NI') {
                $leads_string = "$leads_string, `notinterested_count`=`notinterested_count`+1";
            }
        } else {
//            if (is_array($leads_status)) {
//                $check = $leads_status['old_status'];
//                if ($check == 'A') {
//                    $leads_string = "`active_count`=`active_count`-1";
//                } else if ($check == 'E') {
//                    $leads_string = "`enrolled_count`=`enrolled_count`-1";
//                } else if ($check == 'NI') {
//                    $leads_string = "`notinterested_count`=`notinterested_count`-1";
//                }
//            } else {
                if ($leads_status == 'A') {
                    $leads_string = "`active_count`=`active_count`+1";
                    $leads_sub_string = "`active_count`=`active_count`-1";
                } else if ($leads_status == 'E') {
                    $leads_string = "`enrolled_count`=`enrolled_count`+1";
                    $leads_sub_string = "`enrolled_count`=`enrolled_count`-1";
                } else if ($leads_status == 'NI') {
                    $leads_string = "`notinterested_count`=`notinterested_count`+1";
                    $leads_sub_string = "`notinterested_count`=`notinterested_count`-1";
                }
            }
//        }

        if (!empty(trim($leads_string))) {
            $query1 = sprintf("UPDATE `leads_dimensions` SET $leads_string WHERE `company_id`='%s' AND `program_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `source_id`=0 AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db,$reg_date), mysqli_real_escape_string($this->db,$date_str));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        if (!empty(trim($leads_string))) {
            $query1 = sprintf("UPDATE `leads_dimensions` SET $leads_string WHERE `company_id`='%s' AND `program_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `source_id`='%s' AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db,$reg_date), mysqli_real_escape_string($this->db,$date_str), mysqli_real_escape_string($this->db,$source_id));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        
        if (!empty(trim($leads_sub_string)) && !empty($old_program_id) && !empty($old_source_id)) {
            $query1 = sprintf("UPDATE `leads_dimensions` SET $leads_sub_string WHERE `company_id`='%s' AND `program_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `source_id`=0 AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $old_program_id), mysqli_real_escape_string($this->db,$reg_date), mysqli_real_escape_string($this->db,$date_str));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
        if (!empty(trim($leads_sub_string)) && !empty($old_program_id) && !empty($old_source_id)) {
            $query1 = sprintf("UPDATE `leads_dimensions` SET $leads_sub_string WHERE `company_id`='%s' AND `program_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `source_id`='%s' AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $old_program_id), mysqli_real_escape_string($this->db,$reg_date), mysqli_real_escape_string($this->db,$date_str), mysqli_real_escape_string($this->db,$old_source_id));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }

        date_default_timezone_set($curr_time_zone);
    }
    
    public function getLeadsDashboardDetails($company_id, $leadsource_flag, $conversion_flag, $sales_period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $leads = $leads_new = $source_title_arr = $source_id_arr = $leads_id_arr = $final_ls_array = $final_active_members_array = $final_leads_array = $final_notint_members_array = $final_end_members_array = [];
        $t_total_count_arr = $t_total_conversion_members = $t_total_en_members = $net_gain_arr = [0, 0, 0, 0];
        $active_members = $enrolled_members = $notinterested_members = [];
        $month_array = [];
        $percentage = 0;
        $c_months = ($conversion_flag * 1);
        $plusone = $c_months + 1;
        //conversion
        if ($sales_period == 'A') {
            $curr_date = date('Y-m-d', strtotime("-$c_months year"));
            $curr_datepulsonemonth = date('Y-m-01', strtotime(" $plusone year"));
            for ($i = 0; $i < 4; $i++) {
                $format_array[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date)));
                $month_array[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date)));
                $month_name[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date)));
            }
        } elseif ($sales_period == 'M') {
            $curr_date = date('Y-m-d', strtotime("-$c_months month"));
            $curr_datepulsonemonth = date('Y-m-01', strtotime(" $plusone month"));
            for ($i = 0; $i < 4; $i++) {
                $format_array[] = date('Y-m', strtotime("first day of -$i month", strtotime($curr_date)));
                $month_array[] = date('n', strtotime("first day of -$i month", strtotime($curr_date)));
                $month_name[] = date('M Y', strtotime("first day of -$i month", strtotime($curr_date)));
            }
        }
        $count_type = array("active", "enrolled", "notinterested");
        $format_list = implode("','", $format_array);

        $l_months = ($leadsource_flag * 1);
        $oneplus = $l_months + 1;
//      lead source
        $month_array1 = [];
        if ($sales_period == 'A') {
            $curr_date1 = date('Y-m-d', strtotime("-$l_months year"));
            $curr_datepulsoneyear = date('Y-m-01', strtotime(" $oneplus year"));
            for ($i = 0; $i < 4; $i++) {
                $format_array1[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date1)));
                $month_array1[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date1)));
                $month_name1[] = date('Y', strtotime("first day of -$i year", strtotime($curr_date1)));
            }
        } elseif ($sales_period == 'M') {
            $curr_datepulsoneyear = date('Y-m-01', strtotime(" $oneplus month"));
            $curr_date1 = date('Y-m-d', strtotime("-$l_months month"));
            for ($i = 0; $i < 4; $i++) {
                $format_array1[] = date('Y-m', strtotime("first day of -$i month", strtotime($curr_date1)));
                $month_array1[] = date('n', strtotime("first day of -$i month", strtotime($curr_date1)));
                $month_name1[] = date('M Y', strtotime("first day of -$i month", strtotime($curr_date1)));
            }
        }
        $format_list1 = implode("','", $format_array1);
        $month_name_array = ["first_month", "second_month", "third_month", "fourth_month"];
//         $date_str = "%Y-%m";
        if ($sales_period == 'A') {
            $sql = sprintf("SELECT period, SUBSTR(period, 1, 4) month, ld.program_id, ld.source_id, (select `source_name` from `leads_source` where `source_id`=ld.`source_id` and `company_id`=ld.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=ld.`program_id` and `company_id`=ld.`company_id`) program_interest, (`active_count`+`enrolled_count`+`notinterested_count`) lead_count
                FROM `leads_dimensions`ld WHERE ld.company_id='%s' AND ld.`deleted_flg`!='D' AND  SUBSTR(period, 1, 4) IN ('%s') AND `source_id`!=0 GROUP BY program_id,  SUBSTR(period, 1, 4), source_id", 
                    mysqli_real_escape_string($this->db, $company_id), $format_list1);
        } elseif ($sales_period == 'M') {
            $sql = sprintf("SELECT period, SUBSTR(period, 6, 7) month, ld.program_id, ld.source_id, (select `source_name` from `leads_source` where `source_id`=ld.`source_id` and `company_id`=ld.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=ld.`program_id` and `company_id`=ld.`company_id`) program_interest, (`active_count`+`enrolled_count`+`notinterested_count`) lead_count
                FROM `leads_dimensions`ld WHERE ld.company_id='%s' AND ld.`deleted_flg`!='D' AND  period IN ('%s') AND `source_id`!=0 GROUP BY program_id,  period, source_id",
                    mysqli_real_escape_string($this->db, $company_id), $format_list1);
        }
//        log_info("leads   ".$sql);
        $res = mysqli_query($this->db, $sql);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($res)) {
                    $temp = $temp2 = [];
                    $temp['month'] = $temp2['month'] = (int) $row['month'];
                    $temp['program_id'] = $temp2['program_id'] = $program_id = $row['program_id'];
                    $temp['program_interest'] = $temp2['program_interest'] = $row['program_interest'];
                    $temp['source_id'] = $temp2['source_id'] = $source_id = $row['source_id'];
                    $temp['lead_source'] = $temp2['lead_source'] = $row['lead_source'];
                    $temp['lead_count'] = $lead_count = $row['lead_count'];
                    $temp['db'] = 1;
                    $leads_data[] = $temp;

                    if (!empty($source_id) && !in_array($source_id, $source_id_arr)) {
                        $source_id_arr[] = $source_id;
                        $source_title_arr[] = $row['lead_source'];
                    }
                    if (!empty($program_id) && !in_array($program_id, $leads_id_arr)) {
                        $leads_id_arr[] = $program_id;
                        $leads_title_arr[] = $row['program_interest'];
                    }
                }
                for ($j = 0; $j < 4; $j++) {
                    $t_total_lead_count = 0;
                    for ($x = 0; $x < count($leads_id_arr); $x++) {
                        $temp = [];
                        $t_present = 0;
                        $c_id = $leads_id_arr[$x];
                        $c_title = $leads_title_arr[$x];
                        for ($k = 0; $k < count($leads_data); $k++) {
                            if ($month_array1[$j] == $leads_data[$k]['month'] && (int) $c_id == $leads_data[$k]['program_id']) {
                                $t_present = 1;
                                $leads_new_net[] = $leads_data[$k];
                                $t_total_lead_count += $leads_data[$k]['lead_count'];
                            }
                        }
                        if ($t_present == 0) {
                            $temp['month'] = $month_array1[$j];
                            $temp['program_id'] = $c_id;
                            $temp['program_interest'] = $c_title;
                            $temp['lead_count'] = 0;
                            $temp['db'] = 0;
                            $temp['lead_source'] = '';
                            $leads_new_net[] = $temp;
                        }
                    }
                    $t_total_count_arr[$j] = $t_total_lead_count;
                }
//                log_info("new net  " . json_encode($leads_new_net));
                for ($x = 0; $x < count($leads_id_arr); $x++) {
                    $c_id = $leads_id_arr[$x];
                    $final_lead_group = [];
                    for ($j = 0; $j < 4; $j++) {
                        $r = 0;
                        $lead_count_temp = 0;
                        for ($k = 0; $k < count($leads_new_net); $k++) {
                            if ($leads_new_net[$k]['month'] == $month_array1[$j] && $leads_new_net[$k]['program_id'] == $c_id) {
//                                log_info("LEADS PROGRAM   --$x" . $leads_new_net[$k]['program_interest'] . "  MONTH  --$j" . $leads_new_net[$k]['month']);
                                $title = $leads_new_net[$k]['program_interest'];
                                $id = $leads_new_net[$k]['program_id'];
                                $final_ls_array[$x]['name'] = $title;
                                $final_ls_array[$x]['id'] = $id;
                                $lead_count_temp += $leads_new_net[$k]['lead_count'];
                                $final_ls_array[$x][$month_name_array[$j]] = $lead_count_temp;

                                if ($leads_new_net[$k]['lead_count'] > 0) {
                                    if (array_search($leads_new_net[$k]['lead_source'], array_column($final_lead_group, 'name')) !== false) {
                                        $key = array_search($leads_new_net[$k]['lead_source'], array_column($final_lead_group, 'name'));
                                        $final_lead_group[$key][$month_name_array[$j]] = $leads_new_net[$k]['lead_count'];
//                                        log_info("if R  $r inner loop" . json_encode($final_lead_group));
                                    } else {
                                        if (isset($final_lead_group[$r]) && !empty($final_lead_group)) {
//                                            log_info("isset check in" . $final_lead_group[$r]['name'] . ' ' . $leads_new_net[$k]['lead_source']);
                                            if ($final_lead_group[$r]['name'] !== $leads_new_net[$k]['lead_source']) {
                                                $r = count($final_lead_group);
//                                                for ($z = 0; ($z < count($source_title_arr)); $z++) {
//                                                    if (isset($final_lead_group[$r])) {
//                                                        $r++;
//                                                        log_info("index increment $r");
//                                                    } else {
//                                                        break;
//                                                    }
//                                                }
                                            }
                                        }
                                        $final_lead_group[$r]['name'] = $leads_new_net[$k]['lead_source'];
                                        $final_lead_group[$r][$month_name_array[$j]] = $leads_new_net[$k]['lead_count'];
//                                        log_info("else R  $r inner loop" . json_encode($final_lead_group));
                                        $r++;
                                    }
                                }
                            }
                        }
                    }
                    $final_ls_array[$x]['children'] = $final_lead_group;
                }
            }
        }
        // for splicing of the empty records
        for ($ls = 0; $ls < count($final_ls_array); $ls++) {
            $check_zero = 0;
            for ($j = 2; $j <= 5; $j++) {// first month index starts at 2
                if ((double) $final_ls_array[$ls][$month_name_array[$j - 2]] > 0) {
                    $check_zero = 1;
                }
            }
            if ($check_zero == 0) {
                array_splice($final_ls_array, $ls, 1);
                $ls--;
            }
        }
        //members conversion
        $program_id_arr1 = $program_title_arr1 = [];
        if ($sales_period == 'A') {
            $sql1 = sprintf("SELECT period, SUBSTR(period, 1, 4) month, ld.program_id, ld.source_id, IF(ld.`source_id`!=0,(select `source_name` from `leads_source` where `source_id`=ld.`source_id` and `company_id`=ld.`company_id`),'') lead_source, 
               (select `program_name` from `leads_program` where `program_id`=ld.`program_id` and `company_id`=ld.`company_id`) program_interest, SUM(active_count) active, SUM(enrolled_count) enrolled, SUM(notinterested_count) notinterested
                FROM `leads_dimensions` ld WHERE company_id='%s' AND SUBSTR(period, 1, 4) IN ('%s') AND source_id=0 AND `deleted_flg`!='D' GROUP BY program_id, SUBSTR(period, 1, 4)",
                    mysqli_real_escape_string($this->db, $company_id), $format_list);
        } else if ($sales_period == 'M') {
            $sql1 = sprintf("SELECT period, SUBSTR(period, 6, 7) month, ld.program_id, ld.source_id, IF(ld.`source_id`!=0,(select `source_name` from `leads_source` where `source_id`=ld.`source_id` and `company_id`=ld.`company_id`),'') lead_source, 
               (select `program_name` from `leads_program` where `program_id`=ld.`program_id` and `company_id`=ld.`company_id`) program_interest, SUM(active_count) active, SUM(enrolled_count) enrolled, SUM(notinterested_count) notinterested
                FROM `leads_dimensions` ld WHERE company_id='%s' AND period IN ('%s') AND source_id=0 AND `deleted_flg`!='D' GROUP BY program_id, period",
                    mysqli_real_escape_string($this->db, $company_id), $format_list);
        }
//        log_info("conversion   " . $sql1);
        $res1 = mysqli_query($this->db, $sql1);
        if (!$res1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($res1);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($res1)) {
                    $temp = $temp2 = [];
                    $total = 0;
                    $temp['month'] = $temp2['month'] = (int) $row['month'];
                    $temp['program_id'] = $temp2['program_id'] = $program_id = $row['program_id'];
                    $temp['program_interest'] = $temp2['program_interest'] = $row['program_interest'];
                    $total += $temp['active'] = $temp2['active'] = $row['active'];
                    $total += $temp['enrolled'] = $temp2['enrolled'] = $enrolled = $row['enrolled'];
                    $total += $temp['notinterested'] = $temp2['notinterested'] = $row['notinterested'];
                    if ($total !== 0) {
                        $percentage = number_format(($enrolled / $total) * 100, 0);
                    } else {
                        $percentage = 0;
                    }
                    $temp['c_factor'] = $temp2['c_factor'] = "$percentage% " . $enrolled . "/" . $total;
                    $temp['db'] = 1;
                    $leads[] = $temp;
                    if (!empty($program_id) && !in_array($program_id, $program_id_arr1)) {
                        $program_id_arr1[] = $program_id;
                        $program_title_arr1[] = $row['program_interest'];
                    }
                }

                for ($j = 0; $j < 4; $j++) {
                    $t_percentage = $t_total = $active = $enrolled = $dnd = $notinterest = 0;
                    for ($x = 0; $x < count($program_id_arr1); $x++) {
                        $temp = [];
                        $t_present = 0;
                        $c_id = $program_id_arr1[$x];
                        $c_title = $program_title_arr1[$x];
                        for ($k = 0; $k < count($leads); $k++) {
                            if ($month_array[$j] == $leads[$k]['month'] && (int) $c_id == $leads[$k]['program_id']) {
                                $t_present = 1;
                                $leads_new[] = $leads[$k];
                                $enrolled += $leads[$k]['enrolled'];
                                $t_total += $leads[$k]['active'] + $leads[$k]['notinterested'] + $leads[$k]['enrolled'];
                            }
                        }
                        if ($t_present == 0) {
                            $temp['month'] = $month_array[$j];
                            $temp['program_id'] = $c_id;
                            $temp['program_interest'] = $c_title;
                            $temp['active'] = 0;
                            $temp['enrolled'] = 0;
                            $temp['notinterested'] = 0;
                            $temp['db'] = 0;
                            $temp['c_factor'] = "0% 0/0";
                            $leads_new[] = $temp;
                        }
                    }
                    if ($t_total !== 0) {
                        $t_percentage = number_format(($enrolled / $t_total) * 100, 0);
                    } else {
                        $t_percentage = 0;
                    }
                    $t_total_conversion_members[$j] = "$t_percentage% " . $enrolled . "/" . $t_total;
                }

                for ($x = 0; $x < count($program_id_arr1); $x++) {
                    $c_id = $program_id_arr1[$x];
                    for ($j = 0; $j < 4; $j++) {
                        for ($k = 0; $k < count($leads_new); $k++) {
                            if ($leads_new[$k]['month'] == $month_array[$j] && $leads_new[$k]['program_id'] == $c_id) {
                                $title = $leads_new[$k]['program_interest'];
                                $id = $leads_new[$k]['program_id'];
                                $final_leads_array[$x][$month_name_array[$j]] = $leads_new[$k]['c_factor'];
                                $final_leads_array[$x]['name'] = $title;
                                $final_leads_array[$x]['id'] = $id;
                                $active_members[$month_name_array[$j]] = $leads_new[$k]['active'];
                                $enrolled_members[$month_name_array[$j]] = $leads_new[$k]['enrolled'];
                                $notinterested_members[$month_name_array[$j]] = $leads_new[$k]['notinterested'];
                            }
                        }
                    }
                    $enrolled_members['name'] = 'Enrolled';
                    $final_leads_array[$x]['children'][] = $enrolled_members;
                    $active_members['name'] = 'Active';
                    $final_leads_array[$x]['children'][] = $active_members;
                    $notinterested_members['name'] = 'Not Interested';
                    $final_leads_array[$x]['children'][] = $notinterested_members;
                }
            }
        }
          // for splicing of the empty records
         for ($ls = 0; $ls < count($final_leads_array); $ls++) {
            $check_zero = 0;
            for ($j = 1; $j <= 4; $j++) {// first month index starts at 1
                if ($final_leads_array[$ls][$month_name_array[$j - 1]] !== "0% 0/0") {
                    $check_zero = 1;
                }
            }
            if ($check_zero == 0) {
                array_splice($final_leads_array, $ls, 1);
                $ls--;
            }
        }

        $output['count_type'] = $count_type;
        $output['month_leadsource'] = $month_name1;
        $output['month_conversion'] = $month_name;
        $lead1 = $mem1 = $mem2 = $mem3 = $mem4 = [];
        $lead1['name'] = 'Total';

        for ($i = 0; $i < 4; $i++) {
            $lead1[$month_name_array[$i]] = $t_total_count_arr[$i];
        }
        if (count($final_ls_array) > 0) {
            usort($final_ls_array, function($a, $b) {
                return $a['first_month'] < $b['first_month'];
            });
        }
        $lead1['children'] = $final_ls_array;
        $output['leads'][] = $lead1;


        $mem2['name'] = 'Total';


        for ($i = 0; $i < 4; $i++) {
            $mem2[$month_name_array[$i]] = $t_total_conversion_members[$i];
        }

        $mem2['children'] = $final_leads_array;

        $output['conversion'][] = $mem2;

        $msg = array('status' => "Success", "msg" => $output);
        $this->response($this->json($msg), 200);
        date_default_timezone_set($curr_time_zone);
    }
    
     public function setDeletedflagForLeadsdimensions($company_id, $type, $old_id){
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $date_str = "%Y-%m";
//        $reg_date = '';
        if($type == 'pi'){
         $query1 = sprintf("UPDATE `leads_dimensions` SET `deleted_flg`='D' WHERE `company_id`='%s' AND `program_id`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $old_id));
        }else if($type == 'ls'){
         $query1 = sprintf("UPDATE `leads_dimensions` SET `deleted_flg`='D' WHERE `company_id`='%s' AND `source_id`='%s'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $old_id));
        }
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
         date_default_timezone_set($curr_time_zone);
    }
     
    public function campaignSendmail($reg_id, $company_id, $buyer_email) {
        $check_email=0;
        $sql = sprintf("SELECT c.`campaign_id`,c.`subject`,c.`message`,c.campaign_type,c.`email_banner_image_url`,c.`button_text`,c.`button_url`,    
                        IF(ca.`web_page` IS NULL || TRIM(ca.`web_page`)='', '',ca.`web_page`) web_page, ca.company_name, ca.email_id, ca.`referral_email_list` 
                        FROM `campaign` c join `campaign` cm on c.`parent_id`=cm.`campaign_id` 
                        LEFT join campaign_trigger ct on  c.campaign_id=ct.campaign_id and c.company_id=ct.company_id
                        LEFT JOIN company ca on ca.company_id=c.company_id
                        where  cm.`campaign_type`='L' and c.type='E' and c.status='E' and ct.trigger_event_type='NL' and TRIM(c.`subject`!='') and TRIM(c.`message`!='') 
                        and ct.delay  IS NULL  and ct.deleted_flag='N' and c.deleted_flag='N' and cm.deleted_flag='N'  and c.company_id='%s'
                        and ca.upgrade_status NOT IN ('F', 'B')", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $check_email=1;
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $footer_detail = $this->getunsubscribefooter($company_id);
                while ($row = mysqli_fetch_assoc($result)) {
                    $email_status='';
                    
                    $message = '<div style="max-width:800px;">';
                    if (!empty($row['email_banner_image_url'])) {
                        $message .= '<img src="' . $row['email_banner_image_url'] . '" alt="Banner Image" style="max-width: 100%;display: block;margin-left: auto;margin-right: auto;width: 800px;"/>';
                    }
                    $message .= '<br>'.$row['message'].'<br><br>';
                    $studio_web_page = $row['web_page'];
                    $button_url=$row['button_url'];
                    $button_text=$row['button_text'];
                    
                    // CHECKING GET STARTED BUTTON TEXT AND LINK
                    if (!empty($button_text) && !empty($button_url)) {
                        $message .= '<center><a href="' . $button_url . '"><button type="button" style="width: 230px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;padding: 12px 0;height: 50px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">' . $button_text . '</button></a></center>';
                    } elseif (!empty($studio_web_page)) {
                        $button_text = "Get started";
                        $message .= '<center><a href="' . $studio_web_page . '"><button type="button" style="width: 230px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;padding: 12px 0;height: 50px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">' . $button_text . '</button></a></center>';
                    }
                    
                    $message .='</div>';
                    
                    $sendEmail_status = $this->sendEmail($buyer_email, $row['subject'], $message, $row['company_name'], $row['email_id'], '', '', $row['referral_email_list'],$company_id,'',$footer_detail);
                    if ($sendEmail_status['status'] == "true") {
                        $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email, "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
                        $updatesql = sprintf("UPDATE `campaign` SET `email_sent_count`=email_sent_count+1 WHERE `campaign_id`='%s'", mysqli_real_escape_string($this->db, $row['campaign_id']));
                        $resultupdtae = mysqli_query($this->db, $updatesql);
                        if (!$resultupdtae) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesql");
                            log_info($this->json($error_log));
                            $check_email=1;
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 200);
                        }
                    } else {
                        $email_status='F';
                        $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email, "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
                        $updatesql = sprintf("UPDATE `campaign` SET `email_sent_count`=email_sent_count+1,`email_failed_count`=email_failed_count+1 WHERE `campaign_id`='%s'", mysqli_real_escape_string($this->db, $row['campaign_id']));
                        $resultupdtae = mysqli_query($this->db, $updatesql);
                        if (!$resultupdtae) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesql");
                            log_info($this->json($error_log));
                            $check_email=1;
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 200);
                        }
//                       
                    }

                    $insertsql = sprintf("INSERT INTO `campaign_email_log`(`company_id`, `reg_id`, `email_id`, `campaign_type`,`email_status`,`buyer_email`) VALUES ('%s','%s','%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $row['campaign_id']), mysqli_real_escape_string($this->db, $row['campaign_type'])
                            ,mysqli_real_escape_string($this->db, $email_status),mysqli_real_escape_string($this->db, $buyer_email));
                    $resultinsert = mysqli_query($this->db, $insertsql);
                    if (!$resultinsert) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insertsql");
                        log_info($this->json($error_log));
                        $check_email=1;
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        
        if($check_email==1){
            $msg = "Something went wrong in campaign send email for Lead registration with reg_id=$reg_id, check web log or table";
            $this->sendEmailForAdmins($msg);
        }
    }
    
    private function sendEmailForAdmins($msg){
        if($this->env=='P'){
            $env = "Production";
        }else{
            $env = "Development";
        }
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com,jeeva@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "$env - Membership Handler Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    protected function getunsubscribefooter($company_id) {
        $sql = sprintf("SELECT company_name, CONCAT(street_address, IF(street_address = '' , city, CONCAT(', ',city)),IF(street_address = '' OR city = '', state, CONCAT(', ',state)),IF(street_address = '' OR city = '' OR state = '', postal_code, CONCAT(' ',postal_code))) address 
                    from company where company_id = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_name = $row['company_name'];
                $studio_address = trim($row['address']);
                if(!empty($studio_address)){
                    $studio_address = ($studio_address[strlen($studio_address) - 1] === "," ? substr($studio_address, 0, -1) : $studio_address);
                }
                $footer_detail = $studio_name . ($studio_address === "" ? '' : " | $studio_address");
                return $footer_detail;
            }
        }
    }
}
?>

