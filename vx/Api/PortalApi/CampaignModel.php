<?php

header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token');
header('Access-Control-Allow-Credentials: true');

require_once '../../../Globals/cont_log.php';
require_once('../../PHPMailer_5.2.1/class.phpmailer.php');
require_once('../../PHPMailer_5.2.1/class.smtp.php');



class CampaignModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    public $local_upload_folder_name = '';
    private $processing_fee_file_name;
    private $server_url;

    public function __construct() {
        require_once '../../../Globals/config.php';
        $this->inputs();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->local_upload_folder_name = $GLOBALS['local_uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'dev.mystudio.academy') !== false) {
            $this->server_url = "http://dev.mystudio.academy";
        } elseif (strpos($host, 'dev2.mystudio.academy') !== false) {
            $this->server_url = "http://dev2.mystudio.academy";
        } elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'];
        } else {
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithSalesforce($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
        header('Access-Control-Allow-Origin: '.$GLOBALS['origin']);
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token, withCredentials, withcredentials,');
        header('Access-Control-Allow-Credentials: true');
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    protected function clean($string) {
        $str = preg_replace('/\s+/', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    }
    
    protected function sendEmail($to, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file,$cc_email_list) {
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }            
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            if(!empty($file_name)){
                $mail->AddStringAttachment(base64_decode($attached_file), $file_name, 'base64');
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    public function getAllCampaignDetails($company_id){
        
        $check = $this->checkCampaignAvailability($company_id, 1);
        if($check['status']=='Success'){
            $this->response($this->json($check), 200);
        }else{
            $this->createCampaignForFirstTime($company_id, 0);
        }
        
    }
    
    public function checkCampaignAvailability($company_id, $call_back){
        $query = sprintf("SELECT * FROM `campaign` WHERE `company_id`='%s' ORDER BY `cmpgn_sort_order`", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $output = [];
                $deleted_count = 0;
                while($row = mysqli_fetch_assoc($result)){
                    $deleted_flag = $row['deleted_flag'];
                    $type = $row['type'];
                    if($deleted_flag!='Y' && $type=='C'){
                        $output[] = $row;
                    }else{
                        $deleted_count++;
                    }
                }
                
                $error =  array('status' => "Success", "msg" => "Available.", "category_list" => $output);
                if($call_back==1){
                    return $error;
                }else{
                    $this->response($this->json($error), 200);
                }
            }else{
                $error =  array('status' => "Failed", "msg" => "Not Available.", "category_list" => []);
                if($call_back==1){
                    return $error;
                }else{
                    $this->response($this->json($error), 200);
                }
            }
        }
    }
    
    public function createCampaignForFirstTime($company_id, $call_back){
        $query = sprintf("INSERT INTO `campaign`( `company_id`, `campaign_title`, `campaign_type`, `type`, `cmpgn_sort_order`) VALUES 
                ('%s', 'New leads email flow', 'L', 'C', NextVal('cmpgn_sort_order'))", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_affected_rows($this->db)>0){
                $campaign_id = mysqli_insert_id($this->db);
                $query2 = sprintf("INSERT INTO `campaign`( `company_id`, `type`, `email_title`, `parent_id`, `cmpgn_sort_order`) VALUES
                    ('%s', 'E', 'Email 1', $campaign_id, NextVal('cmpgn_sort_order'))", mysqli_real_escape_string($this->db, $company_id));
                $result2 = mysqli_query($this->db, $query2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $campaign_email_id = mysqli_insert_id($this->db);
                    $query3 = sprintf("INSERT INTO `campaign_trigger`( `company_id`, `campaign_id`, `trigger_event_type`) VALUES ('%s', '%s', '%s')",
                        mysqli_real_escape_string($this->db, $company_id), $campaign_email_id, 'NL');
                    $result3 = mysqli_query($this->db, $query3);
                    if(!$result3){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }else{
                        if($call_back==1){
                            return $error;
                        }else{                        
                            $this->checkCampaignAvailability($company_id, 0);
                        }
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "First Campaign failed to insert.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function getAllEmailListForCampaign($company_id,$campaign_id,$call_back){
        $query = sprintf("SELECT c1.*, ct.*, CONCAT('Send ', IF(ct.`delay` IS NULL || TRIM(ct.`delay`)='', 'immediately', CONCAT(ct.`delay`,' ',ct.period)), CASE WHEN ct.`trigger_event_type`='NL' THEN ' when a New lead is added' WHEN ct.`trigger_event_type`='NT' THEN ' when a New trial is added' WHEN ct.`trigger_event_type`='LA' THEN ' after the Last attendance is added' WHEN ct.`trigger_event_type`='LE' THEN ' after the previous email is sent' ELSE '' END) trigger_string,
                IF(c2.`web_page` IS NULL || TRIM(c2.`web_page`)='', '', c2.`web_page`) web_page, c2.`email_id` studio_email, IF(email_sent_count>0,TRUNCATE(email_failed_count/email_sent_count*100,1),0) bounce_rate
                FROM `campaign` c1 LEFT JOIN `campaign_trigger` ct ON c1.`company_id`=ct.`company_id` AND c1.`campaign_id`=ct.`campaign_id`
                LEFT JOIN `company` c2 ON c1.`company_id`=c2.`company_id` 
                WHERE c1.`company_id`='%s' AND `parent_id`='%s' AND c1.`deleted_flag`!='Y' ORDER BY `cmpgn_sort_order`", 
                mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$campaign_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $email_list = [];
                while($row = mysqli_fetch_assoc($result)){
                    $email_list[] = $row;
                 }
                $error =  array('status' => "Success", "msg" => "Available.", "email_list" => $email_list);
                if($call_back==1){
                    return $error;
                }else{
                    $this->response($this->json($error), 200);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "No Campaign emails available.", "email_list" => []);
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function updateCampaignSortingDetails($company_id, $campaign_id, $campaign_sort_order){
        
        $query = sprintf("SELECT * FROM `campaign` where `campaign_id` = '%s' AND `company_id` ='%s'", mysqli_real_escape_string($this->db,$campaign_id), mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $update_message = sprintf("UPDATE `campaign` SET `cmpgn_sort_order`='%s' WHERE `campaign_id`='%s' AND `company_id`='%s'", $campaign_sort_order, $campaign_id, $company_id);
                $result_update = mysqli_query($this->db, $update_message);
                if (!$result_update) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }

                $campaign_list = $this->checkCampaignAvailability($company_id, 1);
                $out = array('status' => "Success", 'msg' => 'Successfully updated', "category_list" => $campaign_list['category_list']);
                        // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            }else{
                $error = array('status' => "Failed", "msg" => "Campaign detail not available.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function changeCampaignStatus($company_id,$campaign_id,$status){
        $query = sprintf("SELECT * FROM `campaign` where `campaign_id` = '%s' AND `company_id` ='%s'",
                mysqli_real_escape_string($this->db,$campaign_id), mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $update_message = sprintf("UPDATE `campaign` SET `status`='%s' WHERE (`campaign_id`='%s' || `parent_id`='%s' ) AND `company_id`='%s'", mysqli_real_escape_string($this->db,$status), mysqli_real_escape_string($this->db,$campaign_id),mysqli_real_escape_string($this->db,$campaign_id), mysqli_real_escape_string($this->db,$company_id));
                $result_update = mysqli_query($this->db, $update_message);
                if (!$result_update) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                
                $campaign_list = $this->checkCampaignAvailability($company_id, 1);
                $out = array('status' => "Success", 'msg' => 'Campaign status updated successfully', "category_list" => $campaign_list['category_list']);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            }
        }
    }
    
    public function updateEmailCampaignInDb($company_id, $parent_id, $campaign_id, $type, $status, $subject, $message, $email_banner_image, $email_banner_image_type,$show_banner_image,$banner_image_update_status,$email_banner_old_image_url,$button_text, $button_url) {
        $query = sprintf("SELECT * FROM `campaign` where `company_id` ='%s' AND `campaign_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $campaign_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                if ($email_banner_image != '' && $show_banner_image == 'Y') {
                    $email_banner_image_url = "";
                    if ($banner_image_update_status == 'Y') {
                        $cfolder_name = "Company_$company_id";
                        $old = getcwd(); // Save the current directory
                        $dir_name = "../../../$this->upload_folder_name";
                        chdir($dir_name);
                        if (!file_exists($cfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($cfolder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($cfolder_name);
                        $folder_name = "Campaigns";
                        if (!file_exists($folder_name)) {
                            $oldmask = umask(0);
                            mkdir($folder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($folder_name);
                        $image_content = base64_decode($email_banner_image);
                        $file_name = $company_id . "-" . $campaign_id . "_" . time();
                        $file = "$file_name.$email_banner_image_type";
                        $img = $file; // logo - your file name
                        $ifp = fopen($img, "wb");
                        fwrite($ifp, $image_content);
                        fclose($ifp);
                        chdir($old); // Restore the old working directory

                        $email_banner_image_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/$cfolder_name/$folder_name/$file";
                    } else if ($banner_image_update_status == 'N') {
                        $email_banner_image_url = $email_banner_old_image_url;
                    }
                    
                    $update_message = sprintf("UPDATE `campaign` c1, `campaign` c2 SET c1.`subject`='%s',c1.`message`='%s',c1.`email_banner_image_url`='%s',c1.`button_text`='%s',c1.`button_url`='%s',c1.`status`='%s' ,c2.`status`='%s' 
                        WHERE c1.`company_id` ='%s' AND c1.`campaign_id`='%s' AND c2.campaign_id='%s'", mysqli_real_escape_string($this->db, $subject), mysqli_real_escape_string($this->db, $message), 
                        mysqli_real_escape_string($this->db, $email_banner_image_url), mysqli_real_escape_string($this->db, $button_text), mysqli_real_escape_string($this->db, $button_url), 
                        mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $campaign_id), mysqli_real_escape_string($this->db, $parent_id));
                }else{
                    $update_message = sprintf("UPDATE `campaign` c1, `campaign` c2 SET c1.`subject`='%s',c1.`message`='%s',c1.`email_banner_image_url`='',c1.`button_text`='%s',c1.`button_url`='%s',c1.`status`='%s' ,c2.`status`='%s' 
                            WHERE c1.`company_id` ='%s' AND c1.`campaign_id`='%s' AND c2.campaign_id='%s'", mysqli_real_escape_string($this->db, $subject), mysqli_real_escape_string($this->db, $message), 
                            mysqli_real_escape_string($this->db, $button_text), mysqli_real_escape_string($this->db, $button_url), 
                            mysqli_real_escape_string($this->db, $status),mysqli_real_escape_string($this->db, $status), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $campaign_id), mysqli_real_escape_string($this->db, $parent_id));
                }
                $result_update = mysqli_query($this->db, $update_message);
                if (!$result_update) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_message");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }

                $email_list = $this->getAllEmailListForCampaign($company_id,$parent_id,1);
                $campaign_list = $this->checkCampaignAvailability($company_id, 1);
                $out = array('status' => "Success", 'msg' => 'Email campaign updated successfully', "category_list" => $campaign_list['category_list'], "email_list" => $email_list['email_list']);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            }else{
                $error = array('status' => "Failed", "msg" => "No Campaign emails available.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function sendTestEmailForCampaign($company_id,$test_email_id,$subject,$message,$email_banner_image,$email_banner_image_type,$show_banner_image,$banner_image_update_status,$email_banner_old_image_url,$button_text,$button_url) {
        
        $body_msg = $cmp_name = $studio_web_page = $file = '';
        $body_msg = '<div style="max-width:800px;">';
        
        $query = sprintf("SELECT `company_name`,IF(`web_page` IS NULL || TRIM(`web_page`)='', '',`web_page`) web_page FROM `company` where `company_id` ='%s'",
                mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $cmp_name = $row['company_name'];
                $studio_web_page = $row['web_page'];
                
                if ($email_banner_image != '' && $show_banner_image == 'Y') {
                    $email_banner_image_url = "";
                    if ($banner_image_update_status == 'Y') {
                        $cfolder_name = "Company_$company_id";
                        $old = getcwd(); // Save the current directory
                        $dir_name = "../../../$this->upload_folder_name";
                        chdir($dir_name);
                        if (!file_exists($cfolder_name)) {
                            $oldmask = umask(0);
                            mkdir($cfolder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($cfolder_name);
                        $folder_name = "Campaigns_test_email";
                        if (!file_exists($folder_name)) {
                            $oldmask = umask(0);
                            mkdir($folder_name, 0777, true);
                            umask($oldmask);
                        }
                        chdir($folder_name);
                        $image_content = base64_decode($email_banner_image);
                        $file_name = $company_id . "-" . time();
                        $file = "$file_name.$email_banner_image_type";
                        $img = $file; // logo - your file name
                        $ifp = fopen($img, "wb");
                        fwrite($ifp, $image_content);
                        fclose($ifp);
                        chdir($old); // Restore the old working directory
                        $email_banner_image_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/$cfolder_name/$folder_name/$file";
                    }else if($banner_image_update_status == 'N'){
                        $email_banner_image_url = $email_banner_old_image_url;
                    }
                    $body_msg .= '<br>' . '<img src="' . $email_banner_image_url . '" alt="Banner Image" style="max-width: 100%;display: block;margin-left: auto;margin-right: auto;width: 800px;" />';
                }
                $body_msg .= $message.'<br><br>';
                
                
                // CHECKING GET STARTED BUTTON TEXT AND LINK
                if(!empty($button_text) && !empty($button_url)){
                    $body_msg .= '<center><a href="'.$button_url.'"><button type="button" style="width: 230px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;padding: 12px 0;height: 50px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">'.$button_text.'</button></a></center>';
                }elseif(!empty($studio_web_page)){
                    $button_text = "Get started";
                    $body_msg .= '<center><a href="'.$studio_web_page.'"><button type="button" style="width: 230px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;padding: 12px 0;height: 50px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">'.$button_text.'</button></a></center>';
                }
                $body_msg .='</div>';
                
                // SENDING EMAIL CAMPAIGN TO TEST EMAIL
                $test_email_check = $this->verifyBouncedEmailForCC($test_email_id, $company_id);
                if ($test_email_check == 1) {
                    $sendEmail_status = $this->sendEmail($test_email_id, $subject, $body_msg, $cmp_name, '', '', '', '');
                    if ($sendEmail_status['status'] == "true") {
                        $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $test_email_id, "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
                    } else {
                        $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $test_email_id, "Email Message" => $sendEmail_status['mail_status']);
                        log_info($this->json($error_log));
                    }
                    $error = array('status' => "Success", "msg" => "Email sent successfully.");
                    $this->response($this->json($error), 200);
                }else{
                    $error = array('status' => "Failed", "msg" => "Please Re-subscribe your email to send test.");
                    $this->response($this->json($error), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "No company details available.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function verifyBouncedEmailForCC($cc_address, $company_id){
        $selecsql=sprintf("SELECT * FROM `bounce_email_verify` b WHERE b.`bounced_mail`='%s' AND (b.`company_id`=0 OR b.`company_id`='%s')", mysqli_real_escape_string($this->db,$cc_address), mysqli_real_escape_string($this->db,$company_id));
        $result= mysqli_query($this->db,$selecsql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
          if(mysqli_num_rows($result)>0){
              return 0;
          }else{
               return 1;
          }
        }
    }
}
?>