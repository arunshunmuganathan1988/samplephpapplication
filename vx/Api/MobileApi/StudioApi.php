<?php

require_once 'MobileModel.php';

class StudioApi extends MobileModel {

    public function __construct() {
        parent::__construct();          // Init parent contructor
    }

    /*
     * Public method for access api.
     * This method dynmically call the method based on the query string
     */

    public function processApi() {
        $func = strtolower(trim(str_replace("/", "", $_REQUEST['run'])));
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)) {
            $this->_request = json_decode(file_get_contents('php://input'), true);
        }
        if ((int) method_exists($this, $func) > 0) {
            if ($_REQUEST['run'] != 'login' && $_REQUEST['run'] != 'verifyStudentUsername' &&
                    $_REQUEST['run'] != 'verifyStudentStudiocode' && $_REQUEST['run'] != 'addStudentRegistrationDetail' &&
                    $_REQUEST['run'] != 'activateStudentAccount' && $_REQUEST['run'] != 'resendVerificationCode' &&
                    $_REQUEST['run'] != 'studentLogin' && $_REQUEST['run'] != 'forgotLoginDetails' &&
                    $_REQUEST['run'] != 'memberappResetPassword' && $_REQUEST['run'] != 'checkActivationStatus' &&
                    $_REQUEST['run'] != 'verifyAppId' && $_REQUEST['run'] != 'guestLogin' && $_REQUEST['run'] != 'OAuthStudentLogin' &&
                    $_REQUEST['run'] != 'sendExpiryEmailToStudio') {
                $companyid = $studentid = '';
                if (isset($this->_request['companyid'])) {
                    $companyid = $this->_request['companyid'];
                }
                if (isset($this->_request['studentid'])) {
                    $studentid = $this->_request['studentid'];
                }

                if ($studentid == -1) { // GUEST USER ID -1
                    $this->$func();
                } else {
                    $verify = $this->verifystudent($companyid, $studentid);
                    if ($verify['status'] == 'Success') {
                        $this->$func();
                    } else {
                        $error = array('status' => "Failed", "msg" => $verify['msg']);
                        $this->response($this->json($error), 401);
                    }
                }
            } else {
                $this->$func();
            }
        } else {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Page not found.");
            $this->response($this->json($error), 404); // If the method not exist with in this class, response would be "Page not found".
        }
    }

    /*
     * 	Simple login API
     *  Login must be POST method
     *  email : <USER EMAIL>
     */

    private function login() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $email = $student_name = $company_code = $push_device_id = $device_type = $app_id = '';

        if (isset($this->_request['student_email'])) {
            $email = $this->_request['student_email'];
        }
        if (isset($this->_request['student_name'])) {
            $student_name = $this->_request['student_name'];
        }
        if (isset($this->_request['company_code'])) {
            $company_code = $this->_request['company_code'];
        }
        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }

        // Input validations
        if (!empty($email) && !empty($student_name) && !empty($company_code)) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->checklogin($email, $company_code, $student_name, $push_device_id, $device_type, $app_id);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function companydetails() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $companyid = $studentid = '';
        $call_back = 0;
        if (isset($this->_request['companyid'])) {
            $companyid = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $studentid = $this->_request['studentid'];
        }

        if (!empty($companyid)) {
            $this->getcompanydetails($companyid, $studentid, $call_back);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function events() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $companyid = $studentid = '';
        if (isset($this->_request['companyid'])) {
            $companyid = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $studentid = $this->_request['studentid'];
        }

        if (!empty($companyid)) {
            $this->getEventsDetailByCompany($companyid);
//            $this->geteventsdetails($companyid);            
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function curriculum() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $companyid = $studentid = '';
        if (isset($this->_request['companyid'])) {
            $companyid = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $studentid = $this->_request['studentid'];
        }

        if (!empty($companyid)) {
            $this->getcurriculumdetails($companyid, $studentid);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    // HANDLE MESSAGES FOR OLDER VERSIONS OF MOBILE APP
    private function getmessage() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $companyid = $student_id = '';
        if (isset($this->_request['companyid'])) {
            $companyid = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }

        if (!empty($companyid) && !empty($student_id)) {
            $this->getmessagesfromdb($companyid, $student_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    // HANDLE CHAT MESSAGES FOR NEW VERSIONS OF MOBILE APP
    private function getchatmessage() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $companyid = $student_id = $user_tz = $last_msg_time = '';
        $last_msg_id = 0;
        $msg_direction = 'O';   // O - Old messages, N - New messages

        if (isset($this->_request['companyid'])) {
            $companyid = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['last_msg_id'])) {
            $last_msg_id = $this->_request['last_msg_id'];
        }
        if (isset($this->_request['user_tz'])) {
            $user_tz = $this->_request['user_tz'];
        }
        if (isset($this->_request['msg_direction'])) {
            $msg_direction = $this->_request['msg_direction'];
        }
        if (isset($this->_request['last_msg_time'])) {
            $last_msg_time = $this->_request['last_msg_time'];
        }

        if (!empty($companyid) && !empty($student_id)) {
            $this->getchatmessagesfromdb($companyid, $student_id, $last_msg_id, $last_msg_time, $user_tz, $msg_direction);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function postchatmessage() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($error, 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = $message_text = $user_tz = $last_msg_time = '';
        $last_msg_id = 0;

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['message_text'])) {
            $message_text = $this->_request['message_text'];
        }
        if (isset($this->_request['last_msg_id'])) {
            $last_msg_id = $this->_request['last_msg_id'];
        }
        if (isset($this->_request['user_tz'])) {
            $user_tz = $this->_request['user_tz'];
        }
        if (isset($this->_request['last_msg_time'])) {
            $last_msg_time = $this->_request['last_msg_time'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($student_id) && !empty($message_text)) {
            $this->postchatmessagesfrommobile($company_id, $student_id, $message_text, $last_msg_id, $last_msg_time, $user_tz);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function referral() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $student_id = $company_id = $referred_name = $email_id = $phone_number = '';
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['referred_name'])) {
            $referred_name = $this->_request['referred_name'];
        }
        if (isset($this->_request['email_id'])) {
            $email_id = trim($this->_request['email_id']);
        }
        if (isset($this->_request['phone_number'])) {
            $phone_number = $this->_request['phone_number'];
        }

        // Input validations
        if (!empty($student_id) && !empty($company_id) && !empty($referred_name) && ((!empty($email_id) && !filter_var($email_id, FILTER_VALIDATE_EMAIL) === false) || !empty($phone_number))) {
            $this->storeReferralDetails($student_id, $company_id, $referred_name, $email_id, $phone_number);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function getwepaystatus() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }

        if (!empty($company_id)) {
            $this->wepayStatus($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function getstripestatus() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }

        if (!empty($company_id)) {
            $this->stripeStatus($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function wepaycheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_name = $sub_events_id = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = $payment_frequency = $discount_code = '';
        $student_id = $participant_id = $fee = $discount = $registration_amount = $payment_amount = $total_order_quantity = $total_order_amount = 0;
        $quantity = 1;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $event_array = $payment_array = [];
        $upgrade_status = 'F';
        $country = 'US';
        $reg_type_user = 'M';
        $default_cc = 'N';
        $version_number = $build_number = '';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $device_type = $app_id = $push_device_id = '';

        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }
        if (isset($this->_request['version_number'])) {
            $version_number = $this->_request['version_number'];
        }
        if (isset($this->_request['build_number'])) {
            $build_number = $this->_request['build_number'];
        }
        $reg_version_user = "mob_ver-" . $version . " ver_num-" . $version_number . " build_num-" . $build_number;
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['event_name'])) {
            $event_name = $this->_request['event_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if (isset($this->_request['event_array'])) {
            $event_array = $this->_request['event_array'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['cc_id'])) {
            $cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_state'])) {
            $cc_state = $this->_request['cc_state'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['fee'])) {
            $fee = $this->_request['fee'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
        if (isset($this->_request['quantity'])) {
            $quantity = $this->_request['quantity'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['reg_col4'])) {
            $reg_col4 = $this->_request['reg_col4'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['payment_frequency'])) {
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if (isset($this->_request['event_array'])) {
            $event_array = $this->_request['event_array'];
        }
        if (isset($this->_request['payment_array'])) {
            $payment_array = $this->_request['payment_array'];
        }
        if (isset($this->_request['event_type'])) {
            $event_type = $this->_request['event_type'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['total_order_amount'])) {
            $total_order_amount = $this->_request['total_order_amount'];
        }
        if (isset($this->_request['total_order_quantity'])) {
            $total_order_quantity = $this->_request['total_order_quantity'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['default_cc'])) {
            $default_cc = $this->_request['default_cc'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if ($event_type == 'S' && $payment_type == 'onetimesingle') {
            $event_array = [];
            $payment_array = [];
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($cc_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
                $this->wepayCreateCheckout($company_id, $event_id, $student_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, 4, 'CO', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user, $discount_code, $default_cc, $participant_id, $device_type, $app_id, $push_device_id);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }
        } elseif ($event_type == 'S' && $payment_type == 'singlerecurring') {
            $event_array = [];
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($cc_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($payment_array)) {
                $this->wepayCreateCheckout($company_id, $event_id, $student_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, 'RE', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user, $discount_code, $default_cc, $participant_id, $device_type, $app_id, $push_device_id);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }
        } elseif ($event_type == 'M' && $payment_type == 'onetimemultiple') {
            $payment_array = [];
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($cc_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($event_array)) {
                $this->wepayCreateCheckout($company_id, $event_id, $student_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, 4, 'CO', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user, $discount_code, $default_cc, $participant_id, $device_type, $app_id, $push_device_id);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }
        } elseif ($event_type == 'M' && $payment_type == 'multiplerecurring') {
            if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($cc_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($payment_array) && !empty($event_array)) {
                $this->wepayCreateCheckout($company_id, $event_id, $student_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, 'RE', $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user, $discount_code, $default_cc, $participant_id, $device_type, $app_id, $push_device_id);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function addParticipantDetails() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_name = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $processing_fee_type = $event_type = $discount_code = '';
        $student_id = $participant_id = $fee = $dicount = $registration_amount = $payment_amount = 0;
        $quantity = 1;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $event_array = [];
        $reg_type_user = 'M';
        $version_number = $build_number = '';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $device_type = $app_id = $push_device_id = '';

        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }
        if (isset($this->_request['version_number'])) {
            $version_number = $this->_request['version_number'];
        }
        if (isset($this->_request['build_number'])) {
            $build_number = $this->_request['build_number'];
        }
        $reg_version_user = "mob_ver-" . $version . ", ver_num-" . $version_number . ", build_num-" . $build_number;
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['event_name'])) {
            $event_name = $this->_request['event_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['fee'])) {
            $fee = $this->_request['fee'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['reg_col4'])) {
            $reg_col4 = $this->_request['reg_col4'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
        if (isset($this->_request['quantity'])) {
            $quantity = $this->_request['quantity'];
        }
        if (isset($this->_request['event_type'])) {
            $event_type = $this->_request['event_type'];
        }
        if (isset($this->_request['event_array'])) {
            $event_array = $this->_request['event_array'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }


        if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($event_type)) {
            if ($event_type != 'M' && $event_type != 'S') {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            } else {
                if ($event_type == 'M' && empty($event_array)) {
                    $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 400);
                } else {
                    $this->addParticipantDetailsForFreeEvent($company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_type, $event_array, $reg_type_user, $reg_version_user, $discount_code, $device_type, $app_id, $push_device_id);
                }
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    //Membership related functions
    private function membership() {

        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';

        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }

        if (!empty($company_id)) {
            $this->getAllMembershipDetails($company_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function wepayMembershipCheckout() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_option_id = $membership_category_title = $membership_title = $membership_desc = $recurring_start_date = '';
        $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = $payment_frequency = $membership_start_date = $payment_start_date = $delay_recurring_payment_start_date = $custom_recurring_frequency_period_type = '';
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = $membership_discount_code = $signup_discount_code = '';
        $student_id = $participant_id = $payment_amount = $processing_fee = $membership_fee = $membership_fee_disc = $signup_fee = $signup_fee_disc = $initial_payment = $custom_recurring_frequency_period_val = 0;
        $upgrade_status = 'F';
        $country = 'US';
        $prorate_first_payment_flg = $delay_recurring_payment_start_flg = 'N';
        $membership_payment_array = ['N', 'W', 'B', 'M', 'A', 'C'];
        $payment_array = [];
        $no_of_classes = $billing_options_deposit_amount = $billing_options_no_of_payments = $delay_recurring_payment_amount = $first_payment = 0;
        $billing_options = $billing_options_expiration_date = '';
        $membership_structure = 'OE';
        $initial_payment_include_membership_fee = 'Y';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        $reg_type_user = 'M';
        $version_number = $build_number = '';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $exclude_from_billing_flag = 'N';
        $exclude_days_array = [];
        $default_cc = 'N';
        $program_start_date = $program_end_date = $reg_current_date = '';
        $device_type = $app_id = $push_device_id = '';

        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }

        if (isset($this->_request['version_number'])) {
            $version_number = $this->_request['version_number'];
        }
        if (isset($this->_request['build_number'])) {
            $build_number = $this->_request['build_number'];
        }
        $reg_version_user = "mob_ver-" . $version . ", ver_num-" . $version_number . ", build_num-" . $build_number;
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['membership_category_title'])) {
            $membership_category_title = $this->_request['membership_category_title'];
        }
        if (isset($this->_request['membership_title'])) {
            $membership_title = $this->_request['membership_title'];
        }
        if (isset($this->_request['membership_desc'])) {
            $membership_desc = $this->_request['membership_desc'];
        }
        if (isset($this->_request['membership_structure'])) {
            $membership_structure = $this->_request['membership_structure'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['cc_id'])) {
            $cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_state'])) {
            $cc_state = $this->_request['cc_state'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['processing_fee'])) {
            $processing_fee = $this->_request['processing_fee'];
        }
        if (isset($this->_request['membership_fee'])) {
            $membership_fee = $this->_request['membership_fee'];
        }
        if (isset($this->_request['membership_fee_disc'])) {
            $membership_fee_disc = $this->_request['membership_fee_disc'];
        }
        if (isset($this->_request['signup_fee'])) {
            $signup_fee = $this->_request['signup_fee'];
        }
        if (isset($this->_request['signup_fee_disc'])) {
            $signup_fee_disc = $this->_request['signup_fee_disc'];
        }
        if (isset($this->_request['initial_payment'])) {      //registration time payment
            $initial_payment = $this->_request['initial_payment'];
        }
        if (isset($this->_request['first_payment'])) {      //first payment on membership start date
            $first_payment = $this->_request['first_payment'];
        }
        if (isset($this->_request['payment_frequency'])) {
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if (isset($this->_request['payment_start_date'])) {
            $payment_start_date = $this->_request['payment_start_date'];
        }
        if (isset($this->_request['membership_start_date'])) {
            $membership_start_date = $this->_request['membership_start_date'];
        }
        if (isset($this->_request['recurring_start_date'])) {
            $recurring_start_date = $this->_request['recurring_start_date'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['reg_col4'])) {
            $reg_col4 = $this->_request['reg_col4'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['prorate_first_payment_flg']) && !empty(trim($this->_request['prorate_first_payment_flg']))) {
            $prorate_first_payment_flg = $this->_request['prorate_first_payment_flg'];
        }
        if (isset($this->_request['delay_recurring_payment_start_flg']) && !empty(trim($this->_request['delay_recurring_payment_start_flg']))) {
            $delay_recurring_payment_start_flg = $this->_request['delay_recurring_payment_start_flg'];
        }
        if (isset($this->_request['delay_recurring_payment_start_date'])) {
            $delay_recurring_payment_start_date = $this->_request['delay_recurring_payment_start_date'];
        }
        if (isset($this->_request['delay_recurring_payment_amount'])) {
            $delay_recurring_payment_amount = $this->_request['delay_recurring_payment_amount'];
        }
        if (isset($this->_request['custom_recurring_frequency_period_type'])) {
            $custom_recurring_frequency_period_type = $this->_request['custom_recurring_frequency_period_type'];
        }
        if (isset($this->_request['custom_recurring_frequency_period_val'])) {
            $custom_recurring_frequency_period_val = $this->_request['custom_recurring_frequency_period_val'];
        }
        if (isset($this->_request['initial_payment_include_membership_fee'])) {
            $initial_payment_include_membership_fee = $this->_request['initial_payment_include_membership_fee'];
        }
        if (isset($this->_request['billing_options'])) {
            $billing_options = $this->_request['billing_options'];
        }
        if (isset($this->_request['no_of_classes'])) {
            $no_of_classes = $this->_request['no_of_classes'];
        }
        if (isset($this->_request['billing_options_expiration_date'])) {
            $billing_options_expiration_date = $this->_request['billing_options_expiration_date'];
        }
        if (isset($this->_request['billing_options_deposit_amount'])) {
            $billing_options_deposit_amount = $this->_request['billing_options_deposit_amount'];
        }
        if (isset($this->_request['billing_options_no_of_payments'])) {
            $billing_options_no_of_payments = $this->_request['billing_options_no_of_payments'];
        }
        if (isset($this->_request['payment_array'])) {
            $payment_array = $this->_request['payment_array'];
        }
        if (isset($this->_request['participant_street'])) {
            $participant_street = $this->_request['participant_street'];
        }
        if (isset($this->_request['participant_city'])) {
            $participant_city = $this->_request['participant_city'];
        }
        if (isset($this->_request['participant_state'])) {
            $participant_state = $this->_request['participant_state'];
        }
        if (isset($this->_request['participant_zip'])) {
            $participant_zip = $this->_request['participant_zip'];
        }
        if (isset($this->_request['participant_country'])) {
            $participant_country = $this->_request['participant_country'];
        }
        if (isset($this->_request['membership_discount_code'])) {
            $membership_discount_code = $this->_request['membership_discount_code'];
        }
        if (isset($this->_request['signup_discount_code'])) {
            $signup_discount_code = $this->_request['signup_discount_code'];
        }
        if (isset($this->_request['exclude_days_array'])) {
            $exclude_days_array = $this->_request['exclude_days_array'];
        }
        if (isset($this->_request['program_start_date'])) {
            $program_start_date = $this->_request['program_start_date'];
        }
        if (isset($this->_request['program_end_date'])) {
            $program_end_date = $this->_request['program_end_date'];
        }
        if (isset($this->_request['exclude_from_billing_flag'])) {
            $exclude_from_billing_flag = $this->_request['exclude_from_billing_flag'];
        }
        if (isset($this->_request['registration_date'])) {
            $reg_current_date = $this->_request['registration_date'];
        }
        if (isset($this->_request['default_cc'])) {
            $default_cc = $this->_request['default_cc'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }

        //Input validations
        if (!empty(trim($payment_frequency)) && !in_array($payment_frequency, $membership_payment_array)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if (!empty(trim($payment_frequency)) && $payment_frequency == 'C' && (empty(trim($custom_recurring_frequency_period_type)) || empty(trim($custom_recurring_frequency_period_val)))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if (!empty($membership_structure) && $membership_structure == 'OE' && $payment_amount > 0 && !empty(trim($delay_recurring_payment_start_flg)) && $delay_recurring_payment_start_flg == 'Y' && empty(trim($delay_recurring_payment_start_date))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if (!empty($membership_structure) && $membership_structure == 'NC' && empty($billing_options)) {
            if (!empty($billing_options) && $billing_options == 'PP' && empty($payment_array)) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }
        }
        if (!empty($membership_structure) && $membership_structure == 'SE' && ((empty($program_start_date) || $program_start_date == '0000-00-00' || empty($program_end_date) || $program_end_date == '0000-00-00') || (!empty($billing_options) && $billing_options == 'PP' && $payment_amount > 0 && empty($payment_array)))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }


        if (!empty($company_id) && !empty($membership_id) && !empty($membership_option_id) && !empty($membership_category_title) && !empty($membership_title) && !empty($membership_desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
            $this->membershipCheckout($company_id, $membership_id, $membership_option_id, $student_id, $upgrade_status, $membership_category_title, $membership_title, $membership_desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $processing_fee_type, $processing_fee, $membership_fee, $membership_fee_disc, $signup_fee, $signup_fee_disc, $payment_amount, $initial_payment, $first_payment, $payment_frequency, $payment_start_date, $recurring_start_date, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $prorate_first_payment_flg, $delay_recurring_payment_start_flg, $delay_recurring_payment_start_date, $delay_recurring_payment_amount, $custom_recurring_frequency_period_type, $custom_recurring_frequency_period_val, $initial_payment_include_membership_fee, $membership_structure, $billing_options, $no_of_classes, $billing_options_expiration_date, $billing_options_deposit_amount, $billing_options_no_of_payments, $payment_array, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $membership_discount_code, $signup_discount_code, $exclude_days_array, $program_start_date, $program_end_date, $exclude_from_billing_flag, $reg_type_user, $reg_version_user, $reg_current_date, $default_cc, $participant_id, $membership_start_date, $device_type, $app_id, $push_device_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function updateDeviceId() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $student_id = $push_device_id = $device_type = '';
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }

        if (!empty($company_id) && !empty($student_id) && !empty($push_device_id) && !empty($device_type)) {
            $this->updateDeviceIdinDb($company_id, $student_id, $push_device_id, $device_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    // REGISTRATION FUNCTIONS 
    private function verifyStudentUsername() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $student_username = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['student_username'])) {
            $student_username = $this->_request['student_username'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($student_username)) {
            $this->checkStudentUsernameAvailability($student_username);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function verifyStudentStudiocode() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_code = $app_id = $device_type = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['company_code'])) {
            $company_code = $this->_request['company_code'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_code) && !empty($app_id) && !empty($device_type)) {
            $this->checkStudentCompanyCode($company_code, $app_id, $device_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function addStudentRegistrationDetail() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_firstname = $student_lastname = $student_username = $student_password = $student_email = $student_mobile = $company_code = $app_type = $push_device_id = $device_type = $app_id = '';
        $student_reg_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['student_firstname'])) {
            $student_firstname = $this->_request['student_firstname'];
        }
        if (isset($this->_request['student_lastname'])) {
            $student_lastname = $this->_request['student_lastname'];
        }
        if (isset($this->_request['student_username'])) {
            $student_username = $this->_request['student_username'];
        }
        if (isset($this->_request['student_password'])) {
            $student_password = $this->_request['student_password'];
        }
        if (isset($this->_request['student_email'])) {
            $student_email = strtolower($this->_request['student_email']);
        }
        if (isset($this->_request['student_mobile'])) {
            $student_mobile = $this->_request['student_mobile'];
        }
//        if (isset($this->_request['company_code'])) {
//            $company_code = $this->_request['company_code'];
//        }
        if (isset($this->_request['app_type'])) {
            $app_type = $this->_request['app_type'];
        }
        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }
        if (isset($this->_request['student_reg_id'])) {
            $student_reg_id = $this->_request['student_reg_id'];
        }

//        if ($app_type == 'ML' && empty($company_code)) {
//            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
//            $this->response($this->json($error), 400);
//        }
        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($student_firstname) && !empty($student_lastname) && !empty($student_username) && !empty($student_password) && !empty($student_email) && !empty($student_mobile) && !empty($app_type) && !empty($device_type) && !empty($app_id)) {
            $this->addStudentRegistrationDetails($company_id, $student_firstname, $student_lastname, $student_username, $student_password, $student_email, $student_mobile, $push_device_id, $device_type, $app_id, $student_reg_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    public function activateStudentAccount() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $register_verification_id = $user_type = $registration_code = $activated_by = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['register_verification_id'])) {
            $register_verification_id = $this->_request['register_verification_id'];
        }
        if (isset($this->_request['user_type'])) {
            $user_type = $this->_request['user_type'];
        }
        if (isset($this->_request['registration_code'])) {
            $registration_code = $this->_request['registration_code'];
        }
        if (isset($this->_request['activated_by'])) {
            $activated_by = $this->_request['activated_by'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($register_verification_id) && !empty($user_type) && !empty($registration_code) && !empty($activated_by)) {
            $this->activateStudentAccountVerification($register_verification_id, $user_type, $registration_code, $activated_by);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    public function resendVerificationCode() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = $register_verification_id = $user_type = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['register_verification_id'])) {
            $register_verification_id = $this->_request['register_verification_id'];
        }
        if (isset($this->_request['user_type'])) {
            $user_type = $this->_request['user_type'];
        }
        $call_back = 0;

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($student_id) && !empty($register_verification_id) && !empty($user_type)) {
            $this->resendVerficationDetailsEmail($company_id, $student_id, $register_verification_id, $user_type, $call_back);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    // STUDENT LOGIN FUNCTIONS
    public function studentLogin() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $student_username = $student_password = $push_device_id = $device_type = $app_id = $app_type = $company_code = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['student_username'])) {
            $student_username = $this->_request['student_username'];
        }
        if (isset($this->_request['student_password'])) {
            $student_password = $this->_request['student_password'];
        }
        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }
        if (isset($this->_request['app_type'])) {
            $app_type = $this->_request['app_type'];
        }
        if (isset($this->_request['company_code'])) {
            $company_code = $this->_request['company_code'];
        }

        if (isset($app_type) && $app_type == 'ML' && empty($company_code)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($student_username) && !empty($student_password) && !empty($device_type) && !empty($app_id)) {
            $this->checkStudentLogin($student_username, $student_password, $push_device_id, $device_type, $app_id, $app_type, $company_code);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    // STUDENT LOGIN FUNCTIONS
    public function OAuthStudentLogin() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $login_type = $user_detail = $user_email = $push_device_id = $device_type = $app_id = $company_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['login_type'])) {
            $login_type = $this->_request['login_type'];
        }
        if (isset($this->_request['user_detail'])) {
            $user_detail = $this->_request['user_detail'];
        }
        if (isset($this->_request['user_email'])) {
            $user_email = $this->_request['user_email'];
        }
        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }

        if (isset($login_type) && (empty($login_type) || (($login_type == 'Facebook' || $login_type == 'Google') && empty($user_detail)))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if (($login_type == 'Facebook' || $login_type == 'Google') && !empty($user_detail)) {
            if ((empty($user_detail['email']) || $user_detail['email'] == 'null' || is_null($user_detail['email'])) &&
                    (empty($user_detail['providerData'][0]['email']) || $user_detail['providerData'][0]['email'] == 'null' || is_null($user_detail['providerData'][0]['email'])) && empty($user_email)) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }
//            if($login_type=='Google' && ((empty($user_detail['email']) || $user_detail['email']=='null' || is_null($user_detail['email'])) && empty($user_email))){
//                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
//                $this->response($this->json($error), 400);
//            }
//            if($login_type=='Facebook' && (empty($user_detail['providerData'][0]['email']) || $user_detail['providerData'][0]['email']=='null' || is_null($user_detail['providerData'][0]['email'])) && empty($user_email)){
//                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
//                $this->response($this->json($error), 400);
//            }
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($device_type) && !empty($app_id)) {
            $this->checkStudentLoginByOAuth($company_id, $login_type, $user_detail, $user_email, $push_device_id, $device_type, $app_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    public function forgotLoginDetails() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $student_email = $company_code = $recovery_type = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['student_email'])) {
            $student_email = strtolower($this->_request['student_email']);
        }
        if (isset($this->_request['company_code'])) {
            $company_code = $this->_request['company_code'];
        }
        if (isset($this->_request['recovery_type'])) {
            $recovery_type = $this->_request['recovery_type'];
        }


        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($student_email) && !empty($company_code) && !empty($recovery_type)) {
            $this->recoverLoginDetails($student_email, $company_code, $recovery_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    public function memberappResetPassword() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $student_id = $student_username = $student_email = $student_password = $reset_password_timer = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['student_username'])) {
            $student_username = $this->_request['student_username'];
        }
        if (isset($this->_request['student_email'])) {
            $student_email = strtolower($this->_request['student_email']);
        }
        if (isset($this->_request['student_password'])) {
            $student_password = $this->_request['student_password'];
        }
        if (isset($this->_request['reset_timer_start'])) {
            $reset_password_timer = $this->_request['reset_timer_start'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($student_id) && !empty($student_username) && !empty($student_email) && !empty($student_password) && !empty($reset_password_timer)) {
            $this->resetMemberappPassword($student_id, $student_username, $student_email, $student_password, $reset_password_timer);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    public function checkActivationStatus() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }


        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($student_id) && !empty($company_id)) {
            $this->verifyActivationStatus($student_id, $company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    public function addParticipantProfile() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = $participant_first_name = $participant_last_name = $participant_date_of_birth = '';
        $flag = '';    //flag indicates from where this call come from M - Menu, MR - Membership registration, ER - Event Registration
        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_first_name'])) {
            $participant_first_name = $this->_request['participant_first_name'];
        }
        if (isset($this->_request['participant_last_name'])) {
            $participant_last_name = $this->_request['participant_last_name'];
        }
        if (isset($this->_request['date_of_birth'])) {
            $participant_date_of_birth = $this->_request['date_of_birth'];
        }
        if (isset($this->_request['flag'])) {
            $flag = $this->_request['flag'];
        }

        if ($flag != 'ER' && empty($participant_date_of_birth)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($student_id) && !empty($participant_first_name) && !empty($participant_last_name) && !empty($flag)) {
            $this->addParticipantProfileDetails($company_id, $student_id, $participant_first_name, $participant_last_name, $participant_date_of_birth, $flag);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    public function editParticipantProfile() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = $participant_id = $participant_first_name = $participant_last_name = $participant_date_of_birth = $edit_type = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['participant_first_name'])) {
            $participant_first_name = $this->_request['participant_first_name'];
        }
        if (isset($this->_request['participant_last_name'])) {
            $participant_last_name = $this->_request['participant_last_name'];
        }
        if (isset($this->_request['date_of_birth'])) {
            $participant_date_of_birth = $this->_request['date_of_birth'];
        }
        if (isset($this->_request['edit_type'])) {
            $edit_type = $this->_request['edit_type'];
        }

        if (($edit_type != 'edit' && $edit_type != 'delete') || ($edit_type == 'edit' && (empty($participant_date_of_birth) || is_null($participant_date_of_birth) || $participant_date_of_birth == '0000-00-00'))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($student_id) && !empty($participant_id) && !empty($participant_first_name) && !empty($participant_last_name) && !empty($edit_type)) {
            $this->editParticipantProfileDetails($company_id, $student_id, $participant_id, $participant_first_name, $participant_last_name, $participant_date_of_birth, $edit_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function addcreditCardDetails() {

        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $student_id = $company_id = $buyer_name = $buyer_email = $postal_code = $new_cc_id = $cc_state = $country = '';
        $type = $payment_method_id = $payment_support = $stripe_payment_method_id = $first_name = $last_name = '';
        $user_type = 'N';       //user type N- Normal (logged-in) user, G - Guest user

        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['buyer_name'])) {
            $buyer_name = $this->_request['buyer_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['postal_code'])) {
            $postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['cc_id'])) {
            $new_cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_state'])) {
            $cc_state = $this->_request['cc_state'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['type'])) {
            $type = $this->_request['type'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['payment_support'])) {
            $payment_support = $this->_request['payment_support'];
        }
        if (isset($this->_request['stripe_payment_method_id'])) {
            $stripe_payment_method_id = $this->_request['stripe_payment_method_id'];
        }
        if (isset($this->_request['first_name'])) {
            $first_name = $this->_request['first_name'];
        }
        if (isset($this->_request['last_name'])) {
            $last_name = $this->_request['last_name'];
        }

        if (isset($this->_request['user_type']) && !empty($this->_request['user_type'])) {
            $user_type = $this->_request['user_type'];
            if ($user_type != 'G' && $user_type != 'N') {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }
        }
        if (!empty($type) && ($type != 'A' && $type != 'U')) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if (!empty($payment_support) && ($payment_support == 'W') && empty($new_cc_id)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        
        if (!empty($company_id) && !empty($student_id) && !empty($buyer_name) && !empty($buyer_email) && !empty($type) && !empty($user_type) && !empty($payment_support)) {
            $this->addOrUpdatePaymentMethodDetails($company_id, $student_id, $new_cc_id, $cc_state, $buyer_name, $buyer_email, $postal_code, $country, $type, $payment_method_id, $user_type, $payment_support, $stripe_payment_method_id, $first_name, $last_name);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.4 - ");
            $this->response($this->json($error), 400);
        }
    }

    public function editBuyerInformation() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = $student_firstname = $student_lastname = $billing_address1 = $billing_address2 = $city = $state = $zipcode = $country = $student_email = $student_mobile = $student_cc_email = '';
        $flag = '';    //flag indicates from where this call come from M - Menu, MR - Membership registration, ER - Event Registration, TR - Trial Registration
        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['student_name'])) {
            $student_firstname = $this->_request['student_name'];
        }
        if (isset($this->_request['student_lastname'])) {
            $student_lastname = $this->_request['student_lastname'];
        }
        if (isset($this->_request['billing_address1'])) {
            $billing_address1 = $this->_request['billing_address1'];
        }
        if (isset($this->_request['billing_address2'])) {
            $billing_address2 = $this->_request['billing_address2'];
        }
        if (isset($this->_request['city'])) {
            $city = $this->_request['city'];
        }
        if (isset($this->_request['state'])) {
            $state = $this->_request['state'];
        }
        if (isset($this->_request['zipcode'])) {
            $zipcode = $this->_request['zipcode'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['student_email'])) {
            $student_email = $this->_request['student_email'];
        }
        if (isset($this->_request['student_mobile'])) {
            $student_mobile = $this->_request['student_mobile'];
        }
        if (isset($this->_request['student_cc_email'])) {
            $student_cc_email = $this->_request['student_cc_email'];
        }
        if (isset($this->_request['flag'])) {
            $flag = $this->_request['flag'];
        }
        if ($flag != 'M' && $flag != 'ER' && $flag != 'MR' && $flag != 'TR') {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if ($flag != 'ER' && (empty($billing_address1) || empty($city) || empty($state) || empty($zipcode) || empty($country))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if ($flag == 'M' && (empty($student_email) || empty($student_mobile))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($student_id) && !empty($student_firstname) && !empty($student_lastname) && !empty($flag)) {
            $this->updateBuyerInformationDetails($company_id, $student_id, $student_firstname, $student_lastname, $billing_address1, $billing_address2, $city, $state, $zipcode, $country, $flag, $student_email, $student_mobile, $student_cc_email);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    public function getStudentAllDetail() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($student_id)) {
            $this->getStudentAllDetails($company_id, $student_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    // EDIT REGISTRATION FUNCTIONS 
    private function verifyStudentEmail() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = $student_email = '';
        $call_back = 0;

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['student_email'])) {
            $student_email = $this->_request['student_email'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($student_id) && !empty($student_email)) {
            $this->checkStudentEmailAvailability($company_id, $student_id, $student_email, $call_back);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    // EDIT REGISTRATION FUNCTIONS 
    private function updateStudentEmailAndPhone() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = $student_email = $student_mobile = $register_verification_id = $student_name = $student_lastname = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['student_email'])) {
            $student_email = $this->_request['student_email'];
        }
        if (isset($this->_request['student_mobile'])) {
            $student_mobile = $this->_request['student_mobile'];
        }
        if (isset($this->_request['student_name'])) {
            $student_name = $this->_request['student_name'];
        }
        if (isset($this->_request['student_lastname'])) {
            $student_lastname = $this->_request['student_lastname'];
        }
        if (isset($this->_request['register_verification_id'])) {
            $register_verification_id = $this->_request['register_verification_id'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($student_id) && !empty($student_email) && !empty($student_mobile) && !empty($student_name) && !empty($student_lastname) && !empty($register_verification_id)) {
            $this->updateStudentEmailAndPhoneDetails($company_id, $student_id, $student_email, $student_mobile, $student_name, $student_lastname, $register_verification_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function trialdetails() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';

        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }

        if (!empty($company_id)) {
            $this->getTrialDetail($company_id, 0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }

    private function wepayTrialprogramCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_name = $discount_code = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = $program_length_type = $payment_type = $start_date = '';
        $student_id = $participant_id = $fee = $discount = $registration_amount = $payment_amount = $program_length = 0;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $upgrade_status = 'F';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        $country = 'US';
        $version_number = $build_number = '';
        $reg_type_user = 'M';
        $default_cc = 'N';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $device_type = $app_id = $push_device_id = '';

        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }
        if (isset($this->_request['version_number'])) {
            $version_number = $this->_request['version_number'];
        }
        if (isset($this->_request['build_number'])) {
            $build_number = $this->_request['build_number'];
        }
        $reg_version_user = "mob_ver-" . $version . ", ver_num-" . $version_number . ", build_num-" . $build_number;
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['trial_name'])) {
            $trial_name = $this->_request['trial_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['cc_id'])) {
            $cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_state'])) {
            $cc_state = $this->_request['cc_state'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
        if (isset($this->_request['payment_startdate'])) {
            $start_date = $this->_request['payment_startdate'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['lead_source'])) {
            $reg_col4 = $this->_request['lead_source'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['program_length_type'])) {
            $program_length_type = $this->_request['program_length_type'];
        }

        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['program_length'])) {
            $program_length = $this->_request['program_length'];
        }
        if (isset($this->_request['default_cc'])) {
            $default_cc = $this->_request['default_cc'];
        }
        if (isset($this->_request['participant_street'])) {
            $participant_street = $this->_request['participant_street'];
        }
        if (isset($this->_request['participant_city'])) {
            $participant_city = $this->_request['participant_city'];
        }
        if (isset($this->_request['participant_state'])) {
            $participant_state = $this->_request['participant_state'];
        }
        if (isset($this->_request['participant_zip'])) {
            $participant_zip = $this->_request['participant_zip'];
        }
        if (isset($this->_request['participant_country'])) {
            $participant_country = $this->_request['participant_country'];
        }

        if (!empty($company_id) && !empty($trial_id) && !empty($trial_name) && (!empty($cc_id) || $payment_amount == 0) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
            $this->trialProgramCheckout($company_id, $trial_id, $student_id, $participant_id, $trial_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $processing_fee_type, $discount, $payment_type, $upgrade_status, $discount_code, $reg_type_user, $reg_version_user, $program_length, $program_length_type, $start_date, $default_cc, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $device_type, $app_id, $push_device_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }

    public function verifyAppId() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $app_id = $device_type = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($app_id) && !empty($device_type)) {
            $this->verifyAppIdInDB($app_id, $device_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    // GUEST LOGIN FUNCTIONS
    public function guestLogin() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }


        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($student_id)) {
            $this->checkGuestLogin($company_id, $student_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function sendExpiryEmailToStudio() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }


        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id)) {
            $this->sendExpiryEmailDetailsToStudio($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function retail() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id)) {
            $this->getRetailDetails($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function retailCheckout() {
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($error, 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $student_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id)) {
            $this->getRetailDetails($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function updatePhoneLogout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $student_id = '';
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }

        if (!empty($company_id) && !empty($student_id)) {
            $this->updatePhoneLogoutinDb($company_id, $student_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }

    private function setupIntent() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $buyer_email = $student_id = $payment_method_id = $actual_student_id = $stripe_customer_id = '';
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['buyer_email'])) {
            $buyer_email = $this->_request['buyer_email'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['studentid'])) {
            $actual_student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['stripe_customer_id'])) {
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }

        if (!empty($company_id) && !empty($payment_method_id) && !empty($buyer_email) && !empty($buyer_first_name) && !empty($buyer_last_name)) {
            $this->setupIntentCreation($company_id, $actual_student_id, $payment_method_id, $buyer_email, $buyer_first_name, $buyer_last_name, $stripe_customer_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }

    private function stripeTrialprogramCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $trial_id = $trial_name = $discount_code = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $processing_fee_type = $program_length_type = $payment_type = $start_date = '';
        $student_id = $participant_id = $fee = $discount = $registration_amount = $payment_amount = $program_length = 0;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = $payment_method_id = '';
        $upgrade_status = 'F';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        $country = 'US';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = '';
        $version_number = $build_number = '';
        $reg_type_user = 'M';
        $device_type = $app_id = $push_device_id = '';
        $intent_method_type = $verification_status = $payment_intent_id = $cc_type = $stripe_customer_id = $registration_date = $default_cc = '';

        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }
        if (isset($this->_request['version_number'])) {
            $version_number = $this->_request['version_number'];
        }
        if (isset($this->_request['build_number'])) {
            $build_number = $this->_request['build_number'];
        }
        $reg_version_user = "mob_ver-" . $version . ", ver_num-" . $version_number . ", build_num-" . $build_number;
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['trial_id'])) {
            $trial_id = $this->_request['trial_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['trial_name'])) {
            $trial_name = $this->_request['trial_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['fee'])) {
            $fee = $this->_request['fee'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['lead_source'])) {
            $reg_col4 = $this->_request['lead_source'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['participant_street'])) {
            $participant_street = $this->_request['participant_street'];
        }
        if (isset($this->_request['participant_city'])) {
            $participant_city = $this->_request['participant_city'];
        }
        if (isset($this->_request['participant_state'])) {
            $participant_state = $this->_request['participant_state'];
        }
        if (isset($this->_request['participant_zip'])) {
            $participant_zip = $this->_request['participant_zip'];
        }
        if (isset($this->_request['participant_country'])) {
            $participant_country = $this->_request['participant_country'];
        }
        if (isset($this->_request['program_length'])) {
            $program_length = $this->_request['program_length'];
        }
        if (isset($this->_request['program_length_type'])) {
            $program_length_type = $this->_request['program_length_type'];
        }
        if (isset($this->_request['payment_startdate'])) {
            $start_date = $this->_request['payment_startdate'];
        }
        if (isset($this->_request['registration_date'])) {
            $registration_date = $this->_request['registration_date'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['intent_method_type'])) {   //  TYPE -> SI / PI 
            $intent_method_type = $this->_request['intent_method_type'];
        }
        if (isset($this->_request['cc_type'])) {    // cc_type - N - New, E - Existing
            $cc_type = $this->_request['cc_type'];
        }
        if (isset($this->_request['stripe_customer_id'])) {    // cc_type - N - New, E - Existing
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        if (isset($this->_request['default_cc'])) {
            $default_cc = $this->_request['default_cc'];
        }
        if ($payment_amount > 0 && $cc_type == 'E' && empty($payment_method_id)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }

        if (!empty($company_id) && !empty($trial_id) && !empty($trial_name) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
            $this->stripetrialProgramCheckoutcreate($push_device_id, $device_type, $app_id, $reg_version_user, $company_id, $trial_id, $student_id, $participant_id, $trial_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $registration_amount, $payment_amount, $discount, $discount_code, $processing_fee_type, $fee, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $upgrade_status, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $program_length, $program_length_type, $start_date, $registration_date, $payment_method_id, $intent_method_type, $cc_type, $stripe_customer_id, $default_cc, $reg_type_user);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }

    private function stripeEventCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $event_id = $event_name = $event_type = $sub_events_id = $desc = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $processing_fee_type = $payment_frequency = $discount_code = '';
        $student_id = $participant_id = $fee = $discount = $registration_amount = $payment_amount = $total_order_quantity = $total_order_amount = 0;
        $quantity = 1;
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = '';
        $event_array = $payment_array = [];
        $upgrade_status = $p_type = 'F';        //$p_type F - Free, CO - One time Checkout, RE - Recurring Type
        $country = 'US';

        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $version_number = $build_number = '';
        $reg_type_user = 'M';
        $payment_method = "CC";
        $payment_type = 'onetimesingle';
        $payment_start_date = '';
        $device_type = $app_id = $push_device_id = '';
        $intent_method_type = $payment_intent_id = $cc_type = $stripe_customer_id = $registration_date = $default_cc = '';
        $total_no_of_payments = $payment_method_id = '';


        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }
        if (isset($this->_request['version_number'])) {
            $version_number = $this->_request['version_number'];
        }
        if (isset($this->_request['build_number'])) {
            $build_number = $this->_request['build_number'];
        }
        $reg_version_user = "mob_ver-" . $version . ", ver_num-" . $version_number . ", build_num-" . $build_number;
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['event_id'])) {
            $event_id = $this->_request['event_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['event_name'])) {
            $event_name = $this->_request['event_name'];
        }
        if (isset($this->_request['desc'])) {
            $desc = $this->_request['desc'];
        }
        if (isset($this->_request['event_array'])) {
            $event_array = $this->_request['event_array'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['registration_amount'])) {
            $registration_amount = $this->_request['registration_amount'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['fee'])) {
            $fee = $this->_request['fee'];
        }
        if (isset($this->_request['discount'])) {
            $discount = $this->_request['discount'];
        }
        if (isset($this->_request['quantity'])) {
            $quantity = $this->_request['quantity'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['reg_col4'])) {
            $reg_col4 = $this->_request['reg_col4'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['payment_frequency'])) {
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if (isset($this->_request['event_array']) && !empty($this->_request['event_array'])) {
            $event_array = $this->_request['event_array'];
        }
        if (isset($this->_request['payment_array']) && !empty($this->_request['payment_array'])) {
            $payment_array = $this->_request['payment_array'];
        }
        if (isset($this->_request['event_type'])) {
            $event_type = $this->_request['event_type'];
        }
        if (isset($this->_request['payment_type'])) {
            $payment_type = $this->_request['payment_type'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['total_order_amount'])) {
            $total_order_amount = $this->_request['total_order_amount'];
        }
        if (isset($this->_request['total_order_quantity'])) {
            $total_order_quantity = $this->_request['total_order_quantity'];
        }if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['check_number'])) {
            if (!empty($this->_request['check_number'])) {
                $check_number = $this->_request['check_number'];
            }
        }
        if (isset($this->_request['event_optional_discount'])) {
            if (!empty($this->_request['event_optional_discount'])) {
                $optional_discount = $this->_request['event_optional_discount'];
            }
        }
        if (isset($this->_request['event_optional_discount_flag'])) {
            $optional_discount_flag = $this->_request['event_optional_discount_flag'];
        }
        if (isset($this->_request['payment_start_date'])) {
            $payment_start_date = $this->_request['payment_start_date'];
        }
        if (isset($this->_request['total_no_of_payments'])) {
            $total_no_of_payments = $this->_request['total_no_of_payments'];
        }

        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['intent_method_type'])) {
            $intent_method_type = $this->_request['intent_method_type'];
        }
        if (isset($this->_request['cc_type'])) {    // cc_type - N - New, E - Existing
            $cc_type = $this->_request['cc_type'];
        }
        if (isset($this->_request['stripe_customer_id'])) {    // cc_type - N - New, E - Existing
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        if (isset($this->_request['default_cc'])) {
            $default_cc = $this->_request['default_cc'];
        }

        if ($payment_amount == 0) {
            $payment_array = [];
            $p_type = 'F';
            $payment_type = 'free';
            $payment_method = "CA";
            $payment_frequency = 4;
        }
        if ($payment_method === "CC" && empty(trim($payment_method_id))) {
            if ($optional_discount_flag !== "Y") {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        }
        if (!empty($company_id) && !empty($event_id) && !empty($event_name) && !empty($desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && !empty($event_type)) {
            
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if ($event_type == 'S' && $payment_type == 'onetimesingle') {
            $event_array = [];
            $payment_array = [];
            $p_type = 'CO';
            $payment_frequency = 4;
        } elseif ($event_type == 'S' && $payment_type == 'singlerecurring') {
            $event_array = [];
            $p_type = 'RE';
        } elseif ($event_type == 'M' && $payment_type == 'onetimemultiple') {
            $payment_array = [];
            $p_type = 'CO';
            $payment_frequency = 4;
        } elseif ($event_type == 'M' && $payment_type == 'multiplerecurring') {
            $p_type = 'RE';
        }
        if ($payment_method == 'CC') {
            $check_number = 0;
        } elseif ($payment_method == 'CH') {
            $cc_id = $cc_state = '';
        } elseif ($payment_method == 'CA') {
            $cc_id = $cc_state = '';
            $check_number = 0;
        }
        $this->stripeEventpurchase($push_device_id, $device_type, $app_id, $reg_version_user, $company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, $payment_type, $p_type, $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $discount_code, $payment_method_id, $intent_method_type, $cc_type, $stripe_customer_id, $payment_method, $payment_start_date, $total_no_of_payments, $default_cc);
    }
    
     private function stripeMembershipCheckout() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $membership_id = $membership_option_id = $membership_category_title = $membership_title = $membership_desc = $recurring_start_date = '';
        $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code =  $processing_fee_type = $payment_frequency = $membership_start_date = $payment_start_date = $delay_recurring_payment_start_date = $custom_recurring_frequency_period_type = '';
        $reg_col1 = $reg_col2 = $reg_col3 = $reg_col4 = $reg_col5 = $reg_col6 = $reg_col7 = $reg_col8 = $reg_col9 = $reg_col10 = $membership_discount_code = $signup_discount_code = '';
        $student_id = $participant_id = $payment_amount = $processing_fee = $membership_fee = $membership_fee_disc = $signup_fee = $signup_fee_disc = $initial_payment = $custom_recurring_frequency_period_val = 0;
        $upgrade_status = 'F';
        $country = 'US';
        $prorate_first_payment_flg = $delay_recurring_payment_start_flg = 'N';
        $membership_payment_array = ['N', 'W', 'B', 'M', 'A', 'C'];
        $payment_array = [];
        $no_of_classes = $billing_options_deposit_amount = $billing_options_no_of_payments = $delay_recurring_payment_amount = $first_payment = 0;
        $billing_options = $billing_options_expiration_date = '';
        $membership_structure = 'OE';
        $initial_payment_include_membership_fee = 'Y';
        $participant_street = $participant_city = $participant_state = $participant_zip = $participant_country = '';
        $reg_type_user = 'M';
        $version_number = $build_number = '';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $exclude_from_billing_flag = 'N';
        $exclude_days_array = [];
        $default_cc = 'N';
        $program_start_date = $program_end_date = $reg_current_date = '';
        $device_type = $app_id = $push_device_id = '';
        $cc_type = $stripe_customer_id = $payment_method_id = $intent_method_type = '';

        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }

        if (isset($this->_request['version_number'])) {
            $version_number = $this->_request['version_number'];
        }
        if (isset($this->_request['build_number'])) {
            $build_number = $this->_request['build_number'];
        }
        $reg_version_user = "mob_ver-" . $version . ", ver_num-" . $version_number . ", build_num-" . $build_number;
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['membership_id'])) {
            $membership_id = $this->_request['membership_id'];
        }
        if (isset($this->_request['membership_option_id'])) {
            $membership_option_id = $this->_request['membership_option_id'];
        }
        if (isset($this->_request['membership_category_title'])) {
            $membership_category_title = $this->_request['membership_category_title'];
        }
        if (isset($this->_request['membership_title'])) {
            $membership_title = $this->_request['membership_title'];
        }
        if (isset($this->_request['membership_desc'])) {
            $membership_desc = $this->_request['membership_desc'];
        }
        if (isset($this->_request['membership_structure'])) {
            $membership_structure = $this->_request['membership_structure'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['processing_fee_type'])) {
            $processing_fee_type = $this->_request['processing_fee_type'];
        }
        if (isset($this->_request['processing_fee'])) {
            $processing_fee = $this->_request['processing_fee'];
        }
        if (isset($this->_request['membership_fee'])) {
            $membership_fee = $this->_request['membership_fee'];
        }
        if (isset($this->_request['membership_fee_disc'])) {
            $membership_fee_disc = $this->_request['membership_fee_disc'];
        }
        if (isset($this->_request['signup_fee'])) {
            $signup_fee = $this->_request['signup_fee'];
        }
        if (isset($this->_request['signup_fee_disc'])) {
            $signup_fee_disc = $this->_request['signup_fee_disc'];
        }
        if (isset($this->_request['initial_payment'])) {      //registration time payment
            $initial_payment = $this->_request['initial_payment'];
        }
        if (isset($this->_request['first_payment'])) {      //first payment on membership start date
            $first_payment = $this->_request['first_payment'];
        }
        if (isset($this->_request['payment_frequency'])) {
            $payment_frequency = $this->_request['payment_frequency'];
        }
        if (isset($this->_request['payment_start_date'])) {
            $payment_start_date = $this->_request['payment_start_date'];
        }
        if (isset($this->_request['membership_start_date'])) {
            $membership_start_date = $this->_request['membership_start_date'];
        }
        if (isset($this->_request['recurring_start_date'])) {
            $recurring_start_date = $this->_request['recurring_start_date'];
        }
        if (isset($this->_request['reg_col1'])) {
            $reg_col1 = $this->_request['reg_col1'];
        }
        if (isset($this->_request['reg_col2'])) {
            $reg_col2 = $this->_request['reg_col2'];
        }
        if (isset($this->_request['reg_col3'])) {
            $reg_col3 = $this->_request['reg_col3'];
        }
        if (isset($this->_request['reg_col4'])) {
            $reg_col4 = $this->_request['reg_col4'];
        }
        if (isset($this->_request['reg_col5'])) {
            $reg_col5 = $this->_request['reg_col5'];
        }
        if (isset($this->_request['reg_col6'])) {
            $reg_col6 = $this->_request['reg_col6'];
        }
        if (isset($this->_request['reg_col7'])) {
            $reg_col7 = $this->_request['reg_col7'];
        }
        if (isset($this->_request['reg_col8'])) {
            $reg_col8 = $this->_request['reg_col8'];
        }
        if (isset($this->_request['reg_col9'])) {
            $reg_col9 = $this->_request['reg_col9'];
        }
        if (isset($this->_request['reg_col10'])) {
            $reg_col10 = $this->_request['reg_col10'];
        }
        if (isset($this->_request['upgrade_status'])) {
            $upgrade_status = $this->_request['upgrade_status'];
        }
        if (isset($this->_request['prorate_first_payment_flg']) && !empty(trim($this->_request['prorate_first_payment_flg']))) {
            $prorate_first_payment_flg = $this->_request['prorate_first_payment_flg'];
        }
        if (isset($this->_request['delay_recurring_payment_start_flg']) && !empty(trim($this->_request['delay_recurring_payment_start_flg']))) {
            $delay_recurring_payment_start_flg = $this->_request['delay_recurring_payment_start_flg'];
        }
        if (isset($this->_request['delay_recurring_payment_start_date'])) {
            $delay_recurring_payment_start_date = $this->_request['delay_recurring_payment_start_date'];
        }
        if (isset($this->_request['delay_recurring_payment_amount'])) {
            $delay_recurring_payment_amount = $this->_request['delay_recurring_payment_amount'];
        }
        if (isset($this->_request['custom_recurring_frequency_period_type'])) {
            $custom_recurring_frequency_period_type = $this->_request['custom_recurring_frequency_period_type'];
        }
        if (isset($this->_request['custom_recurring_frequency_period_val'])) {
            $custom_recurring_frequency_period_val = $this->_request['custom_recurring_frequency_period_val'];
        }
        if (isset($this->_request['initial_payment_include_membership_fee'])) {
            $initial_payment_include_membership_fee = $this->_request['initial_payment_include_membership_fee'];
        }
        if (isset($this->_request['billing_options'])) {
            $billing_options = $this->_request['billing_options'];
        }
        if (isset($this->_request['no_of_classes'])) {
            $no_of_classes = $this->_request['no_of_classes'];
        }
        if (isset($this->_request['billing_options_expiration_date'])) {
            $billing_options_expiration_date = $this->_request['billing_options_expiration_date'];
        }
        if (isset($this->_request['billing_options_deposit_amount'])) {
            $billing_options_deposit_amount = $this->_request['billing_options_deposit_amount'];
        }
        if (isset($this->_request['billing_options_no_of_payments'])) {
            $billing_options_no_of_payments = $this->_request['billing_options_no_of_payments'];
        }
        if (isset($this->_request['payment_array'])) {
            $payment_array = $this->_request['payment_array'];
        }
        if (isset($this->_request['participant_street'])) {
            $participant_street = $this->_request['participant_street'];
        }
        if (isset($this->_request['participant_city'])) {
            $participant_city = $this->_request['participant_city'];
        }
        if (isset($this->_request['participant_state'])) {
            $participant_state = $this->_request['participant_state'];
        }
        if (isset($this->_request['participant_zip'])) {
            $participant_zip = $this->_request['participant_zip'];
        }
        if (isset($this->_request['participant_country'])) {
            $participant_country = $this->_request['participant_country'];
        }
        if (isset($this->_request['membership_discount_code'])) {
            $membership_discount_code = $this->_request['membership_discount_code'];
        }
        if (isset($this->_request['signup_discount_code'])) {
            $signup_discount_code = $this->_request['signup_discount_code'];
        }
        if (isset($this->_request['exclude_days_array'])) {
            $exclude_days_array = $this->_request['exclude_days_array'];
        }
        if (isset($this->_request['program_start_date'])) {
            $program_start_date = $this->_request['program_start_date'];
        }
        if (isset($this->_request['program_end_date'])) {
            $program_end_date = $this->_request['program_end_date'];
        }
        if (isset($this->_request['exclude_from_billing_flag'])) {
            $exclude_from_billing_flag = $this->_request['exclude_from_billing_flag'];
        }
        if (isset($this->_request['registration_date'])) {
            $reg_current_date = $this->_request['registration_date'];
        }
        if (isset($this->_request['default_cc'])) {
            $default_cc = $this->_request['default_cc'];
        }
        if (isset($this->_request['participant_id'])) {
            $participant_id = $this->_request['participant_id'];
        }
        if (isset($this->_request['cc_type'])) {    // cc_type - N - New, E - Existing
            $cc_type = $this->_request['cc_type'];
        }
        if (isset($this->_request['stripe_customer_id'])) {    // cc_type - N - New, E - Existing
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['intent_method_type'])) {
            $intent_method_type = $this->_request['intent_method_type'];
        }
                

        //Input validations
        if (!empty(trim($payment_frequency)) && !in_array($payment_frequency, $membership_payment_array)) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if (!empty(trim($payment_frequency)) && $payment_frequency == 'C' && (empty(trim($custom_recurring_frequency_period_type)) || empty(trim($custom_recurring_frequency_period_val)))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if (!empty($membership_structure) && $membership_structure == 'OE' && $payment_amount > 0 && !empty(trim($delay_recurring_payment_start_flg)) && $delay_recurring_payment_start_flg == 'Y' && empty(trim($delay_recurring_payment_start_date))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
        if (!empty($membership_structure) && $membership_structure == 'NC' && empty($billing_options)) {
            if (!empty($billing_options) && $billing_options == 'PP' && empty($payment_array)) {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }
        }
        if (!empty($membership_structure) && $membership_structure == 'SE' && ((empty($program_start_date) || $program_start_date == '0000-00-00' || empty($program_end_date) || $program_end_date == '0000-00-00') || (!empty($billing_options) && $billing_options == 'PP' && $payment_amount > 0 && empty($payment_array)))) {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }


        if (!empty($company_id) && !empty($membership_id) && !empty($membership_option_id) && !empty($membership_category_title) && !empty($membership_title) && !empty($membership_desc) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email)) {
            $this->stripeMembershipPurchase($push_device_id, $device_type, $app_id, $company_id, $student_id, $membership_id, $membership_option_id, $upgrade_status, $membership_category_title, $membership_title, $membership_desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $processing_fee_type, $processing_fee, $membership_fee, $membership_fee_disc, $signup_fee, $signup_fee_disc, $payment_amount, $initial_payment, $first_payment, $payment_frequency, $payment_start_date, $recurring_start_date, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $prorate_first_payment_flg, $delay_recurring_payment_start_flg, $delay_recurring_payment_start_date, $delay_recurring_payment_amount, $custom_recurring_frequency_period_type, $custom_recurring_frequency_period_val, $initial_payment_include_membership_fee, $membership_structure, $billing_options, $no_of_classes, $billing_options_expiration_date, $billing_options_deposit_amount, $billing_options_no_of_payments, $payment_array, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $membership_discount_code, $signup_discount_code, $exclude_days_array, $program_start_date, $program_end_date, $exclude_from_billing_flag, $reg_type_user, $reg_version_user, $reg_current_date, $default_cc, $participant_id, $membership_start_date,  $cc_type, $stripe_customer_id, $payment_method_id, $intent_method_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
}

// Initiiate Library
$api = new StudioApi;
$api->processApi();
