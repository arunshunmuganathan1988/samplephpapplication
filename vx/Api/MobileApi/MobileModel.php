<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token');
header('Access-Control-Allow-Credentials: true');

require_once '../../../Globals/cont_log.php';
//require_once '../../../Globals/forceUpgrade.php';
require_once('../../PHPMailer_5.2.1/class.phpmailer.php');
require_once('../../PHPMailer_5.2.1/class.smtp.php');
require_once '../PortalApi/WePay.php';
//require_once '../../../aws/aws-autoloader.php';
require_once __DIR__.'/../../../aws/aws.phar';
require_once '../../Stripe/init.php';

use Aws\Sns\SnsClient;

class MobileModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_redirect_uri = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $server_url;
    private $stripe_pk = '', $stripe_sk = '';

//    private $app_id_array = [];
//    private $force_upgrade;

    public function __construct() {
        require_once '../../../Globals/config.php';
        require_once "../../../Globals/stripe_props.php";
        $this->inputs();
        $this->wp = new wepay();
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        $this->processing_fee_file_name = "../../../Globals/processing_fee.props";
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'dev.mystudio.academy') !== false) {  //Development
            $this->server_url = "http://dev.mystudio.academy";
        } elseif (strpos($host, 'dev2.mystudio.academy') !== false) {  //Development
            $this->server_url = "http://dev2.mystudio.academy";
        } elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'];
        } else {
            $this->server_url = "http://dev.mystudio.academy";
        }
//        $this->app_id_array = ['com.skillz.university', 'com.mcgraws.martial.arts', 'hr.apps.207013388', 'com.almaden.taekwondo'];
//        $this->force_upgrade = $GLOBALS['force_upgrade'];
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

//    private function getProcessingFee($upgrade_status){
//        $temp=[];
//        $x=0;
//        $file_handle = fopen($this->processing_fee_file_name, "r");
//        while (!feof($file_handle)) {
//            $lines = fgets($file_handle);
//            $line_contents = explode(",", $lines);
//            for($i=0;$i<count($line_contents);$i++){                
//                $cnt = explode("=", $line_contents[$i]);
//                $temp[$x][$cnt[0]] = $cnt[1];
//            }
//            $x++;
//        }
//        fclose($file_handle);
//        
//        for($i=0;$i<count($temp);$i++){
//            if($temp[$i]['level']==$upgrade_status){
//                return $temp[$i];
//            }
//        }
//    }

    private function getStripeKeys() {
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }

    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }

    /* login functionality using email_id */

    protected function checklogin($email, $company_code, $student_name, $push_device_id, $device_type, $app_id) {

        $sql = sprintf("SELECT student_id,company_id,student_name,last_login_dt,deleted_flag from student where student_email='%s' limit 0,1", mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $sql);
        $student_id = "";

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    $cmpny_id = $row['company_id'];
                    $stud_name = $row['student_name'];
                    $student_id = $row['student_id'];
                    $deleted_flag = $row['deleted_flag'];
                }
//                if($this->force_upgrade == 1){
//                    if((!in_array($app_id, $this->app_id_array) && $device_type=='I') || $device_type=='A'){
//                        $error = array('status' => "Failed", "msg" => "You are using an older version of app, Please update your app.");
//                        $this->response($this->json($error), 401);
//                    }
//                }
                if (!empty($push_device_id)) {
                    $update_push = sprintf("UPDATE `student` SET `push_device_id`='%s', `device_type`='%s', `last_login_dt`= NOW() WHERE `student_id`='%s'", $push_device_id, $device_type, $student_id);
                    $update_result = mysqli_query($this->db, $update_push);
                }
                //Get company ID by Company code sent by client
                $company_id = $this->getcompanyidbycode($company_code);
                if ((isset($cmpny_id)) && (!empty($cmpny_id)) && ($cmpny_id !== $company_id)) {
                    $update_companyid = sprintf("UPDATE `student` SET `company_id`='%s', `student_name`='%s', `last_login_dt`= NOW() WHERE `student_id`='%s'", $company_id, mysqli_real_escape_string($this->db, $student_name), $student_id);
                    $update_result = mysqli_query($this->db, $update_companyid);
                }

                if (!empty($app_id)) {
                    $update_appid = sprintf("UPDATE `student` SET `app_id`='%s', `last_login_dt`= NOW() WHERE `student_id`='%s'", $app_id, $student_id);
                    $update_appid_result = mysqli_query($this->db, $update_appid);
                }
                if ($deleted_flag == 'Y') {
                    $update_delflag = sprintf("UPDATE `student` SET `deleted_flag`='N' WHERE `student_id`='%s'", $student_id);
                    $update_delflag_result = mysqli_query($this->db, $update_delflag);
                }
                //Print Company details after login success                
                $this->getcompanydetails($company_id, $student_id, 0);
            } else {
                //If Student record doesn't exist, register new student
                $response = $this->registerstudent($email, $company_code, $student_name, $push_device_id, $device_type, $app_id);
                //If registration is success above function returns company ID

                if (strlen($response['student_id']) != 0) {
                    //Print Company details after registration success
                    $this->getcompanydetails($response['company_id'], $response['student_id'], 0);
                }
            }
        }
    }

    //Get Company ID by Code
    protected function getcompanyidbycode($company_code) {
        // $company_id='';
        $sql1 = sprintf("SELECT company_id from `company` WHERE `company_code`='%s' limit 0,1", mysqli_real_escape_string($this->db, $company_code));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows1 = mysqli_num_rows($result1);
            if ($num_of_rows1 > 0) {
                $row = mysqli_fetch_array($result1);
                $company_id = $row['company_id'];
                return $company_id;
            } else {
                $error = array('status' => "Failed", "msg" => "Company Code doesn't match our records.");
                $this->response($this->json($error), 401);
            }
        }
    }

    //Register New student
    protected function registerstudent($email, $company_code, $student_name, $push_device_id, $device_type, $app_id) {

        $company_id = $this->getcompanyidbycode($company_code);
        if (!empty($app_id)) {
            $sql1 = sprintf("INSERT INTO `student` (`company_id`, `student_name`, `student_email`, `first_login_dt`, `last_login_dt`, `push_device_id`, `device_type`, `app_id`) VALUES ( '%s', '%s', '%s', NOW(), NOW(), '%s', '%s', '%s');", $company_id, mysqli_real_escape_string($this->db, $student_name), mysqli_real_escape_string($this->db, $email), $push_device_id, $device_type, $app_id);
        } else {
            $sql1 = sprintf("INSERT INTO `student` (`company_id`, `student_name`, `student_email`, `first_login_dt`, `last_login_dt`, `push_device_id`, `device_type`) VALUES ( '%s', '%s', '%s', NOW(), NOW(), '%s', '%s');", $company_id, mysqli_real_escape_string($this->db, $student_name), mysqli_real_escape_string($this->db, $email), $push_device_id, $device_type);
        }
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $response['student_id'] = mysqli_insert_id($this->db);
            $response['company_id'] = $company_id;
        }
        return $response;
    }

    //Get Company details by Company ID
    protected function getcompanydetails($company_id, $student_id, $call_back) {  //$call_back --> 0 - echo & exit, 1 - return
        $this->getStripeKeys();
        $stripe_publish_key = $this->stripe_pk;
        $this->getStudioSubscriptionStatus($company_id);
        $current_date = gmdate("Y-m-d");
        $sql2 = sprintf("select t1.*,IFNULL(t2.unread_msg_count,0) unread_msg_count from 
                (SELECT `company_id`,`company_name`, `logo_URL`, `logo_content`, `company_code`, `twitter_id`, `twitter_url`, `facebook_id`, `facebook_url`, `instagram_id`, `instagram_url`, `event_flag`, `eventbrite_id`, `eventbrite_url`, `vimeo_id`, `vimeo_url`, `youtube_id`, `youtube_url`,`web_page`,`phone_number`,`email_id`,`street_address`,`city`,`state`,`postal_code`,`country`,`wp_currency_code`, `wp_currency_symbol`,`trial_name`,`membership_name`, `curriculum_name`,`event_name`,`message_name`,`referral_message`,`google_url`,`yelp_url`,`class_schedule`,`upgrade_status`,`studio_expiry_level`, `retail_enabled`,`event_enabled`
                ,`stripe_status`
                FROM `company` WHERE `company_id`='%s') t1 LEFT JOIN 
                (SELECT count(*) unread_msg_count,company_id from message m LEFT JOIN message_mapping mp USING (company_id, message_id) where message_from='P' and IF(message_to='I', m.read_status='U', mp.read_status='U') and company_id='%s' and IFNULL(m.student_id,mp.student_id)='%s' ) 
                t2 USING(company_id)", $company_id, $company_id, $student_id);
        $result2 = mysqli_query($this->db, $sql2);

        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 0) {
                $this->response($this->json($error), 500);
            } else {
                return $error;
            }
        } else {
            $num_of_rows2 = mysqli_num_rows($result2);
            if ($num_of_rows2 > 0) {
                while ($row = mysqli_fetch_assoc($result2)) {
                    $curriculum_name = $row['curriculum_name'];
                    $event_name = $row['event_name'];
                    $message_name = $row['message_name'];
                    $trial_name = $row['trial_name'];
                    $membership_name = $row['membership_name'];
                    $country = $row['country'];
                    if (!empty($curriculum_name)) {
                        
                    } else {
                        $row['curriculum_name'] = "Curriculum";
                    }

                    if (!empty($event_name)) {
                        
                    } else {
                        $row['event_name'] = "Events";
                    }

                    if (!empty($message_name)) {
                        
                    } else {
                        $row['message_name'] = "Messages";
                    }
                    if (!empty($trial_name)) {
                        
                    } else {
                        $row['trial_name'] = "Trial Program";
                    }
                    if (!empty($membership_name)) {
                        
                    } else {
                        $row['membership_name'] = "Memberships";
                    }

                    if (empty($row['logo_URL'])) {
                        $row['logo_URL'] = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/$this->upload_folder_name/Default/yourlogo.png";
                    }

                    $row['student_id'] = $student_id;
                    $row['wp_client_id'] = $this->wp_client_id;
                    $row['wp_level'] = $this->wp_level;
                    $process_fee = $this->getProcessingFee($company_id, $row['upgrade_status']);
                    $row['processing_fee_percentage'] = $process_fee['PP'];
                    $row['processing_fee_transaction'] = $process_fee['PT'];
                    if ($row['upgrade_status'] == 'F') {
                        $row['twitter_id'] = '';
                        $row['twitter_url'] = '';
                        $row['facebook_id'] = '';
                        $row['facebook_url'] = '';
                        $row['instagram_id'] = '';
                        $row['instagram_url'] = '';
                        $row['eventbrite_id'] = '';
                        $row['eventbrite_url'] = '';
                        $row['vimeo_id'] = '';
                        $row['vimeo_url'] = '';
                        $row['youtube_id'] = '';
                        $row['youtube_url'] = '';
                        $row['google_url'] = '';
                        $row['yelp_url'] = '';
                    }
                    $output = $row;
                }
                $output['memberapp_details'] = $this->getStudentAllDetails($company_id, $student_id, 1);
                $output['stripe_publish_key'] = $stripe_publish_key;
                $country_payment_specific = $this->checkCountryPaymentSupport($company_id, $country, 1); // callback - 1/0
                $output['stripe_country_support'] = $stripe_country_support = ($country_payment_specific['status'] == 'Success' && $country_payment_specific['support_status'] == 'Y' && $country_payment_specific['payment_support'] == 'S') ? "Y" : "N";  // CHECK COUNTRY SUPPORT STATUS FOR STRIPE
                $check = $this->getAllMembershipDetails($company_id, 1);
                if ($check['status'] == 'Success') {
                    $output['membership_status'] = 'Y';
                } else {
                    $output['membership_status'] = 'N';
                }
                $check = $this->getTrialDetail($company_id, 1);
                if ($check['status'] == 'Success') {
                    $output['trial_status'] = 'Y';
                } else {
                    $output['trial_status'] = 'N';
                }
//                $sql1 = sprintf("SELECT company_id from company where company_id='%s' and ((date(`expiry_dt`) >='%s')||(`expiry_dt` is null))", $company_id, $current_date);
//                   $result1 = mysqli_query($this->db, $sql1);
//                    if (!$result1) {
//                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
//                        mlog_info($this->json($error_log));
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                        $this->response($this->json($error), 500);
//                    }  else {
//                        $num_of_rows1 = mysqli_num_rows($result1);
//                        if ($num_of_rows1 > 0) {
                //print_r($output);
                // If success everythig is good send header as "OK" and user details
                $out = array('status' => "Success", 'msg' => $output);
                if ($call_back == 0) {
                    $this->response($this->json($out), 200);
                } else {
                    return $out;
                }
//                        }else{
//                            $error = array('status' => "Failed", "msg" => "Company expired, please contact your administrator.");
//                            $this->response($this->json($error), 401);
//                        }
//                    }
            } else {
                $error = array('status' => "Failed", "msg" => "Company details doesn't exist.");
//                $this->response($this->json($error), 401); // If no records "No Content" status
                if ($call_back == 0) {
                    $this->response($this->json($error), 401);
                } else {
                    return $error;
                }
            }
        }
    }

    //Get Event details by Company ID
    protected function geteventsdetails($company_id) {
        $current_date = gmdate("Y-m-d");
        $sql = sprintf("SELECT `event_id`, `company_id`, `parent_id`, `event_type`, `event_status`, `event_begin_dt`, `event_end_dt`, `event_title`, `event_category_subtitle`,
                `event_desc`, `event_more_detail_url`, `event_cost`, `event_compare_price`, `event_capacity`, `capacity_text`, `event_sort_order`, `event_banner_img_url` 
                FROM `event` WHERE `company_id`='%s' 
                ORDER BY `event_begin_dt` DESC", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $parent_integrated = $parent_mapping = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $row['child_events'] = [];
                    if ($row['event_type'] == 'M') {
                        $parent_mapping[$row['event_id']] = count($parent_integrated);
                        $parent_integrated[] = $row;
                    } elseif (empty(trim($row['parent_id']))) {
                        $parent_integrated[] = $row;
                    }
                    $output[] = $row;
                }
                for ($i = 0; $i < count($output); $i++) {//integrate the child event with parent event for multiple event
                    if (!empty(trim($output[$i]['parent_id']))) {
                        $parent_integrated[$parent_mapping[$output[$i]['parent_id']]]['child_events'][] = $output[$i];
                    }
                }
                for ($i = 0; $i < count($parent_integrated); $i++) {//integrate the child event with parent event for multiple event
                    if (count($parent_integrated[$i]['child_events']) > 0) {
                        usort($parent_integrated[$i]['child_events'], function($a, $b) {
                            return $a['event_begin_dt'] > $b['event_begin_dt'];
                        });
                    }
                }
                $out = array('status' => "Success", 'msg' => $parent_integrated);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Event details doesn't exist.");
                $this->response($this->json($error), 401); // If no records "No Content" status
            }
        }
    }

    //Get Curriculum details by Company ID and Student ID
    protected function getcurriculumdetails($company_id, $studentid) {
        $this->getStudioSubscriptionStatus($company_id);
        $sql2 = sprintf("SELECT c.`curriculum_id`, c.`curr_name`, cd.`topic_srt_ord`,cd.`topic_name`,cd.`topic_img_url`,cd.`topic_img_content` FROM `curriculum_detail` cd, `curriculum` c WHERE c.`curriculum_id` in (SELECT `curriculum_id` FROM `curriculum` WHERE `company_id`='%s') and c.`curriculum_id`=cd.`curriculum_id` ORDER BY cd.topic_srt_ord ASC", $company_id);
        $result2 = mysqli_query($this->db, $sql2);

        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows2 = mysqli_num_rows($result2);
            //echo $num_of_rows2;
            if ($num_of_rows2 > 0) {
                while ($row = mysqli_fetch_assoc($result2)) {
                    $output[] = $row;
                    $curriculum_id = $row['curriculum_id'];
                    $sql3 = sprintf("SELECT * FROM (SELECT cd.`content_title`, cd.`content_description`, cd.`ContentVideoUrl`,cd.`video_type`,cd.`video_id`,s.content_srt_ord FROM `content_definition` AS cd LEFT JOIN (SELECT `contentId`, `content_srt_ord` FROM `curriculum_content` WHERE `curriculum_id`='%s' ORDER BY `content_srt_ord` ASC)  s ON s.contentId = cd.content_id where s.contentId = cd.content_id ORDER BY s.`content_srt_ord` ASC) t1 LEFT JOIN video_content t2 ON t2.video_id =  t1.video_id ORDER BY `content_srt_ord` ASC", $curriculum_id);
                    $result3 = mysqli_query($this->db, $sql3);
                    if (!$result3) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $current_pos = count($output) - 1;
                        $num_rows3 = mysqli_num_rows($result3);
                        if ($num_rows3 > 0) {
                            while ($row2 = mysqli_fetch_assoc($result3)) {
                                $db_video_url = $row2['video_url'];
                                $db_thum_nail_url = $row2['thum_nail_url'];

                                if (!empty($db_video_url)) {
                                    $row2['video_url'] = $this->changeToSignedURL($db_video_url);
                                    ;
                                } else {
                                    $row2['video_url'] = "";
                                }

                                if (!empty($db_thum_nail_url)) {
                                    $row2['thum_nail_url'] = $this->changeToSignedURL($db_thum_nail_url);
                                } else {
                                    $row2['thum_nail_url'] = "";
                                }
                                $output[$current_pos]['curriculum_content'][] = $row2;
                            }
                        } else {
                            $output[$current_pos]['curriculum_content'][] = '';
                        }
                    }
                }
                $out = array('status' => "Success", 'msg' => $output);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Curriculum details doesn't exist.");
                $this->response($this->json($error), 401); // If no records "No Content" status
            }
        }
    }

    //Verify student in every method along with companyid and studentid.
    protected function verifystudent($company_id, $student_id) {
        $current_date = gmdate("Y-m-d");
        $sql = sprintf("SELECT company_id,device_type,app_id from student where student_id='%s' limit 0,1", $student_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    $cmpny_id = $row['company_id'];
                    $app_id = $row['app_id'];
                    $device_type = $row['device_type'];
                }
//                if($this->force_upgrade == 1){
//                    if((!in_array($app_id, $this->app_id_array) && $device_type=='I') || $device_type=='A'){
//                        $error = array('status' => "Failed", "msg" => "You are using an older version of app, Please update your app.");
//                        $this->response($this->json($error), 401);
//                    }
//                }

                if ($company_id != $cmpny_id) {
                    $error = array('status' => "Failed", "msg" => "Student doesn't belong to the company.");
                    $this->response($this->json($error), 401);
                } else {
//                   $sql1 = sprintf("SELECT company_id from company where company_id='%s' and ((date(`expiry_dt`) >='%s')||(`expiry_dt` is null))", $cmpny_id, $current_date);
//                   $result1 = mysqli_query($this->db, $sql1);
//                    if (!$result1) {
//                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
//                        mlog_info($this->json($error_log));
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                        $this->response($this->json($error), 500);
//                    }  else {
//                        $num_of_rows1 = mysqli_num_rows($result1);
//                        if ($num_of_rows1 > 0) {
                    $out = array('status' => "Success", 'msg' => "Student details verified successfully.");
                    return $out;
//                        }else{
//                            $error = array('status' => "Failed", "msg" => "Company expired, please contact your administrator.");
//                            $this->response($this->json($error), 401);
//                        }
//                    }
                    // If success everythig is good send header as "OK" and user details
                    //$this->response($this->json($out), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Student doesn't exist.");
                $this->response($this->json($error), 401);
            }
        }
    }

    // HANDLE MESSAGES FOR OLD VERSIONS OF MOBILE APP
    protected function getmessagesfromdb($company_id, $student_id) {
        $this->getStudioSubscriptionStatus($company_id);
        $sql = sprintf("SELECT message_text,message_link,push_delivered_date from message where company_id='%s' and student_id = '%s' and push_delivered_date IS NOT NULL ORDER BY `message_id` desc", $company_id, $student_id);
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            //echo $num_of_rows2;
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                $out = array('status' => "Success", 'msg' => $output);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "No Message for this company.");
                $this->response($this->json($error), 401); // If no records "No Content" status
            }
        }
    }

    // HANDLE CHAT MESSAGES FOR NEW VERSIONS OF MOBILE APP
    protected function getchatmessagesfromdb($company_id, $student_id, $last_msg_id, $last_msg_time, $user_tz, $msg_direction) {
        $last_view_message_id_string = '';
        if (!empty((int) $last_msg_id)) {
            if ($msg_direction == 'N') {
                $last_view_message_id_string = "AND message_id > " . mysqli_real_escape_string($this->db, $last_msg_id) . " AND message_delivery_date > '" . mysqli_real_escape_string($this->db, $last_msg_time) . "' ORDER BY message_delivery_date ASC, message_id ASC ";
            } else {
                $last_view_message_id_string = "AND message_id < " . mysqli_real_escape_string($this->db, $last_msg_id) . " AND message_delivery_date < '" . mysqli_real_escape_string($this->db, $last_msg_time) . "' ORDER BY message_delivery_date DESC, message_id DESC ";
            }
        } else {
            $last_view_message_id_string = "ORDER BY message_delivery_date DESC, message_id DESC ";
        }
        $read_status = sprintf("UPDATE `message` m SET m.`read_status` = 'R', m.`read_time`=CURRENT_TIMESTAMP WHERE m.`student_id`='%s' AND m.company_id='%s' AND message_from='P' AND message_to='I'", mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
        $result0 = mysqli_query($this->db, $read_status);
        if (!$result0) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$read_status");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $read_status2 = sprintf("UPDATE message_mapping mp SET mp.`read_status` = 'R', mp.`read_time`=CURRENT_TIMESTAMP WHERE mp.student_id ='%s' AND mp.company_id='%s'", mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
            $result2 = mysqli_query($this->db, $read_status2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$read_status2");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }

        $this->getStudioSubscriptionStatus($company_id);
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $new_timezone = mysqli_real_escape_string($this->db, $user_tz);
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $message_delivery_date = "CONVERT_TZ(`message_delivery_date`, $tzadd_add,'$new_timezone')";
        $output = $groupdatearray = [];

        $getchat = sprintf("SELECT message_delivery_date,company_id,message_id,message_text,message_from ,$message_delivery_date company_timezone_date
                FROM message_views 
                WHERE `company_id`='%s' and  student_id = '%s' 
                AND message_delivery_date <= CURRENT_TIMESTAMP
                $last_view_message_id_string limit 0, 20", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $getchat);
//        mlog_info($getchat);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getchat");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                for ($i = 0; $i < count($output); $i++) {
                    $sample_array = [];
                    $first_date = explode(" ", $output[$i]['company_timezone_date']);
                    $sample_array['msg_by_date'][] = $output[$i];
                    array_splice($output, $i, 1);
                    for ($j = $i; $j < count($output); $j++) {
                        $compare_date = explode(" ", $output[$j]['company_timezone_date']);
                        if ($compare_date[0] === $first_date[0]) {
                            $sample_array['msg_by_date'][] = $output[$j];
                            array_splice($output, $j, 1);
                            $j--;
                        }
                    }
                    $date = date_create($sample_array['msg_by_date'][0]['company_timezone_date']);
                    $sample_array['day'] = date_format($date, "l, F jS");
                    $groupdatearray[] = $sample_array;
                    $i--;
                }
                $out = array('status' => "Success", 'msg' => $groupdatearray);
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "No Message for this company.");
                $this->response($this->json($error), 204); // If no records "No Content" status
            }
        }
    }

    protected function postchatmessagesfrommobile($company_id, $student_id, $message_text, $last_msg_id, $last_msg_time, $user_tz) {
        $query = sprintf("INSERT INTO `message` (`message_text`, `message_from`, `message_to`, `company_id`, `student_id`, `push_delivered_date`, `push_from`) VALUES ('%s', 'M', 'I', '%s', '%s', CURRENT_TIMESTAMP, 'C')", mysqli_real_escape_string($this->db, $message_text), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
//            $this->getmessagesfromdb($company_id, $student_id, $last_msg_id, $user_tz);
            $message_id = mysqli_insert_id($this->db); //getsingle message
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone = $user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
            $host = $_SERVER['HTTP_HOST'];
            if (strpos($host, 'mystudio.academy') !== false) {
                $new_timezone = mysqli_real_escape_string($this->db, $user_tz);
                $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
            } else {
                $tzadd_add = "'" . $curr_time_zone . "'";
            }
            if (!empty((int) $last_msg_id)) {
                $last_view_message_id_string = "AND message_id > " . mysqli_real_escape_string($this->db, $last_msg_id) . " AND message_delivery_date > '" . mysqli_real_escape_string($this->db, $last_msg_time) . "' ORDER BY message_delivery_date ASC, message_id ASC ";
            } else {
                $last_view_message_id_string = '';
            }

            $message_delivery_date = "CONVERT_TZ(`message_delivery_date`,$tzadd_add,'$new_timezone')";
            $output = $groupdatearray = [];
            $getmessagequery = sprintf("SELECT message_delivery_date,company_id,message_id,message_text,message_from ,$message_delivery_date company_timezone_date
                FROM message_views 
                WHERE `company_id`='%s' and student_id = '%s' 
                AND message_delivery_date <= CURRENT_TIMESTAMP $last_view_message_id_string", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
            $result2 = mysqli_query($this->db, $getmessagequery);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$getmessagequery");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($result2);
                if ($num_rows > 0) {
                    while ($row = mysqli_fetch_assoc($result2)) {
                        $output[] = $row;
                    }
                    for ($i = 0; $i < count($output); $i++) {
                        $sample_array = [];
                        $first_date = explode(" ", $output[$i]['company_timezone_date']);
                        $sample_array['msg_by_date'][] = $output[$i];
                        array_splice($output, $i, 1);
                        for ($j = $i; $j < count($output); $j++) {
                            $compare_date = explode(" ", $output[$j]['company_timezone_date']);
                            if ($compare_date[0] === $first_date[0]) {
                                $sample_array['msg_by_date'][] = $output[$j];
                                array_splice($output, $j, 1);
                                $j--;
                            }
                        }
                        $date = date_create($sample_array['msg_by_date'][0]['company_timezone_date']);
                        $sample_array['day'] = date_format($date, "l, F jS");
                        $groupdatearray[] = $sample_array;
                        $i--;
                    }
                }
                $out = array('status' => "Success", 'msg' => $groupdatearray);
                $this->response($this->json($out), 200);
            }
        }
    }

    public function aws_s3_link($access_key, $secret_key, $bucket, $canonical_uri, $expires, $region, $extra_headers = array()) {

        $encoded_uri = str_replace('%2F', '/', rawurlencode($canonical_uri));
        $signed_headers = array();
        foreach ($extra_headers as $key => $value) {
            $signed_headers[strtolower($key)] = $value;
        }
        if (!array_key_exists('host', $signed_headers)) {
            $signed_headers['host'] = ($region == 'us-east-1') ? "$bucket.s3.amazonaws.com" : "$bucket.s3-$region.amazonaws.com";
        }
        ksort($signed_headers);
        $header_string = '';
        foreach ($signed_headers as $key => $value) {
            $header_string .= $key . ':' . trim($value) . "\n";
        }
        $signed_headers_string = implode(';', array_keys($signed_headers));
        $timestamp = time();
        $date_text = gmdate('Ymd', $timestamp);
        $time_text = $date_text . 'T000000Z';
        $algorithm = 'AWS4-HMAC-SHA256';
        $scope = "$date_text/$region/s3/aws4_request";
        $x_amz_params = array(
            'X-Amz-Algorithm' => $algorithm,
            'X-Amz-Credential' => $access_key . '/' . $scope,
            'X-Amz-Date' => $time_text,
            'X-Amz-SignedHeaders' => $signed_headers_string
        );
        if ($expires > 0)
            $x_amz_params['X-Amz-Expires'] = $expires;
        ksort($x_amz_params);
        $query_string_items = array();
        foreach ($x_amz_params as $key => $value) {
            $query_string_items[] = rawurlencode($key) . '=' . rawurlencode($value);
        }
        $query_string = implode('&', $query_string_items);
        $canonical_request = "GET\n$encoded_uri\n$query_string\n$header_string\n$signed_headers_string\nUNSIGNED-PAYLOAD";
        $string_to_sign = "$algorithm\n$time_text\n$scope\n" . hash('sha256', $canonical_request, false);
        $signing_key = hash_hmac('sha256', 'aws4_request', hash_hmac('sha256', 's3', hash_hmac('sha256', $region, hash_hmac('sha256', $date_text, 'AWS4' . $secret_key, true), true), true), true);
        $signature = hash_hmac('sha256', $string_to_sign, $signing_key);
        $url = 'http://' . $signed_headers['host'] . $encoded_uri . '?' . $query_string . '&X-Amz-Signature=' . $signature;
        return $url;
    }

    protected function changeToSignedURL($url) {
        $stringArray = explode('/', $url);
        //$bucket = "mystudio-test-private";
        $bucket = "mystudio-test.technogemsinc.com";
        $stringArray = explode('/', $url);
        $canonical_uri = "/" . $stringArray[4] . "/" . $stringArray[5];
        $extra_headers = array();
        $expires = "86400";
        $signed_url = $this->aws_s3_link('AKIAI3W3VQSR2VDGYZHA', 'hURk+Od5sZ2+SYKL/IDJ+PDolKSaVFY2JIrKC0GI', $bucket, $canonical_uri, $expires, 'us-east-1', $extra_headers);
        return $signed_url;
    }

    //Sent & Add Referral Details
    protected function storeReferralDetails($student_id, $company_id, $referred_name, $email_id, $phone_number) {
        $refer_check = 0;
        $owner_email_id = $member_name = '';
        $query = sprintf("SELECT s.`student_name`, c.`email_id`, c.`company_name` , c.`upgrade_status`, c.`web_page` , c.`phone_number` , c.`referral_email_list` FROM `student` s,`company` c WHERE s.`company_id`=c.`company_id` AND s.`student_id`='%s' AND s.`company_id`='%s'", $student_id, $company_id);
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $cmp_name = $reply_to = "";
                while ($row = mysqli_fetch_assoc($result)) {
                    $owner_email_id = $row['email_id'];
                    $member_name = $row['student_name'];
                    $company_name = $row['company_name'];
                    $web_page = $row['web_page'];
                    if (!empty(trim($row['web_page']))) {
                        $web_page = "Visit " . $row['web_page'] . " or call";
                    } else {
                        $web_page = "Call";
                    }
                    $cmpny_phone_number = $row['phone_number'];
                    $referral_email_list = $row['referral_email_list'];
                    $cmp_name = $company_name;
                    $reply_to = $owner_email_id;
                }
                $subject = "A Referral Came From $member_name";
                $message = "\n\n Name   :   $referred_name \n Email   :   $email_id \n Phone   :   $phone_number\n\n";
                $sendEmail_status = $this->sendEmail($owner_email_id, $referral_email_list, $subject, $message, $cmp_name, '', false);
                if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
                    $refer_check = 1;
                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $owner_email_id . "   " . $sendEmail_status['mail_status']);
                    mlog_info($this->json($error_log));
                } else {
                    $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $owner_email_id);
                    mlog_info($this->json($error_log));
                }

                if (filter_var($email_id, FILTER_VALIDATE_EMAIL)) {
                    $subject = "$member_name Invited you to join $company_name";
                    $message = "\n \n  $web_page $cmpny_phone_number for our new member special and to learn more about our program. \n \n \n Thank you.";
                    $sendEmail_status = $this->sendEmail($email_id, '', $subject, $message, $cmp_name, $reply_to, false);
                    if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
                        $refer_check = 1;
                        $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $owner_email_id . "   " . $sendEmail_status['mail_status']);
                        mlog_info($this->json($error_log));
                    } else {
                        $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $owner_email_id);
                        mlog_info($this->json($error_log));
                    }
                }

                $refer_query = sprintf("INSERT INTO `referral`(`student_id`, `company_id`, `referred_name`, `referred_email`, `referred_phone_number`) 
                            VALUES('%s','%s','%s','%s','%s')", $student_id, $company_id, $referred_name, $email_id, $phone_number);
                $refer_result = mysqli_query($this->db, $refer_query);
                if (!$refer_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$refer_query");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } else {
                    if ($refer_check == 0) {
                        $out = array('status' => "Success", 'msg' => "Referral Added Successfully.");
                    } else {
                        $out = array('status' => "Success", 'msg' => "Referral Sent Successfully.");
                    }
                    // If success everythig is good send header as "OK" and success message
                    $this->response($this->json($out), 200);
                }
            } else {
                $error_log = array('status' => "Failed", "msg" => "Incorrect Company details with Student.", "query" => "$query");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 400);
            }
        }
    }

    protected function sendEmail($to, $cc_email_list, $subject, $message, $cmp_name, $reply_to, $isHtml) {

        if (empty(trim($cmp_name))) {
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try {
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if (filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)) {
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML($isHtml);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    $mail->AddCC($cc_addresses[$init]);
                }
            }
            if ($isHtml) {
                $mail->CharSet = 'UTF-8';
                $mail->Encoding = 'base64';
            }
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        } catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        mlog_info($this->json($error_log));
        return $response;
    }

    protected function sendEmailForEvent($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list, $student_cc_email_list) {

        if (empty(trim($cmp_name))) {
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try {
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if (filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)) {
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
//            if(filter_var($from, FILTER_VALIDATE_EMAIL)){
//                $mail->setFrom($from, $cmp_name);
//                $mail->AddCC($from);
//            }else{
//                $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if (filter_var(trim($from), FILTER_VALIDATE_EMAIL)) {
                $mail->AddCC($from);
            }
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    if (filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)) {
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if (!empty(trim($student_cc_email_list))) {
                $student_cc_addresses = explode(',', $student_cc_email_list);
                for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                    if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                        $mail->AddCC($student_cc_addresses[$init_1]);
                    }
                }
            }

            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
            $mail->Body = $message;
            if ($waiver_present == 1) {
                //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        } catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if (file_exists($waiver_file)) {
            unlink($waiver_file);
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        mlog_info($this->json($error_log));
        return $response;
    }

    //Get Event details by Company ID
    protected function getEventsDetailByCompany($company_id) {
        $this->getStudioSubscriptionStatus($company_id);
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $sql = sprintf("SELECT  `event_id` ,  `company_id` ,  `parent_id` ,  `event_type` ,  `event_status` ,  `event_begin_dt` ,  `event_end_dt` ,  `event_title` ,  `event_category_subtitle` ,  
            `event_desc` ,  `event_more_detail_url`, `event_video_detail_url` , `event_cost` ,  `event_compare_price` ,  `registrations_count` ,  `event_capacity` ,  `capacity_text` ,  `processing_fees`, `event_onetime_payment_flag`, 
            `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `net_sales` ,  `event_sort_order` ,  
            `event_banner_img_url`, `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, 
            `event_registration_column_2`, `event_reg_col_2_mandatory_flag`,`event_registration_column_3`, `event_reg_col_3_mandatory_flag`,
            `event_registration_column_4`, `event_reg_col_4_mandatory_flag`,`event_registration_column_5`, `event_reg_col_5_mandatory_flag`,
            `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, `event_reg_col_7_mandatory_flag`,
            `event_registration_column_8`, `event_reg_col_8_mandatory_flag`,`event_registration_column_9`, `event_reg_col_9_mandatory_flag`,
            `event_registration_column_10`, `event_reg_col_10_mandatory_flag`, `event_show_status`,
             IF( `event_status` = 'S', 'draft', IF( `event_type`='M', 'past', IF( DATE(`event_begin_dt`) <= ($currdate_add), IF(DATE(`event_end_dt`) >= ($currdate_add) || DATE(`event_end_dt`)='0000-00-00', 'live', 'past' ) , 'live' ))) list_type
             FROM  `event` WHERE  `company_id` = '%s'
            AND  `event_status` !=  'D' ORDER BY `event_sort_order` desc", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $output = $parent_mapping = $parent_integrated = $response['live'] = $response['draft'] = $response['past'] = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $disc = $this->getDiscountDetails($company_id, $row['event_id']);
                    $row['discount'] = $disc['discount'];
                    $row['child_events'] = [];
                    $reg_field_name_array = [];
                    $temp_reg_field = "event_registration_column_";
                    for ($z = 1; $z <= 10; $z++) { //collect registration fields as array
                        $y = $z - 1;
                        $reg_field = $temp_reg_field . "$z";      //for filed names
                        $reg_field_mandatory = "event_reg_col_" . $z . "_mandatory_flag";
                        if ($row[$reg_field] != '') {
                            $reg_field_name_array[] = array("reg_col_name" => $row[$reg_field], "reg_col_mandatory" => $row[$reg_field_mandatory], "reg_col_index" => $y);
                        }
                    }
                    $row['reg_columns'] = $reg_field_name_array;
                    if ($row['event_type'] == 'M') {
                        $parent_mapping[$row['event_id']] = count($parent_integrated);
                        $parent_integrated[] = $row;
                    }
                    if ($row['event_type'] == 'S' && empty($row['parent_id'])) {
                        $parent_integrated[] = $row;
                    }
                    $output[] = $row;
                }
                for ($i = 0; $i < count($output); $i++) {//integrate the child event with parent event for multiple event
                    if (!empty(trim($output[$i]['parent_id']))) {
                        $parent_integrated[$parent_mapping[$output[$i]['parent_id']]]['child_events'][] = $output[$i];
//                        if($output[$i]['list_type']=='live'){//default parent status - past, when atleast a child event is in live then parent status changed to live
//                            $parent_integrated[$parent_mapping[$output[$i]['parent_id']]]['list_type'] = 'live';
//                        }
                    }
                }
                $parent_count = count($parent_integrated);
                for ($i = 0; $i < $parent_count; $i++) {//split-up by live, past & draft
                    if ($parent_integrated[$i]['event_show_status'] == 'Y') {
                        $child_count = count($parent_integrated[$i]['child_events']);
                        if ($child_count > 0) {
                            usort($parent_integrated[$i]['child_events'], function($a, $b) {
                                return $a['event_begin_dt'] > $b['event_begin_dt'];
                            });

                            $check_type_conflict = $cevent_capacity = 0;
                            $inital_child_type = $parent_integrated[$i]['child_events'][0]['list_type'];
                            $parent_integrated[$i]['list_type'] = $inital_child_type;

                            for ($j = 0; $j < $child_count; $j++) {
                                if ($parent_integrated[$i]['child_events'][$j]['event_show_status'] == "Y") {
                                    $cevent_capacity += $parent_integrated[$i]['child_events'][$j]['event_capacity'];
                                    $current_child_type = $parent_integrated[$i]['child_events'][$j]['list_type'];
                                    if ($j > 0 && $current_child_type != $inital_child_type) {
                                        if ($check_type_conflict == 0) {
                                            $check_type_conflict = 1;
                                            $parent_integrated[] = $parent_integrated[$i];
                                            $parent_count++;
                                            $parent_integrated[count($parent_integrated) - 1]['list_type'] = $current_child_type;
                                            $parent_integrated[count($parent_integrated) - 1]['child_events'] = [];
                                            $parent_integrated[count($parent_integrated) - 1]['child_events'][] = $parent_integrated[$i]['child_events'][$j];
                                            array_splice($parent_integrated[$i]['child_events'], $j, 1);
                                            $j--;
                                            $child_count--;
                                        } else {
                                            $parent_integrated[count($parent_integrated) - 1]['child_events'][] = $parent_integrated[$i]['child_events'][$j];
                                            array_splice($parent_integrated[$i]['child_events'], $j, 1);
                                            $child_count--;
                                            $j--;
                                        }
                                    }
                                } else {
                                    array_splice($parent_integrated[$i]['child_events'], $j, 1);
                                    $child_count--;
                                    $j--;
                                }
                            }
                            $parent_integrated[$i]['event_capacity'] = $cevent_capacity;
                        }
                        if ($parent_integrated[$i]['list_type'] == 'live') {
                            $response['live'][] = $parent_integrated[$i];
                        } elseif ($parent_integrated[$i]['list_type'] == 'draft') {
                            $response['draft'][] = $parent_integrated[$i];
                        } elseif ($parent_integrated[$i]['list_type'] == 'past') {
                            $response['past'][] = $parent_integrated[$i];
                        }
                    }
                }
//                for($i=0;$i<count($parent_integrated);$i++){//split-up by live, past & draft
//                    if (count($parent_integrated[$i]['child_events']) > 0) {
//                        usort($parent_integrated[$i]['child_events'], function($a, $b) {
//                            return $a['event_begin_dt'] > $b['event_begin_dt'];
//                        });
//                    }
//                    if($parent_integrated[$i]['list_type']=='live'){
//                        $response['live'][] = $parent_integrated[$i];
//                    }elseif($parent_integrated[$i]['list_type']=='draft'){
//                        $response['draft'][] = $parent_integrated[$i];
//                    }elseif($parent_integrated[$i]['list_type']=='past'){
//                        $response['past'][] = $parent_integrated[$i];
//                    }
//                }

                usort($response['live'], function($a, $b) {
                    return $a['event_sort_order'] < $b['event_sort_order'];
                });
                $out = array('status' => "Success", 'msg' => $response);
                // If success everythig is good send header as "OK" and user details
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Event details doesn't exist.");
                $this->response($this->json($error), 401); // If no records "No Content" status
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

    //Get Discount details by Event ID
    protected function getDiscountDetails($company_id, $event_id) {
        $sql = sprintf("SELECT `event_discount_id` , `discount_type` , `discount_code` , `discount_amount` FROM `event_discount`
                WHERE `company_id` = '%s' AND `event_id`='%s' AND `is_expired`!='Y'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
        $result = mysqli_query($this->db, $sql);
        $output = $response = [];
        if ($result) {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                $response['status'] = "Success";
            } else {
                $response['status'] = "Failed";
            }
        } else {
            $response['status'] = "Failed";
        }
        $response['discount'] = $output;
        return $response;
    }

    private function calculateProcessingFee($amount, $fee_type, $process_fee_per, $process_fee_val) {
        $p1_fee = ($amount * ($process_fee_per / 100)) + $process_fee_val;
        $p2_fee = ($amount + $p1_fee) * ($process_fee_per / 100) + $process_fee_val;
        if ($fee_type === 2 || $fee_type === '2') {
            $p_fee = $p2_fee;
        } else {
            $p_fee = $p1_fee;
        }
        return $p_fee;
    }

    protected function wepayCreateCheckout($company_id, $event_id, $actual_student_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, $payment_type, $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $reg_version_user, $discount_code, $default_cc, $actual_participant_id, $device_type, $app_id, $push_device_id) {
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $upgrade_status = $sts['upgrade_status'];
        $buyer_name = $buyer_first_name . ' ' . $buyer_last_name;
        if (is_null($actual_student_id) || $actual_student_id <= 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $device_type, $app_id, $push_device_id, '', '');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $actual_student_id;
        }
        if (is_null($actual_participant_id) || $actual_participant_id == 0) {
            $participant_id = $this->getParticipantIdForEvent($company_id, $student_id, $reg_col1, $reg_col2);
        } else {
            $participant_id = $actual_participant_id;
        }
        $gmt_date = gmdate(DATE_RFC822);
        $sns_msg = array("buyer_email" => $buyer_email, "membership_title" => $event_name, "gmt_date" => $gmt_date);
        $this->checkQuantityDuringCheckout($company_id, $event_id, $event_array, $quantity, $event_type);
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        $balance_due = $deposit_amt = $pr_fee = $temp_pr_fee = 0;
        $no_of_payments = $authorize_only = 0;
        $payment_startdate = $current_date = date("Y-m-d");
//        $check_deposit_failure = 0;

        $p_f = $this->getProcessingFee($company_id, $upgrade_status);
        $process_fee_per = $p_f['PP'];
        $process_fee_val = $p_f['PT'];
        if ($fee == 0 && $processing_fee_type == 1) {
            $fee = number_format((float) ($payment_amount * $process_fee_per / 100 + $process_fee_val), 2, '.', '');
        }

        if (count($payment_array) > 0) {
            $no_of_payments = count($payment_array) - 1;
            for ($i = 0; $i < count($payment_array); $i++) {
                if (isset($payment_array[$i]['date']) && empty($payment_array[$i]['date'])) {
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 400);
                }
                if (isset($payment_array[$i]['amount']) && empty($payment_array[$i]['amount'])) {
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 400);
                }
                if (isset($payment_array[$i]['type']) && empty($payment_array[$i]['type'])) {
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 400);
                }

                if ($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount'] > 0) {
//                    $balance_due = $payment_array[$i]['amount'];
                    $deposit_amt = $payment_array[$i]['amount'];
                    $pr_fee = $payment_array[$i]['processing_fee'];
                } elseif ($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount'] == 0) {
                    $authorize_only = 1;
                    $no_of_payments = count($payment_array);
                }
                $temp_pr_fee += $payment_array[$i]['processing_fee'];
            }
            $fee = $temp_pr_fee;
        } else {
            $pr_fee = $fee;
        }

        usort($event_array, function($a, $b) {
            return $a['event_begin_dt'] > $b['event_begin_dt'];
        });

        if (count($event_array) > 0) {
            for ($i = 0; $i < count($event_array); $i++) {
                if (isset($event_array[$i]['event_id']) && empty($event_array[$i]['event_id'])) {
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 400);
                }
                if (isset($event_array[$i]['event_quantity']) && empty($event_array[$i]['event_quantity'])) {
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 400);
                }
                if (!isset($event_array[$i]['event_cost'])) {
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 400);
                }
                if (!isset($event_array[$i]['event_discount_amount'])) {
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 400);
                }
                if (!isset($event_array[$i]['event_payment_amount'])) {
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 400);
                }
                if (!isset($event_array[$i]['event_begin_dt'])) {
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error), 400);
                }
                if (count($payment_array) > 0) {
                    if ($balance_due > 0) {
                        $amt = $event_array[$i]['event_payment_amount'];
                        if ($balance_due >= $event_array[$i]['event_payment_amount']) {
                            if ($balance_due == $amt) {
                                $t_amt = $amt;
                            } else {
                                $t_amt = $balance_due - $amt;
                            }
                            $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($t_amt, $processing_fee_type, $process_fee_per, $process_fee_val);
                            $event_array[$i]['balance_due'] = 0;
                            $balance_due = $balance_due - $amt;
                        } else {
                            $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($balance_due, $processing_fee_type, $process_fee_per, $process_fee_val);
                            $event_array[$i]['balance_due'] = $amt - $balance_due;
                            $balance_due = 0;
                        }
                    } elseif ($balance_due == 0) {
                        $event_array[$i]['processing_fee'] = 0;
                        $event_array[$i]['balance_due'] = $event_array[$i]['event_payment_amount'];
                    }
                } else {
                    $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($event_array[$i]['event_payment_amount'], $processing_fee_type, $process_fee_per, $process_fee_val);
                    $event_array[$i]['balance_due'] = 0;
                }
            }
        }

        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency` FROM `wp_account` WHERE `company_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                if ($user_state == 'deleted') {
                    $msg = "Wepay user registered was deleted.";
                    $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
                    $this->response($this->json($log), 200);
                }

                if ($acc_state == 'deleted') {
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log), 200);
                } elseif ($acc_state == 'disabled') {
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log), 200);
                }

                if (!empty($access_token) && !empty($account_id)) {
                    if (!function_exists('getallheaders')) {
                        if (!function_exists('apache_request_headers')) {
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        } else {
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    } else {
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);

//                    $json = array("account_id"=>"$account_id");
//                    $postData = json_encode($json);
//                    $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent,$sns_msg);
//                    $this->wp->log_info("wepay_get_account: ".$response['status']);
//                    if($response['status']=='Success'){
//                        $res = $response['msg'];
//                        $account_state = $res['state'];
//                        $currency = $res['currencies'][0];
//                        $curr_acc = implode(",", $res['currencies']);
//                        $disabled_reasons = implode(",", $res['disabled_reasons']);
//                        if($account_state=='deleted'){
//                            $msg = "Wepay Account associated with this MyStudio account was deleted.";
//                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
//                            $this->response($this->json($log),200);
//                        }elseif($account_state=='disabled'){
//                            $msg = "Wepay Account associated with this MyStudio account was disabled(For ".$disabled_reasons.").";
//                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
//                            $this->response($this->json($log),200);
//                        }
//                    }else{
//                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                        $this->response($this->json($log),200);
//                    }

                    $paid_amount = $paid_fee = $gross = 0;
                    $checkout_id = $checkout_state = '';
                    $cc_name = $cc_month = $cc_year = '';
                    $cc_status = $cc_state;

                    if ($payment_amount > 0 && $authorize_only == 0) {
                        $checkout_id_flag = 1;
                        $rand = mt_rand(1, 99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url . "/Wepay2/updateCheckout";
                        $unique_id = $company_id . "_" . $event_id . "_" . $ip . "_" . $rand . "_" . time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id . "_" . $event_id . "_" . $date;
//                        if($processing_fee_type==2){
//                            $fee_payer = "payer_from_app";
//                        }elseif($processing_fee_type==1){
                        $fee_payer = "payee_from_app";
//                        }
                        $paid_amount = $payment_amount;
                        $paid_fee = $fee;
                        if (count($payment_array) > 0) {
                            for ($i = 0; $i < count($payment_array); $i++) {
                                if ($payment_array[$i]['type'] == 'D') {
                                    $paid_amount = $payment_array[$i]['amount'];
                                    $paid_fee = $payment_array[$i]['processing_fee'];
                                    break;
                                }
                            }
                        }

                        if ($processing_fee_type == 2) {
                            $w_paid_amount = $payment_amount + $fee;
                        } elseif ($processing_fee_type == 1) {
                            $w_paid_amount = $payment_amount;
                        }
                        if (count($payment_array) > 0) {
                            for ($i = 0; $i < count($payment_array); $i++) {
                                if ($payment_array[$i]['type'] == 'D') {
                                    if ($processing_fee_type == 2) {
                                        $w_paid_amount = $payment_array[$i]['amount'] + $payment_array[$i]['processing_fee'];
                                    } elseif ($processing_fee_type == 1) {
                                        $w_paid_amount = $payment_array[$i]['amount'];
                                    }
                                    break;
                                }
                            }
                        }
                        $time = time();
                        $json2 = array("account_id" => "$account_id", "short_description" => "$desc", "type" => "event", "amount" => "$w_paid_amount", "currency" => "$currency",
                            "fee" => array("app_fee" => $paid_fee, "fee_payer" => "$fee_payer"), "reference_id" => "$ref_id", "unique_id" => "$unique_id",
                            "email_message" => array("to_payee" => "Payment has been added for $event_name", "to_payer" => "Payment has been made for $event_name"),
                            "payment_method" => array("type" => "credit_card", "credit_card" => array("id" => $cc_id)), "callback_uri" => "$checkout_callback_url",
                            "payer_rbits" => array(array("receive_time" => $time, "type" => "phone", "source" => "user", "properties" => array("phone" => "$buyer_phone", "phone_type" => "home")),
                                array("receive_time" => $time, "type" => "email", "source" => "user", "properties" => array("email" => "$buyer_email")),
                                array("receive_time" => $time, "type" => "address", "source" => "user", "properties" => array("address" => array("zip" => "$buyer_postal_code", "country" => "$country")))),
                            "transaction_rbits" => array(array("receive_time" => $time, "type" => "transaction_details", "source" => "partner_database",
                                    "properties" => array("itemized_receipt" => array(array("description" => "$event_name", "item_price" => $w_paid_amount, "quantity" => 1, "amount" => $w_paid_amount, "currency" => "$currency"))))));
                        $postData2 = json_encode($json2);
                        $sns_msg['call'] = "Checkout";
                        $sns_msg['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create", 'POST', $postData2, $token, $user_agent, $sns_msg);
                        $this->wp->log_info("wepay_checkout_create: " . $response2['status']);
                        if ($response2['status'] == "Success") {
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $gross = $res2['gross'];
//                            $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                        } else {
                            if ($response2['msg']['error_code'] == 1008) {
                                $response2 = $this->wp->accessWepayApi("checkout/create", 'POST', $postData2, $token, $user_agent, $sns_msg);
                                $this->wp->log_info("wepay_checkout_create: " . $response2['status']);
                                if ($response2['status'] == "Success") {
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $gross = $res2['gross'];
//                                    $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                                } else {
                                    if ($response2['msg']['error_code'] == 1008) {
                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error), 400);
                                    } else {
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error), 400);
                                    }
                                }
                            } else {
                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error), 400);
                            }
                        }
                    } else {
                        $checkout_id_flag = 0;
                        if (!empty(trim($cc_id))) {
                            $json3 = array("client_id" => "$this->wp_client_id", "client_secret" => "$this->wp_client_secret", "credit_card_id" => $cc_id);
                            $postData3 = json_encode($json3);
                            $sns_msg['call'] = "Credit Card";
                            $sns_msg['type'] = "Credit Card details";
                            $response3 = $this->wp->accessWepayApi("credit_card", 'POST', $postData3, $token, $user_agent, $sns_msg);
                            $this->wp->log_info("wepay_credit_card: " . $response3['status']);
                            if ($response3['status'] == "Success") {
                                $res3 = $response3['msg'];
                                if ($res3['state'] == 'new') {
                                    $json2 = array("client_id" => "$this->wp_client_id", "client_secret" => "$this->wp_client_secret", "credit_card_id" => $cc_id, "account_id" => $account_id);
                                    $postData2 = json_encode($json2);
                                    $sns_msg['call'] = "Credit Card";
                                    $sns_msg['type'] = "Credit Card Authorization";
                                    $response2 = $this->wp->accessWepayApi("credit_card/authorize", 'POST', $postData2, $token, $user_agent, $sns_msg);
                                    $this->wp->log_info("wepay_credit_card_authorize: " . $response2['status']);
                                    if ($response2['status'] == "Success") {
                                        $res2 = $response2['msg'];
                                        $cc_name = $res2['credit_card_name'];
                                        $cc_new_state = $res2['state'];
                                        if ($cc_state != $cc_new_state) {
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res2['expiration_month'];
                                        $cc_year = $res2['expiration_year'];
                                    } else {
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error), 400);
                                    }
                                } elseif ($res3['state'] == 'authorized') {
                                    $cc_name = $res3['credit_card_name'];
                                    $cc_new_state = $res3['state'];
                                    $cc_month = $res3['expiration_month'];
                                    $cc_year = $res3['expiration_year'];
                                } else {
                                    if ($res3['state'] == 'expired') {
                                        $msg = "Given credit card is expired.";
                                    } elseif ($res3['state'] == 'deleted') {
                                        $msg = "Given credit card was deleted.";
                                    } else {
                                        $msg = "Given credit card details is invalid.";
                                    }
                                    $error = array("status" => "Failed", "msg" => $msg);
                                    $this->response($this->json($error), 400);
                                }
                            } else {
                                $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                $this->response($this->json($error), 400);
                            }
                        }
//                        $json2 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id,"account_id"=>$account_id);
//                        $postData2 = json_encode($json2);
//                        $response2 = $this->wp->accessWepayApi("credit_card/authorize",'POST',$postData2,$token,$user_agent);
//                        $this->wp->log_info("wepay_credit_card_authorize: ".$response2['status']);
//                        if($response2['status']=="Success"){
//                            $res2 = $response2['msg'];
//                            $cc_name = $res2['credit_card_name'];
//                            $cc_new_state = $res2['state'];
//                            if($cc_state!=$cc_new_state){
//                                $cc_status = $cc_new_state;
//                            }
//                            $cc_month = $res2['expiration_month'];
//                            $cc_year = $res2['expiration_year'];
//                        }else{
//                            $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
//                            $this->response($this->json($error),400);
//                        }
                    }

                    if (!empty(trim($cc_id))) {
                        $json3 = array("client_id" => "$this->wp_client_id", "client_secret" => "$this->wp_client_secret", "credit_card_id" => $cc_id);
                        $postData3 = json_encode($json3);
                        $sns_msg['call'] = "Credit Card";
                        $sns_msg['type'] = "Credit Card details";
                        $response3 = $this->wp->accessWepayApi("credit_card", 'POST', $postData3, $token, $user_agent, $sns_msg);
                        $this->wp->log_info("wepay_credit_card: " . $response3['status']);
                        if ($response3['status'] == "Success") {
                            $res3 = $response3['msg'];
                            $cc_name = $res3['credit_card_name'];
                            $cc_new_state = $res3['state'];
                            if ($cc_state != $cc_new_state) {
                                $cc_status = $cc_new_state;
                            }
                            $cc_month = $res3['expiration_month'];
                            $cc_year = $res3['expiration_year'];
                        } else {
                            for ($k = 0; $k < 2; $k++) {
                                if ($response3['msg']['error_code'] == 503) {
                                    sleep(2);
                                    $response3 = $this->wp->accessWepayApi("credit_card", 'POST', $postData3, $token, $user_agent, $sns_msg);
                                    $this->wp->log_info("wepay_credit_card: " . $response3['status']);
                                    if ($response3['status'] == "Success") {
                                        $res3 = $response3['msg'];
                                        $cc_name = $res3['credit_card_name'];
                                        $cc_new_state = $res3['state'];
                                        if ($cc_state != $cc_new_state) {
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res3['expiration_month'];
                                        $cc_year = $res3['expiration_year'];
                                        break;
                                    }
                                    if ($k == 1) {
                                        if ($checkout_id_flag == 1) {
                                            $type = $response3['msg']['error'];
                                            $description = $response3['msg']['error_description'] . " Buyer Name:" . $buyer_name . " Buyer Email:" . $buyer_email . " Checkout Id:" . $checkout_id . " Participant Name:" . $reg_col1 . " " . $reg_col2;
                                            $insert_alertquery = sprintf("INSERT INTO `wepay_critical_alert`(`company_id`, `type`, `description`) VALUES ('%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $type), mysqli_real_escape_string($this->db, $description));
                                            $result_alert = mysqli_query($this->db, $insert_alertquery);
                                            if (!$result_alert) {
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_alertquery");
                                                mlog_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 500);
                                            }
                                            $type1 = $response3['msg']['error'] . " Regarding Credit Card";
                                            $description1 = "Fetching Credit Card details failed" . "\n" . " Buyer Name:" . $buyer_name . "\n" . " Buyer Email:" . $buyer_email . "\n" . " Checkout Id:" . $checkout_id . "\n" . " Participant Name:" . $reg_col1 . " " . $reg_col2;
                                            $this->sendSnsforfailedcheckout($type1, $description1);
                                        }
                                        $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                        $this->response($this->json($error), 400);
                                    }
                                } else {
                                    $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                    $this->response($this->json($error), 400);
                                }
                            }
                        }
                    }

                    if ($default_cc == 'Y') {
                        $cardselectquery = sprintf("SELECT * from `payment_method_details` where `student_id`='%s' ", mysqli_real_escape_string($this->db, $student_id));
                        $resultcardselectquery = mysqli_query($this->db, $cardselectquery);
                        if (!$resultcardselectquery) {
                            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$cardselectquery");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal server Error.");
                            $this->response($this->json($error), 500);
                        } else {
                            $num_rows = mysqli_num_rows($resultcardselectquery);
                            if ($num_rows > 0) {
                                $updatequery = sprintf("UPDATE `payment_method_details` SET  `credit_card_id`='%s',`credit_card_status`='%s',`credit_card_name`='%s',`credit_card_expiration_month`='%s',`credit_card_expiration_year`='%s',`buyer_email`='%s',`buyer_name`='%s',`buyer_postal_code`='%s'
                               ,`country`='%s' WHERE `student_id`='%s' ", mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name)
                                        , mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name)
                                        , mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $student_id));

                                $resultupdatequery = mysqli_query($this->db, $updatequery);
                                if (!$resultupdatequery) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$updatequery");
                                    mlog_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            } else {
                                $cardinsertquery = sprintf("INSERT INTO `payment_method_details`( `company_id`, `student_id`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `buyer_email`, `buyer_name`, `buyer_postal_code`,`country`) 
                                VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name)
                                        , mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $country));
                                $resultinsertquery = mysqli_query($this->db, $cardinsertquery);
                                if (!$resultinsertquery) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$cardinsertquery");
                                    mlog_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }

                    $query = sprintf("INSERT INTO `event_registration`(`company_id`, `event_id`, `reg_event_type`, `student_id`, `participant_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                        `payment_type`, `payment_amount`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `deposit_amount`, `number_of_payments`, `payment_startdate`, `payment_frequency`, `total_order_amount`, `total_order_quantity`, 
                        `event_registration_column_1`, `event_registration_column_2`, `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`, `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`,`event_reg_type_user`,`event_reg_version_user`)
                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_type), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $deposit_amt), mysqli_real_escape_string($this->db, $no_of_payments), mysqli_real_escape_string($this->db, $payment_startdate), mysqli_real_escape_string($this->db, $payment_frequency), mysqli_real_escape_string($this->db, $total_order_amount), mysqli_real_escape_string($this->db, $total_order_quantity), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4), mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user));
                    $result = mysqli_query($this->db, $query);
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $event_reg_id = mysqli_insert_id($this->db);

                        if (count($payment_array) > 0) {
                            $temp1 = $temp4 = 0;
                            for ($i = 0; $i < count($payment_array); $i++) {
                                $pdate = $payment_array[$i]['date'];
                                $pamount = $payment_array[$i]['amount'];
                                $ptype = $payment_array[$i]['type'];
                                $pfee = $payment_array[$i]['processing_fee'];
                                if ($ptype != 'D') {
                                    $checkout_id = '';
                                    $checkout_state = '';
                                    $pstatus = 'N';
                                } else {
                                    $pstatus = 'S';
                                }

                                $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_date`, `schedule_status`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $pfee), mysqli_real_escape_string($this->db, $pamount), mysqli_real_escape_string($this->db, $pdate), mysqli_real_escape_string($this->db, $pstatus), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name));
                                $payment_result = mysqli_query($this->db, $payment_query);
                                if (!$payment_result) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                                    mlog_info($this->json($error_log));
                                    $temp1 += 1;
                                }
                            }
                            if ($temp1 > 0) {
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp1);
                                $this->response($this->json($error), 500);
                            }
                        } else {
                            $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_status`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name));
                            $payment_result = mysqli_query($this->db, $payment_query);
                            if (!$payment_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                                mlog_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }

                        if (count($event_array) > 0) {
                            $qty = $net_sales = $parent_net_sales = 0;
                            $temp = $temp2 = $temp3 = 0;
                            for ($i = 0; $i < count($event_array); $i++) {
                                $cevent_id = $event_array[$i]['event_id'];
                                $cquantity = $event_array[$i]['event_quantity'];
                                $cevent_cost = $event_array[$i]['event_cost'];
                                $cdiscount = $event_array[$i]['event_discount_amount'];
                                $cpayment_amount = $event_array[$i]['event_payment_amount'];
                                $cbalance = $event_array[$i]['balance_due'];
                                $cprocessing_fee = $event_array[$i]['processing_fee'];
                                if (count($payment_array) > 0) {
                                    if ($cbalance > 0) {
                                        $cstatus = 'RP';
                                    } else {
                                        $cstatus = 'RC';
                                    }
                                } else {
                                    $cstatus = 'CC';
                                }

                                $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $cevent_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cevent_cost), mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cbalance), mysqli_real_escape_string($this->db, $cprocessing_fee), mysqli_real_escape_string($this->db, $cstatus));
                                $reg_det_result = mysqli_query($this->db, $reg_det_query);
                                if (!$reg_det_result) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                                    mlog_info($this->json($error_log));
                                    $temp += 1;
                                }

                                $pd_amount = $cpayment_amount - $cbalance;

                                if (count($payment_array) > 0) {
                                    if ($processing_fee_type == 2) {
                                        $net_sales += $pd_amount;
                                        $parent_net_sales += $pd_amount;
                                    } elseif ($processing_fee_type == 1) {
                                        $net_sales += $pd_amount - $cprocessing_fee;
                                        $parent_net_sales += $pd_amount - $cprocessing_fee;
                                    }
                                } else {
                                    if ($processing_fee_type == 2) {
                                        $parent_net_sales = $payment_amount;
                                    } else {
                                        $parent_net_sales = $payment_amount - $fee;
                                    }
                                }
                                $qty += $cquantity;
//                                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                                $update_event_result = mysqli_query($this->db, $update_event_query);
                                if (!$update_event_result) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                                    mlog_info($this->json($error_log));
                                    $temp2 += 1;
                                }
                            }
                            if ($temp > 0) {
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp);
                                $this->response($this->json($error), 500);
                            }
                            if ($temp2 > 0) {
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp2);
                                $this->response($this->json($error), 500);
                            }

                            $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$qty WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                            $update_event_result = mysqli_query($this->db, $update_event_query);
                            if (!$update_event_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                                mlog_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        } else {
//                            $sbalance = $payment_amount - $paid_amount;
                            $sbalance = $payment_amount;
                            if ($sbalance > 0) {
                                $fee = $pr_fee;
                            }
                            if ($processing_fee_type == 1) {
                                $paid_amount -= $fee;
                            }
                            if (count($payment_array) > 0) {
                                if ($sbalance > 0) {
                                    $sstatus = 'RP';
                                } else {
                                    $sstatus = 'RC';
                                }
                            } else {
                                $sstatus = 'CC';
                            }
                            $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $quantity), mysqli_real_escape_string($this->db, $sbalance), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $sstatus));
                            $reg_det_result = mysqli_query($this->db, $reg_det_query);
                            if (!$reg_det_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                                mlog_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }

                            $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                            $update_event_result = mysqli_query($this->db, $update_event_query);
                            if (!$update_event_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                                mlog_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }
                        $selectcurrency = sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'", mysqli_escape_string($this->db, $company_id));
                        $resultselectcurrency = mysqli_query($this->db, $selectcurrency);
                        if (!$resultselectcurrency) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                            mlog_info($this->json($error_log));
                        } else {
                            $row = mysqli_fetch_assoc($resultselectcurrency);
                            $wp_currency_symbol = $row['wp_currency_symbol'];
                        }

                        $curr_date = gmdate("Y-m-d H:i:s");
                        if ($event_type == 'S') {
                            if (!empty($discount_code)) {
                                $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount . '.Discount code used ' . $discount_code;
                            } else {
                                if (!empty($discount)) {
                                    $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount . '.Discount amount used ' . $discount;
                                } else {
                                    $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount;
                                }
                            }
                            $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                            $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                            if (!$result_insert_event_history) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                                mlog_info($this->json($error_log));
                            }
                        } else {
                            for ($i = 0; $i < count($event_array); $i++) {
                                $event_title = $event_array[$i]['event_title'];
                                $event_quantity = $event_array[$i]['event_quantity'];
                                $event_cost = $event_array[$i]['event_cost'];
                                $event_discount_amount = $event_array[$i]['event_discount_amount'];
                                if (!empty($discount_code)) {
                                    $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol" . (($event_cost - $event_discount_amount) * $event_quantity) . '. Discount code used ' . $discount_code;
                                } else {
                                    if ($event_discount_amount != 0 || !empty($event_discount_amount)) {
                                        $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol" . (($event_cost - $event_discount_amount) * $event_quantity) . ". Discount amount used $wp_currency_symbol" . ($event_discount_amount * $event_quantity);
                                    } else {
                                        $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol" . (($event_cost - $event_discount_amount) * $event_quantity);
                                    }
                                }
                                $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                                $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                                if (!$result_insert_event_history) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                                    mlog_info($this->json($error_log));
                                }
                            }
                        }
                        $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
                        $msg = array("status" => "Success", "msg" => "Event registration was successful. Email confirmation sent.", "memberapp_details" => $student_info_details, "student_id" => $student_id, "participant_id" => $participant_id);
                        $this->responseWithWepay($this->json($msg), 200);
                        $this->sentEmailForEventPaymentSuccess($event_reg_id, $company_id);
                    }
                }
            } else {
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

    protected function rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $amount, $currency) {
        $time = time();
        $gmt_date = gmdate(DATE_RFC822);
        $sns_msg1 = array("buyer_email" => $buyer_email, "membership_title" => $event_name, "gmt_date" => $gmt_date);
        $json = array("associated_object_type" => "checkout", "associated_object_id" => $checkout_id, "receive_time" => $time, "type" => "person", "source" => "user", "properties" => array("name" => "$buyer_name"),
            "related_rbits" => array(array("receive_time" => $time, "type" => "phone", "source" => "user", "properties" => array("phone" => "$buyer_phone", "phone_type" => "home")),
                array("receive_time" => $time, "type" => "email", "source" => "user", "properties" => array("email" => "$buyer_email")),
                array("receive_time" => $time, "type" => "address", "source" => "user", "properties" => array("address" => array("zip" => "$buyer_postal_code", "country" => "$country")))));
        $postData = json_encode($json);
        $response = $this->wp->accessWepayApi("rbit/create", 'POST', $postData, $token, $user_agent, $sns_msg1);
        $this->wp->log_info("wepay_rbit_create: " . $response['status']);
        if ($response['status'] == "Success") {
            $json2 = array("associated_object_type" => "checkout", "associated_object_id" => $checkout_id, "receive_time" => $time, "type" => "transaction_details", "source" => "partner_database",
                "properties" => array("itemized_receipt" => array(array("description" => "$event_name", "item_price" => $amount, "quantity" => 1, "amount" => $amount, "currency" => "$currency"))));
            $postData2 = json_encode($json2);
            $response2 = $this->wp->accessWepayApi("rbit/create", 'POST', $postData2, $token, $user_agent, $sns_msg1);
            $this->wp->log_info("wepay_rbit_create: " . $response2['status']);
            if ($response2['status'] == "Success") {
                
            } else {
                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                $this->response($this->json($error), 400);
            }
        } else {
            $error = array("status" => "Failed", "msg" => $response['msg']['error_description']);
            $this->response($this->json($error), 400);
        }
    }

    protected function addParticipantDetailsForFreeEvent($company_id, $event_id, $actual_student_id, $actual_participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_type, $event_array, $reg_type_user, $reg_version_user, $discount_code, $device_type, $app_id, $push_device_id) {
        $this->getStudioSubscriptionStatus($company_id);
        $this->checkQuantityDuringCheckout($company_id, $event_id, $event_array, $quantity, $event_type);
//        $last_occurance_space = strripos($buyer_name,' ');// spliting buyer name into two
//        $buyer_last_name = trim(substr($buyer_name,$last_occurance_space));
//        $buyer_first_name = trim(substr($buyer_name,0,$last_occurance_space));
        if (is_null($actual_student_id) || $actual_student_id <= 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $device_type, $app_id, $push_device_id, '', '');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $actual_student_id;
        }
        if (is_null($actual_participant_id) || $actual_participant_id == 0) {
            $participant_id = $this->getParticipantIdForEvent($company_id, $student_id, $reg_col1, $reg_col2);
        } else {
            $participant_id = $actual_participant_id;
        }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $current_date = date("Y-m-d");
        $payment_type = 'F';
        $checkout_id = $checkout_state = '';

        $query = sprintf("INSERT INTO `event_registration`(`company_id`, `event_id`,`reg_event_type`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                        `payment_type`, `payment_amount`, `paid_amount`, `event_registration_column_1`, `event_registration_column_2`,
                        `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`, `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`,`event_reg_type_user`,`event_reg_version_user`)
                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_type), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4), mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $event_reg_id = mysqli_insert_id($this->db);
            $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `schedule_status`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, 'S'));
            $payment_result = mysqli_query($this->db, $payment_query);
            if (!$payment_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }

            if (count($event_array) > 0) {
                $qty = $net_sales = 0;
                $temp = $temp2 = $temp3 = 0;
                for ($i = 0; $i < count($event_array); $i++) {
                    $cevent_id = $event_array[$i]['event_id'];
                    $cquantity = $event_array[$i]['event_quantity'];
                    $cevent_cost = $event_array[$i]['event_cost'];
                    $cdiscount = $event_array[$i]['event_discount_amount'];
                    $cpayment_amount = $event_array[$i]['event_payment_amount'];
                    $cstatus = 'CC';

                    $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $cevent_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cevent_cost), mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cstatus));
                    $reg_det_result = mysqli_query($this->db, $reg_det_query);
                    if (!$reg_det_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                        mlog_info($this->json($error_log));
                        $temp += 1;
                    }

                    $qty += $cquantity;

                    $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                    $update_event_result = mysqli_query($this->db, $update_event_query);
                    if (!$update_event_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                        mlog_info($this->json($error_log));
                        $temp2 += 1;
                    }
                }
                if ($temp > 0) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp);
                    $this->response($this->json($error), 500);
                }
                if ($temp2 > 0) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp2);
                    $this->response($this->json($error), 500);
                }

                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$qty WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                $update_event_result = mysqli_query($this->db, $update_event_query);
                if (!$update_event_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            } else {

                $sstatus = 'CC';
                $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $quantity), mysqli_real_escape_string($this->db, $sstatus));
                $reg_det_result = mysqli_query($this->db, $reg_det_query);
                if (!$reg_det_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }

                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                $update_event_result = mysqli_query($this->db, $update_event_query);
                if (!$update_event_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }

            $selectcurrency = sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'", mysqli_escape_string($this->db, $company_id));
            $resultselectcurrency = mysqli_query($this->db, $selectcurrency);
            if (!$resultselectcurrency) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                mlog_info($this->json($error_log));
            } else {
                $row = mysqli_fetch_assoc($resultselectcurrency);
                $wp_currency_symbol = $row['wp_currency_symbol'];
            }

            $curr_date = gmdate("Y-m-d H:i:s");
            if ($event_type == 'S') {
                if (!empty($discount_code)) {
                    $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount . '.Discount code used ' . $discount_code;
                } else {
                    if (!empty($discount)) {
                        $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount . '.Discount amount used ' . $discount;
                    } else {
                        $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount;
                    }
                }
                $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                if (!$result_insert_event_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                    mlog_info($this->json($error_log));
                }
            } else {
                for ($i = 0; $i < count($event_array); $i++) {
                    $event_title = $event_array[$i]['event_title'];
                    $event_quantity = $event_array[$i]['event_quantity'];
                    $event_cost = $event_array[$i]['event_cost'];
                    $event_discount_amount = $event_array[$i]['event_discount_amount'];
                    if (!empty($discount_code)) {
                        $activity_text = "Registered for " . $event_name . " " . $event_title . ',quantity ' . $event_quantity . '.Total cost ' . $wp_currency_symbol . $event_cost . '.Discount code used ' . $discount_code;
                    } else {
                        if (!empty($event_discount_amount)) {
                            $activity_text = "Registered for " . $event_name . " " . $event_title . ',quantity ' . $event_quantity . '.Total cost ' . $wp_currency_symbol . $event_cost . '.Discount amount used ' . $event_discount_amount;
                        } else {
                            $activity_text = "Registered for " . $event_name . " " . $event_title . ',quantity ' . $event_quantity . '.Total cost ' . $wp_currency_symbol . $event_cost;
                        }
                    }
                    $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                    $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                    if (!$result_insert_event_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                        mlog_info($this->json($error_log));
                    }
                }
            }
            $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
            $log = array("status" => "Success", "msg" => "Event registration was successful. Email confirmation sent.", "memberapp_details" => $student_info_details, "student_id" => $student_id, "participant_id" => $participant_id);
            $this->responseWithWepay($this->json($log), 200);
            $this->sentEmailForEventPaymentSuccess($event_reg_id, $company_id);
        }
        date_default_timezone_set($curr_time_zone);
    }

    private function sentEmailForEventPaymentSuccess($event_reg_id, $company_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $created_date = " DATE(CONVERT_TZ(ep.created_dt,$tzadd_add,'$new_timezone'))";
        $waiver_policies = '';
        $query = sprintf("SELECT er.`company_id`, er.`event_id`, `event_title`, if(ep.`payment_from`='S',ep.`stripe_card_name`,ep.`cc_name`) as cc_name, `waiver_policies`, concat(er.event_registration_column_2, ',', er.event_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `payment_type`, 
            er.`payment_amount` total_due, er.`processing_fee_type`, er.`processing_fee` total_processing_fee, `paid_amount`, ep.`payment_amount`, ep.`processing_fee`, ep.`refunded_amount`, `schedule_date`, `schedule_status`, DATE($created_date) created_dt
            FROM `event_registration` er LEFT JOIN `event_payment` ep ON er.`event_reg_id` = ep.`event_reg_id` AND ep.`schedule_status` IN ('N', 'S', 'PR', 'M')
            LEFT JOIN `event` e ON e.`event_id` = er.`event_id` AND e.`company_id` =  er.`company_id`
            WHERE er.`event_reg_id` = '%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            exit();
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {

                $event_name = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $student_cc_email_list = $processing_fee_type = '';
                $inital_stage = $initial_down_pay = 0;
                $current_plan_details = $event_details = $bill_due_details = $payment_details = [];
                $total_due = $paid_due = $rem_due = 0;
                while ($row = mysqli_fetch_assoc($result)) {
                    $paid_amt = 0;
                    $company_id = $row['company_id'];
                    $event_id = $row['event_id'];
                    $event_name = $row['event_title'];
                    $participant_name = $row['participant_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $temp = [];

                    if ($row['processing_fee_type'] == 2) {
                        $temp['amount'] = $row['payment_amount'] + $row['processing_fee'];
                    } else {
                        $temp['amount'] = $row['payment_amount'];
                    }
//                    $temp['amount'] = $row['payment_amount'];
                    $temp['date'] = $row['schedule_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['edit_status'] = 'N';
                    $temp['schedule_status'] = $row['schedule_status'];
                    $temp['processing_fee'] = $row['processing_fee'];
                    $temp['cc_name'] = $row['cc_name'];
                    if ($row['schedule_status'] == 'N') {
                        $temp['status'] = 'N';
                    } elseif ($row['schedule_status'] == 'S') {
                        $temp['status'] = 'S';
                        $paid_amt = $temp['amount'];
                        $inital_stage++;
                    } elseif ($row['schedule_status'] == 'PR' || $row['schedule_status'] == 'M') {
                        $paid_amt = $temp['amount'] - $temp['refunded_amount'];
                    }
//                    $total_due = $current_plan_details['total_due'] = $row['total_due'];
                    $total_due += $temp['amount'];
                    $current_plan_details['total_due'] = $total_due;
                    $paid_due += $paid_amt;
                    $current_plan_details['paid_due'] = $paid_due;

                    $current_plan_details['success_count'] = $inital_stage;
                    $rem_due = $current_plan_details['rem_due'] = number_format((float) ($total_due - $paid_due), 2, '.', '');
//                    $payment_details[] = $temp;
                    if ($row['schedule_status'] == 'N') {
                        $bill_due_details[] = $temp;
                    } elseif ($row['schedule_status'] == 'S') {
                        $payment_details[] = $temp;
                        if ($initial_down_pay > 0) {
                            $bill_due_details[] = $temp;
                        }
                        $initial_down_pay++;
                    } elseif ($row['schedule_status'] == 'M') {
                        $payment_details[] = $temp;
                    }
                }

                $query2 = sprintf("SELECT c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`, e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, `event_end_dt`, erd.`event_cost`, `discount`, erd.`payment_amount`, `quantity`, `payment_status`, `balance_due`,s.`student_cc_email` 
                    FROM `event` e LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` LEFT JOIN `event_registration` er ON er.`event_reg_id`= erd.`event_reg_id`
                    LEFT JOIN `company` c ON e.`company_id` =  c.`company_id` LEFT JOIN `student` s ON s.`company_id`= er.`company_id` AND s.`student_id`= er.`student_id`
                    WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND erd.`event_reg_id`='%s' AND `payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                $result2 = mysqli_query($this->db, $query2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    mlog_info($this->json($error_log));
                    exit();
                } else {
                    $temp_quantity = $temp_char_count = 0;
                    while ($row2 = mysqli_fetch_assoc($result2)) {
                        $temp2 = [];
                        $temp2['title'] = $row2['event_title'];
                        if ($temp2['title'] > $temp_char_count) {
                            $temp_char_count = strlen($temp2['title']);
                        }

                        if (!empty(trim($row2['event_begin_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_begin_dt']) {
                                $start = " - ";
                            } else {
                                $start = $row2['event_begin_dt'];

                                $split_start = explode(" ", $start);
                                $start_date_value = $split_start[0];
                                $start_time_value = $split_start[1];
                                if ("00:00:00" == $start_time_value) {
                                    $start = date('M d, Y', strtotime($start));
                                } else {
                                    $start = date('M d, Y @ h:i A', strtotime($start));
                                }
                            }
                        }

                        if (!empty(trim($row2['event_end_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_end_dt']) {
                                $end = " - ";
                            } else {
                                $end = $row2['event_end_dt'];

                                $split_end = explode(" ", $end);
                                $end_date_value = $split_end[0];
                                $end_time_value = $split_end[1];
                                if ("00:00:00" == $end_time_value) {
                                    $end = date('M d, Y', strtotime($end));
                                } else {
                                    $end = date('M d, Y @ h:i A', strtotime($end));
                                }
                            }
                        }

                        $temp2['start'] = $start;
                        $temp2['end'] = $end;
                        $temp2['event_cost'] = $row2['event_cost'];
                        $temp2['quantity'] = $row2['quantity'];
                        $temp2['event_disc'] = $row2['discount'];
                        $temp2['discounted_price'] = $row2['event_cost'] - $row2['discount'];
                        $temp_quantity += $row2['quantity'];
                        $temp2['payment_amount'] = $row2['payment_amount'];
                        $temp2['paid_due'] = $temp2['payment_amount'] - $row2['balance_due'];
                        $temp2['rem_due'] = $row2['balance_due'];
                        $temp2['state'] = 'N';
                        $event_details[] = $temp2;
                        $studio_name = $row2['company_name'];
                        $studio_mail = $row2['email_id'];
                        $cc_email_list = $row2['referral_email_list'];
                        $wp_currency_code = $row2['wp_currency_code'];
                        $wp_currency_symbol = $row2['wp_currency_symbol'];
                        $student_cc_email_list = $row2['student_cc_email'];
                    }
                    $current_plan_details['total_event'] = $temp_quantity;
                }
            } else {
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                mlog_info($this->json($error_log));
                exit();
            }
        }

        $subject = $event_name . " Order Confirmation";

        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
        $message .= "<b>Event Details</b><br><br>";
        for ($i = 0; $i < count($event_details); $i++) {
            $message .= "Event: " . $event_details[$i]['title'] . "<br>Starts: " . $event_details[$i]['start'] . "<br>Ends: " . $event_details[$i]['end'] . "<br>Participant: " . $participant_name . "<br><br>";
        }

        if ($total_due > 0) {
            $message .= "<b>Billing Details</b><br><br>";
            if (count($bill_due_details) > 0) {
                $message .= "Total due: $wp_currency_symbol" . $total_due . "<br>";
                $message .= "Balance due: $wp_currency_symbol" . $rem_due . "<br>";
                for ($j = 0; $j < count($bill_due_details); $j++) {
                    $x = $j + 1;
                    $amount = $bill_due_details[$j]['amount'];
                    $refunded_amount = $bill_due_details[$j]['refunded_amount'];
                    $final_amt = $amount - $refunded_amount;
                    $schedule_status = $bill_due_details[$j]['schedule_status'];
                    $date = date("M dS, Y", strtotime($bill_due_details[$j]['date']));
                    $recurring_processing_fee = $bill_due_details[$j]['processing_fee'];
                    if ($schedule_status == 'N') {
                        $paid_str = "due";
                    }
                    $recur_pf_str = "";
                    if ($processing_fee_type == 2) {
                        $recur_pf_str = " (plus $wp_currency_symbol" . $recurring_processing_fee . " administrative fees)";
                    }
                    $message .= "Bill " . $x . ": $wp_currency_symbol" . $final_amt . " " . $paid_str . " " . $date . " " . $recur_pf_str . "  <br>";
                }

                if (count($payment_details) > 0) {
                    $pf_str = $pf_str1 = "";
                    $message .= "<br><br><b>Payment Details</b><br><br>";
                    $pr_fee = $payment_details[0]['processing_fee'];
                    if ($processing_fee_type == 2) {
                        $pf_str = " (includes $wp_currency_symbol" . $pr_fee . " administrative fees)";
                    }
                    $down_pay = $payment_details[0]['amount'] - $payment_details[0]['refunded_amount'] . $pf_str . " paid on " . date("M dS, Y", strtotime($payment_details[0]['date'])) . "(" . $payment_details[0]['cc_name'] . ")";

                    $message .= "Down Payment: $wp_currency_symbol" . $down_pay . "<br>";
                    for ($k = 1; $k < count($payment_details); $k++) {
                        $x = $k;
                        $amount = $payment_details[$k]['amount'];
                        $refunded_amount = $payment_details[$k]['refunded_amount'];
                        $final_amt = $amount - $refunded_amount;
                        $schedule_status = $payment_details[$k]['schedule_status'];
                        $date = date("M dS, Y", strtotime($payment_details[$k]['date']));
                        $paid_str = "paid on";
                        $pr_fee1 = $payment_details[$k]['processing_fee'];
                        if ($processing_fee_type == 2) {
                            $pf_str1 = " (includes $wp_currency_symbol" . $pr_fee1 . " administrative fees)";
                        }
                        if ($schedule_status == 'S') {
                            $pay_type = "(" . $payment_details[$k]['cc_name'] . ")";
                        } else {
                            $pay_type = "(manual credit applied)";
                        }
                        $message .= "Payment " . $x . ": $wp_currency_symbol" . $final_amt . " " . $pf_str1 . " " . $paid_str . " " . $date . " " . $pay_type . " <br>";
                    }
                }
            } else {
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $schedule_status = $payment_details[0]['schedule_status'];
                $final_amount = $amount - $refunded_amount;
                $pr_fee2 = $payment_details[0]['processing_fee'];
                $pf_str2 = "";
                if ($processing_fee_type == 2) {
                    $pf_str2 = " (includes $wp_currency_symbol" . $pr_fee2 . " administrative fees)";
                }
                $message .= "Total Due: $wp_currency_symbol" . $final_amount . "<br>Amount Paid: $wp_currency_symbol" . $final_amount . $pf_str2 . "(" . $payment_details[0]['cc_name'] . ")<br>";
            }
            $message .= "<br><br>";
        }

        $waiver_present = 0;
        $file = '';
        if (!empty(trim($waiver_policies))) {
            $waiver_present = 1;
            $file_name_initialize = 10;
            for ($z = 0; $z < $file_name_initialize; $z++) {
                $dt = time();
                $file = "../../../uploads/" . $dt . "_waiver_policies.html";
                if (file_exists($file)) {
                    sleep(1);
                    $file_name_initialize++;
                } else {
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }

        $sendEmail_status = $this->sendEmailForEvent($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list, $student_cc_email_list);
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            mlog_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            mlog_info($this->json($error_log));
        }
        date_default_timezone_set($curr_time_zone);
    }

    protected function wepayStatus($company_id) {
        $query = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if (!empty(trim($curr))) {
                    $currency = explode(",", trim($curr));
                }
                if ($row['account_state'] == 'active') {
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                } else {
                    $res = array('status' => "Success", "msg" => "A", "info" => "Deleted/Disabled/Action needed.");
                }
            } else {
                $res = array('status' => "Failed", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }

    protected function stripeStatus($company_id) {
        $query = sprintf("SELECT * FROM `stripe_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if (!empty(trim($curr))) {
                    $currency = explode(",", trim($curr));
                }
                if ($row['account_state'] == 'Y') {
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                } else {
                    $res = array('status' => "Success", "msg" => "Y", "info" => "Deleted/Disabled/Action needed.");
                }
            } else {
                $res = array('status' => "Failed", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }

    public function getAllMembershipDetails($company_id, $call_back) {

        //check array_slice in following before alter or add any columns in this query
        $sql = sprintf("SELECT m.`membership_id`, m.`category_status`, m.`category_title`, m.`category_subtitle`, m.`category_image_url`, m.`category_video_url`, m.`category_description`, m.`members_active`, m.`members_onhold`, m.`category_average_membership_period`, m.`category_net_sales`, m.`category_sort_order`, IF(m.`category_status`='P','live','Draft') list_type,
                mr.`membership_rank_id` rank_id, mr.`rank_name`, mr.`rank_order`,
                mo.`membership_option_id`, mo.`membership_title`, mo.`membership_subtitle`, mo.`membership_description`, IF(mo.`membership_structure`=1, 'OE', mo.`membership_structure`) membership_structure, mo.`membership_processing_fee_type`, 
                mo.`membership_signup_fee`, mo.`membership_fee`, mo.`show_in_app_flg`, mo.`options_net_sales`, mo.`membership_recurring_frequency`, mo.`custom_recurring_frequency_period_type`, 
                mo.`custom_recurring_frequency_period_val`, mo.`prorate_first_payment_flg`, mo.`delay_recurring_payment_start_flg`, mo.`delayed_recurring_payment_type`, mo.`delayed_recurring_payment_val`, mo.`recurring_start_date`, mo.`option_sort_order`, m.`waiver_policies`, mo.`initial_payment_include_membership_fee`, 
                mo.`billing_options`, mo.`no_of_classes`, mo.`expiration_date_type`, mo.`expiration_date_val`, mo.`deposit_amount`, mo.`no_of_payments`, mo.`billing_payment_start_date_type`, mo.`billing_options_payment_start_date`, mo.`expiration_status`, 
                mo.`specific_start_date`,mo.`specific_end_date`,mo.`specific_payment_frequency`,mo.`exclude_from_billing_flag`,mo.`billing_days`,
                m.`membership_registration_column_1`, m.`membership_reg_col_1_mandatory_flag`, m.`membership_registration_column_2`, m.`membership_reg_col_2_mandatory_flag`, m.`membership_registration_column_3`, m.`membership_reg_col_3_mandatory_flag`, m.`membership_registration_column_4`, m.`membership_reg_col_4_mandatory_flag`, 
                m.`membership_registration_column_5`, m.`membership_reg_col_5_mandatory_flag`, m.`membership_registration_column_6`, m.`membership_reg_col_6_mandatory_flag`, m.`membership_registration_column_7`, m.`membership_reg_col_7_mandatory_flag`, m.`membership_registration_column_8`, m.`membership_reg_col_8_mandatory_flag`, 
                m.`membership_registration_column_9`, m.`membership_reg_col_9_mandatory_flag`, m.`membership_registration_column_10`, m.`membership_reg_col_10_mandatory_flag`, 
                md.`membership_discount_id`, md.`membership_option_id` disc_mem_opt_id, md.`discount_off`, md.`discount_type`, md.`discount_code`, md.`discount_amount`,
                mbx.`membership_billing_exclude_id`, mbx.`membership_option_id` exclude_mem_opt_id, mbx.`billing_exclude_startdate`, mbx.`billing_exclude_enddate`, mbx.`deleted_flag`
                FROM `membership` m LEFT JOIN `membership_ranks` mr ON m.`membership_id` = mr.`membership_id` AND m.`company_id` = mr.`company_id` AND mr.`rank_deleted_flg`!='D'
                LEFT JOIN `membership_options` mo ON m.`membership_id` = mo.`membership_id` AND m.`company_id` = mo.`company_id` AND mo.`deleted_flg`!='D'
                LEFT JOIN `membership_discount` md ON m.`membership_id` = md.`membership_id` AND mo.`membership_option_id` = md.`membership_option_id` AND m.`company_id` = md.`company_id` AND md.`is_expired`='N'
                LEFT JOIN `membership_billing_exclude` mbx ON m.`membership_id` = mbx.`membership_id` AND mo.`membership_option_id` = mbx.`membership_option_id` AND m.`company_id` = mbx.`company_id` AND mbx.`deleted_flag`!='D'
                WHERE m.`company_id`='%s' AND m.`deleted_flg`!='D' AND IF(membership_structure='SE', IF(specific_end_date IS NOT NULL && specific_end_date!='0000-00-00', specific_end_date>=CURDATE(), 0), 1) order by `category_sort_order`", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 1) {
                return $error;
            } else {
                $this->response($this->json($error), 500);
            }
        } else {
            $membership_arr = $membership_id_arr = $membership_options_arr = $membership_options_id_arr = $membership_billing_exclude_id_arr = [];
            $membership_rank_arr = $membership_rank_id_arr = $membership_disc_arr = $membership_disc_id_arr = $membership_billing_exclude = [];

            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $reg_columns = [];
                    $membership_id = $row['membership_id'];

                    $row_category = array_slice($row, 0, 13);
                    if (!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)) {
                        $membership_id_arr[] = $membership_id;
                        $membership_arr[] = $row_category;
                    }

                    $row_ranks = array_slice($row, 13, 3);
                    $row_ranks['membership_id'] = $membership_id;
                    $rank_id = $row_ranks['rank_id'];
                    if (!empty($rank_id) && !in_array($rank_id, $membership_rank_id_arr, true)) {
                        $membership_rank_id_arr[] = $rank_id;
                        $membership_rank_arr[] = $row_ranks;
                    }

                    $row_options = array_slice($row, 16, 55);
                    $row_options['membership_id'] = $membership_id;
                    $option_id = $row_options['membership_option_id'];
                    if (!empty($option_id) && !in_array($option_id, $membership_options_id_arr, true)) {
                        $membership_options_id_arr[] = $option_id;

                        $reg_field_name_array = [];
                        $temp_reg_field = "membership_registration_column_";
                        for ($z = 1; $z <= 10; $z++) { //collect registration fields as array
                            $y = $z - 1;
                            $reg_field = $temp_reg_field . "$z";      //for filed names
                            $reg_field_mandatory = "membership_reg_col_" . $z . "_mandatory_flag";
                            if ($row[$reg_field] != '') {
                                $reg_field_name_array[] = array("reg_col_name" => $row[$reg_field], "reg_col_mandatory" => $row[$reg_field_mandatory], "reg_col_index" => $y);
                            }
                        }
                        $row_options['reg_columns'] = $reg_field_name_array;

                        $membership_options_arr[] = $row_options;
                    }

                    $row_disc = array_slice($row, 71, 6);
                    $row_disc['membership_id'] = $membership_id;
                    $disc_id = $row_disc['membership_discount_id'];
                    if (!empty($disc_id) && !in_array($disc_id, $membership_disc_id_arr, true)) {
                        $row_disc['membership_option_id'] = $row_disc['disc_mem_opt_id'];
                        $membership_disc_id_arr[] = $disc_id;
                        $membership_disc_arr[] = $row_disc;
                    }

                    $row_exclude_days = array_slice($row, 77);
                    $row_exclude_days['membership_id'] = $membership_id;
                    $membership_billing_exclude_id = $row_exclude_days['membership_billing_exclude_id'];

                    if (!empty($membership_billing_exclude_id) && !in_array($membership_billing_exclude_id, $membership_billing_exclude_id_arr, true)) {
                        $row_exclude_days['membership_option_id'] = $row_exclude_days['exclude_mem_opt_id'];
                        $membership_billing_exclude_id_arr[] = $membership_billing_exclude_id;
                        $membership_billing_exclude[] = $row_exclude_days;
                    }
                }

                for ($i = 0; $i < count($membership_arr); $i++) {
                    $rank = $rank_id = $option = $option_id = [];
                    $m_id = $membership_arr[$i]['membership_id'];
                    for ($j = 0; $j < count($membership_rank_arr); $j++) {
                        if ($m_id == $membership_rank_arr[$j]['membership_id'] && !in_array($membership_rank_arr[$j]['rank_id'], $rank_id)) {
                            $rank_id[] = $membership_rank_arr[$j]['rank_id'];
                            $rank[] = $membership_rank_arr[$j];
                        }
                    }
                    for ($k = 0; $k < count($membership_options_arr); $k++) {
                        $disc = $disc_id = [];
                        $exclude = $membership_billing_exclude_id = [];
                        if ($m_id == $membership_options_arr[$k]['membership_id']) {
                            for ($l = 0; $l < count($membership_disc_arr); $l++) {
                                if ($m_id == $membership_disc_arr[$l]['membership_id'] && $membership_disc_arr[$l]['disc_mem_opt_id'] == $membership_options_arr[$k]['membership_option_id']) {
                                    $disc[] = $membership_disc_arr[$l];
                                }
                            }
                            $membership_options_arr[$k]['membership_disc'] = $disc;

                            for ($m = 0; $m < count($membership_billing_exclude); $m++) {
                                if ($m_id == $membership_billing_exclude[$m]['membership_id'] && $membership_billing_exclude[$m]['exclude_mem_opt_id'] == $membership_options_arr[$k]['membership_option_id']) {
                                    $exclude[] = $membership_billing_exclude[$m];
                                }
                            }
                            $membership_options_arr[$k]['mem_billing_exclude'] = $exclude;
                            $option[] = $membership_options_arr[$k];
                        }
                    }
                    if (count($rank) > 0) {
                        usort($rank, function($a, $b) {
                            return $a['rank_order'] > $b['rank_order'];
                        });
                    }
                    if (count($option) > 0) {
                        usort($option, function($a, $b) {
                            return $a['option_sort_order'] < $b['option_sort_order'];
                        });
                    }
                    $membership_arr[$i]['rank_details'] = $rank;
                    $membership_arr[$i]['membership_options'] = $option;
                }

                if (count($membership_arr) > 0) {
                    usort($membership_arr, function($a, $b) {
                        return $a['category_sort_order'] < $b['category_sort_order'];
                    });
                }

                $response['live'] = $response['draft'] = [];
                for ($i = 0; $i < count($membership_arr); $i++) {
                    if ($membership_arr[$i]['list_type'] == 'live') {
                        $response['live'][] = $membership_arr[$i];
                    } else {
                        $response['draft'][] = $membership_arr[$i];
                    }
                }

                $out = array('status' => "Success", 'msg' => $response['live']);
                // If success everythig is good send header as "OK" and user details
                if ($call_back == 1) {
                    return $out;
                } else {
                    $this->response($this->json($out), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Membership category details doesn't exist.");
                if ($call_back == 1) {
                    return $error;
                } else {
                    $this->response($this->json($error), 500);
                }
            }
        }
    }

    //wepay membership checkout
    public function membershipCheckout($company_id, $membership_id, $membership_option_id, $actual_student_id, $upgrade_status, $membership_category_title, $membership_title, $membership_desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $processing_fee_type, $processing_fee, $membership_fee, $membership_fee_disc, $signup_fee, $signup_fee_disc, $payment_amount, $initial_payment, $first_payment, $payment_frequency, $payment_start_date, $recurring_start_date, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $prorate_first_payment_flg, $delay_recurring_payment_start_flg, $delay_recurring_payment_start_date, $delay_recurring_payment_amount, $custom_recurring_frequency_period_type, $custom_recurring_frequency_period_val, $initial_payment_include_membership_fee, $membership_structure, $billing_options, $no_of_classes, $billing_options_expiration_date, $billing_options_deposit_amount, $billing_options_no_of_payments, $payment_array, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $membership_discount_code, $signup_discount_code, $exclude_days_array, $program_start_date, $program_end_date, $exclude_from_billing_flag, $reg_type_user, $reg_version_user, $reg_current_date, $default_cc, $actual_participant_id, $membership_start_date, $device_type, $app_id, $push_device_id) {
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $upgrade_status = $sts['upgrade_status'];
        $buyer_name = $buyer_first_name . ' ' . $buyer_last_name;
        if (is_null($actual_student_id) || $actual_student_id <= 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $device_type, $app_id, $push_device_id, '', '');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $actual_student_id;
        }
        if (is_null($actual_participant_id) || $actual_participant_id == 0) {
            $participant_id = $this->getParticipantId($company_id, $student_id, $reg_col1, $reg_col2, $reg_col3);
        } else {
            $participant_id = $actual_participant_id;
        }

        $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id, $membership_category_title, $membership_title, '');
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $current_date = date("Y-m-d");
        $gmt_date = gmdate(DATE_RFC822);
        $sns_msg2 = array("buyer_email" => $buyer_email, "membership_title" => $membership_title, "gmt_date" => $gmt_date);

        if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') && $billing_options == 'PP' && !empty($payment_array)) {

            if ($payment_amount < 5) {
                $payment_amount = $payment_array[0]['amount'];
            }
//            if($recurring_start_date=='' || $recurring_start_date=='0000-00-00'){
            $recurring_start_date = $payment_array[0]['date'];
            $billing_options_no_of_payments = count($payment_array);
//            }
        }
        if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') && $billing_options == 'PF') {
            $payment_amount = 0;
            $recurring_start_date = '';
        }
        if ($membership_structure == 'SE' && $billing_options == 'PP' && !empty($payment_array) && $initial_payment_include_membership_fee == 'N' && $reg_current_date != $payment_start_date) {
            $first_payment = $payment_array[0]['amount'];
            if (isset($payment_array[1]) && isset($payment_array[1]['date'])) {
                $recurring_start_date = $payment_array[1]['date'];
            }
        }

        if (($first_payment < 5 && $first_payment > 0) && $payment_amount > 0 && $reg_current_date == $payment_start_date && $delay_recurring_payment_start_flg == 'N' && $initial_payment_include_membership_fee == 'N' && $prorate_first_payment_flg == 'Y' && $membership_structure == 'OE' && ($payment_frequency == 'M' || $payment_frequency == 'B')) {
            if ($payment_frequency == 'B') {
                if (date('j', strtotime($recurring_start_date)) < 16) {
                    $next_pay_date = date('Y-m-16', strtotime($recurring_start_date));
                } else {
                    $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($recurring_start_date)));
                }
            } elseif ($payment_frequency == 'M') {
                $next_pay_date = date('Y-m-d', strtotime('next month', strtotime($recurring_start_date)));
            }
            $recurring_start_date = $next_pay_date;
        }
        if (empty($membership_start_date)) {
            $membership_start_date = $payment_start_date;
        }
        if ($membership_structure == 'OE' && $delay_recurring_payment_start_flg == 'Y') {
            $payment_start_date = $delay_recurring_payment_start_date;
        }

        if ($initial_payment_include_membership_fee == 'N' && $reg_current_date == $payment_start_date) {
            $first_payment = 0;
        }

        $p_f = $this->getProcessingFee($company_id, $upgrade_status);
        $process_fee_per = $p_f['PP'];
        $process_fee_val = $p_f['PT'];
        if ($initial_payment <= 0) {
            $initial_payment_processing_fee = 0;
        } else {
            $initial_payment_processing_fee = $this->calculateProcessingFee($initial_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
        }
        if ($first_payment <= 0) {
            $first_payment_processing_fee = 0;
        } else {
            $first_payment_processing_fee = $this->calculateProcessingFee($first_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
        }
        if ($payment_amount <= 0) {
            $recurring_payment_processing_fee = 0;
        } else {
            $recurring_payment_processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
        }
        if ($delay_recurring_payment_amount <= 0) {
            $delay_recurring_payment_processing_fee = 0;
        } else {
            $delay_recurring_payment_processing_fee = $this->calculateProcessingFee($delay_recurring_payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
        }


        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency` FROM `wp_account` WHERE `company_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                if ($user_state == 'deleted') {
                    $msg = "Wepay user registered was deleted.";
                    $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
                    $this->response($this->json($log), 200);
                }

                if ($acc_state == 'deleted') {
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log), 200);
                } elseif ($acc_state == 'disabled') {
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log), 200);
                }

                if (!empty($access_token) && !empty($account_id)) {
                    if (!function_exists('getallheaders')) {
                        if (!function_exists('apache_request_headers')) {
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        } else {
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    } else {
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);

//                    $json = array("account_id"=>"$account_id");
//                    $postData = json_encode($json);
//                    $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent,$sns_msg2);
//                    $this->wp->log_info("wepay_get_account: ".$response['status']);
//                    if($response['status']=='Success'){
//                        $res = $response['msg'];
//                        $account_state = $res['state'];
//                        $currency = $res['currencies'][0];
//                        $curr_acc = implode(",", $res['currencies']);
//                        $disabled_reasons = implode(",", $res['disabled_reasons']);
//                        if($account_state=='deleted'){
//                            $msg = "Wepay Account associated with this MyStudio account was deleted.";
//                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
//                            $this->response($this->json($log),200);
//                        }elseif($account_state=='disabled'){
//                            $msg = "Wepay Account associated with this MyStudio account was disabled(For ".$disabled_reasons.").";
//                            $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
//                            $this->response($this->json($log),200);
//                        }
//                    }else{
//                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                        $this->response($this->json($log),200);
//                    }


                    if ($payment_frequency == 'N') {
                        if ($initial_payment != 0 || $first_payment != 0) {
                            $payment_type = 'O';
                        } else {
                            $payment_type = 'F';
                        }
                    } else {
                        if ($payment_amount != 0) {
                            $payment_type = 'R';
                        } else {
                            if ($initial_payment != 0 || $first_payment != 0) {
                                $payment_type = 'O';
                            } else {
                                $payment_type = 'F';
                            }
                        }
                    }

                    $initial_payment_success_flag = 'Y';
                    $initial_payment_date = $current_date;
                    $next_payment_date = $recurring_start_date;
                    $preceding_payment_date = $current_date;
                    $preceding_payment_status = 'S';

                    $paid_amount = $w_paid_amount = $payment_done = 0;
                    $checkout_id = $checkout_state = '';
                    $cc_name = $cc_month = $cc_year = '';
                    $cc_status = $cc_state;

                    if ($initial_payment >= 5) {
                        $checkout_id_flag = 1;
                        $rand = mt_rand(1, 99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url . "/Wepay2/updateCheckout?for=membership";
                        $unique_id = $company_id . "_" . $membership_id . "_" . $membership_option_id . "_" . $ip . "_" . $rand . "_" . time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id . "_" . $membership_id . "_" . $membership_option_id . "_" . $date;
                        $fee_payer = "payee_from_app";

                        if ($processing_fee_type == 2) {
                            $w_paid_amount = $initial_payment + $initial_payment_processing_fee;
                            $paid_amount = $initial_payment;
                        } elseif ($processing_fee_type == 1) {
                            $w_paid_amount = $initial_payment;
                            $paid_amount = $initial_payment - $initial_payment_processing_fee;
                        }
                        $time = time();
                        $json2 = array("account_id" => "$account_id", "short_description" => "$membership_desc", "type" => "event", "amount" => "$w_paid_amount", "currency" => "$currency",
                            "fee" => array("app_fee" => $initial_payment_processing_fee, "fee_payer" => "$fee_payer"), "reference_id" => "$ref_id", "unique_id" => "$unique_id",
                            "email_message" => array("to_payee" => "Payment has been added for $membership_category_title", "to_payer" => "Payment has been made for $membership_category_title"),
                            "payment_method" => array("type" => "credit_card", "credit_card" => array("id" => $cc_id)), "callback_uri" => "$checkout_callback_url",
                            "payer_rbits" => array(array("receive_time" => $time, "type" => "phone", "source" => "user", "properties" => array("phone" => "$buyer_phone", "phone_type" => "home")),
                                array("receive_time" => $time, "type" => "email", "source" => "user", "properties" => array("email" => "$buyer_email")),
                                array("receive_time" => $time, "type" => "address", "source" => "user", "properties" => array("address" => array("zip" => "$buyer_postal_code", "country" => "$country")))),
                            "transaction_rbits" => array(array("receive_time" => $time, "type" => "transaction_details", "source" => "partner_database",
                                    "properties" => array("itemized_receipt" => array(array("description" => "$membership_category_title", "item_price" => $w_paid_amount, "quantity" => 1, "amount" => $w_paid_amount, "currency" => "$currency"))))));
                        $postData2 = json_encode($json2);
                        $sns_msg2['call'] = "Checkout";
                        $sns_msg2['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create", 'POST', $postData2, $token, $user_agent, $sns_msg2);
                        $this->wp->log_info("wepay_checkout_create: " . $response2['status']);
                        if ($response2['status'] == "Success") {
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $gross = $res2['gross'];
                            $payment_done = 1;
//                            $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $membership_category_title, $w_paid_amount, $currency);
                        } else {
                            if ($response2['msg']['error_code'] == 1008) {
                                $response2 = $this->wp->accessWepayApi("checkout/create", 'POST', $postData2, $token, $user_agent, $sns_msg2);
                                $this->wp->log_info("wepay_checkout_create: " . $response2['status']);
                                if ($response2['status'] == "Success") {
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $gross = $res2['gross'];
                                    $payment_done = 1;
//                                    $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $membership_category_title, $w_paid_amount, $currency);
                                } else {
                                    if ($response2['msg']['error_code'] == 1008) {
                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error), 400);
                                    } else {
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error), 400);
                                    }
                                }
                            } else {
                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error), 400);
                            }
                        }
                    } else {
                        $checkout_id_flag = 0;
                        if (!empty(trim($cc_id))) {
                            $json3 = array("client_id" => "$this->wp_client_id", "client_secret" => "$this->wp_client_secret", "credit_card_id" => $cc_id);
                            $postData3 = json_encode($json3);
                            $sns_msg2['call'] = "Credit Card";
                            $sns_msg2['type'] = "Credit Card details";
                            $response3 = $this->wp->accessWepayApi("credit_card", 'POST', $postData3, $token, $user_agent, $sns_msg2);
                            $this->wp->log_info("wepay_credit_card: " . $response3['status']);
                            if ($response3['status'] == "Success") {
                                $res3 = $response3['msg'];
                                if ($res3['state'] == 'new') {
                                    $json2 = array("client_id" => "$this->wp_client_id", "client_secret" => "$this->wp_client_secret", "credit_card_id" => $cc_id, "account_id" => $account_id);
                                    $postData2 = json_encode($json2);
                                    $sns_msg2['call'] = "Credit Card";
                                    $sns_msg2['type'] = "Credit Card Authorization";
                                    $response2 = $this->wp->accessWepayApi("credit_card/authorize", 'POST', $postData2, $token, $user_agent, $sns_msg2);
                                    $this->wp->log_info("wepay_credit_card_authorize: " . $response2['status']);
                                    if ($response2['status'] == "Success") {
                                        $res2 = $response2['msg'];
                                        $cc_name = $res2['credit_card_name'];
                                        $cc_new_state = $res2['state'];
                                        if ($cc_state != $cc_new_state) {
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res2['expiration_month'];
                                        $cc_year = $res2['expiration_year'];
                                    } else {
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error), 400);
                                    }
                                } elseif ($res3['state'] == 'authorized') {
                                    $cc_name = $res3['credit_card_name'];
                                    $cc_new_state = $res3['state'];
                                    $cc_month = $res3['expiration_month'];
                                    $cc_year = $res3['expiration_year'];
                                } else {
                                    if ($res3['state'] == 'expired') {
                                        $msg = "Given credit card is expired.";
                                    } elseif ($res3['state'] == 'deleted') {
                                        $msg = "Given credit card was deleted.";
                                    } else {
                                        $msg = "Given credit card details is invalid.";
                                    }
                                    $error = array("status" => "Failed", "msg" => $msg);
                                    $this->response($this->json($error), 400);
                                }
                            } else {
                                $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                $this->response($this->json($error), 400);
                            }
                        }

                        $initial_payment_date = '';
                        $initial_payment_success_flag = 'N';
                        $preceding_payment_date = '';
                        $preceding_payment_status = 'N';
                    }

                    if (!empty(trim($cc_id))) {
                        $json3 = array("client_id" => "$this->wp_client_id", "client_secret" => "$this->wp_client_secret", "credit_card_id" => $cc_id);
                        $postData3 = json_encode($json3);
                        $sns_msg2['call'] = "Credit Card";
                        $sns_msg2['type'] = "Credit Card details";
                        $response3 = $this->wp->accessWepayApi("credit_card", 'POST', $postData3, $token, $user_agent, $sns_msg2);
                        $this->wp->log_info("wepay_credit_card: " . $response3['status']);
                        if ($response3['status'] == "Success") {
                            $res3 = $response3['msg'];
                            $cc_name = $res3['credit_card_name'];
                            $cc_new_state = $res3['state'];
                            if ($cc_state != $cc_new_state) {
                                $cc_status = $cc_new_state;
                            }
                            $cc_month = $res3['expiration_month'];
                            $cc_year = $res3['expiration_year'];
                        } else {
                            for ($k = 0; $k < 2; $k++) {
                                if ($response3['msg']['error_code'] == 503) {
                                    sleep(2);
                                    $response3 = $this->wp->accessWepayApi("credit_card", 'POST', $postData3, $token, $user_agent, $sns_msg2);
                                    $this->wp->log_info("wepay_credit_card: " . $response3['status']);
                                    if ($response3['status'] == "Success") {
                                        $res3 = $response3['msg'];
                                        $cc_name = $res3['credit_card_name'];
                                        $cc_new_state = $res3['state'];
                                        if ($cc_state != $cc_new_state) {
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res3['expiration_month'];
                                        $cc_year = $res3['expiration_year'];
                                        break;
                                    }
                                    if ($k == 1) {
                                        if ($checkout_id_flag == 1) {
                                            $type = $response3['msg']['error'];
                                            $description = $response3['msg']['error_description'] . " Buyer Name:" . $buyer_name . " Buyer Email:" . $buyer_email . " Checkout Id:" . $checkout_id . " Participant Name:" . $reg_col1 . " " . $reg_col2;
                                            $insert_alertquery = sprintf("INSERT INTO `wepay_critical_alert`(`company_id`, `type`, `description`) VALUES ('%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $type), mysqli_real_escape_string($this->db, $description));
                                            $result_alert = mysqli_query($this->db, $insert_alertquery);
                                            if (!$result_alert) {
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_alertquery");
                                                mlog_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 500);
                                            }
                                            $type1 = $response3['msg']['error'] . " Regarding Credit Card";
                                            $description1 = "Fetching Credit Card details failed" . "\n" . " Buyer Name:" . $buyer_name . "\n" . " Buyer Email:" . $buyer_email . "\n" . " Checkout Id:" . $checkout_id . "\n" . " Participant Name:" . $reg_col1 . " " . $reg_col2;
                                            $this->sendSnsforfailedcheckout($type1, $description1);
                                        }
                                        $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                        $this->response($this->json($error), 400);
                                    }
                                } else {
                                    $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                    $this->response($this->json($error), 400);
                                }
                            }
                        }
                    }
                    if ($default_cc == 'Y') {
                        $select_card_details = sprintf("SELECT * FROM `payment_method_details` WHERE `company_id`='%s' AND `student_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
                        $result_select_card_details = mysqli_query($this->db, $select_card_details);
                        if (!$result_select_card_details) {
                            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$select_card_details");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal server Error.");
                            $this->response($this->json($error), 500);
                        } else {
                            $select_num_rows = mysqli_num_rows($result_select_card_details);
                            if ($select_num_rows > 0) {
                                $updatequery = sprintf("UPDATE `payment_method_details` SET  `credit_card_id`='%s',`credit_card_status`='%s',`credit_card_name`='%s',`credit_card_expiration_month`='%s',`credit_card_expiration_year`='%s',`buyer_email`='%s',`buyer_name`='%s',`buyer_postal_code`='%s'
                                       ,`country`='%s' WHERE `student_id`='%s' ", mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name)
                                        , mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name)
                                        , mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $student_id));

                                $resultupdatequery = mysqli_query($this->db, $updatequery);
                                if (!$resultupdatequery) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$updatequery");
                                    mlog_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            } else {
                                $insertquery = sprintf("INSERT INTO `payment_method_details`(`company_id`, `student_id`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `buyer_email`, `buyer_name`, `country`, `buyer_postal_code`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $buyer_postal_code));
                                $resultinsertquery = mysqli_query($this->db, $insertquery);
                                if (!$resultinsertquery) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$insertquery");
                                    mlog_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }


                    $rank_details = $this->getInitialRank($membership_id);
                    $rank_status = $rank_details['rank_status'];
                    $rank_id = $rank_details['rank_id'];
                    $insert_reg = sprintf("INSERT INTO `membership_registration`(`company_id`, `membership_id`, `membership_option_id`, `student_id`, `participant_id`, `membership_status`, `membership_category_title`, `membership_title`,
                                `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `processing_fee_type`, `processing_fee`, `payment_type`, `membership_structure`, `prorate_first_payment_flg`, 
                                `delay_recurring_payment_start_flg`, `delay_recurring_payment_start_date`, `delay_recurring_payment_amount`, `custom_recurring_frequency_period_type`, `custom_recurring_frequency_period_val`, `initial_payment_include_membership_fee`, 
                                `billing_options`, `no_of_classes`, `billing_options_expiration_date`, `billing_options_deposit_amount`, `billing_options_no_of_payments`, 
                                `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `signup_fee`, `signup_fee_disc`, 
                                `membership_fee`, `membership_fee_disc`, `initial_payment`, `initial_payment_date`, `initial_payment_success_flag`, `first_payment`, `payment_amount`, `payment_frequency`, `membership_start_date`, `payment_start_date`, `recurring_start_date`, `next_payment_date`, 
                                `preceding_payment_date`, `preceding_payment_status`, `no_of_payments`, `membership_registration_column_1`, `membership_registration_column_2`, `membership_registration_column_3`, 
                                `membership_registration_column_4`, `membership_registration_column_5`, `membership_registration_column_6`, `membership_registration_column_7`, `membership_registration_column_8`, 
                                `membership_registration_column_9`, `membership_registration_column_10`, `participant_street`, `participant_city`, `participant_state`, `participant_zip`, `participant_country`, `rank_status`,`rank_id`,`membership_reg_type_user`,`membership_reg_version_user`,`specific_start_date`,`specific_end_date`,`exclude_billing_flag`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',
                                '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id), mysqli_real_escape_string($this->db, 'R'), mysqli_real_escape_string($this->db, $membership_category_title), mysqli_real_escape_string($this->db, $membership_title), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $membership_structure), mysqli_real_escape_string($this->db, $prorate_first_payment_flg), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_flg), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount), mysqli_real_escape_string($this->db, $custom_recurring_frequency_period_type), mysqli_real_escape_string($this->db, $custom_recurring_frequency_period_val), mysqli_real_escape_string($this->db, $initial_payment_include_membership_fee), mysqli_real_escape_string($this->db, $billing_options), mysqli_real_escape_string($this->db, $no_of_classes), mysqli_real_escape_string($this->db, $billing_options_expiration_date), mysqli_real_escape_string($this->db, $billing_options_deposit_amount), mysqli_real_escape_string($this->db, $billing_options_no_of_payments), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $signup_fee), mysqli_real_escape_string($this->db, $signup_fee_disc), mysqli_real_escape_string($this->db, $membership_fee), mysqli_real_escape_string($this->db, $membership_fee_disc), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_date), mysqli_real_escape_string($this->db, $initial_payment_success_flag), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $payment_frequency), mysqli_real_escape_string($this->db, $membership_start_date), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, $recurring_start_date), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, $preceding_payment_date), mysqli_real_escape_string($this->db, $preceding_payment_status), mysqli_real_escape_string($this->db, $payment_done), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4), mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $participant_street), mysqli_real_escape_string($this->db, $participant_city), mysqli_real_escape_string($this->db, $participant_state), mysqli_real_escape_string($this->db, $participant_zip), mysqli_real_escape_string($this->db, $participant_country), mysqli_real_escape_string($this->db, $rank_status), mysqli_real_escape_string($this->db, $rank_id), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $program_start_date), mysqli_real_escape_string($this->db, $program_end_date), mysqli_real_escape_string($this->db, $exclude_from_billing_flag));
                    $result_reg = mysqli_query($this->db, $insert_reg);

                    if (!$result_reg) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_reg");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $membership_reg_id = mysqli_insert_id($this->db);

                        if ($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') {
                            if ($initial_payment > 0) {
                                $insert_success_payment_nc = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'));
                                $result_paid_insert = mysqli_query($this->db, $insert_success_payment_nc);
                                if (!$result_paid_insert) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_success_payment_nc");
                                    mlog_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                            if ($billing_options == 'PP' && !empty($payment_array)) {
                                for ($i = 0; $i < count($payment_array); $i++) {
                                    if ($membership_structure == 'SE') {
                                        $prorate_flag_se = $payment_array[$i]['prorate_flag'];
                                    } else {
                                        $prorate_flag_se = 'F';
                                    }
                                    $paytime_type_check = $payment_array[$i]['type'];
                                    if ($membership_structure == 'SE' && $initial_payment_include_membership_fee == 'N' && $reg_current_date != $payment_start_date && $i == 0) {
                                        $paytime_type_check = 'F';
                                    }
                                    $insert_mem_payment_NC = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`, `prorate_flag`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_array[$i]['amount']), mysqli_real_escape_string($this->db, $payment_array[$i]['processing_fee']), mysqli_real_escape_string($this->db, $payment_array[$i]['date']), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $paytime_type_check), mysqli_real_escape_string($this->db, $prorate_flag_se));
                                    $result_pay_insert = mysqli_query($this->db, $insert_mem_payment_NC);
                                    if (!$result_pay_insert) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment_NC");
                                        mlog_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 500);
                                    }
                                }
                            } elseif ($membership_structure == 'SE' && $billing_options == 'PF' && $first_payment > 0) {
                                $insert_first_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'));
                                $result_first_insert = mysqli_query($this->db, $insert_first_payment);
                                if (!$result_first_insert) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_first_payment");
                                    mlog_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        } elseif ($membership_structure == 'OE') {
                            if ($initial_payment >= 5) {
                                if ($payment_type == 'R') {
                                    if ($delay_recurring_payment_start_flg == 'Y') {
                                        if ($delay_recurring_payment_amount >= 5) {
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount), mysqli_real_escape_string($this->db, $delay_recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        } elseif ($delay_recurring_payment_amount < 5 && $delay_recurring_payment_amount > 0) {
                                            $added_payment = $payment_amount + $delay_recurring_payment_amount;
                                            if ($added_payment <= 0) {
                                                $added_processing_fee = 0;
                                            } else {
                                                $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                            }

                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        } else {
                                            if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'), ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                                $added_payment = $payment_amount + $first_payment;
                                                if ($added_payment <= 0) {
                                                    $added_processing_fee = 0;
                                                } else {
                                                    $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                                }
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            } else {
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            }
                                        }
                                    } else {
                                        if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'), ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                            $added_payment = $payment_amount + $first_payment;
                                            if ($added_payment <= 0) {
                                                $added_processing_fee = 0;
                                            } else {
                                                $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                            }
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        } else {
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }
                                    }
                                } else {
                                    if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'));
                                    } else {
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'I'));
                                    }
                                }
                                $result_pay = mysqli_query($this->db, $insert_mem_payment);

                                if (!$result_pay) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                                    mlog_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            } else {
                                if ($payment_type == 'R') {
                                    if ($delay_recurring_payment_start_flg == 'Y') {
                                        if ($delay_recurring_payment_amount >= 5) {
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount), mysqli_real_escape_string($this->db, $delay_recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        } elseif ($delay_recurring_payment_amount < 5 && $delay_recurring_payment_amount > 0) {
                                            $added_payment = $payment_amount + $delay_recurring_payment_amount;
                                            if ($added_payment <= 0) {
                                                $added_processing_fee = 0;
                                            } else {
                                                $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                            }

                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        } else {
                                            if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                                $added_payment = $payment_amount + $first_payment;
                                                if ($added_payment <= 0) {
                                                    $added_processing_fee = 0;
                                                } else {
                                                    $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                                }
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            } else {
                                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                            }
                                        }
                                    } else {
                                        if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                            $added_payment = $payment_amount + $first_payment;
                                            if ($added_payment <= 0) {
                                                $added_processing_fee = 0;
                                            } else {
                                                $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                            }
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        } else {
                                            $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                        }
                                    }
                                    $result_pay = mysqli_query($this->db, $insert_mem_payment);

                                    if (!$result_pay) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                                        mlog_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 500);
                                    }
                                } else {
                                    if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'F'));

                                        $result_pay = mysqli_query($this->db, $insert_mem_payment);

                                        if (!$result_pay) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                                            mlog_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        }
                                    }
                                }
                            }

                            if ($payment_amount > 0 && $payment_type == 'R') {
                                $next_pay_date = $payment_start_date = date("Y-m-d", strtotime($next_payment_date));
                                $payment_end_date = date("Y-m-d", strtotime("+93 days"));
                                $recurring_payment_date_array = [];
                                $recurring_payment_date_array[] = $payment_start_date;
                                $iteration_end = 1;
                                $payment_date = $next_pay_date;
                                for ($i = 0; $i < $iteration_end; $i++) {
                                    if ($payment_frequency == 'W') {
                                        $next_pay_date = date('Y-m-d', strtotime('+1 weeks', strtotime($payment_date)));
                                    } elseif ($payment_frequency == 'B') {
                                        if (date('j', strtotime($payment_date)) < 16) {
                                            $next_pay_date = date('Y-m-16', strtotime($payment_date));
                                        } else {
                                            $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($payment_date)));
                                        }
                                    } elseif ($payment_frequency == 'M') {
                                        $next_pay_date = date('Y-m-d', strtotime('next month', strtotime($payment_date)));
                                    } elseif ($payment_frequency == 'A') {
                                        $next_pay_date = date('Y-m-d', strtotime('+1 year', strtotime($payment_date)));
                                    } elseif ($payment_frequency == 'C') {
                                        if ($custom_recurring_frequency_period_type == 'CW') {
                                            $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($payment_date)));
                                        } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                            $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($payment_date)));
                                        }
                                    }
                                    $payment_date = $next_pay_date;
                                    if ($next_pay_date <= $payment_end_date || count($recurring_payment_date_array) == 0) {
                                        $iteration_end++;
                                        $recurring_payment_date_array[] = $next_pay_date;
                                    }
                                }

                                for ($j = 1; $j < count($recurring_payment_date_array); $j++) {
                                    $insert_mem_pay_OE_R = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `paytime_type`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, 'R'));
                                    $result_mem_pay_OE_r = mysqli_query($this->db, $insert_mem_pay_OE_R);
                                    if (!$result_mem_pay_OE_r) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay_OE_R");
                                        mlog_info($this->json($error_log));
                                    }
                                }
                            }
                        }

                        $update_count = sprintf("UPDATE `membership` SET `members_active`=`members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
                        $result_count = mysqli_query($this->db, $update_count);

                        if (!$result_count) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }

                        $update_count2 = sprintf("UPDATE `membership_options` SET `options_members_active`=`options_members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
                        $result_count2 = mysqli_query($this->db, $update_count2);

                        if (!$result_count2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count2");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }

                    $activity_text = 'Registration date.';
                    if (!empty($signup_discount_code)) {
                        $activity_text .= ' Sign-up fee discount code used "' . $signup_discount_code . '".';
                        if (!empty($membership_discount_code)) {
                            $activity_text .= ' Membership fee discount code used "' . $membership_discount_code . '".';
                        } elseif (!empty($membership_fee_disc)) {
                            $activity_text .= ' Membership fee discount value used "' . $membership_fee_disc . '".';
                        }
                    } else {
                        if (!empty($signup_fee_disc)) {
                            $activity_text .= ' Sign-up fee discount value used "' . $signup_fee_disc . '".';
                            if (!empty($membership_discount_code)) {
                                $activity_text .= ' Membership fee discount code used "' . $membership_discount_code . '".';
                            } elseif (!empty($membership_fee_disc)) {
                                $activity_text .= ' Membership fee discount value used "' . $membership_fee_disc . '".';
                            }
                        } else {
                            if (!empty($membership_discount_code)) {
                                $activity_text .= ' Membership fee discount code used "' . $membership_discount_code . '".';
                            } elseif (!empty($membership_fee_disc)) {
                                $activity_text .= ' Membership fee discount value used "' . $membership_fee_disc . '".';
                            }
                        }
                    }

                    $curr_date = gmdate("Y-m-d H:i:s");
                    $sql1 = sprintf("INSERT INTO `membership_history`(`company_id`, `membership_registration_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $student_id), 'registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                    $result1 = mysqli_query($this->db, $sql1);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }

                    if ($exclude_from_billing_flag == 'Y' && count($exclude_days_array) > 0 && $membership_structure == 'SE') {
                        usort($exclude_days_array, function($a, $b) {
                            return $a['billing_exclude_startdate'] > $b['billing_exclude_startdate'];
                        });
                        for ($x = 0; $x < count($exclude_days_array); $x++) {
                            $activity_text = "Exclude days  : from " . date('M d Y', strtotime($exclude_days_array[$x]['billing_exclude_startdate'])) . " to " . date('M d Y', strtotime($exclude_days_array[$x]['billing_exclude_enddate']));
                            $curr_date = gmdate("Y-m-d H:i:s");
                            $insert_history_exclude = sprintf("INSERT INTO `membership_history`(`company_id`, `membership_registration_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Exclude', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                            $result_history_exculde = mysqli_query($this->db, $insert_history_exclude);
                            if (!$result_history_exculde) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history_exclude");
                                mlog_info($this->json($error_log));
                            }
                        }
                    }

                    if ($payment_type == 'F') {
                        $email_msg = "Registration successful.";
                    } else {
                        $email_msg = "Registration successful. Confirmation of purchase has been sent to your email.";
                    }
                    $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
                    $msg = array("status" => "Success", "msg" => $email_msg, "memberapp_details" => $student_info_details, "student_id" => $student_id, "participant_id" => $participant_id);
                    $this->responseWithWepay($this->json($msg), 200);
                    $this->sendEmailForMembershipPaymentSuccess($membership_reg_id, $company_id);
//                    $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id,$membership_category_title, $membership_title, '');

                    $this->updateMembershipDimensionsMembers($company_id, $membership_id, 'A', $membership_option_id);
//                    if($initial_payment>0){
//                        $this->updateMembershipDimensionsNetSales($company_id, $membership_id, '');
//                    }
                    $curr_date_time = date("Y-m-d ");
                    $sql_insert_date = sprintf("UPDATE membership_registration SET start_email_date ='%s' WHERE company_id='%s'  AND membership_registration_id='%s' ", mysqli_real_escape_string($this->db, $curr_date_time), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
                    $sql_insert_date_result = mysqli_query($this->db, $sql_insert_date);
                    if (!$sql_insert_date_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_insert_date");
                        mlog_info($this->json($error_log));
                    } else {
                        $message = array('status' => "Success", "msg" => "Email start date updated successfully.");
                        mlog_info($this->json($message));
                    }
                } else {
                    $error = array("status" => "Failed", "msg" => "Access token error. Cannot be accept payments anymore.");
                    $this->response($this->json($error), 200);
                }
            } else {
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

    public function getInitialRank($membership_id) {
        $query = sprintf("SELECT `rank_name` as rank_status, `membership_rank_id`as rank_id FROM `membership_ranks` WHERE `membership_id`='%s' AND `rank_deleted_flg`!='D' ORDER by `rank_order` ASC LIMIT 0,1", mysqli_real_escape_string($this->db, $membership_id));
        $result = mysqli_query($this->db, $query);
        $rank_details = [];
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $rank_details = mysqli_fetch_assoc($result);
            } else {
                $rank_details['rank_status'] = '';
                $rank_details['rank_id'] = '';
            }
            return $rank_details;
        }
    }

    private function sendEmailForMembershipPaymentSuccess($membership_reg_id, $company_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $created_date = " DATE(CONVERT_TZ(mp.created_dt,$tzadd_add,'$new_timezone'))";

        $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
        $payment_details = $prorate_flag_check = [];
        $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
        $recurring_start_date = $membership_start_date = $payment_start_date = $specific_end_date = $payment_frequency = $custom_recurring_frequency_period_type = $custom_recurring_frequency_period_val = $msg1 = $msg2 = $msg3 = '';
        $membership_structure = $billing_options = $billing_options_no_of_payments = '';
        $total_mem_fee = $registration_fee = 0;

        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, if(mp.`payment_from`='S',mp.`stripe_card_name`,mp.`cc_name`) as cc_name, `waiver_policies`, mr.credit_card_name, 
            mr.`membership_start_date`, mr.`payment_start_date`,mr.`specific_end_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, mr.`custom_recurring_frequency_period_val`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
            `buyer_postal_code`, mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, DATE($created_date) created_dt, mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`,mp.`prorate_flag`
            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id` LEFT JOIN `student` s ON s.`company_id`= mr.`company_id` AND s.`student_id`= mr.`student_id`
            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N')
            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            exit();
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
//                    $company_id = $row['company_id'];
//                    $membership_id = $row['membership_id'];
//                    $membership_option_id = $row['membership_option_id'];
                    $payment_type = $row['payment_type'];
                    $membership_category_title = $row['membership_category_title'];
                    $membership_title = $row['membership_title'];
                    $membership_structure = $row['membership_structure'];
                    $participant_name = $row['participant_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $signup_fee = $row['signup_fee'];
                    $signup_fee_disc = $row['signup_fee_disc'];
                    $mem_fee = $row['membership_fee'];
                    $mem_fee_disc = $row['membership_fee_disc'];
                    $initial_payment = $row['initial_payment'];
                    $inital_processing_fee = $row['initial_processing_fee'];
                    $membership_start_date = $row['membership_start_date'];
                    $payment_start_date = $row['payment_start_date'];
                    $recurring_start_date = $row['recurring_start_date'];
                    $next_payment_date = $row['next_payment_date'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $custom_recurring_frequency_period_val = $row['custom_recurring_frequency_period_val'];
                    $recurring_amount = $row['recurring_amount'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
                    $first_payment = $row['first_payment'];
                    $billing_options = $row['billing_options'];
                    $no_of_classes = $row['no_of_classes'];
                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $credit_card_name = $row['credit_card_name'];
                    $prorate_flag_check[] = $row['prorate_flag'];
                    $specific_end_date = $row['specific_end_date'];
                    $student_cc_email_list = $row['student_cc_email'];
                    $temp = [];
                    if (!empty($row['membership_payment_id'])) {
                        if ($recurring_amount == $row['payment_amount']) {
                            $recurring_processing_fee = $row['processing_fee'];
                        }
                        if ($row['processing_fee_type'] == 2) {
                            if ($membership_structure != 'SE') {
                                $temp['amount'] = $row['payment_amount'] + $row['processing_fee'];
                            } else if ($membership_structure == 'SE') {
                                $temp['amount'] = $row['payment_amount'];
                            }
                        } else {
                            $temp['amount'] = $row['payment_amount'];
                        }
                        $payment_date_temp = $temp['date'] = $row['payment_date'];
                        $temp['created_dt'] = $row['created_dt'];
                        $temp['processing_fee'] = $row['processing_fee'];
                        $temp['payment_status'] = $row['payment_status'];
                        $temp['paytime_type'] = $row['paytime_type'];
                        $temp['cc_name'] = $row['cc_name'];
                        $paid_amt += $temp['amount'];
                        $payment_details[] = $temp;
                    }
                }
                if ($membership_structure == 'SE') {
                    $registration_fee = number_format(($signup_fee - $signup_fee_disc), 2);
                    for ($i = 0; $i < count($payment_details); $i++) {
                        $total_mem_fee += $payment_details[$i]['amount'];
                    }
                    $total_mem_fee -= $registration_fee;
                }
                if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') && $billing_options == 'PP') {
                    if ($billing_options_no_of_payments > 1) {
                        $already_recurring_records_count = 0;
                        $next_date = '';
                        if ($membership_structure !== 'SE') {
                            for ($i = 0; $i < count($payment_details); $i++) {
                                if ($payment_details[$i]['paytime_type'] == 'R') {
                                    $already_recurring_records_count++;
                                    $next_date = $payment_details[$i]['date'];
                                }
                            }
                        }
                        if ($billing_options_no_of_payments > $already_recurring_records_count && $next_date !== '') {
                            $count = $billing_options_no_of_payments - $already_recurring_records_count;
                            $future_date = $next_date;
                            for ($j = 0; $j < $count; $j++) {
                                if ($payment_frequency == 'W') {
                                    $next_payment_date2 = date('Y-m-d', strtotime('+1 weeks', strtotime($future_date)));
                                } elseif ($payment_frequency == 'B') {
                                    $next_payment_date2 = date('Y-m-d', strtotime('+14 days', strtotime($future_date)));
                                } elseif ($payment_frequency == 'M') {
                                    $next_payment_date2 = date('Y-m-d', strtotime('next month', strtotime($future_date)));
                                }

                                $temp = [];
                                if ($processing_fee_type == 2) {
                                    if ($membership_structure != 'SE') {
                                        $temp['amount'] = $recurring_amount + $recurring_processing_fee;
                                    } elseif ($membership_structure == 'SE') {
                                        $temp['amount'] = $recurring_amount;
                                    }
                                } else {
                                    $temp['amount'] = $recurring_amount;
                                }
                                $temp['date'] = $next_payment_date2;
                                $temp['processing_fee'] = $recurring_processing_fee;
                                $temp['payment_status'] = 'N';
                                $temp['paytime_type'] = 'R';
                                $temp['cc_name'] = $credit_card_name;
                                $payment_details[] = $temp;
                                $future_date = $next_payment_date2;
                            }
                        }
                    }
                }
            } else {
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                mlog_info($this->json($error_log));
                exit();
            }
        }

        $subject = $membership_category_title . " Membership Order Confirmation";

        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
        $message .= "<b>Membership Details</b><br><br>";
        $message .= "Membership Start Date: " . date("M dS, Y", strtotime($membership_start_date)) . "<br>";
        if ($membership_structure == 'SE') {
            $message .= "Membership End Date: " . date("M dS, Y", strtotime($specific_end_date)) . "<br>";
            $message .= "Registration Fee: $wp_currency_symbol" . (number_format(($registration_fee), 2)) . "<br>";
            $message .= "Total Membership Fee: $wp_currency_symbol" . (number_format(($total_mem_fee), 2)) . "<br>";
        }
        $message .= "Membership: $membership_category_title, $membership_title<br>Participant: " . $participant_name . "<br>";
        if ($membership_structure == 'NC' && $billing_options == 'PP' && $billing_options_expiration_date != '0000-00-00') {
            $message .= "$no_of_classes Class Package Expires: " . date("D, M d, Y", strtotime($billing_options_expiration_date)) . "<br>";
        }
        $message .= "<br>";

        $prorated = "";
        if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
            $prorated = "(Pro-rated)";
        }

        if ($paid_amt > 0) {
//            if($initial_payment>0){
            if ($membership_structure !== 'SE') {
                $message .= "<br><b>Registration Details</b><br><br>";
                $message .= "Sign-up Fee: $wp_currency_symbol" . (number_format(($signup_fee - $signup_fee_disc), 2)) . "<br>";
            } else if ($membership_structure == 'SE' && $billing_options == 'PF') {
                $message .= "<br><b>Membership payment plan</b><br><br>";
//                $message .= "payment: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
//                $message .= " (plus $wp_currency_symbol".$inital_processing_fee." administrative fees)"."<br>";
            }   //check ravi
            if ($membership_structure !== 'SE') {
                if ($processing_fee_type == 2) {
                    if ($initial_payment > 0) {
                        $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment + $inital_processing_fee), 2)) . " (includes $wp_currency_symbol" . $inital_processing_fee . " administrative fees) <br>";
                    } else {
                        $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . "<br>";
                    }
                } else {
                    if ($initial_payment > 0) {
                        $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . " <br>";
                    } else {
                        $message .= "Total Payment: $wp_currency_symbol" . (number_format(($initial_payment), 2)) . "<br>";
                    }
                }
            }
        }
        if (count($payment_details) > 0) {
            if ($payment_frequency != 'N' && $payment_type == 'R') {
                $freq_str = "";
                if ($membership_structure == 'OE' || $membership_structure == 1) {
                    if ($payment_frequency == 'B') {
                        $freq_str = "Semi-Monthly Recurring: ";
                    } elseif ($payment_frequency == 'M') {
                        $freq_str = "Monthly Recurring: ";
                    } elseif ($payment_frequency == 'W') {
                        $freq_str = "Weekly Recurring: ";
                    } elseif ($payment_frequency == 'A') {
                        $freq_str = "Annual Recurring: ";
                    } elseif ($payment_frequency == 'C') {
                        if ($custom_recurring_frequency_period_type == 'CW') {
                            $freq_str = "Weekly Recurring: ";
                        } elseif ($custom_recurring_frequency_period_type == 'CM') {
                            $freq_str = "Monthly Recurring: ";
                        }
                    }
                } elseif ($billing_options == 'PP') {
                    if ($payment_frequency == 'W') {
                        $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
                    } elseif ($payment_frequency == 'B') {
                        $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
                    } elseif ($payment_frequency == 'M') {
                        $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
                    }
                }
                $message .= "<br><br><b>Membership Payment Plan</b><br><br>";
                $recur_pf_str = "";
                if ($membership_structure !== 'SE') {
                    if ($processing_fee_type == 2) {
                        $recur_pf_str = " (plus $wp_currency_symbol" . $recurring_processing_fee . " administrative fees)";
                    }
                    if ($freq_str != "") {
                        if (($membership_structure == 'OE' || $membership_structure == 1) && $payment_frequency == 'C') { // Ticket 1113
                            if ($custom_recurring_frequency_period_type == 'CW') {
                                $message .= "$wp_currency_symbol" . $recurring_amount . ' Recurring every ' . $custom_recurring_frequency_period_val . ' week(s)' . $recur_pf_str . "<br>";
                            } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                $message .= "$wp_currency_symbol" . $recurring_amount . ' Recurring every ' . $custom_recurring_frequency_period_val . ' month(s)' . $recur_pf_str . "<br>";
                            }
                        } else {
                            $message .= "$freq_str $wp_currency_symbol" . $recurring_amount . $recur_pf_str . "<br>";
                        }
                    }
//                $message .= "Next Payment Date: ".date("M dS, Y", strtotime($next_payment_date))."<br>";
                    for ($init_success_payments = 0; $init_success_payments < count($payment_details); $init_success_payments++) {
                        if ($payment_details[$init_success_payments]['paytime_type'] == 'R' && $payment_details[$init_success_payments]['payment_status'] == 'N') {
                            $date_temp = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                            $message .= "Next Payment Date: " . date("M dS, Y", strtotime($date_temp)) . "<br>";
                            break;
                        }
                    }
                }
            }
            if ($membership_structure !== 'SE') {
                $message .= "<br><br><b>Payment Details</b><br><br>";
            }
            for ($init_success_payments = 0; $init_success_payments < count($payment_details); $init_success_payments++) {
                $prorated_1 = $prorated_2 = $prorated_3 = "";
                $temp_se_date_check = date("d,m,Y,w", strtotime($payment_details[$init_success_payments]['date']));
                $se_check_prorate = explode(",", $temp_se_date_check);
                $pf_str = "";
                if ($processing_fee_type == 2) {
                    if ($membership_structure !== 'SE') {
                        $pf_str = " (includes $wp_currency_symbol" . $payment_details[$init_success_payments]['processing_fee'] . " administrative fees)";
                    } else {
                        $pf_str = " (plus $wp_currency_symbol" . $payment_details[$init_success_payments]['processing_fee'] . " administrative fees)";
                    }
                }
                if ($membership_structure !== 'SE') {
                    if ($processing_fee_type == 2) {
                        $amount_temp = $payment_details[$init_success_payments]['amount'] - $payment_details[$init_success_payments]['processing_fee'];
                    } else {
                        $amount_temp = $payment_details[$init_success_payments]['amount'];
                    }
                    if ($payment_details[$init_success_payments]['paytime_type'] == 'I') {
                        if (($mem_fee - $mem_fee_disc) + ($signup_fee - $signup_fee_disc) == ($amount_temp) || $signup_fee - $signup_fee_disc == ($amount_temp)) {
                            $prorated_1 = "";
                        } else {
                            $prorated_1 = "(Pro-rated)";
                        }
                    } elseif ($payment_details[$init_success_payments]['paytime_type'] == 'F') {
                        if ($mem_fee - $mem_fee_disc == ($amount_temp)) {
                            $prorated_2 = "";
                        } else {
                            $prorated_2 = "(Pro-rated)";
                        }
                    }
                } else if ($membership_structure == 'SE' && $billing_options == 'PP') {
                    if ($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type'] == 'I') {
                        $prorated_1 = "(Pro-rated)";
                    } else if ($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type'] == 'F') {
                        $prorated_2 = "(Pro-rated)";
                    } else if ($prorate_flag_check[$init_success_payments] == 'T' && $payment_details[$init_success_payments]['paytime_type'] == 'R') {
                        $prorated_3 = "(Pro-rated)";
                    }
                }
                if ($membership_structure !== 'SE') {
                    if (!empty($prorated_1)) {
                        $prorated_2 = "";
                    }
                }
                if ($initial_payment > 0 && $payment_details[$init_success_payments]['paytime_type'] == 'I') {
                    $down_pay = $payment_details[$init_success_payments]['amount'] . $pf_str . " paid on " . date("M dS, Y", strtotime($payment_details[$init_success_payments]['date'])) . " (" . $payment_details[$init_success_payments]['cc_name'] . ") " . $prorated_1;
                    $msg1 .= "Payment: $wp_currency_symbol" . $down_pay . "<br>";
                }
                if ($payment_details[$init_success_payments]['paytime_type'] == 'F') {
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    if ($payment_details[$init_success_payments]['payment_status'] == 'S') {
                        $paid_str = "paid on";
                        $pay_type = "(" . $payment_details[$init_success_payments]['cc_name'] . ")";
                    } else {
                        $paid_str = "due";
                        if ($membership_structure === 'SE') {
                            $pay_type = "(" . $payment_details[$init_success_payments]['cc_name'] . ")";
                        } else {
                            $pay_type = "";
                        }
                    }
                    if ($membership_structure !== 'SE' || ($membership_structure == 'SE' && $billing_options == 'PP')) {
                        $msg2 .= "First Payment : $wp_currency_symbol" . $amount . $pf_str . " " . $paid_str . " " . $date . " " . $pay_type . " " . $prorated_2 . " <br>";
                    } else {
                        $msg2 .= "Payment : $wp_currency_symbol" . $amount . $pf_str . " " . $paid_str . " " . $date . " " . $pay_type . " " . $prorated_2 . " <br>";
                    }
                }
                if ($payment_details[$init_success_payments]['paytime_type'] == 'R' && $payment_details[$init_success_payments]['payment_status'] == 'S') {
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "paid on";
                    $pay_type = "(" . $payment_details[$init_success_payments]['cc_name'] . ")";
                    if ($membership_structure !== 'SE') {
                        $msg3 .= "Payment : $wp_currency_symbol" . $amount . $pf_str . " " . $paid_str . " " . $date . " " . $pay_type . " <br>";
                    } else {
                        $msg3 .= "Payment : $wp_currency_symbol" . $amount . $pf_str . " " . $paid_str . " " . $date . " " . $pay_type . " " . $prorated_3 . " <br>";
                    }
                }
                if ($payment_details[$init_success_payments]['paytime_type'] == 'R' && $payment_details[$init_success_payments]['payment_status'] == 'N' && ($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE' ) && $billing_options == 'PP') {
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "due";
                    $pay_type = "(" . $payment_details[$init_success_payments]['cc_name'] . ")";
                    if ($membership_structure !== 'SE') {
                        $msg3 .= "Payment : $wp_currency_symbol" . $amount . $pf_str . " " . $paid_str . " " . $date . " " . $pay_type . " <br>";
                    } else {
                        $msg3 .= "Payment : $wp_currency_symbol" . $amount . $pf_str . " " . $paid_str . " " . $date . " " . $pay_type . " " . $prorated_3 . " <br>";
                    }
                }
            }
            $message .= $msg1 . $msg2 . $msg3;
            $message .= "<br><br><br>";
        }

        $waiver_present = 0;
        $file = '';
        if (!empty(trim($waiver_policies))) {
            $waiver_present = 1;
            $file_name_initialize = 10;
            for ($z = 0; $z < $file_name_initialize; $z++) {
                $dt = time();
                $file = "../../../uploads/" . $dt . "_waiver_policies.html";
                if (file_exists($file)) {
                    sleep(1);
                    $file_name_initialize++;
                } else {
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }

        $sendEmail_status = $this->sendEmailForMembership($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list, $student_cc_email_list);
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            mlog_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            mlog_info($this->json($error_log));
        }
        date_default_timezone_set($curr_time_zone);
    }

    protected function sendEmailForMembership($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list, $student_cc_email_list) {

        if (empty(trim($cmp_name))) {
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try {
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if (filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)) {
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
//            if(filter_var($from, FILTER_VALIDATE_EMAIL)){
//                $mail->setFrom($from, $cmp_name);
//                $mail->AddCC($from);
//            }else{
//                $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if (filter_var(trim($from), FILTER_VALIDATE_EMAIL)) {
                $mail->AddCC($from);
            }
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    if (filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)) {
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if (!empty(trim($student_cc_email_list))) {
                $student_cc_addresses = explode(',', $student_cc_email_list);
                for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                    if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                        $mail->AddCC($student_cc_addresses[$init_1]);
                    }
                }
            }

            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
            $mail->Body = $message;
            if ($waiver_present == 1) {
                //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        } catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if (file_exists($waiver_file)) {
            unlink($waiver_file);
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        mlog_info($this->json($error_log));
        return $response;
    }

    protected function updateDeviceIdinDb($company_id, $student_id, $push_device_id, $device_type) {

        $sql1 = sprintf("UPDATE `student` SET `push_device_id`='%s',`device_type`='%s' WHERE `company_id`='%s' AND `student_id`='%s'", mysqli_real_escape_string($this->db, $push_device_id), mysqli_real_escape_string($this->db, $device_type), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $error_log = array('status' => "Success", "msg" => "push device id updated successfully.", "query" => "$sql1");
            mlog_info($this->json($error_log));
        }
    }

    //dashboard

    public function addMembershipDimensions($company_id, $membership_id, $mem_option_id, $category_title, $option_title, $period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if (!empty($period)) {
            $curr_dt = date("Y-m", strtotime($period));
        } else {
            $curr_dt = date("Y-m");
        }
        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($mem_option_id)) {
            $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                $this->response($this->json($error), 500);
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $selectsql = sprintf("SELECT `active_members` from membership_dimensions where company_id='%s' and `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s' ", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $pre_dt));
                    $resultselectsql = mysqli_query($this->db, $selectsql);
                    if (!$resultselectsql) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql");
                        $this->response($this->json($error), 500);
                    } else {
                        $num_of_rows = mysqli_num_rows($resultselectsql);
                        if ($num_of_rows == 0) {
                            $sqlselect1 = sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and `membership_option_id`='%s' and  `company_id`= '%s'", mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $company_id));
                            $resultsqlselect1 = mysqli_query($this->db, $sqlselect1);
                            $value2 = mysqli_fetch_object($resultsqlselect1);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), mysqli_real_escape_string($this->db, $value2->active));
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                mlog_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                $this->response($this->json($error), 500);
                            }
                        } else {
                            $value = mysqli_fetch_object($resultselectsql);
                            $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`,`active_members`) VALUES('%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title), $value->active_members);
                            $res = mysqli_query($this->db, $insertquery);
                            if (!$res) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                mlog_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                                $this->response($this->json($error), 500);
                            }
                        }
                    }
                }
            }
        }

        $sql2 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows1 = mysqli_num_rows($result2);
            if ($num_of_rows1 == 0) {
                $selectsql2 = sprintf("SELECT `active_members` FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $pre_dt));
                $resultselectsql2 = mysqli_query($this->db, $selectsql2);
                if (!$resultselectsql2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql2");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectsql2");
                    $this->response($this->json($error), 500);
                } else {
                    $num_of_rows = mysqli_num_rows($resultselectsql2);
                    if ($num_of_rows == 0) {
                        $sqlselect2 = sprintf("SELECT count(*) active FROM `membership_registration` WHERE `membership_status`='R' and `membership_id`='%s' and  `company_id`= '%s'", mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $company_id));
                        $resultsqlselect2 = mysqli_query($this->db, $sqlselect2);
                        $value3 = mysqli_fetch_object($resultsqlselect2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $value3->active));
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            $this->response($this->json($error), 500);
                        }
                    } else {
                        $value1 = mysqli_fetch_object($resultselectsql2);
                        $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`,`active_members`) VALUES('%s', '%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title), $value1->active_members);
                        $res2 = mysqli_query($this->db, $insertquery1);
                        if (!$res2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                            $this->response($this->json($error), 500);
                        }
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

//    public function addMembershipDimensions($company_id, $membership_id, $mem_option_id, $category_title, $option_title, $period) {
//        $user_timezone = $this->getUserTimezone($company_id);
//        $curr_time_zone = date_default_timezone_get();
//        $new_timezone = $user_timezone['timezone'];
//        date_default_timezone_set($new_timezone);
//        if(!empty($period)){
//            $curr_dt = date("Y-m", strtotime($period));
//        }else{
//            $curr_dt = date("Y-m");
//        }
//       
//        if(!empty($mem_option_id)){
//            $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $curr_dt));
//            $result = mysqli_query($this->db, $sql1);
//            if (!$result) {
//                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//                mlog_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//                $this->response($this->json($error), 500);
//            } else{
//                $num_of_rows = mysqli_num_rows($result);
//                if ($num_of_rows == 0) {
//                    $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`) VALUES('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title));
//                    $res = mysqli_query($this->db, $insertquery);
//                    if (!$res) {
//                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insertquery");
//                        mlog_info($this->json($error_log));
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insertquery");
//                        $this->response($this->json($error), 500);
//                    }
//                }
//            }
//        }
//
//        $sql2 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $curr_dt));
//        $result2 = mysqli_query($this->db, $sql2);
//        if (!$result2) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//            mlog_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//            $this->response($this->json($error), 500);
//        } else{
//            $num_of_rows1 = mysqli_num_rows($result2);
//            if ($num_of_rows1 == 0) {
//                $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`) VALUES('%s', '%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title));
//                $res2 = mysqli_query($this->db, $insertquery1);
//                if (!$res2) {
//                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insertquery");
//                    mlog_info($this->json($error_log));
//                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insertquery");
//                    $this->response($this->json($error), 500);
//                }
//            }
//        }
//        date_default_timezone_set($curr_time_zone);
//    }

    public function updateMembershipDimensionsNetSales($company_id, $membership_id, $date) {
        
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        } 
        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        $mp_last_upd_date = " DATE(CONVERT_TZ(mp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $last_upd_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";
        if(empty($date)){
            $curr_dt = "($currdate_add)";
        }else{
            $curr_dt = "'$date'";
        }
        $date_inc1= "DATE_FORMAT($curr_dt,'%Y-%m')";
        $date_inc2= "DATE_FORMAT($mp_last_upd_date,'%Y-%m')";
        $date_inc3= "DATE_FORMAT($last_upd_date,'%Y-%m')";
        
        $sql = sprintf("SELECT company_id, membership_id, membership_option_id, mp_ud, sum(amount) amount, payment_date FROM (

                  SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount) amount, 
                  mp.payment_date, mp.membership_payment_id FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND (mp.payment_status='S' AND mp.checkout_status='released' || mp.payment_status IN ('MF') || mp.payment_status='M' AND mp.`credit_method`!='MC')
                  AND %s = %s 
                  
                  UNION
                  
                  SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, concat('-',if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
                  mp.payment_date, mp.membership_payment_id FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  (mp.`payment_status`='R' 
                  AND IF(mr.registration_from='S', mp.payment_intent_id NOT IN (SELECT payment_intent_id FROM membership_payment WHERE payment_status='FR' AND %s = %s),
                  mp.checkout_id NOT IN (SELECT checkout_id FROM membership_payment WHERE payment_status='FR' AND %s = %s)) || mp.`payment_status`='MR')
                  AND %s = %s
                  )t1
                  GROUP BY t1.`membership_id`, t1.`membership_option_id`"

                 , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id),
                 $date_inc2, $date_inc1, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id),
                 $date_inc3, $date_inc1, $date_inc3, $date_inc1, $date_inc2, $date_inc1);
 
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 200);
        } else {
            $category = $membership_id_arr = $membership_options_id_arr = [];
            $number_rows = mysqli_num_rows($result);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $temp = $temp2 = [];
                    $temp['category_id'] = $temp2['category_id'] = $membership_id = $row['membership_id'];
                    $temp2['option_id'] = $membership_option_id = $row['membership_option_id'];
                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
                    $temp2['amount'] = $amount = $row['amount'];
                    $temp2['mp_ud'] = $row['mp_ud'];

                    if (!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)) {
                        $membership_id_arr[] = $membership_id;
                        $category[] = $temp;
                    }
                    $option[] = $temp2;
                }

                for ($i = 0; $i < count($option); $i++) {
                    $date_init = 0;
                   
                    $date_str = "%Y-%m";
                    $curr_dt = date("Y-m-d");
                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
//                     
                    $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')",
                            mysqli_real_escape_string($this->db, $company_id), $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);
                    $res1 = mysqli_query($this->db, $sql1);
                    if (!$res1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        mlog_info($this->json($error_log));
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                        $this->response($this->json($error), 200);
                    } else {
//                           

                        $num_of_rows = mysqli_num_rows($res1);
                        if ($num_of_rows == 1) {

                            $sql2 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`='%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='%s'",
                                    $option[$i]['amount'], mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id']);
                            $res2 = mysqli_query($this->db, $sql2);
                            if (!$res2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                mlog_info($this->json($error_log));
//                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                $this->response($this->json($error), 200);
                            } else {
                                if($i==0){
                                    $sales_value = "'%s'";
                                }else{
                                    $sales_value = "`sales_amount`+'%s'";
                                }
                                $sql3 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='0'",
                                        mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str, mysqli_real_escape_string($this->db, $membership_id));
                                $res3 = mysqli_query($this->db, $sql3);
                                if (!$res3) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                                    mlog_info($this->json($error_log));
//                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                    $this->response($this->json($error), 200);
                                }
                            }
                        }
                    }
                }
            } else {
                 $sql2 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=0 WHERE `company_id`='%s' AND `period`=%s AND `category_id`='%s'", mysqli_real_escape_string($this->db, $company_id),$date_inc1, $membership_id);
                $res2 = mysqli_query($this->db, $sql2);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    mlog_info($this->json($error_log));
//                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                $this->response($this->json($error), 200);
                }
                $error = array('status' => "Failed", "msg" => "No records.", "query" => "$sql");
                mlog_info($this->json($error));
//                $this->response($this->json($error), 204);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }


//    public function updateMembershipDimensionsNetSales($company_id, $membership_id, $date) {
//        $user_timezone = $this->getUserTimezone($company_id);
//        $curr_time_zone = date_default_timezone_get();
//        $new_timezone = $user_timezone['timezone'];
//        date_default_timezone_set($new_timezone);
//        $host = $_SERVER['HTTP_HOST'];
//        if (strpos($host, 'mystudio.academy') !== false) {
//            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
//        } else {
//            $tzadd_add = "'" . $curr_time_zone . "'";
//        }
//        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
//        $mp_last_upd_date = " DATE(CONVERT_TZ(mp.last_updt_dt,$tzadd_add,'$new_timezone'))";
//        if (empty($date)) {
//            $curr_dt = "($currdate_add)";
//        } else {
//            $curr_dt = "'$date'";
//        }
//        $sql = sprintf("SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, sum(if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
//                  mp.payment_date FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
//                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
//                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  MONTH( $mp_last_upd_date) = MONTH($curr_dt)
//                  AND YEAR($mp_last_upd_date) = YEAR($curr_dt) AND mp.payment_status='S' GROUP BY mr.`membership_id`, mr.`membership_option_id`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
//
//        $result = mysqli_query($this->db, $sql);
//        if (!$result) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
//            mlog_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 500);
//        } else {
//            $category = $membership_id_arr = $membership_options_id_arr = [];
//            $number_rows = mysqli_num_rows($result);
//            if ($number_rows > 0) {
//                while ($row = mysqli_fetch_assoc($result)) {
//                    $temp = $temp2 = [];
//                    $temp['category_id'] = $temp2['category_id'] = $membership_id = $row['membership_id'];
//                    $temp2['option_id'] = $membership_option_id = $row['membership_option_id'];
//                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
//                    $temp2['amount'] = $amount = $row['amount'];
//                    $temp2['mp_ud'] = $row['mp_ud'];
//
//                    if (!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)) {
//                        $membership_id_arr[] = $membership_id;
//                        $category[] = $temp;
//                    }
//                    $option[] = $temp2;
//                }
//
//                for ($i = 0; $i < count($option); $i++) {
//                    $date_init = 0;
//
//                    $date_str = "%Y-%m";
//                    $curr_dt = date("Y-m-d");
//                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
////                     
//                    $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')", mysqli_real_escape_string($this->db, $company_id), $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);
//                    $res1 = mysqli_query($this->db, $sql1);
//                    if (!$res1) {
//                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//                        mlog_info($this->json($error_log));
//                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                        $this->response($this->json($error), 500);
//                    } else {
////                           
//
//                        $num_of_rows = mysqli_num_rows($res1);
//                        if ($num_of_rows == 1) {
//
//                            $sql2 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`='%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='%s'", $option[$i]['amount'], mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id']);
//                            $res2 = mysqli_query($this->db, $sql2);
//                            if (!$res2) {
//                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                                mlog_info($this->json($error_log));
//                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                $this->response($this->json($error), 500);
//                            } else {
//                                if ($i == 0) {
//                                    $sales_value = "'%s'";
//                                } else {
//                                    $sales_value = "`sales_amount`+'%s'";
//                                }
//                                $sql3 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='0'", mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str, mysqli_real_escape_string($this->db, $membership_id));
//                                $res3 = mysqli_query($this->db, $sql3);
//                                if (!$res3) {
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
//                                    mlog_info($this->json($error_log));
//                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                    $this->response($this->json($error), 500);
//                                }
//                            }
//                        }
//                    }
//                }
//            } else {
//                $error = array('status' => "Failed", "msg" => "No records.", "query" => "$sql");
//                $this->response($this->json($error), 204);
//            }
//        }
//        date_default_timezone_set($curr_time_zone);
//    }

    public function updateMembershipDimensionsMembers($company_id, $membership_id, $membership_status, $membership_option_id) {
        $flag = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        $date_str = "%Y-%m";
        $every_month_dt = date("Y-m-d");

        $selectquery = sprintf("SELECT * FROM `membership_registration` WHERE DATE_FORMAT(`hold_date`,'%s')=DATE_FORMAT(`resume_date`,'%s') and  `company_id`='%s' and `membership_id`='%s' and `membership_option_id`='%s'", $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
        $res = mysqli_query($this->db, $selectquery);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows == 1) {
                $flag = 1;
            }
        }
        if ($membership_status == 'R') {
            if ($flag == 1) {
                $mem_string = "`active_members`=`active_members`+1, `reg_hold`=`reg_hold`-1";
            } elseif ($flag == 0) {
                $mem_string = "`active_members`=`active_members`+1";
            }
        } elseif ($membership_status == 'P') {
            $mem_string = "`active_members`=`active_members`-1, `reg_hold`=`reg_hold`+1";
        } elseif ($membership_status == 'C') {
            $mem_string = "`active_members`=`active_members`-1, `reg_cancelled`=`reg_cancelled`+1";
        } elseif ($membership_status == 'A') {
            $mem_string = "`active_members`=`active_members`+1,`reg_new`=`reg_new`+1";
        }

        if (!empty(trim($mem_string))) {
            $query1 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`=0", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), $every_month_dt, $date_str);
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
        }
        if (!empty(trim($mem_string))) {
            $query2 = sprintf("UPDATE `membership_dimensions` SET $mem_string WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') and `option_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), $every_month_dt, $date_str, mysqli_real_escape_string($this->db, $membership_option_id));
            $result2 = mysqli_query($this->db, $query2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }

    public function checkQuantityDuringCheckout($company_id, $event_id, $event_array, $quantity, $event_type) {
        $event_capacity = $registration_count = $remaining_qty = $child_qty = 0;
        if ($event_type == "M") {
            $capacity_query = sprintf("SELECT `event_id`,`event_capacity`,`registrations_count`,`event_title` FROM `event` WHERE `parent_id`='%s' and `company_id`='%s' ", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
        } else if ($event_type == "S") {
            $capacity_query = sprintf("SELECT `event_id`,`event_capacity`,`registrations_count`,`event_title` FROM `event` WHERE `event_id`='%s' and `company_id`='%s' ", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $company_id));
        }
        $cap_result = mysqli_query($this->db, $capacity_query);
        if (!$cap_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$capacity_query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            if (mysqli_num_rows($cap_result) > 0) {
                while ($row_1 = mysqli_fetch_assoc($cap_result)) {
                    $output_1[] = $row_1;
                }
                if ($event_type == "S") {
                    $event_capacity = $output_1[0]['event_capacity'];
                    $registration_count = $output_1[0]["registrations_count"];
                    if (isset($event_capacity) && $event_capacity > 0) {
                        $remaining_qty = $event_capacity - $registration_count;
                        if ($remaining_qty == 0) {
                            $error = array('status' => "Failed", "msg" => "Event Sold Out.");
                            $this->response($this->json($error), 500);
                        }
                        if ($remaining_qty < $quantity) {
                            $error = array('status' => "Failed", "msg" => "Only $remaining_qty Capacity is Available  .");
                            $this->response($this->json($error), 500);
                        }
                    } elseif (isset($event_capacity) && $event_capacity == 0) {
                        $error = array('status' => "Failed", "msg" => "Event Sold Out.");
                        $this->response($this->json($error), 500);
                    }
                } elseif ($event_type == "M") {
                    $failure_flag = 0;
                    $error_msg = '';
                    for ($idx = 0; $idx < count($event_array); $idx++) {
                        $event_capacity = $registration_count = $remaining_qty = $child_qty = 0;
                        $event_title = '';
                        $child_qty = $event_array[$idx]["event_quantity"];
                        for ($idx_1 = 0; $idx_1 < count($output_1); $idx_1++) {
                            if ($output_1[$idx_1]['event_id'] == $event_array[$idx]['event_id']) {
                                $event_capacity = $output_1[$idx_1]['event_capacity'];
                                $registration_count = $output_1[$idx_1]["registrations_count"];
                                $event_title = $output_1[$idx_1]["event_title"];
                                if (isset($event_capacity) && $event_capacity > 0) {
                                    $remaining_qty = $event_capacity - $registration_count;
                                    if ($remaining_qty == 0) {
                                        $failure_flag = 1;
                                        $error_msg .= "Event $event_title Sold Out. ";
                                    }
                                    if ($remaining_qty < $child_qty) {
                                        $failure_flag = 1;
                                        $error_msg .= "Only $remaining_qty Capacity is available for $event_title. ";
                                    }
                                } elseif (isset($event_capacity) && $event_capacity == 0) {
                                    $error = array('status' => "Failed", "msg" => "Event $event_title Sold Out.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }
                    if ($failure_flag == 1) {
                        $error = array('status' => "Failed", "msg" => $error_msg);
                        $this->response($this->json($error), 500);
                    }
                }
            }
        }
    }

    private function getRealIpAddr() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet        
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    private function sendSnsforfailedcheckout($sub, $msg) {

        $topicarn = 'arn:aws:sns:us-east-1:241334827363:mystudio';

        $subject = $sub;
        //                $subject = $sns_sub . " -transaction failed";
        $message = $msg;

        $credentials = new Aws\Credentials\Credentials('AKIAIGUWYNJGAHDPJGIA', 'w4oKbP0UhJZr1W0hGuULhkdEoxEHY2YxEq/6uycT');
        $sns = SnsClient::factory([
                    'credentials' => $credentials,
                    'region' => 'us-east-1',
                    'version' => 'latest'
        ]);
        //                log_info("object cretaed");
        $result1 = $sns->publish(array(
            'TopicArn' => $topicarn,
            // Message is required
            'Message' => $message,
            'Subject' => $subject,
            //'MessageStructure' => 'string',
            'MessageAttributes' => array(
                // Associative array of custom 'String' key names
                'String' => array(
                    // DataType is required
                    //            'DataType' => '',
                    'DataType' => 'String',
                    'StringValue' => '200',
                ),
            // ... repeated
            ),
        ));
    }

    // NEW FUNCTIONS FOR REGISTRATION AND LOGIN 
    public function checkStudentUsernameAvailability($student_username) {
        $username_query = sprintf("SELECT * FROM `student` WHERE `student_username` = '%s'", mysqli_real_escape_string($this->db, $student_username));
        $username_result = mysqli_query($this->db, $username_query);
        if (!$username_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$username_query");
            mlog_info($this->json($error_log));
//            $error = array('status' => "Failed", 'msg'=>'Internal server error');
            $error = array('status' => "Failed", 'msg' => "$username_query");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($username_result);
            if ($num_rows > 0) {
                $out = array('status' => "Failed", 'msg' => 'Username is already taken');
                $this->response($this->json($out), 401);
            } else {
                $out = array('status' => "Success", 'msg' => 'Username not taken');
                $this->response($this->json($out), 200);
            }
        }
    }

    public function checkStudentCompanyCode($company_code, $app_id, $device_type) {
        $studiocode_query = sprintf("SELECT * FROM `company` WHERE `company_code`='%s'", mysqli_real_escape_string($this->db, $company_code));
        $studiocode_result = mysqli_query($this->db, $studiocode_query);

        if (!$studiocode_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$studiocode_query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", 'msg' => 'Internal server error');
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($studiocode_result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($studiocode_result);
                $company_id = $row['company_id'];
                $this->getStudioSubscriptionStatus($company_id);
                $out = array('status' => "Success", 'msg' => 'Studio code exists', 'company_id' => $company_id);
                $this->response($this->json($out), 200);
            } else {
                $out = array('status' => "Failed", 'msg' => 'Invalid studio code');
                $this->response($this->json($out), 401);
            }
        }
    }

    public function addStudentRegistrationDetails($company_id, $student_firstname, $student_lastname, $student_username, $student_password, $student_email, $student_mobile, $push_device_id, $device_type, $app_id, $student_reg_id) {
        $student_id = $query_str2 = '';
        if (!empty($student_reg_id)) {
            $query_str = "`student_id` = '" . mysqli_real_escape_string($this->db, $student_reg_id) . "'";
            $query_str2 = "`student_email` = '" . mysqli_real_escape_string($this->db, $student_email) . "', ";
        } else {
            $query_str = "`student_email` = '" . mysqli_real_escape_string($this->db, $student_email) . "'";
        }
        $student_list_query = sprintf("SELECT student_id, IFNULL(student_username, '') student_username FROM `student` WHERE `company_id` = '%s' AND $query_str", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $student_list_query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$student_list_query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", 'msg' => 'Internal server error');
            $this->response($this->json($error), 500);
        } else {
            $result_rows = mysqli_num_rows($result);
            if ($result_rows == 0) {
                // NO STUDENT EXISTS WITH GIVEN EMAIL
                $insertquery = sprintf("INSERT INTO  `student`(`company_id`,`student_username`, `student_password`, `student_email`, `student_mobile`, `student_name`, `student_lastname`, `push_device_id`, `device_type`, `app_id`, `registration_status`, `first_login_dt`, `last_login_dt`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','S',NOW(),NOW())", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_username), mysqli_real_escape_string($this->db, md5($student_password)), mysqli_real_escape_string($this->db, $student_email), mysqli_real_escape_string($this->db, $student_mobile), mysqli_real_escape_string($this->db, $student_firstname), mysqli_real_escape_string($this->db, $student_lastname), mysqli_real_escape_string($this->db, $push_device_id), mysqli_real_escape_string($this->db, $device_type), mysqli_real_escape_string($this->db, $app_id));
                $result1 = mysqli_query($this->db, $insertquery);
                if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insertquery");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", 'msg' => 'Internal server error');
                    $this->response($this->json($error), 500);
                } else {
                    $student_id = mysqli_insert_id($this->db);
                }
            } else {
                $row = mysqli_fetch_assoc($result);
                $student_id = $row['student_id'];
                $username = $row['student_username'];
                if (empty(trim($username))) {
                    // STUDENT EXISTS WITH GIVEN EMAIL WITHOUT USERNAME
                    $updatequery = sprintf("UPDATE `student` SET `student_username` = '%s', `student_password` = '%s', `student_name` = '%s', `student_lastname` = '%s', $query_str2 `student_mobile` = '%s', `deleted_flag`='N', `registration_status`='S' WHERE `company_id` = '%s' AND `student_email` = '%s'", mysqli_real_escape_string($this->db, $student_username), mysqli_real_escape_string($this->db, md5($student_password)), mysqli_real_escape_string($this->db, $student_firstname), mysqli_real_escape_string($this->db, $student_lastname), mysqli_real_escape_string($this->db, $student_mobile), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_email));
                    $result2 = mysqli_query($this->db, $updatequery);
                    if (!$result2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatequery");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", 'msg' => 'Internal server error');
                        $this->response($this->json($error), 500);
                    }
                } else {
                    // STUDENT EXISTS WITH GIVEN EMAIL WITH AN USERNAME
                    $error = array('status' => "Failed", 'msg' => 'Email already registered for the studio. Try Forgot password to recover the account.');
                    $this->response($this->json($error), 401);
                }
            }


//            $verification_code = mt_rand(100000, 999999);  // GENERATING RANDOM VERIFICATION CODE
//            // INSERT RECORD TO REGISTER_VERIFICATION_TABLE
//            $insertquery1 = sprintf("INSERT INTO `register_verification` (`register_studio_id`,`register_type`,`registered_type_id`, `registration_code`) VALUES ('%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $user_type), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $verification_code));
//            $result3 = mysqli_query($this->db, $insertquery1);
//            if (!$result3) {
//                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insertquery1");
//                mlog_info($this->json($error_log));
//                $error = array('status' => "Failed", 'msg' => 'Internal server error');
//                $this->response($this->json($error), 500);
//            } else {
//                $verification_id = mysqli_insert_id($this->db);
//            }
//
//            // GETTING COMPANY DETAILS TO SEND VERIFICATION LINK TO EMAIL
//            $company_details_query = sprintf("SELECT c.company_name, c.upgrade_status, rv.last_updated_date FROM `company` c LEFT JOIN `register_verification` rv 
//                    ON c.`company_id`= rv.`register_studio_id` WHERE c.`company_id` = '%s' AND `register_type`='S' AND `registered_type_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
//            $companyresult = mysqli_query($this->db, $company_details_query);
//            if (!$companyresult) {
//                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$company_details_query");
//                mlog_info($this->json($error_log));
//                $error = array('status' => "Failed", 'msg' => 'Internal server error');
//                $this->response($this->json($error), 500);
//            } else {
//                $num_rows = mysqli_num_rows($companyresult);
//                if ($num_rows > 0) {
//                    $company_row = mysqli_fetch_assoc($companyresult);
////                    if ($company_row['upgrade_status'] == 'W') {
//                        $company_name = $company_row['company_name'];
////                    } else {
////                        $company_name = "MyStudio";
////                    }
//                    $this->sendActivationLink($student_email, $company_name, $verification_code, $verification_id, $user_type, $student_username);
//                } else {
//                    $error_log = array('status' => "Failed", "msg" => "No company details exist for student.", "query" => "$company_details_query");
//                    mlog_info($this->json($error_log));
//                    $error = array('status' => "Failed", "msg" => "No company details exist for student.");
//                    $this->response($this->json($error), 401); // If no records "No Content" status
//                }
//            }
//
//            // SENDING REGISTRATION STATUS TO RETRIEVE THE ACTIVATION STATUS
//            $query = sprintf("SELECT `registration_status` FROM `student` WHERE `company_id` = '%s' AND `student_id` = '%s'"
//                    , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
//            $result = mysqli_query($this->db, $query);
//            if (!$result) {
//                $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query");
//                mlog_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "Internal server Error.");
//                $this->response($this->json($error), 500); // If no records "No Content" status
//            } else {
//                $num_rows = mysqli_num_rows($result);
//                if ($num_rows > 0) {
//                    $row = mysqli_fetch_assoc($result);
//                    $registration_status = $row['registration_status'];
//                }
//            }
//            $student_details['register_verification_id'] = $verification_id;
            $student_details['student_id'] = $student_id;
            $student_details['company_id'] = $company_id;
            $company_details = $this->getcompanydetails($company_id, $student_id, 1);
            if ($company_details['msg'] == 'Success' && ($company_details['studio_expiry_level'] == 'L2' || $company_details['upgrade_status'] == 'F')) {
                $msg = array('status' => "Failed", 'msg' => 'Please contact the business operator to activate the app.', "company_id" => $company_id, "company_details" => $company_details, "studio_email" => $company_details['email_id'], "upgrade_status" => $company_details['upgrade_status'], "studio_expiry_level" => $company_details['studio_expiry_level']);
            } else {
                $msg = array('status' => "Success", 'msg' => 'Member app activated successfully.', 'student_details' => $student_details, 'company_details' => $company_details);
            }
//            $student_details['registration_status'] = $registration_status;
            $this->responseWithWepay($this->json($msg), 200);
        }
    }

    public function sendActivationLink($student_email, $company_name, $verification_code, $verification_id, $user_type, $student_username) {
        $message = '';
        $code = md5($verification_code);
        $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $curr_loc = implode('/', explode('/', $curr_loc, -3));
        $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/Api/MemberappActivation.html?token=$code&i=$verification_id&t=$user_type";
        $message .= "Hello $student_username,<br><br>";
        $message .= "<a href='$activation_link'>" . "Click here to activate your account." . "</a><br>";
        $message .= "(or) Use the following verification code to activate your account: ";
        $message .= "<b>$verification_code</b><br>";
        $sendEmail_status = $this->sendEmail($student_email, '', "App Verification Code", $message, $company_name, '', true); //last two parameters are empty for company_name & reply-to addr(only for Whitelabel and notifications)
        if ($sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $student_email, "Email Message" => $sendEmail_status['mail_status']);
            mlog_info($this->json($error_log));
            $out = array('status' => "Success", 'msg' => "Email sent successfully.");
            // If success everythig is good send header as "OK" and user details
//            $this->response($this->json($out), 200);
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $student_email, "Email Message" => $sendEmail_status['mail_status']);
            mlog_info($this->json($error_log));
            $out = array('status' => "Failed", 'msg' => $sendEmail_status['mail_status']);
            // If success everythig is good send header as "OK" and user details
//            $this->response($this->json($out), 500);
        }
    }

    public function activateStudentAccountVerification($register_verification_id, $user_type, $registration_code, $activated_by) {
        $verifyquery = sprintf("SELECT *, TIMESTAMPDIFF(MINUTE, `last_updated_date`, NOW()) as time_diff FROM `register_verification` WHERE `register_verification_id` = '%s' AND `register_type`='%s'", mysqli_real_escape_string($this->db, $register_verification_id), mysqli_real_escape_string($this->db, $user_type));
        $verifyqueryresult = mysqli_query($this->db, $verifyquery);
        if (!$verifyqueryresult) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$verifyquery");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", 'msg' => 'Internal server error');
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($verifyqueryresult);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($verifyqueryresult);
                $registration_code_from_DB = $row['registration_code'];
                $studio_id = $row['register_studio_id'];
                $registered_type_id = $row['registered_type_id'];   // RESPECTIVE ID OF STUDENT/ OPERATOR
                $difference_minutes = $row['time_diff'];

                if ($difference_minutes < 15) {
                    if ($activated_by == 'url') {
                        $code = md5($registration_code_from_DB);
                    } else {
                        $code = $registration_code_from_DB;
                    }
                    if ($code == $registration_code) {
                        // UPDATE DATABASE RECORDS
                        $updatequery = sprintf("UPDATE `student` SET `registration_status` = 'S', `first_login_dt`= NOW(), `last_login_dt`=NOW() WHERE `student_id` = '%s'", mysqli_real_escape_string($this->db, $registered_type_id));
                        $updateresult = mysqli_query($this->db, $updatequery);
                        if (!$updateresult) {
                            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$updatequery");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal server Error.");
                            $this->response($this->json($error), 500); // If no records "No Content" status
                        } else {
                            $delete_query = sprintf("DELETE FROM `register_verification` WHERE `register_verification_id` = '%s' AND `register_type`='%s'", mysqli_real_escape_string($this->db, $register_verification_id), mysqli_real_escape_string($this->db, $user_type));
                            $delete_result = mysqli_query($this->db, $delete_query);
                            if (!$delete_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$delete_query");
                                mlog_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal server Error.");
                                $this->response($this->json($error), 500); // If no records "No Content" status
                            }
                            $company_details = $this->getcompanydetails($studio_id, $registered_type_id, 1);
                            $out = array('status' => "Success", 'msg' => 'Member App activated successfully.', "company_details" => $company_details);
                            $this->response($this->json($out), 200);
                        }
                    } else {
                        $error = array('status' => "Failed", "msg" => "Invalid Verification code.");
                        $this->response($this->json($error), 401); // If no records "No Content" status
                    }
                } else {
                    $error = array('status' => "Failed", "msg" => "Verification code expired.");
                    $this->response($this->json($error), 401); // If no records "No Content" status
                }
            } else {
                $error = array('status' => "Failed", "msg" => "No verification details exists.");
                $this->response($this->json($error), 401); // If no records "No Content" status
            }
        }
    }

    public function resendVerficationDetailsEmail($company_id, $student_id, $register_verification_id, $user_type, $call_back) {//$call_back --> 0 - echo & exit, 1 - return
        $emaildetailsquery = sprintf("SELECT company_name, upgrade_status, s.student_username, s.student_email FROM `company` c LEFT JOIN `student` s 
                    ON c.`company_id`= s.`company_id` WHERE c.`company_id` = '%s' AND `student_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
        $emaildetailsresult = mysqli_query($this->db, $emaildetailsquery);
        if (!$emaildetailsresult) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$emaildetailsquery");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal server Error.");
            if ($call_back == 0) {
                $this->response($this->json($error), 500); // If no records "No Content" status
            } else {
                return $error;
            }
        } else {
            $num_rows = mysqli_num_rows($emaildetailsresult);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($emaildetailsresult);
                $student_email = $row['student_email'];
                $student_username = $row['student_username'];
//                if ($row['upgrade_status'] == 'W') {
                $company_name = $row['company_name'];
//                } else {
//                    $company_name = "MyStudio";
//                }
                $verification_code = mt_rand(100000, 999999);  // GENERATING RANDOM VERIFICATION CODE
                $updatequery = sprintf("UPDATE `register_verification` SET `registration_code` = '%s' WHERE `register_verification_id` = '%s' AND `registered_type_id` = '%s'", mysqli_real_escape_string($this->db, $verification_code), mysqli_real_escape_string($this->db, $register_verification_id), mysqli_real_escape_string($this->db, $student_id));
                $updateresult = mysqli_query($this->db, $updatequery);
                if (!$updateresult) {
                    $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$updatequery");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal server Error.");
                    if ($call_back == 0) {
                        $this->response($this->json($error), 500); // If no records "No Content" status
                    } else {
                        return $error;
                    }
                } else {
                    $verification_id = $register_verification_id;
                    $this->sendActivationLink($student_email, $company_name, $verification_code, $verification_id, $user_type, $student_username);
                }
                $msg = array('status' => "Success", 'msg' => 'Verification code resent successfully.');
                if ($call_back == 0) {
                    $this->response($this->json($msg), 200); // If no records "No Content" status
                } else {
                    return $msg;
                }
            } else {
                $error = array('status' => "Failed", "msg" => "No Email details exists.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 401); // If no records "No Content" status
                } else {
                    return $error;
                }
            }
        }
    }

    public function checkStudentLogin($student_username, $student_password, $push_device_id, $device_type, $app_id, $app_type, $company_code) {
        $query_string = "";
        if ($app_type == 'ML') {
            $query_string = "and c.`company_code`='" . mysqli_real_escape_string($this->db, $company_code) . "'";
        }
        $studentdetailsquery = sprintf("SELECT s.*, r.`register_verification_id` FROM `student` s  
                    LEFT JOIN `register_verification` r ON r.`registered_type_id`=s.`student_id` AND r.`register_type`='S' 
                    LEFT JOIN `company` c ON c.`company_id` = s.`company_id`
                    WHERE `student_username` = '%s' $query_string", mysqli_real_escape_string($this->db, $student_username));
        $studentdetailsqueryresult = mysqli_query($this->db, $studentdetailsquery);
        if (!$studentdetailsqueryresult) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$studentdetailsquery");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal server Error.");
            $this->response($this->json($error), 500); // If no records "No Content" status
        } else {
            $num_rows = mysqli_num_rows($studentdetailsqueryresult);
            if ($num_rows > 0) {
                $studentRecord = mysqli_fetch_assoc($studentdetailsqueryresult);
//                if ($studentRecord['deleted_flag'] == 'N') {
                if (md5($student_password) == $studentRecord['student_password']) {
                    if ($studentRecord['registration_status'] == 'N') {
                        unset($studentRecord['student_password']);
                        $error = array('status' => "Failed", "msg" => "Member Account not yet activated.", "activtion_details" => $studentRecord);
                        $this->response($this->json($error), 401);
                    }
                    $updatelogindetailsquery = sprintf("UPDATE `student` SET `push_device_id` = '%s', `device_type` = '%s', `app_id`  = '%s', `first_login_dt` = IF(`first_login_dt` IS NULL OR `first_login_dt`='0000-00-00 00:00:00', NOW(), `first_login_dt`), `last_login_dt`= NOW(), `deleted_flag`='N' WHERE `student_username` = '%s'", mysqli_real_escape_string($this->db, $push_device_id), mysqli_real_escape_string($this->db, $device_type), mysqli_real_escape_string($this->db, $app_id), mysqli_real_escape_string($this->db, $student_username));
                    $updatelogindetailsresult = mysqli_query($this->db, $updatelogindetailsquery);
                    if (!$updatelogindetailsresult) {
                        $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$updatelogindetailsquery");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $studentDetails['company_id'] = $studentRecord ['company_id'];
                        $studentDetails['student_id'] = $studentRecord ['student_id'];
                        $company_details = $this->getcompanydetails($studentDetails['company_id'], $studentDetails['student_id'], 1);
                        if ($company_details['msg'] == 'Success' && ($company_details['studio_expiry_level'] == 'L2' || $company_details['upgrade_status'] == 'F')) {
                            $msg = array('status' => "Failed", 'msg' => 'Please contact the business operator to activate the app.', "company_id" => $studentDetails['company_id'], "company_details" => $company_details, "studio_email" => $company_details['email_id'], "upgrade_status" => $company_details['upgrade_status'], "studio_expiry_level" => $company_details['studio_expiry_level']);
                        } else {
                            $msg = array('status' => "Success", 'msg' => 'Login success', "company_details" => $company_details);
                        }
                        $this->response($this->json($msg), 200);
                    }
                } else {
                    $error = array('status' => "Failed", "msg" => "We could not find that account. Incorrect username and / or password.");
                    $this->response($this->json($error), 401);
                }
//                } else {
//                    // STUDENT DELETED WITH GIVEN EMAIL 
//                    $error = array('status' => "Failed", 'msg' => 'Student no longer exists');
//                    $this->response($this->json($error), 401);
//                }
            } else {
                $error = array('status' => "Failed", "msg" => "Student details does not exists.");
                $this->response($this->json($error), 401);
            }
        }
    }

    public function recoverLoginDetails($student_email, $company_code, $recovery_type) {
        $query = sprintf("SELECT s.student_username, s.student_id, s.company_id, s.`registration_status`, c.company_code, c.company_name, c.upgrade_status  FROM `student` s LEFT JOIN  `company` c ON c.company_id = s.company_id WHERE `student_email` = '%s'", mysqli_real_escape_string($this->db, $student_email));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal server Error.");
            $this->response($this->json($error), 500); // If no records "No Content" status
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $check = 0;
                while ($studentcompanyrecord = mysqli_fetch_assoc($result)) {
                    $check++;
                    if (!strcasecmp($company_code, $studentcompanyrecord['company_code'])) {  // TO IGNORE CASE SENSITIVIE FOR COMPANY CODES
                        if ($studentcompanyrecord['registration_status'] == 'S') { // CHECKING THE STUDENT ACTIVATED HIS ACCOUNT / NOT S/N
//                        if ($studentcompanyrecord['upgrade_status'] == 'W') {
                            $company_name = $studentcompanyrecord['company_name'];
//                        } else {
//                            $company_name = "MyStudio";
//                        }
                            $student_username = $studentcompanyrecord['student_username'];
                            $company_id = $studentcompanyrecord['company_id'];
                            // CHECKING THE RECOVERY TYPE
                            if ($recovery_type == 'username') {
                                // SEND USERNAME TO THE STUDENT EMAIL 
                                $message = '';
                                $message .= "Hello,<br><br>";
                                $message .= "Your login details for the member app.<br>";
                                $message .= "Username : <b>$student_username</b><br>";
                                $sendEmail_status = $this->sendEmail($student_email, '', "$company_name - Forgot username", $message, $company_name, '', true); //last two parameters are empty for company_name & reply-to addr(only for Whitelabel and notifications)
                                $msg = array('status' => "Success", 'msg' => 'Username has been Emailed');
                                $this->response($this->json($msg), 200);
                            } else if ($recovery_type == 'password') {
                                // UPDATE PASSWORD RESET TIMER IN DB
                                $time = time();
                                $reset_timer_start = gmdate("Y-m-d H-i-s", $time);
                                $query = sprintf("UPDATE `student` SET `password_last_updated` = '%s' WHERE `student_username` = '%s'", mysqli_real_escape_string($this->db, $reset_timer_start), mysqli_real_escape_string($this->db, $student_username));
                                $result = mysqli_query($this->db, $query);
                                if (!$result) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query");
                                    mlog_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal server Error.");
                                    $this->response($this->json($error), 500); // If no records "No Content" status
                                } else {
                                    $query = sprintf("SELECT `password_last_updated` FROM `student` WHERE `student_username` = '%s'", mysqli_real_escape_string($this->db, $student_username));
                                    $result = mysqli_query($this->db, $query);
                                    if (!$result) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query");
                                        mlog_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal server Error.");
                                        $this->response($this->json($error), 500); // If no records "No Content" status
                                    } else {
                                        $num_rows = mysqli_num_rows($result);
                                        if ($num_rows > 0) {
                                            $studentrecord = mysqli_fetch_assoc($result);
                                            $reset_timer_start = $studentrecord['password_last_updated'];
                                            // SEND PASSWORD RESET LINK TO THE STUDENT EMAIL 
                                            $reset_timer_startenc = md5($reset_timer_start); // ENCRYPTING FOR LINK
                                            $student_id = $studentcompanyrecord['student_id'];
                                            $enc_studentemail = md5($student_email);
                                            $enc_studentusername = md5($student_username);
                                            $message = '';
                                            $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                                            $curr_loc = implode('/', explode('/', $curr_loc, -3));
                                            $reset_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/Api/MemberappResetPassword.html?token=$student_id&e=$enc_studentemail&u=$enc_studentusername&t=$reset_timer_startenc&c=$company_id";
                                            $message .= "Hello,<br><br>";
                                            $message .= "Please use the below link to change the password for your member app.<br><br>";
                                            $message .= "<a href='$reset_link'>" . "Click here" . "</a>";
                                            $sendEmail_status = $this->sendEmail($student_email, '', "$company_name - Forgot password", $message, $company_name, '', true); //last two parameters are empty for company_name & reply-to addr(only for Whitelabel and notifications)
                                            $msg = array('status' => "Success", 'msg' => 'Password reset link has been Emailed');
                                            $this->response($this->json($msg), 200);
                                        } else {
                                            $error = array('status' => "Failed", "msg" => "Last updated password not exists");
                                            $this->response($this->json($error), 401);
                                        }
                                    }
                                }
                            }
                        } else {
                            $error = array('status' => "Failed", "msg" => "Memberapp not yet activated");
                            $this->response($this->json($error), 401);
                        }
                    } else {
                        if ($check == $num_rows) {
                            $error = array('status' => "Failed", "msg" => "We could not find that account. Incorrect email and / or studio code");
                            $this->response($this->json($error), 401);
                        }
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Student details does not exist.");
                $this->response($this->json($error), 401);
            }
        }
    }

    public function resetMemberappPassword($student_id, $student_username, $student_email, $student_password, $reset_password_timer) {
        $query = sprintf("SELECT `student_username`, `student_email`, `password_last_updated` FROM `student` WHERE `student_id` = '%s'", mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal server Error.");
            $this->response($this->json($error), 500); // If no records "No Content" status
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $studentrecord = mysqli_fetch_assoc($result);
                // REFERRING THE TIME ON LINK SENT & RETREIVED
                if ($reset_password_timer == md5($studentrecord['password_last_updated'])) {
                    $student_emailenc = md5($studentrecord['student_email']);
                    $student_usernameenc = md5($studentrecord['student_username']);
                    $time = time();
                    $gmtnow = gmdate("Y-m-d H-i-s", $time);
                    if (($student_username == $student_usernameenc) && ($student_email == $student_emailenc)) {
                        $query = sprintf("UPDATE `student` SET `student_password`= '%s', `password_last_updated` = '%s' WHERE `student_id` = '%s'"
                                , mysqli_real_escape_string($this->db, md5($student_password)), mysqli_real_escape_string($this->db, $gmtnow), mysqli_real_escape_string($this->db, $student_id));
                        $result = mysqli_query($this->db, $query);
                        if (!$result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal server Error.");
                            $this->response($this->json($error), 500); // If no records "No Content" status
                        } else {
                            $msg = array('status' => "Success", 'msg' => 'Password updated successfully');
                            $this->response($this->json($msg), 200);
                        }
                    } else {
                        $error = array('status' => "Failed", "msg" => "Student details mismatched");
                        $this->response($this->json($error), 401);
                    }
                } else {
                    $error = array('status' => "Failed", "msg" => "Reset password link expired");
                    $this->response($this->json($error), 401);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Student details does not exist.");
                $this->response($this->json($error), 401);
            }
        }
    }

    public function verifyActivationStatus($student_id, $company_id) {
        $query = sprintf("SELECT `registration_status` FROM `student` WHERE `student_id`= '%s' AND `company_id` = '%s'", mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal server Error.");
            $this->response($this->json($error), 500); // If no records "No Content" status
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                if ($row['registration_status'] == 'S') {
                    $companyDetails = $this->getCompanyDetails($company_id, $student_id, 1);
                    $msg = array('status' => "Success", 'msg' => 'Member app activated successfully', "company_details" => $companyDetails);
                    $this->response($this->json($msg), 200);
                } else {
                    $error = array('status' => "Failed", "msg" => "Member app not activated");
                    $this->response($this->json($error), 401);
                }
            }
        }
    }

    public function addParticipantProfileDetails($company_id, $student_id, $participant_first_name, $participant_last_name, $participant_date_of_birth, $flag) {
        $select_participant2 = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s' AND `deleted_flag`!='D'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_first_name), mysqli_real_escape_string($this->db, $participant_last_name));
        $result_select_participant2 = mysqli_query($this->db, $select_participant2);
        if (!$result_select_participant2) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$select_participant2");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            if (mysqli_num_rows($result_select_participant2) > 0) {
                $r = mysqli_fetch_assoc($result_select_participant2);
                $participant_id = $r['participant_id'];
                if ($flag == 'ER') {
                    $query = sprintf("UPDATE `participant` SET `participant_first_name`='%s', `participant_last_name`='%s' WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_id`='%s'", mysqli_real_escape_string($this->db, $participant_first_name), mysqli_real_escape_string($this->db, $participant_last_name), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id));
                } else {
                    $query = sprintf("UPDATE `participant` SET `participant_first_name`='%s', `participant_last_name`='%s', `date_of_birth`='%s' WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_id`='%s'", mysqli_real_escape_string($this->db, $participant_first_name), mysqli_real_escape_string($this->db, $participant_last_name), mysqli_real_escape_string($this->db, $participant_date_of_birth), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id));
                }
            } else {
                if ($flag == 'ER') {
                    $query = sprintf("INSERT INTO `participant`  (`company_id`, `student_id`, `participant_first_name`, `participant_last_name`) VALUES ('%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_first_name), mysqli_real_escape_string($this->db, $participant_last_name));
                } else {
                    $query = sprintf("INSERT INTO `participant`  (`company_id`, `student_id`, `participant_first_name`, `participant_last_name`, `date_of_birth`) VALUES ('%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_first_name), mysqli_real_escape_string($this->db, $participant_last_name), mysqli_real_escape_string($this->db, $participant_date_of_birth));
                }
            }
            $result = mysqli_query($this->db, $query);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal server Error.");
                $this->response($this->json($error), 500); // If no records "No Content" status
            }
            $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
            $msg = array('status' => "Success", 'msg' => 'Participant details added successfully', "memberapp_details" => $student_info_details);
            $this->response($this->json($msg), 200);
        }
    }

    public function editParticipantProfileDetails($company_id, $student_id, $participant_id, $participant_first_name, $participant_last_name, $participant_date_of_birth, $edit_type) {
        $sql_select = sprintf("SELECT `participant_first_name`,`participant_last_name`,`date_of_birth` FROM `participant` WHERE  `participant_id` = '%s' AND `company_id` = '%s' AND `student_id`= '%s' AND `deleted_flag`!='D' ", mysqli_real_escape_string($this->db, $participant_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
        $result_select = mysqli_query($this->db, $sql_select);
        if (!$result_select) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query  " => "$sql_select");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal server Error.");
            $this->response($this->json($error), 500); // If no records "No Content" status
        } else {
            $num_of_rows1 = mysqli_num_rows($result_select);
            if ($num_of_rows1 > 0) {
                if ($edit_type == 'edit') {
                    if ($participant_date_of_birth == '' || $participant_date_of_birth == '0000-00-00' || is_null($participant_date_of_birth)) {
                        $query = sprintf("UPDATE `participant`  SET `participant_first_name` = '%s', `participant_last_name` = '%s', `date_of_birth` = NULL WHERE `participant_id` = '%s'", mysqli_real_escape_string($this->db, $participant_first_name), mysqli_real_escape_string($this->db, $participant_last_name), mysqli_real_escape_string($this->db, $participant_id));
                    } else {
                        $query = sprintf("UPDATE `participant`  SET `participant_first_name` = '%s', `participant_last_name` = '%s', `date_of_birth` = '%s' WHERE `participant_id` = '%s'", mysqli_real_escape_string($this->db, $participant_first_name), mysqli_real_escape_string($this->db, $participant_last_name), mysqli_real_escape_string($this->db, $participant_date_of_birth), mysqli_real_escape_string($this->db, $participant_id));
                    }
                    $result = mysqli_query($this->db, $query);
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal server Error.");
                        $this->response($this->json($error), 500); // If no records "No Content" status
                    } else {
                        $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
                        $msg = array('status' => "Success", 'msg' => 'Participant details updated successfully', "memberapp_details" => $student_info_details);
                        $this->response($this->json($msg), 200);
                    }
                } elseif ($edit_type == 'delete') {
                    $query = sprintf("UPDATE `participant` SET `deleted_flag`='D' WHERE `participant_id`='%s'", mysqli_real_escape_string($this->db, $participant_id));
                    $result = mysqli_query($this->db, $query);
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal server Error.");
                        $this->response($this->json($error), 500); // If no records "No Content" status
                    } else {
                        $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
                        $msg = array('status' => "Success", 'msg' => 'Participant details deleted successfully', "memberapp_details" => $student_info_details);
                        $this->response($this->json($msg), 200);
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Participant details not available.");
                $this->response($this->json($error), 401); // If no records "No Content" status
            }
        }
    }

    public function addOrUpdatePaymentMethodDetails($company_id, $student_id, $new_cc_id, $cc_state, $buyer_name, $buyer_email, $postal_code, $country, $type, $payment_method_id, $user_type, $payment_support, $stripe_payment_method_id, $first_name, $last_name) {
        $gmt_date = gmdate(DATE_RFC822);
        $sns_msg = array("buyer_email" => $buyer_email, "membership_title" => $student_id, "gmt_date" => $gmt_date);
        $button_url = '';
        
        if ($payment_support == 'W') {
            $query1 = sprintf("SELECT `wp_account_id`,`access_token`,`account_id`,`account_state`,`wp_user_state` FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query1");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal server Error.");
                $this->response($this->json($error), 500); // If no records "No Content" status
            } else {
                $num_rows1 = mysqli_num_rows($result1);
                if ($num_rows1 > 0) {
                    $row = mysqli_fetch_assoc($result1);
                    $access_token = $row['access_token'];
                    $account_id = $row['account_id'];
                    $user_state = $row['wp_user_state'];
                    $acc_state = $row['account_state'];
//                $schedule_date = $row['s_date'];

                    if ($user_state == 'deleted') {
                        $log = array("status" => "Failed", "msg" => "You may delete your account yourselves, or the account may be deleted by WePay customer support.");
                        $this->response($this->json($log), 200);
                    }

                    if ($acc_state == 'deleted') {
                        $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                        $this->response($this->json($log), 200);
                    } elseif ($acc_state == 'disabled') {
                        $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                        $this->response($this->json($log), 200);
                    }

                    if (!empty($access_token) && !empty($account_id)) {
                        if (!function_exists('getallheaders')) {
                            if (!function_exists('apache_request_headers')) {
                                $user_agent = $_SERVER['HTTP_USER_AGENT'];
                            } else {
                                $headers = apache_request_headers();
                                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                            }
                        } else {
                            $headers = getallheaders();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                        $temp_password = $this->wp_pswd;
                        $method = 'aes-256-cbc';
                        // Must be exact 32 chars (256 bit)
                        $password = substr(hash('sha256', $temp_password, true), 0, 32);
                        // IV must be exact 16 chars (128 bit)
                        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                        $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);

//                    $json = array("account_id"=>"$account_id");
//                    $postData = json_encode($json);
//                    $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$user_agent,$sns_msg2);
//                    $this->wp->log_info("wepay_get_account: ".$response['status']);
//                    if($response['status']=='Success'){
//                        $res = $response['msg'];
//                        $account_state = $res['state'];
//                        $currency = $res['currencies'][0];
//                        $curr_acc = implode(",", $res['currencies']);
//                        $disabled_reasons = implode(",", $res['disabled_reasons']);
//                        if($account_state=='deleted'){
//                            $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
//                            $this->response($this->json($log),200);
//                        }elseif($account_state=='disabled'){
//                            $log = array("status" => "Failed", "msg" => "The account has been disabled for following reason(s) - ".$disabled_reasons." by WePay and can no longer accept payments.");
//                            $this->response($this->json($log),200);
//                        }
//                    }else{
//                        $log = array("status" => "Failed", "msg" => $response['msg']['error_description']);
//                        $this->response($this->json($log),200);
//                    }

                        $cc_name = $cc_month = $cc_year = '';
                        $response2 = [];
                        $cc_status = $cc_state;
                        $json2 = array("client_id" => "$this->wp_client_id", "client_secret" => "$this->wp_client_secret", "credit_card_id" => $new_cc_id, "account_id" => $account_id);
                        $postData2 = json_encode($json2);
                        $sns_msg['call'] = "Credit Card";
                        $sns_msg['type'] = "Credit Card Authorization";
                        $response2 = $this->wp->accessWepayApi("credit_card/authorize", 'POST', $postData2, $token, $user_agent, $sns_msg);
                        $this->wp->log_info("wepay_credit_card_authorize: " . $response2['status']);
                        if ($response2['status'] == "Success") {
                            $res2 = $response2['msg'];
                            $cc_name = $res2['credit_card_name'];
                            $cc_new_state = $res2['state'];
                            if ($cc_state != $cc_new_state) {
                                $cc_status = $cc_new_state;
                            }
                            $cc_month = $res2['expiration_month'];
                            $cc_year = $res2['expiration_year'];
                        } else {
                            $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                            $this->response($this->json($error), 400);
                        }

                        if ($user_type == 'G') {
                            $out = array("status" => "Success", "msg" => "Credit Card details fetched successfully", "cc_info" => $response2);
                            $this->response($this->json($out), 200);
                        } else {
                            if ($type == 'A') {
                                $insertquery = sprintf("INSERT INTO `payment_method_details`( `company_id`, `student_id`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `buyer_email`, `buyer_name`, `buyer_postal_code`,`country`) 
                                VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $new_cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name)
                                        , mysqli_real_escape_string($this->db, $postal_code), mysqli_real_escape_string($this->db, $country));
                                $message = "Credit Card details added Successfully";
                            } else if ($type == 'U') {
                                $insertquery = sprintf("UPDATE `payment_method_details` SET  `credit_card_id`='%s',`credit_card_status`='%s',`credit_card_name`='%s',`credit_card_expiration_month`='%s',`credit_card_expiration_year`='%s',`buyer_email`='%s',`buyer_name`='%s',`buyer_postal_code`='%s'
                                   ,`country`='%s' WHERE payment_method_id='%s' ", mysqli_real_escape_string($this->db, $new_cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name)
                                        , mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name)
                                        , mysqli_real_escape_string($this->db, $postal_code), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $payment_method_id));
                                $message = "Credit Card details updated Successfully";
                            }
                            $resultinsertquery = mysqli_query($this->db, $insertquery);
                            if (!$resultinsertquery) {
                                $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$insertquery");
                                mlog_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal server Error.");
                                $this->response($this->json($error), 500);
                            }
                            $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
                            $msg = array('status' => "Success", 'msg' => "$message", "memberapp_details" => $student_info_details, "cc_info" => $response2);
                            $this->response($this->json($msg), 200);
                        }
                    }
                }
            }
        } else {
            $querypf = sprintf("SELECT `company_name` FROM `company` WHERE `company_id`='%s' ", mysqli_real_escape_string($this->db, $company_id));
            $result = mysqli_query($this->db, $querypf);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$querypf");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $studio_name = $row['company_name'];
                }
            }
            $cc_month = $cc_year = $cc_name = $stripe_customer_id = '';
            $cc_state = 'S';
            $setupIntent = $this->setupIntentCreation($company_id, $student_id, $stripe_payment_method_id, $buyer_email, $first_name, $last_name, $stripe_customer_id, 'Student');
            $stripe_customer_id = $setupIntent['stripe_customer_id'];

            if ($setupIntent['temp_payment_status'] == 'requires_action') {
                $cc_state = 'P';
                $next_action_type = $setupIntent['next_action']['type'];
                $next_action_type2 = $setupIntent['next_action']['use_stripe_sdk']['type'];
                $button_url = $setupIntent['next_action']['use_stripe_sdk']['stripe_js'];
                if ($next_action_type != "use_stripe_sdk" || $next_action_type2 != "three_d_secure_redirect") {
                    # Payment not supported
                    $out['status'] = 'Failed';
                    $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                    $this->response($this->json($out), 200);
                } else {
                    $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name);
                }
            }

            if (!empty($stripe_payment_method_id)) {
                try {
                    $retrive_payment_method = \Stripe\PaymentMethod::retrieve($stripe_payment_method_id);
                    $brand = $retrive_payment_method->card->brand;
                    $cc_month = $retrive_payment_method->card->exp_month;
                    $cc_year = $retrive_payment_method->card->exp_year;
                    $last4 = $retrive_payment_method->card->last4;
                    $temp = ' xxxxxx';
                    $cc_name = $brand . $temp . $last4;
                    $response2['msg'] = array("credit_card_name" => $cc_name, "stripe_payment_method_id" => $stripe_payment_method_id, "stripe_customer_id" => $stripe_customer_id);
                } catch (\Stripe\Error\Card $e) {
                    // Since it's a decline, \Stripe\Error\Card will be caught     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\RateLimit $e) {
                    // Too many requests made to the API too quickly     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\InvalidRequest $e) {
                    // Invalid parameters were supplied to Stripe's API     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Authentication $e) {
                    // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\ApiConnection $e) {
                    // Network communication with Stripe failed     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Base $e) {
                    // Display a very generic error to the user, and maybe send yourself an email     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (Exception $e) {
                    // Something else happened, completely unrelated to Stripe     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                }
                if (isset($err) && !empty($err)) {
                    $error = array('status' => "Failed", "msg" => $err['message']);
                    $this->response($this->json($error), 200);
                }
            }

            if ($user_type == 'G') {
                $out = array("status" => "Success", "msg" => "Credit Card details fetched successfully", "cc_info" => $response2);
                $this->response($this->json($out), 200);
            } else {
                if ($type == 'A') {
                    $insertquery = sprintf("INSERT INTO `payment_method_details`( `company_id`, `student_id`, `payment_support`, `stripe_payment_method_id`, `stripe_customer_id`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `buyer_email`, `buyer_name`, `buyer_postal_code`,`country`) 
                    VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $payment_support), mysqli_real_escape_string($this->db, $stripe_payment_method_id), mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name)
                            , mysqli_real_escape_string($this->db, $postal_code), mysqli_real_escape_string($this->db, $country));
                    $message = "Credit Card details added Successfully";
                } else if ($type == 'U') {
                    $insertquery = sprintf("UPDATE `payment_method_details` SET  `payment_support`='%s', `stripe_payment_method_id`='%s',`stripe_customer_id`='%s',`credit_card_name`='%s',`credit_card_expiration_month`='%s',`credit_card_expiration_year`='%s',`buyer_email`='%s',`buyer_name`='%s',`buyer_postal_code`='%s'
                       ,`country`='%s' WHERE payment_method_id='%s' ", mysqli_real_escape_string($this->db, $payment_support), mysqli_real_escape_string($this->db, $stripe_payment_method_id), mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $cc_name)
                            , mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name)
                            , mysqli_real_escape_string($this->db, $postal_code), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $payment_method_id));
                    $message = "Credit Card details updated Successfully";
                }
                $resultinsertquery = mysqli_query($this->db, $insertquery);
                if (!$resultinsertquery) {
                    $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$insertquery");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal server Error.");
                    $this->response($this->json($error), 500);
                }
                $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
                $msg = array('status' => "Success", 'msg' => "$message", "memberapp_details" => $student_info_details, "cc_info" => $response2);
                $this->response($this->json($msg), 200);
            }
        }
    }

    public function updateBuyerInformationDetails($company_id, $student_id, $student_firstname, $student_lastname, $billing_address1, $billing_address2, $city, $state, $zipcode, $country, $flag, $student_email, $student_mobile, $student_cc_email) {

        $select_sql = sprintf("SELECT * FROM `student` WHERE `company_id`='%s' AND `student_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
        $result_select_sql = mysqli_query($this->db, $select_sql);
        if (!$result_select_sql) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$select_sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal server Error.");
            $this->response($this->json($error), 500); // If no records "No Content" status
        } else {
            $num_rows = mysqli_num_rows($result_select_sql);
            if ($num_rows > 0) {
                
            } else {
                $error = array('status' => "Failed", "msg" => "Buyer details not available.");
                $this->response($this->json($error), 401);
            }
        }

        if ($flag == 'M') {
            $email_check = $this->checkStudentEmailAvailability($company_id, $student_id, $student_email, 1);
            if ($email_check['status'] == 'Failed') {
                $this->response($this->json($email_check), 500);
            }
            $query = sprintf("UPDATE `student`  SET `student_name` = '%s', `student_lastname` = '%s', `billing_address1` = '%s', `billing_address2` = '%s', `city` = '%s', `state` = '%s', `zipcode` = '%s', `country` = '%s', `student_email`='%s', `student_mobile`='%s', `student_cc_email`='%s'  WHERE `student_id` = '%s' AND `company_id` = '%s'", mysqli_real_escape_string($this->db, $student_firstname), mysqli_real_escape_string($this->db, $student_lastname), mysqli_real_escape_string($this->db, $billing_address1), mysqli_real_escape_string($this->db, $billing_address2), mysqli_real_escape_string($this->db, $city), mysqli_real_escape_string($this->db, $state), mysqli_real_escape_string($this->db, $zipcode), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $student_email), mysqli_real_escape_string($this->db, $student_mobile), mysqli_real_escape_string($this->db, $student_cc_email), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
        } elseif ($flag == 'ER') {
            $query = sprintf("UPDATE `student`  SET `student_name` = '%s', `student_lastname` = '%s' WHERE `student_id` = '%s' AND `company_id` = '%s'", mysqli_real_escape_string($this->db, $student_firstname), mysqli_real_escape_string($this->db, $student_lastname), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
        } else {
            $query = sprintf("UPDATE `student`  SET `student_name` = '%s', `student_lastname` = '%s', `billing_address1` = '%s', `billing_address2` = '%s', `city` = '%s', `state` = '%s', `zipcode` = '%s', `country` = '%s' WHERE `student_id` = '%s' AND `company_id` = '%s'", mysqli_real_escape_string($this->db, $student_firstname), mysqli_real_escape_string($this->db, $student_lastname), mysqli_real_escape_string($this->db, $billing_address1), mysqli_real_escape_string($this->db, $billing_address2), mysqli_real_escape_string($this->db, $city), mysqli_real_escape_string($this->db, $state), mysqli_real_escape_string($this->db, $zipcode), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
        }
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal server Error.");
            $this->response($this->json($error), 500); // If no records "No Content" status
        } else {
            if ($flag == 'M' && $email_check['status'] == 'Success' && $email_check['to_be_updated'] == 1) {
                $sql = sprintf("UPDATE `student` s LEFT JOIN `membership_registration` m ON m.`student_id`  =  s.`student_id`  and  m.`company_id`= s.`company_id` and m.`buyer_email`  =  s.`student_email`
                                  LEFT JOIN `event_registration` e on e.`student_id`  =  s.`student_id`  and  e.`company_id`= s.`company_id`  and  e.`buyer_email`  =  s.`student_email`
                                  LEFT JOIN `trial_registration` t on t.`student_id`  =  s.`student_id`  and  t.`company_id`= s.`company_id`  and  t.`buyer_email`  =  s.`student_email`
                                  SET  m.`buyer_email` = '%s', e.`buyer_email`='%s', t.`buyer_email`='%s' WHERE s.`student_id`='%s' and s.company_id='%s'", mysqli_real_escape_string($this->db, $student_email), mysqli_real_escape_string($this->db, $student_email), mysqli_real_escape_string($this->db, $student_email), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
                $result = mysqli_query($this->db, $sql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
            $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
            $msg = array('status' => "Success", 'msg' => 'Buyer information updated successfully', "memberapp_details" => $student_info_details);
            $this->response($this->json($msg), 200);
        }
    }

    public function getStudentAllDetails($company_id, $student_id, $callback) {
        $output = $participantDetails = [];
        $query_stud = sprintf("SELECT `company_id`, `student_id` ,`student_username`, `student_email`,`student_cc_email`, `student_mobile`, `student_name` as student_firstname, `student_lastname`, `billing_address1`, `billing_address2`, `city`, `state`, 
                `zipcode`, `country`, `push_device_id`, `device_type`, `registration_status`, `deleted_flag`, `app_id` FROM `student` WHERE `student_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
        $result_stud = mysqli_query($this->db, $query_stud);
        if (!$result_stud) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query_stud");
            mlog_info($this->json($error_log));
            $error_stud = array('status' => "Failed", "msg" => "Internal server Error.");
            $this->response($this->json($error_stud), 500); // If no records "No Content" status
        } else {
            $num_rows_stud = mysqli_num_rows($result_stud);
            if ($num_rows_stud > 0) {
                $out = mysqli_fetch_assoc($result_stud);
                $output['buyer_details'] = $out;
            } else {
                $output['buyer_details'] = [];
            }
        }

        $query_par = sprintf("SELECT `participant_id`, `participant_first_name`, `participant_last_name`, `date_of_birth` FROM `participant` WHERE `student_id` = '%s' AND `company_id` = '%s' AND `deleted_flag`!='D' ORDER BY `created_date` DESC", mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $company_id));
        $result_par = mysqli_query($this->db, $query_par);

        if (!$result_par) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$query_par");
            mlog_info($this->json($error_log));
            $error_par = array('status' => "Failed", "msg" => "Internal server Error.");
            $this->response($this->json($error_par), 500); // If no records "No Content" status
        } else {
            $num_rows_par = mysqli_num_rows($result_par);
            if ($num_rows_par > 0) {
                while ($row_par = mysqli_fetch_assoc($result_par)) {
                    $participantDetails[] = $row_par;
                }
            }
            $output['participant_details'] = $participantDetails;
        }

        $select_pd = sprintf("SELECT `payment_method_id`, `stripe_payment_method_id`, `stripe_customer_id`, `payment_support`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `buyer_name`, `buyer_email`, `buyer_postal_code`
                FROM `payment_method_details` WHERE `company_id`='%s' AND `student_id`='%s'"
                , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
        $result_pd = mysqli_query($this->db, $select_pd);
        if (!$result_pd) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$select_pd");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal server Error.");
            $this->response($this->json($error), 500);
        } else {
            $nums_row_pd = mysqli_num_rows($result_pd);
            if ($nums_row_pd > 0) {
                $out = [];
                while ($row = mysqli_fetch_assoc($result_pd)) {
                    $out[] = $row;
                }
                $output['card_details'] = $out;
            } else {
                $output['card_details'] = [];
            }
        }
        $final_result = array('status' => "Success", 'msg' => $output);
        if ($callback == 0) {
            $this->response($this->json($final_result), 200);
        } elseif ($callback == 1) {
            return $output;
        }
    }

//    public function getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $device_type, $app_id, $push_device_id) {
//        $select_student = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$company_id));
//        $result_select_student = mysqli_query($this->db, $select_student);
//        if (!$result_select_student) {
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student);
//            $this->response($this->json($error), 200);
//        } else {
//            if (mysqli_num_rows($result_select_student) > 0) {
//                $r = mysqli_fetch_assoc($result_select_student);
//                $student_id = $r['student_id'];
//            } else {
//                $insert_student = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_lastname`, `student_email`, `device_type`, `app_id`, `push_device_id`) VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s')", 
//                        mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$buyer_first_name), mysqli_real_escape_string($this->db,$buyer_last_name), 
//                        mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$device_type), mysqli_real_escape_string($this->db,$app_id),
//                        mysqli_real_escape_string($this->db,$push_device_id));
//                $result_insert_student = mysqli_query($this->db, $insert_student);
//                if (!$result_insert_student) {
//                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student);
//                    $this->response($this->json($error), 200);
//                } else {
//                    $student_id = mysqli_insert_id($this->db);
//                }
//            }
//            return $student_id;
//        }
//    }

    public function getParticipantId($company_id, $student_id, $first_name, $last_name, $dob) {

        $select_participant2 = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name), mysqli_real_escape_string($this->db, $dob));
        $result_select_participant2 = mysqli_query($this->db, $select_participant2);
        if (!$result_select_participant2) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_participant2);
            $this->response($this->json($error), 500);
        } else {
            if (mysqli_num_rows($result_select_participant2) > 0) {
                $r = mysqli_fetch_assoc($result_select_participant2);
                $participant_id = $r['participant_id'];
                $deleted_flag = $r['deleted_flag'];
                if ($deleted_flag == 'D') {
                    $update_participant = sprintf("UPDATE `participant` SET `deleted_flag`='N' WHERE `participant_id`='%s'", mysqli_real_escape_string($this->db, $participant_id));
                    $result_update_participant = mysqli_query($this->db, $update_participant);
                    if (!$result_update_participant) {
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_participant);
                        $this->response($this->json($error), 500);
                    }
                }
            } else {
                $insert_participant2 = sprintf("INSERT INTO `participant`(`company_id`, `student_id`, `participant_first_name`, `participant_last_name`, `date_of_birth`) VALUES('%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name), mysqli_real_escape_string($this->db, $dob));
                $result_insert_participant2 = mysqli_query($this->db, $insert_participant2);
                if (!$result_insert_participant2) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_participant2);
                    $this->response($this->json($error), 500);
                } else {
                    $participant_id = mysqli_insert_id($this->db);
                }
            }
            return $participant_id;
        }
    }

    public function getParticipantIdForEvent($company_id, $student_id, $first_name, $last_name) {

        $select_participant2 = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name));
        $result_select_participant2 = mysqli_query($this->db, $select_participant2);
        if (!$result_select_participant2) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_participant2);
            $this->response($this->json($error), 500);
        } else {
            if (mysqli_num_rows($result_select_participant2) > 0) {
                $r = mysqli_fetch_assoc($result_select_participant2);
                $participant_id = $r['participant_id'];
                $deleted_flag = $r['deleted_flag'];
                if ($deleted_flag == 'D') {
                    $update_participant = sprintf("UPDATE `participant` SET `deleted_flag`='N' WHERE `participant_id`='%s'", mysqli_real_escape_string($this->db, $participant_id));
                    $result_update_participant = mysqli_query($this->db, $update_participant);
                    if (!$result_update_participant) {
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_participant);
                        $this->response($this->json($error), 500);
                    }
                }
            } else {
                $insert_participant2 = sprintf("INSERT INTO `participant`(`company_id`, `student_id`, `participant_first_name`, `participant_last_name`) VALUES('%s', '%s', '%s', '%s')", $company_id, $student_id, mysqli_real_escape_string($this->db, $first_name), mysqli_real_escape_string($this->db, $last_name));
                $result_insert_participant2 = mysqli_query($this->db, $insert_participant2);
                if (!$result_insert_participant2) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_participant2);
                    $this->response($this->json($error), 500);
                } else {
                    $participant_id = mysqli_insert_id($this->db);
                }
            }
            return $participant_id;
        }
    }

    // NEW FUNCTIONS FOR REGISTRATION AND LOGIN 
    public function checkStudentEmailAvailability($company_id, $student_id, $student_email, $call_back) {   //$call_back  0-echo & exit, 1 - return
        $username_query = sprintf("SELECT * FROM `student` WHERE `company_id`='%s' AND `student_email`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_email));
        $username_result = mysqli_query($this->db, $username_query);
        if (!$username_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$username_query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", 'msg' => 'Internal server error');
            if ($call_back == 0) {
                $this->response($this->json($error), 500);
            } else {
                return $error;
            }
        } else {
            $num_rows = mysqli_num_rows($username_result);
            if ($num_rows > 0) {
                $student_details = mysqli_fetch_assoc($username_result);
                if ($student_id != $student_details['student_id']) {
                    $out = array('status' => "Failed", 'msg' => 'Email already registered for the studio. Try Forgot password to recover the account.');
//                    $this->response($this->json($out), 401);
                } else {
                    $out = array('status' => "Success", 'msg' => 'Email checked for same student registered for this studio.', "to_be_updated" => 0);
//                    $this->response($this->json($out), 200);
                }
            } else {
                $out = array('status' => "Success", 'msg' => 'Email not registered for the studio.', "to_be_updated" => 1);
//                $this->response($this->json($out), 200);
            }
            if ($call_back == 0) {
                $this->response($this->json($out), 200);
            } else {
                return $out;
            }
        }
    }

    public function updateStudentEmailAndPhoneDetails($company_id, $student_id, $student_email, $student_mobile, $student_name, $student_lastname, $register_verification_id) {
        $query = sprintf("SELECT * FROM `student` WHERE `company_id`='%s' AND `student_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", 'msg' => 'Internal server error');
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $stud = mysqli_fetch_assoc($result);
                $registration_status = $stud['registration_status'];
                $update_sql = sprintf("UPDATE `student` SET `student_email`='%s', `student_mobile`='%s', `student_name` = '%s', `student_lastname` = '%s'  WHERE `company_id`='%s' AND `student_id`='%s'", mysqli_real_escape_string($this->db, $student_email), mysqli_real_escape_string($this->db, $student_mobile), mysqli_real_escape_string($this->db, $student_name), mysqli_real_escape_string($this->db, $student_lastname), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
                $result_update = mysqli_query($this->db, $update_sql);
                if (!$result_update) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sql");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", 'msg' => 'Internal server error');
                    $this->response($this->json($error), 500);
                } else {
                    $student_details['register_verification_id'] = $register_verification_id;
                    $student_details['student_id'] = $student_id;
                    $student_details['company_id'] = $company_id;
                    $student_details['registration_status'] = $registration_status;
                    $verification_details = $this->resendVerficationDetailsEmail($company_id, $student_id, $register_verification_id, 'S', 1);
                    if ($verification_details['status'] == 'Failed') {
                        $this->response($this->json($verification_details), 401);
                    }
                    $out = array('status' => "Success", 'msg' => 'Student details updated successfully.', "student_details" => $student_details);
                    $this->response($this->json($out), 200);
                }
            } else {
                $out = array('status' => "Failed", 'msg' => 'Student details not exist.');
                $this->response($this->json($out), 401);
            }
        }
    }

    public function getTrialDetail($company_id, $call_back) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_add = " DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";

        $trial_list_url_init = 0;
        $trial_list_url = $company_code = '';
        $sql = sprintf("SELECT `trial_id`, t.`company_id`, `trial_status`, `trial_title`, `trial_subtitle`, `trial_desc`, `trial_prog_url`, `trial_welcome_url`, `trial_banner_img_url`, `trial_banner_img_content`, `trial_waiver_policies`,`registrations_count` registration_count, `net_sales`, `processing_fee_type`, 
            `price_amount`, `program_length_type`, `program_length`, `trial_sort_order`,  `trial_registration_column_1`, `trial_registration_column_1_flag`, `trial_registration_column_2`, `trial_registration_column_2_flag`, `trial_registration_column_3`, `trial_registration_column_3_flag`, `trial_registration_column_4`, 
            `trial_registration_column_4_flag`, `trial_registration_column_5`, `trial_registration_column_5_flag`, `trial_registration_column_6`, `trial_registration_column_6_flag`, `trial_registration_column_7`, `trial_registration_column_7_flag`, `trial_registration_column_8`, `trial_registration_column_8_flag`, 
            `trial_registration_column_9`, `trial_registration_column_9_flag`, `trial_registration_column_10`, `trial_registration_column_10_flag`,`trial_lead_source_1`,  `trial_lead_source_2`,  `trial_lead_source_3`, 
             `trial_lead_source_4`, `trial_lead_source_5`,  `trial_lead_source_6`,  `trial_lead_source_7`, `trial_lead_source_8`, 
            `trial_lead_source_9`,  `trial_lead_source_10`, c.`company_code`,IF(t.`trial_status`='P','published',IF(t.`trial_status`='S','draft','unpublished')) list_type
        FROM `trial` t left join company c on t.`company_id`=c.`company_id` where c.`company_id`='%s' and `trial_status`!='D' ORDER BY `trial_sort_order` desc", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 1) {
                return $error;
            } else {
                $this->response($this->json($error), 500);
            }
        } else {
            $output = $response['live'] = $response['draft'] = $response['unpublished'] = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    if ($trial_list_url_init == 0) {
                        $company_code = $row['company_code'];
                        $trial_list_url_init++;
                        $trial_list_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/t/?=$company_code/$company_id";
                    }
                    $disc = $this->getTrialDiscountDetails($company_id, $row['trial_id']);
                    $row['discount'] = $disc['discount'];
                    $reg_field_name_array = [];
                    $temp_reg_field = "trial_registration_column_";
                    for ($z = 1; $z <= 10; $z++) { //collect registration fields as array
                        $y = $z - 1;
                        $reg_field = $temp_reg_field . "$z";      //for filed names
                        $reg_field_mandatory = "trial_registration_column_" . $z . "_flag";
                        if ($row[$reg_field] != '') {
                            $reg_field_name_array[] = array("reg_col_name" => $row[$reg_field], "reg_col_mandatory" => $row[$reg_field_mandatory], "reg_col_index" => $y);
                        }
                    }
                    $row['reg_columns'] = $reg_field_name_array;
                    $lead_name_array = [];
                    $temp_lead_field = "trial_lead_source_";
                    for ($z = 1; $z <= 10; $z++) { //collect registration fields as array
                        $y = $z - 1;
                        $reg_lead = $temp_lead_field . "$z";      //for filed names
                        if ($row[$reg_lead] != '') {
                            $lead_name_array[] = array("lead_col_name" => $row[$reg_lead], "lead_col_index" => $y);
                        }
                    }
                    $row['lead_columns'] = $lead_name_array;

                    $output[] = $row;
                }

                for ($i = 0; $i < count($output); $i++) {//split-up by live, past & draft
                    if ($output[$i]['list_type'] == 'published') {
                        $response['live'][] = $output[$i];
                    } elseif ($output[$i]['list_type'] == 'draft') {
                        $response['draft'][] = $output[$i];
                    } elseif ($output[$i]['list_type'] == 'unpublished') {
                        $response['unpublished'][] = $output[$i];
                    }
                }
                usort($response['live'], function($a, $b) {
                    return $a['trial_sort_order'] < $b['trial_sort_order'];
                });
                $out = array('status' => "Success", 'msg' => $response, 'trial_list_url' => $trial_list_url);
                // If success everythig is good send header as "OK" and user details
                if ($call_back == 1) {
                    return $out;
                } else {
                    $this->response($this->json($out), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Trial details doesn't exist.");
                if ($call_back == 1) {
                    return $error;
                } else {
                    $this->response($this->json($error), 401);
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

    public function trialProgramCheckout($company_id, $trial_id, $actual_student_id, $actual_participant_id, $trial_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $cc_id, $cc_state, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $processing_fee_type, $discount, $payment_type, $upgrade_status, $discount_code, $reg_type_user, $reg_version_user, $program_length, $program_length_type, $start_date, $default_cc, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $device_type, $app_id, $push_device_id) {
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $upgrade_status = $sts['upgrade_status'];
        $buyer_name = $buyer_first_name . ' ' . $buyer_last_name;
        $this->addTrialDimensions($company_id, $trial_id, '');
        if (is_null($actual_student_id) || $actual_student_id <= 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $device_type, $app_id, $push_device_id, '', '');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $actual_student_id;
        }
        if (is_null($actual_participant_id) || $actual_participant_id == 0) {
            $participant_id = $this->getParticipantId($company_id, $student_id, $reg_col1, $reg_col2, $reg_col3);
        } else {
            $participant_id = $actual_participant_id;
        }
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";

        $pr_fee = $temp_pr_fee = 0;
        $current_date = date("Y-m-d");
        if ($program_length_type == 'D') {
            $end_date = date("Y-m-d", strtotime("+$program_length days", strtotime($start_date)));
        } else {
            $end_date = date("Y-m-d", strtotime("+$program_length week", strtotime($start_date)));
        }


//        $check_deposit_failure = 0;
        $gmt_date = gmdate(DATE_RFC822);
        $sns_msg1 = array("buyer_email" => $buyer_email, "membership_title" => $trial_name, "gmt_date" => $gmt_date);
        $p_f = $this->getProcessingFee($company_id, $upgrade_status);
        $process_fee_per = $p_f['PP'];
        $process_fee_val = $p_f['PT'];


        if ($payment_amount > 0) {
            $payment_type = 'O';
            $processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
        } else {
            $payment_type = 'F';
            $processing_fee = 0;
        }


        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency` FROM `wp_account` WHERE `company_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows1 = mysqli_num_rows($result1);
            if ($num_rows1 > 0) {
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                if ($user_state == 'deleted') {
                    $msg = "Wepay user registered was deleted.";
                    $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
                    $this->response($this->json($log), 200);
                }

                if ($acc_state == 'deleted') {
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log), 200);
                } elseif ($acc_state == 'disabled') {
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log), 200);
                }

                if (!empty($access_token) && !empty($account_id)) {
                    if (!function_exists('getallheaders')) {
                        if (!function_exists('apache_request_headers')) {
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        } else {
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    } else {
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);

                    $w_paid_amount = $paid_amount = $paid_fee = $gross = 0;
                    $checkout_id = $checkout_state = '';
                    $cc_name = $cc_month = $cc_year = '';
                    $cc_status = $cc_state;

                    if ($payment_amount > 0) {
                        $checkout_id_flag = 1;
                        $rand = mt_rand(1, 99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url . "/Wepay2/updateCheckout?for=trial";
                        $unique_id = $company_id . "_" . $trial_id . "_" . $ip . "_" . $rand . "_" . time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id . "_" . $trial_id . "_" . $date;
//                        if($processing_fee_type==2){
//                            $fee_payer = "payer_from_app";
//                        }elseif($processing_fee_type==1){
                        $fee_payer = "payee_from_app";
//                        }
                        $paid_amount = $payment_amount;
                        $paid_fee = $processing_fee;

                        if ($processing_fee_type == 2) {
                            $w_paid_amount = $payment_amount + $processing_fee;
                        } elseif ($processing_fee_type == 1) {
                            $w_paid_amount = $payment_amount;
                        }


                        $time = time();
                        $json2 = array("account_id" => "$account_id", "short_description" => "$desc", "type" => "event", "amount" => "$w_paid_amount", "currency" => "$currency",
                            "fee" => array("app_fee" => $paid_fee, "fee_payer" => "$fee_payer"), "reference_id" => "$ref_id", "unique_id" => "$unique_id",
                            "email_message" => array("to_payee" => "Payment has been added for $trial_name", "to_payer" => "Payment has been made for $trial_name"),
                            "payment_method" => array("type" => "credit_card", "credit_card" => array("id" => $cc_id)), "callback_uri" => "$checkout_callback_url",
                            "payer_rbits" => array(array("receive_time" => $time, "type" => "phone", "source" => "user", "properties" => array("phone" => "$buyer_phone", "phone_type" => "home")),
                                array("receive_time" => $time, "type" => "email", "source" => "user", "properties" => array("email" => "$buyer_email")),
                                array("receive_time" => $time, "type" => "address", "source" => "user", "properties" => array("address" => array("zip" => "$buyer_postal_code", "country" => "$country")))),
                            "transaction_rbits" => array(array("receive_time" => $time, "type" => "transaction_details", "source" => "partner_database",
                                    "properties" => array("itemized_receipt" => array(array("description" => "$trial_name", "item_price" => $w_paid_amount, "quantity" => 1, "amount" => $w_paid_amount, "currency" => "$currency"))))));
                        $postData2 = json_encode($json2);
                        $sns_msg1['call'] = "Checkout";
                        $sns_msg1['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create", 'POST', $postData2, $token, $user_agent, $sns_msg1);
                        $this->wp->log_info("wepay_checkout_create: " . $response2['status']);
                        if ($response2['status'] == "Success") {
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $gross = $res2['gross'];
//                            $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                        } else {
                            if ($response2['msg']['error_code'] == 1008) {
                                $response2 = $this->wp->accessWepayApi("checkout/create", 'POST', $postData2, $token, $user_agent, $sns_msg1);
                                $this->wp->log_info("wepay_checkout_create: " . $response2['status']);
                                if ($response2['status'] == "Success") {
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $gross = $res2['gross'];
//                                    $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                                } else {
                                    if ($response2['msg']['error_code'] == 1008) {
                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error), 400);
                                    } else {
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error), 400);
                                    }
                                }
                            } else {
                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error), 400);
                            }
                        }
                    } else {
                        $checkout_id_flag = 0;
                        if (!empty(trim($cc_id))) {
                            $json3 = array("client_id" => "$this->wp_client_id", "client_secret" => "$this->wp_client_secret", "credit_card_id" => $cc_id);
                            $postData3 = json_encode($json3);
                            $sns_msg1['call'] = "Credit Card";
                            $sns_msg1['type'] = "Credit card details";
                            $response3 = $this->wp->accessWepayApi("credit_card", 'POST', $postData3, $token, $user_agent, $sns_msg1);
                            $this->wp->log_info("wepay_credit_card: " . $response3['status']);
                            if ($response3['status'] == "Success") {
                                $res3 = $response3['msg'];
                                if ($res3['state'] == 'new') {
                                    $json2 = array("client_id" => "$this->wp_client_id", "client_secret" => "$this->wp_client_secret", "credit_card_id" => $cc_id, "account_id" => $account_id);
                                    $postData2 = json_encode($json2);
                                    $sns_msg1['call'] = "Credit Card";
                                    $sns_msg1['type'] = "Credit Card Authorization";
                                    $response2 = $this->wp->accessWepayApi("credit_card/authorize", 'POST', $postData2, $token, $user_agent, $sns_msg1);
                                    $this->wp->log_info("wepay_credit_card_authorize: " . $response2['status']);
                                    if ($response2['status'] == "Success") {
                                        $res2 = $response2['msg'];
                                        $cc_name = $res2['credit_card_name'];
                                        $cc_new_state = $res2['state'];
                                        if ($cc_state != $cc_new_state) {
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res2['expiration_month'];
                                        $cc_year = $res2['expiration_year'];
                                    } else {
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error), 400);
                                    }
                                } elseif ($res3['state'] == 'authorized') {
                                    $cc_name = $res3['credit_card_name'];
                                    $cc_new_state = $res3['state'];
                                    $cc_month = $res3['expiration_month'];
                                    $cc_year = $res3['expiration_year'];
                                } else {
                                    if ($res3['state'] == 'expired') {
                                        $msg = "Given credit card is expired.";
                                    } elseif ($res3['state'] == 'deleted') {
                                        $msg = "Given credit card was deleted.";
                                    } else {
                                        $msg = "Given credit card details is invalid.";
                                    }
                                    $error = array("status" => "Failed", "msg" => $msg);
                                    $this->response($this->json($error), 400);
                                }
                            } else {
                                $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                $this->response($this->json($error), 400);
                            }
                        }
                    }

                    if (!empty(trim($cc_id))) {
                        $json3 = array("client_id" => "$this->wp_client_id", "client_secret" => "$this->wp_client_secret", "credit_card_id" => $cc_id);
                        $postData3 = json_encode($json3);
                        $sns_msg1['call'] = "Credit Card";
                        $sns_msg1['type'] = "Credit Card details";
                        $response3 = $this->wp->accessWepayApi("credit_card", 'POST', $postData3, $token, $user_agent, $sns_msg1);
                        $this->wp->log_info("wepay_credit_card: " . $response3['status']);
                        if ($response3['status'] == "Success") {
                            $res3 = $response3['msg'];
                            $cc_name = $res3['credit_card_name'];
                            $cc_new_state = $res3['state'];
                            if ($cc_state != $cc_new_state) {
                                $cc_status = $cc_new_state;
                            }
                            $cc_month = $res3['expiration_month'];
                            $cc_year = $res3['expiration_year'];
                        } else {
                            for ($k = 0; $k < 2; $k++) {
                                if ($response3['msg']['error_code'] == 503) {
                                    sleep(2);
                                    $response3 = $this->wp->accessWepayApi("credit_card", 'POST', $postData3, $token, $user_agent, $sns_msg1);
                                    $this->wp->log_info("wepay_credit_card: " . $response3['status']);
                                    if ($response3['status'] == "Success") {
                                        $res3 = $response3['msg'];
                                        $cc_name = $res3['credit_card_name'];
                                        $cc_new_state = $res3['state'];
                                        if ($cc_state != $cc_new_state) {
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res3['expiration_month'];
                                        $cc_year = $res3['expiration_year'];
                                        break;
                                    }
                                    if ($k == 1) {
                                        if ($checkout_id_flag == 1) {
                                            $type = $response3['msg']['error'];
                                            $description = $response3['msg']['error_description'] . " Buyer Name:" . $buyer_name . " Buyer Email:" . $buyer_email . " Checkout Id:" . $checkout_id . " Participant Name:" . $reg_col1 . " " . $reg_col2;
                                            $insert_alertquery = sprintf("INSERT INTO `wepay_critical_alert`(`company_id`, `type`, `description`) VALUES ('%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $type), mysqli_real_escape_string($this->db, $description));
                                            $result_alert = mysqli_query($this->db, $insert_alertquery);
                                            if (!$result_alert) {
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_alertquery");
                                                mlog_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 500);
                                            }
                                            $type1 = $response3['msg']['error'] . " Regarding Credit Card";
                                            $description1 = "Fetching Credit Card details failed" . "\n" . " Buyer Name:" . $buyer_name . "\n" . " Buyer Email:" . $buyer_email . "\n" . " Checkout Id:" . $checkout_id . "\n" . " Participant Name:" . $reg_col1 . " " . $reg_col2;
                                            $this->sendSnsforfailedcheckout($type1, $description1);
                                        }
                                        $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                        $this->response($this->json($error), 400);
                                    }
                                } else {
                                    $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                    $this->response($this->json($error), 400);
                                }
                            }
                        }
                    }

                    if ($default_cc == 'Y') {
                        $select_card_details = sprintf("SELECT * FROM `payment_method_details` WHERE `company_id`='%s' AND `student_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
                        $result_select_card_details = mysqli_query($this->db, $select_card_details);
                        if (!$result_select_card_details) {
                            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$select_card_details");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal server Error.");
                            $this->response($this->json($error), 500);
                        } else {
                            $select_num_rows = mysqli_num_rows($result_select_card_details);
                            if ($select_num_rows > 0) {
                                $updatequery = sprintf("UPDATE `payment_method_details` SET  `credit_card_id`='%s',`credit_card_status`='%s',`credit_card_name`='%s',`credit_card_expiration_month`='%s',`credit_card_expiration_year`='%s',`buyer_email`='%s',`buyer_name`='%s',`buyer_postal_code`='%s'
                                       ,`country`='%s' WHERE `student_id`='%s' ", mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name)
                                        , mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name)
                                        , mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $student_id));

                                $resultupdatequery = mysqli_query($this->db, $updatequery);
                                if (!$resultupdatequery) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$updatequery");
                                    mlog_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            } else {
                                $insertquery = sprintf("INSERT INTO `payment_method_details`(`company_id`, `student_id`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `buyer_email`, `buyer_name`, `country`, `buyer_postal_code`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $buyer_postal_code));
                                $resultinsertquery = mysqli_query($this->db, $insertquery);
                                if (!$resultinsertquery) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$insertquery");
                                    mlog_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }


                    $query = sprintf("INSERT INTO `trial_registration`(`company_id`, `trial_id`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                        `payment_type`, `payment_amount`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`,  `start_date`, `end_date`, `trial_status`, `discount`,
                        `trial_registration_column_1`, `trial_registration_column_2`, `trial_registration_column_3`, `trial_registration_column_4`, `trial_registration_column_5`, `trial_registration_column_6`, `trial_registration_column_7`, `trial_registration_column_8`, `trial_registration_column_9`, `trial_registration_column_10`,
                        `trial_reg_type_user`,`trial_reg_version_user`,`program_length`,`program_length_type`,`trial_participant_street`, `trial_participant_city`, `trial_participant_state`, `trial_participant_zip`, `trial_participant_country`)
                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $start_date), mysqli_real_escape_string($this->db, $end_date), mysqli_real_escape_string($this->db, 'A'), mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4), mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $program_length), mysqli_real_escape_string($this->db, $program_length_type), mysqli_real_escape_string($this->db, $participant_street), mysqli_real_escape_string($this->db, $participant_city), mysqli_real_escape_string($this->db, $participant_state), mysqli_real_escape_string($this->db, $participant_zip), mysqli_real_escape_string($this->db, $participant_country));
                    $result = mysqli_query($this->db, $query);
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $trial_reg_id = mysqli_insert_id($this->db);

                        if ($payment_amount > 0) {
                            $pstatus = 'S';
                            $payment_query = sprintf("INSERT INTO `trial_payment`(`trial_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `payment_date`, `payment_status`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $pstatus), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name));
                            $payment_result = mysqli_query($this->db, $payment_query);
                            if (!$payment_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                                mlog_info($this->json($error_log));
                            }
                        }

                        if ($processing_fee_type == 1) {
                            $paid_amount -= $processing_fee;
                        }


                        $update_trial_query = sprintf("UPDATE `trial` SET `registrations_count` = `registrations_count`+1,`active_count`=`active_count`+1  WHERE `trial_id`='%s'", mysqli_real_escape_string($this->db, $trial_id));
                        $update_trial_result = mysqli_query($this->db, $update_trial_query);
                        if (!$update_trial_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial_query");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }


                        $selectcurrency = sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'", mysqli_escape_string($this->db, $company_id));
                        $resultselectcurrency = mysqli_query($this->db, $selectcurrency);
                        if (!$resultselectcurrency) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                            mlog_info($this->json($error_log));
                        } else {
                            $row = mysqli_fetch_assoc($resultselectcurrency);
                            $wp_currency_symbol = $row['wp_currency_symbol'];
                        }
                        $activity_text = 'Registration date.';
                        $activity_type = "registration";

                        if (!empty($discount_code)) {
                            $activity_text .= " Trial fee discount code used is " . "$discount_code";
//                        ."$wp_currency_symbol".$discount
                        }

                        $curr_date = gmdate("Y-m-d H:i:s");
                        $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), $activity_type, mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                        $result_history = mysqli_query($this->db, $insert_history);
                        if (!$result_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }

                        $this->updateTrialDimensionsMembers($company_id, $trial_id, 'A', '', $trial_reg_id);
                        $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
                        $msg = array("status" => "Success", "msg" => "Trial program registration was successful. Email confirmation sent.", "memberapp_details" => $student_info_details);
                        $this->responseWithWepay($this->json($msg), 200);
                        $this->sendOrderReceiptForTrialPayment($company_id, $trial_reg_id);
                    }
                }
            } else {
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error), 200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

    public function sendOrderReceiptForTrialPayment($company_id, $trial_reg_id) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $created_date = " DATE(CONVERT_TZ(tp.created_dt,$tzadd_add,'$new_timezone'))";

        $waiver_policies = '';
        $query = sprintf("SELECT tr.`trial_status`, tr.`buyer_name`, tr.`buyer_email`, tr.`buyer_phone`, tr.`buyer_postal_code`,tr.`start_date`,tr.`end_date`,
                tr.`registration_date`, tr.`registration_amount`, tr.`processing_fee_type`, tr.`payment_type`, c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,s.`student_cc_email`,
                tr.`program_length`, tr.`program_length_type`, concat(tr.`trial_registration_column_2`,',',tr.`trial_registration_column_1`) participant,  tr.`trial_registration_column_4`
                lead_Source,t.`trial_title`,t.`trial_waiver_policies`,tp.`payment_date`,tp.`payment_amount`,tp.`processing_fee`,tp.`payment_status`,tp.`refunded_amount`,tp.`refunded_date`,if(tp.`payment_from`='S',tp.`stripe_card_name`,tp.`cc_name`) as cc_name,DATE($created_date) created_dt,tp.`last_updt_dt` FROM `trial_registration` tr 
                left join `company` c on(c.`company_id`=tr.`company_id`) left join `student` s on(tr.`student_id`=s.`student_id`) left join `trial` t
                on(t.`trial_id`=tr.`trial_id`) left join `trial_payment` tp on(tr.`trial_reg_id`=tp.`trial_reg_id`) 
                WHERE tr.`trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
            exit();
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {

                $trial_name = $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $student_cc_email_list = $wp_currency_code = $wp_currency_symbol = $processing_fee_type = '';
                $current_plan_details = $trial_details = $payment_details = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $paid_amt = 0;
                    $trial_name = $row['trial_title'];
                    $participant_name = $row['participant'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['trial_waiver_policies'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $payment_status = $row['payment_status'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $student_cc_email_list = $row['student_cc_email'];
                    $start_date = date("M dS, Y", strtotime($row['start_date']));
                    $end_date = date("M dS, Y", strtotime($row['end_date']));
                    $temp = [];

                    if ($row['processing_fee_type'] == 2) {
                        $payment_amount = $temp['amount'] = $row['payment_amount'] + $row['processing_fee'];
                    } else {
                        $payment_amount = $temp['amount'] = $row['payment_amount'];
                    }
                    $temp['date'] = $row['payment_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
//                    $temp['edit_status'] = 'N';
                    $temp['payment_status'] = $row['payment_status'];
                    $temp['processing_fee'] = $row['processing_fee'];
                    $temp['cc_name'] = $row['cc_name'];


                    $payment_details[] = $temp;
                }
            } else {
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "No records found.");
                $this->response($this->json($error), 200);
                exit();
            }
        }

        $subject = $trial_name . " Order Confirmation";
        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
        $message .= "<b>Order Details</b><br><br>";
        $message .= "$trial_name" . "<br>";
        $message .= "Starts: " . "$start_date" . "<br>";
        $message .= "Ends: " . "$end_date" . "<br>";
        $message .= "Participant: " . "$participant_name" . "<br><br>";

        if ($payment_status == 'S' && $payment_details[0]['amount'] > 0) {
            $message .= "<b>Billing Details</b><br><br>";
            $message .= "Total Due: " . "$wp_currency_symbol$payment_amount" . "<br>";
            if (count($payment_details) > 0) {
                $pf_str = "";
                $pr_fee = $payment_details[0]['processing_fee'];
                if ($processing_fee_type == 2) {
                    $pf_str = " (includes $wp_currency_symbol" . $pr_fee . " administrative fees)";
                }
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $final_amt = $amount - $refunded_amount;
                $payment_status = $payment_details[0]['payment_status'];
//                        $date = date("M dS, Y", strtotime($payment_details[0]['date']));
                $paid_str = "Amount paid: ";
                if ($payment_status == 'S') {
                    $pay_type = "(" . $payment_details[0]['cc_name'] . ")";
                } else {
                    $pay_type = "(manual credit applied)";
                }
                $message .= " $paid_str" . "$wp_currency_symbol$payment_amount" . "$pf_str$pay_type" . " <br>";
            }
            $message .= "<br><br>";
        }

        $waiver_present = 0;
        $file = '';
        if (!empty(trim($waiver_policies))) {
            $waiver_present = 1;
            $file_name_initialize = 10;
            for ($z = 0; $z < $file_name_initialize; $z++) {
                $dt = time();
                $file = "../../../uploads/" . $dt . "_waiver_policies.html";
                if (file_exists($file)) {
                    sleep(1);
                    $file_name_initialize++;
                } else {
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }

        $sendEmail_status = $this->sendEmailForTrial($studio_mail, $buyer_email, $subject, $message, $studio_name, $studio_mail, $file, $waiver_present, $cc_email_list, $student_cc_email_list);
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            mlog_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            mlog_info($this->json($error_log));
        }
        date_default_timezone_set($curr_time_zone);
    }

    public function sendEmailForTrial($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list, $student_cc_email_list) {

        if (empty(trim($cmp_name))) {
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try {
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if (filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)) {
                $mail->AddReplyTo($reply_to, $cmp_name);
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if (filter_var(trim($from), FILTER_VALIDATE_EMAIL)) {
                $mail->AddCC($from);
            }
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    if (filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)) {
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if (!empty(trim($student_cc_email_list))) {
                $student_cc_addresses = explode(',', $student_cc_email_list);
                for ($init_1 = 0; $init_1 < count($student_cc_addresses); $init_1++) {
                    if (filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)) {
                        $mail->AddCC($student_cc_addresses[$init_1]);
                    }
                }
            }

            $mail->Subject = $subject;
            $mail->Body = $message;
            if ($waiver_present == 1) {
                //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        } catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if (file_exists($waiver_file)) {
            unlink($waiver_file);
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        mlog_info($this->json($error_log));
        return $response;
    }

    public function addTrialDimensions($company_id, $trial_id, $period) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if (!empty($period)) {
            $curr_dt = date("Y-m", strtotime($period));
        } else {
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));


        $sql2 = sprintf("SELECT * FROM `trial_dimensions` WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows1 = mysqli_num_rows($result2);
            if ($num_of_rows1 == 0) {
                $insertquery1 = sprintf("INSERT INTO `trial_dimensions`(`company_id`, `period`, `trial_id`) VALUES('%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $trial_id));
                $res2 = mysqli_query($this->db, $insertquery1);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    $this->response($this->json($error), 200);
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }

    public function updateTrialDimensionsMembers($company_id, $trial_id, $trial_status, $old_trial_status, $trial_reg_id) {
        $flag = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $date_string = $trial_string = "";
//        $a_date = $e_date = $c_date = $d_date = '';
        $date_str = "%Y-%m";
        $reg_date = '';
        $every_month_dt = date("Y-m-d");

//        if(!empty($old_date['reg'])){
//            $old_date['active'] = $old_date['reg'];
//        }
//        if(!empty($old_date)){
//        $a_date = "'".$old_date['active']."'";
//        $e_date = "'".$old_date['enrolled']."'";
//        $c_date = "'".$old_date['cancelled']."'";
//        $d_date = "'".$old_date['didnotstart']."'";
//       }
        if ($trial_status == 'A') {
            if ($old_trial_status == 'E') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
                $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'C') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'D') {
//                $date_string = "DATE_FORMAT(`active_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } elseif ($trial_status == 'E') {
            if ($old_trial_status == 'A') {
//                $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                $trial_string = "`active_count`=`active_count`-1";
            } else if ($old_trial_status == 'C') {
//                 $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'D') {
//                $date_string = "DATE_FORMAT(`enrolled_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } elseif ($trial_status == 'C') {
            if ($old_trial_status == 'E') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
                $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'A') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                $trial_string = "`active_count`=`active_count`-1";
            } else if ($old_trial_status == 'D') {
//               $date_string = "DATE_FORMAT(`cancelled_date`,'%s')=DATE_FORMAT($d_date,'%s') ";
                $trial_string = "`didnotstart_count`=`didnotstart_count`-1";
            }
        } else if ($trial_status == 'D') {
            if ($old_trial_status == 'E') {
//                 $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($e_date,'%s') ";
                $trial_string = "`enrolled_count`=`enrolled_count`-1";
            } else if ($old_trial_status == 'C') {
//                  $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($c_date,'%s') ";
                $trial_string = "`cancelled_count`=`cancelled_count`-1";
            } else if ($old_trial_status == 'A') {
//                 $date_string = "DATE_FORMAT(`didnotstart_date`,'%s')=DATE_FORMAT($a_date,'%s') ";
                $trial_string = "`active_count`=`active_count`-1";
            }
        }
//
//        $selectquery = sprintf("SELECT * FROM `trial_registration` WHERE $date_string and  `company_id`='%s' and `trial_id`='%s' and `trial_reg_id`='%s'",
//                $date_str, $date_str, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $trial_reg_id));
//        $res = mysqli_query($this->db, $selectquery);
//       if (!$res) {
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
//            log_info($this->json($selectquery));
//            
//        } else {
//            $num_of_rows = mysqli_num_rows($res);
//            if ($num_of_rows == 1 && !empty($old_trial_status)) {
//                $flag = 1;
//            }
//        }
        $selectquery = sprintf("SELECT registration_date FROM `trial_registration` WHERE `company_id`='%s' and `trial_id`='%s' and `trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $trial_reg_id));
        $res = mysqli_query($this->db, $selectquery);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
            mlog_info($this->json($selectquery));
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows == 1) {
                $rows = mysqli_fetch_object($res);
                $reg_date = $rows->registration_date;
            }
        }

        if (!empty($old_trial_status)) {
            if ($trial_status == 'A') {
//                if($flag == 1){
                $trial_string = "$trial_string, `active_count`=`active_count`+1";
//                }else{
//                  $trial_string = "`active_count`=`active_count`+1";
//                }
            } else if ($trial_status == 'E') {
//                if($flag == 1){
                $trial_string = "$trial_string, `enrolled_count`=`enrolled_count`+1";
//                }else{
//                     $trial_string = "`enrolled_count`=`enrolled_count`+1";
//                }
            } else if ($trial_status == 'C') {
//                if($flag == 1){
                $trial_string = "$trial_string, `cancelled_count`=`cancelled_count`+1";
//                }else{
//                     $trial_string = "`cancelled_count`=`cancelled_count`+1";
//                }
            } else if ($trial_status == 'D') {
//                if($flag == 1){
                $trial_string = "$trial_string, `didnotstart_count`=`didnotstart_count`+1";
//                }else{
//                     $trial_string = "`didnotstart_count`=`didnotstart_count`+1";
//                }
            }
        } else {
            $trial_string = "`active_count`=`active_count`+1";
            $reg_date = $every_month_dt;
        }

        if (!empty(trim($trial_string))) {
            $query1 = sprintf("UPDATE `trial_dimensions` SET $trial_string WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') ", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), $reg_date, $date_str);
            $result1 = mysqli_query($this->db, $query1);
//            log_info("dim   ".$query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
        }

        date_default_timezone_set($curr_time_zone);
    }

    public function getTrialDiscountDetails($company_id, $trial_id) {
        $sql = sprintf("SELECT `trial_discount_id` , `discount_type` , `discount_code` , `discount_amount` FROM `trial_discount`
                WHERE `company_id` = '%s' AND `trial_id`='%s' AND `is_expired`!='Y'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
        $result = mysqli_query($this->db, $sql);
        $output = $response = [];
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                $response['status'] = "Success";
            } else {
                $response['status'] = "Failed";
            }
        }
        $response['discount'] = $output;
        return $response;
    }

    public function verifyAppIdInDB($app_id, $device_type) {
        if ($device_type == 'A') {
            $query = sprintf("SELECT * FROM `company` WHERE `android_bundle_id`='%s'", mysqli_real_escape_string($this->db, $app_id));
        } else {
            $query = sprintf("SELECT * FROM `company` WHERE `ios_bundle_id`='%s'", mysqli_real_escape_string($this->db, $app_id));
        }
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows == 1) {
                $row = mysqli_fetch_assoc($result);
                $out['company_id'] = $row['company_id'];
                $out['app_type'] = 'SL';        // SINGLE LOCATION APP
                $this->getStudioSubscriptionStatusapptype($out['company_id'], $out['app_type']);
            } else {
                $out['app_type'] = 'ML';
            }
            $msg = array('status' => "Success", "msg" => $out);
            $this->response($this->json($msg), 200);
        }
    }

    public function checkGuestLogin($company_id, $student_id) {
        // INSERT GUEST LOGIN DATAS INTO DB
        $company_details = $this->getcompanydetails($company_id, $student_id, 1);
        if ($company_details['msg'] == 'Success' && ($company_details['studio_expiry_level'] == 'L2' || $company_details['upgrade_status'] == 'F')) {
            $msg = array('status' => "Failed", 'msg' => 'Please contact the business operator to activate the app.', "company_id" => $company_id, "company_details" => $company_details, "studio_email" => $company_details['email_id'], "upgrade_status" => $company_details['upgrade_status'], "studio_expiry_level" => $company_details['studio_expiry_level']);
            $this->response($this->json($msg), 500);
        } else {
            $msg = array('status' => "Success", 'msg' => 'Login success', "company_details" => $company_details);
            $this->response($this->json($msg), 200);
        }
    }

    public function sendExpiryEmailDetailsToStudio($company_id) {
        $query = sprintf("SELECT c.`email_id`, c.`company_name`, c.`referral_email_list` FROM `company` c WHERE c.`company_id`='%s'", $company_id);
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $company_name = $reply_to = "";
                $row = mysqli_fetch_assoc($result);
                $owner_email_id = $row['email_id'];
                $referral_email_list = $row['referral_email_list'];

                $subject = "A member has requested that you activate your MyStudio account.";
                $body_msg = '<div style="max-width:520px;">';
                $body_msg .= "Members are trying to access your app and/or URL registration links.<br><br>However, your account is currently inactive, please click here to activate your account.<br><br>";
                $body_msg .= '<center><a href="' . $this->server_url . '/mystudioapp.php?pg=login"><button type="button" style="width: 140px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 500;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 40px;font-size: 15px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">Activate</button></a></center>';
                $body_msg .= "</div>";
                $sendEmail_status = $this->sendEmail($owner_email_id, $referral_email_list, $subject, $body_msg, $company_name, $reply_to, true);
                if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
                    $refer_check = 1;
                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $owner_email_id . "   " . $sendEmail_status['mail_status']);
                    mlog_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                } else {
                    $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $owner_email_id);
                    mlog_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Studio details doesn't exist.");
                $this->response($this->json($error), 401);
            }
        }
    }

    public function getStudioSubscriptionStatus($company_id) {
        $query = sprintf("SELECT email_id,upgrade_status,subscription_status,wepay_status,stripe_status,stripe_subscription,studio_expiry_level,expiry_dt FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_email = $row['email_id'];
                $upgrade_status = $row['upgrade_status'];
                $subscription_status = $row['subscription_status'];
                $wepay_status = $row['wepay_status'];
                $stripe_status = $row['stripe_status'];
                $stripe_subscription = $row['stripe_subscription'];
                $studio_expiry_level = $row['studio_expiry_level'];
                $expiry_dt = $row['expiry_dt'];
                if (!empty(trim($upgrade_status)) && $subscription_status == 'N' && ($studio_expiry_level == 'L2' || $upgrade_status == 'F')) {
                    $res = array('status' => "Failed", "msg" => "Please contact the business operator to activate the app.", "company_id" => $company_id, "studio_email" => $studio_email, "upgrade_status" => $upgrade_status, "studio_expiry_level" => $studio_expiry_level);
                    $this->response($this->json($res), 200);
                } else {
                    $res = array('status' => "Success", "msg" => "Studio subscription status success", "upgrade_status" => $upgrade_status);
                    return $res;
                }
            } else {
                $res = array('status' => "Failed", "msg" => "Studio Details not found.");
                $this->response($this->json($res), 200);
            }
        }
    }

    public function getStudioSubscriptionStatusapptype($company_id, $app_type) {
        $query = sprintf("SELECT email_id,upgrade_status,subscription_status,wepay_status,stripe_status,stripe_subscription,studio_expiry_level,expiry_dt FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_email = $row['email_id'];
                $upgrade_status = $row['upgrade_status'];
                $subscription_status = $row['subscription_status'];
                $wepay_status = $row['wepay_status'];
                $stripe_status = $row['stripe_status'];
                $stripe_subscription = $row['stripe_subscription'];
                $studio_expiry_level = $row['studio_expiry_level'];
                $expiry_dt = $row['expiry_dt'];
                if (!empty(trim($upgrade_status)) && $subscription_status == 'N' && ($studio_expiry_level == 'L2' || $upgrade_status == 'F')) {
                    $res = array('status' => "Failed", "msg" => "Please contact the business operator to activate the app.", "company_id" => $company_id, "studio_email" => $studio_email, "upgrade_status" => $upgrade_status, "studio_expiry_level" => $studio_expiry_level, "app_type" => $app_type);
                    $this->response($this->json($res), 200);
                } else {
                    $res = array('status' => "Success", "msg" => "Studio subscription status success");
                    return $res;
                }
            } else {
                $res = array('status' => "Failed", "msg" => "Studio Details not found.");
                $this->response($this->json($res), 200);
            }
        }
    }

    public function checkStudentLoginByOAuth($company_id, $login_type, $user_detail, $user_email, $push_device_id, $device_type, $app_id) {
        $st_email = $user_email;
        $first_name = $last_name = $phone = '';
        $query_str = "`student_mobile`=`student_mobile`";
        if (($login_type == 'Google' || $login_type == 'Facebook') && isset($user_detail)) {
            if (isset($user_detail['email']) && !empty($user_detail['email']) && $user_detail['email'] != 'null' && !is_null($user_detail['email'])) {
                $st_email = $user_detail['email'];
            } else if (isset($user_detail['providerData'][0]['email']) && !empty($user_detail['providerData'][0]['email']) && $user_detail['providerData'][0]['email'] != 'null' && !is_null($user_detail['providerData'][0]['email'])) {
                $st_email = $user_detail['providerData'][0]['email'];
            }
        }
        if (isset($user_detail) && !empty($user_detail)) {
            if (!empty($user_detail['displayName'])) {
                $name = $user_detail['displayName'];
                $last_occurance_space = strripos($name, ' '); // spliting buyer name into two
                $last_name = trim(substr($name, $last_occurance_space));
                $first_name = trim(substr($name, 0, $last_occurance_space));
            }
            if (!empty($user_detail['phoneNumber']) && $user_detail['phoneNumber'] != 'null' && !is_null($user_detail['phoneNumber'])) {
                $phone = mysqli_real_escape_string($this->db, $user_detail['phoneNumber']);
                $query_str = "IF(`student_mobile` IS NULL OR TRIM(`student_mobile`)='', `student_mobile`=`student_mobile`, `student_mobile`='$phone')";
            }
        }
        $stud_array = $this->getStudentId($company_id, $first_name, $last_name, $st_email, $device_type, $app_id, $push_device_id, '', '');
        $student_id = $stud_array['student_id'];

        $update_query = sprintf("UPDATE `student` SET `push_device_id` = '%s', `device_type` = '%s', `app_id`  = '%s', `login_type`='%s', `uid`='%s', `first_login_dt` = IF(`first_login_dt` IS NULL OR `first_login_dt`='0000-00-00 00:00:00', NOW(), `first_login_dt`), `last_login_dt`= NOW(), `deleted_flag`='N', `registration_status` = 'Y', %s WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $push_device_id), mysqli_real_escape_string($this->db, $device_type), mysqli_real_escape_string($this->db, $app_id), mysqli_real_escape_string($this->db, $login_type), mysqli_real_escape_string($this->db, $user_detail['uid']), $query_str, mysqli_real_escape_string($this->db, $student_id));
        $result_query = mysqli_query($this->db, $update_query);
        if (!$result_query) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }

        $this->getcompanydetails($company_id, $student_id, 0);
    }

    public function getStudioCodeForEmail($email) {
        $sql = sprintf("SELECT c.`company_name`, c.`company_code`, c.`company_id`
                    FROM `student` s LEFT JOIN `company` c ON c.`company_id`=s.`company_id` 
                    WHERE s.`student_email`='%s'", mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $output = [];
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 1) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                $error = array('status' => "Conflicts", "msg" => "Email registered with more than one studio.", "Studio_list" => $output);
                $this->response($this->json($error), 200);
            } elseif ($num_rows == 1) {  //return company_id to callback for when email occurs in only one studio
                $row = mysqli_fetch_assoc($result);
                return $row['company_id'];
            } else {
                //Nothing to do
            }
        }
    }

    public function getRetailDetails($company_id) {
        $this->getStudioSubscriptionStatus($company_id);
        $response = $retail_category = $retail_product = $retail_variants = $category_id_list = $product_id_list = $variant_id_list = $variant_product_id_list = [];

        $query = sprintf("SELECT rp.`retail_product_id`, rp.`retail_parent_id`, rp.`company_id`, rp.`retail_product_type`, rp.`retail_product_status`, rp.`retail_product_title`, rp.`retail_product_subtitle`, 
                    rp.`retail_product_url`, rp.`retail_banner_img_content`, rp.`retail_banner_img_url`, rp.`retail_product_desc`, rp.`retail_product_price`, rp.`retail_product_compare_price`, 
                    rp.`retail_registrations_count`, rp.`retail_net_sales`, rp.`retail_sort_order`, rp.`retail_waiver_policies`, rp.`retail_processing_fees`,
                    rv.`retail_variant_id`, rv.`retail_variants_name`, rv.`inventory_count`
                    FROM `retail_products` rp LEFT JOIN `retail_variants` rv ON rp.`company_id`=rv.`company_id` AND rp.`retail_product_id`=rv.`retail_product_id`
                    WHERE rp.`company_id`='%s' AND rp.`retail_product_status`='P' AND (rp.`retail_product_type` IN ('C', 'S') || 
                    (rp.`retail_product_type`='P' && rp.`retail_parent_id` IN (SELECT `retail_product_id` FROM `retail_products` WHERE `company_id`='%s' AND `retail_product_status`='P' AND `retail_product_type`='C')))
                    ORDER BY rp.`retail_sort_order` desc", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $count_only_check = 1;          //1 - Need only count, 0 - all data
                $disc = $this->getRetailDiscountDetails($company_id, 1, $count_only_check);   //2nd parameter --> 1 - return, 0 - echo and exit
                if ($disc['status'] == 'Success') {
                    if ($count_only_check == 0) {
                        $response['retail_discount'] = $disc['msg'];
                    }
                    $response['retai_discount_available'] = 'Y';
                } else {
                    if ($count_only_check == 0) {
                        $response['retail_discount'] = [];
                    }
                    $response['retai_discount_available'] = 'N';
                }
                while ($row = mysqli_fetch_assoc($result)) {
                    $unique_id = $row['retail_product_id'];
                    $row_category = array_slice($row, 0, 18);
                    if (($row['retail_product_type'] == 'C' || $row['retail_product_type'] == 'S') && !empty($unique_id) && !in_array($unique_id, $category_id_list, true)) {
                        $row_category['products'] = [];
                        $category_id_list[] = $unique_id;
                        $retail_category[] = $row_category;
                    }
                    if ($row['retail_product_type'] == 'P' && !empty($row['retail_parent_id']) && !empty($unique_id) && !in_array($unique_id, $product_id_list, true)) {
                        $product_id_list[] = $unique_id;
                        $retail_product[] = $row_category;
                    }
                    $row_variants = array_slice($row, 18, 3);
                    if (!empty($row_variants['retail_variant_id']) && !in_array($row_variants['retail_variant_id'], $variant_id_list)) {
                        $variant_id_list[] = $row_variants['retail_variant_id'];
                        $variant_product_id_list[] = $row_variants['retail_product_id'] = $unique_id;
                        $retail_variants[] = $row_variants;
                    }
                }
                if (count($retail_category) > 0 && count($retail_variants) > 0) {
                    for ($i = 0; $i < count($retail_category); $i++) {
                        $key_arr = [];
                        if ($retail_category[$i]['retail_product_type'] == 'S' && in_array($retail_category[$i]['retail_product_id'], $variant_product_id_list)) {
                            $key_arr = array_keys($variant_product_id_list, $retail_category[$i]['retail_product_id']);
                            for ($j = 0; $j < count($key_arr); $j++) {
                                $retail_category[$i]['variants'][] = $retail_variants[$key_arr[$j]];
                            }
                        }
                    }
                }
                if (count($retail_product) > 0) {
                    for ($i = 0; $i < count($retail_product); $i++) {
                        $key_arr = [];
                        if (in_array($retail_product[$i]['retail_product_id'], $variant_product_id_list)) {
                            $key_arr = array_keys($variant_product_id_list, $retail_product[$i]['retail_product_id']);
                            for ($j = 0; $j < count($key_arr); $j++) {
                                $retail_product[$i]['variants'][] = $retail_variants[$key_arr[$j]];
                            }
                        }
                    }
                    for ($i = 0; $i < count($retail_product); $i++) {
                        if (in_array($retail_product[$i]['retail_parent_id'], $category_id_list)) {
                            $retail_category[array_search($retail_product[$i]['retail_parent_id'], $category_id_list)]['products'][] = $retail_product[$i];
                        }
                    }
                }
                $response['retail_category'] = $retail_category;
                $out = array('status' => "Success", 'msg' => $response);
                $this->response($this->json($out), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Retail Product details doesn't exist.");
                $this->response($this->json($error), 401); // If no records "No Content" status
            }
        }
    }

    //Get Discount details by Event ID
    protected function getRetailDiscountDetails($company_id, $call_back, $count_only_check) {
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $created_date = " DATE(CONVERT_TZ(NOW(),$tzadd_add,'$new_timezone'))";

        $sql = sprintf("SELECT `retail_discount_id`, `company_id`, `retail_discount_type`, `retail_discount_code`, `retail_discount_amount`, `min_purchase_flag`, `min_purchase_quantity`, `min_purchase_amount`, `discount_begin_dt`, `discount_end_dt`, `discount_limit_flag`, `discount_limit_value` FROM `retail_discount`
                WHERE `company_id` = '%s' AND `deleted_flag`='N' AND (`discount_end_dt` IS NULL OR `discount_end_dt`>$created_date)", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        $output = $response = [];
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $response['status'] = "Success";
                if ($count_only_check == 1) {
                    $response['msg'] = "Discount code available for this studio";
                    if ($call_back == 1) {
                        return $response;
                    } else {
                        $this->response($this->json($response), 200);
                    }
                }
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
            } else {
                $response['status'] = "Failed";
            }
            $response['msg'] = $output;
            if ($call_back == 1) {
                return $response;
            } else {
                $this->response($this->json($response), 200);
            }
        }
    }

    protected function updatePhoneLogoutinDb($company_id, $student_id) {

        $sql1 = sprintf("UPDATE `student` SET `push_device_id`= NULL, logged_out_time=NOW() WHERE `company_id`='%s' AND `student_id`='%s' ", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $error_log = array('status' => "Success", "msg" => "push device id cleared successfully.");
            mlog_info($this->json($error_log));
        }
    }

    protected function checkCountryPaymentSupport($company_id, $country, $callback) { // GET COUNTRY PAYMENT SUPPORT
        $row = [];
        $sql = sprintf("SELECT * FROM country_payment_specific WHERE `payment_support` = 'S' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        UNION
                        SELECT * FROM country_payment_specific WHERE `payment_support` = 'W' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        ORDER BY `payment_support` ASC, `company_id` DESC", mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id)
                , mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                if ($callback == 0) {
                    $out = array("status" => "Success", "msg" => $row);
                } else {
                    $row['status'] = 'Success';
                    return $row;
                }
                $this->response($this->json($out), 200);
            } else {
                $out = array("status" => "Failed", "msg" => "Studio country is not yet available for payment support.");
                return $out;
            }
        }
    }

    public function getCompanyStripeAccount($company_id, $call_back) {
        $query = sprintf("SELECT * FROM `stripe_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                if ($call_back == 1) {
                    return $row;
                } else {
                    $msg = array("status" => "Success", "msg" => "Stripe Account details", "stripe_details" => "$row");
                    $this->response($this->json($msg), 200);
                }
            } else {
                if ($call_back == 1) {
                    return '';
                } else {
                    $msg = array("status" => "Failed", "msg" => "Stripe Account details", "stripe_details" => "");
                    $this->response($this->json($msg), 200);
                }
            }
        }
    }

    public function sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name) {

        // EMAIL CONTENT
        $button_text = 'Launch';
        $subject = 'Stripe authenticate';
        $message = '';
        $message .= '<center><a href="' . $button_url . '"><button type="button" style="width: 230px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;padding: 12px 0;height: 50px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">' . $button_text . '</button></a></center>';
        $sendEmail_status = $this->sendEmail($buyer_email, $subject, $message, $studio_name, '', '', '', '', $company_id, '');
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            mlog_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            mlog_info($this->json($error_log));
        }
    }

    public function stripetrialProgramCheckoutcreate($push_device_id, $device_type, $app_id, $reg_version_user, $company_id, $trial_id, $student_id, $participant_id, $trial_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $registration_amount, $payment_amount, $discount, $discount_code, $processing_fee_type, $fee, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $upgrade_status, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $program_length, $program_length_type, $start_date, $registration_date, $payment_method_id, $intent_method_type, $cc_type, $stripe_customer_id, $default_cc, $reg_type_user) {
        //  MOBILE MODEL PARAMETER ADJUSTMENTS
        $actual_student_id = $student_id;
        $actual_participant_id = $participant_id;
        $payment_method = "CC";
        $has_3D_secure = 'N';
        $button_url = '';
        $buyer_name = $buyer_first_name . ' ' . $buyer_last_name;
        $payment_support = 'S';

        $payment_from = $registration_from = '';
        $stripe_account_id = $stripe_currency = $studio_name = $studio_stripe_status = '';
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
        if (($stripe_account != '' && $stripe_account['charges_enabled'] == 'Y' && $payment_amount > 0) || $payment_amount == 0) {  // ALLOWS FOR CASH & CHECK, IF CC, CHECK THE CHARGES ENABLED TO BE 'Y'
            $upgrade_status = $sts['upgrade_status'];
            $this->addTrialDimensions($company_id, $trial_id, '');

            $query1 = sprintf("SELECT c.`company_name`,c.`stripe_status`,s.`account_state`,s.`currency`,s.`account_id` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error1), 200);
            } else {
                if (mysqli_num_rows($result1) > 0) {
                    $row = mysqli_fetch_assoc($result1);
                    $stripe_account_id = $row['account_id'];
                    $stripe_currency = $row['currency'];
                    $stripe_account_state = $row['account_state'];
                    $studio_name = $row['company_name'];
                    $studio_stripe_status = $row['stripe_status'];
                }
            }

            if (is_null($actual_student_id) || $actual_student_id == 0) {
                $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $device_type, $app_id, $push_device_id, 'Stripe', $studio_name);
                $student_id = $stud_array['student_id'];
                $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
            } else {
                $student_id = $actual_student_id;
                if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                    $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                }
            }
            if (is_null($actual_participant_id) || $actual_participant_id == 0) {
                $participant_id = $this->getParticipantId($company_id, $student_id, $reg_col1, $reg_col2, $reg_col3);
            } else {
                $participant_id = $actual_participant_id;
            }
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone = $user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";

            $current_date = date("Y-m-d");
            if ($program_length_type == 'D') {
                $end_date = date("Y-m-d", strtotime("+$program_length days", strtotime($start_date)));
            } else {
                $end_date = date("Y-m-d", strtotime("+$program_length week", strtotime($start_date)));
            }

            $p_f = $this->getProcessingFee($company_id, $upgrade_status);
            $process_fee_per = $p_f['PP'];
            $process_fee_val = $p_f['PT'];

            if ($payment_amount > 0 && $payment_method == "CC") {
                $payment_type = 'O';
                $processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
            } else {
                if ($payment_amount > 0) {
                    $payment_type = 'O';
                    $processing_fee = 0;
                } else {
                    $payment_type = 'F';
                    $processing_fee = 0;
                }
            }

            $w_paid_amount = $paid_amount = $paid_fee = $amount_in_dollars = $dest_amount = $dest_amount_in_dollars = $w_paid_amount_round = $dest_amount_round = $insert_paid_amount = 0;
            $checkout_state = $cc_month = $cc_year = '';
            $payment_status = $stripe_card_name = $payment_intent_id = '';
            if ($studio_stripe_status == 'Y' && $stripe_account_state != 'N') {
                if ($payment_amount > 0) {
                    $paid_amount = $payment_amount;
                    $paid_fee = $processing_fee;
                    if ($processing_fee_type == 2) {
                        $w_paid_amount = $payment_amount + $processing_fee;
                    } elseif ($processing_fee_type == 1) {
                        $w_paid_amount = $payment_amount;
                    }
                    $w_paid_amount_round = round($w_paid_amount, 2);
                    $dest_amount = $w_paid_amount - $processing_fee;
                    $dest_amount_round = round($dest_amount, 2);
                    $dest_amount_in_dollars = $dest_amount_round * 100;
                    $amount_in_dollars = $w_paid_amount_round * 100;
                    $p_type = 'trial';
                    $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $p_type, $cc_type);
                    if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'released';
                    } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'requires_action';
                        $has_3D_secure = 'Y';
                        $next_action_type = $payment_intent_result['next_action']['type'];
                        if($next_action_type != "redirect_to_url"){
                            # Payment not supported
                            $out['status'] = 'Failed';
                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                            $this->response($this->json($out), 200);
                        }
                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                    } else {
                        # Invalid status
                        $out['status'] = 'Failed';
                        $out['msg'] = 'Invalid SetupIntent status.';
                        $out['error'] = 'Invalid SetupIntent status';
                        $this->response($this->json($out), 200);
                    }
                }
                if (!empty($payment_method_id)) {

                    try {
                        $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                        $brand = $retrive_payment_method->card->brand;
                        $cc_month = $retrive_payment_method->card->exp_month;
                        $cc_year = $retrive_payment_method->card->exp_year;
                        $last4 = $retrive_payment_method->card->last4;
                        $temp = ' xxxxxx';
                        $stripe_card_name = $brand . $temp . $last4;
                    } catch (\Stripe\Error\Card $e) {
                        // Since it's a decline, \Stripe\Error\Card will be caught     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\RateLimit $e) {
                        // Too many requests made to the API too quickly     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        // Invalid parameters were supplied to Stripe's API     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Authentication $e) {
                        // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\ApiConnection $e) {
                        // Network communication with Stripe failed     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Base $e) {
                        // Display a very generic error to the user, and maybe send yourself an email     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (Exception $e) {
                        // Something else happened, completely unrelated to Stripe     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    }
                    if (isset($err) && !empty($err)) {
                        $error = array('status' => "Failed", "msg" => $err['message']);
                        $this->response($this->json($error), 200);
                    }
                }
            }
            // SAVE CARD DETAILS
            if ($default_cc == 'Y') {
                $this ->setStudentDefaultccForStripe($company_id, $student_id, $payment_support, $payment_method_id, $stripe_customer_id, $stripe_card_name, $cc_month, $cc_year, $buyer_email, $buyer_name, $buyer_postal_code, $country);
            }

            $payment_from = 'S';
            $registration_from = 'S';
            if ($checkout_state == 'released'){
                $insert_paid_amount = $w_paid_amount;
            }else{
                $insert_paid_amount = 0;
            }


            $query = sprintf("INSERT INTO `trial_registration`(`stripe_customer_id`,`registration_from`,`payment_method`,`company_id`, `trial_id`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`,
                    `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                    `payment_type`, `payment_amount`, `payment_method_id`, `stripe_card_name`,`credit_card_expiration_month`, `credit_card_expiration_year`, `start_date`, `end_date`, `trial_status`, `discount`,
                    `trial_registration_column_1`, `trial_registration_column_2`, `trial_registration_column_3`, `trial_registration_column_4`, `trial_registration_column_5`, `trial_registration_column_6`, `trial_registration_column_7`, `trial_registration_column_8`, `trial_registration_column_9`, `trial_registration_column_10`,
                    `trial_reg_type_user`,`trial_reg_version_user`,`program_length`,`program_length_type`,`trial_participant_street`, `trial_participant_city`, `trial_participant_state`, `trial_participant_zip`, `trial_participant_country`, `paid_amount`)
                    VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $registration_from), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $start_date), mysqli_real_escape_string($this->db, $end_date), mysqli_real_escape_string($this->db, 'A'), mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4), mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $program_length), mysqli_real_escape_string($this->db, $program_length_type), mysqli_real_escape_string($this->db, $participant_street), mysqli_real_escape_string($this->db, $participant_city), mysqli_real_escape_string($this->db, $participant_state), mysqli_real_escape_string($this->db, $participant_zip), mysqli_real_escape_string($this->db, $participant_country), mysqli_real_escape_string($this->db, $insert_paid_amount));
            $result = mysqli_query($this->db, $query);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $trial_reg_id = mysqli_insert_id($this->db);

                if ($payment_amount > 0) {
                    $pstatus = 'S';
                    $payment_query = sprintf("INSERT INTO `trial_payment`(`payment_from`,`credit_method`,`trial_reg_id`, `student_id`, `checkout_status`, `processing_fee`, `payment_amount`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`,`stripe_card_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $pstatus), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name));
                    $payment_result = mysqli_query($this->db, $payment_query);
                    if (!$payment_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                        mlog_info($this->json($error_log));
                    }
                }
//                if ($processing_fee_type == 1) {
//                    $paid_amount -= $processing_fee;
//                }

                $update_trial_query = sprintf("UPDATE `trial` SET `registrations_count` = `registrations_count`+1,`active_count`=`active_count`+1, `net_sales`=`net_sales`+$insert_paid_amount  WHERE `trial_id`='%s'", mysqli_real_escape_string($this->db, $trial_id));
                $update_trial_result = mysqli_query($this->db, $update_trial_query);
                if (!$update_trial_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_trial_query");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
                if ($payment_amount > 0) {
                    $this->updateTrialDimensionsNetSales($company_id, $trial_id, '');
                }
                $activity_text = 'Registration date.';
                $activity_type = "registration";

                if (!empty($discount_code)) {
                    $activity_text .= " Trial fee discount code used is " . "$discount_code";
                }

                $curr_date = gmdate("Y-m-d H:i:s");
                $insert_history = sprintf("INSERT INTO `trial_history`(`company_id`, `trial_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $student_id), $activity_type, mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
                
                    $this->updateTrialDimensionsMembers($company_id, $trial_id, 'A', '', $trial_reg_id);
                if ($has_3D_secure == 'N') {
                    $text = "Trial program registration was successful. Email confirmation sent.";
                } else {
                    $text = "Trial charge was processed and waiting for customer action. Kindly review and authenticate the payment.";
                }
                $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
                $msg = array("status" => "Success", "msg" => $text, "memberapp_details" => $student_info_details);
                $this->responseWithWepay($this->json($msg), 200);
                if ($has_3D_secure == 'N') {
                    $this->sendOrderReceiptForTrialPayment($company_id, $trial_reg_id);
                } else {
                    $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name);
                }
            }
            date_default_timezone_set($curr_time_zone);
        } else {
            $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
            $this->response($this->json($error), 200);
        }
    }
    public function updateTrialDimensionsNetSales($company_id, $trial_id, $date) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $mp_last_upd_date=" DATE(CONVERT_TZ(tp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $last_upd_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";
        if(empty($date)){
            $curr_dt = "($currdate_add)";
        }else{
            $curr_dt = "'$date'";
        }
        $date_inc1= "DATE_FORMAT($curr_dt,'%Y-%m')";
        $date_inc2= "DATE_FORMAT($mp_last_upd_date,'%Y-%m')";
        $date_inc3= "DATE_FORMAT($last_upd_date,'%Y-%m')";
        
        $sql = sprintf("SELECT company_id, trial_id,  mp_ud, sum(amount) amount, payment_date FROM (

                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND (tp.payment_status='S' AND tp.checkout_status='released' || tp.payment_status IN ('MF', 'FR') || tp.payment_status='M' AND tp.`credit_method`!='MC')
                  AND %s = %s 
                  
                  UNION
                  
                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, concat('-',if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount)) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND  (tp.`payment_status`='R' 
                  AND IF(tr.registration_from='S', tp.payment_intent_id NOT IN (SELECT payment_intent_id FROM trial_payment WHERE payment_status='FR' AND %s = %s),
                  tp.checkout_id NOT IN (SELECT checkout_id FROM trial_payment WHERE payment_status='FR' AND %s = %s)) || tp.`payment_status`='MR')
                  AND %s = %s
                  )t1
                  GROUP BY t1.`trial_id`"

                 , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc2, $date_inc1, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc3, $date_inc1, $date_inc3, $date_inc1, $date_inc2, $date_inc1);
        
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            mlog_info($this->json($error_log));
            $reg_id = 1;
        } else {
            $trial = $trial_id_arr = [];
            $number_rows = mysqli_num_rows($result);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $temp = $temp2 = [];
                    $temp['trial_id'] = $temp2['trial_id'] = $trial_id = $row['trial_id'];
                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
                    $temp2['amount'] = $amount = $row['amount'];
                    $temp2['mp_ud'] = $row['mp_ud'];

                    if (!empty($trial_id) && !in_array($trial_id, $trial_id_arr, true)) {
                        $trial_id_arr[] = $trial_id;
                        $trial[] = $temp;
                    }
                    $option[] = $temp2;
                }

                for ($i = 0; $i < count($option); $i++) {
                    $date_init = 0;
                   
                    $date_str = "%Y-%m";
                    $curr_dt = date("Y-m-d");
                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
//                     
                    $sql1 = sprintf("SELECT * FROM `trial_dimensions` WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')", mysqli_real_escape_string($this->db, $company_id), $option[$i]['trial_id'], $every_month_dt, $date_str);
                    $res1 = mysqli_query($this->db, $sql1);
                    if (!$res1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        mlog_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
//                           

                        $num_of_rows = mysqli_num_rows($res1);
                        if ($num_of_rows == 1) {

                            if($i==0){
                                    $sales_value = "'%s'";
                                }else{
                                    $sales_value = "`sales_amount`+'%s'";
                                }
                                $sql3 = sprintf("UPDATE `trial_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `trial_id`='%s' ",mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str,mysqli_real_escape_string($this->db, $trial_id));
                                $res3 = mysqli_query($this->db, $sql3);
                                if (!$res3) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                                    mlog_info($this->json($error_log));
                                    $reg_id = 1;
                                }
                           
                        }
                    }
                }
            } else {
                $sql3 = sprintf("UPDATE `trial_dimensions` SET `sales_amount`=0 WHERE `company_id`='%s' AND `period`=%s AND `trial_id`='%s' ",mysqli_real_escape_string($this->db, $company_id), $date_inc1, mysqli_real_escape_string($this->db, $trial_id));
                $res3 = mysqli_query($this->db, $sql3);
                if (!$res3) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                    mlog_info($this->json($error_log));
                }
                $error = array('status' => "Failed", "msg" => "No records.", "query" => "$sql");
                mlog_info($this->json($error));
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for updateTrialDimensionsNetSales(), check log";
            $this->sendEmailForAdmins($msg);
        }
    }

    public function payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $p_type, $cc_type) {
        if ($cc_type == 'N') {
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try {
            $payment_intent_create = \Stripe\PaymentIntent::create([
                        'payment_method' => $payment_method_id,
                        'customer' => $stripe_customer_id,
                        'amount' => $amount_in_dollars,
                        'currency' => $stripe_currency,
                        'confirmation_method' => 'automatic',
                        'capture_method' => 'automatic',
                        'confirm' => true,
                        'metadata' => ['c_id' => $company_id, 'p_type' => $p_type, 's_id' => $student_id, 's_name' => $studio_name],
                        'setup_future_usage' => 'off_session',
                        'description' => 'Payment Intent for ' . $buyer_email,
                        'payment_method_types' => ['card'],
                        'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                        'receipt_email' => $buyer_email,
                        'transfer_data' => ['destination' => $stripe_account_id, 'amount' => $dest_amount_in_dollars],
                        'on_behalf_of' => $stripe_account_id,
            ]);
            $body3 = json_encode($payment_intent_create);
            stripe_log_info("Stripe Payment Creation : $body3");
            $return_array['temp_payment_status'] = $payment_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded') {
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['cc_month'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_month;
                $return_array['cc_year'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_year;
                $brand = $payment_intent_create->charges->data[0]->payment_method_details->card->brand;
                $last4 = $payment_intent_create->charges->data[0]->payment_method_details->card->last4;
                $temp = ' xxxxxx';
                $return_array['stripe_card_name'] = $brand . $temp . $last4;
            } else if ($return_array['temp_payment_status'] == 'requires_action') {
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['payment_intent_client_secret'] = $payment_intent_create->client_secret;
                $return_array['next_action'] = $payment_intent_create->next_action;
//                $return_array['next_action']['type'] = $payment_intent_create->next_action->type;
//                $return_array['next_action']['redirect_to_url']['url'] = $payment_intent_create->next_action->redirect_to_url->url;
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }
//    Muthulakshmi
    public function setup_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$studio_name,$buyer_email,$stripe_account_id,$p_type,$cc_type){
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        if ($cc_type == 'N') {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }
        try {
            $setup_intent_create = \Stripe\SetupIntent::create([
                'payment_method' => $payment_method_id,
                'customer' => $stripe_customer_id,
                'confirm' => true,
                'metadata' =>['c_id' => $company_id,'p_type' => $p_type,'s_id' => $student_id ,'s_name' => $studio_name],
                'description' => 'Payment Intent for ' . $buyer_email,
                'payment_method_types' => ['card'],
                'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                'on_behalf_of' => $stripe_account_id,
            ]);
            $body3 = json_encode($setup_intent_create);
            stripe_log_info("Stripe Setup Intent Creation : $body3");
            $return_array['temp_payment_status'] = $setup_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded'){
                $return_array['payment_intent_id'] = $setup_intent_create->id; // return response
                $return_array['setup_intent_client_secret'] = $setup_intent_create->client_secret;
            }else if ($return_array['temp_payment_status'] == 'requires_action'){
                $return_array['payment_intent_id'] = $setup_intent_create->id; // return response
                $return_array['setup_intent_client_secret'] = $setup_intent_create->client_secret;
                $return_array['next_action']['type'] = $setup_intent_create->next_action->type;
                $return_array['next_action']['use_stripe_sdk']['type'] = $setup_intent_create->next_action->use_stripe_sdk->type;
                $return_array['next_action']['use_stripe_sdk']['stripe_js'] = $setup_intent_create->next_action->use_stripe_sdk->stripe_js;         
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }

    protected function setupIntentCreation($company_id, $actual_student_id, $payment_method_id, $buyer_email, $buyer_first_name, $buyer_last_name, $stripe_customer_id, $for) {
        $out = [];
        $studio_name = $stripe_customer_id = "";

        $query2 = sprintf("SELECT c.`company_name`,s.`account_id` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result2 = mysqli_query($this->db, $query2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $row = mysqli_fetch_assoc($result2);
            $stripe_account_id = $row['account_id'];
            $studio_name = $row['company_name'];
        }

        if (is_null($actual_student_id) || $actual_student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, '', '', '', 'Stripe', $studio_name);
            $student_id = $stud_array['student_id'];
            $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
        } else {
            $student_id = $actual_student_id;
            if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
            }
        }

        if (is_null($stripe_customer_id) || empty($stripe_customer_id)) {  //Creates a new customer if there is no previous customer - arun
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");

            try {
                $stripe_customer_create = \Stripe\Customer::create([
                            "email" => $buyer_email,
                            "description" => 'Customer created with ' . $buyer_email . ' for ' . $studio_name . ' studio(' . $company_id . ')',
                ]);
                $body = json_encode($stripe_customer_create);
                stripe_log_info("Customer_new : $body");
                $body_arr = json_decode($body, true);
                $stripe_customer_id = $body_arr['id'];
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }

            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
            //update stripe customer id to db - arun 
            $query2 = sprintf("UPDATE `student` SET `stripe_customer_id` = '%s' WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $student_id));
            $result2 = mysqli_query($this->db, $query2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }

        //attaching customer to payment method - arun 
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try {
            $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
            $payment_method->attach(['customer' => $stripe_customer_id]);
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught       
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        // creatiing setup indent to process payment - arun 
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try {
            $setup_intent_create = \Stripe\SetupIntent::create([
                        'payment_method_types' => ['card'],
                        'customer' => $stripe_customer_id,
                        'payment_method' => $payment_method_id,
                        'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                        'confirm' => true,
                        'metadata' => ['c_id' => $company_id, 'p_type' => $for, 's_id' => $student_id, 's_name' => $studio_name],
                        'on_behalf_of' => $stripe_account_id, // only accepted for card payments
            ]);
//            $body3 = json_encode($setup_intent_create);
//            stripe_log_info("Stripe Setup Intent Creation : $body3");
            $return_array['temp_payment_status'] = $setup_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded') {
                $return_array['payment_intent_id'] = $setup_intent_create->id; // return response
                $return_array['setup_intent_client_secret'] = $setup_intent_create->client_secret;
                $return_array['stripe_customer_id'] = $setup_intent_create->customer;
            } else if ($return_array['temp_payment_status'] == 'requires_action') {
                $return_array['payment_intent_id'] = $setup_intent_create->id; // return response
                $return_array['stripe_customer_id'] = $setup_intent_create->customer;
                $return_array['setup_intent_client_secret'] = $setup_intent_create->client_secret;
                $return_array['next_action']['type'] = $setup_intent_create->next_action->type;
                $return_array['next_action']['use_stripe_sdk']['type'] = $setup_intent_create->next_action->use_stripe_sdk->type;
                $return_array['next_action']['use_stripe_sdk']['stripe_js'] = $setup_intent_create->next_action->use_stripe_sdk->stripe_js;
            } else {
                # Invalid status
                $out['status'] = 'Failed';
                $out['msg'] = 'Invalid SetupIntent status.';
                $out['error'] = 'Invalid SetupIntent status';
                $this->response($this->json($out), 200);
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught       
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }

        // Intent retrieve
        try {
            $setup_intent_retrieve = \Stripe\SetupIntent::retrieve($return_array['payment_intent_id']); // return response
            $return_array['intent'] = $setup_intent_retrieve;
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }

        return $return_array;
    }

    public function getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $device_type, $app_id, $push_device_id, $payment_processor, $studio_name) {
        $select_student = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $company_id));
        $result_select_student = mysqli_query($this->db, $select_student);
        if (!$result_select_student) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student);
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result_select_student) > 0) {
                $r = mysqli_fetch_assoc($result_select_student);
                $student_id = $r['student_id'];
                if (is_null($r['stripe_customer_id']) || empty(trim($r['stripe_customer_id']))) {
                    if ($payment_processor == 'Stripe') {
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    } else {
                        $stripe_customer_id = $r['stripe_customer_id'];
                    }
                } else {
                    $stripe_customer_id = $r['stripe_customer_id'];
                }
            } else {
                $insert_student = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_lastname`, `student_email`, `device_type`, `app_id`, `push_device_id`) VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $device_type), mysqli_real_escape_string($this->db, $app_id), mysqli_real_escape_string($this->db, $push_device_id));
                $result_insert_student = mysqli_query($this->db, $insert_student);
                if (!$result_insert_student) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student);
                    $this->response($this->json($error), 200);
                } else {
                    $student_id = mysqli_insert_id($this->db);
                    if ($payment_processor == 'Stripe') {
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    } else {
                        $stripe_customer_id = '';
                    }
                }
            }
            return array("student_id" => $student_id, "stripe_customer_id" => $stripe_customer_id);
        }
    }

    public function createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name) {
        $stripe_customer_id = '';
        $query = sprintf("SELECT `stripe_customer_id` FROM `student` WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                $stripe_customer_id = $row['stripe_customer_id'];
            }
        }
        if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");

            try {
                $stripe_customer_create = \Stripe\Customer::create([
                            "email" => $buyer_email,
                            "description" => "Customer created with $buyer_email for $studio_name($company_id)",
                ]);
                $body = json_encode($stripe_customer_create);
                stripe_log_info("Customer_new : $body");
                $body_arr = json_decode($body, true);
                $stripe_customer_id = $body_arr['id'];

                //update stripe customer id to db - arun 
                $query2 = sprintf("UPDATE `student` SET `stripe_customer_id` = '%s' WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $student_id));
                $result2 = mysqli_query($this->db, $query2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }

            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }

        return $stripe_customer_id;
    }

    public function stripeEventpurchase($push_device_id, $device_type, $app_id, $reg_version_user, $company_id, $event_id, $student_id, $participant_id, $event_name, $desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $registration_amount, $payment_amount, $fee, $processing_fee_type, $discount, $quantity, $event_array, $payment_array, $payment_frequency, $payment_type, $p_type, $upgrade_status, $total_order_amount, $total_order_quantity, $event_type, $reg_type_user, $discount_code, $payment_method_id, $intent_method_type, $cc_type, $stripe_customer_id, $payment_method, $payment_start_date, $total_no_of_payments, $default_cc) {
        $payment_from = $text = '';
        $registration_from = $payment_intent_id = '';
        $stripe_account_id = $stripe_currency = $studio_name = '';
        $w_paid_amount = $paid_fee = $paid_amount = 0;
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $actual_student_id = $student_id;
        $actual_participant_id = $participant_id;
        $payment_method = "CC";
        $has_3D_secure = 'N';
        $payment_support = 'S';

        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
        if (($payment_method == "CC" && $stripe_account != '' && $stripe_account['charges_enabled'] == 'Y' && $payment_amount > 0) || $payment_amount == 0) {  // ALLOWS FOR 0 COST, IF CC IT CHECK THE CHARGES ENABLED TO BE 'Y'
            $upgrade_status = $sts['upgrade_status'];
            $querypf = sprintf("SELECT c.`company_name`, sa.`account_id`, sa.`currency` FROM `company` c 
                        LEFT JOIN `stripe_account` sa ON c.`company_id`=sa.`company_id`
                        WHERE c.`company_id`='%s' ", mysqli_real_escape_string($this->db, $company_id));
            $result = mysqli_query($this->db, $querypf);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$querypf");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $stripe_account_id = $row['account_id'];
                    $stripe_currency = $row['currency'];
                    $studio_name = $row['company_name'];
                }
            }

            $gmt_date = gmdate(DATE_RFC822);
            $sns_msg = array("buyer_email" => $buyer_email, "membership_title" => $event_name, "gmt_date" => $gmt_date);
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone = $user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
            $host = $_SERVER['HTTP_HOST'];
            if (strpos($host, 'mystudio.academy') !== false) {
                $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
            } else {
                $tzadd_add = "'" . $curr_time_zone . "'";
            }
            $recurring_payment_status = '';
            $checkout_amount = 0;
            $this->checkQuantityDuringCheckout($company_id, $event_id, $event_array, $quantity, $event_type);

            $buyer_name = $buyer_first_name . ' ' . $buyer_last_name;
            if (is_null($actual_student_id) || $actual_student_id == 0) {
                $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $device_type, $app_id, $push_device_id, 'Stripe', $studio_name);
                $student_id = $stud_array['student_id'];
                $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
            } else {
                $student_id = $actual_student_id;
                if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                    $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                }
            }
            if (is_null($actual_participant_id) || $actual_participant_id == 0) {
                $participant_id = $this->getParticipantIdForEvent($company_id, $student_id, $reg_col1, $reg_col2);
            } else {
                $participant_id = $actual_participant_id;
            }
            $balance_due = $deposit_amt = $pr_fee = $temp_pr_fee = 0;
            $authorize_only = 0;
            $no_of_payments = 1;
            $payment_startdate = $current_date = date("Y-m-d");
            $p_f = $this->getProcessingFee($company_id, $upgrade_status);
            $process_fee_per = $p_f['PP'];
            $process_fee_val = $p_f['PT'];
            if ($fee == 0 && $processing_fee_type == 1) {
                $fee = number_format((float) ($payment_amount * $process_fee_per / 100 + $process_fee_val), 2, '.', '');
            }
            if ($payment_method != 'CC') {
                $fee = 0;
            }
            if (count($payment_array) > 0) {
                $no_of_payments = count($payment_array) - 1;
                for ($i = 0; $i < count($payment_array); $i++) {
                    if (isset($payment_array[$i]['date']) && empty($payment_array[$i]['date'])) {
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error), 400);
                    }
                    if (isset($payment_array[$i]['amount']) && empty($payment_array[$i]['amount'])) {
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error), 400);
                    }
                    if (isset($payment_array[$i]['type']) && empty($payment_array[$i]['type'])) {
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error), 400);
                    }

                    if ($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount'] > 0) {
                        //                    $balance_due = $payment_array[$i]['amount'];
                        $deposit_amt = $payment_array[$i]['amount'];
                        if ($payment_method != 'CC') {
                            $balance_due = $payment_array[$i]['amount'];
                        }
                        $pr_fee = $payment_array[$i]['processing_fee'];
                    } elseif ($payment_array[$i]['type'] == 'D' && $payment_array[$i]['amount'] == 0) {
                        $authorize_only = 1;
                        $no_of_payments = count($payment_array);
                    }
                    $temp_pr_fee += $payment_array[$i]['processing_fee'];
                }
                $fee = $temp_pr_fee;
            } else {
                $pr_fee = $fee;
            }

            usort($event_array, function($a, $b) {
                return $a['event_begin_dt'] > $b['event_begin_dt'];
            });

            if (count($event_array) > 0) {
                for ($i = 0; $i < count($event_array); $i++) {
                    if (isset($event_array[$i]['event_id']) && empty($event_array[$i]['event_id'])) {
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error), 400);
                    }
                    if (isset($event_array[$i]['event_quantity']) && empty($event_array[$i]['event_quantity'])) {
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error), 400);
                    }
                    if (!isset($event_array[$i]['event_cost'])) {
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error), 400);
                    }
                    if (!isset($event_array[$i]['event_discount_amount'])) {
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error), 400);
                    }
                    if (!isset($event_array[$i]['event_payment_amount'])) {
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error), 400);
                    }
                    if (!isset($event_array[$i]['event_begin_dt'])) {
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error), 400);
                    }
                    if (count($payment_array) > 0) {
                        if ($balance_due > 0) {
                            $amt = $event_array[$i]['event_payment_amount'];
                            if ($balance_due >= $event_array[$i]['event_payment_amount']) {
                                if ($balance_due == $amt) {
                                    $t_amt = $amt;
                                } else {
                                    $t_amt = $balance_due - $amt;
                                }
                                $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($t_amt, $processing_fee_type, $process_fee_per, $process_fee_val);
                                $event_array[$i]['balance_due'] = 0;
                                $balance_due = $balance_due - $amt;
                            } else {
                                $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($balance_due, $processing_fee_type, $process_fee_per, $process_fee_val);
                                $event_array[$i]['balance_due'] = $amt - $balance_due;
                                $balance_due = 0;
                            }
                        } elseif ($balance_due == 0) {
                            $event_array[$i]['processing_fee'] = 0;
                            $event_array[$i]['balance_due'] = $event_array[$i]['event_payment_amount'];
                        }
                    } else {
                        if ($payment_method == 'CC') {
                            $event_array[$i]['processing_fee'] = $this->calculateProcessingFee($event_array[$i]['event_payment_amount'], $processing_fee_type, $process_fee_per, $process_fee_val);
                        } else {
                            $event_array[$i]['processing_fee'] = 0;
                        }
                        $event_array[$i]['balance_due'] = 0;
                    }
                }
            }
            $paid_fee = $fee; // for deposit discount
            // Amount calculation
            if ($payment_method == "CC") {
                if ($processing_fee_type == 2) {
                    $w_paid_amount = $payment_amount + $fee;
                } elseif ($processing_fee_type == 1) {
                    $w_paid_amount = $payment_amount;
                }
                if (count($payment_array) > 0) {
                    for ($i = 0; $i < count($payment_array); $i++) {
                        if ($payment_array[$i]['type'] == 'D') {
                            if ($processing_fee_type == 2) {
                                $w_paid_amount = $payment_array[$i]['amount'] + $payment_array[$i]['processing_fee'];
                                $paid_fee = $payment_array[$i]['processing_fee'];
                            } elseif ($processing_fee_type == 1) {
                                $w_paid_amount = $payment_array[$i]['amount'];
                                $paid_fee = $payment_array[$i]['processing_fee'];
                            }
                            break;
                        }
                    }
                }
            } else {
                if ($recurring_payment_status == 'N') {
                    $paid_amount = $payment_amount;
                } else {
                    $paid_amount = $checkout_amount;
                }
            }

            // STRIPE ACTION START
            $amount_in_dollars = $dest_amount = $dest_amount_in_dollars = $w_paid_amount_round = $dest_amount_round = 0;
            $checkout_state = $cc_month = $cc_year = $stripe_card_name = $payment_status = $payment_intent_id = '';
            $next_action_type = $next_action_type2 = $button_url = '';
            $has_3D_secure = 'N';

            if (!empty($stripe_customer_id) && !(is_null($stripe_customer_id)) && !empty($payment_method_id) && $payment_method == 'CC') {

                $this->getStripeKeys();
                \Stripe\Stripe::setApiKey($this->stripe_sk);
                \Stripe\Stripe::setApiVersion("2019-05-16");

                if ($payment_amount > 0) {
                    $w_paid_amount_round = round($w_paid_amount, 2);
                    $dest_amount = $w_paid_amount - $paid_fee;
                    $dest_amount_round = round($dest_amount, 2);
                    $dest_amount_in_dollars = $dest_amount_round * 100;
                    $amount_in_dollars = $w_paid_amount_round * 100;

                    $checkout_type = 'event';
                    if ($intent_method_type == "SI") { // If it is setup intent creation
                        $payment_intent_result = $this->setup_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $studio_name, $buyer_email, $stripe_account_id, $checkout_type, $cc_type);
                    } else { //If it is payment intent creation
                        $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $checkout_type, $cc_type);
                    }
                    if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'released';
                    } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'requires_action';
                        $has_3D_secure = 'Y';
                        $next_action_type = $payment_intent_result['next_action']['type'];
                        if($next_action_type != "redirect_to_url"){
                            # Payment not supported
                            $out['status'] = 'Failed';
                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                            $this->response($this->json($out), 200);
                        }
                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                    } else {
                        # Invalid status
                        $out['status'] = 'Failed';
                        $out['msg'] = 'Invalid SetupIntent status.';
                        $out['error'] = 'Invalid SetupIntent status';
                        $this->response($this->json($out), 200);
                    }
                    $payment_status = 'S';
                }
            }
            if (!empty($payment_method_id)) {

                try {
                    $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                    $brand = $retrive_payment_method->card->brand;
                    $cc_month = $retrive_payment_method->card->exp_month;
                    $cc_year = $retrive_payment_method->card->exp_year;
                    $last4 = $retrive_payment_method->card->last4;
                    $temp = ' xxxxxx';
                    $stripe_card_name = $brand . $temp . $last4;
                } catch (\Stripe\Error\Card $e) {
                    // Since it's a decline, \Stripe\Error\Card will be caught     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\RateLimit $e) {
                    // Too many requests made to the API too quickly     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\InvalidRequest $e) {
                    // Invalid parameters were supplied to Stripe's API     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Authentication $e) {
                    // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\ApiConnection $e) {
                    // Network communication with Stripe failed     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Base $e) {
                    // Display a very generic error to the user, and maybe send yourself an email     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (Exception $e) {
                    // Something else happened, completely unrelated to Stripe     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                }
                if (isset($err) && !empty($err)) {
                    $error = array('status' => "Failed", "msg" => $err['message']);
                    $this->response($this->json($error), 200);
                }
            }

            // SAVE CARD DETAILS
            if ($default_cc == 'Y') {
                $this->setStudentDefaultccForStripe($company_id, $student_id, $payment_support, $payment_method_id, $stripe_customer_id, $stripe_card_name, $cc_month, $cc_year, $buyer_email, $buyer_name, $buyer_postal_code, $country);
            }

            $payment_from = 'S';
            $registration_from = 'S';
            if ($payment_method == 'CA' || $payment_method == 'CH') {
                $payment_status = 'M';
            }

            $query = sprintf("INSERT INTO `event_registration`(`payment_method`,`paid_amount`,`company_id`, `event_id`, `reg_event_type`, `student_id`,`participant_id`, `buyer_first_name`, `buyer_last_name`,`buyer_email`, 
                `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `processing_fee_type`, `processing_fee`,
                    `payment_type`, `payment_amount`, `deposit_amount`, `number_of_payments`, `payment_startdate`, `payment_frequency`, `total_order_amount`, `total_order_quantity`, 
                    `event_registration_column_1`, `event_registration_column_2`, `event_registration_column_3`, `event_registration_column_4`, `event_registration_column_5`, `event_registration_column_6`, `event_registration_column_7`, `event_registration_column_8`, `event_registration_column_9`, `event_registration_column_10`,
                    `event_reg_type_user`,`event_reg_version_user`,`payment_method_id`,`stripe_card_name`,`stripe_customer_id`,`registration_from`)
                        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $paid_amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_type), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $p_type), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $deposit_amt), mysqli_real_escape_string($this->db, $no_of_payments), mysqli_real_escape_string($this->db, $payment_startdate), mysqli_real_escape_string($this->db, $payment_frequency), mysqli_real_escape_string($this->db, $total_order_amount), mysqli_real_escape_string($this->db, $total_order_quantity), mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), mysqli_real_escape_string($this->db, $reg_col4), mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $registration_from));
            $result = mysqli_query($this->db, $query);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $event_reg_id = mysqli_insert_id($this->db);

                if (count($payment_array) > 0) {
                    $temp1 = $temp4 = 0;
                    for ($i = 0; $i < count($payment_array); $i++) {
                        $pdate = $payment_array[$i]['date'];
                        $pamount = $payment_array[$i]['amount'];
                        $ptype = $payment_array[$i]['type'];
                        $pfee = $payment_array[$i]['processing_fee'];
                        if ($ptype != 'D') {
                            $pstatus = 'N';
                            $pi_id = '';
                        } else {
                            $pi_id = $payment_intent_id;
                            if ($payment_method == 'CC') {
                                $pstatus = 'S';
                            } else {
                                $pstatus = 'M';
                            }
                        }

                        $payment_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_date`, `schedule_status`, `credit_method`,`payment_method_id`,`stripe_card_name`,`payment_intent_id`,`payment_from`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $pfee), mysqli_real_escape_string($this->db, $pamount), mysqli_real_escape_string($this->db, $pdate), mysqli_real_escape_string($this->db, $pstatus)
                                , mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $pi_id), mysqli_real_escape_string($this->db, $payment_from));
                        $payment_result = mysqli_query($this->db, $payment_query);
                        if (!$payment_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                            mlog_info($this->json($error_log));
                            $temp1 += 1;
                        }
                    }
                    if ($temp1 > 0) {
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp1);
                        mlog_info($this->json($error));
                        $this->response($this->json($error), 500);
                    }
                } else {
                    if ($payment_method == 'CC') {
                        $pstatus = 'S';
                    } else {
                        $pstatus = 'M';
                    }
                    $payment_query = sprintf("INSERT INTO `event_payment`(`credit_method`,`event_reg_id`, `student_id`, `checkout_status`, `processing_fee`, `payment_amount`, `schedule_status`, `payment_method_id`,`stripe_card_name`,`payment_intent_id`,`payment_from`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $pstatus), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $payment_from));
                    $payment_result = mysqli_query($this->db, $payment_query);
                    if (!$payment_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }

                if (count($event_array) > 0) {
                    $qty = $net_sales = $parent_net_sales = 0;
                    $temp = $temp2 = $temp3 = 0;
                    for ($i = 0; $i < count($event_array); $i++) {
                        $cevent_id = $event_array[$i]['event_id'];
                        $cquantity = $event_array[$i]['event_quantity'];
                        $cevent_cost = $event_array[$i]['event_cost'];
                        $cdiscount = $event_array[$i]['event_discount_amount'];
                        $cpayment_amount = $event_array[$i]['event_payment_amount'];
                        $cbalance = $event_array[$i]['balance_due'];
                        $cprocessing_fee = $event_array[$i]['processing_fee'];
                        if (count($payment_array) > 0) {
                            if ($cbalance > 0) {
                                $cstatus = 'RP';
                            } else {
                                $cstatus = 'RC';
                            }
                        } else {
                            $cstatus = 'CC';
                        }

                        $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `event_parent_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $cevent_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $cevent_cost), mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cbalance), mysqli_real_escape_string($this->db, $cprocessing_fee), mysqli_real_escape_string($this->db, $cstatus));
                        $reg_det_result = mysqli_query($this->db, $reg_det_query);
                        if (!$reg_det_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                            mlog_info($this->json($error_log));
                            $temp += 1;
                        }

                        $pd_amount = $cpayment_amount - $cbalance;

                        if (count($payment_array) > 0) {
                            if ($processing_fee_type == 2) {
                                $net_sales += $pd_amount;
                                $parent_net_sales += $pd_amount;
                            } elseif ($processing_fee_type == 1) {
                                $net_sales += $pd_amount - $cprocessing_fee;
                                $parent_net_sales += $pd_amount - $cprocessing_fee;
                            }
                        } else {
                            if ($processing_fee_type == 2) {
                                $parent_net_sales = $payment_amount;
                            } else {
                                $parent_net_sales = $payment_amount - $fee;
                            }
                        }
                        $qty += $cquantity;
                        if ($payment_method != 'CC') {
                            $nt_sales_str = ", `net_sales`=`net_sales`+$pd_amount";
                        } else {
                            $nt_sales_str = '';
                        }
//                                $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                        $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$cquantity $nt_sales_str WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $cevent_id));
                        $update_event_result = mysqli_query($this->db, $update_event_query);
                        if (!$update_event_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                            mlog_info($this->json($error_log));
                            $temp2 += 1;
                        }
                    }
                    if ($temp > 0) {
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp);
                        mlog_info($this->json($error));
                        $this->response($this->json($error), 500);
                    }
                    if ($temp2 > 0) {
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "list" => $temp2);
                        mlog_info($this->json($error));
                        $this->response($this->json($error), 500);
                    }
                    if ($payment_method != 'CC') {
                        $nt_sales_str = ", `net_sales`=`net_sales`+$parent_net_sales";
                    } else {
                        $nt_sales_str = '';
                    }

                    $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$qty $nt_sales_str WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                    $update_event_result = mysqli_query($this->db, $update_event_query);
                    if (!$update_event_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                } else {
//                            $sbalance = $payment_amount - $paid_amount;
                    if ($payment_method == 'CC') {
                        $sbalance = $payment_amount;
                    } else {
                        if ($p_type == 'CO') {
                            $sbalance = 0;
                        } else {
                            $sbalance = $payment_amount - $deposit_amt;
                        }
                    }
                    if ($sbalance > 0) {
                        $fee = $pr_fee;
                    }
//                            if($processing_fee_type==1){
//                                $paid_amount -= $fee;
//                            }
                    if (count($payment_array) > 0) {
                        if ($sbalance > 0) {
                            $sstatus = 'RP';
                        } else {
                            $sstatus = 'RC';
                        }
                    } else {
                        $sstatus = 'CC';
                    }
                    $reg_det_query = sprintf("INSERT INTO `event_reg_details`(`event_reg_id`, `event_id`, `student_id`, `event_cost`, `discount`, `payment_amount`, `quantity`, `balance_due`, `processing_fee`, `payment_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $registration_amount), mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $quantity), mysqli_real_escape_string($this->db, $sbalance), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $sstatus));
                    $reg_det_result = mysqli_query($this->db, $reg_det_query);
                    if (!$reg_det_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }

                    if ($payment_method != 'CC') {
                        $nt_sales_str = ", `net_sales`=`net_sales`+$paid_amount";
                    } else {
                        $nt_sales_str = '';
                    }
                    $update_event_query = sprintf("UPDATE `event` SET `registrations_count` = `registrations_count`+$quantity $nt_sales_str WHERE `event_id`='%s'", mysqli_real_escape_string($this->db, $event_id));
                    $update_event_result = mysqli_query($this->db, $update_event_query);
                    if (!$update_event_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
                
                 //update cc information if release state
                if ($checkout_state == 'released') {
                     $this->updateStripeEventCCDetailsAfterRelease($payment_intent_id);
                }
                //End update cc information if release state

                $selectcurrency = sprintf("SELECT  `wp_currency_symbol` FROM `company` WHERE `company_id`='%s'", mysqli_escape_string($this->db, $company_id));
                $resultselectcurrency = mysqli_query($this->db, $selectcurrency);
                if (!$resultselectcurrency) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectcurrency");
                    mlog_info($this->json($error_log));
                } else {
                    $row = mysqli_fetch_assoc($resultselectcurrency);
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                }

                $curr_date = gmdate("Y-m-d H:i:s");
                if ($event_type == 'S') {
                    if (!empty($discount_code)) {
                        $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount . '.Discount code used ' . $discount_code;
                    } else {
                        if (!empty($discount)) {
                            $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount . '.Discount amount used ' . $discount;
                        } else {
                            $activity_text = "Registered for " . $event_name . ',quantity ' . $quantity . '.Total cost ' . $wp_currency_symbol . $payment_amount;
                        }
                    }
                    $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                    $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                    if (!$result_insert_event_history) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                        mlog_info($this->json($error_log));
                    }
                } else {
                    for ($i = 0; $i < count($event_array); $i++) {
                        $event_title = $event_array[$i]['event_title'];
                        $event_quantity = $event_array[$i]['event_quantity'];
                        $event_cost = $event_array[$i]['event_cost'];
                        $event_discount_amount = $event_array[$i]['event_discount_amount'];
                        if (!empty($discount_code)) {
                            $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol" . (($event_cost - $event_discount_amount) * $event_quantity) . '. Discount code used ' . $discount_code;
                        } else {
                            if ($event_discount_amount != 0 || !empty($event_discount_amount)) {
                                $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol" . (($event_cost - $event_discount_amount) * $event_quantity) . ". Discount amount used $wp_currency_symbol" . ($event_discount_amount * $event_quantity);
                            } else {
                                $activity_text = "Registered for $event_name $event_title, quantity $event_quantity. Total cost $wp_currency_symbol" . (($event_cost - $event_discount_amount) * $event_quantity);
                            }
                        }
                        $insert_event_history = sprintf("INSERT INTO `event_history`(`company_id`, `event_reg_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));

                        $result_insert_event_history = mysqli_query($this->db, $insert_event_history);
                        if (!$result_insert_event_history) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_event_history");
                            mlog_info($this->json($error_log));
                        }
                    }
                }
                if ($has_3D_secure == 'N') {
                    if ($payment_amount > 0) {
                        $text = "Registration successful. Confirmation of purchase has been sent to your email.";
                    } else {
                        $text = "Registration successful.";
                    }
                } else {
                    $text = "Event charge was processed and waiting for customer action. Kindly review and authenticate the payment.";
                }
                $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
                $msg = array("status" => "Success", "msg" => $text, "memberapp_details" => $student_info_details);
                $this->responseWithWepay($this->json($msg), 200);
                if ($has_3D_secure == 'N') {
                    $this->sentEmailForEventPaymentSuccess($event_reg_id,$company_id);
                } else {
                    $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name);
                }
            }
        } else {
            $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
            $this->response($this->json($error), 200);
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function updateStripeEventCCDetailsAfterRelease($payment_intent_id){
        $event_reg_id = $event_id = $payment_id = $processing_fee_type = $payment_type = '';
        $payment_amount = 0;
        
        $query = sprintf("SELECT er.company_id, er.`event_reg_id`, ep.`event_payment_id`, e.`event_title`, ep.`payment_amount`, ep.`processing_fee`, er.`processing_fee_type`, er.`event_id`, er.`payment_type`
                FROM `event_registration` er 
                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` 
                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
                WHERE ep.`payment_intent_id` = '%s' ORDER BY ep.`event_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        mlog_info($this->json($query));
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $event_reg_id = $row['event_reg_id'];
                $event_id = $row['event_id'];
                $payment_id = $row['event_payment_id'];
                $payment_amount = $row['payment_amount'];
                $processing_fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_type = $row['payment_type'];
                
                $temp_payment_amount = $payment_amount;
                $onetime_netsales_for_parent_event = 0;

                $update_event_reg_query = sprintf("UPDATE `event_registration` SET `paid_amount`=`paid_amount`+$payment_amount WHERE `event_reg_id`='%s'",$event_reg_id);
                $result_update_event_reg_query = mysqli_query($this->db, $update_event_reg_query);
                if(!$result_update_event_reg_query){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_reg_query", "ipn" => "1");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }

                if($payment_type=='CO'){
                    $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_type`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
                               LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
                               AND `payment_status` IN ('CC') ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                    $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                    if(!$get_event_details_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query", "ipn" => "1");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $num_rows2 = mysqli_num_rows($get_event_details_result);
                        if($num_rows2>0){
                            $rem_due = 0;
                            while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                                if($temp_payment_amount>0){
                                    $event_reg_details_id = $row2['event_reg_details_id'];
                                    $child_id = $row2['event_id'];
                                    $event_parent_id = $row2['parent_id'];
                                    $child_payment = $row2['payment_amount'];
                                    $rem_due = $row2['balance_due'];
                                    $event_type = $row2['event_type'];
                                    $payment_status = $row2['payment_status'];
                                    $child_net_sales = 0;
                                    if($onetime_netsales_for_parent_event==0){
                                        if($processing_fee_type==2){
                                            $net_sales = $payment_amount;
                                        }elseif($processing_fee_type==1){
                                            $net_sales = $payment_amount-$processing_fee;
                                        }
                                        $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
                                        $result_update_event_query = mysqli_query($this->db, $update_event_query);
                                        if(!$result_update_event_query){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query", "ipn" => "1");
                                            mlog_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        }
                                        $onetime_netsales_for_parent_event=1;
                                    }
                                    if($temp_payment_amount>=$child_payment){
                                        $temp_payment_amount = $temp_payment_amount-$child_payment;
                                        $child_net_sales = $child_payment;
                                        $child_payment = 0;
                                    }else{
                                        $child_payment = $child_payment - $temp_payment_amount;
                                        $child_net_sales = $temp_payment_amount;
                                        $temp_payment_amount = 0;
                                    }
                                    $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$child_payment WHERE `event_reg_details_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
                                    $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
                                    if(!$result_update_reg_query){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query", "ipn" => "1");
                                        mlog_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 500);
                                    }
                                    if($child_id!=$event_parent_id){
                                        $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
                                        $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
                                        if(!$result_update_child_query){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales", "ipn" => "1");
                                            mlog_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    $get_event_details_query = sprintf("SELECT e.`event_id`, IFNULL(e.`parent_id`, e.`event_id`) parent_id, `event_reg_details_id`, `event_title`, `event_type`, `event_begin_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` FROM `event` e 
                               LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' 
                               AND `payment_status` = 'RP' ORDER BY `event_begin_dt` ASC", mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                    $get_event_details_result = mysqli_query($this->db, $get_event_details_query);
                    if(!$get_event_details_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_details_query", "ipn" => "1");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $num_rows2 = mysqli_num_rows($get_event_details_result);
                        if($num_rows2>0){
                            $rem_due = 0;
                            while($row2 = mysqli_fetch_assoc($get_event_details_result)){
                                if($temp_payment_amount>0){
                                    $event_reg_details_id = $row2['event_reg_details_id'];
                                    $child_id = $row2['event_id'];
                                    $event_parent_id = $row2['parent_id'];
                                    $rem_due = $row2['balance_due'];
                                    $event_type = $row2['event_type'];
                                    $payment_status = $row2['payment_status'];
                                    $child_net_sales = 0;
                                    if($onetime_netsales_for_parent_event==0){
                                        if($processing_fee_type==2){
                                            $net_sales = $payment_amount;
                                        }elseif($processing_fee_type==1){
                                            $net_sales = $payment_amount-$processing_fee;
                                        }
                                        $update_event_query = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$net_sales WHERE `event_id`='%s'",$event_parent_id);
                                        $result_update_event_query = mysqli_query($this->db, $update_event_query);
                                        if(!$result_update_event_query){
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_event_query", "ipn" => "1");
                                            mlog_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 500);
                                        }
                                        $onetime_netsales_for_parent_event=1;
                                    }
                                    if($temp_payment_amount>=$rem_due){
                                        $temp_payment_amount = $temp_payment_amount-$rem_due;
                                        $child_net_sales = $rem_due;
                                        $rem_due = 0;
                                        $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due, `payment_status`='RC' WHERE `event_reg_details_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
                                    }else{
                                        $rem_due = $rem_due - $temp_payment_amount;
                                        $child_net_sales = $temp_payment_amount;
                                        $temp_payment_amount = 0;
                                        $update_reg_query = sprintf("UPDATE `event_reg_details` SET `balance_due`=$rem_due WHERE `event_reg_details_id`='%s'", 
                                                mysqli_real_escape_string($this->db, $event_reg_details_id));
                                    }
                                    $result_update_reg_query = mysqli_query($this->db, $update_reg_query);
                                    if(!$result_update_reg_query){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_reg_query", "ipn" => "1");
                                        mlog_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 500);
                                    }else{
                                        if($child_id!=$event_parent_id){
                                            $update_child_netsales = sprintf("UPDATE `event` SET `net_sales`=`net_sales`+$child_net_sales WHERE `event_id`='%s'",$child_id);
                                            $result_update_child_query = mysqli_query($this->db, $update_child_netsales);
                                            if(!$result_update_child_query){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_child_netsales", "ipn" => "1");
                                                mlog_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 500);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $payment_status = "";
                $checkout_state = "released";
                
                $update_query = sprintf("UPDATE `event_payment` SET `checkout_status`='%s' $payment_status WHERE `event_reg_id`='%s' and `event_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                        mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                $update_result = mysqli_query($this->db, $update_query);
                if(!$update_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query", "ipn" => "1");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }
    }
    
    public function stripeMembershipPurchase($push_device_id, $device_type, $app_id, $company_id, $student_id, $membership_id, $membership_option_id, $upgrade_status, $membership_category_title, $membership_title, $membership_desc, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $processing_fee_type, $processing_fee, $membership_fee, $membership_fee_disc, $signup_fee, $signup_fee_disc, $payment_amount, $initial_payment, $first_payment, $payment_frequency, $payment_start_date, $recurring_start_date, $reg_col1, $reg_col2, $reg_col3, $reg_col4, $reg_col5, $reg_col6, $reg_col7, $reg_col8, $reg_col9, $reg_col10, $prorate_first_payment_flg, $delay_recurring_payment_start_flg, $delay_recurring_payment_start_date, $delay_recurring_payment_amount, $custom_recurring_frequency_period_type, $custom_recurring_frequency_period_val, $initial_payment_include_membership_fee, $membership_structure, $billing_options, $no_of_classes, $billing_options_expiration_date, $billing_options_deposit_amount, $billing_options_no_of_payments, $payment_array, $participant_street, $participant_city, $participant_state, $participant_zip, $participant_country, $membership_discount_code, $signup_discount_code, $exclude_days_array, $program_start_date, $program_end_date, $exclude_from_billing_flag, $reg_type_user, $reg_version_user, $reg_current_date, $default_cc, $participant_id, $membership_start_date,  $cc_type, $stripe_customer_id, $payment_method_id, $intent_method_type){
        $payment_intent_id = '';
        $payment_from = $registration_from = '';
        $stripe_account_id = $stripe_currency = $studio_name = '';
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $upgrade_status = $sts['upgrade_status'];
        $buyer_name = $buyer_first_name . ' ' . $buyer_last_name;
        $actual_student_id = $student_id;
        $actual_participant_id = $participant_id;
        $payment_method = "CC";
        $has_3D_secure = 'N';
        $payment_support = 'S';
        
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
        if (($stripe_account != '' && $stripe_account['charges_enabled'] == 'Y' && ($initial_payment > 0 || $first_payment > 0 || $payment_amount > 0)) || ($payment_amount == 0 && $first_payment == 0 && $payment_amount == 0)) {
            $query = sprintf("SELECT c.`company_name`,s.`currency`,s.`account_id` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
            $result = mysqli_query($this->db, $query);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($result);
                if ($num_rows > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $studio_name = $row['company_name'];
                    $stripe_currency = $row['currency'];
                    $stripe_account_id = $row['account_id'];
                }
            }

            if (is_null($actual_student_id) || $actual_student_id == 0) {
                $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $device_type, $app_id, $push_device_id, 'Stripe', $studio_name);
                $student_id = $stud_array['student_id'];
                $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
            } else {
                $student_id = $actual_student_id;
                if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                    $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                }
            }
            if (is_null($actual_participant_id) || $actual_participant_id == 0) {
                $participant_id = $this->getParticipantId($company_id, $student_id, $reg_col1, $reg_col2, $reg_col3);
            } else {
                $participant_id = $actual_participant_id;
            }

            $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id, $membership_category_title, $membership_title, '');
            $user_timezone = $this->getUserTimezone($company_id);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone = $user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
            $current_date = date("Y-m-d");
            if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') && $billing_options == 'PP' && !empty($payment_array)) {
                if ($payment_amount < 5) {
                    $payment_amount = $payment_array[0]['amount'];
                }
//            if($recurring_start_date=='' || $recurring_start_date=='0000-00-00'){
                $recurring_start_date = $payment_array[0]['date'];
                $billing_options_no_of_payments = count($payment_array);
//            }
            }
            if (($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') && $billing_options == 'PF') {
                $payment_amount = 0;
                $recurring_start_date = '';
            }
            if (empty($membership_start_date)) {
                $membership_start_date = $payment_start_date;
            }
            if ($membership_structure == 'SE' && $billing_options == 'PP' && !empty($payment_array) && $initial_payment_include_membership_fee == 'N' && $reg_current_date != $payment_start_date) {
                $first_payment = $payment_array[0]['amount'];
                if (isset($payment_array[1]) && isset($payment_array[1]['date'])) {
                    $recurring_start_date = $payment_array[1]['date'];
                }
            }

            if (($first_payment < 5 && $first_payment > 0) && $payment_amount > 0 && $reg_current_date == $payment_start_date && $delay_recurring_payment_start_flg == 'N' && $initial_payment_include_membership_fee == 'N' && $prorate_first_payment_flg == 'Y' && $membership_structure == 'OE' && ($payment_frequency == 'M' || $payment_frequency == 'B')) {
                if ($payment_frequency == 'B') {
                    if (date('j', strtotime($recurring_start_date)) < 16) {
                        $next_pay_date = date('Y-m-16', strtotime($recurring_start_date));
                    } else {
                        $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($recurring_start_date)));
                    }
                } elseif ($payment_frequency == 'M') {
                    $next_pay_date = date('Y-m-d', strtotime('next month', strtotime($recurring_start_date)));
                }
                $recurring_start_date = $next_pay_date;
            }

            if ($initial_payment_include_membership_fee == 'N' && $reg_current_date == $payment_start_date) {
                $first_payment = 0;
            }

            if ($payment_method == 'CC') {
                $p_f = $this->getProcessingFee($company_id, $upgrade_status);
                $process_fee_per = $p_f['PP'];
                $process_fee_val = preg_replace('~[\r\n]+~', '', $p_f['PT']);
                if ($initial_payment <= 0) {
                    $initial_payment_processing_fee = 0;
                } else {
                    $initial_payment_processing_fee = $this->calculateProcessingFee($initial_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                }
                if ($first_payment <= 0) {
                    $first_payment_processing_fee = 0;
                } else {
                    $first_payment_processing_fee = $this->calculateProcessingFee($first_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                }
                if ($payment_amount <= 0) {
                    $recurring_payment_processing_fee = 0;
                } else {
                    $recurring_payment_processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
                }
                if ($delay_recurring_payment_amount <= 0) {
                    $delay_recurring_payment_processing_fee = 0;
                } else {
                    $delay_recurring_payment_processing_fee = $this->calculateProcessingFee($delay_recurring_payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
                }
            } else {
                $process_fee_per = $process_fee_val = $initial_payment_processing_fee = $first_payment_processing_fee = $recurring_payment_processing_fee = $delay_recurring_payment_processing_fee = 0;
            }

            if ($payment_frequency == 'N') {
                if ($initial_payment != 0 || $first_payment != 0) {
                    $payment_type = 'O';
                } else {
                    $payment_type = 'F';
                }
            } else {
                if ($payment_amount != 0) {
                    $payment_type = 'R';
                } else {
                    if ($initial_payment != 0 || $first_payment != 0) {
                        $payment_type = 'O';
                    } else {
                        $payment_type = 'F';
                    }
                }
            }

            $initial_payment_success_flag = 'Y';
            $initial_payment_date = $current_date;
            $next_payment_date = $recurring_start_date;
            $preceding_payment_date = $current_date;
            $preceding_payment_status = 'S';

            if ($initial_payment == 0 && count($payment_array) > 0) {
                $intent_method_type = "SI";
            } else {
                $intent_method_type = "PI";
            }

            $paid_amount_string = "`paid_amount`,";
            $paid_amount_string1 = "'" . mysqli_real_escape_string($this->db, $initial_payment) . "',";
            $temp_payment_status = 'M';

            $w_paid_amount = $paid_amount = $paid_fee = $amount_in_dollars = $dest_amount = $dest_amount_in_dollars = $w_paid_amount_round = $dest_amount_round = $payment_done = 0;
            $checkout_state = $cc_month = $cc_year = $stripe_card_name = $payment_status = '';
            $next_action_type = $next_action_type2 = $button_url = '';
            $has_3D_secure = 'N';

            if (!empty($stripe_customer_id) && !(is_null($stripe_customer_id)) && !empty($payment_method_id) && $payment_method == 'CC') {
                $this->getStripeKeys();
                \Stripe\Stripe::setApiKey($this->stripe_sk);
                \Stripe\Stripe::setApiVersion("2019-05-16");
                if ($initial_payment >= 5 && $payment_method == 'CC') {
                    $paid_amount = $initial_payment;
                    if ($processing_fee_type == 2) {
                        $w_paid_amount = $initial_payment + $initial_payment_processing_fee;
                    } elseif ($processing_fee_type == 1) {
                        $w_paid_amount = $initial_payment;
                    }

                    $w_paid_amount_round = round($w_paid_amount, 2);
                    $dest_amount = $w_paid_amount - $processing_fee;
                    $dest_amount_round = round($dest_amount, 2);
                    $dest_amount_in_dollars = $dest_amount_round * 100;
                    $amount_in_dollars = $w_paid_amount_round * 100;

                    $checkout_type = 'membership';
                    if ($intent_method_type == "SI") { // If it is setup intent creation
                        $payment_intent_result = $this->setup_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $studio_name, $buyer_email, $stripe_account_id, $checkout_type, $cc_type);
                    } else { //If it is payment intent creation
                        $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $checkout_type, $cc_type);
                    }
                    if ($payment_intent_result['temp_payment_status'] == 'succeeded') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'released';
                    } elseif ($payment_intent_result['temp_payment_status'] == 'requires_action') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'requires_action';
                        $has_3D_secure = 'Y';
                        $next_action_type = $payment_intent_result['next_action']['type'];
                        if($next_action_type != "redirect_to_url"){
                            # Payment not supported
                            $out['status'] = 'Failed';
                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                            $this->response($this->json($out), 200);
                        }
                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                    } else {
                        # Invalid status
                        $out['status'] = 'Failed';
                        $out['msg'] = 'Invalid PaymentIntent status.';
                        $out['error'] = 'Invalid PaymentIntent status';
                        $this->response($this->json($out), 200);
                    }
                    $payment_status = 'S';
                }
            }

            if (!empty($payment_method_id)) {
                try {
                    $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                    $brand = $retrive_payment_method->card->brand;
                    $cc_month = $retrive_payment_method->card->exp_month;
                    $cc_year = $retrive_payment_method->card->exp_year;
                    $last4 = $retrive_payment_method->card->last4;
                    $temp = ' xxxxxx';
                    $stripe_card_name = $brand . $temp . $last4;
                } catch (\Stripe\Error\Card $e) {
                    // Since it's a decline, \Stripe\Error\Card will be caught     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\RateLimit $e) {
                    // Too many requests made to the API too quickly     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\InvalidRequest $e) {
                    // Invalid parameters were supplied to Stripe's API     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Authentication $e) {
                    // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\ApiConnection $e) {
                    // Network communication with Stripe failed     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (\Stripe\Error\Base $e) {
                    // Display a very generic error to the user, and maybe send yourself an email     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                } catch (Exception $e) {
                    // Something else happened, completely unrelated to Stripe     
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $err['status_code'] = $e->getHttpStatus();
                }
                if (isset($err) && !empty($err)) {
                    $error = array('status' => "Failed", "msg" => $err['message']);
                    $this->response($this->json($error), 200);
                }
            }

            // SAVE CARD DETAILS
            if ($default_cc == 'Y') {
                $this->setStudentDefaultccForStripe($company_id, $student_id, $payment_support, $payment_method_id, $stripe_customer_id, $stripe_card_name, $cc_month, $cc_year, $buyer_email, $buyer_name, $buyer_postal_code, $country);
            }

            $payment_from = 'S';
            $registration_from = 'S';
            $rank_details = $this->getInitialRank($membership_id);
            $rank_status = $rank_details['rank_status'];
            $rank_id = $rank_details['rank_id'];
            $insert_reg = sprintf("INSERT INTO `membership_registration`($paid_amount_string`payment_method`,`company_id`, `membership_id`, `membership_option_id`, `student_id`,`participant_id`, `membership_status`, `membership_category_title`, `membership_title`,
                                `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `processing_fee_type`, `processing_fee`, `payment_type`, `membership_structure`, `prorate_first_payment_flg`, 
                                `delay_recurring_payment_start_flg`, `delay_recurring_payment_start_date`, `delay_recurring_payment_amount`, `custom_recurring_frequency_period_type`, `custom_recurring_frequency_period_val`, `initial_payment_include_membership_fee`, 
                                `billing_options`, `no_of_classes`, `billing_options_expiration_date`, `billing_options_deposit_amount`, `billing_options_no_of_payments`, 
                                `credit_card_expiration_month`, `credit_card_expiration_year`, `signup_fee`, `signup_fee_disc`, 
                                `membership_fee`, `membership_fee_disc`, `initial_payment`, `initial_payment_date`, `initial_payment_success_flag`, `first_payment`, `payment_amount`, `payment_frequency`, `membership_start_date`, `payment_start_date`, `recurring_start_date`, `next_payment_date`, 
                                `preceding_payment_date`, `preceding_payment_status`, `no_of_payments`, `membership_registration_column_1`, `membership_registration_column_2`, `membership_registration_column_3`, 
                                `membership_registration_column_4`, `membership_registration_column_5`, `membership_registration_column_6`, `membership_registration_column_7`, `membership_registration_column_8`, 
                                `membership_registration_column_9`, `membership_registration_column_10`, `participant_street`, `participant_city`, `participant_state`, `participant_zip`, `participant_country`, `rank_status`,`rank_id`,`membership_reg_type_user`,`membership_reg_version_user`,`specific_start_date`,`specific_end_date`,`exclude_billing_flag`, `stripe_customer_id`,`registration_from`,`payment_method_id`,`stripe_card_name`) VALUES($paid_amount_string1'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',
                                '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                    mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), 
                    mysqli_real_escape_string($this->db, $membership_option_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $participant_id),
                    mysqli_real_escape_string($this->db, 'R'), mysqli_real_escape_string($this->db, $membership_category_title), mysqli_real_escape_string($this->db, $membership_title), 
                    mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), 
                    mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $processing_fee_type), 
                    mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $membership_structure), 
                    mysqli_real_escape_string($this->db, $prorate_first_payment_flg), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_flg),
                    mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount), 
                    mysqli_real_escape_string($this->db, $custom_recurring_frequency_period_type), mysqli_real_escape_string($this->db, $custom_recurring_frequency_period_val), 
                    mysqli_real_escape_string($this->db, $initial_payment_include_membership_fee), mysqli_real_escape_string($this->db, $billing_options), 
                    mysqli_real_escape_string($this->db, $no_of_classes), mysqli_real_escape_string($this->db, $billing_options_expiration_date), 
                    mysqli_real_escape_string($this->db, $billing_options_deposit_amount), mysqli_real_escape_string($this->db, $billing_options_no_of_payments), 
                    mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $signup_fee), 
                    mysqli_real_escape_string($this->db, $signup_fee_disc), mysqli_real_escape_string($this->db, $membership_fee),
                    mysqli_real_escape_string($this->db, $membership_fee_disc), mysqli_real_escape_string($this->db, $initial_payment), 
                    mysqli_real_escape_string($this->db, $initial_payment_date), mysqli_real_escape_string($this->db, $initial_payment_success_flag), 
                    mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $payment_amount), 
                    mysqli_real_escape_string($this->db, $payment_frequency), mysqli_real_escape_string($this->db, $membership_start_date), 
                    mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, $recurring_start_date), 
                    mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, $preceding_payment_date), 
                    mysqli_real_escape_string($this->db, $preceding_payment_status), mysqli_real_escape_string($this->db, $payment_done), 
                    mysqli_real_escape_string($this->db, $reg_col1), mysqli_real_escape_string($this->db, $reg_col2), mysqli_real_escape_string($this->db, $reg_col3), 
                    mysqli_real_escape_string($this->db, $reg_col4), mysqli_real_escape_string($this->db, $reg_col5), mysqli_real_escape_string($this->db, $reg_col6), 
                    mysqli_real_escape_string($this->db, $reg_col7), mysqli_real_escape_string($this->db, $reg_col8), mysqli_real_escape_string($this->db, $reg_col9), 
                    mysqli_real_escape_string($this->db, $reg_col10), mysqli_real_escape_string($this->db, $participant_street), mysqli_real_escape_string($this->db, $participant_city), 
                    mysqli_real_escape_string($this->db, $participant_state), mysqli_real_escape_string($this->db, $participant_zip), mysqli_real_escape_string($this->db, $participant_country), 
                    mysqli_real_escape_string($this->db, $rank_status), mysqli_real_escape_string($this->db, $rank_id), mysqli_real_escape_string($this->db, $reg_type_user), 
                    mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $program_start_date), 
                    mysqli_real_escape_string($this->db, $program_end_date), mysqli_real_escape_string($this->db, $exclude_from_billing_flag), 
                    mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $registration_from), 
                    mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name));
            $result_reg = mysqli_query($this->db, $insert_reg);

            if (!$result_reg) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_reg");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $membership_reg_id = mysqli_insert_id($this->db);

                if ($membership_structure == 'NC' || $membership_structure == 'C' || $membership_structure == 'SE') {
                    if ($initial_payment > 0) {
                        if ($payment_method == "CA" || $payment_method == "CH") {
                            $temp_payment_status = 'M';
                        }
                        $insert_success_payment_nc = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'));
                        $result_paid_insert = mysqli_query($this->db, $insert_success_payment_nc);
                        if (!$result_paid_insert) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_success_payment_nc");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }
                    if ($billing_options == 'PP' && !empty($payment_array)) {
                        for ($i = 0; $i < count($payment_array); $i++) {
                            if ($membership_structure == 'SE') {
                                $prorate_flag_se = $payment_array[$i]['prorate_flag'];
                            } else {
                                $prorate_flag_se = 'F';
                            }
                            $paytime_type_check = $payment_array[$i]['type'];
                            if ($membership_structure == 'SE' && $initial_payment_include_membership_fee == 'N' && $reg_current_date != $payment_start_date && $i == 0) {
                                $paytime_type_check = 'F';
                            }
                            $insert_mem_payment_NC = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`, `prorate_flag`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_array[$i]['amount']), mysqli_real_escape_string($this->db, $payment_array[$i]['processing_fee']), mysqli_real_escape_string($this->db, $payment_array[$i]['date']), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, $paytime_type_check), mysqli_real_escape_string($this->db, $prorate_flag_se));
                            $result_pay_insert = mysqli_query($this->db, $insert_mem_payment_NC);
                            if (!$result_pay_insert) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment_NC");
                                mlog_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }
                    } elseif ($membership_structure == 'SE' && $billing_options == 'PF' && $first_payment > 0) {
                        $insert_first_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'));
                        $result_first_insert = mysqli_query($this->db, $insert_first_payment);
                        if (!$result_first_insert) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_first_payment");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }
                } elseif ($membership_structure == 'OE') {
                    if ($payment_method == "CA" || $payment_method == "CH") {
                        $temp_payment_status_cashorcheck = 'M';
                    } else {
                        $temp_payment_status_cashorcheck = 'S';
                    }
                    if ($initial_payment >= 5) {
                        if ($payment_type == 'R') {
                            if ($delay_recurring_payment_start_flg == 'Y') {
                                if ($delay_recurring_payment_amount >= 5) {

                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount), mysqli_real_escape_string($this->db, $delay_recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, 'R'));
                                } elseif ($delay_recurring_payment_amount < 5 && $delay_recurring_payment_amount > 0) {
                                    $added_payment = $payment_amount + $delay_recurring_payment_amount;
                                    if ($added_payment <= 0) {
                                        $added_processing_fee = 0;
                                    } else {
                                        $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                    }
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } else {
                                    if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'), ('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                        $added_payment = $payment_amount + $first_payment;
                                        if ($added_payment <= 0) {
                                            $added_processing_fee = 0;
                                        } else {
                                            $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                        }
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    } else {
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    }
                                }
                            } else {
                                if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'), ('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                    $added_payment = $payment_amount + $first_payment;
                                    if ($added_payment <= 0) {
                                        $added_processing_fee = 0;
                                    } else {
                                        $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                    }
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } else {
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                }
                            }
                        } else {
                            if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'));
                            } else {
                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $initial_payment_processing_fee), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status_cashorcheck), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'I'));
                            }
                        }
                        $result_pay = mysqli_query($this->db, $insert_mem_payment);

                        if (!$result_pay) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                            mlog_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    } else {
                        if ($payment_type == 'R') {
                            if ($delay_recurring_payment_start_flg == 'Y') {
                                if ($delay_recurring_payment_amount >= 5) {
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $delay_recurring_payment_amount), mysqli_real_escape_string($this->db, $delay_recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $delay_recurring_payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } elseif ($delay_recurring_payment_amount < 5 && $delay_recurring_payment_amount > 0) {
                                    $added_payment = $payment_amount + $delay_recurring_payment_amount;
                                    if ($added_payment <= 0) {
                                        $added_processing_fee = 0;
                                    } else {
                                        $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                    }

                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`,`company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } else {
                                    if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`,`company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from` `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                        $added_payment = $payment_amount + $first_payment;
                                        if ($added_payment <= 0) {
                                            $added_processing_fee = 0;
                                        } else {
                                            $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                        }
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    } else {
                                        $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                    }
                                }
                            } else {
                                if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'),('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } elseif (!empty($first_payment) && $first_payment < 5 && $first_payment > 0 && $initial_payment_include_membership_fee == 'N') {
                                    $added_payment = $payment_amount + $first_payment;
                                    if ($added_payment <= 0) {
                                        $added_processing_fee = 0;
                                    } else {
                                        $added_processing_fee = $this->calculateProcessingFee($added_payment, $processing_fee_type, $process_fee_per, $process_fee_val);
                                    }
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $added_payment), mysqli_real_escape_string($this->db, $added_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                } else {
                                    $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $next_payment_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                                }
                            }
                            $result_pay = mysqli_query($this->db, $insert_mem_payment);

                            if (!$result_pay) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                                mlog_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        } else {
                            if (!empty($first_payment) && $first_payment >= 5 && $initial_payment_include_membership_fee == 'N') {
                                $insert_mem_payment = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $first_payment), mysqli_real_escape_string($this->db, $first_payment_processing_fee), mysqli_real_escape_string($this->db, $payment_start_date), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'F'));

                                $result_pay = mysqli_query($this->db, $insert_mem_payment);

                                if (!$result_pay) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_payment");
                                    mlog_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }

                    if ($payment_amount > 0 && $payment_type == 'R') {
                        $next_pay_date = $payment_start_date = date("Y-m-d", strtotime($next_payment_date));
                        $payment_end_date = date("Y-m-d", strtotime("+93 days"));
                        $recurring_payment_date_array = [];
                        $recurring_payment_date_array[] = $payment_start_date;
                        $iteration_end = 1;
                        $payment_date = $next_pay_date;
                        for ($i = 0; $i < $iteration_end; $i++) {
                            if ($payment_frequency == 'W') {
                                $next_pay_date = date('Y-m-d', strtotime('+1 weeks', strtotime($payment_date)));
                            } elseif ($payment_frequency == 'B') {
                                if (date('j', strtotime($payment_date)) < 16) {
                                    $next_pay_date = date('Y-m-16', strtotime($payment_date));
                                } else {
                                    $next_pay_date = date('Y-m-d', strtotime('first day of next month', strtotime($payment_date)));
                                }
                            } elseif ($payment_frequency == 'M') {
                                $next_pay_date = date('Y-m-d', strtotime('next month', strtotime($payment_date)));
                            } elseif ($payment_frequency == 'A') {
                                $next_pay_date = date('Y-m-d', strtotime('+1 year', strtotime($payment_date)));
                            } elseif ($payment_frequency == 'C') {
                                if ($custom_recurring_frequency_period_type == 'CW') {
                                    $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val weeks", strtotime($payment_date)));
                                } elseif ($custom_recurring_frequency_period_type == 'CM') {
                                    $next_pay_date = date('Y-m-d', strtotime("+$custom_recurring_frequency_period_val months", strtotime($payment_date)));
                                }
                            }
                            $payment_date = $next_pay_date;
                            if ($next_pay_date <= $payment_end_date || count($recurring_payment_date_array) == 0) {
                                $iteration_end++;
                                $recurring_payment_date_array[] = $next_pay_date;
                            }
                        }

                        for ($j = 1; $j < count($recurring_payment_date_array); $j++) {
                            $insert_mem_pay_OE_R = sprintf("INSERT INTO `membership_payment`(`credit_method`, `company_id`, `membership_registration_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`, `paytime_type`) VALUES('$payment_method','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $recurring_payment_processing_fee), mysqli_real_escape_string($this->db, $recurring_payment_date_array[$j]), mysqli_real_escape_string($this->db, 'N'), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from), mysqli_real_escape_string($this->db, 'R'));
                            $result_mem_pay_OE_r = mysqli_query($this->db, $insert_mem_pay_OE_R);
                            if (!$result_mem_pay_OE_r) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_mem_pay_OE_R");
                                mlog_info($this->json($error_log));
                            }
                        }
                    }
                }

                if ($payment_method !== 'CC') {

                    $update_count = sprintf("UPDATE `membership` SET `category_net_sales`=`category_net_sales`+%s,`members_active`=`members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s'", mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
                    $result_count = mysqli_query($this->db, $update_count);

                    if (!$result_count) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }

                    $update_count2 = sprintf("UPDATE `membership_options` SET `options_net_sales` = `options_net_sales`+%s,`options_members_active`=`options_members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", mysqli_real_escape_string($this->db, $initial_payment), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
                    $result_count2 = mysqli_query($this->db, $update_count2);

                    if (!$result_count2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count2");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $this->updateMembershipDimensionsNetSales($company_id, $membership_id, '');
                } else {
                    $update_count = sprintf("UPDATE `membership` SET `members_active`=`members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
                    $result_count = mysqli_query($this->db, $update_count);

                    if (!$result_count) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }

                    $update_count2 = sprintf("UPDATE `membership_options` SET `options_members_active`=`options_members_active`+1 WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
                    $result_count2 = mysqli_query($this->db, $update_count2);

                    if (!$result_count2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count2");
                        mlog_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
            }

            //update cc information if release state
            if ($checkout_state == 'released') {
                $this->updateStripeCCDetailsAfterRelease($payment_intent_id);
            }
            //End update cc information if release state

            $activity_text = 'Registration date.';
            if (!empty($signup_discount_code)) {
                $activity_text .= ' Sign-up fee discount code used "' . $signup_discount_code . '".';
                if (!empty($membership_discount_code)) {
                    $activity_text .= ' Membership fee discount code used "' . $membership_discount_code . '".';
                } elseif (!empty($membership_fee_disc)) {
                    $activity_text .= ' Membership fee discount value used "' . $membership_fee_disc . '".';
                }
            } else {
                if (!empty($signup_fee_disc)) {
                    $activity_text .= ' Sign-up fee discount value used "' . $signup_fee_disc . '".';
                    if (!empty($membership_discount_code)) {
                        $activity_text .= ' Membership fee discount code used "' . $membership_discount_code . '".';
                    } elseif (!empty($membership_fee_disc)) {
                        $activity_text .= ' Membership fee discount value used "' . $membership_fee_disc . '".';
                    }
                } else {
                    if (!empty($membership_discount_code)) {
                        $activity_text .= ' Membership fee discount code used "' . $membership_discount_code . '".';
                    } elseif (!empty($membership_fee_disc)) {
                        $activity_text .= ' Membership fee discount value used "' . $membership_fee_disc . '".';
                    }
                }
            }

            $curr_date = gmdate("Y-m-d H:i:s");
            $sql1 = sprintf("INSERT INTO `membership_history`(`company_id`, `membership_registration_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $student_id), 'registration', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
            $result1 = mysqli_query($this->db, $sql1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                mlog_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }

            if ($exclude_from_billing_flag == 'Y' && count($exclude_days_array) > 0 && $membership_structure == 'SE') {
                usort($exclude_days_array, function($a, $b) {
                    return $a['billing_exclude_startdate'] > $b['billing_exclude_startdate'];
                });
                for ($x = 0; $x < count($exclude_days_array); $x++) {
                    $activity_text = "Exclude days  : from " . date('M d Y', strtotime($exclude_days_array[$x]['billing_exclude_startdate'])) . " to " . date('M d Y', strtotime($exclude_days_array[$x]['billing_exclude_enddate']));
                    $curr_date = gmdate("Y-m-d H:i:s");
                    $insert_history_exclude = sprintf("INSERT INTO `membership_history`(`company_id`, `membership_registration_id`, `student_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $student_id), 'Exclude', mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                    $result_history_exculde = mysqli_query($this->db, $insert_history_exclude);
                    if (!$result_history_exculde) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history_exclude");
                        mlog_info($this->json($error_log));
                    }
                }
            }
            if ($payment_type == 'F') {
                $email_msg = "Registration successful.";
            } else {
                if ($has_3D_secure == 'N') {
                    $email_msg = "Registration successful. Confirmation of purchase has been sent to your email.";
                } elseif ($has_3D_secure == 'Y') {
                    $email_msg = "Membership Checkout was processed and waiting for customer action. Kindly review and authenticate the payment.";
                }
            }
            $student_info_details = $this->getStudentAllDetails($company_id, $student_id, 1);
            $msg = array("status" => "Success", "msg" => $email_msg, "memberapp_details" => $student_info_details);
            $this->responseWithWepay($this->json($msg), 200);
            if ($has_3D_secure == 'N') {
                $this->sendEmailForMembershipPaymentSuccess($membership_reg_id, $company_id);
            } else {
                $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name);
            }

            $this->updateMembershipDimensionsMembers($company_id, $membership_id, 'A', $membership_option_id);
            $curr_date_time = date("Y-m-d ");
            $sql_insert_date = sprintf("UPDATE membership_registration SET start_email_date ='%s' WHERE company_id='%s'  AND membership_registration_id='%s' ", mysqli_real_escape_string($this->db, $curr_date_time), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_reg_id));
            $sql_insert_date_result = mysqli_query($this->db, $sql_insert_date);
            if (!$sql_insert_date_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_insert_date");
                mlog_info($this->json($error_log));
            } else {
                $message = array('status' => "Success", "msg" => "Email start date updated successfully.");
                mlog_info($this->json($message));
            }
        } else {
            $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
            $this->response($this->json($error), 200);
        }
        date_default_timezone_set($curr_time_zone);
    }
     public function updateStripeCCDetailsAfterRelease($payment_intent_id){
        $checkout_state = $membership_reg_id = $company_id = $membership_id = $payment_id = $processing_fee_type = $payment_type = $membership_option_id = $membership_title = $membership_category_title = '';
        $payment_amount = $fee = 0;
        $payment_status = "";
        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_registration_id`, mr.`membership_category_title`, mr.`membership_title`, mp.`membership_payment_id`, mp.`checkout_status`, mp.`payment_amount`, mp.`processing_fee`, mr.`processing_fee_type`, mp.`checkout_status`
                    FROM `membership_registration` mr 
                    LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id`=mp.`membership_registration_id` 
                    WHERE mp.`payment_intent_id` = '%s' ORDER BY mp.`membership_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $membership_reg_id = $row['membership_registration_id'];
                $payment_id = $row['membership_payment_id'];
                $fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_amount = $row['payment_amount'];
                $company_id = $row['company_id'];
                $membership_id = $row['membership_id'];
                $membership_option_id = $row['membership_option_id'];
                $membership_title = $desc = $row['membership_title'];
                $membership_category_title=$row['membership_category_title']; 
                $checkout_state = $row['checkout_status'];
                
                $paid_amount = 0;
                if($processing_fee_type==1){
                    $paid_amount = $payment_amount-$fee;
                }elseif($processing_fee_type==2){
                    $paid_amount = $payment_amount;
                }
                $update_sales_in_reg = sprintf("UPDATE `membership_registration` SET `paid_amount`=`paid_amount`+$paid_amount WHERE `membership_registration_id`='%s'", mysqli_real_escape_string($this->db, $membership_reg_id));
                $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);

                if(!$result_sales_in_reg){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }

                $update_count = sprintf("UPDATE `membership` SET `category_net_sales`=`category_net_sales`+$paid_amount WHERE `company_id`='%s' AND `membership_id`='%s'",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
                $result_count = mysqli_query($this->db, $update_count);

                if(!$result_count){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }

                $update_count2 = sprintf("UPDATE `membership_options` SET `options_net_sales` = `options_net_sales`+$paid_amount WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s'",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $membership_option_id));
                $result_count2 = mysqli_query($this->db, $update_count2);

                if(!$result_count2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } 
                $update_query = sprintf("UPDATE `membership_payment` SET `checkout_status`='%s' $payment_status WHERE `membership_registration_id`='%s' and `membership_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                        mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                $update_result = mysqli_query($this->db, $update_query);
                if(!$update_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }else{
                    $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                    log_info($this->json($error));
                    $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id, $membership_category_title, $membership_title, '');
                    $this->updateMembershipDimensionsNetSales($company_id, $membership_id, '');
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Payment details doesn't exist.");
                log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
    public function setStudentDefaultccForStripe($company_id, $student_id, $payment_support, $payment_method_id, $stripe_customer_id, $stripe_card_name, $cc_month, $cc_year, $buyer_email, $buyer_name, $buyer_postal_code, $country) {
        $select_card_details = sprintf("SELECT * FROM `payment_method_details` WHERE `company_id`='%s' AND `student_id`='%s' AND `payment_support` = 'S'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id));
        $result_select_card_details = mysqli_query($this->db, $select_card_details);
        if (!$result_select_card_details) {
            $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$select_card_details");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal server Error.");
            $this->response($this->json($error), 500);
        } else {
            $select_num_rows = mysqli_num_rows($result_select_card_details);
            if ($select_num_rows > 0) {
                $updatequery = sprintf("UPDATE `payment_method_details` SET  `payment_support`='%s', `stripe_payment_method_id`='%s',`stripe_customer_id`='%s',`credit_card_name`='%s',`credit_card_expiration_month`='%s',`credit_card_expiration_year`='%s',`buyer_email`='%s',`buyer_name`='%s',`buyer_postal_code`='%s'
                       ,`country`='%s' WHERE student_id='%s' ", mysqli_real_escape_string($this->db, $payment_support), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $stripe_card_name)
                        , mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name)
                        , mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $student_id));

                $resultupdatequery = mysqli_query($this->db, $updatequery);
                if (!$resultupdatequery) {
                    $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$updatequery");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal server Error.");
                    $this->response($this->json($error), 500);
                }
            } else {
                $insertquery = sprintf("INSERT INTO `payment_method_details`(`company_id`, `student_id`, `stripe_payment_method_id`, `stripe_customer_id`, `credit_card_name`,`payment_support`, `credit_card_expiration_month`, `credit_card_expiration_year`, `buyer_email`, `buyer_name`, `country`, `buyer_postal_code`) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, 'S'), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_name), mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $buyer_postal_code));
                $resultinsertquery = mysqli_query($this->db, $insertquery);
                if (!$resultinsertquery) {
                    $error_log = array('status' => "Failed", "msg" => "Internal server Error.", "query" => "$insertquery");
                    mlog_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }
    }

}

?>