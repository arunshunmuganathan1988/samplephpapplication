//declare modules

angular.module("home", []);
angular.module("login", []);
angular.module("addevent", ['colorpicker.module', 'wysiwyg.module', 'angular-img-cropper', 'ngclipboard']);
angular.module("manageevent", ['colorpicker.module', 'wysiwyg.module']);
angular.module("message", []);
angular.module("social", []);
angular.module("curriculum", []);
angular.module("members", ['angular-img-cropper']);
angular.module("register", []);
angular.module("forgotpassword", []);
angular.module("resetpassword", []);
angular.module("referral", []);
//angular.module("billing", []);
angular.module("billingstripe", []);
angular.module("adminpanel", []);
angular.module("template", []);
angular.module("payment", []);
angular.module("customers", []);
angular.module("dashboard", []);
angular.module("membershipdetail", []);
angular.module("eventdetail", []);
angular.module("addmembership", ['colorpicker.module', 'wysiwyg.module', 'angular-img-cropper', 'ngclipboard']);
angular.module("managemembership", ['colorpicker.module', 'wysiwyg.module']);
angular.module("templatemembership", []);
angular.module("wepaycreation", []);
angular.module('addtrial', ['wysiwyg.module', 'angular-img-cropper', 'ngclipboard']);
angular.module('managetrial', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']);
angular.module('manageleads', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']);
angular.module('trialtemplate', []);
angular.module('retailtemplate', []);
angular.module('trialdetail', []);
angular.module('miscdetail', []);
angular.module('leadsdetail', []);
angular.module('posdashboard', []);
angular.module("attendancedashboard", ['ngclipboard']);
angular.module('trials', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']);
angular.module("appverification", ['ngclipboard']);
angular.module("addretail", ['colorpicker.module', 'wysiwyg.module', 'angular-img-cropper', 'ngclipboard']);
angular.module('manageretail', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']);
angular.module('retaildiscount', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']);
angular.module('retailfulfillment', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']);
angular.module("integrations", ['ngclipboard']);
angular.module("adminclonepanel", []);
angular.module('adminstudiomembers', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']);
angular.module('adminwhitelabel', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']);
angular.module('adminhome', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']);
angular.module("adminlogintool", []);
angular.module("emailsettings", []);
angular.module("whitelabelsetup", ['colorpicker.module', 'wysiwyg.module', 'angular-img-cropper', 'ngclipboard']);
angular.module("stripecreation", []);
$('#progress-full').hide();
$('#stripe-progress-full').hide();
$('#modal-message').show();


angular.module("myapp", ['ngRoute', 'home', 'login', 'addevent', 'template', 'payment', 'customers', 'manageevent', 'message', 'social', 'addmembership', 'managemembership', 'templatemembership', 'wepaycreation', 'angular-md5', 'curriculum', 'members', 'dashboard', 'billingstripe', 'referral', 'register', 'forgotpassword', 'resetpassword', 'adminpanel', 'ngStorage', 'dndLists', 'datatables', 'datatables.scroller', 'membershipdetail', 'eventdetail', 'addtrial', 'managetrial', 'trialtemplate','retailtemplate', 'trialdetail','miscdetail', 'leadsdetail','posdashboard', 'trials', 'attendancedashboard', 'appverification', 'integrations', 'addretail','manageretail','retaildiscount','retailfulfillment', 'adminclonepanel', 'manageleads', 'emailsettings', 'adminstudiomembers', 'adminhome', 'adminlogintool', 'whitelabelsetup', 'stripecreation', 'adminwhitelabel'])

        .config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
                $routeProvider
                        .when('/home', {
                            templateUrl: 'Module/View/Home.html',
                            controller: 'HomeController'
                        })
                        .when('/login', {
                            templateUrl: 'Module/View/Login.html',
                            controller: 'LoginController'
                        })
                        .when('/addevent', {
                            templateUrl: 'Module/View/AddEvent.html',
                            controller: 'AddEventController'
                        })
                        .when('/manageevent', {
                            templateUrl: 'Module/View/ManageEvent.html',
                            controller: 'ManageEventController'
                        })

                        .when('/addmembership', {
                            templateUrl: 'Module/View/AddMembership.html',
                            controller: 'AddMembershipController'
                        })
                        .when('/managemembership', {
                            templateUrl: 'Module/View/ManageMembership.html',
                            controller: 'ManageMembershipController'
                        })
                        .when('/templatemembership', {
                            templateUrl: 'Module/View/TemplateMembership.html',
                            controller: 'TemplateMembershipController'
                        })

                        .when('/wepaycreation', {
                            templateUrl: 'Module/View/WepayCreation.html',
                            controller: 'WepayCreationController'
                        })

                        .when('/message', {
                            templateUrl: 'Module/View/Message.html',
                            controller: 'MessageController'
                        })
                        .when('/social', {
                            templateUrl: 'Module/View/Social.html',
                            controller: 'SocialController'
                        })
                        .when('/curriculum', {
                            templateUrl: 'Module/View/CurriculumContent.html',
                            controller: 'CurriculumContentController'
                        })
                        .when('/members', {
                            templateUrl: 'Module/View/Members.html',
                            controller: 'MembersController'
                        })
                        .when('/registration', {
                            templateUrl: 'Module/View/registration.html',
                            controller: 'registrationController'
                        })
                        .when('/forgotpassword', {
                            templateUrl: 'Module/View/ForgotPassword.html',
                            controller: 'ForgotPasswordController'
                        })
                        .when('/resetpassword', {
                            templateUrl: 'Module/View/ResetPassword.html',
                            controller: 'ResetPasswordController'
                        })
                        .when('/referral', {
                            templateUrl: 'Module/View/Referral.html',
                            controller: 'ReferralController'
                        })
//                        .when('/billing', {
//                            templateUrl: 'Module/View/Billing.html',
//                            controller: 'BillingController'
//                        })
                        .when('/billingstripe', {
                            templateUrl: 'Module/View/BillingStripe.html',
                            controller: 'BillingStripeController'
                        })
                        .when('/adminpanel', {
                            templateUrl: 'Module/View/admin.html',
                            controller: 'AdminpageController'
                        })
                        .when('/template', {
                            templateUrl: 'Module/View/Template.html',
                            controller: 'TemplateController'
                        })
                        .when('/payment', {
                            templateUrl: 'Module/View/Payment.html',
                            controller: 'PaymentController'
                        })
                        .when('/customers', {
                            templateUrl: 'Module/View/Customers.html',
                            controller: 'CustomerController'
                        })
                        .when('/dashboard', {
                            templateUrl: 'Module/View/dashBoard.html',
                            controller: 'dashBoardController'
                        })
                        .when('/membershipdetail', {
                            templateUrl: 'Module/View/MembershipDetail.html',
                            controller: 'MembershipDetailController'
                        })
                        .when('/eventdetail', {
                            templateUrl: 'Module/View/EventDetail.html',
                            controller: 'EventDetailController'
                        })
                        .when('/addtrial', {
                            templateUrl: 'Module/View/AddTrial.html',
                            controller: 'AddTrialController'
                        })
                        .when('/managetrial', {
                            templateUrl: 'Module/View/ManageTrial.html',
                            controller: 'ManageTrialController'
                        })
                        .when('/manageleads', {
                            templateUrl: 'Module/View/ManageLeads.html',
                            controller: 'ManageLeadsController'
                        })
                        .when('/retailtemplate', {
                            templateUrl: 'Module/View/RetailTemplate.html',
                            controller: 'RetailTemplateController'
                        })
                        .when('/trialtemplate', {
                            templateUrl: 'Module/View/TrialTemplate.html',
                            controller: 'TrialTemplateController'
                        })
                        .when('/trialdetail', {
                            templateUrl: 'Module/View/TrialDetail.html',
                            controller: 'TrialDetailController'
                        })
                        .when('/miscdetail', {
                            templateUrl: 'Module/View/MiscDetail.html',
                            controller: 'MiscDetailController'
                        })

                        .when('/leadsdetail', {
                            templateUrl: 'Module/View/LeadsDetail.html',
                            controller: 'LeadsDetailController'
                        })
                        .when('/posdashboard', {
                            templateUrl: 'Module/View/PosDashboard.html',
                            controller: 'PosDashboardController'
                        })
                        .when('/attendancedashboard', {
                            templateUrl: 'Module/View/AttendanceDashboard.html',
                            controller: 'AttendanceDashboardController'
                        })
                        .when('/trials', {
                            templateUrl: 'Module/View/Trials.html',
                            controller: 'TrialsController'
                        })
                        .when('/appverification', {
                            templateUrl: 'Module/View/AppVerification.html',
                            controller: 'AppVerificationController'
                        })
                        .when('/addretail', {
                            templateUrl: 'Module/View/AddRetail.html',
                            controller: 'AddRetailController'
                        })
                        .when('/retaildiscount', {
                            templateUrl: 'Module/View/RetailDiscount.html',
                            controller: 'RetailDiscountController'
                        })
                        .when('/retailfulfillment', {
                            templateUrl: 'Module/View/RetailFulfillment.html',
                            controller: 'RetailFulfillmentController'
                        })
                        .when('/manageretail', {
                            templateUrl: 'Module/View/ManageRetail.html',
                            controller: 'ManageRetailController'
                        })
                        .when('/integrations', {
                            templateUrl: 'Module/View/Integrations.html',
                            controller: 'IntegrationsController'
                        })
                        .when('/whitelabelsetup', {
                            templateUrl: 'Module/View/WhiteLabelSetup.html',
                            controller: 'WhiteLabelSetupController'
                        })
                        .when('/adminclonepanel', {
                            templateUrl: 'Module/View/AdminClonePanel.html',
                            controller: 'AdminClonePanelController'
                        })
                        .when('/adminwhitelabel', {
                            templateUrl: 'Module/View/WhiteLabelDashboard.html',
                            controller: 'WhiteLabelDashboardController'
                        })
                        .when('/adminstudiomembers', {
                            templateUrl: 'Module/View/AdminStudioMembers.html',
                            controller: 'AdminStudioMembersController'
                        })
                        .when('/adminhome', {
                            templateUrl: 'Module/View/AdminHome.html',
                            controller: 'AdminHomeController'
                        })
                        .when('/adminlogintool', {
                            templateUrl: 'Module/View/AdminLoginTool.html',
                            controller: 'AdminLoginToolController'
                        })
                        .when('/emailsettings', {
                            templateUrl: 'Module/View/EmailSettings.html',
                            controller: 'EmailSettingsController'
                        })
                        .when('/stripecreation', {
                            templateUrl: 'Module/View/StripeCreation.html',
                            controller: 'StripeCreationController'
                        })
                        .otherwise({
                            redirectTo: '/login'
                        })
//                });

            }]);

angular.module('myapp').controller("myCtrl", ['$scope', '$http', '$location', '$route', '$rootScope', '$localStorage', 'urlservice', 'curriculumLevelDetails','$q','$interval','$window', function ($scope, $http, $location, $route, $rootScope, $localStorage, urlservice, curriculumLevelDetails,$q,$interval,$window) {

        $scope.message = "MyStudio";
        $scope.vercodesuccess = false;
        $scope.logoutDivOpen = false;
        $rootScope.conv_unread_count = null;
        $rootScope.conv_selected_student_id = $rootScope.currentpage_for_conversation = $rootScope.conv_last_msg_id = $rootScope.conv_last_msg_datetime = $rootScope.student_last_msg_id = $rootScope.conversation_search_term = '';
        $rootScope.from_tab = 'I';
        $scope.supported_country_list = [];
        $scope.new_conversation_Count = 0;
        $scope.new_conversation_array = [];
        $scope.new_chat_array = [];    
        $scope.stripe_text_show = false;
        $scope.stripe_redirect_onboarding_link = '';
        
//        GET COUNTRY LIST WITH PAYMENT SUPPORT
        $scope.openRegistration = function () {
            // GET SUPPORTED COUNTRY LIST
            $http({
                method: 'GET',
                url: urlservice.url + 'getSupportedCountryList',
                params: {
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true
            }).then(function (response) {
                $('#progress-full').hide();
                if (response.data.status == 'Success') {
                    $scope.supported_country_list = response.data.supported_country_list;
                    $location.path('/registration');
                } else {
                    $scope.supported_country_list = '';
                    $location.path('/registration');
                }
            }, function (response) {
                console.log(response.data);
            });
        };
        
        //reset password token
        if ($location.absUrl().indexOf("token") > -1 || $location.absUrl().indexOf("resetpassword") > -1) {
            $location.path('/resetpassword');
        } else if ($location.absUrl().indexOf("registration") > -1) {
            $scope.openRegistration();
        } else if ($location.absUrl().indexOf("forgotpassword") > -1) {
            $location.path('/forgotpassword');
        }else if ($location.absUrl().indexOf("adminpanel") > -1) {
            $location.path('/adminpanel');
        } else if ($location.absUrl().indexOf("adminwhitelabel") > 0 && $localStorage.isadminlogin === 'Y') {
            $location.path('/adminwhitelabel');
        }else if ($location.absUrl().indexOf("adminclonepanel") > 0 && $localStorage.isadminlogin === 'Y') {
            $location.path('/adminclonepanel');
        } else if ($location.absUrl().indexOf("adminstudiomembers") > 0 && $localStorage.isadminlogin === 'Y') {
            $location.path('/adminstudiomembers');
        } else if ($location.absUrl().indexOf("adminhome") > 0 && $localStorage.isadminlogin === 'Y') {
            $location.path('/adminhome');
        } else if ($location.absUrl().indexOf("adminlogintool") > 0 && $localStorage.isadminlogin === 'Y') {
            $location.path('/adminlogintool');
        }else {
            $('#progress-full').show();
            // SESSION VERIFICATION TO PREVENT MULTIPLE USERS SIMULTANEOUSLY
            $http({
                method: 'GET',
                url: urlservice.url + 'verifySession',
                params: {
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                    withCredentials: true
            }).then(function (response) {
                if (response.data.status == 'Success') {
                    if ($localStorage.islogin !== 'Y' || $localStorage.islogin === undefined || $localStorage.islogin === null) {
                        $('#progress-full').hide();
                        $location.path('/login');
                    } else if ($location.absUrl().indexOf("login") > -1) {
                        setTimeout(function ()
                        {
                            $('#progress-full').hide();
                            if ($location.absUrl().indexOf("dashboard") < 0) {
                                console.log("dashboard1");
                                $scope.dashboard();
                            }
                        }, 2000);
                    } else {
                        $('#progress-full').hide();
                    }
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $('#progress-full').hide();
                    $location.path('/login');
                }
            }, function (response) {
                console.log(response.data);
            });


            //FOR TOOLTIPS
            if ($localStorage.firstlogin === false || $localStorage.firstlogin === undefined) {
                setTimeout(function ()
                {
                    tooltip.hide();
                }, 6000);
            }
        }

        // WATCHING LOGIN STATUS FOR PREVENTION OF MULTIPLE LOGIN IN DIFFERENT WINDOWS
        $scope.$watch(function () {
            return $localStorage.islogin;
        }, function (newVal, oldVal) {
            if (newVal == 'Y' && newVal != oldVal) {
                setTimeout(function ()
                {
                    if ($location.absUrl().indexOf("dashboard") < 0) {
                        if ($localStorage.firstlogin_welcome === 'Y' || $localStorage.firstlogin_preview === 'Y' || $localStorage.firstlogin_profile === 'Y') {           // PREVENTING DASHBOARD REDIRECTION FOR 1ST TIME TO SHOW TOOLTIPS IN APP MENU SCREEN
                            $location.path('/members');
                        }else{
                                if (($localStorage.studio_expiry_level === 'L1' || $localStorage.studio_expiry_level === 'L2') || ($localStorage.subscription_status === 'N' &&   $localStorage.upgrade_status === 'F')){
                                    $scope.studioexpired();
                                } else {
                                    $scope.dashboard();
                                }
                        }
                    }
                }, 2000);
            } else if (newVal == 'N') {
                if ($location.absUrl().indexOf("token") > -1 || $location.absUrl().indexOf("resetpassword") > -1) {
                    $location.path('/resetpassword');
                } else if ($location.absUrl().indexOf("registration") > -1) {
                    $scope.openRegistration();
                } else if ($location.absUrl().indexOf("forgotpassword") > -1) {
                    $location.path('/forgotpassword');
                } else if ($location.absUrl().indexOf("adminpanel") > 0) {
                    $location.path('/adminpanel');
                }else if ($location.absUrl().indexOf("adminwhitelabel") > 0 && $localStorage.isadminlogin === 'Y') {
                    $location.path('/adminwhitelabel');
                } else if ($location.absUrl().indexOf("adminclonepanel") > 0 && $localStorage.isadminlogin === 'Y') {
                    $location.path('/adminclonepanel');
                } else if ($location.absUrl().indexOf("adminstudiomembers") > 0 && $localStorage.isadminlogin === 'Y') {
                    $location.path('/adminstudiomembers');
                } else if ($location.absUrl().indexOf("adminhome") > 0 && $localStorage.isadminlogin === 'Y') {
                    $location.path('/adminhome');
                } else if ($location.absUrl().indexOf("adminlogintool") > 0 && $localStorage.isadminlogin === 'Y') {
                    $location.path('/adminlogintool');
                } else {
                    $location.path('/login');
                }
            }
        });

        $scope.$watch(function () {
            return $localStorage.user_id;
        }, function (newVal, oldVal) {
            if (newVal != oldVal && newVal != '' && newVal && oldVal !== undefined) {
                setTimeout(function ()
                {
                    $route.reload();
                }, 2000);
            }
        });
        
        $scope.tutorialTour = function () {
            $scope.removeAllActive();
            $localStorage.firstlogin = true;
            $localStorage.firstlogin_welcome = $localStorage.firstlogin_preview = $localStorage.firstlogin_profile = $localStorage.firstlogin_event = $localStorage.firstlogin_message = 'Y';
            $localStorage.firstlogin_curriculum = $localStorage.firstlogin_referral = $localStorage.firstlogin_home = $localStorage.firstlogin_social = 'Y';
            var pagename = window.location.href;
            var pagenameindex = pagename.substr(pagename.lastIndexOf('/') + 1);
            if (pagenameindex === 'members') {
                $route.reload();
            } else {
                $location.path('/members');
            }
        };

        $scope.regComplete = function () {
            //verification code confirm
            if ($localStorage.regstr_complete === undefined || $localStorage.regstr_complete === null || $localStorage.regstr_complete === ''
                    || $localStorage.verfication_complete === undefined || $localStorage.verfication_complete === null || $localStorage.verfication_complete === ''
                    || $localStorage.verfication_complete !== 'Y') {
                $http({
                    method: 'GET',
                    url: urlservice.url + 'checkverification',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8'},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $localStorage.regstr_complete = response.data.msg.registration_complete;       //check verification code successfully completed
                        $localStorage.verfication_complete = response.data.msg.verification_complete;    //check verification not completed & creation within 24hrs
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        console.log(response.data);
                    }
                }, function (response) {
                    $('#progress-full').hide();
                    console.log(response.data);
                });

                if ($localStorage.regstr_complete === 'N' && $localStorage.verfication_complete === 'U') {
                    $("#verifycodemodal").modal({backdrop: 'static', keyboard: false});
                }
            } else {
                // registration completed 
            }
        };
        
        $scope.clearPreviewLocaldata = function () {
            //PREVIEW SINGLE EVENT ADD              
            $localStorage.PreviewEventName = '';
            $localStorage.PreviewEventDate = '';
            $localStorage.PreviewEventTime = '';
            $localStorage.PreviewEventEndDate = '';
            $localStorage.PreviewEventEndTime = '';
            $localStorage.PreviewEventCost = '';
            $localStorage.PreviewEventCompareCost = '';
            $localStorage.PreviewEventUrl = '';
            $localStorage.PreviewEventDesc = '';
            $localStorage.PreviewImageUrl = '';
            $localStorage.PreviewECapacity = '';
            $localStorage.PreviewESpaceRemaining = '';
            $localStorage.PreviewEProcesFeeSelection = '';
            $localStorage.PreviewCEProcesFeeSelection = '';
            $localStorage.PreviewEventRegcols = '';
            $localStorage.eventenabledstatus = '';

            //PREVIEW MULTIPLE- PARENT EVENT ADD
            $localStorage.PreviewMEventCategory = '';
            $localStorage.PreviewMEventSubcategory = '';

            $localStorage.PreviewPEventDesc = '';
            $localStorage.PreviewPEventUrl = '';
            $localStorage.PreviewPImageUrl = '';
            $localStorage.PreviewEditPImageUrl = '';

            //PREVIEW EXISTING CHILD EVENT EDIT
            $localStorage.PreviewCfirstedit = '';
            $localStorage.childeventindex = '';
            $localStorage.PreviewCEventDate = '';
            $localStorage.PreviewCEventTime = '';
            $localStorage.PreviewCEventEndDate = '';
            $localStorage.PreviewCEventEndTime = '';
            $localStorage.PreviewCEventCost = '';
            $localStorage.PreviewCEventCompareCost = '';
            $localStorage.PreviewCEventUrl = '';
            $localStorage.PreviewCEventUrlEdit = '';
            $localStorage.PreviewCEventDescEdit = '';
            $localStorage.PreviewCEventName = '';
            $localStorage.PreviewCECapacity = '';
            $localStorage.PreviewCESpaceRemaining = '';
            $localStorage.PreviewCfirstedit = 'N';

            //PREVIEW NEW CHILD EVENT ADD
            $localStorage.PreviewMEventName = '';
            $localStorage.PreviewMEventDate = '';
            $localStorage.PreviewMEventTime = '';
            $localStorage.PreviewMEventEndDate = '';
            $localStorage.PreviewMEventEndTime = '';
            $localStorage.PreviewMEventUrl = '';
            $localStorage.PreviewMEventDesc = '';
            $localStorage.PreviewMEventCost = '';
            $localStorage.PreviewMEventCompareCost = '';
            $localStorage.preview_eventchildlist = $localStorage.preview_childlisthide = false;
            $localStorage.PreviewMECapacity = '';
            $localStorage.PreviewMESpaceRemaining = '';
            $localStorage.PreviewChildEventList = '';

            $localStorage.PreviewOldRegistrationField = '';
            $localStorage.PreviewOldRegFieldMandatory = '';
            $localStorage.preview_reg_col_name_edit = '';
            $localStorage.preview_reg_fieldindex = '';
            $localStorage.PreviewWaiverText = '';
            $localStorage.currentpreviewtab = '';

            //For membership
            $localStorage.preview_membershipcontent = '';
            $localStorage.PreviewMembershipCategoryName = '';
            $localStorage.PreviewMembershipSubCategoryName = '';
            $localStorage.PreviewMembershipCategoryImage = '';
            $localStorage.PreviewMembershipCategoryVideoUrl = '';
            $localStorage.PreviewMembershipCategoryDesc = '';
            $localStorage.PreviewMembershipOptionTitle = '';
            $localStorage.PreviewMembershipOptionSubtitle = '';
            $localStorage.PreviewMembershipOptionDesc = '';
            $localStorage.PreviewMembershipWaiver = '';
            $localStorage.currentmembershippreviewtab = '';
            $localStorage.childmembershipindex = '';
            $localStorage.preview_membershipchildlist = false;
            $localStorage.preview_member_childlisthide = false;
            $localStorage.PreviewMembershipRegcols = '';
            $localStorage.preview_mem_reg_fieldindex = '';
            $localStorage.preview_mem_reg_col_name_edit = '';
            $localStorage.PreviewOldMemRegFieldMandatory = false;
            $localStorage.PreviewOldMemRegCancel = '';
            $localStorage.PreviewNewMemRegistrationField = '';
            $localStorage.PreviewNewMemRegFieldMandatory = '';
            $localStorage.PreviewOldMemRegistrationField = '';
            $localStorage.PreviewNewMemRegCancel = '';
            $localStorage.preview_mem_reg_fieldindex = '';
            $localStorage.preview_mem_reg_col_name_edit = '';
            $localStorage.PreviewMemProcessingFeeType = '';
            $localStorage.PreviewMemSignUpFee = '';
            $localStorage.PreviewMembershipFee = '';
            $localStorage.PreviewMemRecurringFrequency = '';
            $localStorage.currentpage = '';
            $localStorage.preview_membership_childlist = '';
            $localStorage.PreviewMembershipCustPeriod = '';
            $localStorage.PreviewMembershipCustValue = '';
            $localStorage.preview_membership_option_add = '';

            //Membership Number of classes in payment setup
            $localStorage.PreviewMembershipStructure = '';
            $localStorage.PreviewMembershipDepositValue = '';
            $localStorage.PreviewMembershipExpiryValue = '';
            $localStorage.PreviewMembershipExpiryPeriod = '';
            $localStorage.PreviewMembershipExpiryStatus = '';
            $localStorage.PreviewMembershipBillingOptionValue = '';
            $localStorage.PreviewMembershipNoOfPayments = '';
            $localStorage.PreviewMembershipPaymentStartDate = '';
            $localStorage.PreviewNoofClass = '';
            $localStorage.PreviewMembershipFeeInclude = '';
            $localStorage.PreviewMemCustomPaymentStartDate = '';

            $localStorage.Previewspecific_start_date = '';
            $localStorage.Previewspecific_end_date = '';
            $localStorage.Previewspecific_payment_frequency = '';
            $localStorage.Previewspecific_selecteddays = '';
            $localStorage.Previewexclude_from_billing_flag = '';
            $localStorage.preview_exclude_days_array = '';


            //For Trial Program
            $localStorage.currenttrialpreviewtab = '';
            $localStorage.PreviewTrialName = '';
            $localStorage.PreviewTrialSubName = '';
            $localStorage.PreviewTrialImage = '';
            $localStorage.PreviewTrialDesc = '';
            $localStorage.PreviewTrialPrice = '';
            $localStorage.PreviewTrialProcessingFee = '';
            $localStorage.PreviewTrialprogramlengthquantity = '';
            $localStorage.PreviewTrialprogramlengthperiod = '';
            $localStorage.PreviewTrialWaiver = '';

            $localStorage.PreviewNewTrialRegistrationField = '';
            $localStorage.PreviewNewTrialRegFieldMandatory = '';
            $localStorage.PreviewNewTrialRegCancel = '';
            $localStorage.PreviewOldTrialRegistrationField = '';
            $localStorage.PreviewOldTrialRegFieldMandatory = '';
            $localStorage.preview_Trial_reg_col_name_edit = '';
            $localStorage.PreviewOldTrialRegCancel = '';
            $localStorage.preview_Trial_reg_fieldindex = '';
            $localStorage.PreviewTrialRegcols = '';
            $localStorage.PreviewTrialLeadSource = '';
            $localStorage.trialSorted = 'N';
            
//            For Retail
            $localStorage.currentretailpreviewtab = '';
            $localStorage.PreviewRetailName = '';
            $localStorage.PreviewRetailImage = '';
            $localStorage.PreviewRetailDesc = '';
            $localStorage.PreviewRetailPrice = '';
            $localStorage.PreviewRetailComparePrice = '';
            $localStorage.PreviewRetailVariantFlag = 'Y';
            $localStorage.PreviewRetailTax = '';
            $localStorage.PreviewRetailVariants = '';
            $localStorage.PreviewRetailCatName = '';
            $localStorage.PreviewRetailCatSubname = '';
            $localStorage.PreviewRetailCatImage = '';
            $localStorage.PreviewRetailSalesAgreement = '';
            $localStorage.retailSorted = 'N';
            $localStorage.PreviewRetailVariantName = '';
            $localStorage.retailenabledstatus = '';
            $localStorage.retailVariantSorted = 'N';
            $localStorage.PreviewRetailVariantsprice = '';
            $localStorage.retailTemplate = '';
        };

        //get wepay status of this company
        $scope.getWepayStatus = function () {

            if ($localStorage.preview_wepaystatus === undefined || $localStorage.preview_wepaystatus === null || $localStorage.preview_wepaystatus === ''
                    || $localStorage.preview_currencies === undefined || $localStorage.preview_currencies === null || $localStorage.preview_currencies === ''
                    || $localStorage.preview_wepaystatus !== 'Y') {

                $http({
                    method: 'GET',
                    url: urlservice.url + 'getwepaystatus',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.preview_wepaystatus = $scope.wepaystatus = response.data.msg;
                        $localStorage.preview_currencies = $scope.currencies = response.data.currency;
                        if ($scope.wepaystatus === 'Y') {
                            $localStorage.loginDetails.wp_user_state = 'Done';
                        } else if ($scope.wepaystatus === 'A') {
                            $localStorage.loginDetails.wp_user_state = 'pending';
                        } else {
                            $localStorage.loginDetails.wp_user_state = '';
                        }
                        if($localStorage.preview_wepaystatus !== 'N'){
                            $scope.wepay_enabled = true;
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            } else {
                $scope.wepaystatus = $localStorage.preview_wepaystatus;
                $scope.currencies = $localStorage.preview_currencies;
                if ($scope.wepaystatus === 'Y') {
                    $localStorage.loginDetails.wp_user_state = 'Done';
                } else if ($scope.wepaystatus === 'A') {
                    $localStorage.loginDetails.wp_user_state = 'pending';
                } else {
                    $localStorage.loginDetails.wp_user_state = '';
                }
                if ($localStorage.preview_wepaystatus !== 'N') {
                    $scope.wepay_enabled = true;
                }
            }
        };
        
        //get wepay status of this company
        $scope.getStripeStatus = function () {
            
            if (!$localStorage.preview_stripestatus || !$localStorage.preview_currencies || $localStorage.preview_stripestatus !== 'Y') {
                $scope.wepay_enabled = $scope.stripe_enabled = false;
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getstripestatus',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.preview_stripestatus = $scope.stripestatus = response.data.msg;
                        $localStorage.preview_currencies = $scope.currencies = response.data.currency;
                        if ($scope.stripestatus !== 'N') {
                            $localStorage.preview_wepaystatus = $scope.wepaystatus = 'Y';
                            if ($localStorage.stripe_country_support === 'Y') {
                                $scope.stripe_enabled = true;
                            }else{
                                $scope.getWepayStatus();
                            }
                        }else{
                            $scope.getWepayStatus();
                        }      
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            } else {
                $scope.stripestatus = $localStorage.preview_stripestatus;
                $scope.currencies = $localStorage.preview_currencies;
                if ($scope.stripestatus !== 'N' && $localStorage.stripe_country_support === 'Y') {
                    $scope.stripe_enabled = true;
                    $scope.wepaystatus = $localStorage.preview_wepaystatus = $scope.stripestatus = $localStorage.preview_stripestatus = 'Y';
                } else {
                    $scope.wepay_enabled = $scope.stripe_enabled = false;
                    $scope.getWepayStatus();
                }
            }
            
        };

        $scope.showWepayAccountCreation = function (pagename) {
            $localStorage.linkfrompage = pagename;
            var msg = '';
            if (pagename === 'addmembership') {
                msg = 'Please enable mobile payments to publish this Membership option.';
            } else if (pagename === 'addevent') {
                msg = 'Please enable mobile payments to publish this Event.';
            } else if (pagename === 'addtrial') {
                msg = 'Please enable mobile payments to publish this Trial program.';
            } else if (pagename === 'managemembership') {
                msg = 'Please enable mobile payments to register this Membership option.';
            } else if (pagename === 'manageevent') {
                msg = 'Please enable mobile payments to register this Event.';
            } else if (pagename === 'managetrial') {
                msg = 'Please enable mobile payments to register this Trial program.';
            } else if (pagename === 'customers') {
                msg = 'Please enable mobile payments to add new Member.';
            } else if (pagename === 'manageretail') {
                msg = 'Please enable mobile payments to publish this Online Store.';
            }
            $("#WepayAccountCreationtitle").text('Enable MyStudio Payments');
            $("#WepayAccountCreationcontent").text(msg);
            $("#WepayAccountCreationModal").modal('show');
        };
        
        $scope.WepayAccountReg = function () {
            if ($localStorage.linkfrompage === '' || $localStorage.linkfrompage === undefined) {
                $localStorage.linkfrompage = 'home';
            }
            if($scope.stripestatus === 'N' && $localStorage.stripe_country_support === 'Y'){  // Stripe Payment Enable  
                $location.path('/stripecreation'); 
            }else{
                $location.path('/wepaycreation');
            }
        };

        //setting up wepay environment - stage/production based on hostname 

        $scope.WepayAccountConfirmation = function () {
            if (window.location.href.indexOf("www.mystudio.academy") > -1) {
                window.open('https://wepay.com/login', '_blank');
            } else {
                window.open('https://stage.wepay.com/login', '_blank');
            }
        };

        $scope.verificationCodeUpload = function () {
            $('#progress-full').show();

            $http({
                method: 'POST',
                url: urlservice.url + 'verifyUserRegistration',
                data: {
                    "company_id": $localStorage.company_id,
                    "verification_code": $scope.verifycode
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $localStorage.regstr_complete = 'Y';
                    $localStorage.verfication_complete = 'Y';
                    $('#progress-full').hide();
                    $("#verifycodemodal").modal('hide');
                    $location.path('/home');
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    if (response.data.msg == "Verification code doesn't match.") {
                        $scope.verifycode = "";
                    }
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        $scope.resendVerificationCode = function () {
            $('#progress-full').show();

            $http({
                method: 'POST',
                url: urlservice.url + 'resendverificationlink',
                data: {
                    "company_id": $localStorage.company_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $scope.vercodesuccess = true;
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        $scope.getTutorialMode = function () {
            $scope.removeAllActive();
            if ($localStorage.firstlogin_profile === 'Y') {
                $("#companylink").addClass('active');
                $("#eventlistdown").css("display", "none");
                $("#currlistdown").css("display", "none");
                $location.path('/members');
            } else if ($localStorage.firstlogin_event === 'Y') {
                $("#eventlink").addClass('active');
                $("#eventlistdown").css("display", "block");
                $("#currlistdown").css("display", "none");
                $location.path('/manageevent');
            } else if ($localStorage.firstlogin_message === 'Y') {
                $("#eventlistdown").css("display", "none");
                $("#currlistdown").css("display", "none");
                $("#msglink").addClass('active');
                $location.path('/message');
            } else if ($localStorage.firstlogin_curriculum === 'Y') {
                $("#currlink").addClass('active');
                $("#currlistdown").css("display", "block");
                $("#eventlistdown").css("display", "none");
                $scope.initialShowCurriculum();
            } else if ($localStorage.firstlogin_referral === 'Y') {
                $("#eventlistdown").css("display", "none");
                $("#currlistdown").css("display", "none");
                $("#referrallink").addClass('active');
                $location.path('/referral');
            } else if ($localStorage.firstlogin_home === 'Y') {
                $("#eventlistdown").css("display", "none");
                $("#currlistdown").css("display", "none");
                $("#homelink").addClass('active');
                $location.path('/home');
            } else if ($localStorage.firstlogin_social === 'Y') {
                $("#eventlistdown").css("display", "none");
                $("#currlistdown").css("display", "none");
                $("#sociallink").addClass('active');
                $location.path('/social');
            } else {
                $localStorage.firstlogin = false;
            }
        };

        $scope.testData = {};
        $scope.realname = $scope.testData.name;
        $scope.testhide = "true";
        $scope.thide = "";

        $scope.testMemData = {};
        $scope.realmembershipname = $scope.testMemData.name;
        $scope.testmemhide = "true";
        $scope.tmemhide = "";

        $scope.testeventData = {};
        $scope.realeventname = $scope.testeventData.name;
        $scope.testeventhide = "true";
        $scope.teventhide = "";

        $scope.testmessageData = {};
        $scope.realmessagename = $scope.testmessageData.name;
        $scope.testmessagehide = "true";
        $scope.tmessagehide = "";

        $scope.testtrialData = {};
        $scope.realtrialname = $scope.testtrialData.name;
        $scope.testtrialhide = "true";
        $scope.ttrialhide = "";

        $scope.testleadsData = {};
        $scope.testleadsData.name = "Leads";
        $scope.realleadsname = $scope.testleadsData.name;
        $scope.testleadshide = "true";
        $scope.tleadshide = "";

        $scope.testRetailData = {};
        $scope.testRetailData.name = "Retail";
        $scope.testretailhide = "true";
        $scope.tretailhide = "";
        $rootScope.stop_continuous_call = false;
        $scope.new_message_count = {};
        $scope.new_message_count.count = $localStorage.new_conversation_Count;
        
        $scope.getMenuName = function () {

            if (!$localStorage.PreviewDownEvent || !$localStorage.PreviewDownMembership || !$localStorage.PreviewDownTrial || !$localStorage.PreviewDownleads || !$localStorage.PreviewDownRetail || !$localStorage.PreviewDownCurriculum
                    || !$localStorage.PreviewDownMessage || $localStorage.menuNameUpdated === 'Y') {
                
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getMenuNames',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8'},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $localStorage.menuNameUpdated = 'N';
                        $localStorage.menuList = $scope.menuList = response.data.msg;
                        $scope.testData.name = $scope.menuList.curriculum_name;
                        $scope.testeventData.name = $scope.menuList.event_name;
                        $scope.testMemData.name = $scope.menuList.membership_name;
                        $scope.testmessageData.name = $scope.menuList.message_name;
                        $scope.testtrialData.name = $scope.menuList.trial_name;
                        $scope.testleadsData.name = $scope.menuList.leads_name;
                        $scope.testRetailData.name = $scope.menuList.retail_name;
                        $localStorage.PreviewDownEvent = $scope.testeventData.name;
                        $localStorage.PreviewDownMembership = $scope.testMemData.name;
                        $localStorage.PreviewDownTrial = $scope.testtrialData.name;
                        $localStorage.PreviewDownleads = $scope.testleadsData.name;
                        $localStorage.PreviewDownRetail = $scope.testRetailData.name;
                        $localStorage.PreviewDownCurriculum = $scope.testData.name;
                        $localStorage.PreviewDownMessage = $scope.testmessageData.name;
                        
                        $scope.builderror_flag = false;
                        $scope.builderror_text = "";
                        //whitelabel check

                        if ($localStorage.loginDetails.bundleid_error_flag == "N") {
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Bundle ID error";
                        } else if ($localStorage.loginDetails.credential_error_flag == "N") {
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Username or Password error";
                        } else if ($localStorage.loginDetails.teamid_error_flag == "N") {
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Team ID error";
                        } else if ($localStorage.loginDetails.metadata_error != "" && $localStorage.loginDetails.metadata_error != null) {
                            $scope.builderror_flag = true;
                            $scope.builderror_text = $localStorage.loginDetails.metadata_error;
                        } else if ($localStorage.loginDetails.ios_icon_error == "N") {
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Ios Icon error. Please upload new image";
                        } else if ($localStorage.loginDetails.android_icon_error == "N") {
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Android Icon error. Please upload new image"
                        } else if ($localStorage.loginDetails.ios_screenshot_error == "N") {
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Ios Screenshot error. Please upload new image";
                        } else if ($localStorage.loginDetails.android_screenshot_error == "N") {
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Android Screenshot error. Please upload new image"
                        } else {
                            $scope.builderror_flag = false;
                        }
                        if($scope.builderror_flag){
                            $localStorage.builderror_flag = $scope.builderror_flag;
                            $localStorage.builderror_text = $scope.builderror_text;
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            } else {
                $scope.menuList = $localStorage.menuList;
                $scope.testMemData.name = $localStorage.PreviewDownMembership;
                $scope.testtrialData.name = $localStorage.PreviewDownTrial;
                $scope.testleadsData.name = $localStorage.PreviewDownleads;
                $scope.testeventData.name = $localStorage.PreviewDownEvent;
                $scope.testRetailData.name = $localStorage.PreviewDownRetail;
                $scope.testData.name = $localStorage.PreviewDownCurriculum;
                $scope.testmessageData.name = $localStorage.PreviewDownMessage;
            }
        };
        
        $scope.gowhitelabelsetup = function(){
            $location.path('/whitelabelsetup');
        }

        $scope.updateMenuList = function (membershipname, eventname, curriculumname, messagename, trialname, leadsname, retailname) {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'updateMenuNames',
                data: {
                    "membership_name": membershipname,
                    "event_name": eventname,
                    "curriculum_name": curriculumname,
                    "message_name": messagename,
                    "company_id": $localStorage.company_id,
                    "trial_name": trialname,
                    "leads_name": leadsname,
                    "retail_name":retailname
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $localStorage.menuNameUpdated = 'Y';
                    $scope.getMenuName();
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $('#progress-full').hide();
                    $scope.getMenuName();
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        $scope.catchLeftMenuChange = function () {
            $localStorage.currentpage = "leftmenuedit";
            $localStorage.PreviewDownMembership = $scope.testMemData.name;
            $localStorage.PreviewDownTrial = $scope.testtrialData.name;
            $localStorage.PreviewDownleads = $scope.testleadsData.name;
        };

        $scope.catcheChange = function () {
            $localStorage.PreviewDownEvent = $scope.testeventData.name;
        };
        $scope.catchmChange = function () {
            $localStorage.PreviewDownMessage = $scope.testmessageData.name;
        };
        $scope.catchcChange = function () {
            $localStorage.PreviewDownCurriculum = $scope.testData.name;
        };

        $scope.showme = $scope.showrename = false;
        $rootScope.sideBarHide = false;
        $scope.products = [];
        $(window).scroll(function () {
            var fixedBottom = $(".preview-down");
            if ($('body').height() <= ($(window).height() + $(window).scrollTop() + 500)) {
                $(".preview-down").hide();
            } else {
                if ($(window).width() > 1190) {
                    $(".preview-down").hide();
                } else {
                    $(".preview-down").show();
                }
            }
        });
        $(window).resize(function () {
            if ($(window).width() > 1190) {
                $(".preview-down").hide();
            } else {
                $(".preview-down").show();
            }
        });

        $scope.getName = function () {
            if ($localStorage.sideEvent == false) {
                $scope.sideEvent = false;
            } else {
                $scope.sideEvent = true;
            }

            if ($localStorage.isUpdatedCompanyDetails == 'Y') {

                $scope.cmpnylogo = $localStorage.company_details.logo_URL;
                if ($scope.cmpnylogo == null) {
                    $scope.cmpnylogo = "";
                }
                $scope.cmpnynme = $localStorage.company_details.company_name;
                $scope.cmpnycode = $localStorage.company_details.company_code;
            } else {
                $scope.cmpnylogo = $localStorage.loginDetails.logo_URL;
                if ($scope.cmpnylogo == null) {
                    $scope.cmpnylogo = "";
                }
                $scope.cmpnynme = $localStorage.loginDetails.company_name;
                $scope.cmpnycode = $localStorage.loginDetails.company_code;
            }

            if ($localStorage.subscription_status === 'N' && $localStorage.upgrade_status === 'T' && parseInt($localStorage.no_of_login) < 2) {
                $scope.rem_days_show = true;
                $scope.isexpire = false;               
                $scope.rem_days = $localStorage.loginDetails.remaining_days;
                if (parseInt($scope.rem_days) == 1) {
                    $scope.remaining_days = $scope.rem_days + ' ' + 'day.';
                } else if (parseInt($scope.rem_days) == 0 || parseInt($scope.rem_days) < 1) {
                    $scope.isexpire = true;
                } else if (parseInt($scope.rem_days) > 1) {
                    $scope.remaining_days = $scope.rem_days + ' ' + 'days.';
                }
            } else if ($localStorage.subscription_status === 'N' && $localStorage.upgrade_status === 'F' && parseInt($localStorage.no_of_login) < 2) {
                $scope.isexpire = true;
                $scope.rem_days_show = true;
            } else {
                $scope.rem_days_show = false;
            }

            if (($localStorage.loginDetails.wp_user_state === undefined || $localStorage.loginDetails.wp_user_state === null || $localStorage.loginDetails.wp_user_state === '') && $localStorage.preview_stripestatus !== 'Y') {
                $scope.wepay_status_show = false;
                $scope.wepay_text_show = false;
                $localStorage.preview_wepaystatus = 'N';
                $scope.wepay_text = "";
            } else {
                $scope.wepay_status_show = true;
                $scope.wepay_text_show = true;
                $scope.wepay_text = "";
                $scope.wp_user_state = $localStorage.loginDetails.wp_user_state;
                if ($scope.wp_user_state === 'pending' || $scope.wp_user_state === 'deleted' || $scope.wp_user_state === 'action_required' || $scope.wp_user_state === 'disabled') {
                    $scope.wepay_text = "Get paid! Check your email, confirm your WePay account, and enter your bank information.";
                    $localStorage.preview_wepaystatus = 'A';
                } else {
                    $localStorage.preview_wepaystatus = 'Y';
                    $scope.wepay_status_show = false;
                    $scope.wepay_text_show = false;
                }
            }

            if ($localStorage.loginDetails.last_payment_status === undefined || $localStorage.loginDetails.last_payment_status === null || $localStorage.loginDetails.last_payment_status === '' || $localStorage.loginDetails.last_payment_status === 'Success') {
                $scope.paysimple_status_show = false;
                $scope.paysimple_text_show = false;
                $scope.paysimple_text = "";
            } else {
                $scope.paysimple_status_show = true;
                $scope.paysimple_text_show = true;
                $scope.paysimple_text = "Your last payment has been failed. Please update payment method or your subscription will be suspended.";
            }
        };

        $scope.paynow = function () {
            $location.path('/billingstripe');
        };

        $scope.msgdone = function () {
            setTimeout(function () {
                $route.reload();
            }, 200);
        };

        //Adding multiple Level values - for-loop function 
        $scope.toggleLevelName = function () {
            $scope.alleditClosed();
            $scope.showme = !$scope.showme;
            if ($localStorage.firstlogin_curriculum === 'Y') {
                $scope.showme = true;
            }
        };

        //initial show of curriculum level
        $scope.initialShowCurriculum = function () {
            $scope.alleditClosed();
            $scope.menuhide();

            //First time login curriculum message Pop-up ----START 
            tooltip.hide();
            $scope.currtip = true;

            if ($localStorage.verfication_complete !== 'U') {
                if ($localStorage.firstlogin) {
                    if ($localStorage.firstlogin_curriculum === 'Y') {
                        $("#tooltipfadein").css("display", "block");
                        $localStorage.firstlogin_curriculum = 'N';
                        $scope.currtip = false;
                        tooltip.pop("currlink", "#curriculumtip", {position: 1});
                    }
                }
            }

            $("#curriculumdetailNext").click(function () {
                $("#tooltipfadein").css("display", "none");
                $scope.currtip = true;
                tooltip.hide();
                $scope.toggleLevelName();
            });
            //First time login curriculum message Pop-up ----END


            if ($scope.products.length >= 1) {
                $scope.levelClick($scope.products[0].curr_name, $scope.products[0].curriculum_id, $scope.products[0].curr_detail_id);
            } else {
                $scope.showme = !$scope.showme;
            }
        };
//        Muthulakshmi
        $scope.getRedirectStripeURL = function () {
            $http({
                method: 'POST',
                url: urlservice.url + 'updatePersonalDetails',
                data: {
                    "company_id": $localStorage.company_id,
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') { 
                    $scope.stripe_redirect_onboarding_link = response.data.msg.url;
                    $('#progress-full').hide();
                } else if (response.data.status === 'Expired') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Failed') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                }                 
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };
        $scope.gotoStripePayout = function () {
            window.open($scope.stripe_redirect_onboarding_link, "_blank");
            $("#striperefresh-Modal").modal('show');
        };
        $scope.getStripeRefreshDetails = function () {
            $("#striperefresh-Modal").modal('hide');
            $http({
                method: 'GET',
                url: urlservice.url + 'getStripePayoutDetails',
                params: {
                    "company_id": $localStorage.company_id,
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') { 
                    $('#progress-full').hide();
                    $localStorage.stripe_status = response.data.msg.stripe_status;
                    $localStorage.stripe_charges_enabled = response.data.msg.charges_enabled;
                    $window.location.reload();
                } else if (response.data.status === 'Expired') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Failed') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                }                 
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };
        // If stripe account enabled but not payment enabled
        if ($localStorage.stripe_country_support === 'Y' &&  $localStorage.stripe_status === 'Y' && $localStorage.stripe_charges_enabled === 'N') {
            $scope.stripe_text_show = true;
            $scope.getRedirectStripeURL();
        }
        
        //curriculum
        $scope.editHeader = function () {
            if ($scope.showme === true)
                $scope.close();
            if ($scope.tmemhide)
                $scope.emembershipClose();
            if ($scope.ttrialhide)
                $scope.trialClose();
            if ($scope.teventhide)
                $scope.eeventClose();
            if ($scope.tmessagehide)
                $scope.emessageClose();
            if ($scope.tleadshide)
                $scope.leadsClose();
            if ($scope.tretailhide)
                $scope.eretailClose();
            $scope.testhide = "";
            $scope.thide = "true";
        };
        $scope.eClose = function () {
            $scope.menuList = $localStorage.menuList;
            $scope.testData.name = $scope.menuList.curriculum_name;
            $localStorage.PreviewDownCurriculum = $scope.testData.name;
            $scope.testhide = "true";
            $scope.thide = "";
        };
        $scope.eSave = function (eval) {
            $scope.testhide = "true";
            $scope.thide = "";
            $scope.updateMenuList($scope.testMemData.name, $scope.testeventData.name, $scope.testData.name, $scope.testmessageData.name, $scope.testtrialData.name, $scope.testleadsData.name,  $scope.testRetailData.name);
        };
//        Membership
        $scope.editmembershipHeader = function () {
            if ($scope.showme === true)
                $scope.close();
            if ($scope.thide)
                $scope.eClose();
            if ($scope.tmessagehide)
                $scope.emessageClose();
            if ($scope.ttrialhide)
                $scope.trialClose();
            if ($scope.tleadshide)
                $scope.leadsClose();
            if ($scope.teventhide)
                $scope.eeventClose();
            $scope.testmemhide = "";
            $scope.tmemhide = "true";
        };
        $scope.emembershipClose = function () {
            $scope.menuList = $localStorage.menuList;
            $scope.testMemData.name = $scope.menuList.membership_name;
            $localStorage.PreviewDownMembership = $scope.testMemData.name;
            $scope.testmemhide = "true";
            $scope.tmemhide = "";
            $localStorage.currentpage = "";
        };
        $scope.emembershipSave = function (eval) {
            $scope.testmemhide = "true";
            $scope.tmemhide = "";
            $scope.updateMenuList($scope.testMemData.name, $scope.testeventData.name, $scope.testData.name, $scope.testmessageData.name, $scope.testtrialData.name, $scope.testleadsData.name, $scope.testRetailData.name);
        };

        //event
        $scope.editeventHeader = function () {
            if ($scope.showme === true)
                $scope.close();
            if ($scope.thide)
                $scope.eClose();
            if ($scope.tmemhide)
                $scope.emembershipClose();
            if ($scope.ttrialhide)
                $scope.trialClose();
            if ($scope.tleadshide)
                $scope.leadsClose();
            if ($scope.tmessagehide)
                $scope.emessageClose();
            if ($scope.tretailhide)
                $scope.eretailClose();
            $scope.testeventhide = "";
            $scope.teventhide = "true";
        };
        $scope.eeventClose = function () {
            $scope.menuList = $localStorage.menuList;
            $scope.testeventData.name = $scope.menuList.event_name;
            $localStorage.PreviewDownEvent = $scope.testeventData.name;
            $scope.testeventhide = "true";
            $scope.teventhide = "";
        };
        $scope.eeventSave = function (eval) {
            $scope.testeventhide = "true";
            $scope.teventhide = "";
            $scope.updateMenuList($scope.testMemData.name, $scope.testeventData.name, $scope.testData.name, $scope.testmessageData.name, $scope.testtrialData.name, $scope.testleadsData.name, $scope.testRetailData.name);
        };

        $scope.updateTrialName = function () {
            $scope.testtrialhide = "true";
            $scope.ttrialhide = "";
            $scope.updateMenuList($scope.testMemData.name, $scope.testeventData.name, $scope.testData.name, $scope.testmessageData.name, $scope.testtrialData.name, $scope.testleadsData.name, $scope.testRetailData.name);
        };

        $scope.updateleadsName = function () {
            $scope.testleadshide = "true";
            $scope.tleadshide = "";
            $scope.updateMenuList($scope.testMemData.name, $scope.testeventData.name, $scope.testData.name, $scope.testmessageData.name, $scope.testtrialData.name, $scope.testleadsData.name, $scope.testRetailData.name);
        };

        //trial
        $scope.editTrialHeader = function () {
            if ($scope.showme === true)
                $scope.close();
            if ($scope.thide)
                $scope.eClose();
            if ($scope.tleadshide)
                $scope.leadsClose();
            if ($scope.tmemhide)
                $scope.emembershipClose();
            if ($scope.teventhide)
                $scope.eeventClose();
            if ($scope.tmessagehide)
                $scope.emessageClose();
            if ($scope.tretailhide)
                $scope.eretailClose();
            $scope.ttrialhide = "true";
            $scope.testtrialhide = "";
        };

        $scope.trialClose = function () {
            $scope.menuList = $localStorage.menuList;
            $scope.testtrialData.name = $scope.menuList.trial_name;
            $localStorage.PreviewDownTrial = $scope.testtrialData.name;
            $scope.testtrialhide = "true";
            $scope.ttrialhide = "";
            $localStorage.currentpage = "";
        };

        //leads name edit
        $scope.editleadsHeader = function () {
            if ($scope.showme === true)
                $scope.close();
            if ($scope.thide)
                $scope.eClose();
            if ($scope.tmemhide)
                $scope.emembershipClose();
            if ($scope.teventhide)
                $scope.eeventClose();
            if ($scope.tmessagehide)
                $scope.emessageClose();
            if ($scope.ttrialhide)
                $scope.trialClose();
            if ($scope.tretailhide)
                $scope.eretailClose();
            $scope.tleadshide = "true";
            $scope.testleadshide = "";
        };

        $scope.leadsClose = function () {
            $scope.menuList = $localStorage.menuList;
            $scope.testleadsData.name = $scope.menuList.leads_name;
            $localStorage.PreviewDownleads = $scope.testleadsData.name;
            $scope.testleadshide = "true";
            $scope.tleadshide = "";
            $localStorage.currentpage = "";
        };

//         Retail Edit Name
        $scope.editretailHeader = function () {
            if ($scope.showme === true)
                $scope.close();
            if ($scope.thide)
                $scope.eClose();
            if ($scope.tmemhide)
                $scope.emembershipClose();
            if ($scope.tmessagehide)
                $scope.emessageClose();
            if ($scope.ttrialhide)
                $scope.trialClose();
            if ($scope.tleadshide)
                $scope.leadsClose();
            if ($scope.teventhide)
                $scope.eeventClose();
            $scope.testretailhide = "";
            $scope.tretailhide = "true";
        };
        $scope.eretailClose = function () {
            $scope.menuList = $localStorage.menuList;
            $scope.testRetailData.name = $scope.menuList.retail_name;
            $localStorage.PreviewDownRetail = $scope.testRetailData.name;
            $scope.testretailhide = "true";
            $scope.tretailhide = "";
            $localStorage.currentpage = "";
        };
        $scope.eretailSave = function (eval) {
            $scope.testretailhide = "true";
            $scope.tretailhide = "";
            $scope.updateMenuList($scope.testMemData.name, $scope.testeventData.name, $scope.testData.name, $scope.testmessageData.name, $scope.testtrialData.name, $scope.testleadsData.name, $scope.testRetailData.name);
        };

        //message
        $scope.editmessageHeader = function () {
            if ($scope.showme === true)
                $scope.close();
            if ($scope.thide)
                $scope.eClose();
            if ($scope.tmemhide)
                $scope.emembershipClose();
            if ($scope.ttrialhide)
                $scope.trialClose();
            if ($scope.tleadshide)
                $scope.leadsClose();
            if ($scope.teventhide)
                $scope.eeventClose();
            if ($scope.tretailhide)
                $scope.eretailClose();
            $scope.testmessagehide = "";
            $scope.tmessagehide = "true";
        };
        $scope.emessageClose = function () {
            $scope.menuList = $localStorage.menuList;
            $scope.testmessageData.name = $scope.menuList.message_name;
            $localStorage.PreviewDownMessage = $scope.testmessageData.name;
            $scope.testmessagehide = "true";
            $scope.tmessagehide = "";
        };
        $scope.emessageSave = function (eval) {
            $scope.testmessagehide = "true";
            $scope.tmessagehide = "";
            $scope.updateMenuList($scope.testMemData.name, $scope.testeventData.name, $scope.testData.name, $scope.testmessageData.name, $scope.testtrialData.name, $scope.testleadsData.name);
        };
        //Adding new level 
        $scope.addLevel = function (pro) {
            $scope.alleditClosed();
            $scope.errortext = "";
            if (!$scope.addMe) {
                $scope.errortext = "Enter level name";
                return;
            }
            $scope.showme = !$scope.showme;
            $scope.insertCurriculumLevel(pro);
            $scope.addMe = "";
        };

        //Adding new level close
        $scope.close = function () {
            $scope.showme = !$scope.showme;
            $scope.addMe = $scope.errortext = "";
        };

        //Existing level edit
        $scope.edit = function (editind, item) {
            $scope.showrename = !$scope.showrename;
            $scope.editIndexValue = editind;
            $scope.editSingleValue = item;
        };


        //level rename edit update
        $scope.rename = function (pro, ind, curid, curdetailid) {
            $scope.renameerrortext = "";
            if (!pro) {
                $scope.renameerrortext = "Rename the level name";
                return;
            }
            $scope.products[ind].curr_name = pro;
            $scope.showrename = !$scope.showrename;
            $scope.updateCurriculumLevel(pro, ind, curid, curdetailid);
        };

        //level rename edit close
        $scope.renameclose = function (pro, ind) {
            $scope.showrename = !$scope.showrename;
            $scope.renameerrortext = "";
            if (!pro) {
                $scope.products[ind].curr_name = $scope.editSingleValue;
                return;
            } else {
                $scope.products[ind].curr_name = $scope.editSingleValue;
                return;
            }
        };

        $scope.removeDragSection = function (event) {
            $(event.currentTarget).prop("draggable", false);
            $(event.currentTarget.closest('li')).prop("draggable", false);
        };

        $scope.addDragSection = function (event) {
            $(event.currentTarget).prop("draggable", true);
            $(event.currentTarget.closest('li')).prop("draggable", true);
        };

        $scope.alleditClosed = function () {
            if ($scope.thide)
                $scope.eClose();
            if ($scope.teventhide)
                $scope.eeventClose();
            if ($scope.tmessagehide)
                $scope.emessageClose();
            if ($scope.tmemhide)
                $scope.emembershipClose();
            if ($scope.ttrialhide)
                $scope.trialClose();
            if ($scope.tleadshide)
                $scope.leadsClose();
        };

        $scope.menuhide = function () {
            var windowsize = $(window).width();
            if (windowsize < 992) {
                $('.nav-md').addClass('nav-sm').removeClass('nav-md');
            }
        };
        
        $scope.whitelabelclick = false;
        
        $scope.whitelabelsetup = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $localStorage.currentMembershipregid = '';
            $localStorage.membershippagetype = '';
            $localStorage.page_from = '';
            $scope.whitelabelclick = !$scope.whitelabelclick;
            $location.path('/whitelabelsetup');
            
        };

        $scope.home = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $localStorage.currentMembershipregid = '';
            $localStorage.membershippagetype = '';
            $localStorage.page_from = '';
            $location.path('/dashboard');
        };

        $scope.Customers = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $localStorage.addmembershipfrommemberstab = false;
            $localStorage.currentMembershipregid = '';
            $localStorage.membershippagetype = '';
            $localStorage.page_from = '';
            $location.path('/customers');
        };
        $scope.dashboard = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $localStorage.currentMembershipregid = '';
            $localStorage.membershippagetype = '';
            $localStorage.page_from = '';
            $location.path('/dashboard');
        };
        
        //studio expired
        $scope.studioexpired = function(){
            $scope.alleditClosed();
            $scope.menuhide();
            $location.path('/billingstripe'); 
        };
        
        //check studio expired
        $scope.checkstudioexpired = function () {
                if ($localStorage.studio_expiry_level === 'L2') {
                    $rootScope.sideBarHide = false;
                    $rootScope.hidesidebarlogin = false;
                    $rootScope.adminsideBarHide = false;
                    $scope.alleditClosed();
                    $scope.menuhide();
                    if ($('#rerunsuccess-modal').length)
                    {
                        $('#rerunsuccess-modal').on('hidden.bs.modal', function (e) {
                            $location.path('/billingstripe');
                        })
                    } else {
                        $location.path('/billingstripe');
                    }
                }
        };
        
        //show studio inactive modal
        $scope.showstudioinactivemodal = function () {
            $('#inactive-modal').modal('show');
        };
        
        //redirect from inactive modal
        $scope.redirectfrominactivemodal = function () {
            $location.path('/billingstripe');
        };
        
        $scope.AppUsers = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $localStorage.currentMembershipregid = '';
            $localStorage.membershippagetype = '';
            $localStorage.page_from = '';
            $location.path('/home');
        };

        $scope.removeAllActive = function () {            
            $("#homelink").removeClass('active');
            $("#leadslink").removeClass('active');
            $("#triallink").removeClass('active');
            $("#membership").removeClass('active');
            $("#eventlink").removeClass('active');
            $("#msglink").removeClass('active');
            $("#attendanceportal").removeClass('active');
            $("#myaccountlink").removeClass('active');
//            $("#currlink").removeClass('active');
            $("#sociallink").removeClass('active');
            $("#referrallink").removeClass('active');
            $("#companylink").removeClass('active');
            $("#appverification").removeClass('active');
            $("#retail").removeClass('active');
            $("#integrations").removeClass('active');
            $("#pos").removeClass('active');
            $("#mystripeaccountlink").removeClass('active');
            $rootScope.conv_selected_student_id = $rootScope.currentpage_for_conversation = $rootScope.conv_last_msg_id = $rootScope.conv_last_msg_datetime = $rootScope.student_last_msg_id = $rootScope.conversation_search_term = ''; //for conversation interval call
            $rootScope.from_tab = 'I';
        };

        $scope.logout = function (response_for) {
            //for sidemaenu hiding
            $('#companyname').removeClass('active');
            $("#companylogout").css("display", "none");
            Intercom('shutdown');
            $('#progress-full').show();

            if ($localStorage.isadminlogin === 'Y') {
                $scope.adminlogin = 'Y';
            } else {
                $scope.adminlogin = 'N';
            }

            $http({
                method: 'GET',
                url: urlservice.url + 'logout',
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success' || response.data.status === 'Expired' || response.data.status === 'Version') {
                    $('#progress-full').hide();
                    tooltip.hide();
                    $scope.removeAllActive();
                    $scope.att_company_id = $localStorage.att_company_id;
                    $scope.att_user_email_id = $localStorage.att_user_email_id;
                    if($localStorage.admincmpnynme){
                        $scope.admincmpnynme = $localStorage.admincmpnynme;
                    }else{
                        $scope.admincmpnynme = "";
                    }
                    $scope.pos_email_id = $localStorage.pos_user_email_id;  // For copy url , Not for POS Launch
                    $rootScope.activepagehighlight = '';
                    $localStorage.$reset();
                    $localStorage.att_company_id = $scope.att_company_id;
                    $localStorage.att_user_email_id = $scope.att_user_email_id;
                    $localStorage.admincmpnynme = $scope.admincmpnynme;
                    $localStorage.pos_user_email_id = $scope.pos_email_id ? $scope.pos_email_id : "";   // For copy url , Not for POS Launch
                    $scope.logoutDivOpen = false;
                    $rootScope.convCountTriggers = false;
                    $scope.products = [];
                    if ($scope.adminlogin === 'Y') {
                        if (response_for === "Expired") {
                            $localStorage.isadminlogin = 'N';
                            $location.path('/adminpanel');
                            setTimeout(function () {
                                $route.reload();
                            }, 200);
                        } else {
                            $localStorage.isadminlogin = 'Y';
                            $location.path('/adminhome');
                        }
                        $scope.chatBot();
                    } else {
                        $localStorage.islogin = 'N';
                        $location.path('/login');
                        $scope.chatBot();
                    }
                    //Cancel the Timer.
                    if (angular.isDefined($scope.MsgTimer)) {
                        $interval.cancel($scope.MsgTimer);
                    }
                } else {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        $scope.manageList = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $localStorage.eventHeader = false;
            $localStorage.membershippagetype = '';
            $localStorage.preview_eventcontent = '';
            $localStorage.page_from = '';
            $localStorage.manage_page_from = "";
            $localStorage.addmembershipfrommemberstab = false;
            //redirecting to members/membership options page
            $http({
                method: 'POST',
                url: urlservice.url + 'openMembershipTabs',
                data: {
                    "company_id": $localStorage.company_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    if (response.data.msg === 'members') {
                        $location.path('/customers');
                    } else if (response.data.msg === 'membershipoptions') {
                        $location.path('/managemembership');
                    }

                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        $scope.eventList = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $localStorage.eventHeader = false;
            $location.path('/manageevent');
        };

        $scope.payment = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $localStorage.eventHeader = false;
            $localStorage.page_from = '';
            $localStorage.stripe_onboard_status = "";
            $location.path('/payment');
        };

        $scope.message = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $location.path('/message');
        };


        $scope.manageTrial = function () {
            $localStorage.trialpagetype = '';
            $localStorage.addtriallocaldata = '';
            $localStorage.triallocaldata = '';
            $scope.alleditClosed();
            $scope.menuhide();
            $localStorage.trialredirectto = "DA";
            $location.path('/managetrial');
        };

        $scope.manageleads = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $localStorage.leadspagefrom = "";
            $location.path('/manageleads');
        };

        $scope.levelClick = function (pro, curriculum_id, cur_detail_id) {
            $scope.alleditClosed();
            $scope.levelname = pro;
            $scope.levelnameid = curriculum_id;
            $localStorage.previewCurriculumID = curriculum_id;
            $localStorage.curriculumlocalSingledata = "";
            curriculumLevelDetails.addSingleEvent(pro, curriculum_id, cur_detail_id);
            $location.path('/curriculum');
            $route.reload();
        };
        $scope.social = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $location.path('/social');
        };
        $scope.members = function (view) {
            $localStorage.scroll_to = '';
            $scope.alleditClosed();
            $scope.menuhide();
            $location.path('/members');
            if(view === 'companycode'){
                $localStorage.scroll_to = 'companycode';
            }
        };
        $scope.referral = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $location.path('/referral');
        };
        $scope.stripebilling = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $location.path('/billingstripe');
        };

        $scope.attendance = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $location.path('/attendancedashboard');
        };

        $scope.appverification = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $location.path('/appverification');
        };
        
        $scope.pos = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $location.path('/posdashboard');
        };
        
        $scope.openRetail = function () {
            $scope.alleditClosed();
            $scope.menuhide();
             //redirecting to members/membership options page
            $http({
                method: 'POST',
                url: urlservice.url + 'openRetailTabs',
                data: {
                    "company_id": $localStorage.company_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    if (response.data.msg === 'F') {
                        $location.path('/retailfulfillment');
                    } else if (response.data.msg === 'R') {
                        $location.path('/manageretail');
                    }
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });   
        };
        
        $scope.integrations = function () {
            $scope.alleditClosed();
            $scope.menuhide();
            $location.path('/integrations');
        };

        $scope.insertCurriculumLevel = function (pro) {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'addcurriculum',
                data: {
                    "user_id": $localStorage.user_id,
                    "curriculum_name": pro,
                    "company_id": $localStorage.company_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $localStorage.insertedcurriculumlevels = "";
                    $localStorage.insertedcurriculumlevels = response.data.msg;

                    if ($localStorage.firstlogin && $localStorage.firstlogin_curriculum === 'Y') {      // first time section adding after registration                     
                        $localStorage.firstlogin_curriculum = 'N';
                        $localStorage.firstlogin_curriculum_section = 'Y';

                    } else if ($localStorage.firstlogin_curriculum === 'N' || $localStorage.firstlogin_curriculum === undefined) {
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Success Message');
                        $("#messagecontent").text('Curriculum section successfully Added');
                    }
                    angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
                    $scope.levelClick($localStorage.insertedcurriculumlevels.curriculum[$localStorage.insertedcurriculumlevels.curriculum.length - 1].curr_name, $localStorage.insertedcurriculumlevels.curriculum[$localStorage.insertedcurriculumlevels.curriculum.length - 1].curriculum_id, $localStorage.insertedcurriculumlevels.curriculum[$localStorage.insertedcurriculumlevels.curriculum.length - 1].curr_detail_id);

                } else if (response.data.status === 'Expired') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                $scope.handleError(response.data);
            });
        };



        $scope.getCurriculumLevel = function () {

            if ($localStorage.curriculumlocalSingledata === undefined || $localStorage.curriculumlocalSingledata === null || $localStorage.curriculumlocalSingledata === '') {

                $http({
                    method: 'GET',
                    url: urlservice.url + 'getcurriculumdetails',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8'},
                    withCredentials: true

                }).then(function (response) {
                    $localStorage.previewCurriculumLevelData = response.data;
                    if (response.data.status == 'Success') {
                        $scope.getMenuName();
                        $scope.products = response.data.msg.curriculum;
                        $localStorage.curriculumlocalSingledata = response.data.msg.curriculum;
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $scope.getMenuName();
                        $scope.products = [];
                    }
                }, function (response) {
                    $scope.handleError(response.data);
                });
            } else {
                $scope.getMenuName();
                $scope.products = $localStorage.curriculumlocalSingledata;
            }
        };

        $scope.updateCurriculumLevel = function (pro, perind, curid, curdetailid) {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'updatecurriculum',
                data: {
                    "curriculum_id": curid,
                    "curriculum_name": pro,
                    "company_id": $localStorage.company_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Success Message');
                    $("#messagecontent").text('Curriculum section successfully Updated');
                    $localStorage.updatedcurriculumlevels = response.data.msg;
                    angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
                    $localStorage.curriculumlocalSingledata = "";
                    $scope.levelClick($localStorage.updatedcurriculumlevels.curriculum[perind].curr_name, $localStorage.updatedcurriculumlevels.curriculum[perind].curriculum_id, $localStorage.updatedcurriculumlevels.curriculum[perind].curr_detail_id);

                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Failure Message');
                    $("#messagecontent").text(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        $scope.deleteCurriculumLevel = function (id) {

            $scope.deletelevelvalue = id;
            $("#curriculumLevelDeleteModal").modal('show');
            $("#curriculumLevelDeletetitle").text('Delete');
            $("#curriculumLevelDeletecontent").text("Are you sure want to delete level? ");

        };

        $scope.deleteCancel = function () {
            console.log('cancel delete');
        };

        $scope.deleteConfirm = function () {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'deletecurriculum',
                data: {
                    "curriculum_id": $scope.deletelevelvalue,
                    "company_id": $localStorage.company_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Success Message');
                    $("#messagecontent").text('Curriculum section successfully deleted');
                    $localStorage.updatedcurriculumlevels = response.data.msg;

                    if ($scope.products.length == 1) {
                        $scope.products = [];
                        $localStorage.curriculumlocalSingledata = "";
                        $scope.showme = !$scope.showme;
                        $location.path('/home');
                    } else {
                        $scope.customCurriculumLevel();

                    }

                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $('#progress-full').hide();
                    if ($scope.products.length == 0) {
                        $scope.showme = !$scope.showme;
                        $location.path('/home');
                    }
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        $scope.customCurriculumLevel = function () {
            $http({
                method: 'GET',
                url: urlservice.url + 'getcurriculumdetails',
                params: {
                    "company_id": $localStorage.company_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $scope.products = response.data.msg.curriculum;
                    $localStorage.curriculumlocalSingledata = response.data.msg.curriculum;
                    $scope.levelClick($scope.products[0].curr_name, $scope.products[0].curriculum_id, $scope.products[0].curr_detail_id);
                } else if (response.data.status === 'Expired') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $('#progress-full').hide();

                }
            }, function (response) {
                $scope.handleError(response.data);
            });
        };

        $scope.dragCallback = function () {
            var sortarr = [];
            var curr_id_array = [];
            var sort;
            var curr_id;
            $('#progress-full').show();
            setTimeout(function () {
                for (var i = 0; i < $scope.products.length; i++) {
                    curr_id = sort = "";
                    curr_id = $scope.products[i].curriculum_id;
                    sort = document.getElementById(curr_id).value;
                    sortarr[i] = parseInt(sort) + 1;
                    curr_id_array[i] = curr_id;
                }
                $scope.sortorderupdate(sortarr, curr_id_array);
            }, 1000);

        };

        $scope.sortorderupdate = function (sortarr, curr_id_array) {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'updateCurriculumSortingOrder',
                data: {
                    "sort_id": sortarr,
                    "curriculum_id": curr_id_array,
                    "company_id": $localStorage.company_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    setTimeout(function () {
                        $route.reload();
                    }, 200);
                } else if (response.data.status === 'Expired') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                $scope.handleError(response.data);
            });
        };
        $rootScope.currentwltab = 'DASHBOARD';
        $scope.catchwltab = function(tab){
            $rootScope.currentwltab = tab;
        }
        
        $scope.handleError = function (response) {
            if (response.status === 'Expired') {
                $('#progress-full').hide();
                $("#messageModal").modal('show');
                $("#messagetitle").text('Message');
                $("#messagecontent").text(response.msg);
                $scope.logout(response.status);
            } else if (response.status === 'Version') {
                $('#progress-full').hide();
                $scope.logout();
                var version =  response.msg.split("-")[1];
                window.location.href = version;
                $route.reload();
            } else {
                $('#progress-full').hide();
                $("#messageModal").modal('show');
                $("#messagetitle").text('Server Message');
                $("#messagecontent").text('Invalid server response');
            }
        };

        $scope.handleAdminError = function (response) {
            if (response.status === 'Expired') {
                $('#progress-full').hide();
                $("#failuremessageModal").modal('show');
                $("#failuremessagetitle").text('Message');
                $("#failuremessagecontent").text(response.msg);
                $scope.adminlogout();
            } else if (response.status === 'Version') {
                $('#progress-full').hide();
                $scope.adminlogout();
                var version =  response.msg.split("-")[1];
                window.location.href = version;
                $route.reload();
            } else {
                $('#progress-full').hide();
                $("#messageModal").modal('show');
                $("#messagetitle").text('Server Message');
                $("#messagecontent").text('Invalid server response');
            }
        };

        $scope.handleFailure = function (responsemsg) {
            if (responsemsg.includes("Latest Version-")) {
                $scope.logout();
                var version =  responsemsg.split("-")[1];
                $('#progress-full').hide();
                window.location.href = version;
                $route.reload();
            }else{
                $('#progress-full').hide();
                $("#failuremessageModal").modal('show');
                $("#failuremessagetitle").text('Message');
                $("#failuremessagecontent").text(responsemsg);
            }
        };


        //admin redirections
        $scope.adminhome = function () {
            $location.path('/adminhome');
        };
        $scope.adminmembers = function () {
            $location.path('/adminstudiomembers');
        };
        $scope.adminlogintool = function () {
            $location.path('/adminlogintool');
        };

        $scope.adminclonepanel = function () {
            $rootScope.sideBarHide = true;
            $('#progress-full').show();
            // SESSION VERIFICATION TO PREVENT MULTIPLE USERS SIMULTANEOUSLY
            $http({
                method: 'GET',
                url: urlservice.url + 'verifySession',
                params: {
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $location.path('/adminclonepanel');
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    console.log(response.data);
                    tooltip.hide();
                    angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
                    $localStorage.$reset();
                    $scope.products = [];
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };
        
        $scope.adminWLdashboard = function () {
            $rootScope.sideBarHide = true;
            $('#progress-full').show();
            // SESSION VERIFICATION TO PREVENT MULTIPLE USERS SIMULTANEOUSLY
            $http({
                method: 'GET',
                url: urlservice.url + 'verifySession',
                params: {
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $location.path('/adminwhitelabel');
                } else {
                    console.log(response.data);
                    tooltip.hide();
                    angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
                    $localStorage.$reset();
                    $scope.products = [];
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        $scope.adminlogout = function () {
            $('#companyname').removeClass('active');
            $("#companylogout").css("display", "none");
            Intercom('shutdown');
            $('#progress-full').show();
            $http({
                method: 'GET',
                url: urlservice.url + 'adminlogout',
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success' || response.data.status === 'Expired' || response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.pos_email_id = $localStorage.pos_copy_email_id;
                    $localStorage.$reset();
                    $localStorage.pos_copy_email_id = $scope.pos_email_id ? $scope.pos_email_id : "";
                    $rootScope.sideBarHide = true;
                    $rootScope.adminsideBarHide = false;
                    $rootScope.hidesidebarlogin = true;
                    $scope.logoutDivOpen = false;
                    $location.path('/adminpanel');
                } else {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        $scope.chatBot = function () {
            if ($localStorage.islogin == 'Y') {
                window.intercomSettings = {
                    app_id: "pw4g2jok",
                    name: $localStorage.loginDetails.company_name, // Full name
                    email: $localStorage.loginDetails.email_id, // Email address
                    created_at: $localStorage.loginDetails.created_date // Signup date as a Unix timestamp
                };
            } else {
                window.intercomSettings = {
                    app_id: "pw4g2jok"
//                    name: '', // Full name
//                    email: '', // Email address
//                    created_at: '' // Signup date as a Unix timestamp
                };
            }
            var head = document.getElementsByTagName('head')[0];
            var script = document.createElement('script');
            script.src = "js/intercom_chat.js?v=" + new Date().getTime();
            head.appendChild(script);
        };
        $scope.chatBot();

        // READONLY INPUTS
        $scope.preventTyping = function ($event) {
            if ($event != "undefined") {
//                $event.preventDefault();
            }
        };
        $scope.openLogoutDiv = function(){
            $scope.logoutDivOpen = $scope.logoutDivOpen === false ? true: false;
        };
//        Conversation count GET call     
        $scope.convCountCall = function(status){
            if(!$rootScope.convCountTriggers){
                $rootScope.convCountTriggers = true;
                $scope.MsgTimer = $interval(function () {
                    if(!$rootScope.stop_continuous_call && $localStorage.islogin === 'Y'){
                        $rootScope.stop_continuous_call = true;
                        $localStorage.new_conversation_array = [];
                        $localStorage.new_chat_array = [];
                        $http({
                            method: 'GET',
                            url: urlservice.url + 'communicationIntervalCall',
                            params: {
                                "company_id": $localStorage.company_id,
                                "conv_current_page":$rootScope.currentpage_for_conversation,
                                "selected_student_id":$rootScope.conv_selected_student_id,
                                "last_msg_id":$rootScope.conv_last_msg_id,
                                "last_messagedatetime":$rootScope.conv_last_msg_datetime,
                                "student_last_msg_id":$rootScope.student_last_msg_id,
                                "unread_count":$scope.new_message_count.count,
                                "search_term":$rootScope.conversation_search_term,
                                "from_tab":$rootScope.from_tab
                            },
                            headers: {"Content-Type": 'application/json; charset=utf-8',
                                },
                                  withCredentials: true
                        }).then(function (response) {
                            if (response.data.status === 'Success') {
                                $('#progress-full').hide();
                                $rootScope.stop_continuous_call = false;
                                $localStorage.new_conversation_Count = '';
                                $scope.new_message_count.count = response.data.new_conversation_count;
                                $localStorage.new_conversation_Count = response.data.new_conversation_count;
                                $localStorage.new_conversation_array = response.data.conversation_array;
                                $localStorage.new_chat_array = response.data.chat_array;
                            }else if(response.data.status === 'Expired'){
                                $rootScope.stop_continuous_call = false;
                            } else if (response.data.status === 'Version') {
                                $('#progress-full').hide();
                                $scope.handleFailure(response.data.msg);
                            }else{
                                $rootScope.stop_continuous_call = false;
                                $('#progress-full').hide();
                                $("#messageModal").modal('show');
                                $("#messagetitle").text('Message');
                                $("#messagecontent").text(response.data.msg);
                            }
                        }, function (response) {
                            $rootScope.stop_continuous_call = false;
                            $('#progress-full').hide();
                            console.log(response.data);
                        });
                    }
                }, 30000); 
            }
        };
        
        $rootScope.getinstantmessage = function(){
            $localStorage.new_conversation_Count = '';
                    $localStorage.new_conversation_array = [];
                    $localStorage.new_chat_array = [];
                    $http({
                        method: 'GET',
                        url: urlservice.url + 'communicationIntervalCall',
                        params: {
                            "company_id": $localStorage.company_id,
                            "conv_current_page":$rootScope.currentpage_for_conversation,
                            "selected_student_id":0,
                            "last_msg_id":$rootScope.conv_last_msg_id,
                            "last_messagedatetime":$rootScope.conv_last_msg_datetime,
                            "student_last_msg_id":$rootScope.student_last_msg_id,
                            "unread_count":$scope.new_message_count.count,
                            "search_term":$rootScope.conversation_search_term,
                            "from_tab":$rootScope.from_tab
                        },
                        headers: {"Content-Type": 'application/json; charset=utf-8',
                            },
                              withCredentials: true
                    }).then(function (response) {
                        if (response.data.status === 'Success') {
                            $('#progress-full').hide();
                            $scope.new_message_count.count = response.data.new_conversation_count;
                            $localStorage.new_conversation_Count = response.data.new_conversation_count;
                            $localStorage.new_conversation_array = response.data.conversation_array;
                            $localStorage.new_chat_array = response.data.chat_array;
                        } else if (response.data.status === 'Version') {
                            $('#progress-full').hide();
                            $scope.handleFailure(response.data.msg);
                        } else {
                            $('#progress-full').hide();
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Message');
                            $("#messagetitle").text(response.data.msg);
                        }
                    }, function (response) {
                        $('#progress-full').hide();
                        console.log(response.data);
                    });
        };
        
        
        $scope.togglemobilemenu = function(){
            $("#leftsidebar").toggleClass("togglesidemobilemeny");
        }
        
        
        //End Conversation count get call
        $scope.$watch(function () {return $localStorage.new_conversation_Count;}, function (newVal, oldVal) {
        $scope.new_message_count.count = $localStorage.new_conversation_Count;
        });
        $localStorage.builderror_flag = false;
//        whitelabel
        $scope.$watch(function () {return $localStorage.builderror_flag}, function (newVal, oldVal) {
                            $scope.builderror_flag = $localStorage.builderror_flag;
                            $scope.builderror_text = $localStorage.builderror_text;
        });

        
    }])

    .service('curriculumLevelDetails', function () {
        var curriculumSingleLevelValue;

        var addSingleLevel = function (pro, curriculum_id, cur_detail_id) {
            curriculumSingleLevelValue = [];
            curriculumSingleLevelValue.push(pro);
            curriculumSingleLevelValue.push(curriculum_id);
            curriculumSingleLevelValue.push(cur_detail_id);
        };

        var getSingleLevel = function () {
            return curriculumSingleLevelValue;
        };

        return {
            addSingleEvent: addSingleLevel,
            getSingleLevel: getSingleLevel
        };

    })
        .directive('valueRepeatComplete', function () {
            return function (scope, element, attrs) {
                if (scope.$last) {
                    // iteration is complete, do whatever post-processing
                    // is necessary
                    $('#progress-full').hide();
                } else {
                    $('#progress-full').show();
                }
            };
        })

        .directive('tableRepeatComplete', function () {
            return function (scope, element, attrs) {
                if (scope.$last) {
                    setTimeout(function ()
                    {
                        $('#progress-full').hide();
                    }, 2000);
                } else {
                    $('#progress-full').show();
                }
            };
        })        

        .factory('urlservice', function () {
            var hosturl;
            if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
                       window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                           window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                    var origin = window.location.origin;
                    var version = window.location.pathname.split("/")[1];
                    hosturl = origin + '/' + version;
                }
            } else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 4).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 4).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'beta.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 4).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 4).join('/');
                    hosturl = 'https://' + www_url;
                }
            } else if (window.location.hostname === 'stage.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 4).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 4).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'localhost') {
                hosturl = 'http://' + window.location.hostname + "/nila/Mystudio_webapp/vx";
            } else {
                hosturl = 'http://' + window.location.hostname + "/nila/Mystudio_webapp/vx";
            }
            return {
                url: hosturl + '/Api/PortalApi/'
            };
        });