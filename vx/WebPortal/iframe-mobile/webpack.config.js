var path = require('path');
var webpack = require('webpack');
var HtmlWebPackPlugin = require("html-webpack-plugin");
var MiniCssExtractPlugin = require("mini-css-extract-plugin");
var CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  
    entry: {
        app:['./js/my-app.js','./css/custom.css','./css/my-app.css'],
        initservice:'./js/services/InitService.js',
        indexpage: './js/controller/IndexPageController.js',
       
      
    },
    output: {
        filename: '[name].[contenthash].js',
        path: __dirname + '/dist'
    },
    mode:'development',
    devServer: {
      contentBase: path.join(__dirname, 'dist'),
      compress: true,
      // proxy: {
      //   '/api': {
      //       target: 'http://localhost:80',
      //       secure: false
      //   },
      // },
      port: 9000,
      disableHostCheck: true,
    },
    module: {
        rules: [
          {
            test: /\.html$/,
            use: [
              {
                loader: "html-loader",
                options: { minimize: true },
               
              }
            ]
          },
          {
            test: /\.(png|svg|jpg|gif|jpeg)$/,
            use: [
            //   'file-loader'
            {
                loader: 'file-loader',
                
              }
              
            ]
           
          },
          {
            test: /\.(sa|sc|c)ss$/,
            exclude:/node_modules/,
            use: [
             MiniCssExtractPlugin.loader,
              'css-loader',
             ],
             
          },
          {
            test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
            use: [{
                loader: 'file-loader',
               
            }]
         }
    
        ]
      },
      plugins: [
        new HtmlWebPackPlugin({
            template: "index.html",
            filename: "./index.html",
            chunks:'[name].[contenthash]'
          }),
          new CopyPlugin([
            { from: './js/Angular', to: './js/Angular' },
            { from: './js/framework7.min.js', to: './js/framework7.min.js' },
            { from: './css/framework7.ios.css', to: './css/framework7.ios.css' },
            { from: './css/img', to: './css/img' },
      ]),
    new MiniCssExtractPlugin({
      filename: "[name].css",
    })
      ]
      

}; 
