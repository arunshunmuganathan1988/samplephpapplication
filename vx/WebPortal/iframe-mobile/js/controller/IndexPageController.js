/*jslint browser: true*/
/*global console, MyApp*/
var MyApp =angular.module('index', []);
var f7 = new Framework7();
MyApp.fw7 = {
  app : new Framework7({
    //animateNavBackIcon: true
    pushState: true,
    swipeBackPage: true,
    swipeBackPageAnimateShadow:true,
    swipeBackPageAnimateOpacity:true,
    cache: false
  }),
  options : {
                                                              dynamicNavbar: true,
                                                      //    domCache: true
                                                          },

  views : []
  
};

var $$ = Dom7;
MyApp.controller('IndexPageController', ['$rootScope', '$scope', '$http', '$localStorage', 'urlservice', 'InitService', '$location', function ($rootScope, $scope, $http, $localStorage, urlservice, InitService, $location) {

        InitService.addEventListener('ready', function ()
        {
            $scope.preloaderVisible = true;
            //Dynamic MSG Variables
            $scope.showDMsgView = false;
            $scope.showDMsgVieww = false;
            $scope.showDMsgUrlView = false;
            $scope.newDMsgUrlText = "";
            
            //Dynamic EVENT Variables
            //For Single Events
            $scope.newDEventNameText = "";
            $scope.newDEventDateText = "";
            $scope.newDEventTimeText = "";
            $scope.newDEventCostText = "";            
            $scope.newDEventCompareCostText = "";
            $scope.showDEventName = false;
            $scope.showEventAddEdit = false;
            $scope.showMembershipAddEdit = false;
            $scope.showTrialAddEdit = false;
            $scope.showDEventDate = false;
            $scope.showDEventTime = false;
            $scope.showDDEventDateOnly = false;
            $scope.showDDEventEndDateOnly = false;
            $scope.newDEventProcesFee = '';
            $scope.showDEventCost = false;
            $scope.showDEventCompareCost = false;
            $scope.newDEventurlText = "";
            $scope.showDEventurl = false;
            $scope.newDEventVideourlText = "";
            $scope.newDEventImage = "";
            $scope.showDEventImage = false;
            $scope.showDEventVideo = false;
            $scope.showEEvent = false;
            $scope.showEventNewReg = false;
            $scope.singleeditquantity = '1';
            $scope.singlequantity = '1';
            
            //For Multiple Events
            $scope.newDMEventCategoryText = "";
            $scope.newDMEventSubCategoryText = "";
            $scope.newDMEventNameText = "";
            $scope.newDMEventDateText = "";
            $scope.newDMEventTimeText = "";
            $scope.newDMEventCostText = "";            
            $scope.newDMEventCompareCostText = "";
            $scope.showDMEventName = false;  
            $scope.showDMEventCategory = false;
            $scope.newDMEventImage = "";
            $scope.showDMEventImage = false;
            $scope.showDMEventVideo = false;
            $scope.showEMEvent = false;
           
            
            $scope.newDPEventVideourlText = '';
            $scope.newDPEventdescText = '';
            $scope.newDPEventImage = '';
            $scope.newDEditPEventImage = '';
            $scope.showDPEventVideo = false;
            $scope.showDPEventdesc = false;
            $scope.showDPEventImage = false;
            $scope.showDEditPEventImage = false;
            
            $scope.newDCEventImage = '';
            $scope.showDCEventImage = false;
            $scope.newMEventCapacity = '';
            $scope.newMEventSpaceRemaining = '';
            $scope.showMEventCapacity = false;
            $scope.showMEventSpaceRemaining = false;

            
            //$scope.showEditEventName = false;
            $scope.showDMEventDate = false;
            $scope.showDMEventTime = false;
            $scope.showDDMEventDateOnly = true;
            $scope.showDMEventCost = false;
            $scope.showDMEventCompareCost = false;
            $scope.showDMEventurl = false;
            $scope.newDMEventurlText = "";
            
            //For Child Events
            $scope.eventChildContent = '';
            $scope.showDDCEventDateOnly = false;
            $scope.showDCEventName = false;
            $scope.showDCEventTime = false;
            $scope.showDCEventCompareCost = false;
            $scope.showCMEventdesc = false;
            $scope.eventlistshow = false;
            $scope.newDCEventProcesFee = '';
            $scope.showDCEventCost = false;
            $scope.showDCEventDate = false;
            $scope.showDDCEventEndDate = false;
            $scope.newDCEventVideourlText = '';
            $scope.showDCEventVideo = false;
            $scope.childeditquantity = '1';
            $scope.childquantity = '1';
            $scope.eventenabledstatus = '';
            
            //For Membership
           $scope.newMembershipCategoryText = '';
           $scope.newMembershipSubCategoryText = '';
           $scope.newMembershipCategoryImage = '';
           $scope.newMembershipCategoryUrl = '';
           $scope.newMembershipCategorydesc = '';
           $scope.newMembershipOptionText = '';
           $scope.newMembershipOptionSubText = '';
           $scope.newMembershipOptiondesc = '';
           $scope.newMemNewRegText = '';
           $scope.newMemNewRegMandate = '';
           $scope.newMemNewRegText = '';
           $scope.newMemEditRegText = '';
           $scope.newMemEditRegMandate = '';
           $scope.newMemEditRegText = '';
           $scope.newMemWaiverText = '';
           $scope.newMemOptionAdd = false;
           
            $scope.showMemCategoryname = false;
            $scope.showMemSubCategoryname = false;
            $scope.showMemCategoryImage = false;
            $scope.showMemCategoryUrl = false;
            $scope.showMemCategoryDesc = false;
            $scope.showMemOptionname = false;
            $scope.showMemOptionSubname = false;
            $scope.showMemOptionDesc = false;
            $scope.showMemNewReg = false;
            $scope.showMemNewRegMandate = false;
            $scope.showMemOldRegMandate = false;
            $scope.showMemEditReg = false;
            $scope.showMemOldReg = false;
            $scope.showMemWaiver = false;
            $scope.memberlistshow = false; 
            
            $scope.newTotalFee = 0;
            $scope.newTotalRecurringFee = 0;
            $scope.newMembershipFee_processing_fee = 0;
            $scope.newMemProcessingFee = '2';
            $scope.newSignupFee = 0;
            $scope.newMembershipFee = 0;
            $scope.newTotalMembershipFee = 0;
            $scope.pay_infull_processingfee = 0;
            $scope.newMemRecFrequency = 'M';
            $scope.newMembershipFeeInclude = 'Y';
            $scope.newMemRecValue = '';
            $scope.showMemProcessingFee = false;
            $scope.showSignupFee = false;
            $scope.showMembershipFee = false;
            $scope.showMemRecFrequency = false;
            
            $scope.newMemStructure = '';
            $scope.newNoofclass = 0;
            $scope.newMemExpVal = '';
            $scope.showMemExpVal = false;
            $scope.newMemExpStatus = '';
            $scope.newMemExpPer = '';
            $scope.newMemBillingOption = '';
            $scope.newMemDepositAmnt = '';
            $scope.showMemDepositAmnt = false;
            $scope.newMemNoOfPayments = '';
            $scope.showMemNoOfPayments = false;
            $scope.newMemPaymntStartDate = '';
            $scope.mem_reg_col_names = '';
            $scope.newSEStartDate = '';
            $scope.newSEEndDate = '';
            $scope.newSpecificFrequency = '';     
//            $scope.newbilling_days = ''; 
            $scope.newexclude_from_billing_flag = ''; 
            $scope.new_exclude_days_array = [];
            
            
            //For Trial        
            
            $scope.newTrialText = '';
            $scope.showTrialname = false;
            $scope.newTrialSubText = '';
            $scope.showTrialSubname = false;
            $scope.newTrialImage = '';
            $scope.showTrialImage = false;
            $scope.newTrialPrgmUrl = '';
            $scope.showTrialPrgmUrl = false;
            $scope.newTrialdesc = '';
            $scope.showTrialdesc = false;
            $scope.newTrialPrice = 0;
        
            $scope.trial_reg_col_names = '';
            $scope.newTrialWaiverText = '';            
            $scope.showTrialWaiver = false;                     
            
            $scope.showTrialPrice = false;
            $scope.newTrialProcessingFee = '';
            $scope.showTrialProcessingFee = false;
            $scope.newTrialProgramQuantity = '';
            $scope.showTrialProgramQuantity = false;
            $scope.newTrialProgramPeriod = '';
            $scope.showTrialProgramPeriod = false;
                        
            $scope.showDStudioTrial = false;
            $scope.showDStudioMembership = false;
            
            //Dynamic CURRICULUM Variables
            $scope.showDCurrName = false;
            $scope.newDCurrNameText = "";
            $scope.showDCurrDesc = false;
            $scope.newDCurrDescText = "";
            $scope.newDCurrurlText = "";
            $scope.showDCurrurl = false;
            //LOGO VARIABLE
            $scope.newDLogoImage = "";
            $scope.showDLogoImage = false;
            $scope.wp_currency_code = $localStorage.wp_currency_code;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            
//            For Retail Variable Muthulakshmi
            $scope.showRetailname = false;
            $scope.showRetailCatname = false;
            $scope.showRetailprice = false;
            $scope.showRetailcompareprice = false;
            $scope.newRetailText = '';
            $scope.newRetaildesc = '';
            $scope.newRetailImage = '';
            $scope.newRetailvariantflag = 'Y';
            $scope.newRetailtax = '';
            $scope.newRetailprice = '';
            $scope.newRetailvariantprice = '';
            $scope.newRetailcompareprice = '';
            $scope.newRetailCatText = '';
            $scope.newRetailCatSubtext = '';
            $scope.newRetailCatImage = '';
            $scope.newRetailSalesAgreement = '';
            $scope.newRetailVariantName = '';
            $scope.retailenabledstatus = '';
            $scope.retailtemplateview = false;
            
             //$scope.companydetails();
            if ($localStorage.currentpage === "events" || $localStorage.currentpage === "home") {
                $scope.companydetails();
                $scope.showEventAddEdit = true;
                
            }else if($localStorage.currentpage ==="Template")
            {
                $scope.companydetails();
                $scope.showEventAddEdit = true;
                f7.showTab('#view-1');
                $scope.$watch(function () {return $localStorage.templateEvents;}, function (newVal, oldVal) {
                $scope.eventContent = $localStorage.templateEvents;
                if($scope.eventContent.event_type === 'S'){ 
                    $scope.preview_event_type = 'S';
                    $scope.showEEvent = true;
                    $scope.showEMEvent = false;
                    $scope.newDEventNameText = $scope.eventContent.event_title; 
                    if($scope.newDEventNameText === '' || $scope.newDEventNameText === undefined){
                        $scope.showDEventName = false;
                    }else{
                        $scope.showDEventName = true;
                    }
                    $scope.newDEventImage = $scope.eventContent.event_banner_img_url;
                    if($scope.newDEventImage === '' || $scope.newDEventImage === undefined){
                        $scope.showDEventImage = false;
                    }else{
                        $scope.showDEventImage = true;
                    }
                    
                    $scope.newDEventVideourlText = $scope.eventContent.event_video_detail_url;
                    if($scope.newDEventVideourlText === '' || $scope.newDEventVideourlText === undefined){
                        $scope.showDEventVideo = false;
                    }else{
                        $scope.showDEventVideo = true;
                    }
                    
                    $scope.newDEventurlText = $scope.eventContent.event_more_detail_url;
                    if($scope.newDEventurlText === '' || $scope.newDEventurlText === undefined){
                        $scope.showDEventurl = false;
                    }else{
                        $scope.showDEventurl = true;
                    }
                    
                    $scope.newDEventDateText = $scope.eventContent.event_begin_dt.split(' ')[0];
                    $scope.newDEventTimeTextt = $scope.eventContent.event_begin_dt.split(' ')[1];
                    
                    //Edit event start date time
                    if($scope.eventContent.event_begin_dt === undefined || $scope.eventContent.event_begin_dt === '0000-00-00 00:00:00' ){
                            $scope.showDEventTime=false;
                            $scope.showDDEventDateOnly = false;
                            $scope.newDEventDateText = "";
                            $scope.newDEventTimeTextt="";
                        
                    }else if($scope.eventContent.event_begin_dt !== undefined || $scope.eventContent.event_begin_dt !== '0000-00-00 00:00:00' ){
                        if(($scope.newDEventDateText !== undefined && $scope.newDEventTimeTextt === '00:00:00')|| ($scope.newDEventDateText !== '0000-00-00'&& $scope.newDEventTimeTextt === '00:00:00')){
                            $scope.showDEventTime=false;
                            $scope.showDDEventDateOnly = true;
                            $scope.newDEventDateText = $scope.eventContent.event_begin_dt.split(' ')[0];
                            $scope.newDEventTimeTextt="";
                        }else if(($scope.newDEventDateText !== '0000-00-00' && $scope.newDEventTimeTextt !== undefined )|| ($scope.newDEventDateText !== '0000-00-00' && $scope.newDEventTimeTextt !== '00:00:00')){
                            $scope.showDEventTime=true;
                            $scope.showDDEventDateOnly = false;
                            $scope.newDEventDateText = $scope.eventContent.event_begin_dt.split(' ')[0];
                            $scope.newDEventTimeTextt = $scope.eventContent.event_begin_dt;
                        }
                    }
                
                    
                    $scope.newDEventEndDateText = $scope.eventContent.event_end_dt.split(' ')[0];
                    $scope.newDEventEndTimeTextt = $scope.eventContent.event_end_dt.split(' ')[1];
                    
                    //Edit event end date time
                    if($scope.eventContent.event_end_dt === undefined || $scope.eventContent.event_end_dt === '0000-00-00 00:00:00' ){
                            $scope.showDEventEndTime=false;
                            $scope.showDDEventEndDateOnly = false;
                            $scope.newDEventEndDateText = "";
                            $scope.newDEventEndTimeTextt="";
                        
                    }else if($scope.eventContent.event_end_dt !== undefined || $scope.eventContent.event_end_dt !== '0000-00-00 00:00:00' ){
                        if(($scope.newDEventEndDateText !== undefined && $scope.newDEventEndTimeTextt === '00:00:00')|| ($scope.newDEventEndDateText !== '0000-00-00'&& $scope.newDEventEndTimeTextt === '00:00:00')){
                            $scope.showDEventEndTime=false;
                            $scope.showDDEventEndDateOnly = true;
                            $scope.newDEventEndDateText = $scope.eventContent.event_end_dt.split(' ')[0];
                            $scope.newDEventEndTimeTextt="";
                        }else if(($scope.newDEventEndDateText !== '0000-00-00' && $scope.newDEventTimeTextt !== undefined )|| ($scope.newDEventEndDateText !== '0000-00-00' && $scope.newDEventTimeTextt !== '00:00:00')){
                            $scope.showDEventEndTime=true;
                            $scope.showDDEventEndDateOnly = false;
                            $scope.newDEventEndDateText = $scope.eventContent.event_end_dt.split(' ')[0];
                            $scope.newDEventEndTimeTextt = $scope.eventContent.event_end_dt;
                        }
                    }  
                      
                    
                    $scope.newDEventCostText = $scope.eventContent.event_cost;
                    if($scope.newDEventCostText === '0.00' || $scope.newDEventCostText === undefined){
                        $scope.showDEventCost = false;
                    }else{
                        $scope.showDEventCost = true;
                    }
                    $scope.newDEventCompareCostText = $scope.eventContent.event_compare_price;
                    if($scope.newDEventCompareCostText === '0.00' || $scope.newDEventCompareCostText === undefined || $scope.newDEventCompareCostText === null){
                        $scope.showDEventCompareCost = false;
                    }else{
                        $scope.showDEventCompareCost = true;
                    }
                    
                    $scope.newDEventdescText = $scope.eventContent.event_desc;
                    if($scope.newDEventdescText === '' || $scope.newDEventdescText === undefined){
                        $scope.showDEventdesc = false;
                    }else{
                        $scope.showDEventdesc = true;
                    }
                    
                    $scope.newDEventCapacity = $scope.eventContent.event_capacity;
                    if($scope.newDEventCapacity === '' || $scope.newDEventCapacity === undefined || $scope.newDEventCapacity === 0){
                        $scope.showDEventSpaceRemaining = false;
                    }else{
                        $scope.showDEventSpaceRemaining = true;
                    }
                    
                    $scope.newDEventSpaceRemaining = $scope.eventContent.capacity_text;
                    if($scope.newDEventSpaceRemaining === '' || $scope.newDEventSpaceRemaining === undefined){
                        $scope.showDEventSpaceRemaining = false;
                    }else{
                        $scope.showDEventSpaceRemaining = true;
                    }
                    
                    $scope.newDEventProcesFee = $scope.eventContent.processing_fees;
                    if($scope.newDEventProcesFee === '2') {
                            $scope.EditEventCost = $scope.newDEventCostText;
                            $scope.EditEventProcessCost = $scope.getProcessingCost($scope.newDEventCostText);
                            $scope.EditEventTotalProcessCost = $scope.TotalEventProcessingCost($scope.newDEventCostText);
                    }
                     
                    $scope.reg_col_names = $scope.eventContent.reg_columns; 
                    if($scope.eventContent.waiver_policies === null || $scope.eventContent.waiver_policies === undefined || $scope.eventContent.waiver_policies === '' ){ 
                        $scope.newEventWaiverText = '';
                        $scope.showEventWaiver = false;                         
                    } else{
                        $scope.newEventWaiverText = $scope.eventContent.waiver_policies;
                        $scope.showEventWaiver = true;
                    }
                    
                }else if($scope.eventContent.event_type === 'M'){   
                    $scope.preview_event_type = 'S';
                    $scope.showEMEvent = true;
                    $scope.showEEvent = false;
                    $scope.newDMEventImage = $scope.eventContent.event_banner_img_url;
                    if($scope.newDMEventImage === '' || $scope.newDMEventImage === undefined){
                        $scope.showDMEventImage = false;
                    }else{
                        $scope.showDMEventImage = true;
                    }
                    
                    $scope.newDMEventCategoryText = $scope.eventContent.event_title;
                    if($scope.newDMEventCategoryText === '' || $scope.newDMEventCategoryText === undefined){
                        $scope.showDMEventCategory = false;
                    }else{
                        $scope.showDMEventCategory = true;
                    }
                    $scope.newDMEventSubCategoryText = $scope.eventContent.event_category_subtitle;
                    if($scope.newDMEventSubCategoryText === '' || $scope.newDMEventSubCategoryText === undefined){
                        $scope.showDMEventSubCategory = false;
                    }else{
                        $scope.showDMEventSubCategory = true;
                    }
                    
                    $scope.newDPEventVideourlText = $scope.eventContent.event_video_detail_url;
                    if($scope.newDPEventVideourlText === '' || $scope.newDPEventVideourlText === undefined){
                        $scope.showDPEventVideo = false;
                    }else{
                        $scope.showDPEventVideo = true;
                    }
                    
                    $scope.newDPEventdescText = $scope.eventContent.event_desc;
                    if($scope.newDPEventdescText === '' || $scope.newDPEventdescText === undefined){
                        $scope.showDPEventdesc = false;
                    }else{
                        $scope.showDPEventdesc = true;
                    }
                    
                    if( $scope.eventContent.event_type==='M' && $scope.eventChildContent.length > 0){
                        $scope.eventChildContent = $scope.eventContent.child_events;
                    }else{
                        $scope.eventChildContent = "";
                    }
                    $scope.reg_col_names = $scope.eventContent.reg_columns; 
                    if($scope.eventContent.waiver_policies === null || $scope.eventContent.waiver_policies === undefined || $scope.eventContent.waiver_policies === '' ){ 
                        $scope.newEventWaiverText = '';
                        $scope.showEventWaiver = false;                         
                    } else{
                        $scope.newEventWaiverText = $scope.eventContent.waiver_policies;
                        $scope.showEventWaiver = true;
                    }
                }
              });
                
            }else if($localStorage.currentpage === "editevents" || $localStorage.currentpage === "addevents"){
                $scope.companydetails();
                f7.showTab('#view-1');
                
                $scope.$watch(function () {return $localStorage.preview_eventcontent;}, function (newVal, oldVal) {
                    $scope.eventContent = $localStorage.preview_eventcontent;
                if($scope.eventContent.event_type === 'S'){ 
                    $scope.preview_event_type = 'S';
                    $scope.showEEvent = true;
                    $scope.showEMEvent = false;
                    $scope.newDEventNameText = $scope.eventContent.event_title; 
                    if($scope.newDEventNameText === '' || $scope.newDEventNameText === undefined){
                        $scope.showDEventName = false;
                    }else{
                        $scope.showDEventName = true;
                    }
                    $scope.newDEventImage = $scope.eventContent.event_banner_img_url;
                    if($scope.newDEventImage === '' || $scope.newDEventImage === undefined){
                        $scope.showDEventImage = false;
                    }else{
                        $scope.showDEventImage = true;
                    }
                    
                    $scope.newDEventVideourlText = $scope.eventContent.event_video_detail_url;
                    if($scope.newDEventVideourlText === '' || $scope.newDEventVideourlText === undefined){
                        $scope.showDEventVideo = false;
                    }else{
                        $scope.showDEventVideo = true;
                    }
                    
                    $scope.newDEventurlText = $scope.eventContent.event_more_detail_url;
                    if($scope.newDEventurlText === '' || $scope.newDEventurlText === undefined){
                        $scope.showDEventurl = false;
                    }else{
                        $scope.showDEventurl = true;
                    }
                    
                    $scope.newDEventDateText = $scope.eventContent.event_begin_dt.split(' ')[0];
                    $scope.newDEventTimeTextt = $scope.eventContent.event_begin_dt.split(' ')[1];
                    
                    //Edit event start date time
                    if($scope.eventContent.event_begin_dt === undefined || $scope.eventContent.event_begin_dt === '0000-00-00 00:00:00' ){
                            $scope.showDEventTime=false;
                            $scope.showDDEventDateOnly = false;
                            $scope.newDEventDateText = "";
                            $scope.newDEventTimeTextt="";
                        
                    }else if($scope.eventContent.event_begin_dt !== undefined || $scope.eventContent.event_begin_dt !== '0000-00-00 00:00:00' ){
                        if(($scope.newDEventDateText !== undefined && $scope.newDEventTimeTextt === '00:00:00')|| ($scope.newDEventDateText !== '0000-00-00'&& $scope.newDEventTimeTextt === '00:00:00')){
                            $scope.showDEventTime=false;
                            $scope.showDDEventDateOnly = true;
                            $scope.newDEventDateText = $scope.eventContent.event_begin_dt.split(' ')[0];
                            $scope.newDEventTimeTextt="";
                        }else if(($scope.newDEventDateText !== '0000-00-00' && $scope.newDEventTimeTextt !== undefined )|| ($scope.newDEventDateText !== '0000-00-00' && $scope.newDEventTimeTextt !== '00:00:00')){
                            $scope.showDEventTime=true;
                            $scope.showDDEventDateOnly = false;
                            $scope.newDEventDateText = $scope.eventContent.event_begin_dt.split(' ')[0];
                            $scope.newDEventTimeTextt = $scope.eventContent.event_begin_dt;
                        }
                    }
                    
                    $scope.newDEventEndDateText = $scope.eventContent.event_end_dt.split(' ')[0];
                    $scope.newDEventEndTimeTextt = $scope.eventContent.event_end_dt.split(' ')[1];
                    
                    //Edit event end date time
                    if($scope.eventContent.event_end_dt === undefined || $scope.eventContent.event_end_dt === '0000-00-00 00:00:00' ){
                            $scope.showDEventEndTime=false;
                            $scope.showDDEventEndDateOnly = false;
                            $scope.newDEventEndDateText = "";
                            $scope.newDEventEndTimeTextt="";
                        
                    }else if($scope.eventContent.event_end_dt !== undefined || $scope.eventContent.event_end_dt !== '0000-00-00 00:00:00' ){
                        if(($scope.newDEventEndDateText !== undefined && $scope.newDEventEndTimeTextt === '00:00:00')|| ($scope.newDEventEndDateText !== '0000-00-00'&& $scope.newDEventEndTimeTextt === '00:00:00')){
                            $scope.showDEventEndTime=false;
                            $scope.showDDEventEndDateOnly = true;
                            $scope.newDEventEndDateText = $scope.eventContent.event_end_dt.split(' ')[0];
                            $scope.newDEventEndTimeTextt="";
                        }else if(($scope.newDEventEndDateText !== '0000-00-00' && $scope.newDEventTimeTextt !== undefined )|| ($scope.newDEventEndDateText !== '0000-00-00' && $scope.newDEventTimeTextt !== '00:00:00')){
                            $scope.showDEventEndTime=true;
                            $scope.showDDEventEndDateOnly = false;
                            $scope.newDEventEndDateText = $scope.eventContent.event_end_dt.split(' ')[0];
                            $scope.newDEventEndTimeTextt = $scope.eventContent.event_end_dt;
                        }
                    }                      
                    
                    $scope.newDEventCostText = $scope.eventContent.event_cost;
                    if($scope.newDEventCostText === '0.00' || $scope.newDEventCostText === undefined){
                        $scope.showDEventCost = false;
                    }else{
                        $scope.showDEventCost = true;
                    }
                    $scope.newDEventCompareCostText = $scope.eventContent.event_compare_price;
                    if($scope.newDEventCompareCostText === '0.00' || $scope.newDEventCompareCostText === undefined || $scope.newDEventCompareCostText === null){
                        $scope.showDEventCompareCost = false;
                    }else{
                        $scope.showDEventCompareCost = true;
                    }
                    
                    $scope.newDEventdescText = $scope.eventContent.event_desc;
                    if($scope.newDEventdescText === '' || $scope.newDEventdescText === undefined){
                        $scope.showDEventdesc = false;
                    }else{
                        $scope.showDEventdesc = true;
                    }
                    
                    $scope.newDEventRegistered = $scope.eventContent.registrations_count;
                    $scope.newDEventCapacity = $scope.eventContent.event_capacity;
                    if($scope.newDEventCapacity === '' || $scope.newDEventCapacity === undefined || $scope.newDEventCapacity === 0){
                        $scope.showDEventSpaceRemaining = false;
                    }else{
                        $scope.showDEventSpaceRemaining = true;
                    }
                    
                    $scope.newDEventProcesFee = $scope.eventContent.processing_fees;
                    if($scope.newDEventProcesFee === '2') {
                            $scope.EditEventCost = $scope.newDEventCostText;
                            $scope.EditEventProcessCost = $scope.getProcessingCost($scope.newDEventCostText);
                            $scope.EditEventTotalProcessCost = $scope.TotalEventProcessingCost($scope.newDEventCostText);
                    }
                    $scope.newDEventSpaceRemaining = $scope.eventContent.capacity_text;
                    if($scope.newDEventSpaceRemaining === '' || $scope.newDEventSpaceRemaining === undefined){
                        $scope.showDEventSpaceRemaining = false;
                    }else{
                        $scope.showDEventSpaceRemaining = true;
                    }
                    
                    $scope.reg_col_names = $scope.eventContent.reg_columns; 
                    if($scope.eventContent.waiver_policies === null || $scope.eventContent.waiver_policies === undefined || $scope.eventContent.waiver_policies === '' ){ 
                        $scope.newEventWaiverText = '';
                        $scope.showEventWaiver = false;                         
                    } else{
                        $scope.newEventWaiverText = $scope.eventContent.waiver_policies;
                        $scope.showEventWaiver = true;
                    }
                    
                }else if($scope.eventContent.event_type === 'M'){   
                    $scope.preview_event_type = 'S';
                    $scope.showEMEvent = true;
                    $scope.showEEvent = false;
                    
                    $scope.newDMEventImage = $scope.eventContent.event_banner_img_url;
                    if($scope.newDMEventImage === '' || $scope.newDMEventImage === undefined){
                        $scope.showDMEventImage = false;
                    }else{
                        $scope.showDMEventImage = true;
                    }
                    $scope.newDMEventCategoryText = $scope.eventContent.event_title;
                    if($scope.newDMEventCategoryText === '' || $scope.newDMEventCategoryText === undefined){
                        $scope.showDMEventCategory = false;
                    }else{
                        $scope.showDMEventCategory = true;
                    }
                    $scope.newDMEventSubCategoryText = $scope.eventContent.event_category_subtitle;
                    if($scope.newDMEventSubCategoryText === '' || $scope.newDMEventSubCategoryText === undefined){
                        $scope.showDMEventSubCategory = false;
                    }else{
                        $scope.showDMEventSubCategory = true;
                    }
                    
                    $scope.newDPEventVideourlText = $scope.eventContent.event_video_detail_url;
                    if($scope.newDPEventVideourlText === '' || $scope.newDPEventVideourlText === undefined){
                        $scope.showDPEventVideo = false;
                    }else{
                        $scope.showDPEventVideo = true;
                    }
                    
                    $scope.newDPEventdescText = $scope.eventContent.event_desc;
                    if($scope.newDPEventdescText === '' || $scope.newDPEventdescText === undefined){
                        $scope.showDPEventdesc = false;
                    }else{
                        $scope.showDPEventdesc = true;
                    }
                    
                    if( $scope.eventContent.event_type==='M' && $scope.eventContent.child_events.length > 0){                        
                        $scope.eventChildContent = $scope.eventContent.child_events;
                    }else{
                        $scope.eventChildContent = "";
                    }
                    $scope.reg_col_names = $scope.eventContent.reg_columns; 
                    if($scope.eventContent.waiver_policies === null || $scope.eventContent.waiver_policies === undefined || $scope.eventContent.waiver_policies === '' ){ 
                        $scope.newEventWaiverText = '';
                        $scope.showEventWaiver = false;                         
                    } else{
                        $scope.newEventWaiverText = $scope.eventContent.waiver_policies;
                        $scope.showEventWaiver = true;
                    }
                }
                });                                
            }
            
           
            //Membership preview details
           if ($localStorage.currentpage === "membership") {
                $scope.companydetails();
                f7.showTab('#memberships-view');
                $scope.showMembershipAddEdit = true;
                $scope.preloaderVisible = false;
            }else if($localStorage.currentpage ==="templatemembership")
            {   
                f7.showTab('#memberships-view');
                $scope.showMembershipAddEdit = true;
                $scope.preloaderVisible = false;
                $scope.membershipContentStatus = 'Success';                
                $scope.membershipListContent = $localStorage.templateMembership;
                $scope.$apply();
                
            }else if($localStorage.currentpage === "editmembership" || $localStorage.currentpage === "addmembership"){
                    f7.showTab('#memberships-view');
                    $scope.showMembershipAddEdit = false;
                    $scope.preloaderVisible = false;
//                $scope.$watch(function () {return $localStorage.preview_membershipcontent;}, function (newVal, oldVal) {
                    $scope.membershipContent = $localStorage.preview_membershipcontent;
                    $scope.newMembershipCategoryText = $scope.membershipContent.category_title;
                    if($scope.newMembershipCategoryText === '' || $scope.newMembershipCategoryText === undefined){
                        $scope.showMemCategoryname = false;
                    }else{
                        $scope.showMemCategoryname = true;
                    }
                    
                    $scope.$apply();
                    $scope.newMembershipSubCategoryText = $scope.membershipContent.category_subtitle;
                    if($scope.newMembershipSubCategoryText === '' || $scope.newMembershipSubCategoryText === undefined){
                        $scope.showMemSubCategoryname = false;
                    }else{
                        $scope.showMemSubCategoryname = true;
                    }
                    
                    $scope.newMembershipCategoryImage = $scope.membershipContent.category_image_url;
                    if($scope.newMembershipCategoryImage === '' || $scope.newMembershipCategoryImage === undefined){
                        $scope.showMemCategoryImage = false;
                    }else{
                        $scope.showMemCategoryImage = true;
                    }
                    
                    $scope.newMembershipCategoryUrl = $scope.membershipContent.category_video_url;
                    if($scope.newMembershipCategoryUrl === '' || $scope.newMembershipCategoryUrl === undefined){
                        $scope.showMemCategoryUrl = false;
                    }else{
                        $scope.showMemCategoryUrl = true;
                    }
                    
                    $scope.newMembershipCategorydesc = $scope.membershipContent.category_description;
                    if($scope.newMembershipCategorydesc === '' || $scope.newMembershipCategorydesc === undefined){
                        $scope.showMemCategoryDesc = false;
                    }else{
                        $scope.showMemCategoryDesc = true;
                    }
                    
                    if($scope.membershipContent.membership_options.length > 0){
                        $scope.membershipOptionContent = $scope.membershipContent.membership_options;
                        $scope.membershipOptionContentStatus = true;
                    }else{
                        $scope.membershipOptionContent = "";
                        $scope.membershipOptionContentStatus = false;
                    }  
                   
            }
            
            
             //Trial preview details
           if ($localStorage.currentpage === "trial") {
                $scope.companydetails();
                f7.showTab('#trial-view');
                $scope.showTrialAddEdit = true;
                $scope.preloaderVisible = false;
            }else if($localStorage.currentpage ==="templatetrial"){   
                f7.showTab('#trial-view');
                $scope.showTrialAddEdit = true;
                $scope.preloaderVisible = false;
                $scope.trialListContent = $localStorage.trialTemplate;
                $scope.trialContentStatus = 'Success';  
                $scope.$apply();
                
            }else if($localStorage.currentpage === "edittrial" || $localStorage.currentpage === "addtrial"){
                
                    f7.showTab('#trial-view');
                    $scope.showTrialAddEdit = false;
                    $scope.preloaderVisible = false;
                    $scope.trialContent = $localStorage.preview_trialcontent;
                    $scope.newTrialText = $scope.trialContent.trial_title;
                    if($scope.newTrialText === '' || $scope.newTrialText === undefined){
                        $scope.showTrialname = false;
                    }else{
                        $scope.showTrialname = true;
                    }
                    
                    $scope.$apply();
                    $scope.newTrialSubText = $scope.trialContent.trial_subtitle;
                    if($scope.newTrialSubText === '' || $scope.newTrialSubText === undefined){
                        $scope.showTrialSubname = false;
                    }else{
                        $scope.showTrialSubname = true;
                    }
                    
                    $scope.newTrialImage = $scope.trialContent.trial_banner_img_url;
                    if($scope.newTrialImage === '' || $scope.newTrialImage === undefined){
                        $scope.showTrialImage = false;
                    }else{
                        $scope.showTrialImage = true;
                    }
                    
                    $scope.newTrialPrgmUrl = $scope.trialContent.trial_prog_url;
                    if($scope.newTrialPrgmUrl === '' || $scope.newTrialPrgmUrl === undefined){
                        $scope.showTrialPrgmUrl = false;
                    }else{
                        $scope.showTrialPrgmUrl = true;
                    }
                    
                    $scope.newTrialdesc = $scope.trialContent.trial_desc;
                    if($scope.newTrialdesc === '' || $scope.newTrialdesc === undefined){
                        $scope.showTrialdesc = false;
                    }else{
                        $scope.showTrialdesc = true;
                    }
                    
                    $scope.newTrialPrice = $scope.trialContent.price_amount;
                    if(!$scope.newTrialPrice){
                        $scope.showTrialPrice = false;
                        $scope.newTrialPrice = 0;
                    }else{
                        $scope.showTrialPrice = true;
                    }
                    
                    $scope.newTrialProgramPeriod = $scope.trialContent.program_length_type;
                    if($scope.newTrialProgramPeriod === '' || $scope.newTrialProgramPeriod === undefined){
                        $scope.showTrialProgramPeriod = false;
                    }else{
                        $scope.showTrialProgramPeriod = true;
                    }
                    
                    $scope.newTrialProgramQuantity = $scope.trialContent.program_length;
                    if($scope.newTrialProgramQuantity === '' || $scope.newTrialProgramQuantity === undefined){
                        $scope.showTrialProgramQuantity = false;
                    }else{
                        $scope.showTrialProgramQuantity = true;
                    }
                    
                    $scope.newTrialProcessingFee = $scope.trialContent.processing_fee_type;
                    if($scope.newTrialProcessingFee === '' || $scope.newTrialProcessingFee === undefined){
                        $scope.showTrialProcessingFee = false;
                    }else{
                        $scope.showTrialProcessingFee = true;
                    }
                    
                    $scope.newTrialWaiverText = $scope.trialContent.trial_waiver_policies;
                    if($scope.newTrialWaiverText === '' || $scope.newTrialWaiverText === undefined){
                        $scope.showTrialWaiver = false;
                    }else{
                        $scope.showTrialWaiver = true;
                    }
                    
                    $scope.edit_trial_reg_col_names = $scope.trialContent.reg_columns;
                    $scope.edit_trial_lead_columns = $scope.trialContent.lead_columns;
                    
                    $scope.editTrialshowTotal();
            }
            
//            Retail preview details
            if ($localStorage.currentpage === "retail") {
                $scope.companydetails();
                f7.showTab('#Retail-view');
                $scope.preloaderVisible = false;
                $scope.retailtemplateview = false;
            }else if($localStorage.currentpage ==='templateretail'){  
                f7.showTab('#Retail-view');
                $scope.retailContentStatus = 'Success';  
                $scope.retailtemplateview = true;
                $scope.preloaderVisible = false;
                $scope.retailListContent = $localStorage.retailTemplate;
                $scope.$apply();
            }else if($localStorage.currentpage === "editretail" || $localStorage.currentpage === "addretail"){
                $scope.retailtemplateview = false;
                f7.showTab('#Retail-view');
                $scope.preloaderVisible = false;
                $scope.retailContent = $localStorage.preview_retailcontent;
                if($scope.retailContent.retail_variant_flag === 'Y'){
                    $scope.getRetailVariant($scope.retailContent.retail_product_id);
                }
                if($scope.retailContent.retail_variant_flag === 'N'){
                    $scope.edit_retail_vaiants_array = '';
                    $scope.newRetailvariantprice = '';
                }
                $scope.newRetailCatId = $scope.retailContent.retail_product_id;
                if($scope.retailContent.retail_product_type === 'C'){
                   $scope.newRetailCatText = $scope.retailContent.retail_product_title;
                    if($scope.newRetailCatText === '' || $scope.newRetailCatText === undefined){
                        $scope.showRetailCatname = false;
                    }else{
                        $scope.showRetailCatname = true;
                    } 
                }else{
                   $scope.newRetailText = $scope.retailContent.retail_product_title; 
                    if($scope.newRetailText === '' || $scope.newRetailText === undefined){
                        $scope.showRetailname = false;
                    }else{
                        $scope.showRetailname = true;
                    }
                }
                $scope.newRetaildesc = $scope.retailContent.retail_product_desc;
                $scope.newRetailCatImage = $scope.newRetailImage = $scope.retailContent.retail_banner_img_url;
                $scope.newRetailvariantflag = $scope.retailContent.retail_variant_flag;
                if($scope.newRetailvariantflag === '' || $scope.newRetailvariantflag === undefined){
                        $scope.newRetailvariantflag = 'Y';
                }
                $scope.newRetailtax = $scope.retailContent.product_tax_rate;
                $scope.newRetailprice = $scope.retailContent.retail_product_price;
                $scope.newRetailvariantprice = $scope.retailContent.retail_variant_price;
                if($scope.newRetailprice === '' || $scope.newRetailprice === undefined){
                    $scope.showRetailprice = false;
                }else{
                    $scope.showRetailprice = true;
                }
                $scope.newRetailcompareprice = $scope.retailContent.retail_product_compare_price;
                if($scope.newRetailcompareprice === '' || $scope.newRetailcompareprice === undefined){
                    if($scope.newRetailcompareprice <= 0){
                        $scope.showRetailcompareprice = false;
                    }else{
                        $scope.showRetailcompareprice = true;
                    }
                }else{
                    $scope.showRetailcompareprice = true;
                }
                $scope.newRetailCatSubtext = $scope.retailContent.retail_product_subtitle;
                $scope.$apply();
            }
//            End Retail preview details
            
            if ($localStorage.currentpage === "message") {
                $scope.companydetails();
                $scope.msgcontent = $localStorage.msgcontent;
                $scope.getmessageList();
                f7.showTab('#view-2');
            }
            if ($localStorage.currentpage === "curriculum") {
                $scope.companydetails();
                $scope.getCurriculum('0');
                f7.showTab('#view-3');
            }
            if ($localStorage.currentpage === "referrals") {
                $scope.companydetails();
                f7.showTab('#view-4');
            }
            if ($localStorage.currentpage === "social") {
                $scope.companydetails();
                f7.openPanel('left');
            }            
            
        });
        //PreviewMessageText                                
        $scope.$watch(function () {return $localStorage.PreviewMessageDText;}, function (newVal, oldVal) {

            $scope.newDMsgText = newVal;
            if ($localStorage.PreviewMessageDText) {
                $scope.showDMsgView = true;
            } else {
                $scope.showDMsgView = false;
            }
        });
        //PreviewMURLDText
        $scope.$watch(function () {return $localStorage.PreviewMURLDText;}, function (newValue, oldValue) {
            $scope.newDMsgUrlText = newValue;
            if ($scope.newDMsgUrlText && $scope.newDMsgUrlText.length >= 1) {
                $scope.showDMsgUrlView = true;
            } else {
                $scope.showDMsgUrlView = false;
            }
        });

        //Event Watch Variables to look for Changes
        //EVENT TYPE - SINGLE/MULTIPLE
        $scope.$watch(function () {return $localStorage.PreviewEventType;}, function (newVal, oldVal) {

            $scope.newDEventType = newVal;
            $scope.newDEventNameText = "";
            $scope.newDEventDateText = "";
            $scope.newDEventTimeText = "";
            $scope.newDEventCostText = "";            
            $scope.newDEventCompareCostText = "";
            $scope.showDEventName = false;
            $scope.showDEventDate = false;
            $scope.showDEventTime = false;
            $scope.showDDEventDateOnly = false;
            $scope.showDDEventEndDateOnly = false;
            $scope.showDEventCost = false;
            $scope.showDEventCompareCost = false;
            $scope.newDEventurlText = "";
            $scope.showDEventurl = false;
            $scope.newDEventVideourlText = "";
            $scope.showDEventVideo = false;
            $scope.newDEventImage = "";
            $scope.showDEventImage = false;
            $scope.showDEventCapacity = false;
            $scope.showDEventSpaceRemaining = false;
            
            //For Multiple Events
            $scope.newDMEventCategoryText = "";
            $scope.newDMEventSubCategoryText = "";
            $scope.newDMEventNameText = "";
            $scope.newDMEventDateText = "";
            $scope.newDMEventTimeText = "";
            $scope.newDMEventCostText = "";            
            $scope.newDMEventCompareCostText = "";
            $scope.showDMEventName = false;  
            $scope.showDMEventCategory = false;
            $scope.newDMEventImage = "";
            $scope.showDMEventImage = false;
            $scope.showDMEventDate = false;
            $scope.showDMEventTime = false;
            $scope.showDDMEventDateOnly = false;
            $scope.showDMEventCost = false;
            $scope.showDMEventCompareCost = false;
            $scope.showDMEventurl = false;
            $scope.newDMEventurlText = "";            
            
            $scope.newDPEventVideourlText = '';
            $scope.newDPEventdescText = '';
            $scope.newDPEventImage = '';
            $scope.newDEditPEventImage = '';
            $scope.showDPEventVideo = false;
            $scope.showDPEventdesc = false;
            $scope.showDPEventImage = false;
            $scope.showDEditPEventImage = false;
            $scope.newMEventCapacity = '';
            $scope.newMEventSpaceRemaining = '';
            $scope.showMEventCapacity = false;
            $scope.showMEventSpaceRemaining = false;
            
            //For Child Events
            $scope.eventChildContent = '';
            $scope.showDDCEventDateOnly = false;
            $scope.showDCEventName = false;
            $scope.showDCEventTime = false;
            $scope.showDCEventCost = false;
            $scope.showDCEventCompareCost = false;
            $scope.showCMEventdesc = false;
            $scope.eventlistshow = false;
            $scope.showDCEventDate = false;
            $scope.showDDCEventEndDate = false;
            $scope.newDCEventVideourlText = '';
            $scope.showDCEventVideo = false;
        }); 
        
        //EVENT TOP TAB BAR MENU SELECTION CHANGE
        
        //TAB TOGGLE ON VALUE CHANGE
       $scope.indexeventtabPreview = function(){
           var eventView = f7.addView('.event-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = eventView.activePage.name;
                    if(currentipage !== 'index'){
                    eventView.router.loadPage({
                       pageName: 'index'
                    });
                    
                }
       };
       
       $scope.childeventtabPreview = function(){
           var eventView = f7.addView('.event-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = eventView.activePage.name;
                    if(currentipage !== 'childevents'){
                    eventView.router.loadPage({
                       pageName: 'childevents'
                    });
                }
       };
       
       $scope.editchildeventtabPreview = function(){
           var eventView = f7.addView('.event-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = eventView.activePage.name;
                    if(currentipage !== 'editchildevents'){
                    eventView.router.loadPage({
                       pageName: 'editchildevents'
                    });
                }
       };
       
       $scope.registrationeventtabPreview = function(){
           var eventView = f7.addView('.event-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = eventView.activePage.name;
                    if(currentipage !== 'registration'){
                    eventView.router.loadPage({
                       pageName: 'registration'
                    });
                }
       };
       
       $scope.editregistrationeventtabPreview = function(){
           var eventView = f7.addView('.event-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = eventView.activePage.name;
                    if(currentipage !== 'editregistration'){
                    eventView.router.loadPage({
                       pageName: 'editregistration'
                    });
                }
       };
       
       $scope.descriptioneventtabPreview = function(){
           var eventView = f7.addView('.event-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = eventView.activePage.name;
                    if(currentipage !== 'description' && ($localStorage.currentpreviewtab !== 'events' || $localStorage.currentpreviewtab !== 'payment')){
                    eventView.router.loadPage({
                       pageName: 'description'
                    });
                }
       };
       
        $scope.editdescriptioneventtabPreview = function(){
           var eventView = f7.addView('.event-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = eventView.activePage.name;
                    if(currentipage !== 'editdescription' && ($localStorage.currentpreviewtab !== 'events' || $localStorage.currentpreviewtab !== 'payment')){
                    eventView.router.loadPage({
                       pageName: 'editdescription'
                    });
                }
       };
       
       $scope.childdescriptioneventtabPreview = function(){
           var eventView = f7.addView('.event-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = eventView.activePage.name;
                    if(currentipage !== 'childdescription'){
                    eventView.router.loadPage({
                       pageName: 'childdescription'
                    });
                }
       };
       
        $scope.editchilddescriptioneventtabPreview = function(){
           var eventView = f7.addView('.event-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = eventView.activePage.name;
                    if(currentipage !== 'editchilddescription'){
                    eventView.router.loadPage({
                       pageName: 'editchilddescription'
                    });
                }
       };
       
       $scope.editparentdescriptioneventtabPreview = function(){
           var eventView = f7.addView('.event-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = eventView.activePage.name;
                    if(currentipage !== 'editparentdescription'){
                    eventView.router.loadPage({
                       pageName: 'editparentdescription'
                    });
                }
       };
       
       //COMPANY WEPAY STATUS
        $scope.$watch(function () {return $localStorage.preview_wepaystatus;}, function (newVal, oldVal) {                
                    $scope.wepay_status = newVal;
        });
       
        
        //SINGLE EVENT PREVIEW VALUES
        //SINGLE EVENT NAME
        $scope.$watch(function () {return $localStorage.PreviewEventName;}, function (newVal, oldVal) {
            
          $scope.newDEventNameText = newVal;
          if($localStorage.PreviewEventName) {
                    $scope.indexeventtabPreview();
                    $scope.showDEventName = true;
            }else{ 
                    $scope.showDEventName = false;
            }
        });
        //SINGLE EVENT DATE
        $scope.$watch(function () {return $localStorage.PreviewEventDate;}, function (newVal, oldVal) {
            
            if($localStorage.PreviewEventDate === '' || $localStorage.PreviewEventTime === undefined ){
                $scope.showDEventTime=false;
                $scope.showDDEventDateOnly = false;
                $scope.newDEventTimeTextt='';
                $scope.newDEventDateText = '';
            }
            
            if ($scope.newDEventTimeTextt) {
                $scope.showDEventDate = true;
                $scope.newDEventDateText = newVal;
                if($scope.newDEventTimeText){
                    var time = $scope.newDEventTimeText;
                var hours = Number(time.match(/^(\d+)/)[1]);
                var minutes = Number(time.match(/:(\d+)/)[1]);
                var AMPM = time.match(/\s(.*)$/)[1];
                if (AMPM == "PM" && hours < 12)
                    hours = hours + 12;
                if (AMPM == "AM" && hours == 12)
                    hours = hours - 12;
                var sHours = hours.toString();
                var sMinutes = minutes.toString();
                if (hours < 10)
                    sHours = "0" + sHours;
                if (minutes < 10)
                    sMinutes = "0" + sMinutes;
                $scope.newDETime = sHours + ":" + sMinutes + ":00";
                $scope.newDEventTimeTextt = newVal + " " + $scope.newDETime;
                }else{
                 $scope.showDEventTime = false;
                 $scope.newDEventTimeTextt = newVal + " " + $scope.newDEventTimeText; 
                 $scope.showDDEventDateOnly = true;
                 
                }
                
            } else {
                $scope.showDDEventDateOnly = true;
                $scope.newDEventDateText = newVal;
            }
        });
        
        //SINGLE EVENT TIME
        $scope.$watch(function () {return $localStorage.PreviewEventTime;}, function (newVal, oldVal) {
            if($localStorage.PreviewEventTime == undefined || $localStorage.PreviewEventTime == ""){
                $scope.showDEventTime=false;
                $scope.showDDEventDateOnly = true;
                $scope.newDEventTimeText="";
            }
            else{
                    $scope.newDEventTimeText = newVal;
                    var time = $scope.newDEventTimeText;
                    var hours = Number(time.match(/^(\d+)/)[1]);
                    var minutes = Number(time.match(/:(\d+)/)[1]);
                    var AMPM = time.match(/\s(.*)$/)[1];
                    if (AMPM == "PM" && hours < 12)
                        hours = hours + 12;
                    if (AMPM == "AM" && hours == 12)
                        hours = hours - 12;
                    var sHours = hours.toString();
                    var sMinutes = minutes.toString();
                    if (hours < 10)
                        sHours = "0" + sHours;
                    if (minutes < 10)
                        sMinutes = "0" + sMinutes;
                    $scope.newDEventTimeTextFrmt = sHours + ":" + sMinutes + ":00";
                    $scope.newDEventTimeTextt = $scope.newDEventDateText + " " + $scope.newDEventTimeTextFrmt;
           
                if ($scope.newDEventTimeText) {
                    $scope.showDEventTime = true;
                    $scope.showDDEventDateOnly = "";
                } else {
                    $scope.showDEventTime = false;
                }
            }
            
        });
        
        
        //SINGLE End EVENT DATE
        $scope.$watch(function () {return $localStorage.PreviewEventEndDate;}, function (newVal, oldVal) {
            
            if($localStorage.PreviewEventEndDate === '' || $localStorage.PreviewEventEndTime === undefined ){
                $scope.showDEventEndTime=false;
                $scope.showDDEventEndDateOnly = false;
                $scope.newDEventEndTimeText="";
                $scope.newDEventEndTimeTextt = '';
            }
            
            if ($scope.newDEventEndTimeTextt) {
                $scope.showDEventEndDate = true;
                $scope.newDEventEndDateText = newVal;
                if($scope.newDEventEndTimeText){
                    var time = $scope.newDEventEndTimeText;
                var hours = Number(time.match(/^(\d+)/)[1]);
                var minutes = Number(time.match(/:(\d+)/)[1]);
                var AMPM = time.match(/\s(.*)$/)[1];
                if (AMPM == "PM" && hours < 12)
                    hours = hours + 12;
                if (AMPM == "AM" && hours == 12)
                    hours = hours - 12;
                var sHours = hours.toString();
                var sMinutes = minutes.toString();
                if (hours < 10)
                    sHours = "0" + sHours;
                if (minutes < 10)
                    sMinutes = "0" + sMinutes;
                $scope.newDEEndTime = sHours + ":" + sMinutes + ":00";
                $scope.newDEventEndTimeTextt = newVal + " " + $scope.newDEEndTime;
                }else{
                 $scope.showDEventEndTime = false;
                 $scope.newDEventEndTimeTextt = newVal + " " + $scope.newDEventEndTimeText; 
                 $scope.showDDEventEndDateOnly = true;
                 
                }
                
            } else {
                $scope.showDDEventEndDateOnly = true;
                $scope.newDEventEndDateText = newVal;
            }
            
        });
        
        //SINGLE EVENT End TIME
        $scope.$watch(function () {return $localStorage.PreviewEventEndTime;}, function (newVal, oldVal) {
            if($localStorage.PreviewEventEndTime == undefined || $localStorage.PreviewEventEndTime == ""){
                $scope.showDEventEndTime=false;
                $scope.showDDEventEndDateOnly = true;
                $scope.newDEventEndTimeText="";
            }
            else{
                    $scope.newDEventEndTimeText = newVal;
                    var time = $scope.newDEventEndTimeText;
                    var hours = Number(time.match(/^(\d+)/)[1]);
                    var minutes = Number(time.match(/:(\d+)/)[1]);
                    var AMPM = time.match(/\s(.*)$/)[1];
                    if (AMPM == "PM" && hours < 12)
                        hours = hours + 12;
                    if (AMPM == "AM" && hours == 12)
                        hours = hours - 12;
                    var sHours = hours.toString();
                    var sMinutes = minutes.toString();
                    if (hours < 10)
                        sHours = "0" + sHours;
                    if (minutes < 10)
                        sMinutes = "0" + sMinutes;
                    $scope.newDEventEndTimeTextFrmt = sHours + ":" + sMinutes + ":00";
                    $scope.newDEventEndTimeTextt = $scope.newDEventEndDateText + " " + $scope.newDEventEndTimeTextFrmt;
           
                if ($scope.newDEventEndTimeText) {
                    $scope.showDEventEndTime = true;
                    $scope.showDDEventEndDateOnly = "";
                } else {
                    $scope.showDEventEndTime = false;
                }
            }
            
        });
        
        //SINGLE EVENT COST
        $scope.$watch(function () {return $localStorage.PreviewEventCost;}, function (newVal, oldVal) {
                    
            $scope.newDEventCostText = newVal;
            if($scope.newDEventProcesFee === '2'){
                   $scope.EditEventCost = $scope.newDEventCostText;
                   $scope.EditEventProcessCost = $scope.getProcessingCost($scope.newDEventCostText);
                   $scope.EditEventTotalProcessCost = $scope.TotalEventProcessingCost($scope.newDEventCostText);
                }
            if($localStorage.PreviewEventCost) {
                    $scope.showDEventCost = true;
            }else{  
                    $scope.showDEventCost = false;
            }
            
        });
        
        //SINGLE EVENT COMPARE COST
        $scope.$watch(function () {return $localStorage.PreviewEventCompareCost;}, function (newVal, oldVal) {
            $scope.newDEventCompareCostText = newVal;
            if( $localStorage.PreviewEventCompareCost) {
                    $scope.showDEventCompareCost = true;
            }else{  
                    $scope.showDEventCompareCost = false;
            }
        });
        //SINGLE EVENT URL
        $scope.$watch(function () {return $localStorage.PreviewEventUrl;}, function (newVal, oldVal) {
            $scope.newDEventVideourlText = newVal;
            if( $localStorage.PreviewEventUrl) {
                    $scope.editdescriptioneventtabPreview();
                    $scope.showDEventVideo = true;
            }else{  
                    $scope.showDEventVideo = false;
            }
        });
        
        //SINGLE EVENT DESCRIPTION
        $scope.$watch(function () {return $localStorage.PreviewEventDesc;}, function (newVal, oldVal) {
            $scope.newDEventdescText = newVal;
            if( $localStorage.PreviewEventDesc) {
                    $scope.editdescriptioneventtabPreview();
                    $scope.showDEventdesc = true;
                    var elem = document.getElementById('editdescscrolldiv');
                    elem.scrollTop = elem.scrollHeight;
            }else{  
                    $scope.showDEventdesc = false;
            }
            
        });
        
        //SINGLE EVENT IMAGE UPLOAD
        $scope.$watch(function () {return $localStorage.PreviewImageUrl;}, function (newVal, oldVal) { 
                    $scope.newDEventImage = newVal;
            if( $localStorage.PreviewImageUrl){                
                $scope.showDEventImage = true;
            }else{  
                $scope.showDEventImage = false;
            }
//            if ($localStorage.PreviewImageUrl.length >= 1  && $scope.newDEventImage != newVal) { 
//                    $scope.newDEventImage = newVal;                    
//            } //recentfix
        });
        
        //SINGLE EVENT CAPACITY
        $scope.$watch(function () {return $localStorage.PreviewECapacity;}, function (newVal, oldVal) { 
                    $scope.newDEventCapacity = newVal;
            if($localStorage.PreviewECapacity && $localStorage.PreviewECapacity.length >= 1) {
                    $scope.showDEventCapacity = true;
            }else{  
                    $scope.showDEventCapacity = false;
            }
        });
        
        //SINGLE EVENT SPACE REMAINING
        $scope.$watch(function () {return $localStorage.PreviewESpaceRemaining;}, function (newVal, oldVal) { 
            $scope.newDEventSpaceRemaining = newVal;
            if( $localStorage.PreviewESpaceRemaining) {                    
                    $scope.showDEventSpaceRemaining = true;
                    $scope.indexeventtabPreview();
            }else{  
                    $scope.showDEventSpaceRemaining = false;
            }
        });
        
         //SINGLE EVENT PROCESSING FEE SELECTION
        $scope.$watch(function () {return $localStorage.PreviewEProcesFeeSelection;}, function (newVal, oldVal) { 
            $scope.newDEventProcesFee = newVal;            
            if( $localStorage.PreviewEProcesFeeSelection) {
                $scope.editregistrationeventtabPreview();
                $scope.showDEventProcesFee = true;
                if($scope.newDEventProcesFee === '2'){
                   $scope.EditEventCost = $scope.newDEventCostText;
                   $scope.EditEventProcessCost = $scope.getProcessingCost($scope.newDEventCostText);
                   $scope.EditEventTotalProcessCost = $scope.TotalEventProcessingCost($scope.newDEventCostText);
                }
            }else{  
                    $scope.showDEventProcesFee = false;
            }
        });
        
        
        //FOR MULTIPLE EVENT PREVIEW VALUES
        //MULTIPLE EVENT CATEGORY
        $scope.$watch(function () {return $localStorage.PreviewMEventCategory;}, function (newVal, oldVal) {
            
            $scope.newDMEventCategoryText = newVal;
            if( $localStorage.PreviewMEventCategory) {
                    $scope.indexeventtabPreview();
                    $scope.showDMEventCategory = true;
            }else{  
                    $scope.showDMEventCategory = false;
            }
        });
        
        //MULTIPLE EVENT SUB CATEGORY
        $scope.$watch(function () {return $localStorage.PreviewMEventSubcategory;}, function (newVal, oldVal) {
            
            $scope.newDMEventSubCategoryText = newVal;
            if( $localStorage.PreviewMEventSubcategory) {
                    $scope.indexeventtabPreview();
                    $scope.showDMEventSubCategory = true;
            }else{  
                    $scope.showDMEventSubCategory = false;
            }
        });
        
        
        //MULTIPLE EVENT IMAGE UPLOAD
        $scope.$watch(function () {return $localStorage.PreviewImageUrl;}, function (newVal, oldVal) {    
            $scope.newDMEventImage = newVal;
            if( $localStorage.PreviewImageUrl) {
                    $scope.indexeventtabPreview();
                    $scope.showDMEventImage = true;
            }else{  
                    $scope.showDMEventImage = false;
            }
        });
        
         //MULTIPLE EVENT URL
        $scope.$watch(function () {return $localStorage.PreviewPEventUrl;}, function (newVal, oldVal) {
            $scope.newDPEventVideourlText = newVal;
            if( $localStorage.PreviewPEventUrl) {
                    $scope.editparentdescriptioneventtabPreview();
                    $scope.showDPEventVideo = true;
            }else{  
                    $scope.showDPEventVideo = false;
            }
        });
        
         //MULTIPLE EVENT DESCRIPTION
        $scope.$watch(function () {return $localStorage.PreviewPEventDesc;}, function (newVal, oldVal) {
            $scope.newDPEventdescText = newVal;
            if( $localStorage.PreviewPEventDesc) {
                    $scope.editparentdescriptioneventtabPreview();
                    $scope.showDPEventdesc = true;
                    var elem = document.getElementById('editdescscrolldiv');
                    elem.scrollTop = elem.scrollHeight;
            }else{  
                    $scope.showDPEventdesc = false;
            }
            
        });
        
        //MULTIPLE NEW CHILD EVENT NAME - ADD
        $scope.$watch(function () {return $localStorage.PreviewMEventName;}, function (newVal, oldVal) { 
            $scope.newDCEventNameText = newVal;
            if( $localStorage.PreviewMEventName) {
                $scope.showDMEventName = true;
                $scope.showDCEventCost = false;
                $scope.editchildeventtabPreview();
            }else{                    
                $scope.showDMEventName = false;
            }
        });
        
         //MULTIPLE CHILD EVENT LIST -AFTER ADD
        $scope.$watch(function () {return $localStorage.preview_eventchildlist;}, function (newVal, oldVal) {
            if( $localStorage.preview_eventchildlist) {                
                $scope.editchildeventtabPreview();
            }
        });
        
         //MULTIPLE CHILD EVENT LIST -HIDE FOR EDIT
        $scope.$watch(function () {return $localStorage.preview_childlisthide;}, function (newVal, oldVal) { 
            if($localStorage.preview_childlisthide) {                
                $scope.eventlistshow = true;
            }else{
               $scope.eventlistshow = false;
            }
            
        });
        
        //CHILD EVENT IMAGE UPLOAD
        $scope.$watch(function () {return $localStorage.PreviewPImageUrl;}, function (newVal, oldVal) { 
            $scope.newDPEventImage = newVal;
            if( $localStorage.PreviewImageUrl){                
                $scope.showDPEventImage = true;
            }else{  
                $scope.showDPEventImage = false;
            }
        });
        
        //EDIT CHILD EVENT IMAGE UPLOAD
        $scope.$watch(function () {return $localStorage.PreviewEditPImageUrl;}, function (newVal, oldVal) { 
            $scope.newDEditPEventImage = newVal;
            if( $localStorage.PreviewEditPImageUrl){                
                $scope.showDEditPEventImage = true;
            }else{  
                $scope.showDEditPEventImage = false;
            }
        });
        
        //MULTIPLE NEW CHILD EVENT Start DATE - ADD
        $scope.$watch(function () {return $localStorage.PreviewMEventDate;}, function (newVal, oldVal) {
            if($localStorage.PreviewMEventDate === '' || $localStorage.PreviewMEventTime === undefined ){
                $scope.showDCEventTime=false;
                $scope.showDCEventDate = false;
                $scope.newDCEventTimeText="";
                $scope.newDCEventTimeTextt = '';
            }
            
                if ($scope.newDCEventTimeTextt) {
                    $scope.showDCEventDate = false;
                    $scope.newDCEventDateText = newVal;
                    if($scope.newDCEventTimeText){
                    var time = $scope.newDCEventTimeText;
                    var hours = Number(time.match(/^(\d+)/)[1]);
                    var minutes = Number(time.match(/:(\d+)/)[1]);
                    var AMPM = time.match(/\s(.*)$/)[1];
                    if (AMPM == "PM" && hours < 12)
                        hours = hours + 12;
                    if (AMPM == "AM" && hours == 12)
                        hours = hours - 12;
                    var sHours = hours.toString();
                    var sMinutes = minutes.toString();
                    if (hours < 10)
                        sHours = "0" + sHours;
                    if (minutes < 10)
                        sMinutes = "0" + sMinutes;
                    $scope.newDCETime = sHours + ":" + sMinutes + ":00";
                    $scope.newDCEventTimeTextt = newVal + " " + $scope.newDCETime;
                    }else{
                     $scope.showDCEventTime = false;
                     $scope.newDCEventTimeTextt = newVal + " " + $scope.newDCEventTimeText; 
                     $scope.showDCEventDate = true;

                    }

                }else {
                    $scope.showDCEventDate = true;
                    $scope.newDCEventDateText = newVal;
                    $scope.newDCEventTimeTextt = $scope.newDCEventDateText;
                }
            
           
        });
        //MULTIPLE NEW CHILD EVENT Start TIME- ADD
        $scope.$watch(function () {return $localStorage.PreviewMEventTime;}, function (newVal, oldVal) {
            if($localStorage.PreviewMEventTime === undefined || $localStorage.PreviewMEventTime === ""){
                
                $scope.showDCEventTime=false;
                $scope.showDCEventDate = true;
                $scope.newDCEventTimeText="";
            }
            else{
                $scope.newDCEventTimeText = newVal;                
                var time = $scope.newDCEventTimeText;
                var hours = Number(time.match(/^(\d+)/)[1]);
                var minutes = Number(time.match(/:(\d+)/)[1]);
                var AMPM = time.match(/\s(.*)$/)[1];
                if (AMPM == "PM" && hours < 12)
                    hours = hours + 12;
                if (AMPM == "AM" && hours == 12)
                    hours = hours - 12;
                var sHours = hours.toString();
                var sMinutes = minutes.toString();
                if (hours < 10)
                    sHours = "0" + sHours;
                if (minutes < 10)
                    sMinutes = "0" + sMinutes;
                $scope.newDCEventTimeTextFrmt = sHours + ":" + sMinutes + ":00";
                $scope.newDCEventTimeTextt = $scope.newDCEventDateText + " " + $scope.newDCEventTimeTextFrmt;
                
            if ($scope.newDCEventTimeText) {
                $scope.showDCEventTime = true;
                $scope.showDCEventDate = "";
            } else {
                $scope.showDCEventTime = false;
            }
            }
            
        });        
        
        //MULTIPLE NEW CHILD EVENT END DATE- ADD
        $scope.$watch(function () {return $localStorage.PreviewMEventEndDate;}, function (newVal, oldVal) {
            if ($scope.newDCEventENDTimeTextt) {
                $scope.showDDCEventEndDate = false;
                $scope.newDCEventENDDateText = newVal;
                if($scope.newDCEventENDTimeText){
                var time = $scope.newDCEventENDTimeText;
                var hours = Number(time.match(/^(\d+)/)[1]);
                var minutes = Number(time.match(/:(\d+)/)[1]);
                var AMPM = time.match(/\s(.*)$/)[1];
                if (AMPM == "PM" && hours < 12)
                    hours = hours + 12;
                if (AMPM == "AM" && hours == 12)
                    hours = hours - 12;
                var sHours = hours.toString();
                var sMinutes = minutes.toString();
                if (hours < 10)
                    sHours = "0" + sHours;
                if (minutes < 10)
                    sMinutes = "0" + sMinutes;
                $scope.newDCEENDTime = sHours + ":" + sMinutes + ":00";
                $scope.newDCEventENDTimeTextt = newVal + " " + $scope.newDCEENDTime;
                }else{
                 $scope.showDCEventEndTime = false;
                 $scope.newDCEventENDTimeTextt = newVal + " " + $scope.newDCEventENDTimeText; 
                 $scope.showDDCEventEndDate = true;
                 
                }
                
            } else {
                $scope.showDDCEventEndDate = true;
                $scope.newDCEventENDDateText = newVal;
                $scope.newDCEventENDTimeTextt = $scope.newDCEventENDDateText;
            }
            if($localStorage.PreviewMEventEndDate === ''){
                $scope.showDCEventEndTime=false;
                $scope.showDDCEventEndDate = false;
                $scope.newDCEventENDTimeText="";
            }
        });
        //MULTIPLE NEW CHILD EVENT END TIME- ADD
        $scope.$watch(function () {return $localStorage.PreviewMEventEndTime;}, function (newVal, oldVal) {
            if($localStorage.PreviewMEventEndTime === undefined || $localStorage.PreviewMEventEndTime === ""){
                $scope.showDCEventEndTime=false;
                $scope.showDDCEventEndDate = true;
                $scope.newDCEventENDTimeText="";
            }
            else{
                $scope.newDCEventENDTimeText = newVal;                
                var time = $scope.newDCEventENDTimeText;
                var hours = Number(time.match(/^(\d+)/)[1]);
                var minutes = Number(time.match(/:(\d+)/)[1]);
                var AMPM = time.match(/\s(.*)$/)[1];
                if (AMPM == "PM" && hours < 12)
                    hours = hours + 12;
                if (AMPM == "AM" && hours == 12)
                    hours = hours - 12;
                var sHours = hours.toString();
                var sMinutes = minutes.toString();
                if (hours < 10)
                    sHours = "0" + sHours;
                if (minutes < 10)
                    sMinutes = "0" + sMinutes;
                $scope.newDCEventENDTimeTextFrmt = sHours + ":" + sMinutes + ":00";
                $scope.newDCEventENDTimeTextt = $scope.newDCEventENDDateText + " " + $scope.newDCEventENDTimeTextFrmt;
                
            if ($scope.newDCEventENDTimeText) {
                $scope.showDCEventEndTime = true;
                $scope.showDDCEventEndDate = "";
            } else {
                $scope.showDCEventEndTime = false;
            }
            }
            
        });        
        
        //MULTIPLE NEW CHILD EVENT COST - ADD
        $scope.$watch(function () {return $localStorage.PreviewMEventCost;}, function (newVal, oldVal) {
                $scope.newDCEventCostText = newVal; 
                if($scope.newDCEventProcesFee === '2'){
                       $scope.EditCEventCost = $scope.newDCEventCostText;
                       $scope.EditCEventProcessCost = $scope.getProcessingCost($scope.newDCEventCostText);
                       $scope.EditEventTotalProcessCost = $scope.TotalEventProcessingCost($scope.newDCEventCostText);
            }
            if( $localStorage.PreviewMEventCost) {
                $scope.showDMEventCost = true;
                $scope.showDCEventCost = true;
                $scope.editchildeventtabPreview();
            }else{                    
                $scope.showDMEventCost = false;
                $scope.showDCEventCost = false;
            }
        });
        
        //MULTIPLE NEW CHILD EVENT COMPARE COST - ADD
        $scope.$watch(function () {return $localStorage.PreviewMEventCompareCost;}, function (newVal, oldVal) {
            
                $scope.newDMEventCompareCostText = newVal;
            if( $localStorage.PreviewMEventCompareCost) {
                $scope.showDMEventCompareCost = true;
                $scope.editchildeventtabPreview();
            }else{                    
                $scope.showDMEventCompareCost = false;
            }
        });
        
        //MULTIPLE NEW CHILD EVENT URL - ADD
        $scope.$watch(function () {return $localStorage.PreviewMEventUrl;}, function (newVal, oldVal) {
                $scope.newDCEventVideourlText = newVal;
            if( $localStorage.PreviewMEventUrl) {
                $scope.showDCEventVideo = true;
                $scope.editchilddescriptioneventtabPreview();
            }else{                    
                $scope.showDCEventVideo = false;
            }
        });
        
         //MULTIPLE NEW CHILD EVENT DESCRIPTION - ADD
        $scope.$watch(function () {return $localStorage.PreviewMEventDesc;}, function (newVal, oldVal) {
                $scope.newDCEventdescText = newVal;
            if( $localStorage.PreviewMEventDesc) {
                $scope.showCMEventdesc = true;
                $scope.editchilddescriptioneventtabPreview();
                var elem = document.getElementById('editchilddescscrolldiv');
                elem.scrollTop = elem.scrollHeight;
            }else{                    
                $scope.showCMEventdesc = false;
            }
        });
        
       //NEW CHILD EVENT CAPACITY
        $scope.$watch(function () {return $localStorage.PreviewMECapacity;}, function (newVal, oldVal) { 
                    $scope.newMEventCapacity = newVal;
            if($localStorage.PreviewMECapacity) {               
                    $scope.showMEventCapacity = true;
            }else{  
                    $scope.showMEventCapacity = false;
            }
        });
       
        //NEW CHILD EVENT SPACE REMAINING
        $scope.$watch(function () {return $localStorage.PreviewMESpaceRemaining;}, function (newVal, oldVal) { 
            $scope.newMEventSpaceRemaining = newVal;
            if( $localStorage.PreviewMESpaceRemaining) {                 
                    $scope.showMEventSpaceRemaining = true;
                    $scope.editchildeventtabPreview();
            }else{  
                    $scope.showMEventSpaceRemaining = false;
            }
        });
        
         //CHILD EVENT LIST
        $scope.$watch(function () {return $localStorage.PreviewChildEventList;}, function (newVal, oldVal) {
             $scope.eventChildContent = newVal;
//            if($localStorage.PreviewChildEventList.length > 0 && $localStorage.PreviewChildEventList !== '') {
//                    $scope.editchildeventtabPreview();
//            }
        });
        
         //CHILD EVENT INDEX
        $scope.$watch(function () {return $localStorage.childeventindex;}, function (newVal, oldVal) {
             $scope.newDCEventIndex = newVal;
            if($localStorage.childeventindex >=0 && $localStorage.childeventindex !== '') {
                    $scope.editchildeventtabPreview();
            }
        });        
        
        //CHILD EVENT NAME
        $scope.$watch(function () {return $localStorage.PreviewCEventName;}, function (newVal, oldVal) {
            
            $scope.newDCEventNameText = newVal;
            if( $localStorage.PreviewCEventName) {
                    $scope.editchildeventtabPreview();
                    $scope.showDCEventName = true;
            }else {  
                    $scope.showDCEventName = false;
            }
        });
        
        //CHILD EVENT Start DATE
        $scope.$watch(function () {return $localStorage.PreviewCEventDate;}, function (newVal, oldVal) {
            if ($scope.newDCEventTimeTextt) {
                $scope.showDCEventDate = false;
                $scope.newDCEventDateText = newVal;
                if($scope.newDCEventTimeText){
                var time = $scope.newDCEventTimeText;
                var hours = Number(time.match(/^(\d+)/)[1]);
                var minutes = Number(time.match(/:(\d+)/)[1]);
                var AMPM = time.match(/\s(.*)$/)[1];
                if (AMPM == "PM" && hours < 12)
                    hours = hours + 12;
                if (AMPM == "AM" && hours == 12)
                    hours = hours - 12;
                var sHours = hours.toString();
                var sMinutes = minutes.toString();
                if (hours < 10)
                    sHours = "0" + sHours;
                if (minutes < 10)
                    sMinutes = "0" + sMinutes;
                $scope.newDCETime = sHours + ":" + sMinutes + ":00";
                $scope.newDCEventTimeTextt = newVal + " " + $scope.newDCETime;
                }else{
                 $scope.showDCEventTime = false;
                 $scope.newDCEventTimeTextt = newVal + " " + $scope.newDCEventTimeText; 
                 $scope.showDCEventDate = true;
                 
                }
                
            } else {
                $scope.showDCEventDate = true;
                $scope.newDCEventDateText = newVal;
                $scope.newDCEventTimeTextt = $scope.newDCEventDateText;
            }
            if($localStorage.PreviewCEventDate === ''){
                $scope.showDCEventTime=false;
                $scope.showDCEventDate = false;
                $scope.newDCEventTimeText="";
            }
        });
        //CHILD EVENT Start TIME
        $scope.$watch(function () {return $localStorage.PreviewCEventTime;}, function (newVal, oldVal) {
            if($localStorage.PreviewCEventTime == undefined || $localStorage.PreviewCEventTime == ""){
                $scope.showDCEventTime=false;
                $scope.showDCEventDate = true;
                $scope.newDCEventTimeText="";
            }
            else{
                $scope.newDCEventTimeText = newVal;                
                var time = $scope.newDCEventTimeText;
                var hours = Number(time.match(/^(\d+)/)[1]);
                var minutes = Number(time.match(/:(\d+)/)[1]);
                var AMPM = time.match(/\s(.*)$/)[1];
                if (AMPM == "PM" && hours < 12)
                    hours = hours + 12;
                if (AMPM == "AM" && hours == 12)
                    hours = hours - 12;
                var sHours = hours.toString();
                var sMinutes = minutes.toString();
                if (hours < 10)
                    sHours = "0" + sHours;
                if (minutes < 10)
                    sMinutes = "0" + sMinutes;
                $scope.newDCEventTimeTextFrmt = sHours + ":" + sMinutes + ":00";
                $scope.newDCEventTimeTextt = $scope.newDCEventDateText + " " + $scope.newDCEventTimeTextFrmt;
                
            if ($scope.newDCEventTimeText) {
                $scope.showDCEventTime = true;
                $scope.showDCEventDate = "";
            } else {
                $scope.showDCEventTime = false;
            }
            }
            
        });
        
        
        //CHILD EVENT END DATE
        $scope.$watch(function () {return $localStorage.PreviewCEventEndDate;}, function (newVal, oldVal) {
            if ($scope.newDCEventENDTimeTextt) {
                $scope.showDDCEventEndDate = false;
                $scope.newDCEventENDDateText = newVal;
                if($scope.newDCEventENDTimeText){
                var time = $scope.newDCEventENDTimeText;
                var hours = Number(time.match(/^(\d+)/)[1]);
                var minutes = Number(time.match(/:(\d+)/)[1]);
                var AMPM = time.match(/\s(.*)$/)[1];
                if (AMPM == "PM" && hours < 12)
                    hours = hours + 12;
                if (AMPM == "AM" && hours == 12)
                    hours = hours - 12;
                var sHours = hours.toString();
                var sMinutes = minutes.toString();
                if (hours < 10)
                    sHours = "0" + sHours;
                if (minutes < 10)
                    sMinutes = "0" + sMinutes;
                $scope.newDCEENDTime = sHours + ":" + sMinutes + ":00";
                $scope.newDCEventENDTimeTextt = newVal + " " + $scope.newDCEENDTime;
                }else{
                 $scope.showDCEventEndTime = false;
                 $scope.newDCEventENDTimeTextt = newVal + " " + $scope.newDCEventENDTimeText; 
                 $scope.showDDCEventEndDate = true;
                 
                }
                
            } else {
                $scope.showDDCEventEndDate = true;
                $scope.newDCEventENDDateText = newVal;
                $scope.newDCEventENDTimeTextt = $scope.newDCEventENDDateText;
            }
            if($localStorage.PreviewCEventDate === ''){
                $scope.showDCEventEndTime=false;
                $scope.showDDCEventEndDate = false;
                $scope.newDCEventENDTimeText="";
            }
        });
        //CHILD EVENT END TIME
        $scope.$watch(function () {return $localStorage.PreviewCEventEndTime;}, function (newVal, oldVal) {
            if($localStorage.PreviewCEventTime == undefined || $localStorage.PreviewCEventTime == ""){
                $scope.showDCEventEndTime=false;
                $scope.showDDCEventEndDate = true;
                $scope.newDCEventENDTimeText="";
            }
            else{
                $scope.newDCEventENDTimeText = newVal;                
                var time = $scope.newDCEventENDTimeText;
                var hours = Number(time.match(/^(\d+)/)[1]);
                var minutes = Number(time.match(/:(\d+)/)[1]);
                var AMPM = time.match(/\s(.*)$/)[1];
                if (AMPM == "PM" && hours < 12)
                    hours = hours + 12;
                if (AMPM == "AM" && hours == 12)
                    hours = hours - 12;
                var sHours = hours.toString();
                var sMinutes = minutes.toString();
                if (hours < 10)
                    sHours = "0" + sHours;
                if (minutes < 10)
                    sMinutes = "0" + sMinutes;
                $scope.newDCEventENDTimeTextFrmt = sHours + ":" + sMinutes + ":00";
                $scope.newDCEventENDTimeTextt = $scope.newDCEventENDDateText + " " + $scope.newDCEventENDTimeTextFrmt;
                
            if ($scope.newDCEventENDTimeText) {
                $scope.showDCEventEndTime = true;
                $scope.showDDCEventEndDate = "";
            } else {
                $scope.showDCEventEndTime = false;
            }
            }
            
        });
        
        
        //CHILD EVENT COST
        $scope.$watch(function () {return $localStorage.PreviewCEventCost;}, function (newVal, oldVal) {
            $scope.newDCEventCostText = newVal;
            if($scope.newDCEventProcesFee === '2'){
                       $scope.EditCEventCost = $scope.newDCEventCostText;
                       $scope.EditCEventProcessCost = $scope.getProcessingCost($scope.newDCEventCostText);
                       $scope.EditEventTotalProcessCost = $scope.TotalEventProcessingCost($scope.newDCEventCostText);
            }
            if( $localStorage.PreviewCEventCost) {
                    $scope.editchildeventtabPreview();
                    $scope.showDCEventCost = true;
            }else{  
                    $scope.showDCEventCost = false;
            }
        });
        
        
        
        //CHILD EVENT COMPARE COST
        $scope.$watch(function () {return $localStorage.PreviewCEventCompareCost;}, function (newVal, oldVal) {
            
                    $scope.newDCEventCompareCostText = newVal;
            if( $localStorage.PreviewCEventCompareCost) {
                    $scope.editchildeventtabPreview();
                    $scope.showDCEventCompareCost = true;
            }else{  
                    $scope.showDCEventCompareCost = false;
            }
        });
        
        //CHILD EVENT URL
        $scope.$watch(function () {return $localStorage.PreviewCEventUrl;}, function (newVal, oldVal) {
            
            $scope.newDCEventVideourlText = newVal;
            if ($localStorage.PreviewCEventUrl) {
                $scope.editchilddescriptioneventtabPreview();
                $scope.showDCEventVideo = true;
            } else  {
                $scope.showDCEventVideo = false;
            }          
        });
        
         //CHILD EVENT DESCRIPTION
        $scope.$watch(function () {return $localStorage.PreviewCEventDescEdit;}, function (newVal, oldVal) {
           
                $scope.newDCEventdescText = newVal;
             if ($localStorage.PreviewCEventDescEdit) {
                 if($localStorage.PreviewCfirstedit !=='Y'){
                     $scope.editchilddescriptioneventtabPreview();
                 }                
                $scope.showCMEventdesc = true;
                var elem = document.getElementById('editchilddescscrolldiv');
                elem.scrollTop = elem.scrollHeight;
            } else{
                $scope.showCMEventdesc = false;
            } 
            $localStorage.PreviewCfirstedit ='N';
        });
        
         //CHILD EVENT CAPACITY
        $scope.$watch(function () {return $localStorage.PreviewCECapacity;}, function (newVal, oldVal) { 
                    $scope.newCEventCapacity = newVal;
            if($localStorage.PreviewCECapacity) {
                    $scope.showCEventCapacity = true;
            }else{  
                    $scope.showCEventCapacity = false;
            }
        });
        
        //CHILD EVENT SPACE REMAINING
        $scope.$watch(function () {return $localStorage.PreviewCESpaceRemaining;}, function (newVal, oldVal) { 
            $scope.newCEventSpaceRemaining = newVal;
            if( $localStorage.PreviewCESpaceRemaining) {
                    $scope.showCEventSpaceRemaining = true;
                    $scope.editchildeventtabPreview();
            }else{  
                    $scope.showCEventSpaceRemaining = false;
            }
        });
        
         //CHILD EVENT PROCESSING FEE SELECTION
        $scope.$watch(function () {return $localStorage.PreviewCEProcesFeeSelection;}, function (newVal, oldVal) { 
            
            $scope.newDCEventProcesFee = newVal;            
            if( $localStorage.PreviewCEProcesFeeSelection) {
                    $scope.editchildeventtabPreview();
                    $scope.showDCEventProcesFee = true;
                    if($scope.newDCEventProcesFee === '2'){
                       $scope.EditCEventCost = $scope.newDCEventCostText;
                       $scope.EditCEventProcessCost = $scope.getProcessingCost($scope.newDCEventCostText);
                       $scope.EditEventTotalProcessCost = $scope.TotalEventProcessingCost($scope.newDCEventCostText);
                    }
            }else{  
                    $scope.showDCEventProcesFee = false;
                    $localStorage.PreviewCEProcesFeeSelection = '';
            }
        });
        
        
        //EVENT TOP TAB BAR MENU SELECTION CHANGE
        $scope.$watch(function () {return $localStorage.currentpreviewtab;}, function (newVal, oldVal) {
            if ($localStorage.currentpreviewtab === 'events' || $localStorage.currentpreviewtab === 'payment') {
                     $scope.indexeventtabPreview();
            }else if($localStorage.currentpreviewtab === 'childevents'){
                     $scope.editchildeventtabPreview();
            }else if ($localStorage.currentpreviewtab === 'registration' || $localStorage.currentpreviewtab === 'waiver') {
               if($localStorage.currentpage === "editevents"){                
                     $scope.editregistrationeventtabPreview();
                }else if($localStorage.currentpage === "" || $localStorage.currentpage === undefined){
                     $scope.registrationeventtabPreview();
                }
            }
            
        });
        
        // List of registration fields
        $scope.$watch(function () {return $localStorage.PreviewEventRegcols;}, function (newVal, oldVal) {
             $scope.reg_col_names = newVal;
        });
        
        
        // SINGLE OR MULTIPLE EVENT NEW REGISTRATION
        $scope.$watch(function () {return $localStorage.PreviewNewRegistrationField;}, function (newVal, oldVal) {            
             $scope.newEventNewRegText = newVal;
          if($localStorage.PreviewNewRegistrationField) {
                    $scope.editregistrationeventtabPreview();
                    $scope.showEventNewReg = true;
            }else{ 
                    $scope.showEventNewReg = false;
            }
             
        });
        
        // SINGLE OR MULTIPLE EVENT NEW MANDATORY CHECK
        $scope.$watch(function () {return $localStorage.PreviewNewRegFieldMandatory;}, function (newVal, oldVal) {
           
             $scope.newEventNewRegMandate = newVal;
          if($localStorage.PreviewNewRegFieldMandatory) {
                    $scope.editregistrationeventtabPreview();
                    $scope.showENewRegMandate = true;
            }else{ 
                    $scope.showENewRegMandate = false;
            }
             
        });
        
        // SINGLE OR MULTIPLE EVENT NEW REG CANCEL
        $scope.$watch(function () {return $localStorage.PreviewNewRegCancel;}, function (newVal, oldVal) {
           
          if($localStorage.PreviewNewRegCancel === true ) {
                    $scope.newEventNewRegText = '';
                    $scope.showEventNewReg = false;
                    $scope.showENewRegMandate = false;
            }
             
        });
        
        // SINGLE OR MULTIPLE EVENT EXISTING REGISTRATION
        $scope.$watch(function () {return $localStorage.PreviewOldRegistrationField;}, function (newVal, oldVal) {
             $scope.newEventEditRegText = newVal;
          if($localStorage.PreviewOldRegistrationField) {
                    $scope.editregistrationeventtabPreview();
                    $scope.showEventEditReg = true;
            }else{ 
                    $scope.showEventEditReg = false;
            }
             
        });
        
        // SINGLE OR MULTIPLE EVENT EXISTING MANDATORY CHECK
        $scope.$watch(function () {return $localStorage.PreviewOldRegFieldMandatory;}, function (newVal, oldVal) {            
          $scope.newEventEditRegMandate = newVal;
          if($localStorage.PreviewOldRegFieldMandatory) {
                    $scope.editregistrationeventtabPreview();
                    $scope.showEOldRegMandate = true;
            }else{ 
                    $scope.showEOldRegMandate = false;
            }
             
        });
        
        // SINGLE OR MULTIPLE EVENT EDIT REGISTRATION
        $scope.$watch(function () {return $localStorage.preview_reg_col_name_edit;}, function (newVal, oldVal) {
             $scope.newEventEditRegText = newVal;
            if($localStorage.preview_reg_col_name_edit) {
                    $scope.editregistrationeventtabPreview();
                    $scope.showEventEditReg = true;
                    if( $localStorage.PreviewOldRegFieldMandatory === true){
                      $scope.showEOldRegMandate = true;  
                    }else{
                        $scope.showEOldRegMandate = false;
                    }
            }else{ 
                    $scope.showEventEditReg = false;
            }
             
        });
        
         // SINGLE OR MULTIPLE EVENT EXISTING REG CANCEL
        $scope.$watch(function () {return $localStorage.PreviewOldRegCancel;}, function (newVal, oldVal) {
          if($localStorage.PreviewOldRegCancel === true ) {
                    $scope.newEventEditRegText = $localStorage.preview_reg_col_name_edit;
                    if($localStorage.PreviewOldRegFieldMandatory){
                      $scope.showEOldRegMandate = true;  
                    }else{
                      $scope.showEOldRegMandate = false;
                    }
                    $scope.showEventOldReg = false;
            }
             
        });
        
         //SINGLE OR MULTIPLE EVENT REGISTRATION FIELD INDEX EDIT
        $scope.$watch(function () {return $localStorage.preview_reg_fieldindex;}, function (newVal, oldVal) {
            if( $localStorage.preview_reg_fieldindex) {
                $scope.editregistrationeventtabPreview();                 
                $scope.newRegFieldIndex = newVal; 
            }
        });
        

//        
        // SINGLE OR MULTIPLE EVENT WAIVER
       $scope.$watch(function () {return $localStorage.PreviewWaiverText;}, function (newVal, oldVal) {
           $scope.newEventWaiverText = newVal;
           if($localStorage.PreviewWaiverText) {
                   $scope.editregistrationeventtabPreview();                    
                   $scope.showEventWaiver = true;
                   var elem = document.getElementById('editregscrolldiv');
                   elem.scrollTop = elem.scrollHeight;
           }else{ 
                   $scope.showEventWaiver = false;
           }
       });
 
         //MEMBERSHIP TOP TAB BAR MENU SELECTION CHANGE
        
        //TAB TOGGLE ON VALUE CHANGE
       $scope.indexmembershiptabPreview = function(){
           var membershipView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = membershipView.activePage.name;
                    f7.showTab('#memberships-view');
                    $scope.preloaderVisible = false;
                    if(currentipage !== 'memberships-listing'){
                    membershipView.router.loadPage({
                       pageName: 'memberships-listing'
                    });   
                }
       };
       
       $scope.descriptionmembershipcategorytabPreview = function(){
           var membershipView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = membershipView.activePage.name;
                    if(currentipage !== 'childmembershipdescription'){
                    membershipView.router.loadPage({
                       pageName: 'childmembershipdescription'
                    });
                }
       };
       
       
       $scope.editdescriptionmembershipcategorytabPreview = function(){
           var membershipView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = membershipView.activePage.name;
                    if(currentipage !== 'editmembershipparentdescription'){
                    membershipView.router.loadPage({
                       pageName: 'editmembershipparentdescription'
                    });
                }
       };
       
       $scope.childmembershiptabPreview = function(){
           var membershipView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = membershipView.activePage.name;
                    if(currentipage !== 'childmembershipslist'){
                    membershipView.router.loadPage({
                       pageName: 'childmembershipslist'
                    });
                }
       };
       
       $scope.editchildmembershiptabPreview = function(){
           var membershipView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = membershipView.activePage.name;
                    if(currentipage !== 'editchildmembershipslist'){
                    membershipView.router.loadPage({
                       pageName: 'editchildmembershipslist'
                    });
                }
       };
       
        $scope.descriptionmembershipoptiontabPreview = function(){
           var membershipView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = membershipView.activePage.name;
                    if(currentipage !== 'membershipparentdescription'){
                    membershipView.router.loadPage({
                       pageName: 'childmembershipdescription'
                    });
                }
       };
       
       
       $scope.editdescriptionmembershipoptiontabPreview = function(){
           var membershipView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = membershipView.activePage.name;
                    if(currentipage !== 'editmembershipparentdescription'){
                    membershipView.router.loadPage({
                       pageName: 'editchildmembershipdescription'
                    });
                }
       };
       
       
       $scope.registrationmembershipoptiontabPreview = function(){
           var membershipView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = membershipView.activePage.name;
                    if(currentipage !== 'membershipregistration'){
                    membershipView.router.loadPage({
                       pageName: 'membershipregistration'
                    });
                }
       };
       
       $scope.editregistrationmembershipoptiontabPreview = function(){
           var membershipView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = membershipView.activePage.name;
                    if(currentipage !== 'editmembershipregistration'){
                    membershipView.router.loadPage({
                       pageName: 'editmembershipregistration'
                    });
                }
       };
       
       $scope.editpaymentmembershipoptiontabPreview = function(){
           var membershipView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = membershipView.activePage.name;
                    if(currentipage !== 'editMembershipsCart'){
                    membershipView.router.loadPage({
                       pageName: 'editMembershipsCart'
                    });
                }
       };
       
        
      // NEW MEMBERSHIP CATEGORY NAME - ADD
        $scope.$watch(function () {return $localStorage.PreviewMembershipCategoryName;}, function (newVal, oldVal) {
                $scope.newMembershipCategoryText = newVal;
            if( $localStorage.PreviewMembershipCategoryName) {
                $scope.showMemCategoryname = true;
                $scope.indexmembershiptabPreview();
            }else{ 
                $scope.showMemCategoryname = false;
            }
        });
        
        // NEW MEMBERSHIP CATEGORY SUBTITLE  - ADD
        $scope.$watch(function () {return $localStorage.PreviewMembershipSubCategoryName;}, function (newVal, oldVal) {
                $scope.newMembershipSubCategoryText = newVal;
            if( $localStorage.PreviewMembershipSubCategoryName) {
                $scope.showMemSubCategoryname = true;
                $scope.indexmembershiptabPreview();
            }else{                    
                $scope.showMemSubCategoryname = false;
            }
        });
        
        //NEW MEMBERSHIP CATEGORY IMAGE  - ADD
        $scope.$watch(function () {return $localStorage.PreviewMembershipCategoryImage;}, function (newVal, oldVal) { 
            $scope.newMembershipCategoryImage = newVal;
            if( $localStorage.PreviewMembershipCategoryImage){                
                $scope.showMemCategoryImage = true;
            }else{  
                $scope.showMemCategoryImage = false;
            }
        });
       // NEW MEMBERSHIP URL - ADD
        $scope.$watch(function () {return $localStorage.PreviewMembershipCategoryVideoUrl;}, function (newVal, oldVal) {
                $scope.newMembershipCategoryUrl = newVal;
            if( $localStorage.PreviewMembershipCategoryVideoUrl) {
                $scope.showMemCategoryUrl = true;
                $scope.editdescriptionmembershipcategorytabPreview();
            }else{                    
                $scope.showMemCategoryUrl = false;
            }
        });
        
        // NEW MEMBERSHIP DESCRIPTION - ADD
        $scope.$watch(function () {return $localStorage.PreviewMembershipCategoryDesc;}, function (newVal, oldVal) {
                $scope.newMembershipCategorydesc = newVal;
            if( $localStorage.PreviewMembershipCategoryDesc) {
                $scope.showMemCategoryDesc = true;
                $scope.editdescriptionmembershipcategorytabPreview();
            }else{                    
                $scope.showMemCategoryDesc = false;
            }
        });
        
          //MEMBERSHIP OPTION TOP TAB BAR MENU SELECTION CHANGE
        $scope.$watch(function () {return $localStorage.currentmembershippreviewtab;}, function (newVal, oldVal) {
            if ($localStorage.currentmembershippreviewtab === 'details') {
                     $scope.indexmembershiptabPreview();
            }else if($localStorage.currentmembershippreviewtab === 'membershipoptions' || $localStorage.currentmembershippreviewtab === 'memoptdetails'){
                     $scope.editchildmembershiptabPreview();
            }else if ($localStorage.currentmembershippreviewtab === 'memoptpayment') {
                     $scope.editpaymentmembershipoptiontabPreview();
            }else if ($localStorage.currentmembershippreviewtab === 'memoptregistration' || $localStorage.currentmembershippreviewtab === 'memoptwaiver') {
                     $scope.editregistrationmembershipoptiontabPreview();
            }            
        });
        
         //NEW MEMBERSHIP OPTION ADD
        $scope.$watch(function () {return $localStorage.preview_membership_option_add;}, function (newVal, oldVal) {   
            $scope.membershipOptionAdd = newVal;
            if($scope.membershipOptionAdd=== 'add') {
                $scope.pay_infull_processingfee = 0;
                $scope.newTotalFee = 0;
                $scope.newMembershipFee_processing_fee = 0;
                $scope.newTotalMembershipFee = 0;
                $scope.newTotalRecurringFee = 0;
            }
             
        });
        
         // MEMBERSHIP OPTION CHILD CONTENT UPDATE
        $scope.$watch(function () {return $localStorage.preview_membership_childlist;}, function (newVal, oldVal) {            
             $scope.membershipOptionContent = newVal;
             if($localStorage.preview_membership_childlist && $localStorage.preview_membership_childlist.length > 0){                 
                  $scope.membershipOptionContentStatus = true;
             }else{
                 $scope.membershipOptionContentStatus = false;
             }
        });
        
          //MEMBERSHIP CHILD OPTION LIST -HIDE FOR EDIT
        $scope.$watch(function () {return $localStorage.preview_member_childlisthide;}, function (newVal, oldVal) { 
            if($localStorage.preview_member_childlisthide) {                
                $scope.memberlistshow = true;
//                $scope.newSEStartDate = '';
//                $scope.newSEEndDate = '';
            }else{
               $scope.memberlistshow = false;
            }
            
        });
        
          //MEMBERSHIP OPTION NEW ADD
        $scope.$watch(function () {return $localStorage.childmembershipoptionadd;}, function (newVal, oldVal) {
             if($localStorage.childmembershipoptionadd) {                
                $scope.newMemOptionAdd = true;
            }else{
               $scope.newMemOptionAdd = false;
            }
            
        });
        
          //MEMBERSHIP OPTION CHILD INDEX
        $scope.$watch(function () {return $localStorage.childmembershipindex;}, function (newVal, oldVal) {
             $scope.newmembershipOptionIndex = newVal;
            if($localStorage.childmembershipindex >=0 && $localStorage.childmembershipindex !== '') {
                    $scope.editchildmembershiptabPreview();
            }
        });
        
        
         // NEW MEMBERSHIP OPTION NAME - ADD
        $scope.$watch(function () {return $localStorage.PreviewMembershipOptionTitle;}, function (newVal, oldVal) {
                $scope.newMembershipOptionText = newVal;
            if( $localStorage.PreviewMembershipOptionTitle) {
                $scope.showMemOptionname = true;
                $scope.editchildmembershiptabPreview();
            }else{                    
                $scope.showMemOptionname = false;
            }
        });
        
        // NEW MEMBERSHIP OPTION SUBTITLE  - ADD
        $scope.$watch(function () {return $localStorage.PreviewMembershipOptionSubtitle;}, function (newVal, oldVal) {
                $scope.newMembershipOptionSubText = newVal;
            if( $localStorage.PreviewMembershipOptionSubtitle) {
                $scope.showMemOptionSubname = true;
                $scope.editchildmembershiptabPreview();
            }else{                    
                $scope.showMemOptionSubname = false;
            }
        });
        
         // NEW MEMBERSHIP OPTION DESCRIPTION - ADD
        $scope.$watch(function () {return $localStorage.PreviewMembershipOptionDesc;}, function (newVal, oldVal) {
                $scope.newMembershipOptiondesc = newVal;
            if($localStorage.PreviewMembershipOptionDesc) {
                $scope.showMemOptionDesc = true;
                $scope.descriptionmembershipoptiontabPreview();
            }else{                    
                $scope.showMemOptionDesc = false;
            }
        });
        
        $scope.newSEProcessingFee = function (recurring_amnt) { 
            var process_fee_per = 0, process_fee_val = 0, recurring_pro_fee = 0;
            var process_fee_per = parseFloat($scope.process_fee_per);
            var process_fee_val = parseFloat($scope.process_fee_val);
            if (parseFloat(recurring_amnt) >= 0 && parseFloat(recurring_amnt) != ''){
                if ($scope.newMemProcessingFee === 2 || $scope.newMemProcessingFee === '2') {
                    if (recurring_amnt > 0) {
                        var process_fee1 = +(parseFloat(recurring_amnt)) + +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                        if(isFinite(recurring_pro_fee)===false){
                            recurring_pro_fee = 0;
                        }
                    } else {
                        recurring_pro_fee = 0;
                    }
                } else if ($scope.newMemProcessingFee === 1 || $scope.newMemProcessingFee === '1'){
                    if (recurring_amnt > 0) {
                        var process_fee = +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                        if(isFinite(recurring_pro_fee)===false){
                            recurring_pro_fee = 0;
                        }
                    } else {
                        recurring_pro_fee = 0;
                    } 
                }
            }
            return recurring_pro_fee;
        };
        
        
        $scope.newSEMembershipFee = function (count) { 
            var SEMem_cost = 0;
            var selected_count = ($scope.newbilling_days.match(/1/g) || []).length;
            if (parseFloat($scope.newMembershipFee) >= 0 && parseFloat($scope.newMembershipFee) != ''){
                    if ($scope.newSpecificFrequency === 'PW' && parseFloat(count) > 0){   
                        SEMem_cost = (parseFloat($scope.newMembershipFee) / parseFloat(selected_count)) *parseFloat(count);
                    } else if ($scope.newSpecificFrequency === 'PM'  && parseFloat(count) > 0){  
                        SEMem_cost = (parseFloat($scope.newMembershipFee) / (parseFloat(selected_count) * 4.345)) * parseFloat(count);
                    }else{
                        SEMem_cost = 0;
                    }
            }else{
                SEMem_cost = 0;
            }

            return SEMem_cost;
        };
     
             
        $scope.newstartandenddate = function (start, end) {
            if(start !== undefined && start !== '' && end !== undefined && end !== ''){
                var check_start_date = start;
                var check_end_date = end;
                $scope.daysarray = $scope.newbilling_days.split('');
                $scope.checked_days_count = 0;
                for (var check_date = check_start_date; check_date <= check_end_date; check_date.setDate(check_date.getDate() + 1)) {
                    $scope.day_check = check_date.getDay() - 1;
                    if ($scope.day_check === -1) {
                        $scope.day_check = 6;
                    }
                    if ($scope.daysarray[$scope.day_check] === '1') {
                        $scope.checked_days_count += 1;
                    }
                }
                return $scope.checked_days_count;
            }
        };
        
        $scope.editshowTotal = function(){
            $scope.edit_pay_infull_processingfee = $scope.newMembershipFee_processing_fee = $scope.newTotalRecurringFee = 0;
            var total_amount_edit = 0;
            $scope.newfirst_payment_status = false;            
            
            if ($scope.newMemStructure === 'OE') {
                total_amount_edit = +$scope.newSignupFee + +$scope.newMembershipFee;
                $scope.newTotalFee = +total_amount_edit;
            } else if ($scope.newMemStructure === 'NC' || $scope.newMemStructure === 'C') {
                if ($scope.newMemBillingOption === 'PP') {
                        total_amount_edit = +$scope.newSignupFee + +$scope.newMemDepositAmnt;
                        $scope.newTotalFee = +total_amount_edit;
                    }else{
                    total_amount_edit = +$scope.newSignupFee + +$scope.newMembershipFee;
                        $scope.newTotalFee = +total_amount_edit;
                    }
            }else if($scope.newMemStructure === 'SE'){
                if ($scope.newMemBillingOption === 'PP') {
                    total_amount_edit = +$scope.newSignupFee;
                    $scope.newTotalFee = +total_amount_edit;
                } else {
                    total_amount_edit = +$scope.newSignupFee;
                    $scope.newTotalFee = +total_amount_edit;
                }
            }
//            if($scope.newMembershipFeeInclude === 'N' && $scope.newMemStructure === 'OE'){
//                    $scope.newTotalFee = +$scope.newTotalFee - +$scope.newMembershipFee;
//            }
            
            if ($scope.newMemProcessingFee == 2 || $scope.newMemProcessingFee === '2') {
                if (total_amount_edit == 0 || total_amount_edit === '0' || total_amount_edit == '' || total_amount_edit === undefined) {
                    $scope.edit_pay_infull_processingfee = 0;
                } else {
                    $scope.edit_pay_infull_processingfee = $scope.getProcessingCost(total_amount_edit);
                }
                if ($scope.newMembershipFee == 0 || $scope.newMembershipFee === '0' || $scope.newMembershipFee == '' || $scope.newMembershipFee === undefined) {
                    $scope.newMembershipFee_processing_fee = 0;
                } else {
                    $scope.newMembershipFee_processing_fee = $scope.getProcessingCost($scope.newMembershipFee);
                }
                $scope.newTotalRecurringFee = +$scope.edit_pay_infull_processingfee + +total_amount_edit;
            } else {
                if (total_amount_edit == 0 || total_amount_edit === '0' || total_amount_edit == '' || total_amount_edit === undefined) {
                    $scope.edit_pay_infull_processingfee = 0;
                } else {
                    $scope.edit_pay_infull_processingfee = $scope.absorbFeeCost(total_amount_edit);
                }
                $scope.newMembershipFee_processing_fee = $scope.absorbFeeCost($scope.newMembershipFee);
                $scope.newTotalRecurringFee = +total_amount_edit;
            }
            
            
            if(($scope.newMemStructure === 'NC' || $scope.newMemStructure === 'C')&& $scope.newMemBillingOption === 'PP'){
                var number_of_payments = +$scope.newMemNoOfPayments;
                var recurring_amnt = +$scope.newMembershipFee - +$scope.newMemDepositAmnt;
                var recurring_amnt = parseFloat(recurring_amnt) / parseFloat(number_of_payments);
                var recurring_pro_fee = $scope.getProcessingCost(recurring_amnt);
                var intervalType = $scope.newMemPaymntStartDate;
                var frequencyType = $scope.newMemRecFrequency;
//                console.log("Edit Recurring Amount :"+recurring_amnt);
                var curr_date = new Date();
                $scope.newstartdate = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);
                var dup_date = $scope.safariiosdate($scope.newstartdate);
                if (intervalType === '1') {          //30 days after registration
                    dup_date.setDate(dup_date.getDate() + 30);
                    var dup = dup_date;
                    $scope.new_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                } else if (intervalType === '2') {       //1 week after registration
                    dup_date.setDate(dup_date.getDate() + 7);
                    var dup = dup_date;
                    $scope.new_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                } else if (intervalType === '3') {       //2 week after registration
                    dup_date.setDate(dup_date.getDate() + 14);
                    var dup = dup_date;
                    $scope.new_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                }else if (intervalType === '4') {       //custom date selection
                    var newstartdate = new Date($scope.newMemCustDate);
                    var cust_newstartdate = newstartdate.getFullYear() + '-' + ('0' + (newstartdate.getMonth()+1)).slice( - 2) + '-' + ('0' + newstartdate.getDate()).slice( - 2);
                    $scope.new_startdate = cust_newstartdate;  
                }else if (intervalType === '5') {       //on membership start date
                    $scope.new_startdate = $scope.newstartdate;  
                }
                var dup_date = $scope.safariiosdate($scope.new_startdate);
                
                $scope.editpay = [];
                $scope.recurrent = {};
                
                
                    if (parseFloat(recurring_amnt)>0 && frequencyType === 'M') {             //monthly
                        for (var i = 0; i < number_of_payments; i++) {
                            $scope.recurrent = {};
                            if (i === 0) {
                                $scope.recurrent['date'] = $scope.new_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.editpay.push($scope.recurrent);
                            } else {
                                dup_date = $scope.addMonths(dup_date, 1);
    //                            var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                var date = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-' + ('0' + dup_date.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] =  recurring_pro_fee;
                                $scope.editpay.push($scope.recurrent);
                            }
                        }
                    } else if (parseFloat(recurring_amnt)>0 && frequencyType === 'W') {           //weekly
                        for (var i = 0; i < number_of_payments; i++) {
                            $scope.recurrent = {}; 
                            if (i === 0) {
                                $scope.recurrent['date'] = $scope.new_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.editpay.push($scope.recurrent);
                            } else {
                                dup_date.setDate(dup_date.getDate() + (+7));
                                var dup =  dup_date;
                                var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.editpay.push($scope.recurrent);
                            }
                        }
                    } else if (parseFloat(recurring_amnt)>0 && frequencyType === 'B') {           //Bi-weekly
                        for (var i = 0; i < number_of_payments; i++) {
                             $scope.recurrent = {}; 
                            if (i === 0) {
                                $scope.recurrent['date'] = $scope.new_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.editpay.push($scope.recurrent);
                            } else {
                                dup_date.setDate(dup_date.getDate() + (+14));
                                var dup =  dup_date;
                                var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.editpay.push($scope.recurrent);
                            }
                        }
                    }              
                    
            }else if($scope.newMemStructure === 'SE'){              
                var curr_date = new Date($scope.newSEStartDate);
                $scope.newstartdate = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);
                var dup_date = $scope.safariiosdate($scope.newstartdate);
                var frequencyType = $scope.newMemRecFrequency;
                var specific_payment_frequency = $scope.newSpecificFrequency;
                var billing_option = $scope.newMemBillingOption; 
                
                    $scope.editpay = [];
                    if (specific_payment_frequency === 'PM') {             //per monthly
                            var membership_firstDay = $scope.newSEdateFormat($scope.newSEStartDate);
                            var membership_lastDay = $scope.newSEdateFormat($scope.newSEEndDate); 
                            $scope.editpay = [];
                            $scope.recurrent = {};
                            var se_amount = 0;
                            var se_processing_fee = 0;
                            $scope.month_array = [];
                            $scope.total_count_array = [];
                            for (date = membership_firstDay; date <= membership_lastDay; date.setMonth(date.getMonth() + 1, 1)) {
                                var count = 0, exclude_count = 0;
                                var y = date.getFullYear(), m = date.getMonth(), da = date.getDate(), hr = date.getHours(), min = date.getMinutes();
                                var startdate = new Date(y, m, da, hr, min);
                                var enddate = new Date(y, m + 1, 0, hr, min);
                                if (membership_lastDay < enddate) {
                                    enddate = membership_lastDay;
                                }
                                count = $scope.newstartandenddate(startdate, enddate);
                                $scope.total_count_array.push(count);
                                if ($scope.newexclude_from_billing_flag === 'Y' && $scope.new_exclude_days_array.length > 0) {
                                    for (i = 0; i < $scope.new_exclude_days_array.length; i++) {
                                        var final_exclude_startdate = "";
                                        var final_exclude_enddate = "";
                                        var exclude_start_date = $scope.newSEdateFormat($scope.new_exclude_days_array[i].billing_exclude_startdate);
                                        var exclude_end_date = $scope.newSEdateFormat($scope.new_exclude_days_array[i].billing_exclude_enddate);
                                        if ((exclude_start_date.getMonth() === date.getMonth()) && (exclude_start_date.getFullYear() === date.getFullYear())) {
                                             var y = exclude_start_date.getFullYear(), m = exclude_start_date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                                             var end_of_month = new Date(y, m + 1, 0, hr, min);
                                            if (date <= exclude_start_date || date <= exclude_end_date) {
                                                if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                    var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                    final_exclude_startdate = new Date(y, m, day, hr, min);
                                                    if (exclude_end_date > end_of_month) {
                                                         final_exclude_enddate = end_of_month;
                                                     }else{
                                                         final_exclude_enddate = exclude_end_date; //check
                                                     }
                                                } else {
                                                    final_exclude_startdate = exclude_start_date;
                                                    if (exclude_end_date > end_of_month) {
                                                        final_exclude_enddate = end_of_month;
                                                    } else {
                                                        final_exclude_enddate = exclude_end_date;
                                                    }
                                                }
                                            }
                                        } else if ((exclude_end_date.getMonth() === date.getMonth()) && (exclude_end_date.getFullYear() === date.getFullYear())) {
                                            if (date <= exclude_end_date || date <= exclude_start_date) {
                                                if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                    var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                    final_exclude_startdate = new Date(y, m, day, hr, min);
                                                    final_exclude_enddate = exclude_end_date;
                                                } else {
                                                    var y = exclude_end_date.getFullYear(), m = exclude_end_date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                                                    var startdate_of_month = new Date(y, m, 1, hr, min);
                                                    final_exclude_enddate = exclude_end_date;
                                                    if (exclude_start_date < startdate_of_month) {
                                                        final_exclude_startdate = startdate_of_month;
                                                    } else {
                                                        final_exclude_startdate = exclude_start_date;
                                                    }
                                                }
                                            }
                                        } else if (((exclude_start_date.getMonth() < date.getMonth() && exclude_start_date.getFullYear() === date.getFullYear()) || (exclude_start_date.getFullYear() < date.getFullYear())) && ((exclude_end_date.getMonth() > date.getMonth() && exclude_end_date.getFullYear() === date.getFullYear()) || (exclude_end_date.getFullYear() > date.getFullYear()))) {
                                           var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day =date.getDate();
                                            if (date > exclude_start_date && date < exclude_end_date) {//for date picking in registration
                                                    final_exclude_startdate = new Date(y, m, day, hr, min);
                                                    final_exclude_enddate = new Date(y, m + 1, 0, hr, min);
                                                } else {
                                            final_exclude_startdate = new Date(y, m, 1, hr, min);
                                            final_exclude_enddate = new Date(y, m + 1, 0, hr, min);
                                        }
                                      }
                                        if (final_exclude_startdate !== '' && final_exclude_enddate !== '') {
                                            exclude_count = +exclude_count + +$scope.newstartandenddate(final_exclude_startdate, final_exclude_enddate);

                                        }
                                    }
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1] - +exclude_count;
                                    count = $scope.total_count_array[$scope.total_count_array.length - 1];
                                } else {
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1];
                                }
                                var se_start_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                                $scope.recurrent = {};
                                se_amount = $scope.newSEMembershipFee(count);
                                se_processing_fee = $scope.newSEProcessingFee(se_amount);
                                $scope.recurrent['date'] = se_start_date;
                                $scope.recurrent['amount'] = se_amount;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = se_processing_fee;
                                $scope.editpay.push($scope.recurrent);                      
                            }
                        } else if (specific_payment_frequency === 'PW') {           //per weekly
                            var membership_firstDay = $scope.newSEdateFormat($scope.newSEStartDate);
                            var membership_lastDay = $scope.newSEdateFormat($scope.newSEEndDate); 
                            $scope.editpay = [];
                            $scope.recurrent = {};
                            var se_amount = 0, se_processing_fee = 0;
                            $scope.week_array = [];
                            $scope.total_count_array = [];
                            for (date = membership_firstDay; date <= membership_lastDay; date.setDate(date.getDate() + 7)) {
                                var start_date = "";
                                var end_date, da;
                                var count, exclude_count = 0;
                                var first = date.getDate() - (date.getDay() - 1); // start of the week
                                var last = first + 6;//last day of week  
                                var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                                var firstday_of_week = new Date(y, m, first, hr, min);
                                var lastday_of_week = new Date(y, m, last, hr, min);
                                if (date <= membership_firstDay) {
                                    start_date = membership_firstDay;
                                } else {
                                    start_date = firstday_of_week;
                                }
                                if (lastday_of_week >= membership_lastDay) {
                                    end_date = membership_lastDay;
                                } else {
                                    end_date = lastday_of_week;
                                }
                                var y = start_date.getFullYear(), m = start_date.getMonth(), da = start_date.getDate(), shr = date.getHours(), smin = date.getMinutes();
                                var startdate = new Date(y, m, da, shr, smin);
                                var ey = end_date.getFullYear(), em = end_date.getMonth(), eda = end_date.getDate(), ehr = date.getHours(), emin = date.getMinutes();
                                var enddate = new Date(ey, em, eda, ehr, emin);
                                count = $scope.newstartandenddate(startdate, enddate);
                                $scope.total_count_array.push(count);

                                if ($scope.newexclude_from_billing_flag === 'Y' && $scope.new_exclude_days_array.length > 0) {
                                    for (i = 0; i < $scope.new_exclude_days_array.length; i++) {
                                        var exclude_start_date = $scope.newSEdateFormat($scope.new_exclude_days_array[i].billing_exclude_startdate);
                                        var exclude_end_date = $scope.newSEdateFormat($scope.new_exclude_days_array[i].billing_exclude_enddate);
                                        var final_exclude_startdate = "";
                                        var final_exclude_enddate = "";
//                                        if ((exclude_start_date.getMonth() === date.getMonth()) && (exclude_start_date.getFullYear() === date.getFullYear())) {
                                            if (date <= exclude_end_date || date <= exclude_start_date) {
                                                if (exclude_start_date >= firstday_of_week && exclude_start_date <= lastday_of_week) {
                                                    if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                        var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                        final_exclude_startdate = new Date(y, m, day, hr, min);
                                                        if(exclude_end_date > lastday_of_week){
                                                            final_exclude_enddate = lastday_of_week;
                                                        }else{
                                                            final_exclude_enddate = exclude_end_date;
                                                        }
                                                    } else {
                                                        final_exclude_startdate = exclude_start_date;
                                                        if (exclude_end_date > lastday_of_week) {
                                                            final_exclude_enddate = lastday_of_week;
                                                        } else {
                                                            final_exclude_enddate = exclude_end_date;
                                                        }
                                                    }
                                                } else if (exclude_end_date >= firstday_of_week && exclude_end_date <= lastday_of_week) {
                                                    if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                        var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                        final_exclude_startdate = new Date(y, m, day, hr, min);
                                                        final_exclude_enddate = exclude_end_date;
                                                    } else {
                                                        final_exclude_enddate = exclude_end_date;
                                                        if (exclude_start_date > firstday_of_week) {
                                                            final_exclude_startdate = exclude_start_date;
                                                        } else {
                                                            final_exclude_startdate = firstday_of_week;
                                                        }
                                                    }
                                                } else if (exclude_start_date < firstday_of_week && exclude_end_date > lastday_of_week) {
                                                    if (date > exclude_start_date && date < exclude_end_date) {//for date picking in registration
                                                        var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                        final_exclude_startdate = new Date(y, m, day, hr, min);
                                                            final_exclude_enddate = lastday_of_week;
                                                        } else {
                                                    final_exclude_startdate = firstday_of_week;
                                                    final_exclude_enddate = lastday_of_week;
                                                    
                                                }
                                            }
                                            }
                                        if (final_exclude_startdate !== '' && final_exclude_enddate !== '') {
                                            exclude_count = parseInt(exclude_count) + parseInt($scope.newstartandenddate(final_exclude_startdate, final_exclude_enddate));
                                        }
                                    }
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1] - +exclude_count;
                                    count = $scope.total_count_array[$scope.total_count_array.length - 1];
                                } else {
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1];
                                }
                                var se_start_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                                if (date.getDay() !== 1) {
                                    date = firstday_of_week;
                                }
                                $scope.recurrent = {};
                                se_amount = $scope.newSEMembershipFee(count);
                                se_processing_fee = $scope.newSEProcessingFee(se_amount);
                                $scope.recurrent['date'] = se_start_date;
                                $scope.recurrent['amount'] = se_amount;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = se_processing_fee;
                                $scope.editpay.push($scope.recurrent);                            
                            }                         
                        }
                        
//                            
                        for (i = 0; i < $scope.editpay.length; i++) {//wepay less than 5 calculation
                            $scope.editpay[i].prorate_flag = "F";
                            while ($scope.editpay[i].amount < 5) {
                                if ($scope.editpay[i].amount === 0) {
                                    $scope.editpay.splice(i, 1);
                                    i--;
                                    break;
                                }
                                var j = i + 1;
                                if ($scope.editpay[j] === undefined)
                                    break;
                                $scope.editpay[i].amount = $scope.editpay[i].amount + $scope.editpay[j].amount;
                                $scope.editpay[i].processing_fee = $scope.newSEProcessingFee($scope.editpay[i].amount);
                                $scope.editpay[i].prorate_flag = "T";
                                $scope.editpay.splice(j, 1);
                        }
                              
                    }
                    
                    if (specific_payment_frequency === 'PM' && $scope.editpay.length > 0) {  //first and last dates prorate check  
                        var first_date = $scope.newSEdateFormat($scope.editpay[0].date);
                        var last_date = $scope.newSEdateFormat($scope.newSEEndDate); 
                        var ye = last_date.getFullYear(), me = last_date.getMonth(), hre = last_date.getHours(), mine = last_date.getMinutes();
                        var end_of_months = new Date(ye, me + 1, 0, hre, mine);
                        if ($scope.editpay[0].prorate_flag !== "T") {
                            $scope.editpay[0].prorate_flag = (first_date.getDate() === 1) ? "F" : "T";
                        }
                        if ($scope.editpay[$scope.editpay.length - 1].prorate_flag !== "T" && $scope.editpay.length > 1) {
                            $scope.editpay[$scope.editpay.length - 1].prorate_flag = (last_date.getDate() === end_of_months.getDate()) ? "F" : "T";
                        }
                    } else if (specific_payment_frequency === 'PW' && $scope.editpay.length > 0) {
                        var first_date = $scope.newSEdateFormat($scope.editpay[0].date);
                        var last_date = $scope.newSEdateFormat($scope.newSEEndDate); 
                        var checkprorate_start, checkprorate_end = '';
                        $scope.newselected_days = $scope.newbilling_days.split('');
                        if ($scope.editpay[0].prorate_flag !== "T") {    //week start
                            for (i = 0; i < $scope.newselected_days.length; i++) {
                                if ($scope.newselected_days[i] === "1") {
                                    checkprorate_start = i;
                                    break;
                                }
                            }
                            $scope.days_check_start = first_date.getDay() - 1;
                            $scope.days_check_start = ($scope.days_check_start === -1) ? '6' : $scope.days_check_start;
                            $scope.editpay[0].prorate_flag = (checkprorate_start === $scope.days_check_start) ? "F" : "T";
                        }
                        if ($scope.editpay[$scope.editpay.length - 1].prorate_flag !== "T" && $scope.editpay.length > 1) {     //week end
                            for (j = $scope.newselected_days.length - 1; j >= 0; j--) {
                                if ($scope.newselected_days[j] === "1") {
                                    checkprorate_end = j;
                                    break;
                                }
                            }
                            $scope.days_check_last = last_date.getDay() - 1;
                            $scope.days_check_last = ($scope.days_check_last === -1) ? '6' : $scope.days_check_last;
                            $scope.editpay[$scope.editpay.length - 1].prorate_flag = (checkprorate_end === $scope.days_check_last) ? "F" : "T";
                        }
                    }
                        
                    if(parseInt($scope.editpay.length) > 1 && $scope.editpay !== undefined && $scope.editpay !== ''){    
                        if ($scope.editpay[$scope.editpay.length - 1].amount < 5) {
                            $scope.editpay[$scope.editpay.length - 2].amount += $scope.editpay[$scope.editpay.length - 1].amount;
                            $scope.editpay[$scope.editpay.length - 2].processing_fee =  $scope.newSEProcessingFee($scope.editpay[$scope.editpay.length - 2].amount);
                            $scope.editpay[$scope.editpay.length - 2].prorate_flag="T";
                            $scope.editpay.splice($scope.editpay.length - 1, 1);
                        }
                    }
                    
                    var se_mem_amount = 0; //total with exlude days
                    for(i=0; i<$scope.editpay.length; i++ ){
                        se_mem_amount = parseFloat(se_mem_amount) + parseFloat($scope.editpay[i].amount);
                    }
                    $scope.newTotalMembershipFee = se_mem_amount;
                    
                    if(billing_option === 'PF'){                        
                        if ($scope.newMembershipFeeInclude === 'Y') {
                            $scope.payment_amount = se_mem_amount;                            
                            total_amount_edit = +$scope.newSignupFee  + $scope.payment_amount;
                            $scope.newTotalFee = +total_amount_edit;
                        } else if ($scope.newMembershipFeeInclude === 'N') {
                            var curr_date = new Date();
                            var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                            if (new Date(server_curr_date) < new Date($scope.newstartdate)) { 
                                total_amount_edit = +$scope.newSignupFee;
                                $scope.newTotalFee = +total_amount_edit;
                                $scope.newfirst_payment_status = true;
                            }else{
                                total_amount_edit = +$scope.newSignupFee  + $scope.payment_amount;
                                $scope.newTotalFee = +total_amount_edit;
                                $scope.newfirst_payment_status = false;
                            }
                        }
                        $scope.editpay = [];
                        $scope.recurring_payment_startdate = '';
                    }
                    
                    if(billing_option === 'PP' && $scope.newMembershipFeeInclude === 'Y'){
                        if(parseInt($scope.editpay.length) > 0){
                            total_amount_edit = +$scope.newSignupFee + +$scope.editpay[0].amount;
                            $scope.newTotalFee = +total_amount_edit; 
                            $scope.editpay.splice(0, 1);
                        }
                    }
                    
                    if ($scope.newMemProcessingFee === 2 || $scope.newMemProcessingFee === '2') {
                        if (total_amount_edit === 0 || total_amount_edit === '0' || total_amount_edit === '' || total_amount_edit === undefined) {
                                $scope.edit_pay_infull_processingfee = 0;
                            } else {
                                $scope.edit_pay_infull_processingfee = $scope.getProcessingCost(total_amount_edit);
                            }
                            $scope.newTotalRecurringFee = +$scope.edit_pay_infull_processingfee + +total_amount_edit;
                    } else {
                        if (total_amount_edit === 0 || total_amount_edit === '0' || total_amount_edit === '' || total_amount_edit === undefined) {
                            $scope.edit_pay_infull_processingfee = 0;
                        } else {
                            $scope.edit_pay_infull_processingfee = $scope.absorbFeeCost(total_amount_edit);
                        }
                        $scope.newTotalRecurringFee = +total_amount_edit;
                    }
                                        
                    if($scope.editpay.length > 0){
                        $scope.recurring_payment_startdate = $scope.editpay[0].date;
                        $scope.payment_amount = $scope.editpay[0].amount;
                    }else{
                        $scope.editpay = [];
                        $scope.recurring_payment_startdate = '';
                   }
            }
        };
        
        
        $scope.expiry_date_view = function(){
            var curr_date = new Date();
            $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);
            
            if($scope.newMemStructure === 'NC'  && $scope.newMemExpStatus==='Y'){
                    var dup_date = $scope.safariiosdate($scope.payment_start_date);
                    if ($scope.newMemExpPer === 'M') {
                        dup_date = $scope.addMonths(dup_date, $scope.newMemExpVal);
                        var dup = dup_date;
                        $scope.newMemExpDate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }else if ($scope.newMemExpPer === 'W') {
                        dup_date.setDate(dup_date.getDate() + (+$scope.newMemExpVal * 7));
                        var dup = dup_date;
                        $scope.newMemExpDate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }else if ($scope.newMemExpPer === 'Y') {
                        dup_date.setFullYear(dup_date.getFullYear() + +$scope.newMemExpVal);
                        var dup = dup_date;
                        $scope.newMemExpDate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }                    
                }else{
                    $scope.newMemExpDate = '';
                }
        };
        
         // MEMBERSHIP PROCESSING FEE TYPE 
        $scope.$watch(function () {return $localStorage.PreviewMemProcessingFeeType;}, function (newVal, oldVal) {            
             $scope.newMemProcessingFee = newVal;
          if($localStorage.PreviewMemProcessingFeeType) {
                    $scope.editshowTotal();
                    $scope.showMemProcessingFee = true;
            }else{ 
                    $scope.showMemProcessingFee = false;
            }
             
        });
        
         // MEMBERSHIP SIGN UP FEE
        $scope.$watch(function () {return $localStorage.PreviewMemSignUpFee;}, function (newVal, oldVal) {           
             $scope.newSignupFee = newVal;
            if($localStorage.PreviewMemSignUpFee) {                     
                    $scope.showSignupFee = true;
            }else{ 
                    $scope.showSignupFee = false;
                    $scope.newSignupFee = 0;
            }
            $scope.editshowTotal();
             
        });
        
         // MEMBERSHIP FEE 
        $scope.$watch(function () {return $localStorage.PreviewMembershipFee;}, function (newVal, oldVal) {            
             $scope.newMembershipFee = newVal;
            if($localStorage.PreviewMembershipFee) {                    
                    $scope.showMembershipFee = true;
            }else{ 
                    $scope.showMembershipFee = false;
                    $scope.newMembershipFee = 0;
            }
            $scope.editshowTotal();
             
        });
        
         // MEMBERSHIP FEE INCLUDE 
        $scope.$watch(function () {return $localStorage.PreviewMembershipFeeInclude;}, function (newVal, oldVal) {            
             $scope.newMembershipFeeInclude = newVal;
             $scope.editshowTotal();
        });
        
         // MEMBERSHIP RECURRING TYPE
        $scope.$watch(function () {return $localStorage.PreviewMemRecurringFrequency;}, function (newVal, oldVal) {           
             $scope.newMemRecFrequency = newVal;
          if($localStorage.PreviewMemSignUpFee) {
                    $scope.showMemRecFrequency = true;
                    $scope.editshowTotal();
            }else{ 
                    $scope.showMemRecFrequency = false;
            }
             
        });
        
         // MEMBERSHIP RECURRING CUSTOM PERIOD
        $scope.$watch(function () {return $localStorage.PreviewMembershipCustPeriod;}, function (newVal, oldVal) {           
             $scope.newMemRecperiod = newVal;
        });
        
         // MEMBERSHIP RECURRING CUSTOM VALUE
        $scope.$watch(function () {return $localStorage.PreviewMembershipCustValue;}, function (newVal, oldVal) {           
             $scope.newMemRecValue = newVal;
        });
        
         // MEMBERSHIP OPTION STRUCTURE
        $scope.$watch(function () {return $localStorage.PreviewMembershipStructure;}, function (newVal, oldVal) {              
             $scope.newMemStructure = newVal;
             $scope.expiry_date_view();
        });
        
         // MEMBERSHIP OPTION CLASS PACKAGES
        $scope.$watch(function () {return $localStorage.PreviewNoofClass;}, function (newVal, oldVal) {  
            $scope.newNoofclass = parseFloat(newVal);
        });
        
        // MEMBERSHIP OPTION EXPIRATION STATUS
        $scope.$watch(function () {return $localStorage.PreviewMembershipExpiryStatus;}, function (newVal, oldVal) {              
               $scope.newMemExpStatus = newVal;
               $scope.expiry_date_view();
                if($scope.newMemExpStatus === 'Y') {
                    $scope.showMemExpVal = true;
                }else{ 
                    $scope.showMemExpVal = false;
                }
        });
        
         // MEMBERSHIP OPTION EXPIRATION VALUE
        $scope.$watch(function () {return $localStorage.PreviewMembershipExpiryValue;}, function (newVal, oldVal) {           
               $scope.newMemExpVal = newVal;
               $scope.expiry_date_view();
        });
        
        // MEMBERSHIP OPTION EXPIRATION PERIOD
        $scope.$watch(function () {return $localStorage.PreviewMembershipExpiryPeriod;}, function (newVal, oldVal) {              
               $scope.newMemExpPer = newVal;
               $scope.expiry_date_view();
        });
        
         // MEMBERSHIP OPTION BILLING OPTION
        $scope.$watch(function () {return $localStorage.PreviewMembershipBillingOptionValue;}, function (newVal, oldVal) {           
             $scope.newMemBillingOption = newVal;
             if($localStorage.PreviewMembershipBillingOptionValue) {
                    $scope.editshowTotal();
            }else{ 
                
            }
        });
        
         // MEMBERSHIP OPTION DEPOSIT AMOUNT
        $scope.$watch(function () {return $localStorage.PreviewMembershipDepositValue;}, function (newVal, oldVal) {           
               $scope.newMemDepositAmnt = newVal;
          if($localStorage.PreviewMembershipDepositValue) {
                    $scope.editshowTotal();
                    $scope.showMemDepositAmnt = true;
            }else{ 
                    $scope.showMemDepositAmnt = false;
            }
        });
        
         // MEMBERSHIP OPTION NUMBER OF PAYMENTS
        $scope.$watch(function () {return $localStorage.PreviewMembershipNoOfPayments;}, function (newVal, oldVal) {           
               $scope.newMemNoOfPayments = newVal;
          if($localStorage.PreviewMembershipNoOfPayments) {
                    $scope.showMemNoOfPayments = true;
                    $scope.editshowTotal();
            }else{ 
                    $scope.showMemNoOfPayments = false;
            }
        });
        
         // MEMBERSHIP OPTION PAYMENT START DATE TYPE
        $scope.$watch(function () {return $localStorage.PreviewMembershipPaymentStartDate;}, function (newVal, oldVal) {           
               $scope.newMemPaymntStartDate = newVal; 
               $scope.editshowTotal();
        });
        
         // MEMBERSHIP OPTION CUSTOM PAYMENT START DATE
        $scope.$watch(function () {return $localStorage.PreviewMemCustomPaymentStartDate;}, function (newVal, oldVal) {           
               $scope.newMemCustDate = newVal; 
               $scope.editshowTotal();
        });
        
         // MEMBERSHIP SPECIFIC START DATE
        $scope.$watch(function () {return $localStorage.Previewspecific_start_date;}, function (newVal, oldVal) { 
          if($localStorage.Previewspecific_start_date) {
                    $scope.newSEStartDate = newVal;
                    $scope.editshowTotal();
            }else{ 
                    $scope.newSEStartDate = '';
            }
        });
        
         // MEMBERSHIP SPECIFIC END DATE
        $scope.$watch(function () {return $localStorage.Previewspecific_end_date;}, function (newVal, oldVal) {   
          if($localStorage.Previewspecific_end_date) {
                    $scope.newSEEndDate =  newVal;
                    $scope.editshowTotal();
            }else{ 
                    $scope.newSEEndDate = '';
            }
        });
        
         // MEMBERSHIP SPECIFIC FREQUENCY
        $scope.$watch(function () {return $localStorage.Previewspecific_payment_frequency;}, function (newVal, oldVal) {    
            if($localStorage.Previewspecific_payment_frequency){
                $scope.newSpecificFrequency = newVal;
                $scope.editshowTotal();
            }else{
                $scope.newSpecificFrequency = '';
            }
        });
        
         // MEMBERSHIP SPECIFIC SELECTED DAYS 
        $scope.$watch(function () {return $localStorage.Previewspecific_selecteddays;}, function (newVal, oldVal) {  
            if($localStorage.Previewspecific_selecteddays){
                $scope.newbilling_days = newVal;
                $scope.editshowTotal();
            }else{
                $scope.newbilling_days = '';
            }
        });
        
         // MEMBERSHIP SPECIFIC EXCLUDE BILLING FLAG
        $scope.$watch(function () {return $localStorage.Previewexclude_from_billing_flag;}, function (newVal, oldVal) {            
            $scope.newexclude_from_billing_flag = newVal;
            $scope.editshowTotal();
        });
        
         // MEMBERSHIP SPECIFIC EXCLUDE DAYS LIST
        $scope.$watch(function () {return $localStorage.preview_exclude_days_array;}, function (newVal, oldVal) {            
            $scope.new_exclude_days_array = newVal;
            $scope.editshowTotal();
        });
        
         // MEMBERSHIP OPTION NEW REGISTRATION
        $scope.$watch(function () {return $localStorage.PreviewMembershipRegcols;}, function (newVal, oldVal) {            
             $scope.mem_reg_col_names = newVal;
        });
        
        
         // MEMBERSHIP OPTION NEW REGISTRATION
        $scope.$watch(function () {return $localStorage.PreviewNewMemRegistrationField;}, function (newVal, oldVal) {            
             $scope.newMemNewRegText = newVal;
          if($localStorage.PreviewNewMemRegistrationField) {
                    $scope.editregistrationmembershipoptiontabPreview();
                    $scope.showMemNewReg = true;
            }else{ 
                    $scope.showMemNewReg = false;
            }
        });
        
         // MEMBERSHIP OPTION NEW MANDATORY CHECK
        $scope.$watch(function () {return $localStorage.PreviewNewMemRegFieldMandatory;}, function (newVal, oldVal) {
           
             $scope.newMemNewRegMandate = newVal;
          if($localStorage.PreviewNewMemRegFieldMandatory) {
                    $scope.editregistrationmembershipoptiontabPreview();
                    $scope.showMemNewRegMandate = true;
            }else{ 
                    $scope.showMemNewRegMandate = false;
            }
             
        });
        
         // MEMBERSHIP OPTION NEW REG CANCEL
        $scope.$watch(function () {return $localStorage.PreviewNewMemRegCancel;}, function (newVal, oldVal) {
           
          if($localStorage.PreviewNewMemRegCancel === true ) {
                    $scope.newMemNewRegText = '';
                    $scope.showMemNewReg = false;
                    $scope.showMemNewRegMandate = false;
            }
             
        });
        
         // MEMBERSHIP OPTION EXISTING REGISTRATION
        $scope.$watch(function () {return $localStorage.PreviewOldMemRegistrationField;}, function (newVal, oldVal) {
             $scope.newMemEditRegText = newVal;
          if($localStorage.PreviewOldMemRegistrationField) {
                    $scope.editregistrationmembershipoptiontabPreview();
                    $scope.showMemEditReg = true;
            }else{ 
                    $scope.showMemEditReg = false;
            }
             
        });
        
         // MEMBERSHIP OPTION EXISTING MANDATORY CHECK
        $scope.$watch(function () {return $localStorage.PreviewOldMemRegFieldMandatory;}, function (newVal, oldVal) {            
          $scope.newMemEditRegMandate = newVal;
          if($localStorage.PreviewOldMemRegFieldMandatory) {
                    $scope.editregistrationeventtabPreview();
                    $scope.showMemOldRegMandate = true;
            }else{ 
                    $scope.showMemOldRegMandate = false;
            }
             
        });
        
        // MEMBERSHIP OPTION EDIT REGISTRATION
        $scope.$watch(function () {return $localStorage.preview_mem_reg_col_name_edit;}, function (newVal, oldVal) {
             $scope.newMemEditRegText = newVal;
            if($localStorage.preview_reg_col_name_edit) {
                    $scope.editregistrationmembershipoptiontabPreview();
                    $scope.showMemEditReg = true;
                    if( $localStorage.PreviewOldMemRegFieldMandatory === true){
                      $scope.showMemOldRegMandate = true;  
                    }else{
                        $scope.showMemOldRegMandate = false;
                    }
            }else{ 
                    $scope.showMemEditReg = false;
            }
             
        });
        
         // MEMBERSHIP OPTION EXISTING REG CANCEL
        $scope.$watch(function () {return $localStorage.PreviewOldMemRegCancel;}, function (newVal, oldVal) {
          if($localStorage.PreviewOldMemRegCancel === true ) {
                    $scope.newMemEditRegText = $localStorage.preview_mem_reg_col_name_edit;
                    if($localStorage.PreviewOldMemRegFieldMandatory){
                      $scope.showMemOldRegMandate = true;  
                    }else{
                      $scope.showMemOldRegMandate = false;
                    }
                    $scope.showMemOldReg = false;
            }
             
        });
        
          // MEMBERSHIP OPTION REGISTRATION FIELD INDEX EDIT
        $scope.$watch(function () {return $localStorage.preview_mem_reg_fieldindex;}, function (newVal, oldVal) {
            if( $localStorage.preview_mem_reg_fieldindex) {
                $scope.editregistrationmembershipoptiontabPreview();                 
                $scope.newMemRegFieldIndex = newVal; 
            }
        });
        

       
        // MEMBERSHIP OPTION WAIVER
       $scope.$watch(function () {return $localStorage.PreviewMembershipWaiver;}, function (newVal, oldVal) {
           $scope.newMemWaiverText = newVal;
           if($localStorage.PreviewMembershipWaiver) {              
                   $scope.showMemWaiver = true;
                   var elem = document.getElementById('editmemregscrolldiv');
                   elem.scrollTop = elem.scrollHeight;
           }else{ 
                   $scope.showMemWaiver = false;
           }
       });
       
        $scope.editTrialshowTotal = function(){
            var process_fee_per = 0, process_fee_val = 0;
            var process_fee_per = parseFloat($localStorage.companydetails.processing_fee_percentage);
            var process_fee_val = parseFloat($localStorage.companydetails.processing_fee_transaction);
            $scope.edit_trial_processing_fee_value = 0;
            $scope.edit_trial_total_amount_view = 0;
            var curr_date = new Date();
            $scope.edit_trial_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);
            var dupdate = $scope.safariiosdate($scope.edit_trial_start_date);
            var trialenddatequantity, trial_end;
            if ($scope.newTrialProgramPeriod == 'W') {
                trialenddatequantity = $scope.newTrialProgramQuantity * 7;   
                dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
            } else if ($scope.newTrialProgramPeriod == 'D'){
                trialenddatequantity = $scope.newTrialProgramQuantity;
                dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
            }
            $scope.edit_trial_start_date_view = $scope.dateString($scope.edit_trial_start_date);
            $scope.edit_trial_end_date_view = $scope.dateString(trial_end);
            $scope.edit_trial_amount_view = $scope.newTrialPrice;
            $scope.setEditTrialStartDate();    
            
            var amount = parseFloat($scope.newTrialPrice);
            $scope.edit_trial_amount_view = amount;
            if ($scope.newTrialProcessingFee === 2 || $scope.newTrialProcessingFee === '2') {
                if (amount > 0) {
                    var process_fee1 = +amount + +((+amount * +process_fee_per) / 100) + +process_fee_val;
                    var process_fee = +((+process_fee1 * +process_fee_per) / 100) + +process_fee_val;
                    $scope.edit_trial_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    $scope.edit_trial_processing_fee_value = 0;
                }
                $scope.edit_trial_total_amount_view = parseFloat(amount) + parseFloat($scope.edit_trial_processing_fee_value); 
            } else if ($scope.newTrialProcessingFee === 1 || $scope.newTrialProcessingFee === '1') {
                if (amount > 0) {
                    var process_fee = +((+amount * +process_fee_per) / 100) + +process_fee_val;
                    $scope.edit_trial_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    $scope.edit_trial_processing_fee_value = 0;
                }
                $scope.edit_trial_total_amount_view = parseFloat(amount); 
            }
            
        };
       
    
       $scope.indexTrialtabPreview = function(){
           var trialView = f7.addView('.trial-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = trialView.activePage.name;
                    f7.showTab('#trial-view');
                    $scope.preloaderVisible = false;
                    if(currentipage !== 'trial-listing'){
                    trialView.router.loadPage({
                       pageName: 'trial-listing'
                    });   
            }
       };
       
       $scope.editTrialdescriptionPreview = function(){
           var trialView = f7.addView('.trial-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = trialView.activePage.name;
                    if(currentipage !== 'edittrialdescription'){
                    trialView.router.loadPage({
                       pageName: 'edittrialdescription'
                    });
                }
                    
       };
       
       $scope.editTrialregistrationPreview = function(){
           var trialView = f7.addView('.trial-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = trialView.activePage.name;
                    if(currentipage !== 'edittrialregistration'){
                    trialView.router.loadPage({
                       pageName: 'edittrialregistration'
                    });
                }
       };
       
       $scope.editTrialpaymentPreview = function(){
           var trialView = f7.addView('.trial-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    // Load page:
                    var currentipage = trialView.activePage.name;
                    if(currentipage !== 'edittrialCart'){
                    trialView.router.loadPage({
                       pageName: 'edittrialCart'
                    });
                }
       };
       
                
      // NEW TRIAL NAME - ADD
        $scope.$watch(function () {return $localStorage.PreviewTrialName;}, function (newVal, oldVal) {
                $scope.newTrialText = newVal;
            if( $localStorage.PreviewTrialName) {
                $scope.showTrialname = true;
                $scope.indexTrialtabPreview();
            }else{ 
                $scope.showTrialname = false;
            }
        });
        
        // NEW TRIAL SUBTITLE  - ADD
        $scope.$watch(function () {return $localStorage.PreviewTrialSubName;}, function (newVal, oldVal) {
                $scope.newTrialSubText = newVal;
            if( $localStorage.PreviewTrialSubName) {
                $scope.showTrialSubname = true;
                $scope.indexTrialtabPreview();
            }else{                    
                $scope.showTrialSubname = false;
            }
        });
        
        //NEW TRIAL IMAGE  - ADD
        $scope.$watch(function () {return $localStorage.PreviewTrialImage;}, function (newVal, oldVal) { 
            $scope.newTrialImage = newVal;
            if( $localStorage.PreviewTrialImage){                
                $scope.showTrialImage = true;
            }else{  
                $scope.showTrialImage = false;
            }
        });
        
         // NEW TRIAL DESCRIPTION - ADD
        $scope.$watch(function () {return $localStorage.PreviewTrialDesc;}, function (newVal, oldVal) {
            $scope.newTrialdesc = newVal;
            if( $localStorage.PreviewTrialDesc) {
                $scope.showTrialDesc = true;
                $scope.editTrialdescriptionPreview();
            }else{                    
                $scope.showTrialDesc = false;
            }
        });       
       
        
        // NEW TRIAL PRICE  - ADD
        $scope.$watch(function () {return $localStorage.PreviewTrialPrice;}, function (newVal, oldVal) {
                $scope.newTrialPrice = newVal;
            if( $localStorage.PreviewTrialPrice) {
                $scope.showTrialPrice = true;
                $scope.editTrialshowTotal();
                $scope.editTrialpaymentPreview();
            }else{                    
                $scope.showTrialPrice = false;
            }
        });
        
        //NEW TRIAL PROCESSING FEE  - ADD
        $scope.$watch(function () {return $localStorage.PreviewTrialProcessingFee;}, function (newVal, oldVal) { 
            $scope.newTrialProcessingFee = newVal;
            if($localStorage.PreviewTrialProcessingFee) {
                    $scope.editTrialshowTotal();
                    $scope.showTrialProcessingFee = true;
            }else{ 
                    $scope.showTrialProcessingFee = false;
            }
        });
        
         //NEW TRIAL PROGRAM LENGTH QUANTITY- ADD RAJINI
        $scope.$watch(function () {return $localStorage.PreviewTrialprogramlengthquantity;}, function (newVal, oldVal) { 
            $scope.newTrialProgramQuantity = newVal;
            if( $localStorage.PreviewTrialprogramlengthquantity){ 
                $scope.editTrialshowTotal();
                $scope.showTrialProgramQuantity = true;
            }else{  
                $scope.showTrialProgramQuantity = false;
            }
        });
        
        //NEW TRIAL PROGRAM LENGTH PERIOD - ADD
        $scope.$watch(function () {return $localStorage.PreviewTrialprogramlengthperiod;}, function (newVal, oldVal) { 
            $scope.newTrialProgramPeriod = newVal;
            if( $localStorage.PreviewTrialprogramlengthperiod){ 
                $scope.editTrialshowTotal();
                $scope.showTrialProgramPeriod = true;
            }else{  
                $scope.showTrialProgramPeriod = false;
            }
        });
        
          //TRIAL TOP TAB BAR MENU SELECTION CHANGE
        $scope.$watch(function () {return $localStorage.currenttrialpreviewtab;}, function (newVal, oldVal) {
            if ($localStorage.currenttrialpreviewtab === 'trialdetails') {
                     $scope.indexTrialtabPreview();
            }else if ($localStorage.currenttrialpreviewtab === 'trialpayment') {
                     $scope.editTrialshowTotal();
                     $scope.editTrialpaymentPreview();
            }else if ($localStorage.currenttrialpreviewtab === 'trialregistration' || $localStorage.currenttrialpreviewtab === 'trialwaiver' || $localStorage.currenttrialpreviewtab === 'trialleadsource') {
                    $scope.editTrialregistrationPreview();
            }            
        });
        
         // TRIAL REGISTRATIONS
        $scope.$watch(function () {return $localStorage.PreviewTrialRegcols;}, function (newVal, oldVal) {
            if( $localStorage.PreviewTrialRegcols) {              
                $scope.edit_trial_reg_col_names = newVal; 
            }
        });
        
         // TRIAL NEW REGISTRATION
        $scope.$watch(function () {return $localStorage.PreviewNewTrialRegistrationField;}, function (newVal, oldVal) {            
             $scope.newTrialNewRegText = newVal;
          if($localStorage.PreviewNewTrialRegistrationField) {
                    $scope.editTrialregistrationPreview();
                    $scope.showTrialNewReg = true;
            }else{ 
                    $scope.showTrialNewReg = false;
            }
        });
        
         // TRIAL NEW MANDATORY CHECK
        $scope.$watch(function () {return $localStorage.PreviewNewTrialRegFieldMandatory;}, function (newVal, oldVal) {
           
             $scope.newTrialNewRegMandate = newVal;
          if($localStorage.PreviewNewTrialRegFieldMandatory) {
                    $scope.editTrialregistrationPreview();
                    $scope.showTrialNewRegMandate = true;
            }else{ 
                    $scope.showTrialNewRegMandate = false;
            }
             
        });
        
         //TRIAL NEW REG CANCEL
        $scope.$watch(function () {return $localStorage.PreviewNewTrialRegCancel;}, function (newVal, oldVal) {
           
          if($localStorage.PreviewNewTrialRegCancel === true ) {
                    $scope.newTrialNewRegText = '';
                    $scope.showTrialNewReg = false;
                    $scope.showTrialNewRegMandate = false;
            }
             
        });
        
         // TRIAL EXISTING REGISTRATION
        $scope.$watch(function () {return $localStorage.PreviewOldTrialRegistrationField;}, function (newVal, oldVal) {
             $scope.newTrialEditRegText = newVal;
          if($localStorage.PreviewOldTrialRegistrationField) {
                    $scope.editTrialregistrationPreview();
                    $scope.showTrialEditReg = true;
            }else{ 
                    $scope.showTrialEditReg = false;
            }
             
        });
        
         // TRIAL EXISTING MANDATORY CHECK
        $scope.$watch(function () {return $localStorage.PreviewOldTrialRegFieldMandatory;}, function (newVal, oldVal) {            
          $scope.newTrialEditRegMandate = newVal;
          if($localStorage.PreviewOldTrialRegFieldMandatory) {
                    $scope.editregistrationeventtabPreview();
                    $scope.showTrialOldRegMandate = true;
            }else{ 
                    $scope.showTrialOldRegMandate = false;
            }
             
        });
        
        // TRIAL EDIT REGISTRATION
        $scope.$watch(function () {return $localStorage.preview_Trial_reg_col_name_edit;}, function (newVal, oldVal) {
             $scope.newTrialEditRegText = newVal;
            if($localStorage.preview_reg_col_name_edit) {
                    $scope.editTrialregistrationPreview();
                    $scope.showTrialEditReg = true;
                    if( $localStorage.PreviewOldTrialRegFieldMandatory === true){
                      $scope.showTrialOldRegMandate = true;  
                    }else{
                        $scope.showTrialOldRegMandate = false;
                    }
            }else{ 
                    $scope.showTrialEditReg = false;
            }
             
        });
        
         // TRIAL EXISTING REG CANCEL
        $scope.$watch(function () {return $localStorage.PreviewOldTrialRegCancel;}, function (newVal, oldVal) {
          if($localStorage.PreviewOldTrialRegCancel === true ) {
                    $scope.newTrialEditRegText = $localStorage.preview_Trial_reg_col_name_edit;
                    if($localStorage.PreviewOldTrialRegFieldMandatory){
                      $scope.showTrialOldRegMandate = true;  
                    }else{
                      $scope.showTrialOldRegMandate = false;
                    }
                    $scope.showTrialOldReg = false;
            }
             
        });
        
          // TRIAL REGISTRATION FIELD INDEX EDIT
        $scope.$watch(function () {return $localStorage.preview_Trial_reg_fieldindex;}, function (newVal, oldVal) {
            if( $localStorage.preview_Trial_reg_fieldindex) {
                $scope.editTrialregistrationPreview();                 
                $scope.newTrialRegFieldIndex = newVal; 
            }
        });
         
        
         // TRIAL LEAD SOURCE EDIT
        $scope.$watch(function () {return $localStorage.PreviewTrialLeadSource;}, function (newVal, oldVal) {
            if( $localStorage.PreviewTrialLeadSource) {              
                $scope.edit_trial_lead_columns = newVal; 
            }
        });
        
       
        // TRIAL WAIVER
       $scope.$watch(function () {return $localStorage.PreviewTrialWaiver;}, function (newVal, oldVal) {
           $scope.newTrialWaiverText = newVal;
           if($localStorage.PreviewTrialWaiver) {              
                   $scope.showTrialWaiver = true;
                   var elem = document.getElementById('edittrialregscrolldiv');
                   elem.scrollTop = elem.scrollHeight;
           }else{ 
                   $scope.showTrialWaiver = false;
           }
       });
       
       //TRIAL SORTING DRAG AND DROP
        $scope.$watch(function () {   return $localStorage.trialSorted; }, function (newVal, oldVal) {
            if ($localStorage.trialSorted === "Y") {
                $scope.companydetails();
                f7.showTab('#trial-view');
                $scope.showTrialAddEdit = true;
                $scope.preloaderVisible = false;
            }
        });
        
        
//        Muthulakshmi watch function for retail
        // NEW RETAIL NAME - ADD
        $scope.indexRetailtabPreview = function(){
           var retailView = f7.addView('.retail-main', {
                domCache: true,  dynamicNavbar: true
                });
                // Load page:
                var currentipage = retailView.activePage.name;
                f7.showTab('#Retail-view');
                $scope.preloaderVisible = $scope.retailtemplateview = false;
                if(currentipage !== 'retail_listing'){
                retailView.router.loadPage({
                   pageName: 'retail_listing'
                });   
            }
       };
       $scope.editRetaildescriptionPreview = function(){
           var retailView = f7.addView('.retail-main', {
                domCache: true,  dynamicNavbar: true
                });
                // Load page:
                var currentipage = retailView.activePage.name;
                f7.showTab('#Retail-view');
                $scope.preloaderVisible = $scope.retailtemplateview = false;
                if(currentipage !== 'editretaildescription'){
                retailView.router.loadPage({
                   pageName: 'editretaildescription'
                });
            }
        };
        $scope.editRetailcheckoutPreview = function(){
           var retailView = f7.addView('.retail-main', {
                domCache: true,  dynamicNavbar: true
                });
                // Load page:
                var currentipage = retailView.activePage.name;
                f7.showTab('#Retail-view');
                $scope.preloaderVisible = $scope.retailtemplateview = false;
                if(currentipage !== 'editretailcheckout'){
                retailView.router.loadPage({
                   pageName: 'editretailcheckout'
                });
            }
        };
        $scope.openEditRetailDescription = function(){
           $scope.editRetaildescriptionPreview();
           $scope.editRetTotalAmount($scope.newRetailvariantflag,$scope.newRetailprice,$scope.newRetailtax,$scope.edit_retail_prod_quantity,$scope.edit_variant_selection); 
        }
        //NEW RETAIL STATUS ENABLED  
        $scope.$watch(function () {return $localStorage.retailenabledstatus;}, function (newVal, oldVal) {
                $scope.retailenabledstatus = newVal;
        });
        //NEW RETAIL NAME IMAGE  - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailName;}, function (newVal, oldVal) {
                $scope.newRetailText = newVal;
            if( $localStorage.PreviewRetailName) {
                $scope.showRetailname = true;
                $scope.indexRetailtabPreview();
            }else{ 
                $scope.showRetailname = false;
            }
        });
        //NEW RETAIL IMAGE - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailImage;}, function (newVal, oldVal) { 
            $scope.newRetailImage = newVal;
        });
        // NEW RETAIL PRICE - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailPrice;}, function (newVal, oldVal) {
            $scope.newRetailprice = newVal;
            if( $localStorage.PreviewRetailPrice) {
                $scope.showRetailprice = true;
                $scope.editRetTotalAmount($scope.newRetailvariantflag,$scope.newRetailprice,$scope.newRetailtax,$scope.edit_retail_prod_quantity,$scope.edit_variant_selection);
            }else{
                $scope.showRetailprice = false;
            }
        });
        // NEW RETAIL COMPARE PRICE - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailComparePrice;}, function (newVal, oldVal) {
            $scope.newRetailcompareprice = newVal;
            if( $localStorage.PreviewRetailComparePrice) {
                if($localStorage.PreviewRetailComparePrice <= 0){
                    $scope.showRetailcompareprice = false;
                }else{
                    $scope.showRetailcompareprice = true;
                }
            }else{
                $scope.showRetailcompareprice = false;
            }
        });
         // NEW RETAIL DESCRIPTION - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailDesc;}, function (newVal, oldVal) {
            $scope.newRetaildesc = newVal;
            if( $localStorage.PreviewRetailDesc) {
                $scope.editRetTotalAmount($scope.newRetailvariantflag,$scope.newRetailprice,$scope.newRetailtax,$scope.edit_retail_prod_quantity,$scope.edit_variant_selection);
                $scope.editRetaildescriptionPreview();
            }
        });
        // NEW RETAIL VARIANT - ADD
        $scope.$watch(function () { return $localStorage.PreviewRetailVariantFlag;}, function (newVal, oldVal) {
            $scope.newRetailvariantflag = newVal;
            if( $localStorage.PreviewRetailVariantFlag) {
                $scope.editRetTotalAmount($scope.newRetailvariantflag,$scope.newRetailprice,$scope.newRetailtax,$scope.edit_retail_prod_quantity,$scope.edit_variant_selection);
            }
            if($scope.newRetailvariantflag === 'N'){
//                $scope.edit_retail_vaiants_array = '';
            }else{
                $scope.edit_ret_product_price = 0;
            }
        });
        // NEW RETAIL TAX - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailTax;}, function (newVal, oldVal) {
            $scope.newRetailtax = newVal;
            if( $localStorage.PreviewRetailTax) {
                $scope.editRetTotalAmount($scope.newRetailvariantflag,$scope.newRetailprice,$scope.newRetailtax,$scope.edit_retail_prod_quantity,$scope.edit_variant_selection);
            }
        });
        //NEW RETAIL VARIANTS - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailVariants;}, function (newVal, oldVal) {
            $scope.edit_retail_vaiants_array = newVal; 
            if( $localStorage.PreviewRetailVariants && parseInt($scope.edit_retail_vaiants_array.length)>0) {
                $scope.edit_variant_selection = $scope.edit_retail_vaiants_array[0].variant_price;
                $scope.editRetTotalAmount($scope.newRetailvariantflag,$scope.newRetailprice,$scope.newRetailtax,$scope.edit_retail_prod_quantity,$scope.edit_variant_selection);
            }
        });
         //NEW RETAIL VARIANTS - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailVariantsprice;}, function (newVal, oldVal) {
            $scope.newRetailvariantprice = newVal; 
        });
        //NEW RETAIL CATEGORY NAME - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailCatName;}, function (newVal, oldVal) {
                $scope.newRetailCatText = newVal;
            if( $localStorage.PreviewRetailCatName) {
                $scope.showRetailCatname = true;
                $scope.indexRetailtabPreview();
            }else{ 
                $scope.showRetailCatname = false;
            }
        });
        //NEW RETAIL CATEGORY SUBTITLE - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailCatSubname;}, function (newVal, oldVal) { 
            $scope.newRetailCatSubtext = newVal;
        });
        //NEW RETAIL CATEGORY IMAGE - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailCatImage;}, function (newVal, oldVal) { 
            $scope.newRetailCatImage = newVal;
        });
        //NEW RETAIL SALES AGREEMENT - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailSalesAgreement;}, function (newVal, oldVal) { 
            $scope.newRetailSalesAgreement = newVal;
        });
        //RETAIL TEMPLATE
        $scope.$watch(function () {return $localStorage.retailTemplate;}, function (newVal, oldVal) { 
            $scope.retailtemplateview = true;
            $scope.preloaderVisible = false;
            $scope.retailListContent = newVal;
        });
        //NEW RETAIL SALES AGREEMENT - ADD
        $scope.$watch(function () {return $localStorage.PreviewRetailVariantName;}, function (newVal, oldVal) { 
            $scope.newRetailVariantName = newVal;
        });
        //RETAIL SORTING DRAG AND DROP
        $scope.$watch(function () {   return $localStorage.retailSorted; }, function (newVal, oldVal) {
            if ($localStorage.retailSorted === "Y") {
                $scope.companydetails();
                f7.showTab('#Retail-view');
                $scope.preloaderVisible = false;
            }
        });
        //RETAIL SORTING DRAG AND DROP
        $scope.$watch(function () {   return $localStorage.retailVariantSorted; }, function (newVal, oldVal) {
            if ($localStorage.retailVariantSorted === "Y") {
                $scope.companydetails();
                f7.showTab('#Retail-view');
                $scope.preloaderVisible = false;
            }
        });
          //RETAIL TOP TAB BAR MENU SELECTION CHANGE
        $scope.$watch(function () {return $localStorage.currentretailpreviewtab;}, function (newVal, oldVal) {
            if ($localStorage.currentretailpreviewtab === 'retaildetails') {
                $scope.indexRetailtabPreview();
            }else if ($localStorage.currentretailpreviewtab === 'retailvariants') {
                $scope.editRetaildescriptionPreview();
            }else if($localStorage.currentretailpreviewtab ==='retailsettings'){
                $scope.newRetailSalesAgreement = $localStorage.PreviewRetailSalesAgreement;
                $scope.editRetailcheckoutPreview();
            }else if($localStorage.currentretailpreviewtab === ''){
                $scope.companydetails();
            }
        });
        $scope.catcheditretailvariant = function(variantprice){
            $scope.edit_variant_selection = variantprice;
            $scope.editRetTotalAmount($scope.newRetailvariantflag,$scope.newRetailprice,$scope.newRetailtax,$scope.edit_retail_prod_quantity,$scope.edit_variant_selection);
        }
        $scope.editRetTotalAmount = function(var_flag,prodprice,taxrate,qty,variant){
//            alert("var_flag :" +var_flag + "price of product" + prodprice +"tax of product" +taxrate + "variantprice" +variant)
            if(!qty || qty < 0){
                qty = 1;
            }
            if(!taxrate || taxrate < 0){
                taxrate = 0;
            }
            if(!prodprice || prodprice < 0){
                prodprice = 0;
            }
            var retailprice;
            $scope.edit_ret_product_price = 0;
            if(var_flag === 'Y'){
                if($scope.edit_retail_vaiants_array.length > 0){ 
                    retailprice = variant;
                }else{
                    retailprice = 0;
                }
            } else{
                retailprice = prodprice;
            }  
            var taxvalue = parseFloat(retailprice) * parseInt(qty) * 0.01 * parseFloat(taxrate);
            $scope.edit_ret_product_price = (parseFloat(retailprice) * parseInt(qty)) + parseFloat(taxvalue);
        };
        $scope.forwardretailcatogoryview = function(){
            if($scope.showRetailname){
                $scope.editRetaildescriptionPreview();
                $scope.editRetTotalAmount($scope.newRetailvariantflag,$scope.newRetailprice,$scope.newRetailtax,$scope.edit_retail_prod_quantity,$scope.edit_variant_selection);  
            }else if($scope.showRetailCatname){
                $scope.openretailcategoryview();   
            }
        }
       // End of Retail watch function
      
        //CURRICULUM NAME
        $scope.$watch(function () { return $localStorage.PreviewCurriculumDName;}, function (newVal, oldVal) {
            $scope.newDCurrNameText = newVal;
            $scope.showDCurrName = true;
            if ($scope.newDCurrNameText == "") {
                $scope.showDCurrName = false;
            }
        });
        //CURRICULUM DESC
        $scope.$watch(function () {return $localStorage.PreviewCurriculumDDesc; }, function (newVal, oldVal) {
            $scope.showDCurrDesc = true;
            $scope.newDCurrDescText = newVal;
        });
        //CURRICULUM URL
        $scope.$watch(function () {return $localStorage.PreviewCurriculumDUrl;}, function (newVal, oldVal) {

            $scope.newDCurrurlText = newVal;            
            if ($scope.newDCurrurlText && $scope.newDCurrurlText.length >= 1) {
                $scope.showDCurrurl = true;
            } else {
                $scope.showDCurrurl = false;
            }
        });
        
        //LOGO IMAGE CHANGE

        $scope.$watch(function () { return $localStorage.previewDLogoScreen; }, function (newVal, oldVal) {
            $scope.logoUrl = newVal;
            if ($scope.logoUrl === "") {
                $scope.showDLogoImage = false;
            } else {
                $scope.showDLogoImage = true;
            }
        });
        //MENU STUDIO WEB PAGE
        $scope.$watch(function () {return $localStorage.previewMenuWebPage; }, function (newVal, oldVal) {
            if ($localStorage.previewMenuWebPage == 'true') {
                $scope.showDStudioWebPage = true;
            } else {
                $scope.showDStudioWebPage = false;
            }
        });
        //MENU STUDIO SCHEDULE
        $scope.$watch(function () {return $localStorage.previewMenuschedulefile; }, function (newVal, oldVal) {
            if ($localStorage.previewMenuschedulefile == 'true') {
                $scope.showDStudioSchedule = true;
            } else {
                $scope.showDStudioSchedule = false;
            }
        });
        
        //MENU STUDIO DIRECTION
        $scope.$watch(function () {return $localStorage.previewMenuDirection; }, function (newVal, oldVal) {
            if ($localStorage.previewMenuDirection == 'true') {
                $scope.showDStudioDirection = true;
            } else {
                $scope.showDStudioDirection = false;
            }
        });
        //MENU STUDIO PHONE
        $scope.$watch(function () {return $localStorage.previewMenuPhone; }, function (newVal, oldVal) {
            if ($localStorage.previewMenuPhone == 'true') {
                $scope.showDStudioPhone = true;
            } else {
                $scope.showDStudioPhone = false;
            }
        });
        //MENU STUDIO EMAIL
        $scope.$watch(function () {return $localStorage.previewMenuEmail; }, function (newVal, oldVal) {
            if ($localStorage.previewMenuEmail == 'true') {
                $scope.showDStudioEmail = true;
            } else {
                $scope.showDStudioEmail = false;
            }
        });
        
         //Referral Message change
        $scope.$watch(function () {return $localStorage.PreviewReferralMsg;}, function (newVal, oldVal) {
            $scope.referral_companymsg = newVal;
        });
        
        //MENU STUDIO Google
        $scope.$watch(function () {return $localStorage.previewMenuGoogle;  }, function (newVal, oldVal) {
            if ($localStorage.previewMenuGoogle == 'true') {
                $scope.showDStudioGoogle = true;
            } else {
                $scope.showDStudioGoogle = false;
            }
        });
        
        
        //MENU STUDIO Yelp
        $scope.$watch(function () {return $localStorage.previewMenuYelp;  }, function (newVal, oldVal) {
            if ($localStorage.previewMenuYelp == 'true') {
                $scope.showDStudioYelp = true;
            } else {
                $scope.showDStudioYelp = false;
            }
        });
        //MENU STUDIO FB
        $scope.$watch(function () {return $localStorage.previewMenuFBID;  }, function (newVal, oldVal) {
            if ($localStorage.previewMenuFBID == 'true') {
                $scope.showDStudioFB = true;
            } else {
                $scope.showDStudioFB = false;
            }
        });
        //MENU STUDIO TWITTER
        $scope.$watch(function () {return $localStorage.previewMenuTweet;  }, function (newVal, oldVal) {
            if ($localStorage.previewMenuTweet == 'true') {
                $scope.showDStudioTW = true;
            } else {
                $scope.showDStudioTW = false;
            }
        });
        //MENU STUDIO INSTAGRAM
        $scope.$watch(function () { return $localStorage.previewMenuInst;  }, function (newVal, oldVal) {
            if ($localStorage.previewMenuInst == 'true') {
                $scope.showDStudioINST = true;
            } else {
                $scope.showDStudioINST = false;
            }
        });
        //MENU STUDIO VIMEO
        $scope.$watch(function () {  return $localStorage.previewMenuVimeo;   }, function (newVal, oldVal) {
            if ($localStorage.previewMenuVimeo == 'true') {
                $scope.showDStudioVimeo = true;
            } else {
                $scope.showDStudioVimeo = false;
            }
        });
        //MENU STUDIO YOUTUBE
        $scope.$watch(function () {   return $localStorage.previewMenuYou; }, function (newVal, oldVal) {
            if ($localStorage.previewMenuYou == 'true') {
                $scope.showDStudioYTube = true;
            } else {
                $scope.showDStudioYTube = false;
            }
        });
        
        //LEFT MENU OPEN
        $scope.$watch(function () {   return $localStorage.currentpage; }, function (newVal, oldVal) {
            if ($localStorage.currentpage === "leftmenuedit") {
                $scope.companydetails();
            }
        });
        
        
        //LEFT MENU MEMBERSHIP TEXT
        $scope.$watch(function () {   return $localStorage.PreviewDownMembership; }, function (newVal, oldVal) {
            if ($localStorage.PreviewDownMembership) {
                $scope.membershipnname = $localStorage.PreviewDownMembership;
                $scope.showDStudioMembership = true;
            } else {
                $scope.showDStudioMembership = false;
            }
        });
        
        //LEFT MENU TRIAL TEXT
        $scope.$watch(function () {   return $localStorage.PreviewDownTrial; }, function (newVal, oldVal) {
            if ($localStorage.PreviewDownTrial) {
                $scope.trialnname = $localStorage.PreviewDownTrial;
                $scope.showDStudioTrial = true;
            } else {
                $scope.showDStudioTrial = false;
            }
        });
        
        //EDIT MESSAGE INDEX
        $scope.$watch(function () {  return $localStorage.previewEditMessageIndex; }, function (newVal, oldVal) {
            if ($localStorage.previewEditMessageIndex) {
                $scope.showDMsgView = true;
                $scope.newDMsgText = $scope.msgcontent[newVal].message_text;
                $scope.newDMsgUrlText = $scope.msgcontent[newVal].message_link;
                $scope.editDeliveryDate = $scope.msgcontent[newVal].push_delivered_date;
                if ($scope.newDMsgUrlText.length >= 1) {
                    $scope.showDMsgUrlView = true;
                } else {
                    $scope.showDMsgUrlView = false;
                }
            } else {
                $scope.editDeliveryDate = "";
            }
        });
        
        
        //BOTTOM EVENT TEXT
        $scope.$watch(function () {   return $localStorage.PreviewDownEvent; }, function (newVal, oldVal) {
            if ($localStorage.PreviewDownEvent) {
                $scope.eventnname = $localStorage.PreviewDownEvent;
            } else {
                
            }
        });
        //BOTTOM MESSAGE TEXT
        $scope.$watch(function () {   return $localStorage.PreviewDownMessage; }, function (newVal, oldVal) {
            if ($localStorage.PreviewDownMessage) {
                $scope.messagenname = $localStorage.PreviewDownMessage;
            } else {
                
            }
        });
        //BOTTOM CURRICULUM TEXT
        $scope.$watch(function () {   return $localStorage.PreviewDownCurriculum; }, function (newVal, oldVal) {
            if ($localStorage.PreviewDownCurriculum) {
                $scope.curriculumnname = $localStorage.PreviewDownCurriculum;
            } else {
                
            }
        });

        //EDIT CURRICULUM NAME INDEX
        $scope.$watch(function () {   return $localStorage.previewCirName; }, function (newVal, oldVal) {
            if($scope.curriculumDetail && parseInt($localStorage.cirind) >= 0){
                $scope.curriculumDetail[$localStorage.cirind].content_title = $localStorage.previewCirName;
            }
        });

        //EDIT CURRICULUM DESC INDEX
        $scope.$watch(function () {  return $localStorage.previewCirDesc;   }, function (newVal, oldVal) {
            if($scope.curriculumDetail && parseInt($localStorage.ccirind) >= 0){
                $scope.curriculumDetail[$localStorage.ccirind].content_description = $localStorage.previewCirDesc;               
            }
        });

        $scope.valuess = $localStorage.valuei;
        //Internet show alert function
        $scope.istatus = function () {
            f7.alert('Please turn on internet', 'No Internet');
            return;
        };

//           MEMBERSHIPS PAGE REDIRECTION
        $scope.openmems = function(){
            f7.showTab('#memberships-view');
            f7.closePanel();
        };
        
        $scope.getStudioSocialDetails = function(){
            if($scope.cmpny_website){
                 $scope.showDStudioWebPage = true;
            }else{
                 $scope.showDStudioWebPage = false;
            }            
            if($scope.logoUrl){
                 $scope.showDLogoImage = true;
            }else{
                 $scope.showDLogoImage = false;
            }
            if($scope.class_schedule){
                 $scope.showDStudioSchedule = true;
            }else{
                 $scope.showDStudioSchedule = false;
            }
            if($scope.cmpny_address){
                 $scope.showDStudioDirection = true;
            }else{
                 $scope.showDStudioDirection = false;
            }
            if($scope.cmpny_phone){
                 $scope.showDStudioPhone = true;
            }else{
                 $scope.showDStudioPhone = false;
            }
            if($scope.cmpny_email){
                 $scope.showDStudioEmail = true;
            }else{
                 $scope.showDStudioEmail = false;
            }
            if($scope.google){
                 $scope.showDStudioGoogle = true;
            }else{
                 $scope.showDStudioGoogle = false;
            }
            if($scope.yelp){
                 $scope.showDStudioYelp = true;
            }else{
                 $scope.showDStudioYelp = false;
            }
            if($scope.facebook){
                 $scope.showDStudioFB = true;
            }else{
                 $scope.showDStudioFB = false;
            }
            if($scope.twitter){
                 $scope.showDStudioTW = true;
            }else{
                 $scope.showDStudioTW = false;
            }
            if($scope.insta){
                 $scope.showDStudioINST = true;
            }else{
                 $scope.showDStudioINST = false;
            }
            if($scope.vimeo){
                 $scope.showDStudioVimeo = true;
            }else{
                 $scope.showDStudioVimeo = false;
            }
            if($scope.youtube){
                 $scope.showDStudioYTube = true;
            }else{
                 $scope.showDStudioYTube = false;
            }
            if($scope.trialnname){
                 $scope.showDStudioTrial = true;
            }else{
                 $scope.showDStudioTrial = false;
            }
            if($scope.membershipnname){
                 $scope.showDStudioMembership = true;
            }else{
                 $scope.showDStudioMembership = false;
            }
        };

        //Get Company Details
        $scope.companydetails = function ()
        {
            $scope.preloaderVisible = true;
            $http({
                method: 'POST',
                url: urlservice.url + 'getCompanyInfo',
                data: {
                     "company_id": $localStorage.company_id,
                     "user_id": $localStorage.user_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            })
                    .then(function (response) {
                        if (response.data.status == 'Success') {
                            $localStorage.companydetails = response.data.msg;
                            $scope.referral_companymsg = $localStorage.companydetails.referral_message;
                            $scope.google = $localStorage.companydetails.google_url;
                            $scope.yelp = $localStorage.companydetails.yelp_url;
                            $scope.class_schedule = $localStorage.companydetails.class_schedule;                            
                            $scope.cmpny_website = $localStorage.companydetails.web_page;
                            $scope.cmpny_phone = $localStorage.companydetails.phone_number;
                            $scope.cmpny_email = $localStorage.companydetails.email_id;
                            $scope.cmpny_address = $localStorage.companydetails.street_address + " " + $localStorage.companydetails.city + " " + $localStorage.companydetails.state + " " + $localStorage.companydetails.postal_code + " " + $localStorage.companydetails.country;
                            $scope.facebook = $localStorage.companydetails.facebook_url;
                            $scope.twitter = $localStorage.companydetails.twitter_url;
                            $scope.insta = $localStorage.companydetails.instagram_url;
                            $scope.vimeo = $localStorage.companydetails.vimeo_url;
                            $scope.youtube = $localStorage.companydetails.youtube_url;
                            $scope.eventbrite = $localStorage.companydetails.eventbrite_url;

                            $scope.facebookid = $localStorage.companydetails.facebook_id;
                            $scope.twitterid = $localStorage.companydetails.twitter_id;
                            $scope.instaid = $localStorage.companydetails.instagram_id;
                            $scope.youtubeid = $localStorage.companydetails.youtube_id;
                            $scope.vimeoid = $localStorage.companydetails.vimeo_id;
                            $scope.eventbriteid = $localStorage.companydetails.eventbrite_id;
                            $scope.loginDetails = $localStorage.companydetails;
                            $scope.logoUrl = $localStorage.logoUrl = $scope.loginDetails.logo_URL;
                            $scope.membershipnname = $localStorage.companydetails.membership_name;
                            $scope.trialnname = $localStorage.companydetails.trial_name;
                            $scope.eventnname = $localStorage.companydetails.event_name;
                            $scope.messagenname = $localStorage.companydetails.message_name;
                            $scope.curriculumnname = $localStorage.companydetails.curriculum_name;
                            if($scope.membershipnname == ''){
                                $scope.membershipnname = "Memberships";
                            }
                            if($scope.trialnname == ''){
                                $scope.trialnname = "Trial Program";
                            }
                            if($scope.eventnname == ''){
                                $scope.eventnname = "Events";
                            }
                            if($scope.messagenname == ''){
                                $scope.messagenname = "Messages";
                            }
                            if($scope.curriculumnname == ''){
                                $scope.curriculumnname = "Curriculum";
                            }
                            $scope.preloaderVisible = false;

                            if ($localStorage.currentpage == "events" || $localStorage.currentpage == "home") {
                                $scope.getEventsList('P');
                                f7.showTab('#view-1');
                            }else if ($localStorage.currentpage === "membership") {
                                $scope.getmembershipdetails();
                                f7.showTab('#memberships-view');
                            }else if ($localStorage.currentpage === "trial") {
                                $scope.gettrialdetails('P');
                                f7.showTab('#trial-view');
                            }else if ($localStorage.currentpage === "leftmenuedit") {  
                                $scope.getStudioSocialDetails();
                                f7.openPanel('left');
                            }else if($localStorage.currentpage === "retail"){
                                if($localStorage.currentretailpreviewtab === 'retailsettings'){
                                    $scope.editRetailcheckoutPreview();
                                }else{                                    
                                    $scope.getretaildetails('P','');
                                }
                                f7.showTab('#Retail-view');
                            }
                        } else {
                            $scope.preloaderVisible = false;
                            console.log(response.data);
                        }
                    }, function (response) {
                        $scope.preloaderVisible = false;
                        console.log(response.data);
                        f7.alert(response.data.status, 'Message');
                    });
        };
        
        $scope.resetMembershipcartdetails = function () {
                
                MyApp.closePanel();
                MyApp.confirm(' Discard cart items?', 'Alert', function () {
                    
                    $scope.resetForm();
                    $scope.mems_paymentform1.$setPristine();
                    $scope.mems_paymentform1.$setUntouched();
                    
                    calendarInline.setValue([new Date()]);
                    calendarInline1.setValue([new Date()]);
                    //     document.getElementById("paymentform").reset();
                    
                    $scope.preloader = false;
                    $scope.reg_col_names = [];
                    $scope.MSoptions_waiver_text = '';
                    $scope.show_in_app_flg = '';
                    $scope.show_in_app = '';
                    $scope.current_processing_fee_type ='';
                    $scope.membershipoptiontitle = '';
                    $scope.membershipoption_id = '';
                    $scope.membershipfee_DiscountAmount = 0;
                    $scope.signupfee_DiscountAmount = 0;
                    $scope.final_membership_payment_amount = 0;
                    $scope.membership_structure = '';
                    $scope.processing_fee_show = '';
                    $scope.memsignupfee = 0;
                    $scope.memfee = 0;
                    $scope.mem_rec_frequency = '';
                    $scope.prorate_first_payment = 0;
                    $scope.cus_rec_freq_period_type = '';
                    $scope.current_membership_manage = '';
                    $scope.delay_rec_payment_start_flg = '';
                    $scope.delayed_recurring_payment_type = '';
                    $scope.delayed_recurring_payment_val = 0;
                    
                });
        };
        
        //DATE TO READABLE FORMAT
        $scope.dateString = function (date) {
            if(date){
                var Displaydate = date.toString();
                Displaydate = date.split("-");
                var year = Displaydate[0];
                var month = Displaydate[1]-1;
                var date = Displaydate[2];
                var ReadDate = new Date(year, month, date);
                var datesuccess = ReadDate.toString();
                var finaldateformat = datesuccess.split(" ");
                var finalized = finaldateformat[0] + " " + finaldateformat[1] + " " + finaldateformat[2] + " " + finaldateformat[3];
                return finalized;
            }
        };
        
        $scope.addMonths = function (adate, months) {
            var ad = adate.getDate();
            adate.setMonth(adate.getMonth() + +months);
            if (adate.getDate() != ad) {
                adate.setDate(0);
            }
            return adate;
        };
        
        $scope.currentDate = function () {
            var current_date = new Date();
            return current_date;
        };
        
        $scope.safariiosdate = function (dat) {
            if(dat){
                var dda = dat.toString();
                dda = dda.replace(/-/g, "/");
                var curr_date = new Date(dda);   //  FOR DATE FORMAT IN SAFARI 

                var given_start_year = curr_date.getFullYear();
                var given_start_month = curr_date.getMonth();
                var given_start_day = curr_date.getDate();
                var given_start_month = curr_date.getMonth() + 1;
                var dda = given_start_year + "-" + given_start_month + "-" + given_start_day;
                dda = dda.toString();
                dda = dda.replace(/-/g, "/");
                var dup_date = new Date(dda);  //  FOR DATE FORMAT IN SAFARI 
                return dup_date;
            }else{
                return '';
            }
        };
        
        $scope.newSEdateFormat = function (SEdate){
            if(SEdate){
                var sdate = SEdate.split("-");
                var syear = sdate[0];
                var smonth = parseInt(sdate[1], 10) - 1; 
                var sdate = sdate[2];
                var sedate = new Date(syear, smonth, sdate);
                return sedate;
            }
        };
        
        $scope.SEProcessingFee = function (recurring_amnt) { 
            var process_fee_per = 0, process_fee_val = 0, recurring_pro_fee = 0;  
            var process_fee_per = parseFloat($scope.process_fee_per);
            var process_fee_val = parseFloat($scope.process_fee_val);
            if (parseFloat(recurring_amnt) >= 0 && parseFloat(recurring_amnt) != ''){
                if ($scope.processing_fee_show === 2 || $scope.processing_fee_show === '2') {
                    if (recurring_amnt > 0) {
                        var process_fee1 = +(parseFloat(recurring_amnt)) + +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                        if(isFinite(recurring_pro_fee)===false){
                            recurring_pro_fee = 0;
                        }
                    } else {
                        recurring_pro_fee = 0;
                    }
                } else if ($scope.processing_fee_show === 1 || $scope.processing_fee_show === '1'){
                    if (recurring_amnt > 0) {
                        var process_fee = +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                        if(isFinite(recurring_pro_fee)===false){
                            recurring_pro_fee = 0;
                        }
                    } else {
                        recurring_pro_fee = 0;
                    } 
                }
            }
            return recurring_pro_fee;
        }; 
        
        
        $scope.SEMembershipFee = function (count) { 
            var SEMem_cost = 0;
            var selected_count = ($scope.billing_days.match(/1/g) || []).length;
            if (parseFloat($scope.memfee) >= 0 && parseFloat($scope.memfee) != ''){
                if ($scope.specific_payment_frequency === 'PW' && parseFloat(count) > 0){   
                    SEMem_cost = (parseFloat($scope.memfee) / parseFloat(selected_count)) * parseFloat(count);
                } else if ($scope.specific_payment_frequency === 'PM'  && parseFloat(count) > 0){  
                    SEMem_cost = (parseFloat($scope.memfee) / (parseFloat(selected_count) * 4.345)) * parseFloat(count);
                }else{
                    SEMem_cost = 0;
                }
            }else{
                SEMem_cost = 0;
            }
            return SEMem_cost;
        };
        
           
        $scope.startandenddate = function (start, end) { 
                if(start !== undefined && start !== '' && end !== undefined && end !== ''){
                    var check_start_date = start;
                    var check_end_date = end;
                    $scope.daysarray = $scope.billing_days.split('');
                    $scope.checked_days_count = 0;
                    for (var check_date = check_start_date; check_date <= check_end_date; check_date.setDate(check_date.getDate() + 1)) {
                        $scope.day_check = check_date.getDay() - 1;
                        if ($scope.day_check === -1) {
                            $scope.day_check = 6;
                        }
                        if ($scope.daysarray[$scope.day_check] === '1') {
                            $scope.checked_days_count += 1;
                        }
                    }
                    return $scope.checked_days_count;
                }
            };
//        Muthulakshmi
//        Retail List start here
            
            $scope.edit_retail_vaiants_array = []; 
            $scope.retail_checkout_price = $scope.current_ret_product_price = 0;
            
            $scope.getretaildetails = function (list_type,parent_id){ 
                $scope.preloaderVisible = true;
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getretailmanagedetails',
                    params: {
                        "company_id": $localStorage.company_id,
                        "list_type":list_type,
                        "parent_id":parent_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',
                        },
                         withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        if (parent_id != '') {
                            var retailcategoryView = f7.addView('.retail-main', {
                                domCache: true, dynamicNavbar: true
                            });
                            retailcategoryView.router.loadPage({
                                pageName: 'retail_category'
                            });
                            f7.showTab('#Retail-view');
                            $scope.retailcategoryContent = response.data.msg.live;
                        }else{
                            var retailView = f7.addView('.retail-main', {
                                domCache: true, dynamicNavbar: true
                            });
                            retailView.router.loadPage({
                                pageName: 'retail_listing'
                            }); 
                            f7.showTab('#Retail-view');
                            $scope.retailListContent = response.data.msg.live;
                        }
                        $scope.retailenabledstatus = response.data.sales.retail_enabled; 
                        $scope.retailsalesAgreement = response.data.sales.retail_agreement_text; 
                        $scope.retailFeeType = response.data.sales.retail_processingfee; 
                        $scope.retailContentStatus = response.data.status; 
                        $scope.preloaderVisible = false;
                    } else {
                        $scope.retailListContent = $scope.retailcategoryContent = $scope.retailsalesAgreement = $scope.retailFeeType = "";
                        $scope.preloaderVisible = false;
                        $scope.retailContentStatus = response.data.status;
                    }
                }, function (response) {
                    $scope.retailListContent = $scope.retailcategoryContent = "";
                    $scope.retailContentStatus = response.data.status; 
                    $scope.preloaderVisible = false;
                    console.log(response.data);
                });
        };
        
        $scope.retailCategoryDetail = function (detail) { 
            $scope.retail_prod_quantity = '';
            $scope.preloaderVisible = true;
            $scope.current_ret_product_price = 0;
            $http({
                method: 'GET',
                url: urlservice.url + 'getIndividualRetail',
                params: {
                    "company_id": $localStorage.company_id,
                    "retail_product_id": detail.retail_product_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.preloaderVisible = false;
                    $scope.retailProductContent = response.data.msg;
                    $scope.retail_banner_img_url = $scope.retailProductContent.retail_banner_img_url;
                    $scope.retail_product_title = $scope.retailProductContent.retail_product_title;
                    $scope.retail_product_id = $scope.retailProductContent.retail_product_id;
                    $scope.retail_product_desc = $scope.retailProductContent.retail_product_desc;
                    $scope.retail_product_price = $scope.retailProductContent.retail_product_price;
                    $scope.retail_product_compare_price = $scope.retailProductContent.retail_product_compare_price;
                    $scope.retail_processing_fees_type = $scope.retailProductContent.retail_processing_fees;
                    $scope.retail_variant_flag = $scope.retailProductContent.retail_variant_flag;
                    $scope.retail_product_inventory = $scope.retailProductContent.retail_product_inventory;
                    $scope.retail_purchase_count = $scope.retailProductContent.retail_purchase_count;
                    $scope.product_tax_rate = $scope.retailProductContent.product_tax_rate;
                    $scope.retail_banner_img_url = $scope.retailProductContent.retail_banner_img_url;
                    $scope.retail_product_type = $scope.retailProductContent.retail_product_type;
                    $scope.retailProductvariant_arr = []; 
                    
                    if($scope.retail_variant_flag === 'N'){
                        var taxvalue = parseFloat($scope.retail_product_price) * 0.01 * parseFloat($scope.product_tax_rate);
                        $scope.current_ret_product_price = parseFloat($scope.retail_product_price) + parseFloat(taxvalue);
                    }
                    if($scope.retail_variant_flag === 'Y'){
                        $scope.getRetailVariant($scope.retail_product_id);
                    }
                    var retailView = f7.addView('.retail-main', {
                        domCache: true,  dynamicNavbar: true
                    });
                    retailView.router.loadPage({
                       pageName: 'retail_product'
                    });
                } else {
                    $scope.preloaderVisible = false;
                }
            }, function (response) {
                $scope.preloaderVisible = false;
                console.log(response.data);
            });
        };
        
        $scope.getRetailVariant = function (retailid){
            $scope.retailProductvariant_arr = []; 
            $http({
                method: 'GET',
                url: urlservice.url + 'getretailvariants',
                params: {
                    "company_id": $localStorage.company_id,
                    "retail_id":retailid	
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    },
                     withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $scope.retailProductvariant_arr = response.data.msg;
                    $scope.edit_retail_vaiants_array = $scope.retailProductvariant_arr;
                    $scope.edit_variant_selection = '';
                    $scope.newRetailvariantprice = (response.data.retail_variant_price) ? (response.data.retail_variant_price) :  ($scope.wp_currency_symbol+'0.0');
                        if($scope.retailProductvariant_arr.length > 0){
                            $scope.edit_variant_selection = $scope.edit_retail_vaiants_array[0].variant_price;
                            $scope.prod_variant_selection = $scope.retailProductvariant_arr[0];
                            $scope.variant_price = $scope.retailProductvariant_arr[0].variant_price;                            
                            var taxvalue = parseFloat($scope.variant_price) * 0.01 * parseFloat($scope.product_tax_rate);
                            $scope.current_ret_product_price = parseFloat($scope.variant_price) + parseFloat(taxvalue);
                        }else{
                            $scope.current_ret_product_price = 0;
                        }
                } else {
                    $scope.variant_price = $scope.current_ret_product_price = 0;
                    $scope.prod_variant_selection = "";
                    $scope.retailProductvariant_arr = [];
                    console.log(response.data);
                }
            }, function (response) {
                $scope.variant_price = $scope.current_ret_product_price = 0;
                $scope.prod_variant_selection = "";
                $scope.retailProductvariant_arr = [];
                console.log(response.data);
            });
        };
        $scope.gobackforretailview = function (){
            var retailView = f7.addView('.retail-main', {
                domCache: true,  dynamicNavbar: true
            });
            if($scope.retail_product_type === 'P'){
                retailView.router.loadPage({
                   pageName: 'retail_category'
                });
            }else{
                retailView.router.loadPage({
                   pageName: 'retail_listing'
                });
            }
        }
        $scope.carousalOpenView = function (list){
            if(list.retail_product_type === 'C'){
                $scope.getretaildetails('P',list.retail_product_id);
            }else{
                $scope.retailCategoryDetail(list);
            }
        }
        $scope.openretailcategoryview = function (){
            if($scope.newRetailCatId){
                $scope.getretaildetails('P',$scope.newRetailCatId);
            }else{
                $scope.retailcategoryContent = "";
            }
        }
          
        $scope.calculateRetTotalAmount = function(qty,variantflag,selected_variant){
            var variantid;
            $scope.retail_checkout_price = 0;
            $scope.prod_variant_selection = selected_variant;
            if(!qty || qty < 0){
                qty = 1;
            }
            if(variantflag === 'Y' && $scope.retailProductvariant_arr.length > 0){ 
                variantid = selected_variant.variant_id;
                if ($scope.retailProductvariant_arr.length > 0) {
                    for (var i = 0; i < $scope.retailProductvariant_arr.length; i++) {
                        if (variantid == $scope.retailProductvariant_arr[i].variant_id) {
                            $scope.variant_price = $scope.retailProductvariant_arr[i].variant_price;                            
                        }
                    }
                    var taxvalue = parseFloat($scope.variant_price)* parseInt(qty) * 0.01 * parseFloat($scope.product_tax_rate);
                    $scope.current_ret_product_price = (parseFloat($scope.variant_price) * parseInt(qty)) + parseFloat(taxvalue); 
                }else{
                    
                }
            } else{
                variantid = "";
                var taxvalue = parseFloat($scope.retail_product_price)* parseInt(qty) * 0.01 * parseFloat($scope.product_tax_rate);
                $scope.current_ret_product_price = (parseFloat($scope.retail_product_price) * parseInt(qty)) + parseFloat(taxvalue); 
            }           
        };
        
        //GET MEMBERSHIP LIST   
        $scope.gettrialdetails = function (list_type)
        {  
            $scope.preloaderVisible = true;
            $http({
                method: 'GET',
                url: urlservice.url + 'gettrialdetails',
                params: {
                    "company_id": $localStorage.company_id,
                    "list_type" : list_type
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    var trialView = f7.addView('.trial-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    trialView.router.loadPage({
                       pageName: 'trial-listing'
                    }); 
                    f7.showTab('#trial-view');
                    $scope.trialListContent = response.data.msg.live;
                    $scope.trialContentStatus = response.data.status; 
                    $scope.preloaderVisible = false;
                } else {
                    $scope.trialListContent = "";
                    $scope.preloaderVisible = false;
                    $scope.trialContentStatus = response.data.status;
                }
            }, function (response) {
                $scope.trialListContent = "";
                $scope.trialContentStatus = response.data.status; 
                $scope.preloaderVisible = false;
                console.log(response.data);
            });

        };
        
        $scope.trialdetail = function (ind) {    
            var process_fee_per = 0, process_fee_val = 0;
            var process_fee_per = $localStorage.companydetails.processing_fee_percentage;
            var process_fee_val = $localStorage.companydetails.processing_fee_transaction;
            $scope.trial_processing_fee_value = 0;
            $scope.trial_total_amount_view = 0;
            
            $scope.newTrialImage = $scope.trialListContent[ind].trial_banner_img_url;
            if ($scope.newTrialImage === '' || $scope.newTrialImage === undefined) {
                $scope.showTrialImage = false;
            } else {
                $scope.showTrialImage = true;
            }
            
            $scope.newTrialText = $scope.trialListContent[ind].trial_title;            
            $scope.newTrialPrgmUrl = $scope.trialListContent[ind].trial_prog_url;
            if ($scope.newTrialPrgmUrl === '' || $scope.newTrialPrgmUrl === undefined) {
                $scope.showTrialPrgmUrl = false;
            } else {
                $scope.showTrialPrgmUrl = true;
            }

            $scope.newTrialdesc = $scope.trialListContent[ind].trial_desc;
            if ($scope.newTrialdesc === '' || $scope.newTrialdesc === undefined) {
                $scope.showTrialDesc = false;
            } else {
                $scope.showTrialDesc = true;
            } 
            
            $scope.trial_reg_col_names = $scope.trialListContent[ind].reg_columns;
            $scope.trial_lead_columns = $scope.trialListContent[ind].lead_columns;
            if(!$scope.trialListContent[ind].trial_waiver_policies) {
                $scope.newTrialWaiverText = '';
                $scope.showTrialWaiver = false;
            } else {
                $scope.newTrialWaiverText = $scope.trialListContent[ind].trial_waiver_policies;
                $scope.showTrialWaiver = true;
            }    
            $scope.triallengthtype = $scope.trialListContent[ind].program_length_type;
            $scope.triallength = $scope.trialListContent[ind].program_length;
            $scope.trial_amount_view = $scope.trialListContent[ind].price_amount;
            $scope.trial_processing_fee_type = $scope.trialListContent[ind].processing_fee_type;
            
            var amount = parseFloat($scope.trial_amount_view);
            if ($scope.trial_processing_fee_type === 2 || $scope.trial_processing_fee_type === '2') {
                if (amount > 0) {
                    var process_fee1 = +amount + +((+amount * +process_fee_per) / 100) + +process_fee_val;
                    var process_fee = +((+process_fee1 * +process_fee_per) / 100) + +process_fee_val;
                    $scope.trial_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    $scope.trial_processing_fee_value = 0;
                }
                $scope.trial_total_amount_view = parseFloat($scope.trial_amount_view) + parseFloat($scope.trial_processing_fee_value);
            } else if ($scope.trial_processing_fee_type === 1 || $scope.trial_processing_fee_type === '1') {
                if (amount > 0) {
                    var process_fee = +((+amount * +process_fee_per) / 100) + +process_fee_val;
                    $scope.trial_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    $scope.trial_processing_fee_value = 0;
                }
                $scope.trial_total_amount_view = parseFloat($scope.trial_amount_view);
            }
            
        };
        
        $scope.setDefault_trialstartDate = function () {
            var curr_date = new Date();
            $scope.trial_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);
            var dupdate = $scope.safariiosdate($scope.trial_start_date);
            var trialenddatequantity, trial_end;
            if ($scope.triallengthtype == 'W') {
                trialenddatequantity = $scope.triallength * 7;   
                dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
            } else if ($scope.triallengthtype == 'D'){
                trialenddatequantity = $scope.triallength;
                dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
            }
            $scope.setTrialStartDate(); 
            $scope.trial_start_date_view = $scope.dateString($scope.trial_start_date);
            $scope.trial_end_date_view = $scope.dateString(trial_end);

            var memberView = f7.addView('.trial-main', {
                domCache: true, dynamicNavbar: true
            });
            memberView.router.loadPage({
                pageName: 'trialdateselect'
            });
        };
        
        
        $scope.showMembershipTotal = function () {
            $scope.signupfee_DiscountAmount = $scope.membershipfee_DiscountAmount = 0;
            $scope.total_amount_view = $scope.total_reg_amount_view = 0;
            $scope.total_membership_amount_view = 0;
            $scope.total_mem_amount_view = 0;
            $scope.total_mem_amount_withfee_view = 0;
            $scope.total_signup_amount_view = 0;
            $scope.processing_fee_value = 0;
            $scope.recurring_processing_fee_value = 0;
            $scope.final_membership_payment_amount = $scope.pay_infull_processingfee = 0;
            $scope.register_signup_discount_value = $scope.register_membership_discount_value = 0;
            $scope.recurring_payment_startdate_view = $scope.total_payment_startdate_view = '';
            $scope.membership_discount_show = $scope.signup_discount_show = true;
            $scope.zerocostview = true;

            var process_fee_per = 0, process_fee_val = 0;
            var process_fee_per = $localStorage.companydetails.processing_fee_percentage;
            var process_fee_val = $localStorage.companydetails.processing_fee_transaction;

            if (parseFloat($scope.current_membership_manage.membership_signup_fee) > 0) {
                $scope.actual_signup_cost = parseFloat($scope.current_membership_manage.membership_signup_fee);
                $scope.register_signup_cost = $scope.actual_signup_cost;
            } else {
                $scope.actual_signup_cost = 0;
                $scope.register_signup_cost = 0;
                $scope.signup_discount_show = false;
            }

            if (parseFloat($scope.current_membership_manage.membership_fee) > 0) {
                $scope.actual_membership_cost = parseFloat($scope.current_membership_manage.membership_fee);
                $scope.register_membership_cost = $scope.actual_membership_cost;
            } else {
                $scope.actual_membership_cost = 0;
                $scope.register_membership_cost = 0;
                $scope.membership_discount_show = false;
            }

            $scope.current_processing_fee_type = $scope.current_membership_manage.membership_processing_fee_type;
            if ($scope.signupfee_DiscountAmount > 0) {
                $scope.total_signup_amount_view = (Math.round((+$scope.register_signup_cost - +$scope.signupfee_DiscountAmount + +0.00001) * 100) / 100).toFixed(2);
                $scope.register_signup_discount_value = $scope.signupfee_DiscountAmount;
            } else {
                $scope.total_signup_amount_view = (Math.round((+$scope.register_signup_cost + +0.00001) * 100) / 100).toFixed(2);
                $scope.register_signup_discount_value = 0;
            }

            if ($scope.membershipfee_DiscountAmount > 0) {
                $scope.total_membership_amount_view = +$scope.register_membership_cost - +$scope.membershipfee_DiscountAmount;
                $scope.register_membership_discount_value = $scope.membershipfee_DiscountAmount;
            } else {
                $scope.total_membership_amount_view = +$scope.register_membership_cost;
                $scope.register_membership_discount_value = 0;
            }

            $scope.total_mem_amount_view = $scope.total_membership_amount_view;
            var amount = $scope.total_mem_amount_view;
            if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                if (amount > 0) {
                    var process_fee1 = +amount + +((+amount * +process_fee_per) / 100) + +process_fee_val;
                    var process_fee = +((+process_fee1 * +process_fee_per) / 100) + +process_fee_val;
                    $scope.recurring_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    $scope.recurring_processing_fee_value = 0;
                }
            } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1') {
                if (amount > 0) {
                    var process_fee = +((+amount * +process_fee_per) / 100) + +process_fee_val;
                    $scope.recurring_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    $scope.recurring_processing_fee_value = 0;
                }
            }
            
            if($scope.membership_structure === 'OE'){   
            var dup_date = $scope.safariiosdate($scope.total_payment_startdate);
            var act_date = $scope.safariiosdate($scope.actual_total_payment_startdate);
            $scope.prorate_includes_recurr_note = false;
            $scope.first_payment_status = false;
            if ($scope.prorate_first_payment === 'Y' && $scope.mem_rec_frequency === 'M') {
                   
                    if($scope.delay_rec_payment_start_flg === 'Y'){
                        var ld = $scope.safariiosdate($scope.total_payment_startdate); 
                        var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                        $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / lastDay.getDate()) * (+lastDay.getDate() + 1 - ld.getDate());
                        $scope.total_membership_amount_view = 0;
                        $scope.total_reg_amt_for_check = +$scope.delay_membership_amount_view;
                        $scope.first_payment_amount = 0;
                    }else{
                        var ld = $scope.safariiosdate($scope.actual_total_payment_startdate);
                        var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                        $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / lastDay.getDate()) * (+lastDay.getDate() + 1 - ld.getDate()); 
                        if ($scope.mem_fee_include_status === 'N'){
                            $scope.first_payment_status = true;
                            $scope.first_payment_amount = parseFloat($scope.total_membership_amount_view);
                            $scope.first_payment_startdate = $scope.dateString($scope.actual_total_payment_startdate);
                        }else{
                            $scope.first_payment_status = false;
                            $scope.first_payment_amount = 0;
                            $scope.first_payment_startdate = '';
                        }
                        $scope.total_reg_amt_for_check = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                        if ($scope.total_reg_amt_for_check < 5) {
                            $scope.prorate_includes_recurr_note = true;
                            $scope.total_membership_amount_view = +$scope.total_membership_amount_view + +$scope.total_mem_amount_view;
                        }
                    }                     
                    dup_date = $scope.addMonths(dup_date, 1);
                    var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    
                } else if ($scope.prorate_first_payment === 'Y' && $scope.mem_rec_frequency === 'B') {
                    if($scope.delay_rec_payment_start_flg === 'Y'){                          
                        if (dup_date.getDate() < 16) {
                            $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / 15) * (+16 - act_date.getDate()); 
                            $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16';
                        } else {   
                            var ld = $scope.safariiosdate($scope.total_payment_startdate);
                            var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                            if (dup_date.getDate() == 16)
                                $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view));
                            else
                                $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / (lastDay.getDate() - 15)) * (+lastDay.getDate() + 1 - act_date.getDate());
                            
                            dup_date = $scope.addMonths(dup_date, 1);
                            var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                            $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }
                        $scope.total_membership_amount_view = 0;
                        $scope.first_payment_amount = 0;
                    }else{                         
                        if (act_date.getDate() < 16) {
                            $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / 15) * (+16 - act_date.getDate());
                            if ($scope.mem_fee_include_status === 'N'){
                                $scope.first_payment_status = true;
                                $scope.first_payment_amount = parseFloat($scope.total_membership_amount_view);
                                $scope.first_payment_startdate = $scope.dateString($scope.actual_total_payment_startdate);
                            }else{
                                $scope.first_payment_status = false;
                                $scope.first_payment_amount = 0;
                                $scope.first_payment_startdate = '';
                            }
                        } else {
                            var ld = $scope.safariiosdate($scope.actual_total_payment_startdate);
                            var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                            if (dup_date.getDate() == 16)
                                $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view));
                            else
                                $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / (lastDay.getDate() - 15)) * (+lastDay.getDate() + 1 - act_date.getDate());
                            if ($scope.mem_fee_include_status === 'N'){
                                $scope.first_payment_status = true;
                                $scope.first_payment_amount = parseFloat($scope.total_membership_amount_view);
                                $scope.first_payment_startdate = $scope.dateString($scope.actual_total_payment_startdate);
                            }else{
                                $scope.first_payment_status = false;
                                $scope.first_payment_amount = 0;
                                $scope.first_payment_startdate = '';
                            }
                        }
                        
                        if (dup_date.getDate() < 16) {
                            $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16';

                            $scope.total_reg_amt_for_check = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                            if ($scope.total_reg_amt_for_check < 5) {
                                $scope.prorate_includes_recurr_note = true;
                                $scope.total_membership_amount_view = +$scope.total_membership_amount_view + +$scope.total_mem_amount_view;
                                dup_date = $scope.addMonths(dup_date, 1);
                                var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                            }
                        } else {

                            dup_date = $scope.addMonths(dup_date, 1);
                            var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                            $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);

                            $scope.total_reg_amt_for_check = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                            if ($scope.total_reg_amt_for_check < 5) {
                                $scope.prorate_includes_recurr_note = true;
                                $scope.total_membership_amount_view = +$scope.total_membership_amount_view + +$scope.total_mem_amount_view;
                                $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16';
                            }
                        }
                    }
                }
                
                var cus_curr_date = new Date();
                var cus_current_date = cus_curr_date.getFullYear() + '-' + ('0' + (cus_curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + cus_curr_date.getDate()).slice( - 2);
                if ($scope.mem_rec_frequency === 'A') {
                    dup_date.setFullYear(dup_date.getFullYear() + 1);
                    var dup = dup_date;
                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y')  {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'W') {
                    dup_date.setDate(dup_date.getDate() + (+7));
                    var dup = dup_date;
    //                dup.setDate(dup.getDate() + (7 - dup.getDay()) % 7 + 1);
                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y')  {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'C') {
                    if ($scope.cus_rec_freq_period_type === 'CM') {
                        dup_date = $scope.addMonths(dup_date, $scope.cus_rec_freq_period_val);
                        var dup = dup_date;
    //                    var dup = new Date(dup_date.getFullYear(), dup_date.getMonth() + 1, 1);
                        $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else {
                        dup_date.setDate(dup_date.getDate() + (+$scope.cus_rec_freq_period_val * 7));
                        var dup = dup_date;
    //                    dup.setDate(dup.getDate() + (7 - dup.getDay()) % 7 + 1);
                        $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y')  {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'B' && $scope.prorate_first_payment !== 'Y') {
                    if (dup_date.getDate() < 16) {
                        $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16';
                    } else {
                        var dup_date = $scope.addMonths(dup_date, 1);
                        var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                        $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y')  {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'M' && $scope.prorate_first_payment !== 'Y') {
                    dup_date = $scope.addMonths(dup_date, 1);
//                    var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                    $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-' + ('0' + dup_date.getDate()).slice(-2);
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y')  {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }

                }else if($scope.mem_rec_frequency === 'N'){
                    $scope.recurring_payment_startdate = '';
                    if ($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);                       
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                }
                
                if ($scope.mem_rec_frequency !== 'N')
                    $scope.recurring_payment_startdate_view = $scope.dateString($scope.recurring_payment_startdate);
            $scope.total_payment_startdate_view = $scope.dateString($scope.total_payment_startdate);
            
            
                if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate)))|| $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                    $scope.total_membership_amount_view = 0;
                }
                
            }else if($scope.membership_structure === 'NC' || $scope.membership_structure === 'C'){
                var dup_date = $scope.safariiosdate($scope.total_payment_startdate);
                var act_date = $scope.safariiosdate($scope.actual_total_payment_startdate);
                var number_of_payments = +$scope.membership_no_of_payments;
                var intervalType = $scope.mem_rec_frequency;
                var billing_option = $scope.membership_billing_option;
                var recurring_amnt = (+$scope.total_membership_amount_view - +$scope.membership_billing_deposit_amount)/(+$scope.membership_no_of_payments);
                var recurring_pro_fee;
                if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                    if (recurring_amnt > 0) {
                        var process_fee1 = +(parseFloat(recurring_amnt)) + +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        recurring_pro_fee = 0;
                    }
                } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1'){
                    if (recurring_amnt > 0) {
                        var process_fee = +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        recurring_pro_fee = 0;
                    } 
                }

                $scope.pay = [];
                $scope.recurrent = {};
                
                if(parseFloat(recurring_amnt) > 0 && billing_option === 'PP'){
                    if (intervalType === 'M') {             //monthly
                        for (var i = 0; i < number_of_payments; i++) {
                            $scope.recurrent = {};
                            if (i === 0) {
                                $scope.recurrent['date'] = $scope.total_payment_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            } else {
                                dup_date = $scope.addMonths(dup_date, 1);
    //                            var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                var date = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-' + ('0' + dup_date.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            }
                        }
                    } else if (intervalType === 'W') {           //weekly
                        for (var i = 0; i < number_of_payments; i++) {
                            $scope.recurrent = {};
                            if (i === 0) {
                                $scope.recurrent['date'] = $scope.total_payment_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            } else {
                                dup_date.setDate(dup_date.getDate() + (+7));
                                var dup =  dup_date;
                                var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            }
                        }
                    } else if (intervalType === 'B') {           //Bi-weekly
                        for (var i = 0; i < number_of_payments; i++) {
                             $scope.recurrent = {};
                            if (i === 0) {
                                $scope.recurrent['date'] = $scope.total_payment_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            } else {
                                dup_date.setDate(dup_date.getDate() + (+14));
                                var dup =  dup_date;
                                var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            }
                        }
                    }                 
                
                    if($scope.pay.length > 0) {
                        $scope.recurring_payment_startdate = $scope.pay[0].date;
                        $scope.payment_amount = $scope.pay[0].amount;
                    }
                } else {
                    $scope.pay = [];
                    $scope.recurring_payment_startdate = '';
                    $scope.payment_amount = 0;
                }
                
            }  else if($scope.membership_structure === 'SE'){ 
                var dup_date = $scope.safariiosdate($scope.total_payment_startdate);
                var act_date = $scope.safariiosdate($scope.actual_total_payment_startdate);
                var specific_payment_frequency = $scope.specific_payment_frequency;
                var billing_option = $scope.membership_billing_option;
                $scope.TotalMembershipFeeSE = 0;
                $scope.pay = [];
                $scope.recurrent = {}; 
                
                if (specific_payment_frequency === 'PM') {             //per monthly 
                            var membership_firstDay = $scope.newSEdateFormat($scope.payment_start_date);
                            var membership_lastDay = $scope.newSEdateFormat($scope.specific_end_date);
                            var se_amount = 0;
                            var se_processing_fee = 0;
                            $scope.month_array = [];
                            $scope.total_count_array = [];
                            for (date = membership_firstDay; date <= membership_lastDay; date.setMonth(date.getMonth() + 1, 1)) {
                                var count = 0, exclude_count = 0;
                                var y = date.getFullYear(), m = date.getMonth(), da = date.getDate(), hr = date.getHours(), min = date.getMinutes();
                                var startdate = new Date(y, m, da, hr, min);
                                var enddate = new Date(y, m + 1, 0, hr, min);
                                if (membership_lastDay < enddate) {
                                    enddate = membership_lastDay;
                                }
                                count = $scope.startandenddate(startdate, enddate);
                                $scope.total_count_array.push(count);
                                if ($scope.exclude_from_billing_flag === 'Y' && $scope.mem_billing_exclude_days.length > 0) {
                                    for (i = 0; i < $scope.mem_billing_exclude_days.length; i++) {
                                        var final_exclude_startdate = "";
                                        var final_exclude_enddate = "";
                                        var exclude_start_date = $scope.newSEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_startdate);
                                        var exclude_end_date = $scope.newSEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_enddate);
                                        if ((exclude_start_date.getMonth() === date.getMonth()) && (exclude_start_date.getFullYear() === date.getFullYear())) {
                                            var y = exclude_start_date.getFullYear(), m = exclude_start_date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                                            var end_of_month = new Date(y, m + 1, 0, hr, min);
                                            if (date <= exclude_start_date || date <= exclude_end_date) {
                                                if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                    var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                    final_exclude_startdate = new Date(y, m, day, hr, min);
                                                    if (exclude_end_date > end_of_month) {
                                                         final_exclude_enddate = end_of_month;
                                                     }else{
                                                         final_exclude_enddate = exclude_end_date; //check
                                                     }
                                                } else {
                                                    final_exclude_startdate = exclude_start_date;
                                                    if (exclude_end_date > end_of_month) {
                                                        final_exclude_enddate = end_of_month;
                                                    } else {
                                                        final_exclude_enddate = exclude_end_date;
                                                    }
                                                }
                                            }
                                        } else if ((exclude_end_date.getMonth() === date.getMonth()) && (exclude_end_date.getFullYear() === date.getFullYear())) {
                                            if (date <= exclude_end_date || date <= exclude_start_date) {
                                                if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                    var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                    final_exclude_startdate = new Date(y, m, day, hr, min);
                                                    final_exclude_enddate = exclude_end_date;
                                                } else {
                                                    var y = exclude_end_date.getFullYear(), m = exclude_end_date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                                                    var startdate_of_month = new Date(y, m, 1, hr, min);
                                                    final_exclude_enddate = exclude_end_date;
                                                    if (exclude_start_date < startdate_of_month) {
                                                        final_exclude_startdate = startdate_of_month;
                                                    } else {
                                                        final_exclude_startdate = exclude_start_date;
                                                    }
                                                }
                                            }
                                        } else if (((exclude_start_date.getMonth() < date.getMonth() && exclude_start_date.getFullYear() === date.getFullYear()) || (exclude_start_date.getFullYear() < date.getFullYear())) && ((exclude_end_date.getMonth() > date.getMonth() && exclude_end_date.getFullYear() === date.getFullYear()) || (exclude_end_date.getFullYear() > date.getFullYear()))) {
                                           var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day =date.getDate();
                                            if (date > exclude_start_date && date < exclude_end_date) {//for date picking in registration
                                                    final_exclude_startdate = new Date(y, m, day, hr, min);
                                                    final_exclude_enddate = new Date(y, m + 1, 0, hr, min);
                                                } else {
                                            final_exclude_startdate = new Date(y, m, 1, hr, min);
                                            final_exclude_enddate = new Date(y, m + 1, 0, hr, min);
                                        }
                                      }
                                        if (final_exclude_startdate !== '' && final_exclude_enddate !== '') {
                                            exclude_count = +exclude_count + +$scope.startandenddate(final_exclude_startdate, final_exclude_enddate);

                                        }
                                    }
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1] - +exclude_count;
                                    count = $scope.total_count_array[$scope.total_count_array.length - 1];
                                } else {
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1];
                                }
                                var se_start_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                                $scope.recurrent = {};
                                se_amount = $scope.SEMembershipFee(count);
                                se_processing_fee = $scope.SEProcessingFee(se_amount);
                                $scope.recurrent['date'] = se_start_date;
                                $scope.recurrent['amount'] = se_amount;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = se_processing_fee;
                                $scope.pay.push($scope.recurrent);                      
                            }
                        } else if (specific_payment_frequency === 'PW') {           //per weekly
                        
                            var membership_firstDay = $scope.newSEdateFormat($scope.payment_start_date);
                            var membership_lastDay = $scope.newSEdateFormat($scope.specific_end_date);
                            var se_amount = 0, se_processing_fee = 0;
                            $scope.week_array = [];
                            $scope.total_count_array = [];
                            for (date = membership_firstDay; date <= membership_lastDay; date.setDate(date.getDate() + 7)) {
                                var start_date = "";
                                var end_date, da;
                                var count, exclude_count = 0;
                                var first = date.getDate() - (date.getDay() - 1); // start of the week
                                var last = first + 6;//last day of week  
                                var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                                var firstday_of_week = new Date(y, m, first, hr, min);
                                var lastday_of_week = new Date(y, m, last, hr, min);
                                if (date <= membership_firstDay) {
                                    start_date = membership_firstDay;
                                } else {
                                    start_date = firstday_of_week;
                                }
                                if (lastday_of_week >= membership_lastDay) {
                                    end_date = membership_lastDay;
                                } else {
                                    end_date = lastday_of_week;
                                }
                                var y = start_date.getFullYear(), m = start_date.getMonth(), da = start_date.getDate(), shr = date.getHours(), smin = date.getMinutes();
                                var startdate = new Date(y, m, da, shr, smin);
                                var ey = end_date.getFullYear(), em = end_date.getMonth(), eda = end_date.getDate(), ehr = date.getHours(), emin = date.getMinutes();
                                var enddate = new Date(ey, em, eda, ehr, emin);
                                count = $scope.startandenddate(startdate, enddate);
                                $scope.total_count_array.push(count);

                                if ($scope.exclude_from_billing_flag === 'Y' && $scope.mem_billing_exclude_days.length > 0) {
                                    for (i = 0; i < $scope.mem_billing_exclude_days.length; i++) {
                                        var exclude_start_date = $scope.newSEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_startdate);
                                        var exclude_end_date = $scope.newSEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_enddate);
                                        var final_exclude_startdate = "";
                                        var final_exclude_enddate = "";
//                                        if ((exclude_start_date.getMonth() === date.getMonth()) && (exclude_start_date.getFullYear() === date.getFullYear())) {
                                            if (date <= exclude_end_date || date <= exclude_start_date) {
                                                if (exclude_start_date >= firstday_of_week && exclude_start_date <= lastday_of_week) {
                                                    if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                        var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                        final_exclude_startdate = new Date(y, m, day, hr, min);
                                                        if(exclude_end_date > lastday_of_week){
                                                            final_exclude_enddate = lastday_of_week;
                                                        }else{
                                                            final_exclude_enddate = exclude_end_date;
                                                        }
                                                    } else {
                                                        final_exclude_startdate = exclude_start_date;
                                                        if (exclude_end_date > lastday_of_week) {
                                                            final_exclude_enddate = lastday_of_week;
                                                        } else {
                                                            final_exclude_enddate = exclude_end_date;
                                                        }
                                                    }
                                                } else if (exclude_end_date >= firstday_of_week && exclude_end_date <= lastday_of_week) {
                                                    if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                        var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                        final_exclude_startdate = new Date(y, m, day, hr, min);
                                                        final_exclude_enddate = exclude_end_date;
                                                    } else {
                                                        final_exclude_enddate = exclude_end_date;
                                                        if (exclude_start_date > firstday_of_week) {
                                                            final_exclude_startdate = exclude_start_date;
                                                        } else {
                                                            final_exclude_startdate = firstday_of_week;
                                                        }
                                                    }
                                                } else if (exclude_start_date < firstday_of_week && exclude_end_date > lastday_of_week) {
                                                    if (date > exclude_start_date && date < exclude_end_date) {//for date picking in registration
                                                        var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                        final_exclude_startdate = new Date(y, m, day, hr, min);
                                                            final_exclude_enddate = lastday_of_week;
                                                        } else {
                                                    final_exclude_startdate = firstday_of_week;
                                                    final_exclude_enddate = lastday_of_week;
                                                    
                                                }
                                            }
                                            }
                                        if (final_exclude_startdate !== '' && final_exclude_enddate !== '') {
                                            exclude_count = parseInt(exclude_count) + parseInt($scope.startandenddate(final_exclude_startdate, final_exclude_enddate));
                                        }
                                    }
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1] - +exclude_count;
                                    count = $scope.total_count_array[$scope.total_count_array.length - 1];
                                } else {
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1];
                                }
                                var se_start_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                                if (date.getDay() !== 1) {
                                    date = firstday_of_week;
                                }
                                $scope.recurrent = {};
                                se_amount = $scope.SEMembershipFee(count);
                                se_processing_fee = $scope.SEProcessingFee(se_amount);
                                $scope.recurrent['date'] = se_start_date;
                                $scope.recurrent['amount'] = se_amount;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = se_processing_fee;
                                $scope.pay.push($scope.recurrent);                            
                            }  
                            
                        }
                        
                        for (i = 0; i < $scope.pay.length; i++) {//wepay less than 5 calculation
                            $scope.pay[i].prorate_flag = "F";
                            while ($scope.pay[i].amount < 5) {
                                if ($scope.pay[i].amount === 0) {
                                    $scope.pay.splice(i, 1);
                                    i--;
                                    break;
                                }
                                var j = i + 1;
                                if ($scope.pay[j] === undefined)
                                    break;
                                $scope.pay[i].amount = $scope.pay[i].amount + $scope.pay[j].amount;
                                $scope.pay[i].processing_fee = $scope.SEProcessingFee($scope.pay[i].amount);
                                $scope.pay[i].prorate_flag = "T";
                                $scope.pay.splice(j, 1);
                        }
                    }
                    
                    if (specific_payment_frequency === 'PM' && $scope.pay.length > 0) {  //first and last dates prorate check  
                        var first_date = $scope.newSEdateFormat($scope.pay[0].date);
                        var last_date = $scope.newSEdateFormat($scope.specific_end_date);
                        var ye = last_date.getFullYear(), me = last_date.getMonth(), hre = last_date.getHours(), mine = last_date.getMinutes();
                        var end_of_months = new Date(ye, me + 1, 0, hre, mine);
                        if ($scope.pay[0].prorate_flag !== "T") {
                            $scope.pay[0].prorate_flag = (first_date.getDate() === 1) ? "F" : "T";
                        }
                        if ($scope.pay[$scope.pay.length - 1].prorate_flag !== "T" && $scope.pay.length > 1) {
                            $scope.pay[$scope.pay.length - 1].prorate_flag = (last_date.getDate() === end_of_months.getDate()) ? "F" : "T";
                        }
                    } else if (specific_payment_frequency === 'PW' && $scope.pay.length > 0) {
                        var first_date = $scope.newSEdateFormat($scope.pay[0].date);
                        var last_date = $scope.newSEdateFormat($scope.specific_end_date);
                        var checkprorate_start, checkprorate_end = '';
                        $scope.selected_days = $scope.billing_days.split('');
                        if ($scope.pay[0].prorate_flag !== "T") {    //week start
                            for (i = 0; i < $scope.selected_days.length; i++) {
                                if ($scope.selected_days[i] === "1") {
                                    checkprorate_start = i;
                                    break;
                                }
                            }
                            $scope.days_check_start = first_date.getDay() - 1;
                            $scope.days_check_start = ($scope.days_check_start === -1) ? '6' : $scope.days_check_start;
                            $scope.pay[0].prorate_flag = (checkprorate_start === $scope.days_check_start) ? "F" : "T";
                        }
                        if ($scope.pay[$scope.pay.length - 1].prorate_flag !== "T" && $scope.pay.length > 1) {     //week end
                            for (j = $scope.selected_days.length - 1; j >= 0; j--) {
                                if ($scope.selected_days[j] === "1") {
                                    checkprorate_end = j;
                                    break;
                                }
                            }
                            $scope.days_check_last = last_date.getDay() - 1;
                            $scope.days_check_last = ($scope.days_check_last === -1) ? '6' : $scope.days_check_last;
                            $scope.pay[$scope.pay.length - 1].prorate_flag = (checkprorate_end === $scope.days_check_last) ? "F" : "T";
                        }
                    }
                    
                    if(parseInt($scope.pay.length) > 1 && $scope.pay !== undefined && $scope.pay !== ''){ 
                        if ($scope.pay[$scope.pay.length - 1].amount < 5) {
                            $scope.pay[$scope.pay.length - 2].amount += $scope.pay[$scope.pay.length - 1].amount;
                            $scope.pay[$scope.pay.length - 2].processing_fee =  $scope.SEProcessingFee($scope.pay[$scope.pay.length - 2].amount);
                            $scope.pay[$scope.pay.length - 2].prorate_flag="T";
                            $scope.pay.splice($scope.pay.length - 1, 1);
                        }
                    }
                    
                        var se_mem_amount = 0; //total with exclude both pp,pf
                        for(i=0; i<$scope.pay.length; i++ ){
                            se_mem_amount = parseFloat(se_mem_amount) + parseFloat($scope.pay[i].amount);
                        }
                        $scope.TotalMembershipFeeSE = se_mem_amount;
                         
                    if(billing_option === 'PF'){
                        $scope.payment_amount = se_mem_amount;
                        $scope.pay = [];
                        $scope.recurring_payment_startdate = '';
                    }
                    
                    if($scope.pay.length > 0){
                        $scope.recurring_payment_startdate = $scope.pay[0].date;
                        $scope.payment_amount = $scope.pay[0].amount;
                        if(parseFloat($scope.payment_amount) >= 5){
                            if($scope.membershippaymentform)
                            $scope.membershippaymentform.$setValidity("membershippaymentform", true);
                        }else{
                            if($scope.membershippaymentform)
                            $scope.membershippaymentform.$setValidity("membershippaymentform", false);
                        }
                    }else{
                        $scope.pay = [];
                        $scope.recurring_payment_startdate = '';
                    }
            } 

            if ($scope.total_membership_amount_view > 0 || $scope.total_signup_amount_view > 0) {
                $scope.total_amount_view = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
//                console.log("TAV - 2 : " + $scope.total_amount_view);
                $scope.total_reg_amount_view = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
            } else {
                $scope.membership_discount_show = false;
                $scope.signup_discount_show = false;
            }
            
            if($scope.membership_structure === 'NC' || $scope.membership_structure === 'C') {
                var curr_date = new Date();
                var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                var payment_date = new Date($scope.total_payment_startdate);
                var actual_payment_date = new Date($scope.actual_total_payment_startdate);
                if ($scope.membership_billing_option === 'PP' && $scope.pay.length > 0) {
                    if((+actual_payment_date ===  +payment_date) && (+server_curr_date === +actual_payment_date)){
                        $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.membership_billing_deposit_amount+ +$scope.payment_amount;
                        var amount = $scope.total_amount_view;
                        $scope.final_membership_payment_amount = amount;
                        $scope.pay.shift();
                        $scope.recurring_payment_startdate = ($scope.pay.length > 0) ? $scope.pay[0].date : "";
                    }else{
                        $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.membership_billing_deposit_amount;
                        var amount = $scope.total_amount_view;
                        $scope.final_membership_payment_amount = amount;
                    }
                } else {
                    var amount = +$scope.total_amount_view;
                    $scope.final_membership_payment_amount = amount;
                }
            }else if($scope.membership_structure === 'SE'){
                var curr_date = new Date();
                var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                var payment_date = new Date($scope.total_payment_startdate);
                var actual_payment_date = new Date($scope.actual_total_payment_startdate);
                if ($scope.membership_billing_option === 'PP' && $scope.pay.length > 0 ) {
                    if((+actual_payment_date ===  +payment_date) && (+server_curr_date === +actual_payment_date)){
                        $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.pay[0].amount;
                        var amount = $scope.total_amount_view;
                        $scope.final_membership_payment_amount = amount;
                        $scope.pay.shift();
                    }else{
                        if ($scope.mem_fee_include_status === 'Y') {
                            $scope.total_amount_view = +$scope.total_signup_amount_view + $scope.pay[0].amount;
                            $scope.pay.splice(0, 1);
                            var amount = $scope.total_amount_view;
                            $scope.final_membership_payment_amount = amount;
                        }else{
                            $scope.total_amount_view = +$scope.total_signup_amount_view;
                            var amount = +$scope.total_amount_view;
                            $scope.final_membership_payment_amount = amount;
                        }
                    }
                    $scope.recurring_payment_startdate = ($scope.pay.length > 0) ? $scope.pay[0].date : "";
                    
                } else {
                    if ($scope.mem_fee_include_status === 'N' && (new Date(server_curr_date) < new Date($scope.actual_total_payment_startdate))) {   // Membership fee doesnot include on first payment
                        $scope.total_amount_view = +$scope.total_signup_amount_view;
                        var amount = $scope.total_amount_view;
                        $scope.final_membership_payment_amount = amount;
                        $scope.membership_fee_include = $scope.payment_amount;                       
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.payment_amount;
                        var amount = $scope.total_amount_view;
                        $scope.final_membership_payment_amount = amount;
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                }
            }else {
                var amount = +$scope.total_amount_view;
                $scope.final_membership_payment_amount = amount;
            } 

            if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                if (amount > 0) {
                    var process_fee1 = +amount + +((+amount * +process_fee_per) / 100) + +process_fee_val;
                    var process_fee = +((+process_fee1 * +process_fee_per) / 100) + +process_fee_val;
                    $scope.processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    $scope.processing_fee_value = 0;
                }
//                console.log("PFV : " + $scope.processing_fee_value);
                $scope.total_amount_view = (Math.round((+amount + +$scope.processing_fee_value + +0.00001) * 100) / 100).toFixed(2);
//                console.log("TAV - 4 : " + $scope.total_amount_view);
            } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1') {
                if (amount > 0) {
                    var process_fee = +((+amount * +process_fee_per) / 100) + +process_fee_val;
                    $scope.processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    $scope.processing_fee_value = 0;
                }
                $scope.total_amount_view = (Math.round((+amount + +0.00001) * 100) / 100).toFixed(2);
//                console.log("TAV - 5 : " + $scope.total_amount_view);
            }
            $scope.pay_infull_processingfee = $scope.processing_fee_value;
//            console.log("TAV - 6 : " + $scope.total_amount_view);
            if (parseFloat($scope.total_amount_view) == parseFloat('0')) {
                if ($scope.membership_structure === 'OE' && ((parseFloat($scope.delay_membership_amount_view) > 0) || (parseFloat($scope.first_payment_amount) > 0))) {
                    $scope.zerocostview = true;
                }else if(($scope.membership_structure === 'NC' || $scope.membership_structure === 'C') && (parseFloat($scope.total_mem_amount_view) > 0)){
                    $scope.zerocostview = true;
                } else {
                    $scope.zerocostview = false;
                }
            } else {
                $scope.zerocostview = true;
            }

        };
        
        
        //GET MEMBERSHIP LIST
        $scope.getmembershipdetails = function ()
        {  
            $scope.preloaderVisible = true;
            $http({
                method: 'GET',
                url: urlservice.url + 'getAllMembershipCategory',
                params: {
                    "company_id": $localStorage.company_id,
                    "list_type": 'P'
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    
                    var membersView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    membersView.router.loadPage({
                       pageName: 'memberships-listing'
                    });
                    
                    $scope.membershipListContent = response.data.msg.live;
                    
                                    
                    $scope.membershipContentStatus = response.data.status; 
                    $scope.preloaderVisible = false;
                } else {
                    $scope.membershipListContent = "";
                    $scope.preloaderVisible = false;
                    $scope.membershipContentStatus = response.data.status;
                }
            }, function (response) {
                $scope.membershipListContent = "";
                $scope.membershipContentStatus = response.data.status; 
                $scope.preloaderVisible = false;
                console.log(response.data);
            });

        };
        
        $scope.membershipdetail = function (detail) { //kamal

            $scope.preloaderVisible = true;
            $http({
                method: 'GET',
                url: urlservice.url + 'getMembershipCategory',
                params: {
                    "company_id": $localStorage.company_id,
                    "list_type": detail.category_status,
                    "membership_id": detail.membership_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                   },
                    withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.preloaderVisible = false;
                    $scope.membershipOptionDetail = response.data.msg;

                    $scope.rank_col_names = $scope.membershipOptionDetail.rank_details;

                    $scope.newMembershipCategoryImage = $scope.membershipOptionDetail.category_image_url;
                    if ($scope.newMembershipCategoryImage === '' || $scope.newMembershipCategoryImage === undefined) {
                        $scope.showMemCategoryImage = false;
                    } else {
                        $scope.showMemCategoryImage = true;
                    }

                    $scope.newMembershipCategoryText = $scope.membershipOptionDetail.category_title;

                    $scope.newMembershipCategoryUrl = $scope.membershipOptionDetail.category_video_url;
                    if ($scope.newMembershipCategoryUrl === '' || $scope.newMembershipCategoryUrl === undefined) {
                        $scope.showMemCategoryUrl = false;
                    } else {
                        $scope.showMemCategoryUrl = true;
                    }

                    $scope.newMembershipCategorydesc = $scope.membershipOptionDetail.category_description;
                    if ($scope.newMembershipCategorydesc === '' || $scope.newMembershipCategorydesc === undefined) {
                        $scope.showMemCategoryDesc = false;
                    } else {
                        $scope.showMemCategoryDesc = true;
                    }

                    if ($scope.membershipOptionDetail.membership_options.length > 0) {
                        $scope.membershipOptionContent = $scope.membershipOptionDetail.membership_options;
                        $scope.membershipOptionContentStatus = true;
                    } else {
                        $scope.membershipOptionContent = "";
                        $scope.membershipOptionContentStatus = false;
                    }
                    
                    var membersView = f7.addView('.membership-main', {
                    domCache: true,  dynamicNavbar: true
                    });
                    membersView.router.loadPage({
                       pageName: 'membershipparentdescription'
                    });
                } else {
                    $scope.membershipOptionDetail = "";
                    $scope.preloaderVisible = false;
                }
            }, function (response) {
                $scope.membershipOptionDetail = "";
                $scope.preloaderVisible = false;
                console.log(response.data);
            });
        };
        
        $scope.membershipOptionDescription = function (ind) {            
            $scope.membershipOptiondescContent = $scope.membershipOptionContent[ind]; 
            $scope.newMembershipOptionText = $scope.membershipOptiondescContent.membership_title;
            $scope.newMembershipOptionSubText = $scope.membershipOptiondescContent.membership_subtitle;
            $scope.newMembershipOptiondesc = $scope.membershipOptiondescContent.membership_description;
            if ($scope.newMembershipOptiondesc === '' || $scope.newMembershipOptiondesc === undefined) {
                $scope.showMemOptionDesc = false;
            } else {
                $scope.showMemOptionDesc = true;
            }   
            $scope.mem_reg_col_names = $scope.membershipOptiondescContent.reg_columns;
            if($scope.membershipOptiondescContent.waiver_policies === null || $scope.membershipOptiondescContent.waiver_policies === undefined || $scope.membershipOptiondescContent.waiver_policies === '') {
                $scope.newMemWaiverText = '';
                $scope.showMemWaiver = false;
            } else {
                $scope.newMemWaiverText = $scope.membershipOptiondescContent.waiver_policies;
                $scope.showMemWaiver = true;
            }
            
                $scope.current_membership_manage = $scope.membershipOptiondescContent;
                
                $scope.show_in_app_flg = $scope.current_membership_manage.show_in_app_flg;
                if($scope.show_in_app_flg === 'Y'){
                    $scope.show_in_app = true;
                }else{
                    $scope.show_in_app = false;
                }
                $scope.membership_structure = $scope.current_membership_manage.membership_structure;
                $scope.processing_fee_show = $scope.current_membership_manage.membership_processing_fee_type;
                $scope.memsignupfee = parseFloat($scope.current_membership_manage.membership_signup_fee);
                $scope.memfee = parseFloat($scope.current_membership_manage.membership_fee);
                $scope.mem_fee_include_status = $scope.current_membership_manage.initial_payment_include_membership_fee;
                
                $scope.mem_rec_frequency = $scope.current_membership_manage.membership_recurring_frequency;
                
                $scope.membership_structure = $scope.current_membership_manage.membership_structure;
                $scope.membership_billing_option = $scope.current_membership_manage.billing_options;
                $scope.membership_billing_startdate_type = $scope.current_membership_manage.billing_payment_start_date_type;
                $scope.membership_billing_startdate = $scope.current_membership_manage.billing_options_payment_start_date;
                $scope.membership_billing_deposit_amount = parseFloat($scope.current_membership_manage.deposit_amount);
                $scope.membership_no_of_payments = $scope.current_membership_manage.no_of_payments;
                $scope.classPackagesView = parseFloat($scope.current_membership_manage.no_of_classes);
                $scope.membership_expiry_status = $scope.current_membership_manage.expiration_status;
                $scope.membership_expiry_period = $scope.current_membership_manage.expiration_date_type;
                $scope.membership_expiry_value = $scope.current_membership_manage.expiration_date_val; 
                
                
                if($scope.mem_rec_frequency==='B' || $scope.mem_rec_frequency==='M'){
                    $scope.prorate_first_payment = $scope.current_membership_manage.prorate_first_payment_flg;                    
                    $scope.cus_rec_freq_period_type = '';
                    $scope.cus_rec_freq_period_val = '';
                }else if($scope.mem_rec_frequency==='C'){
                    $scope.prorate_first_payment = 'N';
                    $scope.cus_rec_freq_period_type = $scope.current_membership_manage.custom_recurring_frequency_period_type;
                    $scope.cus_rec_freq_period_val = $scope.current_membership_manage.custom_recurring_frequency_period_val;
                }else{
                    $scope.prorate_first_payment = 'N';
                    $scope.cus_rec_freq_period_type = '';
                    $scope.cus_rec_freq_period_val = '';
                }
                
                $scope.delay_rec_payment_start_flg = $scope.current_membership_manage.delay_recurring_payment_start_flg;
                if($scope.delay_rec_payment_start_flg === 'Y'){
                    $scope.delayed_recurring_payment_type =$scope.current_membership_manage.delayed_recurring_payment_type;
                    $scope.delayed_recurring_payment_val = $scope.current_membership_manage.delayed_recurring_payment_val;
                }else{
                    $scope.delayed_recurring_payment_type = '';
                    $scope.delayed_recurring_payment_val = '';
                } 
                
                $scope.membership_structure_datepicker = $scope.current_membership_manage.membership_structure;
                $scope.specific_start_date =$scope.current_membership_manage.specific_start_date;
                $scope.specific_end_date = $scope.current_membership_manage.specific_end_date;
                $scope.specific_payment_frequency =$scope.current_membership_manage.specific_payment_frequency;
                $scope.billing_days =$scope.current_membership_manage.billing_days;
                $scope.exclude_from_billing_flag =$scope.current_membership_manage.exclude_from_billing_flag;
                if($scope.exclude_from_billing_flag === 'Y'){
                    $scope.mem_billing_exclude_days = $scope.current_membership_manage.mem_billing_exclude;
                }else{
                    $scope.mem_billing_exclude_days = '';
                } 
                
                $scope.newTotalFee = 0;
                $scope.newTotalRecurringFee = 0;
                $scope.newMembershipFee_processing_fee = 0;
                $scope.newMemProcessingFee = $scope.processing_fee_show;
                $scope.newSignupFee = $scope.memsignupfee;
                $scope.newMembershipFee = $scope.memfee;
                $scope.pay_infull_processingfee = 0;
                $scope.newMemRecFrequency = $scope.mem_rec_frequency;
                       
                    
                $scope.newMemStructure = $scope.membership_structure;
                $scope.newMemDepositAmnt = $scope.membership_billing_deposit_amount;
                $scope.newMemBillingOption = $scope.membership_billing_option;
                $scope.newMemNoOfPayments = $scope.membership_no_of_payments;
                $scope.newMemPaymntStartDate = $scope.membership_billing_startdate_type;
                $scope.newMemCustDate = $scope.membership_billing_startdate; 
                $scope.newMembershipFeeInclude = $scope.mem_fee_include_status;
                $scope.newSEStartDate = $scope.specific_start_date;
                $scope.newSEEndDate = $scope.specific_end_date;
                $scope.newSpecificFrequency = $scope.specific_payment_frequency;
                $scope.newexclude_from_billing_flag = $scope.exclude_from_billing_flag;
                $scope.new_exclude_days_array = $scope.mem_billing_exclude_days;
                $scope.newbilling_days = $scope.billing_days;
        };
        
         $scope.membership_paymentdetails = function (){ 
                var dup_date = $scope.safariiosdate($scope.payment_start_date);
                var du_date = dup_date;
                $scope.actual_total_payment_startdate = du_date.getFullYear() + '-' + ('0' + (du_date.getMonth() + 1)).slice( - 2) + '-' + ('0' + du_date.getDate()).slice( - 2); 
                $scope.payment_start_date_view = $scope.dateString($scope.payment_start_date); //kumar
                
                if($scope.membership_structure === 'SE'){ // Has specific start and end date
                    $scope.payment_end_date_view = $scope.dateString($scope.specific_end_date);
                }else{
                    $scope.payment_end_date_view = "";
                }
                
                if($scope.membership_structure === 'OE'){
                    if($scope.delay_rec_payment_start_flg === 'N') { //multiple recurring payment details
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else {
                        if ($scope.delayed_recurring_payment_type === 'DM') {
                            dup_date = $scope.addMonths(dup_date, $scope.delayed_recurring_payment_val);
                            var dup = dup_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        } else {
                            dup_date.setDate(dup_date.getDate() + (+$scope.delayed_recurring_payment_val * 7));
                            var dup = dup_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }
                        $scope.delayed_recurring_payment_startdate = $scope.total_payment_startdate;
                        $scope.delayed_rec_payment_date = $scope.dateString($scope.total_payment_startdate);
                    }
                }else if($scope.membership_structure === 'NC'|| $scope.membership_structure === 'C'){
                    if($scope.membership_billing_option === 'PF'){
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }else if($scope.membership_billing_option === 'PP'){
                        if ($scope.membership_billing_startdate_type === '1') {          //30 days after registration
                            var cus_curr_date = new Date();
                            cus_curr_date.setDate(cus_curr_date.getDate() + 30);
                            var dup = cus_curr_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        } else if ($scope.membership_billing_startdate_type === '2') {       //1 week after registration
                            var cus_curr_date = new Date();
                            cus_curr_date.setDate(cus_curr_date.getDate() + 7);
                            var dup = cus_curr_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);                         
                        }else if ($scope.membership_billing_startdate_type === '3') {       //2 week after registration
                            var cus_curr_date = new Date();
                            cus_curr_date.setDate(cus_curr_date.getDate() + 14);
                            var dup = cus_curr_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);  
                        }else if ($scope.membership_billing_startdate_type === '4') {       //custom date selection
                            $scope.total_payment_startdate = $scope.membership_billing_startdate;
                                var cus_curr_date = new Date();
                                var cus_current_date = cus_curr_date.getFullYear() + '-' + ('0' + (cus_curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + cus_curr_date.getDate()).slice( - 2);
                                if(new Date(cus_current_date) > new Date($scope.membership_billing_startdate)){ 
                                    f7.alert('Payment plan for this membership has already started, please contact studio for assistance.', ' ');  
                                    return false;
                                }
                        }else if ($scope.membership_billing_startdate_type === '5') {       //on membership start date
                            $scope.total_payment_startdate = $scope.actual_total_payment_startdate;
                        }

                    }

                }else if($scope.membership_structure === 'SE'){
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                }
                
                var membershipView = f7.addView('.membership-main', {
                    domCache: true, dynamicNavbar: true
                });
                membershipView.router.loadPage({
                    pageName: 'MembershipsCart'
                });
                
                if(($scope.membership_structure === 'NC' || $scope.membership_structure === 'C') && $scope.membership_expiry_status ==='Y'){
                    if ($scope.membership_expiry_period === 'M') {
                        dup_date = $scope.addMonths(dup_date, $scope.membership_expiry_value);
                        var dup = dup_date;
                        $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }else if ($scope.membership_expiry_period === 'W') {
                        dup_date.setDate(dup_date.getDate() + (+$scope.membership_expiry_value * 7));
                        var dup = dup_date;
                        $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }else if ($scope.membership_expiry_period === 'Y') {
                        dup_date.setFullYear(dup_date.getFullYear() + +$scope.membership_expiry_value);
                        var dup = dup_date;
                        $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }                    
                }else{
                    $scope.membership_expiry_date = '';
                }
                $scope.showMembershipTotal();
         };
        

        //GET EVENTS LIST
        $scope.getEventsList = function (list_type)
        {
            
            $scope.preloaderVisible = true;
            $http({
                method: 'GET',
                url: urlservice.url + 'geteventdetails',
                params: {
                    "company_id": $localStorage.company_id,
                    "list_type" : list_type,
                    "from":'I' //Iframe
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {

                    $localStorage.eventContent = $scope.eventContent = response.data.msg.live;
                    $scope.eventenabledstatus = response.data.event_status.event_enabled;
                    if( $scope.eventContent.event_type==='M' && $scope.eventContent.child_events.length > 0){
                        $scope.eventChildContent = $scope.eventContent.child_events;
                    }else{
                        $scope.eventChildContent = "";
                    }                    
                    $scope.eventContentStatus = response.data.status; 
                    $scope.showEventAddEdit = true;
                    $scope.preloaderVisible = false;
                } else {
                    $scope.eventContent = "";
                    $scope.preloaderVisible = false;
                    $scope.eventContentStatus = response.data.status;
                }
            }, function (response) {
                $scope.eventContent = "";
                $scope.eventContentStatus = response.data.status; 
                $scope.preloaderVisible = false;
                console.log(response.data);
            });
        };
        //EVENT STATUS ENABLED  
        $scope.$watch(function () {return $localStorage.eventenabledstatus;}, function (newVal, oldVal) {
                $scope.eventenabledstatus = newVal;
        });
        $scope.get_remaining_spaces = function(capacity_text,event_capacity,registered_count){
               if(registered_count === undefined){
                   registered_count = '0';
               }
           
            $scope.availablespace = event_capacity - registered_count;
            if(event_capacity > 0){
                    if(capacity_text ==='1'){
                            if($scope.availablespace <'1'){
                                return 'sold out';
                            }
                        return '';
                    }
                    else if(capacity_text ==='2'){
                        if($scope.availablespace <'1'){
                            return 'sold out';
                        }
                        else if($scope.availablespace =='1' ){
                            return '1 spot left';
                        }
                        else{
                            return $scope.availablespace+' spots left';
                        }
                    }
                    else if(capacity_text ==='3'){
                        if(Math.ceil(($scope.availablespace)/2) <'1'){
                            return 'sold out';
                        }
                        else if(Math.ceil(($scope.availablespace)/2) =='1'){
                            return '1 spot left';
                        }
                        else{
                            return Math.ceil(($scope.availablespace)/2)+' spots left';
                        }
                    }
                    else {
                            if($scope.availablespace <'1'){
                                return 'sold out';
                            }
                            else{
                                return '<span style="font-size:12px;">Limited space remaining</span>';
                            }
                    }
            }
            else{
                return '';
            }
            
        };
         
        $scope.eventdetail = function (value) {
            $scope.eventChildContent = [];
            $http({
                method: 'GET',
                url: urlservice.url + 'getsingleeventdetails',
                params: {
                    "company_id": $localStorage.company_id,
                    "event_type": value.event_type,
                    "list_type": value.event_status === 'P' ? 'live' : (value.event_status === 'S' ? 'draft' :  'past'),
                    "event_id": value.event_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $scope.eventContent = [];
                    $scope.eventContent = response.data.msg;
                    $scope.eventChildContent = $scope.eventContent.child_events;
                    if($scope.eventChildContent.length > 0) {
                        $scope.childeventContentStatus = 'Success';
                    }
                    $scope.preview_event_type = $scope.eventContent.event_type;
                    $scope.reg_col_names = $scope.eventContent.reg_columns;   

                    $scope.newDMEventImage = $scope.eventContent.event_banner_img_url;
                    if ($scope.newDMEventImage === '' || $scope.newDMEventImage === undefined) {
                        $scope.showDMEventImage = false;
                    } else {
                        $scope.showDMEventImage = true;
                    }

                    $scope.newDMEventCategoryText = $scope.eventContent.event_title;

                    $scope.newDPEventVideourlText = $scope.eventContent.event_video_detail_url;
                    if ($scope.newDPEventVideourlText === '' || $scope.newDPEventVideourlText === undefined) {
                        $scope.showDPEventVideo = false;
                    } else {
                        $scope.showDPEventVideo = true;
                    }

                    $scope.newDPEventdescText = $scope.eventContent.event_desc;
                    if ($scope.newDPEventdescText === '' || $scope.newDPEventdescText === undefined) {
                        $scope.showDPEventdesc = false;
                    } else {
                        $scope.showDPEventdesc = true;
                    }

                    if($scope.eventContent.waiver_policies === null || $scope.eventContent.waiver_policies === undefined || $scope.eventContent.waiver_policies === '') {
                        $scope.newEventWaiverText = '';
                        $scope.showEventWaiver = false;
                    } else {
                        $scope.newEventWaiverText = $scope.eventContent.waiver_policies;
                        $scope.showEventWaiver = true;
                    }
                    $scope.newDCEventProcesFee = $scope.eventContent.processing_fees;
                }else{
                    $scope.eventContent = "";
                    $scope.preloaderVisible = false;
                    console.log(response.data);
                }
            }, function (response) {
                $scope.eventContent = "";
                $scope.preloaderVisible = false;
                console.log(response.data);
            });
        };
        
        $scope.editdesc = function(){
            $scope.esevent_quantity = 1;
        };
        
        $scope.eprocessingfee =function(ecost,eprocess){
            $scope.edittotal = parseFloat(ecost) + parseFloat(eprocess);
            return $scope.edittotal;
        };
        
        $scope.eventdescription = function (value) {
            $scope.eventChildContent = [];
            $http({
                method: 'GET',
                url: urlservice.url + 'getsingleeventdetails',
                params: {
                    "company_id": $localStorage.company_id,
                    "event_type": value.event_type,
                    "list_type": value.event_status === 'P' ? 'live' : (value.event_status === 'S' ? 'draft' :  'past'),
                    "event_id": value.event_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $scope.eventContent = [];
                    $scope.eventContent = response.data.msg;
                    $scope.eventChildContent = $scope.eventContent.child_events;
                    if($scope.eventChildContent.length > 0) {
                        $scope.childeventContentStatus = 'Success';
                    }
                    $scope.preview_event_type = $scope.eventContent.event_type;
                    $scope.reg_col_names = $scope.eventContent.reg_columns; 
                    $scope.eventdescContent = $scope.eventContent;
                    $scope.sevent_cost = $scope.eventContent.event_cost;
                    $scope.newDEventProcesFee = $scope.eventContent.processing_fees;
                    $scope.sevent_quantity = 1;            
                    if($scope.eventdescContent.waiver_policies === null || $scope.eventdescContent.waiver_policies === undefined || $scope.eventdescContent.waiver_policies === '') {
                        $scope.newEventWaiverText = '';
                        $scope.showEventWaiver = false;
                    } else {
                        $scope.newEventWaiverText = $scope.eventdescContent.waiver_policies;
                        $scope.showEventWaiver = true;
                    }
                    $scope.updateSingleTotalAmount();
                }else{
                    $scope.eventContent = "";
                    $scope.preloaderVisible = false;
                    console.log(response.data);
                }
            }, function (response) {
                $scope.eventContent = "";
                $scope.preloaderVisible = false;
                console.log(response.data);
            });
        };
        
        $scope.eventchilddescription = function (ind) {
            $scope.eventchilddescContent = $scope.eventChildContent[ind]; 
            $scope.newDCEventVideourlText = $scope.eventChildContent[ind].event_video_detail_url;
                if($scope.newDCEventVideourlText === '' || $scope.newDCEventVideourlText === undefined){
                    $scope.showDCEventVideo = false;
                }else{
                    $scope.showDCEventVideo = true;
                }
            $scope.cevent_cost = $scope.eventChildContent[ind].event_cost;
            $scope.cevent_quantity = 1;
            $scope.updateMultipleTotalAmount();
        };

        $scope.eventeditchilddescription = function (ind) {
            $scope.newDCEventProcesFee = $scope.eventContent.processing_fees;
            $scope.newDMEventImage = $scope.eventContent.event_banner_img_url;            
            $scope.newDCEventNameText = $scope.eventChildContent[ind].event_title;
            $scope.newDCEventdescText = $scope.eventChildContent[ind].event_desc;
            
            $scope.newDCEventImage = $scope.eventChildContent[ind].event_banner_img_url;
            if($scope.newDCEventImage === '' || $scope.newDCEventImage === undefined){
                $scope.showDCEventImage = false;
            }else{
                $scope.showDCEventImage = true;
            }
         
                 $scope.newDCEventDateText = $scope.eventChildContent[ind].event_begin_dt.split(' ')[0];
                    $scope.newDCEventTimeTextt = $scope.eventChildContent[ind].event_begin_dt.split(' ')[1];
                    //Edit event start date timenewDCEventTimeTextt
                    if( $scope.eventChildContent[ind].event_begin_dt === undefined ||  $scope.eventChildContent[ind].event_begin_dt === '0000-00-00 00:00:00' ){
                            $scope.showDCEventDate=false;
                            $scope.showDCEventTime = false;
                            $scope.newDCEventDateText = "";
                            $scope.newDCEventTimeTextt="";
                        
                    }else if( $scope.eventChildContent[ind].event_begin_dt !== undefined ||  $scope.eventChildContent[ind].event_begin_dt !== '0000-00-00 00:00:00' ){
                        if(($scope.newDEventDateText !== undefined && $scope.newDCEventTimeTextt === '00:00:00')|| ($scope.newDEventDateText !== '0000-00-00'&& $scope.newDCEventTimeTextt === '00:00:00')){
                            $scope.showDCEventTime=false;
                            $scope.showDCEventDate = true;
                            $scope.newDCEventDateText =  $scope.eventChildContent[ind].event_begin_dt.split(' ')[0];
                            $scope.newDCEventTimeTextt="";
                        }else if(($scope.newDEventDateText !== '0000-00-00' && $scope.newDCEventTimeTextt !== undefined )|| ($scope.newDEventDateText !== '0000-00-00' && $scope.newDCEventTimeTextt !== '00:00:00')){
                            $scope.showDCEventTime=true;
                            $scope.showDCEventDate = false;
                            $scope.newDCEventDateText =  $scope.eventChildContent[ind].event_begin_dt.split(' ')[0];
                            $scope.newDCEventTimeTextt =  $scope.eventChildContent[ind].event_begin_dt;
                        }
                    }
                    
                    
                    $scope.newDCEventVideourlText = $scope.eventChildContent[ind].event_video_detail_url;
                    if($scope.newDCEventVideourlText === '' || $scope.newDCEventVideourlText === undefined){
                        $scope.showDCEventVideo = false;
                    }else{
                        $scope.showDCEventVideo = true;
                    }
                    
                    $scope.newDCEventENDDateText =  $scope.eventChildContent[ind].event_end_dt.split(' ')[0];
                    $scope.newDCEventENDTimeTextt =  $scope.eventChildContent[ind].event_end_dt.split(' ')[1];
                    
                    //Edit event end date time
                    if( $scope.eventChildContent[ind].event_end_dt === undefined ||  $scope.eventChildContent[ind].event_end_dt === '0000-00-00 00:00:00' ){
                            $scope.showDCEventEndTime=false;
                            $scope.showDDCEventEndDate = false;
                            $scope.newDCEventENDDateText = "";
                            $scope.newDCEventENDTimeTextt="";
                        
                    }else if( $scope.eventChildContent[ind].event_end_dt !== undefined ||  $scope.eventChildContent[ind].event_end_dt !== '0000-00-00 00:00:00' ){
                        if(($scope.newDCEventENDDateText !== undefined && $scope.newDCEventENDTimeTextt === '00:00:00')|| ($scope.newDCEventENDDateText !== '0000-00-00'&& $scope.newDCEventENDTimeTextt === '00:00:00')){
                            $scope.showDCEventEndTime=false;
                            $scope.showDDCEventEndDate = true;
                            $scope.newDCEventENDDateText =  $scope.eventChildContent[ind].event_end_dt.split(' ')[0];
                            $scope.newDCEventENDTimeTextt="";
                        }else if(($scope.newDCEventENDDateText !== '0000-00-00' && $scope.newDCEventENDTimeTextt !== undefined )|| ($scope.newDCEventENDDateText !== '0000-00-00' && $scope.newDCEventENDTimeTextt !== '00:00:00')){
                            $scope.showDCEventEndTime=true;
                            $scope.showDDCEventEndDate = false;
                            $scope.newDCEventENDDateText =  $scope.eventChildContent[ind].event_end_dt.split(' ')[0];
                            $scope.newDCEventENDTimeTextt =  $scope.eventChildContent[ind].event_end_dt;
                        }
                    }
                    
                    $scope.newDCEventCostText = $scope.eventChildContent[ind].event_cost;
                    if($scope.newDCEventCostText === '0.00' || $scope.newDCEventCostText === undefined){
                        $scope.showDCEventCost = false;
                    }else{
                        $scope.showDCEventCost = true;
                    }
                    
                    if($scope.newDCEventProcesFee === '2') {
                            $scope.EditCEventCost = $scope.newDCEventCostText;
                            $scope.EditCEventProcessCost = $scope.getProcessingCost($scope.newDCEventCostText);
                            $scope.EditEventTotalProcessCost = $scope.TotalEventProcessingCost($scope.newDCEventCostText);
                    }
                   
                    
                    $scope.newDCEventdescText = $scope.eventChildContent[ind].event_desc;
                    if($scope.newDCEventdescText === '' || $scope.newDCEventdescText === undefined){
                        $scope.showCMEventdesc = false;
                    }else{
                        $scope.showCMEventdesc = true;
                    }
        };

        //GET MESSAGE LIST
        $scope.getmessageList = function (){

            $scope.preloaderVisible = true;

            $http({
                method: 'GET',
                url: urlservice.url + 'getmessagedetails',
                params: {
                    "company_id": $localStorage.company_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response)
            {
                if (response.data.status == 'Success') {
                    $scope.msgcontentstatus = response.data.status;
                    $localStorage.msgcontent = $scope.msgcontent = response.data.msg;
                    $scope.preloaderVisible = false;
                } else {
                    $scope.msgcontentstatus = response.data.status;
                    $localStorage.msgcontent = $scope.msgcontent = "";
                    $scope.preloaderVisible = false;

                }
            }, function (response) {
                $scope.msgcontentstatus = response.data.status;
                $scope.preloaderVisible = false;
            });
        };

        //GET CURRICULUM
        $scope.getCurriculum = function (preview_value) {
            $scope.preloaderVisible = true;

            $http({
                method: 'GET',
                url: urlservice.url + 'getcurriculumdetails',
                params: {
                    "company_id": $localStorage.company_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $scope.curriculumContentStatus = response.data.status;
                    $localStorage.curriculumContent = $scope.curriculumContent = response.data.msg.curriculum;
                    if(parseInt(preview_value) == 1){
                        $localStorage.previewCurriculumID = $localStorage.curriculumContent[0].curriculum_id;
                    }
                    $scope.gtr();
                    $scope.preloaderVisible = false;
                } else {
                    $scope.curriculumContentStatus = response.data.status;
                    $localStorage.curriculumContent = $scope.curriculumContent = "";
                    console.log(response.data);
                    $scope.preloaderVisible = false;
                }
            }, function (response) {
                $scope.curriculumContentStatus = response.data.status;
                $localStorage.curriculumContent = $scope.curriculumContent = "";
                $scope.preloaderVisible = false;
                console.log(response.data);
            });
        };

        $scope.curriculum = function () {
            $scope.getCurriculum('1');
        };

        $scope.moveToCurriculumDetail = function () {
            $scope.showcurriculum = true;
        };


        $scope.gtr = function () {
            $http({
                method: 'GET',
                url: urlservice.url + 'getcurriculumcontentdetails',
                params: {
                    "company_id": $localStorage.company_id,
                    "curriculum_id": $localStorage.previewCurriculumID
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $scope.curriculumCStatus = response.data.status;
                    $scope.curriculumDetail = response.data.msg.curriculum_content;
                } else {
                    $scope.curriculumDetail = "";
                    $scope.curriculumCStatus = response.data.status;
                    console.log(response.data);
                }
            }, function (response) {
                $scope.curriculumCStatus = response.data.status;
                $scope.curriculumDetail = "";
                console.log(response.data);
            });
        };

        $scope.gtrr = function (jio) {

            $http({
                method: 'GET',
                url: urlservice.url + 'getcurriculumcontentdetails',
                params: {
                    "company_id": $localStorage.company_id,
                    "curriculum_id": $scope.curriculumContent[jio].curriculum_id
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8'},
                    withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $scope.curriculumCStatus = response.data.status;
                    $scope.curriculumDetail = response.data.msg.curriculum_content;
                } else {
                    $scope.curriculumCStatus = response.data.status;
                    $scope.curriculumDetail = "";
                    console.log(response.data);
                }
            }, function (response) {
                $scope.curriculumCStatus = response.data.status;
                $scope.curriculumDetail = "";
                console.log(response.data);
            });
        };
        
         $scope.checkdatetimevalue = function (dat) {
            if (dat) {
                var timecheck = dat.split(" ");
                if (timecheck[1] === "00:00:00") {
                    return 1;
                } else {
                    return 0;
                }
            }
        };
                                                
        $scope.getstartDate = function (dat) {
            if (dat) {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari) {
                    var safaritimecheck = dat.split(" ");
                    if (safaritimecheck[1] === "00:00:00") {
                        var ddateonly = new Date(safaritimecheck[0].replace(/-/g, "/"));
                        var dateStr = new Date(ddateonly);
                        return dateStr;
                    } else {
                        var dda = dat.toString();
                        dda = dda.replace(/-/g, "/");
                        var time = new Date(dda).toISOString();   // For date in safari format
                        return time;
                    }
                } else {
                    var datetimecheck = dat.split(" ");
                    if (datetimecheck[1] === "00:00:00") {
                        var ddateonly = datetimecheck[0] + 'T00:00:00+00:00';
                        var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                        return newDate;
                    } else {
                        var ddateonly = datetimecheck[0] + 'T' + datetimecheck[1] + '+00:00';
                        var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                        return newDate;

                    }
                }
            }
        };

        $scope.getstartEditDate = function (da) {
            var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                   navigator.userAgent && !navigator.userAgent.match('CriOS');
           if(isSafari){
            var dateString = da;
            var d = dateString.replace(' ', 'T');
            return d;
           }else{
            var dateString = da;
            var d = new Date(dateString.replace(' ', 'T'));
            return d;
           }
           
        };

        $scope.formatDate = function (date) {
            var date = date.split("-").join("/");
            var dateOut = new Date(date);
            return dateOut;
        };
        
        $scope.$watch(function () {return $localStorage.processing_fee_percentage;}, function (newVal, oldVal) {     
          $scope.process_fee_per = parseFloat(newVal);
        });
        
        $scope.$watch(function () {return $localStorage.processing_fee_transaction;}, function (newVal, oldVal) {            
          $scope.process_fee_val = parseFloat(newVal);
        });
        
        $scope.TotalEventProcessingCost = function (total_amount) {
            var process_total_cost1 = (parseFloat(total_amount) + (((parseFloat(total_amount) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val)));
            var process_total_cost = parseFloat(total_amount) + (((parseFloat(process_total_cost1) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));                
            return process_total_cost;
        };
        
        $scope.getProcessingCost = function (total_amount) {
            var process_cost1 = (parseFloat(total_amount) + (((parseFloat(total_amount) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val)));
            var process_cost = (((parseFloat(process_cost1) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
            return process_cost;
        };
        
        $scope.absorbFeeCost = function (total_amount) {
            var process_cost = ((parseFloat(total_amount) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val);
            return process_cost;
        };
        
        $scope.resetcartdetails = function(){
            if(($scope.event_type==='S' && ($scope.total_quantity>0 || $scope.total_quantity === undefined || $scope.total_quantity=== null || $scope.total_quantity === '')) || ($scope.event_type!=='S')){
                f7.closePanel();
                f7.confirm(' Discard cart items?', 'Alert', function () {
                    $scope.resetForm();
                    $scope.paymentform.$setPristine();
                    $scope.paymentform.$setUntouched();
//                    document.getElementById("paymentform").reset();
                    $scope.preloader = false;
                    $scope.reg_col_names = [];
                    $scope.waiver_text = '';
                    $scope.processing_fee_type = '';
                    $scope.onetime_payment_flag = 'N';
                    $scope.recurring_payment_flag = 'N';
                    $scope.total_order_amount = '';
                    $scope.total_order_quantity = '';
                    $scope.total_deposit_amount = 0;
                    $scope.total_number_of_payments = '';
                    $scope.total_payment_startdate = '';
                    $scope.total_payment_frequency = '';
                    $scope.po_total_amount = 0;
                    $scope.po_view_total_amount = 0;
                    if($scope.event_type==='S'){
                        $scope.sevent_quantity = 0;
                        $scope.sdiscount_value  = 0;
                        $scope.sdiscount_code_value  = '';
                        $scope.sevent_discount_array = [];
                        $scope.sdiscount_code_display = true;
                        $scope.sdiscount_value_display = false;
                        $scope.singleEventDetails = {};
                        $scope.sevent_id = '';
                        $scope.sevent_cost = '';
                        $scope.sevent_title = '';
                        $scope.sevent_desc = '';
                        $scope.sevent_banner_img_url = '';
                        $scope.sevent_begin_dt = '';
                        $scope.sevent_end_dt = '';
                        $scope.sevent_more_detail_url='';
                        $scope.sevent_video_detail_url='';
                        
                        $scope.single_total_amount = 0;
                        $scope.single_payment_amount = 0;
                        $scope.total_quantity = 0;
                        $scope.final_payment_amount = 0;
                    }else{
                        $scope.childeventContentStatus = 'Failure';
                        $scope.eventChildContent = "";
                        $scope.eventChildBannerImage = "";
                        $scope.childcartcontents = [];
                        $scope.mevent_discount_array = [];
                        $scope.cevent_quantity = 1;
                        $scope.cevent_title = '';
                        $scope.cevent_desc = '';
                        $scope.mdiscountcode = true;
                        $scope.mdiscountval = false;
                        $scope.mdiscount_value  = 0;
                        $scope.mdiscount_code_value  = '';
                        $scope.mevent_title = '';
                        $scope.mevent_desc = '';
                        
                        $scope.multiple_total_amount = 0;
                        $scope.multiple_payment_amount = 0;
                        $scope.total_quantity = 0;
                        $scope.final_payment_amount = 0;
                    }
                    $scope.$apply();
                    var eventView = f7.addView('.event-main', {
                        domCache: true,  dynamicNavbar: true
                    });
                    // Load page:                                       
                    eventView.router.loadPage({
                        pageName: 'index'
                    });
                });
            }else{
                var eventView = f7.addView('.event-main', {
                    domCache: true,  dynamicNavbar: true
                });
                // Load page: 
                eventView.router.loadPage({
                    pageName: 'index'
                });
            }
        };
        
        $scope.updateSingleTotalAmount = function(){
            $scope.single_processing_fee_value = 0;
            $scope.po_single_total_amount = 0;
            $scope.single_payment_view = 0;
            var process_fee_per=0;
            var process_fee_val=0;
            $scope.single_payment_amount = $scope.sevent_cost;
            
            var amount = $scope.sevent_quantity * $scope.single_payment_amount;
            $scope.po_single_total_amount = parseFloat(amount);
            
            if($scope.newDEventProcesFee === 2 || $scope.newDEventProcesFee === '2'){
                var process_fee_per = $scope.process_fee_per;
                var process_fee_val = $scope.process_fee_val;
                if(parseFloat(amount)>0){
                    var process_fee = (parseFloat(amount)) + ((parseFloat(amount) * parseFloat(process_fee_per))/100) + parseFloat(process_fee_val);
                    $scope.single_processing_fee_value = ((parseFloat(process_fee) * parseFloat(process_fee_per))/100) + parseFloat(process_fee_val);
                }else{
                    $scope.single_processing_fee_value = 0;
                }
                $scope.single_total_amount = parseFloat(amount) + $scope.single_processing_fee_value;
            }else{
                $scope.single_total_amount = amount;
                $scope.single_absorb_fee_value = ((parseFloat(amount) * parseFloat(process_fee_per))/100) + parseFloat(process_fee_val);
            }
            $scope.single_payment_view = $scope.single_total_amount;
            
        };
        
        $scope.updateMultipleTotalAmount = function(){
            $scope.multiple_processing_fee_value = 0;
            $scope.po_multiple_total_amount = 0;
            $scope.multiple_payment_view = 0;
            var process_fee_per=0;
            var process_fee_val=0;
            $scope.multiple_payment_amount = $scope.cevent_cost;
            
            var amount = $scope.cevent_quantity * $scope.multiple_payment_amount;
            $scope.po_multiple_total_amount = parseFloat(amount);
            
            if($scope.newDCEventProcesFee === 2 || $scope.newDCEventProcesFee === '2'){
                var process_fee_per = $scope.process_fee_per;
                var process_fee_val = $scope.process_fee_val;
                if(parseFloat(amount)>0){
                    var process_fee = (parseFloat(amount)) + ((parseFloat(amount) * parseFloat(process_fee_per))/100) + parseFloat(process_fee_val);
                    $scope.multiple_processing_fee_value = ((parseFloat(process_fee) * parseFloat(process_fee_per))/100) + parseFloat(process_fee_val);
                }else{
                    $scope.multiple_processing_fee_value = 0;
                }
                $scope.multiple_total_amount = parseFloat(amount) + $scope.multiple_processing_fee_value;
            }else{
                $scope.multiple_total_amount = amount;
                $scope.multiple_absorb_fee_value = ((parseFloat(amount) * parseFloat(process_fee_per))/100) + parseFloat(process_fee_val);
            }
            $scope.multiple_payment_view = $scope.multiple_total_amount;
            
        };
        
        $scope.setDefault_startDate_edit = function () {
            if($scope.newMemStructure === 'SE'){
                    var curr_date = new Date();
                    var current_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                    var specific_start_date = $scope.newSEdateFormat($scope.newSEStartDate);
                    var specific_end_date = $scope.newSEdateFormat($scope.newSEEndDate);
                    if(+specific_start_date < +current_date){ 
                        var curr_date = new Date(); 
                        $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);
                    }else{ 
                        var curr_date = $scope.newSEdateFormat($scope.newSEStartDate); 
                        $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);
                    } 
                    $scope.payment_end_date = specific_end_date.getFullYear() + '-' + ('0' + (specific_end_date.getMonth()+1)).slice( - 2) + '-' + ('0' + specific_end_date.getDate()).slice( - 2);
            }else{
                var curr_date = new Date();
                $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);
            }  
            
            var current_day = $scope.newSEdateFormat($scope.payment_start_date);
            var maxdate = $scope.newMemStructure === 'SE' ? $scope.newSEdateFormat($scope.payment_end_date) : null ;
            $scope.setStartEndDateEdit(current_day, maxdate); 

            var memberView = f7.addView('.membership-main', {
                domCache: true, dynamicNavbar: true
            });
            // Load page: 
            memberView.router.loadPage({
                pageName: 'editchildmembershipdateselect'
            });
        };
        
        $scope.setDefault_startDate = function () {      
            if($scope.membership_structure_datepicker === 'SE'){
                    var curr_date = new Date();
                    var current_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                    var specific_start_date = $scope.newSEdateFormat($scope.specific_start_date);
                    var specific_end_date = $scope.newSEdateFormat($scope.specific_end_date);
                    if(+specific_start_date < +current_date){ 
                        var curr_date = new Date(); 
                        $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);
                    }else{ 
                        var curr_date = $scope.newSEdateFormat($scope.specific_start_date); 
                        $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);
                    } 
                    $scope.payment_end_date = specific_end_date.getFullYear() + '-' + ('0' + (specific_end_date.getMonth()+1)).slice( - 2) + '-' + ('0' + specific_end_date.getDate()).slice( - 2);
            }else{
                var curr_date = new Date();
                $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2);
            }  
            
            var current_day = $scope.newSEdateFormat($scope.payment_start_date);
            var maxdate = $scope.membership_structure_datepicker === 'SE' ? $scope.newSEdateFormat($scope.payment_end_date) : null ;
            $scope.setStartEndDate(current_day, maxdate); 

            var memberView = f7.addView('.membership-main', {
                domCache: true, dynamicNavbar: true
            });
            // Load page: 
            memberView.router.loadPage({
                pageName: 'childmembershipdateselect'
            });
            
        };

        $scope.dateformat = function (date) {
            var Date_Format = date.toString();
            var Display_Format = Date_Format.split(' ');
            var newdate = Display_Format[0] + ", " + Display_Format[1] + " " + Display_Format[2] + " " + Display_Format[3];
            return newdate;
        }    
        
         //  INITIALIZING THE MEMBERSHIP DATEPICKER
        var calendarInline1; var calendarInline; var calendarInline2; var calendarInline3; var $$ = Dom7;
        
        $scope.setStartEndDateEdit = function(current_day, maxdate){            
        //  INITIALIZING THE MEMBERSHIP DATEPICKER 
            if(calendarInline1 !== undefined){
                calendarInline1.destroy();
                document.getElementById("calendar-inline-container1").innerHTML = '';
            }
            
            var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var today = new Date(current_day);
            var yesterday = new Date(current_day);
            yesterday.setDate(yesterday.getDate() - 1);
            var enddate = maxdate;
            
            console.log('current_day : '+current_day);
            console.log('maxdate : '+maxdate);
            console.log('yesterday : '+yesterday);
            console.log('enddate : '+enddate);
            calendarInline1 = f7.calendar({
                container: '#calendar-inline-container1',
                minDate: today,
                maxDate: enddate,
                value: [today],
                weekHeader: true,
                firstDay:0,
                toolbarTemplate:
                        '<div class="toolbar calendar-custom-toolbar">' +
                        '<div class="toolbar-inner">' +
                        '<div class="left">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
                        '</div>' +
                        '<div class="center"></div>' +
                        '<div class="right">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
                        '</div>' +
                        '</div>' +
                        '</div>',
                onOpen: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                    $$('.calendar-custom-toolbar .left .link').on('click', function () {
                        calendarInline1.prevMonth();
                    });
                    $$('.calendar-custom-toolbar .right .link').on('click', function () {
                        calendarInline1.nextMonth();
                    });
                },
                onMonthYearChangeStart: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                },
                onDayClick: function (p, dayContainer, year, month, day) {
                    var selecteddate = new Date(year, month, day);
                    $scope.edit_payment_start_date = year + '-' + ('0' + (parseInt(month) + parseInt(1))).slice(-2) + '-' + ('0' + day).slice(-2);
                }
            });
        }; 
       
        
        $scope.setStartEndDate = function(current_day, maxdate){
        //  INITIALIZING THE MEMBERSHIP DATEPICKER 
            if(calendarInline !== undefined){               
                calendarInline.destroy();
                document.getElementById("calendar-inline-container").innerHTML = '';
            }
            
            var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var today = new Date(current_day);
            var yesterday = new Date(current_day);
            yesterday.setDate(yesterday.getDate() - 1);
            var enddate = maxdate;
            
            console.log('current_day : '+current_day);
            console.log('maxdate : '+maxdate);
            console.log('yesterday : '+yesterday);
            console.log('enddate : '+enddate);
            calendarInline = f7.calendar({
                container: '#calendar-inline-container',
                minDate: today,
                maxDate: enddate,
                value: [today],
                weekHeader: true,
                firstDay:0,
                toolbarTemplate:
                        '<div class="toolbar calendar-custom-toolbar">' +
                        '<div class="toolbar-inner">' +
                        '<div class="left">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
                        '</div>' +
                        '<div class="center"></div>' +
                        '<div class="right">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
                        '</div>' +
                        '</div>' +
                        '</div>',
                onOpen: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                    $$('.calendar-custom-toolbar .left .link').on('click', function () {
                        calendarInline.prevMonth();
                    
                    });
                    $$('.calendar-custom-toolbar .right .link').on('click', function () {
                        calendarInline.nextMonth();
                    });
                },
                onMonthYearChangeStart: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                },
                onDayClick: function (p, dayContainer, year, month, day) {
                    var selecteddate = new Date(year, month, day);
                    $scope.payment_start_date = year + '-' + ('0' + (parseInt(month) + parseInt(1))).slice(-2) + '-' + ('0' + day).slice(-2);
                }
            });
        };
        
        //  INITIALIZING THE TRIAL DATEPICKER        
        
        $scope.setTrialStartDate = function(){   
            if(calendarInline2 !== undefined){
                calendarInline2.destroy();
                document.getElementById("calendar-inline-container2").innerHTML = '';
            }
            
            var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            
            var curr_date = new Date();
            var current_day = $scope.newSEdateFormat(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2));
            var today = new Date(current_day);
            
            calendarInline2 = f7.calendar({
                container: '#calendar-inline-container2',
                minDate: today,
                value: [today],
                weekHeader: true,
                firstDay:0,
                toolbarTemplate:
                        '<div class="toolbar calendar-custom-toolbar">' +
                        '<div class="toolbar-inner">' +
                        '<div class="left">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
                        '</div>' +
                        '<div class="center"></div>' +
                        '<div class="right">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
                        '</div>' +
                        '</div>' +
                        '</div>',
                onOpen: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                    $$('.calendar-custom-toolbar .left .link').on('click', function () {
                        calendarInline2.prevMonth();
                    });
                    $$('.calendar-custom-toolbar .right .link').on('click', function () {
                        calendarInline2.nextMonth();
                    });
                },
                onMonthYearChangeStart: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                },
                onDayClick: function (p, dayContainer, year, month, day) {
                    $scope.trial_start_date = year + '-' + ('0' + (parseInt(month) + parseInt(1))).slice(-2) + '-' + ('0' + day).slice(-2);
                    
                    var dupdate = $scope.safariiosdate($scope.trial_start_date);
                    var trialenddatequantity, trial_end;
                    if ($scope.triallengthtype == 'W') {
                        trialenddatequantity = $scope.triallength * 7;   
                        dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                        trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
                    } else if ($scope.triallengthtype == 'D'){
                        trialenddatequantity = $scope.triallength;
                        dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                        trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
                    }
                    $scope.trial_start_date_view = $scope.dateString($scope.trial_start_date);
                    $scope.trial_end_date_view = $scope.dateString(trial_end);
                }
            });
        }; 
        
        //  INITIALIZING THE EDIT TRIAL DATEPICKER        
        
        $scope.setEditTrialStartDate = function(){   
            if(calendarInline3 !== undefined){
                calendarInline3.destroy();
                document.getElementById("calendar-inline-container3").innerHTML = '';
            }
            
            var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            
            var curr_date = new Date();
            var current_day = $scope.newSEdateFormat(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2));
            var today = new Date(current_day);
            
            calendarInline3 = f7.calendar({
                container: '#calendar-inline-container3',
                minDate: today,
                value: [today],
                weekHeader: true,
                firstDay:0,
                toolbarTemplate:
                        '<div class="toolbar calendar-custom-toolbar">' +
                        '<div class="toolbar-inner">' +
                        '<div class="left">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
                        '</div>' +
                        '<div class="center"></div>' +
                        '<div class="right">' +
                        '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
                        '</div>' +
                        '</div>' +
                        '</div>',
                onOpen: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                    $$('.calendar-custom-toolbar .left .link').on('click', function () {
                        calendarInline3.prevMonth();
                    });
                    $$('.calendar-custom-toolbar .right .link').on('click', function () {
                        calendarInline3.nextMonth();
                    });
                },
                onMonthYearChangeStart: function (p) {
                    $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ', ' + p.currentYear);
                },
                onDayClick: function (p, dayContainer, year, month, day) {
                    $scope.edit_trial_start_date = year + '-' + ('0' + (parseInt(month) + parseInt(1))).slice(-2) + '-' + ('0' + day).slice(-2);
                    
                    var dupdate = $scope.safariiosdate($scope.edit_trial_start_date);
                    var trialenddatequantity, trial_end;
                    if ($scope.newTrialProgramPeriod == 'W') {
                        trialenddatequantity = $scope.newTrialProgramQuantity * 7;   
                        dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                        trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
                    } else if ($scope.newTrialProgramPeriod == 'D'){
                        trialenddatequantity = $scope.newTrialProgramQuantity;
                        dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                        trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
                    }
                    $scope.edit_trial_start_date_view = $scope.dateString($scope.edit_trial_start_date);
                    $scope.edit_trial_end_date_view = $scope.dateString(trial_end);
                }
            });
        };

        
    }]);

MyApp.filter('date2', ['$filter' ,function ($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function (input, format) {
//        console.log(input, format);
        if(input){
        var dtfilter = $filter('date')(input, format);
        var day = parseInt($filter('date')(input, 'dd'));
        var relevantDigits = (day < 30) ? day % 20 : day % 30;
//        console.log(day, relevantDigits);
        var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
        return dtfilter.replace('oo', suffix);
    }
    };
}]);

// MyApp.filter('utcToLocal', function () {
//     return function (ddate) {
//         var date = new Date(ddate);
//         date = new Date(date + " UTC").toString();
//         var localdtime = date.split(" ");
//         var localdatetime = localdtime[0] + ', ' + localdtime[1] + ' ' + localdtime[2] + ' ' + localdtime[3] + ',  ' + localdtime[4].slice(0, 5);
//         return localdatetime;
//     };
// });


MyApp.filter("trust", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);

MyApp.directive('compileTemplate', ['$compile','$parse',function($compile, $parse){
    return {
          link: function(scope, element, attr){
            var parsed = $parse(attr.ngBindHtml);
          function getStringValue() { return (parsed(scope) || '').toString(); }

            //Recompile if the template changes
          scope.$watch(getStringValue, function() {
                $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
            });
        }
    }
}]);
//compile-template
