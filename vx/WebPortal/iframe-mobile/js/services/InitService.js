/*jslint browser: true*/
/*global console, Framework7, MyApp, $document*/
var MyApp =angular.module('ini', []);
var f7 = new Framework7();
MyApp.fw7 = {
  app : new Framework7({
    //animateNavBackIcon: true
    pushState: true,
    swipeBackPage: true,
    swipeBackPageAnimateShadow:true,
    swipeBackPageAnimateOpacity:true,
    cache: false
  }),
  options : {
                                                              dynamicNavbar: true,
                                                      //    domCache: true
                                                          },

  views : []
  
};

var $$ = Dom7;
MyApp.factory('InitService', ['$document', function ($document) {
  'use strict';

  var pub = {},
    eventListeners = {
      'ready' : []
    };
  
  pub.addEventListener = function (eventName, listener) {
    eventListeners[eventName].push(listener);
  };

  function onReady() {
    var fw7 = MyApp.fw7,
      i;

    fw7.views.push(fw7.app.addView('.view-main', {
    domCache: true, dynamicNavbar: true //enable inline pages
    },fw7.options));
    
    fw7.views.push(fw7.app.addView('.event-main', {
    domCache: true, dynamicNavbar: true //enable inline pages
    },fw7.options));
    
     fw7.views.push(fw7.app.addView('.membership-main', {
    domCache: true, dynamicNavbar: true //enable inline pages
    },fw7.options));
    
    fw7.views.push(fw7.app.addView('.trial-main', {
    domCache: true, dynamicNavbar: true //enable inline pages
    },fw7.options));
    
    for (i = 0; i < eventListeners.ready.length; i = i + 1) {
      eventListeners.ready[i]();
    }
  }
  
  // Init
  (function () {
    $document.ready(function () {

      if (document.URL.indexOf("http://") === -1 && document.URL.indexOf("https://") === -1) {
        // Cordova
        console.log("Using Cordova/PhoneGap setting");
        document.addEventListener("deviceready", onReady, false);
      } else {
        // Web browser
        console.log("Using web browser setting");
        onReady();
      }
      
    });
  }());

  return pub;
  
}]);