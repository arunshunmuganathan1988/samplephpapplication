var app = angular.module('videoupload',[]);

app.controller("VideoUploadController",['$scope','$http',function($scope,$http){
          
          $('#progress-full').hide();
          $scope.sizeLimit= 10585760; // 10MB in Bytes
          $scope.uploadProgress = 0;
//          $localStorage.company_id = "0";
//          $localStorage.user_id = "0";          
          $scope.bucketFolder = "company_0";
          $scope.creds          = {
              bucket: 'mystudio-test.technogemsinc.com/'+$scope.bucketFolder,
//              bucket: 'mystudio-test-private/'+$scope.bucketFolder,
              access_key: 'AKIAI3W3VQSR2VDGYZHA',
              secret_key: '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'
          };


$scope.videodetailsupload = function(){
        $http({
                        method: 'POST',
                        url: 'http://mystudio-test.technogemsinc.com/Api/PortalApi/v2/addvideos',
                        data: {
                            "category_name":$scope.category_name,
                            "title": $scope.title,
                            "description": $scope.description,
                            "thum_nail_url":$scope.s3ThumbImageUrl,
                            "video_url":$scope.s3Videourl,
                            "company_id": '0'
                        },
                        headers:{"Content-Type":'application/json; charset=utf-8',
                                 'Access-Control-Allow-Origin': '*'}
                        
                    }).then(function (response) {
                        if(response.data.status == 'Success'){
                            $('#progress-full').hide();
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Message');
                            $("#messagecontent").text(response.data.msg);
                            $('#prevwimge').attr('src', '');
                            $scope.category_name = $scope.title = $scope.description = $scope.s3ThumbImageUrl = $scope.s3Videourl = "";
                        }else{
                            console.log(response.data);
                            $('#progress-full').hide();
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Message');
                            $("#messagecontent").text(response.data.msg);
                        }   
                    }, function (response) {
                            $('#progress-full').hide();
                            console.log(response.data);
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Server Message');
                            $("#messagecontent").text('Invalid server response');
                    });
     };
     
 
    $scope.videofileup = function(){ 
         $('#progress-full').show();
    AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
    AWS.config.region = 'us-east-1';
    var bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });
    if($scope.file) {
            var fileSize = Math.round(parseInt($scope.file.size));

            // Prepend Unique String To Prevent Overwrites $scope.file.name
            $scope.fileExtension = $scope.file.name.substring($scope.file.name.indexOf(".") + 1);
              var uniqueFileName =  'C0U0-' +$scope.uniqueString()+'.'+$scope.fileExtension;
              
//            var uniqueFileName =  'C'+$localStorage.company_id+'U'+$localStorage.user_id+'-' +$scope.uniqueString()+'.'+$scope.fileExtension; 

            var params = { Key: uniqueFileName, ContentType: $scope.file.type, Body: $scope.file, ServerSideEncryption: 'AES256' };
            
            bucket.putObject(params, function(err, data) {
                  if(err) {
                      //muthu
                      $("#failuremessageModal").modal('show');
                      $("#failuremessagetitle").text('Message');
                      $("#failuremessagecontent").text(err.message,err.code);
                    return false;
                  }
                  else {
                    // Upload Successfully Finished
                    console.log(JSON.stringify(data));
                    $scope.uniqueVideoName = uniqueFileName;
                    $scope.s3Videourl = "https://s3.amazonaws.com/mystudio-test.technogemsinc.com/"+$scope.bucketFolder+"/"+uniqueFileName;
                    $scope.videoThumbUpload();
                    // Reset The Progress Bar
                    setTimeout(function() {
                      $scope.uploadProgress = 0;
                      $scope.$digest();
                    }, 4000);
                  }
            })
            .on('httpUploadProgress',function(progress) {
              $scope.uploadProgress = Math.round(progress.loaded / progress.total * 100);
              $scope.$digest();
            });
      }
      else {
        // No File Selected
                  $("#failuremessageModal").modal('show');
                  $("#failuremessagetitle").text('Message');
                  $("#failuremessagecontent").text('Please select a file to upload');
      }
    };
                    
    
    $scope.videoThumbUpload = function(){
//        alert('thum');
        $('#progress-full').show();
        AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
        AWS.config.region = 'us-east-1';
        var bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });
        
            if($scope.file) {

            var binary = atob($scope.VideoThumb.split(',')[1]);
            var array = [];
            for(var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            $scope.ufile = new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
            var uFileName = 'C0U0-' +$scope.uniqueString() + '.jpeg';  
            
//            var uFileName = 'C'+$localStorage.company_id+'U'+$localStorage.user_id+'-' +$scope.uniqueString() + '.jpeg';            
            
            var params = { Key: uFileName, ContentType: $scope.ufile.type, Body: $scope.ufile, ServerSideEncryption: 'AES256' };
            
            
            bucket.putObject(params, function(err, data) {
              if(err) {
                  //muthu
                  $("#failuremessageModal").modal('show');
                  $("#failuremessagetitle").text('Message');
                  $("#failuremessagecontent").text(err.message,err.code);
//                toastr.error(err.message,err.code);
                return false;
              }
              else {
                // Upload Successfully Finished
                $("#cancel").removeAttr("disabled");
                $("#uploadImage").removeAttr("disabled");
                console.log(JSON.stringify(data));
//                toastr.success('IMAGE Uploaded Successfully', 'Done');
                $scope.uniqueThumbName = uFileName;
//                alert($scope.uniqueThumbName+' File name image: '+uFileName);
                $scope.s3ThumbImageUrl = "https://s3.amazonaws.com/mystudio-test.technogemsinc.com/"+$scope.bucketFolder+"/"+uFileName;
//                alert($scope.s3ThumbImageUrl+' Final img Url');
                $scope.videodetailsupload();
            }
        })
        }
    };
    
    $scope.videoFileChange = function(){
//        alert('video file '+$scope.file);
        if($scope.file =="" || $scope.file==undefined){
            //first time
        }
        else{
            $scope.createImage="";
            $('#prevwimge').attr('src', '');
        }
    };
//    
    $scope.callingClose = function(){
         $scope.category_name = $scope.title = $scope.description = $scope.s3ThumbImageUrl = $scope.s3Videourl = $scope.file ="";
         $scope.curriculumvideo.$setPristine();
         $scope.curriculumvideo.$setUntouched();
         $('#prevwimge').attr('src', '');
    };
//    
    $scope.videoCancel = function(){
//        $scope.vSize = false;
        $scope.category_name = $scope.title = $scope.description = $scope.s3ThumbImageUrl = $scope.s3Videourl = $scope.file ="";
        $scope.curriculumvideo.$setPristine();
        $scope.curriculumvideo.$setUntouched();
        //document.getElementById('previewdiv').innerHTML = "";
        $('#prevwimge').attr('src', '');
    };
//
    $scope.fileSizeLabel = function() {
    // Convert Bytes To MB
    return Math.round($scope.sizeLimit / 1024 / 1024) + 'MB';
    };

    $scope.uniqueString = function() {
        var text     = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 8; i++ ) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    };
//    
     //Image Conversion - Uploaded Video Image View on Modal Upload Video Tab
   document.getElementById('swupload').addEventListener('change', function(event) {
            // alert('1');
    var file = event.target.files[0];
    var fileReader = new FileReader();
 
    fileReader.onload = function() {
          var blob = new Blob([fileReader.result], {type: file.type});
          var url = URL.createObjectURL(blob);
          var video = document.createElement('video');
          var timeupdate = function() {
            if (snapImage()) {
              video.removeEventListener('timeupdate', timeupdate);
              video.pause();
            }
          };
          video.addEventListener('loadeddata', function() {
            if (snapImage()) {
              video.removeEventListener('timeupdate', timeupdate);
            }
          });
          var snapImage = function() {
            var canvas = document.createElement('canvas');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
            var image = canvas.toDataURL();
            $scope.VideoThumb = canvas.toDataURL();
            var success = image.length > 100000;
            if (success) {
            var img = document.getElementById('prevwimge');
             img.src = image;
             $scope.createImage = image;
             URL.revokeObjectURL(url);
            }
            return success;
          };
          video.addEventListener('timeupdate', timeupdate);
          video.preload = 'metadata';
          video.src = url;
          // Load video in Safari / IE11
          video.muted = true;
          video.playsInline = true;
          video.play();
        };
        fileReader.readAsArrayBuffer(file);

       });  

 }]);
 
 //video upload
    app.directive('file', function() {
      return {
        restrict: 'AE',
        controller:"VideoUploadController",
        scope: {
          file: '@'
        },
        link: function(scope, el, attrs){
          el.bind('change', function(event){
//              alert('file choose '+scope.vSize);
//              scope.vSize = false;
//              alert('file choose '+scope.vSize);
            var files = event.target.files;
            var file = files[0];
            scope.file = file;
            scope.$parent.file = file;
            scope.$apply();
          });
        }
      };
    });
    