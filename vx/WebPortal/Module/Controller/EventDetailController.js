
EventDetailController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$rootScope'];
function EventDetailController($scope, $location, $http, $localStorage, urlservice, $filter, $rootScope)
    {
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();            
            angular.element(document.getElementById('sidetopbar')).scope().getStripeStatus();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.sideBarHide = false;
            tooltip.hide();            
            $rootScope.activepagehighlight = 'events';
            $scope.eventdetailsview = true;
            $scope.participantinfoview = false;
            $scope.eventhistoryview = false;
            $scope.paymenthistoryview = false;
            $scope.currentBuyerName = $localStorage.buyerName;
            $scope.currentParticipantName = $localStorage.participantName; 
            $scope.currentEvent_reg_id = $localStorage.currentEventregid;
            $scope.currentEvent_id = $localStorage.currentEventid;
            $scope.currentEvent_title = $localStorage.current_event_title;
//            $scope.currentEvent_Details = $localStorage.currentOrderDetails;
            $scope.wepaystatus = $localStorage.preview_wepaystatus;
            $scope.wp_currency_code = $localStorage.wp_currency_code;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.onetimecheckout = 0;
            $scope.ccyearlist = ["2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037"];
            $scope.ccmonthlist = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
            $scope.parfirstname = $scope.parlastname = "";
            $scope.buyerfirstname = $scope.buyerlastname = "";
            $scope.showTransferconfirm = false;
            $scope.showEditconfirm = false;
            $scope.showMemCategory = false; 
            $scope.selectOPtionConfirm = false;
            $scope.showParEdit = $scope.showCusParEdit = false;
            $scope.actionValue = '';
            $localStorage.eventpagetype = '';
            $scope.call_back_from = 0;
            $scope.page_From = $localStorage.page_from;
            $scope.showquantityedit = false;
            $scope.editquantityIndex = "";
            $scope.qtyvalue = "";
            $scope.cancellation_button_text = "Process Cancellation";
            $scope.showChangesinCancellation = false;            
//            $scope.currency = '$';
            $scope.cancel_type = '';
            $scope.update_Payment_plan_status = 'N';
            $scope.updatedPaymentArray = [];
            $scope.selectpaymentmethod = 'CC';
            $scope.updatedEventArray = [];
            $scope.planupdated = false;
            $scope.cancel_balance_due = $scope.cancel_process = $scope.cancel_with_currentplan = $scope.cancel_with_newplan = $scope.cancel_with_refund = $scope.cancel_without_refund = false;
            $scope.cancel_status = 'C';
            $scope.creditmethod = 'MC';
            $scope.checknumber = '';
            $scope.stripe_card = $scope.stripe_publish_key = $scope.window_hostname = '';
            $scope.stripe_publish_key = $localStorage.stripe_publish_key;
            
            if ($localStorage.wepay_level === 'P') {
                $scope.wepay_env = "production";
                $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
            } else if ($localStorage.wepay_level === 'S') {
                $scope.wepay_env = "stage";
                $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
            }

            WePay.set_endpoint($scope.wepay_env); //    CHANGE TO "PRODUCTION" WHEN LIVE
            // SHORTCUTS
            var d = document;
            d.id = d.getElementById,
                    valueById = function (id) {
                        return d.id(id).value;
                    };

            // FOR THOSE NOT USING DOM LIBRARIES
            var addEvent = function (e, v, f) {
                if (!!window.attachEvent) {
                    e.attachEvent('on' + v, f);
                } else {
                    e.addEventListener(v, f, false);
                }
            };

            // ATTACH THE EVENT TO THE DOM
            addEvent(d.id('cc-submit4'), 'click', function () {
                $('#progress-full').show();
//                    var userName = [valueById('name')].join(' ');
                var userName = [valueById('firstName')] + ' ' + [valueById('lastName')];
                var user_first_name = valueById('firstName');
                var user_last_name = valueById('lastName');
                var email = valueById('email').trim();
                var postal_code = valueById('postal_code');
                response = WePay.credit_card.create({
                    "client_id": $localStorage.wepay_client_id,
                    "user_name": userName,
                    "email": valueById('email').trim(),
                    "cc_number": valueById('cc-number'),
                    "cvv": valueById('cc-cvv'),
                    "expiration_month": valueById('cc-month'),
                    "expiration_year": valueById('cc-year'),
                    "virtual_terminal": "web",
                    "address": {
                        "country": valueById('country'),
                        "postal_code": valueById('postal_code')
                    }
                }, function (data) {
                    if (data.error) {
                        $scope.onetimecheckout = 0;
                        $('#progress-full').hide();
                        console.log(data);
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Wepay Message');
                        $("#successmessagecontent").text(data.error_description);
                        // HANDLE ERROR RESPONSE
                    } else {
                        if ($scope.onetimecheckout == 0) {
                            $scope.onetimecheckout = 1;
//                                    $('#progress-full').show();
                            $scope.editEventDBPaymentMethod(data.credit_card_id, data.state, user_first_name, user_last_name,email, postal_code);
                            console.log(data);
                        }

                        // CALL YOUR OWN APP'S API TO SAVE THE TOKEN INSIDE THE DATA;
                        // SHOW A SUCCESS PAGE
                    }
                });
            });
            
            $scope.$on('$viewContentLoaded', function () {
                $('#paymenthistorystartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true,
                    startDate: ('setStartDate', new Date())
                });
            });
            
            $scope.formatDateToday = function(){
                var d = new Date,
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                return [year, month, day].join('-');
            };          
            
            
            $scope.Event_Detail = function () {
                $scope.showRemDueProccessingFee = false;
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getParticipantDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "event_id": $scope.currentEvent_id,
                        "event_reg_id": $scope.currentEvent_reg_id                       
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $(".modal-backdrop").removeClass('modal-backdrop fade in');
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        if(response.data.info==='normal'){
                            $("#successmessageModal").modal('show');
                            $("#successmessagetitle").text('Message');
                            $("#successmessagecontent").text(response.data.msg);
                        }else if(response.data.info==='payment plan'){
                            var message = response.data.msg;
                            $scope.current_plan_details = message.current_plan_details;
                            $scope.current_due_details = message.current_plan_details.due_details;
                            $scope.current_event_details = message.event_details;
                            $scope.current_manual_credit_details = message.current_plan_details.manual_credit_details;
                            $scope.buyer_name=message.buyer_name;
                            $scope.participant_name=message.participant_name;
                            
                            $scope.new_plan_details = angular.copy($scope.current_plan_details);
                            $scope.new_due_details = angular.copy($scope.current_due_details);
                            $scope.new_event_details = angular.copy($scope.current_event_details);
                            $scope.new_manual_credit_details = angular.copy($scope.current_manual_credit_details);
                            $scope.remainingDueValue = angular.copy($scope.new_plan_details.rem_due);
                            $scope.show_new_dew_details = true;
                            $scope.eventdetailsExist = true;
                        }else{
//                            $('#progress-full').hide();
                            $("#successmessageModal").modal('show');
                            $("#successmessagetitle").text('Message');
                            $("#successmessagecontent").text('Invalid server response');
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $scope.eventdetailsExist = false;
                        $('#progress-full').hide();                        
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };  
            
            //converting server date format value into string on modal screen
            $scope.getPaymentdate = function (dat) {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari){
                    var dat = dat+' 00:00:00';
                    var dda = dat.toString();
                    dda = dda.replace(/ /g, "T");
                    var time = new Date(dda);
                    var dateonly = time.toString();
                    var fulldateonly = dateonly.replace(/GMT.*/g, "");
                    var localdate = fulldateonly.slice(4, -9);
                    return localdate;
                } else {
                    var ddateonly = dat + 'T00:00:00+00:00';
                    var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                    var localdate = $filter('date')(newDate, 'MMM d, y');
                    return localdate;
                }
            };
            
            $scope.getTotalFee = function(amount, refunded_amount, process_fee, processing_fee_type){
                var total_fee = 0;
                if(processing_fee_type === '2'){
                    total_fee = parseFloat(amount) + parseFloat(refunded_amount) + parseFloat(process_fee);
                }else{
                    total_fee = parseFloat(amount) + parseFloat(refunded_amount);
                }
                return total_fee;
            }
            
            //edit quantity on modal window for order editing
            $scope.editquantity = function(ind){
                if($scope.showquantityedit){
                   $scope.quantityerrortext = "Kindly save the edited quantity.";
                   return;
                }
                $scope.qtyvalue = "";
                $scope.quantityerrortext = "";
                $scope.showquantityedit = true;
                $scope.editquantityIndex = ind;
                $scope.qtyvalue = $scope.new_event_details[ind].quantity;
                $scope.reset_quantity = $scope.current_event_details[ind].quantity;
            };
            
              //cancel quantity edit
            $scope.cancelquantity = function(value, e_id,ind){
                $scope.quantityerrortext = "";
//                $scope.showquantityedit = false;
                $scope.new_event_details[ind].quantity = $scope.reset_quantity;
                $scope.updatequantity(value, e_id, ind);
            };
            
            //update quantity on modal window for order editing
            $scope.updatequantity = function(value, e_id, ind){
                $scope.showChangesinCancellation = true;
                $scope.subtractedText = "";
                $scope.quantityerrortext = "";
                $scope.cancellation_button_text = "Process Cancellation";
                $scope.remDueFeeText = '';
                $scope.remainingDueValue = 0;
                $scope.showRemDueProccessingFee = false;
                $scope.planupdated = false;
                $scope.manual_credit_add_show = false;
                $scope.manual_credit_add = 0;
                $scope.new_due_details = angular.copy($scope.current_due_details);
                $scope.cancel_balance_due = $scope.cancel_process = $scope.cancel_with_currentplan = $scope.cancel_with_newplan = $scope.cancel_with_refund = $scope.cancel_without_refund = false;
                
                if (!value || parseInt(value)<0) {
                    $scope.quantityerrortext = "Please enter the quantity";
                    return;
                }
                if ( parseInt($scope.current_event_details[ind].quantity) < parseInt(value)) {
                    $scope.quantityerrortext = "You cannot increase quantity than registered quantity.";
                    return;
                }
                $scope.showquantityedit = !$scope.showquantityedit;                
                $scope.new_event_details[ind].state = 'U';
                
                var total_event_count = 0, temp_total_event_cost = 0, temp_rem_due = 0, single_payment_amt = 0, succeeded_payment_count = 0, rem_due_count=0, subtractedamount = 0, overall_paid_due = 0, temp_paid_due = 0;
                for(var i=0;i<$scope.new_event_details.length;i++){
                    $scope.new_event_details[i].cancelled_quantity = +$scope.current_event_details[i].quantity - +$scope.new_event_details[i].quantity;
                    total_event_count += +$scope.new_event_details[i].quantity;
                    temp_total_event_cost += (+$scope.new_event_details[i].event_cost- +$scope.new_event_details[i].event_disc)* $scope.new_event_details[i].quantity;
                }
                
//                for(var j=succeeded_payment_count;j<$scope.new_due_details.length;j++){
                if($scope.new_due_details !== undefined && $scope.new_due_details !== null && $scope.new_due_details !== []){
                    for(var j=0;j<$scope.new_due_details.length;j++){
                        $scope.new_due_details[j]['to_refund'] = 0;
                        $scope.new_due_details[j]['edit_status'] = 'N';
                        if($scope.new_due_details[j].event_payment_id === '' || $scope.new_due_details[j].event_payment_id === undefined || $scope.new_due_details[j].status === 'M'){
                            $scope.new_due_details.splice(j, 1);
                            if($scope.new_due_details[j].status === 'M'){
                                j--;
                            }
                        }
                    }
                }else{
                    $scope.new_due_details = [];
                }
                
                $scope.new_plan_details.total_event = total_event_count;
                temp_rem_due = +temp_total_event_cost - +$scope.new_plan_details.paid_due;
                $scope.new_plan_details.total_due = (Math.round((+temp_total_event_cost + +0.00001) * 100) / 100).toFixed(2);
                $scope.new_plan_details.rem_due = (Math.round((+temp_rem_due + +0.00001) * 100) / 100).toFixed(2);
//                console.log("Rem Due : "+ temp_rem_due);
//                console.log("Tot Due : "+ $scope.new_plan_details.total_due);
//                console.log("Man Credit : "+ $scope.current_plan_details.manual_credit);
                if(parseFloat(temp_rem_due)<0){
                    if(parseFloat($scope.new_plan_details.total_due)<parseFloat($scope.current_plan_details.manual_credit)){
                        var temp_due = parseFloat($scope.current_plan_details.paid_due)-parseFloat(temp_rem_due);
                        if(parseFloat(temp_due)==0){
                            $scope.manual_credit_refund = $scope.current_plan_details.manual_credit;
                        }else{
                            $scope.manual_credit_refund = parseFloat($scope.current_plan_details.manual_credit)-parseFloat($scope.new_plan_details.total_due);
                        }
                        $scope.manual_credit_show = true;
                        if($scope.manual_credit_refund>0){
                            var temp_manual_refund = angular.copy(parseFloat($scope.manual_credit_refund));
                            if($scope.new_manual_credit_details !== undefined && $scope.new_manual_credit_details !== '' && $scope.new_manual_credit_details !== null){
                                for(var s=0; s<$scope.new_manual_credit_details.length; s++){
                                    if(parseFloat(temp_manual_refund)>0){
                                        if(parseFloat(temp_manual_refund)>parseFloat($scope.new_manual_credit_details[s]['amount'])-parseFloat($scope.new_manual_credit_details[s]['refunded_amount'])){
                                            temp_manual_refund -= parseFloat($scope.new_manual_credit_details[s]['amount'])-parseFloat($scope.new_manual_credit_details[s]['refunded_amount'])
                                            $scope.new_due_details.unshift($scope.new_manual_credit_details[s]);
                                        }else if(parseFloat(temp_manual_refund)<=parseFloat($scope.new_manual_credit_details[s]['amount'])-parseFloat($scope.new_manual_credit_details[s]['refunded_amount'])){
                                            temp_manual_refund = 0;
                                            $scope.new_due_details.unshift($scope.new_manual_credit_details[s]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    $scope.manual_credit_show = false;
                    $scope.manual_credit_refund = 0;
                }
//              
                subtractedamount = +$scope.current_plan_details.total_due - +$scope.new_plan_details.total_due;
                succeeded_payment_count = $scope.new_plan_details.success_count;
                temp_paid_due = angular.copy($scope.new_plan_details.paid_due);
                var temp_manual_amt = 0;
                for(var x=0;x<$scope.new_event_details.length;x++){
//                    if(parseFloat($scope.new_event_details[x].cancelled_quantity)>0){
                        temp_manual_amt += +$scope.new_event_details[x].manual_payment_amount;
//                    }
                }
                for(var x=0;x<$scope.new_event_details.length;x++){
                    $scope.new_event_details[x].payment_amount = $scope.new_event_details[x].discounted_price * $scope.new_event_details[x].quantity;
                    if($scope.new_event_details[x].payment_amount>0){
                        if(temp_paid_due > $scope.new_event_details[x].payment_amount){
                            $scope.new_event_details[x].state = 'U';
                            $scope.new_event_details[x].paid_due = $scope.new_event_details[x].payment_amount;
                            if(parseFloat(temp_manual_amt)>0){
                                if(parseFloat($scope.new_event_details[x].paid_due)>parseFloat(temp_manual_amt)){
                                    $scope.new_event_details[x].manual_payment_amount = temp_manual_amt;
                                    temp_manual_amt = 0;
                                }else{
                                    temp_manual_amt -= $scope.new_event_details[x].paid_due;
                                    $scope.new_event_details[x].manual_payment_amount = $scope.new_event_details[x].paid_due;
                                }
                            }else{
                                $scope.new_event_details[x].manual_payment_amount = $scope.new_event_details[x].old_manual_payment_amount;
                            }
                            $scope.new_event_details[x].rem_due = 0;
                            temp_paid_due = +temp_paid_due - +$scope.new_event_details[x].payment_amount;
                        }else if(temp_paid_due<=$scope.new_event_details[x].payment_amount){
                            $scope.new_event_details[x].state = 'U';
                            $scope.new_event_details[x].paid_due = temp_paid_due;
                            if(parseFloat(temp_manual_amt)>0){
                                if(parseFloat($scope.new_event_details[x].paid_due)>parseFloat(temp_manual_amt)){
                                    $scope.new_event_details[x].manual_payment_amount = temp_manual_amt;
                                    temp_manual_amt = 0;
                                }else{
                                    temp_manual_amt -= $scope.new_event_details[x].paid_due;
                                    $scope.new_event_details[x].manual_payment_amount = $scope.new_event_details[x].paid_due;
                                }
                            }else{
                                $scope.new_event_details[x].manual_payment_amount = $scope.new_event_details[x].old_manual_payment_amount;
                            }
                            $scope.new_event_details[x].rem_due = +$scope.new_event_details[x].payment_amount - +temp_paid_due;
                            temp_paid_due = 0;
                        }
                    }else{
                        $scope.new_event_details[x].manual_payment_amount = 0;
                        $scope.new_event_details[x].paid_due = 0;
                        $scope.new_event_details[x].rem_due = 0;
                    }
                }
                
                
                $scope.cancellation_without_refund_txt = "Cancel Event Without Refund";
                $scope.cancellation_with_refund_txt = "Cancel Event & Refund";
                $scope.cancellation_with_currentplan_txt = "Cancel Event & Keep Current Payment Plan";
                $scope.cancellation_with_newplan_txt = "Cancel Event & Adopt New Payment Plan";
                $scope.cancellation_button_text = "Process Cancellation";
                
                if($scope.new_plan_details.payment_type=='RE' && (parseFloat($scope.new_plan_details.total_order_amount)>parseFloat($scope.new_plan_details.total_due) || parseInt($scope.new_plan_details.total_order_quantity)>=parseInt($scope.new_plan_details.total_event))){
                    var temp_amount = 0;
                    
                    for(var j=succeeded_payment_count;j<$scope.new_due_details.length;j++){
                        $scope.new_due_details[j].edit_status = 'D';
                        $scope.new_due_details[j].to_refund = 0;
                    }
                    
                    if($scope.new_plan_details.rem_due>0){
                        $scope.show_new_dew_details = false;
                        $scope.cancel_process = $scope.cancel_with_newplan = $scope.cancel_with_refund = $scope.cancel_without_refund = false;
                        $scope.cancel_balance_due = true;    //CANCELLATION AND PROCESS BALANCE DUE
                        $scope.cancel_with_currentplan = true;  //CANCELLATION WITH CURRENT PLAN
                        $scope.cancellation_button_text = "Cancel Event & Process Balance Due";
                        $scope.subtractedText = " ("+$scope.wp_currency_symbol+subtractedamount+" subtracted for event cancellation)";
                        
                        var temp_obj = {};
                        temp_obj.event_payment_id = '';
                        temp_obj.amount = $scope.new_plan_details.rem_due;
                        if($scope.new_plan_details.payment_method!=='CC'){
                            $scope.manual_credit_add_show = true;
                            $scope.manual_credit_add = $scope.new_plan_details.rem_due;
                            temp_obj.process_fee = 0;
                            temp_obj.credit_method = 'CA';
                        }else{
                            $scope.manual_credit_add_show = false;
                            $scope.manual_credit_add = 0;
                            temp_obj.credit_method = 'CC';
                            temp_obj.process_fee = $scope.processProcessingFee($scope.new_plan_details.rem_due, $scope.new_plan_details.processing_fee_type);
                        }
                        temp_obj.date = '';
                        temp_obj.edit_status = 'N';
                        temp_obj.status = 'N';
                        temp_obj.to_refund = 0;
                        $scope.new_due_details.push(temp_obj);
                        
                        $scope.cancel_type = 'PD';   //Plan delete & update all payment subscription
                        
                    }else{
                        if(parseInt($scope.new_plan_details.rem_due)===0){
                            $scope.cancel_balance_due = $scope.cancel_with_newplan = $scope.cancel_with_refund = $scope.cancel_without_refund = false;
                            $scope.cancel_process = true;    //CANCELLATION
                            $scope.cancel_with_currentplan = true;  //CANCELLATION WITH CURRENT PLAN
                            $scope.cancellation_button_text = "Process Cancellation";
                        }else{
                            $scope.cancel_balance_due = $scope.cancel_process = $scope.cancel_with_newplan = $scope.cancel_with_currentplan = false;
                            $scope.cancel_with_refund = true;   //CANCELLATION WITH REFUND
                            $scope.cancel_without_refund = true;  //CANCELLATION WITHOUT REFUND 
                        }
                        $scope.show_new_dew_details = false;
                        $scope.subtractedText = " ("+$scope.wp_currency_symbol+subtractedamount+" subtracted for event cancellation)";
                        
                        temp_amount = subtractedamount;
                        var rem_pay_amount = 0;
                        
                        for(var j=+$scope.new_due_details.length - +1;j>=0;j--){
                            if(temp_amount>0){
                                rem_pay_amount = (+$scope.new_due_details[j].amount - +$scope.new_due_details[j].refunded_amount);
                                if($scope.new_due_details[j].status === 'N'){
                                    $scope.new_due_details[j].edit_status = 'D';
                                    $scope.new_due_details[j].to_refund = 0;
                                    temp_amount -= +rem_pay_amount;
                                }else{
                                    if(temp_amount>=rem_pay_amount){
                                        $scope.new_due_details[j].to_refund = rem_pay_amount;
                                        $scope.new_due_details[j].process_fee = 0;
                                        temp_amount -= +rem_pay_amount;
                                        if($scope.new_due_details[j].status==='M'){
                                            $scope.new_due_details[j].edit_status = 'MU';
                                        }else{
                                            $scope.new_due_details[j].edit_status = 'U';
                                        }
                                    }else{
                                        $scope.new_due_details[j].process_fee = $scope.processProcessingFee(rem_pay_amount, $scope.new_plan_details.processing_fee_type);
                                        $scope.new_due_details[j].to_refund = (Math.round((+temp_amount + +0.00001) * 100) / 100).toFixed(2);
                                        temp_amount = 0;
                                        if($scope.new_due_details[j].status==='M'){
                                            $scope.new_due_details[j].edit_status = 'MU';
                                        }else{
                                            $scope.new_due_details[j].edit_status = 'U';
                                        }
                                    }
                                }
                            }else{
                                $scope.new_due_details[j].to_refund = 0;
                            }
                        }
                        
                        $scope.cancel_type = 'PU';   //Plan Update all payment subscription
                    }
                }else{                    
                    var temp_amount = 0;
                    var rem_pay_amount = 0;
                    rem_due_count = +$scope.new_due_details.length - +succeeded_payment_count;
                    single_payment_amt = (Math.round(((+$scope.new_plan_details.rem_due/rem_due_count) + +0.00001) * 100) / 100).toFixed(2);
                    
                    if($scope.new_plan_details.rem_due > 0){
                        for(var j=succeeded_payment_count;j<$scope.new_due_details.length;j++){
                            $scope.new_due_details[j].amount = single_payment_amt;
                            $scope.new_due_details[j].process_fee = $scope.processProcessingFee($scope.new_due_details[j].amount, $scope.new_plan_details.processing_fee_type);
                            $scope.new_due_details[j].edit_status = 'U';
                        }
                        if($scope.new_plan_details.payment_type=='RE' && parseFloat($scope.new_plan_details.total_order_amount)<parseFloat($scope.new_plan_details.total_due) && parseInt($scope.new_plan_details.total_order_quantity)<parseInt($scope.new_plan_details.total_event)){
                            $scope.cancel_balance_due = $scope.cancel_process = $scope.cancel_with_refund = $scope.cancel_without_refund = false;
                            $scope.cancel_with_currentplan = true; //CANCELLATION WITH CURRENT PLAN
                            $scope.cancel_with_newplan = true;  //CANCELLATION WITH NEW PLAN
                        }else{
                            $scope.cancel_balance_due = $scope.cancel_with_currentplan = $scope.cancel_with_newplan = $scope.cancel_with_refund = $scope.cancel_without_refund = false;
                            $scope.cancel_process = true;
                        }
                        
                        $scope.show_new_dew_details = true;
                        $scope.subtractedText = " ("+$scope.wp_currency_symbol+subtractedamount+" subtracted for event cancellation)";
                        $scope.cancel_type = 'U';   //Update future payment subscription
                    }else{
                        if(parseInt($scope.new_plan_details.rem_due)===0){
                            if($scope.new_plan_details.payment_type=='RE' && parseFloat($scope.new_plan_details.total_order_amount)<parseFloat($scope.new_plan_details.total_due) && parseInt($scope.new_plan_details.total_order_quantity)<parseInt($scope.new_plan_details.total_event)){
                                $scope.cancel_balance_due = $scope.cancel_process = $scope.cancel_with_refund = $scope.cancel_without_refund = false;
                                $scope.cancel_with_currentplan = true; //CANCELLATION WITH CURRENT PLAN
                                $scope.cancel_with_newplan = true;  //CANCELLATION WITH NEW PLAN
                            }else{
                                $scope.cancel_balance_due = $scope.cancel_with_currentplan = $scope.cancel_with_newplan = $scope.cancel_with_refund = $scope.cancel_without_refund = false;
                                $scope.cancel_process = true;
                            }
                        }else{
                            $scope.cancel_balance_due = $scope.cancel_process = $scope.cancel_with_currentplan = $scope.cancel_with_newplan = $scope.cancel_with_refund = $scope.cancel_without_refund = false;
                            if($scope.new_plan_details.payment_type=='CO'){
                                $scope.cancel_with_refund = true;
                                $scope.cancel_without_refund = true;
                            }else{
                                if($scope.new_plan_details.payment_type=='RE' && parseFloat($scope.new_plan_details.total_order_amount)<parseFloat($scope.new_plan_details.total_due) && parseInt($scope.new_plan_details.total_order_quantity)<parseInt($scope.new_plan_details.total_event)){
                                    $scope.cancel_balance_due = $scope.cancel_process = $scope.cancel_with_newplan =  $scope.cancel_with_currentplan = false;
                                    $scope.cancel_with_refund = true;
                                    $scope.cancel_without_refund = true;
                                }else{
                                    $scope.cancel_balance_due = $scope.cancel_process = $scope.cancel_with_currentplan = $scope.cancel_with_newplan = $scope.cancel_with_refund = $scope.cancel_without_refund = false;
                                }
                            }
                        }
                        $scope.show_new_dew_details = false;
                        $scope.subtractedText = " ("+$scope.wp_currency_symbol+subtractedamount+" subtracted for event cancellation)";
                        
                        temp_amount = subtractedamount;
                        
                        for(var j=+$scope.new_due_details.length - +1;j>=0;j--){
                            if(temp_amount>0){
                                rem_pay_amount = (+$scope.new_due_details[j].amount - +$scope.new_due_details[j].refunded_amount);
                                if($scope.new_due_details[j].status === 'N'){
                                    $scope.new_due_details[j].edit_status = 'D';
                                    $scope.new_due_details[j].to_refund = 0;
                                    temp_amount -= +rem_pay_amount;
                                }else{
                                    if(temp_amount>=rem_pay_amount){
                                        $scope.new_due_details[j].to_refund = rem_pay_amount;
                                        $scope.new_due_details[j].process_fee = 0;
                                        temp_amount -= +rem_pay_amount;
                                        if($scope.new_due_details[j].status==='M'){
                                            $scope.new_due_details[j].edit_status = 'MU';
                                        }else{
                                            $scope.new_due_details[j].edit_status = 'U';
                                        }
                                    }else{
                                        $scope.new_due_details[j].process_fee = $scope.processProcessingFee(rem_pay_amount, $scope.new_plan_details.processing_fee_type);
                                        $scope.new_due_details[j].to_refund = (Math.round((+temp_amount + +0.00001) * 100) / 100).toFixed(2);
                                        temp_amount = 0;
                                        if($scope.new_due_details[j].status==='M'){
                                            $scope.new_due_details[j].edit_status = 'MU';
                                        }else{
                                            $scope.new_due_details[j].edit_status = 'U';
                                        }
                                    }
                                }
                            }else{
                                $scope.new_due_details[j].to_refund = 0;
                            }
                        }
                        $scope.cancel_type = 'D';   //Update future payment subscription
                    }
                }                
                if(($scope.current_plan_details.processing_fee_type==='2' || $scope.current_plan_details.processing_fee_type===2) && (parseFloat($scope.new_plan_details.rem_due)<0 || $scope.show_new_dew_details === false ) && $scope.current_plan_details.payment_method==='CC'){
                    $scope.updateRemDueFeeText($scope.new_plan_details.rem_due, $scope.current_plan_details.processing_fee_type);
                }else{
                    $scope.remainingDueValue = angular.copy($scope.new_plan_details.rem_due);
                }
               
                
                var new_plan;
                for (new_plan = 0; new_plan < $scope.new_event_details.length; new_plan++) {
                    if (parseInt($scope.current_event_details[new_plan].quantity) === parseInt($scope.new_event_details[new_plan].quantity)) {
                        $scope.planupdated = false;
                        $scope.update_Payment_plan_status = 'N';
                    } else {
                        $scope.planupdated = true;
                        $scope.update_Payment_plan_status = 'U';
                        return;
                    }
                }
                
            };
            
            //update remaining due after quantity updation
            $scope.updateRemDueFeeText = function(amount, fee_type){
                var rem_due = 0;
                var p_fee = 0;
                if($scope.manual_credit_refund>0){
                    p_fee = $scope.processProcessingFee(parseFloat(amount) + parseFloat($scope.manual_credit_refund), fee_type);
                }else{
                    p_fee = $scope.processProcessingFee(amount, fee_type);
                }
                var process_fee_show = (Math.round((+p_fee + +0.00001) * 100) / 100).toFixed(2);
                if(parseFloat(amount)<0){
                    p_fee *= -1;
                }
                rem_due = +amount + +p_fee;
                $scope.remainingDueValue = (Math.round((+rem_due + +0.00001) * 100) / 100).toFixed(2);
                $scope.remDueFeeText = "includes $"+process_fee_show+" processing fee";
                if(parseFloat($scope.remainingDueValue) == 0){
                    $scope.showRemDueProccessingFee = false;
                }else{
                    $scope.showRemDueProccessingFee = true;
                }
                return;
            };
            
            //processing fee calculation for remaining due amount
            $scope.processProcessingFee = function(amount, fee_type){
                if(parseFloat(amount)<0){
                    amount *= -1; 
                }
                var p1_fee = 0, p2_fee = 0, p_fee = 0;
                var process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                var process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                
                if (parseFloat(amount) !== 0) {
                    p1_fee = +(amount * (process_fee_per / 100)) + +process_fee_val;
                    p2_fee = (+amount + +p1_fee) * (process_fee_per / 100) + +process_fee_val;
                    if (fee_type === 2 || fee_type === '2') {
                        p_fee = p2_fee;
                    } else {
                        p_fee = p1_fee;
                    }
                }
                return Math.round((+p_fee + +0.00001) * 100) / 100;
            };
            
            //refund event payment confirmation
            $scope.confirmPaymentPlanRefund = function(){
                if($scope.update_Payment_plan_status === 'N'){
                    return false;
                }
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'processPaymentPlanRefund',
                    data: {
                        company_id: $localStorage.company_id,
                        event_reg_id: $scope.currentEvent_reg_id,
                        event_array: $scope.new_event_details,
                        plan_array: $scope.new_due_details,
                        total_due: $scope.new_plan_details.total_due,
                        cancel_flag: $scope.cancel_status
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.showquantityedit = false;
                        $scope.editquantityIndex = "";
                        $scope.qtyvalue = "";
                        $scope.subtractedText = "";
                        $scope.quantityerrortext = "";
                        $scope.cancellation_button_text = "Process Cancellation";
                        $scope.showChangesinCancellation = false; 
                        $scope.cancel_type = '';
                        $scope.update_Payment_plan_status = 'N';
                        $scope.updatedPaymentArray = [];
                        $scope.updatedEventArray = [];
                        $scope.planupdated = $scope.recurring_status =  $scope.refund_status = false;
                        $scope.cancel_status = 'C';
                        var manage_event_details = response.data.event_details.msg;
                        if(manage_event_details.child_events.length>0){
                                $scope.childeventarray = manage_event_details.child_events;      
                        }
                        if (response.data.reg_details.status === 'Success') {
                           if(response.data.reg_details.info==='payment plan'){
                                var message = response.data.reg_details.msg;
                                $scope.current_plan_details = message.current_plan_details;
                                $scope.current_due_details = message.current_plan_details.due_details;
                                $scope.current_event_details = message.event_details;
                                $scope.current_manual_credit_details = message.current_plan_details.manual_credit_details;

                                $scope.new_plan_details = angular.copy($scope.current_plan_details);
                                $scope.new_due_details = angular.copy($scope.current_due_details);
                                $scope.new_event_details = angular.copy($scope.current_event_details);
                                $scope.new_manual_credit_details = angular.copy($scope.current_manual_credit_details);
                                $scope.remainingDueValue = angular.copy($scope.new_plan_details.rem_due);
                                $scope.show_new_dew_details = true;
                                $scope.eventdetailsExist = true;
                            }
                        }else{
                            $scope.eventdetailsExist = false;
                        }
                        
                        $('#progress-full').hide(); 
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);                 
                        
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $scope.update_Payment_plan_status = 'N';
                        $scope.showChangesinCancellation = true;
                        $scope.subtractedText = "";
                        $scope.quantityerrortext = "";
                        $scope.new_plan_details = angular.copy($scope.current_plan_details);
                        $scope.new_due_details = angular.copy($scope.current_due_details);
                        $scope.new_event_details = angular.copy($scope.current_event_details);
                        $scope.new_manual_credit_details = angular.copy($scope.current_plan_details.manual_credit_details);
                        $scope.remainingDueValue = angular.copy($scope.new_plan_details.rem_due);
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        if(response.data.error){
                            $("#successmessagetitle").text(response.data.error);
                            $("#successmessagecontent").text(response.data.error_description);
                        }else{
                            $("#successmessagetitle").text('Message');
                            $("#successmessagecontent").text(response.data.msg);
                        }
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            //edit cancellation of refund event payment
            $scope.cancelPaymentPlanEdit = function(){ //kamal
                $scope.planupdated = $scope.recurring_status =  $scope.refund_status = false;
                $scope.showChangesinCancellation = true;
                $scope.subtractedText = "";
                $scope.quantityerrortext = "";
                $scope.showquantityedit = false;
                $scope.new_plan_details = angular.copy($scope.current_plan_details);
                $scope.new_due_details = angular.copy($scope.current_due_details);
                $scope.new_event_details = angular.copy($scope.current_event_details);                
                $scope.new_manual_credit_details = $scope.current_plan_details.manual_credit_details;
                $scope.remainingDueValue = angular.copy($scope.new_plan_details.rem_due);
            };
            
            
            //refund action confirmation
            $scope.refundAndDeleteParticipantConfirm = function (cancel_status) {

                if ($scope.current_plan_details.pending_payment_state === '1' || $scope.current_plan_details.pending_payment_state === 1) {
                    $("#eventCancellationActionModal2").modal('show');
                    $("#eventCancellationActiontitle2").text('Edit Order');
                    $("#eventCancellationActioncontent2").text("Payment can be refunded only after it is released. Please try again after it is released.");
                } else {
                    if (cancel_status === 'C') { //PROCESS CANCELLATION
                        $scope.cancel_status = cancel_status;
                        $("#eventCancellationActionModal").modal('show');
                        $("#eventCancellationActiontitle").text('Edit Order');
                        $("#eventCancellationActioncontent").text("Are you sure you want to process the cancellation?");
                    } else if (cancel_status === 'WOR') { //CANCEL WITH-OUT REFUND
                        $scope.cancel_status = cancel_status;
                        $("#eventCancellationActionModal").modal('show');
                        $("#eventCancellationActiontitle").text('Edit Order');
                        $("#eventCancellationActioncontent").text("Are you sure you want to process the cancellation without refund?");
                    } else if (cancel_status === 'WR') { //CANCEL WITH REFUND
                        $scope.cancel_status = cancel_status;
                        $("#eventCancellationActionModal").modal('show');
                        $("#eventCancellationActiontitle").text('Edit Order');
                        $("#eventCancellationActioncontent").text("Are you sure you want to process the cancellation with refund?");
                    } else if (cancel_status === 'CP') { // CANCEL AND KEEP CURRENT PLAN
                        $scope.cancel_status = cancel_status;
                        $("#eventCancellationActionModal").modal('show');
                        $("#eventCancellationActiontitle").text('Edit Order');
                        $("#eventCancellationActioncontent").text("Are you sure you want to process the cancellation and keep the current payment plan?");
                    } else if (cancel_status === 'NP') { // CANCEL AND KEEP NEW PLAN
                        $scope.cancel_status = cancel_status;
                        $("#eventCancellationActionModal").modal('show');
                        $("#eventCancellationActiontitle").text('Edit Order');
                        $("#eventCancellationActioncontent").text("Are you sure you want to process the cancellation and adopt the new payment plan?");
                    }

                }
            };
            
            
            
            $scope.event_Participant_Info = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getEventParticipantdetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id": $scope.currentEvent_reg_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                    if (response.data.status === 'Success') {
                        $scope.event_participant_info = response.data.msg; 
                        
                        $scope.eventeditparticipant = angular.copy($scope.event_participant_info);
                        $scope.parfirstname = $scope.eventeditparticipant.event_registration_column_1;
                        $scope.parlastname = $scope.eventeditparticipant.event_registration_column_2;
                        $scope.buyerfirstname = $scope.eventeditparticipant.buyer_first_name;
                        $scope.buyerlastname = $scope.eventeditparticipant.buyer_last_name;
                        $scope.parfirst_temp = $scope.parfirstname;
                        $scope.parlastname_temp = $scope.parlastname;
                        $scope.buyerfirst_temp = $scope.buyerfirstname;
                        $scope.buyerlastname_temp = $scope.buyerlastname;
                        $scope.participant_phone_temp = $scope.eventeditparticipant.buyer_phone;
                        $scope.participant_email_temp = $scope.eventeditparticipant.buyer_email;
                        $scope.participant_col_3_temp = $scope.eventeditparticipant.event_registration_column_3;
                        $scope.participant_col_4_temp = $scope.eventeditparticipant.event_registration_column_4;
                        $scope.participant_col_5_temp = $scope.eventeditparticipant.event_registration_column_5;
                        $scope.participant_col_6_temp = $scope.eventeditparticipant.event_registration_column_6;
                        $scope.participant_col_7_temp = $scope.eventeditparticipant.event_registration_column_7;
                        $scope.participant_col_8_temp = $scope.eventeditparticipant.event_registration_column_8;
                        $scope.participant_col_9_temp = $scope.eventeditparticipant.event_registration_column_9;
                        $scope.participant_col_10_temp = $scope.eventeditparticipant.event_registration_column_10;
                        
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.event_Payment_History = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'paymentHistoryDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id" : $scope.currentEvent_reg_id,
                        "category": 'event'
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                    if (response.data.status === 'Success') {
                        $scope.editevent_reg_id = $scope.event_reg_details = '';
                        $scope.buyer_name=response.data.reg_details.msg.buyer_name;
                        $scope.participant_name=response.data.reg_details.msg.participant_name;
                        $scope.event_payment_history = response.data.msg.history; 
                        $scope.event_payment_upcoming = response.data.msg.upcoming; 
                        if(response.data.reg_details.event_id && response.data.reg_details.event_id.length > 0){
                            $scope.event_reg_details = response.data.reg_details.event_id;
                        }
                        if($scope.event_payment_history.length > 0){
                            $scope.editevent_reg_id = $scope.event_payment_history[0].reg_id; 
                        }else if($scope.event_payment_upcoming.length > 0){
                            $scope.editevent_reg_id = $scope.event_payment_upcoming[0].reg_id;
                        }
                        if($scope.event_reg_details.length > 0){
                            $scope.currentEvent_id = $scope.event_reg_details; 
                        }else{
                            $scope.currentEvent_id = '';
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                        
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
                        
            $scope.Event_History = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getEventHistory',
                    data: {
                        "company_id": $localStorage.company_id,
                        "event_reg_id" : $scope.currentEvent_reg_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                    if (response.data.status === 'Success') {
                        $scope.event_history = response.data.msg;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.openselectpaymentmethod = function(){
                $("#selectpaymentmethod-modal").modal('show');
            }
            $scope.catchselectpaymentmethod = function(){
                if($scope.selectpaymentmethod == "CC"){
                    $scope.showEventPaymentMethodModal();
                    return false;
                }
                else if($scope.wepay_enabled){
                    $scope.editEventDBPaymentMethod('','','','','','');
                }
                else{
                     $scope.updateStripePaymentMethodInDB('','', '', '');
                    
                }
            }
            
            $scope.showEventPaymentMethodModal = function () { // SHOW EDIT PAYMENT METHOD FORM
                if($scope.wepay_enabled){
                    $("#editEventpaymentMethodModal").modal('show');
                    $("#editEventpaymentMethodActiontitle").text('Edit Payment Method');
                }else if($scope.stripe_enabled){
                    $("#UpdateEventPaymentMethodModal").modal('show');
                    $("#UpdateEventPaymentMethodModaltitle").text('Edit Payment Method');
                    if($scope.stripe_publish_key){
                        $scope.stripe = Stripe($scope.stripe_publish_key);
                    }
                    $scope.clearStripeCardData();
                    $scope.getStripeElements();
                }
            };
            $scope.clearStripeCardData = function(){
                $scope.stripe_first_name = "";
                $scope.stripe_last_name = "";
                $scope.stripe_email = "";
                $scope.valid_stripe_card = false;
                if ($scope.stripe_card) {
                    $scope.stripe_card.clear();
                }
            };
            // STRIPE TOKEN CALL
            $scope.getStripeElements = function () {
                // Create a Stripe client
                $scope.valid_stripe_card = false;

                // Create an instance of Elements
                var elements =  $scope.stripe.elements();

                // Custom styling can be passed to options when creating an Element.
                // (Note that this demo uses a wider set of styles than the guide below.)
                var style = {
                    base: {
                        color: 'rgb(74,74,74)',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                            color: '#aab7c4'
                        }
                    },
                    invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                    }
                };

                // Create an instance of the card Element
                $scope.stripe_card = elements.create('card', {style: style});

                // Add an instance of the card Element into the `card-element` <div>
                $scope.stripe_card.mount('#event-stripe-card-element');

                // Handle real-time validation errors from the card Element.
                $scope.stripe_card.addEventListener('change', function (event) {
                    var displayError = document.getElementById('event-stripe-card-errors');
                    if (event.error) {
                        displayError.textContent = event.error.message;
                        $scope.valid_stripe_card = false;
                    } else {
                        displayError.textContent = '';
                        $scope.valid_stripe_card = true;
                    }
                });
            };
            
            
            $scope.serverdateformatfilter = function (date) {
                if (date !== undefined && date !== '' && date !== null) {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }
            };
            
            $scope.localdateformatfilter = function (date) {
                if (date !== '0000-00-00' && date !== '' && date !== null) {
                    var datePart = date.split("-");
                    var day = datePart[2];
                    var year = datePart[0];
                    var month = datePart[1];
                    var datevalue = month + '/' + day + '/' + year;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }
            };
            
            $scope.resetEditEventParticipant = function () {
                $scope.showParEdit = $scope.showCusParEdit = false;
                $scope.parfirstname = $scope.parfirst_temp;
                $scope.parlastname = $scope.parlastname_temp;
                $scope.buyerfirstname = $scope.buyerfirst_temp;
                $scope.buyerlastname = $scope.buyerlastname_temp;
                $scope.eventeditparticipant.buyer_phone = $scope.participant_phone_temp;
                $scope.eventeditparticipant.buyer_email = $scope.participant_email_temp;
                $scope.eventeditparticipant.membership_registration_column_3 = $scope.participant_col_3_temp;
                $scope.eventeditparticipant.membership_registration_column_4 = $scope.participant_col_4_temp;
                $scope.eventeditparticipant.membership_registration_column_5 = $scope.participant_col_5_temp;
                $scope.eventeditparticipant.membership_registration_column_6 = $scope.participant_col_6_temp;
                $scope.eventeditparticipant.membership_registration_column_7 = $scope.participant_col_7_temp;
                $scope.eventeditparticipant.membership_registration_column_8 = $scope.participant_col_8_temp;
                $scope.eventeditparticipant.membership_registration_column_9 = $scope.participant_col_9_temp;
                $scope.eventeditparticipant.membership_registration_column_10 = $scope.participant_col_10_temp;
            };
            
            $scope.showEventParticipantEdit = function () {
                $scope.resetEditEventParticipant();
                $scope.showParEdit = true;
                $scope.showCusParEdit = false;
            };
            
            $scope.showCustomEventParticipantEdit = function () {
                $scope.resetEditEventParticipant();
                $scope.showParEdit = false;
                $scope.showCusParEdit = true;
            };
            
            // UPDATE PARTICIPANT NAME ON THE TABLE LISTING
            $scope.updateEventParticipantName = function (fname, lname, bfname, blname, phone, email, value3, value4, value5, value6, value7, value8, value9, value10) {
                $('#progress-full').show(); 
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateEventParticipantName',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id": $scope.currentEvent_reg_id,
                        "pfirst_name": fname,
                        "plast_name": lname,
                        "bfirst_name": bfname,
                        "blast_name": blname,
                        "buyer_phone": phone,
                        "buyer_email": email,
                        "event_field_3": value3,
                        "event_field_4": value4,
                        "event_field_5": value5,
                        "event_field_6": value6,
                        "event_field_7": value7,
                        "event_field_8": value8,
                        "event_field_9": value9,
                        "event_field_10": value10
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                   
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.showParEdit = $scope.showCusParEdit = false;
                        
                        $scope.event_participant_info  = response.data.participant_details.msg; 
                        
                        $scope.eventeditparticipant = angular.copy($scope.event_participant_info);
                        $scope.parfirstname = $scope.eventeditparticipant.event_registration_column_1;
                        $scope.parlastname = $scope.eventeditparticipant.event_registration_column_2;
                        $scope.buyerfirstname = $scope.eventeditparticipant.buyer_first_name;
                        $scope.buyerlastname = $scope.eventeditparticipant.buyer_last_name;
                        $scope.parfirst_temp = $scope.parfirstname;
                        $scope.parlastname_temp = $scope.parlastname;
                        $scope.buyerfirst_temp = $scope.buyerfirstname;
                        $scope.buyerlastname_temp = $scope.buyerlastname;
                        $scope.participant_phone_temp = $scope.eventeditparticipant.buyer_phone;
                        $scope.participant_email_temp = $scope.eventeditparticipant.buyer_email;
                        $scope.participant_col_3_temp = $scope.eventeditparticipant.event_registration_column_3;
                        $scope.participant_col_4_temp = $scope.eventeditparticipant.event_registration_column_4;
                        $scope.participant_col_5_temp = $scope.eventeditparticipant.event_registration_column_5;
                        $scope.participant_col_6_temp = $scope.eventeditparticipant.event_registration_column_6;
                        $scope.participant_col_7_temp = $scope.eventeditparticipant.event_registration_column_7;
                        $scope.participant_col_8_temp = $scope.eventeditparticipant.event_registration_column_8;
                        $scope.participant_col_9_temp = $scope.eventeditparticipant.event_registration_column_9;
                        $scope.participant_col_10_temp = $scope.eventeditparticipant.event_registration_column_10;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.resetForm = function () { // RESET CREDIT CARD FORM
                $scope.firstname = '';
                $scope.lastname = '';
                $scope.email = '';
                $scope.phone = '';
                $scope.cardnumber = '';
                $scope.$parent.ccmonth = '';
                $scope.$parent.ccyear = '';
                $scope.cvv = '';
                $scope.$parent.country = '';
                $scope.postal_code = '';
                $scope.paymentform.$setPristine();
                $scope.paymentform.$setUntouched();               

            };
            
            $scope.editEventDBPaymentMethod = function (cc_id, cc_state, user_first_name, user_last_name, email, postal_code) { // UPDATE CREDIT CARD DETAILS
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatePaymentMethod',
                    data: {
                        company_id: $localStorage.company_id,
                        reg_id: $scope.currentEvent_reg_id ,
                        buyer_first_name: user_first_name,
                        buyer_last_name: user_last_name,
                        email: email,
                        postal_code: postal_code,
                        cc_id: cc_id,
                        cc_state: cc_state,
                        category_type: 'E',
                        page_key: 'ED',     // EVENT DETAIL
                        type:$scope.selectpaymentmethod
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $("#editEventpaymentMethodModal").modal('hide');
                        $("#editEventpaymentMethodActiontitle").text('');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text('Payment method successfully updated.');
                        $scope.onetimecheckout = 0;
                        $scope.resetForm();
                        if(response.data.reg_details.info==='normal'){
                            $("#successmessageModal").modal('show');
                            $("#successmessagetitle").text('Message');
                            $("#successmessagecontent").text(response.data.msg);
                        }else if(response.data.reg_details.info==='payment plan'){
                            var message = response.data.reg_details.msg;
                            $scope.current_plan_details = message.current_plan_details;
                            $scope.current_due_details = message.current_plan_details.due_details;
                            $scope.current_event_details = message.event_details;
                            $scope.current_manual_credit_details = message.current_plan_details.manual_credit_details;
                            
                            $scope.new_plan_details = angular.copy($scope.current_plan_details);
                            $scope.new_due_details = angular.copy($scope.current_due_details);
                            $scope.new_event_details = angular.copy($scope.current_event_details);
                            $scope.new_manual_credit_details = angular.copy($scope.current_manual_credit_details);
                            $scope.remainingDueValue = angular.copy($scope.new_plan_details.rem_due);
                            $scope.show_new_dew_details = true;
                            $scope.eventdetailsExist = true;
                        }else{
                            $("#successmessageModal").modal('show');
                            $("#successmessagetitle").text('Message');
                            $("#successmessagecontent").text('Invalid server response');
                        }
                    } else if (response.data.status === 'Expired') {
                        $("#editEventpaymentMethodModal").modal('hide');
                        $("#editEventpaymentMethodActiontitle").text('');
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $("#editEventpaymentMethodModal").modal('hide');
                        $("#editEventpaymentMethodActiontitle").text('');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        if (response.data.error) {
                            $("#successmessagetitle").text(response.data.error);
                            $("#successmessagecontent").text(response.data.error_description);
                        } else {
                            $("#successmessagetitle").text('Message');
                            $("#successmessagecontent").text(response.data.msg);
                        }
                        $scope.onetimecheckout = 0;
                    }
                }, function (response) {
                    $("#editEventpaymentMethodModal").modal('hide');
                    $("#editEventpaymentMethodActiontitle").text('');
                    console.log(response.data);
                    $scope.handleError(response.data);
                    $scope.onetimecheckout = 0;
                });
            };
            
            
            $scope.editEventHistoryNote = function (ind, hist_id, action) {
                if(action === 'add'){
                    $scope.edit_status = 'add';
                    $scope.edited_event_history_id = '';
                    $scope.eventhistorynote = '';
                    $scope.HistoryNoteButton = 'Add';
                    $("#eventHistoryNoteModal").modal('show');
                    $("#eventHistoryNotetitle").text('Add Note');                    
                }else if(action === 'edit'){
                    $scope.edited_event_history_id = hist_id ;
                    $scope.edit_status = 'update';
                    $scope.HistoryNoteButton = 'Update';
                    $scope.eventhistorynote = $scope.event_history[ind].activity_text;
                    $("#eventHistoryNoteModal").modal('show');
                    $("#eventHistoryNotetitle").text('Update Note');
                }else if(action === 'delete'){
                    $scope.edited_event_history_id = hist_id ;
                    $scope.eventhistorynote =  '';
                    $scope.edit_status = 'delete';
                    $("#evntHistoryNoteDeleteModal").modal('show');
                    $("#evntHistoryNoteDeletetitle").text('Delete Note');
                    $("#evntHistoryNoteDeletecontent").text('Are you sure want to delete the note?');
                }
            }
            
            $scope.confirmeditEventHistoryNote = function () {
                $("#eventHistoryNoteModal").modal('hide');
                $("#evntHistoryNoteDeleteModal").modal('hide');
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'EventHistoryNotes',
                    data: {
                        "company_id": $localStorage.company_id,
                        "history_id": $scope.edited_event_history_id,
                        "status":$scope.edit_status,
                        "reg_id": $scope.currentEvent_reg_id,
                        "note_text": $scope.eventhistorynote
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.event_history = response.data.event_history.msg; 
                        
                    }else if(response.data.status === 'Failed'){
                        console.log(response.data);
                        $('#progress-full').hide();         
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text(response.data.msg); 
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.sendPaymentHistory = function () {
                $("#payEvntHistoryEmailModal").modal('show');
                $("#payEvntHistoryEmailtitle").text('Send Email');
                $("#payEvntHistoryEmailcontent").text('Are you sure want to send a email with payment history details?');
            };
            
            
            $scope.confirmsendPayHistoryEmail = function () {
                $("#payHistoryEmailModal").modal('hide');
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendPaymentHistory',
                    data: {
                        "company_id": $localStorage.company_id,
                        "membership_reg_id": $scope.currentEvent_reg_id,
                        "category": 'events'
                        
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $("#payEvntHistoryEmailModal").modal('hide');
                        $("#refundModal").modal('hide');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text('Event History sent successfully.');
                    }else if(response.data.status === 'Failed'){
                        console.log(response.data);
                        $('#progress-full').hide();         
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text(response.data.msg); 
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.editPaymentHistory = function (ind, history_type) { //Show list of Actions in Past tab//
                if(history_type === 'upcoming'){
                    $scope.actionpayment_array = $scope.event_payment_upcoming[ind];
                }else{
                    $scope.actionpayment_array = $scope.event_payment_history[ind];
                }
                $scope.editPaymentMethod_reg_id = $scope.actionpayment_array.reg_id;
                $scope.editPaymentMethod_payment_id = $scope.actionpayment_array.payment_id;
                $scope.temp_hist_paymentamnt1 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                $scope.temp_hist_paymentamnt2 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                var pay_start_date = $scope.actionpayment_array.payment_date.split("-");                   
                $scope.paymenthistorystartdate = pay_start_date[1] + "/" + pay_start_date[2] + "/" + pay_start_date[0];
                $scope.paymenthistorystartdate_temp = $scope.paymenthistorystartdate;
                $("#paymentHistoryEditModal").modal('show');
                $("#paymentHistoryEdittitle").text('Update Payment details');
               
            };
            
            $scope.updatePaymentHistory = function (value, newval) { //Validate The Credit amount Entered  By User//
                $scope.new_payment_amount = 0;
                if (!value || parseFloat(newval) < 5) {
                    $("#successmessageModal").modal('show');
                    $("#successmessagetitle").text('Message');
                    $("#successmessagecontent").text('Payment amount should not be lower than $5.');
                } else {
                    $scope.new_payment_amount = newval;
                    $scope.final_payment_start_date = $scope.serverdateformatfilter($scope.paymenthistorystartdate);                    
                    $scope.curr_date_new = $scope.formatDateToday();
                    if($scope.final_payment_start_date > $scope.curr_date_new){
                        $scope.curr_payment_start_date_flag = 'N';
                    }else{
                        $scope.curr_payment_start_date_flag = 'Y';
                    }
                    $("#successmessageModal").modal('hide');
                    $("#paymentEditHistoryModal").modal('show');
                    $("#paymentEditHistorytitle").text('Message');
                    $("#paymentEditHistorycontent").text('Are you sure want to update payment details?');
                }
            };
            
            $scope.updatePaymentHistoryCancel = function () { 
                $scope.new_payment_amount = 0;
                $scope.final_payment_start_date = '';
            };
            
            $scope.confirmUpdatePaymentHistory = function () { 
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'editPaymentFromHistory',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id": $scope.editPaymentMethod_reg_id,
                        "payment_id": $scope.editPaymentMethod_payment_id,
                        "payment_amount": $scope.new_payment_amount,
                        "payment_date": $scope.final_payment_start_date,
                        "pay_now_flg": $scope.curr_payment_start_date_flag,
                        "category":'events'
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') { 
                        $("#paymentHistoryEditModal").modal('hide');
                        $('#progress-full').hide();                        
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.editevent_reg_id = '';
                        $scope.event_payment_history = response.data.payment.msg.history; 
                        $scope.event_payment_upcoming = response.data.payment.msg.upcoming;   
                        if($scope.event_payment_history.length > 0){
                            $scope.editevent_reg_id = $scope.event_payment_history[0].reg_id; 
                        }else if($scope.event_payment_upcoming.length > 0){
                            $scope.editevent_reg_id = $scope.event_payment_upcoming[0].reg_id;
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            
            $scope.performActionUpdate = function (ind,schedule_status,history_type) {
                
                if(schedule_status==='S'|| schedule_status==='M'){
                    $scope.eventdetails();
                }else if (schedule_status === 'F' || (schedule_status === 'N' )){ // APPLY MANUAL CREDIT
                    if(history_type === 'upcoming'){
                        $scope.actionpayment_array = $scope.event_payment_upcoming[ind];
                    }else{
                        $scope.actionpayment_array = $scope.event_payment_history[ind];
                    }
                    $scope.editPaymentMethod_reg_id = $scope.actionpayment_array.reg_id;
                    $scope.editPaymentMethod_payment_id = $scope.actionpayment_array.payment_id;
                    $scope.editpaymentmethod_payment_count=$scope.actionpayment_array.payment_count;
                    $scope.editpaymentmethod_student_id=$scope.actionpayment_array.student_id;
                    $scope.temp_past_paymentarray1 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                    $scope.temp_past_paymentarray2 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                    $scope.actionValue = 2;
                    $scope.creditmethod = 'MC';
                    $scope.checknumber = '';
                    $("#paymentHistoryModal1").modal('show');
                    $("#paymentHistorytitle1").text('Apply Payment Credit');
                    $("#paymentHistorytitle1").text('Enter credit to be applied to this bill');
                }
            };
            
            $scope.performRerunAction = function (ind, payment_status, history_type) { //Show list of Actions in Past tab
                if(history_type === 'upcoming'){
                    $scope.actionpayment_array = $scope.event_payment_upcoming[ind];
                }else{
                    $scope.actionpayment_array = $scope.event_payment_history[ind];
                }
                $scope.editPaymentMethod_reg_id = $scope.actionpayment_array.reg_id;
                $scope.editPaymentMethod_payment_id = $scope.actionpayment_array.payment_id;
                $scope.temp_past_paymentarray1 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                $scope.temp_past_paymentarray2 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                if (payment_status === 'F' || payment_status === 'N') { //RE-RUN
                    $scope.actionValue = 0;
                    $("#paymentHistoryModal").modal('show');
                    $("#paymentHistorytitle").text('Re-Run Payment');
                    $("#paymentHistorycontent").text("Are you sure to Re-Run the Payment?");
                }
            };
            
            $scope.updatePaidquantity = function (value, newval) {
                //Validate The Credit amount Entered  By User//
                $scope.upcoming_creditamount = 0;
                if (!value || parseFloat(newval) < 5) {
                    $("#successmessageModal").modal('show');
                    $("#successmessagetitle").text('Message');
                    $("#successmessagecontent").text('Credit amount or remaining bill amount should not be lower than $5.');

                } else if (!value || parseFloat(value) < parseFloat(newval)) {
                    $("#successmessageModal").modal('show');
                    $("#successmessagetitle").text('Message');
                    $("#successmessagecontent").text('Credit amount should not be greater than bill amount.');

                } else if (!value || ((parseFloat(value) - parseFloat(newval) < 5) && (parseFloat(value) - parseFloat(newval) > 0)))  {
                    $("#successmessageModal").modal('show');
                    $("#successmessagetitle").text('Message');
                    $("#successmessagecontent").text('Credit amount or remaining bill amount should not be lower than $5.');

                } else {
                    $scope.upcoming_creditamount = newval;
                    $("#successmessageModal").modal('hide');
                    $("#paymentHistoryModal3").modal('show');
                    $("#paymentHistorytitle3").text('Message');
                    $("#paymentHistorycontent3").text('Are you sure want to add a credit to this bill?');
                    return;
                }
            };
            
            $scope.confirmCreditAmount = function (creditamount,creditmethod) { //Function call After Getting Credit amount From user//
                $scope.markAsMembershipPaid($scope.actionpayment_array, creditamount, creditmethod);
            };
            
            $scope.actionReRunConfirm = function (value) { //Function call after confirmation//
                $scope.editSingle = "";
                $scope.actionPastchoosen = "";
                if (value === 0) {
                    $scope.reRunPayment($scope.actionpayment_array);
                }
            };
            
            
            $scope.reRunPayment = function () { //update credit card details
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'reRunPayment',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id": $scope.editPaymentMethod_reg_id,
                        "payment_id": $scope.editPaymentMethod_payment_id,
                        "category_type": 'E'
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {                        
                        $('#progress-full').hide();
                        $("#successmessageModalrerun").modal('show');
                        $("#successmessagetitlererun").text('Message');
                        $("#successmessagecontentrerun").text(response.data.msg);
                        $scope.editevent_reg_id = '';
                        $scope.event_payment_history = response.data.payment.msg.history; 
                        $scope.event_payment_upcoming = response.data.payment.msg.upcoming;   
                        if($scope.event_payment_history.length > 0){
                            $scope.editevent_reg_id = $scope.event_payment_history[0].reg_id; 
                        }else if($scope.event_payment_upcoming.length > 0){
                            $scope.editevent_reg_id = $scope.event_payment_upcoming[0].reg_id;
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.markAsMembershipPaid = function (payment_array, creditamount, creditmethod) { //Mark the membership as Paid
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'eventPaymentMarkedAsPaid',
                    data: {
                        "company_id": $localStorage.company_id,
                        "event_payment_id": payment_array.payment_id,
                        "event_reg_id": payment_array.reg_id,
                        "credit_amount": creditamount,
                        "payment_Count": $scope.editpaymentmethod_payment_count,
                        "credit_method": creditmethod,
                        "check_number": $scope.checknumber
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $("#paymentHistoryModal1").modal('hide');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.upcoming_creditamount = $scope.editevent_reg_id = '';
                        $scope.event_payment_history = response.data.payment.msg.history; 
                        $scope.event_payment_upcoming = response.data.payment.msg.upcoming;   
                        if($scope.event_payment_history.length > 0){
                            $scope.editevent_reg_id = $scope.event_payment_history[0].reg_id; 
                        }else if($scope.event_payment_upcoming.length > 0){
                            $scope.editevent_reg_id = $scope.event_payment_upcoming[0].reg_id;
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('#li-customers').addClass('active-event-list').removeClass('event-list-title');
                $('.greentab-text').removeClass('greentab-text-active');
            };
            
            $scope.eventdetails = function () {
                $scope.eventdetailsview = true;
                $scope.participantinfoview = false;
                $scope.eventhistoryview = false;
                $scope.paymenthistoryview = false;
                $scope.resettabbarclass();
                $('#li-eventdetails').addClass('active-event-list').removeClass('event-list-title');
                $('.eventdetail-tabtext-1').addClass('greentab-text-active');
                $scope.Event_Detail();
            };
            
            $scope.eventparticipantinfo = function () {
                $scope.eventdetailsview = false;
                $scope.participantinfoview = true;
                $scope.eventhistoryview = false;
                $scope.paymenthistoryview = false;  
                $scope.resettabbarclass();
                $('#li-eventparticipantinfo').addClass('active-event-list').removeClass('event-list-title');
                $('.eventdetail-tabtext-2').addClass('greentab-text-active');
                $scope.event_Participant_Info();
            };
            
            $scope.eventhistory = function () {
                $scope.eventdetailsview = false;
                $scope.participantinfoview = false;
                $scope.eventhistoryview = true;
                $scope.paymenthistoryview = false;  
                $scope.resettabbarclass();
                $('#li-eventhistory').addClass('active-event-list').removeClass('event-list-title');
                $('.eventdetail-tabtext-3').addClass('greentab-text-active');
                $scope.Event_History();
            };
            
            $scope.eventpaymenthistory = function () {
                $scope.eventdetailsview = false;
                $scope.participantinfoview = false;
                $scope.eventhistoryview = false;
                $scope.paymenthistoryview = true;  
                $scope.resettabbarclass();
                $('#li-eventpaymenthistory').addClass('active-event-list').removeClass('event-list-title');
                $('.eventdetail-tabtext-4').addClass('greentab-text-active');
                $scope.event_Payment_History();
            };
            
            
            
            $scope.closeEventDetailView = function () {
                $localStorage.buyerName = "";
                $localStorage.participantName = "";
                $localStorage.currentMembershipregid = "";
                $localStorage.currentEventregid = "";
                 if ($scope.page_From === "payment_approved" || $scope.page_From === "payment_past" || $scope.page_From === "payment_upcoming")
                {
                    $location.path('/payment');
                }  else if ($scope.page_From === "events_order" || $scope.page_From === "events_cancel"){
                    $location.path('/manageevent');
                }
            };

            $scope.openEventDetailsPage = function () {
                if ($scope.page_From === "payment_approved" || $scope.page_From === "payment_past" || $scope.page_From === "payment_upcoming")
                {
                    $scope.eventpaymenthistory();
                } else if ($scope.page_From === "events_order" || $scope.page_From === "events_cancel")
                {
                    $scope.eventdetails();
                } else {
                    $scope.eventdetails();
                }                
            };
            $scope.createStripePaymentMethod = function(){
                $scope.stripe.createPaymentMethod('card', $scope.stripe_card).then(function (result) {
                    if (result.error) {
                        // Inform the user if there was an error. 
                        var errorElement = document.getElementById('event-stripe-card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        // Send paymentMethod.id to server 
                        (result.paymentMethod.id)? $scope.payment_method_id = result.paymentMethod.id : $scope.payment_method_id = "";
                        $scope.updateStripePaymentMethodInDB($scope.payment_method_id, $scope.stripe_first_name, $scope.stripe_last_name, $scope.stripe_email);
                    }
                });
            };
            $scope.updateStripePaymentMethodInDB = function (payment_method_id, user_first_name, user_last_name, email) {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'updateStripePaymentMethod',
                data: {
                    company_id: $localStorage.company_id,
                    reg_id: $scope.currentEvent_reg_id ,
                    buyer_first_name: user_first_name,
                    buyer_last_name: user_last_name,
                    email: email,
                    category_type: 'E',
                    page_key: 'ED', // EVENT DETAIL
                    type: $scope.selectpaymentmethod,
                    payment_method: payment_method_id,
                },
                headers: {"Content-Type": 'application/json; charset=utf-8', },
                withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $("#UpdateEventPaymentMethodModal").modal('hide');
                    $("#UpdateEventPaymentMethodModaltitle").text('');
                    $('#progress-full').hide();
                    $("#successmessageModal").modal('show');
                    $("#successmessagetitle").text('Message');
                    $("#successmessagecontent").text('Payment method successfully updated.');
                    $scope.onetimecheckout = 0;
                    $scope.clearStripeCardData();
                    if (response.data.reg_details.info === 'normal') {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    } else if (response.data.reg_details.info === 'payment plan') {
                        var message = response.data.reg_details.msg;
                        $scope.current_plan_details = message.current_plan_details;
                        $scope.current_due_details = message.current_plan_details.due_details;
                        $scope.current_event_details = message.event_details;
                        $scope.current_manual_credit_details = message.current_plan_details.manual_credit_details;

                        $scope.new_plan_details = angular.copy($scope.current_plan_details);
                        $scope.new_due_details = angular.copy($scope.current_due_details);
                        $scope.new_event_details = angular.copy($scope.current_event_details);
                        $scope.new_manual_credit_details = angular.copy($scope.current_manual_credit_details);
                        $scope.remainingDueValue = angular.copy($scope.new_plan_details.rem_due);
                        $scope.show_new_dew_details = true;
                        $scope.eventdetailsExist = true;
                    } else {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text('Invalid server response');
                    }
                } else if (response.data.status === 'Expired') {
                    $("#editEventpaymentMethodModal").modal('hide');
                    $("#editEventpaymentMethodActiontitle").text('');
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();

                } else {
                    $("#editEventpaymentMethodModal").modal('hide');
                    $("#editEventpaymentMethodActiontitle").text('');
                    $('#progress-full').hide();
                    $("#successmessageModal").modal('show');
                    if (response.data.error) {
                        $("#successmessagetitle").text(response.data.error);
                        $("#successmessagecontent").text(response.data.error_description);
                    } else {
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                    $scope.onetimecheckout = 0;
                }
            }, function (response) {
                $("#editEventpaymentMethodModal").modal('hide');
                $("#editEventpaymentMethodActiontitle").text('');
                console.log(response.data);
                $scope.handleError(response.data);
                $scope.onetimecheckout = 0;
            });
        };

            
        } else {
            $location.path('/login');
        }
    }
    module.exports = EventDetailController;

