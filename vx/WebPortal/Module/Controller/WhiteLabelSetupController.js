WhiteLabelSetupController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$route', '$timeout','$rootScope','$window'];
function WhiteLabelSetupController ($scope, $location, $http, $localStorage, urlservice, $filter, $route, $timeout,$rootScope,$window) {

        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
//            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
//            angular.element(document.getElementById('sidetopbar')).scope().getWepayStatus();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            tooltip.hide();
            $rootScope.activepagehighlight = 'Whitelabel';
            $scope.wl_type = 'E';
            $scope.androidmetadatamanualcheck = false;
            $scope.iosmetadatamanualcheck = false;
            $scope.appiconname = $scope.appiconname_copy ="";
            $scope.appiconnameandroid = $scope.appiconnameandroid_copy ="";
            $scope.bundle_id = "";
            $scope.bundle_id_status = "";
            $scope.errormsg = "";
            $scope.my_color = '#ffffff';
            $scope.my_color_android = '#ffffff';
            $scope.upgrade_status = "";
            $scope.dist_cert =$scope.dev_cert  = $scope.SSL_dev =  $scope.SSL_prod = $scope.Provisional_dev =$scope.Provisional_dist = "";
            $scope.showpassword = false; // TOGGLE PASSWORD/TEXT
            $scope.iosiconbase64 = null;
            $scope.showpicker = false;
            $scope.ios_splash_option = "G";
            $scope.android_splash_option = "G";
            $scope.responsemessage = $scope.responsebutton = "";
            $scope.filepreview = $scope.filepreview1 = $scope.filepreview2 = $scope.filepreview3 = $scope.filepreview4 = $scope.filepreview5 = "";
            $scope.developerid_copy = $scope.teamid_copy = "";
            $scope.username = $scope.password = $scope.username_copy = $scope.password_copy = "";
            $scope.ios_dev_expiry_date = "";
            $scope.ios_prod_expiry_date = "";
            $scope.provision_dev_expiry_date = "";
            $scope.provision_prod_expiry_date = "";
            $scope.dev_pem_expiry = "";
            $scope.prod_pem_expiry = "";
            $scope.developerinfoview = true;
            $scope.appinfoview = false;
            $scope.appstoreinfoview = false;
            $scope.ASlanguage = "en-US";
            $scope.filepreview6 = "";
            $scope.step_1 = $scope.step_2 = $scope.step_3 = false;
            $scope.wldashboardview = true;
            $scope.wlaftersubmit = false;
            $scope.appleview = false;
            $scope.firebaseview = false;
            $scope.androidview = false;
            $scope.firebase = "";
            $scope.firebase_copy = "";
            $scope.dynamic_url = "";
            $scope.dynamic_url_copy = "";
            $scope.androidiconcheck = true;
            $scope.json_flag = false;
            $scope.AStradecountry = "United States";
            $scope.devprovname = "AppName_development.mobileprovision";
            $scope.prodprovname = "AppName_distribution.mobileprovision";
            $(document).ready(function () {
            $('[data-toggle="popover"]').popover();
        });
        
        $('body').on('click', function (e) {
            $('[data-toggle="popover"]').each(function () {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
            $scope.goandroidai = function(){
                $scope.current_wl_tab_android = "App Information";
                $scope.androidaiview = true;
                $scope.androidpsiview = false;
                $scope.resettabbarclass();
                $('.hometab-text-3').addClass('greentab-text-active');
                $('#li-google').addClass('active-event-list').removeClass('event-list-title');
                $('#li-googleai').addClass('active-event-list').removeClass('event-list-title');
                $scope.geticonurl("A");
            }
            
            $scope.wldashboard = function(){
                $scope.current_wl_tab = "Developer Information";
                $scope.appleview = false;
                $scope.firebaseview = false;
                $scope.androidview = false;
                $scope.wldashboardview = true;
                $scope.wlaftersubmit = false;
                $scope.resettabbarclass();
                $('.hometab-text-1').addClass('greentab-text-active');
                $('#li-dashboard').addClass('active-event-list').removeClass('event-list-title');
                $scope.getwldashboarddetails();
            }
            
            $scope.apple = function(){
                $scope.current_wl_tab = "Developer Information";
                $scope.appleview = true;
                $scope.firebaseview = false;
                $scope.androidview = false;
                $scope.wldashboardview = false;
                $scope.wlaftersubmit = false;
                $scope.developerinfoview = true;
                $scope.appinfoview = false;
                $scope.appstoreinfoview = false;
                $scope.resettabbarclass();
                $('.hometab-text-2').addClass('greentab-text-active');
                $('#li-di').addClass('active-event-list').removeClass('event-list-title');
                $('#li-apple').addClass('active-event-list').removeClass('event-list-title');
                $scope.checkbundleid();
            }
            
            $scope.gofirebase = function()
            {
                if($scope.bundle_id == "" || $scope.app_id == "" || $scope.scheme_name == "" || $scope.bundle_id == null || $scope.app_id == null || $scope.scheme_name == null || $scope.bundle_id == undefined || $scope.app_id == undefined || $scope.scheme_name == undefined){
                    $scope.responsemessage = "Please create and input Apple Bundle ID in step 2 to continue";
                    $scope.responsebutton = "Ok";
                    $("#message-modal").modal('show');
                    return false;
                }
                $scope.appleview = false;
                $scope.firebaseview = true;
                $scope.androidview = false;
                $scope.wldashboardview = false;
                $scope.wlaftersubmit = false;
                $scope.resettabbarclass();
                $('.hometab-text-4').addClass('greentab-text-active');
                $('#li-firebase').addClass('active-event-list').removeClass('event-list-title');
            }  
            
            $scope.goandroid = function()
            {
                if($scope.bundle_id == "" || $scope.app_id == "" || $scope.scheme_name == "" || $scope.bundle_id == null || $scope.app_id == null || $scope.scheme_name == null || $scope.bundle_id == undefined || $scope.app_id == undefined || $scope.scheme_name == undefined){
                    $scope.responsemessage = "Please create and input Apple Bundle ID in step 2 to continue";
                    $scope.responsebutton = "Ok";
                    $("#message-modal").modal('show');
                    return false;
                }
                $scope.current_wl_tab_android = "App Information";
                $scope.wldashboardview = false;
                $scope.wlaftersubmit = false;
                $scope.appleview = false;
                $scope.firebaseview = false;
                $scope.androidview = true;
                $scope.androidaiview = true;
                $scope.androidpsiview = false;
                $scope.resettabbarclass();
                $('.hometab-text-3').addClass('greentab-text-active');
                $('#li-google').addClass('active-event-list').removeClass('event-list-title');
                $('#li-googleai').addClass('active-event-list').removeClass('event-list-title');
                $scope.geticonurl("A");
            }  
            $scope.catchfiles = function(cno){
                $scope.dist_cert_copy = $scope.dist_cert.replace("C:\\fakepath\\", "");
                $scope.dev_cert_copy = $scope.dev_cert.replace("C:\\fakepath\\", "");
                $scope.SSL_dev_copy = $scope.SSL_dev.replace("C:\\fakepath\\", "");
                $scope.SSL_prod_copy = $scope.SSL_prod.replace("C:\\fakepath\\", "");
                $scope.Provisional_dev_copy = $scope.Provisional_dev.replace("C:\\fakepath\\", "");
                $scope.Provisional_dist_copy = $scope.Provisional_dist.replace("C:\\fakepath\\", "");
                $scope.uploadcertificate(cno);
            }
            
            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('.greentab-text').removeClass('greentab-text-active');
            };
            
            $scope.godeveloperinfo = function () {
                $scope.current_wl_tab = "Developer Information";
                $scope.developerinfoview = true;
                $scope.appinfoview = false;
                $scope.appstoreinfoview = false;
                $scope.resettabbarclass();
                $('#li-di').addClass('active-event-list').removeClass('event-list-title');
                $('#li-apple').addClass('active-event-list').removeClass('event-list-title');
                $('.hometab-text-2').addClass('greentab-text-active');
            };
            
            $scope.goappinfo = function () {
                if($scope.bundle_id == "" || $scope.app_id == "" || $scope.scheme_name == "" || $scope.bundle_id == null || $scope.app_id == null || $scope.scheme_name == null || $scope.bundle_id == undefined || $scope.app_id == undefined || $scope.scheme_name == undefined){
                    $scope.responsemessage = "Please create and input Apple Bundle ID in step 2 to continue";
                    $scope.responsebutton = "Ok";
                    $("#message-modal").modal('show');
                    return false;
                }
                $scope.current_wl_tab = "App Information";
                $scope.developerinfoview = false;
                $scope.appinfoview = true;
                $scope.appstoreinfoview = false;
                $scope.resettabbarclass();
                $('#li-apple').addClass('active-event-list').removeClass('event-list-title');
                $('#li-ai').addClass('active-event-list').removeClass('event-list-title');
                $('.hometab-text-2').addClass('greentab-text-active');
                $scope.geticonurl("I");
            };
            
            $scope.goappstoreinfo = function () {
                if($scope.bundle_id == "" || $scope.app_id == "" || $scope.scheme_name == "" || $scope.bundle_id == null || $scope.app_id == null || $scope.scheme_name == null || $scope.bundle_id == undefined || $scope.app_id == undefined || $scope.scheme_name == undefined){
                    $scope.responsemessage = "Please create and input Apple Bundle ID in step 2 to continue";
                    $scope.responsebutton = "Ok";
                    $("#message-modal").modal('show');
                    return false;
                }
                $scope.current_wl_tab = "App Store Information";
                $scope.getappstoreinfo();
                $scope.developerinfoview = false;
                $scope.appinfoview = false;
                $scope.appstoreinfoview = true;
                $scope.resettabbarclass();
                $('#li-apple').addClass('active-event-list').removeClass('event-list-title');
                $('#li-asi').addClass('active-event-list').removeClass('event-list-title');
                $('.hometab-text-2').addClass('greentab-text-active');
            };
            
            
            
            $scope.goandroidpsi = function(){
                if($scope.bundle_id == "" || $scope.app_id == "" || $scope.scheme_name == "" || $scope.bundle_id == null || $scope.app_id == null || $scope.scheme_name == null || $scope.bundle_id == undefined || $scope.app_id == undefined || $scope.scheme_name == undefined){
                    $scope.responsemessage = "Please create and input Apple Bundle ID in step 2 to continue";
                    $scope.responsebutton = "Ok";
                    $("#message-modal").modal('show');
                    return false;
                }
                $scope.current_wl_tab_android = "Play Store Information";
                $scope.androidaiview = false;
                $scope.androidpsiview = true;
                $scope.resettabbarclass();
                $('.hometab-text-3').addClass('greentab-text-active');
                $('#li-google').addClass('active-event-list').removeClass('event-list-title');
                $('#li-googlepsi').addClass('active-event-list').removeClass('event-list-title');
                $scope.getandroidstoreinfo();
            }
            $scope.ios_icon_url = null;
            $scope.android_icon_url = null;
            $scope.geticonurl  = function(type){
                 $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'geticonurl',
                    params: {
                        "company_id": $localStorage.company_id,
                        "scheme_name":$scope.scheme_name,
                        "bundle_id":$scope.bundle_id,
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        
                        
                        
                        if(response.data.ios_portrait != '' && response.data.ios_portrait != null && response.data.ios_portrait != 'null'){
                            $scope.ios_portrait = response.data.ios_portrait;
                            $('#iosportraitpreviewurl').attr('src', $scope.ios_portrait);
                            $scope.iosoverwriteimage = true;
                        }
                        
                        if(response.data.ios_landscape != '' && response.data.ios_landscape != null && response.data.ios_landscape != 'null'){
                            $scope.ios_landscape = response.data.ios_landscape;
                            $('#iosoverwriteimageurl').attr('src', $scope.ios_landscape);
                            $scope.iosoverwriteimage = true;
                        }
                        
                        if(response.data.android_portait != '' && response.data.android_portait != null && response.data.android_portait != 'null'){
                            $scope.android_portait = response.data.android_portait;
                            $('#androidportraitpreviewurl').attr('src', $scope.android_portait);
                            $scope.androidoverwriteimage = true;
                        }
                        
                        if(response.data.android_landscape != '' && response.data.android_landscape != null && response.data.android_landscape != 'null'){
                            $scope.android_landscape = response.data.android_landscape;
                            $('#androidlandscapepreviewurl').attr('src', $scope.android_landscape);
                            $scope.androidoverwriteimage = true;
                        }
                        
                        
                            if (response.data.hex.ios_hex_value != "" && response.data.hex.ios_hex_value != null && response.data.hex.ios_hex_value != "null") {
                                $scope.my_color = response.data.hex.ios_hex_value;
                            } else {
                                $scope.my_color = "#ffffff";
                            }

                            if (response.data.hex.android_hex_value != "" && response.data.hex.android_hex_value != null && response.data.hex.android_hex_value != "null") {
                                $scope.my_color_android = response.data.hex.android_hex_value;
                            } else {
                                $scope.my_color_android = "#ffffff";
                            }
                        
                        $scope.ios_icon_url = response.data.ios_icon_url;
                        $scope.android_icon_url = response.data.android_icon_url;
                        if($scope.ios_icon_url != "null" && type == "I"){
                            $('#iosiconpreview').attr('src', $scope.ios_icon_url);
                            $("#iosiconpreview").css("visibility", "visible");
                            $('#iosportraitpreview').attr('src', $scope.ios_icon_url);
                            $("#iosportraitpreview").css("visibility", "visible");
                            $('#ioslandscapepreview').attr('src', $scope.ios_icon_url);
                            $("#ioslandscapepreview").css("visibility", "visible");
                        }
                        if($scope.android_icon_url != "null" && type == "A"){
                            $('#androidiconpreview').attr('src', $scope.android_icon_url);
                                $("#androidiconpreview").css("visibility", "visible");
                                $('#androidportraitpreview').attr('src', $scope.android_icon_url);
                                $("#androidportraitpreview").css("visibility", "visible");
                                $('#androidlandscapepreview').attr('src', $scope.android_icon_url);
                                $("#androidlandscapepreview").css("visibility", "visible");
                        }
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.firebasetick = false;
            $scope.uploadcertificate = function (cno) {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'uploadwhitelabelcert',
                    data: {
                        "company_id": $localStorage.company_id,
                        "ios_development_cer":$scope.filepreview,
                        "ios_distribution_cer":$scope.filepreview1,
                        "aps_development_cer":$scope.filepreview2,
                        "aps_Production_cer":$scope.filepreview3,
                        "AppName_development_cer":$scope.filepreview4,
                        "AppName_distribution_cer":$scope.filepreview5,
                        "ios_development_cer_name":$scope.dev_cert,
                        "ios_distribution_cer_name":$scope.dist_cert,
                        "aps_development_cer_name":$scope.SSL_dev,
                        "aps_Production_cer_name":$scope.SSL_prod,
                        "AppName_development_cer_name":$scope.Provisional_dev,
                        "AppName_distribution_cer_name":$scope.Provisional_dist,
                        "ios_account_type":$scope.wl_type
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.dev_cert = "";
                            $scope.filepreview = "";
                            $scope.dist_cert  = "";
                            $scope.filepreview1 = "";
                            $scope.SSL_dev =  "";
                            $scope.filepreview2 = "";
                            $scope.SSL_prod = "";
                            $scope.filepreview3 = "";
                            $scope.Provisional_dev = "";
                            $scope.filepreview4 = "";
                            $scope.Provisional_dist = "";
                            $scope.filepreview5 = "";
                        $scope.responsemessage = response.data.msg;
                        $scope.responsebutton = "Ok";
                        $('#progress-full').hide();
                        $scope.checkbundleid();
                        $("#message-modal").modal('show');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                            $scope.dev_cert = "";
                            $scope.filepreview = "";
                            $scope.dist_cert  = "";
                            $scope.filepreview1 = "";
                            $scope.SSL_dev =  "";
                            $scope.filepreview2 = "";
                            $scope.SSL_prod = "";
                            $scope.filepreview3 = "";
                            $scope.Provisional_dev = "";
                            $scope.filepreview4 = "";
                            $scope.Provisional_dist = "";
                            $scope.filepreview5 = "";
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $localStorage.builderror_flag = false;
            $scope.builderror_flag = false;
            $scope.builderror_text = "";
            $scope.checkbundleid = function(){
                $http({
                    method: 'GET',
                    url: urlservice.url + 'checkbundleid',
                    params: {
                        "company_id": $localStorage.company_id,
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').show();
                        $scope.server_key = response.data.msg.android_server_key;
                        $scope.app_id = response.data.msg.app_id;
                        $scope.scheme_name = response.data.msg.ios_target_name;
                        $scope.upgrade_status = response.data.upgrade_status;
                        $localStorage.whilelabel_setup_page_access = response.data.whilelabel_setup_page_access;
                        if ($scope.upgrade_status != "W" && $scope.upgrade_status != "WM" && $localStorage.whilelabel_setup_page_access == 'N'){
                            $("#custommsg").modal('show');
                            $scope.errormsg = "This feature is only available for White Label Plan";
                        }
                        
                        $scope.appiconname = response.data.msg.ios_app_name;
                        $scope.appiconname_copy = response.data.msg.ios_app_name;
                        $scope.appiconnameandroid = response.data.msg.appiconnameandroid;
                        $scope.appiconnameandroid_copy = response.data.msg.appiconnameandroid;
                        if(response.data.msg.appiconnameandroid != '' && response.data.msg.appiconnameandroid != null){
                            $scope.and_appname_flag = true;
                        }
                        $scope.bundle_id_status = response.data.bundle_id_status;
                        $scope.bundle_id = $scope.bundle_id_copy = response.data.bundle_id;
                        $scope.ASbundleid = response.data.bundle_id;
                        $scope.wl_type = response.data.ios_account_type;
                        $scope.wl_type_copy = response.data.ios_account_type;
                        $scope.teamid_copy = $scope.teamid = response.data.msg.ios_teamid;
                        $scope.developerid_copy = $scope.developerid = response.data.msg.ios_developerid;
                        if(response.data.msg.ios_app_name != "" && response.data.msg.ios_app_name != null){
                            $scope.appiconname_flag = true;
                        }
                        
                        if(response.data.msg.ios_developerid != "" && response.data.msg.ios_developerid != null){
                            $scope.developerid_flag = true;
                        }
                        if($scope.mystudio_legacy_app == 'N'){
                            $scope.developerid_flag = true;
                        }
                        if(response.data.msg.ios_teamid != "" && response.data.msg.ios_teamid != null){
                            $scope.teamid_flag = true;
                        }
                        if(response.data.msg.ios_account_type != "" && response.data.msg.ios_account_type != null){
                            $scope.wl_type_flag = true;
                        }
                        if(response.data.msg.ios_bundle_id != "" && response.data.msg.ios_bundle_id != null){
                            $scope.bundle_id_flag = true;
                        }
                        $scope.wl_type = response.data.msg.ios_account_type ? response.data.msg.ios_account_type :'E';
                        if ($scope.bundle_id_status === 'Y') {
                            $scope.username_copy = $scope.username = response.data.msg.ios_username;
                            if(response.data.msg.ios_username != "" && response.data.msg.ios_username != null) {
                                    $scope.username_flag = true;
                                }
                            $scope.password_copy = $scope.password = response.data.msg.ios_password;
                            if(response.data.msg.ios_password != "" && response.data.msg.ios_password != null) {
                                    $scope.password_flag = true;
                                }
                            $scope.ios_dev_flag = (response.data.file_flag.ios_dev_status) == 'Y' ? true : false;
                            $scope.ios_prod_flag = response.data.file_flag.ios_prod_status == 'Y' ? true : false;
                            $scope.provision_dev_flag = response.data.file_flag.provision_dev_status == 'Y' ? true : false;
                            $scope.provision_prod_flag = response.data.file_flag.provision_prod_status == 'Y' ? true : false;
                            $scope.aps_dev_flag = response.data.file_flag.aps_dev_status == 'Y' ? true : false;
                            $scope.aps_prod_flag = response.data.file_flag.aps_prod_status == 'Y' ? true : false;
                            $scope.ios_dev_expiry_date = response.data.file_flag.ios_dev_expiry_date;
                            $scope.ios_prod_expiry_date = response.data.file_flag.ios_prod_expiry_date;
                            $scope.provision_dev_expiry_date = response.data.file_flag.provision_dev_expiry_date;
                            $scope.provision_prod_expiry_date = response.data.file_flag.provision_prod_expiry_date;
                            $scope.dev_pem_expiry = response.data.file_flag.dev_pem_expiry;
                            $scope.prod_pem_expiry = response.data.file_flag.prod_pem_expiry;
                        }

                        // SETTING STATUS FOR STEPS
                        if ($scope.teamid && $scope.developerid) {
                            $scope.step_1 = true;
                        }
                        if ($scope.bundle_id) {
                            $scope.step_2 = true;
                        }
                        if ($scope.username && $scope.password) {
                            $scope.step_3 = true;
                        }
                        $scope.all_files_uploaded_flag = response.data.msg.all_files_uploaded_flag;
                        $scope.getiosscreenshoturl('A');
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.updatebundleid = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatebundleid',
                    data: {
                        "company_id": $localStorage.company_id,
                        "bundle_id":$scope.bundle_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.bundle_id_status = response.data.bundle_id_status;
                        $scope.bundle_id = response.data.bundle_id;
                        $scope.scheme_name = response.data.msg.ios_target_name;
                        $scope.responsemessage = response.data.msgs;
                        $scope.responsebutton = "Ok";
                        $('#progress-full').hide();
                        $scope.checkbundleid();
//                        $("#message-modal").modal('show');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.gotostripe = function () {
                $('#custommsg').modal('hide');
                $('#custommsg').on('hidden.bs.modal', function (e) {
                    $window.location.href = window.location.origin + window.location.pathname + '#/billingstripe';
                }); 
            };
            
            // SEND APP STORE DETAILS IN EMAIL TO MYSTUDIO 
            $scope.appinfotomystudio = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'appinfotomystudio',
                    data: {
                        "company_id": $localStorage.company_id,
                        "ios_developerid":$scope.developerid,
                        "ios_teamid":$scope.teamid
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.responsemessage = response.data.msg;
                        $scope.responsebutton = "Ok";
                        $('#progress-full').hide();
                        $scope.checkbundleid();
//                        $("#message-modal").modal('show');
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.catchgoogleservicejson = function(){
                $scope.savefirebase("json");
            }
            
            $scope.googleservicejson = false;
             $scope.savefirebase = function(type){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'savefirebase',
                    data: {
                        "company_id": $localStorage.company_id,
                        "firebase":$scope.firebase,
                        "dynamic_url":$scope.dynamic_url,
                        "bundle_id":$scope.bundle_id,
                        "server_key":$scope.server_key,
                        "service_json":$scope.filepreview9,
                        "type":type
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.responsemessage = response.data.msg;
                            if(type != 'json'){
                                $scope.firebase = response.data.firebase;
                                $scope.dynamic_url = response.data.dynamic_url;
                                $scope.server_key = response.data.server_key;
                                if(response.data.server_key != "" && response.data.server_key != null && response.data.server_key !="null"){
                                    $scope.android_server_key_flag = true;
                                }
                                if($scope.exclude_android_server_key == 'Y') {
                                    $scope.android_server_key_flag = true;
                                }
                            }else{
                                $scope.googleservicejson = true;
                            }
                                if(($scope.firebase != "" && $scope.firebase != null) && ($scope.dynamic_url != "" && $scope.dynamic_url != null) && $scope.googleservicejson && $scope.android_server_key_flag) {
                                    $scope.firebasetick = true;
                                }
                        $scope.responsebutton = "Ok";
                        $('#progress-full').hide();
                        $("#message-modal").modal('show');
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.filepreview7 = null;
            $scope.getandroidstoreinfo = function(){
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getandroidstoreinfo',
                    params: {
                        "company_id": $localStorage.company_id,
                        "app_id":$scope.app_id                        
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        if(response.data.msg.android_metadata_manual_check == 'Y'){
                            $scope.androidmetadatamanualcheck = true;
                        }else{
                            $scope.androidmetadatamanualcheck = false;
                        }
                        if(response.data.msg.android_screenshot_manual_check == 'Y'){
                            $scope.androidscreenshotmanualcheck = true;
                        }else{
                            $scope.androidscreenshotmanualcheck = false;
                        }
                        $scope.androidtitle = response.data.msg.android_title;
                        $scope.googlefulldesc = response.data.msg.android_full_desc;
                        $scope.googleshortdesc = response.data.msg.android_short_desc;
                        $scope.skeleton_apk_status = response.data.msg.skeleton_apk_status;
                        if(response.data.msg.json_file != null && response.data.msg.json_file != ""){
                            $scope.json_flag = true;
                        }else{
                            $scope.json_flag = false;
                        }
                        if($scope.mystudio_legacy_app != 'N'){
                            $scope.json_flag = true;
                        }
                        if($scope.json_flag){
                            $scope.android_tickflag = true;
                        }
                        $('#progress-full').hide();
                        $scope.getiosscreenshoturl("A");
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.catchjson = function(){
                console.log("json");
                $scope.saveandroidstoreinfo("F");
            }
            
            $scope.saveandroidstoreinfo = function(type){
                if(type == "F"){
                if($scope.filepreview7 == null || $scope.filepreview7 == ""){
                    return false;
                }
                }else{
                    if($scope.androidmetadatamanualcheck){
                        return false;
                    }
                }
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'saveandroidstoreinfo',
                    data: {
                        "company_id": $localStorage.company_id,
                        "app_id":$scope.app_id,
                        "json_file":$scope.filepreview7,
                        "short_desc":$scope.googleshortdesc,
                        "full_desc":$scope.googlefulldesc,
                        "title":$scope.androidtitle,
                        "type":type
                        
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.responsemessage = response.data.msg;
                        $scope.responsebutton = "Ok";
                        $("#message-modal").modal('show');
                        $('#progress-full').hide();
                        $scope.getandroidstoreinfo();
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.updateappinfo = function(type){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateappinfo',
                    data: {
                        "company_id": $localStorage.company_id,
                        "appiconname":$scope.appiconname,
                        "appiconnameandroid":$scope.appiconnameandroid,
                        "bundle_id":$scope.bundle_id,
                        "type":type,
                        "app_id":$scope.app_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.responsemessage = response.data.msg;
                        $scope.responsebutton = "Ok";
                        $scope.appiconname = response.data.appiconname;
                        $scope.appiconname_copy = response.data.appiconname;
                        $scope.appiconnameandroid = response.data.appiconnameandroid;
                        $scope.appiconnameandroid_copy = response.data.appiconnameandroid;
                        $('#progress-full').hide();
                        $scope.checkbundleid();
                        $("#message-modal").modal('show');
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            // SAVE MANAGER DETAILS IN DB 
            $scope.savemanagerdetails = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'savemanagerdetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "ios_username":$scope.username,
                        "ios_password":$scope.password,
                        "ios_account_type" : $scope.wl_type
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.responsemessage = response.data.msg;
                        $scope.responsebutton = "Ok";
                        $('#progress-full').hide();
                        $scope.checkbundleid();
//                        $("#message-modal").modal('show');
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.uploadiosscreenshots = function(){
                $('#progress-full').show();
                setTimeout(function(){ 
                    
                    if(($scope.ios5inchimage1base64 == null || !$scope.ios5inchimage1base64) && ($scope.ios5inchimage2base64 == null || !$scope.ios5inchimage2base64) && ($scope.ios5inchimage3base64 == null || !$scope.ios5inchimage3base64) && ($scope.ios5inchimage4base64 == null || !$scope.ios5inchimage4base64)
                         && ($scope.ios6inchimage1base64 == null || !$scope.ios6inchimage1base64) && ($scope.ios6inchimage2base64 == null || !$scope.ios6inchimage2base64) && ($scope.ios6inchimage3base64 == null || !$scope.ios6inchimage3base64) && ($scope.ios6inchimage4base64 == null || !$scope.ios6inchimage4base64)
                          && ($scope.iosipadinchimage1base64 == null || !$scope.iosipadinchimage1base64) && ($scope.iosipadinchimage2base64 == null || !$scope.iosipadinchimage2base64)
                          && ($scope.iosipadinchimage3base64 == null || !$scope.iosipadinchimage3base64) && ($scope.iosipadinchimage4base64 == null || !$scope.iosipadinchimage4base64)){
                      console.log("entered");
                      $('#progress-full').hide();
                    return false;
                }
                $http({
                    method: 'POST',
                    url: urlservice.url + 'uploadiosscreenshots',
                    data: {
                        "company_id": $localStorage.company_id,
                        "scheme_name":$scope.scheme_name,
                        "5inch_file1":$scope.ios5inchimage1base64,
                        "5inch_file2":$scope.ios5inchimage2base64,
                        "5inch_file3":$scope.ios5inchimage3base64,
                        "5inch_file4":$scope.ios5inchimage4base64,
                        
                        "6inch_file1":$scope.ios6inchimage1base64,
                        "6inch_file2":$scope.ios6inchimage2base64,
                        "6inch_file3":$scope.ios6inchimage3base64,
                        "6inch_file4":$scope.ios6inchimage4base64,
                        
                        "ipad_file1":$scope.iosipadinchimage1base64,
                        "ipad_file2":$scope.iosipadinchimage2base64,
                        "ipad_file3":$scope.iosipadinchimage3base64,
                        "ipad_file4":$scope.iosipadinchimage4base64,
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.ios5inchimage1base64 = null;
                        $scope.ios5inchimage2base64 = null;
                        $scope.ios5inchimage3base64 = null;
                        $scope.ios5inchimage4base64 = null;
                        $scope.ios6inchimage1base64 = null;
                        $scope.ios6inchimage2base64 = null;
                        $scope.ios6inchimage3base64 = null;
                        $scope.ios6inchimage4base64 = null;
                        $scope.iosipadinchimage1base64 = null;
                        $scope.iosipadinchimage2base64 = null;
                        $scope.iosipadinchimage3base64 = null;
                        $scope.iosipadinchimage4base64 = null;
                        $('#progress-full').hide();
                        $("#custommsg").modal('show');
                        $scope.errormsg = "Uploaded Successfully";
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
                    
                }, 1000);
            }
            
            $scope.uploadandroidscreenshots = function(){
                $('#progress-full').show();
                setTimeout(function(){
                    
                    if(($scope.AND5inchimage1base64 == null || !$scope.AND5inchimage1base64) && ($scope.AND5inchimage2base64 == null || !$scope.AND5inchimage2base64) && ($scope.AND5inchimage3base64 == null || !$scope.AND5inchimage3base64) && ($scope.AND5inchimage4base64 == null || !$scope.AND5inchimage4base64)
                          && ($scope.ANDTABinchimage1base64 == null || !$scope.ANDTABinchimage1base64) && ($scope.ANDTABinchimage2base64 == null || !$scope.ANDTABinchimage2base64)
                          && ($scope.ANDTABinchimage3base64 == null || !$scope.ANDTABinchimage3base64) && ($scope.ANDTABinchimage4base64 == null || !$scope.ANDTABinchimage4base64)){
                      console.log("entered");
                      $('#progress-full').hide();
                    return false;
                }
                $http({
                    method: 'POST',
                    url: urlservice.url + 'uploadandroidscreenshots',
                    data: {
                        "company_id": $localStorage.company_id,
                        "scheme_name":$scope.scheme_name,
                        "and_phone_file1":$scope.AND5inchimage1base64,
                        "and_phone_file2":$scope.AND5inchimage2base64,
                        "and_phone_file3":$scope.AND5inchimage3base64,
                        "and_phone_file4":$scope.AND5inchimage4base64,
                        
                        "and_tab_file1":$scope.ANDTABinchimage1base64,
                        "and_tab_file2":$scope.ANDTABinchimage2base64,
                        "and_tab_file3":$scope.ANDTABinchimage3base64,
                        "and_tab_file4":$scope.ANDTABinchimage4base64,
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.AND5inchimage1base64,
                         $scope.AND5inchimage2base64 = null;
                         $scope.AND5inchimage3base64= null;
                         $scope.AND5inchimage4base64= null;
                         $scope.ANDTABinchimage1base64= null;
                         $scope.ANDTABinchimage2base64= null;
                         $scope.ANDTABinchimage3base64= null;
                         $scope.ANDTABinchimage4base64= null;
                        $('#progress-full').hide();
                        $("#custommsg").modal('show');
                        $scope.errormsg = "Uploaded Successfully";
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
                    
                }, 1000);
                
            }
            
            
            //android screenshots
            //tablet
            $scope.ANDTABinchimage1base64 = null;
            $scope.ANDTABinchimage1 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#ANDTABinchimage1preview').attr('src', e.target.result);
                    $("#ANDTABinchimage1preview").css("visibility", "visible");
                    $scope.ANDTABinchimage1base64 = e.target.result;
                    console.log($scope.ANDTABinchimage1base64);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadandroidscreenshots();
            };
            
            $scope.ANDTABinchimage4base64 = null;
            $scope.ANDTABinchimage4 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#ANDTABinchimage4preview').attr('src', e.target.result);
                    $("#ANDTABinchimage4preview").css("visibility", "visible");
                    $scope.ANDTABinchimage4base64 = e.target.result;
                    console.log($scope.ANDTABinchimage4base64);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadandroidscreenshots();
            };
            
            $scope.ANDTABinchimage2base64 = null;
            $scope.ANDTABinchimage2 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#ANDTABinchimage2preview').attr('src', e.target.result);
                    $("#ANDTABinchimage2preview").css("visibility", "visible");
                    $scope.ANDTABinchimage2base64 = e.target.result;
                    console.log($scope.ANDTABinchimage2base64);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadandroidscreenshots();
            };
            
            $scope.ANDTABinchimage3base64 = null;
            $scope.ANDTABinchimage3 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                   $('#ANDTABinchimage3preview').attr('src', e.target.result);
                    $("#ANDTABinchimage3preview").css("visibility", "visible");
                    $scope.ANDTABinchimage3base64 = e.target.result;
                    console.log($scope.ANDTABinchimage3base64);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadandroidscreenshots();
            };
            
            //phone
            $scope.AND5inchimage4base64 = null;
            $scope.AND5inchimage4 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#AND5inchimage4preview').attr('src', e.target.result);
                    $("#AND5inchimage4preview").css("visibility", "visible");
                    $scope.AND5inchimage4base64 = e.target.result;
                    console.log($scope.AND5inchimage4base64);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadandroidscreenshots();
            };
            
            $scope.AND5inchimage3base64 = null;
            $scope.AND5inchimage3 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#AND5inchimage3preview').attr('src', e.target.result);
                    $("#AND5inchimage3preview").css("visibility", "visible");
                    $scope.AND5inchimage3base64 = e.target.result;
                    console.log($scope.AND5inchimage3base64);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadandroidscreenshots();
            };
            
            $scope.AND5inchimage2base64 = null;
            $scope.AND5inchimage2 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#AND5inchimage2preview').attr('src', e.target.result);
                    $("#AND5inchimage2preview").css("visibility", "visible");
                    $scope.AND5inchimage2base64 = e.target.result;
                    console.log($scope.AND5inchimage2base64);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadandroidscreenshots();
            };
            
            $scope.AND5inchimage1base64 = null;
            $scope.AND5inchimage1 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {                    
                    $('#AND5inchimage1preview').attr('src', e.target.result);
                    $("#AND5inchimage1preview").css("visibility", "visible");
                    $scope.AND5inchimage1base64 = e.target.result;
                    console.log($scope.AND5inchimage1base64);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadandroidscreenshots();
            };
            
            //ios screenshots
            $scope.ios5inchimage1base64 = null;
            $scope.ios5inchimage1 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height !=2208  && img.width != 1242){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                        $('#ios5inchimage1preview').attr('src', e.target.result);
                        $("#ios5inchimage1preview").css("visibility", "visible");
                        $scope.ios5inchimage1base64 = e.target.result;
                        console.log($scope.ios5inchimage1base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
            $scope.ios5inchimage2base64 = null;
            $scope.ios5inchimage2 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height !=2208  && img.width != 1242){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                    $('#ios5inchimage2preview').attr('src', e.target.result);
                    $("#ios5inchimage2preview").css("visibility", "visible");
                    $scope.ios5inchimage2base64 = e.target.result;
                    console.log($scope.ios5inchimage2base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
            $scope.ios5inchimage3base64 = null;
            $scope.ios5inchimage3 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height !=2208  && img.width != 1242){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                            return false;
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                    $('#ios5inchimage3preview').attr('src', e.target.result);
                    $("#ios5inchimage3preview").css("visibility", "visible");
                    $scope.ios5inchimage3base64 = e.target.result;
                    console.log($scope.ios5inchimage3base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
            $scope.ios5inchimage4base64 = null;
            $scope.ios5inchimage4 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height !=2208  && img.width != 1242){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                            return false;
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                    $('#ios5inchimage4preview').attr('src', e.target.result);
                    $("#ios5inchimage4preview").css("visibility", "visible");
                    $scope.ios5inchimage4base64 = e.target.result;
                    console.log($scope.ios5inchimage4base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
//            6 inch

            $scope.ios6inchimage1base64 = null;
            $scope.ios6inchimage1 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height != 2688  && img.width != 1242){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                            return false;
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                    $('#ios6inchimage1preview').attr('src', e.target.result);
                    $("#ios6inchimage1preview").css("visibility", "visible");
                    $scope.ios6inchimage1base64 = e.target.result;
                    console.log($scope.ios6inchimage1base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
            $scope.ios6inchimage2base64 = null;
            $scope.ios6inchimage2 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height != 2688  && img.width != 1242){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                            return false;
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                    $('#ios6inchimage2preview').attr('src', e.target.result);
                    $("#ios6inchimage2preview").css("visibility", "visible");
                    $scope.ios6inchimage2base64 = e.target.result;
                    console.log($scope.ios6inchimage2base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
            $scope.ios6inchimage3base64 = null;
            $scope.ios6inchimage3 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height != 2688  && img.width != 1242){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                            return false;
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                    $('#ios6inchimage3preview').attr('src', e.target.result);
                    $("#ios6inchimage3preview").css("visibility", "visible");
                    $scope.ios6inchimage3base64 = e.target.result;
                    console.log($scope.ios6inchimage3base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
            $scope.ios6inchimage4base64 = null;
            $scope.ios6inchimage4 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height != 2688  && img.width != 1242){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                            return false;
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                    $('#ios6inchimage4preview').attr('src', e.target.result);
                    $("#ios6inchimage4preview").css("visibility", "visible");
                    $scope.ios6inchimage4base64 = e.target.result;
                    console.log($scope.ios6inchimage4base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
//            ipad
            $scope.iosipadinchimage1base64 = null;
            $scope.iosipadinchimage1 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height != 2732  && img.width != 2048){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                            return false;
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                    $('#iosipadinchimage1preview').attr('src', e.target.result);
                    $("#iosipadinchimage1preview").css("visibility", "visible");
                    $scope.iosipadinchimage1base64 = e.target.result;
                    console.log($scope.iosipadinchimage1base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
            $scope.iosipadinchimage4base64 = null;
            $scope.iosipadinchimage4 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height != 2732  && img.width != 2048){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                            return false;
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                    $('#iosipadinchimage4preview').attr('src', e.target.result);
                    $("#iosipadinchimage4preview").css("visibility", "visible");
                    $scope.iosipadinchimage4base64 = e.target.result;
                    console.log($scope.iosipadinchimage4base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
            $scope.iosipadinchimage2base64 = null;
            $scope.iosipadinchimage2 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height != 2732  && img.width != 2048){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                            return false;
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                    $('#iosipadinchimage2preview').attr('src', e.target.result);
                    $("#iosipadinchimage2preview").css("visibility", "visible");
                    $scope.iosipadinchimage2base64 = e.target.result;
                    console.log($scope.iosipadinchimage2base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
            $scope.iosipadinchimage3base64 = null;
            $scope.iosipadinchimage3 = function(input){
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if(img.height != 2732  && img.width != 2048){
                            $scope.errormsg = "Image resolution is wrong!";
                            $scope.$apply();
                            $("#custommsg").modal('show');
                            return false;
                        }
                    };
                    img.src = reader.result;
                    setTimeout(function(){ 
                    var text = ($("#custommsg").data('bs.modal') || {}).isShown;
                    if(text){
                        return false;
                    }else{
                    $('#iosipadinchimage3preview').attr('src', e.target.result);
                    $("#iosipadinchimage3preview").css("visibility", "visible");
                    $scope.iosipadinchimage3base64 = e.target.result;
                    console.log($scope.iosipadinchimage3base64);
                    }
                    }, 100);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.$apply();
                $scope.uploadiosscreenshots();
            };
            
            //ios image icon
            $scope.iosimgchanged = function(input){
                $scope.iosoverwriteimage = false;
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#iosiconpreview').attr('src', e.target.result);
                    $("#iosiconpreview").css("visibility", "visible");
                    $('#iosportraitpreview').attr('src', e.target.result);
                    $("#iosportraitpreview").css("visibility", "visible");
                    $('#ioslandscapepreview').attr('src', e.target.result);
                    $("#ioslandscapepreview").css("visibility", "visible");
                    $scope.iosiconbase64 = e.target.result;
                    console.log($scope.iosiconbase64);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.showpicker = true;
                $scope.$apply();
            };
            $scope.androidiconbase64 = null;
            $scope.androidimgchanged = function(input){
                $scope.androidoverwriteimage = false;
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#androidiconpreview').attr('src', e.target.result);
                    $("#androidiconpreview").css("visibility", "visible");
                    $('#androidportraitpreview').attr('src', e.target.result);
                    $("#androidportraitpreview").css("visibility", "visible");
                    $('#androidlandscapepreview').attr('src', e.target.result);
                    $("#androidlandscapepreview").css("visibility", "visible");
                    $scope.androidiconbase64 = e.target.result;
                    console.log($scope.androidiconbase64);
                }
                reader.readAsDataURL(input.files[0]);
                }
                $scope.showpickerandroid = true;
                $scope.$apply();
            };
            
            $scope.ioszipchanged = function(input){
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.readAsDataURL(input.files[0]);
                    reader.onload = function (e) {
                        $scope.ioszip = e.target.result;
                        console.log($scope.ioszip);
                        $scope.updateioszip();
                    }
                }
            };
        
             $scope.updateiosimages = function(){
                 var type = 'I';
                 if($scope.iosiconbase64 == null || $scope.iosiconbase64 == ""){
                     console.log("empty");
                     return false;
                 }
                 if($scope.ios_splash_option == 'U' && $scope.filepreview6 == "" ){
                     return false;
                 }
                 if($scope.ios_splash_option == 'G'){
                     $scope.filepreview6 = "";
                 }
                 if($scope.androidiconcheck){
                     type = 'B'; //both
                 }
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'iosimageupload',
                    data: {
                        "company_id": $localStorage.company_id,
                        "ios_zip":$scope.filepreview6,
                        "splash_flag":$scope.ios_splash_option,
                        "hex_value":$scope.my_color,
                        "icon_content":$scope.iosiconbase64,
                        "ios_bundle_id":$scope.bundle_id,
                        "type":type,
                        "scheme_name":$scope.scheme_name
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#custommsg").modal('show');
                        $scope.errormsg = response.data.msg;
                        $scope.ios_icon_flag = true;
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.updateandroidimages = function(){
                 var type = 'A';
                 if($scope.androidiconbase64 == null || $scope.androidiconbase64 == ""){
                     console.log("empty");
                     return false;
                 }
                 if($scope.android_splash_option == 'U' && $scope.filepreview8 == "" ){
                     return false;
                 }
                 if($scope.android_splash_option == 'G'){
                     $scope.filepreview8 = "";
                 }
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'iosimageupload',
                    data: {
                        "company_id": $localStorage.company_id,
                        "ios_zip":$scope.filepreview8,
                        "splash_flag":$scope.android_splash_option,
                        "hex_value":$scope.my_color_android,
                        "icon_content":$scope.androidiconbase64,
                        "ios_bundle_id":$scope.bundle_id,
                        "type":type,
                        "scheme_name":$scope.scheme_name
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#custommsg").modal('show');
                        $scope.errormsg = response.data.msg;
                        if(type == 'B' || type == 'A'){
                            $scope.android_icon_url_flag = true;
                        }
                        if(type == 'B' || type == 'I'){
                            $scope.ios_icon_flag = true;
                        }
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.getiosscreenshoturl = function(type){
                if($scope.bundle_id_status != "Y"){
                    return false;
                }
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getiosscreenshoturl',
                    params: {
                        "company_id": $localStorage.company_id,
                        "scheme_name":$scope.scheme_name,
                        "type":type,
                        "bundle_id":$scope.bundle_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.skeleton_apk_url = response.data.skeleton_apk_url;
                        if($scope.skeleton_apk_url != "" && $scope.skeleton_apk_url != "null" && $scope.skeleton_apk_url != null){
                            $scope.skeleton_apk_url_flag = true;
                        }
                        if(type == "I"){
                            $('#ios5inchimage1preview').attr('src', response.data.screenshot_url._5inch_1);
                            $("#ios5inchimage1preview").css("visibility", "visible");
                            $('#ios5inchimage2preview').attr('src', response.data.screenshot_url._5inch_2);
                            $("#ios5inchimage2preview").css("visibility", "visible");
                            $('#ios5inchimage3preview').attr('src', response.data.screenshot_url._5inch_3);
                            $("#ios5inchimage3preview").css("visibility", "visible");
                            $('#ios5inchimage4preview').attr('src', response.data.screenshot_url._5inch_4);
                            $("#ios5inchimage4preview").css("visibility", "visible");

                            $('#ios6inchimage1preview').attr('src', response.data.screenshot_url._6inch_1);
                            $("#ios6inchimage1preview").css("visibility", "visible");
                            $('#ios6inchimage2preview').attr('src', response.data.screenshot_url._6inch_2);
                            $("#ios6inchimage2preview").css("visibility", "visible");
                            $('#ios6inchimage3preview').attr('src', response.data.screenshot_url._6inch_3);
                            $("#ios6inchimage3preview").css("visibility", "visible");
                            $('#ios6inchimage4preview').attr('src', response.data.screenshot_url._6inch_4);
                            $("#ios6inchimage4preview").css("visibility", "visible");

                            $('#iosipadinchimage1preview').attr('src', response.data.screenshot_url.ipad_1);
                            $("#iosipadinchimage1preview").css("visibility", "visible");
                            $('#iosipadinchimage2preview').attr('src', response.data.screenshot_url.ipad_2);
                            $("#iosipadinchimage2preview").css("visibility", "visible");
                            $('#iosipadinchimage3preview').attr('src', response.data.screenshot_url.ipad_3);
                            $("#iosipadinchimage3preview").css("visibility", "visible");
                            $('#iosipadinchimage4preview').attr('src', response.data.screenshot_url.ipad_4);
                            $("#iosipadinchimage4preview").css("visibility", "visible");
                        }else if(type == "A"){
                            $('#AND5inchimage1preview').attr('src', response.data.screenshot_url.phone1);
                            $("#AND5inchimage1preview").css("visibility", "visible");
                            $('#AND5inchimage2preview').attr('src', response.data.screenshot_url.phone2);
                            $("#AND5inchimage2preview").css("visibility", "visible");
                            $('#AND5inchimage3preview').attr('src', response.data.screenshot_url.phone3);
                            $("#AND5inchimage3preview").css("visibility", "visible");
                            $('#AND5inchimage4preview').attr('src', response.data.screenshot_url.phone4);
                            $("#AND5inchimage4preview").css("visibility", "visible");

                            $('#ANDTABinchimage1preview').attr('src', response.data.screenshot_url.tab_1);
                            $("#ANDTABinchimage1preview").css("visibility", "visible");
                            $('#ANDTABinchimage2preview').attr('src', response.data.screenshot_url.tab_2);
                            $("#ANDTABinchimage2preview").css("visibility", "visible");
                            $('#ANDTABinchimage3preview').attr('src', response.data.screenshot_url.tab_3);
                            $("#ANDTABinchimage3preview").css("visibility", "visible");
                            $('#ANDTABinchimage4preview').attr('src', response.data.screenshot_url.tab_4);
                            $("#ANDTABinchimage4preview").css("visibility", "visible");
                        }
                            $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }
            
            $scope.getappstoreinfo = function(){
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getappstoreinfo',
                    params: {
                        "company_id": $localStorage.company_id,
                        "app_id":$scope.app_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                            if(response.data.msg.ios_metadata_manual_check == 'Y') {
                                $scope.iosmetadatamanualcheck = true;
                            } else {
                                $scope.iosmetadatamanualcheck = false;
                            }
                            if(response.data.msg.ios_screenshot_manual_check == 'Y') {
                                $scope.iosscreenshotmanualcheck = true;
                            } else {
                                $scope.iosscreenshotmanualcheck = false;
                            }
                            $scope.ASlocalizablename = response.data.msg.localizable_info_name;
                            $scope.ASlocalizableurl = response.data.msg.localizable_info_policy_url;
                            $scope.ASlocalizablesubtile = response.data.msg.localizable_info_subtitle;

                            $scope.ASappleiD = response.data.msg.general_info_apple_id;
                            $scope.ASdescription = response.data.msg.general_info_description;
                            $scope.ASpromotext = response.data.msg.general_info_promotional_text;
                            $scope.ASsupporturl = response.data.msg.general_info_support_url;
                            $scope.ASmarketingurl = response.data.msg.general_info_marketing_url;
                            $scope.ASkeywords = response.data.msg.general_info_keywords;

                            $scope.AScopyright = response.data.msg.copyright;

                            $scope.AStradefname = response.data.msg.trade_rep_contact_info_fname;
                            $scope.AStradelname = response.data.msg.trade_rep_contact_info_lname;
                            $scope.AStradeaddress = response.data.msg.trade_rep_contact_info_address;
                            $scope.AStradebuilding = response.data.msg.trade_rep_contact_info_suite;
                            $scope.AStradecity = response.data.msg.trade_rep_contact_info_city;
                            $scope.AStradestate = response.data.msg.trade_rep_contact_info_state;
                            $scope.AStradepostal = response.data.msg.trade_rep_contact_info_postal_code;
                            $scope.AStradephone = response.data.msg.trade_rep_contact_info_phone;
                            if(response.data.msg.trade_rep_contact_info_country != null && response.data.msg.trade_rep_contact_info_country != ""){
                                $scope.AStradecountry = response.data.msg.trade_rep_contact_info_country;
                            }else{
                                $scope.AStradecountry = "United States";
                            }
                            $scope.AStradeemail = response.data.msg.trade_rep_contact_info_email;

                            $scope.ASARIusername = response.data.msg.app_review_info_username;
                            $scope.ASARIpassword = response.data.msg.app_review_info_password;
                            $scope.ASARIfname = response.data.msg.app_review_info_fname;
                            $scope.ASARIlname = response.data.msg.app_review_info_lname;
                            $scope.ASARIemail = response.data.msg.app_review_info_email;
                            $scope.ASARIphone = response.data.msg.app_review_info_phone;
                            $scope.ASARInotes = response.data.msg.app_review_info_note;
                            $('#progress-full').hide();
                            $scope.getiosscreenshoturl("I");
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }
            
            $scope.updateaccounttype = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateaccounttype',
                    data: {
                        "company_id": $localStorage.company_id,
                        "wl_type":$scope.wl_type,
                        "bundle_id":$scope.bundle_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.responsemessage = response.data.msg;
                        $scope.responsebutton = "Ok";
                        $('#progress-full').hide();
                        $scope.checkbundleid();
//                        $("#message-modal").modal('show');
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.updateappstoreinformation = function(type){
                if($scope.iosmetadatamanualcheck && type != 'I'){
                    return false;
                }
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateappstoreinformation',
                    data: {
                        "company_id": $localStorage.company_id,
                        "app_id":$scope.app_id,
                        "type":type,
                        "company_code":$scope.studio_code,
                        "localizable_info_name":$scope.ASlocalizablename,
                        "localizable_info_policy_url":$scope.ASlocalizableurl,
                        "localizable_info_subtitle":$scope.ASlocalizablesubtile,
                        
                        "general_info_apple_id":$scope.ASappleiD,
                        "general_info_description":$scope.ASdescription,
                        "general_info_promotional_text":$scope.ASpromotext,
                        "general_info_support_url":$scope.ASsupporturl,
                        "general_info_marketing_url":$scope.ASmarketingurl,
                        "general_info_keywords":$scope.ASkeywords,
                        
                        "copyright":$scope.AScopyright,
                        
                        "trade_rep_contact_info_fname":$scope.AStradefname,
                        "trade_rep_contact_info_lname":$scope.AStradelname,
                        "trade_rep_contact_info_address":$scope.AStradeaddress,
                        "trade_rep_contact_info_suite":$scope.AStradebuilding,
                        "trade_rep_contact_info_city":$scope.AStradecity,
                        "trade_rep_contact_info_state":$scope.AStradestate,
                        "trade_rep_contact_info_postal_code":$scope.AStradepostal,
                        "trade_rep_contact_info_phone":$scope.AStradephone,
                        "trade_rep_contact_info_country":$scope.AStradecountry,
                        "trade_rep_contact_info_email":$scope.AStradeemail,
                        
                        "app_review_info_username":$scope.ASARIusername,
                        "app_review_info_password":$scope.ASARIpassword,
                        "app_review_info_fname":$scope.ASARIfname,
                        "app_review_info_lname":$scope.ASARIlname,
                        "app_review_info_email":$scope.ASARIemail,
                        "app_review_info_phone":$scope.ASARIphone,
                        "app_review_info_note":$scope.ASARInotes,
                        
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                            $scope.ASlocalizablename = response.data.msg.localizable_info_name;
                            $scope.ASlocalizableurl = response.data.msg.localizable_info_policy_url;
                            $scope.ASlocalizablesubtile = response.data.msg.localizable_info_subtitle;

                            $scope.ASappleiD = response.data.msg.general_info_apple_id;
                            $scope.ASdescription = response.data.msg.general_info_description;
                            $scope.ASpromotext = response.data.msg.general_info_promotional_text;
                            $scope.ASsupporturl = response.data.msg.general_info_support_url;
                            $scope.ASmarketingurl = response.data.msg.general_info_marketing_url;
                            $scope.ASkeywords = response.data.msg.general_info_keywords;

                            $scope.AScopyright = response.data.msg.copyright;

                            $scope.AStradefname = response.data.msg.trade_rep_contact_info_fname;
                            $scope.AStradelname = response.data.msg.trade_rep_contact_info_lname;
                            $scope.AStradeaddress = response.data.msg.trade_rep_contact_info_address;
                            $scope.AStradebuilding = response.data.msg.trade_rep_contact_info_suite;
                            $scope.AStradecity = response.data.msg.trade_rep_contact_info_city;
                            $scope.AStradestate = response.data.msg.trade_rep_contact_info_state;
                            $scope.AStradepostal = response.data.msg.trade_rep_contact_info_postal_code;
                            $scope.AStradephone = response.data.msg.trade_rep_contact_info_phone;
                            if(response.data.msg.trade_rep_contact_info_country != null && response.data.msg.trade_rep_contact_info_country != ""){
                                $scope.AStradecountry = response.data.msg.trade_rep_contact_info_country;
                            }else{
                                $scope.AStradecountry = "United States";
                            }
                            $scope.AStradeemail = response.data.msg.trade_rep_contact_info_email;

                            $scope.ASARIusername = response.data.msg.app_review_info_username;
                            $scope.ASARIpassword = response.data.msg.app_review_info_password;
                            $scope.ASARIfname = response.data.msg.app_review_info_fname;
                            $scope.ASARIlname = response.data.msg.app_review_info_lname;
                            $scope.ASARIemail = response.data.msg.app_review_info_email;
                            $scope.ASARIphone = response.data.msg.app_review_info_phone;
                            $scope.ASARInotes = response.data.msg.app_review_info_note;
                            $scope.errormsg = "Updated Successfully";
                            $('#progress-full').hide();
                            $("#custommsg").modal('show');
                        } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.getwldashboarddetails = function(){
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getwldashboarddetails',
                    params: {
                        "company_id": $localStorage.company_id,
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.studio_code = response.data.msg.company_code;
                        $scope.mystudio_legacy_app =response.data.msg.mystudio_legacy_app;
                        if($scope.mystudio_legacy_app == "" || $scope.mystudio_legacy_app == null || $scope.mystudio_legacy_app == "null" || !$scope.mystudio_legacy_app){
                            $scope.mystudio_legacy_app = 'N';
                        }
                        $scope.exclude_android_server_key = response.data.msg.exclude_android_server_key;
                        $scope.new_build_version = response.data.msg.new_build_version;
                            $scope.app_store_status = response.data.msg.app_store_status;
                            if(response.data.msg.latest_version != null && response.data.msg.latest_version != '') {
                            var latest_version = response.data.msg.latest_version;
                            var count = 0;
                            var string = '';
                            for(var i = 0 ; i < latest_version.length ;i++){
                                if(latest_version[i] == '.'){
                                    count++;
                                }
                                if(count == 2){
                                    break;
                                }
                                string = string + latest_version[i];
                            }
                            $scope.latest_version = string;
                        }
                            $('#progress-full').show();
                            $scope.current_build_version = response.data.msg.new_build_version;
                            if(response.data.msg.new_build_version != null && response.data.msg.new_build_version != '') {
                                var current_build_version = response.data.msg.new_build_version;
                                var count = 0;
                                var string = '';
                                for (var i = 0; i < current_build_version.length; i++) {
                                    if (current_build_version[i] == '.') {
                                        count++;
                                    }
                                    if (count == 2) {
                                        break;
                                    }
                                    string = string + current_build_version[i];
                                }
                                $scope.current_build_version_2digit = string;
                            }
                            if($scope.app_store_status == 'Prepare for Submission'){
                                $scope.current_build_version = "Pending";
                            }
                            if(response.data.msg.mystudio_google_app == 'Y'){
                                $scope.mystudio_google_app = true;
                            }else{
                                $scope.mystudio_google_app = false;
                            }
                            $scope.android_version_number = response.data.msg.android_version_number;
                            $scope.application_status =  response.data.msg.application_status;
                            $scope.android_application_status = response.data.msg.android_application_status;
                            $scope.stripe_status = response.data.msg.stripe_status;
                            $scope.wepay_status = response.data.msg.wepay_status;
                            $scope.app_id = response.data.msg.app_id;
                            if(response.data.msg.android_server_key != "" && response.data.msg.android_server_key != null && response.data.msg.android_server_key != "null"){
                                $scope.android_server_key_flag = true;
                            }
                            if($scope.exclude_android_server_key == 'Y'){
                                $scope.android_server_key_flag = true;
                            }
                            if(response.data.msg.google_services_json != "" && response.data.msg.google_services_json != null && response.data.msg.google_services_json != "null"){
                                $scope.googleservicejson = true;
                            }
                            if($scope.stripe_status == 'Y' || $scope.wepay_status == 'Y'){
                                $scope.account_status_flag = true;
                            }
                            $('#progress-full').show();
                    
                        $scope.firebase = response.data.msg.firebase_snippet;
                        $scope.firebase_copy = response.data.msg.firebase_snippet;
                        $scope.dynamic_url = response.data.msg.dynamic_url;
                        $scope.dynamic_url_copy = response.data.msg.dynamic_url;
                        $scope.server_key = $scope.server_key_copy =  response.data.msg.android_server_key;
                        
                        if(($scope.firebase != "" && $scope.firebase != null) && ($scope.dynamic_url != "" && $scope.dynamic_url != null) && $scope.googleservicejson && $scope.android_server_key_flag){
                            $scope.firebasetick = true;
                        }
                        $('#progress-full').show();
                            if (response.data.msg.ios_username != "" && response.data.msg.ios_username != null) {
                                $scope.username_flag = true;
                            }
                            if (response.data.msg.ios_password != "" && response.data.msg.ios_password != null) {
                                $scope.password_flag = true;
                            }
                        $scope.ios_icon_flag = $scope.android_icon_url_flag = false;
                        if(response.data.artwork.ios_icon_url != "" && response.data.artwork.ios_icon_url != 'null'){
                            $scope.ios_icon_flag = true;
                        }
                        if(response.data.artwork.android_icon_url != "" && response.data.artwork.android_icon_url != 'null'){
                            $scope.android_icon_url_flag = true;
                        }
                        if(response.data.msg.ios_developerid != "" && response.data.msg.ios_developerid != null && response.data.msg.ios_developerid != "null"){
                            $scope.developerid_flag = true;
                        }
                        if($scope.mystudio_legacy_app == 'N'){
                            $scope.developerid_flag = true;
                        }
                        if(response.data.msg.ios_teamid != "" && response.data.msg.ios_teamid != null && response.data.msg.ios_teamid != "null"){
                            $scope.teamid_flag = true;
                        }
                        if(response.data.msg.ios_account_type != "" && response.data.msg.ios_account_type != null && response.data.msg.ios_account_type != "null"){
                            $scope.wl_type_flag = true;
                        }
                        if(response.data.msg.ios_bundle_id != "" && response.data.msg.ios_bundle_id != null && response.data.msg.ios_bundle_id != "null"){
                            $scope.bundle_id_flag = true;
                        }
                        $scope.all_files_uploaded_flag = response.data.msg.all_files_uploaded_flag;
                        $scope.wl_type = response.data.msg.ios_account_type ? response.data.msg.ios_account_type :'E';
                        if($scope.wl_type == 'I' && ($scope.all_files_uploaded_flag == 'Y' || $scope.all_files_uploaded_flag == 'R')){
                            $scope.iosdectick = true;
                        }else if($scope.wl_type == 'E'){
                            if($scope.developerid_flag && $scope.teamid_flag && $scope.bundle_id_flag && $scope.username_flag && $scope.password_flag){
                                $scope.iosdectick = true;
                            }
                        }
                        if(response.data.msg.json_file != null && response.data.msg.json_file != ""){
                            $scope.json_flag = true;
                        }else{
                            $scope.json_flag = false;
                        }
                        if($scope.mystudio_legacy_app != 'N'){
                            $scope.json_flag = true;
                        }
                        $scope.skeleton_apk_status = response.data.msg.skeleton_apk_status;
                        if($scope.json_flag){
                            $scope.android_tickflag = true;
                        }
                        var percent = 0;
                        if($scope.account_status_flag){
                            percent += 16.6666666667;
                        }
                        if($scope.ios_icon_flag){
                            percent += 16.6666666667;
                        }
                        if($scope.iosdectick){
                            percent += 16.6666666667;
                        }
                        if($scope.android_tickflag){
                            percent += 16.6666666667;
                        }
                        if($scope.firebasetick){
                            percent += 16.6666666667;
                        }
                        if($scope.json_flag){
                            percent += 16.6666666667;
                        }
                        var decimal = (percent.toString()).split(".");
                        $scope.percent = decimal[0];
                        percent = percent+"%"
                        $scope.prog_per = {
                                "width": percent
                            }
                        if($scope.percent != 100){
                            $scope.wlaftersubmit = false;
                            $scope.wldashboardview = true;
                        }
                        $('#progress-full').show();
                        if($scope.current_build_version_2digit < $scope.latest_version){
                            $scope.wlaftersubmit = false;
                            $scope.wldashboardview = true;
                        }
                        if($scope.application_status == 'C' && $scope.percent == 100 && $scope.current_build_version_2digit < $scope.latest_version){
                            $scope.wlaftersubmit = true;
                            $scope.wldashboardview = false;
                            $scope.upgradebutton_flag = true;
                        }
                        if($scope.application_status == 'C' && $scope.percent != 100  && $scope.current_build_version_2digit < $scope.latest_version){
                           $scope.wlaftersubmit = false;
                           $scope.wldashboardview = true;
                        }
                        if($scope.application_status == 'C' && $scope.percent == 100 && $scope.current_build_version_2digit == $scope.latest_version){
                            $scope.wlaftersubmit = true;
                            $scope.wldashboardview = false;
                            $scope.upgradebutton_flag = false;
                        }
                        if($scope.application_status == 'T' || $scope.application_status == 'R' || $scope.application_status == 'P') {
                                $scope.wlaftersubmit = true;
                                $scope.wldashboardview = false;
                                $scope.upgradebutton_flag = false;
                                $scope.disablealltabs = {"pointer-events": "none", "opacity": 0.7, "background": "#CCC"};
                        }
                        if($scope.application_status == 'PF') {
                                $scope.wlaftersubmit = true;
                                $scope.wldashboardview = false;
                        }  
                        if ($scope.application_status == 'F') {
                            $scope.error_msg = response.data.msg.error_msg;
                        }
                        if(response.data.msg.bundleid_error_flag == "Y" && $scope.application_status == 'F'){
                            $scope.application_status = '';
                            $scope.wlaftersubmit = false;
                                $scope.wldashboardview = true;
                                $scope.bundleid_error_flag = true;
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "The App bundle ID you provided is not valid. Revise this here.";
                        }else if(response.data.msg.credential_error_flag == "Y" && $scope.application_status == 'F'){
                            $scope.application_status = '';
                            $scope.wlaftersubmit = false;
                            $scope.credential_error_flag = true;
                                $scope.wldashboardview = true;
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Username or Password error";
                        }else if(response.data.msg.teamid_error_flag == "Y" && $scope.application_status == 'F'){
                            $scope.application_status = '';
                            $scope.wlaftersubmit = false;
                            $scope.teamid_error_flag = true;
                                $scope.wldashboardview = true;
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Team ID error";
                        }else if(response.data.msg.metadata_error != "" && response.data.msg.metadata_error != null && $scope.application_status == 'F'){
                            $scope.application_status = '';
                            $scope.wlaftersubmit = false;
                                $scope.wldashboardview = true;
                                $scope.metadata_error = true;
                            $scope.builderror_flag = true;
                            $scope.builderror_text = response.data.msg.metadata_error;
                        }else if(response.data.msg.ios_icon_error == "Y" && $scope.application_status == 'F'){
                            $scope.application_status = '';
                            $scope.ios_icon_error = true;
                            $scope.wlaftersubmit = false;
                                $scope.wldashboardview = true;
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Ios Icon error. Please upload new image";
                        }else if(response.data.msg.android_icon_error == "Y" && $scope.application_status == 'F'){
                            $scope.application_status = '';
                            $scope.android_icon_error = true;
                            $scope.wlaftersubmit = false;
                                $scope.wldashboardview = true;
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Android Icon error. Please upload new image"
                        }else if(response.data.msg.ios_screenshot_error == "Y" && $scope.application_status == 'F'){
                            $scope.application_status = '';
                            $scope.ios_screenshot_error = true;
                            $scope.wlaftersubmit = false;
                                $scope.wldashboardview = true;
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Ios Screenshot error. Please upload new image";
                        }else if(response.data.msg.android_screenshot_error == "Y" && $scope.application_status == 'F'){
                            $scope.application_status = '';
                            $scope.android_screenshot_error = true;
                            $scope.wlaftersubmit = false;
                                $scope.wldashboardview = true;
                            $scope.builderror_flag = true;
                            $scope.builderror_text = "Android Screenshot error. Please upload new image";
                        }else{
                            $scope.builderror_flag = false;
                        }
                        if (($scope.app_store_status == 'Rejected' && $scope.percent == 100 && $scope.current_build_version_2digit == $scope.latest_version) || ($scope.app_store_status == 'Metadata Rejected' && $scope.percent == 100 && $scope.current_build_version_2digit == $scope.latest_version)) {
                            $scope.app_store_error_msg = response.data.msg.app_store_error_msg;
                            $scope.wlaftersubmit = true;
                            $scope.wldashboardview = false;
                        }
                        $('#progress-full').show();
                        $scope.checkbundleid();
//                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }
            
            
            //DOCUMENT READY FUNCTION
            angular.element(document).ready(function () {
//                $scope.getwldashboarddetails();
            });
            
            $scope.buildtheapp = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'buildtheapp',
                    data: {
                        "company_id": $localStorage.company_id,
                        "app_id":$scope.app_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                         $scope.getwldashboarddetails();   
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }
            
            
            $scope.processskeletonapk = function(){
                if($scope.skeleton_apk_status == 'P' || $scope.json_flag){
                    return false;
                }
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'processskeletonapk',
                    data: {
                        "company_id": $localStorage.company_id,
                        "bundle_id":$scope.bundle_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.responsemessage = "Creating APK process started . Download option will be available soon.";
                        $scope.responsebutton = "Ok";
                        $('#progress-full').hide();
                        $("#message-modal").modal('show');
                        $scope.skeleton_apk_status = response.data.skeleton_apk_status;
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }
            
            $scope.catchmanualmeta = function(type){
                var flag;
                if(type == 'A'){
                    if($scope.androidmetadatamanualcheck){
                        flag = 'Y';
                    }else{
                        flag = 'N';
                    }
                }else if(type == 'I'){
                    if($scope.iosmetadatamanualcheck){
                        flag = 'Y';
                    }else{
                        flag = 'N';
                    }
                }
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'catchmanualmeta',
                    data: {
                        "company_id": $localStorage.company_id,
                        "app_id":$scope.app_id,
                        "flag":flag,
                        "type":type
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        if(response.data.msg.android_metadata_manual_check == 'Y'){
                            $scope.androidmetadatamanualcheck = true;
                        }else{
                            $scope.androidmetadatamanualcheck = false;
                        }
                        if(response.data.msg.ios_metadata_manual_check == 'Y'){
                            $scope.iosmetadatamanualcheck = true;
                        }else{
                            $scope.iosmetadatamanualcheck = false;
                        }
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }
            
            $scope.catchmanualscreen = function(type){
                var flag;
                if(type == 'A'){
                    if($scope.androidscreenshotmanualcheck){
                        flag = 'Y';
                    }else{
                        flag = 'N';
                    }
                }else if(type == 'I'){
                    if($scope.iosscreenshotmanualcheck){
                        flag = 'Y';
                    }else{
                        flag = 'N';
                    }
                }
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'catchmanualscreen',
                    data: {
                        "company_id": $localStorage.company_id,
                        "app_id":$scope.app_id,
                        "flag":flag,
                        "type":type
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                    })
                        .then(function (response) {
                    if (response.data.status == 'Success') {
                        if(response.data.msg.android_screenshot_manual_check == 'Y'){
                            $scope.androidscreenshotmanualcheck = true;
                        }else{
                            $scope.androidscreenshotmanualcheck = false;
                        }
                        if(response.data.msg.ios_screenshot_manual_check == 'Y'){
                            $scope.iosscreenshotmanualcheck = true;
                        }else{
                            $scope.iosscreenshotmanualcheck = false;
                        }
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        $("#message-modal").modal('show');
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }
            
            $scope.$watch(function () {return $rootScope.currentwltab;}, function (newVal, oldVal) {
                if($rootScope.currentwltab == 'DASHBOARD'){
                    $scope.wldashboard();
                }else if($rootScope.currentwltab == 'APPLE'){
                    $scope.apple();
                }else if($rootScope.currentwltab == 'GOOGLE'){
                    $scope.goandroid();
                }else if($rootScope.currentwltab == 'FIREBASE'){
                    $scope.gofirebase();
                }
            });
            
        } else {
            $location.path('/login');
        }
        
        //DOCUMENT READY FUNCTION
        angular.element(document).ready(function () {
            var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
            if(isChrome){
                $('#eyeicon').css('margin-left','140px');
            }else{
                $('#eyeicon').css('margin-left','157px');
            }
            if ($(window).width() < 720) {
                $scope.devprovname = "dev.mobileprovision";
                $scope.prodprovname = "dist.mobileprovision";
            }
            
            $(window).on('resize', function () {
            console.log("width is :" + $(window).width());
            if ($(window).width() < 720) {
                $scope.devprovname = "dev.mobileprovision";
                $scope.prodprovname = "dist.mobileprovision";
                $scope.$apply();
            }else{
                $scope.devprovname = "AppName_development.mobileprovision";
                $scope.prodprovname = "AppName_distribution.mobileprovision";
                $scope.$apply();
            }
            });
        });

    };
    
    module.exports = WhiteLabelSetupController;
   