AdminStudioMembersController.$inject=['$scope',  '$compile','$location', '$http', '$localStorage', 'urlservice',  '$filter', '$route', 'DTOptionsBuilder', 'DTColumnDefBuilder','DTColumnBuilder','DTDefaultOptions','$window','$rootScope'];
function AdminStudioMembersController($scope, $compile, $location, $http, $localStorage, urlservice, $filter, $route,DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder,DTDefaultOptions,$window,$rootScope) {

if($localStorage.isadminlogin === 'Y') {
        $rootScope.sideBarHide = true;
        $rootScope.adminsideBarHide = true;
        $rootScope.hidesidebarlogin = false;
        $rootScope.activepagehighlight = 'adminmembers';
        $rootScope.admincmpnynme = $localStorage.admincmpnynme;
        tooltip.hide();
        $scope.showeditview = false;
        $scope.countrylist = ["United States", "Canada", "United Kingdom", "Australia", "Other"];
        $scope.leadsourcelist = ["Referral", "Google Search", "Facebook", "Email Marketing", "Tradeshow"];
        $scope.countrycodes = ["US", "CA", "UK",'AU'];
        $scope.industrylist = ["Martial Arts", "Dance", "Yoga", "Crossfit", "Gymnastics", "Summer Camps", "Nonprofit", "Church", "Consulting", "Other"];
               
        $scope.getAccounts = function(){
                DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                $scope.accountdtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'getadminaccounts',
                           xhrFields: {
                            withCredentials: true
                         },   
                            dataSrc: function (json) {
                                $scope.accountlistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                    $scope.adminlogout();
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            }                           
                        })
                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('order', [0, 'desc'])
                    $scope.accountdtColumns = [
                            DTColumnBuilder.newColumn('company_name').withTitle('Studio Name').withClass('col_ta1').renderWith(
                            function (data, type, full,meta) {
//                                return '<span class="buyername"  data-toggle="tooltip" title="' + data + '" ng-click="openmodal(\'' + full.web_site + '\' ,\''+ full.studio_phone +'\',\''+ full.user_lastname +'\',\''+ full.user_firstname +'\',\''+ full.payment_plan +'\',\''+ full.owner_name +'\',\''+ full.lead_source +'\',\''+ data +'\');">' + data + '</span>';
                            return '<span class="buyername"  data-toggle="tooltip" title="' + data + '" ng-click="openmodal(\'' + full.company_id + '\' ,\'' + meta.row + '\',\'' + full.user_id + '\' );">' + data+ '</span>';
                            
                            }),
                            DTColumnBuilder.newColumn('payment_plan').withTitle('Payment Plan').withClass('col_ta1').renderWith(
                            function (data, type, full,meta) {
                                if(full.payment_plan !== 'Inactive' && full.payment_plan !== 'Trial') {
                                    return '<span id="payment_plan" class="buyername"  data-toggle="tooltip" title="' + data + '" ng-click="downgradeplan(\'' + full.company_id + '\' ,\'' + meta.row + '\',\'' + full.user_id + '\' );">' + full.payment_plan + '</span>';
                                }else{
                                    return '<span id="payment_plan" data-toggle="tooltip" title="' + data + '" >' + full.payment_plan + '</span>';
                                }
                            }),
                            DTColumnBuilder.newColumn('payment_status').withTitle('Payment Status').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.payment_status + '</span>';
                            }),
                            DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_ta6').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registered_date + '</span>';
                            }),
                            DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_ta6').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.lead_source + '</span>';
                            }),
                            DTColumnBuilder.newColumn('owner_name').withTitle('Account Owner').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.owner_name + '</span>';
                            }),
                            DTColumnBuilder.newColumn('stripe_account').withTitle('Stripe Account').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.stripe_account + '</span>';
                            }),
                            
                            DTColumnBuilder.newColumn('cc').withTitle('Credit Card %').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                if(full.cc_processing !=null ){
                                    return '<span  data-toggle="tooltip" title="' + full.cc_processing.PP + '" >' + full.cc_processing.PP + '%</span>';
                                }else{
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + '</span>';
                                }                                
                            }).notSortable(),
                            
                            DTColumnBuilder.newColumn('processing_rate').withTitle('Transaction Cost').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                if(full.cc_processing !=null ){
                                    return '<span  data-toggle="tooltip" title="' + full.cc_processing.PT  + '" >$'+full.cc_processing.PT + '</span>';
                                }else{
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + '</span>';
                                }
                            }).notSortable(),
                            
                            DTColumnBuilder.newColumn('total_app_users').withTitle('Total App Users').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.total_app_users + '</span>';
                            }),
//                            DTColumnBuilder.newColumn('total_event_sales').withTitle('Total Event Sales').withClass('col_tc4').renderWith(
//                            function (data, type, full) {
//                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.total_event_sales + '</span>';
//                            }),
//                            DTColumnBuilder.newColumn('total_membership_sales').withTitle('Total Membership Sales').withClass('col_tc4').renderWith(
//                            function (data, type, full) {
//                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.total_membership_sales + '</span>';
//                            }),
                            DTColumnBuilder.newColumn('user_firstname').withTitle('First Name').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.user_firstname + '</span>';
                            }),
                            DTColumnBuilder.newColumn('user_lastname').withTitle('Last Name').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.user_lastname + '</span>';
                            }),
                            DTColumnBuilder.newColumn('user_phone').withTitle('Phone').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.user_phone + '</span>';
                            }),
                            DTColumnBuilder.newColumn('email_for_log_in').withTitle('Email for Log In').withClass('col_tc4').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.email_for_log_in + '</span>';
                            }),
                            DTColumnBuilder.newColumn('company_code').withTitle('Studio Code').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.company_code + '</span>';
                            }),
                            DTColumnBuilder.newColumn('Industry').withTitle('Industry').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.Industry + '</span>';
                            }),
                            DTColumnBuilder.newColumn('next_payment_date').withTitle('Next Payment Date').withClass('col_tc4').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.next_payment_date + '</span>';
                            }),
                            DTColumnBuilder.newColumn('next_payment_amount').withTitle('Next Payment Amount').withClass('col_tc4').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.next_payment_amount + '</span>';
                            }),
                            DTColumnBuilder.newColumn('last_login').withTitle('Last Log In').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.last_login + '</span>';
                            }),
                            DTColumnBuilder.newColumn('studio_address').withTitle('Studio Address').withClass('col_tc4').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.studio_address + '</span>';
                            }),
                            DTColumnBuilder.newColumn('studio_phone').withTitle('Studio Phone').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.studio_phone + '</span>';
                            }),
                            DTColumnBuilder.newColumn('web_site').withTitle('Studio Website').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.web_site + '</span>';
                            }),                            
                    ];
                   $scope.admindtInstance = {};
                
                $('#progress-full').hide();
                angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                })
            };
            $scope.getAccounts();
            //edit modal
            $scope.openmodal = function (company_id, row, user_id) {
                $('#progress-full').show();
                $scope.company_id = company_id;
                $scope.user_id = user_id;
                $scope.website = $scope.edit_website = $scope.accountlistcontents[row].web_site;
                $scope.studiophone = $scope.studio_phone = $scope.accountlistcontents[row].studio_phone;
                $scope.industry = $scope.accountlistcontents[row].Industry;
                $scope.userphone = $scope.user_phone = $scope.accountlistcontents[row].user_phone;
                $scope.userlastname = $scope.user_lastname = $scope.accountlistcontents[row].user_lastname;
                $scope.userfirstname = $scope.user_firstname = $scope.accountlistcontents[row].user_firstname;
                $scope.companyname = $scope.studio_name = $scope.accountlistcontents[row].company_name;
                $scope.ownername = $scope.owner_name = $scope.accountlistcontents[row].owner_name;
                $scope.leadsource = $scope.lead_source = $scope.accountlistcontents[row].lead_source;
                $scope.email_for_log_in = $scope.edit_email_for_log_in = $scope.accountlistcontents[row].email_for_log_in;
                $scope.company_code = $scope.edit_company_code = $scope.accountlistcontents[row].company_code;
                $scope.street_address = $scope.edit_street_address = $scope.accountlistcontents[row].street_address;
                $scope.city = $scope.edit_city = $scope.accountlistcontents[row].city;
                $scope.state = $scope.edit_state = $scope.accountlistcontents[row].state;
                $scope.postal_code = $scope.edit_postal_code = $scope.accountlistcontents[row].postal_code;
                $scope.country = $scope.accountlistcontents[row].country;

                $scope.otherindustrytype = $scope.industrylist.indexOf($scope.industry);
                if ($scope.otherindustrytype !== -1 || !$scope.industry) {
                    $scope.edit_industry = $scope.industry;
                } else if ($scope.otherindustrytype === -1) {
                    $scope.edit_industry = 'Other';
                    $scope.edit_other_industry = $scope.industry;
                }

                $scope.othercountrytype = $scope.countrycodes.indexOf($scope.country);
                if ($scope.othercountrytype !== -1 || !$scope.country) {
                    if ($scope.country === 'US') {
                        $scope.edit_country = "United States";
                    } else if ($scope.country === 'UK') {
                        $scope.edit_country = "United Kingdom";
                    } else if ($scope.country === 'CA') {
                        $scope.edit_country = "Canada";
                    } else if ($scope.country === 'AU') {
                        $scope.edit_country = "Australia";
                    } else {
                        $scope.edit_country = "United States";
                    }
                } else if ($scope.othercountrytype === -1) {
                    $scope.edit_country = 'Other';
                    $scope.edit_other_country = $scope.country;
                }

                $scope.disableifnotavailable = false;
                $scope.setEditModalWidth();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getStudioSalesFromAdminPanel',
                    data: {
                        'company_id': $scope.company_id,
                        'user_email': $scope.edit_email_for_log_in,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then(function (response) {
                    if (response.data.status === 'Success') {  
                        $scope.wp_currency_symbol = response.data.msg.wp_currency_symbol;
                        $scope.total_event_sales = response.data.msg.total_event_sales;
                        $scope.total_membership_sales = response.data.msg.total_membership_sales;
                        $("#editaccount-modal").modal('show');
                        $('#progress-full').hide();
                    }else if (response.data.status === 'Expired') {
                        $scope.adminlogout();
                        $("#failuremessageModal").modal('show');
                        $("#failuremessagetitle").text('Message');
                        $("#failuremessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $scope.wp_currency_symbol = "";
                        $scope.total_event_sales = "";
                        $scope.total_membership_sales = "";
                        $("#editaccount-modal").modal('show');
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    $scope.handleAdminError(response.data);
                });
            };
            
            $scope.setEditModalWidth = function(){
                if($(window).height() > 1280) {
                        $("#editaccount-modal > .modal-dialog").attr("style", "width:70%;margin: 10px auto;");
                } else {
                    if ($('#editaccount-modal > .col-sm-7 .col-sm-offset-3')) {
                         $("#editaccount-modal > .modal-dialog").attr("style", "width:80%;margin: 10px auto;");
                    }else{
                         $("#editaccount-modal > .modal-dialog").attr("style", "width:50%;margin: 10px auto;");
                    }
                }
            };
            
            //editview
            $scope.showeditviewgrid = function(){
                $("#eidtdelbutton").addClass("ng-hide");
                $("#left").removeClass("col-sm-7 col-sm-offset-3");
                $("#left").addClass("col-sm-6");
                $("#editaccount-modal > .modal-dialog").attr("style","width:70%;margin: 10px auto;");
                $scope.setEditModalWidth();
                $scope.showeditview = true;
            };
            
            //canceleditview
            $scope.canceleditview = function(){
                $scope.edit_website = $scope.website;
                $scope.studio_phone = $scope.studiophone; 
                $scope.edit_industry = $scope.industry;
                $scope.user_phone = $scope.userphone;
                $scope.user_lastname = $scope.userlastname;
                $scope.user_firstname = $scope.userfirstname;
                $scope.studio_name = $scope.companyname;
                $scope.owner_name = $scope.ownername;
                $scope.lead_source = $scope.leadsource;
                $scope.edit_company_code = $scope.company_code;
                $scope.edit_street_address = $scope.street_address;
                $scope.edit_city = $scope.city;
                $scope.edit_state = $scope.state;
                $scope.edit_postal_code = $scope.postal_code;
                $scope.edit_country = $scope.country;
                $scope.edit_email_for_log_in = $scope.email_for_log_in;
                
                $scope.otherindustrytype =  $scope.industrylist.indexOf($scope.industry);
                if( $scope.otherindustrytype !== -1 || !$scope.industry){
                    $scope.edit_industry = $scope.industry;
                }else if($scope.otherindustrytype === -1){
                    $scope.edit_industry = 'Other';
                    $scope.edit_other_industry = $scope.industry; 
                }
                
                $scope.othercountrytype =  $scope.countrycodes.indexOf($scope.country);
                if($scope.othercountrytype !== -1 || !$scope.country){
                    if($scope.country === 'US'){
                        $scope.edit_country = "United States";
                    }else if($scope.country === 'UK'){
                        $scope.edit_country = "United Kingdom";
                    }else if($scope.country === 'CA'){
                        $scope.edit_country = "Canada";
                    }else if($scope.country === 'AU'){
                        $scope.edit_country = "Australia";
                    }else{
                       $scope.edit_country = "United States"; 
                    }
                }else if($scope.othercountrytype === -1){
                    $scope.edit_country = 'Other';
                    $scope.edit_other_country = $scope.country; 
                }
                
                $scope.disableifnotavailable = false;
                $("#eidtdelbutton").removeClass("ng-hide");
                $("#left").removeClass("col-sm-6");
                $("#left").addClass("col-sm-7 col-sm-offset-3");
                $("#editaccount-modal > .modal-dialog").attr("style","width:50%");
                $scope.setEditModalWidth();
                $scope.showeditview = false;    
            };
            
            //clear edit modal
            $scope.clearmodal = function(){
                $scope.company_id = "";
                $scope.user_id = "";
                $scope.website = "";
                $scope.studiophone = "";
                $scope.industry = "";
                $scope.userphone = "";
                $scope.userlastname  = "";
                $scope.userfirstname = "";
                $scope.companyname= "";
                $scope.paymentplan = "";
                $scope.ownername = "";
                $scope.leadsource = "";
                $scope.company_code = "";
                $scope.email_for_log_in = "";
                $scope.street_address = "";
                $scope.city = "";
                $scope.state = "";
                $scope.postal_code = "";
                $scope.country = "";
                $scope.edit_other_industry = "";
                $scope.edit_other_country = "";                
            };
            
            $scope.showdeleteview = function(){
                $("#editaccount-modal").modal('hide');  
                $("#deletet-modal").modal('show'); 
            };
            
            $scope.isJson = function (str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            };
            //check available email
            $scope.checkemailavailablity = function(){
                if($scope.edit_company_code !== ""){
                $http({
                method: 'POST',
                url: urlservice.url+'verifyUserEmailForAdminPanel',
                data:{
                    'company_id':$scope.company_id,
                    'user_email':$scope.edit_email_for_log_in,
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                        $scope.disableifnotavailable = false;
                    }else if(response.data.status === 'Failed'){
                        $scope.disableifnotavailable = true;
                    }else if(response.data.status === 'Expired'){
                        $scope.adminlogout();
                        $("#failuremessageModal").modal('show');
                        $("#failuremessagetitle").text('Message');
                        $("#failuremessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $('#progress-full').hide();    
                        $("#adminfailuremessageModal").modal('show');
                        $("#adminfailuremessagetitle").text('Message');
                        $("#adminfailuremessagecontent").text(response.data.msg);
                    }   
                }, function (response) {
                    $scope.handleAdminError(response.data);
                });
            }
            };
            //check studio code available
            $scope.checkcodeavailablity = function(){
                if($scope.edit_email_for_log_in !== ""){
                $http({
                method: 'POST',
                url: urlservice.url+'verifyCompanyCodeForAdminPanel',
                data:{
                    'company_id':$scope.company_id,
                    'company_code':$scope.edit_company_code,
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                        $scope.disableifnotavailable = false;
                    }else if(response.data.status === 'Failed'){
                        $scope.disableifnotavailable = true;
                    }else if(response.data.status === 'Expired'){
                        $scope.adminlogout();
                        $("#failuremessageModal").modal('show');
                        $("#failuremessagetitle").text('Message');
                        $("#failuremessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $('#progress-full').hide();    
                        $("#adminfailuremessageModal").modal('show');
                        $("#adminfailuremessagetitle").text('Message');
                        $("#adminfailuremessagecontent").text(response.data.msg);
                    }   
                }, function (response) {
                    $scope.handleAdminError(response.data);
                });
            }
            };
            //update company detail
            $scope.updateaccountdetail = function(){
                $('#progress-full').show();   
                var country = 'US';
                if($scope.edit_country === "United States"){
                    country = 'US';
                }else if($scope.edit_country === "United Kingdom"){
                    country = 'UK';
                }else if($scope.edit_country === "Australia"){
                   country = 'AU';
                }else if($scope.edit_country === "Canada"){
                   country = 'CA';
                }
                
                $http({
                method: 'POST',
                url: urlservice.url+'updatestudiodetail',
                data:{
                    'company_id':$scope.company_id,
                    'user_id':$scope.user_id,
                    'studio_name':$scope.studio_name,
                    'lead_source':$scope.lead_source,
                    'owner_name':$scope.owner_name,
                    'user_firstname':$scope.user_firstname,
                    'user_lastname':$scope.user_lastname,
                    'user_phone':$scope.user_phone,
                    'industry': ($scope.edit_industry === 'Other') ? $scope.edit_other_industry : $scope.edit_industry,
                    'website':$scope.edit_website,
                    'studio_phone':$scope.studio_phone,
                    'company_code':$scope.edit_company_code,
                    'login_email':$scope.edit_email_for_log_in,
                    'street_address':$scope.edit_street_address,
                    'city':$scope.edit_city,
                    'state':$scope.edit_state,
                    'postal_code':$scope.edit_postal_code,
                    'country': ($scope.edit_country === 'Other') ? $scope.edit_other_country : country
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                        $scope.canceleditview();
                        $("#editaccount-modal").modal('hide');
                        $('#progress-full').hide();
                        $("#adminmessageModal").modal('show');
                        $("#adminmessagetitle").text('Message');
                        $("#adminmessagecontent").text(response.data.msg);
                        $scope.admindtInstance.rerender();
                    }else if(response.data.status === 'Expired'){
                        $scope.adminlogout();
                        $("#failuremessageModal").modal('show');
                        $("#failuremessagetitle").text('Message');
                        $("#failuremessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $('#progress-full').hide();    
                        $("#adminfailuremessageModal").modal('show');
                        $("#adminfailuremessagetitle").text('Message');
                        $("#adminfailuremessagecontent").text(response.data.msg);
                    }   
                }, function (response) {
                    $scope.handleAdminError(response.data);
                });
            };
            //csv export
            $scope.exportstudiocsvforadmin = function(){
                function formatTodayDate() {
                    var d = new Date,
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }
                $scope.today_date = formatTodayDate();
              $http({
                    method: 'POST',
                    url: urlservice.url + 'createCsvForadmin',
                    responseType: 'arraybuffer',
                    data: {
                        "search":$scope.searchTerm
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).success(function (data, status, headers) {
                    var response = '';
                    if ('TextDecoder' in window) {
                        // DECODE AS UTF-8
                        var dataView = new DataView(data);
                        var decoder = new TextDecoder('utf8');
                        if ($scope.isJson(decoder.decode(dataView)) === true) {
                            response = JSON.parse(decoder.decode(dataView));
                        }
                    } else {
                        // FALLBACK DECODE AS ASCII
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                        if ($scope.isJson(decodedString) === true) {
                            response = JSON.parse(decodedString);
                        }
                    }
                    if (response.status !== undefined && response.status === 'Failed') {
                        $('#progress-full').hide();
                        $("#adminfailuremessageModal").modal('show');
                        $("#adminfailuremessagetitle").text('Message');
                        $("#adminfailuremessagecontent").text(response.msg);                        
                        return false;
                    } else {
                        $('#progress-full').hide();
                        $("#adminmessageModal").modal('show');
                        $("#adminmessagetitle").text('Message');
                        $("#adminmessagecontent").text("Studio Details exported as csv file successfully.");
                    }
                    var msg = '';
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], {type: "application/x-download"});
                        var objectUrl = URL.createObjectURL(blob);
                        linkElement.setAttribute('href', objectUrl);
                        linkElement.setAttribute("download", "studio_" + $scope.today_date + ".csv");
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        msg = ex;
                        console.log(ex);
                    }
                    if (msg === '') {
                        msg = "CSV file downloaded Successfully";
                    }
                    $('#progress-full').hide();
                    $("#adminmessageModal").modal('show');
                    $("#adminmessagetitle").text('Message');
                    $("#adminmessagecontent").text(msg);

                }).error(function () {
                    $('#progress-full').hide();
                    $("#adminfailurmessageModal").modal('show');
                    $("#adminfailurmessagetitle").text('Message');
                    $("#adminfailurmessagecontent").text("Unable to reach Server, Please Try again.");
                });  
            };
            
            //downgrade modal
            $scope.downgradeplan = function(comp_id,row,user_id){
                $scope.downgradeplanvalue='';
                $("#downgradeplan-modal").modal('show');
                $scope.companyid_for_planchange = comp_id;
                $scope.userid_for_planchange = user_id;
            }
            
            //upgradepaymentplan
            $scope.updatepaymentplan = function(){
                $http({
                method: 'POST',
                url: urlservice.url+'downgradeStudioSubscription',
                data:{
                    'company_id':$scope.companyid_for_planchange,
                    'user_id':$scope.userid_for_planchange,
                    'payment_plan':$scope.downgradeplanvalue
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                        $("#downgradeplan-modal").modal('hide');
                        $('#progress-full').hide();
                        $("#adminmessageModal").modal('show');
                        $("#adminmessagetitle").text('Message');
                        $("#adminmessagecontent").text(response.data.msg);
                        $scope.admindtInstance.rerender();
                    }else if(response.data.status === 'Expired'){
                        $scope.adminlogout();
                        $("#failuremessageModal").modal('show');
                        $("#failuremessagetitle").text('Message');
                        $("#failuremessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $('#progress-full').hide();    
                        $("#adminfailuremessageModal").modal('show');
                        $("#adminfailuremessagetitle").text('Message');
                        $("#adminfailuremessagecontent").text(response.data.msg);
                    }   
                }, function (response) {
                    $scope.handleAdminError(response.data);
                });
            };
        
    }else {
            $location.path('/adminpanel');
    }
    
     //DOCUMENT READY FUNCTION
        angular.element(document).ready(function () {
            var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
            if(isChrome){
                $('#lead_source').css('width','173px');
                $('#edit_industry').css('width','173px');
                $('#edit_country').css('width','173px');
            }else{
                $('#lead_source').css('width','188px');
                $('#edit_industry').css('width','188px');
                $('#edit_country').css('width','188px');
            }
        });
        
}
module.exports = AdminStudioMembersController;