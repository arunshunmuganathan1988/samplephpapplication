ResetPasswordController.$inject = ['$scope', '$location', '$http', '$localStorage', 'urlservice', '$route', '$rootScope'];
 function ResetPasswordController ($scope, $location, $http, $localStorage, urlservice, $route, $rootScope)
    {
        $rootScope.hidesidebarlogin = true;
        $rootScope.sideBarHide = true;
        $rootScope.adminsideBarHide = true;
        tooltip.hide();
        $scope.failure_response_text = '';
        $scope.tokenvalueurl = $location.absUrl().split('=')[1];
//        $scope.tokenvalueurl = decodeURI($scope.urltoken);
        if ($scope.tokenvalueurl != undefined || $scope.tokenvalueurl != null) {
            $http({
                method: 'POST',
                url: urlservice.url + 'tokenvalidation',
                data: {
                    "token": $scope.tokenvalueurl
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $localStorage.tokenvalue = $scope.tokenvalueurl;
                    $localStorage.localregemail = response.data.msg.user_email;
                    $location.url($location.path());
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                }else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    console.log(response.data);
                    $location.url($location.path());
                    $('#progress-full').hide();
                    $("#failuremessageModal").modal('show');
                    $("#failuremessagetitle").text('Message');
                    $("#failuremessagecontent").text(response.data.msg);
                    $location.path('/forgotpassword');
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        }

        $scope.regemail = $localStorage.localregemail;


        $scope.redirectlogin = function () {
            $localStorage.tokenvalue = $localStorage.localregemail = "";
            $location.url($location.path());
            $location.path('/login');
        };


        $scope.resetPswdSubmit = function () {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'resetPassword',
                data: {
                    "password": $scope.newpass,
                    "retypepassword": $scope.newcpass,
                    "token": $localStorage.tokenvalue
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $localStorage.company_id = response.data.msg.company_id;
                    $location.url($location.path());
                    $localStorage.tokenvalue = $localStorage.localregemail = "";                                          
                    $scope.failure_response_text = response.data.msg;
                    $('#response-modal').modal('show');
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $scope.failure_response_text = response.data.msg;
                    $('#response-modal').modal('show');
                    $scope.logout();
                }else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $('#progress-full').hide();
                    $location.path('/forgotpassword');                     
                    $scope.failure_response_text = response.data.msg;
                    $('#response-modal').modal('show');
                }
            }, function (response) {
                $location.url($location.path());
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

    }
    module.exports = ResetPasswordController;

