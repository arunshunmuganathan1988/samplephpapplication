
dashBoardController.$inject = ['$scope', '$location', '$http', '$localStorage', 'urlservice', '$route', '$rootScope', '$filter'];
 function dashBoardController($scope, $location, $http, $localStorage, urlservice, $route, $rootScope, $filter)
    {
if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().regComplete();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
//            angular.element(document.getElementById('sidetopbar')).scope().getWepayStatus();
            angular.element(document.getElementById('sidetopbar')).scope().getStripeStatus();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $localStorage.currentpage = "dashboard";
            $scope.wp_currency_code = $localStorage.wp_currency_code;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.netsales_flag=0;
            $scope.active_flag=0;
            $scope.rentation_flag=0;
            $scope.mem_disable='E';
            $scope.sales_period = 'M';
            $rootScope.sideBarHide = false;
            $rootScope.hidesidebarlogin = false;
            $rootScope.adminsideBarHide = false;
            $rootScope.activepagehighlight = 'home';
            
            $scope.dashboard_flag=function($flag){
                if($flag==='N'){
                    $scope.netsales_flag=$scope.netsales_flag-1;
                     $scope.dashboardTable($scope.netsales_flag);
                }else if($flag==='P'){
                    $scope.netsales_flag=$scope.netsales_flag+1;
                    $scope.dashboardTable($scope.netsales_flag);
                }
                else if($flag==='F'){
                     $scope.active_flag=$scope.active_flag-1;
                     $scope.dashboardTable($scope.active_flag);
                }else if($flag==='O'){
                    $scope.active_flag=$scope.active_flag+1;
                    $scope.dashboardTable($scope.active_flag);
                }else if($flag==='G'){
                     $scope.rentation_flag=$scope.rentation_flag-1;
                     $scope.dashboardTable($scope.rentation_flag);
                }else if($flag==='B'){
                    $scope.rentation_flag=$scope.rentation_flag+1;
                    $scope.dashboardTable($scope.rentation_flag);
            }
        };
        
        $scope.sales_period_selection = function(sales_period){
            if(sales_period === 'M'){
                $scope.netsales_flag=0;
                $scope.sales_period = 'M';
                $scope.dashboardTable();
                
            }else if(sales_period === 'A'){
                $scope.netsales_flag=0;
                $scope.sales_period = 'A';
                $scope.dashboardTable();
            }            
        };
        
         
            
            $scope.dashboardTable = function () {
                var enable_next_button=0;
                // Loads data and ToTal Amount In All the Tabs of Dashboard Page//
                $('#progress-full').show();
//                $scope.months = $scope.members = $scope.sales= $scope.hold = $scope.retention='';
//                if(parseInt($scope.d_flag)==0){
//                    $scope.dashboard_flag($scope.n_flag);
//                }
                $http({
                    method: 'POST',
                    url: urlservice.url + 'dashboard',
                    data: {
                        "company_id": $localStorage.company_id,
                        "n_flag":$scope.netsales_flag,
                        "mem":'A',
                        "net":'N',
                        "a_flag":$scope.active_flag,
                        "r_flag":$scope.rentation_flag,
                        "sales_period": $scope.sales_period

                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8'},
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') { 
                        $scope.sales = response.data.msg.sales;
                        
                        $scope.members = response.data.msg.members;
                         for( var j=0; j <  $scope.members.length;j++){
                            if($scope.members[j].first_month == 0 && $scope.members[j].second_month == 0 && $scope.members[j].third_month == 0 && $scope.members[j].fourth_month == 0) {
                            }else{
                                enable_next_button = 1;
                            }
                                
                        }
                        if(enable_next_button==1 || enable_next_button===1){
                            $scope.mem_disable = 'E';
                        }else{
                            $scope.mem_disable = 'D';
                        }
                        $scope.month_netsales=response.data.msg.month_netsales;
                        $scope.month_ret=response.data.msg.month_ret;
                        $scope.months = response.data.msg.months;
                        $scope.hold=response.data.msg.hold;
                        $scope.failed=response.data.msg.failed;
                        $scope.upcoming=response.data.msg.upcoming;           
                        $scope.retention=response.data.msg.retention;
                        
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            }; 
            
              $scope.paymentpage= function(flag){
                  if(flag==='P'){
                     $localStorage.paymentdshboard = 'Y';
                    $location.path('/payment');
                  }else if(flag==='U'){
                       $localStorage.paymentdshboard = 'U';
                    $location.path('/payment');
                  }
                
              }
              $scope.customerpage= function(){
                 $localStorage.customerdshboard='Y';
                 $location.path('/customers');
                  
              }
            
    
    } else {
            $location.path('/login');
        }
    }
    module.exports = dashBoardController;

