AdminHomeController.$inject=['$scope',  '$compile','$location', '$http', '$localStorage', 'urlservice',  '$filter', '$route', 'DTOptionsBuilder', 'DTColumnDefBuilder','DTColumnBuilder','DTDefaultOptions','$window','$rootScope'];
function AdminHomeController($scope, $compile, $location, $http, $localStorage, urlservice, $filter, $route,DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder,DTDefaultOptions,$window,$rootScope) {

if($localStorage.isadminlogin === 'Y') {
        $rootScope.sideBarHide = true;
        $rootScope.adminsideBarHide = true;
        $rootScope.hidesidebarlogin = false;
        $rootScope.activepagehighlight = 'adminhome';
        $rootScope.admincmpnynme = $localStorage.admincmpnynme;
        tooltip.hide();
         $scope.getdashboarddetails = function(){
              $('#progress-full').show();
                $http({
                method: 'GET',
                url: urlservice.url+'getadmindashboarddetails',
                params:{
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                        $('#progress-full').hide();
                        $scope.total_members = response.data.msg.total_members;
                        $scope.Inactive = response.data.msg.Inactive;
                        $scope.basic = response.data.msg.basic;
                        $scope.premium_yearly = response.data.msg.premium_yearly;
                        $scope.premium_monthly = response.data.msg.premium_monthly;
                        $scope.white_lable_monthly = response.data.msg.white_lable_monthly;
                        $scope.white_lable_yearly = response.data.msg.white_lable_yearly;
                        $scope.failed_payments = response.data.msg.failed_payments;
                        $scope.Cancelled_account = response.data.msg.Cancelled_account;
                    }else if(response.data.status === 'Expired'){
                        $scope.adminlogout();
                        $("#failuremessageModal").modal('show');
                        $("#failuremessagetitle").text('Message');
                        $("#failuremessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $('#progress-full').hide();    
                        $("#failuremessageModal").modal('show');
                        $("#failuremessagetitle").text('Message');
                        $("#failuremessagecontent").text(response.data.msg);
                    }   
                }, function (response) {
                    $scope.handleAdminError(response.data);
                });
            };
            $scope.getdashboarddetails();
    }else {
            $location.path('/adminpanel');
    }
        
}
module.exports = AdminHomeController;