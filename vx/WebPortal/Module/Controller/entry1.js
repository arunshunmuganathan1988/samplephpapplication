
var app = angular.module('addretail', ['wysiwyg.module', 'angular-img-cropper', 'ngclipboard']).controller('AddRetailController', require('./AddRetailController')).
directive("preventTypingGreater", function () {
       return {
           link: function (scope, element, attributes) {
               var oldVal = null;
               element.on("keydown keyup", function (e) {
                   if (Number(element.val()) > 20 &&
                           e.keyCode != 46 // delete
                           &&
                           e.keyCode != 8 // backspace
                           ) {
                       e.preventDefault();
                       element.val(oldVal);
                   } else {
                       oldVal = Number(element.val());
                   }
               });
           }
       };
   });

var app = angular.module('manageretail', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']).controller('ManageRetailController', require('./ManageRetailController'))
.service('retailListDetails', function () {   
   var editSingleRetail = [];
   var addSingleRetail = function (newObj) {
       editSingleRetail = [];
       editSingleRetail.push(newObj);
   };

   var getSingleRetail = function () {
       return editSingleRetail;
   };

   return {
       addSingleRetail: addSingleRetail,
       getSingleRetail: getSingleRetail
   };

});

var app = angular.module('retaildiscount', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']).controller('RetailDiscountController', require('./RetailDiscountController'));

var app = angular.module('retailfulfillment', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']).controller('RetailFulfillmentController', require('./RetailFulfillmentController'));

var app= angular.module('retailtemplate').controller('RetailTemplateController', require('./RetailTemplateController'));

var app = angular.module('whitelabelsetup', ['wysiwyg.module', 'angular-img-cropper', 'ngclipboard','color.picker']).controller('WhiteLabelSetupController', require('./WhiteLabelSetupController'))
        .directive("attachstudentfileinput", ['$sce', function ($sce) {
            return {
                require: 'ngModel',
                $scope: {
                    attachfileinput: "=",
                    filepreview: "="
                },
                link: function ($scope, element, attributes, ngModel) {
                    element.bind("change", function (changeEvent) {
                        $scope.maxClassScheduleSizeError = false;
                        var schedulefileSize = this.files[0].size / 1024 / 1024;
                        if (schedulefileSize >= 10) {
                            $scope.maxEmailAttachmentSizeError = true;
                            changeEvent.target.files[0] = "";
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                            $scope.filepreview = "";
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                            });
                         }
                        } else {
                            $scope.maxEmailAttachmentSizeError = false;
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                        reader.readAsDataURL( $scope.attachfileinput);
    
                    });
                }
            }
        }]).directive("attachstudentfileinput1", ['$sce', function ($sce) {
            return {
                require: 'ngModel',
                $scope: {
                    attachfileinput: "=",
                    filepreview1: "="
                },
                link: function ($scope, element, attributes, ngModel) {
                    element.bind("change", function (changeEvent) {
                        $scope.maxClassScheduleSizeError = false;
                        var schedulefileSize = this.files[0].size / 1024 / 1024;
                        if (schedulefileSize >= 10) {
                            $scope.maxEmailAttachmentSizeError = true;
                            changeEvent.target.files[0] = "";
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                            $scope.filepreview1 = "";
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                            });
                         }
                        } else {
                            $scope.maxEmailAttachmentSizeError = false;
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview1 = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                        reader.readAsDataURL( $scope.attachfileinput);
    
                    });
                }
            }
        }]).directive("attachstudentfileinput2", ['$sce', function ($sce) {
            return {
                require: 'ngModel',
                $scope: {
                    attachfileinput: "=",
                    filepreview2: "="
                },
                link: function ($scope, element, attributes, ngModel) {
                    element.bind("change", function (changeEvent) {
                        $scope.maxClassScheduleSizeError = false;
                        var schedulefileSize = this.files[0].size / 1024 / 1024;
                        if (schedulefileSize >= 10) {
                            $scope.maxEmailAttachmentSizeError = true;
                            changeEvent.target.files[0] = "";
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                            $scope.filepreview2 = "";
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                            });
                         }
                        } else {
                            $scope.maxEmailAttachmentSizeError = false;
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview2 = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                        reader.readAsDataURL( $scope.attachfileinput);
    
                    });
                }
            }
        }]).directive("attachstudentfileinput3", ['$sce', function ($sce) {
            return {
                require: 'ngModel',
                $scope: {
                    attachfileinput: "=",
                    filepreview3: "="
                },
                link: function ($scope, element, attributes, ngModel) {
                    element.bind("change", function (changeEvent) {
                        $scope.maxClassScheduleSizeError = false;
                        var schedulefileSize = this.files[0].size / 1024 / 1024;
                        if (schedulefileSize >= 10) {
                            $scope.maxEmailAttachmentSizeError = true;
                            changeEvent.target.files[0] = "";
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                            $scope.filepreview3 = "";
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                            });
                         }
                        } else {
                            $scope.maxEmailAttachmentSizeError = false;
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview3 = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                        reader.readAsDataURL( $scope.attachfileinput);
    
                    });
                }
            }
        }]).directive("attachstudentfileinput4", ['$sce', function ($sce) {
            return {
                require: 'ngModel',
                $scope: {
                    attachfileinput: "=",
                    filepreview4: "="
                },
                link: function ($scope, element, attributes, ngModel) {
                    element.bind("change", function (changeEvent) {
                        $scope.maxClassScheduleSizeError = false;
                        var schedulefileSize = this.files[0].size / 1024 / 1024;
                        if (schedulefileSize >= 10) {
                            $scope.maxEmailAttachmentSizeError = true;
                            changeEvent.target.files[0] = "";
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                            $scope.filepreview4 = "";
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                            });
                         }
                        } else {
                            $scope.maxEmailAttachmentSizeError = false;
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview4 = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                        reader.readAsDataURL( $scope.attachfileinput);
    
                    });
                }
            }
        }]).directive("attachstudentfileinput5", ['$sce', function ($sce) {
            return {
                require: 'ngModel',
                $scope: {
                    attachfileinput: "=",
                    filepreview5: "="
                },
                link: function ($scope, element, attributes, ngModel) {
                    element.bind("change", function (changeEvent) {
                        $scope.maxClassScheduleSizeError = false;
                        var schedulefileSize = this.files[0].size / 1024 / 1024;
                        if (schedulefileSize >= 10) {
                            $scope.maxEmailAttachmentSizeError = true;
                            changeEvent.target.files[0] = "";
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                            $scope.filepreview5 = "";
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                            });
                         }
                        } else {
                            $scope.maxEmailAttachmentSizeError = false;
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview5 = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                        reader.readAsDataURL( $scope.attachfileinput);
    
                    });
                }
            }
        }]).directive("attachstudentfileinput6", ['$sce', function ($sce) {
            return {
                require: 'ngModel',
                $scope: {
                    attachfileinput: "=",
                    filepreview6: "="
                },
                link: function ($scope, element, attributes, ngModel) {
                    element.bind("change", function (changeEvent) {
                        $scope.maxClassScheduleSizeError = false;
                        var schedulefileSize = this.files[0].size / 1024 / 1024;
                        if (schedulefileSize >= 10) {
                            $scope.maxEmailAttachmentSizeError = true;
                            changeEvent.target.files[0] = "";
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                            $scope.filepreview6 = "";
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                            });
                         }
                        } else {
                            $scope.maxEmailAttachmentSizeError = false;
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview6 = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                        reader.readAsDataURL( $scope.attachfileinput);
    
                    });
                }
            }
        }]).directive("attachstudentfileinput7", ['$sce', function ($sce) {
            return {
                require: 'ngModel',
                $scope: {
                    attachfileinput: "=",
                    filepreview7: "="
                },
                link: function ($scope, element, attributes, ngModel) {
                    element.bind("change", function (changeEvent) {
                        $scope.maxClassScheduleSizeError = false;
                        var schedulefileSize = this.files[0].size / 1024 / 1024;
                        if (schedulefileSize >= 10) {
                            $scope.maxEmailAttachmentSizeError = true;
                            changeEvent.target.files[0] = "";
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                            $scope.filepreview7 = "";
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                            });
                         }
                        } else {
                            $scope.maxEmailAttachmentSizeError = false;
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview7 = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                        reader.readAsDataURL( $scope.attachfileinput);
    
                    });
                }
            }
        }]).directive("attachstudentfileinput8", ['$sce', function ($sce) {
            return {
                require: 'ngModel',
                $scope: {
                    attachfileinput: "=",
                    filepreview8: "="
                },
                link: function ($scope, element, attributes, ngModel) {
                    element.bind("change", function (changeEvent) {
                        $scope.maxClassScheduleSizeError = false;
                        var schedulefileSize = this.files[0].size / 1024 / 1024;
                        if (schedulefileSize >= 10) {
                            $scope.maxEmailAttachmentSizeError = true;
                            changeEvent.target.files[0] = "";
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                            $scope.filepreview8 = "";
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                            });
                         }
                        } else {
                            $scope.maxEmailAttachmentSizeError = false;
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview8 = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                        reader.readAsDataURL( $scope.attachfileinput);
    
                    });
                }
            }
        }]).directive("attachstudentfileinput9", ['$sce', function ($sce) {
            return {
                require: 'ngModel',
                $scope: {
                    attachfileinput: "=",
                    filepreview9: "="
                },
                link: function ($scope, element, attributes, ngModel) {
                    element.bind("change", function (changeEvent) {
                        $scope.maxClassScheduleSizeError = false;
                        var schedulefileSize = this.files[0].size / 1024 / 1024;
                        if (schedulefileSize >= 10) {
                            $scope.maxEmailAttachmentSizeError = true;
                            changeEvent.target.files[0] = "";
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                            $scope.filepreview9 = "";
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                            });
                         }
                        } else {
                            $scope.maxEmailAttachmentSizeError = false;
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview9 = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                        reader.readAsDataURL( $scope.attachfileinput);
    
                    });
                }
            }
        }]);  

var app = angular.module('posdashboard').controller('PosDashboardController',require('./PosDashboardController'));

var app = angular.module('miscdetail').controller('MiscDetailController',require('./MiscDetailController'));

var app = angular.module('adminwhitelabel',['datatables', 'datatables.scroller','datatables.select','ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']).controller('WhiteLabelDashboardController',require('./WhiteLabelDashboardController'));