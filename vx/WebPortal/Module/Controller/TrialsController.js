TrialsController.$inject=['$scope',  '$compile','$location', '$http', '$localStorage', 'urlservice',  '$filter', '$route', 'DTOptionsBuilder', 'DTColumnDefBuilder','DTColumnBuilder','DTDefaultOptions','attendanceFilterDetails','$window','$rootScope'];
 function TrialsController($scope, $compile, $location, $http, $localStorage, urlservice, $filter, $route,DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder,DTDefaultOptions,attendanceFilterDetails,$window,$rootScope) {
    
        
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            tooltip.hide();
            $rootScope.activepagehighlight = 'trials';
            $scope.triallistcontents = [];
            $scope.selectedData = [];
            $scope.selected = {};
            $scope.selectAll = false;
            $scope.all_select_flag='N';
            $scope.color_green='N';
            $scope.TrialIndividualDetail = function (trial_reg_id, page, trial_id, tab) {
                $localStorage.currentTrialregid = trial_reg_id;
                $localStorage.currentTrial_id = trial_id;
                $localStorage.trialpagefrom = page+tab;
                $location.path('/trialdetail');
            };
             
           $scope.getTrialMember = function(tab){
                 $scope.current_tab=tab;
                    DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                    var titleHtml = '<input type="checkbox" ng-model="selectAll" ng-click="trialtoggleAll(selectAll, selected)">';
                var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                $scope.selected = {};
                $scope.selectAll = false;
                if ($scope.current_tab === 'A') {
                   $scope.trialdtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'trialcustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.triallistcontents = json.data;
                                $scope.liveviewcount = json.live_count;
                                $scope.draftviewcount = json.draft_count;
                                $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "trial_status_tab": tab,
                                 "trial_days_type": $scope.activetrialdays
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.trialdtColumns = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_ta1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'trials'" + ',' + full.trial_id + ',' + "'active'" + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_ta2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_ta8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_ta3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_ta4').renderWith(
                                function (data, type, full, meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                                  }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_ta5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_ta8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_ta9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_ta10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                                DTColumnBuilder.newColumn('l_att').withTitle('Last Attendance').withClass('col_ta11').renderWith(
                                function (data, type, full) {
                                    if (data > 0)
                                        return (data) + ' days ago';                                    
                                    else if (data == '0')
                                        return 'Today';
                                    else 
                                        return '';
                                }),
                                DTColumnBuilder.newColumn('act_att').withTitle('Attendance count').withClass('col_ta12').notSortable().renderWith(
                                function (data, type, full) {
                                    if (full.act_att > 0)
                                        return full.act_att+' total';
                                    else
                                        return full.act_att;
                                }),
                                DTColumnBuilder.newColumn('14_att').withTitle('Attendance Last 14 Days').withClass('col_ta13'),
                                DTColumnBuilder.newColumn('30_att').withTitle('Attendance Last 30 Days').withClass('col_ta14'),
                    ];
                   $scope.dtInstance = {};
                }if ($scope.current_tab === 'E') {
                   $scope.trialdtOptions1 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'trialcustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.triallistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "trial_status_tab": tab,
                                 "trial_days_type": $scope.holdtrialdays
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.trialdtColumns1 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_te1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'trials'" + ',' + full.trial_id + ',' + "'enrolled'" + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_te2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_te8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_te3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_te4').renderWith(
                                function (data, type, full,meta) {
                                    if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                                }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_te5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_te6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_te7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_te8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_te9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_te10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                                DTColumnBuilder.newColumn('l_att').withTitle('Last Attendance').withClass('col_te11').renderWith(
                                function (data, type, full) {
                                    if (data > 0)
                                        return (data) + ' days ago';                                    
                                    else if (data == '0')
                                        return 'Today';
                                    else 
                                        return '';
                                }),
                                DTColumnBuilder.newColumn('act_att').withTitle('Attendance count').withClass('col_te12').notSortable().renderWith(
                                function (data, type, full) {
                                    if (full.act_att > 0)
                                        return full.act_att+' total';
                                    else
                                        return full.act_att;
                                }),
                                DTColumnBuilder.newColumn('14_att').withTitle('Attendance Last 14 Days').withClass('col_te13'),
                                DTColumnBuilder.newColumn('30_att').withTitle('Attendance Last 30 Days').withClass('col_te14'),
                    ];
                   $scope.dtInstance1 = {};
                }if ($scope.current_tab === 'C') {
                   $scope.trialdtOptions2 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'trialcustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.triallistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "trial_status_tab": tab,
                                 "trial_days_type": $scope.canceltrialdays
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.trialdtColumns2 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_tc1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'trials'" + ',' + full.trial_id + ',' + "'cancelled'" + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_tc2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_tc8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_tc3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_tc4').renderWith(
                                function (data, type, full,meta) {
                                    if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                                }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_tc5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_tc6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_tc7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_tc8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_tc9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_tc10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                                DTColumnBuilder.newColumn('l_att').withTitle('Last Attendance').withClass('col_tc11').renderWith(
                                function (data, type, full) {
                                    if (data > 0)
                                        return (data) + ' days ago';                                    
                                    else if (data == '0')
                                        return 'Today';
                                    else 
                                        return '';
                                }),
                                DTColumnBuilder.newColumn('act_att').withTitle('Attendance count').withClass('col_tc12').notSortable().renderWith(
                                function (data, type, full) {
                                    if (full.act_att > 0)
                                        return full.act_att+' total';
                                    else
                                        return full.act_att;
                                }),
                                DTColumnBuilder.newColumn('14_att').withTitle('Attendance Last 14 Days').withClass('col_tc13'),
                                DTColumnBuilder.newColumn('30_att').withTitle('Attendance Last 30 Days').withClass('col_tc14'),
                    ];
                   $scope.dtInstance2 = {};
                }if ($scope.current_tab === 'D') {
                   $scope.trialdtOptions3 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'trialcustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.triallistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "trial_status_tab": tab,
                                 "trial_days_type": $scope.didntstarttrialdays
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.trialdtColumns3 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_td1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'trials'" + ',' + full.trial_id + ',' + "'didntstart'" + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_td2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_td8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_td3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_td4').renderWith(
                                function (data, type, full,meta) {
                                    if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                                }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_td5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_td6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_td7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_td8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_td9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_td10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                                DTColumnBuilder.newColumn('l_att').withTitle('Last Attendance').withClass('col_td11').renderWith(
                                function (data, type, full) {
                                    if (data > 0)
                                        return (data) + ' days ago';                                    
                                    else if (data == '0')
                                        return 'Today';
                                    else 
                                        return '';
                                }),
                                DTColumnBuilder.newColumn('act_att').withTitle('Attendance count').withClass('col_td12').notSortable().renderWith(
                                function (data, type, full) {
                                    if (full.act_att > 0)
                                        return full.act_att+' total';
                                    else
                                        return full.act_att;
                                }),
                                DTColumnBuilder.newColumn('14_att').withTitle('Attendance Last 14 Days').withClass('col_td13'),
                                DTColumnBuilder.newColumn('30_att').withTitle('Attendance Last 30 Days').withClass('col_td14'),
                    ];
                   $scope.dtInstance3 = {};
                }
                $('#progress-full').hide();
                angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                    console.log('dataTables search : ' + $scope.searchTerm);
                })
            };
            
            $scope.showemailerror = function (row, type, data, event, id) {
//                angular.element(event.target).parent().html('<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + id + '\' ,' + "'mail'" + ');">' + data + '</a>');
                $scope.email_updt = event.target;
                $scope.row=row;
                $scope.mem_id = id;
                $scope.btype=type;
                $scope.errormsg = $scope.triallistcontents[row].error;
                $scope.bouncedemail = data;
                $("#emailerrormessageModal").modal('show');
                $("#emailerrormessagetitle").text('Message');
                $("#emailerrormessagecontent").text($scope.errormsg);
            };
            
            $scope.unblockbounceemail = function () {
                $('#progress-full').show();
                 $("#emailerrormessageModal").modal('hide');
                $http({
                    method: 'POST',
                   url: urlservice.url + 'unblockbounceemail',
                   data: {
                        "bounced_email": $scope.bouncedemail,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
//                      angular.element($scope.email_updt).text($scope.bouncedemail);
                        angular.element($scope.email_updt).parent().html('<a class="buyername" data-toggle="tooltip" title="' + $scope.bouncedemail + '" ng-click="sendTrialindividualpush(\'' + $scope.mem_id + '\' ,' + "'mail'" + ');">' + $scope.bouncedemail + '</a>');
//                      angular.element($scope.email_updt).attr('ng-click',"sendindividualpush('"+$scope.mem_id+"','email')");
                        $compile(angular.element(document.getElementById('DataTables_Table_trail_1')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_trail_2')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_trail_3')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_trail_4')).contents())($scope);
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text('Student email unblocked successfully ');
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
                  $('#progress-full').hide();
            };
            
            $scope.trialtoggleAll = function (selectAll, selectedItems) {
                $scope.selectedData = [];
                 $scope.color_green='N';
                 $scope.all_select_flag='N';
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        selectedItems[id] = selectAll;
                        if(selectAll!==false){
                            $scope.all_select_flag='Y';
                            $scope.color_green='Y';
                        }
                    }
                }
                $scope.maintainselected = selectedItems;
                console.log($scope.all_select_flag+ "color"+ $scope.color_green);
            };
          
           $scope.trialtoggleOne= function (selectedItems) {
                var check = 0;
                $scope.maintainselected = selectedItems;
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.selectedData = [];
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        if (!selectedItems[id]) {
                            check = 1;
                        } else {
                            $scope.selectedData.push({"id": id});
                            $scope.color_green = 'Y';
                        }

                    }
                }
                
//                console.log(JSON.stringify(selectedItems));
                if(check === 1){
                    $scope.selectAll = false;
                }else{
                    $scope.selectAll = true;
                }
//                console.log($scope.all_select_flag+"color"+ $scope.color_green);
//                console.log(JSON.stringify($scope.selectedData));
            };
           
           $scope.showName=function(full) {
                $localStorage.currenttrialregid = full;
                $location.path('/trialdetail');
            }   

            $scope.trialactiveview = true;
            $scope.trialonholdeview = false;
            $scope.trialcancelview = false;
            $scope.trialdidntstartview = false;
            $scope.activetrialdays = '5';
            $localStorage.currentpage = "trials";
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.pushmsgdata = '';
            $scope.activestartdate = '';
            $scope.activeenddate = '';
            $scope.onholdstartdate = '';
            $scope.onholdenddate = '';
            $scope.cancelledstartdate = '';
            $scope.cancelledenddate = '';
            $scope.didntstartstartdate = '';
            $scope.didntstartenddate = '';
            $scope.also_send_email = false;
            $localStorage.buyerName = "";
            $localStorage.participantName = "";
            $localStorage.currenttrialregid = ""; 
            $scope.maxEmailAttachmentSizeError = false;
            $scope.filepreview = ""; 
            $scope.attachfile = '';
            $scope.file_name = '';
            $scope.fileurl = '';
            $scope.data_loaded_length = 0;
            
            
            $scope.removeAttachfile = function () {
                $scope.maxEmailAttachmentSizeError = false;
                $scope.filepreview = ""; 
                $scope.attachfile = '';
                $scope.file_name = '';
                $scope.fileurl = '';
            };

            $scope.trialexportcheckedmembersexcel = function () {
                var selected_days_type;
                if($scope.current_tab === 'A'){
                    selected_days_type = $scope.activetrialdays;
                }else if($scope.current_tab === 'E'){
                    selected_days_type = $scope.holdtrialdays;
                }else if($scope.current_tab === 'C'){
                    selected_days_type = $scope.canceltrialdays;
                }else if($scope.current_tab === 'D'){
                    selected_days_type = $scope.didntstarttrialdays;
                }
                
             function formatTodayDate() {
                    var d = new Date,
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }
                $scope.today_date = formatTodayDate();
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'createTrialSelectonCSV',
                    responseType: 'arraybuffer',
                    data: {
                        "company_id": $localStorage.company_id,
                        "selected_trial_list": $scope.selectedData,
                        "all_select_flag": $scope.all_select_flag,
                        "trial_status": $scope.current_tab,
                        "trial_days_type": selected_days_type,
                        "trial_startdate_limit": $scope.trial_startdate_limit,
                        "trial_enddate_limit": $scope.trial_enddate_limit,
                        "search":$scope.searchTerm,
                        "from":"Trial",
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).success(function (data, status, headers) {
                    $scope.clearSelectedList();
                    var response = '';
                    if ('TextDecoder' in window) {
                        // DECODE AS UTF-8
                        var dataView = new DataView(data);
                        var decoder = new TextDecoder('utf8');
                        if ($scope.isJson(decoder.decode(dataView)) === true) {
                            response = JSON.parse(decoder.decode(dataView));
                        }
                    } else {
                        // FALLBACK DECODE AS ASCII
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                        if ($scope.isJson(decodedString) === true) {
                            response = JSON.parse(decodedString);
                        }
                    }
                    if (response.status !== undefined && response.status === 'Failed') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.msg);                        
                        return false;
                    } else {
                        $scope.clearSelectedList();
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text("Participant Details exported as csv file successfully.");
                    }
                    var msg = '';
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], {type: "application/x-download"});
                        var objectUrl = URL.createObjectURL(blob);
                        linkElement.setAttribute('href', objectUrl);
                        linkElement.setAttribute("download", "Trial_" + $scope.today_date + ".csv");
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        msg = ex;
                        console.log(ex);
                    }
                    if (msg === '') {
                        msg = "CSV file downloaded Successfully";
                    }
                    $scope.clearSelectedList();
                    $('#progress-full').hide();
                    $("#trialmessageModal").modal('show');
                    $("#trialmessagetitle").text('Message');
                    $("#trialmessagecontent").text(msg);

                }).error(function () {
                    $('#progress-full').hide();
                    $("#trialmessageModal").modal('show');
                    $("#trialmessagetitle").text('Message');
                    $("#trialmessagecontent").text("Unable to reach Server, Please Try again.");
                });
            };
            
            $scope.isJson = function (str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            };
            
            
            $scope.sendTrialindividualpush = function (selectedMember, type) { // INDIVIDUAL PUSH MESSAGE
                $scope.clearfromindividual = true; 
                $scope.selectAll = false;
                $scope.trialtoggleAll(false,$scope.maintainselected);
                $scope.message_type = type;
                $scope.selectedData = [];
//                $scope.clearSelectedList();
                $scope.selectedData.push({"id":selectedMember});
                $scope.grpbuttonText = "Send"; 
                if (type === 'push') {
                    $("#trialgrppushmessagecontModal").modal('show');
                    $("#trialgrppushmessageconttitle").text('Send Push Message');
                } else {
                    $("#trialgrpemailmessagecontModal").modal('show');
                    $("#trialgrpemailmessageconttitle").text('Send Email');
                    $("#message_content_1_desclink").hide();
                    $("#message_content_1_maillink").show();
                }
            };
            
            $scope.trialsendGroupMsg = function (tab,type) { 
                $scope.current_tab = tab;
                $scope.message_type = type;
                $scope.grpbuttonText = "Send";
                if ($scope.selectedData.length > 0 || $scope.all_select_flag==='Y') {
                    if ($scope.message_type === 'push') {
                        $("#trialgrppushmessagecontModal").modal('show');
                        $("#trialgrppushmessageconttitle").text('Send Push Message');
                    } else if ($scope.message_type === 'mail') {
                        $("#trialgrpemailmessagecontModal").modal('show');
                        $("#trialgrpemailmessageconttitle").text('Send Email');
                        $("#message_content_1_desclink").hide();
                        $("#message_content_1_maillink").show();
                    } else if ($scope.message_type === 'csv') {
                        $("#trialgrpcsvdownloadModal").modal('show');
                        $("#trialgrpcsvdownloadtitle").text('Message');
                        $("#trialgrpcsvdownloadcontent").text('Do you want to download csv file for selected members?');
                    }
                } else {
                    var x = document.getElementById("trialsnackbar")
                    x.className = "show";
                    setTimeout(function () {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                }
            }

            $scope.trialsendGroupEmailPush = function () {
                $('#progress-full').show();
                var selected_days_type,unsubscribe_email;
                if($scope.current_tab === 'A'){
                    selected_days_type = $scope.activetrialdays;
                }else if($scope.current_tab === 'E'){
                    selected_days_type = $scope.holdtrialdays;
                }else if($scope.current_tab === 'C'){
                    selected_days_type = $scope.canceltrialdays;
                }else if($scope.current_tab === 'D'){
                    selected_days_type = $scope.didntstarttrialdays;
                }
                var msg = '';
                var subject = '';
                if ($scope.message_type === 'push') {
                    if ($scope.grppushmsgdata.trim().length === 0) {
                        return;
                    }
                    subject = '';
                    msg = $scope.grppushmsgdata;
                } else if ($scope.message_type === 'mail') {
                    if ($scope.grpemailmsgdata.trim().length === 0 || $scope.grpemailsubjectdata.trim().length === 0) {
                        return;
                    }
                    subject = $scope.grpemailsubjectdata;
                    msg = $scope.grpemailmsgdata;
                    
                    if($scope.attachfile !== '' && $scope.attachfile !== undefined && $scope.attachfile !== null){
                        $scope.fileurl = $scope.filepreview.substr($scope.filepreview.indexOf(",") + 1);
                        $scope.file_name = $scope.attachfile.split("fakepath")[1].substr(1);
                    }else{
                        $scope.fileurl = '';
                        $scope.file_name = '';
                    }
                }else if($scope.message_type === 'subscribe'){
                    subject = 'Subscribe to our email communications!';
                    msg = 'click here to subscribe';
                    unsubscribe_email = $scope.unsubscribe_email;
                }
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendTrialGroupPush',
                    data: {
                        "company_id": $localStorage.company_id,
                        "type": $scope.message_type,
                        "subject": subject,
                        "message": msg,
                        "also_send_mail": $scope.also_send_email ? 'Y' : 'N',
                        "selected_trial_list": $scope.selectedData,
                        "file_name": $scope.file_name,
                        "attached_file": $scope.fileurl,
                        "all_select_flag": $scope.all_select_flag,
                        "trial_status": $scope.current_tab,
                        "trial_days_type": selected_days_type,
                        "trial_startdate_limit": $scope.trial_startdate_limit,
                        "trial_enddate_limit": $scope.trial_enddate_limit,
                        "search":$scope.searchTerm,
                        "from" : "Trial",
                        "unsubscribe_email":unsubscribe_email

                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $scope.clearSelectedList();
                        $('#progress-full').hide();
                        $("#trialgrpemailmessagecontModal").modal('hide');
                        $("#trialgrpemailmessageconttitle").text('');
                        $("#trialgrppushmessagecontModal").modal('hide');
                        $("#trialgrppushmessageconttitle").text('');
                        $("#sendsubscriptioninviteModal").modal('hide');
                    if (response.data.status === 'Success') {                        
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                          $scope.clearSelectedList();
                         $("#trialgrpemailmessagecontModal").modal('hide');
                        $("#trialgrpemailmessageconttitle").text('');
                        $("#trialgrppushmessagecontModal").modal('hide');
                        $("#trialgrppushmessageconttitle").text('');
                        $scope.handleFailure(response.data.msg);
                    }                   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.active = function () {
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.change_made = $scope.search_value = '';
                $scope.activetrialdays = '5';
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.selectedData = [];
                $scope.trialactiveview = true;
                $scope.trialonholdeview = false;
                $scope.trialcancelview = false;
                $scope.trialdidntstartview = false;
                $scope.activestartdate = '';
                $scope.activeenddate = '';
                $scope.onholdstartdate = '';
                $scope.onholdenddate = '';
                $scope.cancelledstartdate = '';
                $scope.cancelledenddate = '';
                $scope.didntstartstartdate = '';
                $scope.didntstartenddate = '';
                $scope.color_green='N';
                $scope.resettabbarclass();
               $('#li-trialactive').addClass('active-event-list').removeClass('event-list-title');
               $('.trialtab-text-2').addClass('greentab-text-active');
                $scope.liveregistertypeselection = '1';
                $scope.liveregistershow = true;
                $scope.liveregistershowattended = false;
                $scope.liveregistershowattended1 = false;
                $scope.getTrialMember('A');
                $scope.trialdtOptions
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
          };
           
            $scope.enrolled = function () {
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.change_made = $scope.search_value = '';
                $scope.holdtrialdays = '5';
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.selectedData = [];
                $scope.trialactiveview = false;
                $scope.trialonholdeview = true;
                $scope.trialcancelview = false;
                $scope.trialdidntstartview = false;
                $scope.activestartdate = '';
                $scope.activeenddate = '';
                $scope.onholdstartdate = '';
                $scope.onholdenddate = '';
                $scope.cancelledstartdate = '';
                $scope.cancelledenddate = '';
                $scope.didntstartstartdate = '';
                $scope.didntstartenddate = '';
                $scope.color_green='N';
                $scope.resettabbarclass();
               $('#li-trialenrolled').addClass('active-event-list').removeClass('event-list-title');
               $('.trialtab-text-2').addClass('greentab-text-active');
                $scope.enrolledregistertypeselection = '1';
                $scope.enrolledregistershow = true;
                $scope.enrolledregistershowattended = $scope.enrolledregistershowattended1 = false;
                $scope.getTrialMember('E');
                $scope.trialdtOptions1
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
          };
          
            $scope.cancelled = function () {
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.change_made = $scope.search_value = '';
                $scope.canceltrialdays = '5';
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.selectedData = [];
                $scope.trialactiveview = false;
                $scope.trialonholdeview = false;
                $scope.trialcancelview = true;
                $scope.trialdidntstartview = false;
                $scope.activestartdate = '';
                $scope.activeenddate = '';
                $scope.onholdstartdate = '';
                $scope.onholdenddate = '';
                $scope.cancelledstartdate = '';
                $scope.cancelledenddate = '';
                $scope.didntstartstartdate = '';
                $scope.didntstartenddate = '';
                $scope.color_green = 'N';
                $scope.resettabbarclass();
                $('#li-trialcancelled').addClass('active-event-list').removeClass('event-list-title');
                $('.trialtab-text-2').addClass('greentab-text-active');
                $scope.cancelledregistertypeselection = '1';
                $scope.cancelledregistershow = true;
                $scope.cancelledregistershowattended = $scope.cancelledregistershowattended1 = false;
                $scope.getTrialMember('C');
                $scope.trialdtOptions2
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
           };
           
           $scope.didnotstart = function () {
               $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.change_made = $scope.search_value = '';
                $scope.didntstarttrialdays = '5';
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.selectedData = [];
                $scope.trialactiveview = false;
                $scope.trialonholdeview = false;
                $scope.trialcancelview = false;
                $scope.trialdidntstartview = true;
                $scope.activestartdate = '';
                $scope.activeenddate = '';
                $scope.onholdstartdate = '';
                $scope.onholdenddate = '';
                $scope.cancelledstartdate = '';
                $scope.cancelledenddate = '';
                $scope.didntstartstartdate = '';
                $scope.didntstartenddate = '';
                $scope.color_green = 'N';
                $scope.resettabbarclass();
                $('#li-trialdidntstart').addClass('active-event-list').removeClass('event-list-title');
                $('.trialtab-text-2').addClass('greentab-text-active');
                $scope.didntstartregistertypeselection = '1';
                $scope.didntstartregistershow = true;
                $scope.didntstartregistershowattended = $scope.didntstartregistershowattended1 = false;
                $scope.getTrialMember('D');
                $scope.trialdtOptions3
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
           };
          
           $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('.greentab-text').removeClass('greentab-text-active');
            };
            $scope.active();
             $scope.reloadData=function(cur_tab){
                 $scope.refresh_tab=cur_tab;
             }
                
             $scope.$on('$viewContentLoaded', function () {
                $('#activetrialstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });               
            });
            
             $scope.$on('$viewContentLoaded', function () {
                $('#activetrialenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });                
            });
            
              $scope.$on('$viewContentLoaded', function () {
                $('#holdtrialstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });               
            });
            
             $scope.$on('$viewContentLoaded', function () {
                $('#holdtrialenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });                
            });
            
            
            $scope.$on('$viewContentLoaded', function () {
                $('#canceltrialstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });               
            });
            
             $scope.$on('$viewContentLoaded', function () {
                $('#canceltrialenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });                
            });
            
            $scope.$on('$viewContentLoaded', function () {
                $('#didntstarttrialstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });               
            });
            
             $scope.$on('$viewContentLoaded', function () {
                $('#didntstarttrialenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });                
            });
            
            
            $scope.dateformatfilter = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }

            };   
            
            $scope.trialdayselection = function (value) {
                var daystype;
                this.activestartdate = '';
                this.activeenddate = '';
                this.onholdstartdate = '';
                this.onholdenddate = '';
                this.cancelledstartdate = '';
                this.cancelledenddate = '';
                this.didntstartstartdate = '';
                this.didntstartenddate = '';
                
                if(value === 'A'){
                    daystype= this.activetrialdays;
                }else if(value === 'E'){
                    daystype= this.holdtrialdays;
                }else if(value === 'C'){
                    daystype= this.canceltrialdays;
                }else if(value === 'D'){
                    daystype= this.didntstarttrialdays;
                }
                
                if (daystype === '1' || daystype === '2' || daystype === '3' || daystype === '5') {
                    $scope.trialclearDateFields();
                    $scope.trialfilterByDate(value,daystype);
                } else {
                    $scope.trialclearDateFields();
                    // DATE RANGE SELECTION FILTER
                }
            }; 

            $scope.clearSelectedList = function () {
                $scope.removeAttachfile();
                $scope.grppushmsgdata = $scope.grpemailsubjectdata = $scope.grpemailmsgdata = '';
                $scope.also_send_email = false;
                if($scope.clearfromindividual){//clear if modal open from individual
                   $scope.selectedData = [];
                }
                $scope.clearfromindividual = false; 
            }; 
            
            $scope.trialfilterByDate = function (value,daystype) {
                $scope.current_tab = value;
                $scope.trial_array = [];
                var current_tab_id = '';
               
               $('#progress-full').show();
                if (value === 'A') { // FILTERS DATA FOR ACTIVE TAB //
                    value = 'A';
                    $scope.activetrialdays = daystype;
                    if ($scope.activetrialdays === '4') {
                        if (this.activestartdate === '' || this.activestartdate === undefined) {
                            $scope.trial_startdate_limit = '';
                            $scope.trial_enddate_limit = '';
                        } else {
                            $scope.trial_startdate_limit = $scope.dateformatfilter(this.activestartdate);
                        }

                        if (this.activeenddate === '' || this.activeenddate === undefined) {
                            $scope.trial_startdate_limit = '';
                            $scope.trial_enddate_limit = '';
                        } else {
                            $scope.trial_enddate_limit = $scope.dateformatfilter(this.activeenddate);
                        }
                    } else {
                        $scope.trial_startdate_limit = '';
                        $scope.trial_enddate_limit = '';
                    }
                    current_tab_id = 'trial_table_active';
                }

                if (value === 'E') { // FILTERS DATA FOR ON HOLD TAB //
                    value = 'E';
                    $scope.holdtrialdays = daystype;
                    if ($scope.holdtrialdays === '4') {
                        if (this.onholdstartdate === '' || this.onholdstartdate === undefined) {
                            $scope.trial_startdate_limit = '';
                            $scope.trial_enddate_limit = '';
                        } else {
                            $scope.trial_startdate_limit = $scope.dateformatfilter(this.onholdstartdate);
                        }

                        if (this.onholdenddate === '' || this.onholdenddate === undefined) {
                            $scope.trial_startdate_limit = '';
                            $scope.trial_enddate_limit = '';
                        } else {
                            $scope.trial_enddate_limit = $scope.dateformatfilter(this.onholdenddate);
                        }
                    } else {
                        $scope.trial_startdate_limit = '';
                        $scope.trial_enddate_limit = '';
                    }
                     current_tab_id = 'trial_table_enrolled';
                }
                if (value === 'C') { // FILTERS DATA FOR CANCELLED TAB //
                    value = 'C';
                    $scope.canceltrialdays = daystype;

                    if ($scope.canceltrialdays === '4') {
                        if (this.cancelledstartdate === '' || this.cancelledstartdate === undefined) {
                            $scope.trial_startdate_limit = '';
                            $scope.trial_enddate_limit = '';
                        } else {
                            $scope.trial_startdate_limit = $scope.dateformatfilter(this.cancelledstartdate);
                        }

                        if (this.cancelledenddate === '' || this.cancelledenddate === undefined) {
                            $scope.trial_startdate_limit = '';
                            $scope.trial_enddate_limit = '';
                        } else {
                            $scope.trial_enddate_limit = $scope.dateformatfilter(this.cancelledenddate);
                        }
                    } else {
                        $scope.trial_startdate_limit = '';
                        $scope.trial_enddate_limit = '';
                    }
                     current_tab_id = 'trial_table_cancelled';
                }
                if (value === 'D') { // FILTERS DATA FOR CANCELLED TAB //
                    value = 'D';
                    $scope.didntstarttrialdays = daystype;

                    if ($scope.didntstarttrialdays === '4') {
                        if (this.didntstartstartdate === '' || this.didntstartstartdate === undefined) {
                            $scope.trial_startdate_limit = '';
                            $scope.trial_enddate_limit = '';
                        } else {
                            $scope.trial_startdate_limit = $scope.dateformatfilter(this.didntstartstartdate);
                        }

                        if (this.didntstartenddate === '' || this.didntstartenddate === undefined) {
                            $scope.trial_startdate_limit = '';
                            $scope.trial_enddate_limit = '';
                        } else {
                            $scope.trial_enddate_limit = $scope.dateformatfilter(this.didntstartenddate);
                        }
                    } else {
                        $scope.trial_startdate_limit = '';
                        $scope.trial_enddate_limit = '';
                    }
                     current_tab_id = 'trial_table_didntstart';
                }

                $scope.gettrialdetailsbyFilter(value,current_tab_id,daystype);
            };
            
            $scope.trialclearfilter = function (type) { // CLEAR THE APPLIED FILTER //                
                    
                if (type === 'A') {
                    this.activestartdate = '';
                    this.activeenddate = '';
                    this.activetrialdays = '5';
                    $scope.active();
                } else if (type === 'E') {
                    this.onholdstartdate = '';
                    this.onholdenddate = '';
                    this.holdtrialdays = '5';
                    $scope.enrolled();
                } else if (type === 'C') {
                    this.cancelledstartdate = '';
                    this.cancelledenddate = '';
                    this.canceltrialdays = '5';
                    $scope.cancelled();
                } else if (type === 'D') {
                    this.didntstartstartdate = '';
                    this.didntstartenddate = '';
                    this.didntstarttrialdays = '5';
                    $scope.didnotstart();
                } 
                $scope.clearSelectedList();
            };
            
            $scope.showMessage = function () {
                $("#trialmessageModal").modal('show');
                $("#trialmessagetitle").text('Buyer Info');
                $("#trialmessagecontent").text("Coming soon.  This page will provide the following\n\
                data for the Participant from this Buyer : Previous payment history. Upcoming scheduled payments. \n\
                Events that have been purchased. Total revenue generated from Participant.");
            }  
            
            $scope.dateformat = function (date) {
                if(date){
                    $scope.datefor = date.split(" ");
                    return  $scope.datefor[0];
                }
            };
            
            $scope.catchactivesdate = function(){
                if($scope.activestartdate !== ''){
                    $('#activetrialstartdate').datepicker('setStartDate', $scope.activestartdate);
                }else{
                    $('#activetrialenddate').datepicker('setStartDate', null);
                    $scope.activeenddate = '';
                }
            };
            
            $scope.catchactiveedate = function(){
                if($scope.activeenddate !== ''){
                    $('#activetrialstartdate').datepicker('setEndDate', $scope.activeenddate);
                }else{
                    $('#activetrialstartdate').datepicker('setEndDate', null);
                }
            };
            
            $scope.catchholdsdate = function(){
                if($scope.onholdstartdate !== ''){
                    $('#holdtrialstartdate').datepicker('setStartDate', $scope.onholdstartdate);
                }else{
                    $('#holdtrialenddate').datepicker('setStartDate', null);
                    $scope.onholdenddate = '';
                }
            };
            
            $scope.catchholdedate = function(){
                if($scope.onholdenddate !== ''){
                    $('#holdtrialstartdate').datepicker('setEndDate', $scope.onholdenddate);
                }else{
                    $('#holdtrialstartdate').datepicker('setEndDate', null);
                }
            };
            
            $scope.catchcancelsdate = function(){
                if($scope.cancelledstartdate !== ''){
                    $('#canceltrialenddate').datepicker('setStartDate', $scope.cancelledstartdate);
                }else{
                    $('#canceltrialenddate').datepicker('setStartDate', null);
                    $scope.cancelledenddate = '';
                }
            };
            
            $scope.catchcanceledate = function(){
                if($scope.cancelledenddate !== ''){
                    $('#canceltrialstartdate').datepicker('setEndDate', $scope.cancelledenddate);
                }else{
                    $('#canceltrialstartdate').datepicker('setEndDate', null);
                }
            };
            
            $scope.catchdidntsdate = function(){
                if($scope.didntstartstartdate !== ''){
                    $('#didntstarttrialenddate').datepicker('setStartDate', $scope.didntstartstartdate);
                }else{
                    $('#didntstarttrialenddate').datepicker('setStartDate', null);
                    $scope.cancelledenddate = '';
                }
            };
            
            $scope.catchdidntedate = function(){
                if($scope.didntstartenddate !== ''){
                    $('#didntstarttrialstartdate').datepicker('setEndDate', $scope.didntstartenddate);
                }else{
                    $('#didntstarttrialstartdate').datepicker('setEndDate', null);
                }
            };
            
            $scope.trialclearDateFields = function(){
                $scope.activestartdate = $scope.activeenddate = $scope.onholdstartdate = $scope.onholdenddate = $scope.cancelledstartdate = $scope.cancelledenddate = '' ,$scope.didntstartstartdate = '',$scope.didntstartenddate = '';     
                
                $('#activetrialstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
                
                 $('#activetrialenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
                
                $('#holdtrialstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
                
                $('#holdtrialenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });  
                
                $('#canceltrialstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                }); 
                
                $('#canceltrialenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                }); 
                
                $('#didntstarttrialstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                }); 
                
                $('#didntstarttrialenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                }); 
            };
//            MM Attendance filter
            $scope.liveregistershow = $scope.enrolledregistershow = $scope.cancelledregistershow = $scope.didntstartregistershow = true;
            $scope.liveregistershowattended = $scope.liveregistershowattended1 = false;
            $scope.enrolledregistershowattended = $scope.enrolledregistershowattended1 = false;
            $scope.cancelledregistershowattended = $scope.cancelledregistershowattended1 = false;
            $scope.didntstartregistershowattended = $scope.didntstartregistershowattended1 = false;
            $scope.liveregistertypeselection = $scope.enrolledregistertypeselection = $scope.cancelledregistertypeselection = $scope.didntstartregistertypeselection = '1';
                    
            $scope.trialregistertypechange = function (tabtype) {
                if(tabtype === 'A'){
                    $scope.liveregistertype = this.liveregistertypeselection;
                    if (this.liveregistertypeselection === '1') {
                        $scope.liveregistershow = true;
                        $scope.liveregistershowattended = false;
                        $scope.liveregistershowattended1 = false;
                        $scope.getTrialMember('A');
                    } else if (this.liveregistertypeselection === '2') {
                        $scope.livenotattended1='';
                        $scope.livenotattended = '';
                        $scope.liveregistershow = false;
                        $scope.liveshowtextbefore = " to ";
                        $scope.liveshowtextafter = " days ago "
                        $scope.liveregistershowattended = true;
                        $scope.liveregistershowattended1 = false;
                    } else if (this.liveregistertypeselection === '3') {
                        $scope.livenotattendedday = 2;
                        $scope.livenotattended1='14';
                        $scope.livenotattendedday1 = this.livenotattended1;
                        $scope.liveregistershow = false;
                        $scope.liveshowtextbefore = "2 or less class(es) in the last ";
                        $scope.liveshowtextafter = " days "
                        $scope.liveregistershowattended = false;
                        $scope.liveregistershowattended1 = true;                   
                        $scope.gettrialdetailsbyFilter('A','',3);
                    }
                }else if(tabtype === 'E'){
                    $scope.enrolledregistertype = this.enrolledregistertypeselection;
                    if (this.enrolledregistertypeselection === '1') {
                        $scope.enrolledregistershow = true;
                        $scope.enrolledregistershowattended = false;
                        $scope.enrolledregistershowattended1 = false;
                        $scope.getTrialMember('E');
                    } else if (this.enrolledregistertypeselection === '2') {
                        $scope.enrollednotattended1='';
                        $scope.enrollednotattended = '';
                        $scope.enrolledregistershow = false;
                        $scope.enrolledshowtextbefore = " to ";
                        $scope.enrolledshowtextafter = " days ago "
                        $scope.enrolledregistershowattended = true;
                        $scope.enrolledregistershowattended1 = false;
                    } else if (this.enrolledregistertypeselection === '3') {
                        $scope.enrollednotattendedday = 2;
                        $scope.enrollednotattended1 = '14';
                        $scope.enrollednotattendedday1 = this.enrollednotattended1;
                        $scope.enrolledregistershow = false;
                        $scope.enrolledshowtextbefore = "2 or less class(es) in the last ";
                        $scope.enrolledshowtextafter = " days "
                        $scope.enrolledregistershowattended = false;
                        $scope.enrolledregistershowattended1 = true;                   
                        $scope.gettrialdetailsbyFilter('E','',3);
                    }
                }else if(tabtype === 'C'){
                    $scope.cancelledregistertype = this.cancelledregistertypeselection;
                    if (this.cancelledregistertypeselection === '1') {
                        $scope.cancelledregistershow = true;
                        $scope.cancelledregistershowattended = false;
                        $scope.cancelledregistershowattended1 = false;
                        $scope.getTrialMember('C');
                    } else if (this.cancelledregistertypeselection === '2') {
                        $scope.cancellednotattended1='';
                        $scope.cancellednotattended = '';
                        $scope.cancelledregistershow = false;
                        $scope.cancelledshowtextbefore = " to ";
                        $scope.cancelledshowtextafter = " days ago "
                        $scope.cancelledregistershowattended = true;
                        $scope.cancelledregistershowattended1 = false;
                    } else if (this.cancelledregistertypeselection === '3') {
                        $scope.cancellednotattendedday = 2;
                        $scope.cancellednotattended1='14';
                        $scope.cancellednotattendedday1 = this.cancellednotattended1;
                        $scope.cancelledregistershow = false;
                        $scope.cancelledshowtextbefore = "2 or less class(es) in the last ";
                        $scope.cancelledshowtextafter = " days "
                        $scope.cancelledregistershowattended = false;
                        $scope.cancelledregistershowattended1 = true;                   
                        $scope.gettrialdetailsbyFilter('C','',3);
                    }
                }else if(tabtype === 'D'){
                    $scope.didntstartregistertype = this.didntstartregistertypeselection;
                    if (this.didntstartregistertypeselection === '1') {
                        $scope.didntstartregistershow = true;
                        $scope.didntstartregistershowattended = false;
                        $scope.didntstartregistershowattended1 = false;
                        $scope.getTrialMember('D');
                    } else if (this.didntstartregistertypeselection === '2') {
                        $scope.didntstartnotattended1 = '';
                        $scope.didntstartnotattended = '';
                        $scope.didntstartregistershow = false;
                        $scope.didntstartshowtextbefore = " to ";
                        $scope.didntstartshowtextafter = " days ago "
                        $scope.didntstartregistershowattended = true;
                        $scope.didntstartregistershowattended1 = false;
                    } else if (this.didntstartregistertypeselection === '3') {
                        $scope.didntstartnotattendedday = 2;
                        $scope.didntstartnotattended1 = '14';
                        $scope.didntstartnotattendedday1 = this.didntstartnotattended1;
                        $scope.didntstartregistershow = false;
                        $scope.didntstartshowtextbefore = "2 or less class(es) in the last ";
                        $scope.didntstartshowtextafter = " days "
                        $scope.didntstartregistershowattended = false;
                        $scope.didntstartregistershowattended1 = true;                   
                        $scope.gettrialdetailsbyFilter('D','',3);
                    }
                }
            };
            $scope.trialfilterattend = function (value, daystyp) {
                if(value === 'A'){
                $scope.livenotattendedday = this.livenotattended;
                $scope.livenotattendedday1 = this.livenotattended1;
                $scope.liveregistertype = daystyp;
                $scope.gettrialdetailsbyFilter(value, '', '') 
                }else if(value === 'E'){
                $scope.enrollednotattendedday = this.enrollednotattended;
                $scope.enrollednotattendedday1 = this.enrollednotattended1;
                $scope.enrolledregistertype = daystyp;
                $scope.gettrialdetailsbyFilter(value, '', '') 
                }else if(value === 'C'){
                $scope.cancellednotattendedday = this.cancellednotattended;
                $scope.cancellednotattendedday1 = this.cancellednotattended1;
                $scope.cancelledregistertype = daystyp;
                $scope.gettrialdetailsbyFilter(value, '', '')
                }else if(value === 'D'){
                $scope.didntstartnotattendedday = this.didntstartnotattended;
                $scope.didntstartnotattendedday1 = this.didntstartnotattended1;
                $scope.didntstartregistertype = daystyp;
                $scope.gettrialdetailsbyFilter(value, '', '')
                }
            };
            $scope.cleartrialfilterattend = function () {
                $route.reload();
            };
            $scope.triallastattenedclasschange = function (value, daystyp) {
                if(value === 'A'){
                    $scope.livenotattendedday = 2;
                    $scope.livenotattendedday1 = this.livenotattended1;
                    $scope.liveregistertype = daystyp;
                    $scope.gettrialdetailsbyFilter(value, '', '');
                }else if(value === 'E'){
                    $scope.enrollednotattendedday = 2;
                    $scope.enrollednotattendedday1 = this.enrollednotattended1;
                    $scope.enrolledregistertype = daystyp;
                    $scope.gettrialdetailsbyFilter(value, '', '');
                }else if(value === 'C'){
                    $scope.cancellednotattendedday = 2;
                    $scope.cancellednotattendedday1 = this.cancellednotattended1;
                    $scope.cancellederegistertype = daystyp;
                    $scope.gettrialdetailsbyFilter(value, '', '');
                }else if(value === 'D'){
                    $scope.didntstartenotattendedday = 2;
                    $scope.didntstartnotattendedday1 = this.didntstartnotattended1;
                    $scope.didntstartregistertype = daystyp;
                    $scope.gettrialdetailsbyFilter(value, '', '');
                }
            };
            
            $scope.gettrialdetailsbyFilter = function(value, current_tab_id, daystype){
                $scope.current_tab=value;
                DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                    var titleHtml = '<input type="checkbox" ng-model="selectAll" ng-click="trialtoggleAll(selectAll, selected)">';
                var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                $scope.selected = {};
                $scope.selectAll = false;
                if ($scope.current_tab === 'A') {
                    $scope.trialdtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'trialcustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.triallistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                    "company_id": $localStorage.company_id,
                                    "trial_status_tab": $scope.current_tab,
                                    "trial_days_type": daystype,
                                    "trial_startdate_limit": $scope.trial_startdate_limit,
                                    "trial_enddate_limit": $scope.trial_enddate_limit,
                                    "trialregister_type": $scope.liveregistertype,
                                    "trialnotattendeddays": $scope.livenotattendedday,
                                    "trialnotattendeddays1": $scope.livenotattendedday1
                                },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.trialdtColumns = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                                DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_ta1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'trials'" + ',' + full.trial_id + ',' + "'active'" + ');">' + full.buyer_name + '</a>';
                                }),
                                  DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_ta2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_ta8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_ta3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_ta4').renderWith(
                                function (data, type, full,meta) {
                                    if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                                }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_ta5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_ta8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_ta9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_ta10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                                DTColumnBuilder.newColumn('l_att').withTitle('Last Attendance').withClass('col_ta11').renderWith(
                                function (data, type, full) {
                                    if (data > 0)
                                        return (data) + ' days ago';                                    
                                    else if (data == '0')
                                        return 'Today';
                                    else 
                                        return '';
                                }),
                                DTColumnBuilder.newColumn('act_att').withTitle('Attendance count').withClass('col_ta12').notSortable().renderWith(
                                function (data, type, full) {
                                    if (full.act_att > 0)
                                        return full.act_att+' total';
                                    else
                                        return full.act_att;
                                }),
                                DTColumnBuilder.newColumn('14_att').withTitle('Attendance Last 14 Days').withClass('col_ta13'),
                                DTColumnBuilder.newColumn('30_att').withTitle('Attendance Last 30 Days').withClass('col_ta14'),
                    ];
                   $scope.dtInstance = {};
                }if ($scope.current_tab === 'E') {
                   $scope.trialdtOptions1 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'trialcustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                               $scope.triallistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                    "trial_status_tab": $scope.current_tab,
                                    "trial_days_type": daystype,
                                    "trial_startdate_limit": $scope.trial_startdate_limit,
                                    "trial_enddate_limit": $scope.trial_enddate_limit,
                                    "trialregister_type": $scope.enrolledregistertype,
                                    "trialnotattendeddays": $scope.enrollednotattendedday,
                                    "trialnotattendeddays1": $scope.enrollednotattendedday1
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.trialdtColumns1 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_te1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'trials'" + ',' + full.trial_id + ',' + "'enrolled'" + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_te2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_te8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_te3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_te4').renderWith(
                                function (data, type, full,meta) {
                                    if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                                }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_te5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_te6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_te7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_te8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_te9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_te10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                                DTColumnBuilder.newColumn('l_att').withTitle('Last Attendance').withClass('col_te11').renderWith(
                                function (data, type, full) {
                                    if (data > 0)
                                        return (data) + ' days ago';                                    
                                    else if (data == '0')
                                        return 'Today';
                                    else 
                                        return '';
                                }),
                                DTColumnBuilder.newColumn('act_att').withTitle('Attendance count').withClass('col_te12').notSortable().renderWith(
                                function (data, type, full) {
                                    if (full.act_att > 0)
                                        return full.act_att+' total';
                                    else
                                        return full.act_att;
                                }),
                                DTColumnBuilder.newColumn('14_att').withTitle('Attendance Last 14 Days').withClass('col_te13'),
                                DTColumnBuilder.newColumn('30_att').withTitle('Attendance Last 30 Days').withClass('col_te14'),
                    ];
                   $scope.dtInstance1 = {};
                }if ($scope.current_tab === 'C') {
                   $scope.trialdtOptions2 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'trialcustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.triallistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                    "trial_status_tab": $scope.current_tab,
                                    "trial_days_type": daystype,
                                    "trial_startdate_limit": $scope.trial_startdate_limit,
                                    "trial_enddate_limit": $scope.trial_enddate_limit,
                                    "trialregister_type": $scope.cancelledregistertype,
                                    "trialnotattendeddays": $scope.cancellednotattendedday,
                                    "trialnotattendeddays1": $scope.cancellednotattendedday1
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.trialdtColumns2 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_tc1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'trials'" + ',' + full.trial_id + ',' + "'cancelled'" + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_tc2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_tc8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_tc3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_tc4').renderWith(
                                function (data, type, full,meta) {
                                    if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                                }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_tc5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_tc6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_tc7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_tc8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_tc9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_tc10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                                DTColumnBuilder.newColumn('l_att').withTitle('Last Attendance').withClass('col_tc11').renderWith(
                                function (data, type, full) {
                                    if (data > 0)
                                        return (data) + ' days ago';                                    
                                    else if (data == '0')
                                        return 'Today';
                                    else 
                                        return '';
                                }),
                                DTColumnBuilder.newColumn('act_att').withTitle('Attendance count').withClass('col_tc12').notSortable().renderWith(
                                function (data, type, full) {
                                    if (full.act_att > 0)
                                        return full.act_att+' total';
                                    else
                                        return full.act_att;
                                }),
                                DTColumnBuilder.newColumn('14_att').withTitle('Attendance Last 14 Days').withClass('col_tc13'),
                                DTColumnBuilder.newColumn('30_att').withTitle('Attendance Last 30 Days').withClass('col_tc14'),
                    ];
                   $scope.dtInstance2 = {};
                }if ($scope.current_tab === 'D') {
                   $scope.trialdtOptions3 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'trialcustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.triallistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                    "trial_status_tab": $scope.current_tab,
                                    "trial_days_type": daystype,
                                    "trial_startdate_limit": $scope.trial_startdate_limit,
                                    "trial_enddate_limit": $scope.trial_enddate_limit,
                                    "trialregister_type": $scope.didntstartregistertype,
                                    "trialnotattendeddays": $scope.didntstartnotattendedday,
                                    "trialnotattendeddays1": $scope.didntstartnotattendedday1
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.trialdtColumns3 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_td1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'trials'" + ',' + full.trial_id + ',' + "'didntstart'" + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_td2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_td8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_td3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_td4').renderWith(
                                function (data, type, full,meta) {
                                    if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                                }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_td5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_td6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_td7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_td8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_td9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_td10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                                DTColumnBuilder.newColumn('l_att').withTitle('Last Attendance').withClass('col_td11').renderWith(
                                function (data, type, full) {
                                    if (data > 0)
                                        return (data) + ' days ago';                                    
                                    else if (data == '0')
                                        return 'Today';
                                    else 
                                        return '';
                                }),
                                DTColumnBuilder.newColumn('act_att').withTitle('Attendance count').withClass('col_td12').notSortable().renderWith(
                                function (data, type, full) {
                                    if (full.act_att > 0)
                                        return full.act_att+' total';
                                    else
                                        return full.act_att;
                                }),
                                DTColumnBuilder.newColumn('14_att').withTitle('Attendance Last 14 Days').withClass('col_td13'),
                                DTColumnBuilder.newColumn('30_att').withTitle('Attendance Last 30 Days').withClass('col_td14'),
                    ];
                   $scope.dtInstance3 = {};
                }
                $('#progress-full').hide();
                angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                    console.log('dataTables search : ' + $scope.searchTerm);
                })
            };
            
            $scope.trialdashboardredirectview = function(){
                $localStorage.trialredirectto = "DA";
                $location.path('/managetrial');
            };
            $scope.trialredirectlivelistview = function(){
                $localStorage.trialredirectto = "L";
                $location.path('/managetrial');
            };

            if ($localStorage.attendancedashboard === 'Y') {
                $scope.change_made = $scope.search_value = '';  
                $scope.clearSelectedList();
                $scope.trialactiveview = true;
                $scope.trialonholdeview = false;
                $scope.trialcancelview = false;
                $scope.trialdidntstartview = false;
                $scope.resettabbarclass();  
                $('#li-trialactive').addClass('active-event-list').removeClass('event-list-title');
                $('.trialtab-text-2').addClass('greentab-text-active');
                $localStorage.attendancedashboard = 'N';
                $scope.attendancefilterdata = attendanceFilterDetails.getattendanceFilter(); 
                $scope.liveregistertypeselection = $scope.attendancefilterdata[0].type;
                if($scope.liveregistertypeselection === '2'){                   
                    $scope.livenotattended = $scope.attendancefilterdata[0].frequency_from;
                    $scope.livenotattended1 =$scope.attendancefilterdata[0].frequency_to;
                    $scope.liveregistershow = false;
                    $scope.liveshowtextbefore = " to ";
                    $scope.liveshowtextafter = " days ago "
                    $scope.liveregistershowattended = true;
                    $scope.liveregistershowattended1 = false;
                    $scope.trialfilterattend('A','2');
                }else{
                    $scope.livenotattended1 = $scope.attendancefilterdata[0].frequency_from;
                    $scope.livenotattendedday = 2;
                    $scope.livenotattendedday1 = $scope.livenotattended1;
                    $scope.liveregistershow = false;
                    $scope.liveshowtextbefore = " 2 or less class(es) in the last ";
                    $scope.liveshowtextafter = " days "
                    $scope.liveregistershowattended1 = true;
                    $scope.liveregistershowattended = false;   
                    $scope.liveregistertype = '3';
                    $scope.gettrialdetailsbyFilter('A','','');
                }
            }else {
                if ($localStorage.trialpagefrom === 'trialsactive') {
                    $scope.change_made = $scope.search_value = '';
                    $scope.activetrialdays = '5';
                    $scope.getTrialMember('A');
                    $scope.clearSelectedList();
                    $scope.trialactiveview = true;
                    $scope.trialonholdeview = false;
                    $scope.trialcancelview = false;
                    $scope.trialdidntstartview = false;
                    $scope.resettabbarclass();
                    $('#li-trialactive').addClass('active-event-list').removeClass('event-list-title');
                    $('.trialtab-text-2').addClass('greentab-text-active');
                    $localStorage.trialpagefrom = '';
                } else if ($localStorage.trialpagefrom === 'trialsenrolled') {
                    $scope.change_made = $scope.search_value = '';
                    $scope.holdtrialdays = '5';
                    $scope.getTrialMember('E');
                    $scope.clearSelectedList();
                    $scope.trialactiveview = false;
                    $scope.trialonholdeview = true;
                    $scope.trialcancelview = false;
                    $scope.trialdidntstartview = false;
                    $scope.resettabbarclass();
                    $('#li-trialenrolled').addClass('active-event-list').removeClass('event-list-title');
                    $('.trialtab-text-2').addClass('greentab-text-active');
                    $localStorage.trialpagefrom = '';
                } else if ($localStorage.trialpagefrom === 'trialscancelled') {
                    $scope.change_made = $scope.search_value = '';
                    $scope.canceltrialdays = '5';
                    $scope.getTrialMember('C');
                    $scope.clearSelectedList();
                    $scope.trialactiveview = false;
                    $scope.trialonholdeview = false;
                    $scope.trialcancelview = true;
                    $scope.trialdidntstartview = false;
                    $scope.resettabbarclass();
                    $('#li-trialcancelled').addClass('active-event-list').removeClass('event-list-title');
                    $('.trialtab-text-2').addClass('greentab-text-active');
                    $localStorage.trialpagefrom = '';
                }else if ($localStorage.trialpagefrom === 'trialsdidntstart') {
                    $scope.change_made = $scope.search_value = '';
                    $scope.didntstarttrialdays = '5';
                    $scope.getTrialMember('D');
                    $scope.clearSelectedList();
                    $scope.trialactiveview = false;
                    $scope.trialonholdeview = false;
                    $scope.trialcancelview = false;
                    $scope.trialdidntstartview = true;
                    $scope.resettabbarclass();
                    $('#li-trialdidntstart').addClass('active-event-list').removeClass('event-list-title');
                    $('.trialtab-text-2').addClass('greentab-text-active');
                    $localStorage.trialpagefrom = '';
                }else {
                    $scope.change_made = $scope.search_value = '';
                    $scope.getTrialMember('A');
                    $scope.trialdtOptions
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                    $localStorage.trialpagefrom = '';
                }
            }    
            
            $scope.sendSubscribeEmail = function(mail){
                $scope.unsubscribe_email = mail;
                $scope.message_type = 'subscribe';
                $("#sendsubscriptioninviteModal").modal('show');
            };
            
        } else {
            $location.path('/login');
        }

    }
    module.exports =  TrialsController;