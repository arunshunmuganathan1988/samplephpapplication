AddEventController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', 'eventListDetails', '$filter', '$route','$timeout','$rootScope'];
 function AddEventController ($scope, $location, $http, $localStorage, urlservice, eventListDetails, $filter, $route, $timeout,$rootScope) {

        if ($localStorage.islogin === 'Y') {

            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
//            angular.element(document.getElementById('sidetopbar')).scope().getWepayStatus();
            angular.element(document.getElementById('sidetopbar')).scope().getStripeStatus();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            tooltip.hide();
            $rootScope.activepagehighlight = 'events';

//        $scope.urlfmt =  /^\s*((ftp|http|https)\:\/\/)?([a-z\d\-]{1,63}\.)*[a-z\d\-]{1,255}\.[a-z]{2,6}\s*.*$/; 

            $scope.message = "MyStudio";
            //PREVIEW SINGLE EVENT ADD
           $localStorage.PreviewEventName = '';
           $localStorage.PreviewEventDate = '';
           $localStorage.PreviewEventTime = '';
           $localStorage.PreviewEventEndDate = '';
           $localStorage.PreviewEventEndTime = '';
           $localStorage.PreviewEventCost = '';
           $localStorage.PreviewEventCompareCost = '';
           $localStorage.PreviewEventUrl = '';
           $localStorage.PreviewEventDesc = '';
           $localStorage.PreviewImageUrl =  '';
           $localStorage.PreviewECapacity = '';
           $localStorage.PreviewESpaceRemaining = '';
           
            //PREVIEW MULTIPLE- PARENT EVENT ADD
           $localStorage.PreviewMEventCategory = '';
           $localStorage.PreviewMEventSubcategory = '';
           
            $localStorage.PreviewPEventDesc = '';
            $localStorage.PreviewPEventUrl = '';
            $localStorage.PreviewPImageUrl = '';
            $localStorage.PreviewEditPImageUrl = '';
            
           //PREVIEW EXISTING CHILD EVENT EDIT
           $localStorage.PreviewCfirstedit = '';
           $localStorage.childeventindex = '';
           $localStorage.PreviewCEventDate = '';
           $localStorage.PreviewCEventTime = '';
           $localStorage.PreviewCEventEndDate = '';
           $localStorage.PreviewCEventEndTime = '';
           $localStorage.PreviewCEventCost = '';
           $localStorage.PreviewCEventCompareCost = '';
           $localStorage.PreviewCEventUrl = '';
           $localStorage.PreviewCEventUrlEdit = '';
           $localStorage.PreviewCEventDescEdit =  '';
           $localStorage.PreviewCEventName = '';
           $localStorage.PreviewCECapacity = '';
           $localStorage.PreviewCESpaceRemaining = '';
           $localStorage.PreviewEProcesFeeSelection = '';
           $localStorage.PreviewCEProcesFeeSelection = '';
           $localStorage.PreviewCfirstedit = 'N';
           
           //PREVIEW NEW CHILD EVENT ADD
            $localStorage.PreviewMEventName = '';           
            $localStorage.PreviewMEventDate = '';
            $localStorage.PreviewMEventTime = '';
            $localStorage.PreviewMEventEndDate = '';
            $localStorage.PreviewMEventEndTime = '';
            $localStorage.PreviewMEventUrl = '';
            $localStorage.PreviewMEventDesc = '';
            $localStorage.PreviewMEventCost = '';
            $localStorage.PreviewMEventCompareCost = '';
            $localStorage.preview_eventchildlist =  $localStorage.preview_childlisthide = false;
            $localStorage.PreviewMECapacity = '';
            $localStorage.PreviewMESpaceRemaining = '';
            $localStorage.PreviewChildEventList = '';
            
            $localStorage.PreviewOldRegistrationField='';
            $localStorage.PreviewOldRegFieldMandatory= '';
            $localStorage.preview_reg_col_name_edit = '';
            $localStorage.preview_reg_fieldindex = '';
            $localStorage.PreviewWaiverText = '';
            $localStorage.currentpreviewtab = '';
            
            $localStorage.eventHeader = $scope.eventadded = $scope.IsVisible = false;
            $scope.eventview = $scope.AddeventButton = true;
            $scope.paymentview = $scope.registrationview = $scope.waiverview = $scope.childEventListView = false;
            $scope.childEventCardView = $scope.childEditFormView = false;
            $scope.event_type = "S";
            $scope.single_eventid = $scope.multiple_parent_id = "";
            $scope.discnt_type = $scope.mdiscnt_type = 'V';
            $scope.onetimeschecked = $scope.onetimemchecked = true;
            $scope.recurringschecked = $scope.recurringmchecked = false;
            $scope.onetimesinglepayment = $scope.recurringsinglepayment = 'N';
            $scope.onetimemultiplepayment = $scope.recurringmultiplepayment = 'N';
            $scope.singlepaymentview = $scope.multiplepaymentview = $scope.singlediscountListView = $scope.singlediscountEditView = false;
            $scope.childpaymentCardView = true;
            $scope.eventpaymentformview = $scope.childEventPaymentView = false;
            $scope.singlePaymentUpdateButton = $scope.multiplePaymentUpdateButton = false;
            $scope.selectedchildpayment = $scope.selectedeventlisttype = "";
            $scope.event_image_update = $scope.initialmeventadd = 'N';
            $scope.col_view = true;
            $scope.edit_col_view = $scope.regfieldexceeds = false;
            $scope.waiverpolicy = '';
            $scope.eventprocessingfee = $scope.ceventprocessingfee = '2';
            $scope.spayment_startdate_type = $scope.mpayment_startdate_type ='1';
            $scope.eventspaceremng = $scope.meventspaceremng ='1';
            $scope.checkin_type = 'R';
            $scope.wepaystatus = $localStorage.preview_wepaystatus;
            $scope.wp_currency_code = $localStorage.wp_currency_code;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.event_sdeposit_Amnt_status = $scope.event_mdeposit_Amnt_status = false;
            $localStorage.current_page_from = 'events';

            $scope.singledesc = {text: ""}
            $scope.parentdesc = {text: ""}
            $scope.childdesc = {text: ""}
            $scope.mcevent_desc = {text: ""}
            $scope.cropper = {};
            $scope.cropper.sourceImage = null;
            $scope.cropper.croppedImage = null;
            $scope.bounds = {};
            $scope.bounds.left = 0;
            $scope.bounds.right = 0;
            $scope.bounds.top = 0;
            $scope.bounds.bottom = 0;
            $scope.croppedEventImage = "";
            $scope.eventCregurl = '';
            
            //New event
            $scope.showMevent = $scope.showPevent = $scope.showevent = 'Y';

            $scope.discounttype = function (discounttype) {
                if (discounttype === 'dollar') {
                    $scope.discnt_type = 'V';

                } else if (discounttype === 'percent') {
                    $scope.discnt_type = 'P';
                }
            }

            $scope.mdiscounttype = function (mdiscounttype) {
                if (mdiscounttype === 'dollar') {
                    this.mdiscnt_type = 'V';
                } else if (mdiscounttype === 'percent') {
                    this.mdiscnt_type = 'P';
                }

            }

            $scope.editdiscounttype = function (discnttype) {
                if (discnttype === 'dollar') {
                    this.edit_discnt_type = 'V';

                } else if (discnttype === 'percent') {
                    this.edit_discnt_type = 'P';
                }
            }

            $scope.meditdiscounttype = function (mdiscnttype) {
                if (mdiscnttype === 'dollar') {
                    this.medit_discnt_type = 'V';

                } else if (mdiscnttype === 'percent') {
                    this.medit_discnt_type = 'P';
                }
            }


            $scope.onetimespayment = function () {
                if ($scope.onetimeschecked === true) {
                    $scope.onetimesinglepayment = 'Y';
                } else if ($scope.onetimeschecked === false) {
                    $scope.onetimesinglepayment = 'N';
                }
            }

            $scope.recurringspayment = function () {
                if ($scope.recurringschecked === true) {
                    $scope.recurringsinglepayment = 'Y';
                } else if ($scope.recurringschecked === false) {
                    $scope.recurringsinglepayment = 'N';
                }
            }

            $scope.onetimempayment = function () {
                if ($scope.onetimemchecked === true) {
                    $scope.onetimemultiplepayment = 'Y';
                } else if ($scope.onetimemchecked === false) {
                    $scope.onetimemultiplepayment = 'N';
                }
            }

            $scope.recurringmpayment = function () {
                if ($scope.recurringmchecked === true) {
                    $scope.recurringmultiplepayment = 'Y';
                } else if ($scope.recurringmchecked === false) {
                    $scope.recurringmultiplepayment = 'N';
                }
            }

            $scope.editsinglediscount = function (singlediscount, ind) {
                $scope.singlediscountView = false;
                $scope.singlediscountEditView = false;
                $scope.singlePaymentUpdateButton = true;

                $scope.selecteddiscountindex = ind;
                $scope.edit_event_discount_id = singlediscount.event_discount_id;
                $scope.editdiscountcode = singlediscount.discount_code;
                $scope.edit_discnt_type = singlediscount.discount_type;
                $scope.editdiscountamount = singlediscount.discount_amount;
            }

            $scope.editsinglediscountCancel = function (singlediscount) {
                this.editdiscountcode = singlediscount.discount_code;
                this.edit_discnt_type = singlediscount.discount_type;
                this.editdiscountamount = singlediscount.discount_amount;
                $scope.singlediscountView = true;
                $scope.singlediscountEditView = false;
                $scope.singlePaymentUpdateButton = false;
                $scope.selecteddiscountindex = "";
            };

            $scope.editmultiplediscount = function (multiplediscount, ind) {
                $scope.multiplediscountView = false;
                $scope.multiplediscountEditView = false;
                $scope.selectedmdiscountindex = ind;
                $scope.multiplePaymentUpdateButton = true;

                $scope.medit_event_discount_id = multiplediscount.event_discount_id;
                $scope.meditdiscountcode = multiplediscount.discount_code;
                $scope.medit_discnt_type = multiplediscount.discount_type;
                $scope.meditdiscountamount = multiplediscount.discount_amount;
            };

            $scope.editmultiplediscountCancel = function (multiplediscount) {
                this.medit_discnt_type = multiplediscount.discount_type;
                this.meditdiscountcode = multiplediscount.discount_code;
                this.meditdiscountamount = multiplediscount.discount_amount;
                $scope.multiplediscountView = true;
                $scope.multiplediscountEditView = false;
                $scope.multiplePaymentUpdateButton = false;
                $scope.selectedmdiscountindex = "";
            };

            $scope.cancelchildpayment = function (childpayment) {
                $localStorage.childeventindex = '';
                $localStorage.preview_childlisthide = false;
                $localStorage.preview_eventchildlist = true;
                if( parseFloat(childpayment.event_capacity) > 0 ){
                   this.ceventcapacity = childpayment.event_capacity;
                } else{
                   this.ceventcapacity = '';
                }
                this.ceventspaceremng = childpayment.capacity_text;
                $scope.childpaymentCardView = true;
                $scope.eventpaymentformview = false;
                $scope.selectedchildpayment = "";
                $localStorage.PreviewCEProcesFeeSelection = '';
            }

            $scope.imgchanged = function () {
                $("#eventcropmodal").modal('show');
                $scope.event_image_update = 'N';
            };

            $scope.imgcropcancel = function () {
                $scope.event_image_update = 'N';
            };

            $scope.imgcropped = function () {
                $scope.event_image_update = 'Y';
                $scope.croppedEventImage = $scope.cropper.croppedImage;
            };

            $scope.resetimgcropped = function () {
                $scope.event_image_update = 'Y';
                $scope.croppedEventImage = null;
                $scope.croppedEventImage = '';
            };
            
            $scope.cimgchanged = function () {
                $("#ceventcropmodal").modal('show');
                $scope.cevent_image_update = 'N';
            };
            
            $scope.cimgcropcancel = function () {
                $scope.cevent_image_update = 'N';
            };

            $scope.cimgcropped = function () {
                $scope.cevent_image_update = 'Y';
                $scope.ccroppedEventImage = $scope.cropper.croppedImage;                
            };

            $scope.cresetimgcropped = function () {
                $scope.cevent_image_update = 'Y';
                $scope.ccroppedEventImage = '';
            };
            
            $scope.editcimgchanged = function () {
                $("#editceventcropmodal").modal('show');
                $scope.editcevent_image_update = 'N';   
            };
            
            $scope.editcimgcropcancel = function () {
                $scope.editcevent_image_update = 'N';
            };

            $scope.editcimgcropped = function () {
                $scope.editcevent_image_update = 'Y';
                this.editcroppedEventImage = $scope.cropper.croppedImage;
            };

            $scope.editcresetimgcropped = function () {
                $scope.editcevent_image_update = 'Y';
                this.editcroppedEventImage = '';
            };
            
            $scope.toastmsg = function () {
                var x = document.getElementById("snackbar")
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            
            
            $scope.paymentnow = function(){
                  $("#sidetopbar").removeClass('modal-open');
                  $(".modal-backdrop").css("display", "none");
                  $location.path('/billingstripe');                
            };
            
            $scope.eventpaste = function (e) {
                var pasted_value = e.originalEvent.clipboardData.getData('text/plain');
                var pasted_text_length = pasted_value.length;  
                if(pasted_text_length > 100){
                    var x = document.getElementById("snackbarpaste")
                    x.className = "showpaste";
                    setTimeout(function(){ x.className = x.className.replace("showpaste", ""); }, 3000);
                }
            };

            
            $scope.opentabbar = function (viewname) {
                if ($localStorage.isAddedEventDetails === 'Y' || $localStorage.eventpagetype === 'edit') {
                    if (viewname === 'events') {
                        $localStorage.currentpreviewtab = 'events';
                        $scope.eventview = true;
                        $scope.paymentview = false;
                        $scope.childeventview=false;
                        $scope.registrationview = false;
                        $scope.waiverview = false;
                        $scope.resettabbarclass();
                        $('#tab1').addClass('done');
                        $scope.childEditFormView = false;
                        $scope.selectedchildindex = "";
                        if($scope.event_type === 'M'){
                            $scope.multipleSubEventHide();
                        }
                    }
                    else if (viewname === 'childevents') {
                        $localStorage.currentpreviewtab = 'childevents';
                        $scope.eventview = false;
                        $scope.paymentview = false;
                        $scope.childeventview=true;
                        $scope.registrationview = false;
                        $scope.waiverview = false;
                        $scope.resettabbarclass();
                        $('#tab2').addClass('done');
                        $scope.eventpaymentformview = false;
                        $scope.selectedchildpayment = "";
                         
                    } else if (viewname === 'payment') {
                        $localStorage.currentpreviewtab = 'payment';
                        $scope.eventview = false;
                        $scope.paymentview = true;
                        $scope.childeventview=false;
                        $scope.registrationview = false;
                        $scope.waiverview = false;
                        $scope.resettabbarclass();
                        $('#tab3').addClass('done');
                        $scope.eventpaymentformview = false;
                        $scope.selectedchildpayment = "";

                    } else if (viewname === 'registration') {
                        $localStorage.currentpreviewtab = 'registration';
                        $scope.eventview = false;
                        $scope.paymentview = false;
                        $scope.childeventview=false;
                        $scope.registrationview = true;
                        $scope.waiverview = false;
                        $scope.resettabbarclass();
                        $('#tab4').addClass('done');

                    } else if (viewname === 'waiver') {
                        $localStorage.currentpreviewtab = 'waiver';
                        $scope.eventview = false;
                        $scope.paymentview = false;
                        $scope.registrationview = false;
                        $scope.waiverview = true;
                         $scope.childeventview=false;
                        $scope.resettabbarclass();
                        $('#tab5').addClass('done');

                    }
                } else {
                    $scope.eventview = true;
                    $scope.paymentview = false;
                    $scope.registrationview = false;
                     $scope.childeventview=false;
                    $scope.waiverview = false;
                    $scope.resettabbarclass();
                    $('#tab1').addClass('done');
                }
            };

            $scope.resettabbarclass = function (viewname) {
                $('.done').addClass('disabled').removeClass('done');
            };
            
            $scope.timeServerformat = function (time) {
                if (time.length > 0) {
                    var hours = Number(time.match(/^(\d+)/)[1]);
                    var minutes = Number(time.match(/:(\d+)/)[1]);
                    var AMPM = time.match(/\s(.*)$/)[1];
                    if (AMPM == "PM" && hours < 12)
                        hours = hours + 12;
                    if (AMPM == "AM" && hours == 12)
                        hours = hours - 12;
                    var sHours = hours.toString();
                    var sMinutes = minutes.toString();
                    if (hours < 10)
                        sHours = "0" + sHours;
                    if (minutes < 10)
                        sMinutes = "0" + sMinutes;
                    var formatedtime = sHours + ":" + sMinutes + ":01";
                    return formatedtime;
                } else {
                    var formatedtime = "";
                    return formatedtime;
                }
            };

            $scope.dateformat = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }

            }

            $scope.formatserverdate = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("-");
                    var day = datePart[2];
                    var year = datePart[0];
                    var month = datePart[1];
                    return month + '/' + day + '/' + year;
                } else {
                    var datevalue = "";
                    return datevalue;
                }

            }

            $scope.formatAMPM = function (dtime) {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                
                if (isSafari) {                                         //For safari browser
                    var safaritimecheck = dtime.split(" ");
                    if (safaritimecheck[1] === "00:00:00") {
                        var localTime = "";
                        return localTime;
                    } else {
                        var dda = dtime.toString();
                        dda = dda.replace(/ /g, "T");
                        var date = new Date(dda).toUTCString(); //For date in safari format

                        var time = date.slice(17, 25);
                        // format AM PM start
                        var hours = time.slice(0, 2);
                        var minutes = time.slice(3, 5);
                        var ampm = hours >= 12 ? 'PM' : 'AM';
                        hours = hours % 12;
                        hours = hours ? hours : 12; // the hour '0' should be '12'
                        var localTime = hours + ':' + minutes + ' ' + ampm;
                        // format AM PM end 
                        return localTime;
                    }

                } else {                                              //For other browsers
                    var servertime = dtime.split(" ");
                    if (servertime[1] !== "00:00:00") {                 
                        var date = new Date(dtime);
                        var hours = date.getHours();
                        var minutes = date.getMinutes();
                        var ampm = hours >= 12 ? 'PM' : 'AM';
                        hours = hours % 12;
                        hours = hours ? hours : 12; // the hour '0' should be '12'
                        minutes = minutes < 10 ? '0' + minutes : minutes;
                        var strTime = hours + ':' + minutes + ' ' + ampm;
                        return strTime;
                    } else {
                        var strTime = "";
                        return strTime;
                    }
                }
            };

            $scope.datetimestring = function (ddate) {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari) {
                    if (ddate !== '0000-00-00 00:00:00') {
                        var safaritimecheck = ddate.split(" ");
                        if (safaritimecheck[1] === "00:00:00") {
                            var dda = ddate.toString();
                            dda = dda.replace(/ /g, "T");
                            var time = new Date(dda).toUTCString();
                            var dateonly = time.toString();
                            var fulldateonly = dateonly.replace(/GMT.*/g, "");
                            var localdate = fulldateonly.slice(0, -9);
                            return localdate;
                        } else {
                            var dda = ddate.toString();
                            dda = dda.replace(/ /g, "T");
                            var date = new Date(dda).toUTCString(); //For date in UTC format (without GMT) for safari
                            var datealone = date.slice(0, 12);
                            var time = date.slice(17, 25);
                            // format AM PM start
                            var hours = time.slice(0, 2);
                            var minutes = time.slice(3, 5);
                            var ampm = hours >= 12 ? 'PM' : 'AM';
                            hours = hours % 12;
                            hours = hours ? hours : 12; // the hour '0' should be '12'
                            var localTime = hours + ':' + minutes + ' ' + ampm;
                            var localdatetime = datealone + "  " + localTime;
                            return localdatetime;
                        }
                    } else {
                        var localdatetime = "";
                        return localdatetime;
                    }
                } else {
                    if (ddate !== '0000-00-00 00:00:00') {
                        var timecheck = ddate.split(" ");
                        if (timecheck[1] === "00:00:00") {
                            var ddateonly = timecheck[0] + 'T00:00:00+00:00';
                            var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                            var localdate = $filter('date')(newDate, 'EEE, MMM d');
                            return localdate;
                        } else {
                            var ddateonly = timecheck[0] + 'T' + timecheck[1] + '+00:00';
                            var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                            var localdatetime = $filter('date')(newDate, 'EEE, MMM d h:mm a');
                            return localdatetime;
                        }
                    } else {
                        var localdatetime = "";
                        return localdatetime;
                    }
                }
            };


            $scope.dateonly = function (d) {
                if (d !== '0000-00-00 00:00:00') {
                    var datealone = d.split(" ")[0];
                    return datealone;
                } else {
                    var datealone = "";
                    return datealone;
                }

            }
            
            
            
            
            $scope.CompareTime = function (stime, etime) {
//                alert('Im in Compare time');
                var regex = /^([0-1][0-9])\:[0-5][0-9]\s*[ap]m$/i;
                var smatch = stime.match(regex);
                var ematch = etime.match(regex);
                if (smatch && ematch) {
                    var shour = parseInt(smatch[1]);
                    var ehour = parseInt(ematch[1]);
                    var smin = parseInt(stime.split(':')[1].substring(0, 2));
                    var sfrmt = stime.split(':')[1].substring(2, 5).trim();
                    var emin = parseInt(etime.split(':')[1].substring(0, 2));
                    var efrmt = etime.split(':')[1].substring(2, 5).trim();
//                    alert(sfrmt + ' ' + efrmt);
                    // if ( !isNaN( shour) && shour <= 11 && !isNaN( ehour) && ehour <= 11 ) { 
                    if (sfrmt == efrmt) {
                        if (shour > ehour) {
                            if (shour == 12) {
//                                    alert(shour + '>' + ehour + '::F');
                                return false;
                            }
//                            alert(shour + '>' + ehour + '::F');
                            return false;
                        } else if (shour === ehour) {
                            if (emin >= smin) {
//                                alert('H: ' + shour + '===' + ehour + ' M:' + emin + '>' + smin + '::T');
                                return true;
                            } else
                            {
//                                alert('H: ' + shour + '===' + ehour + ' M:' + emin + '>' + smin + '::F');
                                return false;
                            }
                        } else {
                            if (sfrmt == 'AM' || sfrmt == 'am' || sfrmt == 'PM' || sfrmt == 'pm') {
                                if (parseInt(ehour) == 12) {
//                                    alert(shour + '>' + ehour + '::F');
                                    return false;
                                }
                            }
//                            alert(shour + '<' + ehour + ':::T');
                            return true;
                        }
                    } else if (sfrmt == 'AM' || sfrmt == 'am') {
//                        alert(sfrmt + '<' + efrmt + '::T');
                        return true;
                    } else {
//                        alert(sfrmt + '>' + efrmt + '::F');
                        return false;
                    }
                }
//                alert('IM out in compare');
            };

            $scope.listevents = function () {
                $('html, body').animate({scrollTop:0}, 'slow');
                $timeout( function(){
                    $location.path('/manageevent');
                }, 1000 );
            };
            
            $scope.addeventinfo = function(addeventinfo){
                var msg;
                if(addeventinfo === 'single'){
                    msg = "Use this option for a single, non-repeating event.";
                }
                if(addeventinfo === 'multiple'){
                    msg = "Use this option for an event that has multiple dates such as Belt Testings, Summer Camps, Birthday Parties.";
                }
                if(addeventinfo === 'processingfee'){
                    msg = "These are merchant fees that are charged by credit card companies and the processing bank.";
                }
                if(addeventinfo === 'publicurl'){
                    msg = "<p>This URL is a web link to your event, allowing users without the app to access and register for your event.<p><p>Use this link to promote your event on Facebook, Google, or link it your website.</p>";
                }
                
                $("#failuremessageModal").modal('show');
                $("#failuremessagetitle").text('Info');
                $("#failuremessagecontent").html(msg);
            };

            $scope.multipleSubEventShow = function (evnt_status) {
                $scope.AddeventButton = false;
                $scope.childEventListView = false;
                $scope.childEventCardView = false;
                $scope.childEditFormView = false;
                $scope.IsVisible = true;
                $scope.initialmeventadd = 'Y';
                $scope.addevents(evnt_status);
            }

            $scope.mulSubEventShow = function () {
                $scope.AddeventButton = false;
                $scope.MEventName = $scope.meventstartdate = $scope.meventenddate = $scope.meventstarttime = $scope.meventendtime = "";
                $scope.MEventCost = $scope.MEventCompCost = $scope.MEventUrl = $scope.childdesc = $scope.meventcapacity = "";
                $scope.meventspaceremng = '1';
                $scope.IsVisible = true;
                $scope.resetimgcropped();
                $scope.ccroppedEventImage = "";
                $scope.cropper = {};
                $scope.cropper.sourceImage = null;
                $scope.cropper.croppedImage = null;
                document.getElementById("ceventimage").src = ''; 
                document.getElementById("ccanvasimage").value = ''; 
                $scope.childEventListView = true;
                $scope.childEventCardView = true;
                $scope.childEditFormView = false;
                $('#meventstartdate').datepicker('setStartDate', null);  
                $('#meventstartdate').datepicker('setEndDate', null);
                $('#meventenddate').datepicker('setStartDate', null);
                $('#meventenddate').datepicker('setEndDate', null);
                $('#meventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                $('#meventendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                
                $localStorage.PreviewMEventName = '';
                $localStorage.PreviewMEventDate = '';
                $localStorage.PreviewMEventEndDate = '';
                $localStorage.PreviewMEventTime = '';
                $localStorage.PreviewMEventEndTime = '';
                $localStorage.PreviewMEventCost = '';
                $localStorage.PreviewMEventCompareCost = '';
                $localStorage.PreviewMEventDesc ='';
                $localStorage.PreviewMECapacity = '';
                $localStorage.PreviewMESpaceRemaining ='1';
            }

            $scope.multipleSubEventHide = function () {
                $localStorage.preview_eventchildlist = true;
                $localStorage.PreviewMEventName = '';
                $localStorage.PreviewMEventDate = '';
                $localStorage.PreviewMEventTime = '';
                $localStorage.PreviewMEventEndDate = '';
                $localStorage.PreviewMEventEndTime = '';
                $localStorage.PreviewMEventDesc = '';
                $localStorage.PreviewMEventCost = '';
                $localStorage.PreviewMECapacity = '';
                $localStorage.PreviewMESpaceRemaining = '';
                

                $localStorage.PreviewCEventDate = '';
                $localStorage.PreviewCEventTime = '';
                $localStorage.PreviewCEventEndDate = '';
                $localStorage.PreviewCEventEndTime = '';
                $localStorage.PreviewCEventName = '';
                $localStorage.PreviewCEventDescEdit = '';
                $localStorage.PreviewCEventCost = '';
                $localStorage.PreviewCECapacity = '';
                $localStorage.PreviewCESpaceRemaining = '';
                
                $scope.IsVisible = false;
                $scope.AddeventButton = true;
                $scope.childEventListView = true;
                $scope.childEventCardView = true;
                $scope.childEditFormView = false;
                
                
                $scope.ccroppedEventImage = "";
                $scope.cropper = {};
                $scope.cropper.sourceImage = null;
                $scope.cropper.croppedImage = null;
                $scope.croppedEventImage = $scope.event_picture_url;
                $scope.cropper.sourceImage = $scope.croppedEventImage;
            }

            $scope.editchildpayment = function (childpayment, evnt_ind) {                
                $scope.childEventPaymentView = true;
                $scope.childpaymentCardView = false;
                $scope.eventpaymentformview = false;

                $scope.selectedchildpayment = evnt_ind;
                $scope.medit_event_id = childpayment.event_id;
                if( parseFloat(childpayment.event_capacity) > 0 ){
                   $scope.ceventcapacity = childpayment.event_capacity;
                } else{
                   $scope.ceventcapacity = '';
                }
                $scope.ceventspaceremng = childpayment.capacity_text;
                $scope.editChildEvent(childpayment, evnt_ind);
                $localStorage.PreviewCEProcesFeeSelection = '';
            }

            $scope.editChildEvent = function (childevnt, evnt_ind) {
                $localStorage.preview_eventchildlist = false;
                $localStorage.preview_childlisthide = true;
                $scope.AddeventButton = false;
                $scope.IsVisible = false;
                $scope.childEventListView = true;
                $scope.childEventCardView = false;
                $scope.childEditFormView = false;
//jim
                $scope.selectedchildindex = evnt_ind;

                $scope.mcevent_id = childevnt.event_id;
                $scope.mcparent_id = childevnt.parent_id;
                $scope.mcevent_type = childevnt.event_type;
                $scope.mcevent_status = childevnt.event_status;
                $scope.mcevent_title = childevnt.event_title;
                $scope.child_event_picture_url = childevnt.event_banner_img_url;
                $scope.editcroppedEventImage = childevnt.event_banner_img_url;
                $scope.cropper.sourceImage = $scope.editcroppedEventImage;
                $scope.mcevent_begin_date = $scope.formatserverdate($scope.dateonly(childevnt.event_begin_dt));
                $scope.mcevent_begin_time = $scope.formatAMPM(childevnt.event_begin_dt);
                $scope.mcevent_end_date = $scope.formatserverdate($scope.dateonly(childevnt.event_end_dt));
                $scope.mcevent_end_time = $scope.formatAMPM(childevnt.event_end_dt);
                $scope.mcevent_cost = childevnt.event_cost;
                $scope.mcevent_compare_price = childevnt.event_compare_price;
//                $scope.mcevent_more_detail_url = childevnt.event_video_detail_url;
                this.mcevent_desc = childevnt.event_desc;
                $scope.ceventcapacity = childevnt.event_capacity;
                $scope.ceventspaceremng = childevnt.capacity_text;
                $scope.showCevent = childevnt.event_show_status;
                $scope.eventCregurl = childevnt.event_url;
                
                $localStorage.childeventindex = $scope.selectedchildindex;
                $localStorage.PreviewCEventDate = $scope.dateformat($scope.mcevent_begin_date);
                $localStorage.PreviewCEventTime = $scope.mcevent_begin_time;
                $localStorage.PreviewCEventEndDate = $scope.dateformat($scope.mcevent_end_date);
                $localStorage.PreviewCEventEndTime = $scope.mcevent_end_time;
                $localStorage.PreviewCEventCost = $scope.mcevent_cost;
                $localStorage.PreviewCEventCompareCost = $scope.mcevent_compare_price;
//                $localStorage.PreviewCEventUrlEdit = $scope.mcevent_more_detail_url;
                $localStorage.PreviewCEventDescEdit =  this.mcevent_desc;
                $localStorage.PreviewCEventName = $scope.mcevent_title;
                $localStorage.PreviewCECapacity = $scope.ceventcapacity;
                $localStorage.PreviewCESpaceRemaining = $scope.ceventspaceremng;
                $localStorage.PreviewCEProcesFeeSelection = $scope.mcevent_processing_fee;
                $localStorage.PreviewCfirstedit = 'Y';

                $('.childstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
                $('.childstarttime').timepicker({
                    timeFormat: 'h:i A',
                    step: 15,
                    ignoreReadonly: true
                });

                $('.childenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
                $('.childendtime').timepicker({
                    timeFormat: 'h:i A',
                    step: 15,
                    ignoreReadonly: true
                });

                $scope.catchShowCEvent = function () {
                    $scope.showCevent = this.showCevent;
                };
                
                $scope.catchCEName = function () {
                    $localStorage.PreviewCEventName = this.mcevent_title;
                };
                
                $scope.catchCEStartDate = function () {
                    if (this.mcevent_begin_date == '') {
                        $('.childstartdate').datepicker('setEndDate', null);
//                    alert('Reset start date');
                    }
                    else{
                    $('.childenddate').datepicker('setStartDate', this.mcevent_begin_date);
                    }
                    
                    if (this.mcevent_begin_date == this.mcevent_end_date) {
                        if (this.mcevent_begin_time === '') {
                            $scope.esttime = '12:00 AM';
                        } else {
                            $scope.esttime = this.mcevent_begin_time;
                        }

                        if (this.mcevent_end_time === '') {
                            $scope.eedtime = '11:59 PM';
                        } else {
                            $scope.eedtime = this.mcevent_end_time;
                        }
                        $('.childstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': $scope.eedtime, 'timeFormat': 'h:i A'});
                        $('.childendtime').timepicker('option', {'minTime': $scope.esttime, 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                        if ($scope.CompareTime(this.mcevent_begin_time, this.mcevent_end_time) == false) {
                            this.mcevent_begin_time = '';
                        }
                    } else {
                        $('.childstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                        $('.childendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                    }
                    $localStorage.PreviewCEventDate = $scope.dateformat(this.mcevent_begin_date);
                    if (this.mcevent_begin_date === '') {
                        this.mcevent_begin_time = "";
                    }
                };
                $scope.catchCEEndDate = function () {
                    if (this.mcevent_end_date == '') {
                        $('.childstartdate').datepicker('setEndDate', null);
//                    alert('Reset start date');
                    } else {
                        $('.childstartdate').datepicker('setEndDate', this.mcevent_end_date);
//                    alert('set limit start date');
                    }
                    if (this.mcevent_begin_date === this.mcevent_end_date) {

                        if (this.mcevent_begin_time === '') {
                            $scope.esttime = '12:00 AM';
                        } else {
                            $scope.esttime = this.mcevent_begin_time;
                        }

                        if (this.mcevent_end_time === '') {
                            $scope.eedtime = '11:59 PM';
                        } else {
                            $scope.eedtime = this.mcevent_end_time;
                        }

                        $('.childstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': $scope.eedtime, 'timeFormat': 'h:i A'});
                        $('.childendtime').timepicker('option', {'minTime': $scope.esttime, 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                        if ($scope.CompareTime(this.mcevent_begin_time, this.mcevent_end_time) == false) {
                            this.mcevent_end_time = '';
                        }

                    } else {
                        $('.childstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                        $('.childendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                    }
                    $localStorage.PreviewCEventEndDate = $scope.dateformat(this.mcevent_end_date);
                    if (this.mcevent_end_date === '') {
                        this.mcevent_end_time = "";
                    }
                };
                
                $scope.catchCEStartTime = function () {
                    if (this.mcevent_begin_date == this.mcevent_end_date) {
                        $('.childendtime').timepicker('option', {'minTime': this.mcevent_begin_time, 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});


                    } else {
                        $('.childstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                        $('.childendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                    }
                    $localStorage.PreviewCEventTime = this.mcevent_begin_time;
                };

                $scope.catchCEEndTime = function () {
                    if (this.mcevent_begin_date === this.mcevent_end_date) {
                        $('.childstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': this.mcevent_end_time, 'timeFormat': 'h:i A'});

                    } else {
                        $('.childstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                        $('.childendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                    }
                    $localStorage.PreviewCEventEndTime = this.mcevent_end_time;
                };
                


                $scope.catchCECost = function () {
                    $localStorage.PreviewCEventCost = this.mcevent_cost;
                };
                $scope.catchCECompCost = function () {
                    $localStorage.PreviewCEventCompareCost = this.mcevent_compare_price;
                };
                $scope.catchCEUrl = function (ind) {
                    $('#progress-full').show();
                    if($localStorage.subscription_status === 'N' && $localStorage.upgrade_status === 'F') {
                        $("#urlexpiryModal").modal('show');
                        $("#urlexpirytitle").text('Message');
                        $('#progress-full').hide();
                        var urlid = 'cevent_more_detail_url'+ind;
                        document.getElementById(urlid).readOnly = true;
                        return false;
                    }else{
                        var urlid = 'cevent_more_detail_url'+ind;
                        document.getElementById(urlid).readOnly = false;
                        $('#progress-full').hide();
                        $localStorage.PreviewCEventUrl = this.mcevent_more_detail_url;
                    }
                };

                $scope.catchCeventDesc = function () {
                    $localStorage.PreviewCEventDescEdit = this.mcevent_desc;
                };
                
                $scope.catchCEStartDate();
                $scope.catchCEEndDate();
                $scope.catchCEStartTime();
                $scope.catchCEEndTime();               

            }


            $scope.childEvenFormtHide = function (childevnt, evnt_ind) {
                $localStorage.preview_childlisthide = false;
                $localStorage.preview_eventchildlist = true;
                this.mcevent_id = childevnt.event_id;
                this.mcparent_id = childevnt.parent_id;
                this.mcevent_type = childevnt.event_type;
                this.mcevent_status = childevnt.event_status;
                this.mcevent_title = childevnt.event_title;
                this.mcevent_begin_date = $scope.formatserverdate($scope.dateonly(childevnt.event_begin_dt));
                this.mcevent_begin_time = $scope.formatAMPM(childevnt.event_begin_dt);
                this.mcevent_end_date = $scope.formatserverdate($scope.dateonly(childevnt.event_end_dt));
                this.mcevent_end_time = $scope.formatAMPM(childevnt.event_end_dt);
                this.mcevent_cost = childevnt.event_cost;
                this.mcevent_compare_price = childevnt.event_compare_price;
                this.mcevent_more_detail_url = childevnt.event_video_detail_url;
                this.mcevent_desc = childevnt.event_desc;
                this.editcroppedEventImage = childevnt.event_banner_img_url;
                $scope.cropper.sourceImage = $scope.event_picture_url;
                this.ceventcapacity = childevnt.event_capacity;
                this.ceventspaceremng = childevnt.capacity_text;
//                this.showCevent = childevnt.event_show_status;
//                this.eventCregurl = childevnt.event_url;
                
                $localStorage.childeventindex = '';
                $localStorage.PreviewCEventDate = $scope.dateformat($scope.mcevent_begin_date);
                $localStorage.PreviewCEventTime = $scope.mcevent_begin_time;
                $localStorage.PreviewCEventEndDate = $scope.dateformat($scope.mcevent_end_date);
                $localStorage.PreviewCEventEndTime = $scope.mcevent_end_time;
                $localStorage.PreviewCEventCost = $scope.mcevent_cost;
                $localStorage.PreviewCEventCompareCost = $scope.mcevent_compare_price;
//                $localStorage.PreviewCEventUrlEdit = $scope.mcevent_more_detail_url;
                $localStorage.PreviewCECapacity = $scope.ceventcapacity;
                $localStorage.PreviewCESpaceRemaining = $scope.ceventspaceremng;
                $localStorage.PreviewCEventDescEdit =  this.mcevent_desc;
                $localStorage.PreviewCEventName = $scope.mcevent_title;
                $localStorage.currentpreviewtab = 'childevents';
                
                $scope.AddeventButton = true;
                $scope.IsVisible = false;
                $scope.childEventListView = true;
                $scope.childEventCardView = true;
                $scope.childEditFormView = false;
                $scope.selectedchildindex = "";
            }

            $scope.$on('$viewContentLoaded', function () {
                $('#eventstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
                $('#eventstarttime').timepicker({
                    timeFormat: 'h:i A',
                    step: 15,
                    ignoreReadonly: true
//                    showClose:true,
//                    showClear:true
                });
            });

            $scope.$on('$viewContentLoaded', function () {
                $('#eventenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
                $('#eventendtime').timepicker({
                    timeFormat: 'h:i A',
                    step: 15,
                    ignoreReadonly: true
//                    showClose:true,
//                    showClear:true
                });
            });
            

            $scope.catchEName = function () {
                $localStorage.PreviewEventName = $scope.EventName;
            };
            
            $scope.catchStartDate = function () {
                
                if (($scope.eventstartdate === '')){
//                    alert('Im in Empty');
                  $('#eventstartdate').datepicker('setEndDate', null);  
                }
                else{
//                    alert('Im in values');
                $('#eventenddate').datepicker('setStartDate', $scope.eventstartdate);
                }
                
                if ($scope.eventstartdate == $scope.eventenddate) {
                    if ($scope.eventstarttime === '') {
                        $scope.esttime = '12:00 AM';
                    } else {
                        $scope.esttime = $scope.eventstarttime;
                    }

                    if ($scope.eventendtime === '') {
                        $scope.eedtime = '11:59 PM';
                    } else {
                        $scope.eedtime = $scope.eventendtime;
                    }
                    $('#eventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': $scope.eedtime, 'timeFormat': 'h:i A'});
                    $('#eventendtime').timepicker('option', {'minTime': $scope.esttime, 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    if ($scope.CompareTime($scope.eventstarttime, $scope.eventendtime) == false) {
                        $scope.eventstarttime = '';
                    }
                } else {
                    $('#eventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    $('#eventendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                }
                $localStorage.PreviewEventDate = $scope.dateformat($scope.eventstartdate);
                if ($scope.eventstartdate == '') {
                    $scope.eventstarttime = "";
//                    $scope.eventenddate = '';
                }
            };
            $scope.catchEndDate = function () {
                if ($scope.eventenddate == '') {
                    $('#eventstartdate').datepicker('setEndDate', null);
//                    alert('Reset start date');
                } else {
                    $('#eventstartdate').datepicker('setEndDate', $scope.eventenddate);
//                    alert('set limit start date');
                }
                if ($scope.eventstartdate === $scope.eventenddate) {

                    if ($scope.eventstarttime === '') {
                        $scope.esttime = '12:00 AM';
                    } else {
                        $scope.esttime = $scope.eventstarttime;
                    }

                    if ($scope.eventendtime === '') {
                        $scope.eedtime = '11:59 PM';
                    } else {
                        $scope.eedtime = $scope.eventendtime;
                    }

                    $('#eventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': $scope.eedtime, 'timeFormat': 'h:i A'});
                    $('#eventendtime').timepicker('option', {'minTime': $scope.esttime, 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    if ($scope.CompareTime($scope.eventstarttime, $scope.eventendtime) == false) {
                        $scope.eventendtime = '';
                    }

                } else {
                    $('#eventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    $('#eventendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                }
                $localStorage.PreviewEventEndDate = $scope.dateformat($scope.eventenddate);
                if ($scope.eventenddate == '') {
                    $scope.eventendtime = "";
                }
            };
            $scope.catchStartTime = function () {
                if ($scope.eventstartdate == $scope.eventenddate) {
                    $('#eventendtime').timepicker('option', {'minTime': $scope.eventstarttime, 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});


                } else {
                    $('#eventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    $('#eventendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                }
                $localStorage.PreviewEventTime = $scope.eventstarttime;
            };

            $scope.catchEndTime = function () {
                if ($scope.eventstartdate === $scope.eventenddate) {
//                    alert(' End Time: Max start time - '+$scope.eventendtime);
                    $('#eventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': $scope.eventendtime, 'timeFormat': 'h:i A'});

                } else {
                    $('#eventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    $('#eventendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                }
                $localStorage.PreviewEventEndTime = $scope.eventendtime;
            };
            
            $scope.dateTimevalidationChange = function(){
                $scope.catchStartDate();
                $scope.catchEndDate();
                $scope.catchStartTime();
                $scope.catchEndTime();
            }


            $scope.catchECost = function () {
                $localStorage.PreviewEventCost = $scope.EventCost;
            };
            $scope.catchECompCost = function () {
                $localStorage.PreviewEventCompareCost = $scope.EventCompCost;
            };
            $scope.catchEUrl = function () {
                  $('#progress-full').show();
                    if($localStorage.subscription_status === 'N' && $localStorage.upgrade_status === 'F') {
                        $("#urlexpiryModal").modal('show');
                        $("#urlexpirytitle").text('Message');
                        $("#urlexpirycontent").text('Easily link URLs to your events. Upgrade your account to enable this functionality.');
                        $('#progress-full').hide();
                        document.getElementById("eventurl").readOnly = true;
                        return false;
                    }else{
                        document.getElementById("eventurl").readOnly = false;
                        $('#progress-full').hide();
                        $localStorage.PreviewEventUrl = $scope.EventUrl;
                    }
                
            };

            $scope.$watch(function () {
                return $scope.croppedEventImage;
            }, function (newVal, oldVal) {
                $localStorage.PreviewImageUrl = newVal;
            });
            
            $scope.$watch(function () {
                return $scope.multipleeventlist;
            }, function (newVal, oldVal) {
                $localStorage.PreviewChildEventList = newVal;
            });
            

            $scope.topreview = function () {
                document.getElementById('previewapp').scrollIntoView();
            };
            $scope.catcheventDesc = function () {
                $localStorage.PreviewEventDesc = $scope.singledesc;
            };
            
            $scope.catchECapacity = function () {
                $localStorage.PreviewECapacity = $scope.eventcapacity;
            };
            
            $scope.catchESpaceRemaining = function () {
                $localStorage.PreviewESpaceRemaining = $scope.eventspaceremng;
            };
            
            $scope.catchProcesFeeSelection = function () {
                    $localStorage.PreviewEProcesFeeSelection = $scope.eventprocessingfee;
            };
            
            $scope.catchProcesFeeSelection = function () {
                    $localStorage.PreviewEProcesFeeSelection = $scope.eventprocessingfee;
            };
            
            $scope.catchSingleDepositAmount = function () {
                if(parseFloat($scope.depositamnt) >= 5 || parseFloat($scope.depositamnt) == 0){
                        $scope.event_sdeposit_Amnt_status = false;
                }else{
                    $scope.event_sdeposit_Amnt_status = true;
                }
            };
            
            $scope.catchMultipleDepositAmount = function () {
                if(parseFloat($scope.mdepositamnt) >= 5 || parseFloat($scope.mdepositamnt) == 0){
                        $scope.event_mdeposit_Amnt_status = false;
                }else{
                    $scope.event_mdeposit_Amnt_status = true;
                }
            };
            
            
            $("#eventstarttime").on("dp.change", function () {
                $scope.eventstarttime = $("#eventstarttime").val();
            });

            $("#eventstartdate").on("dp.change", function () {
                $scope.eventstartdate = $("#eventstartdate").val();
            });
            $("#eventendtime").on("dp.change", function () {
                $scope.eventendtime = $("#eventendtime").val();
            });

            $("#eventenddate").on("dp.change", function () {
                $scope.eventenddate = $("#eventenddate").val();
            });

            $scope.$on('$viewContentLoaded', function () {
                $('#meventstartdate').datepicker({ 
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
                $('#meventstarttime').timepicker({
                    timeFormat: 'h:i A',
                    step: 15,
                    ignoreReadonly: true
                });
            });

            $scope.$on('$viewContentLoaded', function () {
                $('#meventenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
                $('#meventendtime').timepicker({
                    timeFormat: 'h:i A',
                    step: 15,
                    ignoreReadonly: true
                });
            });
            
            $scope.catchPEUrl = function () {
                  $('#progress-full').show();
                    if($localStorage.subscription_status === 'N' && $localStorage.upgrade_status === 'F') {
                        $("#urlexpiryModal").modal('show');
                        $("#urlexpirytitle").text('Message');
                        $("#urlexpirycontent").text('Easily link URLs to your events. Upgrade your account to enable this functionality.');
                        $('#progress-full').hide();
                        document.getElementById("eventPurl").readOnly = true;
                        return false;
                    }else{
                        document.getElementById("eventPurl").readOnly = false;
                        $('#progress-full').hide();
                        $localStorage.PreviewPEventUrl = $scope.PEventUrl;
                    }
                
            };
            
            $scope.catchPeventDesc = function () {
                $localStorage.PreviewPEventDesc = $scope.parentdesc;
            };
            
            $scope.$watch(function () {
                return $scope.ccroppedEventImage;
            }, function (newVal, oldVal) {
                $localStorage.PreviewPImageUrl = newVal;
            });
            
             $scope.$watch(function () {
                return $scope.editcroppedEventImage;
            }, function (newVal, oldVal) {
                $localStorage.PreviewEditPImageUrl = newVal;
            });

            $scope.catchMEName = function () {
                $localStorage.PreviewMEventName = $scope.MEventName;
            };
            $scope.catchMETitle = function () {
                $localStorage.PreviewMEventCategory = $scope.EventCategory;
            };
            $scope.catchMESubTitle = function () {
                $localStorage.PreviewMEventSubcategory = $scope.EventSubCategory;
            };
            
            $scope.catchMEStartDate = function () {
                if($scope.meventstartdate === ''){
                    $('#meventenddate').datepicker('setStartDate', null);
                }
                else{
                $('#meventenddate').datepicker('setStartDate', $scope.meventstartdate);
                }
                  
                if ($scope.meventstartdate == $scope.meventenddate) {
                    if ($scope.meventstarttime === '') {
                        $scope.esttime = '12:00 AM';
                    } else {
                        $scope.esttime = $scope.meventstarttime;
                    }

                    if ($scope.meventendtime === '') {
                        $scope.eedtime = '11:59 PM';
                    } else {
                        $scope.eedtime = $scope.meventendtime;
                    }
                    $('#meventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': $scope.eedtime, 'timeFormat': 'h:i A'});
                    $('#meventendtime').timepicker('option', {'minTime': $scope.esttime, 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    if ($scope.CompareTime($scope.meventstarttime, $scope.meventendtime) == false) {
                        $scope.meventstarttime = '';
                    }
                } else {
                    $('#meventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    $('#meventendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                }
                $localStorage.PreviewMEventDate = $scope.dateformat($scope.meventstartdate);
                if ($scope.meventstartdate == '') {
                    $scope.meventstarttime = "";
                }
            }; 
            $scope.catchMEEndDate = function () {
                if ($scope.meventenddate == '') {
                    $('#meventstartdate').datepicker('setEndDate', null);
//                    alert('Reset start date');
                } else {
                    $('#meventstartdate').datepicker('setEndDate', $scope.meventenddate);
//                    alert('set limit start date');
                }
                if ($scope.meventstartdate === $scope.meventenddate) {

                    if ($scope.meventstarttime === '') {
                        $scope.esttime = '12:00 AM';
                    } else {
                        $scope.esttime = $scope.meventstarttime;
                    }

                    if ($scope.meventendtime === '') {
                        $scope.eedtime = '11:59 PM';
                    } else {
                        $scope.eedtime = $scope.meventendtime;
                    }

                    $('#meventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': $scope.eedtime, 'timeFormat': 'h:i A'});
                    $('#meventendtime').timepicker('option', {'minTime': $scope.esttime, 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    if ($scope.CompareTime($scope.meventstarttime, $scope.meventendtime) == false) {
                        $scope.meventendtime = '';
                    }

                } else {
                    $('#meventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    $('#meventendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                }
                $localStorage.PreviewMEventEndDate = $scope.dateformat($scope.meventenddate);
                if ($scope.meventenddate == '') {
                    $scope.meventendtime = "";
                }
            };
            $scope.catchMEStartTime = function () {
                if ($scope.meventstartdate == $scope.meventenddate) {
                    $('#meventendtime').timepicker('option', {'minTime': $scope.meventstarttime, 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});


                } else {
                    $('#meventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    $('#meventendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                }
                $localStorage.PreviewMEventTime = $scope.meventstarttime;
            };

            $scope.catchMEEndTime = function () {
                if ($scope.meventstartdate === $scope.meventenddate) {
//                    alert(' End Time: Max start time - '+$scope.eventendtime);
                    $('#meventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': $scope.meventendtime, 'timeFormat': 'h:i A'});

                } else {
                    $('#meventstarttime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});
                    $('#meventendtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:59 PM', 'timeFormat': 'h:i A'});

                }
                $localStorage.PreviewMEventEndTime = $scope.meventendtime;
            };

            $scope.catchMECost = function () {
                $localStorage.PreviewMEventCost = $scope.MEventCost;
            };
            $scope.catchMECompCost = function () {
                $localStorage.PreviewMEventCompareCost = $scope.MEventCompCost;
            };
            $scope.catchMEUrl = function () {
                  $('#progress-full').show();
                    if($localStorage.subscription_status === 'N' && $localStorage.upgrade_status === 'F') {
                        $("#urlexpiryModal").modal('show');
                        $("#urlexpirytitle").text('Message');
                        $('#progress-full').hide();
                        document.getElementById("meventurl").readOnly = true;
                        return false;
                    }else{
                       document.getElementById("meventurl").readOnly = false;
                       $('#progress-full').hide();
                       $localStorage.PreviewMEventUrl = $scope.MEventUrl;
                    }
                
            };
            
            $scope.catchMeventDesc = function () {
                $localStorage.PreviewMEventDesc = $scope.singledesc;
            };
            
            $scope.catchMECapacity = function () {
                $localStorage.PreviewMECapacity = $scope.meventcapacity; 
            };
            
            $scope.catchMESpaceRemaining = function () {
                $localStorage.PreviewMESpaceRemaining = $scope.meventspaceremng;
            };
            
            $scope.catchCECapacity = function () {
                $localStorage.PreviewCECapacity = this.ceventcapacity;
            };
            
            $scope.catchCESpaceRemaining = function () {
                $localStorage.PreviewCESpaceRemaining = this.ceventspaceremng;
            };
            
            $scope.catchCProcesFeeSelection = function () {
                    $localStorage.PreviewCEProcesFeeSelection = $scope.ceventprocessingfee;
            };
            

            $("#meventstarttime").on("dp.change", function () {
                $scope.meventstarttime = $("#meventstarttime").val();
            });

            $("#meventstartdate").on("dp.change", function () {
                $scope.meventstartdate = $("#meventstartdate").val();
            });
            $("#meventendtime").on("dp.change", function () {
                $scope.meventendtime = $("#meventendtime").val();
            });

            $("#meventenddate").on("dp.change", function () {
                $scope.meventenddate = $("#meventenddate").val();
            });


            $scope.$on('$viewContentLoaded', function () {
                $('#paymntstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
            });

            $scope.$on('$viewContentLoaded', function () {
                $('#mpaymntstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
            });

            $("#paymntstartdate").on("dp.change", function () {
                $scope.paymntstartdate = $("#paymntstartdate").val();
            });

            $("#mpaymntstartdate").on("dp.change", function () {
                $scope.mpaymntstartdate = $("#mpaymntstartdate").val();
            });

            $scope.catchNewRegFieldText = function () {
                $localStorage.PreviewNewRegistrationField = $scope.reg_col_newvalue;
            };
            
            $scope.catchNewRegFieldMandatory = function () {
                $localStorage.PreviewNewRegFieldMandatory = $scope.checked;
            };
            
            
            $scope.catchOldRegFieldText = function () {
                $localStorage.PreviewOldRegistrationField = this.register_column_name;
            };
            
            $scope.catchOldRegFieldMandatory = function () {
                $localStorage.PreviewOldRegFieldMandatory = this.mand_checked;
            };            
            
            $scope.catchWaiverText = function () {
                $localStorage.PreviewWaiverText = $scope.waiverpolicy;                
            };
            
            $scope.htmltoPlainText = function (event) {
                event.preventDefault();
                event.stopPropagation();
                var plaintext = event.originalEvent.clipboardData.getData('text/plain');
                return document.execCommand('inserttext', false, plaintext);
            };

//radio button selection of multiple event
            $scope.Singleeventselection = function () {
                $scope.isDirty = $scope.addmultipleevent.$dirty;
                if ($scope.isDirty === true) {
                    $("#singleeventToggleModal").modal('show');
                    $("#singleeventToggletitle").text('Message');
                    $("#singleeventTogglecontent").text("Are you sure want to discard the changes? ");
                } else {
                    $scope.Singleeventopen();
                }
            };

//radio button selection of multiple event
            $scope.Multipleeventselection = function () {
                $scope.isDirty = $scope.addsingleevent.$dirty;
                if ($scope.isDirty === true) {
                    $("#multipleeventToggleModal").modal('show');
                    $("#multipleeventToggletitle").text('Message');
                    $("#multipleeventTogglecontent").text("Are you sure want to discard the changes? ");
                } else {
                    $scope.Multipleeventopen();
                }
            };

//modal confirmation cancel for single event toggling
            $scope.Singleeventcancel = function () {
                $scope.event_type = "M";
                $localStorage.PreviewEventType = "M";
            };

//modal confirmation cancel for multiple event toggling
            $scope.Multipleeventcancel = function () {
                $scope.event_type = "S";
                $localStorage.PreviewEventType = "S";
            };
            
//single event form opening
            $scope.Singleeventopen = function () {
                $scope.addsingleevent.$setPristine();
                $scope.addsingleevent.$setUntouched();
                $scope.resetimgcropped();
                $scope.croppedEventImage = "";
                $scope.cropper = {};
                $scope.cropper.sourceImage = null;
                $scope.cropper.croppedImage = null;
                $scope.singledesc = '';
                $scope.event_type = "S";
                $localStorage.PreviewEventType = "S";
                $scope.EventCategory = $scope.EventSubCategory = "";
                document.getElementById("AddSingleEvent").reset();
            };
            
//resetting form when single/multiple event toggling  
            $scope.Multipleeventopen = function () {
                $scope.addmultipleevent.$setPristine();
                $scope.addmultipleevent.$setUntouched();
                $scope.resetimgcropped();
                $scope.croppedEventImage = "";
                $scope.parentdesc = '';
                $scope.cropper = {};
                $scope.cropper.sourceImage = null;
                $scope.cropper.croppedImage = null;
                $scope.event_type = "M";
                $localStorage.PreviewEventType = "M";
                $scope.AddeventButton = true;
                $scope.EventName = $scope.eventstartdate = $scope.eventstarttime = $scope.eventenddate = $scope.eventendtime = "";
                $scope.EventCost = $scope.EventCompCost = $scope.EventUrl = "";
                document.getElementById("AddMultipleEvent").reset();

            };

            //setting up wepay environment - stage/production based on hostname 
            if($localStorage.wepay_level === 'P'){
               $scope.wepay_env = "production";
               $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
            }else if($localStorage.wepay_level === 'S'){
               $scope.wepay_env = "stage";
               $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
            }
            WePay.set_endpoint($scope.wepay_env); // stage or production
            
                                    
// Single/Multiple event processing fee calculation       
            $scope.getTotalprocessingfee = function (total_amount) { 
                $scope.process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                $scope.process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                var process_total_cost1 = parseFloat(total_amount) + (((parseFloat(total_amount) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
                var p_fee2 = (((parseFloat(process_total_cost1) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
                var process_total_cost = parseFloat(total_amount) + Math.round((+p_fee2 + +0.00001) * 100) / 100;
                return process_total_cost;               
            };

// Single/Multiple event absorb fee calculation
            $scope.getAbsorbFess = function (total_amount) {
                $scope.process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                $scope.process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                var p_fee = (((parseFloat(total_amount) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val)); 
                var process_total_cost = parseFloat(total_amount) - Math.round((+p_fee + +0.00001) * 100) / 100;
                return process_total_cost;               
            };

// Single/Multiple event details value getting from manage event listing page for editing
            if ($localStorage.eventpagetype === 'edit') {
                $localStorage.currentpage = "editevents";
                if ($localStorage.eventlocalSingledata === undefined || $localStorage.eventlocalSingledata === "") {
                    $localStorage.eventlocalSingledata = eventListDetails.getSingleEvent();                   
                    $scope.eventSingledata = $localStorage.eventlocalSingledata;
                    $localStorage.preview_eventcontent = $scope.eventSingledata[0];
                } else {
                    $scope.eventSingledata = $localStorage.eventlocalSingledata;
                    $localStorage.preview_eventcontent = $scope.eventSingledata[0];
                }

                if ($scope.eventSingledata[0].event_type === 'S') {                 
                    $scope.eventadded = true;
                    $scope.EventName = $scope.eventSingledata[0].event_title;
                    $scope.single_eventid = $scope.eventSingledata[0].event_id;
                    $scope.event_status = $scope.eventSingledata[0].event_status;
                    $scope.event_type = $scope.eventSingledata[0].event_type;
                    $scope.showevent =  $scope.eventSingledata[0].event_show_status;
                    if ($scope.eventSingledata[0].event_begin_dt === '0000-00-00 00:00:00') {
                        $scope.eventstartdate = "";
                        $scope.eventstarttime = "";
                    } else {
                        $scope.eventstartdate = $scope.formatserverdate($scope.eventSingledata[0].event_begin_dt.split(" ")[0]);
                        if ($scope.eventSingledata[0].event_begin_dt.split(" ")[1] === "00:00:00") {
                            $scope.eventstarttime = "";
                        } else {
                            $scope.eventstarttime = $scope.formatAMPM($scope.eventSingledata[0].event_begin_dt);
                        }
                    }
                    if ($scope.eventSingledata[0].event_end_dt === '0000-00-00 00:00:00') {
                        $scope.eventenddate = "";
                        $scope.eventendtime = "";
                    } else {
                        $scope.eventenddate = $scope.formatserverdate($scope.eventSingledata[0].event_end_dt.split(" ")[0]);
                        if ($scope.eventSingledata[0].event_end_dt.split(" ")[1] === "00:00:00") {
                            $scope.eventendtime = "";
                        } else {
                            $scope.eventendtime = $scope.formatAMPM($scope.eventSingledata[0].event_end_dt);
                        }
                    }
                    $scope.dateTimevalidationChange();
                    $scope.EventCost = $scope.eventSingledata[0].event_cost;
                    $scope.EventCompCost = $scope.eventSingledata[0].event_compare_price;
                    $scope.EventUrl = $scope.eventSingledata[0].event_video_detail_url;
                    $scope.event_picture_url = $scope.eventSingledata[0].event_banner_img_url;
                    $scope.croppedEventImage = $scope.eventSingledata[0].event_banner_img_url;
                    $scope.cropper.sourceImage = $scope.croppedEventImage;
                    $scope.singledesc = $scope.eventSingledata[0].event_desc;
                    $scope.Eventregurl = $scope.eventSingledata[0].event_url;
                    if($scope.eventSingledata[0].waiver_policies === null){ 
                         $scope.waiverpolicy = '';
                    } else{
                        $scope.waiverpolicy = $scope.eventSingledata[0].waiver_policies;
                    }
//                  
                    $scope.singlepaymentview = true;
//                    if(parseFloat($scope.eventSingledata[0].event_capacity) > 0){
                        $scope.eventcapacity = $scope.eventSingledata[0].event_capacity;
//                    }else{
//                         $scope.eventcapacity = '';
//                    }
                    $scope.eventspaceremng = $scope.eventSingledata[0].capacity_text;
                    $scope.eventprocessingfee = $scope.eventSingledata[0].processing_fees;                    
                    $scope.eventnet_sales = $scope.eventSingledata[0].net_sales;

                    if ($scope.eventSingledata[0].event_recurring_payment_flag === 'Y') {
                        $scope.recurringschecked = true;
                        $scope.recurringsinglepayment = 'Y';
                        $scope.eventorderover = $scope.eventSingledata[0].total_order_text;
                        $scope.eventquantityover = $scope.eventSingledata[0].total_quantity_text;
                        $scope.depositamnt = $scope.eventSingledata[0].deposit_amount;
                        $scope.noofpayment = $scope.eventSingledata[0].number_of_payments;
                        $scope.spayment_startdate_type = $scope.eventSingledata[0].payment_startdate_type;
                        $scope.paymntstartdate = $scope.formatserverdate($scope.eventSingledata[0].payment_startdate);
                        $scope.paymentfreq = $scope.eventSingledata[0].payment_frequency;

                    } else if ($scope.eventSingledata[0].event_recurring_payment_flag === 'N') {
                        $scope.recurringschecked = false;
                        $scope.recurringsinglepayment = 'N';
                        $scope.eventorderover = "";
                        $scope.eventquantityover = "";
                        $scope.depositamnt = "";
                        $scope.noofpayment = "";
                        $scope.paymntstartdate = "";
                        $scope.paymentfreq = "1";
                    }

                    if ($scope.eventSingledata[0].event_onetime_payment_flag === 'Y') {
                        $scope.onetimeschecked = true;
                        $scope.onetimesinglepayment = 'Y';
                    } else if ($scope.eventSingledata[0].event_onetime_payment_flag === 'N') {
                        $scope.onetimeschecked = false;
                        $scope.onetimesinglepayment = 'N';
                    }

                    if ($scope.eventSingledata[0].discount.length > 0) {
                        $scope.singlediscountList = $scope.eventSingledata[0].discount;
                        $scope.singlediscountListView = true;
                        $scope.singlediscountView = true;
                        $scope.singlediscountEditView = false;
                    } else {
                        $scope.singlediscountList = "";
                        $scope.singlediscountListView = false;
                        $scope.singlediscountView = false;
                        $scope.singlediscountEditView = false;
                    }
                    $scope.selecteddiscountindex = "";
                    $scope.selectedeventlisttype = $scope.eventSingledata[0].list_type;
                    $scope.registration_columns = $scope.eventSingledata[0].reg_columns;
                    $localStorage.PreviewEventRegcols = $scope.registration_columns;
                    if($scope.registration_columns.length >= 10) {
                        $scope.regfieldexceeds = true;
                    } else {
                        $scope.regfieldexceeds = false;
                    }
                    $scope.selectedregcolindex = "";
                    
                } else if ($scope.eventSingledata[0].event_type === 'M') {
                    $scope.IsVisible = false;
                    $scope.AddeventButton = true;
                    $scope.childEditFormView = false;
                    $scope.selectedchildindex = "";
                    $scope.eventadded = true;
                    $scope.event_type = $scope.eventSingledata[0].event_type;
                    $scope.parent_event_status = $scope.eventSingledata[0].event_status;
                    $scope.multiple_parent_id = $scope.eventSingledata[0].event_id;
                    $scope.EventCategory = $scope.eventSingledata[0].event_title;
                    $scope.EventSubCategory = $scope.eventSingledata[0].event_category_subtitle;
                    $scope.event_picture_url = $scope.eventSingledata[0].event_banner_img_url;
                    $scope.croppedEventImage = $scope.eventSingledata[0].event_banner_img_url;
                    $scope.cropper.sourceImage = $scope.croppedEventImage;
                    $scope.PEventUrl = $scope.eventSingledata[0].event_video_detail_url;
                    $scope.parentdesc = $scope.eventSingledata[0].event_desc;
                    $scope.EventPregurl = $scope.eventSingledata[0].event_url;
                    $scope.showPevent =  $scope.eventSingledata[0].event_show_status;
                    
                    if($scope.eventSingledata[0].waiver_policies === null){ 
                         $scope.waiverpolicy = '';
                    } else{
                        $scope.waiverpolicy = $scope.eventSingledata[0].waiver_policies;
                    }
                    $scope.ceventprocessingfee = $scope.eventSingledata[0].processing_fees;
                    $scope.multiplepaymentview = true;
                    if ($scope.eventSingledata[0].child_events.length > 0) {
                        $scope.multipleeventlist = $scope.eventSingledata[0].child_events;
                        $scope.childEventListView = true;
                        $scope.childEventCardView = true;
                        $scope.childEventPaymentView = true;
                        $scope.childpaymentCardView = true;
                        $scope.eventpaymentformview = false;
                    } else {
                        $scope.multipleeventlist = "";
                        $scope.childEventListView = false;
                        $scope.childEventCardView = false;
                        $scope.childEventPaymentView = false;
                        $scope.childpaymentCardView = false;
                        $scope.eventpaymentformview = false;
                    }

                    if ($scope.eventSingledata[0].event_recurring_payment_flag === 'Y') {
                        $scope.recurringmchecked = true;
                        $scope.recurringmultiplepayment = 'Y';
                        $scope.meventorderover = $scope.eventSingledata[0].total_order_text;
                        $scope.meventquantityover = $scope.eventSingledata[0].total_quantity_text;
                        $scope.mdepositamnt = $scope.eventSingledata[0].deposit_amount;
                        $scope.mnoofpayment = $scope.eventSingledata[0].number_of_payments;
                        $scope.mpayment_startdate_type = $scope.eventSingledata[0].payment_startdate_type;
                        $scope.mpaymntstartdate = $scope.formatserverdate($scope.eventSingledata[0].payment_startdate);
                        $scope.mpaymentfreq = $scope.eventSingledata[0].payment_frequency;

                    } else if ($scope.eventSingledata[0].event_recurring_payment_flag === 'N') {
                        $scope.recurringmchecked = false;
                        $scope.recurringmultiplepayment = 'N';
                        $scope.meventorderover = "";
                        $scope.meventquantityover = "";
                        $scope.mdepositamnt = "";
                        $scope.mnoofpayment = "";
                        $scope.mpaymntstartdate = "";
                        $scope.mpaymentfreq = "1";
                    }

                    if ($scope.eventSingledata[0].event_onetime_payment_flag === 'Y') {
                        $scope.onetimemchecked = true;
                        $scope.onetimemultiplepayment = 'Y';
                    } else if ($scope.eventSingledata[0].event_onetime_payment_flag === 'N') {
                        $scope.onetimemchecked = false;
                        $scope.onetimemultiplepayment = 'N';
                    }
                    
                    if ($scope.eventSingledata[0].discount.length > 0) {
                        $scope.multiplediscountList = $scope.eventSingledata[0].discount;
                        $scope.multiplediscountListView = true;
                        $scope.multiplediscountView = true;
                        $scope.multiplediscountEditView = false;
                    } else {
                        $scope.multiplediscountList = "";
                        $scope.multiplediscountListView = false;
                        $scope.multiplediscountView = false;
                        $scope.multiplediscountEditView = false;
                    }
                    
                    $scope.mcevent_processing_fee = $scope.eventSingledata[0].processing_fees;
                    $scope.selectedmdiscountindex = "";
                    $scope.cdiscountcode = $scope.cdiscountamount = "";
                    $scope.mdiscnt_type = 'V';
                    $scope.multiplePaymentUpdateButton = false;   //not used

                    $scope.selectedeventlisttype = $scope.eventSingledata[0].list_type;
                    $scope.registration_columns = $scope.eventSingledata[0].reg_columns;
                    $localStorage.PreviewEventRegcols = $scope.registration_columns;
                    if($scope.registration_columns.length >= 10) {
                        $scope.regfieldexceeds = true;
                    } else {
                        $scope.regfieldexceeds = false;
                    }
                    $scope.selectedregcolindex = "";
                }
            }

// Single/Multiple event details value maintaining and setting when reload the page after event addition
            if ($localStorage.isAddedEventDetails === 'Y') {
                $scope.eventdatadetails = $localStorage.event_details;
                $localStorage.preview_eventcontent = $scope.eventdatadetails;

                if ($scope.eventdatadetails.event_type === 'S') {                  
                    $scope.eventadded = true;
                    $scope.EventName = $scope.eventdatadetails.event_title;
                    $scope.single_eventid = $scope.eventdatadetails.event_id;
                    $scope.event_status = $scope.eventdatadetails.event_status;
                    $scope.event_type = $scope.eventdatadetails.event_type;
                    if ($scope.eventdatadetails.event_begin_dt === '0000-00-00 00:00:00') {
                        $scope.eventstartdate = "";
                        $scope.eventstarttime = "";
                    } else {
                        $scope.eventstartdate = $scope.formatserverdate($scope.eventdatadetails.event_begin_dt.split(" ")[0]);
                        if ($scope.eventdatadetails.event_begin_dt.split(" ")[1] === "00:00:00") {
                            $scope.eventstarttime = "";
                        } else {
                            $scope.eventstarttime = $scope.formatAMPM($scope.eventdatadetails.event_begin_dt);
                        }
                    }
                    if ($scope.eventdatadetails.event_end_dt === '0000-00-00 00:00:00') {
                        $scope.eventenddate = "";
                        $scope.eventendtime = "";
                    } else {
                        $scope.eventenddate = $scope.formatserverdate($scope.eventdatadetails.event_end_dt.split(" ")[0]);
                        if ($scope.eventdatadetails.event_end_dt.split(" ")[1] === "00:00:00") {
                            $scope.eventendtime = "";
                        } else {
                            $scope.eventendtime = $scope.formatAMPM($scope.eventdatadetails.event_end_dt);
                        }
                    }
                    $scope.dateTimevalidationChange();
                    $scope.EventCost = $scope.eventdatadetails.event_cost;
                    $scope.EventCompCost = $scope.eventdatadetails.event_compare_price;
                    $scope.EventUrl = $scope.eventdatadetails.event_video_detail_url;
                    $scope.event_picture_url = $scope.eventdatadetails.event_banner_img_url;
                    $scope.croppedEventImage = $scope.eventdatadetails.event_banner_img_url;
                    $scope.cropper.sourceImage = $scope.croppedEventImage;
                    $scope.singledesc = $scope.eventdatadetails.event_desc;
                    $scope.Eventregurl = $scope.eventdatadetails.event_url;
                    if($scope.eventdatadetails.waiver_policies === null){ 
                         $scope.waiverpolicy = '';
                    } else{
                        $scope.waiverpolicy = $scope.eventdatadetails.waiver_policies;
                    }

                    $scope.singlepaymentview = true;
//                    if(parseFloat($scope.eventdatadetails.event_capacity) > 0){
                       $scope.eventcapacity = $scope.eventdatadetails.event_capacity;
//                    }else{
//                       $scope.eventcapacity = ''; 
//                    }
                    $scope.eventspaceremng = $scope.eventdatadetails.capacity_text;
                    $scope.eventprocessingfee = $scope.eventdatadetails.processing_fees;
                    $scope.eventnet_sales = $scope.eventdatadetails.net_sales;
                    if ($scope.eventdatadetails.event_recurring_payment_flag === 'Y') {
                        $scope.recurringschecked = true;
                        $scope.recurringsinglepayment = 'Y';
                        $scope.eventorderover = $scope.eventdatadetails.total_order_text;
                        $scope.eventquantityover = $scope.eventdatadetails.total_quantity_text;
                        $scope.depositamnt = $scope.eventdatadetails.deposit_amount;
                        $scope.noofpayment = $scope.eventdatadetails.number_of_payments;
                        $scope.spayment_startdate_type = $scope.eventdatadetails.payment_startdate_type;
                        $scope.paymntstartdate = $scope.formatserverdate($scope.eventdatadetails.payment_startdate);
                        $scope.paymentfreq = $scope.eventdatadetails.payment_frequency;

                    } else if ($scope.eventdatadetails.event_recurring_payment_flag === 'N') {
                        $scope.recurringschecked = false;
                        $scope.recurringsinglepayment = 'N';
                        $scope.eventorderover = "";
                        $scope.eventquantityover = "";
                        $scope.depositamnt = "";
                        $scope.noofpayment = "";
                        $scope.paymntstartdate = "";
                        $scope.paymentfreq = "1";
                    }
                    if ($scope.eventdatadetails.discount.length > 0) {
                        $scope.singlediscountList = $scope.eventdatadetails.discount;
                        $scope.singlediscountListView = true;
                        $scope.singlediscountView = true;
                        $scope.singlediscountEditView = false;
                    } else {
                        $scope.singlediscountList = "";
                        $scope.singlediscountListView = false;
                        $scope.singlediscountView = false;
                        $scope.singlediscountEditView = false;
                    }
                    $scope.selecteddiscountindex = "";
                    $scope.selectedeventlisttype = $scope.eventdatadetails.list_type;
                    
                    $scope.registration_columns = $scope.eventdatadetails.reg_columns;
                    $localStorage.PreviewEventRegcols = $scope.registration_columns;
                    if($scope.registration_columns.length >= 10) {
                        $scope.regfieldexceeds = true;
                    } else {
                        $scope.regfieldexceeds = false;
                    }
                    $scope.selectedregcolindex = "";

                } else if ($scope.eventdatadetails.event_type === 'M') {

                    $scope.IsVisible = false;
                    $scope.AddeventButton = true;
                    $scope.childEditFormView = false;
                    $scope.selectedchildindex = "";
                    $scope.eventadded = true;
                    $scope.event_type = $scope.eventdatadetails.event_type;
                    $scope.parent_event_status = $scope.eventdatadetails.event_status;
                    $scope.multiple_parent_id = $scope.eventdatadetails.event_id;
                    $scope.EventCategory = $scope.eventdatadetails.event_title;
                    $scope.EventSubCategory = $scope.eventdatadetails.event_category_subtitle;
                    $scope.event_picture_url = $scope.eventdatadetails.event_banner_img_url;
                    $scope.croppedEventImage = $scope.eventdatadetails.event_banner_img_url;
                    $scope.cropper.sourceImage = $scope.croppedEventImage;
                    $scope.PEventUrl = $scope.eventdatadetails.event_video_detail_url;
                    $scope.parentdesc = $scope.eventdatadetails.event_desc;
                    $scope.EventPregurl = $scope.eventdatadetails.event_url;
                    
                    if($scope.eventdatadetails.waiver_policies === null){ 
                         $scope.waiverpolicy = '';
                    } else{
                        $scope.waiverpolicy = $scope.eventdatadetails.waiver_policies;
                    }
                    $scope.ceventprocessingfee = $scope.eventdatadetails.processing_fees;
                    $scope.multiplepaymentview = true;
                    if ($scope.eventdatadetails.child_events.length > 0) {
                        $scope.IsVisible = false;
                        $scope.multipleeventlist = $scope.eventdatadetails.child_events;
                        $scope.childEventListView = true;
                        $scope.childEventCardView = true;
                        $scope.childEventPaymentView = true;
                        $scope.childpaymentCardView = true;
                        $scope.eventpaymentformview = false;
                    } else {
                        $scope.multipleeventlist = "";
                        $scope.childEventListView = false;
                        $scope.childEventCardView = false;
                        $scope.childEventPaymentView = false;
                        $scope.childpaymentCardView = false;
                        $scope.eventpaymentformview = false;
                    }

                    if ($scope.eventdatadetails.event_recurring_payment_flag === 'Y') {
                        $scope.recurringmchecked = true;
                        $scope.recurringmultiplepayment = 'Y';
                        $scope.meventorderover = $scope.eventdatadetails.total_order_text;
                        $scope.meventquantityover = $scope.eventdatadetails.total_quantity_text;
                        $scope.mdepositamnt = $scope.eventdatadetails.deposit_amount;
                        $scope.mnoofpayment = $scope.eventdatadetails.number_of_payments;
                        $scope.mpayment_startdate_type = $scope.eventdatadetails.payment_startdate_type;
                        $scope.mpaymntstartdate = $scope.formatserverdate($scope.eventdatadetails.payment_startdate);
                        $scope.mpaymentfreq = $scope.eventdatadetails.payment_frequency;

                    } else if ($scope.eventdatadetails.event_recurring_payment_flag === 'N') {
                        $scope.recurringmchecked = false;
                        $scope.recurringmultiplepayment = 'N';
                        $scope.meventorderover = "";
                        $scope.meventquantityover = "";
                        $scope.mdepositamnt = "";
                        $scope.mnoofpayment = "";
                        $scope.mpaymntstartdate = "";
                        $scope.mpaymentfreq = "1";
                    }

                    if ($scope.eventdatadetails.event_onetime_payment_flag === 'Y') {
                        $scope.onetimemchecked = true;
                        $scope.onetimemultiplepayment = 'Y';
                    } else if ($scope.eventdatadetails.event_onetime_payment_flag === 'N') {
                        $scope.onetimemchecked = false;
                        $scope.onetimemultiplepayment = 'N';
                    }
                    
                    if ($scope.eventdatadetails.discount.length > 0) {
                        $scope.multiplediscountList = $scope.eventdatadetails.discount;
                        $scope.multiplediscountListView = true;
                        $scope.multiplediscountView = true;
                        $scope.multiplediscountEditView = false;
                    } else {
                        $scope.multiplediscountList = "";
                        $scope.multiplediscountListView = false;
                        $scope.multiplediscountView = false;
                        $scope.multiplediscountEditView = false;
                    }
                    
                    $scope.mcevent_processing_fee = $scope.eventdatadetails.processing_fees;
                    $scope.selectedmdiscountindex = "";
                    $scope.cdiscountcode = $scope.cdiscountamount = "";
                    $scope.mdiscnt_type = 'V';
                    $scope.multiplePaymentUpdateButton = false; //not used

                    $scope.selectedeventlisttype = $scope.eventdatadetails.list_type;
                    $scope.registration_columns = $scope.eventdatadetails.reg_columns;
                    $localStorage.PreviewEventRegcols = $scope.registration_columns;
                    if($scope.registration_columns.length >= 10) {
                        $scope.regfieldexceeds = true;
                    } else {
                        $scope.regfieldexceeds = false;
                    }
                    $scope.selectedregcolindex = "";
                }
            }

// New Single/Multiple event details addition
            $scope.addevents = function (evnt_status) {
                $('#progress-full').show();
//                alert('parent id '+$scope.multiple_parent_id+' '+$scope.event_type+' '+evnt_status);

                if ($scope.event_type === "S") {

                    if (($scope.eventstartdate === undefined && $scope.eventstarttime === undefined) || ($scope.eventstartdate === undefined && $scope.eventstarttime !== undefined) || ($scope.eventstartdate === "" && $scope.eventstarttime === "") || ($scope.eventstartdate === "" && $scope.eventstarttime !== "")) {
                        $scope.gmt_eventStartDateTime = "";
                    } else if (($scope.eventstartdate !== undefined && $scope.eventstarttime === undefined) || ($scope.eventstartdate !== "" && $scope.eventstarttime === "")) {
                        $scope.gmt_eventStartDateTime = $scope.dateformat($scope.eventstartdate) + ' 00:00:00';
                    } else if (($scope.eventstartdate !== undefined && $scope.eventstarttime !== undefined) || ($scope.eventstartdate !== "" && $scope.eventstarttime !== "")) {
                        $scope.gmt_eventStartDateTime = $scope.dateformat($scope.eventstartdate) + " " + $scope.timeServerformat($scope.eventstarttime);

                    }

                    if (($scope.eventenddate === undefined && $scope.eventendtime === undefined) || ($scope.eventenddate === undefined && $scope.eventendtime !== undefined) || ($scope.eventenddate === "" && $scope.eventendtime === "") || ($scope.eventenddate === "" && $scope.eventendtime !== "")) {
                        $scope.gmt_eventEndDateTime = "";
                    } else if (($scope.eventenddate !== undefined && $scope.eventendtime === undefined) || ($scope.eventenddate !== "" && $scope.eventendtime === "")) {
                        $scope.gmt_eventEndDateTime = $scope.dateformat($scope.eventenddate) + ' 00:00:00';
                    } else if (($scope.eventenddate !== undefined && $scope.eventendtime !== undefined) || ($scope.eventenddate !== "" && $scope.eventendtime !== "")) {
                        $scope.gmt_eventEndDateTime = ($scope.dateformat($scope.eventenddate) + " " + $scope.timeServerformat($scope.eventendtime));
                    }

                    if ($scope.EventUrl === "" || $scope.EventUrl === undefined || $scope.EventUrl === null ) {
                        $scope.EventUrlformat = "";
                    } else {
                        if ($scope.EventUrl.indexOf("http://") === 0 || $scope.EventUrl.indexOf("https://") === 0) {
                            $scope.EventUrlformat = $scope.EventUrl;
                        } else {
                            $scope.EventUrlformat = 'http://' + $scope.EventUrl;
                        }
                    }

                    if ($scope.croppedEventImage === "" || $scope.croppedEventImage === undefined || $scope.croppedEventImage === null) {
                        $scope.imagetype = "";
                        $scope.event_banner_image = "";
                    } else {
                        $scope.imagetype = "png";
                        $scope.event_banner_image = $scope.croppedEventImage.substr($scope.croppedEventImage.indexOf(",") + 1);
                    }
                    
                    
                    if ($scope.EventCost === "" || $scope.EventCost === undefined) {
                        $scope.Single_EventCost = "0.00";
                    } else {
                            if ((parseFloat($scope.EventCost) >= 5) || (parseFloat($scope.EventCost) == 0)) {
                                $scope.Single_EventCost = $scope.EventCost;
                            } else {
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                $("#failuremessagetitle").text('Message');
                                $("#failuremessagecontent").text("Event Cost must be at-least "+$scope.wp_currency_symbol+"5 after applying discount code");
                                return false;
                            }
                    }


                    $scope.eventdesc = document.getElementById("singletextarea").innerHTML;
                    
                    if ($scope.eventcapacity === "" || $scope.eventcapacity === undefined) {
                        $scope.evnt_capacity = "";
                    } else {
                        $scope.evnt_capacity = $scope.eventcapacity;
                    }

                    if ($scope.eventspaceremng === "" || $scope.eventspaceremng === undefined) {
                        $scope.evnt_capacity_text = "";
                    } else {
                        $scope.evnt_capacity_text = $scope.eventspaceremng;
                    }

                    $scope.event_type = "S";
                    $scope.event_status = evnt_status;
                    $scope.parent_id = "";
                    $scope.event_name = $scope.EventName;
                    $scope.event_subcategory = "";
                    $scope.event_start_datetime = $scope.gmt_eventStartDateTime;
                    $scope.event_end_datetime = $scope.gmt_eventEndDateTime;
                    $scope.event_cost = $scope.Single_EventCost;
                    $scope.event_compare_cost = $scope.EventCompCost;
                    $scope.event_more_detail_url = $scope.EventUrlformat;
                    $scope.event_picture = $scope.event_banner_image;
                    $scope.event_picture_type = $scope.imagetype;
                    $scope.company_id = $localStorage.company_id;
                    $scope.eventdescription = $scope.eventdesc;
                    $scope.final_evnt_capacity = $scope.evnt_capacity;
                    $scope.final_evnt_capacity_text =  $scope.evnt_capacity_text;

                } else if ($scope.event_type === "M") {

                    if ($scope.EventCategory === "" || $scope.EventCategory === undefined) {
                        $scope.MulEventCategory = "";
                    } else {
                        $scope.MulEventCategory = $scope.EventCategory;
                    }

                    if ($scope.EventSubCategory === "" || $scope.EventSubCategory === undefined) {
                        $scope.MulEventSubCategory = "";
                    } else {
                        $scope.MulEventSubCategory = $scope.EventSubCategory;
                    }

                    if ($scope.croppedEventImage === "" || $scope.croppedEventImage === undefined || $scope.croppedEventImage === null) {
                        $scope.mimagetype = "";
                        $scope.mevent_banner_image = "";
                    } else {
                        $scope.mimagetype = "png";
                        $scope.mevent_banner_image = $scope.croppedEventImage.substr($scope.croppedEventImage.indexOf(",") + 1);
                    }
                     if ($scope.PEventUrl === "" || $scope.PEventUrl === undefined || $scope.PEventUrl === null ) {
                        $scope.PEventUrlformat = "";
                    } else {
                        if ($scope.PEventUrl.indexOf("http://") === 0 || $scope.PEventUrl.indexOf("https://") === 0) {
                            $scope.PEventUrlformat = $scope.PEventUrl;
                        } else {
                            $scope.PEventUrlformat = 'http://' + $scope.PEventUrl;
                        }
                    }

                    if ($scope.MEventName === "" || $scope.MEventName === undefined) {
                        $scope.MulEventName = "";
                    } else {
                        $scope.MulEventName = $scope.MEventName;
                    }
                    if ($scope.ccroppedEventImage === "" || $scope.ccroppedEventImage === undefined || $scope.ccroppedEventImage === null) {
                        $scope.ccimagetype = "";
                        $scope.ccevent_banner_image = "";
                    } else {
                        $scope.ccimagetype = "png";
                        $scope.ccevent_banner_image = $scope.ccroppedEventImage.substr($scope.ccroppedEventImage.indexOf(",") + 1);
                    }

                    if (($scope.meventstartdate === undefined && $scope.meventstarttime === undefined) || ($scope.meventstartdate === undefined && $scope.meventstarttime !== undefined) || ($scope.meventstartdate === "" && $scope.meventstarttime === "") || ($scope.meventstartdate === "" && $scope.meventstarttime !== "")) {
                        $scope.gmt_MeventStartDateTime = "";
                    } else if (($scope.meventstartdate !== undefined && $scope.meventstarttime === undefined) || ($scope.meventstartdate !== "" && $scope.meventstarttime === "")) {
                        $scope.gmt_MeventStartDateTime = $scope.dateformat($scope.meventstartdate) + ' 00:00:00';

                    } else if (($scope.meventstartdate !== undefined && $scope.meventstarttime !== undefined) || ($scope.meventstartdate !== "" && $scope.meventstarttime !== "")) {
                        $scope.gmt_MeventStartDateTime = ($scope.dateformat($scope.meventstartdate) + " " + $scope.timeServerformat($scope.meventstarttime));
                    }

                    if (($scope.meventenddate === undefined && $scope.meventendtime === undefined) || ($scope.meventenddate === undefined && $scope.meventendtime !== undefined) || ($scope.meventenddate === "" && $scope.meventendtime !== "")) {
                        $scope.gmt_MeventEndDateTime = "";
                    } else if (($scope.meventenddate !== undefined && $scope.meventendtime === undefined) || ($scope.meventenddate !== "" && $scope.meventendtime === "")) {
                        $scope.gmt_MeventEndDateTime = $scope.dateformat($scope.meventenddate) + ' 00:00:00';
                    } else if (($scope.meventenddate !== undefined && $scope.meventendtime !== undefined) || ($scope.meventenddate !== "" && $scope.meventendtime !== "")) {
                        $scope.gmt_MeventEndDateTime = ($scope.dateformat($scope.meventenddate) + " " + $scope.timeServerformat($scope.meventendtime));
                    }

                    if ($scope.MEventCost === "" || $scope.MEventCost === undefined  || (parseFloat($scope.MEventCost) == 0)) {
                        $scope.MulEventCost = "0.00";
                    } else if($scope.multiplediscountList.length > 0 && $scope.multiplediscountList !== undefined && $scope.multiplediscountList !== '' && $scope.multiplediscountList !== null) {
                            for (var i = 0; i < $scope.multiplediscountList.length; i++) {
                                if ($scope.multiplediscountList[i].discount_type === 'V') {
                                    var check_dollar_discount = parseFloat($scope.MEventCost) - parseFloat($scope.multiplediscountList[i].discount_amount);
                                    if (parseFloat('5') < parseFloat(check_dollar_discount) || parseFloat('0') == parseFloat(check_dollar_discount) ) {
                                            $scope.MulEventCost = $scope.MEventCost;                                        
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Event Cost must be at-least "+$scope.wp_currency_symbol+"5 when discount value is applied");
                                        return false;
                                    }
                                } else if ($scope.multiplediscountList[i].discount_type === 'P') {
                                    var check_percent_discount = parseFloat($scope.MEventCost) - ((parseFloat($scope.MEventCost) * parseFloat($scope.multiplediscountList[i].discount_amount)) / parseFloat(100));
                                    if (parseFloat('5') < parseFloat(check_percent_discount) || parseFloat('0') == parseFloat(check_percent_discount)) {
                                            $scope.MulEventCost = $scope.MEventCost;
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Event Cost must be at-least "+$scope.wp_currency_symbol+"5 when discount percentage is applied");
                                        return false;
                                    }
                                }
                            }

                        } else {
                            if (parseFloat($scope.MEventCost) >= 5) {
                                $scope.MulEventCost = $scope.MEventCost;
                            } else {
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                $("#failuremessagetitle").text('Message');
                                $("#failuremessagecontent").text("Event Cost must be at-least "+$scope.wp_currency_symbol+"5");
                                return false;
                            }
                        }

                    if ($scope.MEventCompCost === "" || $scope.MEventCompCost === undefined) {
                        $scope.MulCmpEventCost = "";
                    } else {
                        $scope.MulCmpEventCost = $scope.MEventCompCost;
                    }


                    if ($scope.MEventUrl === "" || $scope.MEventUrl === undefined || $scope.MEventUrl === null) {
                        $scope.MEventUrlformat = "";
                    } else {
                        if ($scope.MEventUrl.indexOf("http://") === 0 || $scope.MEventUrl.indexOf("https://") === 0) {
                            $scope.MEventUrlformat = $scope.MEventUrl;
                        } else {
                            $scope.MEventUrlformat = 'http://' + $scope.MEventUrl;
                        }
                    }                    
                    
                    if ($scope.meventcapacity === "" || $scope.meventcapacity === undefined) {
                        $scope.evnt_capacity = "";
                    } else {
                        $scope.evnt_capacity = $scope.meventcapacity;
                    }

                    if ($scope.meventspaceremng === "" || $scope.meventspaceremng === undefined) {
                        $scope.evnt_capacity_text = "";
                    } else {
                        $scope.evnt_capacity_text = $scope.meventspaceremng;
                    }


                    if ($scope.multiple_parent_id === "" && (evnt_status === "S" || evnt_status === "P")) {
//                            alert('parent event');
                        $scope.peventdesc = document.getElementById("multipletextarea").innerHTML;
                        $scope.event_type = "M";
                        $scope.event_status = evnt_status;
                        $scope.parent_id = "";
                        $scope.event_name = $scope.MulEventCategory;
                        $scope.event_subcategory = $scope.MulEventSubCategory;
                        $scope.event_start_datetime = "";
                        $scope.event_end_datetime = "";
                        $scope.event_cost = "";
                        $scope.event_compare_cost = "";
                        $scope.event_more_detail_url = $scope.PEventUrlformat;
                        $scope.event_picture = $scope.mevent_banner_image;
                        $scope.event_picture_type = $scope.mimagetype;
                        $scope.company_id = $localStorage.company_id;
                        $scope.eventdescription = $scope.peventdesc;
                        $scope.final_evnt_capacity = '';
                        $scope.final_evnt_capacity_text = '';

                    } else if ($scope.multiple_parent_id && evnt_status === "C") {
//                            alert('child event');
                        $scope.meventdesc = document.getElementById("newchildtextarea").innerHTML;
                        $scope.event_type = "C";
                        $scope.event_status = $scope.parent_event_status;
                        $scope.parent_id = $scope.multiple_parent_id;
                        $scope.event_name = $scope.MulEventName;
                        $scope.event_subcategory = "";
                        $scope.event_start_datetime = $scope.gmt_MeventStartDateTime;
                        $scope.event_end_datetime = $scope.gmt_MeventEndDateTime;
                        $scope.event_cost = $scope.MulEventCost;
                        $scope.event_compare_cost = $scope.MulCmpEventCost;
                        $scope.event_more_detail_url = $scope.MEventUrlformat;
                        $scope.event_picture =$scope.ccevent_banner_image;
                        $scope.event_picture_type = $scope.ccimagetype;
                        $scope.company_id = $localStorage.company_id;
                        $scope.eventdescription = $scope.meventdesc;
                        $scope.final_evnt_capacity = $scope.evnt_capacity;
                        $scope.final_evnt_capacity_text =  $scope.evnt_capacity_text;
                    }

                }

                $http({
                    method: 'POST',
                    url: urlservice.url + 'addevents',
                    data: {
                        "event_title": $scope.event_name,
                        "event_category_subtitle": $scope.event_subcategory,
                        "event_start_datetime": $scope.event_start_datetime,
                        "event_end_datetime": $scope.event_end_datetime,
                        "event_cost": $scope.event_cost,
                        "event_capacity": $scope.final_evnt_capacity,
                        "capacity_text": $scope.final_evnt_capacity_text,
                        "event_compare_price": $scope.event_compare_cost,
                        "event_video_detail_url": $scope.event_more_detail_url,
                        "event_banner_img_content": $scope.event_picture,
                        "event_banner_img_type": $scope.event_picture_type,
                        "event_desc": $scope.eventdescription,
                        "event_status": $scope.event_status,
                        "list_type": $scope.selectedeventlisttype,
                        "parent_id": $scope.parent_id,
                        "event_type": $scope.event_type,
                        "company_id": $scope.company_id,
                        "event_show_status":evnt_status === 'C' ? $scope.showMevent : ($scope.event_type === 'S' ? $scope.showevent :$scope.showPevent)

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8"
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedEventDetails = 'Y';
                        $localStorage.currentpage = "editevents";
                        $localStorage.preview_eventcontent = $localStorage.event_details = $scope.event_details = response.data.msg;
                        $scope.event_image_update = $scope.cevent_image_update = $scope.editcevent_image_update = 'N';
                        $('#progress-full').hide();

                        if ($localStorage.firstlogin && $localStorage.firstlogin_event === 'Y') {
                            $localStorage.firstlogin_event = 'N';
                            angular.element(document.getElementById('sidetopbar')).scope().getTutorialMode();

                        } else if ($localStorage.firstlogin_event === 'N' || $localStorage.firstlogin_event === undefined) {

                            $("#AddEventmessageModal").modal('show');
                            $("#AddEventmessagetitle").text('Success Message');
                            $("#AddEventmessagecontent").text('Event successfully Added');
                            
                            if ($scope.event_details.event_type === "S" && ($scope.event_details.parent_id === "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                                $scope.eventadded = true;
                                $scope.EventName = $scope.event_details.event_title;
                                $scope.single_eventid = $scope.event_details.event_id;
                                $scope.event_status = $scope.event_details.event_status;
                                $scope.event_type = $scope.event_details.event_type;
                                if ($scope.event_details.event_begin_dt === '0000-00-00 00:00:00') {
                                    $scope.eventstartdate = "";
                                    $scope.eventstarttime = "";
                                } else {
                                    $scope.eventstartdate = $scope.formatserverdate($scope.event_details.event_begin_dt.split(" ")[0]);
                                    if ($scope.event_details.event_begin_dt.split(" ")[1] === "00:00:00") {
                                        $scope.eventstarttime = "";
                                    } else {
                                        $scope.eventstarttime = $scope.formatAMPM($scope.event_details.event_begin_dt);
                                    }
                                }
                                if ($scope.event_details.event_end_dt === '0000-00-00 00:00:00') {
                                    $scope.eventenddate = "";
                                    $scope.eventendtime = "";
                                } else {
                                    $scope.eventenddate = $scope.formatserverdate($scope.event_details.event_end_dt.split(" ")[0]);
                                    if ($scope.event_details.event_end_dt.split(" ")[1] === "00:00:00") {
                                        $scope.eventendtime = "";
                                    } else {
                                        $scope.eventendtime = $scope.formatAMPM($scope.event_details.event_end_dt);
                                    }
                                }
                                $scope.dateTimevalidationChange();
                                $scope.EventCost = $scope.event_details.event_cost;
                                $scope.EventCompCost = $scope.event_details.event_compare_price;
                                $scope.EventUrl = $scope.event_details.event_video_detail_url;
                                $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                                $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                                $scope.cropper.sourceImage = $scope.croppedEventImage;
                               
                                $scope.singledesc = $scope.event_details.event_desc;
                                $scope.Eventregurl = $scope.event_details.event_url;
                                $scope.singlepaymentview = true;
                                $scope.discnt_type = 'V';
                                $scope.selectedeventlisttype = $scope.event_details.list_type;
                                
                                if ($scope.event_details.discount.length > 0) {
                                    $scope.singlediscountList = $scope.event_details.discount;
                                    $scope.singlediscountListView = true;
                                    $scope.singlediscountView = true;
                                    $scope.singlediscountEditView = false;
                                } else {
                                    $scope.singlediscountList = "";
                                    $scope.singlediscountListView = false;
                                    $scope.singlediscountView = false;
                                    $scope.singlediscountEditView = false;
                                }
                                
//                                if(parseFloat($scope.event_details.event_capacity) > 0){
                                    $scope.eventcapacity = $scope.event_details.event_capacity;
//                                }else{
//                                     $scope.eventcapacity = ''; 
//                                }
                                $scope.eventspaceremng = $scope.event_details.capacity_text;
                                $scope.eventprocessingfee = $scope.event_details.processing_fees;
                                $scope.registration_columns = $scope.event_details.reg_columns;
                                $localStorage.PreviewEventRegcols = $scope.registration_columns;
                                if($scope.registration_columns.length >= 10) {
                                    $scope.regfieldexceeds = true;
                                } else {
                                    $scope.regfieldexceeds = false;
                                }
                                $scope.selectedregcolindex = "";
                                

                            } else if ($scope.event_details.event_type === "M" && ($scope.event_details.parent_id !== "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {
                               
                                if ($scope.event_details.event_status === 'P') {
                                    $scope.IsVisible = false;
                                    $scope.AddeventButton = true;
                                } else if ($scope.event_details.event_status === 'S') {
                                    if ($scope.initialmeventadd === 'Y') {
                                        $scope.IsVisible = true;
                                        $scope.AddeventButton = false;
                                    } else if ($scope.initialmeventadd === 'N') {
                                        $scope.IsVisible = false;
                                        $scope.AddeventButton = true;
                                    }
                                    $scope.initialmeventadd = 'N';
                                }
                                $scope.eventadded = true;
                                $scope.event_type = $scope.event_details.event_type;
                                $scope.parent_event_status = $scope.event_details.event_status;
                                $scope.multiple_parent_id = $scope.event_details.event_id;
                                $scope.EventCategory = $scope.event_details.event_title;
                                $scope.EventSubCategory = $scope.event_details.event_category_subtitle;
                                $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                                $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                                $scope.cropper.sourceImage = $scope.croppedEventImage;
                                $scope.PEventUrl = $scope.event_details.event_video_detail_url;
                                $scope.parentdesc = $scope.event_details.event_desc;
                                $scope.EventPregurl = $scope.event_details.event_url;
                                $scope.multiplepaymentview = true;
                                $scope.ceventprocessingfee = $scope.event_details.processing_fees;
                                $scope.mcevent_processing_fee = $scope.event_details.processing_fees;
                                
                                if ($scope.event_details.child_events.length > 0) {                                    
                                    $scope.multipleSubEventHide(); 
                                    $localStorage.preview_childlisthide = false;
                                    $scope.IsVisible = false;
                                    $scope.multipleeventlist = $scope.event_details.child_events;
                                    $scope.childEventListView = true;
                                    $scope.childEventCardView = true;
                                    $scope.AddeventButton = true;
                                    $scope.childEventPaymentView = true;
                                    $scope.childpaymentCardView = true;
                                } else {
                                    $scope.multipleeventlist = "";
                                    $scope.childEventListView = false;
                                    $scope.childEventCardView = false;
                                    $scope.childEventPaymentView = false;
                                    $scope.childpaymentCardView = false;
                                }                                
                                
                                if ($scope.event_details.discount.length > 0) {
                                    $scope.multiplediscountList = $scope.event_details.discount;
                                    $scope.multiplediscountListView = true;
                                    $scope.multiplediscountView = true;
                                    $scope.multiplediscountEditView = false;
                                } else {
                                    $scope.multiplediscountList = "";
                                    $scope.multiplediscountListView = false;
                                    $scope.multiplediscountView = false;
                                    $scope.multiplediscountEditView = false;
                                }
                                
                                $scope.cdiscountcode = $scope.cdiscountamount = "";
                                $scope.mdiscnt_type = 'V';
                                $scope.selectedeventlisttype = $scope.event_details.list_type;
                                $scope.registration_columns = $scope.event_details.reg_columns;
                                $localStorage.PreviewEventRegcols = $scope.registration_columns;
                                if($scope.registration_columns.length >= 10) {
                                    $scope.regfieldexceeds = true;
                                } else {
                                    $scope.regfieldexceeds = false;
                                }
                                $scope.selectedregcolindex = "";

                            }
                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedEventDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $localStorage.isAddedEventDetails = 'N';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedEventDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };




// Single/Multiple event details updation
            $scope.updateevents = function (evnt_status, evnt_id) {
                $('#progress-full').show();
//                alert('event id '+evnt_id+' event status '+evnt_status+' event type'+$scope.event_type);

                if ($scope.event_type === "S") {
                    if (($scope.eventstartdate === undefined && $scope.eventstarttime === undefined) || ($scope.eventstartdate === undefined && $scope.eventstarttime !== undefined) || ($scope.eventstartdate === "" && $scope.eventstarttime === "") || ($scope.eventstartdate === "" && $scope.eventstarttime !== "")) {
                        $scope.gmt_eventStartDateTime = "";
                    } else if (($scope.eventstartdate !== undefined && $scope.eventstarttime === undefined) || ($scope.eventstartdate !== "" && $scope.eventstarttime === "")) {
                        $scope.gmt_eventStartDateTime = $scope.dateformat($scope.eventstartdate) + ' 00:00:00';
                    } else if (($scope.eventstartdate !== undefined && $scope.eventstarttime !== undefined) || ($scope.eventstartdate !== "" && $scope.eventstarttime !== "")) {
                        $scope.gmt_eventStartDateTime = ($scope.dateformat($scope.eventstartdate) + " " + $scope.timeServerformat($scope.eventstarttime));
                    }
                    
                   
                    if (($scope.eventenddate === undefined && $scope.eventendtime === undefined) || ($scope.eventenddate === undefined && $scope.eventendtime !== undefined) || ($scope.eventenddate === "" && $scope.eventendtime === "") || ($scope.eventenddate === "" && $scope.eventendtime !== "")) {
                        $scope.gmt_eventEndDateTime = "";
                    } else if (($scope.eventenddate !== undefined && $scope.eventendtime === undefined) || ($scope.eventenddate !== "" && $scope.eventendtime === "")) {
                        $scope.gmt_eventEndDateTime = $scope.dateformat($scope.eventenddate) + ' 00:00:00';
                    } else if (($scope.eventenddate !== undefined && $scope.eventendtime !== undefined) || ($scope.eventenddate !== "" && $scope.eventendtime !== "")) {
                        $scope.gmt_eventEndDateTime = ($scope.dateformat($scope.eventenddate) + " " + $scope.timeServerformat($scope.eventendtime));
                    }
                    
                    
                    
                    if ($scope.EventCost === "" || $scope.EventCost === undefined  || (parseFloat($scope.EventCost) == 0)) {
                        $scope.SingleEventCost = "0.00";
                    } else if ($scope.singlediscountList.length > 0 && $scope.singlediscountList !== undefined && $scope.singlediscountList !== '' && $scope.singlediscountList !== null) {
                        for (var i = 0; i < $scope.singlediscountList.length; i++) {
                            if ($scope.singlediscountList[i].discount_type === 'V') {
                                var check_dollar_discount = parseFloat($scope.EventCost) - parseFloat($scope.singlediscountList[i].discount_amount);
                                if (parseFloat('5') < parseFloat(check_dollar_discount) || parseFloat('0') == parseFloat(check_dollar_discount) ) {
                                        $scope.SingleEventCost = $scope.EventCost;
                                } else {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text("Event Cost must be at-least "+$scope.wp_currency_symbol+"5 when discount value is applied");
                                    return false;
                                }
                            } else if ($scope.singlediscountList[i].discount_type === 'P') {
                                var check_percent_discount = parseFloat($scope.EventCost) - (((parseFloat($scope.EventCost) * parseFloat($scope.singlediscountList[i].discount_amount)) / parseFloat(100)));
                                if (parseFloat('5') < parseFloat(check_percent_discount) || parseFloat('0') == parseFloat(check_percent_discount)) {
                                        $scope.SingleEventCost = $scope.EventCost;
                                } else {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text("Event Cost must be at-least "+$scope.wp_currency_symbol+"5 when discount percentage is applied");
                                    return false;
                                }
                            }
                        }

                    } else {
                        if (parseFloat($scope.EventCost) >= 5) {
                            $scope.SingleEventCost = $scope.EventCost;
                        } else {
                            $('#progress-full').hide();
                            $("#failuremessageModal").modal('show');
                            $("#failuremessagetitle").text('Message');
                            $("#failuremessagecontent").text("Event Cost must be at-least "+$scope.wp_currency_symbol+"5");
                            return false;
                        }
                    }
                                      
                    
                    if ($scope.EventUrl === "" || $scope.EventUrl === undefined || $scope.EventUrl === null) {
                        $scope.EventUrlformat = "";
                    } else {
                        if ($scope.EventUrl.indexOf("http://") === 0 || $scope.EventUrl.indexOf("https://") === 0) {
                            $scope.EventUrlformat = $scope.EventUrl;
                        } else {
                            $scope.EventUrlformat = 'http://' + $scope.EventUrl;
                        }
                    }
                    
                    if ($scope.croppedEventImage === "" || $scope.croppedEventImage === undefined || $scope.croppedEventImage === null) {
                        $scope.imagetype = "";
                        $scope.event_banner_image = "";
                    } else {
                        $scope.imagetype = "png";
                        $scope.event_banner_image = $scope.croppedEventImage.substr($scope.croppedEventImage.indexOf(",") + 1);
                    }

                    $scope.eventdesc = document.getElementById("singletextarea").innerHTML;
                    
                    if ($scope.eventcapacity === "" || $scope.eventcapacity === undefined) {
                        $scope.evnt_capacity = "";
                    } else {
                        $scope.evnt_capacity = $scope.eventcapacity;
                    }

                    if ($scope.eventspaceremng === "" || $scope.eventspaceremng === undefined) {
                        $scope.evnt_capacity_text = "";
                    } else {
                        $scope.evnt_capacity_text = $scope.eventspaceremng;
                    }
                    
                    $scope.event_type = "S";
                    $scope.event_status = evnt_status;
                    $scope.parent_id = "";
                    $scope.event_id = evnt_id;
                    $scope.event_name = $scope.EventName;
                    $scope.event_subcategory = "";
                    $scope.event_start_datetime = $scope.gmt_eventStartDateTime;
                    $scope.event_end_datetime = $scope.gmt_eventEndDateTime;
                    $scope.event_cost = $scope.SingleEventCost;
                    $scope.event_compare_cost = $scope.EventCompCost;
                    $scope.event_more_detail_url = $scope.EventUrlformat;
                    $scope.old_event_picture = $scope.event_picture_url;
                    $scope.event_picture = $scope.event_banner_image;
                    $scope.event_picture_type = $scope.imagetype;
                    $scope.image_update = $scope.event_image_update;
                    $scope.company_id = $localStorage.company_id;
                    $scope.eventdescription = $scope.eventdesc;
                    $scope.final_evnt_capacity = $scope.evnt_capacity;
                    $scope.final_evnt_capacity_text =  $scope.evnt_capacity_text;

                } else if ($scope.event_type === "M") {

                    if ($scope.EventCategory === "" || $scope.EventCategory === undefined) {
                        $scope.MulEventCategory = "";
                    } else {
                        $scope.MulEventCategory = $scope.EventCategory;
                    }

                    if ($scope.EventSubCategory === "" || $scope.EventSubCategory === undefined) {
                        $scope.MulEventSubCategory = "";
                    } else {
                        $scope.MulEventSubCategory = $scope.EventSubCategory;
                    }


                    if ($scope.croppedEventImage === "" || $scope.croppedEventImage === undefined || $scope.croppedEventImage === null) {
                        $scope.cimagetype = "";
                        $scope.cevent_banner_image = "";
                    } else {
                        $scope.cimagetype = "png";
                        $scope.cevent_banner_image = $scope.croppedEventImage.substr($scope.croppedEventImage.indexOf(",") + 1);
                    }
                    
                     if ($scope.PEventUrl === "" || $scope.PEventUrl === undefined || $scope.PEventUrl === null) {
                        $scope.PEventUrlFormat = "";
                    } else {
                        if ($scope.PEventUrl.indexOf("http://") === 0 || $scope.PEventUrl.indexOf("https://") === 0) {
                            $scope.PEventUrlFormat = $scope.PEventUrl;
                        } else {
                            $scope.PEventUrlFormat = 'http://' + $scope.PEventUrl;
                        }
                    }
                    
                    if (this.editcroppedEventImage === "" || this.editcroppedEventImage === undefined || this.editcroppedEventImage === null) {
                        $scope.childimagetype = "";
                        $scope.childevent_banner_image = "";
                    } else {
                        $scope.childimagetype = "png";
                        $scope.childevent_banner_image = this.editcroppedEventImage.substr(this.editcroppedEventImage.indexOf(",") + 1);
                    }

                    if (this.mcevent_title === "" || this.mcevent_title === undefined) {
                        $scope.ChildEventName = "";
                    } else {
                        $scope.ChildEventName = this.mcevent_title;
                    }

                    if ((this.mcevent_begin_date === undefined && this.mcevent_begin_time === undefined) || (this.mcevent_begin_date === undefined && this.mcevent_begin_time !== undefined) || (this.mcevent_begin_date === "" && this.mcevent_begin_time === "") || (this.mcevent_begin_date === "" && this.mcevent_begin_time !== "")) {
                        $scope.gmt_CeventStartDateTime = "";
                    } else if ((this.mcevent_begin_date !== undefined && this.mcevent_begin_time === undefined) || (this.mcevent_begin_date !== "" && this.mcevent_begin_time === "")) {
                        $scope.gmt_CeventStartDateTime = $scope.dateformat(this.mcevent_begin_date) + ' 00:00:00';

                    } else if ((this.mcevent_begin_date !== undefined && this.mcevent_begin_time !== undefined) || (this.mcevent_begin_date !== "" && this.mcevent_begin_time !== "")) {
                        $scope.gmt_CeventStartDateTime = ($scope.dateformat(this.mcevent_begin_date) + " " + $scope.timeServerformat(this.mcevent_begin_time));
                    }

                    if ((this.mcevent_end_date === undefined && this.mcevent_end_time === undefined) || (this.mcevent_end_date === undefined && this.mcevent_end_time !== undefined) || (this.mcevent_end_date === "" && this.mcevent_end_time === "") || (this.mcevent_end_date === "" && this.mcevent_end_time !== "")) {
                        $scope.gmt_CeventEndDateTime = "";
                    } else if ((this.mcevent_end_date !== undefined && this.mcevent_end_time === undefined) || (this.mcevent_end_date !== "" && this.mcevent_end_time === "")) {
                        $scope.gmt_CeventEndDateTime = $scope.dateformat(this.mcevent_end_date) + ' 00:00:00';
                    } else if ((this.mcevent_end_date !== undefined && this.mcevent_end_time !== undefined) || (this.mcevent_end_date !== "" && this.mcevent_end_time !== "")) {
                        $scope.gmt_CeventEndDateTime = ($scope.dateformat(this.mcevent_end_date) + " " + $scope.timeServerformat(this.mcevent_end_time));
                    }
                    
                    
                    if (this.mcevent_cost === "" || this.mcevent_cost === undefined || (parseFloat(this.mcevent_cost) == 0)) {
                        $scope.ChildEventCost = "0.00";
                    } else if ($scope.multiplediscountList.length > 0 && $scope.multiplediscountList !== undefined && $scope.multiplediscountList !== '' && $scope.multiplediscountList !== null) {
                        for (var i = 0; i < $scope.multiplediscountList.length; i++) {
                            if ($scope.multiplediscountList[i].discount_type === 'V') {
                                var check_dollar_discount = parseFloat(this.mcevent_cost) - parseFloat($scope.multiplediscountList[i].discount_amount);
                                if (parseFloat('5') < parseFloat(check_dollar_discount) || parseFloat('0') == parseFloat(check_dollar_discount) ) {
                                        $scope.ChildEventCost = this.mcevent_cost;
                                } else {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text("Event Cost must be at-least "+$scope.wp_currency_symbol+"5 when discount value is applied");
                                    return false;
                                }
                            } else if ($scope.multiplediscountList[i].discount_type === 'P') {
                                var check_percent_discount = parseFloat(this.mcevent_cost) - (parseFloat(this.mcevent_cost) * parseFloat($scope.multiplediscountList[i].discount_amount) / parseFloat(100));
                                if (parseFloat('5') < parseFloat(check_percent_discount) || parseFloat('0') == parseFloat(check_percent_discount)) {
                                        $scope.ChildEventCost = this.mcevent_cost;
                                } else {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text("Event Cost must be at-least "+$scope.wp_currency_symbol+"5 when discount percentage is applied");
                                    return false;
                                }
                            }
                        }

                    } else {
                        if (parseFloat(this.mcevent_cost) >= 5) {
                            $scope.ChildEventCost = this.mcevent_cost;
                        } else {
                            $('#progress-full').hide();
                            $("#failuremessageModal").modal('show');
                            $("#failuremessagetitle").text('Message');
                            $("#failuremessagecontent").text("Event Cost must be at-least "+$scope.wp_currency_symbol+"5");
                            return false;
                        }
                    }
                    
                    
                    if (this.mcevent_compare_price === "" || this.mcevent_compare_price === undefined) {
                        $scope.ChildCmpEventCost = "";
                    } else {
                        $scope.ChildCmpEventCost = this.mcevent_compare_price;
                    }


//                    if (this.mcevent_more_detail_url === "" || this.mcevent_more_detail_url === undefined || this.mcevent_more_detail_url === null) {
//                        $scope.CEventUrlformat = "";
//                    } else {
//                        if (this.mcevent_more_detail_url.indexOf("http://") === 0 || this.mcevent_more_detail_url.indexOf("https://") === 0) {
//                            $scope.CEventUrlformat = this.mcevent_more_detail_url;
//                        } else {
//                            $scope.CEventUrlformat = 'http://' + this.mcevent_more_detail_url;
//                        }
//                    }
                    
                    
                    if (this.ceventcapacity === "" || this.ceventcapacity === undefined) {
                        $scope.evnt_capacity = "";
                    } else {
                        $scope.evnt_capacity = this.ceventcapacity;
                    }

                    if (this.ceventspaceremng === "" || this.ceventspaceremng === undefined) {
                        $scope.evnt_capacity_text = "";
                    } else {
                        $scope.evnt_capacity_text = this.ceventspaceremng;
                    }
                    
                    if (evnt_status === "S" || evnt_status === "P") {
//                            alert('parent event');
                        $scope.event_type = "M";
                        $scope.event_status = evnt_status;
                        $scope.parent_id = "";
                        $scope.event_id = $scope.multiple_parent_id;
                        $scope.event_name = $scope.MulEventCategory;
                        $scope.event_subcategory = $scope.MulEventSubCategory;
                        $scope.event_start_datetime = "";
                        $scope.event_end_datetime = "";
                        $scope.event_cost = "";
                        $scope.event_compare_cost = "";
                        $scope.event_more_detail_url = $scope.PEventUrlFormat;
                        $scope.old_event_picture = $scope.event_picture_url;
                        $scope.image_update = $scope.event_image_update;
                        $scope.event_picture = $scope.cevent_banner_image;
                        $scope.event_picture_type = $scope.cimagetype;
                        $scope.company_id = $localStorage.company_id;
                        $scope.eventdescription = document.getElementById("multipletextarea").innerHTML;
                        $scope.final_evnt_capacity = '';
                        $scope.final_evnt_capacity_text =  '';

                    } else if ($scope.multiple_parent_id && evnt_status === "C") {
//                            alert('child event');
                        $scope.ceventdesc = this.mcevent_desc;
                        $scope.event_type = "C";
                        $scope.event_status = $scope.mcevent_status;
                        $scope.parent_id = $scope.mcparent_id;
                        $scope.event_id = evnt_id;
                        $scope.event_name = $scope.ChildEventName;
                        $scope.event_subcategory = "";
                        $scope.event_start_datetime = $scope.gmt_CeventStartDateTime;
                        $scope.event_end_datetime = $scope.gmt_CeventEndDateTime;
                        $scope.event_cost = $scope.ChildEventCost;
                        $scope.event_compare_cost = $scope.ChildCmpEventCost;
                        $scope.event_more_detail_url = '';
                        $scope.old_event_picture = $scope.child_event_picture_url;
                        $scope.event_picture = $scope.childevent_banner_image;
                        $scope.event_picture_type = $scope.childimagetype;
                        $scope.image_update = $scope.editcevent_image_update;
                        $scope.company_id = $localStorage.company_id;
                        $scope.eventdescription = $scope.ceventdesc;
                        $scope.final_evnt_capacity = $scope.evnt_capacity;
                        $scope.final_evnt_capacity_text =  $scope.evnt_capacity_text;
                    }

                }

                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateevents',
                    data: {
                        "event_title": $scope.event_name,
                        "event_category_subtitle": $scope.event_subcategory,
                        "event_start_datetime": $scope.event_start_datetime,
                        "event_end_datetime": $scope.event_end_datetime,
                        "event_cost": $scope.event_cost,
                        "event_capacity": $scope.final_evnt_capacity,
                        "capacity_text": $scope.final_evnt_capacity_text,
                        "event_compare_price": $scope.event_compare_cost,
                        "event_video_detail_url": $scope.event_more_detail_url,
                        "event_banner_img_content": $scope.event_picture,
                        "event_banner_img_type": $scope.event_picture_type,
                        "event_banner_img_url": $scope.old_event_picture,
                        "event_image_update": $scope.image_update,
                        "event_desc": $scope.eventdescription,
                        "event_status": $scope.event_status,
                        "list_type": $scope.selectedeventlisttype,
                        "parent_id": $scope.parent_id,
                        "event_id": $scope.event_id,
                        "event_type": $scope.event_type,
                        "company_id": $scope.company_id,
                        "event_show_status":evnt_status === 'C' ? $scope.showCevent : ($scope.event_type === 'S' ? $scope.showevent :$scope.showPevent)
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8"
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedEventDetails = 'Y';
                        $localStorage.currentpage = "editevents";
                        $localStorage.childeventindex = '';
                        $scope.event_image_update = $scope.cevent_image_update = $scope.editcevent_image_update = 'N';
                        $localStorage.preview_eventcontent = $localStorage.event_details = $scope.event_details = response.data.msg;                    
                        $('#progress-full').hide();

                        if ($localStorage.firstlogin && $localStorage.firstlogin_event === 'Y') {
                            $localStorage.firstlogin_event = 'N';
                            angular.element(document.getElementById('sidetopbar')).scope().getTutorialMode();

                        } else if ($localStorage.firstlogin_event === 'N' || $localStorage.firstlogin_event === undefined) {
                                                    
                            if ($scope.event_details.event_type === "S" && ($scope.event_details.parent_id === "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                                $scope.eventadded = true;
                                $scope.EventName = $scope.event_details.event_title;
                                $scope.single_eventid = $scope.event_details.event_id;
                                $scope.event_status = $scope.event_details.event_status;
                                $scope.event_type = $scope.event_details.event_type;
                                if ($scope.event_details.event_begin_dt === '0000-00-00 00:00:00') {
                                    $scope.eventstartdate = "";
                                    $scope.eventstarttime = "";
                                } else {
                                    $scope.eventstartdate = $scope.formatserverdate($scope.event_details.event_begin_dt.split(" ")[0]);
                                    if ($scope.event_details.event_begin_dt.split(" ")[1] === "00:00:00") {
                                        $scope.eventstarttime = "";
                                    } else {
                                        $scope.eventstarttime = $scope.formatAMPM($scope.event_details.event_begin_dt);
                                    }
                                }
                                if ($scope.event_details.event_end_dt === '0000-00-00 00:00:00') {
                                    $scope.eventenddate = "";
                                    $scope.eventendtime = "";
                                } else {
                                    $scope.eventenddate = $scope.formatserverdate($scope.event_details.event_end_dt.split(" ")[0]);
                                    if ($scope.event_details.event_end_dt.split(" ")[1] === "00:00:00") {
                                        $scope.eventendtime = "";
                                    } else {
                                        $scope.eventendtime = $scope.formatAMPM($scope.event_details.event_end_dt);
                                    }
                                }
                                $scope.dateTimevalidationChange();
                                $scope.EventCost = $scope.event_details.event_cost;
                                $scope.EventCompCost = $scope.event_details.event_compare_price;
                                $scope.EventUrl = $scope.event_details.event_video_detail_url;
                                $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                                $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                                $scope.cropper.sourceImage = $scope.croppedEventImage;
                                $scope.singledesc = $scope.event_details.event_desc;
                                $scope.Eventregurl = $scope.event_details.event_url;
                                if($scope.event_details.waiver_policies === null) {
                                    $scope.waiverpolicy = '';
                                } else {
                                    $scope.waiverpolicy = $scope.event_details.waiver_policies;
                                }

                                $scope.singlepaymentview = true;
//                                if(parseFloat($scope.event_details.event_capacity) > 0) {
                                    $scope.eventcapacity = $scope.event_details.event_capacity;
//                                } else {
//                                    $scope.eventcapacity = '';
//                                }
                                $scope.eventspaceremng = $scope.event_details.capacity_text;
                                $scope.eventprocessingfee = $scope.event_details.processing_fees;
                                $scope.eventnet_sales = $scope.event_details.net_sales;
                                if ($scope.event_details.event_recurring_payment_flag === 'Y') {
                                    $scope.recurringschecked = true;
                                    $scope.recurringsinglepayment = 'Y';
                                    $scope.eventorderover = $scope.event_details.total_order_text;
                                    $scope.eventquantityover = $scope.event_details.total_quantity_text;
                                    $scope.depositamnt = $scope.event_details.deposit_amount;
                                    $scope.noofpayment = $scope.event_details.number_of_payments;
                                    $scope.spayment_startdate_type = $scope.event_details.payment_startdate_type;
                                    $scope.paymntstartdate = $scope.formatserverdate($scope.event_details.payment_startdate);
                                    $scope.paymentfreq = $scope.event_details.payment_frequency;

                                } else if ($scope.event_details.event_recurring_payment_flag === 'N') {
                                    $scope.recurringschecked = false;
                                    $scope.recurringsinglepayment = 'N';
                                    $scope.eventorderover = "";
                                    $scope.eventquantityover = "";
                                    $scope.depositamnt = "";
                                    $scope.noofpayment = "";
                                    $scope.paymntstartdate = "";
                                    $scope.paymentfreq = "1";
                                }

                                if ($scope.event_details.event_onetime_payment_flag === 'Y') {
                                    $scope.onetimeschecked = true;
                                    $scope.onetimesinglepayment = 'Y';
                                } else if ($scope.event_details.event_onetime_payment_flag === 'N') {
                                    $scope.onetimeschecked = false;
                                    $scope.onetimesinglepayment = 'N';
                                }


                                if ($scope.event_details.discount.length > 0) {
                                    $scope.singlediscountList = $scope.event_details.discount;
                                    $scope.singlediscountListView = true;
                                    $scope.singlediscountView = true;
                                    $scope.singlediscountEditView = false;
                                } else {
                                    $scope.singlediscountList = "";
                                    $scope.singlediscountListView = false;
                                    $scope.singlediscountView = false;
                                    $scope.singlediscountEditView = false;
                                }
                                $scope.selecteddiscountindex = "";
                                $scope.selectedeventlisttype = $scope.event_details.list_type;
                                $scope.registration_columns = $scope.event_details.reg_columns;
                                $localStorage.PreviewEventRegcols = $scope.registration_columns;
                                if($scope.registration_columns.length >= 10){
                                   $scope.regfieldexceeds = true; 
                                }else{
                                    $scope.regfieldexceeds = false; 
                                }
                                $scope.selectedregcolindex = "";
                                $scope.showevent = $scope.event_details.event_show_status;
                            } else if ($scope.event_details.event_type === "M" && ($scope.event_details.parent_id !== "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {
                                
                                $scope.IsVisible = false;
                                $scope.AddeventButton = true;
                                $scope.eventadded = true;
                                $scope.childEditFormView = false;
                                $scope.selectedchildindex = "";
                                $scope.event_type = $scope.event_details.event_type;
                                $scope.parent_event_status = $scope.event_details.event_status;
                                $scope.multiple_parent_id = $scope.event_details.event_id;
                                $scope.EventCategory = $scope.event_details.event_title;
                                $scope.EventSubCategory = $scope.event_details.event_category_subtitle;
                                $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                                $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                                $scope.cropper.sourceImage = $scope.croppedEventImage;
                                $scope.PEventUrl = $scope.event_details.event_video_detail_url;
                                $scope.parentdesc = $scope.event_details.event_desc;
                                $scope.EventPregurl = $scope.event_details.event_url;
                                
                                if($scope.event_details.waiver_policies === null) {
                                    $scope.waiverpolicy = '';
                                } else {
                                    $scope.waiverpolicy = $scope.event_details.waiver_policies;
                                }
                                
                                $scope.ceventprocessingfee = $scope.event_details.processing_fees;
                                if ($scope.event_details.child_events.length > 0) {
                                    $localStorage.preview_eventchildlist = true;
                                    $localStorage.preview_childlisthide = false;
                                    $scope.multipleeventlist = $scope.event_details.child_events;
                                    $scope.childEventListView = true;
                                    $scope.childEventCardView = true;
                                    $scope.childEventPaymentView = true;
                                    $scope.childpaymentCardView = true;
                                    $scope.eventpaymentformview = false;
                                } else {
                                    $scope.multipleeventlist = "";
                                    $scope.childEventListView = false;
                                    $scope.childEventCardView = false;
                                    $scope.childEventPaymentView = false;
                                    $scope.childpaymentCardView = false;
                                    $scope.eventpaymentformview = false;
                                }

                                $scope.multiplepaymentview = true;
                                if ($scope.event_details.event_recurring_payment_flag === 'Y') {
                                    $scope.recurringmchecked = true;
                                    $scope.recurringmultiplepayment = 'Y';
                                    $scope.meventorderover = $scope.event_details.total_order_text;
                                    $scope.meventquantityover = $scope.event_details.total_quantity_text;
                                    $scope.mdepositamnt = $scope.event_details.deposit_amount;
                                    $scope.mnoofpayment = $scope.event_details.number_of_payments;
                                    $scope.mpayment_startdate_type = $scope.event_details.payment_startdate_type;
                                    $scope.mpaymntstartdate = $scope.formatserverdate($scope.event_details.payment_startdate);
                                    $scope.mpaymentfreq = $scope.event_details.payment_frequency;

                                } else if ($scope.event_details.event_recurring_payment_flag === 'N') {
                                    $scope.recurringmchecked = false;
                                    $scope.recurringmultiplepayment = 'N';
                                    $scope.meventorderover = "";
                                    $scope.meventquantityover = "";
                                    $scope.mdepositamnt = "";
                                    $scope.mnoofpayment = "";
                                    $scope.mpaymntstartdate = "";
                                    $scope.mpaymentfreq = "1";
                                }

                                if ($scope.event_details.event_onetime_payment_flag === 'Y') {
                                    $scope.onetimemchecked = true;
                                    $scope.onetimemultiplepayment = 'Y';
                                } else if ($scope.event_details.event_onetime_payment_flag === 'N') {
                                    $scope.onetimemchecked = false;
                                    $scope.onetimemultiplepayment = 'N';
                                }
                                
                                if ($scope.event_details.discount.length > 0) {
                                    $scope.multiplediscountList = $scope.event_details.discount;
                                    $scope.multiplediscountListView = true;
                                    $scope.multiplediscountView = true;
                                    $scope.multiplediscountEditView = false;
                                } else {
                                    $scope.multiplediscountList = "";
                                    $scope.multiplediscountListView = false;
                                    $scope.multiplediscountView = false;
                                    $scope.multiplediscountEditView = false;
                                }
                                
                                $scope.mcevent_processing_fee = $scope.event_details.processing_fees;
                                $scope.selectedmdiscountindex = "";
                                $scope.cdiscountcode = $scope.cdiscountamount = "";
                                $scope.mdiscnt_type = 'V';
                                $scope.selectedeventlisttype = $scope.event_details.list_type;
                                
                                $scope.registration_columns = $scope.event_details.reg_columns;
                                $localStorage.PreviewEventRegcols = $scope.registration_columns;
                                if($scope.registration_columns.length >= 10){
                                   $scope.regfieldexceeds = true; 
                                }else{
                                    $scope.regfieldexceeds = false; 
                                }
                                $scope.selectedregcolindex = "";
                                $scope.showPevent = $scope.event_details.event_show_status;
                            }

                            $("#AddEventmessageModal").modal('show');
                            $("#AddEventmessagetitle").text('Message');
                            $("#AddEventmessagecontent").text('Event successfully Updated');
                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedEventDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $localStorage.isAddedEventDetails = 'N';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedEventDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

         // Single/Multiple event delete confirmation modal
            $scope.deleteEvent = function (eid) {
                $scope.deleteeventid = eid;
                $("#eventDeleteModal").modal('show');
                $("#eventDeletetitle").text('Delete');
                $("#eventDeletecontent").text("Are you sure want to delete event? ");
            };

// Single/Multiple event delete cancel
            $scope.eventdeleteCancel = function () {
                console.log('cancel delete');
                $location.path('/addevent');
            };

// Single/Multiple event delete completely
            $scope.eventdeleteConfirm = function () {                
                $("#eventDeleteModal").modal('hide');
                $('#progress-full').show();
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteevents',
                    data: {
                        "event_id": $scope.deleteeventid,
                        "list_type": $scope.selectedeventlisttype,
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8'},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $('html, body').animate({scrollTop:0}, 'slow');
                        $scope.event_details = response.data.msg;
                        if ($scope.event_details.event_type === 'M') {
                            
                            $("#AddEventmessageModal").modal('show');
                            $("#AddEventmessagetitle").text('Success Message');
                            $("#AddEventmessagecontent").text('Event successfully deleted');

                            $localStorage.isAddedEventDetails = 'Y';
                            $localStorage.preview_eventcontent = $localStorage.event_details = $scope.event_details = response.data.msg;                            
                            $scope.IsVisible = false;
                            $scope.selectedchildindex = "";
                            $scope.childEditFormView = false;                            

                            if ($scope.event_details.child_events.length > 0) {
                                $localStorage.preview_eventchildlist = true;
                                $scope.multipleeventlist = $scope.event_details.child_events;
                                $scope.childEventListView = true;
                                $scope.childEventCardView = true;
                                $scope.AddeventButton = true;
                            } else {
                                $localStorage.currentpreviewtab = 'events';
                                $scope.multipleeventlist = "";
                                $scope.childEventListView = false;
                                $scope.childEventCardView = false;
                                $scope.AddeventButton = false;
                            }
                        } else {
                            $localStorage.eventdeleteredirect = 'Y'; 
                             $timeout( function(){
                                $location.path('/manageevent');
                            }, 1000 );
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $('#progress-full').hide();
                    console.log(response.data);
                    $scope.handleError(response.data);
                });

            };
            
// Multiple Event - child event copy function
            $scope.copychildevent = function (child_evnt_id) {
                $('#progress-full').show();

                $http({
                    method: 'POST',
                    url: urlservice.url + 'copyevents',
                    data: {
                        'event_id': child_evnt_id,
                        'copy_event_type': 'Child',
                        'event_template_flag': 'N',
                        "list_type": $scope.selectedeventlisttype,
                        'company_id': $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'                        
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $localStorage.isAddedEventDetails = 'Y';
                        $scope.multipleeventlist = response.data.msg;
                        $("#AddEventmessageModal").modal('show');
                        $("#AddEventmessagetitle").text('Message');
                        if($scope.selectedeventlisttype === 'past')
                            $("#AddEventmessagecontent").text('Event duplicate copy added successfully inside live');
                        else
                        $("#AddEventmessagecontent").text('Event duplicate copy added successfully');
                        $scope.IsVisible = false;
                        $scope.childEditFormView = false;
                        $scope.selectedchildindex = "";
                        $scope.childEventListView = true;
                        $scope.childEventCardView = true;
                        $scope.AddeventButton = true;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        tooltip.hide();
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        console.log(response.data);
                        tooltip.hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };


// single/multiple new discount value addition
            $scope.adddiscount = function (evnt_type, evnt_id) {
                $('#progress-full').show();
                
                if(parseFloat($scope.discountamount) <= 0){
                    $scope.discountamount = '';
                    $("#failuremessageModal").modal('show');
                    $("#failuremessagetitle").text('Message');
                    $("#failuremessagecontent").text("Discount amount should be greater than "+$scope.wp_currency_symbol+"0");
                    $('#progress-full').hide();
                    return;
                }

                if (evnt_type === "S") {
                    if ($scope.discnt_type === 'V') {
                        var dollar_value = parseFloat($scope.EventCost) - parseFloat($scope.discountamount);
                        if (parseFloat('5') < parseFloat(dollar_value) || parseFloat('0') == parseFloat(dollar_value)) {
                            $scope.discount_type = "V";
                            $scope.discount_code = $scope.discountcode;
                            $scope.discount_value = $scope.discountamount;
                            $scope.evnt_id = $scope.single_eventid;
                        } else {
                            $('#progress-full').hide();
                            $("#failuremessageModal").modal('show');
                            $("#failuremessagetitle").text('Message');
                            $("#failuremessagecontent").text("Event cost cannot be less than 5"+$scope.wp_currency_symbol+" when discount value is applied");
                            return false;
                        }

                    } else if ($scope.discnt_type === 'P') {
                         var percent_value = parseFloat($scope.EventCost) - ((parseFloat($scope.EventCost) * parseFloat($scope.discountamount)) / parseFloat(100));
                        if (parseFloat('5') < parseFloat(percent_value) || parseFloat('0') == parseFloat(percent_value)) {
                            $scope.discount_type = "P";
                            $scope.discount_code = $scope.discountcode;
                            $scope.discount_value = $scope.discountamount;
                            $scope.evnt_id = $scope.single_eventid;
                        } else {
                            $('#progress-full').hide();
                            $("#failuremessageModal").modal('show');
                            $("#failuremessagetitle").text('Message');
                            $("#failuremessagecontent").text("Event cost cannot be less than 5"+$scope.wp_currency_symbol+" when discount percentage is applied");
                            return false;

                        }

                    }


                } else if (evnt_type === "M") {

                    if ($scope.mdiscnt_type === 'V') {
                        $scope.discount_type = "V";
                        if ($scope.multipleeventlist.length > 0) {
                            for (var i = 0; i < $scope.multipleeventlist.length; i++) {                                
                                if (parseFloat($scope.multipleeventlist[i].event_cost) > 0) {
                                    var dollar_value = parseFloat($scope.multipleeventlist[i].event_cost) - parseFloat($scope.cdiscountamount);
                                    if(parseFloat('5') < parseFloat(dollar_value) || parseFloat('0') == parseFloat(dollar_value)){
                                        $scope.discount_type = "V";
                                        $scope.discount_code = $scope.cdiscountcode;
                                        $scope.discount_value = $scope.cdiscountamount;
                                        $scope.evnt_id = $scope.multiple_parent_id;
                                    }else{
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Event cost cannot be less than 5"+$scope.wp_currency_symbol+" when discount value is applied");
                                        return false;
                                    }
                                } else {
                                    $scope.discount_type = "V";
                                    $scope.discount_code = $scope.cdiscountcode;
                                    $scope.discount_value = $scope.cdiscountamount;
                                    $scope.evnt_id = $scope.multiple_parent_id;
                                }
                            }
                        } else {
                            $scope.discount_type = "V";
                            $scope.discount_code = $scope.cdiscountcode;
                            $scope.discount_value = $scope.cdiscountamount;
                            $scope.evnt_id = $scope.multiple_parent_id;
                        }
                    } else if ($scope.mdiscnt_type === 'P') {
                        $scope.discount_type = "P";
                        if ($scope.multipleeventlist.length > 0) {
                            for (var i = 0; i < $scope.multipleeventlist.length; i++) {
                                if (parseFloat($scope.multipleeventlist[i].event_cost) > 0) {
                                    var percent_value = parseFloat($scope.multipleeventlist[i].event_cost) - ((parseFloat($scope.multipleeventlist[i].event_cost) * parseFloat($scope.cdiscountamount)) / parseFloat(100));
                                    if (parseFloat('5') < parseFloat(percent_value) || parseFloat('0') == parseFloat(percent_value)){
                                        $scope.discount_type = "P";
                                        $scope.discount_code = $scope.cdiscountcode;
                                        $scope.discount_value = $scope.cdiscountamount;
                                        $scope.evnt_id = $scope.multiple_parent_id;
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Event cost cannot be less than 5"+$scope.wp_currency_symbol+" when discount percentage is applied");
                                        return false;
                                    }
                                } else {
                                    $scope.discount_type = "P";
                                    $scope.discount_code = $scope.cdiscountcode;
                                    $scope.discount_value = $scope.cdiscountamount;
                                    $scope.evnt_id = $scope.multiple_parent_id;
                                }
                            }
                        } else {
                            $scope.discount_type = "P";
                            $scope.discount_code = $scope.cdiscountcode;
                            $scope.discount_value = $scope.cdiscountamount;
                            $scope.evnt_id = $scope.multiple_parent_id;
                        }
                    }
                }

                $http({
                    method: 'POST',
                    url: urlservice.url + 'addeventsdiscount',
                    data: {
                        "event_id": $scope.evnt_id,
                        "discount_type": $scope.discount_type,
                        "discount_code": $scope.discount_code,
                        "discount_amount": $scope.discount_value,
                        "list_type": $scope.selectedeventlisttype,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8"
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedEventDetails = 'Y';
                        $localStorage.preview_eventcontent = $localStorage.event_details = $scope.event_details = response.data.msg;
                        $('#progress-full').hide();

                        $("#AddEventmessageModal").modal('show');
                        $("#AddEventmessagetitle").text('Message');
                        $("#AddEventmessagecontent").text('Discount successfully Added');

                        if ($scope.event_details.event_type === "S" && ($scope.event_details.parent_id === "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                            $scope.eventadded = true;
                            $scope.EventName = $scope.event_details.event_title;
                            $scope.single_eventid = $scope.event_details.event_id;
                            $scope.event_status = $scope.event_details.event_status;
                            $scope.event_type = $scope.event_details.event_type;
                            if ($scope.event_details.event_begin_dt === '0000-00-00 00:00:00') {
                                $scope.eventstartdate = "";
                                $scope.eventstarttime = "";
                            } else {
                                $scope.eventstartdate = $scope.formatserverdate($scope.event_details.event_begin_dt.split(" ")[0]);
                                if ($scope.event_details.event_begin_dt.split(" ")[1] === "00:00:00") {
                                    $scope.eventstarttime = "";
                                } else {
                                    $scope.eventstarttime = $scope.formatAMPM($scope.event_details.event_begin_dt);
                                }
                            }
                            if ($scope.event_details.event_end_dt === '0000-00-00 00:00:00') {
                                $scope.eventenddate = "";
                                $scope.eventendtime = "";
                            } else {
                                $scope.eventenddate = $scope.formatserverdate($scope.event_details.event_end_dt.split(" ")[0]);
                                if ($scope.event_details.event_end_dt.split(" ")[1] === "00:00:00") {
                                    $scope.eventendtime = "";
                                } else {
                                    $scope.eventendtime = $scope.formatAMPM($scope.event_details.event_end_dt);
                                }
                            }
                            $scope.dateTimevalidationChange();
                            $scope.EventCost = $scope.event_details.event_cost;
                            $scope.EventCompCost = $scope.event_details.event_compare_price;
                            $scope.EventUrl = $scope.event_details.event_video_detail_url;
                            $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                            $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                            $scope.cropper.sourceImage = $scope.croppedEventImage;
                            $scope.singledesc = $scope.event_details.event_desc;
                            $scope.Eventregurl = $scope.event_details.event_url;
                            if($scope.event_details.waiver_policies === null) {
                                $scope.waiverpolicy = '';
                            } else {
                                $scope.waiverpolicy = $scope.event_details.waiver_policies;
                            }

                            if ($scope.event_details.discount.length > 0) {
                                $scope.singlediscountList = $scope.event_details.discount;
                                $scope.singlediscountListView = true;
                                $scope.singlediscountView = true;
                                $scope.singlediscountEditView = false;
                            } else {
                                $scope.singlediscountList = "";
                                $scope.singlediscountListView = false;
                                $scope.singlediscountView = false;
                                $scope.singlediscountEditView = false;
                            }

                            $scope.discountcode = "";
                            $scope.discountamount = "";
                            $scope.discnt_type = 'V';
                            $scope.selecteddiscountindex = "";
                            $scope.singlePaymentUpdateButton = false;
                            $scope.selectedeventlisttype = $scope.event_details.list_type;

                        } else if ($scope.event_details.event_type === "M" && ($scope.event_details.parent_id !== "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                            if ($scope.event_details.event_status === 'P') {
                                $scope.IsVisible = false;
                            } else if ($scope.event_details.event_status === 'S') {
                                $scope.IsVisible = true;
                            }
                            $scope.eventadded = true;
                            $scope.event_type = $scope.event_details.event_type;
                            $scope.parent_event_status = $scope.event_details.event_status;
                            $scope.multiple_parent_id = $scope.event_details.event_id;
                            $scope.EventCategory = $scope.event_details.event_title;
                            $scope.EventSubCategory = $scope.event_details.event_category_subtitle;
                            $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                            $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                            $scope.cropper.sourceImage = $scope.croppedEventImage;
                            $scope.PEventUrl = $scope.event_details.event_video_detail_url;
                            $scope.parentdesc = $scope.event_details.event_desc;
                            $scope.EventPregurl = $scope.event_details.event_url;
                                
                            if($scope.event_details.waiver_policies === null) {
                                $scope.waiverpolicy = '';
                            } else {
                                $scope.waiverpolicy = $scope.event_details.waiver_policies;
                            }
                            
                            $scope.ceventprocessingfee = $scope.event_details.processing_fees;
                            
                            if ($scope.event_details.child_events.length > 0) {
                                $scope.multipleeventlist = $scope.event_details.child_events;
                                $scope.IsVisible = false;
                                $scope.childEventListView = true;
                                $scope.childEventCardView = true;
                                $scope.AddeventButton = true;
                            } else {
                                $scope.multipleeventlist = "";
                                $scope.childEventListView = false;
                                $scope.childEventCardView = false;
                                $scope.AddeventButton = true;
                            }
                            
                            
                            if ($scope.event_details.discount.length > 0) {
                                    $scope.multiplediscountList = $scope.event_details.discount;
                                    $scope.multiplediscountListView = true;
                                    $scope.multiplediscountView = true;
                                    $scope.multiplediscountEditView = false;
                                } else {
                                    $scope.multiplediscountList = "";
                                    $scope.multiplediscountListView = false;
                                    $scope.multiplediscountView = false;
                                    $scope.multiplediscountEditView = false;
                                }
                                
                            $scope.mcevent_processing_fee = $scope.event_details.processing_fees;
                            $scope.selectedmdiscountindex = "";
                            $scope.cdiscountcode = $scope.cdiscountamount = "";
                            $scope.mdiscnt_type = 'V';
                            $scope.multiplePaymentUpdateButton = false;
                            $scope.selectedeventlisttype = $scope.event_details.list_type;

                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedEventDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $localStorage.isAddedEventDetails = 'N';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedEventDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };


// single/multiple existing discount value updation
            $scope.updatediscount = function (evnt_type, discnt_id) {
                $('#progress-full').show();
                
                if(parseFloat(this.editdiscountamount) <= 0){
                    $("#failuremessageModal").modal('show');
                    $("#failuremessagetitle").text('Message');
                    $("#failuremessagecontent").text("Discount amount should be greater than "+$scope.wp_currency_symbol+"0");
                    $('#progress-full').hide();
                    return;
                }

                if (evnt_type === "S") {
                    if (this.edit_discnt_type === 'V') {
                         var dollar_value = parseFloat($scope.EventCost) - parseFloat(this.editdiscountamount);
                        if (parseFloat('5') < parseFloat(dollar_value) || parseFloat('0') == parseFloat(dollar_value)) {
                                $scope.discount_type = "V";
                                $scope.discount_code = this.editdiscountcode;
                                $scope.discount_value = this.editdiscountamount;
                                $scope.evnt_id = this.single_eventid;
                                $scope.event_discount_id = discnt_id;
                            } else {
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                $("#failuremessagetitle").text('Message');
                                $("#failuremessagecontent").text("Event cost cannot be less than 5"+$scope.wp_currency_symbol+" when discount value is applied");
                                return false;
                            }

                    } else if (this.edit_discnt_type === 'P') {
                         var percent_value = parseFloat($scope.EventCost) - ((parseFloat($scope.EventCost) * parseFloat(this.editdiscountamount)) / parseFloat(100));
                        if (parseFloat('5') < parseFloat(percent_value) || parseFloat('0') == parseFloat(percent_value)) {
                                $scope.discount_type = "P";
                                $scope.discount_code = this.editdiscountcode;
                                $scope.discount_value = this.editdiscountamount;
                                $scope.evnt_id = this.single_eventid;
                                $scope.event_discount_id = discnt_id;
                            } else {
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                $("#failuremessagetitle").text('Message');
                                $("#failuremessagecontent").text("Event cost cannot be less than 5"+$scope.wp_currency_symbol+" when discount percentage is applied");
                                return false;

                            }
                        }
                } else if (evnt_type === "M") {

                    if (this.medit_discnt_type === 'V') {
                        $scope.discount_type = "V";
                        if ($scope.multipleeventlist.length > 0) {
                            for (var i = 0; i < $scope.multipleeventlist.length; i++) {                                
                                if (parseFloat($scope.multipleeventlist[i].event_cost) > 0) {
                                    var dollar_value = parseFloat($scope.multipleeventlist[i].event_cost) - parseFloat(this.meditdiscountamount);
                                    if (parseFloat('5') < parseFloat(dollar_value) || parseFloat('0') == parseFloat(dollar_value)) {
                                        $scope.discount_type = "V";
                                        $scope.discount_code = this.meditdiscountcode;
                                        $scope.discount_value = this.meditdiscountamount;
                                        $scope.evnt_id = $scope.multiple_parent_id;
                                        $scope.event_discount_id = discnt_id;
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Event cost cannot be less than 5"+$scope.wp_currency_symbol+" when discount value is applied");
                                        return false;
                                    }
                            }else{
                                $scope.discount_type = "V";
                                $scope.discount_code = this.meditdiscountcode;
                                $scope.discount_value = this.meditdiscountamount;
                                $scope.evnt_id = $scope.multiple_parent_id;
                                $scope.event_discount_id = discnt_id;
                            }
                            }
                        } else {
                            $scope.discount_type = "V";
                            $scope.discount_code = this.meditdiscountcode;
                            $scope.discount_value = this.meditdiscountamount;
                            $scope.evnt_id = $scope.multiple_parent_id;
                            $scope.event_discount_id = discnt_id;
                        }
                    } else if (this.medit_discnt_type === 'P') {
                        $scope.discount_type = "P";
                        if ($scope.multipleeventlist.length > 0) {
                            for (var i = 0; i < $scope.multipleeventlist.length; i++) {
                                if(parseFloat($scope.multipleeventlist[i].event_cost) > 0){
                                    var percent_value = parseFloat($scope.multipleeventlist[i].event_cost) - ((parseFloat($scope.multipleeventlist[i].event_cost) * parseFloat(this.meditdiscountamount)) / parseFloat(100));
                                    if (parseFloat('5') < parseFloat(percent_value) || parseFloat('0') == parseFloat(percent_value)) {
                                        $scope.discount_type = "P";
                                        $scope.discount_code = this.meditdiscountcode;
                                        $scope.discount_value = this.meditdiscountamount;
                                        $scope.evnt_id = $scope.multiple_parent_id;
                                        $scope.event_discount_id = discnt_id;
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Event cost cannot be less than 5"+$scope.wp_currency_symbol+" when discount percentage is applied");
                                        return false;
                                    }
                            }else{
                                $scope.discount_type = "P";
                                $scope.discount_code = this.meditdiscountcode;
                                $scope.discount_value = this.meditdiscountamount;
                                $scope.evnt_id = $scope.multiple_parent_id;
                                $scope.event_discount_id = discnt_id;
                            }
                            }
                        } else {
                            $scope.discount_type = "P";
                            $scope.discount_code = this.meditdiscountcode;
                            $scope.discount_value = this.meditdiscountamount;
                            $scope.evnt_id = $scope.multiple_parent_id;
                            $scope.event_discount_id = discnt_id;
                        }
                    }
                }


                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateeventsdiscount',
                    data: {
                        "event_id": $scope.evnt_id,
                        "event_discount_id": $scope.event_discount_id,
                        "discount_type": $scope.discount_type,
                        "discount_code": $scope.discount_code,
                        "discount_amount": $scope.discount_value,
                        "list_type": $scope.selectedeventlisttype,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8"
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedEventDetails = 'Y';
                        $localStorage.preview_eventcontent = $localStorage.event_details = $scope.event_details = response.data.msg;
                        $('#progress-full').hide();

                        $("#AddEventmessageModal").modal('show');
                        $("#AddEventmessagetitle").text('Message');
                        $("#AddEventmessagecontent").text('Discount successfully Updated');

                        if ($scope.event_details.event_type === "S" && ($scope.event_details.parent_id === "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                            $scope.eventadded = true;
                            $scope.EventName = $scope.event_details.event_title;
                            $scope.single_eventid = $scope.event_details.event_id;
                            $scope.event_status = $scope.event_details.event_status;
                            $scope.event_type = $scope.event_details.event_type;
                            if ($scope.event_details.event_begin_dt === '0000-00-00 00:00:00') {
                                $scope.eventstartdate = "";
                                $scope.eventstarttime = "";
                            } else {
                                $scope.eventstartdate = $scope.formatserverdate($scope.event_details.event_begin_dt.split(" ")[0]);
                                if ($scope.event_details.event_begin_dt.split(" ")[1] === "00:00:00") {
                                    $scope.eventstarttime = "";
                                } else {
                                    $scope.eventstarttime = $scope.formatAMPM($scope.event_details.event_begin_dt);
                                }
                            }
                            if ($scope.event_details.event_end_dt === '0000-00-00 00:00:00') {
                                $scope.eventenddate = "";
                                $scope.eventendtime = "";
                            } else {
                                $scope.eventenddate = $scope.formatserverdate($scope.event_details.event_end_dt.split(" ")[0]);
                                if ($scope.event_details.event_end_dt.split(" ")[1] === "00:00:00") {
                                    $scope.eventendtime = "";
                                } else {
                                    $scope.eventendtime = $scope.formatAMPM($scope.event_details.event_end_dt);
                                }
                            }
                            $scope.dateTimevalidationChange();
                            $scope.EventCost = $scope.event_details.event_cost;
                            $scope.EventCompCost = $scope.event_details.event_compare_price;
                            $scope.EventUrl = $scope.event_details.event_video_detail_url;
                            $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                            $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                            $scope.cropper.sourceImage = $scope.croppedEventImage;
                            $scope.singledesc = $scope.event_details.event_desc;
                            $scope.Eventregurl = $scope.event_details.event_url;
                            
                            if($scope.event_details.waiver_policies === null) {
                                $scope.waiverpolicy = '';
                            } else {
                                $scope.waiverpolicy = $scope.event_details.waiver_policies;
                            }

                            $scope.singlepaymentview = true;
//                            if(parseFloat($scope.event_details.event_capacity)>0){
                                $scope.eventcapacity = $scope.event_details.event_capacity;
//                            } else{
//                                $scope.eventcapacity = '';
//                            }
                            $scope.eventspaceremng = $scope.event_details.capacity_text;
                            $scope.eventprocessingfee = $scope.event_details.processing_fees;
                            $scope.eventnet_sales = $scope.event_details.net_sales;

                            if ($scope.event_details.discount.length > 0) {
                                $scope.singlediscountList = $scope.event_details.discount;
                                $scope.singlediscountListView = true;
                                $scope.singlediscountView = true;
                                $scope.singlediscountEditView = false;
                            } else {
                                $scope.singlediscountList = "";
                                $scope.singlePaymentUpdateButton = false;
                                $scope.singlediscountListView = false;
                                $scope.singlediscountView = false;
                                $scope.singlediscountEditView = false;
                            }

                            $scope.discountcode = "";
                            $scope.discountamount = "";
                            $scope.discnt_type = 'V';
                            $scope.selecteddiscountindex = "";
                            $scope.singlePaymentUpdateButton = false;

                            if ($scope.event_details.event_recurring_payment_flag === 'Y') {
                                $scope.recurringschecked = true;
                                $scope.recurringsinglepayment = 'Y';
                                $scope.eventorderover = $scope.event_details.total_order_text;
                                $scope.eventquantityover = $scope.event_details.total_quantity_text;
                                $scope.depositamnt = $scope.event_details.deposit_amount;
                                $scope.noofpayment = $scope.event_details.number_of_payments;
                                $scope.spayment_startdate_type = $scope.event_details.payment_startdate_type;
                                $scope.paymntstartdate = $scope.formatserverdate($scope.event_details.payment_startdate);
                                $scope.paymentfreq = $scope.event_details.payment_frequency;

                            } else if ($scope.event_details.event_recurring_payment_flag === 'N') {
                                $scope.recurringschecked = false;
                                $scope.recurringsinglepayment = 'N';
                                $scope.eventorderover = "";
                                $scope.eventquantityover = "";
                                $scope.depositamnt = "";
                                $scope.noofpayment = "";
                                $scope.paymntstartdate = "";
                                $scope.paymentfreq = "1";
                            }

                            if ($scope.event_details.event_onetime_payment_flag === 'Y') {
                                $scope.onetimeschecked = true;
                                $scope.onetimesinglepayment = 'Y';
                            } else if ($scope.event_details.event_onetime_payment_flag === 'N') {
                                $scope.onetimeschecked = false;
                                $scope.onetimesinglepayment = 'N';
                            }
                            $scope.selectedeventlisttype = $scope.event_details.list_type;


                        } else if ($scope.event_details.event_type === "M" && ($scope.event_details.parent_id !== "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                            if ($scope.event_details.event_status === 'P') {
                                $scope.IsVisible = false;
                            } else if ($scope.event_details.event_status === 'S') {
                                $scope.IsVisible = true;
                            }
                            $scope.eventadded = true;
                            $scope.event_type = $scope.event_details.event_type;
                            $scope.parent_event_status = $scope.event_details.event_status;
                            $scope.multiple_parent_id = $scope.event_details.event_id;
                            $scope.EventCategory = $scope.event_details.event_title;
                            $scope.EventSubCategory = $scope.event_details.event_category_subtitle;
                            $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                            $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                            $scope.cropper.sourceImage = $scope.croppedEventImage;
                            $scope.PEventUrl = $scope.event_details.event_video_detail_url;
                            $scope.parentdesc = $scope.event_details.event_desc;
                            $scope.EventPregurl = $scope.event_details.event_url;
                                
                            if($scope.event_details.waiver_policies === null) {
                                $scope.waiverpolicy = '';
                            } else {
                                $scope.waiverpolicy = $scope.event_details.waiver_policies;
                            }
                            
                            $scope.ceventprocessingfee = $scope.event_details.processing_fees;
                            $scope.mcevent_processing_fee = $scope.event_details.processing_fees;
                            if ($scope.event_details.child_events.length > 0) {
                                $scope.IsVisible = false;
                                $scope.multipleeventlist = $scope.event_details.child_events;
                                $scope.childEventListView = true;
                                $scope.childEventCardView = true;
                                $scope.AddeventButton = true;
                            } else {
                                $scope.multipleeventlist = "";
                                $scope.childEventListView = false;
                                $scope.childEventCardView = false;
                                $scope.AddeventButton = true;
                            }
                            
                            if ($scope.event_details.discount.length > 0) {
                                    $scope.multiplediscountList = $scope.event_details.discount;
                                    $scope.multiplediscountListView = true;
                                    $scope.multiplediscountView = true;
                                    $scope.multiplediscountEditView = false;
                                } else {
                                    $scope.multiplediscountList = "";
                                    $scope.multiplediscountListView = false;
                                    $scope.multiplediscountView = false;
                                    $scope.multiplediscountEditView = false;
                                }
                                
                            $scope.selectedmdiscountindex = "";
                            $scope.cdiscountcode = $scope.cdiscountamount = "";
                            $scope.mdiscnt_type = 'V';
                            $scope.multiplePaymentUpdateButton = false;
                            $scope.selectedeventlisttype = $scope.event_details.list_type;


                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedEventDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $localStorage.isAddedEventDetails = 'N';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedEventDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };


// single/multiple discount value delete confirmation modal
            $scope.deletediscount = function (evnt_type, discnt_id) {
                $scope.deletediscountevent_type = evnt_type;
                $scope.deletediscountdiscnt_id = discnt_id;
                $("#discountDeleteModal").modal('show');
                $("#discountDeletetitle").text('Delete');
                $("#discountDeletecontent").text("Are you sure want to delete discount detail? ");
            };

// single/multiple discount value delete cancel
            $scope.discountdeleteCancel = function () {
                console.log('cancel delete');
                $location.path('/addevent');
            };

// single/multiple discount value delete
            $scope.discountdeleteConfirm = function () {
                $('#progress-full').show();

                if ($scope.deletediscountevent_type === "S") {
                    $scope.evnt_id = $scope.single_eventid;
                    $scope.event_discount_id = $scope.deletediscountdiscnt_id;

                } else if ($scope.deletediscountevent_type === "M") {
                    $scope.evnt_id = $scope.multiple_parent_id;
                    $scope.event_discount_id = $scope.deletediscountdiscnt_id;
                }

                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteEventDiscount',
                    data: {
                        "event_id": $scope.evnt_id,
                        "event_discount_id": $scope.event_discount_id,
                        "list_type": $scope.selectedeventlisttype,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8"
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedEventDetails = 'Y';
                        $localStorage.preview_eventcontent = $localStorage.event_details = $scope.event_details = response.data.msg;
                        $('#progress-full').hide();

                        $("#AddEventmessageModal").modal('show');
                        $("#AddEventmessagetitle").text('Message');
                        $("#AddEventmessagecontent").text('Discount successfully deleted');

                        if ($scope.event_details.event_type === "S" && ($scope.event_details.parent_id === "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                            $scope.eventadded = true;
                            $scope.EventName = $scope.event_details.event_title;
                            $scope.single_eventid = $scope.event_details.event_id;
                            $scope.event_status = $scope.event_details.event_status;
                            $scope.event_type = $scope.event_details.event_type;
                            if ($scope.event_details.event_begin_dt === '0000-00-00 00:00:00') {
                                $scope.eventstartdate = "";
                                $scope.eventstarttime = "";
                            } else {
                                $scope.eventstartdate = $scope.formatserverdate($scope.event_details.event_begin_dt.split(" ")[0]);
                                if ($scope.event_details.event_begin_dt.split(" ")[1] === "00:00:00") {
                                    $scope.eventstarttime = "";
                                } else {
                                    $scope.eventstarttime = $scope.formatAMPM($scope.event_details.event_begin_dt);
                                }
                            }
                            if ($scope.event_details.event_end_dt === '0000-00-00 00:00:00') {
                                $scope.eventenddate = "";
                                $scope.eventendtime = "";
                            } else {
                                $scope.eventenddate = $scope.formatserverdate($scope.event_details.event_end_dt.split(" ")[0]);
                                if ($scope.event_details.event_end_dt.split(" ")[1] === "00:00:00") {
                                    $scope.eventendtime = "";
                                } else {
                                    $scope.eventendtime = $scope.formatAMPM($scope.event_details.event_end_dt);
                                }
                            }
                            $scope.dateTimevalidationChange();
                            $scope.EventCost = $scope.event_details.event_cost;
                            $scope.EventCompCost = $scope.event_details.event_compare_price;
                            $scope.EventUrl = $scope.event_details.event_video_detail_url;
                            $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                            $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                            $scope.cropper.sourceImage = $scope.croppedEventImage;
                            $scope.singledesc = $scope.event_details.event_desc;
                            $scope.Eventregurl = $scope.event_details.event_url;

                            if ($scope.event_details.discount.length > 0) {
                                $scope.singlediscountList = $scope.event_details.discount;
                                $scope.singlediscountListView = true;
                                $scope.singlediscountView = true;
                                $scope.singlediscountEditView = false;
                            } else {
                                $scope.singlediscountList = "";
                                $scope.singlediscountListView = false;
                                $scope.singlediscountListView = false;
                                $scope.singlediscountEditView = false;
                            }
                            $scope.discountcode = "";
                            $scope.discountamount = "";
                            $scope.discnt_type = 'V';
                            $scope.selecteddiscountindex = "";
                            $scope.singlePaymentUpdateButton = false;
                            $scope.selectedeventlisttype = $scope.event_details.list_type;

                        } else if ($scope.event_details.event_type === "M" && ($scope.event_details.parent_id !== "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                            if ($scope.event_details.event_status === 'P') {
                                $scope.IsVisible = false;
                            } else if ($scope.event_details.event_status === 'S') {
                                $scope.IsVisible = true;
                            }
                            $scope.eventadded = true;
                            $scope.event_type = $scope.event_details.event_type;
                            $scope.parent_event_status = $scope.event_details.event_status;
                            $scope.multiple_parent_id = $scope.event_details.event_id;
                            $scope.EventCategory = $scope.event_details.event_title;
                            $scope.EventSubCategory = $scope.event_details.event_category_subtitle;
                            $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                            $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                            $scope.cropper.sourceImage = $scope.croppedEventImage;
                            $scope.PEventUrl = $scope.event_details.event_video_detail_url;
                            $scope.parentdesc = $scope.event_details.event_desc;
                            $scope.EventPregurl = $scope.event_details.event_url;
                            
                            if ($scope.event_details.child_events.length > 0) {
                                $scope.IsVisible = false;
                                $scope.multipleeventlist = $scope.event_details.child_events;
                                $scope.childEventListView = true;
                                $scope.childEventCardView = true;
                                $scope.AddeventButton = true;
                            } else {
                                $scope.multipleeventlist = "";
                                $scope.childEventListView = false;
                                $scope.childEventCardView = false;
                                $scope.AddeventButton = true;
                            }
                            
                            
                            if ($scope.event_details.discount.length > 0) {
                                    $scope.multiplediscountList = $scope.event_details.discount;
                                    $scope.multiplediscountListView = true;
                                    $scope.multiplediscountView = true;
                                    $scope.multiplediscountEditView = false;
                                } else {
                                    $scope.multiplediscountList = "";
                                    $scope.multiplediscountListView = false;
                                    $scope.multiplediscountView = false;
                                    $scope.multiplediscountEditView = false;
                                }
                            
                            $scope.cdiscountcode = "";
                            $scope.cdiscountamount = "";
                            $scope.mdiscnt_type = 'V';
                            $scope.selectedmdiscountindex = "";
                            $scope.multiplePaymentUpdateButton = false;
                            $scope.selectedeventlisttype = $scope.event_details.list_type;
                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedEventDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $localStorage.isAddedEventDetails = 'N';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedEventDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

//single/multiple update payment setup - once purchase,recurring setup
            $scope.updatepaymentsetup = function (evnt_type, evnt_id) {
                $('#progress-full').show();
                if (evnt_type === "S") {
                    $scope.evnt_id = $scope.single_eventid;

//                    if ($scope.eventcapacity === "" || $scope.eventcapacity === undefined) {
//                        $scope.evnt_capacity = "";
//                    } else {
//                        $scope.evnt_capacity = $scope.eventcapacity;
//                    }
//
//                    if ($scope.eventspaceremng === "" || $scope.eventspaceremng === undefined) {
//                        $scope.evnt_capacity_text = "1";
//                    } else {
//                        $scope.evnt_capacity_text = $scope.eventspaceremng;
//                    }

                    if ($scope.eventprocessingfee === "" || $scope.eventprocessingfee === undefined) {
                        $scope.evnt_processing_fees = "2";
                    } else {
                        $scope.evnt_processing_fees = $scope.eventprocessingfee;
                    }

                    if ($scope.recurringsinglepayment === 'Y') {
                        $scope.event_recurring_payment_flag = 'Y';

                        if ($scope.eventorderover === "" || $scope.eventorderover === undefined) {
                            $scope.evnt_total_order_text = "";
                        } else {
                            $scope.evnt_total_order_text = $scope.eventorderover;
                        }

                        if ($scope.eventquantityover === "" || $scope.eventquantityover === undefined) {
                            $scope.evnt_total_quantity_text = "";
                        } else {
                            $scope.evnt_total_quantity_text = $scope.eventquantityover;
                        }

                        if ($scope.depositamnt === "" || $scope.depositamnt === undefined) {
                            $scope.evnt_deposit_amount = "";
                        } else {
                            $scope.evnt_deposit_amount = $scope.depositamnt;
                        }

                        if ($scope.noofpayment === "" || $scope.noofpayment === undefined) {
                            $scope.evnt_number_of_payments = "";
                        } else {
                            $scope.evnt_number_of_payments = $scope.noofpayment;
                        }
                        
                        if ($scope.spayment_startdate_type === '4') {
                            $scope.evnt_payment_startdate_type = $scope.spayment_startdate_type;
                            if ($scope.paymntstartdate === undefined || $scope.paymntstartdate === "") {
                                $scope.evnt_payment_startdate = "";
                            } else {
                                $scope.evnt_payment_startdate = $scope.dateformat($scope.paymntstartdate);
                            }
                        } else {
                            $scope.evnt_payment_startdate_type = $scope.spayment_startdate_type;
                            $scope.evnt_payment_startdate = "";
                        }

                        if ($scope.paymentfreq === "" || $scope.paymentfreq === undefined) {
                            $scope.evnt_payment_frequency = "1";
                        } else {
                            $scope.evnt_payment_frequency = $scope.paymentfreq;
                        }
                    } else if ($scope.recurringsinglepayment === 'N') {
                        $scope.event_recurring_payment_flag = 'N';
                        $scope.evnt_total_order_text = "";
                        $scope.evnt_total_quantity_text = "";
                        $scope.evnt_deposit_amount = "";
                        $scope.evnt_number_of_payments = "";
                        $scope.evnt_payment_startdate = "";
                        $scope.evnt_payment_startdate_type = "1";
                        $scope.evnt_payment_frequency = "1";
                    }

                    if ($scope.onetimesinglepayment === 'Y') {
                        $scope.event_onetime_payment_flag = 'Y';
                    } else if ($scope.onetimesinglepayment === 'N') {
                        $scope.event_onetime_payment_flag = 'N';
                    }

                } else if (evnt_type === "M") {
                    $scope.evnt_id = $scope.multiple_parent_id;
//                    $scope.evnt_capacity = "";
//                    $scope.evnt_capacity_text = "1";

                    if ($scope.recurringmultiplepayment === 'Y') {
                        $scope.event_recurring_payment_flag = 'Y';

                        if ($scope.meventorderover === "" || $scope.meventorderover === undefined) {
                            $scope.evnt_total_order_text = "";
                        } else {
                            $scope.evnt_total_order_text = $scope.meventorderover;
                        }

                        if ($scope.meventquantityover === "" || $scope.meventquantityover === undefined) {
                            $scope.evnt_total_quantity_text = "";
                        } else {
                            $scope.evnt_total_quantity_text = $scope.meventquantityover;
                        }

                        if ($scope.mdepositamnt === "" || $scope.mdepositamnt === undefined) {
                            $scope.evnt_deposit_amount = "";
                        } else {
                            $scope.evnt_deposit_amount = $scope.mdepositamnt;
                        }

                        if ($scope.mnoofpayment === "" || $scope.mnoofpayment === undefined) {
                            $scope.evnt_number_of_payments = "";
                        } else {
                            $scope.evnt_number_of_payments = $scope.mnoofpayment;
                        }
                        
                        if ($scope.mpayment_startdate_type === '4') {
                            $scope.evnt_payment_startdate_type = $scope.mpayment_startdate_type;
                            if ($scope.mpaymntstartdate === undefined || $scope.mpaymntstartdate === "") {
                                $scope.evnt_payment_startdate = "";
                            } else {
                                $scope.evnt_payment_startdate = $scope.dateformat($scope.mpaymntstartdate);
                            }
                        } else {
                            $scope.evnt_payment_startdate_type = $scope.mpayment_startdate_type;
                            $scope.evnt_payment_startdate = "";
                        }                        
                        

                        if ($scope.mpaymentfreq === "" || $scope.mpaymentfreq === undefined) {
                            $scope.evnt_payment_frequency = "1";
                        } else {
                            $scope.evnt_payment_frequency = $scope.mpaymentfreq;
                        }
                    } else if ($scope.recurringmultiplepayment === 'N') {
                        $scope.event_recurring_payment_flag = 'N';
                        $scope.evnt_total_order_text = "";
                        $scope.evnt_total_quantity_text = "";
                        $scope.evnt_deposit_amount = "";
                        $scope.evnt_number_of_payments = "";
                        $scope.evnt_payment_startdate = "";
                        $scope.evnt_payment_startdate_type = "1";
                        $scope.evnt_payment_frequency = "1";
                    }
                    
                    
                    if ($scope.ceventprocessingfee === "" || $scope.ceventprocessingfee === undefined) {
                        $scope.evnt_processing_fees = "2";
                    } else {
                        $scope.evnt_processing_fees = $scope.ceventprocessingfee;
                    }

                    if ($scope.onetimemultiplepayment === 'Y') {
                        $scope.event_onetime_payment_flag = 'Y';
                    } else if ($scope.onetimemultiplepayment === 'N') {
                        $scope.event_onetime_payment_flag = 'N';
                    }

                } 

                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateEventPaymentDetails',
                    data: {
                        "processing_fees": $scope.evnt_processing_fees,
                        "event_onetime_payment_flag": $scope.event_onetime_payment_flag,
                        "event_recurring_payment_flag": $scope.event_recurring_payment_flag,
                        "total_order_text": $scope.evnt_total_order_text,
                        "total_quantity_text": $scope.evnt_total_quantity_text,
                        "deposit_amount": $scope.evnt_deposit_amount,
                        "number_of_payments": $scope.evnt_number_of_payments,
                        "payment_startdate": $scope.evnt_payment_startdate,
                        "payment_startdate_type": $scope.evnt_payment_startdate_type,
                        "payment_frequency": $scope.evnt_payment_frequency,
                        "list_type": $scope.selectedeventlisttype,
                        "event_id": $scope.evnt_id,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8"
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedEventDetails = 'Y';
                        $localStorage.currentpage = "editevents";
                        $localStorage.childeventindex = '';
                        $localStorage.preview_eventcontent =  $localStorage.event_details = $scope.event_details = response.data.msg;
                        $('#progress-full').hide();

                        $("#AddEventmessageModal").modal('show');
                        $("#AddEventmessagetitle").text('Message');
                        $("#AddEventmessagecontent").text('Payment details successfully Updated');

                        if ($scope.event_details.event_type === "S" && ($scope.event_details.parent_id === "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                            $scope.eventadded = true;
                            $scope.EventName = $scope.event_details.event_title;
                            $scope.single_eventid = $scope.event_details.event_id;
                            $scope.event_status = $scope.event_details.event_status;
                            $scope.event_type = $scope.event_details.event_type;
                            if ($scope.event_details.event_begin_dt === '0000-00-00 00:00:00') {
                                $scope.eventstartdate = "";
                                $scope.eventstarttime = "";
                            } else {
                                $scope.eventstartdate = $scope.formatserverdate($scope.event_details.event_begin_dt.split(" ")[0]);
                                if ($scope.event_details.event_begin_dt.split(" ")[1] === "00:00:00") {
                                    $scope.eventstarttime = "";
                                } else {
                                    $scope.eventstarttime = $scope.formatAMPM($scope.event_details.event_begin_dt);
                                }
                            }
                            if ($scope.event_details.event_end_dt === '0000-00-00 00:00:00') {
                                $scope.eventenddate = "";
                                $scope.eventendtime = "";
                            } else {
                                $scope.eventenddate = $scope.formatserverdate($scope.event_details.event_end_dt.split(" ")[0]);
                                if ($scope.event_details.event_end_dt.split(" ")[1] === "00:00:00") {
                                    $scope.eventendtime = "";
                                } else {
                                    $scope.eventendtime = $scope.formatAMPM($scope.event_details.event_end_dt);
                                }
                            }
                            $scope.dateTimevalidationChange();
                            $scope.EventCost = $scope.event_details.event_cost;
                            $scope.EventCompCost = $scope.event_details.event_compare_price;
                            $scope.EventUrl = $scope.event_details.event_video_detail_url;
                            $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                            $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                            $scope.cropper.sourceImage = $scope.croppedEventImage;
                            $scope.singledesc = $scope.event_details.event_desc;
                            $scope.Eventregurl = $scope.event_details.event_url;

                            $scope.discountcode = "";
                            $scope.discountamount = "";
                            $scope.singlepaymentview = true;
//                            if(parseFloat($scope.event_details.event_capacity)>0){
                                $scope.eventcapacity = $scope.event_details.event_capacity;
//                            } else{
//                                $scope.eventcapacity = '';
//                            }
                            $scope.eventspaceremng = $scope.event_details.capacity_text;
                            $scope.eventprocessingfee = $scope.event_details.processing_fees;
                            $scope.eventnet_sales = $scope.event_details.net_sales;
                            if ($scope.event_details.event_recurring_payment_flag === 'Y') {
                                $scope.recurringschecked = true;
                                $scope.recurringsinglepayment = 'Y';
                                $scope.eventorderover = $scope.event_details.total_order_text;
                                $scope.eventquantityover = $scope.event_details.total_quantity_text;
                                $scope.depositamnt = $scope.event_details.deposit_amount;
                                $scope.noofpayment = $scope.event_details.number_of_payments;
                                $scope.spayment_startdate_type = $scope.event_details.payment_startdate_type;
                                $scope.paymntstartdate = $scope.formatserverdate($scope.event_details.payment_startdate);
                                $scope.paymentfreq = $scope.event_details.payment_frequency;

                            } else if ($scope.event_details.event_recurring_payment_flag === 'N') {
                                $scope.recurringschecked = false;
                                $scope.recurringsinglepayment = 'N';
                                $scope.eventorderover = "";
                                $scope.eventquantityover = "";
                                $scope.depositamnt = "";
                                $scope.noofpayment = "";
                                $scope.paymntstartdate = "";
                                $scope.paymentfreq = "1";
                            }

                            if ($scope.event_details.event_onetime_payment_flag === 'Y') {
                                $scope.onetimeschecked = true;
                                $scope.onetimesinglepayment = 'Y';
                            } else if ($scope.event_details.event_onetime_payment_flag === 'N') {
                                $scope.onetimeschecked = false;
                                $scope.onetimesinglepayment = 'N';
                            }


                            if ($scope.event_details.discount.length > 0) {
                                $scope.singlediscountList = $scope.event_details.discount;
                                $scope.singlediscountListView = true;
                                $scope.singlediscountView = true;
                                $scope.singlediscountEditView = false;
                            } else {
                                $scope.singlediscountList = "";
                                $scope.singlediscountListView = false;
                                $scope.singlediscountView = false;
                                $scope.singlediscountEditView = false;
                            }
                            $scope.discountcode = "";
                            $scope.discountamount = "";
                            $scope.discnt_type = 'V';
                            $scope.selecteddiscountindex = "";
                            $scope.singlePaymentUpdateButton = false;
                            $scope.selectedeventlisttype = $scope.event_details.list_type;


                        } else if ($scope.event_details.event_type === "M" && ($scope.event_details.parent_id !== "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                            if ($scope.event_details.event_status === 'P') {
                                $scope.IsVisible = false;
                            } else if ($scope.event_details.event_status === 'S') {
                                $scope.IsVisible = true;
                            }
                            $scope.eventadded = true;
                            $scope.event_type = $scope.event_details.event_type;
                            $scope.parent_event_status = $scope.event_details.event_status;
                            $scope.multiple_parent_id = $scope.event_details.event_id;
                            $scope.EventCategory = $scope.event_details.event_title;
                            $scope.EventSubCategory = $scope.event_details.event_category_subtitle;
                            $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                            $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                            $scope.cropper.sourceImage = $scope.croppedEventImage;
                            $scope.PEventUrl = $scope.event_details.event_video_detail_url;
                            $scope.parentdesc = $scope.event_details.event_desc;
                            $scope.EventPregurl = $scope.event_details.event_url;
                            
                            $scope.ceventprocessingfee = $scope.event_details.processing_fees;
                            $scope.mcevent_processing_fee = $scope.event_details.processing_fees;
                            if ($scope.event_details.child_events.length > 0) {                                
                                $localStorage.preview_childlisthide = false;
                                $localStorage.preview_eventchildlist = true;
                                $scope.IsVisible = false;
                                $scope.multipleeventlist = $scope.event_details.child_events;
                                $scope.childEventListView = true;
                                $scope.childEventCardView = true;
                                $scope.childEventPaymentView = true;
                                $scope.childpaymentCardView = true;
                                $scope.eventpaymentformview = false;
                                $scope.AddeventButton = true;
                            } else {
                                $scope.multipleeventlist = "";
                                $scope.childEventListView = false;
                                $scope.childEventCardView = false;
                                $scope.childEventPaymentView = false;
                                $scope.childpaymentCardView = false;
                                $scope.eventpaymentformview = false;
                                $scope.AddeventButton = true;
                            }
                            if ($scope.event_details.event_recurring_payment_flag === 'Y') {
                                $scope.recurringmchecked = true;
                                $scope.recurringmultiplepayment = 'Y';
                                $scope.meventorderover = $scope.event_details.total_order_text;
                                $scope.meventquantityover = $scope.event_details.total_quantity_text;
                                $scope.mdepositamnt = $scope.event_details.deposit_amount;
                                $scope.mnoofpayment = $scope.event_details.number_of_payments;
                                $scope.mpayment_startdate_type = $scope.event_details.payment_startdate_type;
                                $scope.mpaymntstartdate = $scope.formatserverdate($scope.event_details.payment_startdate);
                                $scope.mpaymentfreq = $scope.event_details.payment_frequency;

                            } else if ($scope.event_details.event_recurring_payment_flag === 'N') {
                                $scope.recurringmchecked = false;
                                $scope.recurringmultiplepayment = 'N';
                                $scope.meventorderover = "";
                                $scope.meventquantityover = "";
                                $scope.mdepositamnt = "";
                                $scope.mnoofpayment = "";
                                $scope.mpaymntstartdate = "";
                                $scope.mpaymentfreq = "1";
                            }

                            if ($scope.event_details.event_onetime_payment_flag === 'Y') {
                                $scope.onetimemchecked = true;
                                $scope.onetimemultiplepayment = 'Y';
                            } else if ($scope.event_details.event_onetime_payment_flag === 'N') {
                                $scope.onetimemchecked = false;
                                $scope.onetimemultiplepayment = 'N';
                            }
                            
                            if ($scope.event_details.discount.length > 0) {
                                    $scope.multiplediscountList = $scope.event_details.discount;
                                    $scope.multiplediscountListView = true;
                                    $scope.multiplediscountView = true;
                                    $scope.multiplediscountEditView = false;
                                } else {
                                    $scope.multiplediscountList = "";
                                    $scope.multiplediscountListView = false;
                                    $scope.multiplediscountView = false;
                                    $scope.multiplediscountEditView = false;
                                }

                            $scope.cdiscountcode = "";
                            $scope.cdiscountamount = "";
                            $scope.mdiscnt_type = 'V';
                            $scope.selectedchildpayment = "";
                            $scope.selectedmdiscountindex = "";
                            $scope.multiplePaymentUpdateButton = false;
                            $scope.selectedeventlisttype = $scope.event_details.list_type;

                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedEventDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $localStorage.isAddedEventDetails = 'N';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedEventDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

//update existing waiver text
            $scope.updatewaiver = function () {
                $('#progress-full').show();
                if ($scope.event_type === 'S') {
                    $scope.evnt_id = $scope.single_eventid;
                } else if ($scope.event_type === 'M') {
                    $scope.evnt_id = $scope.multiple_parent_id;
                }
                $scope.waiver_policies_encode = document.getElementById("waivertextarea").innerHTML;
                $scope.waiver_policies = $scope.waiver_policies_encode;

                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateWaiver',
                    data: {
                        "waiver_policies": $scope.waiver_policies,
                        "event_id": $scope.evnt_id,
                        "list_type": $scope.selectedeventlisttype,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8"
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedEventDetails = 'Y';
                        $localStorage.currentpage = "editevents";
                        $localStorage.preview_eventcontent = $localStorage.event_details = $scope.event_details = response.data.msg;
                        $('#progress-full').hide();

                        $("#AddEventmessageModal").modal('show');
                        $("#AddEventmessagetitle").text('Message');
                        $("#AddEventmessagecontent").text('Waiver text updated successfully');

                        if ($scope.event_details.event_type === "S" && ($scope.event_details.parent_id === "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                            $scope.eventadded = true;
                            $scope.EventName = $scope.event_details.event_title;
                            $scope.single_eventid = $scope.event_details.event_id;
                            $scope.event_status = $scope.event_details.event_status;
                            $scope.event_type = $scope.event_details.event_type;
                            if ($scope.event_details.event_begin_dt === '0000-00-00 00:00:00') {
                                $scope.eventstartdate = "";
                                $scope.eventstarttime = "";
                            } else {
                                $scope.eventstartdate = $scope.formatserverdate($scope.event_details.event_begin_dt.split(" ")[0]);
                                if ($scope.event_details.event_begin_dt.split(" ")[1] === "00:00:00") {
                                    $scope.eventstarttime = "";
                                } else {
                                    $scope.eventstarttime = $scope.formatAMPM($scope.event_details.event_begin_dt);
                                }
                            }
                            if ($scope.event_details.event_end_dt === '0000-00-00 00:00:00') {
                                $scope.eventenddate = "";
                                $scope.eventendtime = "";
                            } else {
                                $scope.eventenddate = $scope.formatserverdate($scope.event_details.event_end_dt.split(" ")[0]);
                                if ($scope.event_details.event_end_dt.split(" ")[1] === "00:00:00") {
                                    $scope.eventendtime = "";
                                } else {
                                    $scope.eventendtime = $scope.formatAMPM($scope.event_details.event_end_dt);
                                }
                            }
                            $scope.dateTimevalidationChange();
                            $scope.EventCost = $scope.event_details.event_cost;
                            $scope.EventCompCost = $scope.event_details.event_compare_price;
                            $scope.EventUrl = $scope.event_details.event_video_detail_url;
                            $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                            $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                            $scope.cropper.sourceImage = $scope.croppedEventImage;
                            $scope.singledesc = $scope.event_details.event_desc;
                            $scope.Eventregurl = $scope.event_details.event_url;
                            if($scope.event_details.waiver_policies === null) {
                                $scope.waiverpolicy = '';
                            } else {
                                $scope.waiverpolicy = $scope.event_details.waiver_policies;
                            }

                            $scope.discountcode = "";
                            $scope.discountamount = "";
                            $scope.singlepaymentview = true;
//                           if(parseFloat($scope.event_details.event_capacity)>0){
                                $scope.eventcapacity = $scope.event_details.event_capacity;
//                            } else{
//                                $scope.eventcapacity = '';
//                            }
                            $scope.eventspaceremng = $scope.event_details.capacity_text;
                            $scope.eventprocessingfee = $scope.event_details.processing_fees;
                            $scope.eventnet_sales = $scope.event_details.net_sales;

                            if ($scope.event_details.event_recurring_payment_flag === 'Y') {
                                $scope.recurringschecked = true;
                                $scope.recurringsinglepayment = 'Y';
                                $scope.eventorderover = $scope.event_details.total_order_text;
                                $scope.eventquantityover = $scope.event_details.total_quantity_text;
                                $scope.depositamnt = $scope.event_details.deposit_amount;
                                $scope.noofpayment = $scope.event_details.number_of_payments;
                                $scope.spayment_startdate_type = $scope.event_details.payment_startdate_type;
                                $scope.paymntstartdate = $scope.formatserverdate($scope.event_details.payment_startdate);
                                $scope.paymentfreq = $scope.event_details.payment_frequency;

                            } else if ($scope.event_details.event_recurring_payment_flag === 'N') {
                                $scope.recurringschecked = false;
                                $scope.recurringsinglepayment = 'N';
                                $scope.eventorderover = "";
                                $scope.eventquantityover = "";
                                $scope.depositamnt = "";
                                $scope.noofpayment = "";
                                $scope.paymntstartdate = "";
                                $scope.paymentfreq = "1";
                            }

                            if ($scope.event_details.event_onetime_payment_flag === 'Y') {
                                $scope.onetimeschecked = true;
                                $scope.onetimesinglepayment = 'Y';
                            } else if ($scope.event_details.event_onetime_payment_flag === 'N') {
                                $scope.onetimeschecked = false;
                                $scope.onetimesinglepayment = 'N';
                            }
                            if ($scope.event_details.discount.length > 0) {
                                $scope.singlediscountList = $scope.event_details.discount;
                                $scope.singlediscountListView = true;
                                $scope.singlediscountView = true;
                                $scope.singlediscountEditView = false;
                            } else {
                                $scope.singlediscountList = "";
                                $scope.singlediscountListView = false;
                                $scope.singlediscountView = false;
                                $scope.singlediscountEditView = false;
                            }
                            $scope.discountcode = "";
                            $scope.discountamount = "";
                            $scope.discnt_type = 'V';
                            $scope.selecteddiscountindex = "";
                            $scope.singlePaymentUpdateButton = false;
                            $scope.selectedeventlisttype = $scope.event_details.list_type;

                        } else if ($scope.event_details.event_type === "M" && ($scope.event_details.parent_id !== "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                            if ($scope.event_details.event_status === 'P') {
                                $scope.IsVisible = false;
                            } else if ($scope.event_details.event_status === 'S') {
                                $scope.IsVisible = true;
                            }
                            $scope.eventadded = true;
                            $scope.event_type = $scope.event_details.event_type;
                            $scope.parent_event_status = $scope.event_details.event_status;
                            $scope.multiple_parent_id = $scope.event_details.event_id;
                            $scope.EventCategory = $scope.event_details.event_title;
                            $scope.EventSubCategory = $scope.event_details.event_category_subtitle;
                            $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                            $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                            $scope.cropper.sourceImage = $scope.croppedEventImage;
                            $scope.PEventUrl = $scope.event_details.event_video_detail_url;
                            $scope.parentdesc = $scope.event_details.event_desc;
                            $scope.EventPregurl = $scope.event_details.event_url;
                                
                            if($scope.event_details.waiver_policies === null) {
                                $scope.waiverpolicy = '';
                            } else {
                                $scope.waiverpolicy = $scope.event_details.waiver_policies;
                            }
                            
                            $scope.ceventprocessingfee = $scope.event_details.processing_fees;
                            $scope.mcevent_processing_fee = $scope.event_details.processing_fees;
                            if ($scope.event_details.child_events.length > 0) {
                                $scope.IsVisible = false;
                                $scope.multipleeventlist = $scope.event_details.child_events;
                                $scope.childEventListView = true;
                                $scope.childEventCardView = true;
                                $scope.childEventPaymentView = true;
                                $scope.childpaymentCardView = true;
                                $scope.eventpaymentformview = false;
                                $scope.AddeventButton = true;
                            } else {
                                $scope.multipleeventlist = "";
                                $scope.childEventListView = false;
                                $scope.childEventCardView = false;
                                $scope.childEventPaymentView = false;
                                $scope.childpaymentCardView = false;
                                $scope.eventpaymentformview = false;
                                $scope.AddeventButton = true;
                            }

                            if ($scope.event_details.event_recurring_payment_flag === 'Y') {
                                $scope.recurringmchecked = true;
                                $scope.recurringmultiplepayment = 'Y';
                                $scope.meventorderover = $scope.event_details.total_order_text;
                                $scope.meventquantityover = $scope.event_details.total_quantity_text;
                                $scope.mdepositamnt = $scope.event_details.deposit_amount;
                                $scope.mnoofpayment = $scope.event_details.number_of_payments;
                                $scope.mpayment_startdate_type = $scope.event_details.payment_startdate_type;
                                $scope.mpaymntstartdate = $scope.formatserverdate($scope.event_details.payment_startdate);
                                $scope.mpaymentfreq = $scope.event_details.payment_frequency;

                            } else if ($scope.event_details.event_recurring_payment_flag === 'N') {
                                $scope.recurringmchecked = false;
                                $scope.recurringmultiplepayment = 'N';
                                $scope.meventorderover = "";
                                $scope.meventquantityover = "";
                                $scope.mdepositamnt = "";
                                $scope.mnoofpayment = "";
                                $scope.mpaymntstartdate = "";
                                $scope.mpaymentfreq = "1";
                            }

                            if ($scope.event_details.event_onetime_payment_flag === 'Y') {
                                $scope.onetimemchecked = true;
                                $scope.onetimemultiplepayment = 'Y';
                            } else if ($scope.event_details.event_onetime_payment_flag === 'N') {
                                $scope.onetimemchecked = false;
                                $scope.onetimemultiplepayment = 'N';
                            }
                            
                            
                            if ($scope.event_details.discount.length > 0) {
                                    $scope.multiplediscountList = $scope.event_details.discount;
                                    $scope.multiplediscountListView = true;
                                    $scope.multiplediscountView = true;
                                    $scope.multiplediscountEditView = false;
                                } else {
                                    $scope.multiplediscountList = "";
                                    $scope.multiplediscountListView = false;
                                    $scope.multiplediscountView = false;
                                    $scope.multiplediscountEditView = false;
                                }
                                                        
                            $scope.cdiscountcode = "";
                            $scope.cdiscountamount = "";
                            $scope.mdiscnt_type = 'V';
                            $scope.selectedchildpayment = "";
                            $scope.selectedmdiscountindex = "";
                            $scope.multiplePaymentUpdateButton = false;
                            $scope.selectedeventlisttype = $scope.event_details.list_type;

                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedEventDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $localStorage.isAddedEventDetails = 'N';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedEventDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

        //open new input box for registration field addition
            $scope.showtextview = function () {
                $scope.showtext = true;
            };
            
        //confirm existing registration field deletion
            $scope.deletecol = function (registration_index) {
                $scope.event_registration_colunm_count = registration_index+1;
                $scope.event_registration_colunm_del = "event_registration_column_" + $scope.event_registration_colunm_count;
                $("#registerDeleteModal").modal('show');
                $("#registerDeletetitle").text('Delete');
                $("#registerDeletecontent").text("Are you sure want to delete register? ");
            };

         //Cancel existing registration field deletion
            $scope.registerdeleteCancel = function () {
                console.log('cancel delete');
                $location.path('/addevent');
            };


        //Adding new registration field
            $scope.savecolregistration = function (reg_type,event_register_field) {
                
                if ($scope.event_type === 'S') {
                    $scope.evnt_id = $scope.single_eventid;
                } else if ($scope.event_type === 'M') {
                    $scope.evnt_id = $scope.multiple_parent_id;
                }
                
                if (reg_type === 'add') {
                   $scope.registerfieldcount = $scope.registration_columns.length + 1;                   
                   $scope.registerfieldname =  "event_registration_column_" +$scope.registerfieldcount;
                   $scope.registerfieldmandatoryname =  "event_reg_col_"+$scope.registerfieldcount+"_mandatory_flag";
                   $scope.registerfieldvalue = $scope.reg_col_newvalue;
                   if ($scope.checked) {
                        $scope.event_mandatory_flag = 'Y';
                    } else {
                        $scope.event_mandatory_flag = 'N';
                    }
                } else if (reg_type === 'update') {
                       if (this.register_column_name !== "" ||this.register_column_name !== undefined ) {
                        $scope.registerfieldcount = event_register_field + 1;
                        $scope.registerfieldname = "event_registration_column_" + $scope.registerfieldcount;
                        $scope.registerfieldmandatoryname =  "event_reg_col_"+$scope.registerfieldcount+"_mandatory_flag";
                        $scope.registerfieldvalue = this.register_column_name;
                        if (this.mand_checked) {
                            $scope.event_mandatory_flag = 'Y';
                        } else {
                            $scope.event_mandatory_flag = 'N';
                        }
                    } else {
                        $scope.registerfieldname = "";
                        $scope.event_mandatory_flag= 'N';
                        $scope.registerfieldvalue = "";
                    }
                }

                

                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateEventRegistration',
                    data: {
                        "event_column_name": $scope.registerfieldname,
                        "event_mandatory_column_name":$scope.registerfieldmandatoryname,
                        "event_mandatory_column_value":$scope.event_mandatory_flag,
                        "event_column_value": $scope.registerfieldvalue,
                        "event_id": $scope.evnt_id,
                        "list_type": $scope.selectedeventlisttype,
                        "company_id": $localStorage.company_id,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.showtext = false;
                        $scope.reg_col_newvalue = "";
                        $scope.checked = false;
                        $scope.cancelsave($scope.registerfieldvalue);
                        $localStorage.isAddedEventDetails = 'Y';
                        $localStorage.currentpage = "editevents";
                        $localStorage.preview_eventcontent = $localStorage.event_details = $scope.event_details = response.data.msg;
                        $('#progress-full').hide();

                        if ($localStorage.firstlogin && $localStorage.firstlogin_event === 'Y') {
                            $localStorage.firstlogin_event = 'N';
                            angular.element(document.getElementById('sidetopbar')).scope().getTutorialMode();

                        } else if ($localStorage.firstlogin_event === 'N' || $localStorage.firstlogin_event === undefined) {
                            
                            $("#AddEventmessageModal").modal('show');
                            $("#AddEventmessagetitle").text('Success Message');
                            if(reg_type === 'add'){
                                $("#AddEventmessagecontent").text('Registration field successfully added');
                            }else if(reg_type === 'update'){
                               $("#AddEventmessagecontent").text('Registration field successfully updated'); 
                            }

                            if ($scope.event_details.event_type === "S" && ($scope.event_details.parent_id === "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                                $scope.eventadded = true;
                                $scope.EventName = $scope.event_details.event_title;
                                $scope.single_eventid = $scope.event_details.event_id;
                                $scope.event_status = $scope.event_details.event_status;
                                $scope.event_type = $scope.event_details.event_type;
                                if ($scope.event_details.event_begin_dt === '0000-00-00 00:00:00') {
                                    $scope.eventstartdate = "";
                                    $scope.eventstarttime = "";
                                } else {
                                    $scope.eventstartdate = $scope.formatserverdate($scope.event_details.event_begin_dt.split(" ")[0]);
                                    if ($scope.event_details.event_begin_dt.split(" ")[1] === "00:00:00") {
                                        $scope.eventstarttime = "";
                                    } else {
                                        $scope.eventstarttime = $scope.formatAMPM($scope.event_details.event_begin_dt);
                                    }
                                }
                                if ($scope.event_details.event_end_dt === '0000-00-00 00:00:00') {
                                    $scope.eventenddate = "";
                                    $scope.eventendtime = "";
                                } else {
                                    $scope.eventenddate = $scope.formatserverdate($scope.event_details.event_end_dt.split(" ")[0]);
                                    if ($scope.event_details.event_end_dt.split(" ")[1] === "00:00:00") {
                                        $scope.eventendtime = "";
                                    } else {
                                        $scope.eventendtime = $scope.formatAMPM($scope.event_details.event_end_dt);
                                    }
                                }
                                $scope.dateTimevalidationChange();
                                $scope.EventCost = $scope.event_details.event_cost;
                                $scope.EventCompCost = $scope.event_details.event_compare_price;
                                $scope.EventUrl = $scope.event_details.event_video_detail_url;
                                $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                                $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                                $scope.cropper.sourceImage = $scope.croppedEventImage;
                                $scope.singledesc = $scope.event_details.event_desc;
                                $scope.Eventregurl = $scope.event_details.event_url;
                                $scope.singlepaymentview = true;
                                $scope.discnt_type = 'V';
                                $scope.selectedeventlisttype = $scope.event_details.list_type;
                                
                                $scope.registration_columns = $scope.event_details.reg_columns;
                                $localStorage.PreviewEventRegcols = $scope.registration_columns;
                                if($scope.registration_columns.length >= 10) {
                                    $scope.regfieldexceeds = true;
                                } else {
                                    $scope.regfieldexceeds = false;
                                }
                                $scope.selectedregcolindex = "";

                            } else if ($scope.event_details.event_type === "M" && ($scope.event_details.parent_id !== "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                                if ($scope.event_details.event_status === 'P') {
                                    $scope.IsVisible = false;
                                    $scope.AddeventButton = true;
                                } else if ($scope.event_details.event_status === 'S') {
                                    if ($scope.initialmeventadd === 'Y') {
                                        $scope.IsVisible = true;
                                        $scope.AddeventButton = false;
                                    } else if ($scope.initialmeventadd === 'N') {
                                        $scope.IsVisible = false;
                                        $scope.AddeventButton = true;
                                    }
                                    $scope.initialmeventadd = 'N';
                                }
                                $scope.eventadded = true;
                                $scope.event_type = $scope.event_details.event_type;
                                $scope.parent_event_status = $scope.event_details.event_status;
                                $scope.multiple_parent_id = $scope.event_details.event_id;
                                $scope.EventCategory = $scope.event_details.event_title;
                                $scope.EventSubCategory = $scope.event_details.event_category_subtitle;
                                $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                                $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                                $scope.cropper.sourceImage = $scope.croppedEventImage;
                                $scope.PEventUrl = $scope.event_details.event_video_detail_url;
                                $scope.parentdesc = $scope.event_details.event_desc;
                                $scope.EventPregurl = $scope.event_details.event_url;
                                
                                $scope.multiplepaymentview = true;
                                if ($scope.event_details.child_events.length > 0) {
                                    $scope.IsVisible = false;
                                    $scope.multipleeventlist = $scope.event_details.child_events;
                                    $scope.childEventListView = true;
                                    $scope.childEventCardView = true;
                                    $scope.AddeventButton = true;
                                    $scope.childEventPaymentView = true;
                                    $scope.childpaymentCardView = true;
                                } else {
                                    $scope.multipleeventlist = "";
                                    $scope.childEventListView = false;
                                    $scope.childEventCardView = false;
                                    $scope.childEventPaymentView = false;
                                    $scope.childpaymentCardView = false;
                                }
                                
                                if ($scope.event_details.discount.length > 0) {
                                    $scope.multiplediscountList = $scope.event_details.discount;
                                    $scope.multiplediscountListView = true;
                                    $scope.multiplediscountView = true;
                                    $scope.multiplediscountEditView = false;
                                } else {
                                    $scope.multiplediscountList = "";
                                    $scope.multiplediscountListView = false;
                                    $scope.multiplediscountView = false;
                                    $scope.multiplediscountEditView = false;
                                }
                            
                                $scope.cdiscountcode = "";
                                $scope.cdiscountamount = "";
                                $scope.mdiscnt_type = 'V';
                                $scope.selectedmdiscountindex = "";
                                $scope.multiplePaymentUpdateButton = false;
                                $scope.selectedeventlisttype = $scope.event_details.list_type;
                                
                                $scope.registration_columns = $scope.event_details.reg_columns;
                                $localStorage.PreviewEventRegcols = $scope.registration_columns;
                                if($scope.registration_columns.length >= 10) {
                                    $scope.regfieldexceeds = true;
                                } else {
                                    $scope.regfieldexceeds = false;
                                }
                                $scope.selectedregcolindex = "";

                            }
                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedEventDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $localStorage.isAddedEventDetails = 'N';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedEventDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

             //delete existing registration field
            $scope.registerdeleteConfirm = function () {
                if ($scope.event_type === 'S') {
                    $scope.evnt_id = $scope.single_eventid;
                } else if ($scope.event_type === 'M') {
                    $scope.evnt_id = $scope.multiple_parent_id;
                }

                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteEventRegistration',
                    data: {
                        "event_column_name": $scope.event_registration_colunm_del,
                        "event_id": $scope.evnt_id,
                        "list_type": $scope.selectedeventlisttype,
                        "company_id": $localStorage.company_id,
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8'},
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedEventDetails = 'Y';
                        $localStorage.currentpage = "editevents";
                        $localStorage.preview_eventcontent = $localStorage.event_details = $scope.event_details = response.data.msg;
                        $('#progress-full').hide();

                        if ($localStorage.firstlogin && $localStorage.firstlogin_event === 'Y') {
                            $localStorage.firstlogin_event = 'N';
                            angular.element(document.getElementById('sidetopbar')).scope().getTutorialMode();

                        } else if ($localStorage.firstlogin_event === 'N' || $localStorage.firstlogin_event === undefined) {

                            $("#AddEventmessageModal").modal('show');
                            $("#AddEventmessagetitle").text('Success Message');
                            $("#AddEventmessagecontent").text('Registration field successfully deleted');

                            if ($scope.event_details.event_type === "S" && ($scope.event_details.parent_id === "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {


                                $scope.EventName = $scope.event_details.event_title;
                                $scope.single_eventid = $scope.event_details.event_id;
                                $scope.event_status = $scope.event_details.event_status;
                                $scope.event_type = $scope.event_details.event_type;
                                if ($scope.event_details.event_begin_dt === '0000-00-00 00:00:00') {
                                    $scope.eventstartdate = "";
                                    $scope.eventstarttime = "";
                                } else {
                                    $scope.eventstartdate = $scope.formatserverdate($scope.event_details.event_begin_dt.split(" ")[0]);
                                    if ($scope.event_details.event_begin_dt.split(" ")[1] === "00:00:00") {
                                        $scope.eventstarttime = "";
                                    } else {
                                        $scope.eventstarttime = $scope.formatAMPM($scope.event_details.event_begin_dt);
                                    }
                                }
                                if ($scope.event_details.event_end_dt === '0000-00-00 00:00:00') {
                                    $scope.eventenddate = "";
                                    $scope.eventendtime = "";
                                } else {
                                    $scope.eventenddate = $scope.formatserverdate($scope.event_details.event_end_dt.split(" ")[0]);
                                    if ($scope.event_details.event_end_dt.split(" ")[1] === "00:00:00") {
                                        $scope.eventendtime = "";
                                    } else {
                                        $scope.eventendtime = $scope.formatAMPM($scope.event_details.event_end_dt);
                                    }
                                }
                                $scope.dateTimevalidationChange();
                                $scope.EventCost = $scope.event_details.event_cost;
                                $scope.EventCompCost = $scope.event_details.event_compare_price;
                                $scope.EventUrl = $scope.event_details.event_video_detail_url;
                                $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                                $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                                $scope.cropper.sourceImage = $scope.croppedEventImage;
                                $scope.singledesc = $scope.event_details.event_desc;
                                $scope.Eventregurl = $scope.event_details.event_url;
                                $scope.singlepaymentview = true;
                                $scope.discnt_type = 'V';
                                $scope.selectedeventlisttype = $scope.event_details.list_type;
                                
                                $scope.registration_columns = $scope.event_details.reg_columns;
                                $localStorage.PreviewEventRegcols = $scope.registration_columns;
                                if($scope.registration_columns.length >= 10) {
                                    $scope.regfieldexceeds = true;
                                } else {
                                    $scope.regfieldexceeds = false;
                                }
                                $scope.selectedregcolindex = "";

                            } else if ($scope.event_details.event_type === "M" && ($scope.event_details.parent_id !== "" || $scope.event_details.parent_id === undefined || $scope.event_details.parent_id === null)) {

                                if ($scope.event_details.event_status === 'P') {
                                    $scope.IsVisible = false;
                                    $scope.AddeventButton = true;
                                } else if ($scope.event_details.event_status === 'S') {
                                    if ($scope.initialmeventadd === 'Y') {
                                        $scope.IsVisible = true;
                                        $scope.AddeventButton = false;
                                    } else if ($scope.initialmeventadd === 'N') {
                                        $scope.IsVisible = false;
                                        $scope.AddeventButton = true;
                                    }
                                    $scope.initialmeventadd = 'N';
                                }
                                $scope.eventadded = true;
                                $scope.event_type = $scope.event_details.event_type;
                                $scope.parent_event_status = $scope.event_details.event_status;
                                $scope.multiple_parent_id = $scope.event_details.event_id;
                                $scope.EventCategory = $scope.event_details.event_title;
                                $scope.EventSubCategory = $scope.event_details.event_category_subtitle;
                                $scope.event_picture_url = $scope.event_details.event_banner_img_url;
                                $scope.croppedEventImage = $scope.event_details.event_banner_img_url;
                                $scope.cropper.sourceImage = $scope.croppedEventImage;
                                $scope.PEventUrl = $scope.event_details.event_video_detail_url;
                                $scope.parentdesc = $scope.event_details.event_desc;
                                $scope.EventPregurl = $scope.event_details.event_url;
                                
                                $scope.multiplepaymentview = true;
                                if ($scope.event_details.child_events.length > 0) {
                                    $scope.IsVisible = false;
                                    $scope.multipleeventlist = $scope.event_details.child_events;
                                    $scope.childEventListView = true;
                                    $scope.childEventCardView = true;
                                    $scope.AddeventButton = true;
                                    $scope.childEventPaymentView = true;
                                    $scope.childpaymentCardView = true;
                                } else {
                                    $scope.multipleeventlist = "";
                                    $scope.childEventListView = false;
                                    $scope.childEventCardView = false;
                                    $scope.childEventPaymentView = false;
                                    $scope.childpaymentCardView = false;
                                }
                                
                                if ($scope.event_details.discount.length > 0) {
                                    $scope.multiplediscountList = $scope.event_details.discount;
                                    $scope.multiplediscountListView = true;
                                    $scope.multiplediscountView = true;
                                    $scope.multiplediscountEditView = false;
                                } else {
                                    $scope.multiplediscountList = "";
                                    $scope.multiplediscountListView = false;
                                    $scope.multiplediscountView = false;
                                    $scope.multiplediscountEditView = false;
                                }
                            
                                $scope.cdiscountcode = "";
                                $scope.cdiscountamount = "";
                                $scope.mdiscnt_type = 'V';
                                $scope.selectedmdiscountindex = "";
                                $scope.multiplePaymentUpdateButton = false;
                                $scope.selectedeventlisttype = $scope.event_details.list_type;
                                
                                $scope.registration_columns = $scope.event_details.reg_columns;
                                $localStorage.PreviewEventRegcols = $scope.registration_columns;
                                if($scope.registration_columns.length >= 10) {
                                    $scope.regfieldexceeds = true;
                                } else {
                                    $scope.regfieldexceeds = false;
                                }
                                $scope.selectedregcolindex = "";

                            }
                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedEventDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $localStorage.isAddedEventDetails = 'N';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedEventDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

        //edit function call for existing registration field
            $scope.editcol = function (event_registration_column,event_mandatory_check,ind) {
                
                this.register_column_name = event_registration_column;
                this.register_mandatory_check = event_mandatory_check;
                if(this.register_mandatory_check === 'Y'){
                     this.mand_checked = true;
                }else if(this.register_mandatory_check === 'N'){
                    this.mand_checked = false;
                }
                
                $scope.edit_col_view = false;
                $scope.col_view = false;
                $scope.selectedregcolindex = ind;
                
                $localStorage.preview_reg_fieldindex = ind;
                $localStorage.preview_reg_col_name_edit = event_registration_column; 
                if(event_mandatory_check === 'Y'){
                     $localStorage.PreviewOldRegFieldMandatory = true;
                }else if(event_mandatory_check === 'N'){
                    $localStorage.PreviewOldRegFieldMandatory = false;
                }
                 $localStorage.PreviewOldRegCancel = false;               
            }
          
            //Cancel existing registration field updating
            $scope.cancelsave = function (event_registration_column,event_mandatory_check) {
                $localStorage.PreviewOldRegCancel = true;
                $localStorage.preview_reg_fieldindex = '';
                $localStorage.preview_reg_col_name_edit = event_registration_column;
                this.register_column_name = event_registration_column; 
                if(event_mandatory_check === 'Y'){
                     this.mand_checked = true;
                }else if(event_mandatory_check === 'N'){
                    this.mand_checked = false;
                }
                $scope.edit_col_view = false;
                $scope.col_view = true;
                $scope.selectedregcolindex = "";
                if(event_mandatory_check === 'Y'){
                     $localStorage.PreviewOldRegFieldMandatory = true;
                }else if(event_mandatory_check === 'N'){
                    $localStorage.PreviewOldRegFieldMandatory = false;
                }
                $localStorage.PreviewNewRegCancel = true;
                $localStorage.PreviewNewRegFieldMandatory = false;
                $localStorage.PreviewNewRegistrationField = '';
            }

            //Cancel new registration field adding
            $scope.cancelnewsave = function () {
                $localStorage.PreviewNewRegCancel = true;
                $localStorage.PreviewNewRegFieldMandatory = false;
                $localStorage.PreviewNewRegistrationField = '';
                $scope.reg_col_newvalue = "";
                $scope.checked = false;
                $scope.showtext = false;
            }
            
            //Registration field drag and drop sorting
            $scope.dragregfieldCallback = function(drag_type,ind,reg_col_index) {
               if(drag_type === 'start'){
                   $scope.old_location_id = ind + 1;
                   $scope.old_location_col_index = reg_col_index;
               } else if(drag_type === 'end'){
                    setTimeout(function(){                   
                    for (var i=0;i<$scope.registration_columns.length;i++){ 
                       if($scope.registration_columns[i].reg_col_index === $scope.old_location_col_index){
                           $scope.new_location_id = +i + 1;
                    }
                    }
                    if($scope.old_location_id !== $scope.new_location_id){
                        $scope.update_regfieldSorting();
                    }
                }, 1000);          
            }
           }

//Registration field name update
            $scope.update_regfieldSorting = function(){
                $('#progress-full').show();
                if ($scope.event_type === 'S') {
                    $scope.evnt_id = $scope.single_eventid;
                } else if ($scope.event_type === 'M') {
                    $scope.evnt_id = $scope.multiple_parent_id;
                }
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'eventRegistrationSorting',
                    data: {
                        "old_location_id":$scope.old_location_id,
                        "new_location_id":$scope.new_location_id,
                        "event_id": $scope.evnt_id,
                        "list_type": $scope.selectedeventlisttype,
                        "company_id": $localStorage.company_id,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                       $localStorage.isAddedEventDetails = 'Y';
                       $localStorage.currentpage = "editevents";
                       $localStorage.preview_eventcontent = $localStorage.event_details = $scope.event_details = response.data.msg; 
                       $scope.registration_columns = $scope.event_details.reg_columns;
                       $('#progress-full').hide();
                    }else if (response.data.status === 'Expired') {
                        $localStorage.isAddedEventDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else {
                        $localStorage.isAddedEventDetails = 'N';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedEventDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
          
    }else {
            $location.path('/login');
        }
    
    }
    module.exports = AddEventController;
