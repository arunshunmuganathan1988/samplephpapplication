/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
MembershipDetailController.$inject=['$scope', '$location', '$http', '$localStorage', 'membershipTransferDetails', 'urlservice', '$filter', '$rootScope'];
function MembershipDetailController($scope, $location, $http, $localStorage, membershipTransferDetails, urlservice, $filter, $rootScope)
    {
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();           
            angular.element(document.getElementById('sidetopbar')).scope().getStripeStatus();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.sideBarHide = false;
            $rootScope.activepagehighlight = 'membership';
            tooltip.hide();            
            
            $scope.membershipdetailsview = true;
            $scope.participantinfoview = false;
            $scope.membershiphistoryview = false;
            $scope.paymenthistoryview = false;
            $scope.attendancedetailview = false;
            $scope.currentBuyerName = $localStorage.buyerName;
            $scope.currentParticipantName = $localStorage.participantName; 
            $scope.currentMembership_reg_id = $localStorage.currentMembershipregid;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.onetimecheckout = 0;
            $scope.ccyearlist = ["2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037"];
            $scope.ccmonthlist = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
            $scope.selected_category_index = $scope.selected_option_index = $scope.mem_category_id = $scope.mem_option_id = '';
            $scope.editStatus = $scope.showMemDateEdit = $scope.showMemFeeEdit = $scope.membership_updated = false;
            $scope.parfirstname = $scope.parlastname = "";
            $scope.buyerfirstname = $scope.buyerlastname = "";
            $scope.showTransferconfirm = false;
            $scope.showEditconfirm = false;
            $scope.showMemCategory = false; 
            $scope.selectOPtionConfirm = false;
            $scope.showParEdit = $scope.showCusParEdit = $scope.showMemFeeExists = $scope.showMemDateUpdate = $scope.showMemFeeUpdate = false;
            $scope.showMemSDateEdit =  $scope.showMemEDateEdit =$scope.showMemClassStartDateUpdate = $scope.showMemClassEndDateUpdate =  false;
            $scope.showFirstPaymntExists = $scope.showFirstPaymntDateUpdate = $scope.showFirstPaymntUpdate = false;    
            $scope.showFirstPaymntDateEdit = $scope.showFirstPaymntEdit = $scope.showFirstPaymntExists = false;
            $scope.showbutton = true;
            $scope.curr_first_paymnt_date_flag = $scope.first_paymnt_delete_flag = 'N';            
            $scope.actionValue = '';
            $localStorage.membershippagetype = '';
            $scope.membership_class_start_date = $scope.membership_class_end_date = ''; 
            $scope.call_back_from = 0;
            $scope.page_From = $localStorage.page_from;
            $scope.resume_date_flag = 'N';
            $scope.payment_pause_flag = 'N';           
            $scope.pausescheduled = 'N';
            $scope.wo_resumescheduled = 'Y';
            $scope.resume_lesser_than_pause = false;
            $scope.payment_pause_date = '';
            $scope.cancel_date_flag = 'N';
//            $scope.datetimeerror = false;
            $scope.cancelmodal = 'N';
            $scope.cancelled_date = '';
            $scope.transfer_status = false;
            $scope.creditmethod = 'MC';
            $scope.checknumber = '';
            $scope.allowmanualcreditrefund = false;
            $scope.pause_modal_flag = false;
            $scope.stripe_card = $scope.stripe_publish_key = $scope.window_hostname = '';
            $scope.stripe_publish_key = $localStorage.stripe_publish_key;
            
            
            
            if ($localStorage.wepay_level === 'P') {
                $scope.wepay_env = "production";
                $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
            } else if ($localStorage.wepay_level === 'S') {
                $scope.wepay_env = "stage";
                $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
            }

            WePay.set_endpoint($scope.wepay_env); //    CHANGE TO "PRODUCTION" WHEN LIVE
            // SHORTCUTS
            var d = document;
            d.id = d.getElementById,
                    valueById = function (id) {
                        return d.id(id).value;
                    };

            // FOR THOSE NOT USING DOM LIBRARIES
            var addEvent = function (e, v, f) {
                if (!!window.attachEvent) {
                    e.attachEvent('on' + v, f);
                } else {
                    e.addEventListener(v, f, false);
                }
            };

            // ATTACH THE EVENT TO THE DOM
            addEvent(d.id('cc-submit'), 'click', function () {
                $('#progress-full').show();
//                    var userName = [valueById('name')].join(' ');
                var userName = [valueById('firstName')] + ' ' + [valueById('lastName')];
                var user_first_name = valueById('firstName');
                var user_last_name = valueById('lastName');
                var email = valueById('email').trim();
                var postal_code = valueById('postal_code');
                response = WePay.credit_card.create({
                    "client_id": $localStorage.wepay_client_id,
                    "user_name": userName,
                    "email": valueById('email').trim(),
                    "cc_number": valueById('cc-number'),
                    "cvv": valueById('cc-cvv'),
                    "expiration_month": valueById('cc-month'),
                    "expiration_year": valueById('cc-year'),
                    "virtual_terminal": "web",
                    "address": {
                        "country": valueById('country'),
                        "postal_code": valueById('postal_code')
                    }
                }, function (data) {
                    if (data.error) {
                        $scope.onetimecheckout = 0;
                        $('#progress-full').hide();
                        console.log(data);
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Wepay Message');
                        $("#successmessagecontent").text(data.error_description);
                        // HANDLE ERROR RESPONSE
                    } else {
                        if ($scope.onetimecheckout == 0) {
                            $scope.onetimecheckout = 1;
//                                    $('#progress-full').show();
                            $scope.editMembershipDBPaymentMethod(data.credit_card_id, data.state, user_first_name, user_last_name, email, postal_code);
                            console.log(data);
                        }

                        // CALL YOUR OWN APP'S API TO SAVE THE TOKEN INSIDE THE DATA;
                        // SHOW A SUCCESS PAGE
                    }
                });
            });
            
            $scope.$on('$viewContentLoaded', function () {
                $('#birthstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
            });
            
            $scope.$on('$viewContentLoaded', function () {
                $('#paymenthistorystartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
            });
            
            
            $scope.$on('$viewContentLoaded', function () {
                $('#ncpaymntstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
            });
            
            $scope.$on('$viewContentLoaded', function () {
                $('#ncpaymntenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
            });
            
            $scope.$on('$viewContentLoaded', function () {
                $('#paymntstartdate').datepicker({ 
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true,
                    startDate: ('setStartDate', new Date())
                });
            });
            
                //BI-MONTHLY DATEPICKER FUNCTION CALL
            $scope.$on('$viewContentLoaded', function () {
                    $('#bimothlypaymntstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                    startDate: ('setStartDate', new Date()),
                    
                    beforeShowDay: function (date) {

                        if (date.getDate() === 1 || date.getDate() === 16)
                            return true;
                        else
                            return false;
                    }
                });

            });
            
            $scope.$on('$viewContentLoaded', function () {
                $('#paymntfirststartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true,
                    startDate: ('setStartDate', new Date())
                });
            });
            
            $scope.$on('$viewContentLoaded', function () {
                $('#paymntfirststartdatehistorymodal').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true,
                    startDate: ('setStartDate', new Date())
                });
            });
            
            $scope.$on('$viewContentLoaded', function () {
                $('#paymntstartdatehistorymodal').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true,
                    startDate: ('setStartDate', new Date())
                });
            });
            
               //BI-MONTHLY DATEPICKER FUNCTION CALL
            $scope.$on('$viewContentLoaded', function () {
                    $('#bimothlypaymntstartdatehistorymodal').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                    startDate: ('setStartDate', new Date()),
                    
                    beforeShowDay: function (date) {

                        if (date.getDate() === 1 || date.getDate() === 16)
                            return true;
                        else
                            return false;
                    }
                });

            });
           
            
            $scope.$on('$viewContentLoaded', function () {
                $('#paymntstartdateActivemodal').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true,
                    startDate: ('setStartDate', new Date())
                });
            });
            
               //BI-MONTHLY DATEPICKER FUNCTION CALL
            $scope.$on('$viewContentLoaded', function () {
                    $('#bimothlypaymntstartdateActivemodal').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                    startDate: ('setStartDate', new Date()),
                    
                    beforeShowDay: function (date) {

                        if (date.getDate() === 1 || date.getDate() === 16)
                            return true;
                        else
                            return false;
                    }
                });

            });
            
             
            $scope.$on('$viewContentLoaded', function () {
                     $('#paymntstartdateOnholdmodal').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true,
                    startDate: ('setStartDate', new Date())
                });
            });
            
               //BI-MONTHLY DATEPICKER FUNCTION CALL
            $scope.$on('$viewContentLoaded', function () {
                    $('#bimothlypaymntstartdateOnholdmodal').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                    startDate: ('setStartDate', new Date()),
                    
                    beforeShowDay: function (date) {

                        if (date.getDate() === 1 || date.getDate() === 16)
                            return true;
                        else
                            return false;
                    }
                });

            });
            
            
            $scope.formatDateToday = function(){
                var d = new Date,
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                return [year, month, day].join('-');
            };
            
            
            
            $scope.Membership_Detail = function () {
                $('body').removeClass('modal-open');
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'membersRegDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "selected_membership_id": $scope.currentMembership_reg_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                        $(".modal-backdrop").removeClass('modal-backdrop fade in');
                    if (response.data.status === 'Success') {
                        $scope.membership_detail = response.data.msg;
                        $scope.without_resume_date_flg = $scope.membership_detail.without_resume_date_flg;
//                        $scope.resumedate = $scope.membership_detail.resume_date;
                        $scope.cancelleddate = $scope.membership_detail.cancelled_date;
                        $scope.cancel_date_flg = $scope.membership_detail.cancel_date_flg;
                        $scope.pause_date_flg = $scope.membership_detail.scheduled_hold_flag;
                        $scope.scheduled_hold_date = $scope.membership_detail.scheduled_hold_date;
                        $scope.category_list = $scope.membership_detail.All;
                        $scope.memstatus = $scope.membership_detail.membership_status;
                        $scope.upcomingpaymentdates = (response.data.msg.payment_dates).reverse();//workkkkk	
                        $scope.canceldates = $scope.upcomingpaymentdates[$scope.upcomingpaymentdates.length-1];
                        $scope.membershipedit = angular.copy($scope.membership_detail);
                        $scope.editMembership_reg_id = $scope.membership_detail.membership_registration_id;
                        $scope.editMembership_opt_id = $scope.membership_detail.membership_option_id;
                        $scope.editMembership_id = $scope.membership_detail.membership_id;
                        $scope.editMembershipStatus = $scope.membership_detail.membership_status;
                        $scope.editMembershipCategoryTitle = $scope.membership_detail.membership_category_title;
                        $scope.editMembershipOption_title = $scope.membership_detail.membership_title;
                        $scope.name= $scope.membership_detail.buyer_name.split(" ");
                        $scope.firstname = $scope.name[0];
                        $scope.lastname = $scope.name[1];
                        $scope.email = $scope.membership_detail.buyer_email;
                        $scope.payment_pause_selection_dates = response.data.msg.payment_dates.reverse();
                        
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            $scope.Participant_Info = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'participantInfoDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "selected_membership_id": $scope.currentMembership_reg_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                    if (response.data.status === 'Success') {
                        $scope.participant_info = response.data.msg; 
                        
                        $scope.memeditparticipant = angular.copy($scope.participant_info);
                        $scope.parfirstname = $scope.memeditparticipant.membership_registration_column_1;
                        $scope.parlastname = $scope.memeditparticipant.membership_registration_column_2;
                        $scope.buyerfirstname = $scope.memeditparticipant.buyer_first_name;
                        $scope.buyerlastname = $scope.memeditparticipant.buyer_last_name;
                        $scope.parfirst_temp = $scope.parfirstname;
                        $scope.parlastname_temp = $scope.parlastname;
                        $scope.buyerfirst_temp = $scope.buyerfirstname;
                        $scope.buyerlastname_temp = $scope.buyerlastname;
                        var bchoose_date = $scope.memeditparticipant.membership_registration_column_3.split("-");                   
                        $scope.birthdate_curr = bchoose_date[1] + "/" + bchoose_date[2] + "/" + bchoose_date[0];
                        $scope.participant_birthdate_temp = $scope.birthdate_curr;
                        $scope.participant_phone_temp = $scope.memeditparticipant.buyer_phone;
                        $scope.participant_email_temp = $scope.memeditparticipant.buyer_email;
                        $scope.participant_street_temp = $scope.memeditparticipant.participant_street;
                        $scope.participant_city_temp = $scope.memeditparticipant.participant_city;
                        $scope.participant_state_temp = $scope.memeditparticipant.participant_state;
                        $scope.participant_country_temp = $scope.memeditparticipant.participant_country;
                        $scope.participant_zip_temp = $scope.memeditparticipant.participant_zip;
                        $scope.participant_col_4_temp = $scope.memeditparticipant.membership_registration_column_4;
                        $scope.participant_col_5_temp = $scope.memeditparticipant.membership_registration_column_5;
                        $scope.participant_col_6_temp = $scope.memeditparticipant.membership_registration_column_6;
                        $scope.participant_col_7_temp = $scope.memeditparticipant.membership_registration_column_7;
                        $scope.participant_col_8_temp = $scope.memeditparticipant.membership_registration_column_8;
                        $scope.participant_col_9_temp = $scope.memeditparticipant.membership_registration_column_9;
                        $scope.participant_col_10_temp = $scope.memeditparticipant.membership_registration_column_10;
                        
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.Payment_History = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'paymentHistoryDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id" : $scope.currentMembership_reg_id,
                        "category": 'membership'
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                    if (response.data.status === 'Success') {
                        $scope.mem_upcoming_history = response.data.msg.upcoming;
                        $scope.mem_payment_history = response.data.msg.history; 
                        $scope.membership_detail = response.data.reg_details.msg;                        
                        $scope.category_list = $scope.membership_detail.All;
                        $scope.cancelleddate = $scope.membership_detail.cancelled_date;
                        $scope.cancel_date_flg = $scope.membership_detail.cancel_date_flg;
                        $scope.transfer_details = response.data.transfer_details;                       
                        if($scope.transfer_details && $scope.transfer_details[0].transferred_status === 'Y'){
                            $scope.transfer_status = true;
                            $scope.transfer_date = $scope.transfer_details[0].transfer_date;
                        }else{
                            $scope.transfer_status = false;
                            $scope.transfer_date = "";
                        }
                        
                        $scope.membershipedit = angular.copy($scope.membership_detail);
                        if($scope.mem_upcoming_history && $scope.mem_upcoming_history.length > 0){
                            $scope.editMembership_reg_id = $scope.mem_upcoming_history[0].reg_id;
                            $scope.editMembership_opt_id = $scope.mem_upcoming_history[0].membership_option_id;
                            $scope.editMembership_id = $scope.mem_upcoming_history[0].membership_id;
                            $scope.editMembershipStatus = $scope.mem_upcoming_history[0].membership_status;
                        }else if($scope.mem_payment_history && $scope.mem_payment_history.length > 0){
                            $scope.editMembership_reg_id = $scope.mem_payment_history[0].reg_id;
                            $scope.editMembership_opt_id = $scope.mem_payment_history[0].membership_option_id;
                            $scope.editMembership_id = $scope.mem_payment_history[0].membership_id;
                            $scope.editMembershipStatus = $scope.mem_payment_history[0].membership_status;
                        }                                                
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                        
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
                        
            $scope.Membership_History = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'membershipHistoryDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "selected_membership_id" : $scope.currentMembership_reg_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                        $(".modal-backdrop").removeClass('modal-backdrop fade in');
                    if (response.data.status === 'Success') {
                        $scope.membership_history = response.data.msg;
                        $scope.mem_cancel_dates = (response.data.dates.payment_date).reverse();
                        $scope.memcanceldates = $scope.mem_cancel_dates[$scope.mem_cancel_dates.length-1];                        
                        $scope.transfer_details = response.data.transfer_details;                       
                        if($scope.transfer_details && $scope.transfer_details[0].transferred_status === 'Y'){
                            $scope.transfer_status = true;
                            $scope.transfer_date = $scope.transfer_details[0].transfer_date;
                        }else{
                            $scope.transfer_status = false;
                            $scope.transfer_date = "";
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            $scope.ShowNewEditMemDetails = function () {
                $scope.showTransferconfirm = false;
                $scope.showEditconfirm = true;
                $scope.showMemCategory = false;
                $scope.showbutton = true;
            };
            
            $scope.confirmTransfer = function () {
                $scope.showTransferconfirm = false;
                $scope.showEditconfirm = false;
                $scope.showMemCategory = true; 
            };
            
            $scope.cancelTransfer = function () {
                $scope.showTransferconfirm = false;
                $scope.showEditconfirm = false;
                $scope.showMemCategory = false;
            };
            
            $scope.cancelMemDetailUpdate = function () {
                $scope.showTransferconfirm = false;
                $scope.showEditconfirm = false;
                $scope.showMemCategory = false;
                $scope.showbutton = false;
                
                $scope.membershipedit.next_payment_date = $scope.membershipedit.next_payment_date_temp;
                $scope.membershipedit.payment_amount = $scope.membershipedit.recurring_amount_temp;
                $scope.showMemDateEdit = $scope.showMemFeeEdit = $scope.showMemFeeExists = false;
                $scope.showMemDateUpdate = $scope.showMemFeeUpdate = false; 
                $scope.showFirstPaymntExists = $scope.showFirstPaymntDateUpdate = $scope.showFirstPaymntUpdate = false;    
                $scope.showFirstPaymntDateEdit = $scope.showFirstPaymntEdit = $scope.showFirstPaymntExists = false;
                $scope.curr_first_paymnt_date_flag = $scope.first_paymnt_delete_flag = 'N';       
                $scope.closeDateEdit();
            };
            $scope.selectpaymentmethod = "CC";
            
            $scope.clearStripeCardData = function(){
                $scope.stripe_first_name = "";
                $scope.stripe_last_name = "";
                $scope.stripe_email = "";
                $scope.valid_stripe_card = false;
                if ($scope.stripe_card) {
                    $scope.stripe_card.clear();
                }
            };
            
            // STRIPE TOKEN CALL
            $scope.getStripeElements = function () {
                // Create a Stripe client
                $scope.valid_stripe_card = false;

                // Create an instance of Elements
                var elements =  $scope.stripe.elements();

                // Custom styling can be passed to options when creating an Element.
                // (Note that this demo uses a wider set of styles than the guide below.)
                var style = {
                    base: {
                        color: 'rgb(74,74,74)',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                            color: '#aab7c4'
                        }
                    },
                    invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                    }
                };

                // Create an instance of the card Element
                $scope.stripe_card = elements.create('card', {style: style});

                // Add an instance of the card Element into the `card-element` <div>
                $scope.stripe_card.mount('#mem-stripe-card-element');

                // Handle real-time validation errors from the card Element.
                $scope.stripe_card.addEventListener('change', function (event) {
                    var displayError = document.getElementById('mem-stripe-card-errors');
                    if (event.error) {
                        displayError.textContent = event.error.message;
                        $scope.valid_stripe_card = false;
                    } else {
                        displayError.textContent = '';
                        $scope.valid_stripe_card = true;
                    }
                });
            };
            
            $scope.openselectpaymentmethod = function(){
                $("#selectpaymentmethod-modal").modal('show');
            }
            $scope.showMemPaymentMethodModal = function () { // SHOW EDIT PAYMENT METHOD FORM
                if($scope.wepay_enabled){                   
                    $("#editMempaymentMethodModal").modal('show');
                    $("#editMempaymentMethodActiontitle").text('Edit Payment Method');
                }else if($scope.stripe_enabled){                    
                    $("#UpdateMemPaymentMethodModal").modal('show');
                    $("#UpdateMemPaymentMethodModaltitle").text('Edit Payment Method');
                    if($scope.stripe_publish_key){
                        $scope.stripe = Stripe($scope.stripe_publish_key);
                    }
                    $scope.clearStripeCardData();
                    $scope.getStripeElements();
                }
            };            
            $scope.catchselectpaymentmethod = function(){
                if($scope.selectpaymentmethod == "CC"){
                    $scope.showMemPaymentMethodModal();
                    return false;
                }else if($scope.wepay_enabled){
                    $scope.editMembershipDBPaymentMethod('','','','','','');
                }else{
                    $scope.updateStripePaymentMethodInDB('','', '', '');
                }
                
            }
            
            $scope.getMembershipCategory = function(membership_id){              
                $scope.membership_category_detail = "";
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getMembershipCategory',
                    params: {
                        "company_id": $localStorage.company_id,
                        "membership_id": membership_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'}

                    }).then(function (response) {
                        if (response.data.status === 'Success') {
                            $scope.membership_category_detail = response.data.msg;
                            $scope.mem_option_list = response.data.msg.membership_options;
                            $('#progress-full').hide();
                        } else if (response.data.status === 'Expired') {
                            console.log(response.data);
                            $('#progress-full').hide();
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Message');
                            $("#messagecontent").text(response.data.msg);
                            $scope.logout();
                        } else if (response.data.status === 'Version') {
                            $('#progress-full').hide();
                            $scope.handleFailure(response.data.msg);
                        } else {
                            $('#progress-full').hide();
                        }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });  
            }
            
            
            $scope.showMemCatOptUpdate = function () { 
                $scope.selected_category_index = '';
                $scope.mem_category_id = '';
                $scope.selected_option_index = '';
                $scope.mem_option_id = '';
                $scope.selected_membership_category = '';
                $scope.selected_membership_option = '';
                $scope.showTransferconfirm = true;
                $scope.showEditconfirm = false;
                $scope.showMemCategory = false; 
                
                for (i = 0; i < $scope.category_list.length; i++) {
                    if ($scope.category_list[i].membership_id === $scope.editMembership_id) {
                        $scope.selected_category_index = i;                        
                        $scope.mem_category_id = $scope.category_list[$scope.selected_category_index].membership_id;
                        $scope.selected_membership_category = $scope.category_list[i];
                    }
                }
                $scope.getMembershipCategory($scope.mem_category_id);
                
            };            
            
            $scope.showMemCancel = function () {
                $scope.selected_category_index = '';
                $scope.mem_category_id = '';
                $scope.selected_option_index = '';
                $scope.mem_option_id = '';
                $scope.selected_membership_category = '';
                $scope.selected_membership_option = '';
                $scope.cancelTransfer();
            };
            
            $scope.memcategorySelection = function () { //show category selection
                $scope.selected_option_index = $scope.mem_option_id = '';
                $scope.selectOPtionConfirm = false;               
                
                for (i = 0; i < $scope.category_list.length; i++) {
                    if ($scope.selected_category_index == i) {
//                        $scope.mem_option_list = $scope.category_list[$scope.selected_category_index].membership_options;
                        $scope.mem_category_id = $scope.category_list[$scope.selected_category_index].membership_id;
                        $scope.selected_membership_category = $scope.category_list[$scope.selected_category_index];
                    }
                }
                $scope.getMembershipCategory($scope.mem_category_id);
            };
            
            $scope.memoptionSelection = function () { 
                $scope.selected_membership_option = '';
                $scope.selectOPtionConfirm = true;
                for (i = 0; i < $scope.mem_option_list.length; i++) {
                    if ($scope.selected_option_index == i) {
                        $scope.mem_option_id = $scope.mem_option_list[$scope.selected_option_index].membership_option_id;
                        $scope.selected_membership_option = $scope.mem_option_list[$scope.selected_option_index];
                    }
                }
            };
            
            $scope.transferRegDetails = function () {  
                $localStorage.detail_page_from = $localStorage.page_from;
                $localStorage.page_from = '';
                $localStorage.membershippagetype = 'transfer';
                $scope.transfer_mem_detail = [];
                $scope.transfer_mem_detail.push({'selected_membership_category':$scope.membership_category_detail},{'selected_membership_option':$scope.selected_membership_option});
                membershipTransferDetails.addTransferMembershipDetail($scope.transfer_mem_detail);
                $localStorage.membershiptransferlocaldata = '';
                $location.path('/managemembership');
            };
            
            $scope.serverdateformatfilter = function (date) {
                if (date !== undefined && date !== '' && date !== null) {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }
            };
            
            $scope.localdateformatfilter = function (date) {
                if (date !== '0000-00-00' && date !== '' && date !== null) {
                    var datePart = date.split("-");
                    var day = datePart[2];
                    var year = datePart[0];
                    var month = datePart[1];
                    var datevalue = month + '/' + day + '/' + year;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }
            };
            
            $scope.showMemStatusEdit = function () {
                $scope.editStatus = true;
            };
            
            $scope.showMemStatusEditCancel = function (exist_status) {
                $scope.wo_resumescheduled = 'Y';
                $scope.payment_pause_date = '';
                $scope.payment_resume_date = '';
                $scope.memstatus = exist_status;
                $scope.editStatus = false;
                $scope.cancelamount();
                $scope.canceldate();
                $scope.cancelMSdate();
                $scope.showbutton = true;
                $scope.pausescheduled = 'N';
                $scope.disablepauseoptions = false;
                $scope.disableresumeoptions = false;  
                
            };
            
            $scope.showParticipantEdit = function () {
                $scope.resetEditParticipant();
                $scope.showParEdit = true;
                $scope.showCusParEdit = false;
            };
            
            $scope.showCustomParticipantEdit = function () {
                $scope.resetEditParticipant();
                $scope.showParEdit = false;
                $scope.showCusParEdit = true;
            };
            
            $scope.performMemAction = function (a) { //Show list of Actions 
                $scope.pause_modal_flag = false;
                if (a === 'C') {
                    //cancel
                    $scope.actionValue = 0;
                    $("#membershipCancelModal").modal('show');
                    $("#membershipCanceltitle").text('Update Membership Status');
                } else if (a === 'P') {
                    //pause
                    $scope.actionValue = 1;
                    if($scope.membership_detail.payment_type ==='R'){                        
                        $scope.editdate($scope.membership_detail.next_payment_date);
                        $scope.editamount($scope.membership_detail.payment_amount);
                        $scope.showMemDateEdit = false; 
                        $scope.showMemFeeEdit = false; 
                        if ($scope.payment_pause_selection_dates.length <= 1)
                        {
                            $scope.pausescheduled = 'N';
                            $scope.disablepauseoptions = true;
                            $scope.disableresumeoptions = true;
                        }
                        else
                        {
                            $scope.pausescheduled = 'N';
                            $scope.disablepauseoptions = false;
                            $scope.disableresumeoptions = false;   
                        }
                        $scope.wo_resumescheduled = 'Y';
                        $scope.payment_pause_date = $scope.payment_pause_selection_dates[0];
                        $scope.payment_resume_date = $scope.payment_pause_selection_dates[0];
                    }else{
                        $scope.pausescheduled = 'N';
                        $scope.wo_resumescheduled = 'Y';
                        $scope.payment_pause_date = '';
                        $scope.payment_resume_date = '';
                    }
                    $("#membershipHoldModal").modal('show');
                    $("#membershipHoldtitle").text('Update Membership Status');
                    
                } else if (a === 'R') {
                    //active
                    $scope.actionValue = 2;
                    if($scope.membership_detail.membership_structure ==='OE' && $scope.membership_detail.payment_type ==='R'){                        
                        $scope.editdate($scope.membership_detail.next_payment_date);
                        $scope.editamount($scope.membership_detail.payment_amount);
                        $scope.showMemDateEdit = false; 
                        $scope.showMemFeeEdit = false; 
                    }
                    $("#memActiveModal").modal('show');
                    $("#memActivetitle").text('Update Membership Status');
                }
            };
                        
            $scope.actionMemConfirm = function (value, resume_date_flag, payment_pause_flag, payment_pause_date) {//work
                if (value === 0) {
//                    var canceldates = new Date($scope.canceldates);
//                    $scope.canceldates = canceldates.getFullYear() + '-' + ('0' + (canceldates.getMonth() + 1)).slice(-2) + '-' + ('0' + canceldates.getDate()).slice(-2);
//
//                    if ($scope.cancel_date_flag !== 'N' && ($scope.canceldates < $scope.resumedate)) {
//                        $("#successmessageModal").modal('show');
//                        $("#successmessagetitle").text('Message');
//                        $("#successmessagecontent").text("Cancellation Date should not be lesser than Scheduled Resume date");
//                    } else {
                        $scope.cancel_date_flag = $scope.cancelmodal;
                        if ($scope.cancel_date_flag === 'Y') {
                            $scope.cancelled_date = $scope.canceldates;
                        }
                        if ($scope.pause_date_flg === 'Y') {
                            $scope.payment_pause_date = $scope.scheduled_hold_date;
                        }
                        $scope.payment_pause_flag = $scope.pausescheduled;
                        $scope.resume_date_flag = resume_date_flag;
                        $scope.updateMembershipStatus('C');
//                    }
                }
                if (value === 1) {
                    
//                    $scope.resumedate = $scope.showMemDateUpdate ? $scope.newpaymentdatevalue : $scope.membership_detail.next_payment_date;
//                    if ($scope.cancelleddate < $scope.resumedate) {
//                        $("#successmessageModal").modal('show');
//                        $("#successmessagetitle").text('Message');
//                        $("#successmessagecontent").text("Resume Date should not be greater than Scheduled Cancelled date");
//                    } else {
                    if($scope.cancel_date_flg === 'Y'){
                        $scope.cancelled_date = $scope.cancelleddate;
                    }else{
                        $scope.cancelled_date = '';
                    }
                    if ($scope.pause_date_flg === 'Y' && $scope.pausescheduled === 'N') {
                        $scope.payment_pause_date = $scope.scheduled_hold_date;
                    }
                        $scope.cancel_date_flag = 'N';
                        $scope.resume_date_flag = resume_date_flag;
                    $scope.payment_pause_flag = $scope.pausescheduled;
                        $scope.updateMembershipStatus('P');
//                    }
                }
                if (value === 2) {
//                    $scope.resumedate = $scope.showMemDateUpdate ? $scope.newpaymentdatevalue : $scope.membership_detail.next_payment_date;
//                    if ($scope.cancelleddate < $scope.resumedate) {
//                        $("#successmessageModal").modal('show');
//                        $("#successmessagetitle").text('Message');
//                        $("#successmessagecontent").text("Resume Date should not be greater than Scheduled Cancelled date");
//                    } else {
                        if ($scope.pause_date_flg === 'Y') {
                            $scope.payment_pause_date = $scope.scheduled_hold_date;
                        } 
                        $scope.payment_pause_flag = $scope.pausescheduled;
                        $scope.cancelled_date = '';
                        $scope.cancel_date_flag = 'N';
                        $scope.resume_date_flag = resume_date_flag;
                        $scope.updateMembershipStatus('R');
//                    }
                }
            };
            
            
            $scope.updateMembershipStatus = function (status) { 
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateMembershipParticipantStatus',
                    data: {
                        "company_id": $localStorage.company_id,
                        "membership_id": $scope.editMembership_id,
                        "membership_reg_id": $scope.editMembership_reg_id,
                        "membership_option_id": $scope.editMembership_opt_id,
                        "membership_status": status,
                        "category_title": $scope.editMembershipCategoryTitle,
                        "option_title": $scope.editMembershipOption_title,
                        "updated_amount": $scope.showMemFeeUpdate ? $scope.newpaymentvalue : $scope.membership_detail.payment_amount,
                        "next_date": $scope.showMemDateUpdate ? $scope.newpaymentdatevalue : $scope.membership_detail.next_payment_date,
                        "pay_now_flg": $scope.curr_date_flag,
                        "resume_without_date_flg": $scope.resume_date_flag,
                        "scheduled_cancel_flag": $scope.cancel_date_flag,
                        "cancellation_date": $scope.cancelled_date,
                        "upgrade_status": $localStorage.upgrade_status,
                        "payment_pause_date":$scope.payment_pause_date,
                        "payment_pause_flag":$scope.payment_pause_flag,
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        if(!$scope.pause_modal_flag){
                            $scope.resettabbarclass();
                            $('#li-membershipdetails').addClass('active-event-list').removeClass('event-list-title'); 
                            $('.membershipdetail-tabtext-1').addClass('greentab-text-active');
                        }else{
                            $scope.membershiphistory();
                        }
                        $scope.membership_detail = response.data.reg_details.msg;
                        $scope.cancel_date_flg = $scope.membership_detail.cancel_date_flg;
                        $scope.pause_date_flg = $scope.membership_detail.scheduled_hold_flag;
                        $scope.scheduled_hold_date = $scope.membership_detail.scheduled_hold_date;
                        $scope.without_resume_date_flg = $scope.membership_detail.without_resume_date_flg;
                        $scope.cancelleddate = $scope.membership_detail.cancelled_date;
                        $scope.category_list = $scope.membership_detail.All;
                        $scope.memstatus = $scope.membership_detail.membership_status;
                        $scope.upcomingpaymentdates = ($scope.membership_detail.payment_dates).reverse();//work	
                        if($scope.cancel_date_flg === 'Y' ){
                           $scope.cancelmodal = 'N';
                        }
                        if ($scope.pause_date_flg === 'Y')
                        {
                            $scope.payment_pause_flag = 'N';
                        }
                        $scope.canceldates = $scope.upcomingpaymentdates[$scope.upcomingpaymentdates.length-1];
                        $scope.membershipedit = angular.copy($scope.membership_detail);
                        $scope.editMembership_reg_id = $scope.membership_detail.membership_registration_id;
                        $scope.editMembership_opt_id = $scope.membership_detail.membership_option_id;
                        $scope.editMembership_id = $scope.membership_detail.membership_id;
                        $scope.editMembershipStatus = $scope.membership_detail.membership_status;
                        $scope.editMembershipCategoryTitle = $scope.membership_detail.membership_category_title;
                        $scope.editMembershipOption_title = $scope.membership_detail.membership_title;
                        $scope.showMemFeeExists = $scope.showFirstPaymntExists = false;
                        $scope.showMemStatusEditCancel($scope.editMembershipStatus);
                   
                } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $scope.memstatus = $scope.membership_detail.membership_status;
                        if($scope.actionValue !== 0){
                        $scope.showMemStatusEditCancel($scope.membership_detail.membership_status);
                    }
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            // UPDATE PARTICIPANT NAME ON THE TABLE LISTING
            $scope.updateMemParticipantName = function (fname, lname, bfname, blname, birthdate, phone, email, street, city, state, country, zip, value4, value5, value6, value7, value8, value9, value10) {
                $('#progress-full').show();
                
                if(birthdate){
                    var value_birth_date = birthdate.split("/");
                    $scope.birth_day = value_birth_date[2] + "-" + value_birth_date[0] + "-" + value_birth_date[1];
                }else{
                    $scope.birth_day = "";
                }
                    
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateMembershipParticipantName',
                    data: {
                        "company_id": $localStorage.company_id,
                        "membership_id": $scope.editMembership_id,
                        "membership_reg_id": $scope.currentMembership_reg_id,
                        "membership_option_id": $scope.editMembership_opt_id,
                        "membership_status": $scope.editMembershipStatus,
                        "pfirst_name": fname,
                        "plast_name": lname,
                        "bfirst_name": bfname,
                        "blast_name": blname,
                        "participant_street": street,
                        "participant_city": city,
                        "participant_state": state,
                        "participant_zip": zip,
                        "participant_country": country,
                        "participant_birth_date": $scope.birth_day,
                        "participant_phone": phone,
                        "participant_email": email,
                        "membership_registration_Cvalue_4": value4,
                        "membership_registration_Cvalue_5": value5,
                        "membership_registration_Cvalue_6": value6,
                        "membership_registration_Cvalue_7": value7,
                        "membership_registration_Cvalue_8": value8,
                        "membership_registration_Cvalue_9": value9,
                        "membership_registration_Cvalue_10": value10
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.showParEdit = $scope.showCusParEdit = false;
                        
                        $scope.participant_info = response.data.participant_details.msg; 
                        
                        $scope.memeditparticipant = angular.copy($scope.participant_info);
                        $scope.parfirstname = $scope.memeditparticipant.membership_registration_column_1;
                        $scope.parlastname = $scope.memeditparticipant.membership_registration_column_2;
                        $scope.buyerfirstname = $scope.memeditparticipant.buyer_first_name;
                        $scope.buyerlastname = $scope.memeditparticipant.buyer_last_name;
                        $scope.parfirst_temp = $scope.parfirstname;
                        $scope.parlastname_temp = $scope.parlastname;
                        $scope.buyerfirst_temp = $scope.buyerfirstname;
                        $scope.buyerlastname_temp = $scope.buyerlastname;
                        var bchoose_date = $scope.memeditparticipant.membership_registration_column_3.split("-");                   
                        $scope.birthdate_curr = bchoose_date[1] + "/" + bchoose_date[2] + "/" + bchoose_date[0];
                        $scope.participant_birthdate_temp = $scope.birthdate_curr;
                        $scope.participant_phone_temp = $scope.memeditparticipant.buyer_phone;
                        $scope.participant_email_temp = $scope.memeditparticipant.buyer_email;
                        $scope.participant_street_temp = $scope.memeditparticipant.participant_street;
                        $scope.participant_city_temp = $scope.memeditparticipant.participant_city;
                        $scope.participant_state_temp = $scope.memeditparticipant.participant_state;
                        $scope.participant_country_temp = $scope.memeditparticipant.participant_country;
                        $scope.participant_zip_temp = $scope.memeditparticipant.participant_zip;
                        $scope.participant_col_4_temp = $scope.memeditparticipant.membership_registration_column_4;
                        $scope.participant_col_5_temp = $scope.memeditparticipant.membership_registration_column_5;
                        $scope.participant_col_6_temp = $scope.memeditparticipant.membership_registration_column_6;
                        $scope.participant_col_7_temp = $scope.memeditparticipant.membership_registration_column_7;
                        $scope.participant_col_8_temp = $scope.memeditparticipant.membership_registration_column_8;
                        $scope.participant_col_9_temp = $scope.memeditparticipant.membership_registration_column_9;
                        $scope.participant_col_10_temp = $scope.memeditparticipant.membership_registration_column_10;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.resetForm = function () { // RESET CREDIT CARD FORM
                $scope.firstname = '';
                $scope.lastname = '';
                $scope.email = '';
                $scope.phone = '';
                $scope.cardnumber = '';
                $scope.$parent.ccmonth = '';
                $scope.$parent.ccyear = '';
                $scope.cvv = '';
                $scope.$parent.country = '';
                $scope.postal_code = '';
                $scope.paymentform.$setPristine();
                $scope.paymentform.$setUntouched();               

            };
            
            $scope.editMembershipDBPaymentMethod = function (cc_id, cc_state, user_first_name, user_last_name, email, postal_code) { // UPDATE CREDIT CARD DETAILS
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatePaymentMethod',
                    data: {
                        company_id: $localStorage.company_id,
                        reg_id: $scope.editMembership_reg_id,
                        buyer_first_name: user_first_name,
                        buyer_last_name: user_last_name,
                        email: email,
                        postal_code: postal_code,
                        cc_id: cc_id,
                        cc_state: cc_state,
                        category_type: 'M',
                        page_key: 'MD',     // MEMBERSHIP DETAIL
                        type:$scope.selectpaymentmethod
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $("#editMempaymentMethodModal").modal('hide');
                        $("#editMempaymentMethodActiontitle").text('');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text('Payment method successfully updated.');
                        $scope.onetimecheckout = 0;
                        $scope.resetForm();
                        
                        $scope.membership_detail = response.data.reg_details.msg;
                        $scope.category_list = $scope.membership_detail.All;
                        $scope.memstatus = $scope.membership_detail.membership_status;
                        
                        $scope.membershipedit = angular.copy($scope.membership_detail);
                        $scope.editMembership_reg_id = $scope.membership_detail.membership_registration_id;
                        $scope.editMembership_opt_id = $scope.membership_detail.membership_option_id;
                        $scope.editMembership_id = $scope.membership_detail.membership_id;
                        $scope.editMembershipStatus = $scope.membership_detail.membership_status;
                        $scope.editMembershipCategoryTitle = $scope.membership_detail.membership_category_title;
                        $scope.editMembershipOption_title = $scope.membership_detail.membership_title;
                        $scope.name= $scope.membership_detail.buyer_name.split(" ");
                        $scope.firstname = $scope.name[0];
                        $scope.lastname = $scope.name[1];
                        $scope.email = $scope.membership_detail.buyer_email;
                        
                    } else if (response.data.status === 'Expired') {
                        $("#editMempaymentMethodModal").modal('hide');
                        $("#editMempaymentMethodActiontitle").text('');
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $("#editMempaymentMethodModal").modal('hide');
                        $("#editMempaymentMethodActiontitle").text('');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        if (response.data.error) {
                            $("#successmessagetitle").text(response.data.error);
                            $("#successmessagecontent").text(response.data.error_description);
                        } else {
                            $("#successmessagetitle").text('Message');
                            $("#successmessagecontent").text(response.data.msg);
                        }
                        $scope.onetimecheckout = 0;
                    }
                }, function (response) {
                    $("#editMempaymentMethodModal").modal('hide');
                    $("#editMempaymentMethodActiontitle").text('');
                    console.log(response.data);
                    $scope.handleError(response.data);
                    $scope.onetimecheckout = 0;
                });
            };
            
            
            
            $scope.resetEditParticipant = function () {
                $scope.showParEdit = $scope.showCusParEdit = false;
                $scope.parfirstname = $scope.parfirst_temp;
                $scope.parlastname = $scope.parlastname_temp;
                $scope.buyerfirstname = $scope.buyerfirst_temp;
                $scope.buyerlastname = $scope.buyerlastname_temp;
                $scope.birthdate_curr = $scope.participant_birthdate_temp;
                $scope.memeditparticipant.buyer_phone = $scope.participant_phone_temp;
                $scope.memeditparticipant.buyer_email = $scope.participant_email_temp;
                $scope.memeditparticipant.participant_street = $scope.participant_street_temp;
                $scope.memeditparticipant.participant_city = $scope.participant_city_temp;
                $scope.memeditparticipant.participant_state = $scope.participant_state_temp;
                $scope.memeditparticipant.participant_country = $scope.participant_country_temp;
                $scope.memeditparticipant.participant_zip = $scope.participant_zip_temp;
                $scope.memeditparticipant.membership_registration_column_4 = $scope.participant_col_4_temp;
                $scope.memeditparticipant.membership_registration_column_5 = $scope.participant_col_5_temp;
                $scope.memeditparticipant.membership_registration_column_6 = $scope.participant_col_6_temp;
                $scope.memeditparticipant.membership_registration_column_7 = $scope.participant_col_7_temp;
                $scope.memeditparticipant.membership_registration_column_8 = $scope.participant_col_8_temp;
                $scope.memeditparticipant.membership_registration_column_9 = $scope.participant_col_9_temp;
                $scope.memeditparticipant.membership_registration_column_10 = $scope.participant_col_10_temp;
            };
            
            
            $scope.editamount = function (value) {
                $scope.showFirstPaymntEdit = $scope.showFirstPaymntExists = $scope.showFirstPaymntUpdate = false;
                $scope.showFirstPaymntDateEdit = $scope.showFirstPaymntDateUpdate = false;
                $scope.showMemFeeExists = false;
                
                $scope.membershipedit.recurring_amount_temp = $scope.membership_detail.payment_amount;
                $scope.membershipedit.next_payment_date_temp = $scope.membership_detail.next_payment_date;
                $scope.mem_payment_frequency = $scope.membership_detail.payment_frequency;
                $scope.mem_prorate_first_payment_flg = $scope.membership_detail.prorate_first_payment_flg;
                $scope.custom_payment_frequency = $scope.membership_detail.custom_recurring_frequency_period_type;
                $scope.amounterrortext = "";
                $scope.current_amount_value = value;
                $scope.showMemFeeEdit = true;                
                $scope.paymentcurrentvalue = angular.copy($scope.current_amount_value);
                $scope.membershipedit.payment_amount = $scope.paymentcurrentvalue;
                if($scope.showMemFeeUpdate || $scope.showMemDateUpdate || $scope.showMemClassStartDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
            }
            
            $scope.cancelamount = function () {
                $scope.showMemFeeEdit = $scope.showMemFeeExists = false;
                $scope.membershipedit.payment_amount = $scope.paymentcurrentvalue;
                $scope.updateamount($scope.membershipedit.payment_amount);
                $scope.showMemFeeUpdate = false;
                $scope.amounterrortext = "";
                if($scope.showMemFeeUpdate || $scope.showMemDateUpdate || $scope.showMemClassStartDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
                return;
            };

            $scope.updateamount = function (value) {
                $scope.showMemFeeUpdate = true;
                if($scope.showMemFeeUpdate || $scope.showMemDateUpdate || $scope.showMemClassStartDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
                if (!value || parseInt(value) < 5) {
                    $scope.showMemFeeExists = true;
                    $scope.amounterrortext = "Amount Should Be Greater than Or Equal To 5"+$scope.wp_currency_symbol;                    
                    return;
                } else {
                    $scope.showMemFeeExists = false;
                }
                $scope.newpaymentvalue = value;  
            };

            $scope.editdate = function (value) {  
                $scope.showFirstPaymntEdit = $scope.showFirstPaymntExists = $scope.showFirstPaymntUpdate = false;
                $scope.showFirstPaymntDateEdit = $scope.showFirstPaymntDateUpdate = false;
                $scope.current_date_value = value;                
                $scope.showMemDateEdit = true;  
                $scope.mem_payment_frequency = $scope.membership_detail.payment_frequency;
                $scope.mem_prorate_first_payment_flg = $scope.membership_detail.prorate_first_payment_flg;
                $scope.custom_payment_frequency = $scope.membership_detail.custom_recurring_frequency_period_type;
                $scope.paymentcurrentdatevalue = angular.copy($scope.current_date_value); 
                $scope.membershipedit.next_payment_date = $scope.localdateformatfilter(value);
                $scope.curr_date_new = $scope.formatDateToday();
                if($scope.membership_detail.payment_start_date >= $scope.curr_date_new){
                    $('#paymntstartdate').datepicker('setStartDate', $scope.localdateformatfilter($scope.membership_detail.payment_start_date));
                    $('#bimothlypaymntstartdate').datepicker('setStartDate', $scope.localdateformatfilter($scope.membership_detail.payment_start_date));
                }else{
                    $('#paymntstartdate').datepicker('setStartDate',new Date());
                    $('#bimothlypaymntstartdate').datepicker('setStartDate',new Date());
                } 
                if($scope.showMemFeeUpdate || $scope.showMemDateUpdate || $scope.showMemClassStartDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
            };
            
            $scope.canceldate = function () {
                $scope.showMemDateEdit = false;
                $scope.membershipedit.next_payment_date = $scope.paymentcurrentdatevalue;
                $scope.updatedate($scope.membershipedit.next_payment_date);                
                $scope.showMemDateUpdate = false;
                if($scope.showMemFeeUpdate || $scope.showMemDateUpdate || $scope.showMemClassStartDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
                return;
            };

            $scope.updatedate = function (value) {
                $scope.showMemDateUpdate = true;
                if($scope.showMemFeeUpdate || $scope.showMemDateUpdate || $scope.showMemClassStartDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
                $scope.curr_date_new = $scope.formatDateToday();

                if ($scope.mem_payment_frequency === 'B') {
                    $scope.updatedpaymentdate_value = value;
                    var choose_date = $scope.updatedpaymentdate_value.split("/");
                    $scope.new_date = choose_date[2] + "-" + choose_date[0] + "-" + choose_date[1];
                    $scope.final_next_payment_date = $scope.new_date;
                    if (!value) {                        
                        if($scope.paymentcurrentdatevalue >= $scope.membership_detail.payment_start_date){
                            $scope.membershipedit.next_payment_date = $scope.localdateformatfilter($scope.paymentcurrentdatevalue);
                            $scope.newpaymentdatevalue = $scope.paymentcurrentdatevalue;
                        }else{
                            $scope.membershipedit.next_payment_date = $scope.localdateformatfilter($scope.membership_detail.payment_start_date);
                            $scope.newpaymentdatevalue = $scope.membership_detail.payment_start_date;
                        }                         
                        if ($scope.newpaymentdatevalue === $scope.curr_date_new){                          
                            $scope.curr_date_flag = 'Y';
                        } else {                          
                            $scope.curr_date_flag = 'N';
                        }
                    } else { 
                        if($scope.final_next_payment_date >= $scope.membership_detail.payment_start_date){
                            $scope.membershipedit.next_payment_date = $scope.localdateformatfilter($scope.final_next_payment_date);
                            $scope.newpaymentdatevalue = $scope.final_next_payment_date;
                        }else{
                            $scope.membershipedit.next_payment_date = $scope.localdateformatfilter($scope.membership_detail.payment_start_date);
                            $scope.newpaymentdatevalue = $scope.membership_detail.payment_start_date;
                        }                         
                        if ($scope.newpaymentdatevalue === $scope.curr_date_new){                          
                            $scope.curr_date_flag = 'Y';
                        } else {                          
                            $scope.curr_date_flag = 'N';
                        }
                    }
                }else{
                    $scope.updatedpaymentdate_value = value;
                    var choose_date = $scope.updatedpaymentdate_value.split("/");                   
                    $scope.new_date = choose_date[2] + "-" + choose_date[0] + "-" + choose_date[1];
                    $scope.final_next_payment_date = $scope.new_date;
                     if (!value) {
                        if($scope.paymentcurrentdatevalue >= $scope.membership_detail.payment_start_date){
                            $scope.membershipedit.next_payment_date = $scope.localdateformatfilter($scope.paymentcurrentdatevalue);
                            $scope.newpaymentdatevalue = $scope.paymentcurrentdatevalue;
                        }else{
                            $scope.membershipedit.next_payment_date = $scope.localdateformatfilter($scope.membership_detail.payment_start_date);
                            $scope.newpaymentdatevalue = $scope.membership_detail.payment_start_date;
                        }                         
                        if ($scope.newpaymentdatevalue === $scope.curr_date_new){                          
                            $scope.curr_date_flag = 'Y';
                        } else {                          
                            $scope.curr_date_flag = 'N';
                        }
                    } else {
                        if($scope.final_next_payment_date >= $scope.membership_detail.payment_start_date){
                            $scope.membershipedit.next_payment_date = $scope.localdateformatfilter($scope.final_next_payment_date);
                            $scope.newpaymentdatevalue = $scope.final_next_payment_date;
                        }else{
                            $scope.membershipedit.next_payment_date = $scope.localdateformatfilter($scope.membership_detail.payment_start_date);
                            $scope.newpaymentdatevalue = $scope.membership_detail.payment_start_date;
                        }                         
                        if ($scope.newpaymentdatevalue === $scope.curr_date_new){                          
                            $scope.curr_date_flag = 'Y';
                        } else {                          
                            $scope.curr_date_flag = 'N';
                        }
                          
                    }
                }

            };
            
            $scope.editMSdate = function (actual_ms_start_date) {
                $scope.current_msdate_value = actual_ms_start_date;                
                $scope.showMemSDateEdit = true;                
                $scope.paymentcurrentmsdatevalue = angular.copy($scope.current_msdate_value);  
                $scope.membership_class_start_date = $scope.localdateformatfilter(actual_ms_start_date);
                $scope.edit_mem_start_date = $scope.serverdateformatfilter($scope.membership_class_start_date);
                if($scope.membership_detail.membership_structure !=='OE'){
                    if($scope.membership_class_end_date === '' || $scope.membership_class_end_date === undefined){
                        $scope.membership_class_end_date = $scope.localdateformatfilter($scope.membership_detail.last_available_payment_date);
                    }                    
                    $scope.updateMSdate('0');                
                    $scope.updateMEdate('0');
                }else{
                    $scope.showFirstPaymntEdit = $scope.showFirstPaymntExists = $scope.showFirstPaymntUpdate = false;
                    $scope.showFirstPaymntDateEdit = $scope.showFirstPaymntDateUpdate = false;
                    $('#ncpaymntstartdate').datepicker('setEndDate', $scope.localdateformatfilter($scope.membership_detail.payment_start_date));
                    if($scope.showMemFeeUpdate || $scope.showMemDateUpdate || $scope.showMemClassStartDateUpdate){
                        $scope.showbutton = false;
                    }else{
                        $scope.showbutton = true;
                    }
                    $scope.updateMSdate('0'); 
                } 
            };
            
            $scope.cancelMSdate = function () {
                $scope.showMemSDateEdit = false;
                $scope.curr_date_new = $scope.formatDateToday();
                if($scope.membership_detail.membership_start_date >= $scope.curr_date_new){
                    $('#ncpaymntenddate').datepicker('setStartDate', $scope.localdateformatfilter($scope.membership_detail.membership_start_date));
                }else{
                    $('#ncpaymntenddate').datepicker('setStartDate',new Date());
                }   
                $scope.showMemClassStartDateUpdate = false;
                if($scope.membership_detail.membership_structure ==='OE' && ($scope.showMemFeeUpdate || $scope.showMemDateUpdate || $scope.showMemClassStartDateUpdate)){
                  $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
                return;
            };
            
              
            $scope.editMEdate = function (actual_ms_end_date) {
                $scope.current_medate_value = actual_ms_end_date;
                $scope.showMemEDateEdit = !$scope.showMemEDateEdit;           
                $scope.paymentcurrentmedatevalue = angular.copy($scope.current_medate_value);  
                $scope.membership_class_end_date = $scope.localdateformatfilter(actual_ms_end_date);
                $scope.edit_mem_end_date = $scope.serverdateformatfilter($scope.membership_class_end_date); 
                if($scope.membership_class_start_date === '' || $scope.membership_class_start_date === undefined){
                    $scope.membership_class_start_date = $scope.localdateformatfilter($scope.membership_detail.membership_start_date);
                }
                $scope.updateMEdate('0');
                $scope.updateMSdate('0');               
            };
            
            $scope.cancelMEdate = function () {
                $scope.showMemEDateEdit = !$scope.showMemEDateEdit;
                $scope.curr_date_new = $scope.formatDateToday();
                if($scope.membership_detail.last_available_payment_date >= $scope.curr_date_new){
                    $('#ncpaymntstartdate').datepicker('setEndDate',$scope.localdateformatfilter($scope.membership_detail.last_available_payment_date));
                }else{
                    $('#ncpaymntstartdate').datepicker('setEndDate',new Date());
                }
                $scope.showMemClassEndDateUpdate = false;
                return;
            };
            
            $scope.updateMSdate = function(call_from){   
                if(call_from === '1'){
                    if($scope.membership_class_start_date === '' || $scope.membership_class_start_date === undefined){
                        $scope.showMemClassStartDateUpdate = false;  
                    }else{
                        $scope.showMemClassStartDateUpdate = true;
                    }
                    $scope.call_back_from = call_from;
                }else if($scope.call_back_from === '0'){
                    $scope.showMemClassStartDateUpdate = false; 
                }
                if($scope.membership_detail.membership_structure ==='OE' && ($scope.showMemFeeUpdate || $scope.showMemDateUpdate || $scope.showMemClassStartDateUpdate)){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
                
                if($scope.membership_class_start_date !== ''){
                    $('#ncpaymntenddate').datepicker('setStartDate', $scope.membership_class_start_date);
                }else{
                    $('#ncpaymntenddate').datepicker('setStartDate', null);
                    $scope.membership_class_end_date = '';
                }
                $scope.final_mem_start_date =  $scope.serverdateformatfilter($scope.membership_class_start_date);
                $scope.final_mem_end_date =  $scope.serverdateformatfilter($scope.membership_class_end_date);   
            };
            
            $scope.updateMEdate = function(call_from){
                if(call_from === '1'){
                    if($scope.membership_class_end_date === '' || $scope.membership_class_end_date === undefined){
                        $scope.showMemClassEndDateUpdate = false;  
                    }else{
                        $scope.showMemClassEndDateUpdate = true;
                    }
                    $scope.call_back_from = call_from;
                }else if($scope.call_back_from === '0'){
                    $scope.showMemClassEndDateUpdate = false; 
                }
                if($scope.membership_class_end_date !== ''){
                    $('#ncpaymntstartdate').datepicker('setEndDate', $scope.membership_class_end_date);
                }else{
                    $('#ncpaymntstartdate').datepicker('setEndDate', null);
                }
                $scope.final_mem_start_date =  $scope.serverdateformatfilter($scope.membership_class_start_date);
                $scope.final_mem_end_date =  $scope.serverdateformatfilter($scope.membership_class_end_date);
                
            };
            
            $scope.closeDateEdit = function(){
                $scope.showMemSDateEdit = false;
                $scope.showMemEDateEdit = false;
                $scope.showMemClassStartDateUpdate = false; 
                $scope.showMemClassEndDateUpdate = false;  
                $scope.membership_class_start_date = $scope.localdateformatfilter($scope.membership_detail.membership_start_date);
                $scope.membership_class_end_date = $scope.localdateformatfilter($scope.membership_detail.last_available_payment_date);
            }
            
            
            $scope.editFirstPaymntamount = function (value) {  
                $scope.showMemFeeEdit = $scope.showMemFeeExists = $scope.showMemFeeUpdate = false;
                $scope.showMemDateEdit = $scope.showMemDateUpdate = $scope.showMemSDateEdit = $scope.showMemClassStartDateUpdate =false;
                $scope.membershipedit.first_payment_amount = value;
                $scope.amountFirstPaymnterrortext = "";
                $scope.current_first_paymnt_amount_value = value;
                $scope.showFirstPaymntEdit = true;                
                $scope.firstpaymentcurrentvalue = angular.copy($scope.current_first_paymnt_amount_value);
                if($scope.showFirstPaymntUpdate || $scope.showFirstPaymntDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
            }            
            
            $scope.cancelFirstPaymntamount = function () {
                $scope.showFirstPaymntEdit = $scope.showFirstPaymntExists = $scope.showMemFeeUpdate = false;
                $scope.membershipedit.first_payment_amount = $scope.firstpaymentcurrentvalue;
                $scope.showFirstPaymntUpdate = false;
                $scope.amountFirstPaymnterrortext = "";
                if($scope.showFirstPaymntUpdate || $scope.showFirstPaymntDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
                return;
            };

            $scope.updateFirstPaymntamount = function (value) {                
                $scope.showFirstPaymntUpdate = true;
                if($scope.showFirstPaymntUpdate || $scope.showFirstPaymntDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
                if (!value || parseInt(value) < 5) {
                    $scope.showFirstPaymntExists = true;
                    $scope.amountFirstPaymnterrortext = "Amount Should Be Greater than Or Equal To 5"+$scope.wp_currency_symbol;                    
                    return;
                } else {
                    $scope.showFirstPaymntExists = false;
                }
                $scope.newfirstpaymentvalue = value;
            };

            $scope.editFirstPaymntdate = function (value) { 
                $scope.showMemFeeEdit = $scope.showMemFeeExists = $scope.showMemFeeUpdate = false;
                $scope.showMemDateEdit = $scope.showMemDateUpdate = $scope.showMemSDateEdit = $scope.showMemClassStartDateUpdate = false;
                $scope.current_first_paymnt_date_value = value;                
                $scope.showFirstPaymntDateEdit = true; 
                $scope.firstpaymentcurrentdatevalue = angular.copy($scope.current_first_paymnt_date_value); 
                $scope.membershipedit.first_payment_date = $scope.localdateformatfilter(value);                
                $('#paymntfirststartdate').datepicker('setStartDate', $scope.localdateformatfilter($scope.membership_detail.membership_start_date));
                $('#paymntfirststartdate').datepicker('setEndDate', $scope.localdateformatfilter($scope.membership_detail.next_payment_date));
                if($scope.showFirstPaymntUpdate || $scope.showFirstPaymntDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
            };
            
            $scope.cancelFirstPaymntdate = function () {
                $scope.showFirstPaymntDateEdit = false;
                $scope.membershipedit.first_payment_date = $scope.firstpaymentcurrentdatevalue;
                $scope.showFirstPaymntDateUpdate = false;
                if($scope.showFirstPaymntUpdate || $scope.showFirstPaymntDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
                return;
            };
            
            $scope.updateFirstPaymntdate = function(paymnt_start_date){ 
                $scope.showFirstPaymntDateUpdate = true;
                if($scope.showFirstPaymntUpdate || $scope.showFirstPaymntDateUpdate){
                    $scope.showbutton = false;
                }else{
                    $scope.showbutton = true;
                }
                $scope.curr_date_new = $scope.formatDateToday();
                var choose_date = paymnt_start_date.split("/");
                var new_date = choose_date[2] + "-" + choose_date[0] + "-" + choose_date[1];
                
                if((new_date >= $scope.membership_detail.membership_start_date) && ($scope.membership_detail.next_payment_date >= new_date)){
                    $scope.membershipedit.first_payment_date = paymnt_start_date;
                    $scope.final_payment_start_date =  $scope.serverdateformatfilter(paymnt_start_date);
                }else{
                    $scope.membershipedit.first_payment_date = $scope.localdateformatfilter($scope.membership_detail.next_payment_date);
                    $scope.final_payment_start_date =  $scope.membership_detail.next_payment_date;
                } 
                
                if (new_date === $scope.curr_date_new) {
                    $scope.curr_first_paymnt_date_flag = 'Y';
                } else {
                    $scope.curr_first_paymnt_date_flag = 'N';
                }
                
            };
            
            $scope.editMembership = function () {
               $scope.check_date = $scope.showMemDateUpdate ? $scope.newpaymentdatevalue : $scope.membership_detail.next_payment_date;
                if ($scope.cancel_date_flg === 'Y' && $scope.cancelleddate < $scope.check_date) {
                    $("#successmessageModal").modal('show');
                    $("#successmessagetitle").text('Message');
                    $("#successmessagecontent").text("Membership is scheduled for cancellation, this option is not available.");
                } else {
                    $("#confirmPayPlanEditModal").modal('show');
                    $("#confirmPayPlanEdittitle").text('Update Payment plan');
                    $("#confirmPayPlanEditcontent").text("Are you sure want to Update Payment Plan?");
                }
            };

            $scope.editFirstPaymntdetails = function () {
                $scope.check_date = $scope.showFirstPaymntDateUpdate ? $scope.final_payment_start_date : $scope.membership_detail.first_payment_date;
                if ($scope.cancel_date_flg === 'Y' && $scope.cancelleddate < $scope.check_date) {
                    $("#successmessageModal").modal('show');
                    $("#successmessagetitle").text('Message');
                    $("#successmessagecontent").text("Membership is scheduled for cancellation, this option is not available.");
                } else {
                    $("#confirmFirstPaymntModal").modal('show');
                    $("#confirmFirstPaymnttitle").text('Update Payment plan');
                    $("#confirmFirstPaymntcontent").text("Are you sure want to update First Payment details?");
                    $scope.first_paymnt_delete_flag = 'N';
                }
            };
            
            $scope.deleteFirstPaymntdetails = function () {
                $("#confirmFirstPaymntModal").modal('show');
                $("#confirmFirstPaymnttitle").text('Update Payment plan');
                $("#confirmFirstPaymntcontent").text("Are you sure want to delete First Payment details?");
                $scope.first_paymnt_delete_flag = 'Y';
            };

            $scope.confirmeditmembership = function (edit_type) {

                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'editMembership',
                    data: {
                        "company_id": $localStorage.company_id,
                        "updated_amount": $scope.showMemFeeUpdate ? $scope.newpaymentvalue : $scope.membership_detail.payment_amount,
                        "next_date": $scope.showMemDateUpdate ? $scope.newpaymentdatevalue : $scope.membership_detail.next_payment_date,
                        "membership_start_date": $scope.showMemClassStartDateUpdate ? $scope.final_mem_start_date : $scope.membership_detail.membership_start_date,
                        "membership_reg_id": $scope.editMembership_reg_id,
                        "pay_now_flg": $scope.curr_date_flag,
                        "upgrade_status": $localStorage.upgrade_status
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $("#confirmPayPlanEditModal").modal('hide');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.cancelMemDetailUpdate();
                        
                        $scope.membership_detail = response.data.reg_details.msg;
                        $scope.category_list = $scope.membership_detail.All;
                        $scope.memstatus = $scope.membership_detail.membership_status;
                        
                        $scope.membershipedit = angular.copy($scope.membership_detail);
                        $scope.editMembership_reg_id = $scope.membership_detail.membership_registration_id;
                        $scope.editMembership_opt_id = $scope.membership_detail.membership_option_id;
                        $scope.editMembership_id = $scope.membership_detail.membership_id;
                        $scope.editMembershipStatus = $scope.membership_detail.membership_status;
                        $scope.editMembershipCategoryTitle = $scope.membership_detail.membership_category_title;
                        $scope.editMembershipOption_title = $scope.membership_detail.membership_title;
                        if(edit_type === 'history'){
                            $scope.Payment_History();
                        }
                        
                    } else if (response.data.status === 'Failed') {
                        console.log(response.data);
                        $('#progress-full').hide();         
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.confirmeditfirstPayment = function (edit_type) {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'editMembershipFirstPayment',
                    data: {
                        "company_id": $localStorage.company_id,
                        "updated_amount": $scope.showFirstPaymntUpdate ? $scope.newfirstpaymentvalue : $scope.membership_detail.first_payment_amount,
                        "first_payment_date": $scope.showFirstPaymntDateUpdate ? $scope.final_payment_start_date : $scope.membership_detail.first_payment_date,
                        "membership_reg_id": $scope.editMembership_reg_id,
                        "pay_now_flg": $scope.curr_first_paymnt_date_flag,
                        "delete_payment" : $scope.first_paymnt_delete_flag,
                        "upgrade_status": $localStorage.upgrade_status
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $("#confirmFirstPaymntModal").modal('hide');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.cancelMemDetailUpdate();
                        
                        $scope.membership_detail = response.data.reg_details.msg;
                        $scope.category_list = $scope.membership_detail.All;
                        $scope.memstatus = $scope.membership_detail.membership_status;
                        
                        $scope.membershipedit = angular.copy($scope.membership_detail);
                        $scope.editMembership_reg_id = $scope.membership_detail.membership_registration_id;
                        $scope.editMembership_opt_id = $scope.membership_detail.membership_option_id;
                        $scope.editMembership_id = $scope.membership_detail.membership_id;
                        $scope.editMembershipStatus = $scope.membership_detail.membership_status;
                        $scope.editMembershipCategoryTitle = $scope.membership_detail.membership_category_title;
                        $scope.editMembershipOption_title = $scope.membership_detail.membership_title;
                        
                        if(edit_type === 'history'){
                            $scope.Payment_History();
                        }
                        
                    } else if (response.data.status === 'Failed') {
                        console.log(response.data);
                        $('#progress-full').hide();         
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.confirmeditMembershipClassDates = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'editMembershipDatesForClasses',
                    data: {
                        "company_id": $localStorage.company_id,
                        "membership_structure": $scope.membership_detail.membership_structure,
                        "membership_start_date": $scope.showMemClassStartDateUpdate ? $scope.final_mem_start_date : $scope.membership_detail.membership_start_date,
                        "membership_end_date": $scope.showMemClassEndDateUpdate ? $scope.final_mem_end_date : $scope.membership_detail.last_available_payment_date,
                        "reg_id": $scope.editMembership_reg_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $("#refundModal").modal('hide');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        
                        $scope.membership_detail = response.data.reg_details.msg;
                        $scope.category_list = $scope.membership_detail.All;
                        $scope.memstatus = $scope.membership_detail.membership_status;
                        $scope.closeDateEdit();
                        
                        $scope.membershipedit = angular.copy($scope.membership_detail);
                        $scope.editMembership_reg_id = $scope.membership_detail.membership_registration_id;
                        $scope.editMembership_opt_id = $scope.membership_detail.membership_option_id;
                        $scope.editMembership_id = $scope.membership_detail.membership_id;
                        $scope.editMembershipStatus = $scope.membership_detail.membership_status;
                        $scope.editMembershipCategoryTitle = $scope.membership_detail.membership_category_title;
                        $scope.editMembershipOption_title = $scope.membership_detail.membership_title;
                        
                    } else if (response.data.status === 'Failed') {
                        console.log(response.data);
                        $('#progress-full').hide();         
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg); 
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            $scope.editHistoryNote = function (ind, hist_id, action,activity_type) {
                if(action === 'add'){
                    $scope.edit_status = 'add';
                    $scope.edited_mem_history_id = '';
                    $scope.memhistorynote = '';
                    $scope.HistoryNoteButton = 'Add';
                    $("#memHistoryNoteModal").modal('show');
                    $("#memHistoryNotetitle").text('Add Note');                    
                }else if(action === 'edit'){
                    $scope.edited_mem_history_id = hist_id ;
                    $scope.edit_status = 'update';
                    $scope.HistoryNoteButton = 'Update';
                    $scope.memhistorynote = $scope.membership_history[ind].activity_text;
                    $("#memHistoryNoteModal").modal('show');
                    $("#memHistoryNotetitle").text('Update Note');
                }else if(action === 'delete'){
                    $scope.edited_mem_history_id = hist_id ;
                    $scope.memhistorynote =  '';
                    $scope.edit_status = 'delete';
                    $("#memHistoryNoteDeleteModal").modal('show');
                    $("#memHistoryNoteDeletetitle").text('Delete Note');
                    $("#memHistoryNoteDeletecontent").text('Are you sure want to delete the note?');
                }else if(action === 'edit_cancellation' && activity_type ==='scheduled cancel'){
                     $scope.edited_mem_history_id = hist_id ;
                     $scope.edit_status = 'edit_cancellation';
                     $("#memHistoryNoteEditCancelModal").modal('show');
                     $("#memHistoryNoteEditCanceltitle").text('Edit Scheduled Cancellation Date');
                }else if(action === 'delete_cancellation' && activity_type ==='scheduled cancel'){
                     $scope.edited_mem_history_id = hist_id ;
                     $scope.edit_status = 'delete_cancellation';
                    $("#memHistoryNoteDeleteCancelModal").modal('show');
                    $("#memHistoryNoteDeleteCanceltitle").text('Delete Scheduled Cancellation');
                    $("#memHistoryNoteDeleteCancelcontent").text('Are you sure want to delete this Cancellation?'); 
                }else if(action === 'edit_cancellation' && activity_type ==='scheduled hold'){
                    $scope.actionValue = 1;
                    $scope.pause_modal_flag = true;
                    if($scope.membership_detail.payment_type ==='R'){                        
                        $scope.editdate($scope.membership_detail.next_payment_date);
                        $scope.editamount($scope.membership_detail.payment_amount);
                        $scope.showMemDateEdit = false; 
                        $scope.showMemFeeEdit = false; 
                        if ($scope.payment_pause_selection_dates.length <= 1)
                        {
                            $scope.pausescheduled = 'N';
                            $scope.disablepauseoptions = true;
                            $scope.disableresumeoptions = true;
                        }
                        else
                        {
                            $scope.pausescheduled = 'N';
                            $scope.disablepauseoptions = false;
                            $scope.disableresumeoptions = false;   
                        }
                        $scope.wo_resumescheduled = 'Y';
                        $scope.payment_pause_date = $scope.payment_pause_selection_dates[0];
                        $scope.payment_resume_date = $scope.payment_pause_selection_dates[0];
                    }else{
                        $scope.pausescheduled = 'N';
                        $scope.wo_resumescheduled = 'Y';
                        $scope.payment_pause_date = '';
                        $scope.payment_resume_date = '';
                    }
                    $("#membershipHoldModal").modal('show');
                    $("#membershipHoldtitle").text('Edit Membership Status');
                }else if(action === 'delete_cancellation' && activity_type ==='scheduled hold'){
                     $scope.edited_mem_history_id = hist_id ;
                     $scope.edit_status = 'delete_hold';
                    $("#memHistoryHoldDeleteModal").modal('show');
                    $("#memHistoryHoldDeletetitle").text('Delete Scheduled Hold');
                    $("#memHistoryHoldDeleteCancelcontent").text('Are you sure want to delete Scheduled Hold?'); 
                }
            }
            
            $scope.confirmeditHistoryNote = function () {
                $("#memHistoryNoteModal").modal('hide');
                $("#memHistoryNoteDeleteModal").modal('hide');
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'membershipHistoryNotes',
                    data: {
                        "company_id": $localStorage.company_id,
                        "history_id": $scope.edited_mem_history_id,
                        "status":$scope.edit_status,
                        "reg_id": $scope.editMembership_reg_id,
                        "note_text": $scope.memhistorynote
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.membership_history = response.data.membership_history.msg; 
                        
                    }else if(response.data.status === 'Failed'){
                        console.log(response.data);
                        $('#progress-full').hide();         
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text(response.data.msg); 
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.sendPaymentHistory = function () {
                $("#payHistoryEmailModal").modal('show');
                $("#payHistoryEmailtitle").text('Send Email');
                $("#payHistoryEmailcontent").text('Are you sure want to send a email with payment history details?');
            };
            
            
            $scope.confirmsendPayHistoryEmail = function () {
                $("#payHistoryEmailModal").modal('hide');
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendPaymentHistory',
                    data: {
                        "company_id": $localStorage.company_id,
                        "membership_reg_id": $scope.editMembership_reg_id,
                        "category": 'membership'
                        
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $("#refundModal").modal('hide');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text('Membership History sent successfully.');
                    }else if(response.data.status === 'Failed'){
                        console.log(response.data);
                        $('#progress-full').hide();         
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text(response.data.msg); 
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.editPaymentHistory = function (ind, history_type) { //Show list of Actions in Past tab// work1
                if(history_type === 'upcoming'){
                    $scope.actionpayment_array = $scope.mem_upcoming_history[ind];
                }else{
                    $scope.actionpayment_array = $scope.mem_payment_history[ind];
                }
                $scope.editMembership_reg_id = $scope.actionpayment_array.reg_id;
                $scope.editMembership_id=$scope.actionpayment_array.membership_id;
                $scope.editPaymentMethod_payment_id = $scope.actionpayment_array.payment_id;
                $scope.editPaymentMethod_category = $scope.actionpayment_array.category;
               
                if($scope.actionpayment_array.membership_structure === 'NC' || $scope.actionpayment_array.membership_structure === 'C' || $scope.actionpayment_array.membership_structure === 'SE' ){
                    $scope.temp_hist_paymentamnt1 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                    $scope.temp_hist_paymentamnt2 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                    var pay_start_date = $scope.actionpayment_array.payment_date.split("-");                   
                    $scope.paymenthistorystartdate = pay_start_date[1] + "/" + pay_start_date[2] + "/" + pay_start_date[0];
                    $scope.paymenthistorystartdate_temp = $scope.paymenthistorystartdate;
                    $('#paymenthistorystartdate').datepicker('setStartDate', new Date());
                    $("#paymentHistoryEditModal").modal('show');
                    $("#paymentHistoryEdittitle").text('Update Payment details');
                }else{  
                    if($scope.actionpayment_array.paytime_type === 'F'){
                        $scope.membership_detail.first_payment_date = $scope.actionpayment_array.payment_date;
                        $scope.membership_detail.first_payment_amount = $scope.actionpayment_array.payment_amount_without_pf;
                        $scope.editFirstPaymntdate($scope.actionpayment_array.payment_date);
                        $scope.editFirstPaymntamount($scope.actionpayment_array.payment_amount_without_pf);
                        $scope.showFirstPaymntDateEdit = false; 
                        $scope.showFirstPaymntEdit = false;
                    }else{
                        $scope.membership_detail.next_payment_date = $scope.actionpayment_array.payment_date;
                        $scope.membership_detail.payment_amount = $scope.actionpayment_array.payment_amount_without_pf;
                        $scope.editdate($scope.actionpayment_array.payment_date);
                        $scope.editamount($scope.actionpayment_array.payment_amount_without_pf);
                        $scope.showMemDateEdit = false; 
                        $scope.showMemFeeEdit = false; 
                    }
                    $("#memAmntDateUpdateModal").modal('show');
                    $("#memAmntDateUpdatetitle").text('Update Payment details'); 
                }
            };
            
            $scope.updatePaymentHistory = function (value, newval) { //Validate The Credit amount Entered  By User//
                $scope.check_date = $scope.serverdateformatfilter($scope.paymenthistorystartdate);
                $scope.new_payment_amount = 0;
                if (!value || parseFloat(newval) < 5) {
                    $("#successmessageModal").modal('show');
                    $("#successmessagetitle").text('Message');
                    $("#successmessagecontent").text("Payment amount should not be lower than 5"+$scope.wp_currency_symbol);
                } else if($scope.cancel_date_flg === 'Y' && $scope.cancelleddate < $scope.check_date){ 
                    $("#successmessageModal").modal('show');
                    $("#successmessagetitle").text('Message');
                    $("#successmessagecontent").text("Membership is scheduled for cancellation, this option is not available.");
                }else{
                    $scope.new_payment_amount = newval;
                    $scope.final_payment_start_date = $scope.serverdateformatfilter($scope.paymenthistorystartdate);                    
                    $scope.curr_date_new = $scope.formatDateToday();
                    if($scope.final_payment_start_date > $scope.curr_date_new){
                        $scope.curr_payment_start_date_flag = 'N';
                    }else{
                        $scope.curr_payment_start_date_flag = 'Y';
                    }
                    $("#successmessageModal").modal('hide');
                    $("#paymentEditHistoryModal").modal('show');
                    $("#paymentEditHistorytitle").text('Message');
                    $("#paymentEditHistorycontent").text('Are you sure want to update payment details?');
                }
            };
            
            $scope.updatePaymentHistoryCancel = function () { 
                $scope.new_payment_amount = 0;
                $scope.final_payment_start_date = '';
                $scope.cancelamount();
                $scope.canceldate();
                $scope.cancelFirstPaymntamount();
                $scope.cancelFirstPaymntdate();
            };
            
            $scope.confirmUpdatePaymentHistory = function () { 
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'editPaymentFromHistory',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id": $scope.editMembership_reg_id,
                        "payment_id": $scope.editPaymentMethod_payment_id,
                        "payment_amount": $scope.new_payment_amount,
                        "payment_date": $scope.final_payment_start_date,
                        "pay_now_flg": $scope.curr_payment_start_date_flag,
                        "category":'membership'
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') { 
                        $("#paymentHistoryEditModal").modal('hide');
                        $('#progress-full').hide();                        
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.mem_upcoming_history = response.data.payment_history.msg.upcoming;
                        $scope.mem_payment_history = response.data.payment_history.msg.history;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            
            $scope.performActionUpdate = function (ind, payment_status, current_status, mem_structure, history_type) { //Show list of Actions in Past tab//
                if(history_type === 'upcoming'){
                    $scope.actionpayment_array = $scope.mem_upcoming_history[ind];
                }else{
                    $scope.actionpayment_array = $scope.mem_payment_history[ind];
                }
                $scope.editMembership_reg_id = $scope.actionpayment_array.reg_id;
                $scope.editMembership_id=$scope.actionpayment_array.membership_id;
                $scope.editPaymentMethod_payment_id = $scope.actionpayment_array.payment_id;
                $scope.editPaymentMethod_category = $scope.actionpayment_array.category;                
                $scope.temp_past_paymentarray1 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                $scope.temp_past_paymentarray2 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                
               if (payment_status === 'S') { // REFUND
                    $scope.actionValue = 1;
                    if($scope.actionpayment_array.checkout_status==='released'){
                        $scope.allowmanualcreditrefund = false;
                        $("#membershipRefundActionModal").modal('show');
                        $("#membershipRefundActiontitle").text('Edit Order');
                        $("#membershipRefundActioncontent").text("Are you sure you want to process the Membership refund?");
                    }else{
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Edit Order');
                        $("#successmessagecontent").text('Payment can be refunded only after it is released. Please try again after it is released.');
                    }
               }else if (($scope.actionpayment_array.payment_status === 'M' && ($scope.actionpayment_array.credit_method === 'CH' || $scope.actionpayment_array.credit_method === 'CA')) ){
                    $scope.allowmanualcreditrefund = true;
                    $("#membershipRefundActionModal").modal('show');
                    $("#membershipRefundActiontitle").text('Edit Order');
                    if($scope.actionpayment_array.credit_method === 'CA'){
                        $("#membershipRefundActioncontent").text("Are you sure you want to process the Cash refund?");
                    }
                    else if($scope.actionpayment_array.credit_method === 'CH'){
                        $("#membershipRefundActioncontent").text("Are you sure you want to process the Check refund?");
                    }
                }else if (payment_status === 'F' || (payment_status === 'N' && (mem_structure === 'OE' || mem_structure === 'NC' || mem_structure === 'C' || mem_structure === 'SE'))) { // APPLY MANUAL CREDIT
                        $scope.actionValue = 2;
                        $scope.creditmethod = 'MC';
                        $scope.checknumber = '';
                        $("#paymentHistoryModal1").modal('show');
                        $("#paymentHistorytitle").text('Apply Payment Credit');
                        $("#paymentHistorytitle1").text('Enter credit to be applied to this bill');
                }
            };
            
            $scope.performRerunAction = function (ind, payment_status, history_type) { //Show list of Actions in Past tab//
                if(history_type === 'upcoming'){
                    $scope.actionpayment_array = $scope.mem_upcoming_history[ind];
                }else{
                    $scope.actionpayment_array = $scope.mem_payment_history[ind];
                }
                $scope.editMembership_reg_id = $scope.actionpayment_array.reg_id;
                $scope.editMembership_id=$scope.actionpayment_array.membership_id;
                $scope.editPaymentMethod_payment_id = $scope.actionpayment_array.payment_id;
                $scope.editPaymentMethod_category = $scope.actionpayment_array.category;
                $scope.temp_past_paymentarray1 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                $scope.temp_past_paymentarray2 = angular.copy($scope.actionpayment_array.payment_amount_without_pf);
                if (payment_status === 'F' || payment_status === 'N') { //RE-RUN
                    $scope.actionValue = 0;
                    $("#paymentHistoryModal").modal('show');
                    $("#paymentHistorytitle").text('Re-Run Payment');
                    $("#paymentHistorycontent").text("Are you sure to Re-Run the Payment?");
                }
            };
            
            $scope.updatePaidquantity = function (value, newval) { //Validate The Credit amount Entered  By User//
                $scope.upcoming_creditamount = 0;
                if (!value || parseFloat(newval) > parseFloat(value)) {
                    $("#successmessageModal").modal('show');
                    $("#successmessagetitle").text('Message');
                    $("#successmessagecontent").text('Credit amount should be lesser than bill amount.');
                }else if(!value || ((parseFloat(value) - parseFloat(newval)) < parseFloat(5) && (parseFloat(value) - parseFloat(newval)) > parseFloat(0))){
                    $("#successmessageModal").modal('show');
                    $("#successmessagetitle").text('Message');
                    $("#successmessagecontent").text('Recurring amount should be greater than 5.');
                }else {
                    $scope.upcoming_creditamount = newval;
                    $("#successmessageModal").modal('hide');
                    $("#paymentHistoryModal3").modal('show');
                    $("#paymentHistorytitle3").text('Message');
                    $("#paymentHistorycontent3").text('Are you sure want to add a credit to this bill?');
                    return;
                }
            };
            
            $scope.confirmCreditAmount = function (creditamount,creditmethod) { //Function call After Getting Credit amount From user//               
                $scope.markAsMembershipPaid($scope.actionpayment_array, creditamount, creditmethod);
            };
            
            $scope.actionReRunConfirm = function (value) { //Function call after confirmation//
                $scope.editSingle = "";
                $scope.actionPastchoosen = "";
                if (value === 0) {
                    $scope.reRunPayment($scope.actionpayment_array);
                }
            };
            
            
            $scope.reRunPayment = function () { //update credit card details
                var category_type = '';
                if ($scope.editPaymentMethod_category === 'events') {
                    category_type = 'E';
                } else if ($scope.editPaymentMethod_category === 'membership') {
                    category_type = 'M';
                }
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'reRunPayment',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id": $scope.editMembership_reg_id,
                        "payment_id": $scope.editPaymentMethod_payment_id,
                        "category_type": category_type
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {                        
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.mem_upcoming_history = response.data.payment_history.msg.upcoming;
                        $scope.mem_payment_history = response.data.payment_history.msg.history;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.markAsMembershipPaid = function (payment_array, creditamount, creditmethod) { //Mark the membership as Paid
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'membershipPaymentMarkedAsPaid',
                    data: {
                        "company_id": $localStorage.company_id,
                        "payment_id": payment_array.payment_id,
                        "reg_id": payment_array.reg_id,
                        "credit_amount": creditamount,
                        "credit_method": creditmethod,
                        "check_number": $scope.checknumber
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $("#paymentHistoryModal1").modal('hide');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.upcoming_creditamount = '';
                        $scope.mem_upcoming_history = response.data.payment_history.msg.upcoming;
                        $scope.mem_payment_history = response.data.payment_history.msg.history;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.confirmMembershipRefund = function () { //Refund the edited Payment Plan details//

                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'processMembershipPaymentPlanRefund',
                    data: {
                        "company_id": $localStorage.company_id,
                        "membership_reg_id": $scope.editMembership_reg_id,
                        "membership_payment_id": $scope.editPaymentMethod_payment_id,
                        "membership_id":  $scope.editMembership_id,
                        "allow_manual_credit_refund": $scope.allowmanualcreditrefund                     
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                        $scope.mem_upcoming_history = response.data.payment_history.msg.upcoming;
                        $scope.mem_payment_history = response.data.payment_history.msg.history;
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {

                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        if (response.data.error) {
                            $("#successmessagetitle").text(response.data.error);
                            $("#successmessagecontent").text(response.data.error_description);
                        } else {
                            $("#successmessagetitle").text('Message');
                            $("#successmessagecontent").text(response.data.msg);
                        }
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };            
            
            $scope.Attendance_detail = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getAttendanceDetail',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id" : $scope.currentMembership_reg_id,
                        "category" : "M"   //M - Membership
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                    if (response.data.status === 'Success') {
                        $scope.attendance_history = response.data.msg.attendance_history;
                        $scope.attendance_history_array = angular.copy($scope.attendance_history);
                        $scope.attendance_limit_flag = response.data.msg.option.attendance_limit_flag;
                        $scope.attendance_days_count = response.data.days_count;
                        $scope.active_dates = $scope.checkedin_dates = [];
                        $scope.active_dates = response.data.msg.checkin_dates;
                        $("#selectedDateCalender").datepicker('remove');
                        if($scope.active_dates.length > 0){
                            for(i=0;i<$scope.active_dates.length;i++){
                                $scope.checkedin_dates.push($scope.active_dates[i]);
                            }
                        }else{
                            $scope.active_dates = $scope.checkedin_dates = [];
                        }
                        var active_dates = $scope.checkedin_dates;
                        $scope.calender_attendancedate = '';
                        $("#selectedDateCalender").datepicker({
                            format: "dd/mm/yyyy",
                            autoclose: true,                           
                            beforeShowDay: function(date){
                                 var d = date;
                                 var curr_date = d.getDate();
                                 var curr_month = d.getMonth() + 1; //Months are zero based
                                 var curr_year = d.getFullYear();
                                 var formattedDate = curr_date + "/" + curr_month + "/" + curr_year;
                                    if ($.inArray(formattedDate, active_dates) !== -1){
                                       return {
                                          classes: 'activeDates'
                                       };
                                   }
                                  return;
                            }
                        }).on("changeDate", function (e) {
                            var selected_date = new Date(e.dates);
                            var currentDate = ('0' + (selected_date.getMonth() + 1)).slice(-2) + '/' + ('0' + selected_date.getDate()).slice(-2) + '/' + selected_date.getFullYear();
                            $scope.calender_attendancedate = $scope.attendancedate = currentDate;
                        });
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('#li-customers').addClass('active-event-list').removeClass('event-list-title');
                $('.greentab-text').removeClass('greentab-text-active');
            };
            
            $scope.membershipdetails = function () {
                $scope.membershipdetailsview = true;
                $scope.participantinfoview = false;
                $scope.membershiphistoryview = false;
                $scope.paymenthistoryview = false;
                $scope.attendancedetailview = false;
                $scope.resettabbarclass();
                $('#li-membershipdetails').addClass('active-event-list').removeClass('event-list-title'); 
                $('.membershipdetail-tabtext-1').addClass('greentab-text-active');
                $scope.Membership_Detail();
            };
            
            $scope.participantinfo = function () {
                $scope.membershipdetailsview = false;
                $scope.participantinfoview = true;
                $scope.membershiphistoryview = false;
                $scope.paymenthistoryview = false;  
                $scope.attendancedetailview = false;
                $scope.resettabbarclass();
                $('#li-participantinfo').addClass('active-event-list').removeClass('event-list-title');
                $('.membershipdetail-tabtext-2').addClass('greentab-text-active');
                $scope.Participant_Info();
            };
            
            $scope.membershiphistory = function () {
                $scope.membershipdetailsview = false;
                $scope.participantinfoview = false;
                $scope.membershiphistoryview = true;
                $scope.paymenthistoryview = false; 
                $scope.attendancedetailview = false;
                $scope.resettabbarclass();
                $('#li-membershiphistory').addClass('active-event-list').removeClass('event-list-title');
                $('.membershipdetail-tabtext-3').addClass('greentab-text-active');
                $scope.Membership_History();
            };
            
            $scope.paymenthistory = function () {
                $scope.membershipdetailsview = false;
                $scope.participantinfoview = false;
                $scope.membershiphistoryview = false;
                $scope.paymenthistoryview = true;  
                $scope.attendancedetailview = false;
                $scope.resettabbarclass();
                $('#li-paymenthistory').addClass('active-event-list').removeClass('event-list-title');
                $('.membershipdetail-tabtext-4').addClass('greentab-text-active');
                $scope.Payment_History();
            };
            
            $scope.attendanceDetails = function(){
                $scope.membershipdetailsview = false;
                $scope.participantinfoview = false;
                $scope.membershiphistoryview = false;
                $scope.paymenthistoryview = false;
                $scope.attendancedetailview = true;
                $scope.resettabbarclass();
                $('#li-attendance').addClass('active-event-list').removeClass('event-list-title');                
                $('.membershipdetail-tabtext-5').addClass('greentab-text-active');
                $scope.datetimeerror = false;
                $scope.attendance_count_exceed = 'N';
                $scope.override_flag = 'N';
                $scope.attendance_date_change = 'N';
                $scope.calender_attendancedate = '';
                
                $scope.Attendance_detail();                
            };
            
            $scope.closeMembershipDetailView = function () {
                $localStorage.buyerName = "";
                $localStorage.participantName = "";
                $localStorage.currentMembershipregid = "";
                if ($scope.page_From === "payment_approved" || $scope.page_From === "payment_past" || $scope.page_From === "payment_upcoming")
                {
                    $location.path('/payment');
                } else if ($scope.page_From === "members_active" || $scope.page_From === "members_onhold" || $scope.page_From === "members_cancelled") {
                    $location.path('/customers');
                } else if ($scope.page_From === "manage_members_active" || $scope.page_From === "manage_members_onhold" || $scope.page_From === "manage_members_cancelled") {
                    $location.path('/managemembership');
                } else {
                    $location.path('/customers');
                }
            };
            
            $scope.redirectto = function () {
                 $location.path('/payment');
            }

            $scope.openDetailsPage = function () {
                if ($scope.page_From === "payment_approved" || $scope.page_From === "payment_past" || $scope.page_From === "payment_upcoming")
                {
                    $scope.paymenthistory();
                } else if ($scope.page_From === "members_active" || $scope.page_From === "members_onhold" || $scope.page_From === "members_cancelled")
                {
                    $scope.membershipdetails();
                } else {
                    $scope.membershipdetails();
                }
            };
            
            $scope.AttendanceDateOverrideValidation =function() {
                if((($scope.calender_attendancedate && $scope.calender_attendancedate !== "aN/aN/NaN") || ($scope.attendancedate && $scope.attendancedate !== "aN/aN/NaN")) && $scope.attendance_limit_flag === "Y"){
                    var cur_atten_date = $scope.attendancedate.split('/');
                    cur_atten_date = cur_atten_date[2]+"-"+cur_atten_date[0]+"-"+cur_atten_date[1];                    
                    if(new Date(cur_atten_date) > new Date()){
                        var date = new Date();
                        cur_atten_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                    }
                    
                    $('#progress-full').show();
                    $http({
                        method: 'POST',
                        url: urlservice.url + 'getAttendanceDateOverrideValidation',
                        data: {
                            "company_id": $localStorage.company_id,
                            "reg_id": $scope.currentMembership_reg_id,
                            "selected_date": cur_atten_date,
                            "override_flag" : $scope.override_flag
                        },
                        headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                    }).then(function (response) {
                        if (response.data.status === 'Success') {
                            $('#progress-full').hide();
                            $scope.attendance_count_exceed = response.data.msg.class_excd_flg;
                            if ($scope.attendance_date_change === 'N') {
                                if ($scope.attendance_count_exceed === 'Y') {
                                    $scope.showAttendanceExceedModal();
                                } else {
                                    $scope.showAttendanceModal('N');
                                }
                            } else if (response.data.status === 'Expired') {
                                console.log(response.data);
                                $('#progress-full').hide();
                                $("#messageModal").modal('show');
                                $("#messagetitle").text('Message');
                                $("#messagecontent").text(response.data.msg);
                                $scope.logout();
                            } else {
                                if ($scope.attendance_count_exceed === 'Y') {
                                    $scope.showAttendanceExceedModal();
                                } else {
                                    $scope.attendance_date_change = 'N';
                                    $scope.addorDeleteAttendance('A');
                                }
                            }
                        } else if (response.data.status === 'Version') {
                            $('#progress-full').hide();
                            $scope.handleFailure(response.data.msg);
                        } else {
                            $('#progress-full').hide();
                            console.log(response.data);
                        }
                    }, function (response) {
                          console.log(response.data);
                          $scope.handleError(response.data);
                    });                    
                }else if((($scope.calender_attendancedate && $scope.calender_attendancedate !== "aN/aN/NaN") || ($scope.attendancedate && $scope.attendancedate !== "aN/aN/NaN")) && $scope.attendance_limit_flag === "N"){
                    $scope.showAttendanceModal('N');
                }else{
                    $scope.calender_attendancedate = $scope.attendancedate = "";
                    $scope.showAttendanceModal('N');
                }
            };
            
            
            $scope.showAttendanceExceedModal =function() {              
                  $("#attendanceExceedCountModal").modal('show');
                  $("#attendanceExceedCounttitle").text('Message');
                  $("#attendanceExceedCountcontent").text('Attendance limits has been reached for this member, do you want to override and add another attendance record?');
            };
            
            $scope.cancelAttendanceExceedModal =function() {
                $scope.datepickershow = $scope.timepickershow = false;                
                $("#attendanceExceedCountModal").modal('hide');
                $("#attendancemodal").modal('hide');
                $scope.attendancedate = $scope.attendancetime = $scope.delete_attendance_id = '';
                $scope.datetimeerror = false;
                $scope.override_flag = 'N';                
                $scope.attendance_date_change = 'N';
            }; 
            
            
            $scope.showAttendanceModal =function(override_flag) {
                if($scope.attendance_date_change === 'N'){
                    $("#attendanceExceedCountModal").modal('hide');
                    $scope.datepickershow = $scope.timepickershow = false;
                    $("#attendancemodal").modal('show');
                    $scope.attendancedate = $scope.calender_attendancedate;
                    $scope.attendancetime = $scope.delete_attendance_id ='';
                    $scope.override_flag = override_flag;
                    if($scope.attendancedate){
                        var cur_atten_date = $scope.attendancedate.split('/');
                        cur_atten_date = cur_atten_date[2]+"-"+cur_atten_date[0]+"-"+cur_atten_date[1];                    
                        if(new Date(cur_atten_date) > new Date()){
                            var date = new Date();
                            $scope.attendancedate = ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();
                        }
                        $scope.catchAttendanceDate('N');                    
                        $scope.showDatePicker();
                    }else{
                        $scope.attendancedate = "";
                    }                    
                }else{
                    $scope.override_flag = override_flag;
                    $scope.attendance_date_change = 'N';
                    $scope.addorDeleteAttendance('A');
                }                
            };
            
            $scope.showDatePicker =function() {
                $scope.datepickershow = true;
                $scope.attendance_date_change = 'N';
                $('#attendancedate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true                    
                });
                $('#attendancedate').datepicker('setEndDate', new Date());
            };
            
            $scope.showTimePicker =function() {
                $scope.timepickershow = true;
            };
            
            $scope.catchAttendanceDate =function(onselect) {
                var date = new Date();
                $scope.currentDate = ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();
                $scope.attendancetime = "";
                if($scope.attendancedate === $scope.currentDate ){                     
                    $('#attendancetime').timepicker("remove");
                    $('#attendancetime').timepicker({
                        timeFormat: 'h:i A',
                        step: 15,
                        ignoreReadonly: true,
                        maxTime : new Date()
                    });  
                }else{
                    $('#attendancetime').timepicker("remove");
                    $('#attendancetime').timepicker({
                        timeFormat: 'h:i A',
                        step: 15,
                        ignoreReadonly: true
                    }); 
                }
                if(onselect === 'Y'){
                    if($scope.attendance_limit_flag === "Y"){
                        $scope.attendance_date_change = 'Y';
                    }else{
                        $scope.attendance_date_change = 'N';
                    }
                }else{
                    $scope.attendance_date_change = 'N';
                }
            };            
            
            $scope.timeServerformat = function (time) {
                if (time.length > 0) {
                    var hours = Number(time.match(/^(\d+)/)[1]);
                    var minutes = Number(time.match(/:(\d+)/)[1]);
                    var AMPM = time.match(/\s(.*)$/)[1];
                    if (AMPM == "PM" && hours < 12)
                        hours = hours + 12;
                    if (AMPM == "AM" && hours == 12)
                        hours = hours - 12;
                    var sHours = hours.toString();
                    var sMinutes = minutes.toString();
                    if (hours < 10)
                        sHours = "0" + sHours;
                    if (minutes < 10)
                        sMinutes = "0" + sMinutes;
                    var formatedtime = sHours + ":" + sMinutes + ":01";
                    return formatedtime;
                } else {
                    var formatedtime = "";
                    return formatedtime;
                }
            };
            
            $scope.dateformat = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }
            };
            
            $scope.cancelAttendance =function() {
                $scope.datepickershow = $scope.timepickershow = false;                
                $("#attendancemodal").modal('hide');
                $scope.attendancedate = $scope.attendancetime = $scope.delete_attendance_id = '';
                $scope.datetimeerror = false;
                $scope.override_flag = 'N';                
                $scope.attendance_date_change = 'N';
            };   
            
            $scope.cancelDeleteAttendance =function() {
                $scope.datepickershow = $scope.timepickershow = false;   
                $scope.attendancedate = $scope.attendancetime = $scope.delete_attendance_id = '';
                $("#attendanceHistoryDeleteModal").modal('hide');
            };
            
            $scope.deleteAttendanceHistory = function(att_id){
                  $("#attendanceHistoryDeleteModal").modal('show');
                  $("#attendanceHistoryDeletetitle").text('Message');
                  $("#attendanceHistoryDeletecontent").text('Are you sure you want to delete Attendance History');
                  $scope.delete_attendance_id = att_id ;
                  $scope.override_flag = 'N';
            };
            
            $scope.addorDeleteAttendance =function(flag) {
                if($scope.attendance_date_change === 'Y' && $scope.attendance_limit_flag === "Y" && flag === 'A'){
                    $scope.AttendanceDateOverrideValidation();
                }else{
                    var attendance_date_time;
                    var date = new Date();
                    var current_time =  date.getHours() + ':' + date.getMinutes()+":" + date.getSeconds();
                    var current_Date_Time = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2) +" "+current_time;
                    if($scope.attendancedate && $scope.attendancetime){
                        attendance_date_time = $scope.dateformat($scope.attendancedate)+" "+$scope.timeServerformat($scope.attendancetime);
                        if(Date.parse(attendance_date_time) > Date.parse(current_Date_Time)){
                            $scope.datetimeerror = true;
                            return false;
                        }else{
                             $scope.datetimeerror = false;
                        }
                    }else{
                        attendance_date_time = "";
                    }
                    $('#progress-full').show();
                    $http({
                        method: 'POST',
                        url: urlservice.url + 'deleteorinsertAttendance',
                        data: {
                            "company_id": $localStorage.company_id,
                            "reg_id": $scope.currentMembership_reg_id,
                            "attendance_date_time": attendance_date_time,
                            "attendance_id": $scope.delete_attendance_id,
                            "flag": flag,
                            "category": "M",
                            "override_flag" : $scope.override_flag
                        },
                        headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                    }).then(function (response) {
                        if (response.data.status === 'Success') {
                            $('#progress-full').hide();
                            $("#attendancemodal").modal('hide');
                            $("#attendanceHistoryDeleteModal").modal('hide');
                            $("#attendanceExceedCountModal").modal('hide');
                            $("#successmessageModal").modal('show');
                            $("#successmessagetitle").text('Message');
                            $("#successmessagecontent").text(response.data.msg);
                            $scope.attendance_history = response.data.attendance_details.msg.attendance_history;
                            $scope.attendance_history_array = angular.copy($scope.attendance_history); 
                            $scope.attendance_count_exceed = response.data.attendance_details.msg.class_excd_flg;
                            $scope.attendance_days_count = response.data.attendance_details.days_count;
                            $scope.attendancedate = $scope.attendancetime = '';
                            $scope.active_dates = $scope.checkedin_dates = [];
                            $scope.active_dates = response.data.attendance_details.msg.checkin_dates;
                            $scope.datetimeerror = false;
                            $scope.override_flag = $scope.attendance_count_exceed = 'N';
                            $scope.attendance_date_change = 'N';
                            $scope.calender_attendancedate = '';
                            $("#selectedDateCalender").datepicker('remove');
                            if($scope.active_dates.length > 0){
                                for(i=0;i<$scope.active_dates.length;i++){
                                    $scope.checkedin_dates.push($scope.active_dates[i]);
                                }
                            }else{
                                $scope.active_dates = $scope.checkedin_dates = [];
                            }                        
                            var active_dates = $scope.checkedin_dates;
                            $("#selectedDateCalender").datepicker({
                                 beforeShowDay: function(date){
                                     var d = date;
                                     var curr_date = d.getDate();
                                     var curr_month = d.getMonth() + 1; //Months are zero based
                                     var curr_year = d.getFullYear();
                                     var formattedDate = curr_date + "/" + curr_month + "/" + curr_year;
                                        if ($.inArray(formattedDate, active_dates) !== -1){
                                           return {
                                              classes: 'activeDates'
                                           };
                                       }
                                      return;
                                }
                            }).on("changeDate", function (e) {
                                var selected_date = new Date(e.dates);
                                var currentDate = ('0' + (selected_date.getMonth() + 1)).slice(-2) + '/' + ('0' + selected_date.getDate()).slice(-2) + '/' + selected_date.getFullYear();
                                $scope.calender_attendancedate = $scope.attendancedate = currentDate;
                            });
                        } else if (response.data.status === 'Expired') {
                            console.log(response.data);
                            $('#progress-full').hide();
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Message');
                            $("#messagecontent").text(response.data.msg);
                            $scope.logout();
                        } else if (response.data.status === 'Version') {
                            $('#progress-full').hide();
                            $scope.handleFailure(response.data.msg);
                        } else {
                            $('#progress-full').hide();
                            $("#successmessageModal").modal('show');
                            $("#successmessagetitle").text('Message');
                            $("#successmessagecontent").text(response.data.msg);
                            console.log(response.data);
                        }
                    }, function (response) {
                        console.log(response.data);
                         $scope.handleError(response.data);
                    });
                }
            };
            
            
            
            $scope.confirm_attendance_History_Delete = function(){
                $("#attendanceHistoryDeleteModal").modal('hide');
                $('#progress-full').show();
                  $http({
                    method: 'POST',
                    url: urlservice.url + 'confirm_attendance_History_Delete_details',
                    data: {
                        "company_id": $localStorage.company_id,
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        console.log(response.data);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
           
            $scope.updateScheduledCancellation = function () {
                $('#memHistoryNoteDeleteCancelModal').hide();
                $('#progress-full').show();
                if ($scope.edit_status === 'delete_cancellation') {
                    $scope.memcanceldates = "";
                }
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateMembershipHistoryinDB',
                    data: {
                        "company_id": $localStorage.company_id,
                        "history_id": $scope.edited_mem_history_id,
                        "type": $scope.edit_status,
                        "selected_membership_id": $scope.editMembership_reg_id,
                        "cancellation_date": $scope.memcanceldates
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.membership_history = response.data.membership_history.msg;
//                        $scope.Membership_History();
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text(response.data.msg);
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.validatescheduledCancelllationDate = function (check_value) {
                if (check_value === 'next') {
                    $scope.check_date = $scope.showMemDateUpdate ? $scope.newpaymentdatevalue : $scope.membership_detail.next_payment_date;
                    if ($scope.cancel_date_flg === 'Y' && $scope.cancelleddate < $scope.check_date) {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text("Membership is scheduled for cancellation, this option is not available.");
                    } else {
                        $scope.confirmeditmembership('history');
                    }
                }
                if (check_value === 'first') {
                    $scope.check_date = $scope.showFirstPaymntDateUpdate ? $scope.final_payment_start_date : $scope.membership_detail.first_payment_date;
                    if ($scope.cancel_date_flg === 'Y' && $scope.cancelleddate < $scope.check_date) {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text("Membership is scheduled for cancellation, this option is not available.");
                    } else {
                        $scope.confirmeditfirstPayment('history');
                    }
                }
                if (check_value === 'update') {
                    $scope.check_startdate = $scope.showMemClassStartDateUpdate ? $scope.final_mem_start_date : $scope.membership_detail.membership_start_date;
                    $scope.check_enddate = $scope.showMemClassEndDateUpdate ? $scope.final_mem_end_date : $scope.membership_detail.last_available_payment_date;
                    if ( $scope.cancel_date_flg === 'Y' && ($scope.cancelleddate <= $scope.check_startdate ||  $scope.cancelleddate > $scope.check_enddate)) {
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text("Membership is scheduled for cancellation, this option is not available.");
                    } else {
                        $scope.confirmeditMembershipClassDates();
                    }
                }
            };

            $scope.pause_resume_datecompare = function (pause_date,resume_date){
                var first_date = Date.parse(pause_date);
                var second_date = Date.parse(resume_date);
                if($scope.wo_resumescheduled === 'N'){
                    if (second_date > first_date){
                        $scope.resume_lesser_than_pause = false;
                        $scope.resume_date_formatting(resume_date);
                    }else if(second_date <= first_date){
                        $scope.resume_lesser_than_pause = true;
                    }
                }
            };
            
            $scope.resume_date_formatting = function(value){                
                var split_date = value.split("-");
                value = split_date[1] + "/" + split_date[2] + "/" + split_date[0];                
                $scope.updatedate(value); 
            };
            
            $scope.createStripePaymentMethod = function(){
                $scope.stripe.createPaymentMethod('card', $scope.stripe_card).then(function (result) {
                    if (result.error) {
                        // Inform the user if there was an error. 
                        var errorElement = document.getElementById('stripe-card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        // Send paymentMethod.id to server 
                        (result.paymentMethod.id)? $scope.payment_method_id = result.paymentMethod.id : $scope.payment_method_id = "";
                        $scope.updateStripePaymentMethodInDB($scope.payment_method_id, $scope.stripe_first_name, $scope.stripe_last_name, $scope.stripe_email);
                    }
                });
            };
            
            $scope.updateStripePaymentMethodInDB = function(payment_method_id, user_first_name, user_last_name, email){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateStripePaymentMethod',
                    data: {
                        company_id: $localStorage.company_id,
                        reg_id: $scope.editMembership_reg_id,
                        buyer_first_name: user_first_name,
                        buyer_last_name: user_last_name,
                        email: email,
                        category_type: 'M',
                        page_key: 'MD',     // MEMBERSHIP DETAIL
                        type:$scope.selectpaymentmethod,
                        payment_method: payment_method_id, 
                },
                headers: {"Content-Type": 'application/json; charset=utf-8', },
                withCredentials: true

            }).then(function (response) {                
                    if (response.data.status === 'Success') {
                        $("#UpdateMemPaymentMethodModal").modal('hide');
                        $("#UpdateMemPaymentMethodModaltitle").text('');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        $("#successmessagetitle").text('Message');
                        $("#successmessagecontent").text('Payment method successfully updated.');
                        $scope.onetimecheckout = 0;
                        $scope.clearStripeCardData();
                        $scope.membership_detail = response.data.reg_details.msg;
                        $scope.category_list = $scope.membership_detail.All;
                        $scope.memstatus = $scope.membership_detail.membership_status;
                        
                        $scope.membershipedit = angular.copy($scope.membership_detail);
                        $scope.editMembership_reg_id = $scope.membership_detail.membership_registration_id;
                        $scope.editMembership_opt_id = $scope.membership_detail.membership_option_id;
                        $scope.editMembership_id = $scope.membership_detail.membership_id;
                        $scope.editMembershipStatus = $scope.membership_detail.membership_status;
                        $scope.editMembershipCategoryTitle = $scope.membership_detail.membership_category_title;
                        $scope.editMembershipOption_title = $scope.membership_detail.membership_title;
                        $scope.name= $scope.membership_detail.buyer_name.split(" ");
                        $scope.firstname = $scope.name[0];
                        $scope.lastname = $scope.name[1];
                        $scope.email = $scope.membership_detail.buyer_email;
                        
                    } else if (response.data.status === 'Expired') {
                        $("#editMempaymentMethodModal").modal('hide');
                        $("#editMempaymentMethodActiontitle").text('');
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $("#editMempaymentMethodModal").modal('hide');
                        $("#editMempaymentMethodActiontitle").text('');
                        $('#progress-full').hide();
                        $("#successmessageModal").modal('show');
                        if (response.data.error) {
                            $("#successmessagetitle").text(response.data.error);
                            $("#successmessagecontent").text(response.data.error_description);
                        } else {
                            $("#successmessagetitle").text('Message');
                            $("#successmessagecontent").text(response.data.msg);
                        }
                        $scope.onetimecheckout = 0;
                    }
                }, function (response) {
                    $("#editMempaymentMethodModal").modal('hide');
                    $("#editMempaymentMethodActiontitle").text('');
                    console.log(response.data);
                    $scope.handleError(response.data);
                    $scope.onetimecheckout = 0;
                });
            };

        } else {
            $location.path('/login');
        }
    }
    module.exports =  MembershipDetailController;

// TRANSFERRING MEMBERSHIP DETAILS VALUE FROM MEMBERHSIP DETAIL TO MANAGE MEMBERHSIP SCREEN

