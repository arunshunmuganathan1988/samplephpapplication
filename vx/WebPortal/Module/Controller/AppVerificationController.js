
AppVerificationController.$inject = ['$scope', '$location', '$http', '$localStorage', 'urlservice', '$route', '$rootScope'];
 function AppVerificationController($scope, $location, $http, $localStorage, urlservice, $route, $rootScope)
    {
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.sideBarHide = false;
            $rootScope.activepagehighlight = 'appverification';
            $localStorage.currentpage = "appverification";
            $scope.verification_code = "";
            $scope.verification_link = "";
            $scope.memberemail = "";
            
            $scope.getAppVerificationDetails = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendverificationcode',
                    data: {
                        "email": $scope.memberemail,
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $scope.verification_code = response.data.verification_code;
                        $scope.verification_link = response.data.verification_link; 
                        $("#appverifymessageModal").modal('show');
                        $("#appverifymessagetitle").text('Message');
                        $("#appverifymessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#appverifymessageModal").modal('show');
                        $("#appverifymessagetitle").text('Message');
                        $("#appverifymessagecontent").text(response.data.msg);
                        $scope.logout(); 
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $scope.verification_code = "";
                        $scope.verification_link = "";
                        $("#appverifymessageModal").modal('show');
                        $("#appverifymessagetitle").text('Message');
                        $("#appverifymessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            //App verification code copied on clipboard
            $scope.copyAppVerifyCode = function () { 
                var x = document.getElementById("snackbar")
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            
             //App verification link copied on clipboard
            $scope.copyAppVerifyLink = function () { 
                var x = document.getElementById("linksnackbar")
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };


        } else {
            $location.path('/login');
        }
    }
    module.exports =  AppVerificationController;

