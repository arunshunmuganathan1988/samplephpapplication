WhiteLabelDashboardController.$inject=['$scope',  '$compile','$location', '$http', '$localStorage', 'urlservice',  '$filter', '$route', 'DTOptionsBuilder', 'DTColumnDefBuilder','DTColumnBuilder','DTDefaultOptions','$window','$rootScope'];
function WhiteLabelDashboardController($scope, $compile, $location, $http, $localStorage, urlservice, $filter, $route,DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder,DTDefaultOptions,$window,$rootScope) {

if($localStorage.isadminlogin === 'Y') {
        $rootScope.sideBarHide = true;
        $rootScope.adminsideBarHide = true;
        $rootScope.hidesidebarlogin = false;
        $rootScope.activepagehighlight = 'adminwhitelabel';
        $rootScope.admincmpnynme = $localStorage.admincmpnynme;
        tooltip.hide();
        $scope.status = 'Rejected';
        $scope.androidstatus = "A";
        $scope.changestatus = function(){
            $scope.getDetails();
        } 
        $scope.changeandroidstatus = function(){
            $scope.getDetails();
        } 
               
        $scope.getDetails = function(){
//            $scope.$apply();
                DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                $scope.whitelabeldtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'getadminWLdetails',
                           xhrFields: {
                            withCredentials: true
                         },   
                            dataSrc: function (json) {
                                $scope.wllistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "filter":$scope.status,
                                 "android_filter":$scope.androidstatus,
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                    $scope.adminlogout();
                                }else{
                                    $('#progress-full').hide();
                                }
                            }                           
                        })
                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('order', [0, 'desc'])
                    $scope.whitelabeldtColumns = [
                        
                            DTColumnBuilder.newColumn('app_name').withTitle('App Name').withClass('col_ta1').renderWith(
                            function (data, type, full,meta) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.ios_app_name + '</span>';                            
                            }),
                            DTColumnBuilder.newColumn('company_name').withTitle('Company Name').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.company_name + '</span>';
                            }), 
                            
                            DTColumnBuilder.newColumn('company_code').withTitle('Company Code').withClass('col_ta1').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.company_code + '</span>';
                            }),
                            DTColumnBuilder.newColumn('user_email').withTitle('User Email').withClass('wlcustomdt').renderWith(
                            function (data, type, full,meta) {
                                return '<span class="buyername"  data-toggle="tooltip" title="' + data + '"  ng-click="gologintool(\'' + meta.row + '\');">' + full.user_email + '</span>';
                            }),
                            
                            DTColumnBuilder.newColumn('ios_bundle_id').withTitle('Ios Bundle ID').withClass('wlcustomdt').renderWith(
                            function (data, type, full,meta) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.ios_bundle_id + '</span>';                            
                            }), 
                            DTColumnBuilder.newColumn('android_bundle_id').withTitle('Android Bundle ID').withClass('col_tc4').renderWith(
                            function (data, type, full,meta) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.android_bundle_id + '</span>';                            
                            }), 
                            DTColumnBuilder.newColumn('new_build_version').withTitle('Ios Version Number').withClass('col_tc4').renderWith(
                            function (data, type, full,meta) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.new_build_version + '</span>';                            
                            }), 
                            DTColumnBuilder.newColumn('build_status').withTitle('Ios Build Status').withClass('col_tc4').renderWith(
                            function (data, type, full,meta) {
                                if(full.build_status == 'Rejected' || full.build_status == 'Metadata Rejected') {
                                return '<a class="buyername" style="color:red"  data-toggle="tooltip" title="' + data + '" ng-click="shoewlrejectederror(\'' + meta.row + '\');">' + full.build_status + '</a>';
                            } else {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.build_status + '</span>';
                            }                          
                            }).notSortable(),
                            DTColumnBuilder.newColumn('android_version_number').withTitle('Android Version Number').withClass('col_tc4').renderWith(
                            function (data, type, full,meta) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.android_version_number + '</span>';                            
                            }),
                            DTColumnBuilder.newColumn('play_store_status').withTitle('Android Build Status').withClass('col_tc4').renderWith(
                            function (data, type, full,meta) {
                                 return '<span  data-toggle="tooltip" title="' + data + '" >' + full.android_application_status + '</span>';
                            }).notSortable(),
                            DTColumnBuilder.newColumn('ios_log_file_url').withTitle('Ios Log File').withClass('col_tc4').renderWith(
                            function (data, type, full,meta) {
                                if(full.ios_log_file_url == ""){
                                    return '<a ><span data-toggle="tooltip" title="' + data + '" >N/A</span></a>';  
                                }else{
                                    return '<a href='+full.ios_url+'><span class="buyername" data-toggle="tooltip" title="' + data + '" >Click here</span></a>';    
                                }             
                            }),
                            DTColumnBuilder.newColumn('android_log_file_url').withTitle('Android Log File').withClass('col_tc4').renderWith(
                            function (data, type, full,meta) {
                                if(full.android_log_file_url == ""){
                                    return '<a><span data-toggle="tooltip" title="' + data + '" >N/A</span></a>';  
                                }else{
                                    return '<a href='+full.android_url+'><span class="buyername" data-toggle="tooltip" title="' + data + '" >Click here</span></a>';    
                                }                            
                            })
                    ];
                   $scope.whitelabeldtInstance = {};
                   
                $('#progress-full').hide();
                angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                })
            };
            
                   $scope.shoewlrejectederror = function(row){
                       $scope.wlerrormsg = $scope.wllistcontents[row].app_store_error_msg;
                       $("#wldashboarderror").modal('show');
                   }
                   
                   $scope.gologintool = function(row){
                       $localStorage.redirectloginid = $scope.redirectloginid = $scope.wllistcontents[row].user_email;
                       $localStorage.redirectedfromwl = true;
                       $location.path("/adminlogintool");
                   }
            $scope.getDetails();
        
    }else {
            $location.path('/adminpanel');
    }
}
module.exports = WhiteLabelDashboardController;