RetailTemplateController.$inject=['$scope', '$location', '$http','$route' ,'$localStorage', 'urlservice', '$filter','$rootScope'];
function RetailTemplateController($scope, $location, $http, $route, $localStorage, urlservice, $filter,$rootScope) {
        
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.activepagehighlight = 'Retail';
            $scope.retailtemplatetype = $localStorage.retailtemplatetype;
            $localStorage.currentpage = 'templateretail';
            if($scope.retailtemplatetype === 'C'){
                $scope.retailtemplatename = 'Product Category Template';
                $localStorage.currentpage = 'templateretail';
                $('.retail-template').css('width','220px');
            }else{
                $scope.retailtemplatename = 'Product Template';
                $localStorage.currentpage = 'templateretail';
                $('.retail-template').css('width','150px');
            }
            $('#progress-full').show();
            $scope.retailtemplate=function(){    
                $localStorage.retailTemplate = '';
                  $http({
                    method: 'POST',
                    url: urlservice.url + 'getRetailProductTemplates',
                    data: {
                     "company_id": $localStorage.company_id,
                     "template_type": $scope.retailtemplatetype
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                                },
                                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') { 
                       $('#progress-full').hide();
                       $localStorage.retailTemplate = $scope.retailtemplate = response.data.msg.live;
                       $localStorage.currentpage = 'templateretail';
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.retailtemplate();
            
            $scope.retailtempcopy = function (retail_id) {
                $('#progress-full').show();
                $http({
                method: 'POST',
                        url: urlservice.url + 'copyRetail',
                        data: {
                            'retail_product_id': retail_id,
                            'retail_template_flag': 'Y',
                            'company_id': $localStorage.company_id,
                            'list_type':'P',
                            'from':'T'
                        },
                        headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                        }).then(function (response) {
                   if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $localStorage.retailfromliveordraft ='draft';
                        $location.path('/manageretail');
                    }else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else{
                         $scope.handleFailure(response.data.msg);
                    }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                });
            };
           
        } else {
            $location.path('/login');
        }
    };
    
    module.exports=RetailTemplateController;







