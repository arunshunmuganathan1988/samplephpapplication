MessageController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$route', '$filter','$rootScope'];
 function MessageController ($scope, $location, $http, $localStorage, urlservice, $route, $filter, $rootScope)
    {

        if ($localStorage.islogin == 'Y') {

            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().regComplete();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();

            //First time login message Pop-up ----START 
            tooltip.hide();
            $rootScope.activepagehighlight = 'com_center';
            $scope.bottom_hit_limit = 0;
            $scope.top_hit_limit = 0;
            $scope.month_list = ['January','Febrauary','March','April','May','June','July','August','September','October','November','December'];
            $scope.conversationarray = [];
            $scope.chat_msg_arr = [];
            $scope.current_student_id = $scope.searchconversation = '';
            $scope.conversationview = true;
            $scope.pushmessageview = false;
            $scope.composemessageview = $scope.composemessage_send_view = false;
            $scope.composearray = [];
            $scope.compose_studentid = [];
            $scope.bottomhitstopcall = false;
            $scope.stop_double_call = true;
            $scope.tophitstopcall = false;
            $scope.chatmessage = $scope.grpchatmessage = '';
            $scope.send_chat_student_id = [];
            $scope.last_view_message_id = 0;
            $scope.last_view_message_datetime = "";
            $rootScope.currentpage_for_conversation = 'conversation'; 
            $scope.call_started = false;
            $rootScope.stopintervalcall = false;
            $scope.fromtab = $rootScope.from_tab = 'I';
            
            $scope.inboxDivView = false;
            $scope.filtername = 'Inbox';
            var timeout =null;
            $rootScope.conv_selected_student_id = $rootScope.conv_last_msg_id = $rootScope.conv_last_msg_datetime = $rootScope.student_last_msg_id = $rootScope.conversation_search_term = '';
            if ($localStorage.verfication_complete !== 'U') {
                if ($localStorage.firstlogin) {
                    if ($localStorage.firstlogin_message === 'Y') {
                        $("#tooltipfadein").css("display", "block");
                        $localStorage.firstlogin_message = 'N';
                        $scope.msgetip = false;
                        tooltip.pop("msglink", "#msgtip", {position: 1});
                    }
                }
            }

            $("#messagedetailNext").click(function () {
                $("#tooltipfadein").css("display", "none");
                $scope.msgetip = true;
                tooltip.hide();
            });
            //First time login message Pop-up ----END

            $scope.msgurlfrmt = /^\s*((ftp|http|https)\:\/\/)?([a-z\d\-]{1,63}\.)*[a-z\d\-]{1,255}\.[a-z]{2,6}\s*.*$/;
            $localStorage.currentpage = "message";
            $localStorage.PreviewEventNamee = "";
            $localStorage.previewEditMessageIndex = "";
            $('#progress-full').hide();
            $scope.saveedit =  false;
            $scope.nomessage =  true;
            $localStorage.savepushstatus = false;
            $scope.pushemail = $scope.pushmsg = $scope.editmsgkey = "";
            $scope.title = "Send New Push Message";
//            $scope.buttonname = 'Send now';
            $scope.msgtime='';
            $scope.scheduleMessage = false;
            $scope.$on('$viewContentLoaded', function () {
                $('#msgdate').datepicker({
                    format: 'yyyy-mm-dd',
                    ignoreReadonly: true,
                    autoclose: true,
                });
                $('#msgtime').timepicker({
                    timeFormat: 'h:i A',
                    step: 15,
                    ignoreReadonly: true
                });
                $('#msgdate').datepicker('setStartDate', new Date());
                $("#msgdate").on("dp.change", function () {
                    $scope.msgdate = $("#msgdate").val();
                });

                $("#msgtime").on("dp.change", function () {
                    $scope.msgtime = $("#msgtime").val();
                });
            });

            $scope.catchChange = function () {
                $localStorage.PreviewMessageDText = $scope.notification;
            };

            $scope.topreview = function () {
                document.getElementById('previewapp').scrollIntoView();
            };

            $scope.getmessages = function () {
                
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getmessagedetails',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $localStorage.msgcontent = $scope.msgcontent = response.data.msg;
                        $localStorage.company_tz = $scope.company_tz = response.data.tz;
                        $localStorage.company_tz_offset = $scope.company_tz_offset = response.data.tz_offset;
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $scope.nomessage = false;
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            //Muthulakshmi conversation
            $scope.getconversationlist = function () {
                if($scope.bottomhitstopcall){
                    return false;
                }
                if($scope.fromtab === "S" && (!$scope.searchconversation || $scope.searchconversation === "")){
                    return false;
                }
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getChatList',
                    params: {
                        "company_id": $localStorage.company_id,
                        "searchterm":$scope.searchconversation,
                        "limit":$scope.bottom_hit_limit,
                        "from_tab":$scope.fromtab
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        if(response.data.msg.length == 0){
                            $scope.bottomhitstopcall = true;
                        }else{
                          $rootScope.student_last_msg_id = response.data.msg[0].max_msg_id;  
                        }
//                        $scope.chat_msg_arr = [];
                        if($scope.searchconversation){
                            $scope.conversationarray = [];
                        }
                        for (var i = 0; i < response.data.msg.length; i++) {
                            $scope.conversationarray.push(response.data.msg[i]);
                        }
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.getconversationlist();
            
            $scope.getStudentList = function () {
                if($scope.composearray.length > 0){
                    
                }else{
                    $('#progress-full').show();
                    $http({
                        method: 'GET',
                        url: urlservice.url + 'getStudentListForChat',
                        params: {
                            "company_id": $localStorage.company_id
                        },
                        headers: {"Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                    }).then(function (response) {
                        if (response.data.status == 'Success') {
                            $('#progress-full').hide();
                            $scope.studentarray = response.data.msg;
                        } else if (response.data.status === 'Expired') {
                            $('#progress-full').hide();
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Message');
                            $("#messagecontent").text(response.data.msg);
                            $scope.logout();
                        } else {
                            $('#progress-full').hide();
                            $scope.handleFailure(response.data.msg);
                        }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });
                }
            };
            
            $scope.opencomposeModal = function () {
                $scope.inboxDivView = false;
                $('.inbox-or-unread').css("color","black");
                $('.inbox-view-dropdown').css("color","black");
                $scope.chat_msg_arr = [];
                $('#convcomposeicon').addClass('active');
                $scope.chatResetTextarea();
                $scope.stop_top_hit = true;
                $scope.composemessageview = true; 
                $scope.composemessage_send_view = false;
                $scope.composearray = [];
                $scope.compose_studentid = [];
                $scope.studentarray = [];
                $scope.current_student_id = '';
                $scope.grpchatmessage = '';
                $scope.composestudentselected = ""; 
                $rootScope.conv_selected_student_id = '';
                document.getElementById("compose_textarea").value = "";
                $scope.getStudentList();
            };
            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
            };
            $scope.openConversation = function () {
                $scope.filtername = 'Inbox';
                $scope.fromtab = $rootScope.from_tab = 'I' ;
                $scope.stop_top_hit = true;
                $rootScope.currentpage_for_conversation = 'conversation';
                $scope.conversationview = true;
                $scope.pushmessageview = false;
                $scope.composeCancel();
                $scope.chatResetTextarea();
                $scope.resettabbarclass();
                $('#li-messages').addClass('active-event-list').removeClass('event-list-title');
                $('#li-Conversations').addClass('active-event-list').removeClass('event-list-title');
                $scope.bottom_hit_limit = 0;
                $scope.current_student_id = $scope.searchconversation = $rootScope.conversation_search_term = '';
                $rootScope.conv_selected_student_id = $rootScope.conv_last_msg_id = $rootScope.conv_last_msg_datetime= $rootScope.student_last_msg_id ='';
                $scope.bottomhitstopcall = false;
                $scope.inboxDivView = false;
                $('.inbox-or-unread').css("color","black");
                $('.inbox-view-dropdown').css("color","black");
                $scope.conversationarray = [];
                $scope.getconversationlist();
            };
            $scope.openPushMsg = function () {
                $scope.filtername = 'Inbox';
                $rootScope.currentpage_for_conversation = $rootScope.conversation_search_term = '';
                $rootScope.conv_selected_student_id = $rootScope.conv_last_msg_id = $rootScope.conv_last_msg_datetime = $rootScope.student_last_msg_id ='';
                $scope.conversationview = false;
                $scope.pushmessageview = true;
                $scope.inboxDivView = false;
                $('.inbox-or-unread').css("color","black");
                $('.inbox-view-dropdown').css("color","black");
                $scope.resettabbarclass();
                $('#li-messages').addClass('active-event-list').removeClass('event-list-title');
                $('#li-msgallusers').addClass('active-event-list').removeClass('event-list-title');
                $scope.getmessages();
            };
            $scope.openInboxDiv = function(){
                $scope.inboxDivView = $scope.inboxDivView === false ? true: false;
                if($scope.inboxDivView == false){
                    $('.inbox-or-unread').css("color","black");
                    $('.inbox-view-dropdown').css("color","black");
                }else{
                     $('.inbox-or-unread').css("color","#009a61");
                     $('.inbox-view-dropdown').css("color","#009a61");
                }
            };
            $scope.openConversationInbox = function(){
                $rootScope.currentpage_for_conversation = 'conversation'; 
                if($scope.filtername === 'Inbox'){
                    return false;
                }
                $scope.filtername = 'Inbox';
                $scope.fromtab = $rootScope.from_tab = 'I';
                $scope.openConversation();
            };
            
            $scope.openUnread = function(){
                $rootScope.currentpage_for_conversation = 'conversation'; 
                if($scope.filtername === 'Unread'){
                    return false;
                }
                $scope.filtername = 'Unread';
                $scope.inboxDivView = false;
                $('.inbox-or-unread').css("color","black");
                $('.inbox-view-dropdown').css("color","black");
                $scope.bottom_hit_limit = 0;
                $scope.fromtab =  $rootScope.from_tab = 'U';
                $scope.composeCancel();
                $scope.chatResetTextarea();
                $scope.current_student_id = $scope.searchconversation = $rootScope.conversation_search_term = '';
                $rootScope.conv_selected_student_id = $rootScope.conv_last_msg_id =$rootScope.conv_last_msg_datetime = $rootScope.student_last_msg_id ='';
                $scope.bottomhitstopcall = false;
                $scope.conversationarray = [];
                $scope.getconversationlist();
            };
            
            $scope.openSent = function(){
                $rootScope.currentpage_for_conversation = ''; 
                if($scope.filtername === 'Sent'){
                    return false;
                }
                $scope.filtername = 'Sent';
                $scope.inboxDivView = false;
                $('.inbox-or-unread').css("color","black");
                $('.inbox-view-dropdown').css("color","black");
                $scope.bottom_hit_limit = 0;
                $scope.fromtab = $rootScope.from_tab = 'S';
                $scope.composeCancel();
                $scope.chatResetTextarea();
                $scope.current_student_id = $scope.searchconversation = $rootScope.conversation_search_term = '';
                $rootScope.conv_selected_student_id = $rootScope.conv_last_msg_id =$rootScope.conv_last_msg_datetime = $rootScope.student_last_msg_id ='';
                $scope.bottomhitstopcall = false;
                $scope.conversationarray = [];
            };
            
            $scope.catchsearchconversation = function(){
                $scope.inboxDivView = false;
                $('.inbox-or-unread').css("color","black");
                $('.inbox-view-dropdown').css("color","black");
                $scope.composeCancel();
                $scope.bottomhitstopcall = false;
                $scope.bottom_hit_limit = 0;
                $rootScope.conversation_search_term = $scope.searchconversation;
                $scope.current_student_id = $rootScope.conv_selected_student_id = '';
                $scope.chat_msg_arr = [];
                $scope.conversationarray = [];
                clearTimeout(timeout);
                // Make a new timeout set to go off in 800ms
                timeout = setTimeout(function () {
                    $scope.getconversationlist();
                }, 500);
            };
            $scope.catchChatMsg = function () {
                if($scope.chatmessage){
                    var available_height = document.getElementById('chat_textarea').scrollHeight;
                    if(available_height < 48){
                        $('#chat_textarea').height(20);
                        $('#chat_textarea_container').css("bottom", "-12"+ 'px');
                    }else if(available_height < 67){
                        $('#chat_textarea').height(38);
                        $('#chat_textarea_container').css("bottom", "5"+ 'px');
                    }else if(available_height < 86){
                        $('#chat_textarea').height(57);
                        $('#chat_textarea_container').css("bottom", "25"+ 'px');
                    }else{
                        $('#chat_textarea').height(57);
                        $('#chat_textarea').css("overflow", "auto");
                        $('#chat_textarea_container').css("bottom", "25"+ 'px');
                    }
                   $('.conv-send-icon-div').addClass( "active");
                }else{
                    $('#chat_textarea').height(20);
                    $('#chat_textarea_container').css("bottom", "-12"+"px");
                    $('#chat_textarea').css("overflow", "hidden");
                    $('.conv-send-icon-div').removeClass( "active");
                }
            };

            $scope.pushIntoStudentarr = function () {               
                var student = $scope.composestudentselected;
                $scope.composestudentselected = "";    
                var removeindex = $scope.studentarray.map(function(x) {return x.student_id; }).indexOf(student.student_id);
                if(removeindex > -1){
                    var removeid = $scope.studentarray[removeindex].student_id;
                       if(removeid === $scope.composearray.student_id){
                           
                        }else{
                            $scope.compose_studentid.push(student.student_id);
                            $scope.composearray.push(student);
                        } 
                    $scope.studentarray.splice(removeindex,1);
                }
                $scope.$apply();
            };
            
            $scope.popFromComposearr = function (compose) {
                var removeindex = $scope.composearray.map(function(x) {return x.student_id; }).indexOf(compose.student_id);
                $scope.composearray.splice(removeindex,1);
                $scope.compose_studentid.splice(removeindex,1);
                $scope.studentarray.unshift(compose); //insert element at the top
            };
            
            $scope.composeNxtBtn = function () {
                $scope.composemessageview = false; 
                $scope.composemessage_send_view = true;
            };
            
            $scope.composeCancel = function () {
                $scope.inboxDivView = false;
                $('.inbox-or-unread').css("color","black");
                $('.inbox-view-dropdown').css("color","black");
                $('#convcomposeicon').removeClass('active');
                $('#convcomposeicon').attr("src", "image/flags/compose_black_icon.png");
                $scope.grpchatmessage = '';
                $scope.composestudentselected = "";    
                document.getElementById("compose_textarea").value = "";
                $scope.composemessageview = false; 
                $scope.composemessage_send_view = false;
                $rootScope.conv_selected_student_id = '';
                $scope.composearray = [];
                $scope.studentarray = [];
                $scope.compose_studentid = [];
                $scope.chat_msg_arr = [];
            };
            $scope.chatResetTextarea = function () {
               $('#chat_textarea').height(20);
                $('#chat_textarea_container').css("bottom", "-12"+"px");
                $('#chat_textarea').css("overflow", "hidden"); 
            }
            $scope.scrollbottomandgetchat = function(studentid,index){
                $scope.selected_student_index = index;
                $('#progress-full').show();
                $scope.chat_msg_arr = [];
                if($scope.conversationarray[index].read_status === 'U'){
                   $localStorage.new_conversation_Count =$localStorage.new_conversation_Count - 1;
                }
                $scope.stop_double_call = true;
                $scope.composeCancel();
                $scope.chatResetTextarea();
                $scope.chatmessage = '';
                $scope.conversationarray[index].read_status = 'R';
                $scope.stop_top_hit = false;
                $scope.last_view_message_id = 0;
                $scope.last_view_message_datetime = "";
                $scope.composemessageview = false; 
                $scope.composemessage_send_view = false;
                $scope.tophitstopcall = false;
                $scope.current_student_id = studentid;
                $rootScope.conv_selected_student_id = $scope.current_student_id;
                $scope.top_hit_limit = 0;
                setTimeout(function () {
                     console.log("array length = "+$scope.chat_msg_arr.length);
                     console.log("flag"+$scope.call_started);
                    if ($scope.chat_msg_arr.length == 0) {
                        if(!$scope.call_started){
                            console.log("click hit");
                            $scope.call_started = true;
                            $scope.getIndividualChat();
                        }else{
                            $('#progress-full').hide();    
                        }
                    }else{
                        $('#progress-full').hide();    
                    }
                    setTimeout(function(){ $('#conversationmsgs').scrollTop($('#conversationmsgs')[0].scrollHeight); }, 1500);
                }, 2000);            
            };
            
            $scope.getIndividualChat = function () {
                if(!$scope.current_student_id){
                    $scope.call_started = false;
                    return false;
                }
                if($scope.tophitstopcall){
                    $scope.call_started = false;
                    return false;
                }
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getChatConversation',
                    params: {
                        "company_id": $localStorage.company_id,
                        "student_id": $scope.current_student_id,
                        "limit":$scope.top_hit_limit,
                        "last_view_message_id":$scope.last_view_message_id,
                        "last_view_message_datetime":$scope.last_view_message_datetime
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {  
                    if(response.data.msg.length > 0){
                        $scope.last_view_message_id = response.data.msg[response.data.msg.length -1].msg_by_date[response.data.msg[response.data.msg.length -1].msg_by_date.length -1].message_id;
                        $scope.last_view_message_datetime = response.data.msg[response.data.msg.length -1].msg_by_date[response.data.msg[response.data.msg.length -1].msg_by_date.length -1].message_delivery_date;
                    }
                    if (($scope.chat_msg_arr && parseInt($scope.chat_msg_arr.length) > 0) && (response.data.msg && parseInt(response.data.msg.length) > 0)) { 
                        if (($scope.chat_msg_arr[0].day === response.data.msg[0].day)) { // checking top date array(outer array) with responses bottom date array(outer array)
                            for (var i = 0; i < response.data.msg[0].msg_by_date.length; i++) { //if it is same add the reponse bottom's array(inner array) to top date of array(inner array)
                                $scope.chat_msg_arr[0].msg_by_date.unshift(response.data.msg[0].msg_by_date[i]);
                            }
                            for (var i = 1; i < response.data.msg.length; i++) { //rest of the responses dates arrays(outer) will be added to arrays(outer) which was already in front end
                                response.data.msg[i].msg_by_date.reverse();
                                $scope.chat_msg_arr.unshift(response.data.msg[i]);
                            }
                        }else{
                            for (var i = 0; i < response.data.msg.length; i++) {//all the responses dates arrays(outer) will be added to arrays(outer) which was already in front end
                                response.data.msg[i].msg_by_date.reverse();
                                $scope.chat_msg_arr.unshift(response.data.msg[i]);
                            }
                        }
                        
                        $('#progress-full').hide();
                        $scope.call_started = false;
                        return false;
                    }
                    if (response.data.msg.length == 0) {
                        $scope.tophitstopcall = true;
                        $scope.call_started = false;
                        $('#progress-full').hide();
                        return false;
                    }
                    for (var i = 0; i < response.data.msg.length; i++) {
                        response.data.msg[i].msg_by_date.reverse();
                        $scope.chat_msg_arr.unshift(response.data.msg[i]);
                    }
                    if(response.data.msg.length > 0){
                        $rootScope.conv_last_msg_id = response.data.msg[0].msg_by_date[response.data.msg[0].msg_by_date.length -1].message_id;
                        $rootScope.conv_last_msg_datetime = response.data.msg[0].msg_by_date[response.data.msg[0].msg_by_date.length -1].message_delivery_date;
                    }
                    $scope.call_started = false;
                    $('#progress-full').hide();
                   } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.call_started = false;
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $scope.call_started = false;
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.sendMultipleChatMessage = function (msg) {
                if($scope.composearray.length <= 0 || !$scope.grpchatmessage){
                    return;
                }
                if ($localStorage.upgrade_status === 'F' || $localStorage.upgrade_status === 'B') {
                        $("#messageexpiryModal").modal('show');
                        $("#messageexpirytitle").text('Message');
                        $('#progress-full').hide();
                        return false;
                    }
                $scope.composemessageview = false; 
                $scope.composemessage_send_view = false;
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendmultiplechatmessage',
                    data: {
                        "company_id": $localStorage.company_id,
                        "message_text":msg,
                        "student_id":$scope.compose_studentid,
                        "message_from":'P',
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        document.getElementById("compose_textarea").value = "";
                        $scope.composeCancel();
                        $rootScope.getinstantmessage();
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.sendChatMessage = function () {
                if ($localStorage.upgrade_status === 'F' || $localStorage.upgrade_status === 'B') {
                        $("#messageexpiryModal").modal('show');
                        $("#messageexpirytitle").text('Message');
                        $('#progress-full').hide();
                        return false;
                    }
                if(!$scope.chatmessage){
                    return;
                }
                $rootScope.stopintervalcall = true;
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendchatmessage',
                    data: {
                        "company_id": $localStorage.company_id,
                        "message_text":$scope.chatmessage,
                        "student_id":$scope.current_student_id,
                        "message_from":'P',
                        "last_message_id":$rootScope.conv_last_msg_id,
                        "last_messagedatetime":$rootScope.conv_last_msg_datetime
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.conversationarray[$scope.selected_student_index].replied_flag = 'R';
                        $('#progress-full').hide();
                        $scope.chatResetTextarea();
                        $scope.chatmessage = '';
                        $('.conv-send-icon-div').removeClass( "active");
                        if($scope.chat_msg_arr[$scope.chat_msg_arr.length -1].day === response.data.msg[0].day && $scope.chat_msg_arr.length > 0 && response.data.msg.length > 0){
                            for (var i = 0; i < response.data.msg[0].msg_by_date.length; i++) {
                                $scope.chat_msg_arr[$scope.chat_msg_arr.length -1].msg_by_date.push(response.data.msg[0].msg_by_date[i]);
                            }
                            for (var i = 1; i < response.data.msg.length; i++) {
                                response.data.msg[i].msg_by_date.reverse();
                                $scope.chat_msg_arr.push(response.data.msg[i]);
                            }
                        }else{
                            for (var i = 0; i < response.data.msg.length; i++) {
                                response.data.msg[i].msg_by_date.reverse();
                                $scope.chat_msg_arr.push(response.data.msg[i]);//have to test
                            }
                        }
                            for(var k = 0; k < $scope.chat_msg_arr[$scope.chat_msg_arr.length - 1].msg_by_date.length; k++) {//check duplicate in last index of outer array
                            for (var a = k + 1; a < $scope.chat_msg_arr[$scope.chat_msg_arr.length - 1].msg_by_date.length; a++) {
                                if ($scope.chat_msg_arr[$scope.chat_msg_arr.length - 1].msg_by_date[a].message_id === $scope.chat_msg_arr[$scope.chat_msg_arr.length - 1].msg_by_date[k].message_id) {
                                    $scope.chat_msg_arr[$scope.chat_msg_arr.length - 1].msg_by_date.splice(a, 1);
                                    a--;
                                }
                            }
                        }
                        
                        $rootScope.conv_last_msg_id = response.data.msg[response.data.msg.length - 1].msg_by_date[(response.data.msg[response.data.msg.length - 1].msg_by_date.length -1)].message_id;
                        $rootScope.conv_last_msg_datetime = response.data.msg[response.data.msg.length - 1].msg_by_date[(response.data.msg[response.data.msg.length - 1].msg_by_date.length -1)].message_delivery_date;
                        $rootScope.getinstantmessage();
                        setTimeout(function(){ $('#conversationmsgs').scrollTop($('#conversationmsgs')[0].scrollHeight); }, 100);
                        $rootScope.stopintervalcall = false;
                    } else if (response.data.status === 'Expired') {
                        $rootScope.stopintervalcall = false;
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $rootScope.stopintervalcall = false;
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $rootScope.stopintervalcall = false;
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.$watch(function () {
                    if ($localStorage.new_conversation_array) {
                        return $localStorage.new_conversation_array;
                    }
                }, function (newVal, oldVal) {
                if ($scope.conversationview) {
                    $scope.new_chat_array = [];
                    $scope.new_conversation_array = [];
                    $scope.new_chat_array = $localStorage.new_chat_array;
                    $scope.new_conversation_array = $localStorage.new_conversation_array;
//                    if(!$scope.searchconversation){
                    //middle conversation
                    if(($scope.conversationarray && $scope.conversationarray.length === 0) && ($scope.new_conversation_array && $scope.new_conversation_array.length === 0)){
                        for (var i = 0; i < $scope.new_conversation_array.length; i++) {
                            $scope.conversationarray.push($scope.new_conversation_array[i]);
                        }
                    }else if(($scope.conversationarray && $scope.conversationarray.length > 0) && ($scope.new_conversation_array && $scope.new_conversation_array.length >0)){
                        $rootScope.student_last_msg_id = $scope.new_conversation_array[0].max_msg_id;
                        for(var k = 0; k < $scope.new_conversation_array.length; k++){
                            for(var j=0 ; j < $scope.conversationarray.length ; j++){
                                if($scope.conversationarray[j].student_id === $scope.new_conversation_array[k].student_id){
                                    $scope.conversationarray.splice(j,1);
                                    j--;
                                }
                            }
                        }
                        for(var a=$scope.new_conversation_array.length -1 ; a >= 0 ; a--){
                            $scope.conversationarray.unshift($scope.new_conversation_array[a]);
                        }
                    }
                    for(var b=0;b < $scope.conversationarray.length;b++){
                        if($rootScope.conv_selected_student_id === $scope.conversationarray[b].student_id){
                            $scope.conversationarray[b].read_status = "R";
                        }
                    }
                    //right side chat
                    if(($scope.new_chat_array && $scope.new_chat_array.length > 0) && ($scope.chat_msg_arr && $scope.chat_msg_arr.length > 0) & !$rootScope.stopintervalcall){
                        
                        if($scope.chat_msg_arr[$scope.chat_msg_arr.length -1].day === $scope.new_chat_array[0].day){
                            $scope.new_chat_array[0].msg_by_date.reverse();
                            for (var i = 0; i < $scope.new_chat_array[0].msg_by_date.length; i++) {
                                $scope.chat_msg_arr[$scope.chat_msg_arr.length -1].msg_by_date.push($scope.new_chat_array[0].msg_by_date[i]);
                            }
                            for (var i = 1; i < $scope.new_chat_array.length; i++) {
                                $scope.new_chat_array[i].msg_by_date.reverse();
                                $scope.chat_msg_arr.push($scope.new_chat_array[i]);
                            }
                        }else{
                            for (var i = 0; i < $scope.new_chat_array.length; i++) {
                                $scope.new_chat_array[i].msg_by_date.reverse();
                                $scope.chat_msg_arr.push($scope.new_chat_array[i]);//have to test
                            }
                        }
                            for(var k = 0; k < $scope.chat_msg_arr[$scope.chat_msg_arr.length - 1].msg_by_date.length; k++) {//check duplicate in last index of outer array
                            for (var a = k + 1; a < $scope.chat_msg_arr[$scope.chat_msg_arr.length - 1].msg_by_date.length; a++) {
                                if ($scope.chat_msg_arr[$scope.chat_msg_arr.length - 1].msg_by_date[a].message_id === $scope.chat_msg_arr[$scope.chat_msg_arr.length - 1].msg_by_date[k].message_id) {
                                    $scope.chat_msg_arr[$scope.chat_msg_arr.length - 1].msg_by_date.splice(a, 1);
                                    a--;
                                }
                            }
                        }
//                        $scope.removeDuplicateInChatArray();
                        $rootScope.conv_last_msg_id = $scope.new_chat_array[$scope.new_chat_array.length - 1].msg_by_date[($scope.new_chat_array[$scope.new_chat_array.length - 1].msg_by_date.length -1)].message_id;
                        $rootScope.conv_last_msg_datetime = $scope.new_chat_array[$scope.new_chat_array.length - 1].msg_by_date[($scope.new_chat_array[$scope.new_chat_array.length - 1].msg_by_date.length -1)].message_delivery_date;
                    }
                    
                }else { // Not a conversation view
                    $rootScope.conv_selected_student_id = $rootScope.currentpage_for_conversation = $rootScope.conv_last_msg_id = $rootScope.conv_last_msg_datetime = '';
                }
            });
            
            $scope.removeDuplicateInChatArray = function(){
                var chat_msg_arr_length = $scope.chat_msg_arr.length -4;
                if($scope.chat_msg_arr.length >= 4){
                    for(var i = $scope.chat_msg_arr.length -1; i >= chat_msg_arr_length ;i--){//11:59 case(extreme)
                        for(var j = i-1; j >=chat_msg_arr_length ; j--){
                            if(($scope.chat_msg_arr[i].day === $scope.chat_msg_arr[j].day)){
                                $scope.chat_msg_arr.splice(j,1);
                                j--;
                                chat_msg_arr_length --;
                            }
                        }
                    }
                }
                
                for(var k = 0 ; k < $scope.chat_msg_arr[$scope.chat_msg_arr.length -1].msg_by_date.length; k++){//check duplicate in last index of outer array
                    for(var a = k+1; a < $scope.chat_msg_arr[$scope.chat_msg_arr.length -1].msg_by_date.length ; a++){
                        if($scope.chat_msg_arr[$scope.chat_msg_arr.length -1].msg_by_date[a].message_id === $scope.chat_msg_arr[$scope.chat_msg_arr.length -1].msg_by_date[k].message_id){
                            $scope.chat_msg_arr[$scope.chat_msg_arr.length -1].msg_by_date[a].splice(a,1);
                            a--;
                        }
                    }
                }
            };
            
            $("#convcomposeicon").hover(function(){
                    $(this).attr("src", "image/flags/compose_green.png");
                }, function(){
                   if($(this).hasClass('active')){
                        $(this).attr("src", "image/flags/compose_green.png");
                    }else{
                        $(this).attr("src", "image/flags/compose_black_icon.png");
                    }
                }
            );
            
            // Conversation end
            $scope.timeformat = function (time) {
                var hours = Number(time.match(/^(\d+)/)[1]);
                var minutes = Number(time.match(/:(\d+)/)[1]);
                var AMPM = time.match(/\s(.*)$/)[1];
                if (AMPM == "PM" && hours < 12)
                    hours = hours + 12;
                if (AMPM == "AM" && hours == 12)
                    hours = hours - 12;
                var sHours = hours.toString();
                var sMinutes = minutes.toString();
                if (hours < 10)
                    sHours = "0" + sHours;
                if (minutes < 10)
                    sMinutes = "0" + sMinutes;
                var formatedtime = sHours + ":" + sMinutes + ":00";
                return formatedtime;
            };


            $scope.paymentnow = function () {
                $("#sidetopbar").removeClass('modal-open');
                $(".modal-backdrop").css("display", "none");
                $(".modal-backdrop").css("display", "none");
                $localStorage.viewselectplanpage = true;
                $location.path('/billingstripe');
            };

            $scope.postmessage = function () {
                $('#progress-full').show();

                $scope.scheduleMessage ? $scope.pushmsg = 'N' : $scope.pushmsg = 'Y';
                $scope.sendemail ? $scope.pushemail = 'Y' : $scope.pushemail = 'N';

                if ($scope.msglink == "" || $scope.msglink == undefined) {
                    $scope.msglinkformat = "";

                } else {
                    if ($scope.msglink.indexOf("http://") == 0 || $scope.msglink.indexOf("https://") == 0) {
                        $scope.msglinkformat = $scope.msglink;
                    } else {
                        $scope.msglinkformat = 'http://' + $scope.msglink;
                    }
                }


                if (!angular.isUndefined($scope.msgid)) {
                    $localStorage.savepushstatus = true;
                    $scope.updatemessage();
                } else {
                    if ($scope.scheduleMessage) {
                        var date = $scope.msgdate;
                        var time = $scope.timeformat($scope.msgtime);
                        var pickdate = $scope.msgdate + " " + time; //will give local time

                        var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                                navigator.userAgent && !navigator.userAgent.match('CriOS');
                        if (isSafari) {
                            $scope.delivery = pickdate;
                        } else {
                            $scope.delivery = pickdate;
                        }
                    } else {
                        $scope.delivery = '';
                    }

                    //local storage maintain for sending message after premium purchase
                    if ($localStorage.upgrade_status === 'F' || $localStorage.upgrade_status === 'B') {
                        $localStorage.access_page = 'message';
                        $("#messageexpiryModal").modal('show');
                        $("#messageexpirytitle").text('Message');
                        $localStorage.message_text = $scope.notification;
                        $localStorage.message_link = $scope.msglinkformat;
                        $localStorage.schedule_date_time = $scope.delivery;
                        $localStorage.push_status = $scope.pushmsg;
                        $localStorage.email_status = $scope.pushemail;
                        $('#progress-full').hide();
                        return false;

                    } else if ($localStorage.access_page === 'message_premium_ok' && $localStorage.subscription_status === 'Y' && ($localStorage.upgrade_status === 'P' || $localStorage.upgrade_status === 'M' || $localStorage.upgrade_status === 'W')) {
                        $scope.notification = $localStorage.message_text;
                        $scope.msglinkformat = $localStorage.message_link;
                        $scope.delivery = $localStorage.schedule_date_time;
                        $scope.pushmsg = $localStorage.push_status;
                        $scope.pushemail = $localStorage.email_status;
                    }



                    $http({
                        method: 'POST',
                        url: urlservice.url + 'addmessage',
                        data: {
                            "message_text": $scope.notification,
                            "message_link": $scope.msglinkformat,
                            "schedule_date_time": $scope.delivery,
                            "push_status": $scope.pushmsg,
                            "email_status": $scope.pushemail,
                            "company_id": $localStorage.company_id
                        },
                        headers: {"Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                    }).then(function (response) {
                        if (response.data.status == 'Success') {
                            $('#progress-full').hide();
                            $scope.resetmsg();
                            $scope.getmessages();
                            $localStorage.access_page = $localStorage.message_text = $localStorage.message_link = '';
                            $localStorage.schedule_date_time = $localStorage.push_status = $localStorage.email_status = '';
                            if ($localStorage.firstlogin && $localStorage.firstlogin_message === 'Y') {
                                $localStorage.firstlogin_message = 'N';
                                angular.element(document.getElementById('sidetopbar')).scope().getTutorialMode();
                            } else if ($localStorage.firstlogin_message === 'Y' || $localStorage.firstlogin_message === undefined) {
                                $("#communicationModal").modal('show');
                                $("#communicationmodaltitle").text('Success Message');
                                $("#communicationmodalcontent").text(response.data.msg);
                            }
                        } else if (response.data.status === 'Expired') {
                            $('#progress-full').hide();
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Message');
                            $("#messagecontent").text(response.data.msg);
                            $scope.logout();
                        } else if (response.data.status === 'Version') {
                            $('#progress-full').hide();
                            $scope.handleFailure(response.data.msg);
                        } else {
                            $('#progress-full').hide();
                            $localStorage.access_page = $localStorage.message_text = $localStorage.message_link = '';
                            $localStorage.schedule_date_time = $localStorage.push_status = $localStorage.email_status = '';
                            if ($localStorage.firstlogin && $localStorage.firstlogin_message === 'Y') {
                                $localStorage.firstlogin_message = 'N';
                                angular.element(document.getElementById('sidetopbar')).scope().getTutorialMode();
                            } else if ($localStorage.firstlogin_message === 'Y' || $localStorage.firstlogin_message === undefined) {
                                $scope.handleFailure(response.data.msg);
                            }
                        }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });
                }
            };

            $scope.updatemessage = function () {
                $('#progress-full').show();
                if ($localStorage.savepushstatus == true) {
                    $scope.scheduleMessage ? $scope.pushmsg = 'N' : $scope.pushmsg = 'Y';
                    $localStorage.savepushstatus = false;
                } else {
                    $scope.pushmsg = 'Y';
                }
                $scope.sendemail ? $scope.pushemail = 'Y' : $scope.pushemail = 'N';

                if ($scope.msglink == "" || $scope.msglink == undefined) {
                    $scope.msglinkformat = "";

                } else {
                    if ($scope.msglink.indexOf("http://") == 0 || $scope.msglink.indexOf("https://") == 0) {
                        $scope.msglinkformat = $scope.msglink;
                    } else {
                        $scope.msglinkformat = 'http://' + $scope.msglink;
                    }
                }

                if ($scope.scheduleMessage) {
                    var date = $scope.msgdate;
                    var time = $scope.timeformat($scope.msgtime);

                    var pickdate = $scope.msgdate + " " + time;


                    var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                            navigator.userAgent && !navigator.userAgent.match('CriOS');
                    if (isSafari) {
                        $scope.delivery = pickdate;
                    } else {
                        $scope.delivery = pickdate;
                    }

                } else {
                    $scope.delivery = '';
                }

                //local storage maintain for sending message after premium purchase
                if ($localStorage.subscription_status === 'N' && $localStorage.upgrade_status === 'F') {
                    $localStorage.access_page = 'message';
                    $("#messageexpiryModal").modal('show');
                    $("#messageexpirytitle").text('Message');
                    $localStorage.message_id = $scope.msgid;
                    $localStorage.message_text = $scope.notification;
                    $localStorage.message_link = $scope.msglinkformat;
                    $localStorage.schedule_date_time = $scope.delivery;
                    $localStorage.push_status = $scope.pushmsg;
                    $localStorage.email_status = $scope.pushemail;
                    $('#progress-full').hide();
                    return false;
                } else if ($localStorage.access_page === 'message_premium_ok' && $localStorage.subscription_status === 'Y' && ($localStorage.upgrade_status === 'P' || $localStorage.upgrade_status === 'M' || $localStorage.upgrade_status === 'W')) {
                    $scope.msgid = $localStorage.message_id;
                    $scope.notification = $localStorage.message_text;
                    $scope.msglinkformat = $localStorage.message_link;
                    $scope.delivery = $localStorage.schedule_date_time;
                    $scope.pushmsg = $localStorage.push_status;
                    $scope.pushemail = $localStorage.email_status;
                }

                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatemessage',
                    data: {
                        "message_id": $scope.msgid,
                        "message_text": $scope.notification,
                        "message_link": $scope.msglinkformat,
                        "schedule_date_time": $scope.delivery,
                        "push_status": $scope.pushmsg,
                        "email_status": $scope.pushemail,
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $localStorage.access_page = $localStorage.message_id = $localStorage.message_text = $localStorage.message_link = '';
                        $localStorage.schedule_date_time = $localStorage.push_status = $localStorage.email_status = '';
                        $scope.resetmsg();
                        $scope.getmessages();
                        $("#communicationModal").modal('show');
                        $("#communicationmodaltitle").text('Success Message');
                        $("#communicationmodalcontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $localStorage.access_page = $localStorage.message_id = $localStorage.message_text = $localStorage.message_link = '';
                        $localStorage.schedule_date_time = $localStorage.push_status = $localStorage.email_status = '';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

            $scope.msgdone = function () {
                setTimeout(function () {
                    $route.reload();
                }, 200);
            };
            $scope.checkNoValue = function () {
                $scope.showAlert = true;
                if (!$scope.msglink) {
                    $scope.showAlert = false;
                } else {
                }
                $localStorage.PreviewMURLDText = $scope.msglink;

            };


            //Add or Update message using localstorage after premium purchase when free user
            if ($localStorage.access_page === 'message_premium_ok') {
                if ($localStorage.message_id === "" || $localStorage.message_id === undefined) {
                    $scope.postmessage();
                } else {
                    $scope.updatemessage();
                }
            }

            $scope.editmsg = function (msgid, msgtxt, msglink, deli_date, emailstatus, ivalue) {
                $scope.scheduleMessage = false;
                $scope.checkemail_status = emailstatus;
                if (ivalue == 0) {
                    $localStorage.previewEditMessageIndex = "0";
                } else {
                    $localStorage.previewEditMessageIndex = ivalue;
                }

                window.scrollTo(0, 0);
                $scope.resetmsgfunc();
                $scope.msgid = msgid;
                $scope.notification = msgtxt;
                $scope.msglink = msglink;
                $scope.title = "Edit Message";
                var myEl = angular.element(document.querySelector('#Message'));
                myEl.addClass('highlight');
                document.getElementById('notification').focus();
                $scope.saveedit = true;
//                $scope.buttonname = 'Resend';
                $scope.editmsgkey = 'Y';
                if ($scope.checkemail_status === 'Y') {
                    $scope.sendemail = true;
                } else {
                    $scope.sendemail = false;
                }
                
                if (deli_date != undefined && deli_date != '') {
                    if (deli_date.length > 0) {
                        $scope.scheduleMessage = true;
                        var date = new Date(deli_date);

                        var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                                navigator.userAgent && !navigator.userAgent.match('CriOS');
                        if (isSafari) {
                            $scope.msgdate = formatserverdateSafari(deli_date);
                            $scope.msgtime = formatAMPMSafaritime(deli_date);
                            $scope.check_Valid_date();
                        } else {
                            $scope.msgdate = formatserverdate(date);
                            $scope.msgtime = formatAMPM(date);
                            $scope.check_Valid_date();
                        }
                    }
                }
            };

            //Delete Message content
            $scope.deletemsg = function (msgid) {
                $scope.deletemsgid = msgid;
                $("#messageDeleteModal").modal('show');
                $("#messageDeletetitle").text('Delete');
                $("#messageDeletecontent").text("Are you sure want to delete message? ");

            };

            $scope.deleteCancel = function () {
                $scope.getmessages();
            };


            $scope.deleteConfirm = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteMessages',
                    data: {
                        "message_id": $scope.deletemsgid,
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#communicationModal").modal('show');
                        $("#communicationmodaltitle").text('Message');
                        $("#communicationmodalcontent").text('Message successfully deleted');
                        $scope.getmessages();
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });

            };

            $scope.resetmsg = function () {
                $localStorage.PreviewMessageDText = "";
                $localStorage.PreviewMURLDText = "";
                $localStorage.previewEditMessageIndex = "";
                $scope.msgid = angular.copy();
                $scope.notification = angular.copy();
                $scope.msglink = angular.copy();
                $scope.msgdate = angular.copy();
                $scope.msgtime = angular.copy();
                $scope.editmsgkey = 'N';
                $scope.scheduleMessage = false; // FOR CLOSING SHEDULE VIEW
                $scope.saveedit = false;
                $scope.sendemail = false;
                $scope.msgform.$setPristine();
                $scope.msgform.$setUntouched();
                $scope.title = "Send New Push Message";
//                $scope.buttonname = 'Send now';
                var myEl = angular.element(document.querySelector('#Message'));
                myEl.removeClass('highlight');  
            };
            $scope.resetmsgfunc = function () {
                $scope.msgid = angular.copy();
                $scope.notification = angular.copy();
                $scope.msglink = angular.copy();
                $scope.msgdate = angular.copy();
                $scope.msgtime = angular.copy();
                $scope.scheduleMessage = false;
                $scope.saveedit = false;
                $scope.sendemail = false;
                $scope.msgform.$setPristine();
                $scope.msgform.$setUntouched();
                $scope.title = "Send New Push Message";
//                $scope.buttonname = 'Send now';
                var myEl = angular.element(document.querySelector('#Message'));
                myEl.removeClass('highlight');
            }

            $scope.tolocaltime = function (ddate, pddate) {
                var fulldate;
                var time;
                if ((ddate == null || ddate == '') && (pddate == null || pddate == '')) {
                    return "Status: Not sent";
                } else if ((ddate == null || ddate == '') && (pddate != null || pddate != '')) {
                    return "Status: Sent";
                }
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari) {
                    time = formatAMPMSafaritime(ddate);
                    fulldate = ddate.replace(/GMT.*/g, "");
                    fulldate = fulldate.slice(0, -9);
                } else {
                    time = formatAMPM(ddate);
                    fulldate = ddate.replace(/GMT.*/g, "");
                    fulldate = fulldate.slice(0, -9);
                }
                return "Scheduled for delivery on <b> "+$scope.toDisplayFormat(fulldate) + "</b> at <b>"+ time +" " +$scope.company_tz + " </b>";

            };
            $scope.deliveredtimetolocaltime = function (ddate, pddate, received_users) {
                var fulldate;
                var time;
                
                if ((ddate == null || ddate == '') && (pddate == null || pddate == '')) {
                    return "Message Not sent";
                } else if ((ddate != null || ddate != '') && (pddate == null || pddate == '')) {
                    return "Message Scheduled";
                }
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari) {
                    time = formatAMPMSafaritime(pddate);
                    fulldate = pddate.replace(/GMT.*/g, "");
                    fulldate = fulldate.slice(0, -9);
                } else {
                    time = formatAMPM(pddate);
                    fulldate = pddate.replace(/GMT.*/g, "");
                    fulldate = fulldate.slice(0, -9);
                }
                
//                return "Message delivered on " + fulldate + " at " + time;
                 return "<b>"+received_users+"</b> total users received on <b>" + $scope.toDisplayFormat(fulldate) + "</b> at <b>"+ time +" " +$scope.company_tz + " </b>";

            };
            
            $scope.toDisplayFormat = function(serverformat){
                var splitdate = serverformat.split('-');
                var displayformat = $scope.month_list[parseInt(splitdate[1]-1)]+' '+splitdate[2]+', '+splitdate[0];
                return displayformat;
            };

            $scope.check_Valid_date = function () {
                if ($scope.msgdate != undefined) {
                    if ($scope.msgdate.length === 10) {
                        var scheduledate = $scope.msgdate;
                        var today = new Date();
                        var Tyear = today.getFullYear();
                        var Tmonth = today.getMonth() + 1;
                        var Tdate = today.getDate();
                        //Date Comparison
                        var fulldate = scheduledate.split('-');
                        if (((parseInt(fulldate[0]) >= parseInt(Tyear)) && (parseInt(fulldate[1]) >= parseInt(Tmonth)) && (parseInt(fulldate[2])) >= parseInt(Tdate)) ||
                                ((parseInt(fulldate[0]) >= parseInt(Tyear)) && (parseInt(fulldate[1]) >= parseInt(Tmonth))) || (parseInt(fulldate[0]) >= parseInt(Tyear))) {
                            $scope.check_Valid_date_status = false;
                            if ((parseInt(fulldate[0]) === parseInt(Tyear)) && (parseInt(fulldate[1]) === parseInt(Tmonth)) && (parseInt(fulldate[2])) === parseInt(Tdate)) {

                                var current_time = today.getHours() + ':' + today.getMinutes() + ":" + today.getSeconds();
                                var current_Date_Time = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2) + " " + current_time;
                                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                                var localtime = '';
                                if (isSafari) {
                                    localtime = formatAMPMSafaritime(current_Date_Time);
                                } else {
                                    localtime = formatAMPM(current_Date_Time);
                                }
                                var onlylocaltime = localtime.split(' ');
                                var Loc_hour_minute = onlylocaltime[0].split(':');
                                var LocHour = Loc_hour_minute[0];
                                var LocMinute = Loc_hour_minute[1];
                                var LocAmpm = onlylocaltime[1];

                                var LocFinaltime = LocHour + ':' + LocMinute + ' ' + LocAmpm;
                                var set_Hour = LocHour;
                                var set_Minutes;
                                if (parseInt(LocMinute) / 15 < 1) {
                                    set_Minutes = 15;
                                } else if (parseInt(LocMinute) / 15 < 2) {
                                    set_Minutes = 30;
                                } else if (parseInt(LocMinute) / 15 < 3) {
                                    set_Minutes = 45;
                                } else {
                                    set_Minutes = '00';
                                    set_Hour = parseInt(LocHour) + 1;
                                    if (parseInt(set_Hour) === 12) {
                                        if (LocAmpm === 'AM') {
                                            LocAmpm = 'PM';
                                        } else {
                                            set_Hour = 11;
                                            set_Minutes = 45;
                                            LocAmpm = 'PM';
                                        }
                                    }

                                }
                                var set_MIN_Time = set_Hour + ':' + set_Minutes + ' ' + LocAmpm;
                                $('#msgtime').timepicker('option', {'minTime': set_MIN_Time, 'maxTime': '11:45 PM'});
                            } else {
                                $('#msgtime').timepicker('option', {'minTime': '12:00 AM', 'maxTime': '11:45 PM'});
                            }
                            if ($scope.msgtime != '' || $scope.msgtime != null || $scope.msgtime != undefined) {
                                $scope.check_Valid_time();
                            }
                        } else {
                            $scope.check_Valid_date_status = true;
                        }
                    }
                } else {
                    $scope.check_Valid_date_status = true;
                }
            };
            $scope.check_Valid_time = function () {
                if (typeof ($scope.msgtime) !== 'undefined' && $scope.msgtime.length === 8) {
                    var scheduledate = $scope.msgdate;
                    var today = new Date();
                    var Tyear = today.getFullYear();
                    var Tmonth = today.getMonth() + 1;
                    var Tdate = today.getDate();
                    //Date Comparison
                    var fulldate = scheduledate.split('-');
                    if ((parseInt(fulldate[0]) === parseInt(Tyear)) && (parseInt(fulldate[1]) === parseInt(Tmonth)) && (parseInt(fulldate[2]) === parseInt(Tdate))) {

                        var current_time = today.getHours() + ':' + today.getMinutes() + ":" + today.getSeconds();
                        var current_Date_Time = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2) + " " + current_time;
                        var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                                navigator.userAgent && !navigator.userAgent.match('CriOS');
                        var localtime='';
                        if (isSafari) {
                            localtime = formatAMPMSafaritime(current_Date_Time);
                        } else {
                            localtime = formatAMPM(current_Date_Time);
                        }
                        var onlylocaltime = localtime.split(' ');
                        var Loc_hour_minute = onlylocaltime[0].split(':');
                        var LocHour = Loc_hour_minute[0];
                        var LocMinute = Loc_hour_minute[1];
                        var LocAmpm = onlylocaltime[1];

                        var LocFinaltime = LocHour + ':' + LocMinute + ' ' + LocAmpm;
                        //TIME CHECK
                        var msgtime = $scope.msgtime;
                        var msghour = msgtime.slice(0, 2);
                        var msgminute = msgtime.slice(3, 5);
                        var msgampm = msgtime.slice(6, 8);

                        if ((msgampm.startsWith("A") || msgampm.startsWith("P")) && (msgampm.endsWith("M"))) {
                            $scope.check_Valid_time_status = !$scope.CompareTime(LocFinaltime, msgtime);
                        } else {
                            $scope.check_Valid_time_status = true;
                        }
                    } else {
                        $scope.check_Valid_time_status = false;
                    }
                } else {
                    $scope.check_Valid_time_status = true;
                }
            };

            $scope.CompareTime = function (stime, etime) {
                var regex = /^([0-1][0-9])\:[0-5][0-9]\s*[ap]m$/i;
                var smatch = stime.match(regex);
                var ematch = etime.match(regex);
                if (smatch && ematch) {
                    var shour = parseInt(smatch[1]);
                    var ehour = parseInt(ematch[1]);
                    var smin = parseInt(stime.split(':')[1].substring(0, 2));
                    var sfrmt = stime.split(':')[1].substring(2, 5).trim();
                    var emin = parseInt(etime.split(':')[1].substring(0, 2));
                    var efrmt = etime.split(':')[1].substring(2, 5).trim();
                    if (sfrmt == efrmt) {
                        if (shour > ehour) {
                            if (shour == 12) {
                                return true;
                            }
                            return false;
                        } else if (shour === ehour) {
                            if (emin > smin) {
                                return true;
                            } else
                            {
                                return false;
                            }
                        } else {
                            if (sfrmt == 'AM' || sfrmt == 'am' || sfrmt == 'PM' || sfrmt == 'pm') {
                                if (parseInt(ehour) == 12) {
                                    return false;
                                }
                            }
                            return true;
                        }
                    } else if (sfrmt == 'AM' || sfrmt == 'am') {
                        return true;
                    } else {
                        return false;
                    }
                }
            };

            $scope.check_length = function (e) {
                var pasted_value = e.originalEvent.clipboardData.getData('text/plain');
                var pasted_text_length = pasted_value.length;
                if (pasted_text_length > 140) {
                    var x = document.getElementById("snackbarpaste")
                    x.className = "showpaste";
                    setTimeout(function () {
                        x.className = x.className.replace("showpaste", "");
                    }, 3000);
                }
            }

            function formatAMPM(date) {
                var date = new Date(date);
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                hours = hours % 12;
                if(hours===0){
                    hours=12;
                }
                hours = hours < 10 ? '0' + hours : hours; // the hour '0' should be '12'
                minutes = (minutes < 10 && minutes >= 0) ? '0' + minutes : minutes;
                var strTime = hours + ':' + minutes + ' ' + ampm;
                return strTime;
            }

            function formatAMPMSafari(ddate) {
                var dda = ddate.toString();
                dda = dda.replace(/ /g, "T");
                var date = new Date(dda).toUTCString();   // For date in safari format
                var time = date.slice(17, 25);
                // format AM PM start
                var hours = time.slice(0, 2);
                var minutes = time.slice(3, 5);
                var ampm = hours >= 12 ? 'PM' : 'AM';
                hours = hours % 12;
                hours = hours < 10 ? '0' + hours : hours; // the hour '0' should be '12'
                minutes = (minutes < 10 && minutes > 0) ? '0' + minutes : minutes;
                var localTime = hours + ':' + minutes + ' ' + ampm;
                return localTime;
            }
            function formatAMPMSafaritime(ddate) {
                var dda = ddate.toString();
                var ddarray = dda.split(' ');
                var time = ddarray[1];
                var ddtime=time.split(':');
                var hour=parseInt(ddtime[0]);
//                var hour = temp_hour < 10 ? '0' + temp_hour : temp_hour;
                var temp_minutes = ddtime[1];
//                var minutes = (temp_minutes < 10 && temp_minutes > 0) ? '0' + temp_minutes : temp_minutes;
                var minutes='';
                if(parseInt(temp_minutes) < 10){
                   minutes = '0' + temp_minutes; 
                }else{
                   minutes = temp_minutes; 
                }
                // format AM PM start
                var ampm = hour >= 12 ? 'PM' : 'AM';
                var hour_format = hour % 12;
                if(hour_format===0){
                    hour_format=12;
                }
                var local_hour = '';
                if(parseInt(hour_format) < 10){
                   local_hour = '0' + hour_format; 
                }else{
                   local_hour = hour_format; 
                }
//                var local_hour = parseInt(hour_format) < 10 ? '0' + hour_format : hour_format; // the hour '0' should be '12'
                var localTime = local_hour + ':' + minutes + ' ' + ampm;
                return localTime;
            }

            function formatserverdate(date) {
                var date = new Date(date);
                var day = date.getDate();
                if (day.toString().length < 2)
                    day = "0" + day;
                var month = date.getMonth() + 1;
                if (month.toString().length < 2)
                    month = "0" + month;
                var year = date.getFullYear();
                return year + "-" + month + "-" + day;
            }

            function formatserverdateSafari(date) {
                var dda = date.toString();
                dda = dda.replace(/-/g, "/");
                var curr_date = new Date(dda);   //  FOR DATE FORMAT IN SAFARI 
                var given_start_year = curr_date.getFullYear();
                var given_start_month = curr_date.getMonth();
                var given_start_day = curr_date.getDate();
                if (given_start_day.toString().length < 2)
                    given_start_day = "0" + given_start_day;
                var given_start_month = curr_date.getMonth() + 1;
                if (given_start_month.toString().length < 2)
                    given_start_month = "0" + given_start_month;
                return  given_start_year + "-" + given_start_month + "-" + given_start_day;
            }
            
//            ENABLE TO AVOID SPACE INSIDE MESSAGEDATE

//            document.getElementById('msgdate').onkeydown = function (event) {
//                if (event.keyCode === 13) {
//                    event.preventDefault();
//                }
//            };
    
            //find scroll end touched
            jQuery(
                    function ($)
                    {
                        $('#scrollbottomhit').bind('scroll', function ()
                        {
                            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)
                            {
                                $scope.bottom_hit_limit += 20;
                                $scope.getconversationlist();
                            }
                        })
                    }
            );
            jQuery(
                    function ($)
                    {
                        $('#conversationmsgs').bind('scroll', function ()
                        {
                            $scope.stop_double_call = false;
                            if ($(this).scrollTop() == 0)
                            {
                                $scope.top_hit_limit += 20;
                                    if(!$scope.stop_top_hit && !$scope.stop_double_call && $scope.chat_msg_arr.length != 0){
                                        if(!$scope.call_started){
                                            $scope.call_started = true;
                                            console.log("scroll hit");
                                            $scope.getIndividualChat();
                                        }
                                    }
                            }
                        })
                    }
            );
    
            $scope.openPicker = function(picker){
                if(picker){
                    if(picker == 'D'){
                        $("#msgdate").datepicker('show');
                    }else if(picker == 'T'){
                        $("#msgtime").timepicker('show');
                    }
                }
            };
            
            // OPEN EMAIL SETTINGS PAGE
            $scope.openEmailSettings = function(){
                $location.path('/emailsettings');
            };
            
        } else {
            $location.path('/login');
        }
        // ANGULARJS READY FUNCTION
        angular.element(document).ready(function(){
            // MAX-ROWS ATTRIBUTE FOR THE TEXT AR
            $('#compose_textarea_container').on('change keydown keyup paste cut', 'textarea', function () {
                $(this).height(0).height(this.scrollHeight - 10);
                if ($(this).height() >= 38) {
                    $('#compose_textarea').css("overflow", "auto");
                } else {
                    $('#compose_textarea').css("overflow", "hidden");
                }
            }).find('#compose_textarea').change();
        });
    }
    module.exports =  MessageController;
