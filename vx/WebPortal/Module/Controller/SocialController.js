SocialController.$inject = ['$scope','$location','$http','$localStorage','urlservice','$rootScope'];
function SocialController($scope,$location,$http,$localStorage,urlservice,$rootScope)
    {
       
       if($localStorage.islogin == 'Y'){
           
            //Previewscroll             
            $scope.topreview=function(){
                document.getElementById('previewapp').scrollIntoView();
            };
            
           angular.element(document.getElementById('sidetopbar')).scope().getName();
           angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
           angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
           angular.element(document.getElementById('sidetopbar')).scope().regComplete(); 
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
           $rootScope.activepagehighlight = 'appmenu';
           $localStorage.currentpage = "social";
           $localStorage.PreviewEventNamee="";
//           $scope.socurlfrmt =  /^\s*((ftp|http|https)\:\/\/)?([a-z\d\-]{1,63}\.)*[a-z\d\-]{1,255}\.[a-z]{2,6}\s*.*$/;
           $scope.message="MyStudio";
           $scope.socialData = [];
           
            //First time login message Pop-up ----START 
            tooltip.hide();
//            $scope.socialtip = true;
            
            if($localStorage.verfication_complete !== 'U'){
                if($localStorage.firstlogin){
                   if($localStorage.firstlogin_social === 'Y'){
                    $("#tooltipfadein").css("display", "block");
                    $localStorage.firstlogin_social = 'N';
                    $scope.socialtip = false; 
                    tooltip.pop("sociallink", "#socialtipid", {position:1});
                  }
                }
            }
            
            $("#socialdetailNext").click(function(){   
                $("#tooltipfadein").css("display", "none");
                $scope.socialtip = true;   
                tooltip.hide();
            });
            //First time login message Pop-up ----END
       
       $scope.getSocial = function(){
            $('#progress-full').show(); 
           $http({
                   method: 'GET',
                   url: urlservice.url+'getsocialdetails',
                   params: {
                    "company_id":$localStorage.company_id
                   },
                   headers:{"Content-Type":'application/json; charset=utf-8',},
                   withCredentials: true  
               
                }).then(function (response) {
                    if(response.data.status == 'Success'){
                        $('#progress-full').hide(); 
                        $scope.socialData = response.data.msg[0];
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        console.log(response.data);
                        $('#progress-full').hide();
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.getSocial();
            
            
            $scope.$watch('socialData.google_url', function(newVal,oldVal) {
                if(!$scope.socialData.google_url){
                    $localStorage.previewMenuGoogle = "false";
                }
                else{
                    $localStorage.previewMenuGoogle = "true";
                }
            });
            
            $scope.$watch('socialData.yelp_url', function(newVal,oldVal) {
                if(!$scope.socialData.yelp_url){
                    $localStorage.previewMenuYelp = "false";
                }
                else{
                    $localStorage.previewMenuYelp = "true";
                }
            });
            
            $scope.$watch('socialData.facebook_url', function(newVal,oldVal) {
                if(!$scope.socialData.facebook_url){
                    $localStorage.previewMenuFBID = "false";
                }
                else{
                    $localStorage.previewMenuFBID = "true";
                }
            });
            $scope.$watch('socialData.twitter_url', function(newVal,oldVal) {
                if(!$scope.socialData.twitter_url){
                    $localStorage.previewMenuTweet = "false";
                }
                else{
                    $localStorage.previewMenuTweet = "true";
                }
            });
            $scope.$watch('socialData.instagram_url', function(newVal,oldVal) {
                if(!$scope.socialData.instagram_url){
                    $localStorage.previewMenuInst = "false";
                }
                else{
                    $localStorage.previewMenuInst = "true";
                }
            });
            $scope.$watch('socialData.vimeo_url', function(newVal,oldVal) {
                if(!$scope.socialData.vimeo_url){
                    $localStorage.previewMenuVimeo = "false";
                }
                else{
                    $localStorage.previewMenuVimeo = "true";
                }
            });
            $scope.$watch('socialData.youtube_url', function(newVal,oldVal) {
                if(!$scope.socialData.youtube_url){
                    $localStorage.previewMenuYou = "false";
                }
                else{
                    $localStorage.previewMenuYou = "true";
                }
            });
            
            
            
            
            $scope.urlcheck = function(url){
                if(url == "" || url == undefined ){
                     return url;
                }else{
                    if(url.indexOf("http://") == 0 || url.indexOf("https://") == 0) {
                        return url;
                    }else{
                        return 'http://'+url;
                    }                   
                }
            };
            
           $scope.premiumpaynow = function(){
              $("#sidetopbar").removeClass('modal-open');
              $(".modal-backdrop").css("display", "none");
              $location.path('/billingstripe');                
            };
            
            $scope.postSocial = function(){
                $('#progress-full').show(); 
                    $scope.socialData.google_url = $scope.urlcheck($scope.socialData.google_url);
                    $scope.socialData.yelp_url = $scope.urlcheck($scope.socialData.yelp_url);
                    $scope.socialData.twitter_url = $scope.urlcheck($scope.socialData.twitter_url);
                    $scope.socialData.facebook_url = $scope.urlcheck($scope.socialData.facebook_url);
                    $scope.socialData.instagram_url = $scope.urlcheck($scope.socialData.instagram_url);
                    $scope.socialData.vimeo_url = $scope.urlcheck($scope.socialData.vimeo_url);
                    $scope.socialData.youtube_url = $scope.urlcheck($scope.socialData.youtube_url);
                
                 //local storage maintain for integrating social link after premium purchase
                if($localStorage.subscription_status === 'N' && $localStorage.upgrade_status === 'F'){
                    $localStorage.access_page = 'social';
                    $("#socialexpiryModal").modal('show');
                    $("#socialexpirytitle").text('Message');
                    $localStorage.google_url = $scope.socialData.google_url;
                    $localStorage.yelp_url = $scope.socialData.yelp_url;
                    $localStorage.twitter_id = $scope.socialData.twitter_id; 
                    $localStorage.twitter_url = $scope.socialData.twitter_url;
                    $localStorage.facebook_id = $scope.socialData.facebook_id;
                    $localStorage.facebook_url = $scope.socialData.facebook_url;
                    $localStorage.instagram_id = $scope.socialData.instagram_id; 
                    $localStorage.instagram_url = $scope.socialData.instagram_url;
                    $localStorage.vimeo_id = $scope.socialData.vimeo_id;
                    $localStorage.vimeo_url = $scope.socialData.vimeo_url;
                    $localStorage.youtube_id = $scope.socialData.youtube_id;
                    $localStorage.youtube_url = $scope.socialData.youtube_url;
                    $('#progress-full').hide();
                    return false;
                }else if( $localStorage.access_page === 'social_premium_ok' && $localStorage.subscription_status === 'Y' && ($localStorage.upgrade_status === 'P' || $localStorage.upgrade_status === 'M' || $localStorage.upgrade_status === 'W')){
                    $scope.socialData.google_url = $localStorage.google_url;
                    $scope.socialData.yelp_url = $localStorage.yelp_url;
                    $scope.socialData.twitter_id = $localStorage.twitter_id;
                    $scope.socialData.twitter_url = $localStorage.twitter_url;
                    $scope.socialData.facebook_id = $localStorage.facebook_id;
                    $scope.socialData.facebook_url = $localStorage.facebook_url;
                    $scope.socialData.instagram_id = $localStorage.instagram_id;
                    $scope.socialData.instagram_url= $localStorage.instagram_url;
                    $scope.socialData.vimeo_id = $localStorage.vimeo_id;
                    $scope.socialData.vimeo_url = $localStorage.vimeo_url;
                    $scope.socialData.youtube_id = $localStorage.youtube_id;
                    $scope.socialData.youtube_url = $localStorage.youtube_url;
                    
                }
                
                
                $http({
                method: 'POST',
                url: urlservice.url+'updatesocialdetails',
                data:{
                    'google_url':$scope.socialData.google_url,
                    'yelp_url':$scope.socialData.yelp_url,
                    'twitter_id':$scope.socialData.twitter_id, 
                    'twitter_url':$scope.socialData.twitter_url,
                    'facebook_id':$scope.socialData.facebook_id, 
                    'facebook_url':$scope.socialData.facebook_url,
                    'instagram_id':$scope.socialData.instagram_id, 
                    'instagram_url':$scope.socialData.instagram_url,
                    'vimeo_id':$scope.socialData.vimeo_id, 
                    'vimeo_url':$scope.socialData.vimeo_url,
                    'youtube_id':$scope.socialData.youtube_id,
                    'youtube_url':$scope.socialData.youtube_url,
                    'company_id':$localStorage.company_id
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                       $('#progress-full').hide();                      
                       $localStorage.access_page = $localStorage.google_url = $localStorage.yelp_url = $localStorage.twitter_url = '';
                       $localStorage.facebook_url = $localStorage.instagram_url = $localStorage.vimeo_url = $localStorage.youtube_url = '';
                       $localStorage.twitter_id = $localStorage.facebook_id = $localStorage.instagram_id = $localStorage.vimeo_id = $localStorage.youtube_id = '';
                        if($localStorage.firstlogin && $localStorage.firstlogin_social === 'Y'){
                              $localStorage.firstlogin_social = 'N';
                              angular.element(document.getElementById('sidetopbar')).scope().getTutorialMode();
                        }else if($localStorage.firstlogin_social === 'N' || $localStorage.firstlogin_social === undefined){
                               $("#socialinfomessageModal").modal('show');
                               $("#socialinfomessagetitle").text('Message');
                               $("#socialinfomessagecontent").text('Social Network details successfully updated');
                               $scope.socialData = response.data.msg[0];
                        }
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
             //Add or Update social link integration using localstorage after premium purchase when free user
             if( $localStorage.access_page === 'social_premium_ok'){ 
                     $scope.postSocial();
             }
            
            $scope.cancel = function(){
                $location.path('/home');
            };
            
            $scope.socialinfo = function(socialinfo){
                var msg;
                if(socialinfo == 'ye'){
                    msg = "1. Navigate to your public yelp page, click on the “Leave a Review” button. <br> 2. This will take you to the page where members can leave a review. <br> 3. Copy that URL and paste it here. ";
                }
                if(socialinfo == 'fb'){
                    msg = "<b>How to find Facebook ID:</b> <br> 1. Login your Facebook page. <br> 2. Click the About tab. <br> 3. Scroll down to the bottom of the Page Info section. <br> 4. You can find your page ID.";
                }
                if(socialinfo == 'tw'){
                    msg = "<b>How to find Twitter ID:</b> <br> 1. Login your Twitter page.. <br> 2. In the home page the ID is under your name preceded by @.";
                }
                if(socialinfo == 'ig'){
                    msg = "<b>How to find Instagram ID:</b> <br> 1. Username of your account.";
                }
                if(socialinfo == 'yt'){
                    msg = "<b>How to find Youtube ID:</b> <br> 1. Login your Youtube page. <br> 2. Go to settings, by clicking on the gear icon in the top right menu. <br> 3. In Overview, click the Advanced link under the name of the page at right section. <br> 4. You can find YouTube Channel ID.";
                }
                if(socialinfo == 'vo'){
                    msg = "<b>How to find Vimeo ID:</b> <br> 1. Login your Vimeo page. <br> 2. Go to edit profile. <br> 3. You can find User ID at top left.";
                }
                if(socialinfo == 'eb'){
                    msg = "<b>How to find Eventbrite User ID:</b><br> \n\
\n\ To Get user ID, We need to run an API [<a href='http://www.eventbrite.com/developer/v3/endpoints/users/#ebapi-get-users-id' target='_blank'> Learn more </a>], To run the API we need a Oauth token, To create OAuth token follow the following:\n\
<br> 1. Login your account. <br> 2. Go to Account settings, by clicking on the profile icon in the top right navigation bar. <br> 3. In left menu bar, click the App management link under the Developer section.\n\
 <br> 4. Create a New APP by providing required info. <br> 5. Now in App management page click on link <b>Show Client Secret and OAuth Token</b> <br>\n\
6. Copy the <b>Your personal OAuth token</b>. <br> 7. Run the following URL with <br> https://www.eventbriteapi.com/v3/users/me/?token=<b>Your personal OAuth token</b> <br> In the page response you can find your <b>User ID</b> <br>\n\
<br> <b>How to find Eventbrite URL:</b> <br>1. Login your account. <br> 2. Go to Organizer profile, by clicking on the account icon in the top right. <br>\n\
3. In Organizer profile, you can find <b>ORGANIZER PAGE URL</b>.";
            
                }
                
                $("#socialinfomessageModal").modal('show');
                $("#socialinfomessagetitle").text('Info');
                $("#socialinfomessagecontent").html(msg);
            };
            
            }else{
                $location.path('/login');
         }
           
  }

  module.exports = SocialController;