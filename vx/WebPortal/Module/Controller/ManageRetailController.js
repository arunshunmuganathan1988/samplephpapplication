ManageRetailController.$inject=['$scope', '$compile', '$location', '$http', '$localStorage', 'urlservice', 'retailListDetails', '$filter', '$route', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'DTColumnBuilder', 'DTDefaultOptions', '$window','$rootScope'];
function ManageRetailController ($scope, $compile, $location, $http, $localStorage, urlservice, retailListDetails, $filter, $route, DTOptionsBuilder, DTColumnDefBuilder, DTColumnBuilder, DTDefaultOptions, $window,$rootScope) {
if ($localStorage.islogin === 'Y') { 
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
//            angular.element(document.getElementById('sidetopbar')).scope().getWepayStatus();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            tooltip.hide();
            $rootScope.activepagehighlight = 'Retail';
            $scope.company_id = $localStorage.company_id;
            $scope.retailtaxoption = 'N';
            $scope.retaillistshow = true; 
            $scope.liveretailshow = true;
            $scope.draftretailshow = $scope.childretailshow = $scope.retailsettingsview = $scope.retailstatus = false;
            $scope.retailprocessingtype = '2';
            $localStorage.retaillocaldata = "";
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.draftchildretailList = false;
            $localStorage.currentpage = "retail";
            $localStorage.retailtemplatetype = $scope.retailmodaltitle  = $localStorage.retailTemplate = $scope.retailparentname = '';
            //open add product modal
            $scope.openaddproductmodal = function(){
                $("#addnewretail-modal").modal('show');
            };
            
            //add product category page open
            $scope.goaddproductpage = function (type) {
                $localStorage.reloadfrom = "";
                $localStorage.addretailtype = type;
                if(type === 'openproductmodal'){//open Product option modal
                    $scope.retailmodaltitle = 'Add new product option';
                    $("#addnewretail-modal").modal('hide');
                    $("#selectproductoption-modal").modal('show');
                }else if(type === 'opencategorymodal'){//open category option modal
                    $scope.retailmodaltitle = 'Add new product category';
                    $("#addnewretail-modal").modal('hide');
                    $("#selectproductoption-modal").modal('show');
                }else{
                    $localStorage.currentpage = "addretail";
                    $("#selectproductoption-modal").modal('hide');
                    if(type === "P") {
                        $('#selectproductoption-modal').on('hidden.bs.modal', function (e) {
                            $window.location.href = window.location.origin + window.location.pathname + '#/addretail';
                        })
                    }else{
                        $('#selectproductoption-modal').on('hidden.bs.modal', function (e) {
                            $window.location.href = window.location.origin + window.location.pathname + '#/addretail';
                        })
                    }
                }
                
            };

            $scope.selectaddproductpage = function (name) {
                if(name === 'Add new product option'){
                    $scope.goaddproductpage('P');
                }else{
                    $scope.goaddproductpage('C');
                }
            };

            $scope.selectretailtemplate = function (prodname) {
                $("#selectproductoption-modal").modal('hide');
                if(prodname === 'Add new product option'){
                    $localStorage.retailtemplatetype = 'S';
                }else{
                    $localStorage.retailtemplatetype = 'C';
                }
                $('#selectproductoption-modal').on('hidden.bs.modal', function (e) {
                    $window.location.href = window.location.origin + window.location.pathname + '#/retailtemplate';
                })
            };
            
            $scope.catchTaxPercentage = function () {
                $("#taxchange-modal").modal('show');
            };
             
             //go retail discount page
            $scope.goretaildiscount = function(){
                $location.path('/retaildiscount');
            };
            
            //go retail fulfillment
            $scope.openRetailPublicurl = function(){
                window.open($scope.retaillisturllink,"_blank");
            };
            $scope.goretailfulfillment = function(){
                $location.path('/retailfulfillment');
            };
            
            //get retail details of live, draft 
            $scope.getretaildetails = function(list_type,parent_id){
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getretailmanagedetails',
                    params: {
                        "company_id": $localStorage.company_id,
                        "list_type":list_type,
                        "parent_id":parent_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $localStorage.preview_retailcontent = response.data.msg.live;
                        if(list_type === 'P') {
                            if (!parent_id) {   
                                $scope.liveretailList = response.data.msg.live;
                            } else {
                                $scope.childretailList = response.data.msg.live;
                            }
                            $scope.draftretailList =  [];
                            $scope.draftchildretailList = false;
                        } else {
                            if (!parent_id) {
                                $scope.draftretailList = response.data.msg.draft;
                            } else {
                                $scope.childretailList = response.data.msg.draft;
                                $scope.draftchildretailList = true;
                            }
                            $scope.liveretailList =  [];
                        }
                        $scope.retailstatus = response.data.sales.retail_enabled;
                        $scope.retaillisturllink = response.data.retail_list_url;           
                        $window.scrollTo(0, angular.element('#retail-Tab').offsetTop); 
                    }else if(response.data.status === 'Expired'){
                        $('#progress-full').hide();
                        $("#retailmanagemodal").modal('show');
                        $("#retailmanagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{ 
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };  
            
            $scope.goretailoptions = function(){
                $scope.liveretailshow = true;
                $scope.retailsettingsview = false;
                $scope.resettabbarclass();
                $('#li-retailoptions').addClass('active-event-list').removeClass('event-list-title');
                $('#li-live').addClass('active-event-list').removeClass('event-list-title');
                $('#retailoption-tabtext').addClass('greentab-text-active');
                $('#settings-tabtext').removeClass('greentab-text-active');
                $localStorage.currentretailpreviewtab ='';
                $scope.getretaildetails('P','');
            };
            
            //Live Retail Listing
            $scope.livelistview = function(){
                $scope.liveretailshow = true;
                $scope.draftretailshow = false;
                $scope.childretailshow = false;
                $scope.retailsettingsview = false;
                $scope.resettabbarclass();
                $('#li-retailoptions').addClass('active-event-list').removeClass('event-list-title');
                $('#li-live').addClass('active-event-list').removeClass('event-list-title');
                $localStorage.currentretailpreviewtab = '';
                $scope.getretaildetails('P','');
            };
            
            //Draft Retail Listing
            $scope.draftlistview = function(){
                $scope.liveretailshow = false;
                $scope.draftretailshow = true;
                $scope.childretailshow = false;
                $scope.retailsettingsview = false;
                $scope.resettabbarclass();
                $('#li-retailoptions').addClass('active-event-list').removeClass('event-list-title');
                $('#li-draft').addClass('active-event-list').removeClass('event-list-title');
                $localStorage.currentretailpreviewtab = '';
                $scope.getretaildetails('S','');
            };
            
            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
            };
            
            //value copied on clipboard message
            $scope.toastretailmsg = function (retailtype) {   
                var id_name = "";
                if(retailtype === 'retaillist'){
                    id_name = "retaillisturl";
                }else if(retailtype === 'retailcategory'){
                    id_name = "retailcategoryurl";
                }else if(retailtype === 'childretail'){
                    id_name = "childretailurl";
                }
                var x = document.getElementById(id_name)
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            
            //Edit action for Retail
            $scope.editretail = function (detail,from) {
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getIndividualRetail',
                    params: {
                        "company_id": $localStorage.company_id,
                        "retail_product_id": detail.retail_product_id,
                        "retail_product_type": detail.retail_product_type
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        var edit_ret_value = response.data.msg;
                        $localStorage.retailpagetype = 'edit';
                        $localStorage.isAddedRetailDetails = 'N';
                        retailListDetails.addSingleRetail(edit_ret_value);
                        $localStorage.retailfromliveordraft = from;
                        $location.path('/addretail');
                    }else if(response.data.status === 'Expired'){
                        $('#progress-full').hide();
                        $("#retailmanagemodal").modal('show');
                        $("#retailmanagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{ 
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                }); 
            };
            
            $scope.manageretail = function(list_type,parent_id,name){
                $scope.liveretailshow = false;
                $scope.draftretailshow = false;
                $scope.childretailshow = true;
                $scope.retailparentname = name;
                if(list_type === 'P'){
                    $localStorage.retailfromliveordraft ='live';
                    $scope.retailfromliveordraft = $localStorage.retailfromliveordraft;
                }
                else if(list_type === 'S'){
                    $localStorage.retailfromliveordraft ='draft';
                    $scope.retailfromliveordraft = $localStorage.retailfromliveordraft;
                }
                $scope.getretaildetails(list_type,parent_id);
                $scope.retailsettingsview = false;
            };
            
//            Retail Setting 
            $scope.getRetailSettingsDetails = function(){
                $('#progress-full').show(); 
                $http({
                    method: 'GET',
                    url: urlservice.url+'getRetailSettings',
                params:{
                    'company_id':$localStorage.company_id,
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                            },
                            withCredentials: true       
                }).then(function (response) {
                   if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.retailprocessingtype = response.data.msg.retail_processingfee;
                        $scope.retailsalesagreement = response.data.msg.retail_agreement_text;
                        $localStorage.PreviewRetailSalesAgreement = $scope.retailsalesagreement;                        
                        $localStorage.currentretailpreviewtab = 'retailsettings';
                    }else if(response.data.status === 'Expired'){
                        $('#progress-full').hide();
                        $("#retailmanagemodal").modal('show');
                        $("#retailmanagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{ 
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            
            $scope.goretailsettings = function(){
                $scope.liveretailshow = false;
                $scope.draftretailshow = false;
                $scope.childretailshow = false;
                $scope.retailsettingsview = true;
                $scope.resettabbarclass();
                $('#li-settings').addClass('active-event-list').removeClass('event-list-title');
                $('#retailoption-tabtext').removeClass('greentab-text-active');
                $('#settings-tabtext').addClass('greentab-text-active');
                $scope.getRetailSettingsDetails();
            };
            
            $scope.cancelRetailAgreement = function(){
                $scope.goretailoptions();
            };
            
            $scope.updateRetailAgreement = function(value){
                if($scope.retailstatus === true){
                    $(".toggle-tooltip-new").css("visibility", "visible");
                    $scope.retailstatus = 'Y';
                }else{
                    $(".toggle-tooltip-new").css("visibility", "hidden");
                     $scope.retailstatus = 'N';
                }
                $('#progress-full').show(); 
                $http({
                method: 'POST',
                url: urlservice.url+'saveretailagreement',
                data:{
                    'company_id':$localStorage.company_id,
                    'retail_status':$scope.retailstatus,
                    'retail_proceesing_type':$scope.retailprocessingtype,
                    'retail_agreement_text':$scope.retailsalesagreement,
                    'update_type':value
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                   if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        if(value === 'sales'){
                            setTimeout(function () {    
                              $(".toggle-tooltip-new").css("visibility", "hidden");  
                            }, 10000);
                            $scope.retailstatus = response.data.msg.retail_enabled;
                        }else{
                            $("#retailmanagemodal").modal('show');
                            $("#retailmanagecontent").text('Retail Settings Successfully Updated'); 
                            $scope.retailprocessingtype = response.data.msg.retail_processingfee;
                            $scope.retailsalesagreement = response.data.msg.retail_agreement_text;
                        }
                        $localStorage.retailenabledstatus = response.data.msg.retail_enabled;
                    }else if(response.data.status === 'Expired'){
                        $(".toggle-tooltip-new").css("visibility", "hidden");
                        $('#progress-full').hide();
                        $("#retailmanagemodal").modal('show');
                        $("#retailmanagecontent").text(response.data.msg);
                        $scope.logout();  
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $(".toggle-tooltip-new").css("visibility", "hidden");
                        $('#progress-full').hide();
                        if(response.data.msgs && value === 'sales'){
                            $scope.retailstatus = 'N';
                            $localStorage.retailenabledstatus = $scope.retailstatus;
                            $scope.showWepayAccountCreation('manageretail');
                        }else{
                            $scope.handleFailure(response.data.msg);
                        }                        
                    }   
                }, function (response) {
                    $(".toggle-tooltip-new").css("visibility", "hidden");
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            
             //copy action for single/multiple event
            $scope.copyRetail = function(detail, level){
                $('#progress-full').show(); 
                $http({
                method: 'POST',
                url: urlservice.url+'copyRetail',
                data:{
                    'retail_product_id': detail.retail_product_id,
                    'list_type': detail.retail_product_status,
                    'level': level,
                    'company_id': $localStorage.company_id,
                    'from':'L'
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                   if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#retailmanagemodal").modal('show');
                        if (level === 'L1' && detail.retail_product_type === "C") { 
                            $("#retailmanagecontent").text('Category successfully copied inside "Draft" folder');
                        }else{
                            $("#retailmanagecontent").text('Product successfully copied');
                        }                        
                        
                        if(detail.retail_product_status === 'P') {
                            if (level === 'L1') {   
                                $scope.liveretailList = response.data.msg.live;
                                $scope.draftlistview();
                            } else {
                                $scope.childretailList = response.data.msg.live;
                            }
                            $scope.draftretailList =  [];
                        } else {
                            if (level === 'L1') {  
                                $scope.draftretailList = response.data.msg.draft;
                            } else {
                                $scope.childretailList = response.data.msg.draft;
                            }
                            $scope.liveretailList =  [];
                        }
                    }else if(response.data.status === 'Expired'){
                        $('#progress-full').hide();
                        $("#retailmanagemodal").modal('show');
                        $("#retailmanagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{ 
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            
            $scope.gobackformanageretail = function(){
                if($localStorage.retailfromliveordraft === 'live'){
                    $scope.livelistview();
                    $localStorage.retailfromliveordraft = '';
                }else if($localStorage.retailfromliveordraft === 'draft'){
                    $scope.draftlistview();
                    $localStorage.retailfromliveordraft = '';
                }
            };
            
             //Retail product drag and drop on live retail listing
            $scope.dragretailCallback = function (drag_type, ind, list_type) { // C- Category (or) Standalone List , P - Product List
                var dragRetailList = [];
                if(list_type === 'C'){
                    dragRetailList = angular.copy($scope.liveretailList);
                }else{
                    dragRetailList = angular.copy($scope.childretailList);
                }
                
                if (drag_type === 'start') {
                    $scope.old_location_id = ind;
                    $scope.start_retail_id = dragRetailList[ind].retail_product_id;
                } else if (drag_type === 'end') {
                    setTimeout(function () {
                        for(var i=0; i < dragRetailList.length; i++){ 
                            if(dragRetailList[i].retail_product_id === $scope.start_retail_id){
                                $scope.new_location_id = i;
                            }
                        }
                        
                        if ($scope.old_location_id !== $scope.new_location_id) {
                            var sort_retail_id, retail_parent_id, retail_product_status, retail_sort_order, previous_sort_order, next_sort_order;
                            if ($scope.new_location_id === 0) {
                                sort_retail_id = dragRetailList[$scope.new_location_id].retail_product_id;
                                retail_product_status = dragRetailList[$scope.new_location_id].retail_product_status;
                                retail_parent_id = dragRetailList[$scope.new_location_id].retail_parent_id;
                                previous_sort_order = parseFloat(dragRetailList[$scope.new_location_id + 1].retail_sort_order);
                                retail_sort_order = previous_sort_order + 1;
                                $scope.sortorderretailupdate(sort_retail_id, retail_sort_order, retail_product_status, retail_parent_id);
                            } else if ($scope.new_location_id === dragRetailList.length - 1) {
                                sort_retail_id = dragRetailList[$scope.new_location_id].retail_product_id;
                                retail_product_status = dragRetailList[$scope.new_location_id].retail_product_status;
                                retail_parent_id = dragRetailList[$scope.new_location_id].retail_parent_id;
                                retail_sort_order = parseFloat(dragRetailList[$scope.new_location_id - 1].retail_sort_order) / 2;
                                $scope.sortorderretailupdate(sort_retail_id, retail_sort_order, retail_product_status, retail_parent_id);
                            } else {
                                sort_retail_id = dragRetailList[$scope.new_location_id].retail_product_id;
                                retail_product_status = dragRetailList[$scope.new_location_id].retail_product_status;
                                retail_parent_id = dragRetailList[$scope.new_location_id].retail_parent_id;
                                previous_sort_order = parseFloat(dragRetailList[$scope.new_location_id + 1].retail_sort_order);
                                next_sort_order = parseFloat(dragRetailList[$scope.new_location_id - 1].retail_sort_order);
                                retail_sort_order = (previous_sort_order + next_sort_order) / 2;                               
                                $scope.sortorderretailupdate(sort_retail_id, retail_sort_order, retail_product_status, retail_parent_id);
                            }
                        }else{
                           $('#progress-full').hide();
                        }

                    }, 1000);
                }
            };
                        
             //updating retail sort order after drag and drop on live retail listing
            $scope.sortorderretailupdate = function(retail_id, retail_sort_order, retail_product_status, parent_id){
                $('#progress-full').show();
                $localStorage.retailSorted = 'N';     
                $http({
                    method: 'POST',
                    url: urlservice.url+'updateRetailSortingOrder',
                    data: {
                        "retail_product_sort_order": retail_sort_order,
                        "retail_product_id": retail_id,
                        "parent_id":parent_id,
                        "company_id": $localStorage.company_id,
                        "list_type":retail_product_status
                        
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if(response.data.status == 'Success'){
                        $('#progress-full').hide();
                        $localStorage.retailSorted = 'Y';     
                        if(retail_product_status === 'P') {
                            if (!parent_id) {   
                                $scope.liveretailList = response.data.msg.live;
                            } else {
                                $scope.childretailList = response.data.msg.live;
                            }
                            $scope.draftretailList =  [];
                            $scope.draftchildretailList = false;
                        } else {
                            if (!parent_id) {
                                $scope.draftretailList = response.data.msg.draft;
                            } else {
                                $scope.childretailList = response.data.msg.draft;
                                $scope.draftchildretailList = true;
                            }
                            $scope.liveretailList =  [];
                        }
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $localStorage.retailSorted = 'N';     
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.catchRetailSalesAgreement = function (){
                if($scope.retailsalesagreement === undefined || $scope.retailsalesagreement === ""){
                    $localStorage.PreviewRetailSalesAgreement = '';
                }else{
                    $localStorage.PreviewRetailSalesAgreement = $scope.retailsalesagreement;
                }
            };   
            
            if($localStorage.retailfromliveordraft === 'live'){ //from Live
                $scope.livelistview();
                $localStorage.retailfromliveordraft = '';
            }else if($localStorage.retailfromliveordraft === 'draft'){  //from template (or) from Draft
                $scope.draftlistview();
                $localStorage.retailfromliveordraft = '';
            }else if($localStorage.retailfromliveordraft === 'retailsettings'){ // from fulfillment setting
                $scope.goretailsettings();
                $localStorage.retailfromliveordraft = '';
            } else{
                $scope.getretaildetails('P','');
            }            
            
        } else {
            $location.path('/login');
        }

    }
    
  module.exports = ManageRetailController;

