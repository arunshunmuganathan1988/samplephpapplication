TrialTemplateController.$inject=['$scope', '$location', '$http','$route' ,'$localStorage', 'urlservice', '$filter','$rootScope'];
function TrialTemplateController($scope, $location, $http, $route, $localStorage, urlservice, $filter,$rootScope) {
        
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.activepagehighlight = 'trials';
            $localStorage.trialTemplate = '';
            $localStorage.currentpage = 'templatetrial';
            $('#progress-full').show();
            
            $scope.trialtemplate=function(){    
                  $http({
                    method: 'POST',
                    url: urlservice.url + 'getTrialprogramTemplate',
                    data: {
                     "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') { 
                       $('#progress-full').hide();
                       $localStorage.trialTemplate = $scope.trialtemplate = response.data.msg.live;
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.trialtemplate();
            
            $scope.trialtempcopy = function (trial_id) {
                $('#progress-full').show();
                $http({
                method: 'POST',
                        url: urlservice.url + 'copytrialprogram',
                        data: {
                            'trial_id': trial_id,
                            'trial_template_flag': 'Y',
                            'company_id': $localStorage.company_id
                        },
                        headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                        }).then(function (response) {
                   if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $localStorage.redirectedfrmtemplate = true;
                        $location.path('/managetrial');
                    }else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else{
                         $scope.handleFailure(response.data.msg);
                    }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                });
            };
            
            $scope.addTrial = function () {
               $localStorage.trialpagetype = '';
               $localStorage.addtriallocaldata = '';
               $localStorage.triallocaldata = '';
               $location.path("/addtrial");
            };
           
        } else {
            $location.path('/login');
        }
    }
    module.exports = TrialTemplateController;







