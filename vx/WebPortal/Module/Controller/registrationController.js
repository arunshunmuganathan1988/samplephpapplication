registrationController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$route', '$rootScope', '$compile'];
 function registrationController ($scope, $location, $http, $localStorage, urlservice, $route, $rootScope, $compile)
    {
        // NEW FUNCTIONS
        $rootScope.hidesidebarlogin = true;
        $rootScope.sideBarHide = true;
        $rootScope.adminsideBarHide = false;        
        
        $scope.user_email = $scope.user_password = $scope.country = $scope.other_country = $scope.user_firstname = $scope.user_lastname = $scope.company_name = $scope.web_page = $scope.phone_number = $scope.company_type = $scope.other_company = $scope.company_code = $scope.user_phone = $scope.country_code = $scope.user_phone_country = $scope.other_country_code = $scope.selected_country_code = "";
        $scope.activepagename = 'register';
        $scope.activepageid = '1';
        $scope.country = 'Choose your country';
        $scope.selected_country_flag = 'image/flags/Other.svg'; // DEFAULT FLAG OF Other
        $scope.other_country_code_image = $scope.selected_country_code_image = 'image/flags/plus.svg'; //DEFAULT COUNTRY CODE OF Other
        $scope.company_type = 'Choose industry';
//        $scope.verification_sent_to = 'mobile'; // DEFAULT TO MOBILE
        $scope.terms_of_service_link = 'https://mystudio.app/MYSTUDIO_Terms_of_Agreement.html';
        $scope.privacy_statement_link = 'https://mystudio.app/MyStudio_Academy_Privacy_Statement.html';
        $scope.countrylist = ["United States", "Canada", "United Kingdom", "Australia", "Other"];
        $scope.countrycodelist = ["+1", "+1", "+44", "+61", ""];
        $scope.industrylist = ["Martial Arts", "Dance", "Yoga", "Crossfit", "Gymnastics", "Summer Camps", "Nonprofit", "Church", "Consulting", "Other"];
        $scope.user_email_available = $scope.password_match = $scope.min_password_length = $scope.company_code_available = $scope.user_email_available_error = $scope.company_code_available_error = true;
        $scope.failure_response_text = "";
        $scope.wp_checkin_type = 'R';
        $scope.active_country_index = '';
        $scope.invalid_user_email = false;
        $scope.stripe_support = $scope.wepay_support = false;


        $scope.openLogin = function () {
            $location.path('/login');
        };

        // TOP BAR NAVIGATIONS

        $scope.togglePage = function (page, pageid) {
            if (page && pageid) {
                if (page == 'register' && pageid == 4) {
                    // SET DEFAULT COMPANY CODE
//                    $scope.company_code = $scope.phone_number;
//                    $scope.checkCompanyCode();
                }
                $scope.activepagename = page;
                $scope.activepageid = pageid;
                var progresswidth = 22.8571428571 * parseInt(pageid);   //  160/7 = 22.8571428571 [ 160 - width of green progress / 7 - no of register screens]
                $('.registerprogressbar').css('width', progresswidth);
            }
        };

        $scope.navTopBack = function () {
            var pageid = '';
            if ($scope.activepagename == 'register') {
                if ($scope.activepageid) {
                    if ($scope.activepageid == '1') {
                        $location.path('/login');
                    } else {
                        pageid = parseInt($scope.activepageid) - 1;
                        $scope.togglePage('register', pageid);
                    }
                }
            }
        };

        $scope.openNextScreen = function () {
            var pageid = '';
            if ($scope.activepagename === 'register') {
                if ($scope.activepageid) {
                    if ($scope.activepageid == '1') {
                        if ($scope.registerform1.$valid && $scope.user_email_available && $scope.min_password_length && $scope.password_match) {
                            pageid = parseInt($scope.activepageid) + 1;
                            $scope.country_code = ($scope.country === 'Other') ? $scope.other_country : (($scope.country === 'Canada') ? 'CA' : (($scope.country === 'United Kingdom') ? 'UK' : (($scope.country === 'United States') ? 'US' : (($scope.country === 'Australia') ? 'AU' : $scope.country))));
                            // CHECK COUNTRY PAYMENT SUPPORT STATUS
                             $scope.wepay_support = $scope.stripe_support = false;
                            if($scope.country !== 'Other' && $scope.supported_country_list) {
                                for (var i = 0; i < $scope.supported_country_list.length; i++) {
                                    if ($scope.supported_country_list[i].country === $scope.country_code) {
                                        if ($scope.supported_country_list[i].payment_support == 'S' && $scope.supported_country_list[i].support_status == 'Y') {
                                            $scope.stripe_support = true;
                                        } else if ($scope.supported_country_list[i].payment_support == 'W' && $scope.supported_country_list[i].support_status == 'Y') {
                                            $scope.wepay_support = true;
                                        }
                                    }
                                }
                            }
                            $scope.togglePage('register', pageid);
                        }
                    } else if ($scope.activepageid == '2') {
                        if ($scope.registerform2.$valid) {
                            pageid = parseInt($scope.activepageid) + 1;
                            $scope.togglePage('register', pageid);
                        }
                    } else if ($scope.activepageid == '3') {
                        if ($scope.registerform3.$valid) {
                            pageid = parseInt($scope.activepageid) + 1;
                            $scope.togglePage('register', pageid);
                        }
                    } else if ($scope.activepageid == '4') {
                        if ($scope.registerform4.$valid && $scope.company_code_available) {
                            pageid = parseInt($scope.activepageid) + 1;
                            $scope.user_phone_country = $scope.country;
                            $scope.catchSelectedCountryCode($scope.active_country_index);
                            $scope.togglePage('register', pageid);
                        }
                    } else if ($scope.activepageid == '5') {
                        if ($scope.registerform5.$valid) {
                            $scope.openConfirmPhoneModal();
                        }
                    } else if ($scope.activepageid == '6') {
                        if ($scope.registerform6.$valid) {
                            pageid = parseInt($scope.activepageid) + 1;
                            $scope.togglePage('register', pageid);
                        }
                    }
                }
            }
        };
        $scope.emailvalidation=function(user_email){
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var emailvalidity = re.test(user_email);
            if(emailvalidity){
                $scope.invalid_user_email=false;
                $scope.openNextScreen();
            }else{
                $scope.invalid_user_email=true;
            }
        };

        $scope.checkEmail = function () {
            $scope.invalid_user_email = false; // Resetting Email errors
            $scope.user_email_available_error = true;
            if ($scope.user_email) {
                $scope.catchInputChange('user_email',$scope.user_email);
                $http({
                    method: 'POST',
                    url: urlservice.url + 'verifyUserEmail',
                    data: {
                        "user_email": $scope.user_email
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.user_email_available = $scope.user_email_available_error = true;
                    } else {
                        $scope.user_email_available = $scope.user_email_available_error = false;
                    }
                }, function (response) {
                    $scope.failure_response_text = 'Invalid server response';
                    $('#response-modal').modal('show');
                });

            }
        };

        // TOGGLE PASSWORD/TEXT 
        $scope.togglePasswordText = function () {
            $scope.showpassword = !$scope.showpassword;
        };

        // VERIFY PASSWORD & CONFIRM PASSWORD
        $scope.verifyPasswordsMatch = function (id, password, confirmpassword) {
            $scope.password_match = true;
            $scope.min_password_length = true;
            $scope.catchInputChange(id, password);
            if (password != undefined) {
                if (password.length <= 5) {
                    $scope.min_password_length = false;
                }
            }
            if (password != undefined && confirmpassword != undefined) {
                if (password.length > 0 && confirmpassword.length > 0) {
                    if (password == confirmpassword) {
                    } else {
                        $scope.password_match = false;
                    }
                }
            }
        };

        $scope.catchSelectedCountry = function (selectedcountry) {
            $scope.active_country_index = selectedcountry;
            $scope.country = $scope.countrylist[selectedcountry];
            $scope.selected_country_flag = 'image/flags/' + $scope.country + '.svg';
        };

        $scope.catchSelectedIndustry = function (selectedindustry) {
            $scope.company_type = $scope.industrylist[selectedindustry];
        };
       
        $scope.catchSelectedCountryCode = function (selectedcountrycode) {
            $scope.user_phone_country = $scope.countrylist[selectedcountrycode];
            if ($scope.user_phone_country === 'Other') {
                $scope.selected_country_code_image = 'image/flags/plus.svg'; 
                $scope.selected_country_code = $scope.other_country_code;
            } else {
                $scope.selected_country_code = $scope.countrycodelist[selectedcountrycode];
                $scope.selected_country_code_image = 'image/flags/' + $scope.user_phone_country + '.svg';
                $('#user_phone').parents('.form-group-new').addClass('focused');
            }

            // CSS PADDING CHANGES 
            if ($scope.selected_country_code) {
                $scope.setMobileNumberPadding($scope.selected_country_code.length);
            } else {
                $('#user_phone').css("padding-left", "55px");       // DEFAULT PADDING OF THE INPUT
            }
        };

        $scope.catchOtherCountryCode = function () {
            if ($scope.other_country_code) {
                $scope.selected_country_code = '+' + $scope.other_country_code;
                $('#user_phone').parents('.form-group-new').addClass('focused');
                $scope.setMobileNumberPadding($scope.selected_country_code.length);
            } else {
                $scope.selected_country_code = '';
                $('#user_phone').css("padding-left", "55px");       // DEFAULT PADDING OF THE INPUT
            }
        };

        $scope.setMobileNumberPadding = function (inputvaluelength) {
            // CSS PADDING CHANGES FOR INPUT BASED ON THE COUNTRY CODE
            if (inputvaluelength === 1) {
                $('#user_phone').css("padding-left", "70px");
            } else if (inputvaluelength === 2) {
                $('#user_phone').css("padding-left", "80px");
            } else if (inputvaluelength === 3) {
                $('#user_phone').css("padding-left", "90px");
            } else if (inputvaluelength === 4) {
                $('#user_phone').css("padding-left", "100px");
            } else if (inputvaluelength === 5) {
                $('#user_phone').css("padding-left", "110px");
            } else if (inputvaluelength === 6) {
                $('#user_phone').css("padding-left", "120px");
            } else if (inputvaluelength === 7) {
                $('#user_phone').css("padding-left", "130px");   // MAXIMUM PADDING OF THE INPUT
            }
        };

        $scope.checkCompanyCode = function () {
            $scope.company_code_available = $scope.company_code_available_error = true;
            if ($scope.company_code) {
                $http({
                    method: 'POST',
                    url: urlservice.url + 'verifyCompanyCode',
                    data: {
                        "company_code": $scope.company_code
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.company_code_available = $scope.company_code_available_error = true;
                    } else {
                        $scope.company_code_available = $scope.company_code_available_error = false;
                    }
                }, function (response) {
                    $scope.failure_response_text = 'Invalid server response';
                    $('#response-modal').modal('show');
                });
            }
        };

        $scope.openConfirmPhoneModal = function () {
            $('#phoneconfirm-modal').modal('show');
        };


        $scope.userPhoneConfirmation = function () {
            if ($scope.user_phone) {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'registerWebAppUser',
                    data: {
                        "user_email": $scope.user_email,
                        "user_password": $scope.user_password,
                        "country": ($scope.country === 'Other') ? $scope.other_country : (($scope.country === 'Canada') ? 'CA' : (($scope.country === 'United Kingdom') ? 'UK' : (($scope.country === 'United States') ? 'US' : (($scope.country === 'Australia') ? 'AU' : $scope.country)))),
                        "user_firstname": $scope.user_firstname,
                        "user_lastname": $scope.user_lastname,
                        "company_name": $scope.company_name,
                        "web_page": $scope.web_page,
                        "phone_number": $scope.phone_number,
                        "company_type": ($scope.company_type === 'Other') ? $scope.other_company : $scope.company_type,
                        "company_code": $scope.company_code,
                        "user_phone": ($scope.user_phone_country === 'Other') ? ('+' + $scope.other_country_code + ' ' + $scope.user_phone) : $scope.user_phone,
                        "user_phone_country": ($scope.user_phone_country === 'United States') ? 'US' : (($scope.user_phone_country === 'United Kingdom') ? 'Uk' : (($scope.user_phone_country === 'Canada') ? 'CA' : (($scope.user_phone_country === 'Australia') ? 'AU' : 'OT')))
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    $('#progress-full').hide();
                    if (response.data.status === 'Success') {
                        //AFTER SUCCESS 
                        $localStorage.company_id = response.data.company_id;
                        $scope.stripe_country_support = $localStorage.stripe_country_support = response.data.stripe_country_support;
                        // SET FOR FIRST TIME TUTORIAL SHOW
                        $localStorage.firstlogin = true;
                        // FOR ENABLING TUTORIAL POPUP AND TOOL TIP FOR ALL PAGES
                        $localStorage.firstlogin_welcome = $localStorage.firstlogin_preview = $localStorage.firstlogin_profile = $localStorage.firstlogin_event = $localStorage.firstlogin_message = 'Y';
                        $localStorage.firstlogin_curriculum = $localStorage.firstlogin_referral = $localStorage.firstlogin_home = $localStorage.firstlogin_social = 'Y';
                        // NOT ACCEPTING WEPAY MOBILE PAYMENTS FOR OTHER COUNTRIES
                        if($scope.wepay_support){   // PROCEED TO MOBILE PAYMENTS -  WEPAY MODAL
                            $('#wepayconfirm-modal').modal('show');
                        }else{
                            $scope.togglePage('register', '7');
                        }
                    } else {
                        $scope.failure_response_text = response.data.msg;
                        $('#response-modal').modal('show');
                    }
                }, function (response) {
                    $('#progress-full').hide();
                    $scope.failure_response_text = 'Invalid server response';
                    $('#response-modal').modal('show');
                });
            }
        };

        $scope.openResendCodeModal = function () {
            $('#resendverification-modal').modal('show');
        };

        $scope.resendVerficationCodeto = function (to) {
            if (to === 'email') {
                $scope.verification_sent_to = 'xxxx@inc.com'; //  Email
            } else if (to === 'phone') {
                $scope.verification_sent_to = '+91 (xxxxxx)+ xxxx'; //  phone
            }
            $('#verificationsent-modal').modal('show');
        };

        $scope.openLink = function (to) {
            var URL = '';
            if (to === 'terms') {
                URL = 'https://mystudio.app/MYSTUDIO_Terms_of_Agreement.html';
            } else if (to === 'privacy') {
                URL = 'https://mystudio.app/MyStudio_Academy_Privacy_Statement.html';
            } else if (to === 'mystudio-website') {
                URL = 'https://www.mystudio.app';
            } else if (to === 'appstore') {
                URL = 'https://itunes.apple.com/us/app/mystudio-app/id1258207230?mt=8';
            } else if (to === 'playstore') {
                URL = 'https://play.google.com/store/apps/details?id=com.mystudio.app';
            }else if (to === 'stripe_terms') {
                URL = 'https://stripe.com/gb/connect-account/legal';
            }else if (to === 'wepay_terms') {
                URL = 'https://go.wepay.com/terms-of-service';
            }
            if (URL !== '') {
                window.open(URL, '_blank');
            }
        };

        $scope.activateAccount = function () {
            $scope.togglePage('register', '7');
        };

        $scope.activateWepayAccount = function () {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'wepayGenerateUserToken',
                data: {
                    "company_id": $localStorage.company_id,
                    "first_name": $scope.wp_firstname,
                    "last_name": $scope.wp_lastname,
                    "email": $scope.wp_email,
                    "type": $scope.wp_checkin_type                    // TYPE - R -> REGISTER, L -> LOGIN
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            }).then(function (response) {
                $('#progress-full').hide();
                if (response.data.status === 'Success') {
                    $('#wepayconfirm-modal').modal('show');
                } else {
                    $scope.failure_response_text = response.data.msg;
                    $('#response-modal').modal('show');
                }
            }, function (response) {
                $('#progress-full').hide();
                $scope.failure_response_text = 'Invalid server response';
                $('#response-modal').modal('show');
            });
        };
        
        // STRIPE ACCOUNT CREATION FOR ENABLING PAYMENT
        $scope.activateStripeAccount = function () {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'stripeGenerateUserToken',
                data: {
                    company_id: $localStorage.company_id,
                    studio_code: $scope.company_code,
                    email: $scope.user_email
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            }).then(function (response) {
                $('#progress-full').hide();
                if (response.data.status === 'Success') {
                    console.log('1');
                    $localStorage.preview_wepaystatus = $localStorage.preview_stripestatus = 'Y';
                    $('#stripe-modal-msg').text(response.data.msg);
                    $('#stripeconfirm-modal').modal('show');
                } else if (response.data.status === 'Expired') {
                    console.log('2');
                    console.log(response.data);
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $("#messageModal").modal('show');
                    $scope.logout();
                } else {
                    console.log('3');
                    $scope.failure_response_text = response.data.msg;
                    $('#response-modal').modal('show');
                }
            }, function (response) {
                $('#progress-full').hide();
                $scope.failure_response_text = 'Invalid server response';
                $('#response-modal').modal('show');
            });
        };
        $(".stripe-email-input").hover(
                function () {
                    $(".toggle-tooltip-stripe").css("visibility", "visible");
                }, function () {
            $(".toggle-tooltip-stripe").css("visibility", "hidden");
        });

        $scope.paymentConfirmation = function () {
            $scope.togglePage('register', '7');
        };

        $scope.catchInputChange = function (id, inputvalue) {
            if (inputvalue) {
                if (inputvalue.length > 0) {
                    $('#' + id).parents('.form-group-new').addClass('focused');
                }
            }
        };
        
        // OLD FUNCTIONS REUSED

        $scope.startCustomizingApp = function () {
            $('#progress-full').show();
            fbq('track', 'CompleteRegistration', {
                value: 180.00,
                currency: 'USD'
            });
            $localStorage.frthdwnldapp = true;  // STARTS FROM REGISTRATION APP STOPS
            // SET FOR FIRST TIME TUTORIAL SHOW
            $localStorage.firstlogin = true;
            // FOR ENABLING TUTORIAL POPUP AND TOOL TIP FOR ALL PAGES
            $localStorage.firstlogin_welcome = $localStorage.firstlogin_preview = $localStorage.firstlogin_profile = $localStorage.firstlogin_event = $localStorage.firstlogin_message = 'Y';
            $localStorage.firstlogin_curriculum = $localStorage.firstlogin_referral = $localStorage.firstlogin_home = $localStorage.firstlogin_social = 'Y';
            $scope.loginAfterRegistration();
        };

        $scope.loginAfterRegistration = function () {
            $('#progress-full').show();

            $http({
                method: 'POST',
                url: urlservice.url + 'login',
                data: {
                    "email": $scope.user_email,
                    "password": $scope.user_password
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
//                        $scope.newsignup = false;
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $localStorage.islogin = 'Y';
                    $localStorage.usrname = $scope.user_email;
                    $localStorage.pswdname = $scope.user_password;
                    $localStorage.company_id = response.data.msg.company_id;
                    $localStorage.student_id = response.data.msg.student_id;
                    $localStorage.user_id = response.data.msg.user_id;
                    $localStorage.user_email_id = response.data.msg.email_id;
                    $localStorage.studio_code = response.data.msg.company_code;
                    $localStorage.phone_number = response.data.msg.phone_number;
                    $localStorage.wepay_level = response.data.msg.wp_level;
                    $localStorage.wepay_client_id = response.data.msg.wp_client_id;
                    angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
                    $localStorage.loginDetails = response.data.msg;
                    $localStorage.subscription_status = response.data.msg.subscription_status;
                    $localStorage.upgrade_status = response.data.msg.upgrade_status;
                    $localStorage.regstr_complete = response.data.msg.registration_complete;
                    $localStorage.verfication_complete = response.data.msg.verification_complete;
                    $localStorage.processing_fee_percentage = response.data.msg.processing_fee_percentage;
                    $localStorage.processing_fee_transaction = response.data.msg.processing_fee_transaction;
                    $localStorage.wp_currency_code = response.data.msg.wp_currency_code;
                    $localStorage.wp_currency_symbol = response.data.msg.wp_currency_symbol;
                    $localStorage.plan_processing_fee = response.data.msg.plan_processing_fee.all_processing_fee;
                    
                    // STRIPE CRENDENTIALS
                    $localStorage.stripe_upgrade_status = response.data.msg.stripe_upgrade_status;
                    $localStorage.stripe_subscription = response.data.msg.stripe_subscription;
                    $localStorage.stripe_publish_key = response.data.msg.stripe_publish_key;
                    $localStorage.stripe_country_support = response.data.msg.stripe_country_support;
//                    console.log($localStorage.loginDetails);
                    $location.path('/members');
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
//                        $scope.newsignup = true;
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };
        
        angular.element(document).ready(function () {

            var url = window.location.href;
            if (url.indexOf("=") > 0) {
                var c = url.substr(url.indexOf("=") + 1);
                $scope.user_email = c;
            }

            // NEW ANIMATION FORM PLACEHOLDER TO LABELS
            $('.form-input').focus(function () {
                $(this).parents('.form-group-new').addClass('focused');
            });

            $('.form-input').blur(function () {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.form-group-new').removeClass('focused');
                } else {
                    $(this).addClass('filled');
                }
            });
            
            $('#user_phone').blur(function () {
                if ($scope.user_phone) {
                    $(this).parents('.form-group-new').addClass('focused');
                } else {
                    if ($scope.selected_country_code) {
                        $(this).parents('.form-group-new').addClass('focused');
                    } else {
                        $(this).parents('.form-group-new').removeClass('focused');
                    }
                }
            });
            
            if ($scope.user_email != '' && $scope.user_email != undefined) {
                $('#user_email').parents('.form-group-new').addClass('focused');
                $scope.checkEmail();
            }
            
            if ($scope.country != '' && $scope.country != undefined) {
                $('#country').parents('.form-group-new').addClass('focused');
            }

            if ($scope.company_type != '' && $scope.company_type != undefined) {
                $('#company_type').parents('.form-group-new').addClass('focused');
            }

            if ($scope.company_code != '' && $scope.company_code != undefined) {
                $('#company_code').parents('.form-group-new').addClass('focused');
            }
        });
    }

    module.exports = registrationController;
    