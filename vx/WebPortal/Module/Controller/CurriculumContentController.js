CurriculumContentController.$inject = ['$scope', '$location', '$http', '$localStorage', 'urlservice', 'curriculumLevelDetails', '$route', '$rootScope'];
function CurriculumContentController($scope, $location, $http, $localStorage, urlservice, curriculumLevelDetails, $route, $rootScope) {

    if ($localStorage.islogin == 'Y') {

        $scope.message = "MyStudio";
        angular.element(document.getElementById('sidetopbar')).scope().getName();
        angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
        angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
        angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
        $rootScope.activepagehighlight = 'curriculum';
        $localStorage.currentpage = "curriculum";
        $localStorage.PreviewEventNamee = "";
        $localStorage.cirind = "";
        $localStorage.previewCirName = "";
        $localStorage.ccirind = "";
        $localStorage.previewCirDesc = "";
        $localStorage.maintainInsertValue = "false";
        $localStorage.maintainnewSectionCreated = "false";
        $localStorage.Dvvid = "";
        $localStorage.DvvImageUrl = "";


        // GET CURRICULUM
        $scope.getCurriculum = function () {
            $scope.checkdt = curriculumLevelDetails.getSingleLevel();
            if ($scope.checkdt == undefined) {
                $scope.curriculum_name = $localStorage.offlineCurrName;
                $scope.curriculum_id = $localStorage.offlineCurrId;
                $scope.curriculum_detail_id = $localStorage.offlineCurrDetailId;
            } else {
                $scope.funccall = curriculumLevelDetails.getSingleLevel();
                $localStorage.offlineCurrData = curriculumLevelDetails.getSingleLevel();
                $scope.curriculum_name = $scope.funccall[0];
                $localStorage.offlineCurrName = $scope.funccall[0];
                $scope.curriculum_id = $scope.funccall[1];
                $localStorage.offlineCurrId = $scope.funccall[1];
                $scope.curriculum_detail_id = $scope.funccall[2];
                $localStorage.offlineCurrDetailId = $scope.funccall[1];
            }

            $http({
                method: 'GET',
                url: urlservice.url + 'getcurriculumcontentdetails',
                params: {
                    "company_id": $localStorage.company_id,
                    "curriculum_id": $scope.curriculum_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8', },
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $scope.curriculumDetail = response.data.msg.curriculum_content;
                    $scope.models = {
                        selected: null,
                        curriculumDetail: $scope.curriculumDetail
                    }
                    $scope.sortid = $scope.curriculumDetail.length + 1;
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $('#progress-full').hide();
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        $scope.getCurriculum();

        $scope.topreview = function () {
            document.getElementById('previewapp').scrollIntoView();
        };
        
        $scope.addSection = function(){
            $localStorage.maintainnewSectionCreated = "true";
            $scope.newcurriculum = true;
            $scope.msg = $scope.message;
            $scope.cid = $scope.curriculum_id;
            $scope.cdid = $scope.curriculum_detail_id;
            $scope.sid = $scope.sortid;
            $scope.patid = $scope.currurlfrmt;
            $scope.secname =  $scope.secdesc = $scope.addurl = "";
        };
        
        $scope.removeSection = function () {
            $scope.secname =  $scope.secdesc = $scope.addurl = "";
            $localStorage.PreviewCurriculumDName = "";
            $localStorage.DvvImageUrl = "";
            $localStorage.Dvvid = "";
            $scope.newcurriculum = false;
            $localStorage.maintainnewSectionCreated = "false";
            $scope.curriculumdynamic.$setPristine();
            $scope.curriculumdynamic.$setUntouched();
        };
        
        $scope.$watch(function () {return $localStorage.DvvImageUrl;}, function (newVal, oldVal) {
            if (newVal != "") {
                $scope.gowv = $localStorage.DvvImageUrl;
                $scope.gow = true;
            }
        });
        
        $scope.catchCurName = function () {
            $localStorage.PreviewCurriculumDName = $scope.secname;
        };
        $scope.catchCurDesc = function () {
            $localStorage.PreviewCurriculumDDesc = $scope.secdesc;
        };
        $scope.catchCurUrl = function () {
            $localStorage.PreviewCurriculumDUrl = $scope.addurl;
            if ($scope.addurl.indexOf("youtube") > -1) {
                $scope.videotype = "Y";
            } else {
                if ($scope.addurl.indexOf("vimeo") > -1) {
                    $scope.videotype = "V";
                } else {
                    $scope.videotype = "Y";
                }
            }
        };
        
        $scope.insertSection = function () {
                $('#progress-full').show();
                if ($scope.videotype == 'S') {
                    $scope.addurlformat = "";
                    $scope.vid = $localStorage.Dvvid;
                    $scope.petid = $localStorage.DvvImageUrl;
                    if (!$scope.vid) {
                        $scope.videotype = "N";
                        $scope.vid = "";
                    }
                } else {
                   $scope.vid = "";
                    if ($scope.addurl) {
                        if ($scope.addurl.indexOf("http://") == 0 ||$scope.addurl.indexOf("https://") == 0) {
                           $scope.addurlformat =$scope.addurl;
                        } else {
                           $scope.addurlformat = 'http://' +$scope.addurl;
                        }
                    } else {
                       $scope.addurlformat = "";
                       $scope.videotype = "N";
                    }

                }

                //Add Curriculum detail
                $http({
                    method: 'POST',
                    url: urlservice.url + 'addcurriculumcontent',
                    data: {
                        "curriculum_id":$scope.cid,
                        "content_title":$scope.secname,
                        "content_video_url":$scope.addurlformat,
                        "content_description":$scope.secdesc,
                        "content_video_type":$scope.videotype,
                        "content_sort_order":$scope.sid,
                        "curriculum_detail_id":$scope.cdid,
                        "video_id":$scope.vid,
                        "company_id": $localStorage.company_id,
                        "user_id": $localStorage.user_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'
                    },
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                       $('#progress-full').hide();
                       $scope.removeSection();

                        if ($localStorage.firstlogin && $localStorage.firstlogin_curriculum_section === 'Y') {
                            $localStorage.firstlogin_curriculum_section = 'N'; // new sub section at first time
                            angular.element(document.getElementById('sidetopbar')).scope().getTutorialMode();

                        } else if ($localStorage.firstlogin_curriculum_section === 'N' || $localStorage.firstlogin_curriculum_section === undefined) {
                            $("#messageModal").modal('show'); // second sub section onwards
                            $("#messagetitle").text('Success Message');
                            $("#messagecontent").text('Sub section successfully added');
                            $route.reload();
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };


        $scope.catchCirName = function (cind, cname) {
            $localStorage.cirind = cind;
            $localStorage.previewCirName = cname;
        };

        $scope.catchCirDesc = function (ccind, ccdesc) {
            $localStorage.ccirind = ccind;
            $localStorage.previewCirDesc = ccdesc;
        };


        $scope.removeDrag = function (event) {
            $(event.currentTarget).prop("draggable", false);
            $(event.currentTarget.closest('li')).prop("draggable", false);
        };

        $scope.addDrag = function (event) {
            $(event.currentTarget).prop("draggable", true);
            $(event.currentTarget.closest('li')).prop("draggable", true);
        };

        //UPDATE CURRICULUM
        $scope.updateSingleContent = function (indvalue) {
            $('#progress-full').show();

            if ($localStorage.maintainnewSectionCreated == "true") {
                $("#failuremessageModal").modal('show');
                $("#failuremessagetitle").text('Message');
                $("#failuremessagecontent").text('Please save the created section');
                $('#progress-full').hide();
                return false;
            }

            if ($scope.curriculumDetail[indvalue].video_type == 'S') {
                if ($localStorage.maintainInsertValue == "false") {
                    $scope.currvideo = "";
                    if (!$scope.curriculumDetail[indvalue].video_id) {
                        $scope.selectVideoID = "";
                        $scope.curriculumDetail[indvalue].video_type = 'N';
                    }

                } else {
                    $scope.currvideo = "";
                    $scope.selectVideoID = $scope.curriculumDetail[indvalue].video_id;
                }
            } else if ($scope.curriculumDetail[indvalue].video_type === 'Y' || $scope.curriculumDetail[indvalue].video_type === 'V' || $scope.curriculumDetail[indvalue].video_type === 'N') {
                $scope.selectVideoID = "";

                if ($scope.curriculumDetail[indvalue].ContentVideoUrl.indexOf("youtube") > -1)
                {
                    $scope.curriculumDetail[indvalue].video_type = 'Y';
                } else {
                    if ($scope.curriculumDetail[indvalue].ContentVideoUrl.indexOf("vimeo") > -1) {
                        $scope.curriculumDetail[indvalue].video_type = "V";
                    } else {
                        $scope.curriculumDetail[indvalue].video_type = "Y";
                    }
                }
                if ($scope.curriculumDetail[indvalue].ContentVideoUrl != "") {
                    if ($scope.curriculumDetail[indvalue].ContentVideoUrl.indexOf("http://") == 0 || $scope.curriculumDetail[indvalue].ContentVideoUrl.indexOf("https://") == 0) {
                        $scope.currvideo = $scope.curriculumDetail[indvalue].ContentVideoUrl;
                    } else {
                        $scope.currvideo = 'http://' + $scope.curriculumDetail[indvalue].ContentVideoUrl;
                    }
                } else {
                    $scope.currvideo = "";
                    $scope.curriculumDetail[indvalue].video_type = 'N';

                }
            } else {
                $scope.selectVideoID = "";
                $scope.currvideo = "";
                $scope.curriculumDetail[indvalue].video_type = 'N';
            }


            $http({
                method: 'POST',
                url: urlservice.url + 'updatecurriculumcontent',
                data: {
                    "video_id": $scope.selectVideoID,
                    "content_id": $scope.curriculumDetail[indvalue].content_id,
                    "curriculum_id": $scope.curriculumDetail[indvalue].curriculum_id,
                    "content_title": $scope.curriculumDetail[indvalue].content_title,
                    "content_video_url": $scope.currvideo,
                    "content_description": $scope.curriculumDetail[indvalue].content_description,
                    "content_video_type": $scope.curriculumDetail[indvalue].video_type,
                    "content_sort_order": $scope.curriculumDetail[indvalue].content_srt_ord,
                    "curriculum_detail_id": $scope.curriculumDetail[indvalue].curriculum_det_id,
                    "company_id": $localStorage.company_id,
                    "user_id": $localStorage.user_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8', },
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Sucess Message');
                    $("#messagecontent").text("Sucessfully updated");
                    $scope.getCurriculum();
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        //Delete Curriculum content
        $scope.deleteSingleContent = function (indvalue) {
            $scope.deleteindvalue = indvalue;
            $("#curriculumDeleteModal").modal('show');
            $("#curriculumDeletetitle").text('Delete');
            $("#curriculumDeletecontent").text("Are you sure want to delete section? ");

        };

        $scope.deleteCancel = function () {
            console.log('cancel delete');
            $scope.getCurriculum();
        };

        $scope.deleteConfirm = function () {

            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'deletecurriculumcontent',
                data: {
                    "content_id": $scope.curriculumDetail[$scope.deleteindvalue].content_id,
                    "curriculum_id": $scope.curriculumDetail[$scope.deleteindvalue].curriculum_id,
                    "company_id": $localStorage.company_id,
                    "user_id": $localStorage.user_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8', },
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Sucess Message');
                    $("#messagecontent").text("Sucessfully deleted");
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };


        $scope.dragoverCallback = function () {
            var sortarr = [];
            var cidarr = [];
            var curr_id_array = [];
            var sort;
            var cid, curr_id;
            $('#progress-full').show();
            setTimeout(function () {
                for (var i = 0; i < $scope.curriculumDetail.length; i++) {
                    curr_id = cid = sort = "";
                    cid = $scope.curriculumDetail[i].curr_content_id;
                    curr_id = $scope.curriculumDetail[i].curriculum_id;
                    sort = document.getElementById(cid).value;
                    sortarr[i] = parseInt(sort) + 1;
                    cidarr[i] = cid;
                    curr_id_array[i] = curr_id;
                }
                $scope.sortupdate(sortarr, cidarr, curr_id_array);
            }, 1000);

        };

        $scope.sortupdate = function (sortarr, cidarr, curr_id_array) {
            $http({
                method: 'POST',
                url: urlservice.url + 'updateCurriculumContentSortingOrder',
                data: {
                    "content_id": cidarr,
                    "sort_id": sortarr,
                    "curriculum_id": curr_id_array,
                    "company_id": $localStorage.company_id,
                    "user_id": $localStorage.user_id
                },
                headers: {"Content-Type": 'application/json; charset=utf-8', },
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    setTimeout(function () {
                        $route.reload();
                    }, 200);
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

    } else {
        $location.path('/login');
    }

}
module.exports = CurriculumContentController;
   