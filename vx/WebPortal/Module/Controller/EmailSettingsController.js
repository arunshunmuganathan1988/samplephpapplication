EmailSettingsController.$inject= ['$scope', '$location', '$http', '$localStorage', 'urlservice', '$route', '$filter', '$rootScope'];
function EmailSettingsController ($scope, $location, $http, $localStorage, urlservice, $route, $filter, $rootScope)
    {
        if ($localStorage.islogin == 'Y') {

            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            tooltip.hide();
            $rootScope.activepagehighlight = 'com_center';
            // INITIALIZATIONS PART
            $scope.emailallshow = true;
            $scope.customcampaignsshow = $scope.systememailsshow = $scope.send_email_switch = $scope.send_email_sub_switch = $scope.emaillistshow = $scope.campaignlistshow = $scope.editemailshow = $scope.changegetstartedtext = false;
            $scope.attachfile = $scope.all_email_subject = $scope.all_email_message = $scope.getstartedtext = $scope.getstartedtexturl = $scope.testemail = '';
            $scope.emailedit_message = '';
            $scope.cropper = {};
            $scope.cropper.sourceImage = null;
            $scope.cropper.croppedImage = null;
            $scope.cropper.previousImage = null;
            $scope.defaultbannerimageurl = $location.protocol() + '://' + $location.host() + '/uploads/Default/default_email_banner_image.png';
//            $scope.defaultbannerimageurl = $location.protocol()+'://'+$location.host()+'/james/ms_web/uploads/Default/default_email_banner_image.png';
            $scope.croppedEmailImage = $scope.defaultbannerimageurl;
            $scope.banner_image_type = false;
            $scope.banner_image_update_status = 'N';
            $scope.email_banner_old_image_url = "";
            $scope.current_edit_email_index = "";
            $scope.upgrade_status = $localStorage.upgrade_status;
            $scope.subscription_status = $localStorage.subscription_status;

            // FUNCTIONS STARTS HERE

            $scope.checkUserSubscriptionStatus = function (feature) {

                // MAINTAINING THE VALUES TO SET AFTER SUCCESSFUL PURCHASE

                if ($localStorage.upgrade_status === 'F' || $localStorage.upgrade_status === 'B') {
                    $localStorage.access_page = 'emailsettings';
                    $("#emailsettingsexpiryModal").modal('show');
                    if (feature === 'campaignstatusupdate') {
                        $scope.campaignsList_view = angular.copy($scope.campaignsList);
                        if ($scope.campaignsList.length > 0 && $scope.toggle_from === 'C') {
                            for (var i = 0; i < $scope.campaignsList.length; i++) {
                                if ($scope.campaignsList[i].campaign_id === $scope.current_campaign_category_id) {
                                    $scope.current_campaign_category_details = $scope.campaignsList[i];
                                    $scope.campaign_status = $scope.campaignsList[i].status;
                                }
                            }
                            if ($scope.campaign_status === 'E') {
                                $scope.send_email_sub_switch = true;
                            } else {
                                $scope.send_email_sub_switch = false;
                            }
                        } else {
                            $scope.send_email_sub_switch = false;
                        }
                    } else if (feature === 'sendtestemail') {
                        $('#sendtesttmail-modal').modal('hide');
                    }
                    $('#progress-full').hide();
                    return false;
                } else {
                    $localStorage.access_page = '';
                    return true;
                }
            };

            $scope.paymentnow = function () {
                $("#sidetopbar").removeClass('modal-open');
                $(".modal-backdrop").css("display", "none");
                $location.path('/billingstripe');
            };

            $scope.sendEmailPush = function () {

                $scope.msg = '';
                $scope.subject = '';
                if ($scope.all_email_message.trim().length === 0 || $scope.all_email_subject.trim().length === 0) {
                    return;
                }
                $scope.subject = $scope.all_email_subject;
                $scope.msg = $scope.all_email_message;

                if ($scope.attachfile !== '' && $scope.attachfile !== undefined && $scope.attachfile !== null) {
                    $scope.fileurl = $scope.filepreview.substr($scope.filepreview.indexOf(",") + 1);
                    $scope.file_name = $scope.attachfile.split("fakepath")[1].substr(1);
                } else {
                    $scope.fileurl = '';
                    $scope.file_name = '';
                }

                if ($scope.checkUserSubscriptionStatus('emailall')) {
                    $('#progress-full').show();
                    $http({
                        method: 'POST',
                        url: urlservice.url + 'sendStudentGroupPush',
                        data: {
                            "company_id": $localStorage.company_id,
                            "type": 'mail',
                            "subject": $scope.subject,
                            "message": $scope.msg,
                            "file_name": $scope.file_name,
                            "attached_file": $scope.fileurl,
                            "select_all_flag": 'Y',
                            "also_send_mail": 'N'
                        },
                        headers: {
                            "Content-Type": 'application/json; charset=utf-8',},
                            withCredentials: true

                    }).then(function (response) {

                        if (response.data.status === 'Success') {
                            $('#progress-full').hide();
                            $scope.clearSelectedList();
                            $("#campaignmessageModal").modal('show');
                            $("#campaignmessagetitle").text('Message');
                            $("#campaignmessagecontent").text(response.data.msg);
                        } else if (response.data.status === 'Expired') {
                            $('#progress-full').hide();
                            $("#campaignmessageModal").modal('show');
                            $("#campaignmessagetitle").text('Message');
                            $("#campaignmessagecontent").text(response.data.msg);
                            $scope.logout();
                        } else if (response.data.status === 'Failed') {
                            $('#progress-full').hide();
                            $("#campaignmessageModal").modal('show');
                            $("#campaignmessagetitle").text('Message');
                            $("#campaignmessagecontent").text(response.data.msg);
                        } else {
                            $scope.handleFailure(response.data.msg);
                        }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });
                }
            };

            $scope.clearSelectedList = function () {
                $scope.all_email_subject = $scope.all_email_message = '';
                $scope.removeAttachfile();
            };
            $scope.emailimgchanged = function () {
                $("#emailimgcropmodal").modal('show');
            };
            $scope.emailimgcropcancel = function () {
                $scope.cropper.sourceImage = $scope.croppedEmailImage = $scope.cropper.previousImage;
                $scope.banner_image_update_status = 'N';
            };

            $scope.emailimgcropped = function () {
                $scope.cropper.sourceImage = $scope.croppedEmailImage = $scope.cropper.croppedImage;
                $scope.cropper.previousImage = $scope.croppedEmailImage;
                $scope.banner_image_update_status = 'Y';
            };

            $scope.resetemailimgcropped = function () {
                $scope.cropper.sourceImage = $scope.croppedEmailImage = $scope.defaultbannerimageurl;
                $scope.cropper.croppedImage = null;
                $scope.banner_image_update_status = 'N';
            };


            $scope.emailAllView = function () { // EMAIL ALL TAB VIEW
                $scope.emailallshow = true;
                $scope.customcampaignsshow = false;
                $scope.systememailsshow = false;
                $scope.resettabbarclass();
                $('#li-emailall').addClass('active-event-list').removeClass('event-list-title');
            };

            $scope.customCampaignsView = function () { // CUSTOM CAMPAIGNS TAB VIEW
                $scope.getCampaignsList();
                $scope.emailallshow = false;
                $scope.customcampaignsshow = $scope.campaignlistshow = true;
                $scope.systememailsshow = false;
                $scope.emaillistshow = false;
                $scope.editemailshow = false;
                $scope.resettabbarclass();
                $('#li-customcampaigns').addClass('active-event-list').removeClass('event-list-title');
            };

            $scope.systemEmailsView = function () { // SYSTEM EMAILS TAB VIEW
                $scope.emailallshow = false;
                $scope.customcampaignsshow = false;
                $scope.systememailsshow = true;
                $scope.resettabbarclass();
                $('#li-systememails').addClass('active-event-list').removeClass('event-list-title');
            };

            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('#li-emailsettings').addClass('active-event-list').removeClass('event-list-title');
            };

            $scope.removeAttachfile = function () {
                $scope.maxEmailAttachmentSizeError = false;
                $scope.filepreview = "";
                $scope.attachfile = '';
                $scope.file_name = '';
                $scope.fileurl = '';
            };
            $scope.openEmailList = function (campaign) {
                $scope.current_campaign_category_details = campaign;
                $scope.current_campaign_category_id = campaign.campaign_id;
                $scope.campaign_category_id = campaign.campaign_id;
                $scope.campaign_title = campaign.campaign_title;
                $scope.campaign_status = campaign.status;
                if ($scope.campaign_status === 'E') {
                    $scope.send_email_sub_switch = true;
                } else {
                    $scope.send_email_sub_switch = false;
                }
                $scope.emailedit_subject = $scope.emailedit_message = $scope.getstartedtext = $scope.getstartedtexturl = '';
                $scope.resetemailimgcropped();
                $scope.getEmailList();
                $scope.campaignlistshow = $scope.editemailshow = $scope.changegetstartedtext = false;
                $scope.emaillistshow = true;
            };
            $scope.openEmailEdit = function (List, index) {
                $scope.setEmailDetails(List, index);
                $scope.emaillistshow = $scope.campaignlistshow = false;
                $scope.editemailshow = true;
                $('#getstarted_btn').css("margin-left", "40px");
            };

            $scope.openTestEmailModal = function () {
                $scope.testemail = $scope.emailList[0].studio_email;
                $('#sendtesttmail-modal').modal('show');
            };
            $scope.clearTestEmail = function () {
                $scope.testemail = '';
            };

            $scope.confirmEmailDelete = function () {
                $('#deleteemail-modal').modal('show');
            };

            $scope.openMessage = function () {
                $location.path('/message');
            };

            $scope.getCampaignsList = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getCampaignDetails',
                    data: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.campaignsList = response.data.category_list;
                        $scope.campaignsList_view = angular.copy($scope.campaignsList);
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#campaignmessageModal").modal('show');
                        $("#campaignmessagetitle").text('Message');
                        $("#campaignmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

            $scope.getEmailList = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getEmailListForCampaign',
                    data: {
                        "company_id": $localStorage.company_id,
                        "campaign_id": $scope.campaign_category_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.emailList = response.data.email_list;
                        $scope.emailList_view = angular.copy($scope.emailList);
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#campaignmessageModal").modal('show');
                        $("#campaignmessagetitle").text('Message');
                        $("#campaignmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Failed') {
                        $('#progress-full').hide();
                        $scope.emailList = $scope.emailList_view = '';
                        $("#campaignmessageModal").modal('show');
                        $("#campaignmessagetitle").text('Message');
                        $("#campaignmessagecontent").text(response.data.msg);
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

            $scope.toggleEmailSwitch = function (campaign_toggle, toggle_from, emailstatus) {
                $scope.campaign_category_id = campaign_toggle.campaign_id;
                $scope.current_campaign_category_id = $scope.campaign_category_id;
                $scope.toggle_from = toggle_from;
                $scope.emailstatus = emailstatus;

                if ($scope.checkUserSubscriptionStatus('campaignstatusupdate')) {
                    $('#progress-full').show();
                    $http({
                        method: 'POST',
                        url: urlservice.url + 'updateCampaignStatus',
                        data: {
                            "company_id": $localStorage.company_id,
                            "campaign_id": $scope.campaign_category_id,
                            "status": ($scope.emailstatus) ? 'E' : 'D'
                        },
                        headers: {"Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                    }).then(function (response) {
                        if (response.data.status === 'Success') {
                            $scope.campaignsList = response.data.category_list;
                            $scope.campaignsList_view = angular.copy($scope.campaignsList);
                            if ($scope.campaignsList.length > 0 && $scope.toggle_from === 'C') {
                                for (var i = 0; i < $scope.campaignsList.length; i++) {
                                    if ($scope.campaignsList[i].campaign_id === $scope.current_campaign_category_id) {
                                        $scope.current_campaign_category_details = $scope.campaignsList[i];
                                        $scope.campaign_status = $scope.campaignsList[i].status;
                                    }
                                }
                                if ($scope.campaign_status === 'E') {
                                    $scope.send_email_sub_switch = true;
                                } else {
                                    $scope.send_email_sub_switch = false;
                                }
                            } else {
                                $scope.send_email_sub_switch = false;
                            }
                            $('#progress-full').hide();
                        } else if (response.data.status === 'Expired') {
                            $('#progress-full').hide();
                            $("#campaignmessageModal").modal('show');
                            $("#campaignmessagetitle").text('Message');
                            $("#campaignmessagecontent").text(response.data.msg);
                            $scope.logout();
                        } else {
                            $scope.handleFailure(response.data.msg);
                        }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });
                } 
            };

            //Custom Campaign Drag and Drop
            $scope.dragcampaigncallback = function (drag_type, ind) {
                if (drag_type === 'start') {
                    $scope.old_location_id = ind;
                    $scope.start_campaign_id = $scope.campaignsList[ind].campaign_id;
                } else if (drag_type === 'end') {
                    setTimeout(function () {
                        for (var i = 0; i < $scope.campaignsList.length; i++) {
                            if ($scope.campaignsList[i].campaign_id === $scope.start_campaign_id) {
                                $scope.new_location_id = i;
                            }
                        }

                        if ($scope.old_location_id !== $scope.new_location_id) {
                            var sort_campaign_id, campaigns_sort_order, previous_sort_order, next_sort_order;
                            if ($scope.new_location_id === 0) {
                                sort_campaign_id = $scope.campaignsList[$scope.new_location_id].campaign_id;
                                previous_sort_order = parseFloat($scope.campaignsList[$scope.new_location_id + 1].cmpgn_sort_order);
                                campaigns_sort_order = previous_sort_order + 1;
                                $scope.sortordercampaignupdate(sort_campaign_id, campaigns_sort_order);
                            } else if ($scope.new_location_id === $scope.campaignsList.length - 1) {
                                sort_campaign_id = $scope.campaignsList[$scope.new_location_id].campaign_id;
                                campaigns_sort_order = parseFloat($scope.campaignsList[$scope.new_location_id - 1].cmpgn_sort_order) / 2;
                                $scope.sortordercampaignupdate(sort_campaign_id, campaigns_sort_order);
                            } else {
                                sort_campaign_id = $scope.campaignsList[$scope.new_location_id].campaign_id;
                                previous_sort_order = parseFloat($scope.campaignsList[$scope.new_location_id + 1].cmpgn_sort_order);
                                next_sort_order = parseFloat($scope.campaignsList[$scope.new_location_id - 1].cmpgn_sort_order);
                                campaigns_sort_order = (previous_sort_order + next_sort_order) / 2;
                                $scope.sortordercampaignupdate(sort_campaign_id, campaigns_sort_order);
                            }
                        } else {
                            $('#progress-full').hide();
                        }

                    }, 1000);
                }
            };

            //updating Campaign sort order after drag and drop 
            $scope.sortordercampaignupdate = function (campaign_id, sort_id) {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateCampaignSortingOrder',
                    data: {
                        "sort_id": sort_id,
                        "campaign_id": campaign_id,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.campaignsList = response.data.category_list;
                        $scope.campaignsList_view = angular.copy($scope.campaignsList);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#campaignmessageModal").modal('show');
                        $("#campaignmessagetitle").text('Message');
                        $("#campaignmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

            $scope.setEmailDetails = function (emailList, ind) {
                $scope.campaign_email_id = emailList.campaign_id;
                $scope.emailtitle = emailList.email_title;
                $scope.emailedit_subject = emailList.subject;
                $scope.emailedit_message = (emailList.message == '' || emailList.message == null || emailList.message == undefined) ? '<div>One of the most important emails to send your leads is the email to welcome them abroad. <br></div><div><br></div><div><span style="font-weight: bold;">Identify your business, products and service points.</span>Consider including key identification of your business as well as the offers that you want to promote.</div><div><br></div><div><span style="font-weight: bold;">Include a hook or call to action.</span><br></div>' : emailList.message;
                $scope.email_banner_image = emailList.email_banner_image;
                $scope.croppedEmailImage = $scope.cropper.sourceImage = emailList.email_banner_image_url;
                $scope.email_banner_old_image_url = emailList.email_banner_image_url;
                $scope.getstartedtext = (emailList.button_text == '' || emailList.button_text == null || emailList.button_text == undefined) ? 'Get started' : emailList.button_text;
                $scope.getstartedtexturl = emailList.button_url;
                $scope.current_edit_email_index = ind;
            };

            $scope.saveEmailCampaign = function (emailstatus) {
                $scope.emailstatus = emailstatus;
                if ($scope.croppedEmailImage === "" || $scope.croppedEmailImage === undefined || $scope.croppedEmailImage === null) {
                    $scope.email_banner_image_type = "";
                    $scope.email_banner_image = "";
                } else {
                    $scope.email_banner_image_type = "png";
                    $scope.email_banner_image = $scope.croppedEmailImage.substr($scope.croppedEmailImage.indexOf(",") + 1);
                }

                if ($scope.checkUserSubscriptionStatus('saveemailcampaign')) {
                    $('#progress-full').show();
                    $http({
                        method: 'POST',
                        url: urlservice.url + 'updateEmailCampaign',
                        data: {
                            "company_id": $localStorage.company_id,
                            "campaign_id": $scope.campaign_email_id,
                            "parent_id": $scope.campaign_category_id,
                            "status": $scope.emailstatus,
                            "type": 'E',
                            "subject": $scope.emailedit_subject,
                            "message": $scope.emailedit_message,
                            "email_banner_image": $scope.email_banner_image,
                            "email_banner_image_type": $scope.email_banner_image_type,
                            "show_banner_image": ($scope.email_banner_image == $scope.defaultbannerimageurl) ? 'N' : 'Y',
                            "banner_image_update_status": $scope.banner_image_update_status,
                            "email_banner_old_image_url": $scope.banner_image_update_status === 'N' ? $scope.email_banner_old_image_url : "",
                            "button_text": $scope.getstartedtext ? $scope.getstartedtext:'Get started',
                            "button_url": $scope.getstartedtexturl
                        },
                        headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                    }).then(function (response) {
                        if (response.data.status === 'Success') {
                            $('#progress-full').hide();
                            $scope.emailList = response.data.email_list;
                            $scope.emailList_view = angular.copy($scope.emailList);
                            $scope.setEmailDetails($scope.emailList[$scope.current_edit_email_index]);
                            $scope.campaignsList = response.data.category_list;
                            $scope.campaignsList_view = angular.copy($scope.campaignsList);
                            $scope.banner_image_update_status = 'N';
                            if ($scope.campaignsList.length > 0) {
                                for (var i = 0; i < $scope.campaignsList.length; i++) {
                                    if ($scope.campaignsList[i].campaign_id === $scope.current_campaign_category_id) {
                                        $scope.current_campaign_category_details = $scope.campaignsList[i];
                                        $scope.campaign_status = $scope.campaignsList[i].status;
                                    }
                                }
                                if ($scope.campaign_status === 'E') {
                                    $scope.send_email_sub_switch = true;
                                } else {
                                    $scope.send_email_sub_switch = false;
                                }
                            } else {
                                $scope.send_email_sub_switch = false;
                            }
                            $scope.changegetstartedtext = false; // CLOSE GET STARTED BUTTON EDIT VIEW
                            $('#getstarted_btn').css("margin-left", "40px");
                            $("#campaignmessageModal").modal('show');
                            $("#campaignmessagetitle").text('Message');
                            $("#campaignmessagecontent").text(response.data.msg);
                        } else if (response.data.status === 'Expired') {
                            $('#progress-full').hide();
                            $("#campaignmessageModal").modal('show');
                            $("#campaignmessagetitle").text('Message');
                            $("#campaignmessagecontent").text(response.data.msg);
                            $scope.logout();
                        } else {
                            $scope.handleFailure(response.data.msg);
                        }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });
                }
            };

            $scope.sendTestEmail = function () {
                if ($scope.croppedEmailImage === "" || $scope.croppedEmailImage === undefined || $scope.croppedEmailImage === null) {
                    $scope.email_banner_image_type = "";
                    $scope.email_banner_image = "";
                } else {
                    $scope.email_banner_image_type = "png";
                    $scope.email_banner_image = $scope.croppedEmailImage.substr($scope.croppedEmailImage.indexOf(",") + 1);
                }

                if ($scope.checkUserSubscriptionStatus('sendtestemail')) {
                    $('#progress-full').show();
                    $http({
                        method: 'POST',
                        url: urlservice.url + 'sendTestEmailCampaign',
                        data: {
                            "company_id": $localStorage.company_id,
                            "campaign_id": $scope.campaign_email_id,
                            "email_to": $scope.testemail,
                            "subject": $scope.emailedit_subject,
                            "message": $scope.emailedit_message,
                            "email_banner_image": $scope.email_banner_image,
                            "email_banner_image_type": $scope.email_banner_image_type,
                            "show_banner_image": ($scope.email_banner_image == $scope.defaultbannerimageurl) ? 'N' : 'Y',
                            "banner_image_update_status": $scope.banner_image_update_status,
                            "email_banner_old_image_url": $scope.email_banner_old_image_url,
                            "button_text": $scope.getstartedtext ? $scope.getstartedtext:'Get started',
                            "button_url": $scope.getstartedtexturl
                        },
                        headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                    }).then(function (response) {
                        if (response.data.status === 'Success') {
                            $('#progress-full').hide();
                            $scope.testemail = '';
                            $('#sendtesttmail-modal').modal('hide');
                            $("#campaignmessageModal").modal('show');
                            $("#campaignmessagetitle").text('Message');
                            $("#campaignmessagecontent").text(response.data.msg);
                        } else if (response.data.status === 'Expired') {
                            $('#progress-full').hide();
                            $("#campaignmessageModal").modal('show');
                            $("#campaignmessagetitle").text('Message');
                            $("#campaignmessagecontent").text(response.data.msg);
                            $scope.logout();
                        } else {
                            $scope.handleFailure(response.data.msg);
                        }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });
                }
            };


        } else {
            $location.path('/login');
        }

        // OPEN FILE BROWSER (OR) CROPPER DECIDED HERE BY CHANGING THE TYPE OF THE INPUT
        $scope.checkImageType = function () {
            if (!$scope.croppedEmailImage || $scope.croppedEmailImage == $scope.defaultbannerimageurl) {
                $scope.banner_image_type = true;
            } else {
                $scope.banner_image_type = false;
                $scope.emailimgchanged();
            }

        };

        $scope.openButtonEdit = function () {
            $scope.changegetstartedtext = true;
            $('#getstarted_btn').css("margin-left", "18px");
        };

        //DOCUMENT READY FUNCTION
        angular.element(document).ready(function () {
            if ($scope.croppedEmailImage && $scope.defaultbannerimageurl) {
                if (!$scope.croppedEmailImage || $scope.croppedEmailImage == $scope.defaultbannerimageurl) {
                    $scope.banner_image_type = true;
                } else {
                    $scope.banner_image_type = false;
                }
            }
        });

    }

    module.exports = EmailSettingsController;

// EMAIL ATTACH FILE UPLOAD
