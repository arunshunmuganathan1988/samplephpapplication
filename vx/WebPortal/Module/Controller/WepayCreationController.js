WepayCreationController.$inject=['$scope', '$location', '$http','$route' ,'$localStorage', 'urlservice', '$filter'];
 function WepayCreationController($scope, $location, $http, $route, $localStorage, urlservice, $filter) {
        
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $scope.checkin_type = 'R';
            
            
            $scope.wepayAccountRegClose = function(){
                if($localStorage.linkfrompage === 'managemembership'){
                    $location.path('/managemembership');
                }else if($localStorage.linkfrompage === 'manageevent'){
                    $location.path('/manageevent');
                }else if($localStorage.linkfrompage === 'managetrial'){
                    $location.path('/managetrial');
                }else if($localStorage.linkfrompage === 'manageretail'){
                    $location.path('/manageretail');
                }else if($localStorage.linkfrompage === 'addmembership'){
                    $location.path('/addmembership');
                }else if($localStorage.linkfrompage === 'addevent'){
                    $location.path('/addevent');
                }else if($localStorage.linkfrompage === 'addtrial'){
                    $location.path('/addtrial');
                }else if($localStorage.linkfrompage === 'home'){
                    $location.path('/home');
                }else if($localStorage.linkfrompage === 'customers'){
                    $location.path('/customers');
                }
            };
            
            //get wepay status of this company
            $scope.getWepayStatus = function(){
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getwepaystatus',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $localStorage.preview_wepaystatus = $scope.wepaystatus = response.data.msg;
                        $localStorage.preview_currencies = $scope.currencies = response.data.currency;
                        if($scope.wepaystatus === 'Y'){
                           $localStorage.loginDetails.wp_user_state = 'Done';
                        }else if($scope.wepaystatus === 'A'){
                           $localStorage.loginDetails.wp_user_state = 'pending';  
                        }else{
                           $localStorage.loginDetails.wp_user_state = ''; 
                        }
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{ 
                        $('#progress-full').hide();                        
                        $("#failuremessageModal").modal('show');
                        $("#failuremessagetitle").text('Message');
                        $("#failuremessagecontent").text(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            
            
            //wepay account creation for enabling payment
            $scope.wepayAccountRegistration = function(){
                $('#progress-full').show();
                        $http({
                            method: 'POST',
                            url: urlservice.url + 'wepayGenerateUserToken',
                            data: {
                                company_id: $localStorage.company_id,
                                first_name: $scope.wpfirstname,
                                last_name: $scope.wplastname,
                                email: $scope.wpemail,
                                type: $scope.checkin_type
                            },                            
                            headers:{
                                "Content-Type":'application/json; charset=utf-8',
                            },
                            withCredentials: true
                        }).then(function (response) {
                           if (response.data.status === 'Success') {
                                $scope.getWepayStatus();                              
                                $('#progress-full').hide();
                                $("#wepaymessageModal").modal('show');
                                $("#wepaymessageTitle").text('Message');
                                $("#wepaymessageContent").text(response.data.msg);                                
                                $scope.wpfirstname = $scope.wplastname = $scope.wpemail = '';
                                $scope.checkin_type = 'R';
                                $scope.wepayAccountRegClose();
                            }else if(response.data.status === 'Expired'){
                                console.log(response.data);
                                $('#progress-full').hide();
                                $("#messageModal").modal('show');
                                $("#messagetitle").text('Message');
                                $("#messagecontent").text(response.data.msg);
                                $scope.logout();  
                            }else if (response.data.status === 'Version') {
                                $('#progress-full').hide();
                                $scope.handleFailure(response.data.msg);
                            }else{
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                if(response.data.error){
                                    $("#failuremessagetitle").text(response.data.error);
                                    $("#failuremessagecontent").text(response.data.error_description);
                                }else{
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(response.data.msg);
                                }
                            }   
                        }, function (response) {
                            console.log(response.data);
                            $scope.handleError(response.data);
                        });
            };
            
            
        } else {
            $location.path('/login');
        }
    }
    module.exports = WepayCreationController;