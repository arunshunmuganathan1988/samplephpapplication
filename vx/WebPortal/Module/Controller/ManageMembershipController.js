ManageMembershipController.$inject=['$scope', '$compile','$location', '$http', '$localStorage', 'urlservice', 'membershipListDetails', 'membershipTransferDetails', '$filter', '$route','DTOptionsBuilder', 'DTColumnDefBuilder','DTColumnBuilder','DTDefaultOptions','$window','$rootScope'];
function ManageMembershipController($scope,$compile,$location, $http, $localStorage, urlservice, membershipListDetails, membershipTransferDetails, $filter, $route, DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder,DTDefaultOptions,$window,$rootScope) {
        if ($localStorage.islogin === 'Y') {
           
        $('#progress-full').show();
        angular.element(document.getElementById('sidetopbar')).scope().getName();
        angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
        angular.element(document.getElementById('sidetopbar')).scope().regComplete();
        angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
        angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
//        angular.element(document.getElementById('sidetopbar')).scope().getWepayStatus();
        angular.element(document.getElementById('sidetopbar')).scope().getStripeStatus();
        angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
        angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
        tooltip.hide();
        $rootScope.activepagehighlight = 'membership';
            
            $scope.showedit = false;
            $scope.editIndex = "";
            $scope.editSingle = $scope.parfirstname = $scope.parlastname = "";
            $scope.membership_reg_id="";
            
            $localStorage.eventpagetype = 'new';
            $localStorage.currentpreviewtab = 'membership'; 
            
            $scope.nolivemembershiplisted =  $scope.nodraftmembershiplisted = true;
            $scope.livemembershipshow = true;
            $scope.customers = true;   // default false
            $scope.payments = false;
            $scope.appusers = false;
            $scope.members_view = true; // default false
            $scope.trials_view = false;
            $scope.leads_view = false;
            $scope.addcustomlist_view = false;
            $scope.activemembers = true;
            $scope.onholdmembers= false;
            $scope.mainmembershipview = true;
            $scope.childmembershipview = false;
            $localStorage.membershiplocaldata = '';
            $localStorage.currentpage = "membership";
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.wepaystatus = $localStorage.preview_wepaystatus;
            $scope.membershipdiscount = '';
            $scope.signupdiscount = '';
            $scope.prorate_payment_startdate = '';
            $scope.membershipcategorystatus = '';
            
            $scope.current_membership_manage = '';
            $scope.current_membership_manage_options = '';
            $scope.processing_fee_show = '';
            $scope.current_membershipid_manage = '';
            $scope.current_membership_reg_columns = '';
            $scope.noeventslisted = false;
            $scope.childmembershiparray = '';
            $scope.childmembershiparray_copy = '';
            $scope.studentlist = [];
            $scope.waiverarea = '';  
            $scope.pay = [];
            
            //payment related show and hide
            $scope.showmempaymentview = true;
            $scope.zerocostview = true;
            $scope.showcardselection = false;
            $scope.compareDiscountCost = true;
            $scope.mem_selectedcardnumber = '';
            $scope.student_name = '';
            $scope.student_id = 0;
            $scope.student_name2 = '';
            $scope.participant_studentname = '';
            $scope.participant_studentname2 = '';
            $scope.student_id2 = 0;
            $scope.credit_card_id = '';
            $scope.credit_card_status = '';
            $scope.credit_card_name = '';
            $scope.credit_card_expiration_month = '';
            $scope.credit_card_expiration_year = '';
            $scope.postal_code = '';
            $scope.membership_parent_id = '';
            $scope.remainingDueValue = 0;
            $scope.remDueFeeText = '';
            $scope.showRemDueProccessingFee = false;
            $scope.manual_credit_show = false;
            $scope.manual_credit_refund = 0;
            $scope.current_manual_credit_details = [];
            $scope.new_manual_credit_details = [];
            $scope.membership_discount_show = $scope.signup_discount_show = true;
            $scope.exceedsActualcost = $scope.exceedsSignupActualcost = false;
            $scope.delayed_recurring_payment_startdate = '';
            $scope.delay_membership_amount_view = 0;
            $scope.carddetails = [];
            $scope.rank_details = [];
            
            //For multiple event purchase
            $scope.discount_show = true;
            $scope.cancelmembers = false;

            $scope.studentselected = '';
            $scope.message_type = '';
            $scope.buttonText = '';
            $scope.eventtime = '';
            $scope.data = {
                text: ""
            };
            $scope.pushdata='';
            $scope.ccyearlist = ["2018","2019","2020","2021","2022","2023","2024","2025","2026","2027","2028","2029","2030","2031","2032","2033","2034","2035","2036","2037"];
            $scope.ccmonthlist = ["1","2","3","4","5","6","7","8","9","10","11","12"];
            $scope.onetimecheckout = 0;
            $scope.onetimeinitialload = 0;
            $scope.discount = '';
            $scope.edit_mem_transfer = false;
            $scope.rank_updated = false;
            $scope.selectedrank = $scope.rank_id = $scope.rank_name = $scope.membership_transfer_reg_id = '';
            
            $scope.membership_array_running_show = [];
            $scope.membership_array_onhold_show = [];
            $scope.membership_array_cancelled_show = [];
            $scope.membershiplistcontents = [];
            $scope.also_send_email = false;
            $localStorage.buyerName = "";
            $localStorage.participantName = "";
            $scope.maxEmailAttachmentSizeError = false;
            $scope.filepreview = ""; 
            $scope.attachfile = '';
            $scope.file_name = '';
            $scope.fileurl = '';
            $scope.change_made = $scope.search_value = '';
            $scope.data_loaded_length = 0;
            $scope.membershiplistcontents = [];
            $scope.selectedData = [];
            $scope.selected = {};
            $scope.selectAll = false;
            $scope.all_select_flag = 'N';
            $scope.color_green = 'N';
            $scope.upgrade_status = $localStorage.upgrade_status; // for inactive studio-registration block            
            $scope.mem_payment_method = 'CC';
            $scope.payment_method_id = '';
            $scope.stripe_customer_id = ''; 
            $scope.stripe_intent_method_type = "PI";
            $scope.stripe = "";
                        
            if($localStorage.membershipcategorydeleteredirect === 'Y'){
                $("#CopyCategorymessageModal").modal('show');
                $("#CopyCategorymessagetitle").text('Success Message');
                $("#CopyCategorymessagecontent").text('Membership category successfully deleted');   
                $localStorage.membershipcategorydeleteredirect = 'N'; 
            }
                        
            if($localStorage.wepay_level === 'P'){
               $scope.wepay_env = "production";
               $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
            }else if($localStorage.wepay_level === 'S'){
               $scope.wepay_env = "stage";
               $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
            }
            
            WePay.set_endpoint($scope.wepay_env); // stage or production      

            // Shortcuts
            var d = document;
                d.id = d.getElementById,
                valueById = function(id) {
                    return d.id(id).value;
                };

            // For those not using DOM libraries
            var addEvent = function(e,v,f) {
                if (!!window.attachEvent) { e.attachEvent('on' + v, f); }
                else { e.addEventListener(v, f, false); }
            };

            // Attach the event to the DOM
            addEvent(d.id('cc-submit3'), 'click', function() {
                $('#progress-full').show();
                var userName = [valueById('firstName')]+' '+[valueById('lastName')];
                var user_first_name = valueById('firstName');
                var user_last_name = valueById('lastName');
                var email = valueById('email').trim();
                var phone = valueById('phone');
                var postal_code = valueById('postal_code');
                var country = valueById('country');
                    response = WePay.credit_card.create({
                    "client_id":        $localStorage.wepay_client_id,
                    "user_name":        userName,
                    "email":            valueById('email').trim(),
                    "cc_number":        valueById('cc-number'),
                    "cvv":              valueById('cc-cvv'),
                    "expiration_month": valueById('cc-month'),
                    "expiration_year":  valueById('cc-year'),
//                    "cc_number":        $scope.cardnumber,
//                    "cvv":              $scope.cvv,
//                    "expiration_month": $scope.ccmonth,
//                    "expiration_year":  $scope.ccyear,
                    "virtual_terminal": "web",
                    "address": {
                        "country": country,
                        "postal_code": postal_code
                    }
                }, function(data) {
                    if (data.error) {
                        $('#progress-full').hide();
                        console.log(data);
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Wepay Message');
                        $("#pushmessagecontent").text(data.error_description);
                    } else {
                        if ($scope.onetimecheckout == 0) {
                            $scope.onetimecheckout = 1;
                            $scope.membershipcheckout(data.credit_card_id, data.state, user_first_name, user_last_name, email, phone, postal_code, country);
                            console.log(data);
                        }
                    }
                });
            });
            
             // STRIPE TOKEN CALL
        $scope.getStripeElements = function () {
            // Create a Stripe client
            $scope.valid_stripe_card = false;

            // Create an instance of Elements
            var elements = $scope.stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: '#32325d',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element
            $scope.stripe_card = elements.create('card', {style: style});

            // Add an instance of the card Element into the `card-element` <div>
            $scope.stripe_card.mount('#stripe-card-element');

            // Handle real-time validation errors from the card Element.
            $scope.stripe_card.addEventListener('change', function (event) {
                var displayError = document.getElementById('stripe-card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                    $scope.valid_stripe_card = false;
                } else {
                    displayError.textContent = '';
                    $scope.valid_stripe_card = true;
                }
            });            
        };
        
        $scope.submitStripeForm = function(){
            $scope.stripe.createPaymentMethod('card', $scope.stripe_card).then(function (result) {
                if (result.error) {
                    // Inform the user if there was an error. 
                    var errorElement = document.getElementById('stripe-card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send paymentMethod.id to server 
                    (result.paymentMethod.id)? $scope.payment_method_id = result.paymentMethod.id : $scope.payment_method_id = "";
                    $scope.preloader = true;
                    $scope.stripeMembershipcheckout($scope.firstname, $scope.lastname, $scope.email, $scope.phone, $scope.postal_code, $scope.participant_Country);
                }
            });
        };
            
            
             $scope.getMemCustomer_list=function(mem_id, mem_opt_id, tab){
                 $scope.current_tab=tab;
//                var vm = this;
                $scope.membership_ot_id = mem_opt_id;
                DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                    var titleHtml = '<input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)">';
                var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                $scope.selected = {};
                $scope.selectAll = false;
               
                if ($scope.current_tab === 'R') {
                   $scope.dtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'getMembersByMembershipId',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.membershiplistcontents = json.data;
                                    $scope.count_total_hold = json.count_total_hold;
                                    $scope.count_total_active = json.count_total_active;
                                    $scope.count_total_cancel = json.count_total_cancel;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "membership_id": mem_id,
                                 "membership_opt_id": mem_opt_id,
                                 "membership_status": tab,
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();  
                                } else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [10, 'desc'])
                    $scope.dtColumns = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendMemindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="membershipIndividualDetail(\'' + full.id + '\' ,' + "'active'" + ',' + full.membership_option_id + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                        DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col3'),
                        DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col4').renderWith(
                                function (data, type, full, meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendMemindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a  class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');" ><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';
                                }       
                                }),
                        DTColumnBuilder.newColumn('participant_birthdate').withTitle('Birthday').withClass('col5'),
                        DTColumnBuilder.newColumn('participant_age').withTitle('Age').withClass('col6'),
                        DTColumnBuilder.newColumn('rank_status').withTitle('Rank').withClass('col7').renderWith(
                                function (data, type, full) {
                                    
                                    if (data == "") {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.membership_id + ',' + full.id + ',' + '$event' + ');">' + "None" + '</a>';
                                    } else {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.membership_id + ',' + full.id + ',' + '$event' + ');">' + full.rank_status + '</a>';
                                    }
                                }),
                        DTColumnBuilder.newColumn('last_advanced').withTitle('Last Advanced').withClass('col8').renderWith(
                                function (data, type, full) {
                                    if (data == 0)
                                        return 'Today';
                                    else
                                        return (data) + ' days ago';
                                }),
                        DTColumnBuilder.newColumn('created_dt').withTitle('Registration Date').withClass('col9'),
                        DTColumnBuilder.newColumn('membership_reg_type_user').withTitle('Registration Method').withClass('col10'),
                        DTColumnBuilder.newColumn('t').withTitle('Membership').withClass('col11').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.t + '</span>';
                                }),
                        DTColumnBuilder.newColumn('past_due').withTitle('Bill Past Due').withClass('col14').notSortable().renderWith(function(data,type,full){
                            return  $scope.wp_currency_symbol +''+data;
                        }),
                        DTColumnBuilder.newColumn('t').withTitle('End Date').withClass('col16').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                    ];
//                   $scope.dtInstance = {};
                } else if ($scope.current_tab === 'P') {
                    $scope.dtOptions1 = DTOptionsBuilder.newOptions()
                            .withOption('ajax', {
                                 method: 'POST',
                                url: urlservice.url + 'getMembersByMembershipId',
                                xhrFields: {
                                    withCredentials: true
                                 },
                                dataSrc: function (json) {
                                    $scope.membershiplistcontents = json.data;
                                    $scope.count_total_hold = json.count_total_hold;
                                    $scope.count_total_active = json.count_total_active;
                                    $scope.count_total_cancel = json.count_total_cancel;
                                     $scope.$apply();
                                    return json.data;
                                },
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                 "company_id": $localStorage.company_id,
                                 "membership_id": mem_id,
                                 "membership_opt_id": mem_opt_id,
                                 "membership_status": tab,
                                  },
                                error: function (xhr, error, thrown) {
                                    if (xhr.responseJSON.status === "Failed") {
                                        $('#progress-full').hide();
                                        $("#pushmessageModal").modal('show');
                                        $("#pushmessagetitle").text('Message');
                                        $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                    }else if(xhr.responseJSON.status === 'Expired'){
                                        $('#progress-full').hide();
                                        $("#messageModal").modal('show');
                                        $("#messagetitle").text('Message');
                                        $("#messagecontent").text(xhr.responseJSON.msg);
                                        $scope.logout();  
                                    } else if (xhr.responseJSON.status === 'Version') {
                                        $('#progress-full').hide();
                                        $scope.handleFailure(xhr.responseJSON.msg);
                                    }else{
                                        $('#progress-full').hide();
                                    }
                                }
                            })

                            .withOption('createdRow', function (row, data, dataIndex) {
                                // Recompiling so we can bind Angular directive to the DT
                                $compile(angular.element(row).contents())($scope);
                            })
                            .withOption('headerCallback', function (header) {
                                // Use this headerCompiled field to only compile header once
                                $scope.headerCompiled = true;
                                $compile(angular.element(header).contents())($scope);
                            })

                            .withDataProp('data')
                            .withDOM('lfrti')
                            .withScroller()
                            .withOption('deferRender', true)
                            .withOption('scrollY', 350)
                            .withOption('scrollX', '100%')
//                            .withOption('scrollCollapse', true)
//                            .withOption('processing', true)
                            .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                            .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                            .withOption('serverSide', true)
                            .withOption('search', true)
                            .withOption('info', false)
                            .withOption('stateSave',true)
                            .withOption('stateDuration', 60*5)
                            .withOption('order', [5, 'desc'])
                         
                    $scope.dtColumns1 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else{
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendMemindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_hold1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername"  data-toggle="tooltip" title="' + data + '"  ng-click="membershipIndividualDetail(\'' + full.id + '\' ,' + "'onhold'" + ',' + full.membership_option_id + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_hold2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                        DTColumnBuilder.newColumn('created_dt').withTitle('Registration Date').withClass('col_hold3'),
                        DTColumnBuilder.newColumn('hold_date').withTitle('Date Placed On Hold').withClass('col_hold4'),
                        DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_hold6'),
                        DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_hold7').renderWith(
                                function (data, type, full, meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        if (full.email_subscription_status === 'U') {
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                        } else {
                                            return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendMemindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                        }
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendMemindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a  class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');" ><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';
                                }
                            }),
                        DTColumnBuilder.newColumn('participant_birthdate').withTitle('Birthday').withClass('col_hold8'),
                        DTColumnBuilder.newColumn('participant_age').withTitle('Age').withClass('col_hold9'),
                        DTColumnBuilder.newColumn('rank_status').withTitle('Rank').withClass('col_hold10').renderWith(
                                function (data, type, full) {
//                                    return '<a  class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'P'"+ ',' + full.membership_id + ',' + full.id +','+'$event'+');">' + full.rank_status + '</a>';
                                    if (data == "") {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.membership_id + ',' + full.id + ',' + '$event' + ');">' + "None" + '</a>';
                                    } else {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.membership_id + ',' + full.id + ',' + '$event' + ');">' + full.rank_status + '</a>';
                                    }
                                }),
                        DTColumnBuilder.newColumn('last_advanced').withTitle('Last Advanced').withClass('col_hold11').renderWith(
                                function (data, type, full) {
                                    if (data == 0)
                                        return 'Today';
                                    else
                                        return (data) + ' days ago';
                                }),
                        DTColumnBuilder.newColumn('t').withTitle('Membership').withClass('col_hold12').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.t + '</span>';
                                }),
                        DTColumnBuilder.newColumn('past_due').withTitle('Bill Past Due').withClass('col_hold13').notSortable().renderWith(function(data,type,full){
                            return  $scope.wp_currency_symbol +''+data;
                        }),
                        DTColumnBuilder.newColumn('t').withTitle('End Date').withClass('col16').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                    ];
//                      $scope.dtInstance1 = {};
                }else if($scope.current_tab === 'C'){
                    $scope.dtOptions2 = DTOptionsBuilder.newOptions()
                            .withOption('ajax', {
                                 method: 'POST',
                              url: urlservice.url + 'getMembersByMembershipId',
                              xhrFields: {
                                withCredentials: true
                             },
                                dataSrc: function (json) {
                                    $scope.membershiplistcontents = json.data;
                                     $scope.count_total_hold = json.count_total_hold;
                                    $scope.count_total_active = json.count_total_active;
                                    $scope.count_total_cancel = json.count_total_cancel;
                                      $scope.$apply();
                                    return json.data;
                                },
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    "company_id": $localStorage.company_id,
                                    "membership_id": mem_id,
                                    "membership_opt_id": mem_opt_id,
                                    "membership_status": tab,
                                },
                                error: function (xhr, error, thrown) {
                                    if (xhr.responseJSON.status === "Failed") {
                                        $('#progress-full').hide();
                                        $("#pushmessageModal").modal('show');
                                        $("#pushmessagetitle").text('Message');
                                        $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                    }else if(xhr.responseJSON.status === 'Expired'){
                                        $('#progress-full').hide();
                                        $("#messageModal").modal('show');
                                        $("#messagetitle").text('Message');
                                        $("#messagecontent").text(xhr.responseJSON.msg);
                                        $scope.logout();  
                                    } else if (xhr.responseJSON.status === 'Version') {
                                        $('#progress-full').hide();
                                        $scope.handleFailure(xhr.responseJSON.msg);
                                    }else{
                                        $('#progress-full').hide();
                                    }
                                }
                            })

                            .withOption('createdRow', function (row, data, dataIndex) {
                                // Recompiling so we can bind Angular directive to the DT
                                $compile(angular.element(row).contents())($scope);
                            })
                            .withOption('headerCallback', function (header) {
                                // Use this headerCompiled field to only compile header once
                                $scope.headerCompiled = true;
                                $compile(angular.element(header).contents())($scope);
                            })

                            .withDataProp('data')
                            .withDOM('lfrti')
                            .withScroller()
                            .withOption('deferRender', true)
                            .withOption('scrollY', 350)
                            .withOption('scrollX', '100%')
//                            .withOption('scrollCollapse', true)
//                            .withOption('processing', true)
                            .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                            .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                            .withOption('serverSide', true)
                            .withOption('search', true)
                            .withOption('info', false)
                            .withOption('stateSave',true)
                            .withOption('stateDuration', 60*5)
                            .withOption('order', [5, 'desc'])
                    
                    $scope.dtColumns2 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                   if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else{
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  '<a ng-click="sendMemindividualpush(\'' + full.id + '\' ,' + "'push'" + ');">' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_can1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '"  ng-click="membershipIndividualDetail(\'' + full.id + '\' ,' + "'cancelled'" + ',' + full.membership_option_id + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_can2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                        DTColumnBuilder.newColumn('membership_status').withTitle('Status').withClass('col_can3').renderWith(
                                function (data, type, full) {
                                    if (data === 'C')
                                        return 'Cancelled';
                                    else
                                        return  'Completed';
                                }),
                        DTColumnBuilder.newColumn('mem_reg_completion_date').withTitle('Date').withClass('col_can4'),
                        DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_can5'),
                        DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_can6').renderWith(
                                function (data, type, full,meta) {
                                if(full.bounce_flag==='N'){
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendMemindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }else{
                                        return '<a  class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\''+ full.bounce_flag +'\',\''+ data +'\','+'$event'+',\''+full.id+'\');" ><img src="image/mail_error.png"  width="20" height="20">' +"(" + full.type +") "+ full.participant_email + '</a>';
                                   } 
                               }),
                        DTColumnBuilder.newColumn('participant_birthdate').withTitle('Birthday').withClass('col_can7'),
                        DTColumnBuilder.newColumn('participant_age').withTitle('Age').withClass('col_can8'),
                        DTColumnBuilder.newColumn('rank_status').withTitle('Rank').withClass('col_can9'),
                        DTColumnBuilder.newColumn('last_advanced').withTitle('Last Advanced').withClass('col_can10').renderWith(
                                function (data, type, full) {
                                    if (data == 0)
                                        return 'Today';
                                    else
                                        return (data) + ' days ago';
                                }),
                        DTColumnBuilder.newColumn('t').withTitle('Membership').withClass('col_can11').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.t + '</span>';
                                }),
                        DTColumnBuilder.newColumn('past_due').withTitle('Bill Past Due').withClass('col_can12').notSortable().renderWith(function(data,type,full){
                            return  $scope.wp_currency_symbol +''+data;
                        }),
                    ];
//                     $scope.dtInstance2 = {};
                }
                $('#progress-full').hide();
                angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                    
                })
            }
            
             $scope.showemailerror = function (row, type, data, event, id) {
                $scope.email_updt = event.target;
                $scope.row=row;
                $scope.mem_id = id;
                $scope.btype=type;
                $scope.errormsg = $scope.membershiplistcontents[row].error;
                $scope.bouncedemail = data;
                $("#emailerrormessageModal").modal('show');
                $("#emailerrormessagetitle").text('Message');
                $("#emailerrormessagecontent").text($scope.errormsg);
            }
            
             $scope.unblockbounceemail = function () {
                $('#progress-full').show();
                 $("#emailerrormessageModal").modal('hide');
                $http({
                    method: 'POST',
                   url: urlservice.url + 'unblockbounceemail',
                   data: {
                        "bounced_email": $scope.bouncedemail,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        angular.element($scope.email_updt).parent().html('<a class="buyername" data-toggle="tooltip" title="' + $scope.bouncedemail + '" ng-click="sendMemindividualpush(\'' + $scope.mem_id + '\' ,' + "'mail'" + ');">' + $scope.bouncedemail + '</a>');
                        $compile(angular.element(document.getElementById('DataTables_Table_30')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_31')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_32')).contents())($scope);
                        $('#progress-full').hide();
                        $("#msgModal").modal('show');
                        $("#msgtitle").text('Message');
                        $("#msgcontent").text('Student email unblocked successfully ');
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
                  $('#progress-full').hide();
            };
            
            //value copied on clipboard message
            $scope.toastMembershipmsg = function (membershiptype) {   
                var id_name = "";
                if(membershiptype === 'memlist'){
                    id_name = "snackbar";
                }else if(membershiptype === 'memcategory'){
                    id_name = "membershipsnackbar";
                }else if(membershiptype === 'memoption'){
                    id_name = "memoptsnackbar";
                }
                var x = document.getElementById(id_name)
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            
             //Membership list link info message
            $scope.managemembershipinfo = function(managemembershipinfo){
                var msg;
                if(managemembershipinfo === 'avglength'){
                    msg = "<p>Average Membership Length shows you how long a member stays until they quit in this membership category.<p><p>Life Time Value shows you how much a member pays you until they quit in this membership category.</p>";
                } else if(managemembershipinfo === 'publicurl'){
                   msg = "<p>This URL is a web link to your memberships list, allowing users without the app to browse and register for anyone of your memberships.<p><p>Give this link to your web developerto link it your website. Use this link to promote your events on your Facebook page and Google ads.</p>"; 
                }               
                $("#failuremessageModal").modal('show');
                $("#failuremessagetitle").text('Info');
                $("#failuremessagecontent").html(msg);
            };
            
            //recurring paid towards the event cost info
            $scope.paidmembershipinfo = function(){
                var msg = "This amount only shows what the user has paid toward the cost of the event.<br> It does not show any applicable passed on fees.";
                $("#pushmessageModal").modal('show');
                $("#pushmessagetitle").text('Info');
                $("#pushmessagecontent").html(msg);
            };
          
//            Members page active
             $scope.memberslistview = function(){
                $scope.membersview = true;
                $scope.livemembershipshow = false;
                $scope.draftmembershipshow = false;
                $scope.resettabbarclass();
                $('#li-members').addClass('active-event-list').removeClass('event-list-title');
                $('.membertab-text-1').addClass('greentab-text-active');
            };
          
            //LIVE MEMBERSHIP LISTING
            $scope.livemembershiplistview = function(){
                $localStorage.currentpage = "membership";
                $scope.membersview = false;
                $scope.livemembershipshow = true;
                $scope.draftmembershipshow = false;
                $scope.childmembershipview = false;
                $scope.resettabbarclass();
                $('#li-live').addClass('active-event-list').removeClass('event-list-title');
                $('#li-membershipoptions').addClass('active-event-list').removeClass('event-list-title');
                $('.membertab-text-2').addClass('greentab-text-active');
                $scope.getmembershipdetails('P');
            };
            
            //DRAFT MEMBERSHIP LISTING
            $scope.draftmembershiplistview = function(){
                $scope.membersview = false;
                $scope.livemembershipshow = false;
                $scope.draftmembershipshow = true;
                $scope.childmembershipview = false;
                $scope.resettabbarclass();
                $('#li-draft').addClass('active-event-list').removeClass('event-list-title');
                $('#li-membershipoptions').addClass('active-event-list').removeClass('event-list-title');
                $('.membertab-text-2').addClass('greentab-text-active');
                $localStorage.redirectedfrmtemplate = false;
                $scope.getmembershipdetails('S');
            };
            
            //HIGHLIGHTING ACTIVE TAB VIA CSS
            $scope.resettabbarclass = function (viewname) {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('.greentab-text').removeClass('greentab-text-active');
            };
            
            $scope.membershipdateformat = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }
            }

            $scope.formatmembershipserverdate = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("-");
                    var day = datePart[2];
                    var year = datePart[0];
                    var month = datePart[1];
                    return month + '/' + day + '/' + year;
                } else {
                    var datevalue = "";
                    return datevalue;
                }

            }
            
            //get student list of this company
            $scope.getStudentList = function(){
//                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getstudent',
                    params: {
                        "company_id": $localStorage.company_id,
                        "for": "membership"
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function(response){
                    if (response.data.status === 'Success') {
                        $scope.studentlist  = response.data.msg.student_details;
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $scope.studentlist  = [];
                        $('#progress-full').hide();
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.getStudentList();            
            
            //get membership details of live, draft
            $scope.getmembershipdetails = function(list_type){
                $('#progress-full').show();            
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getAllMembershipCategory',
                    params: {
                        "company_id": $localStorage.company_id,
                        "list_type":list_type
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') { 
                        $scope.NumofLive = response.data.count.P;
                        $scope.NumofDraft = response.data.count.S;
                        $scope.Memregurl = response.data.membership_list_url;
                        if(parseInt($scope.NumofLive) > 0){
                            $scope.nolivemembershiplisted = false; 
                        }else{
                            $scope.nolivemembershiplisted = true; 
                        }
                        if(parseInt($scope.NumofDraft) > 0){
                            $scope.nodraftmembershiplisted = false;
                        }else{
                            $scope.nodraftmembershiplisted = true;
                        }
                        
                        if(list_type === 'P'){
                            $scope.liveMembershipList = response.data.msg.live;
                            $scope.draftMembershipList = "";
                        }else if(list_type === 'S'){
                            $scope.draftMembershipList = response.data.msg.draft;
                            $scope.liveMembershipList = "";
                        }                        
                        
                        if ($localStorage.addmembershipfrommemberstab === true) {
                            var memid = $localStorage.redirectmembershipid;
                            $scope.livearray = $scope.liveMembershipList;
                            for (var i = 0; i < $scope.livearray.length; i++) {
                                if ($scope.livearray[i].membership_id == memid) {
                                    $scope.redirectedmembershiparray = $scope.livearray[i];
                                }
                            } 
                            $scope.managemembership($scope.redirectedmembershiparray);
                        }else{
                            $('#progress-full').hide();
                        }
                        
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{                         
                        $scope.liveMembershipList = [];
                        $scope.draftMembershipList = [];
                        $scope.NumofLive = response.data.count.P;
                        $scope.NumofDraft = response.data.count.S;
                        $('#progress-full').hide();
                    }   
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });  
            };   
            
           
            
            $scope.toggleAll = function (selectAll, selectedItems) {
                $scope.selectedData = [];
                 $scope.color_green='N';
                 $scope.all_select_flag='N';
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        selectedItems[id] = selectAll;
                        if(selectAll!==false){
//                            $scope.selectedData.push({"id":id});
                            $scope.all_select_flag='Y';
                            $scope.color_green='Y';
                        }
                    }
                }
                $scope.maintainselected = selectedItems;
            };
            
          
            
          
           $scope.toggleOne= function (selectedItems) {
               $scope.maintainselected = selectedItems;
                var check=0;
                  $scope.all_select_flag='N';
                   $scope.color_green='N';
                $scope.selectedData = [];
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        if (!selectedItems[id]) {
                            check = 1;
                        }else{
                            $scope.selectedData.push({"id":id});
                             $scope.color_green='Y';
                        }
                         
                    }
                }
                if(check === 1){
                    $scope.selectAll = false;
                }else{
                    $scope.selectAll = true;
                }
            }
            
            $scope.safariiosdate = function (dat) {

                var dda = dat.toString();
                dda = dda.replace(/-/g, "/");
                var curr_date = new Date(dda);   //  FOR DATE FORMAT IN SAFARI 

                var given_start_year = curr_date.getFullYear();
                var given_start_month = curr_date.getMonth();
                var given_start_day = curr_date.getDate();
                given_start_month = curr_date.getMonth() + 1;
                var dda = given_start_year + "-" + given_start_month + "-" + given_start_day;
                dda = dda.toString();
                dda = dda.replace(/-/g, "/");
                var dup_date = new Date(dda);  //  FOR DATE FORMAT IN SAFARI 

                return dup_date;
            
            };
            
            $scope.SEdateFormat = function (SEdate){
            
                var sdate = SEdate.split("-");
                var syear = sdate[0];
                var smonth = parseInt(sdate[1], 10) - 1; 
                var sdate = sdate[2];
                var sedate = new Date(syear, smonth, sdate);
                return sedate;
            
            };
            
            
            $scope.handlepaste = function (e) {
                var pasted_value = e.originalEvent.clipboardData.getData('text/plain');
                var pasted_text_length = pasted_value.length;  
                if(pasted_text_length > 100){
                    var x = document.getElementById("snackbarpaste")
                    x.className = "showpaste";
                    setTimeout(function(){ x.className = x.className.replace("showpaste", ""); }, 3000);
                }
            }; 
           
           
           //Manage action for membership category
            $scope.managemembership = function (list) {
                $('#progress-full').show();
                $scope.mainmembershipview = true;
                $scope.childmembershipview = true;
                $scope.membersview = false;
                $scope.livemembershipshow = false;
                $scope.draftmembershipshow = false;
                $scope.showmembershipsublist = false;
                $scope.addmemparticipantview = false;

                $http({
                    method: 'GET',
                    url: urlservice.url + 'getMembershipCategory',
                    params: {
                        "company_id": $localStorage.company_id,
                        "list_type": list.category_status,
                        "membership_id": list.membership_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true 

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.NumofLive = response.data.count.P;
                        $scope.NumofDraft = response.data.count.S;
                        $scope.membershipcategory_id = $scope.membership_details.membership_id;
                        $scope.membershipcategorytitle = $scope.membership_details.category_title;
                        $scope.membershipcategorystatus = $scope.membership_details.category_status;
                        if($scope.membership_details.membership_options.length > 0){
                            $scope.childmembershiparray = $scope.membership_details.membership_options;
                        } else {
                            $scope.childmembershiparray = "";
                        }
                        $scope.rank_details = $scope.membership_details.rank_details;
                        $scope.childmembershiparray_copy = angular.copy($scope.childmembershiparray);
                        $localStorage.currentManageMembershipDetails = $scope.membership_details;

                        if ($localStorage.page_from === "manage_members_active" || $localStorage.page_from === "manage_members_onhold" || $localStorage.page_from === "manage_members_cancelled") {
                            if ($scope.childmembershiparray.length > 0) {
                                for (i = 0; i < $scope.childmembershiparray.length; i++) {
                                    if (parseInt($scope.current_manage_mem_opt_id) === parseInt($scope.childmembershiparray[i].membership_option_id)) {
                                        $scope.managechildmembership($scope.childmembershiparray[i]);
                                    }
                                }
                            }
                        }else if ($localStorage.addmembershipfrommemberstab === true) {
                            var memoptid = $localStorage.redirectmembershipoptionsid;
                            $scope.redirectedmembershipoptionsarray = $scope.childmembershiparray;
                            for (var j = 0; j < $scope.redirectedmembershipoptionsarray.length; j++) {
                                if ($scope.redirectedmembershipoptionsarray[j].membership_option_id == memoptid) {
                                    $scope.redirectedmembershipoptionarray = $scope.redirectedmembershipoptionsarray[j];
                                }
                            }
                            $scope.managechildmembership($scope.redirectedmembershipoptionarray);
                        } else {
                            $('#progress-full').hide();
                        }

                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $scope.NumofLive = response.data.count.P;
                        $scope.NumofDraft = response.data.count.S;
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            //Manage action for membership options
            $scope.managechildmembership = function (lst) { 
                $('#progress-full').show();
                $scope.childmembershipview = false;
                $scope.mainmembershipview = false;
                $scope.showmembershipsublist = true;
                $scope.addmemparticipantview = false;
                $scope.activemembers = true;
                $scope.cancelmembers = false;
                $scope.onholdmembers= false;
                $scope.change_made = $scope.search_value = '';
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('#li-orders').addClass('active-event-list').removeClass('event-list-title');
                $('.managemembersub-tabtext-1').addClass('greentab-text-active');
                
                
                $scope.membershipoptiontitle = lst.membership_title;
                $scope.membershipoption_id = lst.membership_option_id;
                $('#mempaymntstartdate').datepicker('setStartDate', new Date()); 
                $scope.current_membership_manage = lst;
                $scope.getMemCustomer_list($scope.membershipcategory_id, $scope.membershipoption_id, 'R');
                $scope.membership_structure = lst.membership_structure;
                $scope.membership_billing_option = lst.billing_options;
                $scope.membership_billing_startdate_type = lst.billing_payment_start_date_type;
                $scope.membership_billing_startdate = lst.billing_options_payment_start_date;
                $scope.membership_billing_deposit_amount = parseFloat(lst.deposit_amount);
                $scope.membership_no_of_payments = lst.no_of_payments;
                $scope.membership_no_of_classes = parseFloat(lst.no_of_classes);
                $scope.membership_expiry_period = lst.expiration_date_type;
                $scope.membership_expiry_value = lst.expiration_date_val;
                $scope.membership_expiry_status = lst.expiration_status;
                
                $scope.show_in_app_flg = $scope.current_membership_manage.show_in_app_flg;
                if($scope.show_in_app_flg === 'Y'){
                    $scope.show_in_app = true;
                }else{
                    $scope.show_in_app = false;
                }
                $scope.membership_structure = $scope.current_membership_manage.membership_structure;
                $scope.processing_fee_show = $scope.current_membership_manage.membership_processing_fee_type;
                $scope.memsignupfee = parseFloat($scope.current_membership_manage.membership_signup_fee);
                $scope.memfee = parseFloat($scope.current_membership_manage.membership_fee);
                $scope.mem_fee_include_status = $scope.current_membership_manage.initial_payment_include_membership_fee;
                
                $scope.mem_rec_frequency = $scope.current_membership_manage.membership_recurring_frequency;
                if($scope.mem_rec_frequency==='B' || $scope.mem_rec_frequency==='M'){
                    $scope.prorate_first_payment = $scope.current_membership_manage.prorate_first_payment_flg;                    
                    $scope.cus_rec_freq_period_type = '';
                    $scope.cus_rec_freq_period_val = '';
                }else if($scope.mem_rec_frequency==='C'){
                    $scope.prorate_first_payment = 'N';
                    $scope.cus_rec_freq_period_type = $scope.current_membership_manage.custom_recurring_frequency_period_type;
                    $scope.cus_rec_freq_period_val = $scope.current_membership_manage.custom_recurring_frequency_period_val;
                }else{
                    $scope.prorate_first_payment = 'N';
                    $scope.cus_rec_freq_period_type = '';
                    $scope.cus_rec_freq_period_val = '';
                }
                
                $scope.delay_rec_payment_start_flg = $scope.current_membership_manage.delay_recurring_payment_start_flg;
                if($scope.delay_rec_payment_start_flg === 'Y'){
                    $scope.delayed_recurring_payment_type =$scope.current_membership_manage.delayed_recurring_payment_type;
                    $scope.delayed_recurring_payment_val = $scope.current_membership_manage.delayed_recurring_payment_val;
                }else{
                    $scope.delayed_recurring_payment_type = '';
                    $scope.delayed_recurring_payment_val = '';
                } 
                
                $scope.specific_start_date =$scope.current_membership_manage.specific_start_date;
                $scope.specific_end_date = $scope.current_membership_manage.specific_end_date;
                $scope.specific_payment_frequency =$scope.current_membership_manage.specific_payment_frequency;
                $scope.billing_days =$scope.current_membership_manage.billing_days;
                $scope.exclude_from_billing_flag =$scope.current_membership_manage.exclude_from_billing_flag;
                if($scope.exclude_from_billing_flag === 'Y'){
                    if($scope.current_membership_manage.mem_billing_exclude.length > 0){
                        $scope.mem_billing_exclude_days = $scope.current_membership_manage.mem_billing_exclude;
                    }else{
                        $scope.mem_billing_exclude_days = [];
                    }
                }else{
                    $scope.mem_billing_exclude_days = [];
                } 
                
                if($scope.membership_structure === 'SE'){ // Has specific start and end date
                    var curr_date = new Date();
                    var current_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                    var specific_start_date = $scope.SEdateFormat($scope.specific_start_date);
                    if(+specific_start_date < +current_date){ 
                        var curr_date = new Date(); 
                        $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);
                        $scope.mempaymntstartdate = ('0' + (curr_date.getMonth() + 1)).slice( - 2) + '/' + ('0' + curr_date.getDate()).slice( - 2) + '/' +curr_date.getFullYear();
                        $scope.payment_start_date_view = $scope.recurringdatestring($scope.payment_start_date);
                        $scope.payment_end_date_view = $scope.recurringdatestring($scope.specific_end_date);
                        var dup_date = $scope.safariiosdate($scope.payment_start_date);
                        var du_date = dup_date;
                        $scope.actual_total_payment_startdate = du_date.getFullYear() + '-' + ('0' + (du_date.getMonth() + 1)).slice( - 2) + '-' + ('0' + du_date.getDate()).slice( - 2);
                    }else{ 
                        var curr_date = $scope.SEdateFormat($scope.specific_start_date); 
                        $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);
                        $scope.mempaymntstartdate = ('0' + (curr_date.getMonth() + 1)).slice( - 2) + '/' + ('0' + curr_date.getDate()).slice( - 2) + '/' +curr_date.getFullYear();
                        $scope.payment_start_date_view = $scope.recurringdatestring($scope.payment_start_date);
                        $scope.payment_end_date_view = $scope.recurringdatestring($scope.specific_end_date);
                        var dup_date = $scope.safariiosdate($scope.payment_start_date);
                        var du_date = dup_date;
                        $scope.actual_total_payment_startdate = du_date.getFullYear() + '-' + ('0' + (du_date.getMonth() + 1)).slice( - 2) + '-' + ('0' + du_date.getDate()).slice( - 2);
                    }
                }else{      
                    var curr_date = new Date(); 
                    $scope.payment_start_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);
                    $scope.mempaymntstartdate = ('0' + (curr_date.getMonth() + 1)).slice( - 2) + '/' + ('0' + curr_date.getDate()).slice( - 2) + '/' +curr_date.getFullYear();
                    $scope.payment_start_date_view = $scope.recurringdatestring($scope.payment_start_date);
                    var dup_date = $scope.safariiosdate($scope.payment_start_date);
                    var du_date = dup_date;
                    $scope.actual_total_payment_startdate = du_date.getFullYear() + '-' + ('0' + (du_date.getMonth() + 1)).slice( - 2) + '-' + ('0' + du_date.getDate()).slice( - 2); 
                }
           
                if($scope.membership_structure === 'OE'){ 
                    var dup_date = $scope.safariiosdate($scope.payment_start_date);
                    if($scope.delay_rec_payment_start_flg === 'N') { //multiple recurring payment details
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else {
                        if ($scope.delayed_recurring_payment_type === 'DM') {
                            dup_date = $scope.addMonths(dup_date, $scope.delayed_recurring_payment_val);
                            var dup = dup_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        } else {
                            dup_date.setDate(dup_date.getDate() + (+$scope.delayed_recurring_payment_val * 7));
                            var dup = dup_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }
                        $scope.delayed_recurring_payment_startdate = $scope.total_payment_startdate;
                        $scope.delayed_rec_payment_date = $scope.recurringdatestring($scope.total_payment_startdate);
                    }
                }else if($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' ){
                  var dup_date = $scope.safariiosdate($scope.payment_start_date);
                    if($scope.membership_billing_option === 'PF'){
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }else if($scope.membership_billing_option === 'PP'){
                        if ($scope.membership_billing_startdate_type === '1') {          //30 days after registration
                            var cus_curr_date = new Date();
                            cus_curr_date.setDate(cus_curr_date.getDate() + 30);
                            var dup = cus_curr_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        } else if ($scope.membership_billing_startdate_type === '2') {       //1 week after registration
                            var cus_curr_date = new Date();
                            cus_curr_date.setDate(cus_curr_date.getDate() + 7);
                            var dup = cus_curr_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);  
                        }else if ($scope.membership_billing_startdate_type === '3') {       //2 week after registration
                            var cus_curr_date = new Date();
                            cus_curr_date.setDate(cus_curr_date.getDate() + 14);
                            var dup = cus_curr_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }else if ($scope.membership_billing_startdate_type === '4') {       //custom date selection
                            $scope.total_payment_startdate = $scope.membership_billing_startdate;  
                        }else if ($scope.membership_billing_startdate_type === '5') {       //on membership start date
                            $scope.total_payment_startdate = $scope.payment_start_date;  
                        }

                    }

                }else if($scope.membership_structure === 'SE'){
                        var dup_date = $scope.safariiosdate($scope.payment_start_date);
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                }
                
                if(($scope.membership_structure === 'NC' || $scope.membership_structure === 'C')&& $scope.membership_expiry_status==='Y'){
                    var dup_date = $scope.safariiosdate($scope.payment_start_date);
                    if ($scope.membership_expiry_period === 'M') {
                        dup_date = $scope.addMonths(dup_date, $scope.membership_expiry_value);
                        var dup = dup_date;
                        $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }else if ($scope.membership_expiry_period === 'W') {
                        dup_date.setDate(dup_date.getDate() + (+$scope.membership_expiry_value * 7));
                        var dup = dup_date;
                        $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }else if ($scope.membership_expiry_period === 'Y') {
                        dup_date.setFullYear(dup_date.getFullYear() + +$scope.membership_expiry_value);
                        var dup = dup_date;
                        $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }                    
                }else if($scope.membership_structure === 'SE'){
                    $scope.membership_expiry_date = $scope.specific_end_date;
                }else{
                    $scope.membership_expiry_date = '';
                }
                
                if ($scope.current_membership_manage.membership_disc.length > 0) {
                        $scope.msdiscountList = $scope.current_membership_manage.membership_disc;
                } else {
                    $scope.msdiscountList = "";
                }
                $scope.current_membership_reg_columns = $scope.current_membership_manage.reg_columns;
                if ($scope.current_membership_manage.waiver_policies === null) {
                    $scope.waiverShow = false;
                    $scope.waiverarea = '';
                } else {
                    $scope.waiverShow = true;
                    $scope.waiverarea = $scope.current_membership_manage.waiver_policies;
                }
               
                
                if($localStorage.membershippagetype === 'transfer' || $localStorage.addmembershipfrommemberstab === true || $scope.addfromlist === true){ 
                    $scope.addMembershipParticipant();
                } 
                
                if(!$localStorage.page_from){
                    $scope.dtOptions
                         .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }
                $localStorage.page_from = '';
                
                $scope.showMembershipTotal();
                if($localStorage.addmembershipfrommemberstab === true) {

                } else {
                    $('#progress-full').hide();
                }
            };
            
             //Add participant form setup view before submit
            $scope.addMembershipParticipant = function () {
                $('#progress-full').show();
                $scope.change_made = $scope.search_value = '';
                if ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C') {
                    if ($scope.membership_billing_startdate_type === '4') {       //custom date selection
                        var cus_curr_date = new Date();
                        var cus_current_date = cus_curr_date.getFullYear() + '-' + ('0' + (cus_curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + cus_curr_date.getDate()).slice(-2);
                        if (new Date(cus_current_date) > new Date($scope.membership_billing_startdate)) {
                            $('#progress-full').hide();
                            $("#CopyCategorymessageModal").modal('show');
                            $("#CopyCategorymessagetitle").text('Success Message');
                            $("#CopyCategorymessagecontent").text('Payment plan for this membership has already started, please contact studio for assistance.');
                            return false;
                        }
                    }
                }
                
                if ($scope.membership_structure === 'SE') { 
                    var curr_date = new Date();
                    var current_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2));
                    var specific_end_date = new Date($scope.specific_end_date);
                    
                    if (+specific_end_date < +current_date) {
                        $('#progress-full').hide();
                        $("#CopyCategorymessageModal").modal('show');
                        $("#CopyCategorymessagetitle").text('Message');
                        $("#CopyCategorymessagecontent").text('Membership has already expired, please contact studio for assistance.');
                        return false;
                    }
                }

                $scope.childmembershipview = false;
                $scope.mainmembershipview = false;
                $scope.showmembershipsublist = false;
                $scope.addmemparticipantview = true;
                $scope.reg_col_value[0] = '';
                $scope.reg_col_value[1] = '';
                $scope.reg_col_value[2] = '';
                $scope.reg_col_value[3] = '';
                $scope.reg_col_value[4] = '';
                $scope.reg_col_value[5] = '';
                $scope.reg_col_value[6] = '';
                $scope.reg_col_value[7] = '';
                $scope.reg_col_value[8] = '';
                $scope.reg_col_value[9] = '';
                $scope.cardnumber = '';
                $scope.ccmonth = '';
                $scope.ccyear = '';
                $scope.cvv = '';
                $scope.studentselected = '';
                $scope.membershipdiscount = '';
                $scope.signupdiscount = '';
                $scope.zerocostview = true;
                $scope.showmempaymentview = true;
                $scope.waiverchecked = false;
                $scope.stripeccchecked = false;
                $scope.participant_Streetaddress = '';
                $scope.participant_City = '';
                $scope.participant_State = '';
                $scope.participant_Zip = '';
                $scope.participant_Country = '';
                $scope.selectedcardtype = 'new';
                $scope.membershippaymentform.$setPristine();
                $scope.membershippaymentform.$setUntouched();
                document.getElementById("membershippaymentform").reset();

                if ($scope.membership_structure === 'SE') { 
                    var curr_date = new Date();
                    var current_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth() + 1)).slice(-2) + '-' + ('0' + curr_date.getDate()).slice(-2));
                    var specific_start_date = $scope.SEdateFormat($scope.specific_start_date);
                    
                    if (+specific_start_date < +current_date) {
                        $('#mempaymntstartdate').datepicker('setStartDate', new Date());
                        $('#mempaymntstartdate').datepicker('setEndDate', $scope.SEdateFormat($scope.specific_end_date));
                    } else {
                        $('#mempaymntstartdate').datepicker('setStartDate', $scope.SEdateFormat($scope.specific_start_date));
                        $('#mempaymntstartdate').datepicker('setEndDate', $scope.SEdateFormat($scope.specific_end_date));
                    }
                } else {
                    $('#mempaymntstartdate').datepicker('setStartDate', new Date());
                }

                if (parseFloat($scope.memfee) == 0 && parseFloat($scope.memsignupfee) == 0) {
                    $scope.zerocostview = false;
                    $scope.showmempaymentview = false;
                }
                if ((parseFloat($scope.memfee) > 0 || parseFloat($scope.memsignupfee) > 0)) {
                    $scope.zerocostview = true;
                }

                if ($localStorage.membershippagetype === 'transfer') {
                    $scope.reg_col_value[0] = $scope.memeditparticipant.membership_registration_column_1;
                    $scope.reg_col_value[1] = $scope.memeditparticipant.membership_registration_column_2;
                    $scope.reg_col_value[2] = $scope.memeditparticipant.membership_registration_column_3;
                    if ($('#column2').is(':visible')) { //if the container is visible on the page
                        $('#progress-full').show();
                    } else {
                        setTimeout(function () {
                            $("#column2").dateDropdowns({
                                displayFormat: "mdy",
                                required: true
                            });
                            $('#progress-full').hide();
                        }, 50);
                    }
                    $scope.firstname = $scope.memeditparticipant.buyer_name.substr(0, $scope.memeditparticipant.buyer_name.indexOf(' '));
                    $scope.lastname = $scope.memeditparticipant.buyer_name.substr($scope.memeditparticipant.buyer_name.indexOf(' ') + 1);
                    $scope.phone = $scope.memeditparticipant.buyer_phone;
                    $scope.email = $scope.memeditparticipant.buyer_email;
                    $scope.participant_Streetaddress = $scope.memeditparticipant.participant_street;
                    $scope.participant_City = $scope.memeditparticipant.participant_city;
                    $scope.participant_State = $scope.memeditparticipant.participant_state;
                    $scope.participant_Country = $scope.memeditparticipant.participant_country;
                    $scope.participant_Zip = $scope.memeditparticipant.participant_zip;

                    if ($scope.carddetails === undefined || $scope.carddetails.length == 0) {
                        $scope.showcardselection = false;
                        if (parseFloat($scope.final_payment_amount) <= 0) {
                            if ($scope.membership_structure === 'OE' && ((parseFloat($scope.delay_membership_amount_view) > 0) || (parseFloat($scope.first_payment_amount) > 0))) {
                                $scope.showmempaymentview = true;
                            } else if (($scope.membership_structure === 'NC' || $scope.membership_structure === 'C') && (parseFloat($scope.payment_amount) > 0)) {
                                $scope.showmempaymentview = true;
                            } else {
                                $scope.showmempaymentview = false;
                            }
                        } else {
                            $scope.showmempaymentview = true;
                        }
                    } else {
                        $scope.showcardselection = true;
                        $scope.showmempaymentview = false;
                    }
                    var dob = $scope.memeditparticipant.membership_registration_column_3;
                    setTimeout(function () {
                        document.getElementById("column2").value = dob;
                    }, 10);
                }

                if ($scope.rank_details.length > 0) {
                    $scope.selectedrank = 0;
                    $scope.selectRankDetail($scope.selectedrank);
                }

                if ($('#column2').is(':visible')) { //if the container is visible on the page
                    $('#progress-full').show();
                } else {
                    setTimeout(function () {
                        $("#column2").dateDropdowns({
                            displayFormat: "mdy",
                            required: true
                        });
                        $('#progress-full').hide();
                    }, 50);
                }                
                
                if($scope.stripe_enabled){
                    $scope.stripe = Stripe($localStorage.stripe_publish_key);
                    $scope.getStripeElements();
                }
            };
            
            $scope.$on('$viewContentLoaded', function () {
                $('#lstmemadvdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true,
                    endDate: ('setEndDate', new Date())
                });
            });
            
            $scope.$on('$viewContentLoaded', function () {
                $('#mempaymntstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
            });

            $("#mempaymntstartdate").on("dp.change", function () {
                $scope.mempaymntstartdate = $("#mempaymntstartdate").val();                
            });
            
            $scope.membershipTransferDetail = function(){ 
                $localStorage.page_from = $localStorage.manage_page_from;
                $("#memtransfermessageModal").modal('hide');
                $location.path('/membershipdetail');
            };
            
            $scope.membershipIndividualDetail = function ( membership_reg_id, current_tab, membership_option_id) {
                $localStorage.currentMembershipregid = membership_reg_id;
                $localStorage.currentMembership_option_id = membership_option_id;
                $localStorage.page_from = 'manage_members_'+current_tab;
                $localStorage.manage_page_from = $localStorage.page_from;
                console.log($localStorage.page_from);
                $location.path('/membershipdetail');
            };
            
            $scope.catchMemPaymntStartDate = function(){
               if($scope.mempaymntstartdate !=='' && $scope.mempaymntstartdate !== undefined && $scope.mempaymntstartdate !== null ){
                    $scope.payment_start_date = $scope.membershipdateformat($scope.mempaymntstartdate);
                    $scope.payment_start_date_view = $scope.recurringdatestring($scope.payment_start_date);
                    var dup_date = $scope.safariiosdate($scope.payment_start_date);
                    var du_date = dup_date;
                    $scope.actual_total_payment_startdate = du_date.getFullYear() + '-' + ('0' + (du_date.getMonth() + 1)).slice( - 2) + '-' + ('0' + du_date.getDate()).slice( - 2);
                    if($scope.membership_structure === 'OE'){
                        if($scope.delay_rec_payment_start_flg === 'N') { //multiple recurring payment details
                            var dup = dup_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        } else {
                            if ($scope.delayed_recurring_payment_type === 'DM') {
                                dup_date = $scope.addMonths(dup_date, $scope.delayed_recurring_payment_val);
                                var dup = dup_date;
                                $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                            } else {
                                dup_date.setDate(dup_date.getDate() + (+$scope.delayed_recurring_payment_val * 7));
                                var dup = dup_date;
                                $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                            }                        
                            $scope.delayed_recurring_payment_startdate = $scope.total_payment_startdate;
                            $scope.delayed_rec_payment_date = $scope.recurringdatestring($scope.total_payment_startdate);
                        }
                    }else if($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' ){
                        if($scope.membership_billing_option === 'PF'){
                            var dup = dup_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }else if($scope.membership_billing_option === 'PP'){
                            if ($scope.membership_billing_startdate_type === '1') {          //30 days after registration
                                var cus_curr_date = new Date();
                                cus_curr_date.setDate(cus_curr_date.getDate() + 30);
                                var dup = cus_curr_date;
                                $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);  
                            } else if ($scope.membership_billing_startdate_type === '2') {       //1 week after registration
                                var cus_curr_date = new Date();
                                cus_curr_date.setDate(cus_curr_date.getDate() + 7);
                                var dup = cus_curr_date;
                                $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);                         
                            }else if ($scope.membership_billing_startdate_type === '3') {       //2 week after registration
                                var cus_curr_date = new Date();
                                cus_curr_date.setDate(cus_curr_date.getDate() + 14);
                                var dup = cus_curr_date;
                                $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);  
                            }else if ($scope.membership_billing_startdate_type === '4') {       //custom date selection
                                $scope.total_payment_startdate = $scope.membership_billing_startdate;   
                            }else if ($scope.membership_billing_startdate_type === '5') {       //on membership start date
                                $scope.total_payment_startdate = $scope.payment_start_date;  
                            }

                        }

                    }else if($scope.membership_structure === 'SE'){
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }                

                    if(($scope.membership_structure === 'NC' || $scope.membership_structure === 'C')&& $scope.membership_expiry_status==='Y'){
                        var dup_date = $scope.safariiosdate($scope.payment_start_date);
                        if ($scope.membership_expiry_period === 'M') {
                            dup_date = $scope.addMonths(dup_date, $scope.membership_expiry_value);
                            var dup = dup_date;
                            $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }else if ($scope.membership_expiry_period === 'W') {
                            dup_date.setDate(dup_date.getDate() + (+$scope.membership_expiry_value * 7));
                            var dup = dup_date;
                            $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }else if ($scope.membership_expiry_period === 'Y') {
                            dup_date.setFullYear(dup_date.getFullYear() + +$scope.membership_expiry_value);
                            var dup = dup_date;
                            $scope.membership_expiry_date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }                    
                    }else if($scope.membership_structure === 'SE'){
                        $scope.membership_expiry_date = $scope.specific_end_date;
                        $scope.payment_end_date_view = $scope.recurringdatestring($scope.specific_end_date);
                    }else{
                        $scope.membership_expiry_date = '';
                    }

                    $scope.showMembershipTotal();   
                }
            };
            
            //converting server date format value into string on modal screen
            $scope.getParticipantBirthdate = function (dat) {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari){
                    var dat = dat+' 00:00:00';
                    var dda = dat.toString();
                    dda = dda.replace(/ /g, "T");
                    var time = new Date(dda);
                    var dateonly = time.toString();
                    var fulldateonly = dateonly.replace(/GMT.*/g, "");
                    var localdate = fulldateonly.slice(4, -9);
                    return localdate;
                } else {
                    var ddateonly = dat + 'T00:00:00+00:00';
                    var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                    var localdate = $filter('date')(newDate, 'MMM d, y');
                    return localdate;
                }
            };
            
            
            //For screen looks on mobile view
            $scope.topreview=function(){
                    document.getElementById('previewapp').scrollIntoView();
            };
            
            //Edit action for single/multiple event
            $scope.editmembership = function (detail) {
                $localStorage.membershippagetype = 'edit';
                $localStorage.isAddedCategoryDetails = 'N';                
                $('#progress-full').show();
                    $http({
                        method: 'GET',
                        url: urlservice.url + 'getMembershipCategory',
                        params: {
                            "company_id": $localStorage.company_id,
                            "list_type": detail.category_status,
                            "membership_id": detail.membership_id
                        },
                        headers: {"Content-Type": 'application/json; charset=utf-8',
                            },
                            withCredentials: true

                    }).then(function (response) {
                        if (response.data.status === 'Success') {
                            $('#progress-full').hide();                            
                            membershipListDetails.addMembershipDetail(response.data.msg);
                            $location.path('/addmembership');
                        } else if (response.data.status === 'Expired') {
                            console.log(response.data);
                            $('#progress-full').hide();
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Message');
                            $("#messagecontent").text(response.data.msg);
                            $scope.logout();
                        } else if (response.data.status === 'Version') {
                            $('#progress-full').hide();
                            $scope.handleFailure(response.data.msg);
                        } else {
                            $('#progress-full').hide();
                        }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });
            };
            
            $scope.copyMemOption = function (mem_option_id) {
                $('#progress-full').show();

                $http({
                    method: 'POST',
                    url: urlservice.url + 'copyMembershipOptions',
                    data: {
                        "membership_id": $scope.membershipcategory_id,
                        "membership_option_id": mem_option_id,
                        'copy_mem_type': 'Child',
                        'mem_template_flag': 'N',
                        'company_id': $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $("#msgModal").modal('show');
                        $("#msgtitle").text('Success Message');
                        $("#msgcontent").text('Duplicate membership option copied successfully');

                        if($scope.membership_details.membership_options.length > 0){
                            $scope.childmembershiparray = $scope.membership_details.membership_options;
                        } else {
                            $scope.childmembershiparray = "";
                        }

                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);

                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
             //copy action for single/multiple event
            $scope.copymembership = function(detail){
                $('#progress-full').show(); 
                
                $http({
                method: 'POST',
                url: urlservice.url+'copyMembershipCategory',
                data:{
                    'membership_id':detail.membership_id,
                    'copy_membership_type':'Main',
                    'membership_template_flag':'N',
                    'company_id':$localStorage.company_id,
                    'list_type':detail.category_status
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                   if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#CopyCategorymessageModal").modal('show');
                        $("#CopyCategorymessagetitle").text('Success Message');
                        $("#CopyCategorymessagecontent").text('Duplicate membership category copied successfully');
                        
                        $scope.NumofLive = response.data.count.P;
                        $scope.NumofDraft = response.data.count.S;
                        $scope.Memregurl = response.data.membership_list_url;
                        if(parseInt($scope.NumofLive) > 0){
                            $scope.nolivemembershiplisted = false; 
                        }else{
                            $scope.nolivemembershiplisted = true; 
                        }
                        if(parseInt($scope.NumofDraft) > 0){
                            $scope.nodraftmembershiplisted = false;
                        }else{
                            $scope.nodraftmembershiplisted = true;
                        }
                        
                        if(detail.category_status === 'P'){
                            $scope.liveMembershipList = response.data.msg.live;
                            $scope.draftMembershipList = "";
                        }else if(detail.category_status === 'S'){
                            $scope.draftMembershipList = response.data.msg.draft;
                            $scope.liveMembershipList = "";
                        } 
                        
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{ 
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            
               //send payment modal enable confirmation
            $scope.enablemembershippaymentmodal = function(){
                $scope.enablepayment=false;
                $scope.mobileenablepayment=true;
                $("#wpmemtitle").text('Mystudio Payment Processing');
            };         
           
            $scope.cancelwp = function () {
                $scope.wpfirstname = '';
                $scope.wplastname = '';
                $scope.wpemail = '';
                $scope.enablepayment = true;
                $scope.mobileenablepayment = false;
            } 
            
             //discount value exists membership cost validation
             $scope.membershipdiscountMax = function(disc){
                $scope.actual_membership_cost = 0;
                $scope.actual_membership_cost = +$scope.current_membership_manage.membership_fee;
                 
                if(disc===''){
                    return false;
                }
                if(parseFloat(disc)>0 && parseFloat(disc)>parseFloat($scope.actual_membership_cost)){
                    return true;
                }else{
                    return false;
                }
            };
            
             //discount value applied for membership cost below 5 validation
            $scope.MembershipCostMax = function(disc){
                $scope.actual_membership_cost = 0;
                $scope.actual_membership_cost = +$scope.current_membership_manage.membership_fee ;
                
                if(disc===''){
                    return false;
                }
                if ((parseFloat(disc) > 0 && parseFloat('5') <= (parseFloat($scope.actual_membership_cost) - parseFloat(disc))) || (parseFloat(disc) > 0 && parseFloat('0') == (parseFloat($scope.actual_membership_cost) - parseFloat(disc)))) { 
                    return false;
                } else {
                    return true;
                }
               
            };
            
            //discount value exists membership cost validation
            $scope.signupdiscountMax = function(disc){
                $scope.actual_signup_cost = 0;
                $scope.actual_signup_cost = +$scope.current_membership_manage.membership_signup_fee;
                 
                if(disc===''){
                    return false;
                }
                if(parseFloat(disc)>0 && parseFloat(disc)>parseFloat($scope.actual_signup_cost)){
                    return true;
                }else{
                    return false;
                }
            };
            
             //discount value applied for membership cost below 5 validation
            $scope.SignupCostMax = function(disc){
                $scope.actual_signup_cost = 0;
                $scope.actual_signup_cost = +$scope.current_membership_manage.membership_signup_fee;
                
                if(disc===''){
                    return false;
                }
                if ((parseFloat(disc) > 0 && parseFloat('5') <= (parseFloat($scope.actual_signup_cost) - parseFloat(disc))) || (parseFloat(disc) > 0 && parseFloat('0') == (parseFloat($scope.actual_signup_cost) - parseFloat(disc)))) { 
                    return false;
                } else {
                    return true;
                }
               
            };
            
            $scope.dateformat = function (date) {
                if(date){
                    $scope.datefor = date.split(" ");
                    return  $scope.datefor[0];
                }
            };
            
            
           //converting server date-time format value into string on membership pricing screen
            $scope.recurringdatestring = function (ddate) {
                var Displaydate = ddate.toString();
                Displaydate = ddate.split("-");
                var year = Displaydate[0];
                var month = Displaydate[1] - 1;
                var date = Displaydate[2];
                var ReadDate = new Date(year, month, date);
                var datesuccess = ReadDate.toString();
                var finaldateformat = datesuccess.split(" ");
                var finalized = finaldateformat[0] + " " + finaldateformat[1] + " " + finaldateformat[2] + " " + finaldateformat[3];
                return finalized;
            };
            
            $scope.addMonths = function (adate, months) {
                var ad = adate.getDate();
                adate.setMonth(adate.getMonth() + +months);
                if (adate.getDate() != ad) {
                    adate.setDate(0);
                }
                return adate;
            };
            
            $scope.SEProcessingFee = function (recurring_amnt) {
                var process_fee_per = 0, process_fee_val = 0, recurring_pro_fee = 0;
                var process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                var process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                if (parseFloat(recurring_amnt) >= 0 && parseFloat(recurring_amnt) != ''){
                    if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                        if (recurring_amnt > 0) {
                            var process_fee1 = +(parseFloat(recurring_amnt)) + +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                            var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                            recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                            if(isFinite(recurring_pro_fee)===false){
                                recurring_pro_fee = 0;
                            }
                        } else {
                            recurring_pro_fee = 0;
                        }
                    } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1'){
                        if (recurring_amnt > 0) {
                            var process_fee = +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                            recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                            if(isFinite(recurring_pro_fee)===false){
                                recurring_pro_fee = 0;
                            }
                        } else {
                            recurring_pro_fee = 0;
                        } 
                    }
                }
                return recurring_pro_fee;
            };
            
            $scope.SEMembershipFee = function (count) { 
                var SEMem_cost = 0;
                var selected_count = ($scope.billing_days.match(/1/g) || []).length;
                if (parseFloat($scope.recurring_membership_amount_view) >= 0 && parseFloat($scope.recurring_membership_amount_view) != ''){
                    if ($scope.specific_payment_frequency === 'PW' && parseFloat(count) > 0){   
                        SEMem_cost = (parseFloat($scope.recurring_membership_amount_view) / parseFloat(selected_count)) * parseFloat(count);
                    } else if ($scope.specific_payment_frequency === 'PM'  && parseFloat(count) > 0){                             
                        SEMem_cost = (parseFloat($scope.recurring_membership_amount_view) / (parseFloat(selected_count) * 4.345)) * parseFloat(count);
                    }else{
                        SEMem_cost = 0;
                    }
                }else{
                    SEMem_cost = 0;
                }
                return SEMem_cost;
            };
            
            $scope.startandenddate = function (start, end) {
                if(start !== undefined && start !== '' && end !== undefined && end !== ''){
                    var check_start_date = start;
                    var check_end_date = end;
                    $scope.daysarray = $scope.billing_days.split('');
                    $scope.checked_days_count = 0;
                    for (var check_date = check_start_date; check_date <= check_end_date; check_date.setDate(check_date.getDate() + 1)) {
                        $scope.day_check = check_date.getDay() - 1;
                        if ($scope.day_check === -1) {
                            $scope.day_check = 6;
                        }
                        if ($scope.daysarray[$scope.day_check] === '1') {
                            $scope.checked_days_count += 1;
                        }
                    }
                    return $scope.checked_days_count;
                }
            };
            
             //For single event & Multiple event total cost calculation based on discount and quantity
            $scope.showMembershipTotal = function () {
                $scope.total_amount_view = $scope.total_reg_amount_view = 0;
                $scope.total_membership_amount_view = 0;
                $scope.total_signup_amount_view = 0;
                $scope.processing_fee_value =  $scope.first_payment_amount = $scope.payment_amount = 0;
                $scope.recurring_processing_fee_value = 0;  
                $scope.membership_fee_include = 0;
                $scope.final_payment_amount = $scope.pay_infull_processingfee = 0;
                $scope.register_signup_discount_value = $scope.register_membership_discount_value = 0;
                $scope.membership_discount_show = $scope.signup_discount_show = true;
                $scope.exceedsActualcost = $scope.exceedsSignupActualcost = false;  
                $scope.first_payment_status = false;
                
                var process_fee_per = 0, process_fee_val = 0;
                var process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                var process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                
                if (parseFloat($scope.current_membership_manage.membership_signup_fee) > 0){
                    $scope.actual_signup_cost = parseFloat($scope.current_membership_manage.membership_signup_fee);
                    $scope.register_signup_cost =  $scope.actual_signup_cost;
                }else{
                    $scope.actual_signup_cost = 0;
                    $scope.register_signup_cost = 0;
                    $scope.signup_discount_show = false;
                }
                    
                if (parseFloat($scope.current_membership_manage.membership_fee) > 0){    
                    $scope.actual_membership_cost = parseFloat($scope.current_membership_manage.membership_fee);
                    $scope.register_membership_cost =  $scope.actual_membership_cost;
                }else{
                    $scope.actual_membership_cost = 0;
                    $scope.register_membership_cost =  0; 
                    $scope.membership_discount_show = false;
                } 
                
                $scope.current_processing_fee_type = $scope.current_membership_manage.membership_processing_fee_type;

                if (parseFloat($scope.signupdiscount) > 0) {
                    $scope.total_signup_amount_view = +$scope.register_signup_cost - +$scope.signupdiscount;
                    $scope.register_signup_discount_value = $scope.signupdiscount;
                }else{
                    $scope.total_signup_amount_view = +$scope.register_signup_cost;
                    $scope.register_signup_discount_value = 0;
                }                
                   
                if (parseFloat($scope.membershipdiscount) > 0) {
                    $scope.recurring_membership_amount_view = +$scope.actual_membership_cost - +$scope.membershipdiscount;
                    $scope.total_membership_amount_view = +$scope.register_membership_cost - +$scope.membershipdiscount;
                    $scope.register_membership_discount_value = $scope.membershipdiscount;
                }else{
                    $scope.recurring_membership_amount_view = +$scope.actual_membership_cost;
                    $scope.total_membership_amount_view = +$scope.register_membership_cost;
                    $scope.register_membership_discount_value = 0;
                }
                $scope.payment_amount = $scope.total_membership_amount_view;
                var amount = $scope.payment_amount;
                if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                    if (amount > 0) {
                        var process_fee1 = +amount + +((+amount * +process_fee_per) / 100) + +process_fee_val;
                        var process_fee = +((+process_fee1 * +process_fee_per) / 100) + +process_fee_val;
                        $scope.recurring_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.recurring_processing_fee_value = 0;
                    }
                } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1'){
                    if (amount > 0) {
                        var process_fee = +((+amount * +process_fee_per) / 100) + +process_fee_val;
                        $scope.recurring_processing_fee_value = (Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.recurring_processing_fee_value = 0;
                    }
                }
                
            
            
        if($scope.membership_structure === 'OE'){   
            var dup_date = $scope.safariiosdate($scope.total_payment_startdate);
            var act_date = $scope.safariiosdate($scope.actual_total_payment_startdate);
            $scope.prorate_includes_recurr_note = false; 
            $scope.first_payment_status = false;
            if ($scope.prorate_first_payment === 'Y' && $scope.mem_rec_frequency === 'M') { 
                    if($scope.delay_rec_payment_start_flg === 'Y'){
                        var ld = $scope.safariiosdate($scope.total_payment_startdate); 
                        var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                        $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / lastDay.getDate()) * (+lastDay.getDate() + 1 - ld.getDate());
                        $scope.total_membership_amount_view = 0;
                        $scope.total_reg_amt_for_check = +$scope.delay_membership_amount_view;
                        $scope.first_payment_amount = 0;
                        dup_date = $scope.addMonths(dup_date, 1);
                        var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                        $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }else{
                        var ld = $scope.safariiosdate($scope.actual_total_payment_startdate);
                        var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                        $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / lastDay.getDate()) * (+lastDay.getDate() + 1 - ld.getDate());
                        if ($scope.mem_fee_include_status === 'N'){
                            $scope.first_payment_status = true;
                            $scope.first_payment_amount = parseFloat($scope.total_membership_amount_view);
                            $scope.first_payment_startdate = $scope.recurringdatestring($scope.actual_total_payment_startdate);
                        }else{
                            $scope.first_payment_status = false;
                            $scope.first_payment_amount = 0;
                            $scope.first_payment_startdate = '';
                        }                       
                        
                        $scope.total_reg_amt_for_check = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                        if ($scope.total_reg_amt_for_check < 5) {
                            $scope.prorate_includes_recurr_note = true;
                            $scope.total_membership_amount_view = +$scope.total_membership_amount_view + +$scope.recurring_membership_amount_view;                             
                            if($scope.mem_fee_include_status === 'N'){                                
                                dup_date = $scope.addMonths(dup_date, 1);
                                var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);  
                            }else{                                
                                dup_date = $scope.addMonths(dup_date, 2);
                                var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                            }
                        }else{
                            dup_date = $scope.addMonths(dup_date, 1);
                            var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                            $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }
                    }                     
                    

                } else if ($scope.prorate_first_payment === 'Y' && $scope.mem_rec_frequency === 'B') {
                    if($scope.delay_rec_payment_start_flg === 'Y'){                          
                        if (dup_date.getDate() < 16) {
                            $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / 15) * (+16 - act_date.getDate()); 
                            $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16';
                        } else {   
                            var ld = $scope.safariiosdate($scope.total_payment_startdate);
                            var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                            if (dup_date.getDate() == 16)
                                $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view));
                            else
                                $scope.delay_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / (lastDay.getDate() - 15)) * (+lastDay.getDate() + 1 - act_date.getDate());
                            
                            dup_date = $scope.addMonths(dup_date, 1);
                            var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                            $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }
                        $scope.total_membership_amount_view = 0;
                        $scope.first_payment_amount = 0;
                    }else{                         
                        if (act_date.getDate() < 16) {
                            $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / 15) * (+16 - act_date.getDate());
                            if ($scope.mem_fee_include_status === 'N'){
                                $scope.first_payment_status = true;
                                $scope.first_payment_amount = parseFloat($scope.total_membership_amount_view);
                                $scope.first_payment_startdate = $scope.recurringdatestring($scope.actual_total_payment_startdate);
                            }else{
                                $scope.first_payment_status = false;
                                $scope.first_payment_amount = 0;
                                $scope.first_payment_startdate = '';
                            }
                        } else {
                            var ld = $scope.safariiosdate($scope.actual_total_payment_startdate);
                            var lastDay = new Date(ld.getFullYear(), ld.getMonth() + 1, 0);
                            if (dup_date.getDate() == 16)
                                $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view));
                            else
                                $scope.total_membership_amount_view = (parseFloat($scope.total_membership_amount_view) / (lastDay.getDate() - 15)) * (+lastDay.getDate() + 1 - act_date.getDate());
                            if ($scope.mem_fee_include_status === 'N'){
                                $scope.first_payment_status = true;
                                $scope.first_payment_amount = parseFloat($scope.total_membership_amount_view);
                                $scope.first_payment_startdate = $scope.recurringdatestring($scope.actual_total_payment_startdate);
                            }else{
                                $scope.first_payment_status = false;
                                $scope.first_payment_amount = 0;
                                $scope.first_payment_startdate = '';
                            }
                        }
                        
                        if (dup_date.getDate() < 16) {                         

                            $scope.total_reg_amt_for_check = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                            if ($scope.total_reg_amt_for_check < 5) {
                                $scope.prorate_includes_recurr_note = true;
                                $scope.total_membership_amount_view = +$scope.total_membership_amount_view + +$scope.recurring_membership_amount_view;
                                if($scope.mem_fee_include_status === 'N'){
                                    $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16'; 
                                }else{
                                    dup_date = $scope.addMonths(dup_date, 1);
                                    var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                                }
                            }else{
                                $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16'; 
                            }
                        } else {
                            $scope.total_reg_amt_for_check = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                            if ($scope.total_reg_amt_for_check < 5) {
                                $scope.prorate_includes_recurr_note = true;
                                $scope.total_membership_amount_view = +$scope.total_membership_amount_view + +$scope.recurring_membership_amount_view;
                                if($scope.mem_fee_include_status === 'N'){
                                    dup_date = $scope.addMonths(dup_date, 1);
                                    var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2); 
                                }else{
                                    dup_date = $scope.addMonths(dup_date, 1);
                                    var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-16';                             
                                }
                                
                            }else{
                                dup_date = $scope.addMonths(dup_date, 1);
                                var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                            }
                        }
                    }

                }
                
                var cus_curr_date = new Date();
                var cus_current_date = cus_curr_date.getFullYear() + '-' + ('0' + (cus_curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + cus_curr_date.getDate()).slice( - 2);
                if ($scope.mem_rec_frequency === 'A') {
                    dup_date.setFullYear(dup_date.getFullYear() + 1);
                    var dup = dup_date;
                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'W') {
                    dup_date.setDate(dup_date.getDate() + (+7));
                    var dup = dup_date;
    //                dup.setDate(dup.getDate() + (7 - dup.getDay()) % 7 + 1);
                    $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'C') {
                    if ($scope.cus_rec_freq_period_type === 'CM') {
                        dup_date = $scope.addMonths(dup_date, $scope.cus_rec_freq_period_val);
                        var dup = dup_date;
    //                    var dup = new Date(dup_date.getFullYear(), dup_date.getMonth() + 1, 1);
                        $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else {
                        dup_date.setDate(dup_date.getDate() + (+$scope.cus_rec_freq_period_val * 7));
                        var dup = dup_date;
    //                    dup.setDate(dup.getDate() + (7 - dup.getDay()) % 7 + 1);
                        $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'B' && $scope.prorate_first_payment !== 'Y') {
                    if (dup_date.getDate() < 16) {
                        $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-16';
                    } else {
                        var dup_date = $scope.addMonths(dup_date, 1);
                        var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                        $scope.recurring_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                } else if ($scope.mem_rec_frequency === 'M' && $scope.prorate_first_payment !== 'Y') {
                    dup_date = $scope.addMonths(dup_date, 1);                    
                    $scope.recurring_payment_startdate = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-' + ('0' + dup_date.getDate()).slice(-2);
                    if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                }else if($scope.mem_rec_frequency === 'N'){
                    $scope.recurring_payment_startdate = '';
                    if ($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) {   // Membership fee doesnot include on first payment
                        $scope.membership_fee_include = parseFloat($scope.total_membership_amount_view);                       
                        $scope.first_payment_status = true;
                        $scope.first_payment_amount = $scope.membership_fee_include;
                    }else{
                        $scope.first_payment_amount = 0;
                        $scope.first_payment_status = false;
                    }
                }
                
                if ($scope.mem_rec_frequency !== 'N')
                    $scope.recurring_payment_startdate_view = $scope.recurringdatestring($scope.recurring_payment_startdate);
                    $scope.total_payment_startdate_view = $scope.recurringdatestring($scope.total_payment_startdate);
                    
                
                if (($scope.mem_fee_include_status === 'N' && (new Date(cus_current_date) < new Date($scope.actual_total_payment_startdate))) || $scope.delay_rec_payment_start_flg === 'Y') {   // Membership fee doesnot include on first payment
                    $scope.total_membership_amount_view = 0;
                }
                
            }else if($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' ){
                var dup_date = $scope.safariiosdate($scope.total_payment_startdate);
                var act_date = $scope.safariiosdate($scope.actual_total_payment_startdate);
                var number_of_payments = +$scope.membership_no_of_payments;
                var intervalType = $scope.mem_rec_frequency;
                var billing_option = $scope.membership_billing_option;
                var recurring_amnt = (+$scope.total_membership_amount_view - +$scope.membership_billing_deposit_amount)/(+$scope.membership_no_of_payments);
                var recurring_pro_fee;
                if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                    if (recurring_amnt > 0) {
                        var process_fee1 = +(parseFloat(recurring_amnt)) + +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        recurring_pro_fee = 0;
                    }
                } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1'){
                    if (recurring_amnt > 0) {
                        var process_fee = +((parseFloat(recurring_amnt) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        recurring_pro_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        recurring_pro_fee = 0;
                    } 
                }
                
                
                $scope.pay = [];
                $scope.recurrent = {};                
                
                if(parseFloat(recurring_amnt) > 0 && billing_option === 'PP'){
                    if($scope.membershippaymentform)
                    $scope.membershippaymentform.$setValidity("membershippaymentform", true);
                    if (intervalType === 'M') {             //monthly
                        for (var i = 0; i < number_of_payments; i++) {
                            $scope.recurrent = {};
                            if (i === 0) {
                                $scope.recurrent['date'] = $scope.total_payment_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            } else {
                                dup_date = $scope.addMonths(dup_date, 1);
//                                var dup = new Date(dup_date.getFullYear(), dup_date.getMonth(), 1);
                                var date = dup_date.getFullYear() + '-' + ('0' + (dup_date.getMonth() + 1)).slice(-2) + '-' + ('0' + dup_date.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            }
                        }
                    } else if (intervalType === 'W') {           //weekly
                        for (var i = 0; i < number_of_payments; i++) {
                            $scope.recurrent = {};
                             if (i === 0) {
                                $scope.recurrent['date'] = $scope.total_payment_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            } else {
                                dup_date.setDate(dup_date.getDate() + (+7));
                                var dup =  dup_date;
                                var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            }
                        }
                    } else if (intervalType === 'B') {           //Bi-weekly
                        for (var i = 0; i < number_of_payments; i++) {
                            $scope.recurrent = {};
                             if (i === 0) {
                                $scope.recurrent['date'] = $scope.total_payment_startdate;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            } else {
                                dup_date.setDate(dup_date.getDate() + (+14));
                                var dup =  dup_date;
                                var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                                $scope.recurrent['date'] = date;
                                $scope.recurrent['amount'] = recurring_amnt;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = recurring_pro_fee;
                                $scope.pay.push($scope.recurrent);
                            }
                        }
                    } 
                    if($scope.pay.length > 0){
                        $scope.recurring_payment_startdate = $scope.pay[0].date;
                        $scope.payment_amount = $scope.pay[0].amount;
                        if(parseFloat($scope.payment_amount) >= 5){
                            if($scope.membershippaymentform)
                            $scope.membershippaymentform.$setValidity("membershippaymentform", true);
                        }else{
                            if($scope.membershippaymentform)
                            $scope.membershippaymentform.$setValidity("membershippaymentform", false);
                        }
                    }
                }else{
                    $scope.pay = [];
                    $scope.recurring_payment_startdate = '';
                }
                
            } else if ($scope.membership_structure === 'SE') {
                    var dup_date = $scope.safariiosdate($scope.total_payment_startdate);
                    var act_date = $scope.safariiosdate($scope.actual_total_payment_startdate);
                    var specific_payment_frequency = $scope.specific_payment_frequency;
                    var billing_option = $scope.membership_billing_option;
                    $scope.TotalMembershipFeeSE = 0;
                    $scope.pay = [];
                    $scope.recurrent = {};
                    
                        if ($scope.membershippaymentform)
                            $scope.membershippaymentform.$setValidity("membershippaymentform", true);
                        if (specific_payment_frequency === 'PM') {             //per monthly
                            var membership_firstDay = $scope.SEdateFormat($scope.payment_start_date);
                            var membership_lastDay = $scope.SEdateFormat($scope.specific_end_date);
                            var se_amount = 0;
                            var se_processing_fee = 0;
                            $scope.month_array = [];
                            $scope.total_count_array = [];
                            for (date = membership_firstDay; date <= membership_lastDay; date.setMonth(date.getMonth() + 1, 1)) {
                                var count = 0, exclude_count = 0;
                                var y = date.getFullYear(), m = date.getMonth(), da = date.getDate(), hr = date.getHours(), min = date.getMinutes();
                                var startdate = new Date(y, m, da, hr, min);
                                var enddate = new Date(y, m + 1, 0, hr, min);
                                if (membership_lastDay < enddate) {
                                    enddate = membership_lastDay;
                                }
                              
                                count = $scope.startandenddate(startdate, enddate);
                                $scope.total_count_array.push(count);
                                if ($scope.exclude_from_billing_flag === 'Y' && $scope.mem_billing_exclude_days.length > 0) {
                                    for (i = 0; i < $scope.mem_billing_exclude_days.length; i++) {
                                        var exclude_start_date = $scope.SEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_startdate);
                                        var exclude_end_date = $scope.SEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_enddate);
                                        var final_exclude_startdate = "";
                                        var final_exclude_enddate = "";  
                                    if ((exclude_start_date.getMonth() === date.getMonth()) && (exclude_start_date.getFullYear() === date.getFullYear())) {
                                             var y = exclude_start_date.getFullYear(), m = exclude_start_date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                                             var end_of_month = new Date(y, m + 1, 0, hr, min);
                                           if (date <= exclude_start_date || date <= exclude_end_date) {
                                                if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                    var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                    final_exclude_startdate = new Date(y, m, day, hr, min);
                                                     if (exclude_end_date > end_of_month) { //check for exclude_end_date for date picked start date
                                                         final_exclude_enddate = end_of_month;
                                                     }else{
                                                         final_exclude_enddate = exclude_end_date; 
                                                     }
                                                } else {
                                                    final_exclude_startdate = exclude_start_date;
                                                    if (exclude_end_date > end_of_month) {
                                                        final_exclude_enddate = end_of_month;
                                                    } else {
                                                        final_exclude_enddate = exclude_end_date;
                                                    }
                                                }
                                            }
                                        } else if ((exclude_end_date.getMonth() === date.getMonth()) && (exclude_end_date.getFullYear() === date.getFullYear())) {
                                            if (date <= exclude_end_date || date <= exclude_start_date) {
                                                if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                    var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                    final_exclude_startdate = new Date(y, m, day, hr, min);
                                                    final_exclude_enddate = exclude_end_date;
                                                } else {
                                                    var y = exclude_end_date.getFullYear(), m = exclude_end_date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                                                    var startdate_of_month = new Date(y, m, 1, hr, min);
                                                    final_exclude_enddate = exclude_end_date;
                                                    if (exclude_start_date < startdate_of_month) {
                                                        final_exclude_startdate = startdate_of_month;
                                                    } else {
                                                        final_exclude_startdate = exclude_start_date;
                                                    }
                                                }
                                            }
                                        } else if (((exclude_start_date.getMonth() < date.getMonth() && exclude_start_date.getFullYear() === date.getFullYear()) || (exclude_start_date.getFullYear() < date.getFullYear())) && ((exclude_end_date.getMonth() > date.getMonth() && exclude_end_date.getFullYear() === date.getFullYear()) || (exclude_end_date.getFullYear() > date.getFullYear()))) {
                                           var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day =date.getDate();
                                            if (date > exclude_start_date && date < exclude_end_date) {//for date picking in registration
                                                    final_exclude_startdate = new Date(y, m, day, hr, min);
                                                    final_exclude_enddate = new Date(y, m + 1, 0, hr, min);
                                                } else {
                                            final_exclude_startdate = new Date(y, m, 1, hr, min);
                                            final_exclude_enddate = new Date(y, m + 1, 0, hr, min);
                                        }
                                      }
                                        if (final_exclude_startdate !== '' && final_exclude_enddate !== '') {
                                            exclude_count = +exclude_count + +$scope.startandenddate(final_exclude_startdate, final_exclude_enddate);

                                        }
                                    }
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1] - +exclude_count;
                                    count = $scope.total_count_array[$scope.total_count_array.length - 1];
                                   
                                } else {
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1];
                                }
                                var se_start_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                                $scope.recurrent = {};
                                se_amount = $scope.SEMembershipFee(count);
                                se_processing_fee = $scope.SEProcessingFee(se_amount);
                                $scope.recurrent['date'] = se_start_date;
                                $scope.recurrent['amount'] = se_amount;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = se_processing_fee;
                                $scope.pay.push($scope.recurrent);                      
                            }
                             
                        } else if (specific_payment_frequency === 'PW') {           //per weekly
                            var membership_firstDay = $scope.SEdateFormat($scope.payment_start_date);
                            var membership_lastDay = $scope.SEdateFormat($scope.specific_end_date);
                            var se_amount = 0, se_processing_fee = 0;
                            $scope.total_count_array = [];
                            for (date = membership_firstDay; date <= membership_lastDay; date.setDate(date.getDate() + 7)) {
                                var start_date = "";
                                var end_date, da;
                                var count, exclude_count = 0;
                                var first = date.getDate() - (date.getDay() - 1); // start of the week
                                var last = first + 6;//last day of week  
                                var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes();
                                var firstday_of_week = new Date(y, m, first, hr, min);
                                var lastday_of_week = new Date(y, m, last, hr, min);
                                if (date <= membership_firstDay) {
                                    start_date = membership_firstDay;
                                } else {
                                    start_date = firstday_of_week;
                                }
                                if (lastday_of_week >= membership_lastDay) {
                                    end_date = membership_lastDay;
                                } else {
                                    end_date = lastday_of_week;
                                }
                                var y = start_date.getFullYear(), m = start_date.getMonth(), da = start_date.getDate(), shr = date.getHours(), smin = date.getMinutes();
                                var startdate = new Date(y, m, da, shr, smin);
                                var ey = end_date.getFullYear(), em = end_date.getMonth(), eda = end_date.getDate(), ehr = date.getHours(), emin = date.getMinutes();
                                var enddate = new Date(ey, em, eda, ehr, emin);
                                count = $scope.startandenddate(startdate, enddate);
                                $scope.total_count_array.push(count);

                                if ($scope.exclude_from_billing_flag === 'Y' && $scope.mem_billing_exclude_days.length > 0) {
                                    for (i = 0; i < $scope.mem_billing_exclude_days.length; i++) {
                                        var exclude_start_date = $scope.SEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_startdate);
                                        var exclude_end_date = $scope.SEdateFormat($scope.mem_billing_exclude_days[i].billing_exclude_enddate);
                                        var final_exclude_startdate = "";
                                        var final_exclude_enddate = "";
//                                        if ((exclude_start_date.getMonth() === date.getMonth()) && (exclude_start_date.getFullYear() === date.getFullYear())) {
                                            if (date <= exclude_end_date || date <= exclude_start_date) {
                                                if (exclude_start_date >= firstday_of_week && exclude_start_date <= lastday_of_week) {
                                                    if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                        var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                        final_exclude_startdate = new Date(y, m, day, hr, min);
                                                        if(exclude_end_date > lastday_of_week){ //check for exclude_end_date for date picked start date
                                                            final_exclude_enddate = lastday_of_week;
                                                        }else{
                                                            final_exclude_enddate = exclude_end_date;
                                                        }
                                                    } else {
                                                        final_exclude_startdate = exclude_start_date;
                                                        if (exclude_end_date > lastday_of_week) {
                                                            final_exclude_enddate = lastday_of_week;
                                                        } else {
                                                            final_exclude_enddate = exclude_end_date;
                                                        }
                                                    }
                                                } else if (exclude_end_date >= firstday_of_week && exclude_end_date <= lastday_of_week) {
                                                    if (date > exclude_start_date && date <= exclude_end_date) {//for date picking in registration
                                                        var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                        final_exclude_startdate = new Date(y, m, day, hr, min);
                                                        final_exclude_enddate = exclude_end_date;
                                                    } else {
                                                        final_exclude_enddate = exclude_end_date;
                                                        if (exclude_start_date > firstday_of_week) {
                                                            final_exclude_startdate = exclude_start_date;
                                                        } else {
                                                            final_exclude_startdate = firstday_of_week;
                                                        }
                                                    }
                                                } else if (exclude_start_date < firstday_of_week && exclude_end_date > lastday_of_week) {
                                                    if (date > exclude_start_date && date < exclude_end_date) {//for date picking in registration
                                                        var y = date.getFullYear(), m = date.getMonth(), hr = date.getHours(), min = date.getMinutes(), day = date.getDate();
                                                        final_exclude_startdate = new Date(y, m, day, hr, min);
                                                            final_exclude_enddate = lastday_of_week;
                                                        } else {
                                                    final_exclude_startdate = firstday_of_week;
                                                    final_exclude_enddate = lastday_of_week;
                                                    
                                                }
                                            }
                                            }
                                        if (final_exclude_startdate !== '' && final_exclude_enddate !== '') {
                                            exclude_count = parseInt(exclude_count) + parseInt($scope.startandenddate(final_exclude_startdate, final_exclude_enddate));
                                        }
                                    }
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1] - +exclude_count;
                                    count = $scope.total_count_array[$scope.total_count_array.length - 1];
                                } else {
                                    $scope.total_count_array[$scope.total_count_array.length - 1] = +$scope.total_count_array[$scope.total_count_array.length - 1];
                                }
                                var se_start_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                                if (date.getDay() !== 1) {
                                    date = firstday_of_week;
                                }
                                $scope.recurrent = {};
                                se_amount = $scope.SEMembershipFee(count);
                                se_processing_fee = $scope.SEProcessingFee(se_amount);
                                $scope.recurrent['date'] = se_start_date;
                                $scope.recurrent['amount'] = se_amount;
                                $scope.recurrent['type'] = 'R';
                                $scope.recurrent['processing_fee'] = se_processing_fee;
                                $scope.pay.push($scope.recurrent);                            
                            }
//                           
                        }
                        
                        for (i = 0; i < $scope.pay.length; i++) {//wepay less than 5 calculation
                            $scope.pay[i].prorate_flag = "F";
                            while ($scope.pay[i].amount < 5) {
                                if ($scope.pay[i].amount === 0) {
                                    $scope.pay.splice(i, 1);
                                    i--;
                                    break;
                                }
                                var j = i + 1;
                                if ($scope.pay[j] === undefined)
                                    break;
                                $scope.pay[i].amount = $scope.pay[i].amount + $scope.pay[j].amount;
                                $scope.pay[i].processing_fee = $scope.SEProcessingFee($scope.pay[i].amount);
                                $scope.pay[i].prorate_flag = "T";
                                $scope.pay.splice(j, 1);
                        }
                    }
                    
                    if (specific_payment_frequency === 'PM' && $scope.pay.length > 0) {  //first and last dates prorate check  
                        var first_date = $scope.SEdateFormat($scope.pay[0].date);
                        var last_date = $scope.SEdateFormat($scope.specific_end_date);
                        var ye = last_date.getFullYear(), me = last_date.getMonth(), hre = last_date.getHours(), mine = last_date.getMinutes();
                        var end_of_months = new Date(ye, me + 1, 0, hre, mine);
                        if ($scope.pay[0].prorate_flag !== "T") {
                            $scope.pay[0].prorate_flag = (first_date.getDate() === 1) ? "F" : "T";
                        }
                        if ($scope.pay[$scope.pay.length - 1].prorate_flag !== "T" && $scope.pay.length > 1) {
                            $scope.pay[$scope.pay.length - 1].prorate_flag = (last_date.getDate() === end_of_months.getDate()) ? "F" : "T";
                        }
                    } else if (specific_payment_frequency === 'PW' && $scope.pay.length > 0) {
                        var first_date = $scope.SEdateFormat($scope.pay[0].date);
                        var last_date = $scope.SEdateFormat($scope.specific_end_date);
                        var checkprorate_start, checkprorate_end = '';
                        $scope.selected_days = $scope.billing_days.split('');
                        if ($scope.pay[0].prorate_flag !== "T") {    //week start
                            for (i = 0; i < $scope.selected_days.length; i++) {
                                if ($scope.selected_days[i] === "1") {
                                    checkprorate_start = i;
                                    break;
                                }
                            }
                            $scope.days_check_start = first_date.getDay() - 1;
                            $scope.days_check_start = ($scope.days_check_start === -1) ? '6' : $scope.days_check_start;
                            $scope.pay[0].prorate_flag = (checkprorate_start === $scope.days_check_start) ? "F" : "T";
                        }
                        if ($scope.pay[$scope.pay.length - 1].prorate_flag !== "T" && $scope.pay.length > 1) {     //week end
                            for (j = $scope.selected_days.length - 1; j >= 0; j--) {
                                if ($scope.selected_days[j] === "1") {
                                    checkprorate_end = j;
                                    break;
                                }
                            }
                            $scope.days_check_last = last_date.getDay() - 1;
                            $scope.days_check_last = ($scope.days_check_last === -1) ? '6' : $scope.days_check_last;
                            $scope.pay[$scope.pay.length - 1].prorate_flag = (checkprorate_end === $scope.days_check_last) ? "F" : "T";
                        }
                    }
                    
                        
                        if(parseInt($scope.pay.length) > 1 && $scope.pay !== undefined && $scope.pay !== ''){ 
                            if ($scope.pay[$scope.pay.length - 1].amount < 5) {
                                $scope.pay[$scope.pay.length - 2].amount += $scope.pay[$scope.pay.length - 1].amount;
                                $scope.pay[$scope.pay.length - 2].processing_fee =  $scope.SEProcessingFee($scope.pay[$scope.pay.length - 2].amount);
                                $scope.pay[$scope.pay.length - 2].prorate_flag="T";
                                $scope.pay.splice($scope.pay.length - 1, 1);
                            }
                        }
                      
                            var se_mem_amount = 0;
                            for (i = 0; i < $scope.pay.length; i++) {
                                se_mem_amount = parseFloat(se_mem_amount) + parseFloat($scope.pay[i].amount);
                            }
                            $scope.TotalMembershipFeeSE = se_mem_amount;

                        if (billing_option === 'PF') {
                            $scope.payment_amount = se_mem_amount;
                            $scope.pay = [];
                            $scope.recurring_payment_startdate = '';
                        }
                        
                        if ($scope.pay.length > 0) {
                            $scope.recurring_payment_startdate = $scope.pay[0].date;
                            $scope.payment_amount = $scope.pay[0].amount;
                            if (parseFloat($scope.payment_amount) >= 5) {
                                if ($scope.membershippaymentform)
                                    $scope.membershippaymentform.$setValidity("membershippaymentform", true);
                            } else {
                                if ($scope.membershippaymentform)
                                    $scope.membershippaymentform.$setValidity("membershippaymentform", false);
                            }
                        }else {
                            $scope.pay = [];
                            $scope.recurring_payment_startdate = '';
                        }
                }


                if(parseFloat($scope.total_membership_amount_view) < 0 ){
                    $scope.exceedsMembershipActualcost = true;
                    return false;
                }else if(parseFloat($scope.total_signup_amount_view) < 0){
                    $scope.exceedsSignupActualcost = true;
                    return false;
                }else{
                    $scope.exceedsMembershipActualcost = false;
                    $scope.exceedsSignupActualcost = false;
                }
                   
                if ($scope.total_membership_amount_view > 0 && $scope.total_signup_amount_view > 0) {                       
                    $scope.total_amount_view = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                    $scope.total_reg_amount_view = +$scope.total_membership_amount_view + +$scope.total_signup_amount_view;
                }else if($scope.total_membership_amount_view > 0 && $scope.total_signup_amount_view == 0 ){
                    $scope.total_amount_view = +$scope.total_membership_amount_view;
                    $scope.total_reg_amount_view = +$scope.total_membership_amount_view;
                    $scope.membership_discount_show = true;
                }else if($scope.total_signup_amount_view > 0 && $scope.total_membership_amount_view == 0){
                    $scope.total_amount_view = +$scope.total_signup_amount_view;
                    $scope.total_reg_amount_view = +$scope.total_signup_amount_view;
                    $scope.signup_discount_show = true;
                }
                
                if($scope.membership_structure === 'NC' || $scope.membership_structure === 'C'){ 
                    var curr_date = new Date();
                    var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                    var payment_date = new Date($scope.total_payment_startdate);
                    var actual_payment_date = new Date($scope.actual_total_payment_startdate);
                    if($scope.membership_billing_option === 'PP' && $scope.pay.length > 0) {
                        if((+actual_payment_date ===  +payment_date) && (+server_curr_date === +actual_payment_date)){
                            $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.membership_billing_deposit_amount+ +$scope.payment_amount;
                            var amount = $scope.total_amount_view;
                            $scope.final_payment_amount = amount; 
                            $scope.pay.shift();
                            $scope.recurring_payment_startdate = ($scope.pay.length > 0) ? $scope.pay[0].date : "";
                        }else{
                            $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.membership_billing_deposit_amount;
                            var amount = $scope.total_amount_view;
                            $scope.final_payment_amount = amount; 
                        }
                    }else{
                        var amount = $scope.total_amount_view;
                        $scope.final_payment_amount = amount;
                    }
                }else if(  $scope.membership_structure === 'SE'){
                    var curr_date = new Date();
                    var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                    var payment_date = new Date($scope.total_payment_startdate);
                    var actual_payment_date = new Date($scope.actual_total_payment_startdate);
                    if($scope.membership_billing_option === 'PP' && $scope.pay.length > 0){ 
                        if((+actual_payment_date ===  +payment_date) && (+server_curr_date === +actual_payment_date)){
                            $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.pay[0].amount;
                            var amount = $scope.total_amount_view;
                            $scope.final_payment_amount = amount; 
                            $scope.pay.shift();
                        }else{
                            if ($scope.mem_fee_include_status === 'Y') {
                                $scope.total_amount_view = +$scope.total_signup_amount_view + $scope.pay[0].amount;
                                $scope.pay.splice(0, 1);
                                var amount = $scope.total_amount_view;
                                $scope.final_payment_amount = amount;
                            } else {
                                $scope.total_amount_view = +$scope.total_signup_amount_view;
                                var amount = $scope.total_amount_view;
                                $scope.final_payment_amount = amount;
                            }
                        }
                        $scope.recurring_payment_startdate = ($scope.pay.length > 0) ? $scope.pay[0].date : "";
                    }else{
                        if ($scope.mem_fee_include_status === 'N' && (new Date(server_curr_date) < new Date($scope.actual_total_payment_startdate))) {   // Membership fee doesnot include on first payment
                            $scope.total_amount_view = +$scope.total_signup_amount_view;
                            var amount = $scope.total_amount_view;
                            $scope.final_payment_amount = amount;
                            $scope.membership_fee_include = $scope.payment_amount;                       
                            $scope.first_payment_status = true;
                            $scope.first_payment_amount = $scope.membership_fee_include;
                        }else{
                            $scope.total_amount_view = +$scope.total_signup_amount_view + +$scope.payment_amount;
                            var amount = $scope.total_amount_view;
                            $scope.final_payment_amount = amount;
                            $scope.first_payment_amount = 0;
                            $scope.first_payment_status = false;
                        }
                    }
                }else{
                    var amount = $scope.total_amount_view;
                    $scope.final_payment_amount = amount;
                }
                
                if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                    if (amount > 0) {
                        var process_fee1 = +(parseFloat(amount)) + +((parseFloat(amount) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        $scope.processing_fee_value = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.processing_fee_value = 0;
                    }
                    $scope.total_amount_view = +parseFloat(amount) + +$scope.processing_fee_value;
                } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1'){
                    if (amount > 0) {
                        var process_fee = +((parseFloat(amount) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        $scope.processing_fee_value = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.processing_fee_value = 0;
                    } 
                    $scope.total_amount_view = amount;
                }    
                
                $scope.pay_infull_processingfee = $scope.processing_fee_value;
                
                if (parseFloat($scope.total_amount_view) == parseFloat('0')) {
                    if (($scope.membership_structure === 'OE' || $scope.membership_structure === 'SE') && ((parseFloat($scope.delay_membership_amount_view) > 0) || (parseFloat($scope.first_payment_amount) > 0))) {
                        $scope.showmempaymentview = true;
                        $scope.zerocostview = true;
                    } else if(($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' || $scope.membership_structure === 'SE') && (parseFloat($scope.payment_amount) > 0)){
                        $scope.showmempaymentview = true;
                        $scope.zerocostview = true;
                    }else {
                        $scope.compareDiscountCost = false;
                        $scope.zerocostview = false;
                        $scope.showmempaymentview = false;
                        $scope.mem_selectedcardnumber = '';
                    }
                } else {
                    $scope.compareDiscountCost = true;
                    $scope.zerocostview = true;
                    if ($scope.carddetails.length == 0)
                        $scope.showmempaymentview = true;
                }
                   
            };
            
            //For selecting existing card of a user
            $scope.selectRankDetail = function (ind) { 
                    $scope.rankindex = ind;
                    $scope.rank_id = parseInt($scope.rank_details[$scope.rankindex].rank_id);
                    $scope.rank_name = $scope.rank_details[$scope.rankindex].rank_name;
                    $scope.rank_updated = true;
            };
            
            //For selecting existing card of a user
            $scope.selectMembershipCardDetail = function () { 
                    $scope.cardnumber = '';
                    $scope.ccmonth = '';
                    $scope.ccyear = '';
                    $scope.cvv = '';
                    $scope.country = '';
                    $scope.postal_code = '';   
                
                if ($scope.mem_selectedcardnumber === 'Add New credit card') {                    
                    $scope.showmempaymentview = true;                    
                    $scope.selectedcardtype = 'new';
                    $scope.credit_card_id = '';
                    $scope.credit_card_status = '';
                    $scope.credit_card_name = '';
                    $scope.credit_card_expiration_month = '';
                    $scope.credit_card_expiration_year = '';
                    $scope.postal_code = '';
                } else {
                    $scope.cardindex = $scope.mem_selectedcardnumber;
                    if($scope.stripe_enabled) {
                        $scope.payment_method_id = $scope.carddetails[$scope.cardindex].payment_method_id;
                        $scope.stripe_customer_id = $scope.carddetails[$scope.cardindex].stripe_customer_id;
                    }else{ 
                        $scope.credit_card_id = parseInt($scope.carddetails[$scope.cardindex].credit_card_id);
                        $scope.credit_card_status = $scope.carddetails[$scope.cardindex].credit_card_status;
                        $scope.credit_card_name = $scope.carddetails[$scope.cardindex].credit_card_name;
                        $scope.credit_card_expiration_month = $scope.carddetails[$scope.cardindex].credit_card_expiration_month;
                        $scope.credit_card_expiration_year = $scope.carddetails[$scope.cardindex].credit_card_expiration_year;
                        $scope.postal_code = $scope.carddetails[$scope.cardindex].postal_code;
                    }
                    $scope.showmempaymentview = false;
                    $scope.selectedcardtype = 'old';
                }
            };
            
            $scope.$watch(function () {return $scope.studentselected;}, function (newVal, oldVal) {
                $scope.participant_studentname = newVal;
                if ($scope.participant_studentname.student_name !== '' || $scope.participant_studentname.student_name !== undefined) { 
                    if ($scope.studentlist.length > 0) {
                        for (i = 0; i < $scope.studentlist.length; i++) {                            
                            if ($scope.participant_studentname.student_name === $scope.studentlist[i].student_name && $scope.studentlist[i].buyer_first_name !==null && $scope.studentlist[i].buyer_last_name !==null && $scope.participant_studentname.stud_index === $scope.studentlist[i].stud_index) {
                                $scope.student_id = $scope.studentlist[i].student_id;
                                $scope.student_name = $scope.studentlist[i].student_name;
                                $scope.reg_col_value[0] = $scope.studentlist[i].reg_col_1;
                                $scope.reg_col_value[1] = $scope.studentlist[i].reg_col_2;
                                $scope.reg_col_value[2] = $scope.studentlist[i].reg_col_3;                                
                                var bchoose_date = $scope.reg_col_value[2].split("-");
                                document.getElementsByClassName("dob_month")[0].value = bchoose_date[1]; 
                                document.getElementsByClassName("dob_day")[0].value = bchoose_date[2]; 
                                document.getElementsByClassName("dob_year")[0].value = bchoose_date[0]; 
                                document.getElementById("column2").value = $scope.reg_col_value[2]; 
                                $scope.firstname = $scope.studentlist[i].buyer_first_name;                                
                                $scope.lastname = $scope.studentlist[i].buyer_last_name;
                                $scope.email = $scope.studentlist[i].buyer_email;
                                $scope.phone = $scope.studentlist[i].buyer_phone;
                                $scope.postal_code = $scope.studentlist[i].buyer_postal_code;
                                $scope.participant_Streetaddress = $scope.studentlist[i].participant_street; 
                                $scope.participant_City = $scope.studentlist[i].participant_city;
                                $scope.participant_State = $scope.studentlist[i].participant_state;
                                $scope.participant_Zip = $scope.studentlist[i].participant_zip;
                                $scope.participant_Country = $scope.studentlist[i].participant_country;
                                if ($scope.studentlist[i].card_details === undefined || $scope.studentlist[i].card_details.length == 0){
                                    $scope.showcardselection = false;
                                    if(parseFloat($scope.final_payment_amount) <= 0){
                                        if ($scope.membership_structure === 'OE' && ((parseFloat($scope.delay_membership_amount_view) > 0) || (parseFloat($scope.first_payment_amount) > 0))) {
                                            $scope.showmempaymentview = true;
                                        } else if(($scope.membership_structure === 'NC' || $scope.membership_structure === 'C'  || $scope.membership_structure === 'SE') && (parseFloat($scope.payment_amount) > 0)){
                                            $scope.showmempaymentview = true;
                                        }else {
                                            $scope.showmempaymentview = false;
                                        }
                                    }else{
                                        $scope.showmempaymentview = true;
                                    }
                                    $scope.carddetails = [];
                                }else{
                                    $scope.showcardselection = true;
                                    $scope.showmempaymentview = false;
                                    $scope.carddetails = $scope.studentlist[i].card_details;
                                }                                
                                return false;
                            } else {
                                $scope.student_id = 0;
                                $scope.student_name = $scope.participant_studentname;
                                $scope.reg_col_value[0] = '';
                                $scope.reg_col_value[1] = '';
                                $scope.reg_col_value[2] = '';
                                $scope.reg_col_value[3] = '';
                                $scope.reg_col_value[4] = '';
                                $scope.reg_col_value[5] = '';
                                $scope.reg_col_value[6] = '';
                                $scope.reg_col_value[7] = '';
                                $scope.reg_col_value[8] = '';
                                $scope.reg_col_value[9] = '';                                
                                document.getElementsByClassName("dob_month")[0].value = ""; 
                                document.getElementsByClassName("dob_day")[0].value = ""; 
                                document.getElementsByClassName("dob_year")[0].value = "";  
                                document.getElementById("column2").value = "";
                                $scope.firstname = '';
                                $scope.lastname = '';
                                $scope.email = '';
                                $scope.phone = '';
                                $scope.postal_code = '';
                                $scope.carddetails = [];
                                $scope.showcardselection = false;
                                if(parseFloat($scope.final_payment_amount) <= 0){
                                    if ($scope.membership_structure === 'OE' && ((parseFloat($scope.delay_membership_amount_view) > 0) || (parseFloat($scope.first_payment_amount) > 0))) {
                                        $scope.showmempaymentview = true;
                                    } else if(($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' || $scope.membership_structure === 'SE') && (parseFloat($scope.payment_amount) > 0)){
                                        $scope.showmempaymentview = true;
                                    }else {
                                        $scope.showmempaymentview = false;
                                    }
                                }else{
                                $scope.showmempaymentview = true;
                                } 
                                $scope.cardnumber = '';
                                $scope.ccmonth = '';
                                $scope.ccyear = '';
                                $scope.cvv = '';
                                $scope.country = '';
                                $scope.mem_selectedcardnumber = '';
                                $scope.participant_Streetaddress = '';
                                $scope.participant_City = ''; 
                                $scope.participant_State = ''; 
                                $scope.participant_Zip = ''; 
                                $scope.participant_Country = ''; 
                            }
                        }
                    }
                    
                } else {
                    $scope.student_id = 0;
                    $scope.student_name = $scope.participant_studentname;
                    $scope.reg_col_value[0] = '';
                    $scope.reg_col_value[1] = '';
                    $scope.reg_col_value[2] = '';
                    $scope.reg_col_value[3] = '';
                    $scope.reg_col_value[4] = '';
                    $scope.reg_col_value[5] = '';
                    $scope.reg_col_value[6] = '';
                    $scope.reg_col_value[7] = '';
                    $scope.reg_col_value[8] = '';
                    $scope.reg_col_value[9] = ''; 
                    if(document.getElementsByClassName("dob_month")[0] && document.getElementById("column2")){
                        document.getElementsByClassName("dob_month")[0].value = ""; 
                        document.getElementsByClassName("dob_day")[0].value = ""; 
                        document.getElementsByClassName("dob_year")[0].value = "";
                        document.getElementById("column2").value = "";
                    }
                    $scope.firstname = '';
                    $scope.lastname = '';
                    $scope.email = '';
                    $scope.phone = '';
                    $scope.postal_code = '';
                    $scope.carddetails = [];
                    $scope.showcardselection = false;
                    if(parseFloat($scope.final_payment_amount) <= 0){
                        if ($scope.membership_structure === 'OE' && ((parseFloat($scope.delay_membership_amount_view) > 0) || (parseFloat($scope.first_payment_amount) > 0))) {
                            $scope.showmempaymentview = true;
                        } else if(($scope.membership_structure === 'NC' || $scope.membership_structure === 'C'  || $scope.membership_structure === 'SE') && (parseFloat($scope.payment_amount) > 0)){
                            $scope.showmempaymentview = true;
                        }else {
                            $scope.showmempaymentview = false;
                        }
                    }else{
                    $scope.showmempaymentview = true;
                    }                   
                    $scope.cardnumber = '';
                    $scope.ccmonth = '';
                    $scope.ccyear = '';
                    $scope.cvv = '';
                    $scope.country = '';
                    $scope.mem_selectedcardnumber = '';
                    $scope.participant_Streetaddress = '';
                    $scope.participant_City = ''; 
                    $scope.participant_State = ''; 
                    $scope.participant_Zip = ''; 
                    $scope.participant_Country = ''; 
                }
               
            });
            
            //Registration of a membership with cost
            $scope.membershipcheckout = function(credit_card_id, state, user_first_name, user_last_name, email, phone, postal_code,country){
                $('#progress-full').show();
                var curr_date = new Date();
                var reg_current_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);                
                if (!user_first_name) {
                    user_first_name = $scope.firstname;
                }
                if (!user_last_name) {
                    user_last_name = $scope.lastname;
                }
                for(var i=0; i<$scope.current_membership_reg_columns.length; i++){
                    if($scope.reg_col_value[i] && $scope.reg_col_value[i]!== undefined && $scope.reg_col_value[i]!== ''){
                        
                    }else{
                        $scope.reg_col_value[i]='';
                    }
                }
                
                var participant_birth_date = document.getElementById("column2").value;
                
                var delay_recurring_payment_start_date = '';
                var membership_start_date = $scope.payment_start_date;
                if($scope.mem_rec_frequency==='N' && $scope.membership_structure === 'OE'){
                    delay_recurring_payment_start_date = $scope.total_payment_startdate;
                }else if($scope.membership_structure === 'OE' && $scope.delay_rec_payment_start_flg === 'Y'){
                    delay_recurring_payment_start_date = $scope.delayed_recurring_payment_startdate;
                    $scope.payment_start_date = delay_recurring_payment_start_date;
                }else if($scope.membership_billing_option==='PF' && ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' || $scope.membership_structure === 'SE')){
                    delay_recurring_payment_start_date = $scope.payment_start_date;
                }else{
                    delay_recurring_payment_start_date = $scope.recurring_payment_startdate;
                }
                
                var exclude_days = [];
                if ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' || $scope.membership_structure === 'SE' ) {
                       $scope.final_pay = $scope.pay;
                       if($scope.membership_structure === 'SE'){
                            exclude_days = $scope.mem_billing_exclude_days;
                        }else{
                            exclude_days = [];
                        }
                }else{
                       $scope.final_pay = [];
                       exclude_days = [];
                }
                
                $scope.initial_payment_amount = (Math.round(($scope.final_payment_amount + 0.00001) * 100) / 100).toFixed(2);
                var fee=0;
                fee = $scope.pay_infull_processingfee;
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'wepayMembershipCheckout',
                    data: {
                        company_id:  $localStorage.company_id,
                        membership_id:  $scope.membershipcategory_id,  
                        membership_option_id:  $scope.membershipoption_id, 
                        studentid: $scope.student_id,
                        membership_category_title: $scope.membershipcategorytitle,
                        membership_title:  $scope.membershipoptiontitle,
                        membership_desc: $scope.membershipoptiontitle,
                        buyer_first_name: user_first_name,
                        buyer_last_name: user_last_name,
                        email : email,
                        phone : phone,
                        postal_code : postal_code,
                        country : country,
                        cc_id : credit_card_id,
                        cc_state : state,
                        payment_amount: $scope.payment_amount,
                        processing_fee_type: $scope.current_processing_fee_type,
                        processing_fee: fee,
                        membership_fee: $scope.memfee,
                        membership_fee_disc : $scope.membershipdiscount,
                        signup_fee: $scope.memsignupfee,
                        signup_fee_disc : $scope.signupdiscount,
                        initial_payment : $scope.initial_payment_amount, //registration payment amount 
                        first_payment : $scope.first_payment_amount, //Membership start date payment amount
                        payment_frequency : $scope.mem_rec_frequency,
                        payment_start_date : $scope.payment_start_date,
                        membership_start_date : membership_start_date, // Membership start date
                        recurring_start_date : $scope.recurring_payment_startdate,
                        prorate_first_payment_flg : $scope.prorate_first_payment,
                        delay_recurring_payment_start_flg : $scope.delay_rec_payment_start_flg,
                        delay_recurring_payment_start_date : delay_recurring_payment_start_date, 
                        initial_payment_include_membership_fee : $scope.mem_fee_include_status,
                        delay_recurring_payment_amount : $scope.delay_membership_amount_view,
                        custom_recurring_frequency_period_type : $scope.cus_rec_freq_period_type,
                        custom_recurring_frequency_period_val : $scope.cus_rec_freq_period_val,
                        membership_structure : $scope.membership_structure,
                        no_of_classes : $scope.membership_no_of_classes,
                        billing_options_expiration_date : $scope.membership_expiry_date,
                        billing_options : $scope.membership_billing_option, 
                        billing_options_deposit_amount : $scope.membership_billing_deposit_amount,
                        billing_options_no_of_payments : $scope.membership_no_of_payments,
                        payment_array: $scope.final_pay,
                        reg_col1: $scope.reg_col_value[0],
                        reg_col2: $scope.reg_col_value[1],
                        reg_col3: participant_birth_date,
                        reg_col4: $scope.reg_col_value[3],
                        reg_col5: $scope.reg_col_value[4],
                        reg_col6: $scope.reg_col_value[5],
                        reg_col7: $scope.reg_col_value[6],
                        reg_col8: $scope.reg_col_value[7],
                        reg_col9: $scope.reg_col_value[8],
                        reg_col10: $scope.reg_col_value[9],
                        upgrade_status: $localStorage.upgrade_status,
                        participant_street: $scope.participant_Streetaddress,
                        participant_city: $scope.participant_City,
                        participant_state: $scope.participant_State,
                        participant_zip: $scope.participant_Zip,
                        participant_country: $scope.participant_Country,
                        rank_id: $scope.rank_id,
                        rank_name: $scope.rank_name,
                        registration_type: $scope.edit_mem_transfer ? 'transfer':'purchase',
                        old_membership_reg_id: $scope.edit_mem_transfer ? $scope.membership_transfer_reg_id :'',
                        reg_current_date: reg_current_date,
                        exclude_days_array: exclude_days,
                        program_start_date:$scope.specific_start_date,
                        program_end_date:$scope.specific_end_date,
                        exclude_from_billing_flag: $scope.exclude_from_billing_flag 
                    },
                
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        if(response.data.transfer === 'Y'){
                            $("#memtransfermessageModal").modal('show');
                            $("#memtransfermessagetitle").text('Message');
                            $("#memtransfermessagecontent").text(response.data.msg); 
                            $scope.membership_detail = response.data.reg_details.msg;
                            $localStorage.buyerName = $scope.membership_detail.buyer_name;
                            $localStorage.participantName = $scope.membership_detail.participant_name;
                            $localStorage.currentMembershipregid = $scope.membership_detail.membership_registration_id;
                            $localStorage.page_from = 'members_active';
                            $localStorage.membershippagetype = '';
                        }else{                        
                            $("#pushmessageModal").modal('show');
                            $("#pushmessagetitle").text('Message');
                            $("#pushmessagecontent").text(response.data.msg);                    
                            $scope.onetimecheckout = 0;
                            $scope.mem_selectedcardnumber = '';
                            $scope.getmembershipdetails('P');
                            $scope.getStudentList();
                            $scope.addMembershipParticipant();
                            $scope.current_membership_manage_options = response.data.membership_details.msg.membership_options;
                            for(i=0;i< $scope.current_membership_manage_options.length;i++){
                                if($scope.current_membership_manage_options[i].membership_option_id === $scope.membershipoption_id ){
                                   $scope.current_membership_manage = $scope.current_membership_manage_options[i];                               
                                }
                            }
                            $scope.childmembershipview = false;
                            $scope.mainmembershipview = false;
                            $scope.showmembershipsublist = true;
                            $scope.addmemparticipantview = false;
                            $localStorage.addmembershipfrommemberstab = false;
                            $scope.addfromlist = false;
                            $scope.managechildmembership($scope.current_membership_manage);
                        }
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $scope.onetimecheckout = 0;
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        if(response.data.error){
                            $("#pushmessagetitle").text(response.data.error);
                            $("#pushmessagecontent").text(response.data.error_description);
                        }else{
                            $("#pushmessagetitle").text('Message');
                            $("#pushmessagecontent").text(response.data.msg);
                        }
                        if(response.data.student_id){
                            $scope.student_id = response.data.student_id;
                            $scope.getStudentList();
                        }
                    }   
                }, function (response) {
                    $scope.onetimecheckout = 0;
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            //Registration of a membership for zero cost
            $scope.membershipzerocheckout = function(user_first_name, user_last_name, email, phone){
                $('#progress-full').show();
                var curr_date = new Date();
                var reg_current_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);  
                if (!user_first_name) {
                    user_first_name = $scope.firstname;
                }
                if (!user_last_name) {
                    user_last_name = $scope.lastname;
                }
                for(var i=0; i<$scope.current_membership_reg_columns.length; i++){
                    if($scope.reg_col_value[i] && $scope.reg_col_value[i]!== undefined && $scope.reg_col_value[i]!== ''){
                        
                    }else{
                        $scope.reg_col_value[i]='';
                    }
                }
                 var participant_birth_date = document.getElementById("column2").value;
                 var membership_start_date = $scope.payment_start_date;
                 $scope.final_pay = []; 
                 var exclude_days = [];
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'wepayMembershipCheckout',
                    data: {
                        company_id:  $localStorage.company_id,
                        membership_id:  $scope.membershipcategory_id,  
                        membership_option_id:  $scope.membershipoption_id, 
                        studentid: $scope.student_id,
                        membership_category_title: $scope.membershipcategorytitle,
                        membership_title:  $scope.membershipoptiontitle,
                        membership_desc: $scope.membershipoptiontitle,
                        buyer_first_name: user_first_name,
                        buyer_last_name: user_last_name,
                        email : email,
                        phone : phone,
                        postal_code : '',
                        country : '',
                        cc_id : '',
                        cc_state : '',
                        payment_amount: '0',
                        processing_fee_type: $scope.current_processing_fee_type,
                        processing_fee: '0',
                        membership_fee: '0',
                        membership_fee_disc : $scope.membershipdiscount,
                        signup_fee: $scope.memsignupfee,
                        signup_fee_disc : $scope.signupdiscount,
                        initial_payment : '0', //have to work
                        payment_frequency : $scope.mem_rec_frequency,
                        payment_start_date : $scope.payment_start_date, //have to work
                        membership_start_date : membership_start_date, // Membership start date
                        recurring_start_date : '',
                        prorate_first_payment_flg : $scope.prorate_first_payment,
                        delay_recurring_payment_start_flg : $scope.delay_rec_payment_start_flg,
                        delay_recurring_payment_start_date : '', //have to work
                        custom_recurring_frequency_period_type : $scope.cus_rec_freq_period_type,
                        custom_recurring_frequency_period_val : $scope.cus_rec_freq_period_val,
                        membership_structure : $scope.membership_structure,
                        no_of_classes : $scope.membership_no_of_classes,
                        billing_options_expiration_date : $scope.membership_expiry_date,
                        billing_options : $scope.membership_billing_option, 
                        billing_options_deposit_amount : $scope.membership_billing_deposit_amount,
                        billing_options_no_of_payments : $scope.membership_no_of_payments,
                        payment_array: $scope.final_pay,
                        reg_col1: $scope.reg_col_value[0],
                        reg_col2: $scope.reg_col_value[1],
                        reg_col3: participant_birth_date,
                        reg_col4: $scope.reg_col_value[3],
                        reg_col5: $scope.reg_col_value[4],
                        reg_col6: $scope.reg_col_value[5],
                        reg_col7: $scope.reg_col_value[6],
                        reg_col8: $scope.reg_col_value[7],
                        reg_col9: $scope.reg_col_value[8],
                        reg_col10: $scope.reg_col_value[9],
                        upgrade_status: $localStorage.upgrade_status,
                        participant_street: $scope.participant_Streetaddress,
                        participant_city: $scope.participant_City,
                        participant_state: $scope.participant_State,
                        participant_zip: $scope.participant_Zip,
                        participant_country: $scope.participant_Country,
                        rank_id: $scope.rank_id,
                        rank_name: $scope.rank_name,
                        registration_type: $scope.edit_mem_transfer ? 'transfer':'purchase',
                        old_membership_reg_id: $scope.edit_mem_transfer ? $scope.membership_transfer_reg_id :'',
                        reg_current_date: reg_current_date,
                        exclude_days_array: exclude_days,
                        program_start_date:$scope.specific_start_date,
                        program_end_date:$scope.specific_end_date,
                        exclude_from_billing_flag: $scope.exclude_from_billing_flag
                    },
                
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();                        
                        if(response.data.transfer === 'Y'){
                            $("#memtransfermessageModal").modal('show');
                            $("#memtransfermessagetitle").text('Message');
                            $("#memtransfermessagecontent").text(response.data.msg);
                            $scope.membership_detail = response.data.reg_details.msg;
                            $localStorage.buyerName = $scope.membership_detail.buyer_name;
                            $localStorage.participantName = $scope.membership_detail.participant_name;
                            $localStorage.currentMembershipregid = $scope.membership_detail.membership_registration_id;
                            $localStorage.page_from = 'members_active';
                            $localStorage.membershippagetype = '';
                        }else{                        
                            $("#pushmessageModal").modal('show');
                            $("#pushmessagetitle").text('Message');
                            $("#pushmessagecontent").text(response.data.msg);  
                            $scope.onetimecheckout = 0;
                            $scope.mem_selectedcardnumber = '';
                            $scope.getmembershipdetails('P');
                            $scope.getStudentList();
                            $scope.addMembershipParticipant();
                            $scope.current_membership_manage_options = response.data.membership_details.msg.membership_options;
                            for(i=0;i< $scope.current_membership_manage_options.length;i++){
                                if($scope.current_membership_manage_options[i].membership_option_id === $scope.membershipoption_id ){
                                   $scope.current_membership_manage = $scope.current_membership_manage_options[i];                              
                                }
                            }
                            $scope.childmembershipview = false;
                            $scope.mainmembershipview = false;
                            $scope.showmembershipsublist = true;
                            $scope.addmemparticipantview = false; 
                            $localStorage.addmembershipfrommemberstab = false;
                            $scope.addfromlist = false;
                            $scope.managechildmembership($scope.current_membership_manage);
                        }
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $scope.onetimecheckout = 0;
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        if(response.data.error){
                            $("#pushmessagetitle").text(response.data.error);
                            $("#pushmessagecontent").text(response.data.error_description);
                        }else{
                            $("#pushmessagetitle").text('Message');
                            $("#pushmessagecontent").text(response.data.msg);
                        }
                        if(response.data.student_id){
                            $scope.student_id = response.data.student_id;
                            $scope.getStudentList();
                        }
                    }   
                }, function (response) {
                    $scope.onetimecheckout = 0;
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            
            //Registration of a membership with cost
            $scope.stripeMembershipcheckout = function(user_first_name, user_last_name, email, phone, postal_code,country){
                $('#progress-full').show();
                var curr_date = new Date();
                var reg_current_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);                
                if (!user_first_name) {
                    user_first_name = $scope.firstname;
                }
                if (!user_last_name) {
                    user_last_name = $scope.lastname;
                }
                for(var i=0; i<$scope.current_membership_reg_columns.length; i++){
                    if($scope.reg_col_value[i] && $scope.reg_col_value[i]!== undefined && $scope.reg_col_value[i]!== ''){
                        
                    }else{
                        $scope.reg_col_value[i]='';
                    }
                }
                
                var participant_birth_date = document.getElementById("column2").value;
                
                var delay_recurring_payment_start_date = '';
                var membership_start_date = $scope.payment_start_date;
                if($scope.mem_rec_frequency==='N' && $scope.membership_structure === 'OE'){
                    delay_recurring_payment_start_date = $scope.total_payment_startdate;
                }else if($scope.membership_structure === 'OE' && $scope.delay_rec_payment_start_flg === 'Y'){
                    delay_recurring_payment_start_date = $scope.delayed_recurring_payment_startdate;
                    $scope.payment_start_date = delay_recurring_payment_start_date;
                }else if($scope.membership_billing_option==='PF' && ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' || $scope.membership_structure === 'SE')){
                    delay_recurring_payment_start_date = $scope.payment_start_date;
                }else{
                    delay_recurring_payment_start_date = $scope.recurring_payment_startdate;
                }
                
                var exclude_days = [];
                if ($scope.membership_structure === 'NC' || $scope.membership_structure === 'C' || $scope.membership_structure === 'SE' ) {
                       $scope.final_pay = $scope.pay;
                       if($scope.membership_structure === 'SE'){
                            exclude_days = $scope.mem_billing_exclude_days;
                        }else{
                            exclude_days = [];
                        }
                }else{
                       $scope.final_pay = [];
                       exclude_days = [];
                }
                
                $scope.initial_payment_amount = (Math.round(($scope.final_payment_amount + 0.00001) * 100) / 100).toFixed(2);
                var fee=0;
                fee = $scope.pay_infull_processingfee;
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'stripeMembershipCheckout',
                    data: {
                        company_id:  $localStorage.company_id,
                        membership_id:  $scope.membershipcategory_id,  
                        membership_option_id:  $scope.membershipoption_id, 
                        studentid: $scope.student_id,
                        membership_category_title: $scope.membershipcategorytitle,
                        membership_title:  $scope.membershipoptiontitle,
                        membership_desc: $scope.membershipoptiontitle,
                        buyer_first_name: user_first_name,
                        buyer_last_name: user_last_name,
                        email : email,
                        phone : phone,
                        postal_code : postal_code,
                        country : country,
                        payment_amount: $scope.payment_amount,
                        processing_fee_type: $scope.current_processing_fee_type,
                        processing_fee: fee,
                        membership_fee: $scope.memfee,
                        membership_fee_disc : $scope.membershipdiscount,
                        signup_fee: $scope.memsignupfee,
                        signup_fee_disc : $scope.signupdiscount,
                        initial_payment : $scope.initial_payment_amount, //registration payment amount 
                        first_payment : $scope.first_payment_amount, //Membership start date payment amount
                        payment_frequency : $scope.mem_rec_frequency,
                        payment_start_date : $scope.payment_start_date,
                        membership_start_date : membership_start_date, // Membership start date
                        recurring_start_date : $scope.recurring_payment_startdate,
                        prorate_first_payment_flg : $scope.prorate_first_payment,
                        delay_recurring_payment_start_flg : $scope.delay_rec_payment_start_flg,
                        delay_recurring_payment_start_date : delay_recurring_payment_start_date, 
                        initial_payment_include_membership_fee : $scope.mem_fee_include_status,
                        delay_recurring_payment_amount : $scope.delay_membership_amount_view,
                        custom_recurring_frequency_period_type : $scope.cus_rec_freq_period_type,
                        custom_recurring_frequency_period_val : $scope.cus_rec_freq_period_val,
                        membership_structure : $scope.membership_structure,
                        no_of_classes : $scope.membership_no_of_classes,
                        billing_options_expiration_date : $scope.membership_expiry_date,
                        billing_options : $scope.membership_billing_option, 
                        billing_options_deposit_amount : $scope.membership_billing_deposit_amount,
                        billing_options_no_of_payments : $scope.membership_no_of_payments,
                        payment_array: $scope.final_pay,
                        reg_col1: $scope.reg_col_value[0],
                        reg_col2: $scope.reg_col_value[1],
                        reg_col3: participant_birth_date,
                        reg_col4: $scope.reg_col_value[3],
                        reg_col5: $scope.reg_col_value[4],
                        reg_col6: $scope.reg_col_value[5],
                        reg_col7: $scope.reg_col_value[6],
                        reg_col8: $scope.reg_col_value[7],
                        reg_col9: $scope.reg_col_value[8],
                        reg_col10: $scope.reg_col_value[9],
                        upgrade_status: $localStorage.upgrade_status,
                        participant_street: $scope.participant_Streetaddress,
                        participant_city: $scope.participant_City,
                        participant_state: $scope.participant_State,
                        participant_zip: $scope.participant_Zip,
                        participant_country: $scope.participant_Country,
                        rank_id: $scope.rank_id,
                        rank_name: $scope.rank_name,
                        registration_type: $scope.edit_mem_transfer ? 'transfer':'purchase',
                        old_membership_reg_id: $scope.edit_mem_transfer ? $scope.membership_transfer_reg_id :'',
                        reg_current_date: reg_current_date,
                        exclude_days_array: exclude_days,
                        program_start_date:$scope.specific_start_date,
                        program_end_date:$scope.specific_end_date,
                        exclude_from_billing_flag: $scope.exclude_from_billing_flag,
                        payment_method_id: ($scope.mem_payment_method == 'CC' ) ? $scope.payment_method_id : '',
                        payment_method : $scope.mem_payment_method, //CC - credit card
                        cc_type: ($scope.selectedcardtype === 'new') ? 'N' : 'E',
                        stripe_customer_id: ($scope.selectedcardtype === 'old') ? $scope.stripe_customer_id : ''
                    },
                
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        if(response.data.transfer === 'Y'){
                            $("#memtransfermessageModal").modal('show');
                            $("#memtransfermessagetitle").text('Message');
                            $("#memtransfermessagecontent").text(response.data.msg); 
                            $scope.membership_detail = response.data.reg_details.msg;
                            $localStorage.buyerName = $scope.membership_detail.buyer_name;
                            $localStorage.participantName = $scope.membership_detail.participant_name;
                            $localStorage.currentMembershipregid = $scope.membership_detail.membership_registration_id;
                            $localStorage.page_from = 'members_active';
                            $localStorage.membershippagetype = '';
                        }else{                        
                            $("#pushmessageModal").modal('show');
                            $("#pushmessagetitle").text('Message');
                            $("#pushmessagecontent").text(response.data.msg);                    
                            $scope.onetimecheckout = 0;
                            $scope.mem_selectedcardnumber = '';
                            $scope.getmembershipdetails('P');
                            $scope.getStudentList();
                            $scope.addMembershipParticipant();
                            $scope.current_membership_manage_options = response.data.membership_details.msg.membership_options;
                            for(i=0;i< $scope.current_membership_manage_options.length;i++){
                                if($scope.current_membership_manage_options[i].membership_option_id === $scope.membershipoption_id ){
                                   $scope.current_membership_manage = $scope.current_membership_manage_options[i];                               
                                }
                            }
                            $scope.childmembershipview = false;
                            $scope.mainmembershipview = false;
                            $scope.showmembershipsublist = true;
                            $scope.addmemparticipantview = false;
                            $localStorage.addmembershipfrommemberstab = false;
                            $scope.addfromlist = false;
                            $scope.managechildmembership($scope.current_membership_manage);
                        }
                        $scope.stripe_card.clear();
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.onetimecheckout = 0;
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        if(response.data.error){
                            $("#pushmessagetitle").text(response.data.error);
                            $("#pushmessagecontent").text(response.data.error_description);
                        }else{
                            $("#pushmessagetitle").text('Message');
                            $("#pushmessagecontent").text(response.data.msg);
                        }
                        if(response.data.student_id){
                            $scope.student_id = response.data.student_id;
                            $scope.getStudentList();
                        }
                    }   
                }, function (response) {
                    $scope.onetimecheckout = 0;
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            //Registration of a membership for zero cost
            $scope.stripeMembershipzerocheckout = function(user_first_name, user_last_name, email, phone){
                $('#progress-full').show();
                var curr_date = new Date();
                var reg_current_date = curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2);  
                if (!user_first_name) {
                    user_first_name = $scope.firstname;
                }
                if (!user_last_name) {
                    user_last_name = $scope.lastname;
                }
                for(var i=0; i<$scope.current_membership_reg_columns.length; i++){
                    if($scope.reg_col_value[i] && $scope.reg_col_value[i]!== undefined && $scope.reg_col_value[i]!== ''){
                        
                    }else{
                        $scope.reg_col_value[i]='';
                    }
                }
                 var participant_birth_date = document.getElementById("column2").value;
                 var membership_start_date = $scope.payment_start_date;
                 $scope.final_pay = []; 
                 var exclude_days = [];
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'stripeMembershipCheckout',
                    data: {
                        company_id:  $localStorage.company_id,
                        membership_id:  $scope.membershipcategory_id,  
                        membership_option_id:  $scope.membershipoption_id, 
                        studentid: $scope.student_id,
                        membership_category_title: $scope.membershipcategorytitle,
                        membership_title:  $scope.membershipoptiontitle,
                        membership_desc: $scope.membershipoptiontitle,
                        buyer_first_name: user_first_name,
                        buyer_last_name: user_last_name,
                        email : email,
                        phone : phone,
                        postal_code : '',
                        country : '',
                        cc_id : '',
                        cc_state : '',
                        payment_amount: '0',
                        processing_fee_type: $scope.current_processing_fee_type,
                        processing_fee: '0',
                        membership_fee: '0',
                        membership_fee_disc : $scope.membershipdiscount,
                        signup_fee: $scope.memsignupfee,
                        signup_fee_disc : $scope.signupdiscount,
                        initial_payment : '0', //have to work
                        payment_frequency : $scope.mem_rec_frequency,
                        payment_start_date : $scope.payment_start_date, //have to work
                        membership_start_date : membership_start_date, // Membership start date
                        recurring_start_date : '',
                        prorate_first_payment_flg : $scope.prorate_first_payment,
                        delay_recurring_payment_start_flg : $scope.delay_rec_payment_start_flg,
                        delay_recurring_payment_start_date : '', //have to work
                        custom_recurring_frequency_period_type : $scope.cus_rec_freq_period_type,
                        custom_recurring_frequency_period_val : $scope.cus_rec_freq_period_val,
                        membership_structure : $scope.membership_structure,
                        no_of_classes : $scope.membership_no_of_classes,
                        billing_options_expiration_date : $scope.membership_expiry_date,
                        billing_options : $scope.membership_billing_option, 
                        billing_options_deposit_amount : $scope.membership_billing_deposit_amount,
                        billing_options_no_of_payments : $scope.membership_no_of_payments,
                        payment_array: $scope.final_pay,
                        reg_col1: $scope.reg_col_value[0],
                        reg_col2: $scope.reg_col_value[1],
                        reg_col3: participant_birth_date,
                        reg_col4: $scope.reg_col_value[3],
                        reg_col5: $scope.reg_col_value[4],
                        reg_col6: $scope.reg_col_value[5],
                        reg_col7: $scope.reg_col_value[6],
                        reg_col8: $scope.reg_col_value[7],
                        reg_col9: $scope.reg_col_value[8],
                        reg_col10: $scope.reg_col_value[9],
                        upgrade_status: $localStorage.upgrade_status,
                        participant_street: $scope.participant_Streetaddress,
                        participant_city: $scope.participant_City,
                        participant_state: $scope.participant_State,
                        participant_zip: $scope.participant_Zip,
                        participant_country: $scope.participant_Country,
                        rank_id: $scope.rank_id,
                        rank_name: $scope.rank_name,
                        registration_type: $scope.edit_mem_transfer ? 'transfer':'purchase',
                        old_membership_reg_id: $scope.edit_mem_transfer ? $scope.membership_transfer_reg_id :'',
                        reg_current_date: reg_current_date,
                        exclude_days_array: exclude_days,
                        program_start_date:$scope.specific_start_date,
                        program_end_date:$scope.specific_end_date,
                        exclude_from_billing_flag: $scope.exclude_from_billing_flag
                    },
                
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();                        
                        if(response.data.transfer === 'Y'){
                            $("#memtransfermessageModal").modal('show');
                            $("#memtransfermessagetitle").text('Message');
                            $("#memtransfermessagecontent").text(response.data.msg);
                            $scope.membership_detail = response.data.reg_details.msg;
                            $localStorage.buyerName = $scope.membership_detail.buyer_name;
                            $localStorage.participantName = $scope.membership_detail.participant_name;
                            $localStorage.currentMembershipregid = $scope.membership_detail.membership_registration_id;
                            $localStorage.page_from = 'members_active';
                            $localStorage.membershippagetype = '';
                        }else{                        
                            $("#pushmessageModal").modal('show');
                            $("#pushmessagetitle").text('Message');
                            $("#pushmessagecontent").text(response.data.msg);  
                            $scope.onetimecheckout = 0;
                            $scope.mem_selectedcardnumber = '';
                            $scope.getmembershipdetails('P');
                            $scope.getStudentList();
                            $scope.addMembershipParticipant();
                            $scope.current_membership_manage_options = response.data.membership_details.msg.membership_options;
                            for(i=0;i< $scope.current_membership_manage_options.length;i++){
                                if($scope.current_membership_manage_options[i].membership_option_id === $scope.membershipoption_id ){
                                   $scope.current_membership_manage = $scope.current_membership_manage_options[i];                              
                                }
                            }
                            $scope.childmembershipview = false;
                            $scope.mainmembershipview = false;
                            $scope.showmembershipsublist = true;
                            $scope.addmemparticipantview = false; 
                            $localStorage.addmembershipfrommemberstab = false;
                            $scope.addfromlist = false;
                            $scope.managechildmembership($scope.current_membership_manage);
                        }
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.onetimecheckout = 0;
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        if(response.data.error){
                            $("#pushmessagetitle").text(response.data.error);
                            $("#pushmessagecontent").text(response.data.error_description);
                        }else{
                            $("#pushmessagetitle").text('Message');
                            $("#pushmessagecontent").text(response.data.msg);
                        }
                        if(response.data.student_id){
                            $scope.student_id = response.data.student_id;
                            $scope.getStudentList();
                        }
                    }   
                }, function (response) {
                    $scope.onetimecheckout = 0;
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
             //Membership drag and drop on live membership listing
            $scope.dragmembershipCallback = function (drag_type, ind) {
                if (drag_type === 'start') {
                    $scope.old_location_id = ind;
                    $scope.start_membership_id = $scope.liveMembershipList[ind].membership_id;
                } else if (drag_type === 'end') {
                    setTimeout(function () {
                        for(var i=0;i<$scope.liveMembershipList.length;i++){ 
                            if($scope.liveMembershipList[i].membership_id === $scope.start_membership_id){
                                $scope.new_location_id = i;
                            }
                        }
                        
                        if ($scope.old_location_id !== $scope.new_location_id) {
                            var sort_membership_id, category_status, membership_sort_order, previous_sort_order, next_sort_order;
                            if ($scope.new_location_id === 0) {
                                sort_membership_id = $scope.liveMembershipList[$scope.new_location_id].membership_id;
                                category_status = $scope.liveMembershipList[$scope.new_location_id].category_status;
                                previous_sort_order = parseFloat($scope.liveMembershipList[$scope.new_location_id + 1].category_sort_order);
                                membership_sort_order = previous_sort_order + 1;
                                $scope.sortordermembershipupdate(sort_membership_id, membership_sort_order, category_status);
                            } else if ($scope.new_location_id === $scope.liveMembershipList.length - 1) {
                                sort_membership_id = $scope.liveMembershipList[$scope.new_location_id].membership_id;
                                category_status = $scope.liveMembershipList[$scope.new_location_id].category_status;
                                membership_sort_order = parseFloat($scope.liveMembershipList[$scope.new_location_id - 1].category_sort_order) / 2;
                                $scope.sortordermembershipupdate(sort_membership_id, membership_sort_order, category_status);
                            } else {
                                sort_membership_id = $scope.liveMembershipList[$scope.new_location_id].membership_id;
                                category_status = $scope.liveMembershipList[$scope.new_location_id].category_status;
                                previous_sort_order = parseFloat($scope.liveMembershipList[$scope.new_location_id + 1].category_sort_order);
                                next_sort_order = parseFloat($scope.liveMembershipList[$scope.new_location_id - 1].category_sort_order);
                                membership_sort_order = (previous_sort_order + next_sort_order) / 2;                               
                                $scope.sortordermembershipupdate(sort_membership_id, membership_sort_order, category_status);
                            }
                        }else{
                           $('#progress-full').hide();
                        }

                    }, 1000);
                }
            };

             //updating membership sort order after drag and drop on live membership listing
            $scope.sortordermembershipupdate = function(membership_id,sort_id, category_status){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url+'updateMembershipCategorySortOrder',
                    data: {
                        "sort_id": sort_id,
                        "membership_id": membership_id,
                        "company_id": $localStorage.company_id,
                        "list_type":category_status
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if(response.data.status == 'Success'){
                        $('#progress-full').hide();
                        setTimeout(function() { $route.reload(); },200);
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            //Membership option drag and drop on listing
            $scope.dragmemOptionCallback = function (drag_type, ind) {
                if (drag_type === 'start') {
                    $scope.old_location_id = ind;
                    $scope.start_membership_option_id = $scope.childmembershiparray[ind].membership_option_id;
                } else if (drag_type === 'end') {
                    setTimeout(function () {
                        for(var i=0;i<$scope.childmembershiparray.length;i++){ 
                            if($scope.childmembershiparray[i].membership_option_id === $scope.start_membership_option_id){
                                $scope.new_location_id = i;
                            }
                        }
                        
                        if ($scope.old_location_id !== $scope.new_location_id) {
                            var sort_membership_option_id, membership_option_sort_order, previous_sort_order, next_sort_order;
                            if ($scope.new_location_id === 0) {
                                sort_membership_option_id = $scope.childmembershiparray[$scope.new_location_id].membership_option_id;
                                previous_sort_order = parseFloat($scope.childmembershiparray[$scope.new_location_id + 1].option_sort_order);
                                membership_option_sort_order = previous_sort_order + 1;
                                $scope.sortordermemoptionupdate(sort_membership_option_id, membership_option_sort_order);
                            } else if ($scope.new_location_id === $scope.childmembershiparray.length - 1) {
                                sort_membership_option_id = $scope.childmembershiparray[$scope.new_location_id].membership_option_id;
                                membership_option_sort_order = parseFloat($scope.childmembershiparray[$scope.new_location_id - 1].option_sort_order) / 2;
                                $scope.sortordermemoptionupdate(sort_membership_option_id, membership_option_sort_order);
                            } else {
                                sort_membership_option_id = $scope.childmembershiparray[$scope.new_location_id].membership_option_id;
                                previous_sort_order = parseFloat($scope.childmembershiparray[$scope.new_location_id + 1].option_sort_order);
                                next_sort_order = parseFloat($scope.childmembershiparray[$scope.new_location_id - 1].option_sort_order);
                                membership_option_sort_order = (previous_sort_order + next_sort_order) / 2;                               
                                $scope.sortordermemoptionupdate(sort_membership_option_id, membership_option_sort_order);
                            }
                        }

                    }, 1000);
                }
            };

             //updating membership option sort order after drag and drop on listing
            $scope.sortordermemoptionupdate = function(membership_option_id,sort_id){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url+'updateMembershipOptionsSortOrder',
                    data: {
                        "sort_id": sort_id,
                        "membership_option_id": membership_option_id,
                        "membership_id": $scope.membershipcategory_id,
                        "company_id": $localStorage.company_id
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if(response.data.status == 'Success'){
                        $('#progress-full').hide();
                        $localStorage.currentpage = "editmembership";
                        $scope.membership_details = response.data.msg;
                        if($scope.membership_details.membership_options.length > 0){
                            $scope.childmembershiparray = $scope.membership_details.membership_options;
                        } else {
                            $scope.childmembershiparray = "";
                        }
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.paidmeminfo = function () {
                var msg = "This amount only shows what the user has paid toward the cost of the Membership.<br> It does not show any applicable passed on fees.";
                $("#pushmessageModal").modal('show');
                $("#pushmessagetitle").text('Info');
                $("#pushmessagecontent").html(msg);
            };
        
        
         $scope.resetForm = function () { //Reset Credit Card Form
            $scope.editMembershipPaymentMethod_reg_id = '';
            $scope.firstname = '';
            $scope.lastname = '';
            $scope.email = '';
            $scope.phone = '';
            $scope.cardnumber = '';
            $scope.$parent.ccmonth = '';
            $scope.$parent.ccyear = '';
            $scope.cvv = '';
            $scope.$parent.country = '';
            $scope.postal_code = '';
            $scope.participant_Streetaddress = '';
            $scope.participant_City = ''; 
            $scope.participant_State = ''; 
            $scope.participant_Zip = ''; 
            $scope.participant_Country = ''; 
            $scope.paymentform1.$setPristine();
            $scope.paymentform1.$setUntouched();                
        };

            $scope.getRankDetails = function (rank_id, current_tab, m_id, m_rg_id, event) {
                $scope.event_compile = event;
                $scope.evntpara = angular.element(event.target);
                if (current_tab == 'R') {
                    $scope.las_adv = angular.element(event.target).parent().next('.col8')
                } else if (current_tab == 'P') {
                    $scope.las_adv = angular.element(event.target).parent().next('.col_hold11');
                }
                $scope.current_tab=current_tab;
                $scope.event_Rank_updt = event.target;
                $scope.m_id = m_id;
                $scope.membership_reg_id = m_rg_id;
                $('#progress-full').show();
                $scope.rank_array = [];
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getAllRankDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "membership_id": $scope.m_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.rank_array = response.data.msg;
                        $('#progress-full').hide();
                        $scope.existing_rank_array = [];
                        $scope.existing_rank_id = '';
                        $scope.current_tab = current_tab;
                        $scope.existing_rank_id = rank_id;
                        $scope.mem_status = current_tab;
                        for (var j = 0; j < $scope.rank_array.length; j++) {
                            $scope.individual_rank = {};
                            if ($scope.rank_array[j].membership_id == $scope.m_id) {
                                $scope.individual_rank['membership_rank_id'] = $scope.rank_array[j].membership_rank_id;
                                $scope.individual_rank['rank_name'] = $scope.rank_array[j].rank_name;
                                $scope.individual_rank['membership_id'] = $scope.rank_array[j].membership_id;
                                $scope.existing_rank_array.push($scope.individual_rank);
                            }
                        }
                        if ($scope.mem_status === 'R' || $scope.mem_status === 'P') {
                            $("#updateRankModal").modal('show');
                            $("#updateRanktitle").text('Update Rank Details');
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.last_advncedays_count = function(date) {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                  dd = '0' + dd
                }
                if (mm < 10) {
                  mm = '0' + mm
                }
                today = yyyy + '-' + mm + '-' + dd;
                $scope.today = today;
                var date2 = new Date(today);
                var date1 = new Date(date);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
                if($scope.dayDifference == 0){
                    return "Today";
                }else{
                    return $scope.dayDifference+" days ago";
                }
            }
            
            $scope.rankupdateChange = function (membership_rank_id) {
                $scope.rankupdatestatus = false;
                if (membership_rank_id === $scope.existing_rank_id) {
                    $scope.rankupdatestatus = false;
                } else {
                    $scope.rankupdatestatus = true;
                }
            };
            
            $scope.updateRankCancel = function () {
                $scope.existing_rank_array = [];
                $scope.existing_rank_id = "";
                $scope.mem_rank_details = "";
                $scope.mem_status = "";
                $scope.new_adv_date = "";
                $scope.lst_adv_date = "";
                $scope.rankupdatestatus = false;
            };
            
             $scope.showRankEditConfirmModel = function (membership_rank_id) { // SHOW RANK EDIT FORM
                
                $scope.rank_id='';
                $scope.rank_name ='';
                for (var j = 0; j < $scope.rank_array.length; j++) {
                    if ($scope.rank_array[j].membership_rank_id === membership_rank_id) {
                        $scope.rank_id = membership_rank_id;
                        $scope.rank_name = $scope.rank_array[j].rank_name;
                    }
                }
                $("#rankActionModal").modal('show');
                $("#rankActiontitle").text('Confirmation');
                $("#rankActioncontent").text("Are you sure want to Update Membership Rank?");
            };

        $scope.ActivelastAdvancedDateUpdate = function () { //show Last Advanced date changes
                $scope.new_adv_date = "";
                $scope.lst_adv_date = "";
                $("#updateRankModal").modal('hide');
                $("#ActivelastAdvancedDateModal").modal('show');
                $("#ActivelastAdvancedDatetitle").text('Update');
        };
        
        
        $scope.cancelRankChange=function(){
            $('#rank_value option').prop('selected', function() {
                return this.defaultSelected;
            });
        }
        
        $scope.updateMemRankDetails=function(lst_adv_date){
             $scope.new_rank_value = $scope.rank_name;
             $scope.new_adv_date = $scope.dateformatfilter(lst_adv_date);
            
            $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateParticipantMembershipRank',
                    data: {
                        "company_id": $localStorage.company_id,
                        "rank_id": $scope.rank_id,
                        "membership_reg_id":$scope.membership_reg_id,
                        "membership_rank": $scope.new_rank_value,
                        "advance_date": $scope.new_adv_date  
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        angular.element($scope.event_Rank_updt).text($scope.rank_name);
                        angular.element($scope.event_Rank_updt).attr('ng-click', "getRankDetails('" + $scope.rank_id + "','" + $scope.current_tab + "'," + $scope.m_id + "," + $scope.membership_reg_id + ",$event)");
                        $scope.las_adv.text($scope.last_advncedays_count($scope.new_adv_date));
                        $compile(angular.element(document.getElementById('DataTables_Table_30')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_31')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_32')).contents())($scope);
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text('Membership Rank successfully updated.');
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.clearTableData = function(){
                $scope.membership_array_running = []; 
                $scope.membership_array_running_show = [];
                $scope.running_length=0;
                $scope.membership_array_onhold = []; 
                $scope.membership_array_onhold_show = []; 
                $scope.onhold_length=0;
                $scope.membership_array_cancelled = []; 
                $scope.membership_array_cancelled_show = []; 
                $scope.cancelled_length=0;
                $scope.change_made = $scope.search_value = '';
            };
            
           $scope.activememtab = function(){
                $scope.activemembers = true;
                $scope.cancelmembers = false;
                $scope.onholdmembers= false;
                $scope.clearTableData();
                $scope.resettabbarclass();
                $scope.clearMemSelectedList();
                $('#li-orders').addClass('active-event-list').removeClass('event-list-title');
                $('.managemembersub-tabtext-1').addClass('greentab-text-active');
                $scope.getMemCustomer_list($scope.membershipcategory_id, $scope.membershipoption_id, 'R');
                $scope.dtOptions
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
//                $scope.scrollByManageTableId($scope.membershipcategory_id, $scope.membershipoption_id, 'member_table_active', 'R');
            };
            
             $scope.onholdmemtab = function(){
                $scope.activemembers = false;
                $scope.cancelmembers = false;
                $scope.onholdmembers=true;
                $scope.clearTableData();
                $scope.resettabbarclass();
                $scope.clearMemSelectedList();
                $('#li-onhold').addClass('active-event-list').removeClass('event-list-title');
                $('.managemembersub-tabtext-2').addClass('greentab-text-active');
                $scope.getMemCustomer_list($scope.membershipcategory_id, $scope.membershipoption_id, 'P');
                $scope.dtOptions1
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
//                $scope.scrollByManageTableId($scope.membershipcategory_id, $scope.membershipoption_id, 'member_table_onhold', 'P');
            };
            
             $scope.cancelmemtab = function(){
                $scope.activemembers = false;
                $scope.cancelmembers = true;
                $scope.onholdmembers= false;
                $scope.clearTableData();
                $scope.resettabbarclass();
                $scope.clearMemSelectedList();
                $('#li-cancelorders').addClass('active-event-list').removeClass('event-list-title');
                $('.managemembersub-tabtext-3').addClass('greentab-text-active');
                $scope.getMemCustomer_list($scope.membershipcategory_id, $scope.membershipoption_id, 'C');
                $scope.dtOptions2
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
//                $scope.scrollByManageTableId($scope.membershipcategory_id, $scope.membershipoption_id, 'member_table_cancelled', 'C');
            };
            

            $scope.closemanagemembershipview = function (currentpage) { 
                $scope.addfromlist = false;
                $localStorage.addmembershipfrommemberstab = false;
                if (currentpage === 'SM') {
                    if ($scope.membershipcategorystatus === "P") {
                        $('#li-live').addClass('active-event-list').removeClass('event-list-title');
                    } else {
                        $('#li-draft').addClass('active-event-list').removeClass('event-list-title');
                    }
                    $localStorage.membershippagetype = '';
                    $localStorage.page_from = '';
                    $scope.mainmembershipview = true;
                    $scope.childmembershipview = true;
                    $scope.showmembershipsublist = false;
                    $scope.addmemparticipantview = false;
                    this.childmembershiparray = $scope.childmembershiparray_copy;
                } else {
                    $("#CopyCategorymessageModal").modal('hide');
                    $localStorage.membershippagetype = '';
                    $localStorage.page_from = '';
                    if ($scope.membershipcategorystatus === "P") {
                        $scope.livemembershiplistview();
                    }else{
                        $scope.draftmembershiplistview();
                    }
//                    setTimeout(function () {
//                        $route.reload();
//                    }, 500);
                }
            };
        
        //MEMBERSIP SUB LIST TAB
        //RESET TAB BAR HIGHLETED TO RECENT TAB ACCESS
            $scope.customerstab = function () {
            $scope.customers = true;
            $scope.payments = false;
            $scope.appusers = false;
            $scope.resettabbarclass();
            $('#li-customers').addClass('active-event-list').removeClass('event-list-title');
        };

        //RESET TAB BAR HIGHLETED TO RECENT TAB ACCESS
        $scope.paymentstab = function () {
            $scope.customers = false;
            $scope.payments = true;
            $scope.appusers = false;
            $scope.resettabbarclass();
            $('#li-payments').addClass('active-event-list').removeClass('event-list-title');
        };

        //RESET TAB BAR HIGHLETED TO RECENT TAB ACCESS
        $scope.appuserstab = function () {
            $scope.customers = false;
            $scope.payments = false;
            $scope.appusers = true;
            $scope.resettabbarclass();
            $('#li-appusers').addClass('active-event-list').removeClass('event-list-title');
        };
        
        $scope.liveexportcsvmembership = function(membership_id, membership_option_id, type, membership_title){
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'exportMembershipData',
                responseType: 'arraybuffer',
                data: {
                    "company_id": $localStorage.company_id,
                    "membership_id" : membership_id,
                    "membership_option_id" : membership_option_id,
                    "type" : type
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            }).success(function (data, status, headers) {
                var response = '';
                if ('TextDecoder' in window) {
                    // Decode as UTF-8
                    var dataView = new DataView(data);
                    var decoder = new TextDecoder('utf8');
                    if($scope.isJson(decoder.decode(dataView))===true){
                        response = JSON.parse(decoder.decode(dataView));
                    }
                } else {
                    // Fallback decode as ASCII
                    var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                    if($scope.isJson(decodedString)===true){
                        response = JSON.parse(decodedString);
                    }
                }
                if(response.status !== undefined && response.status==='Failed'){
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text(response.msg);
                    $('#progress-full').hide();
                    return false;
                }else{
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text("Participant Details exported as csv file successfully.");
                }
                var msg = '';
                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], {type: "application/x-download"});
                    var objectUrl = URL.createObjectURL(blob);
                    linkElement.setAttribute('href', objectUrl);
                    linkElement.setAttribute("download", membership_title+".csv"); 
                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    msg = ex;
                    console.log(ex);
                }
                if(msg===''){
                    msg = "CSV file downloaded Successfully";
                }
                $('#progress-full').hide();
                $("#pushmessageModal").modal('show');
                $("#pushmessagetitle").text('Message');
                $("#pushmessagecontent").text(msg);

            }).error(function () {
                $('#progress-full').hide();
                $("#pushmessageModal").modal('show');
                $("#pushmessagetitle").text('Message');
                $("#pushmessagecontent").text("Unable to reach Server, Please Try again.");
            });
        };
        
        
            $scope.check_all_manage_active = false;
            $scope.check_all_manage_onhold = false;
            $scope.check_all_manage_cancelled = false;

            $scope.clearMemSelectedList = function () {
                $scope.removeAttachfile();
                $scope.grppushmsgdata = $scope.grpemailsubjectdata = $scope.grpemailmsgdata = '';
                $scope.also_send_email = false;
                if($scope.clearfromindividual){//clear if modal open from individual
                   $scope.selectedData = [];
                }
                $scope.clearfromindividual = false; 
            }; 
        
            $scope.removeAttachfile = function () {
                $scope.maxEmailAttachmentSizeError = false;
                $scope.filepreview = ""; 
                $scope.attachfile = '';
                $scope.file_name = '';
                $scope.fileurl = '';
            };
           
            $scope.exportcheckedmembershipexcel = function () {
             function formatTodayDate() {
                    var d = new Date,
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }
                 angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                    console.log('dataTables search : ' + $scope.searchTerm);
                })
                $scope.today_date = formatTodayDate();
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'createMemSelectonCSV',
                    responseType: 'arraybuffer',
                    data: {
                        "company_id": $localStorage.company_id,
                        "selected_membership_list": $scope.selectedData,
                        "all_select_flag":$scope.all_select_flag,
                        "membership_option_id":$scope.membership_ot_id,
                        "from":"managemembership",
                        "membership_status": $scope.current_tab,
                        "search":$scope.searchTerm
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).success(function (data, status, headers) {
                    $scope.clearMemSelectedList();
                    var response = '';
                    if ('TextDecoder' in window) {
                        // DECODE AS UTF-8
                        var dataView = new DataView(data);
                        var decoder = new TextDecoder('utf8');
                        if ($scope.isJson(decoder.decode(dataView)) === true) {
                            response = JSON.parse(decoder.decode(dataView));
                        }
                    } else {
                        // FALLBACK DECODE AS ASCII
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                        if ($scope.isJson(decodedString) === true) {
                            response = JSON.parse(decodedString);
                        }
                    }
                    if (response.status !== undefined && response.status === 'Failed') {
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.msg);                        
                        return false;
                    } else {
                        $scope.clearMemSelectedList();
                        $('#progress-full').hide();
                        $("#msgModal").modal('show');
                        $("#msgtitle").text('Message');
                        $("#msgcontent").text("Participant Details exported as csv file successfully.");
                    }
                    var msg = '';
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], {type: "application/x-download"});
                        var objectUrl = URL.createObjectURL(blob);
                        linkElement.setAttribute('href', objectUrl);
                        linkElement.setAttribute("download", "Membership_" + $scope.today_date + ".csv");
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        msg = ex;
                        console.log(ex);
                    }
                    if (msg === '') {
                        msg = "CSV file downloaded Successfully";
                    }
                    $scope.clearMemSelectedList();
                    $('#progress-full').hide();
                    $("#msgModal").modal('show');
                    $("#msgtitle").text('Message');
                    $("#msgcontent").text(response.data.msg);

                }).error(function () {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.isJson = function (str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            };
            
            
            $scope.sendMemindividualpush = function (selectedMember, type) {  // INDIVIDUAL PUSH MESSAGE
                $scope.clearfromindividual = true; 
                $scope.selectAll = false;
                $scope.toggleAll(false,$scope.maintainselected);
                $scope.message_type = type;
                 $scope.selectedData = [];
                $scope.selectedData.push({"id":selectedMember});
                $scope.membershiplistcontents.push(selectedMember);
               
                $scope.grpbuttonText = "Send"; 
                if (type === 'push') {
                    $("#grpMempushmessagecontModal").modal('show');
                    $("#grpMempushmessageconttitle").text('Send Push Message');
                } else {
                    $("#grpMememailmessagecontModal").modal('show');
                    $("#grpMememailmessageconttitle").text('Send Email');
                    $("#message_content_1_desclink").hide();
                    $("#message_content_1_maillink").show();
                }
            };
            
            $scope.sendMemGroupMsg = function (type) { 
              console.log($scope.membership_ot_id);
                $scope.message_type = type;
                $scope.grpbuttonText = "Send";
                if ($scope.selectedData.length > 0 || $scope.all_select_flag==='Y') {
                    if (type === 'push') {
//                        for (i = 0; i < $scope.membershiplistcontents.length; i++) {
//                            if ($scope.membershiplistcontents[i].show_icon === 'N') {
//                                $("#msgModal").modal('show');
//                                $("#msgtitle").text('Message');
//                                $("#msgcontent").text('One of the selected member doesnot have registered mobile number to receive push message. Kindly deselect the member who didnot registered mobile number.');
//                                return;
//                            } else {
//                                //SELECTED MEMBER HAS MOBILE DEVICE
//                            }
//                        }
                        $("#grpMempushmessagecontModal").modal('show');
                        $("#grpMempushmessageconttitle").text('Send Push Message');
                    } else if (type === 'mail') {
                        $("#grpMememailmessagecontModal").modal('show');
                        $("#grpMememailmessageconttitle").text('Send Email');
                        $("#message_content_1_desclink").hide();
                        $("#message_content_1_maillink").show();
                    } else if (type === 'csv') {
                        $("#grpMemcsvdownloadModal").modal('show');
                        $("#grpMemcsvdownloadtitle").text('Message');
                        $("#grpMemcsvdownloadcontent").text('Do you want to download csv file for selected members?');
                    }
                } else {
                    var x = document.getElementById("memberselection")
                    x.className = "show";
                    setTimeout(function () {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                }
            }

            $scope.sendGroupMembershipEmailPush = function (message_type) {
                $('#progress-full').show();
                var msg,unsubscribe_email = '';
                var subject = '';
                if (message_type === 'push') {
                    if ($scope.grppushmsgdata.trim().length === 0) {
                        return;
                    }
                    subject = '';
                    msg = $scope.grppushmsgdata;
                } else if (message_type === 'mail') {
                    if ($scope.grpemailmsgdata.trim().length === 0 || $scope.grpemailsubjectdata.trim().length === 0) {
                        return;
                    }
                    subject = $scope.grpemailsubjectdata;
                    msg = $scope.grpemailmsgdata;
                    
                    if($scope.attachfile !== '' && $scope.attachfile !== undefined && $scope.attachfile !== null){
                        $scope.fileurl = $scope.filepreview.substr($scope.filepreview.indexOf(",") + 1);
                        $scope.file_name = $scope.attachfile.split("fakepath")[1].substr(1);
                    }else{
                        $scope.fileurl = '';
                        $scope.file_name = '';
                    }
                }else if(message_type === 'subscribe'){
                    subject = 'Subscribe to our email communications!';
                    msg = 'click here to subscribe';
                    unsubscribe_email = $scope.unsubscribe_email;
                }
//                 angular.element('body').on('search.dt', function () {
//                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
//                    console.log('dataTables search : ' + $scope.searchTerm);
//                })
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendMembershipGroupPush',
                    data: {
                        "company_id": $localStorage.company_id,
                        "type": message_type,
                        "subject": subject,
                        "message": msg,
                        "also_send_mail": $scope.also_send_email ? 'Y' : 'N',
                        "selected_membership_list": $scope.selectedData,
                        "file_name": $scope.file_name,
                        "attached_file": $scope.fileurl,
                        "all_select_flag":$scope.all_select_flag,
                        "membership_option_id":$scope.membership_ot_id,
                        "from":"managemembership",
                        "membership_status": $scope.current_tab,
                        "search": $scope.searchTerm,
                        "unsubscribe_email":unsubscribe_email

                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {  
                        $("#sendsubscriptioninviteModal").modal('hide');
                        $scope.clearMemSelectedList();
                         $scope.color_green = 'N';
                        $('#progress-full').hide();
                        $("#grpMememailmessagecontModal").modal('hide');
                        $("#grpMememailmessageconttitle").text('');
                        $("#grpMempushmessagecontModal").modal('hide');
                        $("#grpMempushmessageconttitle").text('');
                        $("#msgModal").modal('show');
                        $("#msgtitle").text('Message');
                        $("#msgcontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Failed') {
                        $("#sendsubscriptioninviteModal").modal('hide');
                        console.log(response.data);
                         $scope.clearMemSelectedList();
                        $("#grpMememailmessagecontModal").modal('hide');
                        $("#grpMememailmessageconttitle").text('');
                        $("#grpMempushmessagecontModal").modal('hide');
                        $("#grpMempushmessageconttitle").text('');
                        $('#progress-full').hide();
                        $("#msgModal").modal('show');
                        $("#msgtitle").text('Message');
                        $("#msgcontent").text(response.data.msg);
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text('Invalid server response');
                    }                   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.maintain_SelectedMemberList = function (tab) {
                // FOR CHECK BOX FUNCTIONALITIES 
                if (tab === 'R') {
                    for (i = 0; i < $scope.membership_array_running_show.length; i++) {
                        for (j = 0; j < $scope.membershiplistcontents.length; j++) {
                            if ($scope.membership_array_running_show[i].membership_registration_id === $scope.membershiplistcontents[j].membership_registration_id) {
                                var mem_checkbox_id = 'individual_check_manage_active' + i;
                                $("#" + mem_checkbox_id).prop("checked", true);
                            }
                        }
                    }
                    if ($scope.check_all_manage_active) {
                        $scope.membershiplistcontents = [];
                        for (i = 0; i < $scope.membership_array_running_show.length; i++) {
                            $scope.membershiplistcontents.push($scope.membership_array_running_show[i]);
                        }
                    }
                } else if (tab === 'P') {
                    for (i = 0; i < $scope.membership_array_onhold_show.length; i++) {
                        for (j = 0; j < $scope.membershiplistcontents.length; j++) {
                            if ($scope.membership_array_onhold_show[i].membership_registration_id === $scope.membershiplistcontents[j].membership_registration_id) {
                                var mem_checkbox_id = 'individual_check_manage_onhold' + i;
                                $("#" + mem_checkbox_id).prop("checked", true);
                            }
                        }
                    }
                    if ($scope.check_all_manage_onhold) {
                        $scope.membershiplistcontents = [];
                        for (i = 0; i < $scope.membership_array_onhold_show.length; i++) {
                            $scope.membershiplistcontents.push($scope.membership_array_onhold_show[i]);
                        }
                    }
                } else {
                    for (i = 0; i < $scope.membership_array_cancelled_show.length; i++) {
                        for (j = 0; j < $scope.membershiplistcontents.length; j++) {
                            if ($scope.membership_array_cancelled_show[i].membership_registration_id === $scope.membershiplistcontents[j].membership_registration_id) {
                                var mem_checkbox_id = 'individual_check_manage_cancelled' + i;
                                $("#" + mem_checkbox_id).prop("checked", true);
                            }
                        }
                    }
                    if ($scope.check_all_manage_cancelled) {
                        $scope.membershiplistcontents = [];
                        for (i = 0; i < $scope.membership_array_cancelled_show.length; i++) {
                            $scope.membershiplistcontents.push($scope.membership_array_cancelled_show[i]);
                        }
                    }
                }

            };
            
            $scope.dateformatfilter = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }

            }; 
            
            // MULTIPLE SECTION OF MEMBERS
            $scope.selectedmanagemembershiplist = function (selectall, selectedItem, tab, current_table_id) {
                console.log("selectall: "+selectall+' tab '+tab);
                if (selectedItem === '') {
                    if (!selectall) {
                        if (tab === 'R') {
                            selectall = $scope.check_all_manage_active = true;
                        } else if (tab === 'P') {
                            selectall = $scope.check_all_manage_onhold = true;
                        } else {
                            selectall = $scope.check_all_manage_cancelled = true;
                        }
                    } else {
                        if (tab === 'R') {
                            selectall = $scope.check_all_manage_active = false;
                        } else if (tab === 'P') {
                            selectall = $scope.check_all_manage_onhold = false;
                        } else {
                            selectall = $scope.check_all_manage_cancelled = false;
                        }
                    }
                    
                    if(selectall){
                        if (tab === 'R') {
                                $scope.membershiplistcontents = [];
                                for (i = 0; i < $scope.membership_array_running_show.length; i++) {
                                    var reg_manage_element_id = current_table_id+''+$scope.membership_array_running_show[i].membership_registration_id;                                     
                                    if(document.getElementById(reg_manage_element_id) !== null){
                                        $scope.membershiplistcontents.push($scope.membership_array_running_show[i]);
                                    }
                                }
                        } else if (tab === 'P') {
                            $scope.membershiplistcontents = [];
                            for (i = 0; i < $scope.membership_array_onhold_show.length; i++) {
                                var reg_manage_element_id = current_table_id+''+$scope.membership_array_onhold_show[i].membership_registration_id;
                                if(document.getElementById(reg_manage_element_id) !== null){
                                    $scope.membershiplistcontents.push($scope.membership_array_onhold_show[i]);
                                }
                            }
                        } else {
                            $scope.membershiplistcontents = [];
                            for (i = 0; i < $scope.membership_array_cancelled_show.length; i++) {
                                var reg_manage_element_id = current_table_id+''+$scope.membership_array_cancelled_show[i].membership_registration_id;
                                if(document.getElementById(reg_manage_element_id) !== null){
                                    $scope.membershiplistcontents.push($scope.membership_array_cancelled_show[i]);
                                }
                            }
                        }
                    }else{
                        $scope.membershiplistcontents = [];                        
                    }
                    
                    
                } else {
                    var index = $scope.membershiplistcontents.indexOf(selectedItem);
                    if (index == -1) {
                      $scope.membershiplistcontents.push(selectedItem);
                    } else {
                      $scope.membershiplistcontents.splice(index, 1);
                    }
                }
                console.log($scope.membershiplistcontents.length+'  :: '+'2 selectedmembershiplist '+selectall);
            };  
            
            $scope.openAddnewMembershipModal = function () {
                $('#addnewmembership-modal').modal('show');
            };
            
            $scope.openaddmemmodal = function (list) {
                if((!$scope.stripe_enabled && $scope.stripestatus === 'N') && (!$scope.wepay_enabled && $scope.wepaystatus === 'N')){
                    $scope.showWepayAccountCreation('managemembership');
                }else{                    
                    $('#addnewmembershipmodal-modal').modal('show');
                    $scope.modalarrayregister = list;
                    $scope.membershipcatergorytitles = list.category_title;
                    
                    $('#progress-full').show();
                    $http({
                    method: 'GET',
                    url: urlservice.url + 'getMembershipCategory',
                    params: {
                        "company_id": $localStorage.company_id,
                        "membership_id": list.membership_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true

                    }).then(function (response) {
                        if (response.data.status === 'Success') {
                            $scope.modaloptionarray = response.data.msg.membership_options;
                            $('#progress-full').hide();
                        } else if (response.data.status === 'Expired') {
                            console.log(response.data);
                            $('#progress-full').hide();
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Message');
                            $("#messagecontent").text(response.data.msg);
                            $scope.logout();
                        } else if (response.data.status === 'Version') {
                            $('#progress-full').hide();
                            $scope.handleFailure(response.data.msg);
                        } else {
                            $('#progress-full').hide();
                        }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    }); 
                }
            };
            $scope.catchmembershipoptionstwo = function(optionid){
                for(var k=0;k<$scope.modaloptionarray.length;k++){
                    if($scope.modaloptionarray[k].membership_option_id == optionid){
                        $scope.modaloptionarrayregister = $scope.modaloptionarray[k];
                    }
                }
            };
            $scope.addmewmemfrmmodal = function () {
                $('#addnewmembershipmodal-modal').modal('hide');
                $scope.addfromlist = true;
                $scope.managemembership($scope.modalarrayregister);                
                $scope.managechildmembership($scope.modaloptionarrayregister);
            };
            
            $scope.addfrommemoptions = function (list) {
                if((!$scope.stripe_enabled && $scope.stripestatus === 'N') && (!$scope.wepay_enabled && $scope.wepaystatus === 'N')){
                    $scope.showWepayAccountCreation('managemembership');
                }else{ 
                    $scope.addfromlist = true;
                    $scope.managechildmembership(list);
                }
            }
            
            $scope.cancelregister = function () {
                $scope.addmemparticipantview = false;
                $scope.showmembershipsublist = true;
                if($localStorage.addmembershipfrommemberstab === true){
                    $location.path("/customers")
                }else{
                    $scope.getMemCustomer_list($scope.membershipcategory_id, $scope.membershipoption_id, 'R');
                }
            };
            
            $scope.addmembership = function () {
                $('#addnewmembership-modal').modal('hide');
                $localStorage.membershippagetype = 'new';
                $localStorage.membershiplocaldata = "";
                $localStorage.membership_details = "";
                $localStorage.isAddedCategoryDetails = "N";
                $localStorage.currentpage = "addmembership";
                $localStorage.preview_membershipcontent = '';
                $('#addnewmembership-modal').on('hidden.bs.modal', function (e) {
                   $window.location.href = window.location.origin+window.location.pathname+'#/addmembership';
               })
            };
            
            $scope.templatemembership = function () {
                $('#addnewmembership-modal').modal('hide');
                $localStorage.currentpage = '';
                $('#addnewmembership-modal').on('hidden.bs.modal', function (e) {
                   $window.location.href = window.location.origin+window.location.pathname+'#/templatemembership';
               })
            };
            
            if ($localStorage.membershippagetype !== 'transfer' && $localStorage.page_from !== "manage_members_active" && $localStorage.page_from !== "manage_members_onhold" && $localStorage.page_from !== "manage_members_cancelled") {
                $scope.getmembershipdetails('P');
            }else if($localStorage.page_from === "manage_members_active" || $localStorage.page_from === "manage_members_onhold" || $localStorage.page_from === "manage_members_cancelled") {
                $scope.current_manage_mem_opt_id  = $localStorage.currentMembership_option_id;
                $scope.current_MembershipDetails = $localStorage.currentManageMembershipDetails;
                $localStorage.currentManageMembershipDetails = $localStorage.currentMembership_option_id = '';
                
                $scope.managemembership($scope.current_MembershipDetails);
                
            }else if ($localStorage.membershippagetype === 'transfer') { // membership transfering from dashboard membership detail screen
                $localStorage.currentpage = "transfermembership";
                $scope.membership_transfer_reg_id = $localStorage.currentMembershipregid;
                $scope.selected_membership_option_details = $scope.membership_detail =  '';
                $scope.edit_mem_transfer = true;
                
                if ($localStorage.membershiptransferlocaldata === undefined || $localStorage.membershiptransferlocaldata === "") {
                    $localStorage.membershiptransferlocaldata = membershipTransferDetails.getTransferMembershipDetail(); 
                    $scope.membership_transfer_data = $localStorage.membershiptransferlocaldata;
                    $localStorage.preview_membershipcontent = $scope.membership_detail = $scope.membership_transfer_data[0][0].selected_membership_category;
                    $scope.selected_membership_option_details = $scope.membership_transfer_data[0][1].selected_membership_option;
                } else {
                    $scope.membership_transfer_data = $localStorage.membershiptransferlocaldata;
                    $localStorage.preview_membershipcontent = $scope.membership_detail = $scope.membership_transfer_data[0][0].selected_membership_category;
                    $scope.selected_membership_option_details = $scope.membership_transfer_data[0][1].selected_membership_option;
                } 
                
                $scope.selected_membership_option_details_copy = angular.copy($scope.selected_membership_option_details);
                $scope.membershipcategory_id = $scope.membership_detail.membership_id;
                $scope.membershipcategorytitle = $scope.membership_detail.category_title;
                $scope.membershipcategorystatus = $scope.membership_detail.category_status;
                $scope.rank_details = $scope.membership_detail.rank_details;
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'membersRegDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "selected_membership_id" : $scope.membership_transfer_reg_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {                       
                    if (response.data.status === 'Success') {
                        $scope.membership_reg_detail = response.data.msg;                        
                        $scope.participant_info = $scope.membership_reg_detail.reg_columns;
                        $scope.carddetails = $scope.participant_info.card_details;
                        $scope.memeditparticipant = angular.copy($scope.participant_info);                        
                        $scope.managechildmembership($scope.selected_membership_option_details);
                        $('#progress-full').hide();
                        
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text('Invalid server response');
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
                
            }else{
                 $('#progress-full').show();
            }
            
            if($localStorage.redirectedfrmtemplate) {
                $("#membershiptemplatemessageModal").modal('show');
                $("#membershiptemplatemessagetitle").text('Message');
                $("#membershiptemplatemessagecontent").text('Membership category successfully copied inside "Draft" folder');
            }
            
            $scope.sendSubscribeEmail = function(mail){
                $scope.unsubscribe_email = mail;
                $scope.message_type = 'subscribe';
                $("#sendsubscriptioninviteModal").modal('show');
            };   
        
        } else {
            $location.path('/login');
        }
            
 }
    module.exports =  ManageMembershipController;

